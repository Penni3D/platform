/// <binding />
module.exports = function (grunt) {
	// load Grunt plugins from NPM
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-dev-update');
	grunt.loadNpmTasks('grunt-angular-templates');
	
	grunt.loadNpmTasks('grunt-ng-annotate');
	grunt.loadNpmTasks('grunt-ts');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-dom-munger');
	grunt.loadNpmTasks('grunt-cache-breaker');


	// configure plugins
	grunt.initConfig({
		ts: {
			"default": {
				"options": {
					"pretty": false,
					"strictNullChecks": false,
					"noImplicitAny": false,
					"noImplicitThis": false,
					"noEmitOnError": false,
					"failOnTypeErrors": false,
					"skipLibCheck": true,
					"target": "es5",
					"incremental": true,
					"tsBuildInfoFile": "./Cache",
					"outFile": "./wwwroot/private/app.js",
					"outDir": "./Cache",
					"fast": "never"
				},
				tsconfig: './tsconfig.json'
			}
		},
		ngAnnotate: {
			options: {
				singleQuotes: true
			},
			app: {
				files: {
					'wwwroot/private/customer.js': [
						'customer/**/*.module.js',
						'customer/**/*.state.js',
						'customer/**/*.provider.js',
						'customer/**/*.service.js',
						'customer/**/*.directive.js',
						'customer/**/*.controller.js',
						'customer/**/*.js'
					]
				}
			}
		},
		concat: {
			options: {
				stripBanners: {line: false, block: true}
			},
			libs: {
				src: [
					'node_modules/moment/min/moment.min.js',
					'node_modules/quill/dist/quill.min.js',
					'node_modules/lodash/lodash.min.js',
					'node_modules/jquery/dist/jquery.min.js',
					'node_modules/jquery-ui/jquery-ui.min.js',
					'node_modules/d3/build/d3.min.js',
					'node_modules/angular/angular.min.js',
					'node_modules/angular-messages/angular-messages.min.js',
					'node_modules/angular-aria/angular-aria.min.js',
					'node_modules/angular-file-saver/dist/angular-file-saver.bundle.min.js',
					'node_modules/chart.js/dist/Chart.min.js',
					'node_modules/angular-chart.js/dist/angular-chart.min.js',
					'node_modules/angular-cookies/angular-cookies.min.js',
					'node_modules/ng-file-upload/dist/ng-file-upload.min.js',
					'node_modules/angular-sanitize/angular-sanitize.min.js',
					'node_modules/cropper/dist/cropper.min.js',
					'node_modules/angular-touch/angular-touch.min.js',
					'node_modules/css-element-queries/src/ResizeSensor.js',
					'node_modules/css-element-queries/src/ElementQueries.js',
					'node_modules/file-saver/FileSaver.min.js',
					'node_modules/es6-promise/dist/es6-promise.min.js',
					'node_modules/es6-promise/dist/es6-promise.auto.min.js',
					'node_modules/html2canvas/dist/html2canvas.min.js',
					'node_modules/blueimp-canvas-to-blob/js/canvas-to-blob.min.js',
					'node_modules/@aspnet/signalr/dist/browser/signalr.min.js',
					'node_modules/chartjs-plugin-piechart-outlabels/dist/chartjs-plugin-piechart-outlabels.min.js',
					'platform/config/anchorme/anchorme.min.js',
					'node_modules/jspdf/dist/jspdf.min.js',
				],
				dest: 'wwwroot/public/lib/libs.js'
			}
		},
		uglify: {
			options: {
				mangle: true
			},
			scripts: {
				files: {
					'wwwroot/private/app.js': ['wwwroot/private/app.js']
				}
			}
		},
		clean: ['wwwroot/*'],
		copy: {
			main: {
				files: [
					{
						expand: true,
						cwd: 'node_modules',
						src: [
							'angular-material-icons/angular-material-icons.css',
							'flag-icon-css/css/flag-icon.min.css',
							'flexboxgrid/dist/flexboxgrid.min.css',
							'cropper/dist/cropper.min.css',
							'quill/dist/quill.snow.css'
						],
						dest: 'wwwroot/public/css/',
						filter: 'isFile',
						flatten: true
					},
					{
						expand: true,
						cwd: 'node_modules/flag-icon-css/flags',
						src: ['**/*'],
						dest: 'wwwroot/public/flags'
					},
					{
						expand: true,
						cwd: 'platform/resources/helper',
						src: ['**/*'],
						dest: 'wwwroot/public/help'
					},
					{
						expand: true,
						cwd: 'platform/resources/fonts',
						src: ['**/*'],
						dest: 'wwwroot/public/fonts'
					},
					{
						expand: true,
						cwd: 'customer/resources/images',
						src: ['**/*', '!favicon.ico'],
						dest: 'wwwroot/public/images'
					},
					{
						expand: true,
						cwd: 'platform/resources/images',
						src: ['**/*', '!favicon.ico'],
						dest: 'wwwroot/public/images'
					},
					{
						expand: true,
						cwd: 'platform/resources/images',
						src: ['favicon.ico'],
						dest: 'wwwroot/public/'
					},
					{
						expand: true,
						cwd: 'platform/config/jquery-ui',
						src: 'jquery-ui.min.js',
						dest: 'node_modules/jquery-ui'
					},
					{
						expand: true,
						cwd: 'platform/config/jquery-ui',
						src: 'jquery-ui.min.css',
						dest: 'wwwroot/public/css'
					},
					{
						expand: true,
						cwd: 'platform/resources/waves',
						src: ['**/*'],
						dest: 'wwwroot/public/css'
					}
				]
			},
			login: {
				files: [
					{
						expand: true,
						cwd: 'platform/client/login',
						src: ['**/*'],
						dest: 'wwwroot/public/login'
					}
				]
			},
			core: {
				files: [
					{
						expand: true,
						cwd: 'platform/client/core',
						src: 'index.html',
						dest: 'wwwroot/private'
					}
				]
			}
		},
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					sourcemap: 'none',
					noCache: true
				},
				files: {
					'wwwroot/public/css/keto.css': ['platform.scss'],
					'wwwroot/public/css/customer.css': ['customer.scss']
				}
			}
		},
		watch: {
			options: {
				interrupt: true,
				interval: 1314
			},
			css: {
				files: ['platform/resources/scss/**/*.scss', 'customer/scss/**/*.scss'],
				tasks: ['sass', 'cachebreaker']
			},
			tsscripts: {
				files: ['platform/client/**/*.ts'],
				tasks: ['ts:default', 'cachebreaker']
			},
			scripts: {
				files: ['customer/**/*.js'],
				tasks: ['ngAnnotate:app', 'cachebreaker']
			},
			login: {
				files: ['platform/client/login/**/*'],
				tasks: ['copy:login']
			},
			html: {
				files: ['platform/client/**/*.html', 'customer/**/*.html'],
				tasks: ['ngtemplates', 'copy:core']
			},
			config: {
				files: ['platform/config/config.*.json', 'customer/configs/config.*.json'],
				tasks: ['configuration:local']
			}
		},
		devUpdate: {
			main: {
				options: {
					updateType: 'report',
					packages: {
						dependencies: true
					}
				}
			}
		},
		ngtemplates: {
			core: {
				cwd: 'platform/client',
				src: ['**/*.html', '!login/**/*.html'],
				dest: 'wwwroot/private/app.templates.js',
				options: {
					htmlmin: {
						collapseBooleanAttributes: true,
						collapseWhitespace: true,
						removeAttributeQuotes: true,
						removeComments: true, // Only if you don't use comment directives!
						removeEmptyAttributes: true,
						removeRedundantAttributes: true,
						removeScriptTypeAttributes: true,
						removeStyleLinkTypeAttributes: true
					}
				}
			},
			customer: {
				src: ['customer/**/*.html'],
				dest: 'wwwroot/private/customer.templates.js',
				options: {
					htmlmin: {
						collapseBooleanAttributes: true,
						collapseWhitespace: true,
						removeAttributeQuotes: true,
						removeComments: true, // Only if you don't use comment directives!
						removeEmptyAttributes: true,
						removeRedundantAttributes: true,
						removeScriptTypeAttributes: true,
						removeStyleLinkTypeAttributes: true
					}
				}
			}
		},
		cachebreaker: {
			private: {
				options: {
					match: [
						{
							'app.js': 'wwwroot/private/app.js',
							'app.templates.js': 'wwwroot/private/app.templates.js',
							'customer.js': 'wwwroot/private/customer.js',
							'customer.templates.js': 'wwwroot/private/customer.templates.js',
							'keto.css': 'wwwroot/public/css/keto.css',
							'customer.css': 'wwwroot/public/css/customer.css'
						}
					],
					replacement: 'md5'
				},
				files: {
					src: ['wwwroot/private/index.html']
				}
			},
			public: {
				options: {
					match: [
						{
							'keto.css': 'wwwroot/public/css/keto.css',
							'basicLogin.js': 'wwwroot/public/login/basicLogin.js',
							'select.js': 'wwwroot/public/login/ui/select.js'
						}
					],
					replacement: 'md5'
				},
				files: {
					src: ['wwwroot/public/login/login.html']
				}
			}
		}
	});

	grunt.registerTask('configuration', '', function (arg) {
		var json = {};
		var environments = typeof arg === 'string' ? arg.split(',') : [];
		var finished = this.async();
		json.default = grunt.file.readJSON('platform/config/config.default.json');

		if (environments.length) {
			for (var i = 0, l = environments.length; i < l; i++) {
				json[environments[i]] = grunt.file.readJSON('customer/configs/config.' + environments[i] + '.json');
			}
		}

		var result = {"hidden": {}, "static": {}, "dynamic": {}, "domain": {}};

		function toResult(obj, attribute, param) {
			//Lowercase everything!
			attribute = attribute.toLowerCase();
			param = param.toLowerCase();


			//Set default type and format
			var type = typeof obj.type === 'undefined' ? 'static' : obj.type.toLowerCase();
			obj.format = typeof obj.format === 'undefined' ? 'string' : obj.format.toLowerCase();
			delete obj.type;

			//Delete from 'Dynamic' section if same parameter exists in 'Static' section
			function deleteDynamic() {
				if (typeof result['dynamic'][attribute] !== 'undefined' && typeof result['dynamic'][attribute][param] !== 'undefined') {
					delete result['dynamic'][attribute][param];

					//Delete attribute if it does not have any parameters
					if (Object.keys(result['dynamic'][attribute]).length === 0 && result['dynamic'][attribute].constructor === Object) {
						delete result['dynamic'][attribute];
					}
				}
			}

			//Delete from 'Static' section if same parameter exists in 'Dynamic' section
			function deleteStatic() {
				if (typeof result['static'][attribute] !== 'undefined' && typeof result['static'][attribute][param] !== 'undefined') {
					delete result['static'][attribute][param];

					//Delete attribute if it does not have any parameters
					if (Object.keys(result['static'][attribute]).length === 0 && result['static'][attribute].constructor === Object) {
						delete result['static'][attribute];
					}
				}
			}

			//Initialize Config.json with empty attributes
			if (typeof result[type][attribute] === 'undefined') {
				result[type][attribute] = {};
			}

			if (typeof result['hidden'][attribute] !== 'undefined' && typeof result['hidden'][attribute][param] !== 'undefined') {
				type = 'hidden';

				deleteDynamic();
				deleteStatic();
			} else {
				if (type === 'static') {
					deleteDynamic();

				} else {
					deleteStatic();
				}
			}

			result[type][attribute][param] = obj;
		}

		function toResultDomain(obj, domain, attribute, param) {
			//Lowercase everything!
			attribute = attribute.toLowerCase();
			param = param.toLowerCase();

			//Set default type and format
			var type = typeof obj.type === 'undefined' ? 'static' : obj.type.toLowerCase();
			obj.format = typeof obj.format === 'undefined' ? 'string' : obj.format.toLowerCase();
			delete obj.type;

			//Initialize Config.json with empty attributes
			if (typeof result['domain'][domain] === 'undefined') {
				result['domain'][domain] = {"static": {}, "dynamic": {}, 'hidden': {}}
			}
			if (typeof result['domain'][domain][type][attribute] === 'undefined') {
				result['domain'][domain][type][attribute] = {}
			}


			//Delete from 'Dynamic' section if same parameter exists in 'Static' section
			function deleteDynamic() {
				if (typeof result['domain'][domain]['dynamic'][attribute] !== 'undefined' && typeof result['domain'][domain]['dynamic'][attribute][param] !== 'undefined') {
					delete result['domain'][domain]['dynamic'][attribute][param];

					//Delete attribute if it does not have any parameters
					if (Object.keys(result['domain'][domain]['dynamic'][attribute]).length === 0 && result['domain'][domain]['dynamic'][attribute].constructor === Object) {
						delete result['domain'][domain]['dynamic'][attribute];
					}
				}
			}

			//Delete from 'Static' section if same parameter exists in 'Dynamic' section
			function deleteStatic() {
				if (typeof result['domain'][domain]['static'][attribute] !== 'undefined' && typeof result['domain'][domain]['static'][attribute][param] !== 'undefined') {
					delete result['domain'][domain]['static'][attribute][param];

					//Delete attribute if it does not have any parameters
					if (Object.keys(result['domain'][domain]['static'][attribute]).length === 0 && result['domain'][domain]['static'][attribute].constructor === Object) {
						delete result['domain'][domain]['static'][attribute]
					}
				}
			}

			//Hidden fields cannot be overwritten with another type
			if (typeof result['domain'][domain]['hidden'][attribute] !== 'undefined' && typeof result['domain'][domain]['hidden'][attribute][param] !== 'undefined') {
				type = 'hidden';
				deleteDynamic();
				deleteStatic();
			} else {
				if (type === 'static') {
					deleteDynamic();
				} else {
					deleteStatic();
				}
			}

			result['domain'][domain][type][attribute][param] = obj;
		}

		//Loop through default configurations
		for (var i in json.default) {
			var group = json.default[i];
			for (var j in group) {
				var def = group[j];
				for (var k in def) {
					if (i.toLowerCase() !== 'domain') {
						toResult(def[k], j, k);
					} else {
						var attr = def[k];
						for (var l in attr) {
							toResultDomain(attr[l], j, k, l);
						}
					}
				}
			}
		}

		if (environments.length) {
			for (var i = 0, l = environments.length; i < l; i++) {
				for (var j in json[environments[i]]) {
					var group = json[environments[i]][j];
					for (var k in group) {
						var test = group[k];
						for (var l in test) {
							if (j.toLowerCase() !== 'domain') {
								toResult(test[l], k, l);
							} else {
								var attr = test[l];
								for (var m in attr) {
									toResultDomain(attr[m], k, l, m);
								}
							}
						}
					}
				}
			}
		}

		//Extend domain-specific configurations with 'Default' ones.
		(function combineDefaultDomain() {
			var r = result['domain'];
			for (var i in r) {
				if (r[i] !== 'default') {
					var d = JSON.parse(JSON.stringify(r['default']));
					var t = r[i]
					for (var j in t) {
						var p = t[j]
						for (var k in p) {
							var a = p[k]
							for (var l in a) {

								//Delete from 'Dynamic' section if same parameter exists in 'Static' section
								var deleteDynamic = function () {
									if (typeof d['dynamic'][k] !== 'undefined' && typeof d['dynamic'][k][l] !== 'undefined') {
										delete d['dynamic'][k][l];

										//Delete attribute if it does not have any parameters
										if (Object.keys(d['dynamic'][k]).length === 0 && d['dynamic'][k].constructor === Object) {
											delete d['dynamic'][k];
										}

									}
								};

								//Delete from 'Static' section if same parameter exists in 'Dynamic' section
								var deleteStatic = function () {
									if (typeof d['static'][k] !== 'undefined' && typeof d['static'][k][l] !== 'undefined') {
										delete d['static'][k][l];

										//Delete attribute if it does not have any parameters
										if (Object.keys(d['static'][k]).length === 0 && d['static'][k].constructor === Object) {
											delete d['static'][k];
										}
									}
								};

								//Replace default configuration with Domain-specific ones
								if (typeof d[j][k] === 'undefined') {
									d[j][k] = a;
								} else {
									d[j][k][l] = a[l];
								}

								if (typeof d['hidden'][k] !== 'undefined' && typeof d['hidden'][k][l] !== 'undefined') {
									j = 'hidden';
									deleteDynamic();
									deleteStatic();
								} else {
									if (j === 'static') {
										deleteDynamic();
									} else {
										deleteStatic();
									}
								}
							}
						}
					}
					//Add final domain-specific configurations to config.json
					result['domain'][i] = d;
				}
			}
		})();
		var basePath = result['static']['hostingenvironment']['virtualdirectory']['value'];
		if (typeof basePath === 'undefined' || basePath === '') {
			basePath = '/';
		}

		grunt.config.set("basePath", basePath);
		grunt.file.write('wwwroot/private/config.json', JSON.stringify(result));
		finished();
	});

	// define tasks
	grunt.registerTask('initRoot',
		[
			'clean',
			'configuration:local',
			'copy',
			'ts',
			'ngAnnotate',
			'concat',
			'sass',
			'ngtemplates',
			'cachebreaker'
		]
	);
	grunt.registerTask('initRoot+watcher',
		[
			'clean',
			'configuration:local',
			'copy',
			'ts',
			'ngAnnotate',
			'concat',
			'sass',
			'ngtemplates',
			'cachebreaker',
			'watch'
		]
	);
	grunt.registerTask('watcher', ['watch']);
	grunt.registerTask('publish', '', function (argument) {
		var c = 'configuration';

		if (typeof argument === 'string') {
			c += ':' + argument;
		}

		grunt.task.run([
			'clean',
			c,
			'copy',
			'ts',
			'ngAnnotate',
			'concat',
			'sass',
			'ngtemplates',
			'cachebreaker'
		]);
	});
};
