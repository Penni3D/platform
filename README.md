# Keto 5 - The Product

Welcome to the world of amazingness

## Setup
* Clone this repository into wanted folder
* Enter the folder and clone [customer](<https://bitbucket.org/ketosoftware/customer>) repository into subfolder named 'customer' (which will be default name)

* Install all required stuff: dotnet, npm, grunt, some OS like Windows, Linux or Mac OS(if feeling adventurous), etc.

###Install packages
Install npm components
```
npm install
```

Restore dotnet packages
```
dotnet restore
```


###Running
Find or craft proper **config.local.json** file and insert into **customer/configs**-folder

And then...
```
grunt initRoot
dotnet run
```


## Running the tests
Server tests
```
dotnet test
```

Client tests
```
grunt test
```

## When in doubt

* [Benjamin Särkkä](mailto:benjamin.sarkka@ketosoftware.com)
