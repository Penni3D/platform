CREATE NONCLUSTERED INDEX archive_item_data_process_selections_all
  ON [dbo].[archive_item_data_process_selections] ([item_column_id],[archive_end],[archive_start])
INCLUDE ([archive_id],[item_id],[selected_item_id])
  