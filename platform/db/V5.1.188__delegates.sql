DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
      --Get Group
      DECLARE @process_group_id INT 
      SET @process_group_id = (SELECT process_group_id FROM process_groups WHERE process='user' AND instance = @instance)
      
      --Create Tab
      DECLARE @new_tab_id INT
      EXEC app_ensure_tab @instance,'user','USER_TAB4', @process_group_id, @tab_id = @new_tab_id OUTPUT
      
      --Create new User process container
      DECLARE @process_container_id INT
      EXEC app_ensure_container @instance,'user','USERS_DELEGATES', @new_tab_id, 1, @container_id = @process_container_id OUTPUT
      EXEC app_ensure_column @instance, 'user', 'delegate_users', @process_container_id, 5, 1, 1, 0, null,  0, 0, 'user'   
    
    FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor


