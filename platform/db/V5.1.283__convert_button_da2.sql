--Updates all data additiona 2 fields for button data type
--Works only ONCE!
EXEC app_set_archive_user_id 0
DECLARE @json NVARCHAR(max) = '{ "initialState": "__1__", "currentState": "__2__", "optionalDoneDialog": "__3__", "viewActions": ["__4__"] }'
UPDATE item_columns SET
    data_additional2 =  (
        SELECT
            REPLACE(
                    REPLACE(
                            REPLACE(
                                    REPLACE(
                                            REPLACE(@json,'__1__',
                                                    CASE WHEN initialState IN ('close','refresh','refresh_all','new_item','new_item_replace','refresh_all_cache') THEN '' ELSE initialState END
                                                )
                                        ,'__2__', currentState)
                                ,'__3__', optionalDoneDialog)
                        ,'__4__', CASE WHEN viewActions NOT IN ('close','refresh','refresh_all','new_item','new_item_replace','refresh_all_cache') THEN '' ELSE viewActions END)
                ,'[""]','[]') AS result
        FROM (
                 SELECT
                     item_column_id,
                     CASE WHEN LEN(data_additional2) - LEN(REPLACE(data_additional2,';','')) = 1 THEN
                              PARSENAME(REPLACE(data_additional2,';','.'),2)
                          ELSE
                              PARSENAME(REPLACE(data_additional2,';','.'),3)
                         END AS initialState,
                     CASE WHEN LEN(data_additional2) - LEN(REPLACE(data_additional2,';','')) = 1 THEN
                              PARSENAME(REPLACE(data_additional2,';','.'),1)
                          ELSE
                              PARSENAME(REPLACE(data_additional2,';','.'),2)
                         END AS currentState,
                     CASE WHEN LEN(data_additional2) - LEN(REPLACE(data_additional2,';','')) = 1 THEN
                              ''
                          ELSE
                              PARSENAME(REPLACE(data_additional2,';','.'),1)
                         END AS optionalDoneDialog,
                     CASE WHEN LEN(data_additional2) - LEN(REPLACE(data_additional2,';','')) = 1 THEN
                              PARSENAME(REPLACE(data_additional2,';','.'),2)
                          ELSE
                              PARSENAME(REPLACE(data_additional2,';','.'),3)
                         END AS viewActions
                 FROM item_columns
             ) iq
        WHERE iq.item_column_id = item_columns.item_column_id
    ) WHERE data_type = 8