CREATE TABLE [dbo].[process_portfolio_timemachine_saved]( 
  [save_id] [int] IDENTITY(1,1) NOT NULL, 
  [instance] [nvarchar](10) NOT NULL,
  [name] [nvarchar](255) NOT NULL,   
  [process] [nvarchar](50) NOT NULL, 
  [date] [datetime] NOT NULL
 CONSTRAINT [PK_process_portfolio_timemachine_saved] PRIMARY KEY CLUSTERED  
( 
  [save_id] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
GO



