ALTER  TRIGGER [dbo].[processes_fk_delete]
    ON [dbo].[processes]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.parent_process = d.process
	DELETE dd FROM item_columns dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM item_columns dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.data_additional = d.process
    DELETE dd FROM item_tables dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM items dd						INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_groups	dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_tabs dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_containers dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_portfolios dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_tabs_subprocesses dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM conditions dd				
	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_actions dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_dashboard_charts dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process

	DELETE dd FROM instance_menu dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.params  LIKE '"process":"' + d.process + '"'
	
	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	
	DELETE dd FROM processes dd					INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process



GO 


ALTER TRIGGER [dbo].[process_groups_fk_delete]
    ON [dbo].[process_groups]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM condition_groups dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
    DELETE dd FROM process_group_tabs dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
    DELETE dd FROM process_portfolio_groups dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
	DELETE dd FROM process_group_features dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
	DELETE dd FROM process_group_navigation dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
	DELETE dd FROM process_groups dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id


GO 

ALTER TRIGGER [dbo].[process_tabs_fk_delete]
    ON [dbo].[process_tabs]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM condition_tabs dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_group_tabs dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tab_columns dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tab_containers dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tabs_subprocesses dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_group_navigation dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tabs dd			
			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id