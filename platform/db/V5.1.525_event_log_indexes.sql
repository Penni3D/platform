CREATE INDEX IX_occurence ON instance_logs(occurence)
GO
CREATE INDEX IX_log_type ON instance_logs(log_type)
GO
CREATE INDEX IX_log_severity ON instance_logs(log_severity)
GO
CREATE INDEX IX_log_category ON instance_logs(log_category)
GO
CREATE INDEX IX_user_item_id ON instance_logs(user_item_id)