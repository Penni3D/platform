IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_list_budget_item_type'))
  exec('CREATE PROCEDURE [dbo].[app_ensure_process_list_budget_item_type] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_list_budget_item_type]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'list_budget_item_type'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_list @instance, @process, @process, 0

    EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
  END
GO



IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_list_budget_category'))
  exec('CREATE PROCEDURE [dbo].[app_ensure_process_list_budget_category] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_list_budget_category]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'list_budget_category'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_list @instance, @process, @process, 0

    EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
  END

GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_budget_row'))
  exec('CREATE PROCEDURE [dbo].[app_ensure_process_budget_row] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_budget_row]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'budget_row'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_process @instance, @process, @process, 2

    EXEC app_ensure_column @instance, @process, 'project_item_id', 0, 1, 1, 1, 0, null,  0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'description', 0, 4, 1, 1, 0, NULL, 0, 0
    EXEC app_ensure_column @instance, @process, 'category_item_id', 0, 5, 1, 1, 0, null,  0, 0, 'list_budget_category'
  END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_budget_item'))
  exec('CREATE PROCEDURE [dbo].[app_ensure_process_budget_item] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_budget_item]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
BEGIN
  DECLARE @process NVARCHAR(50) = 'budget_item'

  --Archive user is 0
  DECLARE @BinVar varbinary(128);
  SET @BinVar = CONVERT(varbinary(128), 0);
  SET CONTEXT_INFO @BinVar;

  --Create process
  EXEC app_ensure_process @instance, @process, @process, 1

  --Create columns
  EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 5, 1, 1, 0, null,  0, 0, 'budget_row'
  EXEC app_ensure_column @instance, @process, 'type_item_id', 0, 5, 1, 1, 0, null,  0, 0, 'budget_item_type' 
  EXEC app_ensure_column @instance, @process, 'date', 0, 3, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'amount', 0, 2, 1, 1, 0, null,  0, 0
  -- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_item_id', 0, 6, 1, 1, 0, null,  0, 0, 'budget_currency'
  -- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_rate', 0, 2, 1, 1, 0, null,  0, 0
  -- add later when necessary: EXEC app_ensure_column @instance, @process, 'description', 0, 4, 1, 1, 0, null,  0, 0
END
GO

-- EXECUTE PROCEDURE FOR ALL INSTANCES
BEGIN
  DECLARE @instanceName NVARCHAR(10)
  DECLARE Budget_Cursor CURSOR FOR SELECT instance FROM instances
  DECLARE @tableName NVARCHAR(50) = ''
  DECLARE @sql NVARCHAR(MAX) = ''
  DECLARE @table NVARCHAR(MAX) = ''
  
  OPEN Budget_Cursor

  FETCH NEXT FROM Budget_Cursor INTO @instanceName

  WHILE @@fetch_status = 0
    BEGIN

      EXEC app_ensure_process_list_budget_category @instanceName
      EXEC app_ensure_process_list_budget_item_type @instanceName
      EXEC app_ensure_process_budget_row @instanceName
      EXEC app_ensure_process_budget_item @instanceName
      
      SET @table = 'list_budget_category'

      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      DECLARE @itemId INT = @@identity;
      SET @tableName = '_' + @instanceName + '_' + @table
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''ict'' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      exec (@sql)

      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      SET @itemId = @@identity;
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''expert_services'' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      EXEC (@sql)

      
      SET @table = 'list_budget_item_type'
      SET @tableName = '_' + @instanceName + '_' + @table

      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      SET @itemId = @@identity;
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''budget'' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      EXEC (@sql)
      
      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      SET @itemId = @@identity;
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''forecast'' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      EXEC (@sql)

      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      SET @itemId = @@identity;
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''actual'' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      EXEC (@sql)

      
      FETCH  NEXT FROM Budget_Cursor INTO
        @instanceName
    END
  CLOSE Budget_Cursor
  DEALLOCATE Budget_Cursor
END



