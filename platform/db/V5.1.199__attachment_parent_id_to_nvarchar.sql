ALTER TABLE attachments
ALTER COLUMN parent_id NVARCHAR(MAX)

ALTER TABLE archive_attachments
ALTER COLUMN parent_id NVARCHAR(MAX)

EXEC app_ensure_archive 'attachments'