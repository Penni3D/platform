GO

ALTER TABLE process_tabs
ADD background NVARCHAR(255)

GO

exec app_ensure_archive 'process_tabs'