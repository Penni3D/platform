﻿ALTER TABLE process_portfolio_columns ADD  validation  nvarchar(MAX) NULL
exec app_ensure_archive 'process_portfolio_columns'

ALTER TABLE process_container_columns
ALTER COLUMN validation NVARCHAR(MAX) 

ALTER TABLE archive_process_container_columns
ALTER COLUMN validation NVARCHAR(MAX) 

EXEC app_set_archive_user_id 0;
UPDATE process_container_columns
SET validation = NULL WHERE LEN(validation) = 0