CREATE PROCEDURE [dbo].[app_ensure_cache_column]
    @tableName NVARCHAR(200), @columnName NVARCHAR(200), @enabled TINYINT, @dataType INT
AS
BEGIN
    DECLARE @sql NVARCHAR(MAX)
    DECLARE @dt NVARCHAR(MAX) = 'NVARCHAR(MAX)'

    IF @dataType = 1 SET @dt = 'INT'
    IF @dataType = 2 SET @dt = 'FLOAT(8)'
    SET @columnName = 'cache_' + @columnName
    IF @enabled = 1 BEGIN
        SET @sql = 'IF COL_LENGTH(''' + @tableName + ''', ''' + @columnName + ''') IS NULL BEGIN ALTER TABLE ' + @tableName + ' ADD ' + @columnName + ' ' + @dt + ' END'
        EXEC sp_executesql  @sql
    END ELSE BEGIN
        SET @sql = 'IF COL_LENGTH(''' + @tableName + ''', ''' + @columnName + ''') IS NOT NULL BEGIN ALTER TABLE ' + @tableName + ' DROP COLUMN ' + @columnName + ' END'
        EXEC sp_executesql  @sql
    END
END
GO
ALTER TABLE item_columns
    ADD cache_date DATETIME;
GO