
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_deliverables'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_deliverables] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_deliverables]	-- Stored procedure requires instance as a parameter
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'deliverables'
	DECLARE @tableName NVARCHAR(50) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	----Create process
	EXEC app_ensure_process @instance, @process, @process, 2

	--Create group
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g1 OUTPUT
		
	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'TAB', @g1, @tab_id = @t1 OUTPUT
		
	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'CONTAINER', @t1, 1, @container_id = @c1 OUTPUT
		
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'PORTFOLIO', @portfolio_id = @p1 OUTPUT
			
	UPDATE process_portfolios 
	SET process_portfolio_type = 1 --Update portfolio to be selector instead
	WHERE process_portfolio_id = @p1

	--Create conditions
	DECLARE @cId INT	
	EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')

	--Create columns
	DECLARE @i1 INT
	--EXEC app_ensure_column @instance, @process, 'parent_item_id', @c1, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	--INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,2) --Set "Secondary" attribute
	EXEC app_ensure_column @instance, @process, 'parent_item_id', @c1, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'delivery_name', @c1, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Set "Primary" attribute

	EXEC app_ensure_column @instance, @process, 'planned_date', @c1, 3, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'estimated_date', @c1, 3, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'actual_date', @c1, 3, 1, 1, 0, null,  0, 0
	--Create portfolio columns
	
	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 
END;
