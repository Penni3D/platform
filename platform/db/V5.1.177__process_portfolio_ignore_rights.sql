﻿    ALTER TABLE process_portfolios
          ADD ignore_rights  tinyint NOT NULL
          CONSTRAINT portfolios_ignore_rights_df DEFAULT 0

EXEC app_ensure_archive 'process_portfolios'