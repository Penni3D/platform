SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_recursive_list'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_recursive_list] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_recursive_list]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10), 	@process nvarchar(50)
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	--DECLARE @process NVARCHAR(50) = 'user'
	DECLARE @combine TINYINT = 0
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	SET @tableName = '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

			--Create group
			DECLARE @g1 INT
			EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g1 OUTPUT
		
			--Create tabs
			DECLARE @t1 INT
			EXEC app_ensure_tab @instance,@process,'TAB', @g1, @tab_id = @t1 OUTPUT
		
			--Create containers
			DECLARE @c1 INT
			EXEC app_ensure_container @instance,@process,'CONTAINER', @t1, 1, @container_id = @c1 OUTPUT
		
			--Create portfolios
			DECLARE @p1 INT
			EXEC app_ensure_portfolio @instance,@process,'PORTFOLIO', @portfolio_id = @p1 OUTPUT
			
			update process_portfolios set process_portfolio_type = 1 where process_portfolio_id = @p1
			
			--Create conditions
			DECLARE @cId INT	
			EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT

			INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'meta.write')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'meta.read')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'portfolio.write')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'portfolio.read')

			exec app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 0, 0, NULL, 0, 0, null, null
			
			
			EXEC app_ensure_column @instance, @process, 'task_type', 0, 1, 1, 0, 0, NULL, 0, 0 
			
            EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 0, 0, NULL, 0, 0 
			
			
			
			DECLARE @ic1 INT = (SELECT item_column_id FROM item_columns WHERE process = @process AND name = 'task_type')
			DECLARE @ic2 INT = (SELECT item_column_id FROM item_columns WHERE process = @process AND name = 'title')

		--	insert into process_container_columns (process_container_id, item_column_id)  select @c1, item_column_id from item_columns where instance = @instance and process = @process and item_column_id NOT IN (select item_column_id from process_container_columns where process_container_id = @c1 )
			insert into process_portfolio_columns (process_portfolio_id, item_column_id)  select @p1, item_column_id from item_columns where instance = @instance and process = @process and item_column_id NOT IN (select item_column_id from process_portfolio_columns where process_portfolio_id = @p1 )

			
			update process_portfolio_columns SET order_no = ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM process_portfolio_columns WHERE process_portfolio_id = @p1 ORDER BY order_no COLLATE Latin1_general_bin DESC),null)),'U')
			update process_portfolio_columns SET report_column = 2 WHERE item_column_id IN (@ic1,@ic2)
            
			update item_columns set status_column = 1 where instance = @instance and process = @process and name = 'parent_item_id'


			set @sql = 'update ' +  @tableName + ' set parent_item_id = 0 '
			exec(@sql)
END
GO
