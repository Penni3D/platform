IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_hel_resources'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_hel_resources] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_hel_resources]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	exec app_set_archive_user_id 0
	declare @process nvarchar(50) = 'hel_resources'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	----Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create owner item id for row manager
	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 1, 1, 1, 0, null, 0, 0, 0

	--Create list_risks_probability -list
	DECLARE @processListProbability NVARCHAR(50) = 'list_hel_resources_list_1'
	DECLARE @tableNameProbability NVARCHAR(50) = ''
	SET @tableNameProbability = '_' + @instance + '_' + @processListProbability
	DECLARE @sqlProbability NVARCHAR(MAX) = ''

	--Archive user is 0
	DECLARE @BinVar1 varbinary(128);
	SET @BinVar1 = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar1;

	--Create process
	DECLARE @ListProbabilityId INT
	EXEC app_ensure_list @instance, @processListProbability, @processListProbability, 0

	EXEC dbo.app_ensure_column @instance,@processListProbability, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProbability, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProbability, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	--EXEC dbo.app_ensure_column @instance,@processListProbability, 'value', 0, 0, 1, 0, 0, NULL, 0, 0


	--Create list_risks_seriousness -list
	DECLARE @processListseriousness NVARCHAR(50) = 'list_hel_resources_list_2'
	DECLARE @tableNameSeriousness NVARCHAR(50) = ''
	SET @tableNameSeriousness = '_' + @instance + '_' + @processListSeriousness
	DECLARE @sqlSeriousness NVARCHAR(MAX) = ''

	--Archive user is 0
	DECLARE @BinVar2 varbinary(128);
	SET @BinVar2 = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar2;

	--Create process
	DECLARE @ListSeriousnessId INT
	EXEC app_ensure_list @instance, @processListSeriousness, @processListSeriousness, 0

	EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	--EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'value', 0, 0, 1, 0, 0, NULL, 0, 0

	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'group_id', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT
	EXEC app_ensure_column @instance, @process, 'type', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
	EXEC app_ensure_column @instance, @process, 'list_1', 0, 6, 1, 1, 0, null,  0, 0, @processListProbability , null, 0, @item_column_id = @i1 OUTPUT 
	EXEC app_ensure_column @instance, @process, 'list_2', 0, 6, 1, 1, 0, null,  0, 0, @processListSeriousness , null, 0, @item_column_id = @i1 OUTPUT
	EXEC app_ensure_column @instance, @process, 'percentage', 0, 2, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 

	update item_columns set status_column = 1 where instance = @instance and process = @process
	
    
    --Create portfolios
    DECLARE @p1 INT
    EXEC app_ensure_portfolio @instance,@process,'DATA', @portfolio_id = @p1 OUTPUT
    update process_portfolios set process_portfolio_type = 1 where instance = @instance and process = @process --Entity selector
    
    --Create conditions
    DECLARE @cId INT	
    EXEC app_ensure_condition @instance, @process, 0, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT
    INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
    INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
    INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')
    
    
    INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) 
    SELECT @p1, item_column_id FROM item_columns WHERE instance = @instance AND process = @process
END;
