SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_evaluation_matrix]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	exec app_set_archive_user_id 0
	declare @process nvarchar(50) = 'evaluation_matrix'
	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2
	--Create conditions
	EXEC app_ensure_condition @instance, @process, 0, 0
	--Create columns
	EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'list_item_id', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'evaluated_as_zero', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'value', 0, 2, 1, 1, 0, null,  0, 0
	
	--Create list_evaluation_matrix_list -list
	DECLARE @listEmqc NVARCHAR(50) = 'list_evaluation_matrix_question_categories'
	DECLARE @tableNameEmqc NVARCHAR(50) = ''
	SET @tableNameEmqc = '_' + @instance + '_' + @listEmqc
	DECLARE @sqlEmqc NVARCHAR(MAX) = ''
	--Create process
	EXEC app_ensure_list @instance, @ListEmqc, @ListEmqc, 0
	--Create list_evaluation_matrix_categories columns
	EXEC dbo.app_ensure_column @instance,@ListEmqc, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@ListEmqc, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@ListEmqc, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@ListEmqc, 'evaluation_column_name', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@ListEmqc, 'answers_count', 0, 1, 1, 0, 0, NULL, 0, 0

	--Create list_evaluation_matrix_questions -sublist
	DECLARE @sublistEmq NVARCHAR(50) = 'list_evaluation_matrix_questions'
	DECLARE @tableNameEmq NVARCHAR(50) = ''
	SET @tableNameEmq = '_' + @instance + '_' + @sublistEmq
	DECLARE @sqlEmq NVARCHAR(MAX) = ''
	--Create process
	DECLARE @subListEmqId INT
	EXEC app_ensure_list @instance, @sublistEmq, @sublistEmq, 0
	--Create list_evaluation_matrix_categories columns
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'answer_0', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'answer_1', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'answer_2', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'answer_3', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'answer_4', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'answer_5', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'value_0', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'value_1', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'value_2', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'value_3', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'value_4', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@sublistEmq, 'value_5', 0, 2, 1, 0, 0, NULL, 0, 0
	--Transform @sublistEmq list into a sublist
	INSERT INTO process_subprocesses(instance, parent_process, process) VALUES (@instance,@listEmqc,@sublistEmq)
END;