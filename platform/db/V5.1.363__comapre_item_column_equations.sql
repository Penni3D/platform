﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[itemColumnEquations_ParamIdReplace]
(
	@column_type INT,
	@sql_function INT,
	@param_type INT,
	@sql_param NVARCHAR(255)
)
RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @result NVARCHAR(255)

	SET @result = @sql_param

	IF @column_type = 0 AND @sql_function = 8 
		BEGIN
			IF ISNUMERIC(@sql_param) = 1
				BEGIN
					SELECT @result = '#' + variable FROM item_columns WHERE item_column_id = @sql_param
				END
		END

	ELSE IF (@column_type = 0 AND @sql_function = 10 AND @param_type = 1) OR (@column_type = 3 AND @sql_function = 0 AND @param_type = 2)
		BEGIN
			IF SUBSTRING(@sql_param, 1, 1) = '['
				BEGIN
					DECLARE @list_item_id NVARCHAR(MAX)
					DECLARE @guid NVARCHAR(MAX)

					SET @sql_param = REPLACE(@sql_param, '[', '')
					SET @sql_param = REPLACE(@sql_param, ']', '')
					
					DECLARE @delimited CURSOR
					SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT * FROM dbo.DelimitedStringToTable (@sql_param, ',') 
					
					OPEN @delimited
					FETCH NEXT FROM @delimited INTO @list_item_id
							
					DECLARE @start INT
					SET @start = 1
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SELECT @guid = guid FROM items WHERE item_id = @list_item_id
							SET @sql_param = STUFF(@sql_param, @start, LEN(@list_item_id), @guid)

							SET @start = @start + LEN(@guid) + 1

							FETCH NEXT FROM @delimited INTO @list_item_id
						END
					CLOSE @delimited

					SET @result = '[' + @sql_param +']'
				END
		END

	RETURN @result
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[itemColumnEquations_ParamVarReplace]
(
	@instance NVARCHAR(MAX),
	@column_type INT,
	@sql_function INT,
	@param_type INT,
	@sql_param NVARCHAR(255)
)
RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @result NVARCHAR(255)

	SET @result = @sql_param

	IF @column_type = 0 AND @sql_function = 8
		BEGIN
			IF SUBSTRING(@sql_param, 1, 1) = '#'
				BEGIN
					SET @sql_param = REPLACE(@sql_param, '#', '')
					SELECT @result = item_column_id FROM item_columns WHERE instance = @instance AND variable = @sql_param 
				END
		END

	ELSE IF (@column_type = 0 AND @sql_function = 10 AND @param_type = 1) OR (@column_type = 3 AND @sql_function = 0 AND @param_type = 2)
		BEGIN
			IF SUBSTRING(@sql_param, 1, 1) = '['
				BEGIN
					DECLARE @list_item_id INT
					DECLARE @guid NVARCHAR(MAX)

					SET @sql_param = REPLACE(@sql_param, '[', '')
					SET @sql_param = REPLACE(@sql_param, ']', '')
						
					DECLARE @delimited CURSOR
					SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT * FROM dbo.DelimitedStringToTable (@sql_param, ',') 
					
					OPEN @delimited
					FETCH NEXT FROM @delimited INTO @guid
								
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SELECT @list_item_id = item_id FROM items WHERE guid = @guid
							SET @sql_param = REPLACE(@sql_param,  @guid, @list_item_id)

							FETCH NEXT FROM @delimited INTO @guid
						END
					CLOSE @delimited

					SET @result = '[' + @sql_param +']'
				END
		END

	RETURN @result
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									IF @type = 0 
										BEGIN
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
							
									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX(']', @dataAdditional2, @startPos) 
										END
				
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @guid UNIQUEIDENTIFIER
						
													SET @var3 = REPLACE(@var3, '[', '')
													SET @var3 = REPLACE(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													DECLARE @start INT
													SET @start = 1
								  					WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															DECLARE @gu_id NVARCHAR(MAX)
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = 'NULL' 
																END
															SET @var4 = STUFF(@var4, @start, LEN(@listVal), CAST(@gu_id AS NVARCHAR(MAX)))

															SET @start = @start + LEN(@gu_id) + 1

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
				
									IF @type = 0 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processPortfolio_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 2 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
		
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0 
										BEGIN
											SET @var3 =  REPLACE(REPLACE(@var3, 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processActionDialog_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN
			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2

									IF @type = 0 
										BEGIN
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									
									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX(']', @dataAdditional2, @startPos) 
										END
									
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @listItemId INT

													SET @var3 = REPLACE(@var3, '[', '')
													SET @var3 = REPLACE(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
													
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
			
			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 2 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
			
			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2

									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = ' ""'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = ' ""'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[condition_IdReplaceProc]
(
	@condition_json NVARCHAR(MAX),
	@replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;  
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = CHARINDEX('ItemColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.itemColumn_GetVariable(@var3)				
					IF @var4 IS NOT NULL
						BEGIN
							SELECT  @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json), LEN(@var2), 'ItemColumnId":' + @var4)
						END

					DECLARE @replaced AS NVARCHAR (MAX)
					IF dbo.IsListColumn(@var3)  > 0
						BEGIN					
							DECLARE @idCursor CURSOR 
							DECLARE @listItemId INT 
							DECLARE @value NVARCHAR(MAX)

							SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
							SELECT item_id FROM dbo.get_ListColumnIds(@var3);

							OPEN @idCursor
							FETCH NEXT FROM @idCursor INTO @listItemId
							WHILE (@@FETCH_STATUS = 0) 
								BEGIN
									SELECT @value = CAST(dbo.item_GetGuid(@listItemId) AS NVARCHAR(MAX))
									IF @listItemId IS NOT NULL AND @value IS NOT NULL
										BEGIN
											SET @replaced = REPLACE(@condition_json, '"Value":' + CAST(@listItemId AS NVARCHAR) + '}', '"Value":"' + @value + '"}')
											IF @replaced IS NOT NULL
												BEGIN
													SET @condition_json = @replaced
												END
										END

									FETCH NEXT FROM @idCursor INTO @listItemId
								END
						END

						IF @endPos2 > @endPos1
							BEGIN
								SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos1 + LEN(@var4))
							END
						ELSE
							BEGIN
								SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos2 + LEN(@var4))
							END
				END
			ELSE
				BEGIN
					set @replaced = ''
				END

			CONTINUE  
		END

	SET @startPos = CHARINDEX('UserColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  	
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)

			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.itemColumn_GetVariable(@var3)		
					IF @var4 IS NOT NULL 
						BEGIN
							SELECT  @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json), LEN(@var2), 'UserColumnId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos2 + LEN(@var4))
						END
					END
				ELSE
					BEGIN
						IF @var3 <> 'null'
							BEGIN
								SET @condition_json = NULL
							END
					END
			CONTINUE  
		END

	SET @startPos = CHARINDEX('ConditionId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.condition_GetVariable(@var3)		
					IF @var4 IS NOT NULL
						BEGIN
							SELECT  @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json), LEN(@var2), 'ConditionId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos2 + LEN(@var4))
						END
				END
			ELSE
				BEGIN
					IF @var3 <> 'null'
						BEGIN
							SET @condition_json = NULL
						END
				END

			CONTINUE  
		END

	SET @replaced_json = @condition_json
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[condition_VarReplaceProc]
(
	@targetInstance NVARCHAR(10),
	@condition_json NVARCHAR(MAX),
	@replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = CHARINDEX('ItemColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = REPLACE(@condition_json, @var2, 'ItemColumnId":' + @var4)
						END

					IF dbo.IsListColumn(dbo.itemColumn_GetId(@targetInstance, @var3)) > 0
						BEGIN					
							DECLARE @idCursor CURSOR 
							DECLARE @listItemId INT 
							DECLARE @value NVARCHAR(MAX)
							DECLARE @id INT
	
							SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
							SELECT item_id FROM dbo.get_ListColumnIds(dbo.itemColumn_GetId(@targetInstance, @var3));
	
							OPEN @idCursor
							FETCH NEXT FROM @idCursor INTO @listItemId
							WHILE (@@FETCH_STATUS = 0) 
								BEGIN
									SELECT @value = CAST(dbo.item_GetGuid(@listItemId) AS NVARCHAR(MAX))
									IF @listItemId IS NOT NULL AND @value IS NOT NULL
										BEGIN
											SET @condition_json = REPLACE(@condition_json, '"Value":"' + @value + '"}', '"Value":' + CAST(@listItemId AS NVARCHAR) + '}')
										END

									FETCH NEXT FROM @idCursor INTO @listItemId
								END
						END
				END

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos1 + LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @endPos2 + LEN(@var4))
				END

			 CONTINUE  
		END

	SET @startPos = CHARINDEX('UserColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = REPLACE(@condition_json, @var2, 'UserColumnId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = CHARINDEX('UserColumnId', @condition_json, @endPos2 + LEN(@var4))
						END
				END

			CONTINUE  
		END

	SET @startPos = CHARINDEX('ConditionId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)
			
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.condition_GetId(@targetInstance, @var3)		
					IF @var4 IS NOT NULL 
						BEGIN
							SET @condition_json = REPLACE(@condition_json, @var2, 'ConditionId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = CHARINDEX('ConditionId', @condition_json, @endPos2 + LEN(@var4))
						END
				END

			CONTINUE  
		END
 
	SET @replaced_json = @condition_json
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[conditions_replace]
(
	@sourceInstance nvarchar(10)
	,@targetInstance nvarchar(10)
)
AS
BEGIN
	DECLARE @condition_id INT
	DECLARE @instance NVARCHAR(10)
	DECLARE @process NVARCHAR(50)
	DECLARE @variable NVARCHAR(255)
	DECLARE @condition_json NVARCHAR(MAX)
	DECLARE @order_no NVARCHAR(MAX)
	DECLARE @disabled TINYINT

	DECLARE @conditionCursor CURSOR 
	SET @conditionCursor = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT condition_id, instance, process, variable, condition_json, order_no, [disabled] FROM conditions WHERE instance = @sourceInstance

	OPEN @conditionCursor
	FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json, @order_no, @disabled
	WHILE (@@FETCH_STATUS = 0) BEGIN
		DECLARE @json AS NVARCHAR(MAX)
		EXEC dbo.condition_IdReplaceProc @condition_json,  @replaced_json = @json OUTPUT
		INSERT INTO #conditions (condition_id, instance, process, variable, condition_json, order_no, [disabled]) VALUES (@condition_id, @targetInstance, @process, @variable, @json, @order_no, @disabled)
		FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json, @order_no, @disabled
	END
END