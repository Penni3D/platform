﻿DECLARE @id INT = (SELECT instance_menu_id FROM instance_menu WHERE page = 'instanceMenu')
if (@id >0)  begin
INSERT INTO instance_menu_states (instance_menu_id, item_id, state) values (@id, 2, 'read')
INSERT INTO instance_menu_states (instance_menu_id, item_id, state) values (@id, 2, 'write')
INSERT INTO instance_menu_states (instance_menu_id, item_id, state) values (@id, 2, 'delete')
end