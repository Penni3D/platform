SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processGroup_GetIds]
(
	@instance NVARCHAR(10),
	@variables NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @ids NVARCHAR(MAX)
	SET @ids = STUFF((SELECT ',' + CAST(process_group_id AS NVARCHAR) FROM process_groups WHERE instance = @instance AND variable IN (SELECT LTRIM(value) FROM STRING_SPLIT(@variables, ',')) FOR XML PATH('')), 1, 1, '')
	IF @ids IS NULL	BEGIN
		SET @ids = ''
	END
	RETURN '[' + @ids + ']'
END