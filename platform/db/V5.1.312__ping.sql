CREATE index occurence_index ON instance_requests (occurence)
GO
CREATE index user_index ON instance_requests (user_item_id)
GO
CREATE index instance_index ON instance_requests (instance)