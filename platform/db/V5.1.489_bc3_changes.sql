DECLARE @Instance NVARCHAR(255);
DECLARE @sql NVARCHAR(MAX);
DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances 
OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN 
	
		IF(SELECT COUNT (*) FROM processes WHERE process = 'business_3') > 0
			BEGIN
	
        EXEC dbo.app_ensure_column @Instance,'business_3', 'comments', 0, 4, 1, 1, 0, NULL, 0, 0
		
		
		IF(SELECT COUNT (*) FROM item_columns WHERE process = 'business_3' and name = 'value_') > 0
			BEGIN
	
				DECLARE @value_column_id INT = (SELECT item_column_id FROM item_columns WHERE name = 'value_' AND process = 'business_3' AND instance = @Instance)
				UPDATE item_columns SET name = 'value' WHERE item_column_id = @value_column_id

			END	
			
		IF(SELECT COUNT (*) FROM item_columns WHERE process = 'business_3' and name = 'idea_id') > 0
			BEGIN
	
				DECLARE @idea_column_id INT = (SELECT item_column_id FROM item_columns WHERE name = 'idea_id' AND process = 'business_3' AND instance = @Instance)
				UPDATE item_columns SET name = 'process_item_id' WHERE item_column_id = @idea_column_id
			

			END	


			DECLARE @TableName NVARCHAR(50) = '_' + @Instance + '_business_3'
				
			SET @sql = 'app_ensure_archive '''+@TableName+''''
			print @sql
			EXEC (@sql)
			
			END
			

        FETCH NEXT FROM cursor_process INTO @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
	
	

