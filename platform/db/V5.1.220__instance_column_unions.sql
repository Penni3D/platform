CREATE TABLE [dbo].[instance_unions]( 
    [instance_union_id] int NOT NULL IDENTITY (1,1) PRIMARY KEY, 
    [instance] nvarchar(10) NOT NULL, 
    [variable] nvarchar(255)  NULL, 
    CONSTRAINT [FK_instance_unions_instance] FOREIGN KEY ([instance]) REFERENCES [dbo].[instances] (instance) 
) 

CREATE TABLE [dbo].[instance_union_processes]( 
    [instance_union_process_id] int NOT NULL IDENTITY (1,1) PRIMARY KEY, 
    [instance_union_id] int,
    [instance] nvarchar(10) NOT NULL, 
    [process]  nvarchar(50) NOT NULL, 
    CONSTRAINT [FK_instance_unions_processes_id] FOREIGN KEY ([instance_union_id]) REFERENCES [dbo].[instance_unions] (instance_union_id)  ON DELETE NO ACTION ON UPDATE NO ACTION ,
    CONSTRAINT [FK_instance_union_process] FOREIGN KEY (process, instance) REFERENCES [dbo].[processes] (process, instance) 
) 

CREATE TABLE [dbo].[instance_union_rows]( 
    [instance_union_row_id] int NOT NULL IDENTITY (1,1) PRIMARY KEY, 
    [instance_union_id] int,
    CONSTRAINT [FK_instance_unions_rows_id] FOREIGN KEY ([instance_union_id]) REFERENCES [dbo].[instance_unions] (instance_union_id) ON DELETE NO ACTION ON UPDATE NO ACTION
) 

CREATE TABLE [dbo].[instance_union_columns]( 
    [instance_union_column_id] int NOT NULL IDENTITY (1,1) PRIMARY KEY, 
    [instance_union_process_id] int NOT NULL, 
    [instance_union_row_id] int NOT NULL, 
    [item_column_id] INT NOT NULL   ,
    CONSTRAINT [FK_instance_unions_row_id] FOREIGN KEY ([instance_union_row_id]) REFERENCES [dbo].[instance_union_rows] (instance_union_row_id) ON DELETE NO ACTION ON UPDATE NO ACTION ,
    CONSTRAINT [FK_instance_unions_row_process_id] FOREIGN KEY ([instance_union_process_id]) REFERENCES [dbo].[instance_union_processes] (instance_union_process_id) ON DELETE NO ACTION ON UPDATE NO ACTION ,
    CONSTRAINT [FK_instance_unions_item_column_id] FOREIGN KEY ([item_column_id]) REFERENCES [dbo].[item_columns] (item_column_id) ON DELETE CASCADE ON UPDATE CASCADE
) 

GO 

CREATE TRIGGER [dbo].[instance_unions_fk_delete]
    ON [dbo].[instance_unions]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM instance_union_processes dd			INNER JOIN deleted d ON dd.instance_union_id = d.instance_union_id
    DELETE dd FROM instance_union_rows dd			INNER JOIN deleted d ON dd.instance_union_id = d.instance_union_id
	  DELETE dd FROM instance_unions dd					INNER JOIN deleted d ON dd.instance_union_id = d.instance_union_id

GO 

CREATE TRIGGER [dbo].[instance_union_processes_fk_delete]
    ON [dbo].[instance_union_processes]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM instance_union_columns dd			INNER JOIN deleted d ON dd.instance_union_process_id = d.instance_union_process_id
	  DELETE dd FROM instance_union_processes dd					INNER JOIN deleted d ON dd.instance_union_process_id = d.instance_union_process_id

GO

CREATE TRIGGER [dbo].[instance_union_rows_fk_delete]
    ON [dbo].[instance_union_rows]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM instance_union_columns dd			INNER JOIN deleted d ON dd.instance_union_row_id = d.instance_union_row_id
	  DELETE dd FROM instance_union_rows dd					INNER JOIN deleted d ON dd.instance_union_row_id = d.instance_union_row_id