ALTER TRIGGER [dbo].[calendar_fk_delete]
    ON [dbo].[calendars]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM calendar_data dd	INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
	
    DELETE dd FROM calendar_events dd			INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
    DELETE dd FROM calendar_static_holidays dd	INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
	DELETE dd FROM calendars dd	INNER JOIN deleted d ON dd.calendar_id = d.calendar_id