DECLARE @ProcessName NVARCHAR(255);
DECLARE @Instance NVARCHAR(255);
DECLARE @item_column_id INT;
DECLARE cursor_process CURSOR
FOR SELECT process,instance FROM processes WHERE process_type = 4 
OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @ProcessName,@Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
	 EXEC app_ensure_column @Instance, @ProcessName, 'tooltip', 0,24,0,0,null,0,0,null,null
	  
	  SELECT @item_column_id = @@IDENTITY
	  
	  UPDATE item_columns SET status_column = 1 where item_column_id = @item_column_id
	  
		
        FETCH NEXT FROM cursor_process INTO 
            @ProcessName, @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
