IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_costs_budget'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_costs_budget] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_costs_budget]  -- Add the parameters for the stored procedure here
    @instance NVARCHAR(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'costs_budget'

    --Archive user is 0
    DECLARE @BinVar VARBINARY(128);
    SET @BinVar = CONVERT(VARBINARY(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_process @instance, @process, @process, 2

    EXEC dbo.app_ensure_column @instance, @process, 'idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'expected_revenue_increment_3_direct', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process,
                               'expected_sales_margin_increment_3_direct', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'expected_revenue_increment_5_direct', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process,
                               'expected_sales_margin_increment_5_direct', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'expected_revenue_increment_3_indirect', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process,
                               'expected_sales_margin_increment_3_indirect', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'expected_revenue_increment_5_indirect', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process,
                               'expected_sales_margin_increment_5_indirect', 0, 2, 1, 0, 0, NULL, 0, 0


  END
GO

