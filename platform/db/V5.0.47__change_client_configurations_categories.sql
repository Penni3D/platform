UPDATE [dbo].[instance_configurations]
SET category = 'Frontpage'
WHERE category = 'Main application'
AND name IN ('Frontpage editmode', 'Frontpage welcome', 'Frontpage layout');

--Cleanup: This configuration has been replaced with viewConfiguration tabid.
EXEC app_set_archive_user_id 0
DELETE FROM [dbo].[instance_configurations]
WHERE category = 'Evaluation'
AND name = 'EvaluationIdeaTabId';