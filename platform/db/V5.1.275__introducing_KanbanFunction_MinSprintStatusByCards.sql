EXEC app_set_archive_user_id 0;

SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @GetTables CURSOR

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT instance FROM instances
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

  WHILE (@@FETCH_STATUS=0) BEGIN
    SET @sql = 
'
CREATE FUNCTION f_' + @TableName + '_KanbanFunction_MinSprintStatusByCards
(
	 @sprint_item_id INT
	,@status_date DATETIME
) RETURNS INT
AS
BEGIN
  RETURN
  (
	SELECT TOP 1 ts.status_list_item_id
	FROM get__' + @TableName + '_task(@status_date) crd
		LEFT JOIN get_task_status(@status_date) ts ON ts.status_id = crd.status_id
		LEFT JOIN get__' + @TableName + '_list_task_status(@status_date) lts ON lts.item_id = ts.status_list_item_id
	WHERE crd.task_type = 0 AND crd.parent_item_id IN (SELECT tsk.item_id FROM get__' + @TableName + '_task(@status_date) tsk WHERE tsk.parent_item_id = @sprint_item_id AND tsk.task_type = 1)
	ORDER BY lts.kanban_status ASC
  )
END
'
    --PRINT @sql
	--EXEC ('DROP FUNCTION _' + @TableName + '_BudgetItemYearNPV');
    EXEC (@sql);
    FETCH NEXT FROM @GetTables INTO @TableName
  END
  CLOSE @GetTables