DECLARE @Instance NVARCHAR(255);
DECLARE @sql NVARCHAR(MAX);
DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances 
OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN 
		IF(SELECT COUNT (*) FROM item_columns WHERE process = 'list_wtr_location' and name = 'list_item' AND instance = @Instance) = 0 AND (SELECT COUNT (*) FROM processes WHERE process = 'list_wtr_location' AND instance = @Instance) > 0
			BEGIN
	
				 EXEC dbo.app_ensure_column @Instance,'list_wtr_location', 'list_item', 0, 0, 1, 1, 0, NULL, 0, 0
				DECLARE @TableName NVARCHAR(50) = '_' + @Instance + '_list_wtr_location'
				SET @sql = 'app_ensure_archive '''+@TableName+''''
				EXEC (@sql)
			
		
			END	

        FETCH NEXT FROM cursor_process INTO @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
	
	

