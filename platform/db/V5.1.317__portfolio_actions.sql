CREATE TABLE dbo.process_portfolio_actions (
   process_portfolio_action_id INT NOT NULL IDENTITY(1,1),
   process_portfolio_id INT NOT NULL,
   process_action_id INT NOT NULL,
   button_text_variable NVARCHAR(255)
);
GO

ALTER TABLE dbo.process_portfolio_actions ADD CONSTRAINT PK_process_portfolio_actions PRIMARY KEY (process_portfolio_action_id);
GO


ALTER TABLE dbo.process_portfolios
    ADD CONSTRAINT FK_process_portfolio_actions_process_portfolios
        FOREIGN KEY (process_portfolio_id)
            REFERENCES dbo.process_portfolios (process_portfolio_id);

ALTER TABLE dbo.process_actions
    ADD CONSTRAINT FK_process_portfolio_actions_process_actions
        FOREIGN KEY (process_action_id)
            REFERENCES dbo.process_actions (process_action_id); 