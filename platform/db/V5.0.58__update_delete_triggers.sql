GO
ALTER  TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
AS 
 set xact_abort off; 
	DECLARE @ids nvarchar(max) =(select ',' + CAST(item_id as nvarchar) FROM deleted FOR XML PATH(''))
	
	DECLARE @tName as nvarchar(max)
	DECLARE @cName as nvarchar(max)

	DECLARE @countSql as nvarchar (max) = ''


	IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
	CREATE table #DeleteItems (item_id int)
			
	DECLARE @GetTables CURSOR
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	select ccu.TABLE_NAME,  ccu.COLUMN_NAME from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
	inner join  INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
	where UNIQUE_CONSTRAINT_NAME = 'PK_items' AND substring(ccu.TABLE_NAME, 1, 1) = '_'

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName

	WHILE (@@FETCH_STATUS=0) BEGIN
		DECLARE @sql nvarchar(max) = 'INSERT INTO #DeleteItems (item_id) (SELECT item_id FROM ' + @tName + ' WHERE ' + @cName + ' IN (-1' + @ids + '))'
		EXECUTE (@sql)
		set @countSql = @countSql + ' SELECT @childCount = @childCount + count(item_id) FROM ' + @tName + ' WHERE ' + @cName + ' = @delId ; '
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

    DELETE  FROM item_subitems 					WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data 						WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM process_tabs_subprocess_data 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE process_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE task_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)

--	DELETE FROM items WHERE item_id IN (SELECT item_id FROM #DeleteItems)

	DECLARE @delId as int
	DECLARE @GetIds CURSOR

	DECLARE @delIdCount int
	DECLARE @oldDelIdCount int

	DECLARE @DeleteItems2 TABLE (item_id int)	
	DECLARE @DeleteSuccess TABLE (item_id int)	

	INSERT INTO @DeleteItems2 select item_id FROM #DeleteItems

	SELECT @delIdCount = COUNT(item_id)  FROM  @DeleteItems2 

	WHILE @delIdCount > 0 
		BEGIN
		
			SET @GetIds = CURSOR FAST_FORWARD FORWARD_ONLY LOCAL FOR select item_id from @DeleteItems2 WHERE item_id NOT IN (select item_id FROM @DeleteSuccess)
			OPEN @GetIds
			FETCH NEXT FROM @GetIds INTO @delId

			WHILE (@@FETCH_STATUS=0) BEGIN


			declare @cnt int

			SET @countSql = 'SET @childCount = 0; ' + @countSql 
			EXECUTE sp_executesql @countSql, N'@delId int,@childCount int OUTPUT', @delId = @delId, @childCount=@cnt OUTPUT

			IF @cnt = 0
			BEGIN
						DELETE FROM items WHERE item_id = @delId
						INSERT INTO @DeleteSuccess (item_id) values (@delId)
			END
				
			FETCH NEXT FROM @GetIds INTO @delId
			END
			CLOSE @GetIds

			SELECT @delIdCount = COUNT(item_id)  FROM  @DeleteItems2  where item_id NOT IN (select item_id from @DeleteSuccess)
		END

    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data dd						INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.process_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.task_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id



	DELETE dd FROM instance_comments dd INNER JOIN deleted d ON dd.sender = d.item_id
	DELETE dd FROM process_comments dd INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_comments dd INNER JOIN deleted d ON dd.sender = d.item_id
	DELETE dd FROM instance_user_configurations_saved dd INNER JOIN deleted d ON dd.user_id = d.item_id
	DELETE dd FROM instance_user_configurations dd INNER JOIN deleted d ON dd.user_id = d.item_id
	
	--Finally delete the item
	DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id
	

GO	
ALTER TRIGGER [dbo].[instance_fk_delete]
    ON [dbo].[instances]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM attachments dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM calendars dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_translations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_languages dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_logs dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_hosts dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_menu dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations_saved dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM processes dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instances dd	INNER JOIN deleted d ON dd.instance = d.instance


	DELETE dd FROM instance_comments dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations_saved dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM process_comments dd	INNER JOIN deleted d ON dd.instance = d.instance

	
GO
ALTER  TRIGGER [dbo].[processes_fk_delete]
    ON [dbo].[processes]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.parent_process = d.process
	DELETE dd FROM item_columns dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM item_tables dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM items dd						INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_groups	dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_tabs dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_containers dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_portfolios dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_tabs_subprocesses dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM conditions dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_actions dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_dashboard_charts dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process

	DELETE dd FROM processes dd					INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process

	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_comments dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	