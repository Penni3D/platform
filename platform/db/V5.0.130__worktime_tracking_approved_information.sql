﻿ALTER TABLE worktime_tracking_hours ADD 
  line_manager_approved_at DATETIME,
  project_manager_approved_at DATETIME
  