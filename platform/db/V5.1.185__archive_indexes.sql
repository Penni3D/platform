 CREATE NONCLUSTERED INDEX a_idps_selected_item_id
ON [dbo].[archive_item_data_process_selections] ([archive_id])
INCLUDE ([selected_item_id])