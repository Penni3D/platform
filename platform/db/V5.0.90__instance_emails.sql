IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[instance_emails]') AND type IN ('U'))
	DROP TABLE [dbo].[instance_emails]
GO

CREATE TABLE [dbo].[instance_emails] (
  [instance_email_id] int  IDENTITY(1,1) NOT NULL,
  [instance] nvarchar(10) NULL,
  [subject] nvarchar(max) NULL,
  [body] nvarchar(max) NULL,
  [to_recipients] nvarchar(max) NULL,
  [cc_recipients] nvarchar(max) NULL,
  [bcc_recipients] nvarchar(max) NULL,
  [status] int  NULL,
  [error_description] nvarchar(max) NULL
)
GO

ALTER TABLE [dbo].[instance_emails] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Primary Key structure for table instance_emails
-- ----------------------------
ALTER TABLE [dbo].[instance_emails] ADD CONSTRAINT [PK__instance__B9516F1D752767BF] PRIMARY KEY CLUSTERED ([instance_email_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Foreign Keys structure for table instance_emails
-- ----------------------------
ALTER TABLE [dbo].[instance_emails] ADD CONSTRAINT [instance] FOREIGN KEY ([instance]) REFERENCES [dbo].[instances] ([instance]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

