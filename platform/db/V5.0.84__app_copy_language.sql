IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_copy_languages'))
   exec('CREATE PROCEDURE [dbo].[app_copy_languages] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_copy_languages]	-- Add the parameters for the stored procedure here
						@source_language nvarchar(5),
						@target_language nvarchar(5)
AS
  BEGIN
	EXEC app_set_archive_user_id 0
	DECLARE @variable NVARCHAR(50) = ''
	DECLARE @process NVARCHAR(50) = ''
	DECLARE @instance NVARCHAR(10) = ''
	DECLARE @translation NVARCHAR(255) = ''

	DECLARE Language_Cursor CURSOR FOR SELECT variable, process, instance, translation FROM process_translations WHERE code = @source_language AND process IS NOT NULL
	
	OPEN Language_Cursor

	FETCH NEXT FROM Language_Cursor INTO @variable, @process, @instance, @translation
	
	WHILE @@fetch_status = 0
	    BEGIN
			IF ((SELECT COUNT(*) FROM process_translations WHERE code = @target_language AND instance = @instance AND variable = @variable) = 0)
				BEGIN
					INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @process, @target_language, @variable, @translation)
				END 
	        FETCH NEXT FROM Language_Cursor INTO @variable, @process, @instance, @translation
	    END
	CLOSE Language_Cursor
	DEALLOCATE Language_Cursor
	
	
	DECLARE Language_Cursor2 CURSOR FOR SELECT variable, instance, translation FROM process_translations WHERE code = @source_language AND process IS NULL
	
	OPEN Language_Cursor2

	FETCH NEXT FROM Language_Cursor2 INTO @variable, @instance, @translation
	
	WHILE @@fetch_status = 0
	    BEGIN
			IF ((SELECT COUNT(*) FROM process_translations WHERE code = @target_language AND instance = @instance AND variable = @variable) = 0)
				BEGIN
					INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, NULL, @target_language, @variable, @translation)
				END 
	        FETCH NEXT FROM Language_Cursor2 INTO @variable, @instance, @translation
	    END
	CLOSE Language_Cursor2
	DEALLOCATE Language_Cursor2
END
