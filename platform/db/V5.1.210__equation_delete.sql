﻿ALTER TRIGGER [dbo].[item_columns_fk_delete]
    ON [dbo].[item_columns]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM process_container_columns dd			INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM process_portfolio_columns dd	INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM attachments dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM item_data_process_selections dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM process_tab_columns dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM item_data dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM state_rights dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM state_rights dd					INNER JOIN deleted d ON dd.user_column_id = d.item_column_id
    DELETE dd FROM item_column_equations dd					INNER JOIN deleted d ON dd.parent_item_column_id = d.item_column_id
    DELETE dd FROM item_column_equations dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	  DELETE dd FROM item_columns dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id