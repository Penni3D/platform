CREATE TABLE [dbo].[process_portfolio_columns_groups]( 
  [portfolio_column_group_id] [int] IDENTITY(1,1) NOT NULL, 
  [instance] [nvarchar](10) NOT NULL, 
  [process] [nvarchar](50) NOT NULL, 
  [variable] [nvarchar](255) NULL,
  [process_portfolio_id] [int] NOT NULL,   
 CONSTRAINT [PK_process_portfolio_columns_groups] PRIMARY KEY CLUSTERED  
( 
  [portfolio_column_group_id] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
GO
