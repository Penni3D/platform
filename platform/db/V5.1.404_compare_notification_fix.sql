SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[notification_VariableIdReplace]
(
	@variable NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @item_id INT
	DECLARE @column_item_id INT

	DECLARE @item_guid NVARCHAR(MAX)
	DECLARE @column_item_variable NVARCHAR(MAX)

	IF @variable IS NOT NULL AND (@variable LIKE 'variable_name_variable_%' OR @variable LIKE 'variable_title_variable_%' OR @variable LIKE 'variable_body_variable_%')
		BEGIN
			SET @item_id = (SELECT TOP 1 value FROM (SELECT TOP 2 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '_') ORDER BY order_no DESC) AS tmp ORDER BY order_no ASC)
			SET @column_item_id = (SELECT value FROM (SELECT TOP 1 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '_') ORDER BY order_no DESC) AS tmp)
			
			SET @item_guid = CAST(dbo.item_GetGuid(@item_id) AS NVARCHAR(MAX))
			IF @item_guid IS NULL BEGIN
				SET @item_guid = 'NULL'
			END
			
			SET @column_item_variable = dbo.itemColumn_GetVariable(@column_item_id)
			IF @column_item_variable IS NULL BEGIN
				SET @column_item_variable = 'NULL'
			END
			
			SET @variable = STUFF(@variable, CHARINDEX('_' + CAST(@item_id AS NVARCHAR) + '_', @variable), LEN(CAST(@item_id AS NVARCHAR)) + 1, '|' + @item_guid)
			SET @variable = STUFF(@variable, CHARINDEX('_' + CAST(@column_item_id AS NVARCHAR), @variable), LEN(CAST(@column_item_id AS NVARCHAR)) + 1, '|' + @column_item_variable)
		END
	RETURN @variable
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[notification_VariableVarReplace]
(
	@instance NVARCHAR(MAX),
	@variable NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @item_guid NVARCHAR(MAX)
	DECLARE @column_item_variable NVARCHAR(MAX)

	DECLARE @item_id NVARCHAR(MAX)
	DECLARE @column_item_id NVARCHAR(MAX)

	IF @variable IS NOT NULL AND (@variable LIKE 'variable_name_variable|%' OR @variable LIKE 'variable_title_variable|%' OR @variable LIKE 'variable_body_variable|%')
		BEGIN
			SET @item_guid = (SELECT TOP 1 value FROM (SELECT TOP 2 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '|') ORDER BY order_no DESC) AS tmp ORDER BY order_no ASC)
			SET @column_item_variable = (SELECT value FROM (SELECT TOP 1 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '|') ORDER BY order_no DESC) AS tmp)
			
			SET @item_id = @item_guid
			IF @item_guid != 'NULL' BEGIN
				SET @item_id =  CAST(dbo.item_GetId(CAST(@item_guid AS UNIQUEIDENTIFIER)) AS NVARCHAR(MAX))
				IF @item_id IS NULL BEGIN
					SET @item_id = 'NULL'
				END
			END
			
			SET @column_item_id = @column_item_variable
			IF @column_item_variable != 'NULL' BEGIN
				SET @column_item_id = CAST(dbo.itemColumn_GetId(@instance, @column_item_variable) AS NVARCHAR(MAX))
				IF @column_item_id IS NULL BEGIN
					SET @column_item_id = 'NULL'
				END
			END

			SET @variable = STUFF(@variable, CHARINDEX('|' + @item_guid + '|', @variable), LEN(@item_guid) + 1, '_' + @item_id)
			SET @variable = STUFF(@variable, CHARINDEX('|' + @column_item_variable, @variable), LEN(@column_item_variable) + 1, '_' + @column_item_id)
		END
	RETURN @variable
END