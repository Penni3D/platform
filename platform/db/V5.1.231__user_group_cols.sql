﻿  DECLARE @instance nvarchar(10)
  DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
  OPEN instanceCursor
  
  EXEC app_set_archive_user_id 0
  
  FETCH NEXT FROM instanceCursor INTO @instance
    declare @process nvarchar(50) = 'user_group' 
  
  WHILE @@fetch_status = 0
    BEGIN
    declare @containerId int 
    
      SELECT @containerId = MIN(process_container_id) FROM process_containers WHERE process = @process 
    
      if exists (select item_column_id from item_columns where instance = @instance AND process = @process  AND name = 'all_group')
      BEGIN
          declare @itemColumnId int 
          select @itemColumnId =  item_column_id from item_columns where instance = @instance AND process = @process  AND name = 'all_group'
  
          if NOT exists (select process_container_id FROM process_container_columns WHERE process_container_id = @containerId AND item_column_id = @itemColumnId )
          BEGIN         
            INSERT INTO process_container_columns (process_container_id, item_column_id) VALUES (@containerId, @itemColumnId)
          END         
      END
    ELSE
    BEGIN
        exec app_ensure_column @instance, 'user_group', 'all_group', @containerId, 1, 1, 0, '1', 'YES_NO', 0, 0  
    END 
      FETCH  NEXT FROM instanceCursor INTO @instance
    END
  CLOSE instanceCursor
  DEALLOCATE instanceCursor
