﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_budget_item]	-- Add the parameters for the stored procedure here
	@instance NVARCHAR(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'budget_item'

	--Archive user is 0
	DECLARE @BinVar VARBINARY(128);
	SET @BinVar = CONVERT(VARBINARY(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2

	--Create columns
	EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 5, 0, 0, NULL, 0, 0, 'budget_row'
	EXEC app_ensure_column @instance, @process, 'type_item_id', 0, 6, 0, 0, NULL, 0, 0, 'list_budget_item_type'
	EXEC app_ensure_column @instance, @process, 'date', 0, 3, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'amount', 0, 2, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'project_item_id', 0, 1, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'category_item_id', 0, 6, 0, 0, NULL, 0, 0, 'list_budget_category'

	-- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_item_id', 0, 6, 0, 0, NULL, 0, 0, 'list_budget_currency'
	-- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_rate', 0, 2, 0, 0, NULL, 0, 0
	-- add later when necessary: EXEC app_ensure_column @instance, @process, 'description', 0, 4, 0, 0, NULL,  0, 0
END



GO



DECLARE @item_column_id INT
DECLARE item_column_cursor CURSOR FOR SELECT item_column_id FROM item_columns WHERE process = 'budget_row' AND [name] = 'category_item_id' AND data_type = 5
OPEN item_column_cursor
FETCH NEXT FROM item_column_cursor INTO @item_column_id
WHILE @@fetch_status = 0 BEGIN
	EXEC dbo.items_ChangeColumnType @item_column_id, 5, 6
	FETCH NEXT FROM item_column_cursor INTO @item_column_id
END
CLOSE item_column_cursor
DEALLOCATE item_column_cursor



GO



UPDATE item_columns SET data_additional = 'budget_row' WHERE process = 'budget_item' AND [name] = 'parent_item_id' AND data_additional IS NULL

UPDATE item_columns SET data_additional = 'list_budget_category' WHERE process = 'budget_item' AND [name] = 'category_item_id' AND data_additional IS NULL

UPDATE item_columns SET data_additional = 'list_budget_item_type' WHERE process = 'budget_item' AND [name] = 'type_item_id' AND (data_additional IS NULL OR data_additional = 'budget_item_type')