SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @process_type INT,
	@colour nvarchar(50) = null, @listProcess int = 0, @listOrder nvarchar(50) = null, @newRowActionId int = null
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT COUNT(*) FROM processes WHERE instance = @instance AND process = @process) = 0)
	BEGIN
			DECLARE @langVar NVARCHAR (50) = @process

		INSERT INTO processes (instance, process, variable, process_type, colour, list_process, list_order, new_row_action_id) 
		VALUES (@instance, @process, @langVar, @process_type, @colour, @listProcess, @listOrder, @newRowActionId);
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND process = @process AND variable = @langVar) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, @langVar, @variable, 'en-GB')
		END

	END
END