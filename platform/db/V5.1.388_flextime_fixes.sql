SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[flex_time_CalculateMonthBalance]
(
	@instance NVARCHAR(MAX),
	@user_item_id INT,
	@month_date DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @item_id INT
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @balance FLOAT
	DECLARE @required_hours FLOAT
	DECLARE @work_hours FLOAT
	DECLARE @begin_date DATETIME
	DECLARE @year INT
	DECLARE @month INT

	SET @year = YEAR(@month_date)
	SET @month = MONTH(@month_date)

	SET @sql = 'SELECT @begin_date = MIN(start_date) FROM _' + @instance + '_flex_time_tracking WHERE type IN (0, 1) AND special IN (0, 1) AND user_item_id = ' + CAST(@user_item_id AS NVARCHAR) + ' AND draft = 0'
	EXECUTE sp_executesql @sql, N'@begin_date DATETIME OUTPUT', @begin_date = @begin_date OUTPUT

	DECLARE @start_date DATETIME
	DECLARE @end_date DATETIME
	DECLARE @notwork INT
	DECLARE @timesheet_hours FLOAT
	SET @required_hours = 0
	SET @start_date = CAST(@year AS NVARCHAR) + '-' + CAST(@month AS NVARCHAR) + '-1'
	SET @end_date = DATEADD(MONTH, 1, @start_date)
	IF @start_date < @begin_date BEGIN
		SET @start_date = @begin_date
	END

	SET @sql = 'SELECT @work_hours = ISNULL(SUM(work_hours), 0) FROM _' + @instance + '_flex_time_tracking WHERE [type] IN (0, 1) AND special IN (0, 1) AND user_item_id = ' + CAST(@user_item_id AS NVARCHAR) + ' AND [start_date] >= @start_date AND [start_date] < @end_date' + ' AND draft = 0'
	EXECUTE sp_executesql @sql, N'@start_date DATETIME, @end_date DATETIME, @work_hours FLOAT OUTPUT', @start_date = @start_date, @end_date = @end_date, @work_hours = @work_hours OUTPUT
	
	WHILE @start_date < @end_date BEGIN
		SELECT @notwork = COUNT(*) FROM GetCombinedCalendarForUser(@user_item_id, @start_date, @start_date)
		IF @notwork = 0 BEGIN
			SELECT @timesheet_hours = timesheet_hours FROM GetCalendarForUserByDate(@user_item_id, @start_date)
			SET @required_hours = @required_hours + @timesheet_hours
		END
		SET @start_date = DATEADD(DAY, 1, @start_date)
	END

	SET @balance = @work_hours - @required_hours

	SET @sql = 'SELECT @item_id = item_id FROM _' + @instance + '_flex_time_tracking WHERE [type] = 2 AND user_item_id = ' + CAST(@user_item_id AS NVARCHAR) + ' AND YEAR([start_date]) = ' + CAST(@year AS NVARCHAR) + ' AND MONTH([start_date]) = ' + CAST(@month AS NVARCHAR) + ' AND draft = 0'
	EXECUTE sp_executesql @sql, N'@item_id INT OUTPUT', @item_id = @item_id OUTPUT
	IF @item_id IS NULL BEGIN
		INSERT INTO items (instance, process) VALUES (@instance, 'flex_time_tracking')
		SET @item_id = @@identity
		
		SET @sql = 'UPDATE _' + @instance + '_flex_time_tracking SET work_hours = ' + CAST(@balance AS NVARCHAR) + ', [type] = 2, user_item_id = ' + CAST(@user_item_id AS NVARCHAR) + ', [start_date] = ''' + CAST(@year AS NVARCHAR) + '-' + CAST(@month AS NVARCHAR) + '-1'', draft = 0 WHERE item_id = ' + CAST(@item_id AS NVARCHAR)
		
		INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES (@item_id, (SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'flex_time_tracking' AND name = 'flex_user'), @user_item_id)
		EXECUTE sp_executesql @sql
	END
	ELSE BEGIN
		SET @sql = 'UPDATE _' + @instance + '_flex_time_tracking SET work_hours = ' + CAST(@balance AS NVARCHAR) + ' WHERE item_id = ' + CAST(@item_id AS NVARCHAR)
		EXECUTE sp_executesql @sql
	END
END



GO



--Archive user is 0
DECLARE @BinVar varbinary(128);
SET @BinVar = CONVERT(varbinary(128), 0);
SET CONTEXT_INFO @BinVar;

-- Apply changes for all instances that have this process
DECLARE @sql NVARCHAR(MAX)
DECLARE @instanceName NVARCHAR(10)
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instanceName
WHILE @@fetch_status = 0 BEGIN

	SET @sql = 'DELETE FROM _' + @instanceName + '_flex_time_tracking WHERE [type] = 2 AND archive_start NOT IN '
	+ '(SELECT MAX(archive_start) FROM _' + @instanceName + '_flex_time_tracking WHERE [type] = 2 GROUP BY user_item_id, YEAR(start_date), MONTH(start_date))'
	EXECUTE sp_executesql @sql

	SET @sql = 'UPDATE _' + @instanceName + '_flex_time_tracking SET draft = 0 WHERE [type] = 2 AND draft IS NULL'
	EXECUTE sp_executesql @sql

	FETCH  NEXT FROM instance_cursor INTO @instanceName
END
CLOSE instance_cursor
DEALLOCATE instance_cursor