ALTER FUNCTION [dbo].[GetCombinedCalendar] (
    @base_calendar_id INT, @calendar_id INT,
    @start DATE, @end DATE
    ) RETURNS TABLE AS RETURN (
    SELECT
        calendar_id AS calendar_id,
        datadate AS event_date,
        dayoff = ISNULL(dayoff2, dayoff1),
        holiday = ISNULL(holiday2, holiday1),
        notwork = ISNULL(notwork2, notwork1),
        calendar_event_id = ISNULL(
                calendar_event_id2, calendar_event_id1
            ),
        calendar_data_id = ISNULL(
                calendar_data_id2, calendar_data_id1
            ),
        calendar_static_holiday_id = ISNULL(
                calendar_holiday_id2, calendar_holiday_id1
            )
    FROM
        (
            SELECT
                @calendar_id AS calendar_id,
                event_date AS datadate,
                MAX(dayoff1) AS dayoff1,
                MAX(dayoff2) AS dayoff2,
                MAX(holiday1) AS holiday1,
                MAX(holiday2) AS holiday2,
                MAX(notwork1) AS notwork1,
                MAX(notwork2) AS notwork2,
                MAX(calendar_event_id1) AS calendar_event_id1,
                MAX(calendar_event_id2) AS calendar_event_id2,
                MAX(calendar_data_id1) AS calendar_data_id1,
                MAX(calendar_data_id2) AS calendar_data_id2,
                MAX(calendar_holiday_id1) AS calendar_holiday_id1,
                MAX(calendar_holiday_id2) AS calendar_holiday_id2
            FROM
                (
                    SELECT
                        event_date,
                        cd1.calendar_event_id AS calendar_event_id1,
                        NULL AS calendar_event_id2,
                        cd1.calendar_data_id AS calendar_data_id1,
                        NULL AS calendar_data_id2,
                        cd1.calendar_static_holiday_id AS calendar_holiday_id1,
                        NULL AS calendar_holiday_id2,
                        cd1.dayoff AS dayoff1,
                        NULL AS dayoff2,
                        cd1.holiday AS holiday1,
                        NULL AS holiday2,
                        cd1.notwork AS notwork1,
                        NULL AS notwork2
                    FROM
                        calendar_data cd1
                            LEFT JOIN calendar_static_holidays csh ON cd1.calendar_static_holiday_id = csh.calendar_static_holiday_id
                    WHERE
                        (
                                    cd1.calendar_id = (
                                    SELECT
                                        CASE use_base_calendar WHEN 0 THEN 0 ELSE @base_calendar_id END
                                    FROM
                                        calendars
                                    WHERE
                                            calendar_id = @calendar_id
                                )
                                OR (csh.calendar_id = @base_calendar_id AND cd1.calendar_static_holiday_id IS NOT NULL)
                            )
                      AND cd1.event_date BETWEEN @start
                        AND @end
                    UNION
                    SELECT
                        event_date,
                        NULL AS calendar_event_id1,
                        cd2.calendar_event_id AS calendar_event_id2,
                        NULL AS calendar_data_id1,
                        cd2.calendar_data_id AS calendar_data_id2,
                        NULL AS calendar_holiday_id1,
                        cd2.calendar_static_holiday_id AS calendar_holiday_id2,
                        NULL AS dayoff1,
                        cd2.dayoff AS dayoff2,
                        NULL AS holiday1,
                        cd2.holiday AS holiday2,
                        NULL AS notwork1,
                        cd2.notwork AS notwork2
                    FROM
                        calendar_data cd2
                    WHERE
                            cd2.calendar_id = @calendar_id
                      AND cd2.event_date BETWEEN @start
                        AND @end
                ) inner_query
            GROUP BY
                event_date
        ) outer_query
                              )
GO