IF NOT exists(select * from sysobjects WHERE name = 'process_dashboard_user_groups' AND xType= 'U')
  BEGIN
    CREATE TABLE [dbo].[process_dashboard_user_groups] (
      [process_dashboard_id] [INT] NOT NULL,
      [user_group_id]           [INT] NOT NULL,
      CONSTRAINT [FK_process_dashboard_user_groups_dashboard_id] FOREIGN KEY ([process_dashboard_id]) REFERENCES [dbo].[process_dashboards] ([process_dashboard_id])
    )
  END 