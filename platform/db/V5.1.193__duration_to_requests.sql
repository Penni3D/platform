ALTER TABLE instance_requests
  ADD instance_request_id INT IDENTITY
  CONSTRAINT PK_instance_requests PRIMARY KEY CLUSTERED
GO
ALTER TABLE instance_requests ADD durationMS INT DEFAULT(0)
GO
UPDATE instance_requests SET durationMS = 0