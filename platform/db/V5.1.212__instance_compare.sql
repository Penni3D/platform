create FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
 @data_type INT,
 @dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json
	declare @strToFound nvarchar(max) 
	declare @type int

	declare @strTable TABLE (str_to_found nvarchar(max), strtype  int)

	IF @data_type = 16 AND @dataAdditional2 IS NOT NULL
		BEGIN
			insert into @strTable (str_to_found, strtype) 
			VALUES
				 ('@SourceProcessId', 0), 
				('@SourceColumn', 0), 
				('@OptionalList1Id', 0), 
				('@OptionalList1Val', 1),
				('@OptionalList2Id', 0), 
				('@OptionalList2Val', 1),
				('@OptionalOrderColumn', 0)
		
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS=0) BEGIN
					DECLARE @startPos INT = charindex(@strToFound, @dataAdditional2)
					WHILE @startPos > 0
					BEGIN  
		
						IF @type = 0 
							BEGIN
								SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
								SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
							END
						IF @type = 1 
							BEGIN
								set @startPos = @startPos + 1
								SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
								SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
							END
				
							IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
								END
				
							SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
				IF @type = 0 
						BEGIN
							IF ISNUMERIC(@var3) = 1 
								BEGIN
								IF CAST(@var3 as int ) > 0
								BEGIN
									SET @var4 = dbo.itemColumn_GetVariable(@var3)		
									SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
					
									IF @endPos2 > @endPos1
										BEGIN
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
										END
									ELSE
										BEGIN
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
										END
								END
							END
						END
				IF @type = 1
						BEGIN
							IF LEN(@var3) > 0 AND @var3 <> ']'
							BEGIN
									DECLARE @listVal NVARCHAR(max)
									DECLARE @guid uniqueidentifier
						
									SET @var3 = replace(@var3, '[', '')
									SET @var3 = replace(@var3, ']', '')
									SET @var4 = @var3

									DECLARE @delimited CURSOR
									SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
									SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
					
									OPEN @delimited
									FETCH NEXT FROM @delimited INTO @listVal
								
								  	WHILE (@@FETCH_STATUS=0) BEGIN
										FETCH NEXT FROM @delimited INTO @listVal

											SELECT @guid = guid FROM items WHERE item_id = @listVal
									--		SET @var4 = replace(@var4, ',' + @listVal + ',', ',' + CAST(@guid as nvarchar(max) ) + ',')
									--		SET @var4 = replace(@var4, @listVal + ',',  CAST(@guid as nvarchar(max)) + ',')
										--	SET @var4 = replace(@var4, ',' + @listVal, ',' + CAST(@guid as nvarchar(max))  )
											SET @var4 = replace(@var4,  @listVal , CAST(@guid as nvarchar(max) ) )

									  END
									  CLOSE @delimited
									SET @dataAdditional2 = replace(@dataAdditional2, @var3, @var4)
							END
						END
						  CONTINUE  
					END
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		  END
		  CLOSE @getStrs
	END
 RETURN @dataAdditional2
END
GO
	create FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
 @instance NVARCHAR(MAX),
 @data_type INT,
 @dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json
	declare @strToFound nvarchar(max) 
	declare @type int

	declare @strTable TABLE (str_to_found nvarchar(max), strtype  int)

	IF @data_type = 16 AND @dataAdditional2 IS NOT NULL
		BEGIN
			insert into @strTable (str_to_found, strtype) 
			VALUES
				 ('@SourceProcessId', 0), 
				('@SourceColumn', 0), 
				('@OptionalList1Id', 0), 
				('@OptionalList1Val', 1),
				('@OptionalList2Id', 0), 
				('@OptionalList2Val', 1),
				('@OptionalOrderColumn', 0)
		
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS=0) BEGIN
					DECLARE @startPos INT = charindex(@strToFound, @dataAdditional2)
					WHILE @startPos > 0
					BEGIN  
		
						IF @type = 0 
							BEGIN
								SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
								SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
							END
						IF @type = 1 
							BEGIN
								set @startPos = @startPos + 1
								SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
								SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
							END
				
							IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
								END
				
							SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
				IF @type = 0 
						BEGIN
							IF LEN(@var3)  > 0
							BEGIN
								SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
								IF @var4 IS NOT NULL
									BEGIN
										SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
								END

								IF @endPos2 > @endPos1
									BEGIN
										SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
									END
								ELSE
									BEGIN
										SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
									END
							END
						END
				IF @type = 1
						BEGIN
							IF LEN(@var3) > 0 AND @var3 <> ']'
							BEGIN
									DECLARE @listVal NVARCHAR(max)
									DECLARE @listItemId int
									SET @var3 = replace(@var3, '[', '')
									SET @var3 = replace(@var3, ']', '')
									SET @var4 = @var3

									DECLARE @delimited CURSOR
									SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
									SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
									OPEN @delimited
									FETCH NEXT FROM @delimited INTO @listVal
								
								  	WHILE (@@FETCH_STATUS=0) BEGIN
										FETCH NEXT FROM @delimited INTO @listVal
											SELECT @listItemId = item_id FROM items WHERE guid = @listVal
										--	SET @var4 = replace(@var4, ',' + @listVal + ',', ',' + CAST(@listItemId as nvarchar(max) ) + ',')
										--	SET @var4 = replace(@var4, @listVal + ',',  CAST(@listItemId as nvarchar(max)) + ',')
										--	SET @var4 = replace(@var4, ',' + @listVal, ',' + CAST(@listItemId as nvarchar(max))  )
											SET @var4 = replace(@var4,  @listVal ,  CAST(@listItemId as nvarchar(max) ) )


									  END
									  CLOSE @delimited
									SET @dataAdditional2 = replace(@dataAdditional2, @var3, @var4)
							END
						END
						  CONTINUE  
					END
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		  END
		  CLOSE @getStrs
	END
 RETURN @dataAdditional2
END