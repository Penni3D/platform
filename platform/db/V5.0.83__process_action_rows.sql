ALTER TABLE dbo.process_action_rows ADD
	link_item_column_id int null

    ALTER TABLE [dbo].[process_action_rows]  WITH CHECK ADD  CONSTRAINT [FK_process_action_rows_link_item_column_id] FOREIGN KEY([link_item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
ON UPDATE NO ACTION
ON DELETE NO ACTION
