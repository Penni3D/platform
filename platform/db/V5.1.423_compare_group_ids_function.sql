SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processGroup_GetVariables]
(
	@processGroupIds NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @variables NVARCHAR(MAX)
	SET @processGroupIds = REPLACE(REPLACE(@processGroupIds, '[', ''), ']', '')
	SET @variables = STUFF((SELECT ', ' + variable FROM process_groups WHERE process_group_id IN (SELECT value FROM STRING_SPLIT(@processGroupIds, ',')) ORDER BY variable ASC FOR XML PATH('')), 1, 2, '')
	IF @variables IS NULL	BEGIN
		SET @variables = ''
	END
	RETURN @variables
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processGroup_GetIds]
(
	@instance NVARCHAR(10),
	@variables NVARCHAR(255)
)
RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @ids NVARCHAR(MAX)
	SET @ids = STUFF((SELECT ',' + CAST(process_group_id AS NVARCHAR) FROM process_groups WHERE instance = @instance AND variable IN (SELECT LTRIM(value) FROM STRING_SPLIT(@variables, ',')) FOR XML PATH('')), 1, 1, '')
	IF @ids IS NULL	BEGIN
		SET @ids = ''
	END
	RETURN '[' + @ids + ']'
END