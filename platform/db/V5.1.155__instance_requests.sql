CREATE TABLE instance_requests (
	[instance] NVARCHAR(10),
	[path] NVARCHAR(MAX),
	[client] NVARCHAR(255),
	[user_item_id] INT,
	[occurence] DATETIME
)