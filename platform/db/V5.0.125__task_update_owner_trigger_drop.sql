﻿exec app_set_archive_user_id 0

  DECLARE @instance NVARCHAR(10)
  DECLARE instance_Cursor CURSOR FOR SELECT instance FROM instances
  
  OPEN instance_Cursor

  FETCH NEXT FROM instance_Cursor INTO @instance

  WHILE @@fetch_status = 0
    BEGIN
      DECLARE @sql AS nvarchar(max)
      
      SET @sql = 'DROP TRIGGER _' + @instance + '_task_AFTER_UPDATE_OWNER ' 
      EXEC (@sql)
      
      FETCH  NEXT FROM instance_Cursor INTO @instance
    END

