CREATE TABLE instance_helpers (
                                  instance_helper_id int NOT NULL IDENTITY(1,1),
                                  instance nvarchar(10) NOT NULL DEFAULT (''),
                                  identifier nvarchar(255) NOT NULL,
                                  name_variable nvarchar(255) NULL DEFAULT (''),
                                  desc_variable nvarchar(255) NULL DEFAULT (''),
                                  platform tinyint NOT NULL DEFAULT(0),
                                  media_type tinyint NOT NULL DEFAULT(0),
                                  video_link nvarchar(255) NULL DEFAULT('')
);
GO

ALTER TABLE instance_helpers ADD CONSTRAINT PK_instance_help PRIMARY KEY (instance_helper_id);
GO

EXEC app_set_archive_user_id 1
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN

    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.projectAllocation', 'ALLOCATION_HELP_CREATE_1', 'ALLOCATION_HELP_CREATE_1_DESC',1,0,'allocation/allocation-request-1.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.projectAllocation', 'ALLOCATION_HELP_CREATE_2', 'ALLOCATION_HELP_CREATE_2_DESC',1,0,'allocation/allocation-request-2.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.projectAllocation', 'ALLOCATION_HELP_CREATE_3', 'ALLOCATION_HELP_CREATE_3_DESC',1,0,'allocation/allocation-request-3.mp4')

    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'resourceAllocation', 'ALLOCATION_HELP_REQ_1', 'ALLOCATION_HELP_REQ_1_DESC',1,0,'allocation/allocation-approve-1.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'resourceAllocation', 'ALLOCATION_HELP_REQ_2', 'ALLOCATION_HELP_REQ_2_DESC',1,0,'allocation/allocation-approve-2.mp4')

    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.gantt', 'GANTT_HELP_MM', 'GANTT_HELP_MM_DESC',1,0,'gantt/gantt-modify-many.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.gantt', 'GANTT_HELP_KANBAN', 'GANTT_HELP_KANBAN_DESC',1,0,'gantt/gantt-kanban.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.gantt', 'GANTT_HELP_RESCHEDULE', 'GANTT_HELP_RESCHEDULE_DESC',1,0,'gantt/gantt-slack.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.gantt', 'GANTT_HELP_CREATE', 'GANTT_HELP_CREATE_DESC',1,0,'gantt/gantt-create-1.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.gantt', 'GANTT_HELP_ADD_TASK', 'GANTT_HELP_ADD_TASK_DESC',1,0,'gantt/gantt-create-2.mp4')
    INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link) VALUES (@instance,'process.gantt', 'GANTT_HELP_DEPEDENCIES', 'GANTT_HELP_DEPEDENCIES_DESC',1,0,'gantt/gantt-dependencies-1.mp4')


    FETCH  NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor