EXEC app_set_archive_user_id 0;

SET NOCOUNT ON;

	DECLARE @delete_sql NVARCHAR(MAX);
	DECLARE @create_sql NVARCHAR(MAX);
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @GetTables CURSOR

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT instance FROM instances
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

  WHILE (@@FETCH_STATUS=0) BEGIN
    SET @delete_sql = 
'
IF object_id(N''f_' + @TableName + '_BudgetItemYearNPV'', N''FN'') IS NOT NULL
	DROP FUNCTION f_' + @TableName + '_BudgetItemYearNPV;
'
	EXEC (@delete_sql);

    SET @create_sql = 
'
CREATE FUNCTION f_' + @TableName + '_BudgetItemYearNPV
(
	 @project_item_id INT -- t�m�n pit�isi olla projektin tms id
	,@discount_rate DECIMAL(19,4) -- t�m� tulisi projektilta (on yleinen, mutta ei sillain yleinen, ett� jos joku muuttaa nyt niin vaihtuu historiaan)
	,@start_year INT -- t�m� tulisi projektilta
	,@npv_years_count INT -- t�m� tulisi projektilta (tai sitten projektin budjettivuosien mukaan - mutta onko vertailukelpoista jos eri)?
	,@budget_item_type VARCHAR(max) -- t�m�n pit�isi tulla jotenkin konfiguraatioista
	,@budget_item_category_income_type VARCHAR(max) -- t�m�n pit�isi tulla jotenkin konfiguraatioista
	,@budget_item_category_expense_type VARCHAR(max) -- t�m�n pit�isi tulla jotenkin konfiguraatioista
) RETURNS DECIMAL(19,4)
AS
BEGIN

	DECLARE @npv_return_value DECIMAL(19,4) = 0;

	--DECLARE @start_year INT = 2014; -- t�m� pit�isi valita jotenkin annetulta projektilta, ettei tarvitse olla nollalukuja alkuvuodessa
	--DECLARE @discount_rate DECIMAL(19,4) = 0.1; -- t�m�n pit�� tulla projektilta (on yleinen, mutta ei sillain yleinen, ett� jos joku muuttaa nyt niin vaihtuu historiaan)
	--DECLARE @npv_years_count INT = 5; -- t�m�n pit�isi varmaan tulla jotenkin konfiguraatioista (tai sitten projektin budjettivuosien mukaan - mutta onko vertailukelpoista jos eri)?
	--DECLARE @income_budget_type VARCHAR(max) = ''1356''; -- t�m�n pit�isi tulla jotenkin konfiguraatioista
	--DECLARE @expense_budget_type VARCHAR(max) = ''1354,1355''; -- t�m�n pit�isi tulla jotenkin konfiguraatioista

	IF (@start_year IS NULL OR CAST(@start_year AS INT) < 1970)
		RETURN NULL
	IF (@npv_years_count IS NULL OR CAST(@npv_years_count AS INT) > 500)
		RETURN NULL

	DECLARE @start_date DATETIME = (CAST(CASE WHEN @start_year IS NULL THEN YEAR(getdate()) ELSE @start_year END as VARCHAR) + ''-01-01'');

	;WITH yearlist AS 
	(
		SELECT 0 as ix, CAST(@start_date as DATETIME) as [date] 
		UNION ALL
		SELECT yl.ix + 1 as ix, CAST((CAST((YEAR(yl.date) + 1) as VARCHAR) + ''-01-01'') as DATETIME) as [date]
		FROM yearlist yl
		WHERE YEAR(yl.date + 1) < YEAR(CAST(@start_date as DATETIME)) + @npv_years_count
	)
	--select date from yearlist order by date desc;

	SELECT @npv_return_value = SUM(z.sum_amount / POWER(CAST(1+@discount_rate as DECIMAL(19,4)), z.ix+1)) FROM ( -- z.ix --> DATEDIFF(xyz, min_date, z.date)+1
		SELECT yearlist.ix, (SELECT MIN(yearlist.date) FROM yearlist) as min_date, yearlist.date date, y.sum_amount FROM yearlist -- ISNULL(y.sum_amount,0) sum_amount
		LEFT JOIN (
			SELECT 
			   DATEADD(year, DATEDIFF(year, 0, x.date), 0) as date
			  ,SUM(
				CASE WHEN '',''+@budget_item_type+'','' LIKE ''%,'' + CAST(x.budtype as VARCHAR) + '',%'' AND '',''+@budget_item_category_expense_type+'','' LIKE ''%,'' + CAST(x.budcat as VARCHAR) + '',%'' THEN -1 * x.amount ELSE 0 END +
				CASE WHEN '',''+@budget_item_type+'','' LIKE ''%,'' + CAST(x.budtype as VARCHAR) + '',%'' AND '',''+@budget_item_category_income_type+'','' LIKE ''%,'' + CAST(x.budcat as VARCHAR) + '',%'' THEN x.amount ELSE 0 END
			   ) as sum_amount
			FROM (
				SELECT 
				 mbi.project_item_id
				 --,mbr.description
				 ,budcat = (SELECT idps_mbi.selected_item_id FROM [item_data_process_selections] idps_mbi WHERE idps_mbi.item_id =  mbi.item_id AND idps_mbi.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = ''' + @TableName + ''' AND ic.process = ''budget_item'' AND ic.name = ''category_item_id''))
				 ,budtype = (SELECT idps_mbi.selected_item_id FROM [item_data_process_selections] idps_mbi WHERE idps_mbi.item_id =  mbi.item_id AND idps_mbi.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = ''' + @TableName + ''' AND ic.process = ''budget_item'' AND ic.name = ''type_item_id''))
				 --,budcat = (SELECT idps_mbr.selected_item_id FROM [item_data_process_selections] idps_mbr WHERE idps_mbr.item_id =  mbr.item_id AND idps_mbr.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = ''' + @TableName + ''' AND ic.process = ''budget_row'' AND ic.name = ''category_item_id''))
				 ,mbi.date
				 ,mbi.amount
				FROM [_' + @TableName + '_budget_item] mbi
				 --INNER JOIN [_' + @TableName + '_budget_row] mbr ON mbr.item_id = (SELECT idps_mbi.selected_item_id FROM [item_data_process_selections] idps_mbi WHERE idps_mbi.item_id = mbi.item_id AND idps_mbi.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = ''' + @TableName + ''' AND ic.process = ''budget_item'' AND ic.name = ''parent_item_id''))
				WHERE
				 mbi.project_item_id = @project_item_id 
				--ORDER BY mbr.project_item_id, date, budcat
			) as x
			GROUP BY DATEADD(year, DATEDIFF(year, 0, x.date), 0)
		) as y ON YEAR(yearlist.date) = YEAR(y.date)
	) as z
	OPTION (MAXRECURSION 10000)

	RETURN @npv_return_value;

END
'
	EXEC (@create_sql);
    FETCH NEXT FROM @GetTables INTO @TableName
  END
  CLOSE @GetTables