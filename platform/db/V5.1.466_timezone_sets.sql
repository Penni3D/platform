EXEC app_set_archive_user_id 1
DECLARE @Instance NVARCHAR(255);
DECLARE @Sq NVARCHAR(MAX);

DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances 

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
		set @Sq = 'UPDATE _' + @Instance + '_user SET timezone = CASE locale_id WHEN ''en-GB'' THEN ''UTC_0'' ELSE ''FLE Standard Time_120'' END' 
		exec sp_executesql @Sq
	
        FETCH NEXT FROM cursor_process INTO 
            @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;


UPDATE instances SET default_timezone = CASE default_locale_id WHEN 'en-GB' THEN 'UTC_0' ELSE 'FLE Standard Time_120' END