EXEC app_set_archive_user_id 1
DECLARE @Instance NVARCHAR(255);
DECLARE cursor_process CURSOR FOR SELECT instance FROM instances
OPEN cursor_process
FETCH NEXT FROM cursor_process INTO @Instance
WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE @Sql1 NVARCHAR(MAX) = 'UPDATE _' + @instance + '_user SET ketoauth = 1, twofactor = 0 where twofactor = 1'
        EXEC sp_executesql @Sql1
        FETCH NEXT FROM cursor_process INTO @Instance
    END;
CLOSE cursor_process;
DEALLOCATE cursor_process;