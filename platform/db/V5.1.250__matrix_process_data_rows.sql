
	DECLARE @sql NVARCHAR(MAX);
	DECLARE @instance NVARCHAR(MAX);
	DECLARE @GetInstances CURSOR
	DECLARE @i1 INT;

	--Get Tables
	SET @GetInstances = CURSOR FAST_FORWARD LOCAL FOR 
	select instance from instances

	OPEN @GetInstances
	FETCH NEXT FROM @GetInstances INTO @instance

	WHILE (@@FETCH_STATUS=0) BEGIN
		

	DECLARE @process NVARCHAR(50) = 'matrix_process_row_data'
	DECLARE @tableName NVARCHAR(50) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2

	--Create conditions
	EXEC app_ensure_condition @instance, @process, 0, 0

	--Create columns
	EXEC app_ensure_column @instance, @process, 'item_column_id', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'question_item_id', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT
	EXEC app_ensure_column @instance, @process, 'cell_item_id', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'score', 0, 1, 1, 1, 0, null,  0, 0

	update item_columns SET use_fk = 1 WHERE item_column_id = @i1

	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 
		
		FETCH NEXT FROM @GetInstances INTO @instance
	END
	CLOSE @GetInstances