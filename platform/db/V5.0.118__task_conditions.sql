﻿exec app_set_archive_user_id 0

  DECLARE @instance NVARCHAR(10)
  DECLARE instance_Cursor CURSOR FOR SELECT instance FROM instances
  
  OPEN instance_Cursor

  FETCH NEXT FROM instance_Cursor INTO @instance

  WHILE @@fetch_status = 0
    BEGIN
		delete from condition_groups where condition_id in (select condition_id from conditions where instance = @instance AND process = 'task')


		DECLARE @g1 INT
		EXEC app_ensure_group @instance,'task','ALL', @group_id = @g1 OUTPUT
		

		insert into process_group_tabs (process_group_id, process_tab_id, order_no) 
		(select @g1, process_tab_id, 'a' from process_tabs where process = 'task')

	
	--Create conditions
  	DECLARE @con13 INT
	EXEC app_ensure_condition @instance, 'task', 0, 0, 'ALL', @condition_id = @con13 OUTPUT

  	INSERT INTO condition_states (condition_id, state) VALUES (@con13, 'write')

	insert into condition_groups (condition_id, process_group_id) (select condition_id, @g1 from conditions where instance = @instance AND process = 'task' )


      FETCH  NEXT FROM instance_Cursor INTO @instance
    END

