ALTER TABLE dbo.item_columns ADD
	use_fk tinyint NOT NULL CONSTRAINT DF_item_columns_use_fk DEFAULT 0


GO
/****** Object:  Trigger [dbo].[items_fk_delete]    Script Date: 03/10/2017 9.33.00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
AS 
 set xact_abort off; 
	DECLARE @ids nvarchar(max) =(select ',' + CAST(item_id as nvarchar) FROM deleted FOR XML PATH(''))
	
	DECLARE @tName as nvarchar(max)
	DECLARE @cName as nvarchar(max)

	DECLARE @countSql as nvarchar (max) = ''


	IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
	CREATE table #DeleteItems (item_id int)
			
	DECLARE @GetTables CURSOR
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	select ccu.TABLE_NAME,  ccu.COLUMN_NAME from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
	inner join  INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
	where UNIQUE_CONSTRAINT_NAME = 'PK_items' AND substring(ccu.TABLE_NAME, 1, 1) = '_'

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName

	WHILE (@@FETCH_STATUS=0) BEGIN
		DECLARE @sql nvarchar(max) = 'INSERT INTO #DeleteItems (item_id) (SELECT item_id FROM ' + @tName + ' WHERE ' + @cName + ' IN (-1' + @ids + '))'
		EXECUTE (@sql)
		set @countSql = @countSql + ' SELECT @childCount = @childCount + count(item_id) FROM ' + @tName + ' WHERE ' + @cName + ' = @delId ; '
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

    DELETE  FROM item_subitems 					WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data 						WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM process_tabs_subprocess_data 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE process_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE task_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)

--	DELETE FROM items WHERE item_id IN (SELECT item_id FROM #DeleteItems)

	DECLARE @delId as int
	DECLARE @GetIds CURSOR

	DECLARE @delIdCount int
	DECLARE @oldDelIdCount int

	DECLARE @DeleteItems2 TABLE (item_id int)	
	DECLARE @DeleteSuccess TABLE (item_id int)	

	INSERT INTO @DeleteItems2 select item_id FROM #DeleteItems

	SELECT @delIdCount = COUNT(item_id)  FROM  @DeleteItems2 

	WHILE @delIdCount > 0 
		BEGIN
		
			SET @GetIds = CURSOR FAST_FORWARD FORWARD_ONLY LOCAL FOR select item_id from @DeleteItems2 WHERE item_id NOT IN (select item_id FROM @DeleteSuccess)
			OPEN @GetIds
			FETCH NEXT FROM @GetIds INTO @delId

			WHILE (@@FETCH_STATUS=0) BEGIN


			declare @cnt int

			SET @countSql = 'SET @childCount = 0; ' + @countSql 
			EXECUTE sp_executesql @countSql, N'@delId int,@childCount int OUTPUT', @delId = @delId, @childCount=@cnt OUTPUT

			IF @cnt = 0
			BEGIN
						DELETE FROM items WHERE item_id = @delId
						INSERT INTO @DeleteSuccess (item_id) values (@delId)
			END
				
			FETCH NEXT FROM @GetIds INTO @delId
			END
			CLOSE @GetIds

			SELECT @delIdCount = COUNT(item_id)  FROM  @DeleteItems2  where item_id NOT IN (select item_id from @DeleteSuccess)
		END

    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data dd						INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.process_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.task_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id
	
	--Finally delete the item
	DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id



GO
/****** Object:  Trigger [dbo].[AfterUpdateColumn]    Script Date: 02/10/2017 15.31.56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[AfterUpdateColumn] ON [dbo].[item_columns] AFTER update AS
DECLARE @GetChanges CURSOR
DECLARE @TableName NVARCHAR(255)
DECLARE @OldColumnName NVARCHAR(255)
DECLARE @ColumnName NVARCHAR(50)
DECLARE @TableColumnName NVARCHAR(MAX)
DECLARE @DataType TINYINT
DECLARE @UseFk TINYINT

DECLARE @sql nvarchar(max)

SET @GetChanges = CURSOR FAST_FORWARD LOCAL FOR 
SELECT it.name AS TableName, i.name AS ColumnName, i.data_type, d.name As OldColumnName, i.use_fk FROM item_tables it
INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
INNER JOIN deleted d ON i.item_column_id = d.item_column_id

OPEN @GetChanges
FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType,@OldColumnName, @UseFk
WHILE (@@FETCH_STATUS=0) 
BEGIN
	IF @ColumnName IS NOT NULL AND @OldColumnName IS NULL AND LEN(@ColumnName) > 0
		BEGIN
			EXEC dbo.items_AddColumn @TableName,@ColumnName,@DataType
		END
	ELSE 
		BEGIN
			IF @ColumnName <> @OldColumnName
				BEGIN
					SET @TableColumnName = @TableName + '.' + @OldColumnName
					EXEC sp_rename @TableColumnName, @ColumnName, 'COLUMN'
				END
		END
	IF @UseFk = 1 
	BEGIN
	IF Not Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']')) 
		BEGIN			
			SET @sql = ' ALTER TABLE ' + @TableName + ' WITH CHECK ADD  CONSTRAINT FK_' + @TableName+ '_' + @ColumnName + ' FOREIGN KEY(' + @ColumnName + ')
			REFERENCES [dbo].[items] ([item_id])
				 '
			EXEC (@sql)
		END
	END
	ELSE
	BEGIN
		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']')) BEGIN			
			SET @sql = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT FK_' + @TableName+ '_' + @ColumnName
			EXEC (@sql)
		END
	END

	FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType, @OldColumnName, @UseFk
END
