﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[process_action_rows]') AND name = 'value2') 
    ALTER TABLE process_action_rows
          ADD value2 nvarchar(MAX) NULL
