EXEC app_set_archive_user_id 1
DECLARE @instance NVARCHAR(255)
DECLARE @sql NVARCHAR(MAX)
DECLARE @item_column_id INT
DECLARE @process_porfolio_id INT
DECLARE @process_container_id INT
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@FETCH_STATUS = 0 BEGIN 
	IF(SELECT COUNT (*) FROM item_columns WHERE process = 'notification' and name = 'compare' AND instance = @instance) = 0 AND (SELECT COUNT (*) FROM processes WHERE process = 'notification' AND instance = @instance) > 0
		BEGIN
			EXEC dbo.app_ensure_column @instance, 'notification', 'compare', 0, 1, 1, 0, NULL, 0, 0, NULL, NULL, 0, @item_column_id OUTPUT

			DECLARE @table_name NVARCHAR(50) = '_' + @instance + '_notification'

			SET @sql = 'app_ensure_archive ''' + @table_name + ''''
			EXEC (@sql)

			SET @sql = 'ALTER TABLE ' + @table_name + ' ADD DEFAULT 0 FOR compare'
			EXEC (@sql)

			SET @sql = 'UPDATE ' + @table_name + ' SET compare = 0'
			EXEC (@sql)

			SET @process_porfolio_id = 0
			SELECT @process_porfolio_id = process_portfolio_id FROM process_portfolios WHERE instance = @instance AND process = 'notification' AND variable = 'port_notification_HTTP_REQUESTS'
			IF @process_porfolio_id > 0 BEGIN
				INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, show_type, use_in_filter, archive_item_column_id, width, priority, sum_column, is_default, use_in_search, report_column, short_name, validation, portfolio_column_group_id)
				VALUES (@process_porfolio_id, @item_column_id, 'zzz', 0, NULL, 0, NULL, NULL, NULL, 0, 1, 0, 0, NULL, NULL, NULL)
			END

			SET @process_container_id = 0
			SELECT @process_container_id = process_container_id FROM process_containers WHERE instance = @instance AND process = 'notification' AND variable = 'cont_notification_right_container_1'
			IF @process_container_id > 0 BEGIN
				INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, order_no, help_text)
				VALUES (@process_container_id, @item_column_id, NULL, NULL, 'zzz', NULL)
			END
		END	
    FETCH NEXT FROM instance_cursor INTO @instance
END
CLOSE instance_cursor
DEALLOCATE instance_cursor



GO



UPDATE instance_schedules SET compare = 2 WHERE compare = 3