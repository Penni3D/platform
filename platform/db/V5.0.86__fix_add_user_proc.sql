ALTER PROCEDURE [dbo].[app_create_user]
	@instance NVARCHAR(50), @process NVARCHAR(50), @first_name NVARCHAR(255), @last_name NVARCHAR(255), @login NVARCHAR(255), @email NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;
 	
	DECLARE @sql NVARCHAR(MAX) = ''
	DECLARE @itemId INT
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @locale_id NVARCHAR(5)
	DECLARE @date_format NVARCHAR(10)
	DECLARE @first_day_of_week TINYINT
	DECLARE @timezone NVARCHAR(255)

	SET @tableName = '_' + @instance + '_' + @process
	EXEC app_set_archive_user_id 0;

	SET @first_name = REPLACE(@first_name, CHAR(39), '"')
	SET @last_name = REPLACE(@last_name, CHAR(39), '"')
	SET @login = REPLACE(@login, CHAR(39), '"')
	SET @email = REPLACE(@email, CHAR(39), '"')

	SELECT @locale_id = default_locale_id, @date_format = default_date_format, @first_day_of_week = default_first_day_of_week, @timezone = default_timezone FROM instances WHERE instance = @instance
 
	INSERT INTO items (instance, process) VALUES (@instance, @process); SET @itemId = @@identity;
	SET @sql = 'UPDATE ' + @tableName 
							+ ' SET login = ''' + REPLACE(@login,'"',CHAR(39)+CHAR(39))  
							+ ''', first_name=''' + REPLACE(@first_name,'"',CHAR(39)+CHAR(39)) 
							+ ''', last_name=''' + REPLACE(@last_name,'"',CHAR(39)+CHAR(39))  
							+ ''', email = ''' + REPLACE(@email,'"',CHAR(39)+CHAR(39))  
							+ ''', date_format = ''' + @date_format 
							+ ''', timezone = ''' + @timezone 
							+ ''', locale_id = ''' + @locale_id 
							+ ''', first_day_of_week = ' 
							+ CAST(@first_day_of_week AS NVARCHAR) 
							+ ', active = 1 '
							+ 'WHERE item_id = ' + CAST(@itemId AS nvarchar) 
	EXEC (@sql)
END