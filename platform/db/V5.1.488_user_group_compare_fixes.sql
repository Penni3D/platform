DECLARE @instance NVARCHAR(10)
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
DECLARE @column_id INT
DECLARE @process NVARCHAR(255)
DECLARE @action_id INT
DECLARE @code NVARCHAR(5)
DECLARE @process_action_group_id INT
DECLARE @sql NVARCHAR(255)
DECLARE @item_id INT
DECLARE @value NVARCHAR(255)
DECLARE @help_text NVARCHAR(MAX)
DECLARE @container_id INT
EXEC app_set_archive_user_id 1
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
    SELECT @column_id = item_column_id FROM item_columns WHERE instance = @instance AND process = 'user_group' AND [name] = 'compare'

    SELECT @process = process FROM processes WHERE instance = @instance AND process = 'user_group'  AND new_row_action_id IS NULL AND after_new_row_action_id IS NULL
    IF @process = 'user_group' BEGIN
        -- add action
        EXEC app_ensure_action @instance, 'user_group', 'new_user_group', @process_action_id = @action_id OUTPUT

        UPDATE process_translations SET translation = 'New user group' WHERE instance = @instance AND process = 'user_group' AND variable = 'action_user_group_new_user_group' AND code = 'en-GB'

        SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'fi-FI'
        IF @code = 'fi-FI' BEGIN
            INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'action_user_group_new_user_group', 'Uusi käyttäjäryhmä', 'fi-FI')
        END

        SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'en-US'
        IF @code = 'en-US' BEGIN
            INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'action_user_group_new_user_group', 'New user group', 'en-US')
        END

        SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'de-DE'
        IF @code = 'de-DE' BEGIN
            INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'action_user_group_new_user_group', 'New user group', 'de-DE')
        END

        -- add action row group
        INSERT INTO process_action_rows_groups ([type], instance, process, variable, in_use, order_no) VALUES (1, @instance, 'user_group', 'parg_user_group_new_user_group', 1, '')
        SET @process_action_group_id = @@IDENTITY

        SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'en-GB'
        IF @code = 'en-GB' BEGIN
            INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'parg_user_group_new_user_group', 'New user group', 'en-GB')
        END

        SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'fi-FI'
        IF @code = 'fi-FI' BEGIN
            INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'parg_user_group_new_user_group', 'Uusi käyttäjäryhmä', 'fi-FI')
        END

        SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'en-US'
        IF @code = 'en-US' BEGIN
            INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'parg_user_group_new_user_group', 'New user group', 'en-US')
        END

        SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'de-DE'
        IF @code = 'de-DE' BEGIN
            INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'parg_user_group_new_user_group', 'New user group', 'de-DE')
        END

        -- add action row
        SET @sql = 'SELECT TOP 1 @item_id = item_id FROM _' + @instance + '_list_generic_yes_no WHERE value = 0 ORDER BY item_id ASC'
        EXECUTE sp_executesql @sql, N'@item_id NVARCHAR(255) OUTPUT', @item_id = @item_id OUTPUT
        SET @value = '[' + CAST(@item_id AS NVARCHAR) + ']'

        EXEC app_ensure_action_row @action_id, 0, @column_id, 0, @value

        UPDATE process_action_rows SET action_group_id = @process_action_group_id WHERE process_action_id = @action_id

        -- action to process
        UPDATE processes SET new_row_action_id = @action_id WHERE instance = @instance AND process = 'user_group'
    END

    -- change field name
    UPDATE process_translations SET translation = 'Include in sync' WHERE instance = @instance AND process = 'user_group' AND variable = 'IC_USER_GROUP_COMPARE' AND code IN ('en-GB', 'en-US', 'de-DE')
    UPDATE process_translations SET translation = 'Ota mukaan vertailussa' WHERE instance = @instance AND process = 'user_group' AND variable = 'IC_USER_GROUP_COMPARE' AND code = 'fi-FI'

    -- add help text
    SET @help_text = '{"en-GB":{"html":"<p>Setting to include this group in a configurations sync between two environments. For example when transferring configurations from Test-environment to Production-environment. But if group has been assigned with user rights, group is automatically always synced.</p>","delta":{"ops":[{"insert":"Setting to include this group in a configurations sync between two environments. For example when transferring configurations from Test-environment to Production-environment. But if group has been assigned with user rights, group is automatically always synced.\n"}]}},"fi-FI":{"html":"<p>Asetus jolla käyttäjäryhmä voidaan sisällyttää konfiguroinnin synkronointiin ympäristöjen välillä. Esimerkiksi, kun konfigurointimuutoksia siirretään testiympäristöstä tuotantoympäristöön. Mutta jos käyttäjäryhmä on asetettu oikeustasoihin, ryhmä synkronoidaan aina automaattisesti.</p>","delta":{"ops":[{"insert":"Asetus jolla käyttäjäryhmä voidaan sisällyttää konfiguroinnin synkronointiin ympäristöjen välillä. Esimerkiksi, kun konfigurointimuutoksia siirretään testiympäristöstä tuotantoympäristöön. Mutta jos käyttäjäryhmä on asetettu oikeustasoihin, ryhmä synkronoidaan aina automaattisesti.\n"}]}},"en-US":{"html":"<p>Setting to include this group in a configurations sync between two environments. For example when transferring configurations from Test-environment to Production-environment. But if group has been assigned with user rights, group is automatically always synced.</p>","delta":{"ops":[{"insert":"Setting to include this group in a configurations sync between two environments. For example when transferring configurations from Test-environment to Production-environment. But if group has been assigned with user rights, group is automatically always synced.\n"}]}},"de-DE":{"html":"<p>Setting to include this group in a configurations sync between two environments. For example when transferring configurations from Test-environment to Production-environment. But if group has been assigned with user rights, group is automatically always synced.</p>","delta":{"ops":[{"insert":"Setting to include this group in a configurations sync between two environments. For example when transferring configurations from Test-environment to Production-environment. But if group has been assigned with user rights, group is automatically always synced.\n"}]}}}'

    SELECT TOP 1 @container_id = process_container_id FROM process_containers WHERE instance = @instance AND process = 'user_group' ORDER BY process_container_id ASC

    UPDATE process_container_columns SET help_text = @help_text WHERE process_container_id = @container_id AND item_column_id = @column_id

    FETCH  NEXT FROM instance_cursor INTO @instance
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @strToFound2 NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @strTable2 TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @getStrs2 CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0),
						('@PLUGProcessListColumn', 0),
						('@PLUGListsGroupColumns', 0),
						('@PLUGNoUserGroupColumns', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									IF @type = 0 
										BEGIN
											IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
												BEGIN
													SET @startPos = @startPos + 1
													SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
												END
											ELSE
												BEGIN
													SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
												END
										END
							
									IF @type = 1 
										BEGIN
											IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
												BEGIN
													SET @startPos = @startPos + 1
													SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos) 
												END
											ELSE
												BEGIN
													SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos) 
												END
										END
				
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				

									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']' AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @colId NVARCHAR(MAX)
													DECLARE @colVar NVARCHAR(MAX)
													SET @var4 = @var3
													DECLARE @delimited2 CURSOR
													SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

													OPEN @delimited2
													FETCH NEXT FROM @delimited2 INTO @colId

													DECLARE @start2 INT
													SET @start2 = 1
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF CAST(@colId AS INT) > 0
																BEGIN
																	SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))		
																	IF @colVar IS NULL
																		BEGIN
																			SET @colVar = 'NULL' 
																		END
																	SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
																	SET @start2 = @start2 + LEN(@colVar) + 1
																END
															FETCH NEXT FROM @delimited2 INTO @colId
														END
													CLOSE @delimited2

													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']' AND @var3 <> '0'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @guid UNIQUEIDENTIFIER
						
													SET @var3 = REPLACE(@var3, '[', '')
													SET @var3 = REPLACE(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													DECLARE @start INT
													SET @start = 1
								  					WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															DECLARE @gu_id NVARCHAR(MAX)
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = 'NULL' 
																END
															SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

															SET @start = @start + LEN(@gu_id) + 1

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0),
						('fromProcessColumnId', 0),
						('targetProcessColumnId', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
				
									IF @type = 0
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 1
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processPortfolio_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 2
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
				
			IF @data_type = 21
				BEGIN
					INSERT INTO @strTable2 (str_to_found, strtype) 
					VALUES
						('"action":', 2),
						('"containerId":', 3)
	
					SET @getStrs2 = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable2
			
					OPEN @getStrs2
					FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound2)
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 2
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 3
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processContainer_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2, @startPos + 1)
								END
							FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
		
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0 
										BEGIN
											IF LEFT(LTRIM(@var3), 5) = '"tab_'
												BEGIN
													SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
													IF ISNUMERIC(@var3) = 1 
														BEGIN
															IF CAST(@var3 AS INT) > 0
																BEGIN
																	SET @var4 = dbo.processTab_GetVariable(@var3)
																	IF @var4 IS NOT NULL
																		BEGIN
																			SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																		END
																END
														END
													ELSE
														BEGIN
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processActionDialog_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END

	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @strToFound2 NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @strTable2 TABLE (str_to_found2 NVARCHAR(MAX), strtype2 INT)
	DECLARE @getStrs CURSOR
	DECLARE @getStrs2 CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN
			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0),
						('@PLUGProcessListColumn', 0),
						('@PLUGListsGroupColumns', 0),
						('@PLUGNoUserGroupColumns', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2

									IF @type = 0 
										BEGIN
											IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
												BEGIN
													SET @startPos = @startPos + 1
													SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
												END
											ELSE
												BEGIN
													SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
												END
										END
									
									IF @type = 1 
										BEGIN
											IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
												BEGIN
													SET @startPos = @startPos + 1
													SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos) 
												END
											ELSE
												BEGIN
													SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
													SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos) 
												END
										END
									
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											DECLARE @colId INT
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colVar

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @colVar <> 'null'
														BEGIN
															SET @colId = dbo.itemColumn_GetId(@instance, @colVar)		
															IF @colId IS NULL
																BEGIN
																	SET @colId = 0
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colVar), CAST(@colId AS NVARCHAR(MAX)))
															SET @start2 = @start2 + LEN(CAST(@colId AS NVARCHAR(MAX))) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colVar
												END
											CLOSE @delimited2

											SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)											
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']' AND @var3 <> '0'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @listItemId INT

													SET @var3 = REPLACE(@var3, '[', '')
													SET @var3 = REPLACE(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
													
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
			
			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0),
						('fromProcessColumnId', 0),
						('targetProcessColumnId', 0)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 2
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
						
			IF @data_type = 21
				BEGIN
					INSERT INTO @strTable2 (str_to_found2, strtype2) 
					VALUES
						('"action":', 2),
						('"containerId":', 3)
					
					SET @getStrs2 = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found2, strtype2 FROM @strTable2 
					
					OPEN @getStrs2
					FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound2)
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 2
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 3
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processContainer_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2, @startPos + 1)
								END
							FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
						END
				END
			
			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2

									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEFT(@var3, 4) = 'tab_'
												BEGIN
													IF LEN(@var3) > 0
														BEGIN
															SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
															IF @var4 IS NULL
																BEGIN
																	SET @var4 = ' ""'
																END
															ELSE
																BEGIN
																	SET @var4 = ' "tab_' + @var4 + '"'
																END
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
									
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = @var3
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END