ALTER TABLE dbo.process_tabs
	DROP CONSTRAINT FK_process_tabs_item_columns
GO
ALTER TABLE dbo.item_columns SET (LOCK_ESCALATION = TABLE)
GO
--COMMIT
--select Has_Perms_By_Name(N'dbo.item_columns', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.item_columns', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.item_columns', 'Object', 'CONTROL') as Contr_Per 
--BEGIN TRANSACTION
GO
ALTER TABLE dbo.process_tabs ADD CONSTRAINT
	FK_process_tabs_item_columns FOREIGN KEY
	(
	archive_item_column_id
	) REFERENCES dbo.item_columns
	(
	item_column_id
	) ON UPDATE  SET NULL 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.process_tabs SET (LOCK_ESCALATION = TABLE)
GO
--COMMIT
select Has_Perms_By_Name(N'dbo.process_tabs', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.process_tabs', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.process_tabs', 'Object', 'CONTROL') as Contr_Per 