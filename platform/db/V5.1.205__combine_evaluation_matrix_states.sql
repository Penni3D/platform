﻿--This migration fixes different states variable usage in old Keto 5 databases and changes them to the new current feature state.
EXEC app_set_archive_user_id 0;
UPDATE process_group_features SET feature = 'evaluationMatrix' WHERE feature = 'EvaluationMatrix' OR feature = 'evaluation_matrix';
UPDATE condition_features SET feature = 'evaluationMatrix' WHERE feature = 'EvaluationMatrix' OR feature = 'evaluation_matrix';
UPDATE process_group_navigation SET variable = 'evaluationMatrix' WHERE variable = 'EvaluationMatrix' OR variable = 'evaluation_matrix'