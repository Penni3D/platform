﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_issues'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_issues] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_issues]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	exec app_set_archive_user_id 0
	declare @process nvarchar(50) = 'issues'

	----Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--create condition
	EXEC app_ensure_condition @instance, @process, 0, 0

	--Create group
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'ISSUES_PHASE', @group_id = @g1 OUTPUT
		
	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'ISSUES_TAB', @g1, @tab_id = @t1 OUTPUT
		
	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'ISSUES_CONTAINER', @t1, 1, @container_id = @c1 OUTPUT
		
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'ISSUES_PORTFOLIO', @portfolio_id = @p1 OUTPUT

	--Create conditions
	DECLARE @cId INT	
	EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT
	INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')

	--Create columns for row manager
	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 1, 1, 1, 0, null, 0, 0, 0
	EXEC app_ensure_column @instance, @process, 'task_type', 0, 1, 1, 1, 0, null, 0, 0, 0
	
	--Create list_issues_proximity -list
	DECLARE @processListProximity NVARCHAR(50) = 'list_issues_proximity'
	DECLARE @tableNameProximity NVARCHAR(50) = ''
	SET @tableNameProximity = '_' + @instance + '_' + @processListProximity
	DECLARE @sqlProximity NVARCHAR(MAX) = ''

	--Create process
	DECLARE @ListProximityId INT
	EXEC app_ensure_list @instance, @processListProximity, @processListProximity, 0

	UPDATE processes SET list_order = 'value' WHERE process = @processListProximity

	EXEC dbo.app_ensure_column @instance,@processListProximity, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProximity, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProximity, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProximity, 'value', 0, 0, 1, 0, 0, NULL, 0, 0


	INSERT INTO items (instance, process) VALUES (@instance, @processListProximity);
	DECLARE @itemIdProximity INT = @@identity;
	SET @sqlProximity = 'UPDATE ' + @tableNameProximity + ' SET list_item = ''Highly unlikely'', value = 1 WHERE item_id = ' +  CAST(@itemIdProximity AS nvarchar)
	EXEC (@sqlProximity)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProximity);
	SET @itemIdProximity = @@identity;
	SET @sqlProximity = 'UPDATE ' + @tableNameProximity + ' SET list_item = ''Unlikely'', value = 2 WHERE item_id = ' +  CAST(@itemIdProximity AS nvarchar)
	EXEC (@sqlProximity)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProximity);
	SET @itemIdProximity = @@identity;
	SET @sqlProximity = 'UPDATE ' + @tableNameProximity + ' SET list_item = ''Average'', value = 3 WHERE item_id = ' +  CAST(@itemIdProximity AS nvarchar)
	EXEC (@sqlProximity)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProximity);
	SET @itemIdProximity = @@identity;
	SET @sqlProximity = 'UPDATE ' + @tableNameProximity + ' SET list_item = ''Probable'', value = 4 WHERE item_id = ' +  CAST(@itemIdProximity AS nvarchar)
	EXEC (@sqlProximity)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProximity);
	SET @itemIdProximity = @@identity;
	SET @sqlProximity = 'UPDATE ' + @tableNameProximity + ' SET list_item = ''Inevitable'', value = 5 WHERE item_id = ' +  CAST(@itemIdProximity AS nvarchar)
	EXEC (@sqlProximity)


	--Create list_issues_impact -list
	DECLARE @processListImpact NVARCHAR(50) = 'list_issues_impact'
	DECLARE @tableNameImpact NVARCHAR(50) = ''
	SET @tableNameImpact = '_' + @instance + '_' + @processListImpact
	DECLARE @sqlImpact NVARCHAR(MAX) = ''

	--Create process
	DECLARE @ListImpactId INT
	EXEC app_ensure_list @instance, @processListImpact, @processListImpact, 0

	UPDATE processes SET list_order = 'value' WHERE process = @processListImpact

	EXEC dbo.app_ensure_column @instance,@processListImpact, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListImpact, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListImpact, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListImpact, 'value', 0, 0, 1, 0, 0, NULL, 0, 0


	INSERT INTO items (instance, process) VALUES (@instance, @processListImpact);
	DECLARE @itemIdImpact INT = @@identity;
	SET @sqlImpact = 'UPDATE ' + @tableNameImpact + ' SET list_item = ''Minor'', value = 1 WHERE item_id = ' +  CAST(@itemIdImpact AS nvarchar)
	EXEC (@sqlImpact)
	INSERT INTO items (instance, process) VALUES (@instance, @processListImpact);
	SET @itemIdImpact = @@identity;
	SET @sqlImpact = 'UPDATE ' + @tableNameImpact + ' SET list_item = ''Small'', value = 2 WHERE item_id = ' +  CAST(@itemIdImpact AS nvarchar)
	EXEC (@sqlImpact)
	INSERT INTO items (instance, process) VALUES (@instance, @processListImpact);
	SET @itemIdImpact = @@identity;
	SET @sqlImpact = 'UPDATE ' + @tableNameImpact + ' SET list_item = ''Medium'', value = 3 WHERE item_id = ' +  CAST(@itemIdImpact AS nvarchar)
	EXEC (@sqlImpact)
	INSERT INTO items (instance, process) VALUES (@instance, @processListImpact);
	SET @itemIdImpact = @@identity;
	SET @sqlImpact = 'UPDATE ' + @tableNameImpact + ' SET list_item = ''Substantial'', value = 4 WHERE item_id = ' +  CAST(@itemIdImpact AS nvarchar)
	EXEC (@sqlImpact)
	INSERT INTO items (instance, process) VALUES (@instance, @processListImpact);
	SET @itemIdImpact = @@identity;
	SET @sqlImpact = 'UPDATE ' + @tableNameImpact + ' SET list_item = ''Large'', value = 5 WHERE item_id = ' +  CAST(@itemIdImpact AS nvarchar)
	EXEC (@sqlImpact)

	--Create Portfolio columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'title', @c1, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into porfolio columns
	EXEC app_ensure_column @instance, @process, 'proximity', @c1, 6, 1, 1, 0, null,  0, 0, @processListProximity, null, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into portfolio columns
	EXEC app_ensure_column @instance, @process, 'impact', @c1, 6, 1, 1, 0, null,  0, 0, @processListImpact, null, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into portfolio columns
	EXEC app_ensure_column @instance, @process, 'issue_description', @c1, 4, 1, 0, 0, null, 0, 0, 0 --Put description long text into the container
	DECLARE @formula NVARCHAR(MAX) = 'getvalue("proximity","value") * getvalue("impact","value")'
	EXEC app_ensure_column @instance, @process, 'issue_value', @c1, 14, 1, 1, 0, null,  0, 0, @formula, null, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into portfolio columns

	--change feature columns to not to be system columns so they can be configured
	UPDATE item_columns SET system_column = 0 WHERE process = @process AND instance = @instance
	--change feature columns to be status columns so every information is loaded to portfolio regardless of visible columns
	UPDATE item_columns set status_column = 1 where instance = @instance and process = @process
END;
