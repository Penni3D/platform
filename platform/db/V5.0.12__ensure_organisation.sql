-- Stored Procedure
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_organisation'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_organisation] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROCEDURE [dbo].[app_ensure_process_organisation]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'organisation'
	DECLARE @itemId INT
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;
	DECLARE @i1 INT

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'Organisation', 0, 1, @container_id = @c1 OUTPUT

	--Create fields
	EXEC app_ensure_column @instance, @process, 'name', @c1, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT
	EXEC app_ensure_column @instance, @process, 'type', 0, 1, 1, 1, 0, null,  0, 0



	--Create portfolio

	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'Organisation', @portfolio_id = @p1 OUTPUT

	SET @sql = 'UPDATE process_portfolios SET process_portfolio_type = ''1'' WHERE process_portfolio_id = ' +  CAST(@p1 AS nvarchar) 
	EXEC (@sql)


	--Create portfolio columns
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter) VALUES (@p1, @i1, 'V', 1, 1);

	--Create condition
	DECLARE @con1 INT
	EXEC app_ensure_condition @instance, @process, 0, 0, 'all', @condition_id = @con1 OUTPUT
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'read');
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write');
	INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio');
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con1, @p1);
	INSERT INTO condition_containers (condition_id, process_container_id) VALUES (@con1, @c1);

	SET @tableName = '_' + @instance + '_' + @process
	INSERT INTO items (instance, process) VALUES (@instance, @process);
	SET @itemId = @@identity;
	SET @sql = 'UPDATE ' + @tableName + ' SET name = ''Default organisation'', type = ''0'' WHERE item_id = ' +  CAST(@itemId AS nvarchar) 
	EXEC (@sql)

	--User process organisation fields	

	DECLARE @g2 INT
	SELECT @g2 = process_group_id FROM process_groups WHERE instance = @instance AND process = 'user' AND variable = 'USER';

	DECLARE @c4 INT
	SELECT @c4 = ISNULL(process_container_id, 0) FROM process_containers WHERE instance = @instance AND process = 'user' AND variable = 'organisation_management';

	IF (@c4 IS NULL)
	BEGIN
		--Create new User process tab
		DECLARE @t2 INT
		EXEC app_ensure_tab @instance,'user','USER_TAB2', @g2, @tab_id = @t2 OUTPUT

		--Create new User process container	
		EXEC app_ensure_container @instance,'user','organisation_management', @t2, 1, @container_id = @c4 OUTPUT

	END

	--Create organisation field	
	DECLARE @i2 INT
	EXEC app_ensure_column @instance, 'user', 'organisation', @c4, 5, 1, 1, 0, null,  0, 0, 'organisation', @item_column_id = @i2 OUTPUT
	UPDATE process_container_columns SET validation = '{"max":"1"}' WHERE item_column_id = @i2

END;


GO

