ALTER TABLE process_portfolios
ADD portfolio_default_view INT;

ALTER TABLE instance_user_configurations_saved
ADD portfolio_default_view INT;
