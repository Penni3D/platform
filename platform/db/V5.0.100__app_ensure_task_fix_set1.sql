SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_task]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'task'
	DECLARE @link_process NVARCHAR(50) = 'task_link_type'
	DECLARE @tableName NVARCHAR(50) =  '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128)
	SET @BinVar = CONVERT(varbinary(128), 0)
	SET CONTEXT_INFO @BinVar;

	--Create task process
	EXEC app_ensure_process @instance, @process, @process, 1
	
	-- CREATE link process and columns
	EXEC app_ensure_process @instance, @link_process, @link_process, 1
	DECLARE @li1 INT
	EXEC app_ensure_column @instance, @link_process, 'link_type', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @li1 OUTPUT
	UPDATE item_columns SET status_column = 1 WHERE item_column_id = @li1
	
	
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance, @process,'TASK', @portfolio_id = @p1 OUTPUT

	--Create phases
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'BACKLOG', @group_id = @g1 OUTPUT

	DECLARE @g2 INT
	EXEC app_ensure_group @instance,@process,'SPRINT', @group_id = @g2 OUTPUT

	DECLARE @g3 INT
	EXEC app_ensure_group @instance,@process,'PHASE_TASK', @group_id = @g3 OUTPUT

	DECLARE @g4 INT
	EXEC app_ensure_group @instance,@process,'KANBAN', @group_id = @g4 OUTPUT

	DECLARE @g5 INT
	EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g5 OUTPUT

	DECLARE @g6 INT
	EXEC app_ensure_group @instance,@process,'SPRINT_TASK', @group_id = @g6 OUTPUT

	DECLARE @g7 INT
	EXEC app_ensure_group @instance,@process,'PHASE_TASK_SIGN', @group_id = @g7 OUTPUT
	
	DECLARE @g8 INT
	EXEC app_ensure_group @instance,@process,'PHASE_TASK_UNSIGN', @group_id = @g8 OUTPUT
	
	
	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'BACKLOG_BASIC_INFO', @g1, @tab_id = @t1 OUTPUT

	DECLARE @t2 INT
	EXEC app_ensure_tab @instance,@process,'SPRINT_BASIC_INFO', @g2, @tab_id = @t2 OUTPUT

	DECLARE @t3 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_TASK_BASIC_INFO', @g3, @tab_id = @t3 OUTPUT

	DECLARE @t4 INT
	EXEC app_ensure_tab @instance,@process,'KANBAN_BASIC_INFO', @g4, @tab_id = @t4 OUTPUT

	DECLARE @t5 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_BASIC_INFO', @g5, @tab_id = @t5 OUTPUT

	DECLARE @t6 INT
	EXEC app_ensure_tab @instance,@process,'SPRINT_TASK_BASIC_INFO', @g6, @tab_id = @t6 OUTPUT
	
	DECLARE @t7 INT
	EXEC app_ensure_tab @instance,@process,'SPRINT_COMPLETE', @g2, @tab_id = @t7 OUTPUT

	DECLARE @t8 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_TASK_SIGN', @g7, @tab_id = @t8 OUTPUT

	DECLARE @t9 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_TASK_UNSIGN', @g8, @tab_id = @t9 OUTPUT


	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'BACKLOG_BASIC_INFO', @t1, 1, @container_id = @c1 OUTPUT

	DECLARE @c2 INT
	EXEC app_ensure_container @instance,@process,'SPRINT_BASIC_INFO', @t2, 1, @container_id = @c2 OUTPUT

	DECLARE @c3 INT
	EXEC app_ensure_container @instance,@process,'PHASE_TASK_BASIC_INFO', @t3, 1, @container_id = @c3 OUTPUT

	DECLARE @c4 INT
	EXEC app_ensure_container @instance,@process,'KANBAN_BASIC_INFO', @t4, 1, @container_id = @c4 OUTPUT

	DECLARE @c5 INT
	EXEC app_ensure_container @instance,@process,'PHASE_BASIC_INFO', @t5, 1, @container_id = @c5 OUTPUT

	DECLARE @c6 INT
	EXEC app_ensure_container @instance,@process,'SPRINT_TASK_BASIC_INFO', @t6, 1, @container_id = @c6 OUTPUT

	DECLARE @c7 INT
	EXEC app_ensure_container @instance,@process,'DESCRIPTION', 0, 1, @container_id = @c7 OUTPUT

	DECLARE @c8 INT
	EXEC app_ensure_container @instance,@process,'DESCRIPTION_WO_ATTACHMENT', 0, 1, @container_id = @c8 OUTPUT

	DECLARE @c9 INT
	EXEC app_ensure_container @instance,@process,'SIGN', @t8, 1, @container_id = @c9 OUTPUT

	DECLARE @c10 INT
	EXEC app_ensure_container @instance,@process,'UNSIGN', @t9, 1, @container_id = @c10 OUTPUT

	DECLARE @c11 INT
	EXEC app_ensure_container @instance,@process,'COMPLETE_SPRINT', @t7, 1, @container_id = @c11 OUTPUT

	DECLARE @c12 INT
	EXEC app_ensure_container @instance,@process,'SPRINT_TASK_BASIC_INFO', @t6, 1, @container_id = @c12 OUTPUT


	--Create columns
	EXEC app_ensure_column @instance, @process, 'status_id', 0, 1, 1, 1, 0, null,  0, 0
	
  	DECLARE @i19 INT
	EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i19 OUTPUT
	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 0, 1, 1, 0, null,  0, 0
	
	
	
	DECLARE @i20 INT
	EXEC app_ensure_column @instance, @process, 'linked_tasks', 0, 6, 1, 1, 0, null,  0, 0, 'task', @item_column_id = @i20 OUTPUT
	UPDATE item_columns SET link_process = @link_process WHERE item_column_id = @i20
	
 	DECLARE @i17 INT
	EXEC app_ensure_column @instance, @process, 'owner_item_type', 0, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i17 OUTPUT

	DECLARE @i6 INT
	EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 1, 0, null, 0, 0, @item_column_id = @i6 OUTPUT

	DECLARE @i7 INT
	EXEC app_ensure_column @instance, @process, 'description', 0, 4, 1, 1, 0, null, 0, 0, @item_column_id = @i7 OUTPUT

	EXEC app_ensure_column @instance, @process, 'color', 0, 0, 1, 1, 0, null,  0, 0

  	DECLARE @i18 INT
	EXEC app_ensure_column @instance, @process, 'task_type', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i18 OUTPUT

	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'effort', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT

	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'start_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i2 OUTPUT

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'end_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i3 OUTPUT

	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'description', 0, 4, 1, 1, 0, null,  0, 0, @item_column_id = @i4 OUTPUT

	DECLARE @i5 INT
	EXEC app_ensure_column @instance, @process, 'status', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i5 OUTPUT

	DECLARE @i8 INT
	EXEC app_ensure_column @instance, @process, 'actual_effort', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i8 OUTPUT

	DECLARE @i9 INT
	EXEC app_ensure_column @instance, @process, 'responsibles', 0, 5, 0, 1, 0, null,  0, 0, 'user', @item_column_id = @i9 OUTPUT


	DECLARE @i10 INT
	EXEC app_ensure_column @instance, @process, 'actual_start_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i10 OUTPUT

	DECLARE @i11 INT
	EXEC app_ensure_column @instance, @process, 'actual_end_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i11 OUTPUT

	DECLARE @i12 INT
	EXEC app_ensure_column @instance, @process, 'result', 0, 4, 1, 1, 0, null,  0, 0, @item_column_id = @i12 OUTPUT

	DECLARE @i13 INT
	EXEC app_ensure_column @instance, @process, 'attachments', 0, 10, 1, 1, 0, null,  0, 0, @item_column_id = @i13 OUTPUT

	DECLARE @i14 INT
	EXEC app_ensure_column @instance, @process, 'complete_button', 0, 8, 0, 1, 0, null,  0, 0, null, @item_column_id = @i14 OUTPUT

	DECLARE @i15 INT
	EXEC app_ensure_column @instance, @process, 'sign_button', 0, 8, 0, 1, 0, null,  0, 0, null, @item_column_id = @i15 OUTPUT

	DECLARE @i16 INT
	EXEC app_ensure_column @instance, @process, 'unsign_button', 0, 8, 0, 1, 0, null,  0, 0, null, @item_column_id = @i16 OUTPUT

	UPDATE item_columns SET data_additional2 = 'close;close' WHERE item_column_id = @i14
	UPDATE item_columns SET data_additional2 = 'close;close' WHERE item_column_id = @i15
	UPDATE item_columns SET data_additional2 = 'close;close' WHERE item_column_id = @i16

	-- Assign tabs to phases
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i6, 1, 'V')
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i9, 2, 'W')
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i2, 3, 'X')
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i3, 4, 'Y')


	
	-- Assign containers to tabs

	-- BACKLOG
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t1, @c7, 2, 'W')

	-- SPRINT
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t2, @c7, 2, 'W')

	-- GANTT
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t3, @c7, 2, 'W')


	-- KANBAN
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t4, @c7, 2, 'W')

	-- PHASE
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t5, @c8, 2, 'W')

	-- SPRINT TASK
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t6, @c7, 2, 'W')

		
	-- Assign fields to containers

	-- BACKLOG
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c1, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c1, @i1, null, null, 1)


	-- SPRINT
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i2, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"start"}', 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i3, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"end"}', 1)
   		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i9, null, null, 1)


	-- PHASE TASK
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i6, 'name', NULL , 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i2, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"start"}', 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i3, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"end"}', 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i1, null, null, 1)
  		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i9, null, null, 1)

	-- SPRINT TASK
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c12, @i6, 'name', NULL , 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c12, @i1, null, null, 1)
		
	-- KANBAN
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i1, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i8, null, null, 1)
	 	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i9, null, null, 1)


	-- PHASE
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c5, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c5, @i2, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"start"}', 0, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c5, @i3, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"end"}', 0, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c5, @i1, null, null, 0, 1)


	-- DESCRIPTION
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c7, @i7, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c7, @i13, null, null, 1)

	-- DESCRIPTION w/o ATTACHMENT
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c8, @i7, null, null, 1)


	-- SPRINT COMPLETE
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c11, @i12, null, null, 1)

	-- SIGN
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i10, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"start"}', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i11, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"end"}', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i8, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i12, null, null, 1)

	-- UNSIGN
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i10, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"start"}', null, 1, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i11, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"end"}', null, 1, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i8, null, null, 1, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i12, null, null, 1, 1)


	-- CREATE PROCESS ACTION

	-- COMPLETE
		DECLARE @pa1 INT
		EXEC app_ensure_action @instance, @process, 'ACTION_COMPLETE', @process_action_id = @pa1 OUTPUT
		EXEC app_ensure_action_row @pa1, 0, @i5, 0, 1
		UPDATE item_columns SET process_action_id = @pa1 WHERE item_column_id = CAST(@i14 AS nvarchar) OR item_column_id = CAST(@i15 AS nvarchar)


	-- UNCOMPLETE
		DECLARE @pa2 INT
		EXEC app_ensure_action @instance, @process, 'ACTION_UNCOMPLETE', @process_action_id = @pa2 OUTPUT
		EXEC app_ensure_action_row @pa2, 0, @i5, 0, 0
		UPDATE item_columns SET process_action_id = @pa2 WHERE item_column_id = CAST(@i16 AS nvarchar)


	  -- PROCESS ACTION DIALOGS
	  INSERT INTO process_actions_dialog(instance, process, name, type, dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable) VALUES (@instance, @process, 'SPRINT_COMPLETE', 2, 'ACIONDG_COMPLETE_SPRINT_TITLE', 'ACIONDG_COMPLETE_SPRINT_TEXT_', 'ACIONDG_COMPLETE_SPRINT_CANCEL', 'ACIONDG_COMPLETE_SPRINT_CONFIRM')
	  DECLARE @pad1 INT = @@identity
	
	
	  INSERT INTO process_actions_dialog(instance, process, name, type, dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable) VALUES (@instance, @process, 'GANTT_SIGN', 2, 'ACIONDG_GANTT_SIGN_TITLE', 'ACIONDG_GANTT_SIGN_TEXT_', 'ACIONDG_GANTT_SIGN_CANCEL', 'ACIONDG_GANTT_SIGN_CONFIRM')
	  DECLARE @pad2 INT = @@identity
	
	
	  INSERT INTO process_actions_dialog(instance, process, name, type, dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable) VALUES (@instance, @process, 'GANTT_UNSIGN', 2, 'ACIONDG_GANTT_UNSIGN_TITLE', 'ACIONDG_GANTT_UNSIGN_TEXT_', 'ACIONDG_GANTT_UNSIGN_CANCEL', 'ACIONDG_GANTT_UNSIGN_CONFIRM')
	  DECLARE @pad3 INT = @@identity


	--CONDITIONS

	--Create conditions
  	DECLARE @con13 INT
	EXEC app_ensure_condition @instance, @process, 0, 0, 'Portfolio', @condition_id = @con13 OUTPUT
    DELETE FROM condition_features WHERE condition_id = @con13 -- Remove 'Meta' feature. 
  	INSERT INTO condition_features (condition_id, feature) VALUES (@con13, 'portfolio')
  	INSERT INTO condition_states (condition_id, state) VALUES (@con13, 'read')
  	INSERT INTO condition_states (condition_id, state) VALUES (@con13, 'write')
  	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con13, @p1)

	-- BACKLOG
	-- WRITE
  DECLARE @con1 INT
  DECLARE @consql NVARCHAR(MAX)
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"backlog"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_BACKLOG_WRITE', @consql, @condition_id = @con1 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con1, @g1)
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con1, @t1)

  
	-- READ
  DECLARE @con2 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"backlog"},"OperatorId":1}]},"OperatorId":1}]'

  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_BACKLOG_READ', @consql, @condition_id = @con2 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con2, 'read')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con2, @g1)
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con2, @t1)


	-- SPRINT
	-- WRITE
  DECLARE @con3 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_WRITE', @consql, @condition_id = @con3 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con3, 'write')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con3, @g2)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con3, @t2)

	-- READ
  DECLARE @con4 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_READ', @consql, @condition_id = @con4 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con4, 'read')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con4, @g2)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con4, @t2)


	-- PHASE TASK UNSIGN
	-- WRITE
  DECLARE @con5 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_UNSIGN_WRITE', @consql, @condition_id = @con5 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con5, 'write')
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con5, @t9)
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con5, @g8)

	-- READ
  --DECLARE @con6 INT
  --SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
  --EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_GANTT_UNSIGN_READ', @consql, @condition_id = @con6 OUTPUT
  --INSERT INTO condition_states (condition_id, state) VALUES (@con6, 'read')
  --INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con6, @t9)



	-- PHASE TASK SIGN
	-- WRITE
  DECLARE @con7 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_SIGN_WRITE', @consql, @condition_id = @con7 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con7, 'write')
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con7, @t8)
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con7, @g7)

	-- READ
  --DECLARE @con8 INT
  --SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
  --EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_GANTT_SIGN_READ', @consql, @condition_id = @con8 OUTPUT
  --INSERT INTO condition_states (condition_id, state) VALUES (@con8, 'read')
  --INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con8, @t8)


  -- KANBAN
	-- WRITE
  DECLARE @con9 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_KANBAN_WRITE', @consql, @condition_id = @con9 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con9, 'write')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con9, @g4)
INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con9, @t4)

	-- READ
  --DECLARE @con10 INT
  --SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
  --EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_KANBAN_READ', @consql, @condition_id = @con10 OUTPUT
  --INSERT INTO condition_states (condition_id, state) VALUES (@con10, 'read')
  --INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con10, @g4)


	-- PHASE
	-- WRITE
	  DECLARE @con11 INT
	  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
	  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_WRITE', @consql, @condition_id = @con11 OUTPUT
	  INSERT INTO condition_states (condition_id, state) VALUES (@con11, 'write')
	  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con11, @g5)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con11, @t5)
	
	-- READ
  --DECLARE @con12 INT
  --SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
  --EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_READ', @consql, @condition_id = @con12 OUTPUT
  --INSERT INTO condition_states (condition_id, state) VALUES (@con12, 'read')
  --INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con12, @g5)
  --INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con12, @t5)


	-- SPRINT TASK BASIC INFO
	--WRITE
	DECLARE @con14 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_TASK_BASIC_INFO_WRITE', @consql, @condition_id = @con14 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con14, 'write')
  	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con14, @g6)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con14, @t6)

	--READ
	DECLARE @con15 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_TASK_BASIC_INFO_READ', @consql, @condition_id = @con15 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con15, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con15, @g6)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con15, @t6)


	-- PHASE TASK BASIC INFO
	--WRITE
	DECLARE @con16 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_BASIC_INFO_WRITE', @consql, @condition_id = @con16 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con16, 'write')
  	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con16, @g3)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con16, @t3)

	--READ
	DECLARE @con17 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_BASIC_INFO_READ', @consql, @condition_id = @con17 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con17, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con17, @g3)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con17, @t3)


	UPDATE item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process


	  -- INSERT DIALOG TO BUTTON
	UPDATE item_columns SET process_dialog_id = @pad1, process_action_id = @pa1, status_column = 0 WHERE instance = @instance AND process = @process AND item_column_id = @i14
	UPDATE item_columns SET process_dialog_id = @pad2, process_action_id = @pa1, status_column = 0 WHERE instance = @instance AND process = @process AND item_column_id = @i15
	UPDATE item_columns SET process_dialog_id = @pad3, process_action_id = @pa2, status_column = 0 WHERE instance = @instance AND process = @process AND item_column_id = @i16


	-- ASSIGN BUTTONS TO PHASES
	INSERT INTO process_tab_columns (process_tab_id, item_column_id, order_no) VALUES (@t7, @i14, 'V')
	INSERT INTO process_tab_columns (process_tab_id, item_column_id, order_no) VALUES (@t8, @i15, 'V')
	INSERT INTO process_tab_columns (process_tab_id, item_column_id, order_no) VALUES (@t9, @i16, 'V')

	DECLARE @sql nvarchar(MAX)

	SET @sql = 'CREATE TRIGGER ' + @tableName + '_AFTER_UPDATE_OWNER 
				ON ' + @tableName + '
				AFTER UPDATE
				AS

				DECLARE @user_id int;
				SELECT @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()))	

				DECLARE @owner_item_id int, @owner_item_type nvarchar(255)
				SELECT @owner_item_id = owner_item_id, @owner_item_type = owner_item_type FROM inserted;
	
				BEGIN
					WITH rec AS (
						SELECT t.item_id,(SELECT owner_item_id FROM _' + @instance + '_task t2 WHERE t.parent_item_id = t2.item_id) AS owner_item_id, 
							(SELECT owner_item_type FROM _' + @instance + '_task t2 WHERE t.parent_item_id = t2.item_id) AS owner_item_type
						FROM [dbo].[_' + @instance + '_task] t
						WHERE t.parent_item_id IN (SELECT item_id FROM inserted)
						UNION ALL
							SELECT t.item_id, r.owner_item_id AS owner_item_id, r.owner_item_type
							FROM [dbo].[_' + @instance + '_task] t
							INNER JOIN rec r ON t.parent_item_id = r.item_id
						)
					UPDATE st
					SET st.owner_item_id = rec.owner_item_id, st.owner_item_type = rec.owner_item_type
					FROM _' + @instance + '_task st
					INNER JOIN rec
					ON rec.item_id = st.item_id WHERE rec.item_id != (SELECT item_id FROM inserted)
				END'

	EXEC (@sql)

	DECLARE @p2 INT
	DECLARE @con18 INT

	EXEC app_ensure_portfolio @instance, @process,'TASK_MY_TASKS', @portfolio_id = @p2 OUTPUT
	INSERT INTO process_portfolio_columns(process_portfolio_id, item_column_id) (SELECT @p2, item_column_id FROM item_columns WHERE instance = @instance AND process = @process and name <> 'bar_icons')

	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":3,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@i9 AS NVARCHAR) +' ,"Value":"","UserColumnTypeId":2},"OperatorId":null}]},"OperatorId":1},{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":'+ CAST(@i18 AS NVARCHAR) +  ',"Value":"21"},"OperatorId":null},{"ConditionTypeId":4,"Condition":{"ComparisonTypeId":6,"ParentItemColumnId":' + CAST(@i19 AS NVARCHAR) + ',"ItemColumnId":' + CAST(@i18 AS NVARCHAR) + ',"Value":"21"},"OperatorId":2}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_MY_TASKS', @consql, @condition_id = @con18 OUTPUT

	INSERT INTO condition_states (condition_id, state) VALUES (@con18, 'write'), (@con18, 'read')
	INSERT INTO condition_features (condition_id, feature) VALUES (@con18, 'portfolio')
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con18, @p2)

	EXEC app_ensure_configuration @instance, null, null, 'Tasks', 'MyTasksPortfolioId', @p2, 0, null
END
GO