
DECLARE @sql NVARCHAR(MAX);
DECLARE @instance NVARCHAR(MAX);
DECLARE @process NVARCHAR(MAX);
DECLARE @GetInstances CURSOR
DECLARE @GetMatrixProcessses CURSOR

--Get Tables
SET @GetInstances = CURSOR FAST_FORWARD LOCAL FOR 
select instance from instances
EXEC app_set_archive_user_id 0
OPEN @GetInstances
FETCH NEXT FROM @GetInstances INTO @instance
WHILE (@@FETCH_STATUS=0) BEGIN

	SET @GetMatrixProcessses = CURSOR FAST_FORWARD LOCAL FOR 
	select process from processes WHERE instance = @instance AND process_type = 4 
	OPEN @GetMatrixProcessses
	FETCH NEXT FROM @GetMatrixProcessses INTO @process
	
	WHILE (@@FETCH_STATUS=0) BEGIN
	
		
	    EXEC app_ensure_column @instance, @process, 'weighting', 0, 1, 1, 1, 0, null, 0, 0
		update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 	
		SET @sql = ' UPDATE _'  + @instance + '_' + @process + ' SET weighting = 1 WHERE weighting IS NULL AND type = 1'
		EXEC sp_executesql @sql 
			FETCH NEXT FROM @GetMatrixProcessses INTO @process
	END
	CLOSE @GetMatrixProcessses

	FETCH NEXT FROM @GetInstances INTO @instance
END
CLOSE @GetInstances
