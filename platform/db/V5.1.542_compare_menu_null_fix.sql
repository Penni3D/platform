SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[menu_IdReplace]
(
	@params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @params IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('MenuId"', 1, 9),
				('AttachmentId"', 1, 10),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
		
			SET @startPos = 1
		  	WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @params, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 1
									
							IF CHARINDEX('[', @params, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
				
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
								END

							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							SET @var3 = REPLACE(@var3, '"', '')

							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @guid UNIQUEIDENTIFIER
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
								
											DECLARE @start INT
											SET @start = 1
								  			WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													DECLARE @gu_id NVARCHAR(MAX)
													IF ISNUMERIC(@listVal) = 0
														BEGIN
															SET @gu_id = '0' 
														END
													ELSE
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = '0'
																END
														END
													SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

													SET @start = @start + LEN(@gu_id) + 1

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
													
											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
									
							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @colId NVARCHAR(MAX)
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colId

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF ISNUMERIC(@colId) = 1
														BEGIN
															SET @colVar = '0'

															IF @subType = 0
																BEGIN
																	SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colVar = dbo.processPortfolio_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colVar = dbo.condition_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colVar = dbo.processAction_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colVar = dbo.processActionDialog_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colVar = dbo.processTab_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colVar = dbo.processContainer_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colVar = dbo.processDashboard_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colVar = dbo.processDashboardChart_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 9
																BEGIN
																	SET @colVar = dbo.menu_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 10
																BEGIN
																	SET @colVar = CAST(dbo.attachment_GetGuid(CAST(@colId AS INT)) AS NVARCHAR(MAX))
																END

															IF @colVar IS NULL
																BEGIN
																	SET @colVar = '0' 
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
															SET @start2 = @start2 + LEN(@colVar) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colId
												END
											CLOSE @delimited2

											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 2
								BEGIN
									IF LEFT(LTRIM(@var3), 5) = '"tab_'
										BEGIN
											SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END

							IF @type = 3
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SELECT @var4 = REPLACE(calendar_name, ' ', '_') FROM calendars WHERE calendar_id = @var3
													IF @var4 IS NOT NULL
														BEGIN
															SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
								END
										
							SET @startPos = CHARINDEX(@strToFound, @params, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @params
END



