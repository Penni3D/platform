﻿ALTER PROCEDURE [dbo].[app_ensure_process_budget_item]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'budget_item'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_process @instance, @process, @process, 1

    --Create columns
    EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 5, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'type_item_id', 0, 6, 1, 1, 0, null,  0, 0, 'budget_item_type'
    EXEC app_ensure_column @instance, @process, 'date', 0, 3, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'amount', 0, 2, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'project_item_id', 0, 1, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'category_item_id', 0, 6, 1, 1, 0, null,  0, 0

    -- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_item_id', 0, 6, 1, 1, 0, null,  0, 0, 'budget_currency'
    -- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_rate', 0, 2, 1, 1, 0, null,  0, 0
    -- add later when necessary: EXEC app_ensure_column @instance, @process, 'description', 0, 4, 1, 1, 0, null,  0, 0
  END

GO

IF EXISTS (SELECT * FROM item_columns WHERE process='budget_item' AND (name='type_item_id' OR name='category_item_id') AND data_type=5)
  UPDATE item_columns SET data_type=6 WHERE process='budget_item' AND (name='type_item_id' OR name='category_item_id') AND data_type=5
GO