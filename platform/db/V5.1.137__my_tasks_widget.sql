DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
	BEGIN
		DECLARE @parentColumnId INT;
		DECLARE @typeColumnId INT;
		DECLARE @userColumnId INT;
		DECLARE @sql NVARCHAR(MAX);
		DECLARE @process NVARCHAR(50) = 'task';
		DECLARE @portfolioId INT;
		DECLARE @conditionId INT;


		SELECT @parentColumnId = item_column_id FROM item_columns WHERE process = @process AND name = 'parent_item_id';
		SELECT @typeColumnId = item_column_id FROM item_columns WHERE process = @process AND name = 'task_type';
		SELECT @userColumnId = item_column_id FROM item_columns WHERE process = @process AND name = 'responsibles';

		EXEC app_ensure_portfolio @instance, @process,'MY_TASKS_WIDGET', @portfolio_id = @portfolioId OUTPUT
		INSERT INTO process_portfolio_columns(process_portfolio_id, item_column_id)
		(SELECT @portfolioId, item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name IN ('title', 'owner_item_id', 'owner', 'owner_item_type'))

		SET @sql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":4,"Condition":{"ComparisonTypeId":6,"ParentItemColumnId":' + CAST(@parentColumnId AS NVARCHAR) + ',"ItemColumnId":' + CAST(@typeColumnId AS NVARCHAR) + ',"Value":"21"},"OperatorId":null},{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@typeColumnId AS NVARCHAR) + ',"Value":"1"},"OperatorId":1},{"ConditionTypeId":3,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@userColumnId AS NVARCHAR) + ',"Value":"","UserColumnTypeId":2},"OperatorId":1}]},"OperatorId":1},{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@typeColumnId AS NVARCHAR) + ',"Value":"21"},"OperatorId":null},{"ConditionTypeId":3,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@userColumnId AS NVARCHAR) + ',"Value":"","UserColumnTypeId":2},"OperatorId":1}]},"OperatorId":2}]'

		EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_MY_TASKS_WIDGET', @sql, @condition_id = @conditionId OUTPUT
		DELETE FROM condition_features WHERE condition_id = @conditionId

		INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write'), (@conditionId, 'read')
		INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'portfolio')
		INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, @portfolioId)

		FETCH  NEXT FROM instance_cursor INTO @instance
	END
	