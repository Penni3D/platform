﻿ALTER PROCEDURE [dbo].[app_ensure_archive]
	@tableName NVARCHAR(200)
AS
BEGIN
	-- Declaring variables	
	DECLARE @colNames NVARCHAR(MAX);
	DECLARE @colNamesInsert NVARCHAR(MAX);
	DECLARE @colTypes NVARCHAR(MAX);
	DECLARE @colDefs NVARCHAR(MAX);
	DECLARE @pkCols NVARCHAR(MAX);
	DECLARE @pkCols2 NVARCHAR(MAX);

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @viewIndexSql nvarchar(MAX);

	-- Setting action variable
	DECLARE @action nvarchar(20)
	SET @action ='CREATE'
	
	-- Setting table names
	DECLARE @nameArchive NVARCHAR(200);
	DECLARE @nameExpired NVARCHAR(200);
	DECLARE @nameHistoryFunction NVARCHAR(100);

	SET @nameArchive = 'archive_'+@tableName;
	SET @nameExpired = 'old_'+@tableName;
	SET @nameHistoryFunction = 'get_'+@tableName;
	
	-- Setting column names
	DECLARE @archiveId NVARCHAR(100);
	DECLARE @archiveUserId NVARCHAR(100);
	DECLARE @archiveStart NVARCHAR(100);
	DECLARE @archiveEnd NVARCHAR(100);
	DECLARE @archiveType NVARCHAR(100);
		

	SET @archiveId = 'archive_id';
	SET @archiveStart = 'archive_start';
	SET @archiveEnd = 'archive_end';
	SET @archiveUserId = 'archive_userid';
	SET @archiveType = 'archive_type';
		

	DECLARE @defaultValue NVARCHAR(255);
	SET @defaultValue = null;

	DECLARE @archiveColsCount int;
	
	-- Getting list of columns in original table, 
	--	colNames is list of cols, 
	--	colTypes is list of names with datatypes
	--	colDefs is list of colnames encapsulated with isnull and column default value for insert	

		--Drop old archive TRIGGERS
		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
			SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_UPDATE]'
			EXEC (@sql)
		END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_INSERT]')) BEGIN			
			SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_INSERT]'
			EXEC (@sql)
		END
		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_Delete]')) BEGIN			
			SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_Delete]'
			EXEC (@sql)
		END

  IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameArchive+']')) BEGIN
  
    exec app_set_archive_user_id 1
    SET @sql = 'update '+@TableName+' set archive_start = getutcdate() WHERE archive_start is null '
    EXEC (@sql)	
    SET @sql = 'update '+@TableName+' set archive_start = getutcdate(), archive_userid=1 WHERE archive_userid is null or archive_userid is null'
    EXEC (@sql)		  
  END

	IF COL_LENGTH(@tableName,@archiveUserId) IS NULL
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ADD '+@archiveUserId+' int  DEFAULT 0'
			EXEC (@sql)
		END
	ELSE
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN '+@archiveUserId+' int NOT NULL'
			EXEC (@sql)
			
			SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveUserId
			IF @defaultValue IS NULL
			BEGIN
				SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT 0 FOR '+@archiveUserId
				EXEC (@sql)
			END

			EXEC (@sql)
		END

	IF COL_LENGTH(@tableName,@archiveStart) IS NULL
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ADD  '+@archiveStart+' [datetime] DEFAULT getutcdate()'
			EXEC (@sql)
		END
	ELSE
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN  '+@archiveStart+' [datetime]'
			EXEC (@sql)

			SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveStart
			IF @defaultValue IS NULL
			BEGIN
				SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT getutcdate() FOR '+@archiveStart
				EXEC (@sql)
			END
	END

	SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = @nameArchive
	IF @archiveColsCount = 0 
	BEGIN
			SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
					@colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' + 
					data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
					( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' + 
					COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
				@colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
			FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE table_name = @tablename 

			-- Creating archive table
			SET @sql = '
				CREATE TABLE dbo.'+@nameArchive+'(
					['+@archiveId+'] int IDENTITY(1,1) NOT NULL,
					['+@archiveEnd+'] [datetime] NULL,
					['+@archiveType+'] tinyint NOT NULL,
					'+@colTypes+')  ON [PRIMARY]'
			 EXEC (@sql)

		END
	ELSE
		BEGIN
			SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)
			IF @archiveColsCount > 0 
				BEGIN		
					SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
							@colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' + 
							data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
							( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' + 
							COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
						@colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
					FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)
			
					SET @sql = 'ALTER TABLE dbo.'+@nameArchive+' ADD '+@colTypes
					EXEC (@sql)
				END

		SET @action ='ALTER'

		--[item_columns_AFTER_UPDATE]
		--[item_columns_AFTER_INSERT]
		--[item_columns_Delete]
	END

		SET @colNames = null
		SET @colTypes = null
		SET @colDefs = null

		DECLARE @pkColsCount int = 0

		SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
				@colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' + 
				data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
				( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' + 
				COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
			@colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = @tablename 

		SELECT @colNamesInsert = COALESCE(@colNamesInsert+',','')+'['+column_name+']'
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = @tablename AND (select columnproperty(object_id(@tablename),column_name,'IsIdentity')) = 0 AND column_name <> @archiveUserId

		SELECT @pkColsCount = count(column_name)
		from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
			INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
			where 	pk.TABLE_NAME = @tableName
			and	CONSTRAINT_TYPE = 'PRIMARY KEY'
			and	c.TABLE_NAME = pk.TABLE_NAME
			and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
			and pk.table_catalog = (SELECT db_name())		

		SELECT @pkCols = COALESCE(@pkCols+',','')+'['+column_name+']'
		from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
			INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
			where 	pk.TABLE_NAME = @tableName
			and	CONSTRAINT_TYPE = 'PRIMARY KEY'
			and	c.TABLE_NAME = pk.TABLE_NAME
			and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
			and pk.table_catalog = (SELECT db_name())

		IF @pkColsCount > 1
		BEGIN
			SELECT @pkCols2 = COALESCE(@pkCols2+'+','')+'CAST(['+column_name+'] as nvarchar) '
			from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
				INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
				where 	pk.TABLE_NAME = @tableName
				and	CONSTRAINT_TYPE = 'PRIMARY KEY'
				and	c.TABLE_NAME = pk.TABLE_NAME
				and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
				and pk.table_catalog = (SELECT db_name())
		END
		ELSE
		BEGIN
			SELECT @pkCols2 = COALESCE(@pkCols2+'+','')+'['+column_name+']'
			from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
				INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
				where 	pk.TABLE_NAME = @tableName
				and	CONSTRAINT_TYPE = 'PRIMARY KEY'
				and	c.TABLE_NAME = pk.TABLE_NAME
				and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
				and pk.table_catalog = (SELECT db_name())
		END

	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END

	SET @sql =  @action + ' TRIGGER [dbo].['+@tableName+'_INSTEAD_OF_INSERT]
	ON [dbo].['+@tableName+']
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

	INSERT INTO [dbo].['+@tableName+'] ('+@colNamesInsert+', '+@archiveUserId+') 
	SELECT '+@colNamesInsert+' 
	, @user_id
	FROM inserted
	'
	EXEC (@sql)


	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END

	-- Creating UPDATE triggers
	SET @sql = @action + ' TRIGGER [dbo].['+@tableName+'_AFTER_UPDATE]
	ON [dbo].['+@tableName+']
	AFTER UPDATE
	AS

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

    UPDATE [dbo].['+@tableName+']
    SET ' + @archiveStart + '= getutcdate(),
    ' + @archiveUserId + '=  @user_id
    WHERE ' + @pkCols2 + ' IN (SELECT DISTINCT ' + @pkCols2 + ' FROM Inserted)

	INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
	SELECT '+@colNames+',
		'+@archiveEnd+' = getutcdate(),
		'+@archiveType+' = 1
	FROM deleted	
	'
	EXEC (@sql)

	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_DELETE]')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END
		
	-- Creating Delete Trigger 
	SET @sql =  @action +' TRIGGER [dbo].['+@tableName+'_DELETE]
		ON [dbo].['+@tableName+']
		FOR DELETE
		AS		
			INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
			SELECT '+@colNames+',
				'+@archiveEnd+' = getutcdate(),
				'+@archiveType+' = 1
			FROM deleted
			
			DECLARE @user_id int;
			select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

			INSERT INTO '+@nameArchive+'('+REPLACE(@colNames,'['+@archiveUserId+'],','')+','+@archiveEnd+', '+@archiveType+', '+@archiveUserId+')
			SELECT '+REPLACE(@colNames,'['+@archiveUserId+'],','')+',
				'+@archiveEnd+' = getutcdate(),
				'+@archiveType+' = 2,
				'+@archiveUserId+' = @user_id
			FROM deleted
			'
	EXEC (@sql)


	-- Creating or altering history function, depends if it exist

	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameHistoryFunction+']')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END

	SET @sql = @action +' FUNCTION  [dbo].['+@nameHistoryFunction+']( @d datetime )
		RETURNS @rtab TABLE (archive_id INT, '+@colTypes+')
		AS
		BEGIN	
			INSERT @rtab
				SELECT archive_id, '+@colNames+'
				FROM '+@nameArchive+' WHERE '+@archiveId+' IN (
					SELECT MAX('+@archiveId+')
					FROM '+@nameArchive+'
					WHERE '+@archiveStart+' < @d AND archive_end > @d AND ' + @pkCols2 + ' NOT IN
					(SELECT ' + @pkCols2 + ' FROM  '+@tableName+'  WHERE '+@archiveStart+' < @d  )
					GROUP BY ' + @pkCols + '
					)
				UNION 
				SELECT 0, '+@colNames+' 
				FROM '+@tableName+' 
				WHERE '+@archiveStart+' < @d  
			RETURN
		END'	
	EXEC (@sql)	

	exec app_set_archive_user_id 1
	SET @sql = 'update '+@TableName+' set archive_start = getutcdate() WHERE archive_start is null '
	EXEC (@sql)	
	SET @sql = 'update '+@TableName+' set archive_start = getutcdate(), archive_userid=1 WHERE archive_userid is null or archive_userid is null'
	EXEC (@sql)	
END

GO

DECLARE @tableName nvarchar(max)
 DECLARE tableCursor CURSOR FOR select t2.table_name from information_schema.tables t1 
inner join information_schema.tables t2 on substring(t1.table_name, 9, 2000) = t2.table_name
 where t1.table_name like 'archive_%'
  and t2.table_name <> 'item_data_process_selections'
  and t2.table_name <> 'instances'
    and t2.table_name <> 'database_migrations'

 OPEN tableCursor
 
 FETCH NEXT FROM tableCursor INTO @tableName
 WHILE @@fetch_status = 0
   BEGIN
		exec app_ensure_archive @tableName
     FETCH  NEXT FROM tableCursor INTO @tableName
  END
 CLOSE tableCursor
 DEALLOCATE tableCursor


GO 

IF NOT Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[process_baseline]')) 
CREATE TABLE dbo.process_baseline (
	process_baseline_id int NOT NULL IDENTITY(1,1),
	instance nvarchar(10) NULL,
	process nvarchar(50) NULL,
	process_item_id int NULL,
	process_group_id int NULL,
	baseline_date datetime NOT NULL,
	creator_user_item_id int NULL,
	description nvarchar(50) NULL,
	archive_userid int NULL DEFAULT ((0)),
	archive_start datetime NULL DEFAULT (getutcdate())
);

ALTER TABLE dbo.process_baseline ADD CONSTRAINT PK__process___F8C8E2FACFC2460B PRIMARY KEY (process_baseline_id);

ALTER TABLE [dbo].[process_baseline] ADD CONSTRAINT [FK_process_baseline_items] FOREIGN KEY ([process_item_id]) REFERENCES [items] ([item_id]) ON DELETE NO ACTION ON UPDATE NO ACTION

ALTER TABLE [dbo].[process_baseline] ADD CONSTRAINT [FK_process_baseline_users] FOREIGN KEY ([creator_user_item_id]) REFERENCES [items] ([item_id]) ON DELETE SET NULL ON UPDATE SET NULL

ALTER TABLE [dbo].[process_baseline] ADD CONSTRAINT [FK_process_baseline_groups] FOREIGN KEY ([process_group_id]) REFERENCES [process_groups] ([process_group_id]) ON DELETE CASCADE ON UPDATE CASCADE 

END

DECLARE @sql nvarchar(max)
DECLARE histCursor CURSOR FOR 
select 'insert into process_baseline (instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id) 
SELECT ''' + pg.instance + ''', ''' + pg.process + ''', item_id, ' + cast(process_group_id as nvarchar(max)) +
 ', ' + name +  ', (SELECT TOP 1 item_id FROM _' + pg.instance + '_user ORDER BY item_id ASC) from _' + pg.instance + '_' + pg.process + ' WHERE '  + name +  ' is not null ' from process_groups pg inner join item_columns ic on pg.history_date_col = ic.item_column_id where history_date_col is not null

OPEN histCursor

FETCH NEXT FROM histCursor INTO @sql

WHILE @@fetch_status = 0
  BEGIN
    EXEC (@sql)
    FETCH  NEXT FROM histCursor INTO @sql
  END
CLOSE histCursor
DEALLOCATE histCursor

GO
DELETE FROM item_columns WHERE item_column_id IN (SELECT history_date_col FROM process_groups)

GO 
ALTER TABLE process_groups DROP COLUMN history_date_col


GO 

ALTER  TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
AS 
 set xact_abort off; 
	DECLARE @ids nvarchar(max) =(select ',' + CAST(item_id as nvarchar) FROM deleted FOR XML PATH(''))
	
	DECLARE @tName as nvarchar(max)
	DECLARE @cName as nvarchar(max)

	DECLARE @countSql as nvarchar (max) = ''

	IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
	CREATE table #DeleteItems (item_id int, ord int)
			
	DECLARE @GetTables CURSOR
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	select ccu.TABLE_NAME,  ccu.COLUMN_NAME from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
	inner join  INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
	where UNIQUE_CONSTRAINT_NAME = 'PK_items' AND substring(ccu.TABLE_NAME, 1, 1) = '_' 

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName

	WHILE (@@FETCH_STATUS=0) BEGIN	
		DECLARE @sql nvarchar(max) = ' WITH rec AS ( 
		 SELECT t.item_id, t.' + @cName + ', 0 as lvl
		 FROM ' + @tName + ' t 
		 WHERE t.' + @cName + ' IN (-1' + @ids + ')
		 UNION ALL 
		 SELECT t.item_id, t.' + @cName + ' , r.lvl + 1
		 FROM ' + @tName + ' t 
		 INNER JOIN rec r ON r.item_id = t.' + @cName + ') 
			INSERT INTO #DeleteItems (item_id, ord)  SELECT item_id, lvl FROM rec order by lvl desc  '
		EXECUTE (@sql)
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

    DELETE  FROM item_subitems 					WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data 						WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM process_tabs_subprocess_data 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE process_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE task_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM condition_user_groups WHERE  item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM process_baseline WHERE  process_item_id IN (SELECT item_id FROM #DeleteItems)

	DECLARE @delId as int
	DECLARE @GetIds CURSOR

	SET @GetIds = CURSOR FOR SELECT item_id FROM #DeleteItems order by ord desc

	OPEN @GetIds
	FETCH NEXT FROM @GetIds INTO @delId

	WHILE (@@FETCH_STATUS=0) BEGIN
			IF OBJECT_ID('tempdb..#DeleteItems2') IS NOT NULL DROP TABLE #DeleteItems2
				CREATE table #DeleteItems2(item_id int, ord int)
						
				
				SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
				select ccu.TABLE_NAME,  ccu.COLUMN_NAME from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
				inner join  INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
				where UNIQUE_CONSTRAINT_NAME = 'PK_items' AND substring(ccu.TABLE_NAME, 1, 1) = '_' 
			
				OPEN @GetTables
				FETCH NEXT FROM @GetTables INTO @tName, @cName
			
				WHILE (@@FETCH_STATUS=0) BEGIN	
					SET @sql  = ' WITH rec AS ( 
					 SELECT t.item_id, t.' + @cName + ', 0 as lvl
					 FROM ' + @tName + ' t 
					 WHERE t.' + @cName + ' IN (' + CAST(@delId as nvarchar) + ')
					 UNION ALL 
					 SELECT t.item_id, t.' + @cName + ' , r.lvl + 1
					 FROM ' + @tName + ' t 
					 INNER JOIN rec r ON r.item_id = t.' + @cName + ') 
						INSERT INTO #DeleteItems2 (item_id, ord)  SELECT item_id, lvl FROM rec order by lvl desc  '
					EXECUTE (@sql)
					FETCH NEXT FROM @GetTables INTO @tName, @cName
				END
				CLOSE @GetTables
			
			    DELETE  FROM item_subitems 					WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM item_data 						WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM process_tabs_subprocess_data 	WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM item_data_process_selections 	WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_parents 	WHERE process_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_parents 	WHERE task_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM condition_user_groups WHERE  item_id IN (SELECT item_id FROM #DeleteItems)
				DELETE  FROM process_baseline WHERE  process_item_id IN (SELECT item_id FROM #DeleteItems)

				DECLARE @delId2 as int
				DECLARE @GetIds2 CURSOR
			
				SET @GetIds2 = CURSOR FOR SELECT item_id FROM #DeleteItems2 order by ord desc
			
				OPEN @GetIds2
				FETCH NEXT FROM @GetIds2 INTO @delId2
			
				WHILE (@@FETCH_STATUS=0) BEGIN
					INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM items WHERE item_id = @delId2
					DELETE FROM items WHERE item_id = @delId2
					
				FETCH NEXT FROM @GetIds2 INTO @delId2
				END
				CLOSE @GetIds2
		INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM items WHERE item_id = @delId
		DELETE FROM items WHERE item_id = @delId
	FETCH NEXT FROM @GetIds INTO @delId
	END
	CLOSE @GetIds

    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data dd						INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.process_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.task_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id

	DELETE dd FROM instance_comments dd INNER JOIN deleted d ON dd.sender = d.item_id
	DELETE dd FROM process_comments dd INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_comments dd INNER JOIN deleted d ON dd.sender = d.item_id
	DELETE dd FROM instance_user_configurations_saved dd INNER JOIN deleted d ON dd.user_id = d.item_id
	DELETE dd FROM instance_user_configurations dd INNER JOIN deleted d ON dd.user_id = d.item_id
	
	DELETE dd FROM condition_user_groups dd INNER JOIN deleted d ON dd.item_id = d.item_id
	
	DELETE dd FROM process_baseline dd INNER JOIN deleted d ON dd.process_item_id = d.item_id

	--Insert into deleted items
	INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM deleted

	--Finally delete the item
	DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id