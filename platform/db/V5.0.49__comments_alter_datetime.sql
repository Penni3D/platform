 DECLARE @con nvarchar(128)
 SELECT @con = name
 FROM sys.default_constraints
 WHERE parent_object_id = object_id('process_comments')
 AND col_name(parent_object_id, parent_column_id) = 'date';
 IF @con IS NOT NULL
     EXECUTE('ALTER TABLE process_comments DROP CONSTRAINT ' + @con)

ALTER TABLE process_comments 
	ALTER COLUMN [date] DATETIME NOT NULL
ALTER TABLE process_comments 
	ADD DEFAULT GETDATE() FOR [date];


GO
 DECLARE @con nvarchar(128)
 SELECT @con = name
 FROM sys.default_constraints
 WHERE parent_object_id = object_id('instance_comments ')
 AND col_name(parent_object_id, parent_column_id) = 'date';
 IF @con IS NOT NULL
     EXECUTE('ALTER TABLE instance_comments DROP CONSTRAINT ' + @con)
ALTER TABLE instance_comments
	ALTER COLUMN [date] DATETIME NOT NULL
ALTER TABLE instance_comments
	ADD DEFAULT GETDATE() FOR [date];