﻿CREATE TABLE [dbo].[item_column_equations] (
  [item_column_equation_id] int IDENTITY(1,1) NOT NULL,
  [parent_item_column_id] int NOT NULL,
  [column_type] TINYINT NOT NULL DEFAULT(0),
  [item_column_id] INT NULL,
  [sql_alias] NVARCHAR(2) NULL,
  [sql_function] INT NOT NULL DEFAULT (0),
  [sql_param1] NVARCHAR(255) NULL,
  [sql_param2] NVARCHAR(255) NULL,
  [sql_param3] NVARCHAR(255) NULL
)
GO

ALTER TABLE [dbo].[item_column_equations] ADD CONSTRAINT [item_column_equations_parent_col] FOREIGN KEY ([parent_item_column_id]) REFERENCES [dbo].[item_columns] ([item_column_id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[item_column_equations] ADD CONSTRAINT [item_column_equations_item_col] FOREIGN KEY ([item_column_id]) REFERENCES [dbo].[item_columns] ([item_column_id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO