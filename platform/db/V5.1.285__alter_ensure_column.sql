ALTER PROCEDURE [dbo].[app_ensure_column]
	@instance NVARCHAR(10), @process NVARCHAR(50), @column_name NVARCHAR(50), @container_id INT, 
	@data_type TINYINT, @system_column TINYINT, @required INT, @inputMethod INT, @inputName NVARCHAR(255), @portfolio_id INT, @use_in_select_result TINYINT, @dataAdditional1 NVARCHAR(MAX) = null, @dataAdditional2 NVARCHAR(MAX) = null, @use_in_filter INT = 0, @item_column_id INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @item_column_id = 0
	SELECT @item_column_id = item_column_id FROM item_columns WHERE instance = @instance AND name = @column_name AND process = @process
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF (@item_column_id = 0)
	BEGIN
			DECLARE @langVar NVARCHAR (50) = 'IC_' + @process + '_' + @column_name

		INSERT INTO item_columns (instance, process, name, data_type, system_column, variable, input_method, input_name, data_additional, data_additional2) 
		values (@instance, @process, @column_name, @data_type, @system_column, UPPER(@langVar), @inputMethod, @inputName, @dataAdditional1, @dataAdditional2)
		SELECT @item_column_id = @@IDENTITY
		

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @column_name, 'en-GB')
		END

		IF (@container_id > 0) BEGIN
			INSERT INTO process_container_columns (process_container_id, item_column_id, required) values(@container_id, @item_column_id, @required)
		END
	END

	IF (@portfolio_id > 0)
	BEGIN
		IF (SELECT COUNT(item_column_id) FROM process_portfolio_columns WHERE process_portfolio_id = @portfolio_id AND item_column_id = @item_column_id) = 0 
		BEGIN
			INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter)
			VALUES(
				@portfolio_id,
				@item_column_id,
				ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM process_portfolio_columns WHERE process_portfolio_id = @portfolio_id
				ORDER BY order_no COLLATE Latin1_general_bin DESC),null)),'U'),
				@use_in_select_result,
				@use_in_filter
			)
		END
	END
END
GO 
DROP FUNCTION items_GetOrderEnd
GO
DROP TRIGGER items_afterUpdateRow
GO
ALTER PROCEDURE [dbo].[items_BuildTable]
	@Instance NVARCHAR(10),@Process NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ColId NVARCHAR(50)
	DECLARE @ColName NVARCHAR(50)
	DECLARE @DataType TINYINT
	DECLARE @D_item_id INT
	DECLARE @D_order_no NVARCHAR(MAX)
	DECLARE @D_item_column_id INT
	DECLARE @D_nvarchar NVARCHAR(255)
	DECLARE @D_int INT
	DECLARE @D_float FLOAT
	DECLARE @D_date DATE
	DECLARE @D_text NVARCHAR(MAX)
	DECLARE @ColNames NVARCHAR(MAX) = ''
	DECLARE @TableName NVARCHAR(200)
	DECLARE @GetColumns CURSOR
	DECLARE @GetData CURSOR
	DECLARE @CreateSql NVARCHAR(MAX) = ''
	DECLARE @FillSql NVARCHAR(MAX) = ''

	SET @TableName = '_' + @instance + '_' + @Process
	
	--Drop table if it exists // Trigger will actually drop the table
	DELETE FROM item_tables WHERE name = @TableName
	
	--Double Check
	IF dbo.TableExists(@TableName) = 1 BEGIN
		DECLARE @DropSql NVARCHAR(MAX) = 'DROP TABLE ' + @TableName
		EXEC sp_executesql @DropSql
	END

	--Get Columns
	SET @GetColumns = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT name,data_type FROM item_columns WHERE process = @Process ANd instance = @Instance

	OPEN @GetColumns
	FETCH NEXT FROM @GetColumns INTO @ColName,@DataType

	WHILE (@@FETCH_STATUS=0) BEGIN
		--Add Column name
		SET @CreateSql = @CreateSql + ',' + @ColName

		--Add data type
		IF @DataType = 0 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(255)'
		END ELSE IF @DataType = 1 BEGIN
			SET @CreateSql = @CreateSql + ' INT'
		END ELSE IF @DataType = 2 BEGIN
			SET @CreateSql = @CreateSql + ' FLOAT'
		END ELSE IF @DataType = 3 BEGIN
			SET @CreateSql = @CreateSql + ' DATETIME'
		END ELSE IF @DataType = 4 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(MAX)'
		END ELSE IF @DataType = 5 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(MAX)'
		END ELSE IF @DataType = 6 BEGIN
			SET @CreateSql = @CreateSql + ' INT'
		END

		FETCH NEXT FROM @GetColumns INTO @ColName,@DataType
	END
	CLOSE @GetColumns

	--Create Table
	SET @CreateSql = 'CREATE TABLE ' + @TableName + '(item_id INT PRIMARY KEY, order_no NVARCHAR(MAX)' + @CreateSql + ')'
	SET @ColNames = 'data_id,order_no' + @ColNames
	EXEC sp_executesql @CreateSql
	INSERT INTO item_tables (instance,process,name) VALUES (@Instance,@Process,@tablename)
	
	--Create Archive Table
	EXEC app_ensure_archive @TableName
END
GO
DROP FUNCTION PortfolioValue

GO
ALTER TRIGGER [dbo].[items_AfterInsertRow] ON [dbo].[items] AFTER INSERT AS
    DECLARE @sql nvarchar(max) =
        (
            SELECT ';INSERT INTO ' + it.name + ' (item_id, order_no) VALUES (' + CAST(i.item_id AS VARCHAR) + ',' +
                   '(SELECT CASE WHEN ' + CAST(it.ordering AS NVARCHAR) + '=1 THEN ISNULL(dbo.getOrderBetween((SELECT TOP 1 ISNULL(order_no,''U'') FROM ' + it.name + ' ORDER BY order_no COLLATE Latin1_general_bin DESC),null),''U'') ELSE ''U'' END)' + ')'
            FROM item_tables it
                     INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
                 FOR XML PATH('')
        )
		
EXEC sp_executesql @sql