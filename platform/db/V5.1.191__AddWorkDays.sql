SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Timo A.
-- Create date: 10.08.2018
-- Description:	Add x amount of workdays to a start date. The force base calendar route relies on default base calendar, so set that if this is to be used.
-- =============================================
CREATE FUNCTION GetWorkingDayAfter
(
	-- Add the parameters for the function here
	@force_base_calendar BIT, 
	@user_item_id INT, 
	@start DATE, 
	@adddays INT
)
RETURNS DATE
AS
BEGIN
	-- This will be the return value
	DECLARE @end DATE

	-- Loop while there are non-work days found
	WHILE @adddays > 0
	BEGIN

		--Generate a new end date based on the added dates. First round is the inserted value, next rounds are the non-week days found.
		SET @end = DATEADD(DAY, @adddays, @start)

		--Function has two modes, @force_base_calendar = 1 uses the base calendar. @force_base_calendar = 0 uses user's calendar.
		IF @force_base_calendar = 1 
		BEGIN
			SELECT @adddays = COUNT(*) FROM calendar_data WHERE calendar_id = dbo.GetDefaultBaseCalendar() AND event_date BETWEEN DATEADD(DAY,1,@start) AND @end AND dayoff = 1
		END
		ELSE
		BEGIN
			SELECT @adddays = COUNT(*) FROM dbo.GetCombinedCalendarForUser(@user_item_id, DATEADD(DAY,1,@start), @end) WHERE dayoff = 1
		END

		--Create new start point for the next loop.
		set @start = @end

	END

	--Return the result.
	Return @end

END
GO