DECLARE @NAME VARCHAR(100)
DECLARE @SQL NVARCHAR(300)
DECLARE CUR CURSOR FOR
    SELECT TABLE_NAME AS name FROM INFORMATION_SCHEMA.COLUMNS
    WHERE column_name = 'archive_userid' AND IS_NULLABLE = 'YES' --AND table_name = 'item_data_process_selections'
    ORDER BY TABLE_NAME DESC
OPEN CUR

FETCH NEXT FROM CUR INTO @NAME
EXEC app_set_archive_user_id 0
WHILE @@FETCH_STATUS = 0
BEGIN
    print @NAME
    SET @SQL = 'UPDATE ' + @NAME + ' SET archive_userid = 0 WHERE archive_userid IS NULL'
    EXEC Sp_executesql @SQL

    IF OBJECT_ID ('archive_' + @NAME, N'U') IS NOT NULL  BEGIN
        SET @SQL = 'UPDATE archive_' + @NAME + ' SET archive_userid = 0 WHERE archive_userid IS NULL'
        EXEC Sp_executesql @SQL
    END

    SET @SQL = 'ALTER TABLE ' + @name + ' ALTER COLUMN archive_userid INT NOT NULL'
    EXEC Sp_executesql @SQL
    FETCH NEXT FROM CUR INTO @NAME
END

CLOSE CUR
DEALLOCATE CUR 