ALTER TABLE item_tables ADD ordering TINYINT DEFAULT(1)
ALTER TABLE item_tables ADD archive TINYINT DEFAULT(1)
    
GO

ALTER TRIGGER [dbo].[items_AfterInsertRow] ON [dbo].[items] AFTER INSERT AS
    DECLARE @sql nvarchar(max) =
        (
            SELECT ';INSERT INTO ' + it.name + ' (item_id, order_no) VALUES (' + CAST(i.item_id AS VARCHAR) + ',' +
                   '(SELECT CASE WHEN ' + CAST(it.ordering AS NVARCHAR) + '=1 THEN dbo.getOrderBetween((SELECT TOP 1 ISNULL(order_no,''U'') FROM ' + it.name + ' ORDER BY order_no COLLATE Latin1_general_bin DESC),null) ELSE ''U'' END)' + ')'
            FROM item_tables it
                     INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
                 FOR XML PATH('')
        )
EXEC sp_executesql @sql

GO

ALTER TRIGGER [dbo].[items_BeforeInsertRow] ON [dbo].[items] INSTEAD OF INSERT AS
    BEGIN
        INSERT INTO items (instance, process, deleted)
        SELECT instance, process, deleted
        FROM Inserted
    END

GO

ALTER PROCEDURE [dbo].[app_ensure_archive_all]
    AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @sql NVARCHAR(MAX);
        DECLARE @TableName NVARCHAR(MAX);
        DECLARE @GetTables CURSOR

        --Get Tables
        SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
            SELECT table_name FROM INFORMATION_SCHEMA.TABLES  WHERE table_type = 'BASE TABLE' and substring(table_name, 0, 9) <> 'archive_'  and table_name <> 'sysdiagrams'

        OPEN @GetTables
        FETCH NEXT FROM @GetTables INTO @TableName

        WHILE (@@FETCH_STATUS=0) BEGIN
            IF @TableName not in (
                                  'attachments_data',
                                  'instance_configurations',
                                  'items',
                                  'instance_user_configurations',
                                  'instance_user_configurations_saved',
                                  'links'
                ) BEGIN
                print 'ensure archive for ' + @TableName
                SET @sql = 'EXEC app_ensure_archive '''+@TableName+''''
                print @sql
                EXEC (@sql)
            END
            FETCH NEXT FROM @GetTables INTO @TableName
        END
        CLOSE @GetTables
    END
GO

ALTER TABLE dbo.items DROP CONSTRAINT DF_items_order_no
ALTER TABLE dbo.items DROP COLUMN order_no

GO

EXEC app_remove_archive 'tasks'
EXEC app_remove_archive 'task_parents'
EXEC app_remove_archive 'schema_version'
EXEC app_remove_archive 'item_data'

DROP TABLE IF EXISTS tasks
DROP TABLE IF EXISTS task_parents
DROP TABLE IF EXISTS schema_version
DROP TABLE IF EXISTS archive_tasks
DROP TABLE IF EXISTS archive_task_parents
DROP TABLE IF EXISTS archive_schema_version
DROP TABLE IF EXISTS archive_item_data
DROP TABLE IF EXISTS item_data

EXEC app_set_archive_user_id 0
UPDATE item_tables SET ordering = 1, archive = 1

GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_archive]
    @tableName NVARCHAR(200)
    AS
    BEGIN
        -- Declaring variables	
        DECLARE @colNames NVARCHAR(MAX);
        DECLARE @colNamesInsert NVARCHAR(MAX);
        DECLARE @colTypes NVARCHAR(MAX);
        DECLARE @colDefs NVARCHAR(MAX);
        DECLARE @pkCols NVARCHAR(MAX);
        DECLARE @pkCols2 NVARCHAR(MAX);

        DECLARE @sql NVARCHAR(MAX);
        DECLARE @viewIndexSql nvarchar(MAX);

        -- Setting action variable
        DECLARE @action nvarchar(20)
        SET @action ='CREATE'

        -- Setting table names
        DECLARE @nameArchive NVARCHAR(200);
        DECLARE @nameExpired NVARCHAR(200);
        DECLARE @nameHistoryFunction NVARCHAR(100);

        SET @nameArchive = 'archive_'+@tableName;
        SET @nameExpired = 'old_'+@tableName;
        SET @nameHistoryFunction = 'get_'+@tableName;

        -- Setting column names
        DECLARE @archiveId NVARCHAR(100);
        DECLARE @archiveUserId NVARCHAR(100);
        DECLARE @archiveStart NVARCHAR(100);
        DECLARE @archiveEnd NVARCHAR(100);
        DECLARE @archiveType NVARCHAR(100);


        SET @archiveId = 'archive_id';
        SET @archiveStart = 'archive_start';
        SET @archiveEnd = 'archive_end';
        SET @archiveUserId = 'archive_userid';
        SET @archiveType = 'archive_type';


        DECLARE @defaultValue NVARCHAR(255);
        SET @defaultValue = null;

        DECLARE @archiveColsCount int;

        -- Getting list of columns in original table, 
        --	colNames is list of cols, 
        --	colTypes is list of names with datatypes
        --	colDefs is list of colnames encapsulated with isnull and column default value for insert	

        --Drop old archive TRIGGERS
        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN
            SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_UPDATE]'
            EXEC (@sql)
        END

        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_INSERT]')) BEGIN
            SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_INSERT]'
            EXEC (@sql)
        END
        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_Delete]')) BEGIN
            SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_Delete]'
            EXEC (@sql)
        END

        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameArchive+']')) BEGIN

            exec app_set_archive_user_id 1
            SET @sql = 'update '+@TableName+' set archive_start = getutcdate() WHERE archive_start is null '
            EXEC (@sql)
            SET @sql = 'update '+@TableName+' set archive_start = getutcdate(), archive_userid=1 WHERE archive_userid is null or archive_userid is null'
            EXEC (@sql)
        END

        IF COL_LENGTH(@tableName,@archiveUserId) IS NULL
            BEGIN
                SET @sql = 'ALTER TABLE ['+@tableName+'] ADD '+@archiveUserId+' int  DEFAULT 0'
                EXEC (@sql)
            END
        ELSE
            BEGIN
                SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN '+@archiveUserId+' int NOT NULL'
                EXEC (@sql)

                SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveUserId
                IF @defaultValue IS NULL
                    BEGIN
                        SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT 0 FOR '+@archiveUserId
                        EXEC (@sql)
                    END

                EXEC (@sql)
            END

        IF COL_LENGTH(@tableName,@archiveStart) IS NULL
            BEGIN
                SET @sql = 'ALTER TABLE ['+@tableName+'] ADD  '+@archiveStart+' [datetime] DEFAULT getutcdate()'
                EXEC (@sql)
            END
        ELSE
            BEGIN
                SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN  '+@archiveStart+' [datetime]'
                EXEC (@sql)

                SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveStart
                IF @defaultValue IS NULL
                    BEGIN
                        SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT getutcdate() FOR '+@archiveStart
                        EXEC (@sql)
                    END
            END

        SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = @nameArchive
        IF @archiveColsCount = 0
            BEGIN
                SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
                       @colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' +
                                   data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
                                   ( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' +
                                   COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
                       @colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_name = @tablename

                -- Creating archive table
                SET @sql = '
				CREATE TABLE dbo.'+@nameArchive+'(
					['+@archiveId+'] int IDENTITY(1,1) NOT NULL,
					['+@archiveEnd+'] [datetime] NULL,
					['+@archiveType+'] tinyint NOT NULL,
					'+@colTypes+')  ON [PRIMARY]'
                EXEC (@sql)

            END
        ELSE
            BEGIN
                SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)
                IF @archiveColsCount > 0
                    BEGIN
                        SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
                               @colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' +
                                           data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
                                           ( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' +
                                           COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
                               @colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
                        FROM INFORMATION_SCHEMA.COLUMNS
                        WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)

                        SET @sql = 'ALTER TABLE dbo.'+@nameArchive+' ADD '+@colTypes
                        EXEC (@sql)
                    END

                SET @action ='ALTER'

                --[item_columns_AFTER_UPDATE]
                --[item_columns_AFTER_INSERT]
                --[item_columns_Delete]
            END

        SET @colNames = null
        SET @colTypes = null
        SET @colDefs = null

        DECLARE @pkColsCount int = 0

        SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
               @colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' +
                           data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
                           ( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' +
                           COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
               @colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = @tablename

        SELECT @colNamesInsert = COALESCE(@colNamesInsert+',','')+'['+column_name+']'
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = @tablename AND (select columnproperty(object_id(@tablename),column_name,'IsIdentity')) = 0 AND column_name <> @archiveUserId

        SELECT @pkColsCount = count(column_name)
        from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
                INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
        where 	pk.TABLE_NAME = @tableName
          and	CONSTRAINT_TYPE = 'PRIMARY KEY'
          and	c.TABLE_NAME = pk.TABLE_NAME
          and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
          and pk.table_catalog = (SELECT db_name())

        SELECT @pkCols = COALESCE(@pkCols+',','')+'['+column_name+']'
        from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
                INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
        where 	pk.TABLE_NAME = @tableName
          and	CONSTRAINT_TYPE = 'PRIMARY KEY'
          and	c.TABLE_NAME = pk.TABLE_NAME
          and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
          and pk.table_catalog = (SELECT db_name())

        IF @pkColsCount > 1
            BEGIN
                SELECT @pkCols2 = COALESCE(@pkCols2+'+','')+'CAST(['+column_name+'] as nvarchar) '
                from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
                        INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
                where 	pk.TABLE_NAME = @tableName
                  and	CONSTRAINT_TYPE = 'PRIMARY KEY'
                  and	c.TABLE_NAME = pk.TABLE_NAME
                  and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
                  and pk.table_catalog = (SELECT db_name())
            END
        ELSE
            BEGIN
                SELECT @pkCols2 = COALESCE(@pkCols2+'+','')+'['+column_name+']'
                from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
                        INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
                where 	pk.TABLE_NAME = @tableName
                  and	CONSTRAINT_TYPE = 'PRIMARY KEY'
                  and	c.TABLE_NAME = pk.TABLE_NAME
                  and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
                  and pk.table_catalog = (SELECT db_name())
            END

        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN
            SET @action ='ALTER'
        END
        ELSE begin
            SET @action ='CREATE'
        END

        SET @sql =  @action + ' TRIGGER [dbo].['+@tableName+'_INSTEAD_OF_INSERT]
	ON [dbo].['+@tableName+']
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

	INSERT INTO [dbo].['+@tableName+'] ('+@colNamesInsert+', '+@archiveUserId+') 
	SELECT '+@colNamesInsert+' 
	, @user_id
	FROM inserted
	'
        EXEC (@sql)


        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN
            SET @action ='ALTER'
        END
        ELSE begin
            SET @action ='CREATE'
        END

        -- Creating UPDATE triggers
        SET @sql = @action + ' TRIGGER [dbo].['+@tableName+'_AFTER_UPDATE]
	ON [dbo].['+@tableName+']
	AFTER UPDATE
	AS

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
IF (SELECT archive FROM item_tables WHERE name = ''' + @tableName + ''') = 1 BEGIN
    UPDATE [dbo].['+@tableName+']
    SET ' + @archiveStart + '= getutcdate(),
    ' + @archiveUserId + '=  @user_id
    WHERE ' + @pkCols2 + ' IN (SELECT DISTINCT ' + @pkCols2 + ' FROM Inserted)

	INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
	SELECT '+@colNames+',
		'+@archiveEnd+' = getutcdate(),
		'+@archiveType+' = 1
	FROM deleted	
END
	'
        EXEC (@sql)

        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_DELETE]')) BEGIN
            SET @action ='ALTER'
        END
        ELSE begin
            SET @action ='CREATE'
        END

        -- Creating Delete Trigger 
        SET @sql =  @action +' TRIGGER [dbo].['+@tableName+'_DELETE]
		ON [dbo].['+@tableName+']
		FOR DELETE
		AS	
IF (SELECT archive FROM item_tables WHERE name = ''' + @tableName + ''') = 1 BEGIN
			INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
			SELECT '+@colNames+',
				'+@archiveEnd+' = getutcdate(),
				'+@archiveType+' = 1
			FROM deleted
			
			DECLARE @user_id int;
			select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

			INSERT INTO '+@nameArchive+'('+REPLACE(@colNames,'['+@archiveUserId+'],','')+','+@archiveEnd+', '+@archiveType+', '+@archiveUserId+')
			SELECT '+REPLACE(@colNames,'['+@archiveUserId+'],','')+',
				'+@archiveEnd+' = getutcdate(),
				'+@archiveType+' = 2,
				'+@archiveUserId+' = @user_id
			FROM deleted
END
			'
        EXEC (@sql)


        -- Creating or altering history function, depends if it exist

        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameHistoryFunction+']')) BEGIN
            SET @action ='ALTER'
        END
        ELSE begin
            SET @action ='CREATE'
        END

        SET @sql = @action +' FUNCTION  [dbo].['+@nameHistoryFunction+']( @d datetime )
		RETURNS @rtab TABLE (archive_id INT, '+@colTypes+')
		AS
		BEGIN	
			INSERT @rtab
				SELECT archive_id, '+@colNames+'
				FROM '+@nameArchive+' WHERE '+@archiveId+' IN (
					SELECT MAX('+@archiveId+')
					FROM '+@nameArchive+'
					WHERE '+@archiveStart+' <= @d AND archive_end > @d AND ' + @pkCols2 + ' NOT IN
					(SELECT ' + @pkCols2 + ' FROM  '+@tableName+'  WHERE '+@archiveStart+' < @d  )
					GROUP BY ' + @pkCols + '
					)
				UNION 
				SELECT 0, '+@colNames+' 
				FROM '+@tableName+' 
				WHERE '+@archiveStart+' < @d  
			RETURN
		END'
        EXEC (@sql)

        exec app_set_archive_user_id 1
        SET @sql = 'update '+@TableName+' set archive_start = getutcdate() WHERE archive_start is null '
        EXEC (@sql)
        SET @sql = 'update '+@TableName+' set archive_start = getutcdate(), archive_userid=1 WHERE archive_userid is null or archive_userid is null'
        EXEC (@sql)
    END
GO