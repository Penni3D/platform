ALTER PROCEDURE [dbo].[app_ensure_column]
    @instance NVARCHAR(10), @process NVARCHAR(50), @column_name NVARCHAR(50), @container_id INT,
        @data_type TINYINT, @system_column TINYINT, @inputMethod INT, @inputName NVARCHAR(255), @portfolio_id INT, @use_in_select_result TINYINT, @dataAdditional1 NVARCHAR(MAX) = null, @dataAdditional2 NVARCHAR(MAX) = null, @use_in_filter INT = 0, @item_column_id INT = 0 OUTPUT
    AS
    BEGIN
        SET NOCOUNT ON;
        SET @item_column_id = 0
        SELECT @item_column_id = item_column_id FROM item_columns WHERE instance = @instance AND name = @column_name AND process = @process

        --Archive user is 0
        DECLARE @BinVar varbinary(128);
        SET @BinVar = CONVERT(varbinary(128), 0);
        SET CONTEXT_INFO @BinVar;

        IF (@item_column_id = 0)
            BEGIN
                DECLARE @langVar NVARCHAR (50) = 'IC_' + @process + '_' + @column_name
                INSERT INTO item_columns (instance, process, name, data_type, system_column, variable, input_method, input_name, data_additional, data_additional2)
                values (@instance, @process, @column_name, @data_type, 0, UPPER(@langVar), @inputMethod, @inputName, @dataAdditional1, @dataAdditional2)
                SELECT @item_column_id = @@IDENTITY


                IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
                    BEGIN
                        INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @column_name, 'en-GB')
                    END

                IF (@container_id > 0) BEGIN
                    INSERT INTO process_container_columns (process_container_id, item_column_id) values(@container_id, @item_column_id)
                END
            END

        IF (@portfolio_id > 0)
            BEGIN
                IF (SELECT COUNT(item_column_id) FROM process_portfolio_columns WHERE process_portfolio_id = @portfolio_id AND item_column_id = @item_column_id) = 0
                    BEGIN
                        INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter)
                        VALUES(
                                  @portfolio_id,
                                  @item_column_id,
                                  ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM process_portfolio_columns WHERE process_portfolio_id = @portfolio_id
                                                                      ORDER BY order_no COLLATE Latin1_general_bin DESC),null)),'U'),
                                  @use_in_select_result,
                                  @use_in_filter
                              )
                    END
            END
    END
GO
update item_columns set system_column = 0 WHERE system_column = 1
GO