SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER FUNCTION [dbo].[menu_VarReplace]
(
 @instance NVARCHAR(10),
 @params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('portfolioId', @params)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(
',', @params, @startPos)
			SET @endPos2 = charindex('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)
			
			SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		

      IF isnumeric(@var) > 0
      BEGIN 

        SET @params = replace(@params, @var2, 'portfolioId":' + @var4)
  
        IF @endPos2 > @endPos1 AND @endPos1 > 0
          BEGIN
            SET @startPos = charindex('portfolioId', @params, @endPos1+LEN(@var4))
          END
        ELSE
          BEGIN
            SET @startPos = charindex('portfolioId', @params, @endPos2+LEN(@var4))
          END
			END
		  CONTINUE  
	END

 RETURN @params
END
GO