EXEC app_ensure_archive 'process_portfolio_columns'
ALTER TABLE process_portfolio_columns
ADD portfolio_column_group_id INTEGER

ALTER table process_portfolio_columns
  ADD constraint FK_portfolio_column_group_id 
  FOREIGN KEY (portfolio_column_group_id) REFERENCES process_portfolio_columns_groups(portfolio_column_group_id) 
  ON DELETE SET NULL 