SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[item_GetId]
(
 @guid uniqueidentifier
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = item_id FROM items WHERE guid = @guid
 RETURN @id
END
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[item_GetGuid]
(
 @itemId int
)
RETURNS uniqueidentifier
AS
BEGIN
 DECLARE @id uniqueidentifier
 SELECT TOP 1 @id = guid FROM items WHERE item_id = @itemId
 RETURN @id
END
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER FUNCTION [dbo].[menu_VarReplace]
(
 @instance NVARCHAR(10),
 @params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('portfolioId', @params)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(
',', @params, @startPos)
			SET @endPos2 = charindex('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)
			
			SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		

      IF isnumeric(@var4) > 0
      BEGIN 

        SET @params = replace(@params, @var2, 'portfolioId":' + @var4)
  
        IF @endPos2 > @endPos1 AND @endPos1 > 0
          BEGIN
            SET @startPos = charindex('portfolioId', @params, @endPos1+LEN(@var4))
          END
        ELSE
          BEGIN
            SET @startPos = charindex('portfolioId', @params, @endPos2+LEN(@var4))
          END
			END
		  CONTINUE  
	END

 RETURN @params
END
GO
SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[condition_VarReplaceProc]
(
 @sourceInstance NVARCHAR(10),
 @targetInstance NVARCHAR(10),
 @condition_json NVARCHAR(MAX),
 @replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) <> 1 
			BEGIN

				SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		
				if @var4 is not null 
				begin
							SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)
				end
	
				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
					END
	
				IF dbo.IsListColumn(dbo.itemColumn_GetId(@sourceInstance, @var3))  > 0
					BEGIN					
						DECLARE @idCursor CURSOR 
						DECLARE @listItemId INT 
						DECLARE @value nvarchar(max)
						DECLARE @id int
						DECLARE @process nvarchar(50)
	
						SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
						SELECT item_id FROM dbo.get_ListColumnIds(dbo.itemColumn_GetId(@sourceInstance, @var3));
	
						select @process = data_additional FROM item_columns WHERE instance = @sourceInstance and item_column_id = dbo.itemColumn_GetId(@sourceInstance, @var3)
	
						OPEN @idCursor
						FETCH NEXT FROM @idCursor INTO @listItemId
						WHILE (@@FETCH_STATUS=0) BEGIN
	
---							EXEC list_GetValue @listItemId, @value = @value output
--							EXEC list_GetId @targetInstance, @process, @value, @id = @id output
							select @id = dbo.item_GetId(@value)
							if @id is not null 
							begin
								set @condition_json = replace(@condition_json, '"Value":"' + @value + '"}', '"Value":"' + CAST(@id as nvarchar) + '"}')
							end 

							FETCH NEXT FROM @idCursor INTO @listItemId
						END
					END
				END
		  CONTINUE  
	END



	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) > 0 
			BEGIN


				SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)
	
				SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)
	
				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
					END
			END

		  CONTINUE  
	END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)
			
			IF ISNUMERIC(@var3) > 0
			BEGIN

			SET @var4 = dbo.condition_GetId(@targetInstance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'ConditionId":' + @var4)

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2+LEN(@var4))
				END
			END

		  CONTINUE  
	END
 SET @replaced_json = @condition_json
END
GO


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[condition_IdReplaceProc]
(
 @condition_json NVARCHAR(MAX),
 @replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;  
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.itemColumn_GetVariable(@var3)				declare @replaced as nvarchar (max)
		

				--SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)
				select  @condition_json = stuff(@condition_json, charindex(@var2, @condition_json), len(@var2), 'ItemColumnId":' + @var4)

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
					END
				IF dbo.IsListColumn(@var3)  > 0
				BEGIN					

					DECLARE @idCursor CURSOR 
					DECLARE @listItemId INT 
					DECLARE @value nvarchar(max)

					SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
					SELECT item_id FROM dbo.get_ListColumnIds(@var3);

					OPEN @idCursor
					FETCH NEXT FROM @idCursor INTO @listItemId
					WHILE (@@FETCH_STATUS=0) BEGIN
						select @value = cast(dbo.item_GetGuid(@listItemId) as nvarchar(max))

						if @listItemId is not null and @value is not null 
						begin
							set @replaced = replace(@condition_json, '"Value":' + CAST(@listItemId AS nvarchar) + '}', '"Value":"' + @value + '"}')
							if @replaced is not null
							begin
								set @condition_json = @replaced
							end
						end


						FETCH NEXT FROM @idCursor INTO @listItemId
					END
				END
			END
			ELSE
			BEGIN
				--SET @condition_json = NULL
				set @replaced = ''
			END

		  CONTINUE  
	END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  	
			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)

			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.itemColumn_GetVariable(@var3)		

				--SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)
				select  @condition_json = stuff(@condition_json, charindex(@var2, @condition_json), len(@var2), 'UserColumnId":' + @var4)

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
					END
			END
			ELSE
			BEGIN
				IF @var3 <> 'null'
				BEGIN
						SET @condition_json = NULL
				END
			END
		  CONTINUE  
	END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.condition_GetVariable(@var3)		

				--SET @condition_json = replace(@condition_json, @var2, 'ConditionId":' + @var4)
				select  @condition_json = stuff(@condition_json, charindex(@var2, @condition_json), len(@var2), 'ConditionId":' + @var4)

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('ConditionId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('ConditionId', @condition_json, @endPos2+LEN(@var4))
					END
			END
			ELSE
			BEGIN
				IF @var3 <> 'null'
				BEGIN
						SET @condition_json = NULL
				END
			END

		  CONTINUE  
	END
 SET @replaced_json = @condition_json
END
GO