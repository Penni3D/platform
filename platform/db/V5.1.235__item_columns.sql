IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[item_columns]') AND name = 'use_lucene') 
    ALTER TABLE item_columns
          ADD use_lucene tinyint NOT NULL
          CONSTRAINT ic_use_lucene_df DEFAULT 0
    WITH VALUES
