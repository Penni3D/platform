DECLARE @Process NVARCHAR(50);
DECLARE @Instance NVARCHAR(10);
DECLARE @sql NVARCHAR(MAX) = ''
EXEC app_set_archive_user_id 1

DECLARE cursor_process CURSOR
FOR SELECT DISTINCT(p.process), p.instance from item_columns ic 
  INNER JOIN processes p ON p.process = ic.process  
  WHERE ic.[name] IN ('task_type', 'parent_item_id', 'owner_item_id') AND p.process_type IN (0,1,2) AND p.list_process = 0

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Process, @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
	UPDATE processes SET process_type = 5 WHERE process = @Process
	
	 FETCH NEXT FROM cursor_process INTO 
            @Process,
			@Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;


   DECLARE @instanceMenuId INT
   DECLARE @MenuVariable NVARCHAR(255)
   DECLARE @ParentMenuId INT
   DECLARE @AdminColumnId INT

   SET @ParentMenuId = (SELECT instance_menu_id FROM instance_menu WHERE variable = 'PROCESSES')

   SET @MenuVariable = 'instancemenu_' + @Instance + '_table_processes_p'

          INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, icon) VALUES(@Instance, @ParentMenuId, @MenuVariable, 'XU', 'processes', '{"processType":5}', 'wallpaper')
          SET @instanceMenuId = @@IDENTITY


          SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @Instance + '_user_group ORDER BY item_id ASC)); '
          EXEC (@sql)

            DECLARE @Language NVARCHAR(10);

		DECLARE language_c CURSOR
		FOR SELECT
			code from instance_languages

			OPEN language_c

			FETCH NEXT FROM language_c INTO @Language
				WHILE @@FETCH_STATUS = 0
				BEGIN
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@Instance, @Process, @Language, @MenuVariable, 'Table Processes')
				FETCH NEXT FROM language_c INTO
			@Language
				END;
			CLOSE language_c;
			DEALLOCATE language_c;



