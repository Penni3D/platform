﻿CREATE TABLE dbo.items_deleted (
	item_id int NOT NULL,
	instance nvarchar(10) NOT NULL,
	process nvarchar(50) NOT NULL,
	deleted_date DATETIME
);
ALTER TABLE dbo.items_deleted ADD CONSTRAINT PK_items_deleted PRIMARY KEY (item_id);
GO
CREATE VIEW items_all AS
SELECT item_id, instance, process, 0 AS deleted, NULL AS deleted_date FROM items
UNION
SELECT item_id, instance, process, 1 AS deleted, deleted_date FROM items_deleted
GO
ALTER  TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
AS 
 set xact_abort off; 
	DECLARE @ids nvarchar(max) =(select ',' + CAST(item_id as nvarchar) FROM deleted FOR XML PATH(''))
	
	DECLARE @tName as nvarchar(max)
	DECLARE @cName as nvarchar(max)

	DECLARE @countSql as nvarchar (max) = ''

	IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
	CREATE table #DeleteItems (item_id int, ord int)
			
	DECLARE @GetTables CURSOR
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	select ccu.TABLE_NAME,  ccu.COLUMN_NAME from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
	inner join  INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
	where UNIQUE_CONSTRAINT_NAME = 'PK_items' AND substring(ccu.TABLE_NAME, 1, 1) = '_' 

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName

	WHILE (@@FETCH_STATUS=0) BEGIN	
		DECLARE @sql nvarchar(max) = ' WITH rec AS ( 
		 SELECT t.item_id, t.' + @cName + ', 0 as lvl
		 FROM ' + @tName + ' t 
		 WHERE t.' + @cName + ' IN (-1' + @ids + ')
		 UNION ALL 
		 SELECT t.item_id, t.' + @cName + ' , r.lvl + 1
		 FROM ' + @tName + ' t 
		 INNER JOIN rec r ON r.item_id = t.' + @cName + ') 
			INSERT INTO #DeleteItems (item_id, ord)  SELECT item_id, lvl FROM rec order by lvl desc  '
		EXECUTE (@sql)
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

    DELETE  FROM item_subitems 					WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data 						WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM process_tabs_subprocess_data 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM item_data_process_selections 	WHERE item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE process_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_parents 	WHERE task_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
	DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)

	DECLARE @delId as int
	DECLARE @GetIds CURSOR

	SET @GetIds = CURSOR FOR SELECT item_id FROM #DeleteItems order by ord desc

	OPEN @GetIds
	FETCH NEXT FROM @GetIds INTO @delId

	WHILE (@@FETCH_STATUS=0) BEGIN
			IF OBJECT_ID('tempdb..#DeleteItems2') IS NOT NULL DROP TABLE #DeleteItems2
				CREATE table #DeleteItems2(item_id int, ord int)
						
				
				SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
				select ccu.TABLE_NAME,  ccu.COLUMN_NAME from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
				inner join  INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
				where UNIQUE_CONSTRAINT_NAME = 'PK_items' AND substring(ccu.TABLE_NAME, 1, 1) = '_' 
			
				OPEN @GetTables
				FETCH NEXT FROM @GetTables INTO @tName, @cName
			
				WHILE (@@FETCH_STATUS=0) BEGIN	
					SET @sql  = ' WITH rec AS ( 
					 SELECT t.item_id, t.' + @cName + ', 0 as lvl
					 FROM ' + @tName + ' t 
					 WHERE t.' + @cName + ' IN (' + CAST(@delId as nvarchar) + ')
					 UNION ALL 
					 SELECT t.item_id, t.' + @cName + ' , r.lvl + 1
					 FROM ' + @tName + ' t 
					 INNER JOIN rec r ON r.item_id = t.' + @cName + ') 
						INSERT INTO #DeleteItems2 (item_id, ord)  SELECT item_id, lvl FROM rec order by lvl desc  '
					EXECUTE (@sql)
					FETCH NEXT FROM @GetTables INTO @tName, @cName
				END
				CLOSE @GetTables
			
			    DELETE  FROM item_subitems 					WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM item_data 						WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM process_tabs_subprocess_data 	WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM item_data_process_selections 	WHERE item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_parents 	WHERE process_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_parents 	WHERE task_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems2)
				DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems2)
			

				DECLARE @delId2 as int
				DECLARE @GetIds2 CURSOR
			
				SET @GetIds2 = CURSOR FOR SELECT item_id FROM #DeleteItems2 order by ord desc
			
				OPEN @GetIds2
				FETCH NEXT FROM @GetIds2 INTO @delId2
			
				WHILE (@@FETCH_STATUS=0) BEGIN
					INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM items WHERE item_id = @delId2
					DELETE FROM items WHERE item_id = @delId2
					
				FETCH NEXT FROM @GetIds2 INTO @delId2
				END
				CLOSE @GetIds2
		INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM items WHERE item_id = @delId
		DELETE FROM items WHERE item_id = @delId
	FETCH NEXT FROM @GetIds INTO @delId
	END
	CLOSE @GetIds

    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data dd						INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.process_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.task_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id

	DELETE dd FROM instance_comments dd INNER JOIN deleted d ON dd.sender = d.item_id
	DELETE dd FROM process_comments dd INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_comments dd INNER JOIN deleted d ON dd.sender = d.item_id
	DELETE dd FROM instance_user_configurations_saved dd INNER JOIN deleted d ON dd.user_id = d.item_id
	DELETE dd FROM instance_user_configurations dd INNER JOIN deleted d ON dd.user_id = d.item_id
	
	--Insert into deleted items
	INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM deleted

	--Finally delete the item
	DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id