﻿SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_process_comments]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'process_comments'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_list @instance, @process, @process, 0

    EXEC dbo.app_ensure_column @instance,@process, 'instance', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'process', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'process_item_id', 0, 1, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'identifier', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'sender', 0, 1, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'comment', 0, 4, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'date', 0, 13, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'comment_type', 0, 1, 1, 0, 0, NULL, 0, 0

  END
GO


DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor
FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
BEGIN
  exec app_ensure_process_process_comments @instance
END
FETCH NEXT FROM instanceCursor INTO @instance
CLOSE instanceCursor
DEALLOCATE instanceCursor


GO 
exec app_set_archive_user_id 0

DECLARE @instance nvarchar(10)
DECLARE @sql NVARCHAR(MAX) = ''

DECLARE @commentId int
DECLARE @newId int

DECLARE commentsCursor CURSOR FOR SELECT comment_id, instance FROM process_comments
OPEN commentsCursor
FETCH NEXT FROM commentsCursor INTO  @commentId, @instance

WHILE @@fetch_status = 0
  BEGIN
    INSERT INTO items (instance, process) values(@instance, 'process_comments')
    SELECT @newId = @@IDENTITY
    set @sql = 'update pci SET instance = pc.instance, process = pc.process, process_item_id = pc.item_id, identifier = pc.identifier, sender = pc.sender, comment = pc.comment, date = pc.date, comment_type = 0 ' +
    ' FROM _' + @instance + '_process_comments pci INNER JOIN  process_comments pc ON pc.comment_id = ' + CAST (@commentId AS nvarchar(max)) + ' AND pci.item_id = ' +  CAST (@newId AS nvarchar(max))
    print (@sql)
    exec (@sql) 
    FETCH NEXT FROM commentsCursor INTO @commentId, @instance

  END

CLOSE commentsCursor
DEALLOCATE commentsCursor


GO 
exec app_set_archive_user_id 0

DECLARE @instance nvarchar(10)
DECLARE @sql NVARCHAR(MAX) = ''

DECLARE @commentId int
DECLARE @newId int

DECLARE commentsCursor CURSOR FOR SELECT comment_id, instance FROM instance_comments
OPEN commentsCursor
FETCH NEXT FROM commentsCursor INTO  @commentId, @instance

WHILE @@fetch_status = 0
  BEGIN
    INSERT INTO items (instance, process) values(@instance, 'process_comments')
    SELECT @newId = @@IDENTITY
    set @sql = 'update pci SET instance = pc.instance,  identifier = pc.identifier, sender = pc.sender, comment = pc.comment, date = pc.date, comment_type = 1 ' +
    ' FROM _' + @instance + '_process_comments pci INNER JOIN  instance_comments pc ON pc.comment_id = ' + CAST (@commentId AS nvarchar(max)) + ' AND pci.item_id = ' +  CAST (@newId AS nvarchar(max))
    print (@sql)
    exec (@sql) 
    FETCH NEXT FROM commentsCursor INTO @commentId, @instance

  END

CLOSE commentsCursor
DEALLOCATE commentsCursor

GO
DROP TABLE process_comments
GO
DROP TABLE instance_comments

