SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[item_GetIds]
(
	@guids NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @ids NVARCHAR(MAX)
	SET @ids = STUFF((SELECT ',' + CAST(item_id AS NVARCHAR) FROM items WHERE CAST(guid AS NVARCHAR(36)) IN (SELECT LTRIM(value) FROM STRING_SPLIT(@guids, ',')) FOR XML PATH('')), 1, 1, '')
	IF @ids IS NULL	BEGIN
		SET @ids = ''
	END
	RETURN @ids
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[item_GetGuids]
(
	@itemIds NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @variables NVARCHAR(MAX)
	SET @variables = STUFF((SELECT ', ' + CAST(guid AS NVARCHAR(36)) FROM items WHERE item_id IN (SELECT value FROM STRING_SPLIT(@itemIds, ',')) ORDER BY CAST(guid AS NVARCHAR(36)) ASC FOR XML PATH('')), 1, 2, '')
	IF @variables IS NULL	BEGIN
		SET @variables = ''
	END
	RETURN @variables
END



GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processDashboardChart_GetIds]
(
	@instance NVARCHAR(10),
	@variables NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @ids NVARCHAR(MAX)
	SET @ids = STUFF((SELECT ',' + CAST(process_dashboard_chart_id AS NVARCHAR) FROM process_dashboard_charts WHERE variable IN (SELECT value FROM STRING_SPLIT(@variables, ',')) AND instance = @instance ORDER BY variable ASC FOR XML PATH('')), 1, 1, '')
	IF @ids IS NULL	BEGIN
		SET @ids = ''
	END
	RETURN @ids
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processDashboardChart_GetVariables]
(
	@process_dashboard_chart_ids NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @variables NVARCHAR(MAX)
	SET @variables = STUFF((SELECT ',' + variable FROM process_dashboard_charts WHERE process_dashboard_chart_id IN (SELECT value FROM STRING_SPLIT(@process_dashboard_chart_ids, ',')) ORDER BY variable ASC FOR XML PATH('')), 1, 1, '')
	IF @variables IS NULL	BEGIN
		SET @variables = ''
	END
	RETURN @variables
END