EXEC app_set_archive_user_id 0

ALTER TABLE item_columns
ADD parent_select_type TINYINT DEFAULT 0

GO 

UPDATE item_columns
SET parent_select_type = 0
