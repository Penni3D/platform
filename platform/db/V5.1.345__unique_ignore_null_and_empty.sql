ALTER TRIGGER [dbo].[AfterUpdateColumn] ON [dbo].[item_columns] AFTER update AS
    DECLARE @GetChanges CURSOR
DECLARE @TableName NVARCHAR(255)
DECLARE @OldColumnName NVARCHAR(255)
DECLARE @ColumnName NVARCHAR(50)
DECLARE @TableColumnName NVARCHAR(MAX)
DECLARE @DataType TINYINT
DECLARE @UseFk TINYINT
DECLARE @Unique TINYINT

DECLARE @sql nvarchar(max)

SET @GetChanges = CURSOR FAST_FORWARD LOCAL FOR
    SELECT it.name AS TableName, i.name AS ColumnName, i.data_type, d.name As OldColumnName, i.use_fk, i.unique_col FROM item_tables it
                                                                                                                             INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
                                                                                                                             INNER JOIN deleted d ON i.item_column_id = d.item_column_id

OPEN @GetChanges
FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType,@OldColumnName, @UseFk, @Unique
WHILE (@@FETCH_STATUS=0)
    BEGIN
        IF @ColumnName IS NOT NULL AND @OldColumnName IS NULL AND LEN(@ColumnName) > 0
            BEGIN
                EXEC dbo.items_AddColumn @TableName,@ColumnName,@DataType
            END
        ELSE
            BEGIN
                IF @ColumnName <> @OldColumnName
                    BEGIN
                        SET @TableColumnName = @TableName + '.' + @OldColumnName
                        EXEC sp_rename @TableColumnName, @ColumnName, 'COLUMN'
                    END
            END
        IF @UseFk = 1
            BEGIN
                IF Not Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']'))
                    BEGIN
                        SET @sql = ' ALTER TABLE ' + @TableName + ' WITH CHECK ADD  CONSTRAINT FK_' + @TableName+ '_' + @ColumnName + ' FOREIGN KEY(' + @ColumnName + ')
                REFERENCES [dbo].[items] ([item_id])
                    '
                        EXEC (@sql)
                    END
            END
        ELSE
            BEGIN
                IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']')) BEGIN
                    SET @sql = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT FK_' + @TableName+ '_' + @ColumnName
                    EXEC (@sql)
                END
            END

        IF @Unique = 1
            BEGIN
                IF NOT EXISTS (SELECT name from sys.indexes WHERE name = 'IX_' + @TableName + '_' + @ColumnName + '_unique')
                    BEGIN
                        SET @sql = 'CREATE UNIQUE INDEX IX_' + @TableName + '_' + @ColumnName + '_unique ON ' +  @TableName + '(' + @ColumnName + ') WHERE (' + @ColumnName + ' IS NOT NULL AND ' + @ColumnName + ' <> '''')'
                        EXEC (@sql)
                    END
            END

        FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType, @OldColumnName, @UseFk, @Unique
    END