IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_preparation_phase'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_preparation_phase] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_preparation_phase]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'preparation_phase'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2

	EXEC dbo.app_ensure_column @instance,@process,'idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'main_phase_milestone', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'measurable_target', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'userData', 0, 5, 1, 0, 0, NULL, 0, 0, 'user'
	EXEC dbo.app_ensure_column @instance,@process,'schedule_start', 0, 4, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'schedule_end', 0, 4, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'duration', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'cost_type', 0, 6, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'budget', 0, 2, 1, 0, 0, NULL, 0, 0


END
GO

