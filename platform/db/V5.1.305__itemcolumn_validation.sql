EXEC app_ensure_archive 'item_columns'
ALTER TABLE item_columns
ADD ic_validation NVARCHAR(255)

UPDATE process_container_columns SET [validation] = REPLACE(validation, '}',
',"ReadOnly"' + ':' + CAST([read_only] AS NVARCHAR) + 
CASE WHEN [required] IS NULL THEN '' ELSE ',' + '"Required"' + ':' + CAST([required] AS NVARCHAR) END +
CASE WHEN label IS NULL OR label = '' OR label = '{}' THEN '' ELSE ',' + '"Label"' + ':' + '"' + REPLACE(label, '"', '\"') + '"' END
+ '}')

WHERE LEN([validation]) > 2

EXEC app_set_archive_user_id 1
--FIX WHERE THERE IS NO VALIDATION
UPDATE process_container_columns SET [validation] = 
'{' + 
'"ReadOnly"' + ':' + CAST([read_only] AS NVARCHAR) + 
CASE WHEN [required] IS NULL THEN '' ELSE ',' + '"Required"' + ':' + CAST([required] AS NVARCHAR) END +
CASE WHEN label IS NULL OR label = '' OR label = '{}' THEN '' ELSE ',' + '"Label"' + ':' + '"' +  REPLACE(label, '"', '\"') + '"' END
+ '}'
WHERE [validation] IS NULL OR [validation] = '{}'

GO
--FIX WHERE THERE IS AN EXISTING VALIDATION

GO
EXEC app_remove_archive 'process_container_columns'
GO
ALTER TABLE process_container_columns DROP CONSTRAINT DF_process_container_columns_read_only
ALTER TABLE process_container_columns DROP COLUMN [required]
ALTER TABLE process_container_columns DROP COLUMN [read_only]
ALTER TABLE process_container_columns DROP COLUMN [label]
GO
EXEC app_ensure_archive 'process_container_columns'