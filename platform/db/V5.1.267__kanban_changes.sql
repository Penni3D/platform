DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN

        EXEC app_set_archive_user_id 0
        DECLARE @process NVARCHAR(50) = 'list_task_status' 
        EXEC app_ensure_column @instance,@process,'kanban_status',0,1,0,0,0,'',0,0,'','',0,0
  
FETCH  NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor

GO


EXEC app_set_archive_user_id 0
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor
FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
        DECLARE @sql NVARCHAR(MAX)
        DECLARE @process NVARCHAR(50) = 'list_task_status'
        DECLARE @table NVARCHAR(100) = '_' + @instance + '_' + @process
        
        SET @sql = 'UPDATE ' + @table +' SET list_item = ''Not Started'', kanban_status = 0 
        WHERE item_id = (SELECT TOP 1 item_id FROM ' + @table + ' ORDER BY item_id ASC)'
        EXEC (@sql)
        
        SET @sql = 'UPDATE ' + @table +' SET kanban_status = 2 
        WHERE item_id = (SELECT TOP 1 item_id FROM ' + @table + ' ORDER BY item_id DESC)'
        EXEC (@sql)
        
        DECLARE @update_itemid INT
        INSERT INTO items (instance, process) VALUES (@instance, @process);
        SET @update_itemid = @@identity;
        
        SET @sql = 'UPDATE ' + @table + ' SET
                                        list_item = ''In Progress'',
                                        list_symbol = ''lens'',
                                        list_symbol_fill = ''blue'',
                                        kanban_status = 1 WHERE item_id = ' + CAST(@update_itemid AS NVARCHAR)
        EXEC(@sql)
FETCH NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor


GO


EXEC app_remove_archive 'task_status'
ALTER TABLE task_status ADD status_list_item_id INT
EXEC app_ensure_archive 'task_status'


GO


EXEC app_set_archive_user_id 0
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
        DECLARE @process NVARCHAR(50) = 'list_task_status'
        DECLARE @table NVARCHAR(100) = '_' + @instance + '_' + @process
        
        DECLARE @parent_item_id INT
        DECLARE @GetTables CURSOR
        DECLARE @sql NVARCHAR(MAX)
        
        SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
        SELECT DISTINCT parent_item_id FROM task_status
        OPEN @GetTables
        FETCH NEXT FROM @GetTables INTO @parent_item_id
        WHILE (@@FETCH_STATUS=0) BEGIN
          SET @sql = 'UPDATE task_status SET status_list_item_id = 	(SELECT TOP 1 item_id FROM ' + @table + ' WHERE kanban_status = 2) 
          WHERE is_done = 1 AND parent_item_id = ' + CAST(@parent_item_id AS NVARCHAR)
          EXEC (@sql)
        
          SET @sql = 'UPDATE task_status SET status_list_item_id = 
          (SELECT TOP 1 item_id FROM ' + @table + ' WHERE kanban_status = 1) 
          WHERE is_done = 0 AND parent_item_id = ' + CAST(@parent_item_id AS NVARCHAR)
          EXEC (@sql)
        
          SET @sql = 'UPDATE task_status SET status_list_item_id = 
          (SELECT TOP 1 item_id FROM ' + @table + ' WHERE kanban_status = 0) 
          WHERE status_id = (SELECT TOP 1 status_id FROM task_status WHERE parent_item_id = ' + CAST(@parent_item_id AS NVARCHAR) + ' ORDER BY order_no ASC)'
          EXEC (@sql)
        
          FETCH NEXT FROM @GetTables INTO @parent_item_id
        END
        CLOSE @GetTables
        DEALLOCATE @GetTables
FETCH NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor