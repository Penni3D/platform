﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[processes]') AND name = 'system_process') 
    ALTER TABLE processes
          ADD system_process  tinyint NOT NULL
          CONSTRAINT process_system_process_df DEFAULT 0