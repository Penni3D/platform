﻿EXEC app_set_archive_user_id 0
DECLARE @GetTables CURSOR
DECLARE @tableName NVARCHAR(200)
DECLARE @archiveTableName NVARCHAR(200)
DECLARE @instance NVARCHAR (10)
DECLARE @process NVARCHAR(50) = 'risks'
DECLARE @triggerName NVARCHAR(200)
DECLARE @c1 INT = 0 
DECLARE @p1 INT = 0 
DECLARE @ic1 INT = 0
DECLARE @c1_description_order_no NVARCHAR(MAX)
DECLARE @p1_description_order_no NVARCHAR(MAX)
 
 --Get Tables for risks process
 SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
 SELECT instance FROM processes WHERE process = @process 
 
 OPEN @GetTables
 FETCH NEXT FROM @GetTables INTO @instance

 WHILE (@@FETCH_STATUS=0) BEGIN 
 IF (@instance <> 'waltham') --Waltham instance has been fixed prior to this migration already
 BEGIN
     SELECT TOP 1 @c1=process_container_id FROM process_containers WHERE instance = @instance AND process = @process

	 SELECT TOP 1 @p1=process_portfolio_id FROM process_portfolios WHERE instance = @instance AND process = @process AND process_portfolio_type IN (1,10)

	 SELECT TOP 1 @ic1=item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = 'title'

	 SELECT TOP 1 @c1_description_order_no=order_no FROM process_container_columns pcc 
	 INNER JOIN item_columns ic ON ic.item_column_id = pcc.item_column_id WHERE ic.instance = @instance AND ic.process = @process AND ic.name = 'description'
	 
	 SELECT TOP 1 @p1_description_order_no=order_no FROM process_portfolio_columns ppc 
	 INNER JOIN item_columns ic ON ic.item_column_id = ppc.item_column_id WHERE ic.instance = @instance AND ic.process = @process AND ic.name = 'description'
	 
	 INSERT INTO process_container_columns (process_container_id, item_column_id) VALUES (@c1, @ic1);
	 SET @triggerName = '_' + @instance + '_' + @process + '_AFTER_UPDATE'
	 SET @tableName = '_' + @instance + '_' + @process
	 SET @archiveTableName = 'archive_' + @tableName

	 EXEC('DISABLE TRIGGER ' + @triggerName + ' ON ' + @tableName)
	 EXEC('UPDATE ' + @tableName + ' SET title = description')
	 EXEC('ENABLE TRIGGER ' + @triggerName + ' ON ' + @tableName)
	 EXEC('UPDATE ' + @archiveTableName + ' SET title = description')

	 UPDATE process_portfolios SET process_portfolio_type = 0 WHERE process_portfolio_id = @p1 --Change from selector type to portfolio type
	 
	 INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@ic1,1) --Put title into portfolio columns
	 
	 --remove description-field from portfolio. It has been replaced with title. description data and archive data has been updated to title
	 DELETE FROM process_portfolio_columns WHERE process_portfolio_id = @p1 
	 AND item_column_id = (SELECT TOP 1 item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = 'description')
	 
	 --remove description-field from container. It has been replaced with title. description data and archive data has been updated to title
	 DELETE FROM process_container_columns WHERE process_container_id = @c1 
	 AND item_column_id = (SELECT TOP 1 item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = 'description')
	 
	 --Set title order_no to previous of minimum in portfolio
	 UPDATE process_portfolio_columns
	 SET order_no = @p1_description_order_no
	 WHERE process_portfolio_id = @p1 AND item_column_id = @ic1

	 --Set title order_no to previous of minimum in container
	 UPDATE process_container_columns
	 SET order_no = @c1_description_order_no
	 WHERE process_container_id = @c1 AND item_column_id = @ic1

	 END
  FETCH NEXT FROM @GetTables INTO @instance
 END
 CLOSE @GetTables