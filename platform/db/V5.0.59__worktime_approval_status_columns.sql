EXEC sp_rename 'WORKTIME_TRACKING_HOURS.status', 'line_manager_status'
EXEC sp_rename 'WORKTIME_TRACKING_HOURS.approved_by', 'line_manager_user_id'
ALTER TABLE WORKTIME_TRACKING_HOURS
  ADD project_manager_status int, project_manager_user_id int