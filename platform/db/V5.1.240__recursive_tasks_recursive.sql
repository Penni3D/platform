
	DECLARE @sql NVARCHAR(MAX);
	DECLARE @instance NVARCHAR(MAX);
	DECLARE @GetInstances CURSOR

	--Get Tables
	SET @GetInstances = CURSOR FAST_FORWARD LOCAL FOR 
	select instance from processes where process = 'task'

	OPEN @GetInstances
	FETCH NEXT FROM @GetInstances INTO @instance

	WHILE (@@FETCH_STATUS=0) BEGIN
		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[get_' + @instance + '_recursive_tasks_up]')) BEGIN			
			SET @sql = 'DROP function  [dbo].[get_' + @instance + '_recursive_tasks_up]'
			EXEC (@sql)
		END

		SET @sql = 'create FUNCTION  [dbo].[get_' + @instance + '_recursive_tasks_up](@itemId int )
		RETURNS @rtab TABLE ([item_id] int NOT NULL, level int NOT NULL )
		AS
		BEGIN	
				WITH rec AS (
			             SELECT t.*, 0 as level
			             FROM _' + @instance + '_task t
			             WHERE t.item_id = @itemId
			             UNION ALL 
			             SELECT t.*, level + 1 as level
			             FROM _' + @instance + '_task t 
			             INNER JOIN rec r ON r.parent_item_id = t.item_id) 

			INSERT @rtab
				
			             SELECT item_id, level FROM rec
			RETURN
		END' 

		exec (@sql)
		FETCH NEXT FROM @GetInstances INTO @instance
	END
	CLOSE @GetInstances