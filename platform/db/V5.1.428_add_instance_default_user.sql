ALTER TABLE instances
ADD instance_default_user INT;

ALTER TABLE instance_logs
ADD background_log_id NVARCHAR(MAX);
