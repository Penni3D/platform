SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[notification_VariableIdReplace]
(
	@variable NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @item_id INT
	DECLARE @column_item_id INT

	IF @variable IS NOT NULL
		BEGIN
			SET @item_id = (SELECT TOP 1 value FROM (SELECT TOP 2 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '_') ORDER BY order_no DESC) AS tmp ORDER BY order_no ASC)
			SET @column_item_id = (SELECT value FROM (SELECT TOP 1 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '_') ORDER BY order_no DESC) AS tmp)
			SET @variable = STUFF(@variable, CHARINDEX( '_' + CAST(@item_id AS NVARCHAR) + '_', @variable), LEN(CAST(@item_id AS NVARCHAR)) + 1, '|' + CAST(dbo.item_GetGuid(@item_id) AS NVARCHAR(MAX)))
			SET @variable = STUFF(@variable, CHARINDEX('_' + CAST(@column_item_id AS NVARCHAR), @variable), LEN(CAST(@column_item_id AS NVARCHAR)) + 1, '|' + dbo.itemColumn_GetVariable(@column_item_id))
		END
	RETURN @variable
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[notification_VariableVarReplace]
(
	@instance NVARCHAR(MAX),
	@variable NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @item_guid NVARCHAR(MAX)
	DECLARE @column_item_variable NVARCHAR(MAX)

	IF @variable IS NOT NULL
		BEGIN
			SET @item_guid = (SELECT TOP 1 value FROM (SELECT TOP 2 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '|') ORDER BY order_no DESC) AS tmp ORDER BY order_no ASC)
			SET @column_item_variable = (SELECT value FROM (SELECT TOP 1 ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS order_no, value FROM STRING_SPLIT(@variable, '|') ORDER BY order_no DESC) AS tmp)
			SET @variable = STUFF(@variable, CHARINDEX( '|' + @item_guid + '|', @variable), LEN(@item_guid) + 1, '_' + CAST(dbo.item_GetId(CAST(@item_guid AS UNIQUEIDENTIFIER)) AS NVARCHAR(MAX)))
			SET @variable = STUFF(@variable, CHARINDEX('|' + @column_item_variable, @variable), LEN(@column_item_variable) + 1, '_' + CAST(dbo.itemColumn_GetId(@instance, @column_item_variable) AS NVARCHAR(MAX)))
		END
	RETURN @variable
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processPortfolio_DownloadOptionsVarReplace]
(
	@instance NVARCHAR(MAX),
	@downloadOptions NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found NVARCHAR(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @reverseStartPos INT
	DECLARE @len INT

	IF @downloadOptions IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('":{', 0), --ItemColumnId 
				('PortfolioId', 1)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @downloadOptions)
					WHILE @startPos > 0
						BEGIN  
							IF @type = 0
								BEGIN  
									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @reverseStartPos = LEN(@downloadOptions) - @startPos + 2

									SET @endPos1 = CHARINDEX('"', @downloadOptions, @reverseStartPos)
									SET @endPos2 = CHARINDEX('{', @downloadOptions, @reverseStartPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos1 - @reverseStartPos + 4)
											SET @len = @endPos1 - @reverseStartPos
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos2 - @reverseStartPos + 4)
											SET @len = @endPos2 - @reverseStartPos
										END

									SET @var3 = SUBSTRING(@var2, 4, LEN(@var2) - 4)
				
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.itemColumn_GetId(@instance, REVERSE(@var3))	
											IF @var4 IS NULL
												BEGIN
													SET @var4 = '0'
												END
											SET @downloadOptions = STUFF(@downloadOptions, @reverseStartPos, @len, REVERSE(@var4))
										END

									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @startPos = LEN(@downloadOptions) - @reverseStartPos + 2
								END

							IF @type = 1
								BEGIN  
									SET @endPos1 = CHARINDEX(',', @downloadOptions, @startPos)
									SET @endPos2 = CHARINDEX('}', @downloadOptions, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos1 - @startPos)
											SET @len = @endPos1 - @startPos - LEN(@strToFound) - 2
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos2 - @startPos)
											SET @len = @endPos2 - @startPos - LEN(@strToFound) - 2
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)

									SET @startPos = @startPos + LEN(@strToFound) + 2

									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
											IF @var4 IS NULL
												BEGIN
													SET @var4 = '0'
												END
											SET @downloadOptions = STUFF(@downloadOptions, @startPos, @len, @var4)
										END
								END

							SET @startPos = CHARINDEX(@strToFound, @downloadOptions, @startPos + 1)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @downloadOptions
END