IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_costsKTN'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_costsKTN] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_costsKTN]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'costsKTN'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2
	
	EXEC dbo.app_ensure_column @instance,@process,'idea_id', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'personel_costs', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'travel_sundries', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'events', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'external_services', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'ict', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'comms', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'overheads', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'type', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'year', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'month', 0, 1, 1, 0, 0, NULL, 0, 0	
END
GO

