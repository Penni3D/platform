ALTER TABLE instance_menu
DROP COLUMN page

EXEC app_remove_archive 'instance_menu'
EXEC app_ensure_archive 'instance_menu'