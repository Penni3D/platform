DECLARE @Instance NVARCHAR(255);
DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances 
OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN 
        DECLARE @sql NVARCHAR(MAX)
        SET @sql = 'ALTER TABLE _' + @instance + '_worktime_tracking_hours ALTER COLUMN line_manager_status FLOAT'
	    EXECUTE SP_EXECUTESQL @sql

        SET @sql = 'ALTER TABLE _' + @instance + '_worktime_tracking_hours ALTER COLUMN project_manager_status FLOAT'
        EXECUTE SP_EXECUTESQL @sql

        SET @sql = 'ALTER TABLE archive__' + @instance + '_worktime_tracking_hours ALTER COLUMN line_manager_status FLOAT'
        EXECUTE SP_EXECUTESQL @sql

        SET @sql = 'ALTER TABLE archive__' + @instance + '_worktime_tracking_hours ALTER COLUMN project_manager_status FLOAT'
        EXECUTE SP_EXECUTESQL @sql

        UPDATE item_columns SET data_type = 2 
        WHERE (name = 'line_manager_status' OR name = 'project_manager_status') AND process = 'worktime_tracking_hours' AND instance = @instance
        
        FETCH NEXT FROM cursor_process INTO @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
	