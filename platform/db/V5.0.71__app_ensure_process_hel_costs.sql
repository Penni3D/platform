IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_hel_costs'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_hel_costs] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_hel_costs]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	exec app_set_archive_user_id 0
	declare @process nvarchar(50) = 'hel_costs'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	----Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create owner item id for row manager
	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 1, 1, 1, 0, null, 0, 0, 0

	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'type', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
	EXEC app_ensure_column @instance, @process, 'status', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
	EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
	EXEC app_ensure_column @instance, @process, 'amount', 0, 2, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
    EXEC app_ensure_column @instance, @process, 'order_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 

	update item_columns set status_column = 1 where instance = @instance and process = @process
    
    --Create portfolios
    DECLARE @p1 INT
    EXEC app_ensure_portfolio @instance,@process,'DATA', @portfolio_id = @p1 OUTPUT
    update process_portfolios set process_portfolio_type = 1 where instance = @instance and process = @process --Entity selector
    
    --Create conditions
    DECLARE @cId INT	
    EXEC app_ensure_condition @instance, @process, 0, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT
    INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
    INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
    INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')
    
    --INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@cId,  @p1)
    
    INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) 
    SELECT @p1, item_column_id FROM item_columns WHERE instance = @instance AND process = @process
    
    set @process = 'hel_cost_estimates'
    
    	----Create process
    	EXEC app_ensure_process @instance, @process, @process, 1
    
    	--Create owner item id for row manager
    	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 1, 1, 1, 0, null, 0, 0, 0
    
    	--Create columns
       	EXEC app_ensure_column @instance, @process, 'year', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
    	EXEC app_ensure_column @instance, @process, 'month', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
    	EXEC app_ensure_column @instance, @process, 'amount', 0, 2, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT 
    	EXEC app_ensure_column @instance, @process, 'type', 0, 1, 1, 1, 0, null,  0, 0
    	  
    	update item_columns set status_column = 1 where instance = @instance and process = @process
        
        --Create portfolios
        EXEC app_ensure_portfolio @instance,@process,'DATA', @portfolio_id = @p1 OUTPUT
        update process_portfolios set process_portfolio_type = 1 where instance = @instance and process = @process --Entity selector
        
        --Create conditions
        EXEC app_ensure_condition @instance, @process, 0, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT
        INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
        INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
        INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')
        
        --INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@cId,  @p1)
        
        INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) 
        SELECT @p1, item_column_id FROM item_columns WHERE instance = @instance AND process = @process
END;
