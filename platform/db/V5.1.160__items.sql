ALTER TABLE items
    ADD guid UNIQUEIDENTIFIER DEFAULT newsequentialid() NOT null


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[conditions_replace]
(
@sourceInstance nvarchar(10)
,@targetInstance nvarchar(10)
)
AS
BEGIN
	DECLARE @condition_id INT
	DECLARE @instance nvarchar(10)
	DECLARE @process nvarchar(50)
	DECLARE @VARIABLE nvarchar(255)
	DECLARE @condition_json nvarchar(max)

	DECLARE @conditionCursor CURSOR 
	SET @conditionCursor = CURSOR  FAST_FORWARD LOCAL FOR 
	SELECT condition_id, instance, process, variable, condition_json FROM conditions
	-- WHERE instance IN (@sourceInstance, @targetInstance)
	WHERE instance = @sourceInstance

	OPEN @conditionCursor
	FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	WHILE (@@FETCH_STATUS=0) BEGIN
		declare @json as nvarchar(max)
		declare @json2 as nvarchar(max)
		exec dbo.condition_IdReplaceProc @condition_json,  @replaced_json = @json output
		exec dbo.condition_VarReplaceProc @sourceInstance, @sourceInstance, @json,  @replaced_json = @json2 output
		--print '@json1'
	--	print @json
		--print '@json2'
	--	print @json2
		insert into #conditions (condition_id, instance, process, variable, condition_json, replaced_condition_json) values (@condition_id, @targetInstance, @process, @variable, @json, @json2)
		FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	END
END
GO


delete notification_receivers where notification_condition_id not in (select notification_condition_id from notification_conditions)


ALTER TABLE [dbo].[notification_receivers]  WITH CHECK ADD  CONSTRAINT [FK_notification_condition] FOREIGN KEY([notification_condition_id])
REFERENCES [dbo].[notification_conditions] ([notification_condition_id])
ON DELETE CASCADE