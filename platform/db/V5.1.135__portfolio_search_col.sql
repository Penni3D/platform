﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[process_portfolio_columns]') AND name = 'use_in_search') 
    ALTER TABLE process_portfolio_columns
          ADD use_in_search tinyint NOT NULL
          CONSTRAINT ppc_use_in_search_df DEFAULT 1
    WITH VALUES
