ALTER TABLE dbo.process_action_rows ADD
	user_item_column_id int null

    ALTER TABLE [dbo].[process_action_rows]  WITH CHECK ADD  CONSTRAINT [FK_process_action_rows_user_item_columns] FOREIGN KEY([user_item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
ON UPDATE NO ACTION
ON DELETE NO ACTION
