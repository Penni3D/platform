ALTER TABLE process_portfolios
    ADD timemachine TINYINT DEFAULT(0)

ALTER TABLE archive_process_portfolios
    ADD timemachine TINYINT DEFAULT(0)
GO
EXEC app_set_archive_user_id 0
UPDATE process_portfolios SET timemachine = 0