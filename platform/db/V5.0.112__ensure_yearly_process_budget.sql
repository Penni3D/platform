
-- YEARLY BUDGET PROCESSES

-- CATEGORIES FOR YEARLY BUDGET 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_list_yearly_budget_category'))
  exec('CREATE PROCEDURE [dbo].[app_ensure_process_list_yearly_budget_category] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_list_yearly_budget_category]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'list_yearly_budget_category'
    DECLARE @i1 INT
    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_list @instance, @process, @process, 0

    EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0, @item_column_id=@i1 output
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
  END

GO

-- YEARLY BUDGET ROW
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_yearly_budget_row'))
  exec('CREATE PROCEDURE [dbo].[app_ensure_process_yearly_budget_row] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_yearly_budget_row]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'yearly_budget_row'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_process @instance, @process, @process, 2

    EXEC app_ensure_column @instance, @process, 'project_item_id', 0, 1, 1, 1, 0, null,  0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'description', 0, 4, 1, 1, 0, NULL, 0, 0
    EXEC app_ensure_column @instance, @process, 'category_item_id', 0, 5, 1, 1, 0, null,  0, 0, 'list_budget_category'
  END

GO

-- EXECUTE PROCEDURE FOR ALL INSTANCES
BEGIN
  DECLARE @instanceName NVARCHAR(10)
  DECLARE Budget_Cursor CURSOR FOR SELECT instance FROM instances
  DECLARE @tableName NVARCHAR(50) = ''
  DECLARE @sql NVARCHAR(MAX) = ''
  DECLARE @table NVARCHAR(MAX) = ''
  
  OPEN Budget_Cursor

  FETCH NEXT FROM Budget_Cursor INTO @instanceName

  WHILE @@fetch_status = 0
    BEGIN
      DECLARE @itemColumnId INT
      declare @translationRef NVARCHAR(50)
      
      EXEC app_ensure_process_list_yearly_budget_category @instanceName
      EXEC app_ensure_process_yearly_budget_row @instanceName
      
      SET @table = 'list_yearly_budget_category'
      
      UPDATE item_columns SET use_translation=1 WHERE instance=@instanceName and process=@table and name='list_item'

      DECLARE @itemId INT
          
      SET @tableName = '_' + @instanceName + '_' + @table
      
      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      set @itemId = @@identity;
      select @itemColumnId=item_column_id FROM item_columns where instance=@instanceName and process=@table and name='list_item'
      set @translationRef = CAST(@itemId AS nvarchar) + '_' + CAST(@itemColumnId as NVARCHAR)
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''' + @translationRef + ''' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      exec (@sql)
      SET @sql = 'INSERT INTO process_translations (instance, process, code, variable, translation) ' +
                 'VALUES ('''+ @instanceName +''', ''' + @table + ''', ''en-GB'', ''' + @translationRef + ''', ''Investments'')'
      exec (@sql)


      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      SET @itemId = @@identity;
      select @itemColumnId=item_column_id FROM item_columns where instance=@instanceName and process=@table and name='list_item'
      set @translationRef = CAST(@itemId AS nvarchar) + '_' + CAST(@itemColumnId as NVARCHAR)
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''' + @translationRef + ''' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      exec (@sql)
      SET @sql = 'INSERT INTO process_translations (instance, process, code, variable, translation) ' +
                 'VALUES ('''+ @instanceName +''', ''' + @table + ''', ''en-GB'', ''' + @translationRef + ''', ''Yearly Expenses'')'
      exec (@sql)

      INSERT INTO items (instance, process) VALUES (@instanceName, @table);
      SET @itemId = @@identity;
      select @itemColumnId=item_column_id FROM item_columns where instance=@instanceName and process=@table and name='list_item'
      set @translationRef = CAST(@itemId AS nvarchar) + '_' + CAST(@itemColumnId as NVARCHAR)
      SET @sql = 'UPDATE  ' + @tableName + ' SET list_item =''' + @translationRef + ''' WHERE item_id=' +  CAST(@itemId AS nvarchar)
      exec (@sql)
      SET @sql = 'INSERT INTO process_translations (instance, process, code, variable, translation) ' +
                 'VALUES ('''+ @instanceName +''', ''' + @table + ''', ''en-GB'', ''' + @translationRef + ''', ''Profits and Savings'')'
      exec (@sql)
      
      FETCH  NEXT FROM Budget_Cursor INTO
        @instanceName
    END
  CLOSE Budget_Cursor
  DEALLOCATE Budget_Cursor
END



