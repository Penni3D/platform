EXEC app_set_archive_user_id 0

DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
	DECLARE @itemColumnId INT
	DECLARE @conditionId INT;
	SELECT @itemColumnId = item_column_id FROM item_columns WHERE instance = @instance AND process = 'process_comments' AND name = 'sender'
	DECLARE @conditionJson NVARCHAR(MAX) = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":3,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@itemColumnId AS NVARCHAR(MAX)) + ',"UserColumnId":null,"Value":"","UserColumnTypeId":2},"OperatorId":null}]},"OperatorId":1}]'

	EXEC app_ensure_condition @instance, 'process_comments', 0, 0, 'PROCESS_COMMENTS_DELETE', @conditionJson, @condition_id = @conditionId OUTPUT

	INSERT INTO condition_states(condition_id, state) VALUES (@conditionId, 'delete')

    FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor