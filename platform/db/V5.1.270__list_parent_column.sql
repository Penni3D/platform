GO
	DECLARE @instance nvarchar(10)
	DECLARE @process nvarchar(50)
	--DECLARE @update_itemid int
	DECLARE @sql nvarchar(max)
		DECLARE listCursor CURSOR FOR SELECT process, instance FROM processes WHERE list_process = 1
		OPEN listCursor
		FETCH NEXT FROM listCursor INTO @process, @instance
			WHILE @@fetch_status = 0 BEGIN
				EXEC app_ensure_column @instance,@process,'parent_item_id',0,1,0,0,0,'',0,0,'','',0,0
				EXEC app_ensure_column @instance,@process,'system_item',0,1,0,0,0,'',0,0,'','',0,0
				EXEC app_ensure_column @instance,@process,'in_use',0,1,0,0,0,'',0,0,'','',0,0
				DECLARE @table NVARCHAR(100) = '_' + @instance + '_' + @process
				FETCH NEXT FROM listCursor INTO @process, @instance
			END
		CLOSE listCursor
		DEALLOCATE listCursor



