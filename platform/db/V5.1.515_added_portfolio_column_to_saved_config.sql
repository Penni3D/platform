SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExcludeNewColumnInSavedFilters]
	@process_portfolio_id INT,
	@item_column_id INT
AS
BEGIN
	DECLARE @value NVARCHAR(MAX)
	DECLARE @user_configuration_id INT
	DECLARE @start_pos INT
	DECLARE @end_pos INT
	DECLARE @name NVARCHAR(50)
	DECLARE @cursor CURSOR

	SELECT @name = [name] FROM item_columns WHERE item_column_id = @item_column_id

	SET @cursor = CURSOR FAST_FORWARD LOCAL FOR
		SELECT [value], user_configuration_id
		FROM instance_user_configurations_saved
		WHERE identifier = CAST(@process_portfolio_id AS NVARCHAR)
		AND [group] = 'portfolios'

	OPEN @cursor
	FETCH NEXT FROM @cursor INTO @value, @user_configuration_id
	WHILE @@fetch_status = 0 BEGIN
		SET @start_pos = CHARINDEX('"columns"', @value) + 11;
		SET @end_pos = CHARINDEX(']', @value, @start_pos);

		IF @end_pos = @start_pos BEGIN
			SET @value = STUFF(@value, @end_pos, 0, '"' + @name + '"')
		END
		ELSE BEGIN
			SET @value = STUFF(@value, @end_pos, 0, ',"' + @name + '"')
		END

		UPDATE instance_user_configurations_saved SET [value] = @value WHERE user_configuration_id = @user_configuration_id
	
		FETCH NEXT FROM @cursor INTO @value, @user_configuration_id
	END
	CLOSE @cursor
	DEALLOCATE @cursor
END