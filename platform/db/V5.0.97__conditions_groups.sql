
CREATE TABLE [dbo].[conditions_groups]( 
  [condition_group_id] [int] IDENTITY(1,1) NOT NULL, 
  [instance] [nvarchar](10) NOT NULL, 
  [process] [nvarchar](50) NOT NULL, 
  [variable] [nvarchar](255) NULL, 
  [order_no] [nvarchar](MAX) NULL, 
 CONSTRAINT [PK_conditions_groups] PRIMARY KEY CLUSTERED  
( 
  [condition_group_id] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
GO
 

--DROP TABLE conditions_groups_condition
CREATE TABLE [dbo].[conditions_groups_condition]( 
  [condition_group_id] [int] NOT NULL, 
  [condition_id] [int] NOT NULL)
GO
