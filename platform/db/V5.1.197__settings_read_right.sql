EXEC app_set_archive_user_id 0

SET NOCOUNT ON;

	DECLARE @instance NVARCHAR(MAX);
	DECLARE @GetTables CURSOR
	
	DECLARE @newId INT 
	
	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT instance FROM instances
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @instance

DECLARE @cnt INT = 0;



	WHILE (@@FETCH_STATUS=0) BEGIN
	  INSERT INTO instance_menu (instance, variable, default_state, params, link_type) VALUES (@instance, 'instancemenu_settings', 'settings', '{}', 3)
	  
	  SELECT @newId = @@IDENTITY
	  
	  INSERT INTO instance_menu_rights (instance_menu_id, item_id) SELECT @newId, item_id FROM items where instance = @instance AND process = 'user_group'
	  INSERT INTO instance_menu_states(instance_menu_id, item_id, state) SELECT @newId, item_id, 'read' FROM items where instance = @instance AND process = 'user_group'

		FETCH NEXT FROM @GetTables INTO @instance
	END
	CLOSE @GetTables