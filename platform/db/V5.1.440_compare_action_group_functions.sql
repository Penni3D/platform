SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processActionRowsGroups_GetVariable]
(
	@processActionGroupId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	SELECT TOP 1 @var = variable FROM process_action_rows_groups WHERE process_action_group_id = @processActionGroupId
	RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processActionRowsGroups_GetId]
(
	@instance NVARCHAR(10),
	@variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
	DECLARE @id int
	SELECT TOP 1 @id = process_action_group_id FROM process_action_rows_groups WHERE instance = @instance AND variable = @variable
	RETURN @id
END