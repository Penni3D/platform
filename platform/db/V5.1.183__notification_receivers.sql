ALTER TABLE notification_receivers ALTER COLUMN notification_condition_id INT NULL 
GO 

ALTER TABLE notification_receivers ADD  process_action_row_id INT null
GO 

ALTER TABLE [dbo].[notification_receivers]  WITH CHECK ADD  CONSTRAINT [FK_nc_action_row] FOREIGN KEY([process_action_row_id])
REFERENCES [dbo].[process_action_rows] ([process_action_row_id])
GO 

ALTER TABLE notification_receivers ADD  user_group_item_id INT null
GO 

ALTER TABLE [dbo].[notification_receivers]  WITH CHECK ADD  CONSTRAINT [FK_nc_user_group] FOREIGN KEY([user_group_item_id])
REFERENCES [dbo].[items] ([item_id])
GO 

ALTER TABLE notification_receivers ALTER COLUMN  item_column_id INT null
GO

ALTER TABLE archive_notification_receivers ALTER COLUMN  notification_condition_id INT null
GO

ALTER TABLE archive_notification_receivers ALTER COLUMN  item_column_id INT null
GO

exec app_ensure_archive 'notification_receivers'
GO 


ALTER TABLE process_actions ADD  condition_id INT null
GO 


ALTER TABLE [dbo].[process_actions]  WITH CHECK ADD  CONSTRAINT [FK_pa_condition] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
GO 

exec app_ensure_archive 'process_actions'
GO 

exec app_set_archive_user_id 0
DECLARE @id int
DECLARE @value nvarchar(max)

DECLARE idsCursor CURSOR FOR 
select process_action_row_id, replace(replace(value2, ']' , '') , '[', '') from process_action_rows where process_action_type = 5 and value2 is not null and value2 <> '' and value2 <> '[]' 

OPEN idsCursor

FETCH NEXT FROM idsCursor INTO @id, @value

WHILE @@fetch_status = 0
  BEGIN
		DECLARE @id2 int

		DECLARE idsCursor2 CURSOR FOR 
		select item from dbo.DelimitedStringToTable(@value, ',')		
		OPEN idsCursor2
		
		FETCH NEXT FROM idsCursor2 INTO @id2
		
		WHILE @@fetch_status = 0
		  BEGIN
				insert into notification_receivers (process_action_row_id, item_column_id) values (@id,@id2 )		 
		    FETCH  NEXT FROM idsCursor2 INTO @id2
		  END
		CLOSE idsCursor2
		DEALLOCATE idsCursor2
		
    FETCH  NEXT FROM idsCursor INTO @id, @value
  END
CLOSE idsCursor
DEALLOCATE idsCursor