DECLARE @Instance NVARCHAR(255);
DECLARE @AccessContainerId INT;
DECLARE @Ic INT;
DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
	  IF COL_LENGTH('_' + @Instance + '_task_link_type', 'leadtime') IS NULL
	  BEGIN
	  EXEC app_ensure_column @Instance, 'task_link_type', 'leadtime', 0, 1, 0, 0, null, null, 0, null, null
	  SET @Ic = (SELECT item_column_id FROM item_columns where [name] = 'leadtime' AND process = 'task_link_type')

	  SET @AccessContainerId = (select top 1 process_container_id from process_containers
	  where process = 'task_link_type' AND right_container = 1)

	  INSERT INTO process_container_columns (process_container_id, item_column_id) VALUES (@AccessContainerId, @Ic)

	  END
		
        FETCH NEXT FROM cursor_process INTO 
            @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;


