DECLARE @Instance NVARCHAR(255);
DECLARE cursor_process CURSOR
    FOR SELECT instance FROM instances
OPEN cursor_process
FETCH NEXT FROM cursor_process INTO @Instance
WHILE @@FETCH_STATUS = 0
    BEGIN
        EXEC app_ensure_column @instance, 'user_group_type', 'is_admin_group', 0, 1, 1, 1, 0, null, 0, 0, 0
        DECLARE @sql NVARCHAR(MAX) = 'UPDATE _' + @instance + '_user_group_type SET is_admin_group = 1 WHERE list_item = ''admin'''
        EXECUTE SP_EXECUTESQL @sql
        
        FETCH NEXT FROM cursor_process INTO @Instance
    END;
CLOSE cursor_process;
DEALLOCATE cursor_process;