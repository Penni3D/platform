EXEC dbo.items_AddColumn processes, compare, 1
GO
ALTER TABLE processes ADD CONSTRAINT DF_processes_compare DEFAULT 0 FOR compare  
GO
UPDATE processes SET compare = 0
GO
UPDATE processes SET compare = 2 WHERE process IN ('list_admin_types', 'list_azure_ad_groups', 'user_group_type')




GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processSubprocesses_GetParentsForOrder]
(
	@process nvarchar(50)
)
RETURNS nvarchar(MAX)
AS
BEGIN
	DECLARE @result nvarchar(MAX)
	DECLARE @parent_result nvarchar(MAX)
	DECLARE @parent_process nvarchar(50)
	SET @result = @process
	SELECT @parent_process = parent_process FROM process_subprocesses WHERE process = @process
	IF @parent_process IS NOT NULL	BEGIN
		SET @parent_result = dbo.processSubprocesses_GetParentsForOrder(@parent_process)
		IF @parent_result <> '' BEGIN
			SET @result = @parent_result + '_' + @result
		END
	END

	RETURN @result
END