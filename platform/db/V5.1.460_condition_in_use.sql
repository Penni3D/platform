ALTER TABLE item_columns
    ADD in_conditions TINYINT DEFAULT(0)
GO
UPDATE item_columns
SET in_conditions = 
    (SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM conditions c WHERE c.process = item_columns.process AND condition_json LIKE '%:' + CAST(item_columns.item_column_id AS NVARCHAR) + '%')