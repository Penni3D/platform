IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[item_columns]') AND name = 'use_translation') 
    ALTER TABLE item_columns
          ADD use_translation tinyint NOT NULL
          CONSTRAINT ic_use_translation_df DEFAULT 0
    WITH VALUES
