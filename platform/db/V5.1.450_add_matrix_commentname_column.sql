DECLARE @ProcessName NVARCHAR(255);
DECLARE @Instance NVARCHAR(255);
DECLARE @item_column_id INT;
DECLARE @tooltipColumn INT;
DECLARE cursor_process CURSOR
FOR SELECT process,instance FROM processes WHERE process_type = 4 
OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @ProcessName,@Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
	 EXEC app_ensure_column @Instance, @ProcessName, 'comment_name', 0,24,0,0,null,0,0,null,null
	  
	  SELECT @item_column_id = (SELECT TOP 1 item_column_id FROM item_columns WHERE process = @ProcessName ORDER BY item_column_id DESC)
	  
	  UPDATE item_columns SET status_column = 1 where item_column_id = @item_column_id
	  
	  SELECT @tooltipColumn = (SELECT item_column_id FROM item_columns WHERE process = @ProcessName AND name = 'tooltip')
	  
	  UPDATE item_columns SET status_column = 1 where item_column_id = @tooltipColumn
		
		
        FETCH NEXT FROM cursor_process INTO 
            @ProcessName, @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
