--Archive user is 0
DECLARE @BinVar varbinary(128);
SET @BinVar = CONVERT(varbinary(128), 0);
SET CONTEXT_INFO @BinVar;

DECLARE @process NVARCHAR(50) = 'flex_time_tracking'

-- Apply changes for all instances that have this process
DECLARE @instanceName NVARCHAR(10)
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instanceName
WHILE @@fetch_status = 0 BEGIN
	DECLARE @cId INT

	SELECT @cId = process_container_id FROM process_containers WHERE instance = @instanceName AND process = @process

	DECLARE @icId INT

	EXEC dbo.app_ensure_column @instanceName, @process, 'flex_user', @cId, 5, 0, 0, NULL, 0, 0, 'user', NULL, 0, @item_column_id = @icId OUTPUT
		
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) SELECT item_id, @ic_Id, user_item_id FROM _' + @instanceName + '_flex_time_tracking'
	EXECUTE sp_executesql @sql, N'@ic_Id INT', @ic_Id = @icId

	FETCH  NEXT FROM instance_cursor INTO @instanceName
END
CLOSE instance_cursor
DEALLOCATE instance_cursor



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.flex_time_CalculateMonthBalance
(
	@instance NVARCHAR(MAX),
	@user_item_id INT,
	@month_date DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @item_id INT
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @balance FLOAT
	DECLARE @required_hours FLOAT
	DECLARE @work_hours FLOAT
	DECLARE @begin_date DATETIME
	DECLARE @year INT
	DECLARE @month INT

	SET @year = YEAR(@month_date)
	SET @month = MONTH(@month_date)

	SET @sql = 'SELECT @begin_date = MIN(start_date) FROM _' + @instance + '_flex_time_tracking WHERE type IN (0, 1) AND special IN (0, 1) AND user_item_id = ' + CAST(@user_item_id AS NVARCHAR)
	EXECUTE sp_executesql @sql, N'@begin_date DATETIME OUTPUT', @begin_date = @begin_date OUTPUT

	DECLARE @start_date DATETIME
	DECLARE @end_date DATETIME
	DECLARE @notwork INT
	DECLARE @timesheet_hours FLOAT
	SET @required_hours = 0
	SET @start_date = CAST(@year AS NVARCHAR) + '-' + CAST(@month AS NVARCHAR) + '-1'
	SET @end_date = DATEADD(MONTH, 1, @start_date)
	IF @start_date < @begin_date BEGIN
		SET @start_date = @begin_date
	END

	SET @sql = 'SELECT @work_hours = ISNULL(SUM(work_hours), 0) FROM _' + @instance + '_flex_time_tracking WHERE [type] IN (0, 1) AND special IN (0, 1) AND user_item_id = ' + CAST(@user_item_id AS NVARCHAR) + ' AND [start_date] >= @start_date AND [start_date] < @end_date'
	EXECUTE sp_executesql @sql, N'@start_date DATETIME, @end_date DATETIME, @work_hours FLOAT OUTPUT', @start_date = @start_date, @end_date = @end_date, @work_hours = @work_hours OUTPUT
	
	WHILE @start_date < @end_date BEGIN
		SELECT @notwork = COUNT(*) FROM GetCombinedCalendarForUser(@user_item_id, @start_date, @start_date)
		IF @notwork = 0 BEGIN
			SELECT @timesheet_hours = timesheet_hours FROM GetCalendarForUserByDate(@user_item_id, @start_date)
			SET @required_hours = @required_hours + @timesheet_hours
		END
		SET @start_date = DATEADD(DAY, 1, @start_date)
	END

	SET @balance = @work_hours - @required_hours

	SET @sql = 'SELECT @item_id = item_id FROM _' + @instance + '_flex_time_tracking WHERE [type] = 2 AND user_item_id = ' + CAST(@user_item_id AS NVARCHAR) + ' AND YEAR([start_date]) = ' + CAST(@year AS NVARCHAR) + ' AND MONTH([start_date]) = ' + CAST(@month AS NVARCHAR)
	EXECUTE sp_executesql @sql, N'@item_id INT OUTPUT', @item_id = @item_id OUTPUT
	IF @item_id IS NULL BEGIN
		INSERT INTO items (instance, process) VALUES (@instance, 'flex_time_tracking')
		SET @item_id = @@identity
		
		SET @sql = 'UPDATE _' + @instance + '_flex_time_tracking SET work_hours = ' + CAST(@balance AS NVARCHAR) + ', [type] = 2, user_item_id = ' + CAST(@user_item_id AS NVARCHAR) + ', [start_date] = ''' + CAST(@year AS NVARCHAR) + '-' + CAST(@month AS NVARCHAR) + '-1'' WHERE item_id = ' + CAST(@item_id AS NVARCHAR)
		
		INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES (@item_id, (SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'flex_time_tracking' AND name = 'flex_user'), @user_item_id)
		EXECUTE sp_executesql @sql
	END
	ELSE BEGIN
		SET @sql = 'UPDATE _' + @instance + '_flex_time_tracking SET work_hours = ' + CAST(@balance AS NVARCHAR) + ' WHERE item_id = ' + CAST(@item_id AS NVARCHAR)
		EXECUTE sp_executesql @sql
	END
END