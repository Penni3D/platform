ALTER PROCEDURE Calendar_ApplyToPastWeekends
	@calendar_id INT, @startDate DATE = NULL
AS
BEGIN
	DECLARE @RC INT
	BEGIN TRANSACTION
	DECLARE @endDate DATE = GETDATE();
	DECLARE @useBase INT = 1;

	EXEC @RC = sp_getapplock @Resource='CheckDayOffs', @LockMode='Exclusive', @LockOwner='Transaction', @LockTimeout = 15000
	
	SELECT @useBase = use_base_calendar FROM calendars WHERE calendar_id = @calendar_id;
	IF @useBase = 0
	BEGIN
		IF @startDate IS NULL
		BEGIN
			SELECT @startDate = MIN(event_date) FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL;
		END
		DELETE FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL AND event_date BETWEEN @startDate AND @endDate;
		EXEC Calendar_CheckWeekends @calendar_id, @startDate, @endDate
	END
	ELSE
	BEGIN
		DELETE FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL AND event_date < @endDate;
	END
	
	COMMIT TRANSACTION
END