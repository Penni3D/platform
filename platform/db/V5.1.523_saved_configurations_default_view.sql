EXEC app_set_archive_user_id 1
UPDATE instance_user_configurations_saved
SET portfolio_default_view = -1 
WHERE portfolio_default_view IS NULL AND [group] = 'portfolios'