SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									IF @type = 0 
										BEGIN
											SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
											SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
										END

									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
											SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
										END
				
									IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @endChar NVARCHAR(1)
													DECLARE @listVal NVARCHAR(max)
													DECLARE @listItemId int
													
													SET @endChar = ''
													IF charindex(']', @var3) > 0
														BEGIN
															SET @endChar = ']'
														END

													SET @var3 = replace(@var3, '[', '')
													SET @var3 = replace(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = replace(@var4,  @listVal, CAST(@listItemId AS NVARCHAR(max)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = replace(@dataAdditional2, '@' + @var2, @strToFound + '":[' + @var4 + @endChar)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END

									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END

									IF @type = 2 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)
	
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":"tab_' + @var4 + '"')
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processActionRows_ValueIdReplace]
(
	@item_column_id INT,
	@process_action_type INT,
	@value_type INT,
	@type INT,
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @result NVARCHAR(MAX)
	DECLARE @data_type INT
	DECLARE @item_id INT
	DECLARE @guid uniqueidentifier
	DECLARE @tab_id INT
	DECLARE @variable NVARCHAR(255)
	DECLARE @column_id INT
	DECLARE @column_name NVARCHAR(50)

	SET @result = @value

	IF @process_action_type IN (0, 8) AND @value_type = 0 AND @type = 1 BEGIN
		SELECT @data_type = data_type FROM item_columns WHERE item_column_id = @item_column_id
		IF @data_type IN (5, 6) BEGIN
			IF SUBSTRING(@value, 1, 1) = '[' BEGIN
				IF ISNUMERIC(SUBSTRING(@value, 2, LEN(@value) - 2)) = 1 BEGIN
					SET @item_id = CAST(SUBSTRING(@value, 2, LEN(@value) - 2) AS INT)
					SELECT @guid = guid FROM items WHERE item_id = @item_id
					IF @guid IS NOT NULL BEGIN
						SET @result = @guid
					END
					ELSE BEGIN
						SET @result = '[]'
					END
				END
			END
		END
		
		ELSE BEGIN
			SELECT @column_name = name FROM item_columns WHERE item_column_id = @item_column_id
			IF @column_name = 'current_state' BEGIN
				IF SUBSTRING(@value, 1, 4) = 'tab_' BEGIN
					IF ISNUMERIC(SUBSTRING(@value, 5, LEN(@value) - 4)) = 1 BEGIN
						SET @tab_id = CAST(SUBSTRING(@value, 5, LEN(@value) - 4) AS INT)
						SET @variable = dbo.processTab_GetVariable(@tab_id)
						IF @variable IS NOT NULL BEGIN
							SET @result = @variable
						END
						ELSE BEGIN
							SET @result = ''
						END
					END
				END
			END
		END
	END

	IF @process_action_type = 5	BEGIN
		IF @type = 1 BEGIN
			IF ISNUMERIC(@value) = 1 AND NOT @value = '0' BEGIN 
				SET @item_id = CAST(@value AS INT)
				SELECT @guid = guid FROM items WHERE item_id = @item_id
				IF @guid IS NOT NULL BEGIN
					SET @result = @guid
				END
				ELSE BEGIN
					SET @result = '0'
				END
			END
		END
		
		ELSE IF @type = 2 BEGIN
			IF SUBSTRING(@value, 1, 1) = '[' BEGIN
				IF ISNUMERIC(SUBSTRING(@value, 2, LEN(@value) - 2)) = 1 BEGIN
					SET @column_id = CAST(SUBSTRING(@value, 2, LEN(@value) - 2) AS INT)
					SET @variable = dbo.itemColumn_GetVariable(@column_id)
					IF @variable IS NOT NULL BEGIN
						SET @result = '[' + @variable + ']'
					END
					ELSE BEGIN
						SET @result = 'null'
					END
				END
			END
		END

		ELSE IF @type = 3 BEGIN
			IF SUBSTRING(@value, 1, 12) = 'process.tab_' BEGIN
				IF ISNUMERIC(SUBSTRING(@value, 13, LEN(@value) - 12)) = 1 BEGIN
					SET @tab_id = CAST(SUBSTRING(@value, 13, LEN(@value) - 12) AS INT)
					SET @variable = dbo.processTab_GetVariable(@tab_id)
					IF @variable IS NOT NULL BEGIN
						SET @result = @variable
					END
					ELSE BEGIN
						SET @result = ''
					END
				END
			END
		END
	END

	RETURN @result
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processActionRows_ValueVarReplace]
(
	@instance NVARCHAR(MAX),
	@item_column_id INT,
	@process_action_type INT,
	@value_type INT,
	@type INT,
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @result NVARCHAR(MAX)
	DECLARE @data_type INT
	DECLARE @item_id INT
	DECLARE @guid uniqueidentifier
	DECLARE @tab_id INT
	DECLARE @variable NVARCHAR(255)
	DECLARE @column_id INT
	DECLARE @column_name NVARCHAR(50)

	SET @result = @value

	IF @process_action_type IN (0, 8) AND @value_type = 0 AND @type = 1 BEGIN
		SELECT @data_type = data_type FROM item_columns WHERE item_column_id = @item_column_id
		IF @data_type IN (5, 6) BEGIN
			IF @value IS NOT NULL AND NOT SUBSTRING(@value, 1, 1) = '[' BEGIN
				SET @guid = @value
				SELECT @item_id = item_id FROM items WHERE guid = @guid
				IF @item_id IS NOT NULL BEGIN
					SET @result = '[' + CAST(@item_id AS NVARCHAR) + ']'
				END
				ELSE BEGIN
					SET @result = '[]'
				END
			END
		END

		ELSE BEGIN
			SELECT @column_name = name FROM item_columns WHERE item_column_id = @item_column_id
			IF @column_name = 'current_state' BEGIN
				IF NOT @value = '' BEGIN
					SET @tab_id = dbo.processTab_GetId(@instance, @value)
					IF @tab_id IS NOT NULL BEGIN
						SET @result = 'tab_' + CAST(@tab_id AS NVARCHAR)
					END
					ELSE BEGIN
						SET @result = ''
					END
				END
			END
		END
	END

	IF @process_action_type = 5	BEGIN
		IF @type = 1 BEGIN
			IF @value IS NOT NULL AND NOT @value = '0' BEGIN
				SET @guid = @value
				SELECT @item_id = item_id FROM items WHERE guid = @guid
				IF @item_id IS NOT NULL BEGIN
					SET @result = CAST(@item_id AS NVARCHAR)
				END
				ELSE BEGIN
					SET @result = '0'
				END
			END
		END
		
		ELSE IF @type = 2 BEGIN
			IF SUBSTRING(@value, 1, 1) = '[' BEGIN
				SET @column_id = dbo.itemColumn_GetId(@instance, SUBSTRING(@value, 2, LEN(@value) - 2))
				IF @column_id IS NOT NULL BEGIN
					SET @result = '[' + CAST(@column_id AS NVARCHAR) + ']'
				END
				ELSE BEGIN
					SET @result = 'null'
				END
			END
		END

		ELSE IF @type = 3 BEGIN
			IF NOT @value = '' BEGIN
				SET @tab_id = dbo.processTab_GetId(@instance, @value)
				IF @tab_id IS NOT NULL BEGIN
					SET @result = 'process.tab_' + CAST(@tab_id AS NVARCHAR)
				END
				ELSE BEGIN
					SET @result = ''
				END
			END
		END
	END

	RETURN @result
END