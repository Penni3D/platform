EXEC app_set_archive_user_id 1

UPDATE process_translations SET translation = 'Nimi' WHERE translation = 'LIST_ITEM' AND code = 'fi-FI'
UPDATE process_translations SET translation = 'Name' WHERE translation = 'LIST_ITEM' AND code = 'en-GB'
UPDATE process_translations SET translation = 'Symboli' WHERE translation = 'LIST_SYMBOL' AND code = 'fi-FI'
UPDATE process_translations SET translation = 'Symbol' WHERE translation = 'LIST_SYMBOL' AND code = 'en-GB'
UPDATE process_translations SET translation = 'Väri' WHERE translation = 'LIST_SYMBOL_FILL' AND code = 'fi-FI'
UPDATE process_translations SET translation = 'Colour' WHERE translation = 'LIST_SYMBOL_FILL' AND code = 'en-GB'
UPDATE process_translations SET translation = 'Käytössä' WHERE translation = 'IN_USE' AND code = 'fi-FI'
UPDATE process_translations SET translation = 'In use' WHERE translation = 'IN_USE' AND code = 'en-GB'

INSERT INTO process_translations (instance, process, code, variable, translation)
SELECT ic.instance, ic.process, 'en-GB' AS code, ic.variable, 'Name' AS translation FROM item_columns ic
LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = 'en-GB'
WHERE ic.variable LIKE '%_LIST_ITEM' AND pt.variable IS NULL

INSERT INTO process_translations (instance, process, code, variable, translation)
SELECT ic.instance, ic.process, 'en-GB' AS code, ic.variable, 'Symbol' AS translation FROM item_columns ic
LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = 'en-GB'
WHERE ic.variable LIKE '%_LIST_SYMBOL' AND pt.variable IS NULL

INSERT INTO process_translations (instance, process, code, variable, translation)
SELECT ic.instance, ic.process, 'en-GB' AS code, ic.variable, 'Colour' AS translation FROM item_columns ic
LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = 'en-GB'
WHERE ic.variable LIKE '%_LIST_SYMBOL_FILL' AND pt.variable IS NULL

INSERT INTO process_translations (instance, process, code, variable, translation)
SELECT ic.instance, ic.process, 'fi-FI' AS code, ic.variable, 'Nimi' AS translation FROM item_columns ic
LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = 'fi-FI'
WHERE ic.variable LIKE '%_LIST_ITEM' AND pt.variable IS NULL

INSERT INTO process_translations (instance, process, code, variable, translation)
SELECT ic.instance, ic.process, 'fi-FI' AS code, ic.variable, 'Symboli' AS translation FROM item_columns ic
LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = 'fi-FI'
WHERE ic.variable LIKE '%_LIST_SYMBOL' AND pt.variable IS NULL

INSERT INTO process_translations (instance, process, code, variable, translation)
SELECT ic.instance, ic.process, 'fi-FI' AS code, ic.variable, 'Väri' AS translation FROM item_columns ic
LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = 'fi-FI'
WHERE ic.variable LIKE '%_LIST_SYMBOL_FILL' AND pt.variable IS NULL