DECLARE @name NVARCHAR(255)
DECLARE @GetTables CURSOR
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR SELECT '_' + instance + '_' + process FROM processes WHERE list_process = 1
OPEN @GetTables
FETCH NEXT FROM @GetTables INTO @name
WHILE @@FETCH_STATUS = 0 BEGIN
	EXECUTE ('ALTER TABLE ' + @name + ' ALTER COLUMN user_group NVARCHAR(MAX)')	
	EXEC app_ensure_archive @name
	FETCH NEXT FROM @GetTables INTO @name
END
CLOSE @GetTables