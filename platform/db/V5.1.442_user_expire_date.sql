DECLARE @Instance NVARCHAR(255)
DECLARE @SelectorId INT
DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances 
OPEN cursor_process
FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0 BEGIN
		EXEC dbo.app_ensure_column @Instance, 'user', 'expire_date', 0, 3, 0, 0, NULL, NULL, 0
        FETCH NEXT FROM cursor_process INTO @Instance
    END
CLOSE cursor_process
DEALLOCATE cursor_process