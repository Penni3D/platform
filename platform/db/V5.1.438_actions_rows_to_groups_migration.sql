EXEC app_set_archive_user_id 1


DECLARE @instance NVARCHAR(50);
DECLARE @inc INT = 1;

DECLARE cursor_instance CURSOR
    FOR SELECT instance
        FROM instances
OPEN cursor_instance
FETCH NEXT FROM cursor_instance INTO @instance
WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE @process NVARCHAR(255);
        DECLARE cursor_process CURSOR FOR SELECT process FROM processes
        OPEN cursor_process
        FETCH NEXT FROM cursor_process INTO @process
        WHILE @@FETCH_STATUS = 0
            BEGIN
                DECLARE @pa INT;
                DECLARE cursor_pa CURSOR FOR SELECT process_action_id FROM process_actions WHERE process = @process
                OPEN cursor_pa
                FETCH NEXT FROM cursor_pa INTO @pa
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        DECLARE @setbit TINYINT;
                        DECLARE @gid INT;
                        DECLARE @previousCgid INT; -- previous create group id.

                        DECLARE @partype TINYINT;
                        DECLARE @parid INT;
                        DECLARE cursor_par CURSOR FOR SELECT process_action_row_id, process_action_type
                                                      FROM process_action_rows
                                                      WHERE process_action_id = @pa

                        OPEN cursor_par
                        FETCH NEXT FROM cursor_par INTO @parid, @partype
                        WHILE @@FETCH_STATUS = 0
                            BEGIN



                                --print @pa
                                print 'par id ' + CAST(@parid AS NVARCHAR(255))
                                print 'PAR TYPE '  + CAST(@partype AS NVARCHAR(255)) -- haetaan process_actionin tyyppi SET(0,1,2,9) CREATE(3,4,8)		

                                declare @olist NVARCHAR(50) = 'UVWXYZabcdefghijklmnopqrstuvwxyz'


                                IF @partype = 3
                                    BEGIN
                                        print 'LUODAAN CREATE GROUPPI'
                                        SET @setbit = 2;
                                        INSERT INTO process_action_rows_groups (type, instance,
                                                                                process, variable, process_in_group,
                                                                                in_use, order_no)
                                        VALUES (
                                                   2,
                                                   @instance,
                                                   @process,
                                                   ('action_group_variable_' + CAST(@inc AS varchar(255))),
                                                   (SELECT value
                                                    FROM process_action_rows
                                                    where process_action_row_id = @parid),
                                                   1,
                                                   SUBSTRING(@olist, @inc, 1));
                                        SET @inc = @inc + 1;
                                        SET @gid = (SELECT TOP (1) process_action_group_id
                                                    FROM process_action_rows_groups
                                                    ORDER BY process_action_group_id desc);
                                        SET @previousCgid = @gid;

                                        UPDATE process_action_rows
                                        SET action_group_id = @gid
                                        WHERE process_action_row_id = @parid;
                                    END
                                ELSE IF @partype IN (0, 1, 2, 9) --If the latest group was a create group, creates and sets a new action_group_id for updates
                                    BEGIN
                                        IF (SELECT TOP (1) type
                                            FROM process_action_rows_groups parg
                                                     LEFT JOIN process_action_rows par ON par.action_group_id = parg.process_action_group_id WHERE par.process_action_id = @pa

                                            ORDER BY process_action_group_id desc) = 2
                                            BEGIN
                                                print 'LUODAAN SET GROUPPI'
                                                SET @setbit = 1;
                                                INSERT INTO process_action_rows_groups (type,
                                                                                        instance, process, variable,
                                                                                        process_in_group, in_use,
                                                                                        order_no)
                                                VALUES (
                                                           1,
                                                           @instance,
                                                           @process,
                                                           ('action_group_variable_' + CAST(@inc AS varchar(255))),
                                                           null,
                                                           1,
                                                           SUBSTRING(@olist, @inc, 1));
                                                SET @gid = (SELECT TOP (1) process_action_group_id
                                                            from process_action_rows_groups
                                                            ORDER BY process_action_group_id desc);
                                                SET @inc = @inc + 1;

                                                UPDATE process_action_rows
                                                SET action_group_id = @gid
                                                WHERE process_action_row_id = @parid;

                                            END
                                        ELSE IF (SELECT TOP (1) type
                                                 FROM process_action_rows_groups parg
                                                          LEFT JOIN process_action_rows par ON par.action_group_id = parg.process_action_group_id WHERE par.process_action_id = @pa

                                                 ORDER BY process_action_group_id desc) = 1
                                            BEGIN
                                                print 'EI LUODA SET GROUPPIA, ASETETAAN RIVI YLEMP��N SET GROUPPIIN'
                                                UPDATE process_action_rows
                                                SET action_group_id = @gid
                                                WHERE process_action_row_id = @parid
                                            END
                                        ELSE
                                            BEGIN
                                                print 'Luodaan Set group... Koska sellaista ei ole luotu viel�'
                                                SET @setbit = 1;
                                                INSERT INTO process_action_rows_groups (type,
                                                                                        instance, process,
                                                                                        variable,
                                                                                        process_in_group,
                                                                                        in_use, order_no)
                                                VALUES (
                                                           1,
                                                           @instance,
                                                           @process,
                                                           ('action_group_variable_' + CAST(@inc AS varchar(255))),
                                                           null,
                                                           1,
                                                           SUBSTRING(@olist, @inc, 1));
                                                SET @gid = (SELECT TOP (1) process_action_group_id
                                                            from process_action_rows_groups
                                                            ORDER BY process_action_group_id desc);
                                                SET @inc = @inc + 1;

                                                UPDATE process_action_rows
                                                SET action_group_id = @gid
                                                WHERE process_action_row_id = @parid;
                                            END
                                    END
                                ELSE IF @partype IN (4, 8)
                                    BEGIN
                                        IF @previousCgid IS NOT NULL
                                            BEGIN
                                                print 'Create group id on luotu viimeisimp�n�.'
                                                UPDATE process_action_rows
                                                SET action_group_id = @previousCgid
                                                WHERE process_action_row_id = @parid
                                            END
                                        ELSE
                                            IF @previousCgid IS NULL
                                                BEGIN
                                                    print '------------------------------------------------------------------------------------------Jokin action on pieless�---------------------------------------------------------------------------------------------------'
                                                    RAISERROR ('Some action is misconfigured!!', 10, 1, @partype);
                                                END
                                    END
                                FETCH NEXT FROM cursor_par INTO @parid, @partype
                            END;
                        CLOSE cursor_par;
                        DEALLOCATE cursor_par;
                        FETCH NEXT FROM cursor_pa INTO @pa
                    END;
                CLOSE cursor_pa;
                DEALLOCATE cursor_pa;
                FETCH NEXT FROM cursor_process INTO @process
            END;
        CLOSE cursor_process;
        DEALLOCATE cursor_process;
        FETCH NEXT FROM cursor_instance INTO @instance
    END;
CLOSE cursor_instance;
DEALLOCATE cursor_instance;