IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_copy_container'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_copy_container] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_copy_container]
	@instance NVARCHAR(10),	@process NVARCHAR(50), @from_process_container_id INT, @new_container_id INT OUTPUT
AS
BEGIN
	BEGIN		
		-- Insert old container columns to new container
		DECLARE @item_column_id INT, @placeholder NVARCHAR(255), @validation NVARCHAR(MAX), @required INT, @order_no NVARCHAR(MAX), @read_only TINYINT
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no, read_only)
		SELECT @new_container_id, item_column_id, placeholder, validation, required, order_no, read_only FROM process_container_columns WHERE process_container_id = @from_process_container_id ORDER BY order_no COLLATE Latin1_General_bin ASC
  
    END
END
