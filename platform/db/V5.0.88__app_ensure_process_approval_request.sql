IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_request_approval'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_request_approval] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_request_approval]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'request_approval'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2
	
	EXEC dbo.app_ensure_column @instance,@process,'idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'user_id', 0, 5, 1, 0, 0, NULL, 0, 0, 'user'
	EXEC dbo.app_ensure_column @instance,@process,'description', 0, 4, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process,'summary', 0, 4, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'request_sent', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'result', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'type_id', 0, 1, 1, 0, 0, NULL, 0, 0
	
	UPDATE item_columns SET status_column = 1 WHERE process = 'request_approval'
END

EXEC app_set_archive_user_id 0
ALTER TABLE _richmond_request_approval ADD type INT
EXEC app_ensure_archive 'request_approval'
