ALTER TABLE processes ADD require_insert TINYINT NOT NULL DEFAULT(0)
GO
UPDATE processes SET require_insert = 1 WHERE process = 'user' OR process = 'user_group'