SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[AfterUpdateColumn] ON [dbo].[item_columns] AFTER UPDATE AS
DECLARE @GetChanges CURSOR
DECLARE @TableName NVARCHAR(255)
DECLARE @OldColumnName NVARCHAR(255)
DECLARE @ColumnName NVARCHAR(50)
DECLARE @TableColumnName NVARCHAR(MAX)
DECLARE @DataType TINYINT
DECLARE @UseFk TINYINT
DECLARE @Unique TINYINT
DECLARE @sql NVARCHAR(MAX)

SET @GetChanges = CURSOR FAST_FORWARD LOCAL FOR
	SELECT it.[name] AS TableName, i.[name] AS ColumnName, i.data_type, d.[name] AS OldColumnName, i.use_fk, i.unique_col 
	FROM item_tables it
	INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
	INNER JOIN deleted d ON i.item_column_id = d.item_column_id

OPEN @GetChanges
FETCH NEXT FROM @GetChanges INTO @TableName, @ColumnName, @DataType, @OldColumnName, @UseFk, @Unique
WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @ColumnName IS NOT NULL AND @OldColumnName IS NULL AND LEN(@ColumnName) > 0
			BEGIN
				EXEC dbo.items_AddColumn @TableName, @ColumnName, @DataType
			END
		ELSE
			BEGIN
				IF @ColumnName <> @OldColumnName
					BEGIN
						SET @TableColumnName = @TableName + '.' + @OldColumnName
						EXEC sp_rename @TableColumnName, @ColumnName, 'COLUMN'
					END
			END

		IF @UseFk = 1
			BEGIN
				IF NOT EXISTS(SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']')) AND @ColumnName IS NOT NULL AND @ColumnName <> ''
					BEGIN
						SET @sql = 'ALTER TABLE ' + @TableName + ' WITH CHECK ADD  CONSTRAINT FK_' + @TableName+ '_' + @ColumnName + ' FOREIGN KEY(' + @ColumnName + ') REFERENCES [dbo].[items] ([item_id])'
						EXEC (@sql)
					END
			END
		ELSE
			BEGIN
				IF EXISTS(SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']'))
					BEGIN
						SET @sql = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT FK_' + @TableName+ '_' + @ColumnName
						EXEC (@sql)
					END
			END

		IF @Unique = 1
			BEGIN
				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_nonunique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_nonunique ON ' + @TableName
						EXEC (@sql)
					END

				IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_unique') AND @ColumnName IS NOT NULL AND @ColumnName <> ''
					BEGIN
						SET @sql = 'CREATE UNIQUE INDEX IX_' + @TableName + '_' + @ColumnName + '_unique ON ' + @TableName + '(' + @ColumnName + ')'
						EXEC (@sql)
					END
			END
		ELSE IF @Unique = 2
			BEGIN
				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_unique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_unique ON ' + @TableName
						EXEC (@sql)
					END

				IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_nonunique') AND @ColumnName IS NOT NULL AND @ColumnName <> ''
					BEGIN
						SET @sql = 'CREATE INDEX IX_' + @TableName + '_' + @ColumnName + '_nonunique ON ' + @TableName + '(' + @ColumnName + ')'
						EXEC (@sql)
					END
			END
		ELSE
			BEGIN
				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_unique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_unique ON ' +  @TableName
						EXEC (@sql)
					END

				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_nonunique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_nonunique ON ' + @TableName
						EXEC (@sql)
					END
			END

		IF @ColumnName <> @OldColumnName
			BEGIN
				IF EXISTS(SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @OldColumnName + ']'))
					BEGIN
						SET @sql = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT FK_' + @TableName+ '_' + @OldColumnName
						EXEC (@sql)
					END

				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @OldColumnName + '_unique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @OldColumnName + '_unique ON ' + @TableName
						EXEC (@sql)
					END

				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @OldColumnName + '_nonunique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @OldColumnName + '_nonunique ON ' + @TableName
						EXEC (@sql)
					END
			END

		FETCH NEXT FROM @GetChanges INTO @TableName, @ColumnName, @DataType, @OldColumnName, @UseFk, @Unique
	END