ALTER TABLE conditions ADD order_no NVARCHAR(MAX) 
GO

-- ADD FIELD FOR SUPERVISED USERS (FOR HOUR APPROVAL)

DECLARE @instance NVARCHAR(10) = ''
DECLARE @process NVARCHAR(50) = ''

--Archive user is 0
DECLARE @BinVar varbinary(128);
SET @BinVar = CONVERT(varbinary(128), 0);
SET CONTEXT_INFO @BinVar;

DECLARE @lastInstance NVARCHAR(10) = ''
DECLARE @lastProcess NVARCHAR(50) = ''
DECLARE @orderNo NVARCHAR(MAX) = ''

DECLARE @condition_id INT
DECLARE ConditionId_Cursor CURSOR FOR
  SELECT condition_id, instance, process FROM conditions ORDER BY instance, process

OPEN ConditionId_Cursor

FETCH NEXT FROM ConditionId_Cursor INTO @condition_id, @instance, @process

WHILE @@fetch_status = 0
    BEGIN
		IF (@process != @lastProcess OR @instance != @lastInstance)
			BEGIN
				SET @orderNo = 'U'
			END
			
		SET @orderNo = dbo.getOrderBetween(@orderNo, null)
		
		SET @lastProcess = @process
		SET @lastInstance = @instance
		
		UPDATE conditions SET order_no = @orderNo WHERE condition_id = @condition_id AND instance = @instance AND process = @process
		
		FETCH NEXT FROM ConditionId_Cursor INTO @condition_id, @instance, @process
	END
	
CLOSE ConditionId_Cursor
DEALLOCATE ConditionId_Cursor

EXEC app_ensure_archive 'conditions'
GO
