SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION  [dbo].[get_item_data_process_selections_with_ids](@d datetime, @iCol int, @itemId int)
		RETURNS @rtab TABLE ([item_id] int NOT NULL ,[item_column_id] int NOT NULL ,[selected_item_id] int NOT NULL ,[link_item_id] int NULL ,[archive_userid] int NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
		AS
		BEGIN	
			INSERT @rtab
				SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start]
				FROM archive_item_data_process_selections WHERE archive_id IN (
					SELECT MAX(archive_id)
					FROM archive_item_data_process_selections
					WHERE item_id = @itemId AND item_column_id = @iCol AND archive_start < @d AND archive_end > @d AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN
					(SELECT CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) FROM  item_data_process_selections  WHERE  item_id = @itemId AND  item_column_id = @iCol AND archive_start < @d  )
					GROUP BY [item_column_id],[item_id],[selected_item_id]
					)
				UNION 
				SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start] 
				FROM item_data_process_selections 
				WHERE  item_id = @itemId AND  archive_start < @d AND item_column_id = @iCol
			RETURN
		END
GO