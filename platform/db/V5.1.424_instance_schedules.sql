CREATE TABLE instance_schedules (
    schedule_id INT PRIMARY KEY IDENTITY (1, 1),
    instance NVARCHAR(255),
    name NVARCHAR(255),
	schedule_json NVARCHAR(MAX),
	last_modified DATETIME NOT NULL
);


CREATE TABLE instance_schedule_dates (
    schedule_date_id INT PRIMARY KEY IDENTITY (1, 1),
	schedule_id INT NOT NULL,
    date DATETIME NOT NULL,
   -- status INT NOT NULL,
	outcome INT NOT NULL,
	type INT NOT NULL
);


ALTER TABLE [dbo].[instance_schedule_dates]  WITH CHECK ADD  CONSTRAINT [FK_instance_schedule_dates_instance_schedules] FOREIGN KEY([schedule_id])
REFERENCES [dbo].[instance_schedules] ([schedule_id])
ON DELETE CASCADE
GO
 
ALTER TABLE [dbo].[instance_schedule_dates] CHECK CONSTRAINT [FK_instance_schedule_dates_instance_schedules]
GO