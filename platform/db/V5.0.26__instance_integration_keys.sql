CREATE TABLE [dbo].[instance_integration_keys] (
  [instance] nvarchar(10) NOT NULL,
  [user_item_id] int NOT NULL,
  [key] nvarchar(255) NULL
)
GO

ALTER TABLE [dbo].[instance_integration_keys] SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE [dbo].[instance_integration_keys] ADD CONSTRAINT [Unique_Key] UNIQUE NONCLUSTERED ([key] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

ALTER TABLE [dbo].[instance_integration_keys] ADD CONSTRAINT [PK__instance__5DEF6B6197A38FED] PRIMARY KEY CLUSTERED ([user_item_id], [instance])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

ALTER TABLE [dbo].[instance_integration_keys] ADD CONSTRAINT [FK_instances] FOREIGN KEY ([instance]) REFERENCES [instances] ([instance]) ON DELETE CASCADE ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[instance_integration_keys] ADD CONSTRAINT [FK_items] FOREIGN KEY ([user_item_id]) REFERENCES [items] ([item_id]) ON DELETE CASCADE ON UPDATE NO ACTION
GO
