ALTER PROCEDURE [dbo].[app_ensure_process_allocation]	-- Add the parameters for the stored procedure here
  @instance nvarchar(10)
AS
BEGIN
  DECLARE @process NVARCHAR(50) = 'allocation'

  --Archive user is 0
  DECLARE @BinVar varbinary(128);
  SET @BinVar = CONVERT(varbinary(128), 0);
  SET CONTEXT_INFO @BinVar;

  --Create process
  EXEC app_ensure_process @instance, @process, @process, 1

  --Create columns
  EXEC app_ensure_column @instance, @process, 'status', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'resource_item_id', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'process_item_id', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'cost_center'    , 1, 1, 1, 1, 0, NULL, 0, 0
  EXEC app_ensure_column @instance, @process, 'creator_item_id', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'task_item_id', 1, 0, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'resource_type', 0, 0, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'work_category_item_id', 1, 1, 1, 1, 0, null,  0, 0
END;
GO

EXEC app_execute_procedure_for_all_instances 'app_ensure_process_allocation'