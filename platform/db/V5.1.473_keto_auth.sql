DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
    BEGIN
        EXEC app_ensure_column @instance, 'user', 'ketoauth', null, 1, 1, 0, 0, null,  null, 0, 0
        FETCH  NEXT FROM instance_cursor INTO @instance
    END