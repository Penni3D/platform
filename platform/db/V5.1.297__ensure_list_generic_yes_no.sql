-- Stored Procedure

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_list_genyesno'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_list_genyesno] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_list_genyesno]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'list_generic_yes_no'
	DECLARE @tableName NVARCHAR(50) = ''
	SET @tableName = '_' + @instance + '_' + @process
	DECLARE @sql NVARCHAR(MAX) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_list @instance, @process, @process, 0

	UPDATE processes SET list_order = 'list_item' WHERE process = @process

	EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process,'value',0,1,0,0,0,'',0,0,'','',0,0

	INSERT INTO items (instance, process) VALUES (@instance, @process);
	DECLARE @itemId INT = @@identity;

	SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Yes'', value = 1 WHERE item_id = ' +  CAST(@itemId AS nvarchar)
	EXEC (@sql)

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    SET @itemId = @@identity;

    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''No'', value = 0 WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)


END
GO
EXEC app_set_archive_user_id 1
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
    EXEC app_ensure_process_list_genyesno @instance

    FETCH  NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor

