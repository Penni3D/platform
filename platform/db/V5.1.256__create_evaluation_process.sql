IF OBJECT_ID('[dbo].[app_ensure_process_evaluation]') IS NOT NULL BEGIN
  DROP PROCEDURE app_ensure_process_evaluation
END
  
GO
  
CREATE PROCEDURE [dbo].[app_ensure_process_evaluation]	-- Add the parameters for the stored procedure here
  @instance nvarchar(10)
AS
BEGIN
  
  DECLARE @process NVARCHAR(50) = 'evaluation'
  DECLARE @tableName NVARCHAR(50) = ''

  --Archive user is 0
  DECLARE @BinVar varbinary(128);
  SET @BinVar = CONVERT(varbinary(128), 0);
  SET CONTEXT_INFO @BinVar;

  --Create process
  EXEC app_ensure_process @instance, @process, @process, 2


  --Create columns

  EXEC app_ensure_column @instance, @process, 'evaluator_item_id', 0, 1, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'creator_item_id', 0, 1, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'completed_date', 0, 3, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'created_date', 0, 3, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'evaluation_text', 0, 4, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'info_text', 0, 4, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'status', 0, 1, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'matrix_column', 0, 22, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'totalscore', 0, 16, 1, 1, 0, null,  0, 0
update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 


END;
GO
DECLARE @sql NVARCHAR(MAX);
DECLARE @instance NVARCHAR(MAX);
DECLARE @GetInstances CURSOR
DECLARE @i1 INT;

--Get Tables
SET @GetInstances = CURSOR FAST_FORWARD LOCAL FOR 
select instance from instances

OPEN @GetInstances
FETCH NEXT FROM @GetInstances INTO @instance

WHILE (@@FETCH_STATUS=0) BEGIN
  

  
  EXEC app_ensure_process_evaluation @instance

  FETCH NEXT FROM @GetInstances INTO @instance
END
CLOSE @GetInstances