SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceConfiguration_ValueIdReplace]
(
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @value IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('"portfolioId"', 0), 
				('"baselinePortfolioId"', 0),
				('"dashboard_id"', 1),
				('"fields_portfolio"', 0),
				('"signedTaskListValue"', 2),
				('"modelPortfolio"', 0),
				('"ganttPortfolioId"', 0),
				('"kanbanPortfolioId"', 0),
				('"targetColumns"', 3),
				('"userSelectorId"', 0),
				('"BaseCalendar"', 4)
		
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @value)
					IF @startPos > 0
						BEGIN  
							IF @type = 3
								BEGIN
									SET @startPos = CHARINDEX('[', @value, @startPos) + 1
									SET @endPos1 = CHARINDEX(']', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos) 
								END
							ELSE
								BEGIN
									SET @startPos = @startPos + LEN(@strToFound) + 1
									SET @endPos1 = CHARINDEX(',', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END

							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos2 - @startPos)
								END

							SET @var3 = LTRIM(RTRIM(REPLACE(REPLACE(@var2, CHAR(13), ''), CHAR(10), '')))
							SET @var3 = REPLACE(@var3, ' ', '')

							IF @type = 0
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetVariable(@var3)
													
													
													IF @var4 IS NOT NULL
														BEGIN
															SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
														END
												END
										END
								END

							IF @type = 1
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SET @var4 = dbo.processDashboard_GetVariable(@var3)
													IF @var4 IS NOT NULL
														BEGIN
															SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
														END
												END
										END
								END

							IF @type = 2
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SET @var4 = CAST(dbo.item_GetGuid(@var3) AS NVARCHAR(MAX))
													IF @var4 IS NOT NULL
														BEGIN
															SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
														END
												END
										END
								END

							IF @type = 3
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> ']' AND @var3 <> '0'
										BEGIN
											DECLARE @colId NVARCHAR(MAX)
											DECLARE @var NVARCHAR(MAX)
						
											SET @var4 = REPLACE(REPLACE(@var3, '[', ''), ']', '')

											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable(@var4, ',') 
					
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @colId
								
											DECLARE @start INT
											SET @start = 1
								  			WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													SET @var = dbo.itemColumn_GetVariable(@colId)
													IF @var IS NULL
														BEGIN
															SET @var = 'NULL' 
														END
													SET @var4 = STUFF(@var4, @start, LEN(@colId), @var)

													SET @start = @start + LEN(@var) + 1

													FETCH NEXT FROM @delimited INTO @colId
												END
											CLOSE @delimited
													
											SET @value = REPLACE(STUFF(@value, @startPos, LEN(@var2), @var4), ' ', '')
										END
								END

							IF @type = 4
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SELECT @var4 = REPLACE(calendar_name, ' ', '_') FROM calendars WHERE calendar_id = @var3
													IF @var4 IS NOT NULL
														BEGIN
															SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
														END
												END
										END
								END
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END							
		END
	RETURN @value
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceConfiguration_ValueVarReplace]
(
	@instance NVARCHAR(MAX),
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @value IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('"portfolioId"', 0), 
				('"baselinePortfolioId"', 0),
				('"dashboard_id"', 1),
				('"fields_portfolio"', 0),
				('"signedTaskListValue"', 2),
				('"modelPortfolio"', 0),
				('"ganttPortfolioId"', 0),
				('"kanbanPortfolioId"', 0),
				('"targetColumns"', 3),
				('"userSelectorId"', 0),
				('"BaseCalendar"', 4)
					
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
					
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
			WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @value)
					IF @startPos > 0
						BEGIN
							IF @type = 3
								BEGIN
									SET @startPos = CHARINDEX('[', @value, @startPos) + 1
									SET @endPos1 = CHARINDEX(']', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos) 
								END
							ELSE
								BEGIN
								SET @startPos = @startPos + LEN(@strToFound) + 1
									SET @endPos1 = CHARINDEX(',', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END

							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos2 - @startPos)
								END
									
							SET @var3 = @var2

							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
										END
								END

							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.processDashboard_GetId(@instance, @var3)
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
										END
								END

							IF @type = 2
								BEGIN
									IF LEN(@var3) > 0
										BEGIN
											SELECT @var4 = item_id FROM items WHERE [guid] = @var3
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
										END
								END
									
							IF @type = 3
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> ']' AND @var3 <> '0'
										BEGIN
											DECLARE @var NVARCHAR(MAX)
											DECLARE @colId NVARCHAR(MAX)

											SET @var4 = REPLACE(REPLACE(@var3, '[', ''), ']', '')
											
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable(@var4, ',') 
													
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @var
													
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @var = 'NULL'
														BEGIN
															SET @colId = '[]'
														END
													ELSE 
														BEGIN
															SET @colId = dbo.itemColumn_GetId(@instance, @var)
															IF @colId IS NULL
																BEGIN
																	SET @colId = 0
																END
														END
													SET @var4 = REPLACE(@var4, @var, CAST(@colId AS NVARCHAR(MAX)))

													FETCH NEXT FROM @delimited INTO @var
												END
											CLOSE @delimited
											SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
										END
								END

							IF @type = 4
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0'
										BEGIN
											SELECT @var4 = calendar_id FROM calendars WHERE base_calendar = 1 AND REPLACE(calendar_name, ' ', '_') = @var3
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @value = STUFF(@value, @startPos, LEN(@var2), @var4)
										END
								END
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END							
		END
	RETURN @value
END