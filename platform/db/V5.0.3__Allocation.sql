-- Columns
IF NOT exists(select * from sysobjects WHERE name = 'allocation_comments' AND xType= 'U')
	BEGIN
		CREATE TABLE [dbo].[allocation_comments]
		(
		[allocation_comment_id] [int] NOT NULL IDENTITY(1, 1),
		[allocation_item_id] [int] NOT NULL,
		[comment] [nvarchar] (255) NOT NULL,
		[created_at] [datetime] NOT NULL,
		[author_item_id] [int] NOT NULL,
			CONSTRAINT [PK_allocation_comments] PRIMARY KEY CLUSTERED  ([allocation_comment_id]),
			CONSTRAINT [FK_allocation_comments_items] FOREIGN KEY ([allocation_item_id]) REFERENCES [dbo].[items] ([item_id]),
			CONSTRAINT [FK_allocation_comments_items1] FOREIGN KEY ([author_item_id]) REFERENCES [dbo].[items] ([item_id])	
		)
	END			
GO

exec app_ensure_archive 'allocation_comments'
GO

IF NOT exists(select * from sysobjects WHERE name = 'allocation_durations' AND xType= 'U')
	BEGIN
		CREATE TABLE [dbo].[allocation_durations](
			[allocation_item_id] [int] NOT NULL,
			[date] [date] NOT NULL,
			[hours] [float] NOT NULL,
			CONSTRAINT [PK_allocation_durations] PRIMARY KEY CLUSTERED ([allocation_item_id] ASC,[date] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
			CONSTRAINT [FK_allocation_durations_items] FOREIGN KEY([allocation_item_id]) REFERENCES [dbo].[items] ([item_id])
		) ON [PRIMARY]
	END 
GO

IF NOT exists(select * from sysobjects WHERE name = 'task_durations' AND xType= 'U')
	BEGIN
		CREATE TABLE [dbo].[task_durations](
			[task_item_id] [int] NOT NULL,
			[resource_item_id] [int] NOT NULL,
			[date] [date] NOT NULL,
			[hours] [float] NOT NULL,
			CONSTRAINT [PK_task_durations] PRIMARY KEY CLUSTERED ([task_item_id] ASC,[resource_item_id] ASC,[date] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
			CONSTRAINT [FK_task_durations_items_resource] FOREIGN KEY([resource_item_id]) REFERENCES [dbo].[items] ([item_id]),
			CONSTRAINT [FK_task_durations_items_task] FOREIGN KEY([task_item_id]) REFERENCES [dbo].[items] ([item_id]) ON DELETE CASCADE
		) ON [PRIMARY]
	END

GO

ALTER TABLE [dbo].[task_durations] CHECK CONSTRAINT [FK_task_durations_items_task]
GO

ALTER  TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data dd						INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.process_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.task_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
	DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
	DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id
	
	--Finally delete the item
	DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id




