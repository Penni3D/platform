SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[menu_IdReplace]
(
	@params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = CHARINDEX('portfolioId', @params)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @params, @startPos)
			SET @endPos2 = CHARINDEX('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.processPortfolio_GetVariable(@var3)		

					IF @var4 IS NOT NULL
						BEGIN
							SET @params = STUFF(@params, @startPos + 13, LEN(@var2) - 13, @var4)
							SET @startPos = CHARINDEX('portfolioId', @params, @startPos + 13 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = CHARINDEX('portfolioId', @params, @startPos + 1)
						END
				END
			ELSE
				BEGIN
					SET @startPos = CHARINDEX('portfolioId', @params, @startPos + 1)
				END
		END

	SET @startPos = CHARINDEX('PageTypeItemId', @params)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @params, @startPos)
			SET @endPos2 = CHARINDEX('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 17, LEN(@var2) - 16)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = CAST(dbo.item_GetGuid(@var3) AS nvarchar(MAX))
					
					IF @var4 IS NOT NULL
						BEGIN
							SET @params = STUFF(@params, @startPos + 16, LEN(@var2) - 16, @var4)
							SET @startPos = CHARINDEX('PageTypeItemId', @params, @startPos + 16 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = CHARINDEX('PageTypeItemId', @params, @startPos + 1)
						END
				END
			ELSE
				BEGIN
					SET @startPos = CHARINDEX('PageTypeItemId', @params, @startPos + 1)
				END
		END

	RETURN @params
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[menu_VarReplace]
(
	@instance NVARCHAR(10),
	@params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = CHARINDEX('portfolioId', @params)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @params, @startPos)
			SET @endPos2 = CHARINDEX('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)
			
			SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		

			IF ISNUMERIC(@var4) = 0
				BEGIN
					SET @var4 = '0'
				END
			SET @params = STUFF(@params, @startPos + 13, LEN(@var2) - 13, @var4)

			SET @startPos = CHARINDEX('portfolioId', @params, @startPos + 13 + LEN(@var4))
		END

	SET @startPos = CHARINDEX('PageTypeItemId', @params)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @params, @startPos)
			SET @endPos2 = CHARINDEX('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 17, LEN(@var2) - 16)
			
			SET @var4 = dbo.item_GetId(@var3)

			IF ISNUMERIC(@var4) = 0
				BEGIN
					SET @var4 = '0'
				END
			SET @params = STUFF(@params, @startPos + 16, LEN(@var2) - 16, @var4)

			SET @startPos = CHARINDEX('PageTypeItemId', @params, @startPos + 16 + LEN(@var4))
		END

	RETURN @params
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processPortfolio_DownloadOptionsVarReplace]
(
	@instance NVARCHAR(MAX),
	@downloadOptions NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found NVARCHAR(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @reverseStartPos INT
	DECLARE @len INT

	IF @downloadOptions IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('":{', 0), --ItemColumnId 
				('PortfolioId', 1)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @downloadOptions)
					WHILE @startPos > 0
						BEGIN  
							IF @type = 0
								BEGIN  
									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @reverseStartPos = LEN(@downloadOptions) - @startPos + 2

									SET @endPos1 = CHARINDEX('"', @downloadOptions, @reverseStartPos)
									SET @endPos2 = CHARINDEX('{', @downloadOptions, @reverseStartPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos1 - @reverseStartPos + 4)
											SET @len = @endPos1 - @reverseStartPos
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos2 - @reverseStartPos + 4)
											SET @len = @endPos2 - @reverseStartPos
										END

									SET @var3 = SUBSTRING(@var2, 4, LEN(@var2) - 4)
				
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.itemColumn_GetId(@instance, REVERSE(@var3))	
											IF @var4 IS NOT NULL
												BEGIN
													SET @var4 = '0'
												END
											SET @downloadOptions = STUFF(@downloadOptions, @reverseStartPos, @len, REVERSE(@var4))
										END

									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @startPos = LEN(@downloadOptions) - @reverseStartPos + 2
								END

							IF @type = 1
								BEGIN  
									SET @endPos1 = CHARINDEX(',', @downloadOptions, @startPos)
									SET @endPos2 = CHARINDEX('}', @downloadOptions, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos1 - @startPos)
											SET @len = @endPos1 - @startPos - LEN(@strToFound) - 2
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos2 - @startPos)
											SET @len = @endPos2 - @startPos - LEN(@strToFound) - 2
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)

									SET @startPos = @startPos + LEN(@strToFound) + 2

									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
											IF @var4 IS NULL
												BEGIN
													SET @var4 = '0'
												END
											SET @downloadOptions = STUFF(@downloadOptions, @startPos, @len, @var4)
										END
								END

							SET @startPos = CHARINDEX(@strToFound, @downloadOptions, @startPos + 1)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @downloadOptions
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processColumns_ValidationVarReplace]
(
	@instance NVARCHAR(MAX),
	@validation NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(MAX) 
	DECLARE @type INT
	DECLARE @strToFoundLength INT

	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)

	IF @validation IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('ParentId', 0)
			
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
			
			WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					DECLARE @startPos INT = CHARINDEX(@strToFound, @validation)
					IF @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 2

							IF @type = 0 
								BEGIN
									SET @endPos1 = CHARINDEX(',', @validation, @startPos)
									SET @endPos2 = CHARINDEX('}', @validation, @startPos)
								END
							
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos2 - @startPos)
								END
							
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							
							IF @type = 0 
								BEGIN
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
											IF @var4 IS NOT NULL
												BEGIN
													SET @validation = STUFF(@validation, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, '"' + @var4 + '"')
												END
											ELSE
												BEGIN
													SET @validation = STUFF(@validation, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END
						END
						FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
				CLOSE @getStrs
		END
		RETURN @validation
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN
			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2

									IF @type = 0 
										BEGIN
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									
									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX(']', @dataAdditional2, @startPos) 
										END
									
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @listItemId INT

													SET @var3 = REPLACE(@var3, '[', '')
													SET @var3 = REPLACE(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
													
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
			
			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 2 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
			
			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END