﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[items_RebuildOrderNumbers]
(
	@instance NVARCHAR(10),
	@process NVARCHAR(50)
)
AS
BEGIN
	DECLARE @len INT;
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'SELECT @len = COUNT(item_id) FROM _' + @instance + '_' + @process
	EXEC sp_executesql @sql, N'@len INT OUT', @len OUT
	CREATE TABLE #temp (order_no NVARCHAR(MAX))
	DECLARE @charList NVARCHAR(MAX) = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

	DECLARE @step FLOAT = CAST(LEN(@charList) AS FLOAT) / CAST(@len AS FLOAT)
	IF @step < 1 SET @step = 1
	DECLARE @i FLOAT = 1
	WHILE @i < LEN(@charList) BEGIN
		SET @i = @i + @step
		INSERT INTO #temp (order_no) VALUES (SUBSTRING(@charList, CAST(@i AS INT), 1))
	END

	DECLARE @runner INT = 1;
	DECLARE @f NVARCHAR(MAX);
	DECLARE @l NVARCHAR(MAX);
	DECLARE @s NVARCHAR(MAX);

	WHILE @i < @len BEGIN
		SELECT ROW_NUMBER() OVER(ORDER BY order_no COLLATE Latin1_General_bin ASC) AS row, order_no 
		INTO #sorted
		FROM #temp
		SELECT @f = order_no FROM #sorted WHERE Row = @runner
		SELECT @l = order_no FROM #sorted WHERE Row = @runner + 1
		SET @i = @i + 1
		IF @runner + 2 < @i SET @runner = @runner + 2
		ELSE SET @runner = 1
		DROP TABLE #sorted
		SELECT @s = dbo.getOrderBetween(@f, @l)
		INSERT INTO #temp (order_no) VALUES (@s)
	END

	EXEC app_set_archive_user_id 1

	SET @sql = 'UPDATE _' + @instance + '_' + @process + ' ' +
			   'SET order_no = i.order_no ' +
			   'FROM (' +
			   '	SELECT s1.item_id, s2.order_no' +
			   '	FROM (' +
			   '		SELECT row = ROW_NUMBER() OVER(ORDER BY order_no COLLATE Latin1_General_bin ASC, item_id ASC), item_id' +
			   '		FROM _' + @instance + '_' + @process + '' +
			   '	) s1' +
			   '	INNER JOIN (' +
			   '		SELECT row = ROW_NUMBER() OVER(ORDER BY order_no COLLATE Latin1_General_bin ASC), order_no FROM #temp' +
			   '	) s2 ON s1.row = s2.row' +
			   ') i ' +
			   'WHERE _' + @instance + '_' + @process + '.item_id = i.item_id'
	EXEC sp_executesql @sql

	DROP TABLE #temp
END