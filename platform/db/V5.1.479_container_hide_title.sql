ALTER TABLE process_containers ADD hide_title INT NOT NULL DEFAULT 0

GO

UPDATE process_containers SET hide_title = 1 WHERE process_container_id IN (
	SELECT pc.process_container_id FROM process_containers pc 
	INNER JOIN process_tab_containers ptc ON ptc.process_container_id = pc.process_container_id
	WHERE (SELECT COUNT(*) FROM process_translations pt WHERE pt.variable = pc.variable AND pt.translation <> '') = 0
	GROUP BY pc.process_container_id
)