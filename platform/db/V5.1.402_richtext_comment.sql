CREATE FUNCTION veturi
(
	-- Add the parameters for the function here
	@Icid INT, @Code NVARCHAR(10)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @S NVARCHAR(MAX) = (SELECT TOP 1 validation FROM process_container_columns WHERE item_column_id = @Icid)
	DECLARE @ekaindex INT = (SELECT CHARINDEX('Label', @S))
	DECLARE @tokaindex INT = (SELECT CHARINDEX('}', @S))




	DECLARE @Kokoroska NVARCHAR(MAX) = (SELECT SUBSTRING(@S, @ekaindex, @tokaindex)) 

	DECLARE @kolmasindex INT = (SELECT CHARINDEX(@Code, @Kokoroska)) + 10
	DECLARE @neljäsindex INT = (SELECT LEN(@Kokoroska))


	DECLARE @kaannos NVARCHAR(MAX) = (SELECT SUBSTRING(@Kokoroska, @kolmasindex, @neljäsindex)) 


	DECLARE @i5 INT = (SELECT CHARINDEX(':\"''', @kaannos))
	DECLARE @i6 INT = (SELECT CHARINDEX('\"', @kaannos))

	DECLARE @kaannos2 NVARCHAR(MAX) = (SELECT SUBSTRING(@kaannos, @i5, @i6)) 
	RETURN @kaannos2
END
GO

	DECLARE @Code NVARCHAR (255);
	DECLARE @VariableName NVARCHAR (255);
	DECLARE @Translation NVARCHAR (MAX);
	DECLARE @Icid INT;
	DECLARE @NewBody NVARCHAR (MAX);

	DECLARE cursor_process CURSOR
	FOR SELECT item_column_id, variable FROM item_columns WHERE data_type = 17

	OPEN cursor_process

	FETCH NEXT FROM cursor_process INTO @Icid, @VariableName
		WHILE @@FETCH_STATUS = 0
		BEGIN
		  DECLARE cursor_ic CURSOR
		  FOR SELECT code, CASE WHEN (SELECT COUNT (*) FROM process_container_columns WHERE item_column_id = @Icid AND validation LIKE '%"' + code + '%') = 1 
		  THEN dbo.veturi(@icid, code)
		  ELSE (SELECT translation FROM process_translations pt INNER JOIN item_columns ic ON ic.variable = pt.variable WHERE item_column_id = @Icid AND code = code) END as translation 
		  FROM process_translations pt INNER JOIN item_columns ic ON ic.variable = pt.variable WHERE item_column_id = @Icid
		  SET @NewBody = '{';
		  OPEN cursor_ic
			FETCH NEXT FROM cursor_ic INTO @Code, @Translation
			PRINT('---')
			PRINT(@Icid)
			PRINT(@Translation)
				WHILE @@FETCH_STATUS = 0
					BEGIN
					SET @NewBody = (SELECT CONCAT(@NewBody,'"'+@Code+'":{"html":"'+@Translation+'","delta":{"ops":[{"insert":"' + @Translation+'"}]}},'))
						FETCH NEXT FROM cursor_ic INTO @Code, @Translation
					END;
		  CLOSE cursor_ic;
			DEALLOCATE cursor_ic;
					SET @NewBody = left(@NewBody + ',,', charindex(',,', @NewBody+ ',,') - 1)
					SET @NewBody = (SELECT CONCAT(@NewBody, '}'))
					UPDATE item_columns SET data_additional = @NewBody WHERE item_column_id = @Icid
			FETCH NEXT FROM cursor_process INTO 
				@Icid,@VariableName
		END;
	
	CLOSE cursor_process;
	DEALLOCATE cursor_process;

GO
DROP FUNCTION veturi
GO