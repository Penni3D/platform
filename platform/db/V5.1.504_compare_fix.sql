SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[menu_IdReplace]
(
	@params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @params IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('MenuId"', 1, 9),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
		
			SET @startPos = 1
		  	WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @params, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 1
									
							IF CHARINDEX('[', @params, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
				
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
								END

							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							SET @var3 = REPLACE(@var3, '"', '')

							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @guid UNIQUEIDENTIFIER
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
								
											DECLARE @start INT
											SET @start = 1
								  			WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													DECLARE @gu_id NVARCHAR(MAX)
													IF ISNUMERIC(@listVal) = 0
														BEGIN
															SET @gu_id = 'NULL' 
														END
													ELSE
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = 'NULL'
																END
														END
													SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

													SET @start = @start + LEN(@gu_id) + 1

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
													
											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
									
							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @colId NVARCHAR(MAX)
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colId

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF ISNUMERIC(@colId) = 1
														BEGIN
															IF @subType = 0
																BEGIN
																	SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colVar = dbo.processPortfolio_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colVar = dbo.condition_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colVar = dbo.processAction_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colVar = dbo.processActionDialog_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colVar = dbo.processTab_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colVar = dbo.processContainer_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colVar = dbo.processDashboard_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colVar = dbo.processDashboardChart_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 9
																BEGIN
																	SET @colVar = dbo.menu_GetVariable(CAST(@colId AS INT))
																END

															IF @colVar IS NULL
																BEGIN
																	SET @colVar = 'NULL' 
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
															SET @start2 = @start2 + LEN(@colVar) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colId
												END
											CLOSE @delimited2

											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 2
								BEGIN
									IF LEFT(LTRIM(@var3), 5) = '"tab_'
										BEGIN
											SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END

							IF @type = 3
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SELECT @var4 = REPLACE(calendar_name, ' ', '_') FROM calendars WHERE calendar_id = @var3
													IF @var4 IS NOT NULL
														BEGIN
															SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
								END
										
							SET @startPos = CHARINDEX(@strToFound, @params, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @params
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[instanceConfiguration_ValueIdReplace]
(
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @value IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
		
			SET @startPos = 1
		  	WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @value, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 2
									
							IF CHARINDEX('[', @value, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
				
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos2 - @startPos)
								END

							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							SET @var3 = LTRIM(RTRIM(REPLACE(REPLACE(@var3, CHAR(13), ''), CHAR(10), '')))
							SET @var3 = REPLACE(@var3, ' ', '')

							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @guid UNIQUEIDENTIFIER
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
								
											DECLARE @start INT
											SET @start = 1
								  			WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													DECLARE @gu_id NVARCHAR(MAX)
													IF ISNUMERIC(@listVal) = 0
														BEGIN
															SET @gu_id = 'NULL' 
														END
													ELSE
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = 'NULL'
																END
														END
													SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

													SET @start = @start + LEN(@gu_id) + 1

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
													
											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
									
							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @colId NVARCHAR(MAX)
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colId

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF ISNUMERIC(@colId) = 1
														BEGIN
															IF @subType = 0
																BEGIN
																	SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colVar = dbo.processPortfolio_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colVar = dbo.condition_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colVar = dbo.processAction_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colVar = dbo.processActionDialog_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colVar = dbo.processTab_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colVar = dbo.processContainer_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colVar = dbo.processDashboard_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colVar = dbo.processDashboardChart_GetVariable(CAST(@colId AS INT))
																END

															IF @colVar IS NULL
																BEGIN
																	SET @colVar = 'NULL' 
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
															SET @start2 = @start2 + LEN(@colVar) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colId
												END
											CLOSE @delimited2

											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 2
								BEGIN
									IF LEFT(LTRIM(@var3), 5) = '"tab_'
										BEGIN
											SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END

							IF @type = 3
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SELECT @var4 = REPLACE(calendar_name, ' ', '_') FROM calendars WHERE calendar_id = @var3
													IF @var4 IS NOT NULL
														BEGIN
															SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
								END
										
							SET @startPos = CHARINDEX(@strToFound, @value, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @value
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @strToFound2 NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @strTable2 TABLE (str_to_found2 NVARCHAR(MAX), strtype2 INT)
	DECLARE @strTable3 TABLE (str_to_found3 NVARCHAR(MAX), strtype3 INT, strsubtype3 INT)
	DECLARE @getStrs CURSOR
	DECLARE @getStrs2 CURSOR
	DECLARE @getStrs3 CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable3 (str_to_found3, strtype3, strsubtype3)
					VALUES
						('ItemId"', 0, 0),
						('ColumnId"', 1, 0),
						('PortfolioId"', 1, 1),
						('ConditionId"', 1, 2),
						('ActionId"', 1, 3),
						('ActionDialogId"', 1, 4),
						('TabId"', 1, 5),
						('ContainerId"', 1, 6),
						('DashboardId"', 1, 7),
						('ChartId"', 1, 8),
						('CalendarId"', 1, 9),
						('State"', 2, 0)
		
					SET @getStrs3 = CURSOR FAST_FORWARD LOCAL FOR
					SELECT str_to_found3, strtype3, strsubtype3 FROM @strTable3
			
					OPEN @getStrs3
					FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
		
					SET @startPos = 1
		  			WHILE (@@FETCH_STATUS = 0)
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos)
							WHILE @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 1
									
									IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
										BEGIN
											SET @startPos = @startPos + 1
											SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									ELSE
										BEGIN
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
				
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END

									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @guid UNIQUEIDENTIFIER
													SET @var4 = @var3
													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													DECLARE @start INT
													SET @start = 1
								  					WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															DECLARE @gu_id NVARCHAR(MAX)
															IF ISNUMERIC(@listVal) = 0
																BEGIN
																	SET @gu_id = 'NULL' 
																END
															ELSE
																BEGIN
																	SELECT @guid = guid FROM items WHERE item_id = @listVal
																	SET @gu_id = @guid
																	IF @gu_id IS NULL
																		BEGIN
																			SET @gu_id = 'NULL'
																		END
																END
															SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

															SET @start = @start + LEN(@gu_id) + 1

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @colId NVARCHAR(MAX)
													DECLARE @colVar NVARCHAR(MAX)
													SET @var4 = @var3
													DECLARE @delimited2 CURSOR
													SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

													OPEN @delimited2
													FETCH NEXT FROM @delimited2 INTO @colId

													DECLARE @start2 INT
													SET @start2 = 1
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF ISNUMERIC(@colId) = 1
																BEGIN
																	IF @subType = 0
																		BEGIN
																			SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 1
																		BEGIN
																			SET @colVar = dbo.processPortfolio_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 2
																		BEGIN
																			SET @colVar = dbo.condition_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 3
																		BEGIN
																			SET @colVar = dbo.processAction_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 4
																		BEGIN
																			SET @colVar = dbo.processActionDialog_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 5
																		BEGIN
																			SET @colVar = dbo.processTab_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 6
																		BEGIN
																			SET @colVar = dbo.processContainer_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 7
																		BEGIN
																			SET @colVar = dbo.processDashboard_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 8
																		BEGIN
																			SET @colVar = dbo.processDashboardChart_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 9
																		BEGIN
																			SET @colVar = dbo.calendar_GetVariable(CAST(@colId AS INT))
																		END
																	IF @colVar IS NULL
																		BEGIN
																			SET @colVar = 'NULL' 
																		END
																	SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
																	SET @start2 = @start2 + LEN(@colVar) + 1
																END
															FETCH NEXT FROM @delimited2 INTO @colId
														END
													CLOSE @delimited2

													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									IF @type = 2
										BEGIN
											IF LEFT(LTRIM(@var3), 5) = '"tab_'
												BEGIN
													SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
													IF ISNUMERIC(@var3) = 1 
														BEGIN
															IF CAST(@var3 AS INT) > 0
																BEGIN
																	SET @var4 = dbo.processTab_GetVariable(@var3)
																	IF @var4 IS NOT NULL
																		BEGIN
																			SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																		END
																END
														END
													ELSE
														BEGIN
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
														END
												END
										END	
										
									SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos + @strToFoundLength)
								END
							FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0),
						('fromProcessColumnId', 0),
						('targetProcessColumnId', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
				
									IF @type = 0
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 1
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processPortfolio_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 2
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
				
			IF @data_type = 21
				BEGIN
					INSERT INTO @strTable2 (str_to_found2, strtype2) 
					VALUES
						('"action":', 2),
						('"containerId":', 3)
	
					SET @getStrs2 = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found2, strtype2 FROM @strTable2
			
					OPEN @getStrs2
					FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound2)
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 2
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 3
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processContainer_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2, @startPos + 1)
								END
							FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
		
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0 
										BEGIN
											IF LEFT(LTRIM(@var3), 5) = '"tab_'
												BEGIN
													SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
													IF ISNUMERIC(@var3) = 1 
														BEGIN
															IF CAST(@var3 AS INT) > 0
																BEGIN
																	SET @var4 = dbo.processTab_GetVariable(@var3)
																	IF @var4 IS NOT NULL
																		BEGIN
																			SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																		END
																END
														END
													ELSE
														BEGIN
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processActionDialog_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs2')) >= -1
		BEGIN
			DEALLOCATE @getStrs2
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs3')) >= -1
		BEGIN
			DEALLOCATE @getStrs3
		END

	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[menu_VarReplace]
(
	@instance NVARCHAR(10),
	@params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @params IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('MenuId"', 1, 9),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
					
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType

			SET @startPos = 1
			WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @params, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 1

							IF CHARINDEX('[', @params, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
									
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
								END
									
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							SET @var3 = REPLACE(@var3, '"', '')
									
							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @listItemId INT
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
													
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @listVal = 'NULL' OR ISNUMERIC(@listVal) = 1
														BEGIN
															SET @listItemId = 0
														END
													ELSE 
														BEGIN
															SELECT @listItemId = item_id FROM items WHERE guid = @listVal
															IF @listItemId IS NULL
																BEGIN
																	SET @listItemId = 0
																END
														END
													SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 1 
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @colId INT
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colVar

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @colVar = 'NULL' OR ISNUMERIC(@colVar) = 1
														BEGIN
															SET @colId = 0
														END
													ELSE 
														BEGIN
															IF @subType = 0
																BEGIN
																	SET @colId = dbo.itemColumn_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colId = dbo.processPortfolio_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colId = dbo.condition_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colId = dbo.processAction_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colId = dbo.processActionDialog_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colId = dbo.processTab_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colId = dbo.processContainer_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colId = dbo.processDashboard_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colId = dbo.processDashboardChart_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 9
																BEGIN
																	SET @colId = dbo.menu_GetId(@instance, @colVar)
																END

															IF @colId IS NULL
																BEGIN
																	SET @colId = 0
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colVar), CAST(@colId AS NVARCHAR(MAX)))
															SET @start2 = @start2 + LEN(CAST(@colId AS NVARCHAR(MAX))) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colVar
												END
											CLOSE @delimited2
										END

									SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)											
								END
									
							IF @type = 2 
								BEGIN
									IF LEFT(@var3, 4) = 'tab_'
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = ' ""'
														END
													ELSE
														BEGIN
															SET @var4 = ' "tab_' + @var4 + '"'
														END
													SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END

							IF @type = 3
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0'
										BEGIN
											SELECT @var4 = calendar_id FROM calendars WHERE base_calendar = 1 AND REPLACE(calendar_name, ' ', '_') = @var3
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
		
							SET @startPos = CHARINDEX(@strToFound, @params, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @params
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[instanceConfiguration_ValueVarReplace]
(
	@instance NVARCHAR(MAX),
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @value IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
					
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType

			SET @startPos = 1
			WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @value, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 2

							IF CHARINDEX('[', @value, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
									
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos2 - @startPos)
								END
									
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @listItemId INT
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
													
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @listVal = 'NULL' OR ISNUMERIC(@listVal) = 1
														BEGIN
															SET @listItemId = 0
														END
													ELSE 
														BEGIN
															SELECT @listItemId = item_id FROM items WHERE guid = @listVal
															IF @listItemId IS NULL
																BEGIN
																	SET @listItemId = 0
																END
														END
													SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 1 
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @colId INT
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colVar

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @colVar = 'NULL' OR ISNUMERIC(@colVar) = 1
														BEGIN
															SET @colId = 0
														END
													ELSE 
														BEGIN
															IF @subType = 0
																BEGIN
																	SET @colId = dbo.itemColumn_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colId = dbo.processPortfolio_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colId = dbo.condition_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colId = dbo.processAction_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colId = dbo.processActionDialog_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colId = dbo.processTab_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colId = dbo.processContainer_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colId = dbo.processDashboard_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colId = dbo.processDashboardChart_GetId(@instance, @colVar)
																END

															IF @colId IS NULL
																BEGIN
																	SET @colId = 0
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colVar), CAST(@colId AS NVARCHAR(MAX)))
															SET @start2 = @start2 + LEN(CAST(@colId AS NVARCHAR(MAX))) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colVar
												END
											CLOSE @delimited2
										END

									SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)											
								END
									
							IF @type = 2 
								BEGIN
									IF LEFT(@var3, 4) = 'tab_'
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = ' ""'
														END
													ELSE
														BEGIN
															SET @var4 = ' "tab_' + @var4 + '"'
														END
													SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END

							IF @type = 3
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0'
										BEGIN
											SELECT @var4 = calendar_id FROM calendars WHERE base_calendar = 1 AND REPLACE(calendar_name, ' ', '_') = @var3
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
		
							SET @startPos = CHARINDEX(@strToFound, @value, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @value
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @strToFound2 NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @strTable2 TABLE (str_to_found2 NVARCHAR(MAX), strtype2 INT)
	DECLARE @strTable3 TABLE (str_to_found3 NVARCHAR(MAX), strtype3 INT, strsubtype3 INT)
	DECLARE @getStrs CURSOR
	DECLARE @getStrs2 CURSOR
	DECLARE @getStrs3 CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN
			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable3 (str_to_found3, strtype3, strsubtype3)
					VALUES
						('ItemId"', 0, 0),
						('ColumnId"', 1, 0),
						('PortfolioId"', 1, 1),
						('ConditionId"', 1, 2),
						('ActionId"', 1, 3),
						('ActionDialogId"', 1, 4),
						('TabId"', 1, 5),
						('ContainerId"', 1, 6),
						('DashboardId"', 1, 7),
						('ChartId"', 1, 8),
						('CalendarId"', 1, 9),
						('AttachmentId"', 1, 10),
						('State"', 2, 0)

					SET @getStrs3 = CURSOR FAST_FORWARD LOCAL FOR
					SELECT str_to_found3, strtype3, strsubtype3 FROM @strTable3
					
					OPEN @getStrs3
					FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
					
					SET @startPos = 1
					WHILE (@@FETCH_STATUS = 0)
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos)
							WHILE @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 1

									IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
										BEGIN
											SET @startPos = @startPos + 1
											SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									ELSE
										BEGIN
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @listItemId INT
													SET @var4 = @var3
													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
													
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' OR ISNUMERIC(@listVal) = 1
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @colId INT
													DECLARE @colVar NVARCHAR(MAX)
													SET @var4 = @var3
													DECLARE @delimited2 CURSOR
													SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

													OPEN @delimited2
													FETCH NEXT FROM @delimited2 INTO @colVar

													DECLARE @start2 INT
													SET @start2 = 1
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @colVar = 'NULL' OR ISNUMERIC(@colVar) = 1
																BEGIN
																	SET @colId = 0
																END
															ELSE 
																BEGIN
																	IF @subType = 0
																		BEGIN
																			SET @colId = dbo.itemColumn_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 1
																		BEGIN
																			SET @colId = dbo.processPortfolio_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 2
																		BEGIN
																			SET @colId = dbo.condition_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 3
																		BEGIN
																			SET @colId = dbo.processAction_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 4
																		BEGIN
																			SET @colId = dbo.processActionDialog_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 5
																		BEGIN
																			SET @colId = dbo.processTab_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 6
																		BEGIN
																			SET @colId = dbo.processContainer_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 7
																		BEGIN
																			SET @colId = dbo.processDashboard_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 8
																		BEGIN
																			SET @colId = dbo.processDashboardChart_GetId(@instance, @colVar)
																		END
																	ELSE IF @subType = 9
																		BEGIN
																			SET @colId = dbo.calendar_GetId(@instance, @colVar)
																		END
																	IF @colId IS NULL
																		BEGIN
																			SET @colId = 0
																		END
																	SET @var4 = STUFF(@var4, @start2, LEN(@colVar), CAST(@colId AS NVARCHAR(MAX)))
																	SET @start2 = @start2 + LEN(CAST(@colId AS NVARCHAR(MAX))) + 1
																END
															FETCH NEXT FROM @delimited2 INTO @colVar
														END
													CLOSE @delimited2
												END

											SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)											
										END
									
									IF @type = 2 
										BEGIN
											IF LEFT(@var3, 4) = 'tab_'
												BEGIN
													IF LEN(@var3) > 0
														BEGIN
															SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
															IF @var4 IS NULL
																BEGIN
																	SET @var4 = ' ""'
																END
															ELSE
																BEGIN
																	SET @var4 = ' "tab_' + @var4 + '"'
																END
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
		
									SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos + @strToFoundLength)
								END
							FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
						END
				END
			
			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0),
						('fromProcessColumnId', 0),
						('targetProcessColumnId', 0)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 2
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
						
			IF @data_type = 21
				BEGIN
					INSERT INTO @strTable2 (str_to_found2, strtype2) 
					VALUES
						('"action":', 2),
						('"containerId":', 3)
					
					SET @getStrs2 = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found2, strtype2 FROM @strTable2 
					
					OPEN @getStrs2
					FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound2)
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 2
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 3
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processContainer_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2, @startPos + 1)
								END
							FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
						END
				END
			
			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2

									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEFT(@var3, 4) = 'tab_'
												BEGIN
													IF LEN(@var3) > 0
														BEGIN
															SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
															IF @var4 IS NULL
																BEGIN
																	SET @var4 = ' ""'
																END
															ELSE
																BEGIN
																	SET @var4 = ' "tab_' + @var4 + '"'
																END
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
									
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = @var3
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs2')) >= -1
		BEGIN
			DEALLOCATE @getStrs2
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs3')) >= -1
		BEGIN
			DEALLOCATE @getStrs3
		END

	RETURN @dataAdditional2
END