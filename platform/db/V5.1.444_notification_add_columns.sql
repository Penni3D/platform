DECLARE @Instance NVARCHAR(255);
DECLARE @RightContainerId INT;
DECLARE @SelectorId INT;
DECLARE cursor_process CURSOR

FOR SELECT instance FROM instances 

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
      

	  SET @RightContainerId = (SELECT process_container_id FROM process_containers WHERE right_container = 1 AND instance = @Instance AND process = 'notification')
	
	EXEC dbo.app_ensure_column @Instance,'notification','start',@RightContainerId,3,0,0,null,null,0
	
  
        FETCH NEXT FROM cursor_process INTO 
            @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
	