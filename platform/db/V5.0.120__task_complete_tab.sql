﻿exec app_set_archive_user_id 0

  DECLARE @instance NVARCHAR(10)
  DECLARE instance_Cursor CURSOR FOR SELECT instance FROM instances
  
  OPEN instance_Cursor

  FETCH NEXT FROM instance_Cursor INTO @instance

  WHILE @@fetch_status = 0
    BEGIN
	
	--Create conditions
  
    DECLARE @json NVARCHAR(MAX)
    DECLARE @typeCol INT
    DECLARE @statusCol INT
    DECLARE @statusVal INT
    
    SELECT @typeCol = item_column_id FROM item_columns WHERE instance = @instance AND process = 'task' AND name = 'task_type'
    SELECT @statusCol = item_column_id FROM item_columns WHERE instance = @instance AND process = 'task' AND name = 'task_status'
    SELECT @statusVal = MAX(item_id) FROM items WHERE instance = @instance AND process = 'list_task_status'     

  	DECLARE @con13 INT
	  EXEC app_ensure_condition @instance, 'task', 0, 0, 'COMPLETE_TAB_WRITE', @condition_id = @con13 OUTPUT

    SET @json = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@typeCol as NVARCHAR(max)) + ',"Value":"21"},"OperatorId":null},{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":6,"ItemColumnId":' + CAST(@statusCol as NVARCHAR(max)) + ',"Value":' + CAST(@statusVal as NVARCHAR(max)) + '},"OperatorId":1}]},"OperatorId":1}]'

    UPDATE conditions SET condition_json = @json  WHERE condition_id = @con13
    
  	INSERT INTO condition_states (condition_id, state) VALUES (@con13, 'write')

	  insert into condition_groups (condition_id, process_group_id) VALUES(@con13, (SELECT process_group_id FROM process_groups WHERE instance = @instance AND variable = 'ALL'))
	  insert into condition_tabs (condition_id, process_tab_id)  VALUES(@con13, (SELECT process_tab_id FROM process_tabs WHERE instance = @instance AND variable = 'SPRINT_COMPLETE'))


	  EXEC app_ensure_condition @instance, 'task', 0, 0, 'COMPLETE_TAB_READ', @condition_id = @con13 OUTPUT
 
     SET @json = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@typeCol as NVARCHAR(max)) + ',"Value":"21"},"OperatorId":null}]},"OperatorId":1}]' 

    UPDATE conditions SET condition_json = @json  WHERE condition_id = @con13
    
  	INSERT INTO condition_states (condition_id, state) VALUES (@con13, 'read')

	  insert into condition_groups (condition_id, process_group_id) VALUES(@con13, (SELECT process_group_id FROM process_groups WHERE instance = @instance AND variable = 'ALL'))
	  insert into condition_tabs (condition_id, process_tab_id)  VALUES(@con13, (SELECT process_tab_id FROM process_tabs WHERE instance = @instance AND variable = 'SPRINT_COMPLETE'))


      FETCH  NEXT FROM instance_Cursor INTO @instance
    END

GO 

exec app_set_archive_user_id 0

  	DECLARE @instance NVARCHAR(10)
  	DECLARE @process NVARCHAR(50)
  	DECLARE @group int
	DECLARE @i1 INT	 
	DECLARE @historyDateColName nvarchar(max)

  	DECLARE instance_Cursor2 CURSOR FOR select instance, process, process_group_id from process_groups where history_date_col is null and process in (select process from processes where process_type = 0)

  
  OPEN instance_Cursor2

  FETCH NEXT FROM instance_Cursor2 INTO @instance, @process, @group

  WHILE @@fetch_status = 0
    BEGIN
	 	set @historyDateColName = 'g_' + CAST(@group AS NVARCHAR) +  '_date'	
          EXEC app_ensure_column @instance, @process, @historyDateColName, 0, 13, 1, 1, 0, null, 0, 0, 0, @item_column_id = @i1 OUTPUT

			update process_groups set history_date_col = @i1 WHERE process_group_id = @group
      FETCH  NEXT FROM instance_Cursor2 INTO @instance, @process, @group
    END




