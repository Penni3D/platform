CREATE PROCEDURE [dbo].[app_ChangeColumnDataType]
    @tableName NVARCHAR(255), @columnName NVARCHAR(50), @DataTypeName NVARCHAR(25)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @sql NVARCHAR(MAX)
    WHILE 1=1
        BEGIN
            SELECT TOP 1 @sql = N'alter table ' + @tableName + ' drop constraint [' + dc.NAME + N']'
            from sys.default_constraints dc
                     JOIN sys.columns c
                          ON c.default_object_id = dc.object_id
            WHERE
                    dc.parent_object_id = OBJECT_ID(@tableName)
              AND c.name = @columnName
            IF @@ROWCOUNT = 0 BREAK
            EXEC (@sql)
        END

    SET @sql = 'ALTER TABLE ' + @tableName + ' ALTER COLUMN ' + @columnName + ' ' + @DataTypeName
    EXEC (@sql)
END
GO
EXEC app_ChangeColumnDataType 'instance_helpers', 'desc_variable', 'NVARCHAR(MAX)'
GO
INSERT INTO instance_keywords (keyword) VALUES ('internal_order')