GO
/****** Object:  StoredProcedure [dbo].[app_ensure_recursive_list]    Script Date: 5.2.2021 14.07.16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_recursive_list]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10), 	@process nvarchar(50)
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	--DECLARE @process NVARCHAR(50) = 'user'
	DECLARE @combine TINYINT = 0
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	SET @tableName = '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

			--Create group
			DECLARE @g1 INT
			EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g1 OUTPUT
		
			--Create tabs
			DECLARE @t1 INT
			EXEC app_ensure_tab @instance,@process,'TAB', @g1, @tab_id = @t1 OUTPUT
		
			--Create portfolios
			DECLARE @p1 INT
			EXEC app_ensure_portfolio @instance,@process,'PORTFOLIO', @portfolio_id = @p1 OUTPUT
			
			update process_portfolios set process_portfolio_type = 1 where process_portfolio_id = @p1
			
			--Create conditions
			DECLARE @cId INT	
			EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT

			INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'portfolio.write')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'portfolio.read')

			insert into process_portfolio_columns (process_portfolio_id, item_column_id)  select @p1, item_column_id from item_columns where instance = @instance and process = @process and item_column_id NOT IN (select item_column_id from process_portfolio_columns where process_portfolio_id = @p1 )

			exec app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 0, 0, '', 0, 0, null, null
	        
			update item_columns set status_column = 1 where instance = @instance and process = @process and name = 'parent_item_id'


			set @sql = 'update ' +  @tableName + ' set parent_item_id = 0 '
			exec(@sql)
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_user]    Script Date: 5.2.2021 14.08.14 ******/

/****** Object:  StoredProcedure [dbo].[app_ensure_process_organisation]    Script Date: 5.2.2021 14.13.52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_organisation]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'organisation'
	DECLARE @itemId INT
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;
	DECLARE @i1 INT

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'Organisation', 0, 1, @container_id = @c1 OUTPUT

	--Create fields
	EXEC app_ensure_column @instance, @process, 'name', @c1, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT
	EXEC app_ensure_column @instance, @process, 'type', 0, 1, 1, 1, 0, null,  0, 0



	--Create portfolio

	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'Organisation', @portfolio_id = @p1 OUTPUT

	SET @sql = 'UPDATE process_portfolios SET process_portfolio_type = ''1'' WHERE process_portfolio_id = ' +  CAST(@p1 AS nvarchar) 
	EXEC (@sql)


	--Create portfolio columns
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter) VALUES (@p1, @i1, 'V', 1, 1);

	--Create condition
	DECLARE @con1 INT
	EXEC app_ensure_condition @instance, @process, 0, 0, 'all', @condition_id = @con1 OUTPUT
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'portfolio.read');
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'portfolio.write');
	INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio');
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con1, @p1);
	INSERT INTO condition_containers (condition_id, process_container_id) VALUES (@con1, @c1);

	SET @tableName = '_' + @instance + '_' + @process
	INSERT INTO items (instance, process) VALUES (@instance, @process);
	SET @itemId = @@identity;
	SET @sql = 'UPDATE ' + @tableName + ' SET name = ''Default organisation'', type = ''0'' WHERE item_id = ' +  CAST(@itemId AS nvarchar) 
	EXEC (@sql)

	--User process organisation fields	

	DECLARE @g2 INT
	SELECT @g2 = process_group_id FROM process_groups WHERE instance = @instance AND process = 'user' AND variable = 'USER';

	DECLARE @c4 INT
	SELECT @c4 = ISNULL(process_container_id, 0) FROM process_containers WHERE instance = @instance AND process = 'user' AND variable = 'organisation_management';

	IF (@c4 IS NULL)
	BEGIN
		--Create new User process tab
		DECLARE @t2 INT
		EXEC app_ensure_tab @instance,'user','USER_TAB2', @g2, @tab_id = @t2 OUTPUT

		--Create new User process container	
		EXEC app_ensure_container @instance,'user','organisation_management', @t2, 1, @container_id = @c4 OUTPUT

	END

	--Create organisation field	
	DECLARE @i2 INT
	EXEC app_ensure_column @instance, 'user', 'organisation', @c4, 5, 1, 1, 0, null,  0, 0, 'organisation', @item_column_id = @i2 OUTPUT
	UPDATE process_container_columns SET validation = '{"max":"1"}' WHERE item_column_id = @i2

END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_conditions]    Script Date: 5.2.2021 14.14.48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_conditions]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @conditionId INT

		DECLARE @langVar NVARCHAR (50) = 'CONDITION_' + 'user' + '_' + 'ALL'

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, 'user', 'ALL', 'null')
	SET @conditionId = @@IDENTITY

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user',UPPER(@langVar), 'all', 'en-GB')
		END

	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, (SELECT TOP 1 process_group_id FROM process_groups WHERE instance=@instance AND process = 'user'))
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, (SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE instance=@instance AND process = 'user'))

		SET @langVar = 'CONDITION_' + 'user_group' + '_' + 'ALL'

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, 'user_group', 'ALL', 'null')
	SET @conditionId = @@IDENTITY

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group',UPPER(@langVar), 'all', 'en-GB')
		END

	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'meta.write')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'meta.read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, (SELECT TOP 1 process_group_id FROM process_groups WHERE instance=@instance AND  process = 'user_group'))
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, (SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE instance=@instance AND process = 'user_group'))
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_comment]    Script Date: 5.2.2021 14.16.56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_comment]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'comment'
	DECLARE @tableName NVARCHAR(50) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create conditions
	EXEC app_ensure_condition @instance, @process, 0, 0
	
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'COMMENTS', @portfolio_id = @p1 OUTPUT

	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'comment_owner', 0, 5, 1, 1, 0, null,  0, 0, 'user', null, @item_column_id = @i1 OUTPUT
	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'comment_published', 0, 1, 1, 1, 0, null,  0, 0, null, null, @item_column_id = @i2 OUTPUT

	EXEC app_ensure_column @instance, @process, 'comment_text', 0, 0, 1, 1, 0, null,  0, 0

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'comment_created_date', 0, 13, 1, 1, 0, null,  0, 0,  null, null, @item_column_id = @i3 OUTPUT
	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'comment_published_date', 0, 13, 1, 1, 0, null,  0, 0, null, null, @item_column_id = @i4 OUTPUT
	
	--Create conditions
	DECLARE @c1 AS NVARCHAR(MAX)

	DECLARE @cId1 INT
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i2 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'READ', @c1, @condition_id = @cId1 OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId1, 'portfolio.read')

	DECLARE @cId2 INT
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":3,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"UserColumnTypeId":2,"ComparisonTypeId":3},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i2 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'WRITE', @c1, @condition_id = @cId2 OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId2, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId2, 'portfolio.write')
	
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'CommentsPortfolioId', @p1, 0, null

	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 

	--Create actions
	DECLARE @actionId INT
	exec app_ensure_action @instance, @process, 'NEW_ROW', @process_action_id = @actionId OUTPUT
	exec app_ensure_action_row @actionId, 0, @i2, 0, '0'
	exec app_ensure_action_row @actionId, 0, @i1, 2, null
	exec app_ensure_action_row @actionId, 0, @i3, 1, null

	update processes set new_row_action_id = @actionId WHERE instance = @instance and process = @process

	DECLARE @actionId2 INT
	exec app_ensure_action @instance, @process, 'PUBLISH_COMMENT', @process_action_id = @actionId2 OUTPUT
	exec app_ensure_action_row @actionId2, 0, @i2, 0, '1'
	exec app_ensure_action_row @actionId2, 0, @i4, 1, null

	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'CommentsPublishActionId', @actionId2, 0, null
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_costs]    Script Date: 5.2.2021 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_costs]  -- Add the parameters for the stored procedure here
    @instance NVARCHAR(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'costs'

    --Archive user is 0
    DECLARE @BinVar VARBINARY(128);
    SET @BinVar = CONVERT(VARBINARY(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_process @instance, @process, @process, 2

    EXEC dbo.app_ensure_column @instance, @process, 'idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'year', 0, 1, 1, 0, 0, NULL, 0, 0
        EXEC dbo.app_ensure_column @instance, @process, 'month', 0, 1, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'external_workservices_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'external_workservices_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'external_workservices_actual', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'internal_work_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'internal_work_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'internal_work_actual', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'machinesequipments_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'machinesequipments_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'machinesequipments_actual', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'other_internal_costs_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'other_internal_costs_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'other_internal_costs_actual', 0, 2, 1, 0, 0, NULL, 0, 0

  END
  
GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_customer]    Script Date: 5.2.2021 14.19.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_customer]
  @instance nvarchar(10), 	@process nvarchar(50), @processType as int, @colour nvarchar(50), @listProcess int,  @listOrder nvarchar(50), @newRowActionId int
AS
BEGIN
  DECLARE @tab_id INT, @item_column_id INT
  DECLARE @combine TINYINT = 0
  DECLARE @tableName NVARCHAR(50) = ''
  DECLARE @sql NVARCHAR(MAX) = ''

  SET @tableName = '_' + @instance + '_' + @process

  --Archive user is 0
  DECLARE @BinVar varbinary(128);
  SET @BinVar = CONVERT(varbinary(128), 0);
  SET CONTEXT_INFO @BinVar;

  IF NOT EXISTS (SELECT * FROM processes WHERE instance = @instance and process = @process)
    BEGIN

      --Create process
      EXEC app_ensure_process @instance, @process, @process, @processType, @colour, @listProcess, @listOrder, @newRowActionId

      IF @processType = 0 OR @processType = 5
        BEGIN

          --Create group
          DECLARE @g1 INT
          DECLARE @var nvarchar(255) = 'PHASE_' + @process + '_PHASE'
          EXEC app_ensure_group @instance,@process, @var, @group_id = @g1 OUTPUT

          --Create tabs
          DECLARE @t1 INT
          SET @var = 'TAB_' + @process + '_TAB'
          EXEC app_ensure_tab @instance,@process, @var, @g1, @tab_id = @t1 OUTPUT

          --Create containers
          DECLARE @c1 INT
          SET @var = '_CONTAINER' + @process + '_CONTAINER'
          EXEC app_ensure_container @instance,@process, @var, @t1, 1, @container_id = @c1 OUTPUT

          --Create portfolios
          DECLARE @p1 INT
          SET @var = 'PORTFOLIO_' + @process + '_PORTFOLIO'
          EXEC app_ensure_portfolio @instance,@process,@var, @portfolio_id = @p1 OUTPUT

          --Create conditions
          DECLARE @cId INT
          SET @var = 'CONDITION_' + @process + '_ALL'
          EXEC app_ensure_condition @instance, @process, @g1, @p1, @var, 'null', @condition_id = @cId OUTPUT

          INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
          INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'portfolio.write')
          INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'portfolio.read')

          --Create columns
          EXEC app_ensure_column @instance,	@process, 'title', @c1,	0, 0, 0, null, @p1, 0, null, null, 0, 0

 		   DECLARE @i1 INT
  		 	EXEC app_ensure_column @instance, @process, 'current_state', 0, 0, 1, 1, 0, null, 0, 0, 0, @item_column_id = @i1 OUTPUT
			update item_columns set status_column = 1 where item_column_id = @i1
	
          --Create menu
          DECLARE @instanceMenuId INT,
          @instanceMenuId2 INT

          INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, null, @process, '0', null, '{}')
          SET @instanceMenuId = @@IDENTITY

          INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, link_type, icon) VALUES(@instance, @instanceMenuId, 'NEW_' + @process, '0', 'new.process', '{"process":"' + @process + '"}', 2,'add')
          SET @instanceMenuId2 = @@IDENTITY
          SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group ORDER BY item_id ASC)); '
          EXEC (@sql)
		  
          INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, link_type, icon) VALUES(@instance, @instanceMenuId, 'PORTFOLIO_' + @process, '0', 'portfolio', '{"process":"' + @process + '","portfolioId":' + cast(@p1 as nvarchar(max)) + '}', 1, 'folder')
          SET @instanceMenuId2 = @@IDENTITY
          SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group ORDER BY item_id ASC)); '
          EXEC (@sql)
		  
 
		  
		DECLARE @Code NVARCHAR(255);
		DECLARE cursor_process CURSOR
				FOR SELECT code FROM instance_languages 
			OPEN cursor_process

			FETCH NEXT FROM cursor_process INTO @Code
			WHILE @@FETCH_STATUS = 0
		BEGIN
		
		INSERT INTO process_translations (instance,process,code,variable,translation)
		VALUES (@instance, @process, @Code, 'NEW_' + @process, 'New ' + @process)
		
		INSERT INTO process_translations (instance,process,code,variable,translation)
		VALUES (@instance, @process, @Code, 'PORTFOLIO_' + @process, 'Portfolio ' + @process)
      
		
        FETCH NEXT FROM cursor_process INTO 
            @Code
		END;
	
			CLOSE cursor_process;
		DEALLOCATE cursor_process;

	  
		  
        END

    END

END


