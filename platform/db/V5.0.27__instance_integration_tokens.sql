CREATE TABLE [dbo].[instance_integration_tokens] (
  [instance_integration_token_id] int IDENTITY(1,1) NOT NULL,
  [key] nvarchar(255) NOT NULL,
  [token] nvarchar(255) NOT NULL,
  [valid_until] datetime DEFAULT NULL NULL
)
GO

ALTER TABLE [dbo].[instance_integration_tokens] SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE [dbo].[instance_integration_tokens] ADD CONSTRAINT [PK__instance__1E80B91C7B7948B4] PRIMARY KEY CLUSTERED ([instance_integration_token_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

ALTER TABLE [dbo].[instance_integration_tokens] ADD CONSTRAINT [FK_keys] FOREIGN KEY ([key]) REFERENCES [instance_integration_keys] ([key]) ON DELETE CASCADE ON UPDATE NO ACTION
GO
