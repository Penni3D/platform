SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceScheduleGroup_GetId]
(
	@instance NVARCHAR(10),
	@variable NVARCHAR(255)
)
RETURNS INT
AS
BEGIN
	DECLARE @id INT
	SELECT TOP 1 @id = group_id FROM instance_schedule_groups WHERE instance = @instance AND variable = @variable
	RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceScheduleGroup_GetVariable]
(
	@groupId INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	SELECT TOP 1 @var = variable FROM instance_schedule_groups WHERE group_id = @groupId
	RETURN @var
END