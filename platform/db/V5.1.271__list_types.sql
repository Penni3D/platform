ALTER TABLE processes
  ADD [list_type] INT DEFAULT NULL
GO
EXEC app_remove_archive 'processes'
EXEC app_ensure_archive 'processes'

GO
UPDATE processes SET [list_type] = NULL

GO
DECLARE @instance nvarchar(10)
DECLARE @process nvarchar(50)
DECLARE @update_itemid int
DECLARE @sql nvarchar(max)

DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
		SET @process = 'list_admin_types'
		EXEC app_ensure_list @instance, @process, 'list_admin_types', 0
		EXEC app_ensure_column @instance,@process,'is_admin',0,1,0,0,0,'',0,0,'','',0,0
		EXEC app_ensure_column @instance,@process,'parent_item_id',0,1,0,0,0,'',0,0,'','',0,0
		EXEC app_ensure_column @instance,@process,'system_item',0,1,0,0,0,'',0,0,'','',0,0
		EXEC app_ensure_column @instance,@process,'in_use',0,1,0,0,0,'',0,0,'','',0,0
		EXEC app_ensure_column @instance,@process,'list_item',0,0,0,0,0,'',0,0,'','',0,0
		EXEC app_ensure_column @instance,@process,'list_symbol',0,0,0,0,0,'',0,0,'','',0,0
		EXEC app_ensure_column @instance,@process,'list_symbol_fill',0,0,0,0,0,'',0,0,'','',0,0
		DECLARE @table NVARCHAR(100) = '_' + @instance + '_' + @process
FETCH  NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor

GO
DECLARE @instance nvarchar(10)
DECLARE @process nvarchar(50)
DECLARE @update_itemid int
DECLARE @sql nvarchar(max)

DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
		SET @process = 'list_admin_types'
		DECLARE @table NVARCHAR(100) = '_' + @instance + '_' + @process
		 --DECLARE @update_itemid INT
		INSERT INTO items (instance, process) VALUES (@instance, @process);
        SET @update_itemid = @@identity;
        
        SET @sql = 'UPDATE ' + @table + ' SET
                                        list_item = ''admin'',                                     
										system_item = 1,
										is_admin = 1 WHERE item_id = ' + CAST(@update_itemid AS NVARCHAR)
										
		EXEC (@sql)
	
FETCH  NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor

