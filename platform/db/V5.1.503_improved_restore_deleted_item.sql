SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
    AS
    SET XACT_ABORT OFF

DECLARE @delete_timestamp DATETIME = getUTCdate()
DECLARE @ids NVARCHAR(MAX) = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM deleted FOR XML PATH(''))
DECLARE @sql NVARCHAR(MAX)
DECLARE @tName NVARCHAR(MAX)
DECLARE @cName NVARCHAR(MAX)
DECLARE @lvl AS INT = 0

IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
CREATE TABLE #DeleteItems (item_id INT, ord INT)

DECLARE @GetTables CURSOR
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
    SELECT ccu.TABLE_NAME, ccu.COLUMN_NAME 
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
    INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
    WHERE UNIQUE_CONSTRAINT_NAME = 'PK_items' AND SUBSTRING(ccu.TABLE_NAME, 1, 1) = '_'

DECLARE @idps_childs NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 2 FOR XML PATH(''))
DECLARE @idps_parents NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 3 FOR XML PATH(''))

WHILE @ids IS NOT NULL BEGIN
	IF OBJECT_ID('tempdb..#LoopItems') IS NOT NULL DROP TABLE #LoopItems
	CREATE TABLE #LoopItems (item_id INT, ord INT)

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @sql = ' WITH rec AS ( 
			 SELECT t.item_id, t.' + @cName + ', ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			 FROM ' + @tName + ' t 
			 WHERE t.' + @cName + ' IN (-1' + @ids + ')
			 ) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
		EXECUTE (@sql)
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

	SET @sql = ' WITH rec AS ( 
			SELECT t.selected_item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_childs + ') AND item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT selected_item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @sql = ' WITH rec AS ( 
			SELECT t.item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_parents + ') AND selected_item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @ids = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems) FOR XML PATH(''))

	INSERT INTO #DeleteItems (item_id, ord) SELECT item_id, ord FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems)

	SET @lvl = @lvl + 1
END

DELETE  FROM item_subitems 				  WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_tabs_subprocess_data WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM condition_user_groups WHERE  item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_baseline WHERE  process_item_id IN (SELECT item_id FROM #DeleteItems)

DECLARE @delId AS INT
DECLARE @GetIds CURSOR

SET @GetIds = CURSOR FOR SELECT item_id FROM #DeleteItems GROUP BY item_id, ord ORDER BY ord DESC

OPEN @GetIds
FETCH NEXT FROM @GetIds INTO @delId

WHILE @@FETCH_STATUS = 0 BEGIN
	INSERT INTO items_deleted (item_id, instance, process, deleted_date, [guid]) SELECT item_id, instance, process, @delete_timestamp, [guid] FROM items WHERE item_id = @delId
    DELETE FROM items WHERE item_id = @delId
    FETCH NEXT FROM @GetIds INTO @delId
END
CLOSE @GetIds

DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
UPDATE dd SET dd.link_item_id = NULL FROM item_data_process_selections dd INNER JOIN deleted d ON dd.link_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id

DELETE dd FROM instance_user_configurations_saved dd INNER JOIN deleted d ON dd.user_id = d.item_id
DELETE dd FROM instance_user_configurations dd INNER JOIN deleted d ON dd.user_id = d.item_id

DELETE dd FROM condition_user_groups dd INNER JOIN deleted d ON dd.item_id = d.item_id

DELETE dd FROM process_baseline dd INNER JOIN deleted d ON dd.process_item_id = d.item_id

--Insert into deleted items
INSERT INTO items_deleted (item_id, instance, process, deleted_date, [guid]) SELECT item_id, instance, process, @delete_timestamp, [guid] FROM deleted

--Finally delete the item
DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[item_data_process_selections_INSTEAD_OF_INSERT]
	ON [dbo].[item_data_process_selections]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[item_data_process_selections] ([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_start], archive_userid) 
	SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_start] 
	, @user_id
	FROM inserted

	INSERT INTO archive_item_data_process_selections ([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],[archive_end],[archive_type])
    SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted	



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[item_data_process_selections_AFTER_UPDATE]
    ON [dbo].[item_data_process_selections]
    AFTER UPDATE AS

    DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

    UPDATE [dbo].[item_data_process_selections]
    SET archive_start= getutcdate(),
        archive_userid=  @user_id
    WHERE [item_column_id]+[item_id]+[selected_item_id] IN (SELECT DISTINCT [item_column_id]+[item_id]+[selected_item_id] FROM Inserted)
    
    INSERT INTO archive_item_data_process_selections([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],archive_end, archive_type)
    SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],
           archive_end = getutcdate(),
           archive_type = 1
    FROM deleted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[item_data_process_selections_DELETE]
    ON [dbo].[item_data_process_selections]
    FOR DELETE
    AS
    INSERT INTO archive_item_data_process_selections([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],archive_end, archive_type)
    SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],
           archive_end = getutcdate(),
           archive_type = 1
    FROM deleted

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;
    INSERT INTO archive_item_data_process_selections([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_start],archive_end, archive_type, archive_userid)
    SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_start],
           archive_end = getutcdate(),
           archive_type = 2,
           archive_userid = @user_id
    FROM deleted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[get_item_data_process_selections_with_sel_ids](@d datetime, @iCol int, @selId int)
RETURNS @rtab TABLE ([item_id] int NOT NULL ,[item_column_id] int NOT NULL ,[selected_item_id] int NOT NULL ,[link_item_id] int NULL ,[archive_userid] int NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
AS
BEGIN	
	INSERT @rtab
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start]
		FROM archive_item_data_process_selections WHERE archive_id IN (
			SELECT MAX(archive_id)
			FROM archive_item_data_process_selections
			WHERE selected_item_id = @selId AND item_column_id = @iCol AND archive_start <= @d AND archive_end > @d AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN
			(SELECT CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) FROM item_data_process_selections WHERE selected_item_id = @selId AND item_column_id = @iCol AND archive_start <= @d)
			GROUP BY [item_column_id],[item_id],[selected_item_id]
			)
		UNION 
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start] 
		FROM item_data_process_selections 
		WHERE selected_item_id = @selId AND archive_start <= @d AND item_column_id = @iCol
	RETURN
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[get_item_data_process_selections_with_sel_id](@d datetime, @selId int)
RETURNS @rtab TABLE ([item_id] int NOT NULL ,[item_column_id] int NOT NULL ,[selected_item_id] int NOT NULL ,[link_item_id] int NULL ,[archive_userid] int NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
AS
BEGIN	
	INSERT @rtab
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start]
		FROM archive_item_data_process_selections WHERE archive_id IN (
			SELECT MAX(archive_id)
			FROM archive_item_data_process_selections
			WHERE selected_item_id = @selId AND archive_start <= @d AND archive_end > @d AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN
			(SELECT CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) FROM item_data_process_selections WHERE selected_item_id = @selId AND archive_start <= @d)
			GROUP BY [item_column_id],[item_id],[selected_item_id]
			)
		UNION 
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start] 
		FROM item_data_process_selections 
		WHERE selected_item_id = @selId AND archive_start <= @d
	RETURN
END



GO



INSERT INTO archive_item_data_process_selections (archive_start, item_id, item_column_id, selected_item_id, link_item_id, archive_userid, archive_end, archive_type)
SELECT DATEADD(s, -1, idps.archive_start), idps.item_id, idps.item_column_id, idps.selected_item_id, idps.link_item_id, idps.archive_userid, idps.archive_start, 1
FROM item_data_process_selections idps
LEFT JOIN archive_item_data_process_selections aidps ON aidps.item_id = idps.item_id
AND aidps.item_column_id = idps.item_column_id
AND aidps.selected_item_id = idps.selected_item_id
AND COALESCE(aidps.link_item_id, '') = COALESCE(idps.link_item_id, '')
WHERE aidps.item_id IS NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_restore_item_transaction]
	@item_id INT,
	@archive_id INT = NULL,
	@archive_start DATETIME = NULL
AS
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION
			EXEC app_restore_item @item_id, @archive_id, @archive_start
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK TRAN
		DECLARE @msg NVARCHAR(2048) = 'ROLLBACK - ' + ERROR_MESSAGE();
		THROW 51000, @msg, 1
	END CATCH
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_restore_item]
	@item_id INT,
	@archive_id INT = NULL,
	@archive_start DATETIME = NULL
AS
BEGIN
	DECLARE @user_id INT
	SELECT @user_id = CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @instance NVARCHAR(10)
	DECLARE @process NVARCHAR(50)
	DECLARE @archive INT
	DECLARE @msg NVARCHAR(MAX)
	DECLARE @guid UNIQUEIDENTIFIER
	DECLARE @columns NVARCHAR(MAX)
	DECLARE @mode TINYINT
	DECLARE @item_column_id INT
	DECLARE @data_additional NVARCHAR(MAX)
	DECLARE @data_additional2 NVARCHAR(MAX)
	DECLARE @start_pos INT
	DECLARE @end_pos INT

	DECLARE @child_items_cursor CURSOR
	DECLARE @child_items_item_id INT

	DECLARE @parent_items_cursor CURSOR
	DECLARE @parent_items_item_id INT

	DECLARE @table_columns_cursor CURSOR
	DECLARE @table_items_cursor CURSOR
	DECLARE @table_item_column_id NVARCHAR(255)
	DECLARE @table_instance NVARCHAR(10)
	DECLARE @table_process NVARCHAR(50)
	DECLARE @table_column_name NVARCHAR(50)
	DECLARE @table_data_type INT
	DECLARE @table_item_id INT
	CREATE TABLE #table_items (item_id INT)
	CREATE TABLE #table_item_column_ids (item_column_id INT)
	
	DECLARE @constraints_cursor CURSOR
	DECLARE @constraints_table_name NVARCHAR(50)
	DECLARE @constraints_column_name NVARCHAR(50)
	DECLARE @constraints_item_id INT

	-- Discover archive_id and archive_start and mode
	IF @archive_id IS NOT NULL OR @archive_start IS NOT NULL BEGIN
		SELECT @instance = instance, @process = process FROM items WHERE item_id = @item_id

		IF @instance IS NULL OR @process IS NULL BEGIN
			SELECT @instance = instance, @process = process, @archive_start = deleted_date, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

			SET @mode = 1 -- Restore deleted item with historic values
		END
		ELSE BEGIN
			SET @mode = 2 -- Restore item with historic values
		END

		IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
			IF @archive_id IS NOT NULL BEGIN
				SET @sql = 'SELECT @archive_start = archive_start 
							FROM archive__' + @instance + '_' + @process + ' 
							WHERE archive_id = @archive_id'
				EXEC sp_executesql @sql, N'@archive_id INT, @archive_start DATETIME OUTPUT', @archive_id, @archive_start OUTPUT
			END
			ELSE IF @archive_start IS NOT NULL BEGIN
				SET @sql = 'SELECT @archive_id = archive_id 
							FROM get__' + @instance + '_' + @process + '_with_id(@item_id, @archive_start) '
				EXEC sp_executesql @sql, N'@item_id INT, @archive_start DATETIME, @archive_id INT OUTPUT', @item_id, @archive_start, @archive_id OUTPUT
			END
		END
	END
	ELSE BEGIN
		SET @mode = 1 -- Restore deleted item with latest values

		SELECT @instance = instance, @process = process, @archive_start = deleted_date, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

		IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
			SET @sql = 'SELECT TOP 1 @archive_id = archive_id  
						FROM archive__' + @instance + '_' + @process + ' 
						WHERE archive_type = 2 AND item_id = @item_id 
						ORDER BY archive_id DESC'
			EXEC sp_executesql @sql, N'@item_id INT, @archive_id INT OUTPUT', @item_id, @archive_id OUTPUT
		END
	END

	IF @mode = 1 AND @archive_start IS NOT NULL AND @archive_start <= '2021-06-07' THROW 51000, 'Cannot restore. Item is too old to restore', 1

	IF @instance IS NOT NULL AND @process IS NOT NULL AND @archive_id IS NOT NULL AND @archive_start IS NOT NULL BEGIN
		SET @sql = 'SELECT @archive = archive FROM item_tables WHERE name = ''_' + @instance + '_' + @process + ''''
		EXEC sp_executesql @sql, N'@archive INT OUTPUT', @archive OUTPUT
		SET @msg = 'Cannot restore. Process ' + @process + ' doesn''t use archive'
		IF ISNULL(@archive, 1) = 0 THROW 51000, @msg, 1

		-- Remove indications that item is deleted part 1/2
		IF @mode = 1 BEGIN
			DELETE FROM items_deleted WHERE item_id = @item_id
		END

		-- Restore item
		IF @mode = 1 BEGIN
			EXEC sp_set_session_context @key = N'restore_item_id', @value = @item_id
			EXEC sp_set_session_context @key = N'restore_guid', @value = @guid
			INSERT INTO items (instance, process, deleted) VALUES (@instance, @process, 0)
		END

		-- Remove current idps
		ELSE IF @mode = 2 BEGIN
			DELETE FROM item_data_process_selections WHERE item_id = @item_id OR selected_item_id = @item_id 
		END

		-- Restore item's data in row
		SELECT @columns = STUFF((SELECT ', t.' + COLUMN_NAME + ' = a.' + COLUMN_NAME 
								 FROM INFORMATION_SCHEMA.COLUMNS 
								 WHERE TABLE_NAME = '_' + @instance + '_' + @process AND COLUMN_NAME NOT IN ('item_id', 'order_no', 'archive_userid', 'archive_start') 
								 GROUP BY COLUMN_NAME 
								 FOR XML PATH('')), 1, 2, '')
		IF @columns <> '' BEGIN
			SET @sql = 'UPDATE t SET ' + @columns + ' 
						FROM _' + @instance + '_' + @process + ' t 
						INNER JOIN archive__' + @instance + '_' + @process + ' a ON a.item_id = t.item_id AND a.archive_id = @archive_id
						WHERE t.item_id = @item_id'
			EXEC sp_executesql @sql, N'@item_id INT, @archive_id INT', @item_id, @archive_id
		END
		
		-- Restore by constraints
		SET @constraints_cursor = CURSOR FAST_FORWARD LOCAL FOR
		SELECT ccu.TABLE_NAME, ccu.COLUMN_NAME 
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
		INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		WHERE UNIQUE_CONSTRAINT_NAME = 'PK_items' AND SUBSTRING(ccu.TABLE_NAME, 1, 1) = '_'
		OPEN @constraints_cursor
		FETCH NEXT FROM @constraints_cursor INTO @constraints_table_name, @constraints_column_name
		WHILE @@fetch_status = 0 BEGIN
			SET @sql = 'SELECT @constraints_item_id = item_id FROM get_' + @constraints_table_name + '(@archive_start) WHERE ' + @constraints_column_name + ' = @item_id'
			EXEC sp_executesql @sql, N'@item_id INT, @archive_start DATETIME, @constraints_item_id INT OUTPUT', @item_id, @archive_start, @constraints_item_id OUTPUT
			
			IF @constraints_item_id > 0 EXEC app_restore_item @constraints_item_id, NULL, @archive_start
			
			FETCH NEXT FROM @constraints_cursor INTO @constraints_table_name, @constraints_column_name
		END
		
		-- Restore child items by item_columns.use_fk = 2
		SET @child_items_cursor = CURSOR FOR SELECT selected_item_id FROM get_item_data_process_selections_with_id(@archive_start, @item_id) WHERE item_column_id IN (SELECT item_column_id FROM item_columns WHERE use_fk = 2)
		OPEN @child_items_cursor
		FETCH NEXT FROM @child_items_cursor INTO @child_items_item_id
		WHILE @@fetch_status = 0 BEGIN
			EXEC app_restore_item @child_items_item_id, NULL, @archive_start
			
			FETCH NEXT FROM @child_items_cursor INTO @child_items_item_id
		END

		-- Restore parent items by item_columns.use_fk = 3
		SET @parent_items_cursor = CURSOR FOR SELECT item_id FROM get_item_data_process_selections_with_sel_id(@archive_start, @item_id) WHERE item_column_id IN (SELECT item_column_id FROM item_columns WHERE use_fk = 3)
		OPEN @parent_items_cursor
		FETCH NEXT FROM @parent_items_cursor INTO @parent_items_item_id
		WHILE @@fetch_status = 0 BEGIN
			EXEC app_restore_item @parent_items_item_id, NULL, @archive_start
			
			FETCH NEXT FROM @parent_items_cursor INTO @parent_items_item_id
		END
				
		-- Restore idps by item_id
		INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id)
		SELECT idps.item_id, idps.item_column_id, idps.selected_item_id, idps.link_item_id
		FROM get_item_data_process_selections_with_id(@archive_start, @item_id) idps
		LEFT JOIN item_columns ic ON ic.item_column_id = idps.item_column_id
		LEFT JOIN items_deleted s_id ON s_id.item_id = idps.selected_item_id
		LEFT JOIN items_deleted l_id ON l_id.item_id = idps.link_item_id
		WHERE ic.item_column_id IS NOT NULL
		AND s_id.item_id IS NULL
		AND l_id.item_id IS NULL

		-- Restore idps by selected_item_id
		INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id)
		SELECT idps.item_id, idps.item_column_id, idps.selected_item_id, idps.link_item_id
		FROM get_item_data_process_selections_with_sel_id(@archive_start, @item_id) idps
		LEFT JOIN items_deleted i_id ON i_id.item_id = idps.item_id
		LEFT JOIN item_columns ic ON ic.item_column_id = idps.item_column_id
		LEFT JOIN items_deleted l_id ON l_id.item_id = idps.link_item_id
		WHERE i_id.item_id IS NULL
		AND ic.item_column_id IS NOT NULL
		AND l_id.item_id IS NULL

		-- Restore table columns data (18, 21) if parent process column is int/process/list and table process column is item_id
		SET @table_columns_cursor = CURSOR FOR SELECT data_additional2 FROM item_columns WHERE instance = @instance AND process = @process AND data_type in (18, 21) AND CHARINDEX('"parentFieldName"', data_additional2) = 0 
		OPEN @table_columns_cursor
		FETCH NEXT FROM @table_columns_cursor INTO @data_additional2
		WHILE @@fetch_status = 0 BEGIN
			SET @start_pos = CHARINDEX('"itemColumnId"', @data_additional2)
			IF @start_pos = 0 BEGIN
				SET @start_pos = CHARINDEX('"ownerColumnId"', @data_additional2) + 1
			END
			IF @start_pos > 1 BEGIN
				SET @start_pos = @start_pos + 15
				SET @end_pos = CHARINDEX(',', @data_additional2, @start_pos)
				IF @end_pos = 0 BEGIN
					SET @end_pos = CHARINDEX('}', @data_additional2, @start_pos)
				END
				SET @table_item_column_id = SUBSTRING(@data_additional2, @start_pos, @end_pos - @start_pos)
				IF (SELECT COUNT (*) FROM #table_item_column_ids WHERE item_column_id = @table_item_column_id) = 0 BEGIN
					INSERT INTO #table_item_column_ids (item_column_id) VALUES (@table_item_column_id)
					IF ISNUMERIC(@table_item_column_id) = 1 BEGIN
						SELECT @table_instance = instance, @table_process = process, @table_column_name = [name], @table_data_type = data_type, @data_additional = data_additional FROM item_columns WHERE item_column_id = @table_item_column_id
						IF @table_data_type = 1 BEGIN
							SET @sql = 'INSERT INTO #table_items SELECT item_id FROM get__' + @table_instance + '_' + @table_process + '(@archive_start) WHERE ' + @table_column_name + ' = @item_id'
							EXEC sp_executesql @sql, N'@archive_start DATETIME, @item_id INT', @archive_start, @item_id
							SET @table_items_cursor = CURSOR FOR SELECT item_id FROM #table_items
							OPEN @table_items_cursor
							FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							WHILE @@fetch_status = 0 BEGIN
								EXEC app_restore_item @table_item_id, NULL, @archive_start
								
								FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							END
							DELETE FROM #table_items
						END
						ELSE IF @table_data_type IN (5, 6) AND @data_additional = @process BEGIN
							DELETE FROM item_data_process_selections WHERE item_column_id = @table_item_column_id AND selected_item_id = @item_id
							
							INSERT INTO #table_items SELECT item_id FROM get_item_data_process_selections_with_sel_ids(@archive_start, @table_item_column_id, @item_id)
							SET @table_items_cursor = CURSOR FOR SELECT item_id FROM #table_items
							OPEN @table_items_cursor
							FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							WHILE @@fetch_status = 0 BEGIN
								EXEC app_restore_item @table_item_id, NULL, @archive_start
							
								FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							END
							DELETE FROM #table_items
						END
					END
				END
				FETCH NEXT FROM @table_columns_cursor INTO @data_additional2
			END
		END

		-- Remove indications that item is deleted part 2/2
		IF @mode = 1 BEGIN
			SET @sql = 'DELETE FROM archive__' + @instance + '_' + @process + ' WHERE archive_type = 2 AND item_id = @item_id'
			EXEC sp_executesql @sql, N'@item_id INT', @item_id
		END
	END
END