EXEC app_set_archive_user_id 1
UPDATE instance_configurations SET [value] = REPLACE([value], '"dashboard_id"', '"DashboardId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'process.financials'
UPDATE instance_configurations SET [value] = REPLACE([value], '"fields_portfolio"', '"fieldsPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'process.financials'

UPDATE instance_configurations SET [value] = REPLACE([value], '"signedTaskListValue"', '"signedTaskListValueItemId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'process.gantt'
UPDATE instance_configurations SET [value] = REPLACE([value], '"modelPortfolio"', '"modelPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'process.gantt'

UPDATE instance_configurations SET [value] = REPLACE([value], '"targetColumns"', '"targetColumnId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'process.historyEditor'

UPDATE instance_configurations SET [value] = REPLACE([value], '"userSelectorId"', '"userSelectorPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'process.projectAllocation'

UPDATE instance_configurations SET [value] = REPLACE([value], '"BaseCalendar"', '"BaseCalendarId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'process.simpleMonthlyAllocation'

UPDATE instance_configurations SET [value] = REPLACE([value], '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'resourcesAllocations'

UPDATE instance_configurations SET [value] = REPLACE([value], '"ProcessPortfolio"', '"ProcessPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'simple_allocations'
UPDATE instance_configurations SET [value] = REPLACE([value], '"UserPortfolio"', '"UserPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'simple_allocations'
UPDATE instance_configurations SET [value] = REPLACE([value], '"BaseCalendar"', '"BaseCalendarId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'simple_allocations'

UPDATE instance_configurations SET [value] = REPLACE([value], '"TaskSelectionPortfolio"', '"TaskSelectionPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'worktimeTracking'
UPDATE instance_configurations SET [value] = REPLACE([value], '"TaskPortfolio"', '"TaskPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'worktimeTracking'

UPDATE instance_configurations SET [value] = REPLACE([value], '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'worktimeHourStatus'
UPDATE instance_configurations SET [value] = REPLACE([value], '"additionalColumns"', '"additionalColumnId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'worktimeHourStatus'

UPDATE instance_configurations SET [value] = REPLACE([value], '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'WorkLoadStatus'
UPDATE instance_configurations SET [value] = REPLACE([value], '"optionalColumns"', '"optionalColumnId"') WHERE category = 'View Configurator' AND [server] = 0 AND [name] = 'WorkLoadStatus'

UPDATE instance_menu SET params = REPLACE(params, '"availability"', '"availabilityItemId"') WHERE default_state = 'widget.runTimeInfoWidget'
UPDATE instance_menu SET params = REPLACE(params, '"userLicensing"', '"userLicensingItemId"') WHERE default_state = 'widget.runTimeInfoWidget'
UPDATE instance_menu SET params = REPLACE(params, '"performance"', '"performanceItemId"') WHERE default_state = 'widget.runTimeInfoWidget'
UPDATE instance_menu SET params = REPLACE(params, '"eventLog"', '"eventLogItemId"') WHERE default_state = 'widget.runTimeInfoWidget'
UPDATE instance_menu SET params = REPLACE(params, '"admin"', '"adminItemId"') WHERE default_state = 'widget.runTimeInfoWidget'
UPDATE instance_menu SET params = REPLACE(params, '"drilldown"', '"drilldownPortfolioId"') WHERE default_state = 'widget.runTimeInfoWidget'

UPDATE instance_menu SET params = REPLACE(params, '"kpi1Portfolio"', '"kpi1PortfolioId"') WHERE default_state = 'widget.kpiWidget'
UPDATE instance_menu SET params = REPLACE(params, '"kpi2Portfolio"', '"kpi2PortfolioId"') WHERE default_state = 'widget.kpiWidget'
UPDATE instance_menu SET params = REPLACE(params, '"kpi3Portfolio"', '"kpi3PortfolioId"') WHERE default_state = 'widget.kpiWidget'
UPDATE instance_menu SET params = REPLACE(params, '"kpi4Portfolio"', '"kpi4PortfolioId"') WHERE default_state = 'widget.kpiWidget'
UPDATE instance_menu SET params = REPLACE(params, '"kpi5Portfolio"', '"kpi5PortfolioId"') WHERE default_state = 'widget.kpiWidget'

UPDATE instance_menu SET params = REPLACE(params, '"selectedPortfolios"', '"selectedPortfolioId"') WHERE default_state = 'widget.multiplePortfolios'

UPDATE instance_menu SET params = REPLACE(params, '"parentItemId"', '"parentItemName"') WHERE default_state = 'widget.parentPortfolio'

UPDATE instance_menu SET params = REPLACE(params, '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE default_state = 'resourcesAllocation'
UPDATE instance_menu SET params = REPLACE(params, '"FilteringPortfolioCompetency"', '"FilteringPortfolioCompetencyPortfolioId"') WHERE default_state = 'resourcesAllocation'

UPDATE instance_menu SET params = REPLACE(params, '"ProcessPortfolio"', '"ProcessPortfolioId"') WHERE default_state = 'simple_allocations'
UPDATE instance_menu SET params = REPLACE(params, '"UserPortfolio"', '"UserPortfolioId"') WHERE default_state = 'simple_allocations'
UPDATE instance_menu SET params = REPLACE(params, '"BaseCalendar"', '"BaseCalendarId"') WHERE default_state = 'simple_allocations'

UPDATE instance_menu SET params = REPLACE(params, '"returnStaticNavigation"', '"returnStaticNavigationMenuId"') WHERE default_state = 'dashboard'

UPDATE instance_menu SET params = REPLACE(params, '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE default_state = 'projectsAllocations'

UPDATE instance_menu SET params = REPLACE(params, '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE default_state = 'resourcesAllocations'

UPDATE instance_menu SET params = REPLACE(params, '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE default_state = 'WorkLoadStatus'
UPDATE instance_menu SET params = REPLACE(params, '"optionalColumns"', '"optionalColumnId"') WHERE default_state = 'WorkLoadStatus'

UPDATE instance_menu SET params = REPLACE(params, '"FilteringPortfolio"', '"FilteringPortfolioId"') WHERE default_state = 'worktimeHourStatus'
UPDATE instance_menu SET params = REPLACE(params, '"additionalColumns"', '"additionalColumnId"') WHERE default_state = 'worktimeHourStatus'

UPDATE instance_menu SET params = REPLACE(params, '"TaskSelectionPortfolio"', '"TaskSelectionPortfolioId"') WHERE default_state = 'worktimeTracking'
UPDATE instance_menu SET params = REPLACE(params, '"TaskPortfolio"', '"TaskPortfolioId"') WHERE default_state = 'worktimeTracking'



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[instanceConfiguration_ValueIdReplace]
(
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @value IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
		
			SET @startPos = 1
		  	WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @value, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 2
									
							IF CHARINDEX('[', @value, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
				
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos2 - @startPos)
								END

							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							SET @var3 = LTRIM(RTRIM(REPLACE(REPLACE(@var3, CHAR(13), ''), CHAR(10), '')))
							SET @var3 = REPLACE(@var3, ' ', '')

							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @guid UNIQUEIDENTIFIER
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
								
											DECLARE @start INT
											SET @start = 1
								  			WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													SELECT @guid = guid FROM items WHERE item_id = @listVal
													DECLARE @gu_id NVARCHAR(MAX)
													SET @gu_id = @guid
													IF @gu_id IS NULL
														BEGIN
															SET @gu_id = 'NULL' 
														END
													SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

													SET @start = @start + LEN(@gu_id) + 1

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
													
											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
									
							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @colId NVARCHAR(MAX)
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colId

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF CAST(@colId AS INT) > 0
														BEGIN
															IF @subType = 0
																BEGIN
																	SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colVar = dbo.processPortfolio_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colVar = dbo.condition_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colVar = dbo.processAction_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colVar = dbo.processActionDialog_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colVar = dbo.processTab_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colVar = dbo.processContainer_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colVar = dbo.processDashboard_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colVar = dbo.processDashboardChart_GetVariable(CAST(@colId AS INT))
																END

															IF @colVar IS NULL
																BEGIN
																	SET @colVar = 'NULL' 
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
															SET @start2 = @start2 + LEN(@colVar) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colId
												END
											CLOSE @delimited2

											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 2
								BEGIN
									IF LEFT(LTRIM(@var3), 5) = '"tab_'
										BEGIN
											SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END

							IF @type = 3
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SELECT @var4 = REPLACE(calendar_name, ' ', '_') FROM calendars WHERE calendar_id = @var3
													IF @var4 IS NOT NULL
														BEGIN
															SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
								END
										
							SET @startPos = CHARINDEX(@strToFound, @value, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @value
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[instanceConfiguration_ValueVarReplace]
(
	@instance NVARCHAR(MAX),
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @value IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
					
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType

			SET @startPos = 1
			WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @value, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 2

							IF CHARINDEX('[', @value, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @value, @startPos)
									SET @endPos2 = CHARINDEX('}', @value, @startPos)
								END
									
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@value, @startPos, @endPos2 - @startPos)
								END
									
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @listItemId INT
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
													
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @listVal = 'NULL' 
														BEGIN
															SET @listItemId = 0
														END
													ELSE 
														BEGIN
															SELECT @listItemId = item_id FROM items WHERE guid = @listVal
															IF @listItemId IS NULL
																BEGIN
																	SET @listItemId = 0
																END
														END
													SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 1 
								BEGIN
									DECLARE @colId INT
									DECLARE @colVar NVARCHAR(MAX)
									SET @var4 = @var3
									DECLARE @delimited2 CURSOR
									SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
									SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

									OPEN @delimited2
									FETCH NEXT FROM @delimited2 INTO @colVar

									DECLARE @start2 INT
									SET @start2 = 1
									WHILE (@@FETCH_STATUS = 0) 
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													IF @subType = 0
														BEGIN
															SET @colId = dbo.itemColumn_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 1
														BEGIN
															SET @colId = dbo.processPortfolio_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 2
														BEGIN
															SET @colId = dbo.condition_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 3
														BEGIN
															SET @colId = dbo.processAction_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 4
														BEGIN
															SET @colId = dbo.processActionDialog_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 5
														BEGIN
															SET @colId = dbo.processTab_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 6
														BEGIN
															SET @colId = dbo.processContainer_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 7
														BEGIN
															SET @colId = dbo.processDashboard_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 8
														BEGIN
															SET @colId = dbo.processDashboardChart_GetId(@instance, @colVar)
														END

													IF @colId IS NULL
														BEGIN
															SET @colId = 0
														END
													SET @var4 = STUFF(@var4, @start2, LEN(@colVar), CAST(@colId AS NVARCHAR(MAX)))
													SET @start2 = @start2 + LEN(CAST(@colId AS NVARCHAR(MAX))) + 1
												END
											FETCH NEXT FROM @delimited2 INTO @colVar
										END
									CLOSE @delimited2

									SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)											
								END
									
							IF @type = 2 
								BEGIN
									IF LEFT(@var3, 4) = 'tab_'
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = ' ""'
														END
													ELSE
														BEGIN
															SET @var4 = ' "tab_' + @var4 + '"'
														END
													SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END

							IF @type = 3
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0'
										BEGIN
											SELECT @var4 = calendar_id FROM calendars WHERE base_calendar = 1 AND REPLACE(calendar_name, ' ', '_') = @var3
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @value = STUFF(@value, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
		
							SET @startPos = CHARINDEX(@strToFound, @value, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @value
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[menu_IdReplace]
(
	@params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @params IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('MenuId"', 1, 9),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
		
			SET @startPos = 1
		  	WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @params, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 1
									
							IF CHARINDEX('[', @params, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
				
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
								END

							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							SET @var3 = REPLACE(@var3, '"', '')

							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @guid UNIQUEIDENTIFIER
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
								
											DECLARE @start INT
											SET @start = 1
								  			WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													SELECT @guid = guid FROM items WHERE item_id = @listVal
													DECLARE @gu_id NVARCHAR(MAX)
													SET @gu_id = @guid
													IF @gu_id IS NULL
														BEGIN
															SET @gu_id = 'NULL' 
														END
													SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

													SET @start = @start + LEN(@gu_id) + 1

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
													
											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
									
							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @colId NVARCHAR(MAX)
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colId

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF CAST(@colId AS INT) > 0
														BEGIN
															IF @subType = 0
																BEGIN
																	SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colVar = dbo.processPortfolio_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colVar = dbo.condition_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colVar = dbo.processAction_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colVar = dbo.processActionDialog_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colVar = dbo.processTab_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colVar = dbo.processContainer_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colVar = dbo.processDashboard_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colVar = dbo.processDashboardChart_GetVariable(CAST(@colId AS INT))
																END
															ELSE IF @subType = 9
																BEGIN
																	SET @colVar = dbo.menu_GetVariable(CAST(@colId AS INT))
																END

															IF @colVar IS NULL
																BEGIN
																	SET @colVar = 'NULL' 
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
															SET @start2 = @start2 + LEN(@colVar) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colId
												END
											CLOSE @delimited2

											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 2
								BEGIN
									IF LEFT(LTRIM(@var3), 5) = '"tab_'
										BEGIN
											SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END

							IF @type = 3
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SELECT @var4 = REPLACE(calendar_name, ' ', '_') FROM calendars WHERE calendar_id = @var3
													IF @var4 IS NOT NULL
														BEGIN
															SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
								END
										
							SET @startPos = CHARINDEX(@strToFound, @params, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @params
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[menu_VarReplace]
(
	@instance NVARCHAR(10),
	@params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT, strsubtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @params IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype, strsubtype) 
			VALUES
				('ItemId"', 0, 0),
				('ColumnId"', 1, 0),
				('PortfolioId"', 1, 1),
				('ConditionId"', 1, 2),
				('ActionId"', 1, 3),
				('ActionDialogId"', 1, 4),
				('TabId"', 1, 5),
				('ContainerId"', 1, 6),
				('DashboardId"', 1, 7),
				('ChartId"', 1, 8),
				('MenuId"', 1, 9),
				('State"', 2, 0),
				('CalendarId"', 3, 0)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype, strsubtype FROM @strTable 
					
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType

			SET @startPos = 1
			WHILE (@@FETCH_STATUS = 0)
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @params, @startPos)
					WHILE @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 1

							IF CHARINDEX('[', @params, @startPos) = @startPos + @strToFoundLength
								BEGIN
									SET @startPos = @startPos + 1
									SET @endPos1 = CHARINDEX(']', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
							ELSE
								BEGIN
									SET @endPos1 = CHARINDEX(',', @params, @startPos)
									SET @endPos2 = CHARINDEX('}', @params, @startPos)
								END
									
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@params, @startPos, @endPos2 - @startPos)
								END
									
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							SET @var3 = REPLACE(@var3, '"', '')
									
							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
										BEGIN
											DECLARE @listVal NVARCHAR(MAX)
											DECLARE @listItemId INT
											SET @var4 = @var3
											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
													
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF @listVal = 'NULL' 
														BEGIN
															SET @listItemId = 0
														END
													ELSE 
														BEGIN
															SELECT @listItemId = item_id FROM items WHERE guid = @listVal
															IF @listItemId IS NULL
																BEGIN
																	SET @listItemId = 0
																END
														END
													SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END

							IF @type = 1 
								BEGIN
									DECLARE @colId INT
									DECLARE @colVar NVARCHAR(MAX)
									SET @var4 = @var3
									DECLARE @delimited2 CURSOR
									SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
									SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

									OPEN @delimited2
									FETCH NEXT FROM @delimited2 INTO @colVar

									DECLARE @start2 INT
									SET @start2 = 1
									WHILE (@@FETCH_STATUS = 0) 
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													IF @subType = 0
														BEGIN
															SET @colId = dbo.itemColumn_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 1
														BEGIN
															SET @colId = dbo.processPortfolio_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 2
														BEGIN
															SET @colId = dbo.condition_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 3
														BEGIN
															SET @colId = dbo.processAction_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 4
														BEGIN
															SET @colId = dbo.processActionDialog_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 5
														BEGIN
															SET @colId = dbo.processTab_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 6
														BEGIN
															SET @colId = dbo.processContainer_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 7
														BEGIN
															SET @colId = dbo.processDashboard_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 8
														BEGIN
															SET @colId = dbo.processDashboardChart_GetId(@instance, @colVar)
														END
													ELSE IF @subType = 9
														BEGIN
															SET @colId = dbo.menu_GetId(@instance, @colVar)
														END

													IF @colId IS NULL
														BEGIN
															SET @colId = 0
														END
													SET @var4 = STUFF(@var4, @start2, LEN(@colVar), CAST(@colId AS NVARCHAR(MAX)))
													SET @start2 = @start2 + LEN(CAST(@colId AS NVARCHAR(MAX))) + 1
												END
											FETCH NEXT FROM @delimited2 INTO @colVar
										END
									CLOSE @delimited2

									SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)											
								END
									
							IF @type = 2 
								BEGIN
									IF LEFT(@var3, 4) = 'tab_'
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = ' ""'
														END
													ELSE
														BEGIN
															SET @var4 = ' "tab_' + @var4 + '"'
														END
													SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END

							IF @type = 3
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> '0'
										BEGIN
											SELECT @var4 = calendar_id FROM calendars WHERE base_calendar = 1 AND REPLACE(calendar_name, ' ', '_') = @var3
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @params = STUFF(@params, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
		
							SET @startPos = CHARINDEX(@strToFound, @params, @startPos + @strToFoundLength)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type, @subType
				END
		END

	RETURN @params
END