DECLARE @P NVARCHAR(255);
DECLARE @Instance NVARCHAR(255);
DECLARE @item_column_id INT;
DECLARE cursor_process CURSOR
FOR SELECT process, instance FROM processes where process_type = 4

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @P, @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
  
	  BEGIN
	  EXEC app_ensure_column @Instance, @P, 'custom_width', 0, 2, 0, 0, null, null, 0, null, null
	   SELECT @item_column_id = (SELECT item_column_id FROM item_columns where process = @P AND name = 'custom_width')
	  
	  UPDATE item_columns SET status_column = 1 where item_column_id = @item_column_id
	  END
		
        FETCH NEXT FROM cursor_process INTO 
            @P,@Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;


