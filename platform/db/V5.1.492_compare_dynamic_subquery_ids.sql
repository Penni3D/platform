UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@AttachlinkColumn"', '"@AttachlinkColumnId"') WHERE data_type = 16 AND data_additional = 'AttachmentCount'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@BudgetItemTypeValues"', '"@BudgetItemTypeValuesItemId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearNPV'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@BudgetItemCategoryIncomeValues"', '"@BudgetItemCategoryIncomeValuesItemId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearNPV'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@BudgetItemCategoryExpenseValues"', '"@BudgetItemCategoryExpenseValuesItemId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearNPV'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@NPVDiscountValueIntField"', '"@NPVDiscountValueIntFieldColumnId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearNPV'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@NPVStartYearIntField"', '"@NPVStartYearIntFieldColumnId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearNPV'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@NPVNumberOfYearsIntField"', '"@NPVNumberOfYearsIntFieldColumnId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearNPV'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@CurrencySelection"', '"@CurrencySelectionColumnId"') WHERE data_type = 16 AND data_additional = 'CurrencyExchange'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@ToCurrency"', '"@ToCurrencyItemId"') WHERE data_type = 16 AND data_additional = 'CurrencyExchange'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@PaybackItemTypes"', '"@PaybackItemTypesItemId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearPayback'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@PaybackItemCategoryIncomeTypes"', '"@PaybackItemCategoryIncomeTypesItemId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearPayback'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@PaybackItemCategoryExpenseTypes"', '"@PaybackItemCategoryExpenseTypesItemId"') WHERE data_type = 16 AND data_additional = 'BudgetItemYearPayback'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@ProcessActualEffortTaskFilterList"', '"@ProcessActualEffortTaskFilterListColumnId"') WHERE data_type = 16 AND data_additional = 'ProcessActualEffort'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@ProcessActualEffortAllowedListValues"', '"@ProcessActualEffortAllowedListValuesItemId"') WHERE data_type = 16 AND data_additional = 'ProcessActualEffort'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@ProcessAllocationWorkCategories"', '"@ProcessAllocationWorkCategoriesItemId"') WHERE data_type = 16 AND data_additional = 'ProcessAllocation'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@ProcessAllocationTaskFilterList"', '"@ProcessAllocationTaskFilterListColumnId"') WHERE data_type = 16 AND data_additional = 'ProcessAllocation'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@ProcessAllocationAllowedListValues"', '"@ProcessAllocationAllowedListValuesItemId"') WHERE data_type = 16 AND data_additional = 'ProcessAllocation'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@PLUGProcessListColumn"', '"@PLUGProcessListColumnId"') WHERE data_type = 16 AND data_additional = 'SQ_ProcessesListUsersOfGroup'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@PLUGListsGroupColumns"', '"@PLUGListsGroupsColumnId"') WHERE data_type = 16 AND data_additional = 'SQ_ProcessesListUsersOfGroup'
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@PLUGNoUserGroupColumns"', '"@PLUGNoUserGroupsColumnId"') WHERE data_type = 16 AND data_additional = 'SQ_ProcessesListUsersOfGroup'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@SprintResponsiblesCol"', '"@SprintResponsiblesColumnId"') WHERE data_type = 16 AND data_additional = 'SprintResponsibles'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@SourceProcessId"', '"@SourceProcessColumnId"') WHERE data_type = 16 AND data_additional IN ('GenericProcessAggregateFunction', 'GenericListAggregateFunction', 'GenericNumberAggregateFunction', 'GenericStringAggregateFunction', 'GenericLongStringAggregateFunction', 'GenericDateAggregateFunction')
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@SourceColumn"', '"@SourceColumnId"') WHERE data_type = 16 AND data_additional IN ('GenericProcessAggregateFunction', 'GenericListAggregateFunction', 'GenericNumberAggregateFunction', 'GenericStringAggregateFunction', 'GenericLongStringAggregateFunction', 'GenericDateAggregateFunction')
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@OptionalList1Id"', '"@OptionalList1ColumnId"') WHERE data_type = 16 AND data_additional IN ('GenericProcessAggregateFunction', 'GenericListAggregateFunction', 'GenericNumberAggregateFunction', 'GenericStringAggregateFunction', 'GenericLongStringAggregateFunction', 'GenericDateAggregateFunction')
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@OptionalList1Val"', '"@OptionalList1ItemId"') WHERE data_type = 16 AND data_additional IN ('GenericProcessAggregateFunction', 'GenericListAggregateFunction', 'GenericNumberAggregateFunction', 'GenericStringAggregateFunction', 'GenericLongStringAggregateFunction', 'GenericDateAggregateFunction')
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@OptionalList2Id"', '"@OptionalList2ColumnId"') WHERE data_type = 16 AND data_additional IN ('GenericProcessAggregateFunction', 'GenericListAggregateFunction', 'GenericNumberAggregateFunction', 'GenericStringAggregateFunction', 'GenericLongStringAggregateFunction', 'GenericDateAggregateFunction')
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@OptionalList2Val"', '"@OptionalList2ItemId"') WHERE data_type = 16 AND data_additional IN ('GenericProcessAggregateFunction', 'GenericListAggregateFunction', 'GenericNumberAggregateFunction', 'GenericStringAggregateFunction', 'GenericLongStringAggregateFunction', 'GenericDateAggregateFunction')
UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@OptionalOrderColumn"', '"@OptionalOrderColumnId"') WHERE data_type = 16 AND data_additional IN ('GenericProcessAggregateFunction', 'GenericListAggregateFunction', 'GenericNumberAggregateFunction', 'GenericStringAggregateFunction', 'GenericLongStringAggregateFunction', 'GenericDateAggregateFunction')

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@TaskEffortResponsiblesCol"', '"@TaskEffortResponsiblesColumnId"') WHERE data_type = 16 AND data_additional = 'TaskEffort'

UPDATE item_columns SET data_additional2 = REPLACE(data_additional2, '"@TaskCostsResponsiblesCol"', '"@TaskCostsResponsiblesColumnId"') WHERE data_type = 16 AND data_additional = 'TaskCost'



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @strToFound2 NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @strTable2 TABLE (str_to_found2 NVARCHAR(MAX), strtype2 INT)
	DECLARE @strTable3 TABLE (str_to_found3 NVARCHAR(MAX), strtype3 INT, strsubtype3 INT)
	DECLARE @getStrs CURSOR
	DECLARE @getStrs2 CURSOR
	DECLARE @getStrs3 CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable3 (str_to_found3, strtype3, strsubtype3)
					VALUES
						('ItemId"', 0, 0),
						('ColumnId"', 1, 0),
						('PortfolioId"', 1, 1),
						('ConditionId"', 1, 2),
						('ActionId"', 1, 3),
						('ActionDialogId"', 1, 4),
						('TabId"', 1, 5),
						('ContainerId"', 1, 6),
						('DashboardId"', 1, 7),
						('ChartId"', 1, 8),
						('State"', 2, 0)
		
					SET @getStrs3 = CURSOR FAST_FORWARD LOCAL FOR
					SELECT str_to_found3, strtype3, strsubtype3 FROM @strTable3
			
					OPEN @getStrs3
					FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
		
					SET @startPos = 1
		  			WHILE (@@FETCH_STATUS = 0)
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos)
							WHILE @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 1
									
									IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
										BEGIN
											SET @startPos = @startPos + 1
											SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									ELSE
										BEGIN
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
				
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END

									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @guid UNIQUEIDENTIFIER
													SET @var4 = @var3
													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													DECLARE @start INT
													SET @start = 1
								  					WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															DECLARE @gu_id NVARCHAR(MAX)
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = 'NULL' 
																END
															SET @var4 = STUFF(@var4, @start, LEN(@listVal), @gu_id)

															SET @start = @start + LEN(@gu_id) + 1

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @colId NVARCHAR(MAX)
													DECLARE @colVar NVARCHAR(MAX)
													SET @var4 = @var3
													DECLARE @delimited2 CURSOR
													SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

													OPEN @delimited2
													FETCH NEXT FROM @delimited2 INTO @colId

													DECLARE @start2 INT
													SET @start2 = 1
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF CAST(@colId AS INT) > 0
																BEGIN
																	IF @subType = 0
																		BEGIN
																			SET @colVar = dbo.itemColumn_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 1
																		BEGIN
																			SET @colVar = dbo.processPortfolio_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 2
																		BEGIN
																			SET @colVar = dbo.condition_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 3
																		BEGIN
																			SET @colVar = dbo.processAction_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 4
																		BEGIN
																			SET @colVar = dbo.processActionDialog_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 5
																		BEGIN
																			SET @colVar = dbo.processTab_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 6
																		BEGIN
																			SET @colVar = dbo.processContainer_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 7
																		BEGIN
																			SET @colVar = dbo.processDashboard_GetVariable(CAST(@colId AS INT))
																		END
																	ELSE IF @subType = 8
																		BEGIN
																			SET @colVar = dbo.processDashboardChart_GetVariable(CAST(@colId AS INT))
																		END

																	IF @colVar IS NULL
																		BEGIN
																			SET @colVar = 'NULL' 
																		END
																	SET @var4 = STUFF(@var4, @start2, LEN(@colId), @colVar)
																	SET @start2 = @start2 + LEN(@colVar) + 1
																END
															FETCH NEXT FROM @delimited2 INTO @colId
														END
													CLOSE @delimited2

													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									IF @type = 2
										BEGIN
											IF LEFT(LTRIM(@var3), 5) = '"tab_'
												BEGIN
													SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
													IF ISNUMERIC(@var3) = 1 
														BEGIN
															IF CAST(@var3 AS INT) > 0
																BEGIN
																	SET @var4 = dbo.processTab_GetVariable(@var3)
																	IF @var4 IS NOT NULL
																		BEGIN
																			SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																		END
																END
														END
													ELSE
														BEGIN
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
														END
												END
										END	
										
									SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos + @strToFoundLength)
								END
							FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0),
						('fromProcessColumnId', 0),
						('targetProcessColumnId', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
				
									IF @type = 0
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 1
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processPortfolio_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 2
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
				
			IF @data_type = 21
				BEGIN
					INSERT INTO @strTable2 (str_to_found2, strtype2) 
					VALUES
						('"action":', 2),
						('"containerId":', 3)
	
					SET @getStrs2 = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found2, strtype2 FROM @strTable2
			
					OPEN @getStrs2
					FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound2)
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 2
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									IF @type = 3
										BEGIN
											IF ISNUMERIC(@var3) = 1
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processContainer_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
										END

									SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2, @startPos + 1)
								END
							FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @strToFoundLength = LEN(@strToFound) + 2
		
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

									IF @type = 0 
										BEGIN
											IF LEFT(LTRIM(@var3), 5) = '"tab_'
												BEGIN
													SET @var3 =  REPLACE(REPLACE(LTRIM(@var3), 'tab_', ''), '"', '')
													IF ISNUMERIC(@var3) = 1 
														BEGIN
															IF CAST(@var3 AS INT) > 0
																BEGIN
																	SET @var4 = dbo.processTab_GetVariable(@var3)
																	IF @var4 IS NOT NULL
																		BEGIN
																			SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																		END
																END
														END
													ELSE
														BEGIN
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS INT) > 0
														BEGIN
															SET @var4 = dbo.processActionDialog_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																END
														END
												END
											ELSE
												BEGIN
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs2')) >= -1
		BEGIN
			DEALLOCATE @getStrs2
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs3')) >= -1
		BEGIN
			DEALLOCATE @getStrs3
		END

	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX) 
	DECLARE @strToFound2 NVARCHAR(MAX) 
	DECLARE @type INT
	DECLARE @subType INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @strTable2 TABLE (str_to_found2 NVARCHAR(MAX), strtype2 INT)
	DECLARE @strTable3 TABLE (str_to_found3 NVARCHAR(MAX), strtype3 INT, strsubtype3 INT)
	DECLARE @getStrs CURSOR
	DECLARE @getStrs2 CURSOR
	DECLARE @getStrs3 CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN
			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable3 (str_to_found3, strtype3, strsubtype3)
					VALUES
						('ItemId"', 0, 0),
						('ColumnId"', 1, 0),
						('PortfolioId"', 1, 1),
						('ConditionId"', 1, 2),
						('ActionId"', 1, 3),
						('ActionDialogId"', 1, 4),
						('TabId"', 1, 5),
						('ContainerId"', 1, 6),
						('DashboardId"', 1, 7),
						('ChartId"', 1, 8),
						('State"', 2, 0)

					SET @getStrs3 = CURSOR FAST_FORWARD LOCAL FOR
					SELECT str_to_found3, strtype3, strsubtype3 FROM @strTable3
					
					OPEN @getStrs3
					FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
					
					SET @startPos = 1
					WHILE (@@FETCH_STATUS = 0)
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos)
							WHILE @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 1

									IF CHARINDEX('[', @dataAdditional2, @startPos) = @startPos + @strToFoundLength
										BEGIN
											SET @startPos = @startPos + 1
											SET @endPos1 = CHARINDEX(']', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									ELSE
										BEGIN
											SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)
										END
									
									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
												BEGIN
													DECLARE @listVal NVARCHAR(MAX)
													DECLARE @listItemId INT
													SET @var4 = @var3
													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
													
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
													
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = REPLACE(@var4, @listVal, CAST(@listItemId AS NVARCHAR(MAX)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									IF @type = 1 
										BEGIN
											DECLARE @colId INT
											DECLARE @colVar NVARCHAR(MAX)
											SET @var4 = @var3
											DECLARE @delimited2 CURSOR
											SET @delimited2 = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 

											OPEN @delimited2
											FETCH NEXT FROM @delimited2 INTO @colVar

											DECLARE @start2 INT
											SET @start2 = 1
											WHILE (@@FETCH_STATUS = 0) 
												BEGIN
													IF LEN(@var3) > 0 AND @var3 <> '0' AND @var3 <> '-1' AND @var3 <> 'null'
														BEGIN
															IF @subType = 0
																BEGIN
																	SET @colId = dbo.itemColumn_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 1
																BEGIN
																	SET @colId = dbo.processPortfolio_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 2
																BEGIN
																	SET @colId = dbo.condition_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 3
																BEGIN
																	SET @colId = dbo.processAction_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 4
																BEGIN
																	SET @colId = dbo.processActionDialog_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 5
																BEGIN
																	SET @colId = dbo.processTab_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 6
																BEGIN
																	SET @colId = dbo.processContainer_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 7
																BEGIN
																	SET @colId = dbo.processDashboard_GetId(@instance, @colVar)
																END
															ELSE IF @subType = 8
																BEGIN
																	SET @colId = dbo.processDashboardChart_GetId(@instance, @colVar)
																END

															IF @colId IS NULL
																BEGIN
																	SET @colId = 0
																END
															SET @var4 = STUFF(@var4, @start2, LEN(@colVar), CAST(@colId AS NVARCHAR(MAX)))
															SET @start2 = @start2 + LEN(CAST(@colId AS NVARCHAR(MAX))) + 1
														END
													FETCH NEXT FROM @delimited2 INTO @colVar
												END
											CLOSE @delimited2

											SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)											
										END
									
									IF @type = 2 
										BEGIN
											IF LEFT(@var3, 4) = 'tab_'
												BEGIN
													IF LEN(@var3) > 0
														BEGIN
															SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
															IF @var4 IS NULL
																BEGIN
																	SET @var4 = ' ""'
																END
															ELSE
																BEGIN
																	SET @var4 = ' "tab_' + @var4 + '"'
																END
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
		
									SET @startPos = CHARINDEX(@strToFound, @dataAdditional2, @startPos + @strToFoundLength)
								END
							FETCH NEXT FROM @getStrs3 INTO @strToFound, @type, @subType
						END
				END
			
			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0),
						('fromProcessColumnId', 0),
						('targetProcessColumnId', 0)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
					
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 2
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
						
			IF @data_type = 21
				BEGIN
					INSERT INTO @strTable2 (str_to_found2, strtype2) 
					VALUES
						('"action":', 2),
						('"containerId":', 3)
					
					SET @getStrs2 = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found2, strtype2 FROM @strTable2 
					
					OPEN @getStrs2
					FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound2)
									
									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 2
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
									
									IF @type = 3
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processContainer_GetId(@instance, @var3)
													IF @var4 IS NULL
														BEGIN
															SET @var4 = '0'
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END

									SET @startPos = CHARINDEX(@strToFound2, @dataAdditional2, @startPos + 1)
								END
							FETCH NEXT FROM @getStrs2 INTO @strToFound2, @type
						END
				END
			
			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
					
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
					WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = CHARINDEX(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN
									SET @strToFoundLength = LEN(@strToFound) + 2

									SET @endPos1 = CHARINDEX(',', @dataAdditional2, @startPos)
									SET @endPos2 = CHARINDEX('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
									
									SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
									
									IF @type = 0 
										BEGIN
											IF LEFT(@var3, 4) = 'tab_'
												BEGIN
													IF LEN(@var3) > 0
														BEGIN
															SELECT @var4 = dbo.processTab_GetId(@instance, @var3)		
															IF @var4 IS NULL
																BEGIN
																	SET @var4 = ' ""'
																END
															ELSE
																BEGIN
																	SET @var4 = ' "tab_' + @var4 + '"'
																END
															SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
									
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NULL
														BEGIN
															SET @var4 = @var3
														END
													SET @dataAdditional2 = STUFF(@dataAdditional2, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs2')) >= -1
		BEGIN
			DEALLOCATE @getStrs2
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs3')) >= -1
		BEGIN
			DEALLOCATE @getStrs3
		END

	RETURN @dataAdditional2
END