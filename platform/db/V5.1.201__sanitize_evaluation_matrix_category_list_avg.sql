EXEC app_set_archive_user_id 0
DECLARE @GetTables CURSOR
DECLARE @tableName NVARCHAR(200)
DECLARE @instance NVARCHAR (10)
DECLARE @deleteSql NVARCHAR(MAX)
DECLARE @process NVARCHAR(50) = 'list_evaluation_matrix_question_categories'
 SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
 SELECT instance FROM processes WHERE process = @process 
 
 OPEN @GetTables
 FETCH NEXT FROM @GetTables INTO @instance
 WHILE (@@FETCH_STATUS=0) BEGIN 
    SET @tableName = 'dbo._' + @instance + '_' + @process
    EXEC dbo.app_ensure_column @instance, @process, 'evaluation_column_name', 0, 0, 1, 0, 0, null, 0, 0
    EXEC('UPDATE ' + @tableName + ' SET evaluation_column_name = evaluation_average_value_column')
	SET @deleteSql = 'DELETE FROM item_columns WHERE process = ''' + @process + ''' AND instance = ''' + @instance + ''' AND name = ''evaluation_average_value_column'''
    EXEC(@deleteSql)
  FETCH NEXT FROM @GetTables INTO @instance
 END
 CLOSE @GetTables