EXEC app_remove_archive 'calendars'

ALTER TABLE calendars
ADD weekly_hours DECIMAL(18,2) NULL DEFAULT -2
GO
UPDATE calendars SET weekly_hours = -2
UPDATE calendars SET weekly_hours = 37.5 WHERE parent_calendar_id IS NULL

EXEC app_ensure_archive 'calendars'
