ALTER PROCEDURE [dbo].[app_ensure_create_right_container]
  @instance nvarchar(10), @Process nvarchar(50)
AS
BEGIN
 
 DECLARE @LanguageVariable NVARCHAR(50)
 DECLARE @ConditionVariable NVARCHAR(50)
 DECLARE @ContainerId INT
 
 SET @LanguageVariable = 'cont_' + @Process + '_right_container_1';
 
 INSERT INTO process_containers (instance, process) VALUES(@instance, @Process)	
	SET @ContainerId = @@IDENTITY
	UPDATE process_containers SET variable = @LanguageVariable, right_container = 1 
	WHERE instance = @instance AND process = @Process AND process_container_id = @ContainerId
 
 	DECLARE @ConditionId INT;
	DECLARE @ConditionId2 INT;
		INSERT INTO conditions (instance, process, condition_json, order_no, variable) VALUES(@instance, @Process, 'null', 'U', 'condition_' + @Process + '_access_container_write')
		INSERT INTO conditions (instance, process, condition_json, order_no, variable) VALUES(@instance, @Process, 'null', 'U', 'condition_' + @Process + '_access_container_read')

		SET @ConditionId = (SELECT condition_id from conditions WHERE variable = 'condition_' + @Process + '_access_container_write')
		SET @ConditionId2 = (SELECT condition_id from conditions WHERE variable = 'condition_' + @Process + '_access_container_read')

		INSERT INTO conditions_groups_condition (condition_group_id, condition_id) VALUES(0, @ConditionId)
		INSERT INTO condition_containers (condition_id, process_container_id) VALUES (@ConditionId, @ContainerId)
		INSERT INTO condition_features (condition_id, feature) VALUES (@ConditionId, 'meta')
		INSERT INTO condition_states (condition_id, state) VALUES (@ConditionId, 'meta.write')
		
		
		INSERT INTO conditions_groups_condition (condition_group_id, condition_id) VALUES(0, @ConditionId2)
		INSERT INTO condition_containers (condition_id, process_container_id) VALUES (@ConditionId2, @ContainerId)
		INSERT INTO condition_features (condition_id, feature) VALUES (@ConditionId2, 'meta')
		INSERT INTO condition_states (condition_id, state) VALUES (@ConditionId2, 'meta.read')
		
		DECLARE @Language NVARCHAR(10);

		DECLARE language_c CURSOR 
		FOR SELECT 
			code from instance_languages

			OPEN language_c
		
			FETCH NEXT FROM language_c INTO @Language
				WHILE @@FETCH_STATUS = 0
				BEGIN
		
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @Process, @Language, 'condition_' + @Process + '_access_container_write', 'Access Container condition Write')
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @Process, @Language, 'condition_' + @Process + '_access_container_read', 'Access Container condition Read')
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @Process, @Language, @LanguageVariable, 'Access Container')
				FETCH NEXT FROM language_c INTO
			@Language
				END;
			CLOSE language_c;
			DEALLOCATE language_c;
 
END
GO

EXEC app_remove_archive 'item_columns'

