ALTER TABLE items ADD owner_item_id int DEFAULT(0)
GO
ALTER TRIGGER [dbo].[items_BeforeInsertRow] ON [dbo].[items] INSTEAD OF INSERT AS
    BEGIN
        IF SESSION_CONTEXT(N'restore_item_id') IS NULL BEGIN -- New item
			INSERT INTO items (instance, process, deleted, owner_item_id, created_by)
			SELECT instance, process, deleted, owner_item_id, CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
			FROM Inserted
		END ELSE BEGIN -- Restore deleted item
			SET IDENTITY_INSERT items ON
			IF SESSION_CONTEXT(N'restore_guid') IS NULL BEGIN -- No GUID
				INSERT INTO items (item_id, instance, process, deleted, owner_item_id, created_by)
				SELECT CAST(SESSION_CONTEXT(N'restore_item_id') AS INT), instance, process, deleted, owner_item_id, CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
				FROM Inserted
			END ELSE BEGIN -- Has GUID
				INSERT INTO items (item_id, [guid], instance, process, deleted, owner_item_id, created_by)
				SELECT CAST(SESSION_CONTEXT(N'restore_item_id') AS INT), CAST(SESSION_CONTEXT(N'restore_guid') AS UNIQUEIDENTIFIER), instance, process, deleted, owner_item_id, CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
				FROM Inserted
			END
			SET IDENTITY_INSERT items OFF
			EXEC sp_set_session_context @key = N'restore_item_id', @value = NULL
			EXEC sp_set_session_context @key = N'restore_guid', @value = NULL
		END
    END
    
GO

ALTER TRIGGER [dbo].[items_AfterInsertRow] ON [dbo].[items] AFTER INSERT AS
    DECLARE @user_id int;

select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));


DECLARE @sql nvarchar(max) =
    (
        SELECT
            CASE WHEN it.process = 'worktime_tracking_hours' THEN
                   ';INSERT INTO ' + it.name + ' (user_item_id, item_id, order_no) VALUES (' + CAST(@user_id AS VARCHAR) + ',' + CAST(i.item_id AS VARCHAR) + ',' +
                   '(SELECT CASE WHEN ' + CAST(it.ordering AS NVARCHAR) + '=1 THEN ISNULL(dbo.getOrderBetween((SELECT TOP 1 ISNULL(order_no,''U'') FROM ' + it.name + ' ORDER BY order_no DESC),null),''U'') ELSE ''U'' END)' + ')'
            ELSE CASE WHEN i.owner_item_id > 0 THEN
                   ';INSERT INTO ' + it.name + ' (item_id, order_no) VALUES (' + CAST(i.item_id AS VARCHAR) + ',' +
                   '(SELECT CASE WHEN ' + CAST(it.ordering AS NVARCHAR) + '=1 THEN ISNULL(dbo.getOrderBetween((SELECT TOP 1 ISNULL(order_no,''U'') FROM ' + it.name + ' WHERE owner_item_id = ' + CAST(i.owner_item_id AS NVARCHAR) + ' ORDER BY order_no DESC),null),''U'') ELSE ''U'' END)' + ')' 
            ELSE
                   ';INSERT INTO ' + it.name + ' (item_id, order_no) VALUES (' + CAST(i.item_id AS VARCHAR) + ',' +
                   '(SELECT CASE WHEN ' + CAST(it.ordering AS NVARCHAR) + '=1 THEN ISNULL(dbo.getOrderBetween((SELECT TOP 1 ISNULL(order_no,''U'') FROM ' + it.name + ' ORDER BY order_no DESC),null),''U'') ELSE ''U'' END)' + ')'
            END END
        FROM item_tables it
                 INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
        FOR XML PATH('')
    )

EXEC sp_executesql @sql

GO

ALTER PROCEDURE [dbo].[items_BuildTable]
	@Instance NVARCHAR(10),@Process NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ColId NVARCHAR(50)
	DECLARE @ColName NVARCHAR(50)
	DECLARE @DataType TINYINT
	DECLARE @D_item_id INT
	DECLARE @D_order_no NVARCHAR(MAX)
	DECLARE @D_item_column_id INT
	DECLARE @D_nvarchar NVARCHAR(255)
	DECLARE @D_int INT
	DECLARE @D_float FLOAT
	DECLARE @D_date DATE
	DECLARE @D_text NVARCHAR(MAX)
	DECLARE @ColNames NVARCHAR(MAX) = ''
	DECLARE @TableName NVARCHAR(200)
	DECLARE @GetColumns CURSOR
	DECLARE @GetData CURSOR
	DECLARE @CreateSql NVARCHAR(MAX) = ''
	DECLARE @FillSql NVARCHAR(MAX) = ''

	SET @TableName = '_' + @instance + '_' + @Process
	
	--Drop table if it exists // Trigger will actually drop the table
	DELETE FROM item_tables WHERE name = @TableName
	
	--Double Check
	IF dbo.TableExists(@TableName) = 1 BEGIN
		DECLARE @DropSql NVARCHAR(MAX) = 'DROP TABLE ' + @TableName
		EXEC sp_executesql @DropSql
	END

	--Get Columns
	SET @GetColumns = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT name,data_type FROM item_columns WHERE process = @Process ANd instance = @Instance

	OPEN @GetColumns
	FETCH NEXT FROM @GetColumns INTO @ColName,@DataType

	WHILE (@@FETCH_STATUS=0) BEGIN
		--Add Column name
		SET @CreateSql = @CreateSql + ',' + @ColName

		--Add data type
		IF @DataType = 0 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(255)'
		END ELSE IF @DataType = 1 BEGIN
			SET @CreateSql = @CreateSql + ' INT'
		END ELSE IF @DataType = 2 BEGIN
			SET @CreateSql = @CreateSql + ' FLOAT'
		END ELSE IF @DataType = 3 BEGIN
			SET @CreateSql = @CreateSql + ' DATETIME'
		END ELSE IF @DataType = 4 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(MAX)'
		END ELSE IF @DataType = 5 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(MAX)'
		END ELSE IF @DataType = 6 BEGIN
			SET @CreateSql = @CreateSql + ' INT'
		END

		FETCH NEXT FROM @GetColumns INTO @ColName,@DataType
	END
	CLOSE @GetColumns

	--Create Table
	SET @CreateSql = 'CREATE TABLE ' + @TableName + '(item_id INT PRIMARY KEY, order_no NVARCHAR(MAX) COLLATE Latin1_general_bin ' + @CreateSql + ')'
	SET @ColNames = 'data_id,order_no' + @ColNames
	EXEC sp_executesql @CreateSql
	INSERT INTO item_tables (instance,process,name) VALUES (@Instance,@Process,@tablename)
	
	--Create Archive Table
	EXEC app_ensure_archive @TableName
END

GO

DECLARE @sql nvarchar(max)
DECLARE @instance nvarchar(255)
DECLARE instanceCursor CURSOR FOR
    SELECT t.name TableName
    FROM sys.columns c  
    inner join sys.tables t on c.object_id = t.object_id
    where c.name = 'order_no' and collation_name <> 'Latin1_general_bin';

OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
		DECLARE @c NVARCHAR(255) =
		(SELECT
		    df.name
		FROM sys.default_constraints df
		INNER JOIN sys.tables t ON df.parent_object_id = t.object_id
		INNER JOIN sys.columns c ON df.parent_object_id = c.object_id AND df.parent_column_id = c.column_id
		WHERE t.name = @instance AND c.name = 'order_no')

		SET @sql = 'ALTER TABLE dbo.' + @instance + ' DROP CONSTRAINT ' + @c
    	EXEC (@sql)

		SET @sql = 'ALTER TABLE dbo.' + @instance + ' ALTER COLUMN order_no VARCHAR(MAX) COLLATE Latin1_general_bin'
    	EXEC (@sql)
        FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_archive]
@tableName NVARCHAR(200)
AS
BEGIN
    -- Declaring variables	
    DECLARE @colNames NVARCHAR(MAX);
    DECLARE @colNamesInsert NVARCHAR(MAX);
    DECLARE @colTypes NVARCHAR(MAX);
    DECLARE @colDefs NVARCHAR(MAX);
    DECLARE @pkCols NVARCHAR(MAX);
    DECLARE @pkCols2 NVARCHAR(MAX);

    DECLARE @sql NVARCHAR(MAX);
    DECLARE @viewIndexSql nvarchar(MAX);

    -- Setting action variable
    DECLARE @action nvarchar(20)
    SET @action ='CREATE'

    -- Setting table names
    DECLARE @nameArchive NVARCHAR(200);
    DECLARE @nameExpired NVARCHAR(200);
    DECLARE @nameHistoryFunction NVARCHAR(100);
    DECLARE @nameHistoryFunctionWithId NVARCHAR(100);

    SET @nameArchive = 'archive_'+@tableName;
    SET @nameExpired = 'old_'+@tableName;
    SET @nameHistoryFunction = 'get_'+@tableName;
    SET @nameHistoryFunctionWithId = 'get_'+@tableName + '_with_id';

    -- Setting column names
    DECLARE @archiveId NVARCHAR(100);
    DECLARE @archiveUserId NVARCHAR(100);
    DECLARE @archiveStart NVARCHAR(100);
    DECLARE @archiveEnd NVARCHAR(100);
    DECLARE @archiveType NVARCHAR(100);


    SET @archiveId = 'archive_id';
    SET @archiveStart = 'archive_start';
    SET @archiveEnd = 'archive_end';
    SET @archiveUserId = 'archive_userid';
    SET @archiveType = 'archive_type';


    DECLARE @defaultValue NVARCHAR(255);
    SET @defaultValue = null;

    DECLARE @archiveColsCount int;

    -- Getting list of columns in original table, 
    --	colNames is list of cols, 
    --	colTypes is list of names with datatypes
    --	colDefs is list of colnames encapsulated with isnull and column default value for insert	

    --Drop old archive TRIGGERS
    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN
        SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_UPDATE]'
        EXEC (@sql)
    END

    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_INSERT]')) BEGIN
        SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_INSERT]'
        EXEC (@sql)
    END
    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_Delete]')) BEGIN
        SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_Delete]'
        EXEC (@sql)
    END




    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameArchive+']')) BEGIN

        exec app_set_archive_user_id 1
        SET @sql = 'update '+@TableName+' set archive_start = getutcdate() WHERE archive_start is null '
        EXEC (@sql)
        SET @sql = 'update '+@TableName+' set archive_start = getutcdate(), archive_userid=1 WHERE archive_userid is null or archive_userid is null'
        EXEC (@sql)
    END

    IF COL_LENGTH(@tableName,@archiveUserId) IS NULL
        BEGIN
            SET @sql = 'ALTER TABLE ['+@tableName+'] ADD '+@archiveUserId+' int  DEFAULT 0'
            EXEC (@sql)
        END
    ELSE
        BEGIN
            SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN '+@archiveUserId+' int NOT NULL'
            EXEC (@sql)

            SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveUserId
            IF @defaultValue IS NULL
                BEGIN
                    SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT 0 FOR '+@archiveUserId
                    EXEC (@sql)
                END

            EXEC (@sql)
        END

    IF COL_LENGTH(@tableName,@archiveStart) IS NULL
        BEGIN
            SET @sql = 'ALTER TABLE ['+@tableName+'] ADD '+@archiveStart+' [datetime] DEFAULT getutcdate()'
            EXEC (@sql)
        END
    ELSE
        BEGIN
            SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN  '+@archiveStart+' [datetime]'
            EXEC (@sql)

            SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveStart
            IF @defaultValue IS NULL
                BEGIN
                    SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT getutcdate() FOR '+@archiveStart
                    EXEC (@sql)
                END
        END

    SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = @nameArchive
    IF @archiveColsCount = 0
        BEGIN
            SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
                   @colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' +
                               data_type +
                               CASE WHEN DATA_TYPE = 'decimal' THEN '(' + CAST(NUMERIC_PRECISION AS nvarchar) + ',' + CAST(NUMERIC_SCALE AS nvarchar) + ')' ELSE '' END +
                               COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
                               ( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' +
                               CASE WHEN DATA_TYPE = 'uniqueidentifier' THEN '' ELSE COALESCE('DEFAULT '+COLUMN_DEFAULT,'') END,
                   @colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE table_name = @tablename

            -- Creating archive table
            SET @sql = '
				CREATE TABLE dbo.'+@nameArchive+'(
					['+@archiveId+'] int IDENTITY(1,1) NOT NULL,
					['+@archiveEnd+'] [datetime] NULL,
					['+@archiveType+'] tinyint NOT NULL,
					'+@colTypes+')  ON [PRIMARY]'
            EXEC (@sql)

        END
    ELSE
        BEGIN
            SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)
            IF @archiveColsCount > 0
                BEGIN
                    SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
                           @colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' +
                                       data_type +
                                       CASE WHEN DATA_TYPE = 'decimal' THEN '(' + CAST(NUMERIC_PRECISION AS nvarchar) + ',' + CAST(NUMERIC_SCALE AS nvarchar) + ')' ELSE '' END +
                                       COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
                                       ( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' +
                                       COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
                           @colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
                    FROM INFORMATION_SCHEMA.COLUMNS
                    WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)

                    SET @sql = 'ALTER TABLE dbo.'+@nameArchive+' ADD '+@colTypes
                    EXEC (@sql)
                END

            SET @action ='ALTER'

            --[item_columns_AFTER_UPDATE]
            --[item_columns_AFTER_INSERT]
            --[item_columns_Delete]
        END


	SET @sql = 'IF COL_LENGTH(''' + @nameArchive + ''', ''order_no'') IS NOT NULL
				BEGIN
				ALTER TABLE dbo.' + @nameArchive + ' ALTER COLUMN order_no VARCHAR(MAX) COLLATE Latin1_general_bin
				END'
    EXEC (@sql)


    SET @colNames = null
    SET @colTypes = null
    SET @colDefs = null

    DECLARE @pkColsCount int = 0

    SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
           @colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' +
                       data_type +
                       CASE WHEN DATA_TYPE = 'decimal' THEN '(' + CAST(NUMERIC_PRECISION AS nvarchar) + ',' + CAST(NUMERIC_SCALE AS nvarchar) + ')' ELSE '' END +
                       COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
                       ( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' +
                       CASE WHEN DATA_TYPE = 'uniqueidentifier' THEN '' ELSE COALESCE('DEFAULT '+COLUMN_DEFAULT,'') END,
           @colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = @tablename

    SELECT @colNamesInsert = COALESCE(@colNamesInsert+',','')+'['+column_name+']'
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = @tablename AND (select columnproperty(object_id(@tablename),column_name,'IsIdentity')) = 0 AND column_name <> @archiveUserId

    SELECT @pkColsCount = count(column_name)
    from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
            INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
    where 	pk.TABLE_NAME = @tableName
      and	CONSTRAINT_TYPE = 'PRIMARY KEY'
      and	c.TABLE_NAME = pk.TABLE_NAME
      and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
      and pk.table_catalog = (SELECT db_name())

    SELECT @pkCols = COALESCE(@pkCols+',','')+'['+column_name+']'
    from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
            INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
    where 	pk.TABLE_NAME = @tableName
      and	CONSTRAINT_TYPE = 'PRIMARY KEY'
      and	c.TABLE_NAME = pk.TABLE_NAME
      and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
      and pk.table_catalog = (SELECT db_name())

    IF @pkColsCount > 1
        BEGIN
            SELECT @pkCols2 = COALESCE(@pkCols2+'+','')+'CAST(['+column_name+'] as nvarchar) '
            from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
                    INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
            where 	pk.TABLE_NAME = @tableName
              and	CONSTRAINT_TYPE = 'PRIMARY KEY'
              and	c.TABLE_NAME = pk.TABLE_NAME
              and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
              and pk.table_catalog = (SELECT db_name())
        END
    ELSE
        BEGIN
            SELECT @pkCols2 = COALESCE(@pkCols2+'+','')+'['+column_name+']'
            from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
                    INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
            where 	pk.TABLE_NAME = @tableName
              and	CONSTRAINT_TYPE = 'PRIMARY KEY'
              and	c.TABLE_NAME = pk.TABLE_NAME
              and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
              and pk.table_catalog = (SELECT db_name())
        END

    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN
        SET @action ='ALTER'
    END
    ELSE begin
        SET @action ='CREATE'
    END

    SET @sql =  @action + ' TRIGGER [dbo].['+@tableName+'_INSTEAD_OF_INSERT]
	ON [dbo].['+@tableName+']
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
    IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, ''Archive User Id has not been set'', 1; 
	INSERT INTO [dbo].['+@tableName+'] ('+@colNamesInsert+', '+@archiveUserId+') 
	SELECT '+@colNamesInsert+' 
	, @user_id
	FROM inserted
	'
    EXEC (@sql)


    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN
        SET @action ='ALTER'
    END
    ELSE begin
        SET @action ='CREATE'
    END

    -- Creating UPDATE triggers
    SET @sql = @action + ' TRIGGER [dbo].['+@tableName+'_AFTER_UPDATE]
	ON [dbo].['+@tableName+']
	AFTER UPDATE
	AS

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
    IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, ''Archive User Id has not been set'', 1; 
IF ISNULL((SELECT archive FROM item_tables WHERE name = ''' + @tableName + '''),1) = 1 AND @user_id <> -1 BEGIN
    UPDATE [dbo].['+@tableName+']
    SET ' + @archiveStart + '= getutcdate(),
    ' + @archiveUserId + '=  @user_id
    WHERE ' + @pkCols2 + ' IN (SELECT DISTINCT ' + @pkCols2 + ' FROM Inserted)

	INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
	SELECT '+@colNames+',
		'+@archiveEnd+' = getutcdate(),
		'+@archiveType+' = 1
	FROM deleted	
END
	'
    EXEC (@sql)

    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_DELETE]')) BEGIN
        SET @action ='ALTER'
    END
    ELSE begin
        SET @action ='CREATE'
    END

    -- Creating Delete Trigger 
    SET @sql =  @action +' TRIGGER [dbo].['+@tableName+'_DELETE]
		ON [dbo].['+@tableName+']
		FOR DELETE
		AS	
            DECLARE @user_id int;
			select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
IF ISNULL((SELECT archive FROM item_tables WHERE name = ''' + @tableName + '''),1) = 1 AND @user_id <> -1 BEGIN
			INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
			SELECT '+@colNames+',
				'+@archiveEnd+' = getutcdate(),
				'+@archiveType+' = 1
			FROM deleted
			
			
            IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, ''Archive User Id has not been set'', 1; 
			INSERT INTO '+@nameArchive+'('+REPLACE(@colNames,'['+@archiveUserId+'],','')+','+@archiveEnd+', '+@archiveType+', '+@archiveUserId+')
			SELECT '+REPLACE(@colNames,'['+@archiveUserId+'],','')+',
				'+@archiveEnd+' = getutcdate(),
				'+@archiveType+' = 2,
				'+@archiveUserId+' = @user_id
			FROM deleted
END
			'
    EXEC (@sql)


    -- Creating or altering history function, depends if it exist

    IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameHistoryFunction+']')) BEGIN
        SET @action ='ALTER'
    END
    ELSE begin
        SET @action ='CREATE'
    END

    SET @sql = @action +' FUNCTION  [dbo].['+@nameHistoryFunction+']( @d datetime )
		RETURNS @rtab TABLE (archive_id INT, '+@colTypes+')
		AS
		BEGIN	
			INSERT @rtab
				SELECT archive_id, '+@colNames+'
				FROM '+@nameArchive+' WHERE '+@archiveId+' IN (
					SELECT MAX('+@archiveId+')
					FROM '+@nameArchive+'
					WHERE '+@archiveStart+' <= @d AND archive_end > @d AND ' + @pkCols2 + ' NOT IN
					(SELECT ' + @pkCols2 + ' FROM  '+@tableName+'  WHERE '+@archiveStart+' <= @d  )
					GROUP BY ' + @pkCols + '
					)
				UNION 
				SELECT 0, '+@colNames+' 
				FROM '+@tableName+' 
				WHERE '+@archiveStart+' <= @d  
			RETURN
		END'
    EXEC (@sql)

    -- Creating or altering history function with item_id, depends if it exist

    DECLARE @isDynamicTable int;
    SELECT @isDynamicTable = COUNT(name) FROM item_tables WHERE name = @tablename
    IF @isDynamicTable > 0 BEGIN
        IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameHistoryFunctionWithId+']')) BEGIN
            SET @action ='ALTER'
        END
        ELSE begin
            SET @action ='CREATE'
        END

        SET @sql = @action +' FUNCTION  [dbo].['+@nameHistoryFunctionWithId+']( @item_id int, @d datetime  )
			RETURNS @rtab TABLE (archive_id INT, '+@colTypes+')
			AS
			BEGIN	
				INSERT @rtab
					SELECT archive_id, '+@colNames+'
					FROM '+@nameArchive+' WHERE '+@archiveId+' IN (
						SELECT MAX('+@archiveId+')
						FROM '+@nameArchive+'
						WHERE item_id = @item_id AND '+@archiveStart+' <= @d AND archive_end > @d AND ' + @pkCols2 + ' NOT IN
						(SELECT ' + @pkCols2 + ' FROM  '+@tableName+'  WHERE item_id = @item_id AND '+@archiveStart+' <= @d  )
						GROUP BY ' + @pkCols + '
						)
					UNION 
					SELECT 0, '+@colNames+' 
					FROM '+@tableName+' 
					WHERE item_id = @item_id AND '+@archiveStart+' <= @d  
				RETURN
			END'
        EXEC (@sql)
    END

    exec app_set_archive_user_id 1
    SET @sql = 'update '+@TableName+' set archive_start = getutcdate() WHERE archive_start is null '
    EXEC (@sql)
    SET @sql = 'update '+@TableName+' set archive_start = getutcdate(), archive_userid=1 WHERE archive_userid is null or archive_userid is null'
    EXEC (@sql)

	-- Creating indexies if not exist

	CREATE TABLE #indexes (index_name SYSNAME, index_description VARCHAR(210), index_keys NVARCHAR(2078))
	INSERT INTO #indexes EXEC sp_helpindex @TableName

	IF NOT EXISTS (SELECT * FROM #indexes WHERE index_keys = 'archive_start')
		BEGIN
			SET @sql = 'CREATE INDEX IX_' + @TableName + '_archive_start ON ' + @TableName + '(archive_start)'
			EXEC (@sql)
		END

	IF NOT EXISTS (SELECT * FROM #indexes WHERE index_keys IN ('item_id, archive_start'
															 , 'archive_start, item_id'))
		BEGIN
			SET @sql = 'CREATE INDEX IX_' + @TableName + '_item_id_archive_start ON ' + @TableName + '(item_id, archive_start)'
			EXEC (@sql)
		END

	DROP TABLE #indexes
	
	CREATE TABLE #archive_indexes (index_name SYSNAME, index_description VARCHAR(210), index_keys NVARCHAR(2078))
	INSERT INTO #archive_indexes EXEC sp_helpindex @nameArchive

	IF NOT EXISTS (SELECT * FROM #archive_indexes WHERE index_keys = 'archive_id')
		BEGIN
			SET @sql = 'CREATE UNIQUE CLUSTERED INDEX IX_' + @nameArchive + '_archive_id ON ' + @nameArchive + '(archive_id)'
			EXEC (@sql)
		END

	IF NOT EXISTS (SELECT * FROM #archive_indexes WHERE index_keys IN ('item_id, archive_start, archive_end'
																	 , 'item_id, archive_end, archive_start'
																	 , 'archive_start, item_id, archive_end'
																	 , 'archive_start, archive_end, item_id'
																	 , 'archive_end, item_id, archive_start'
																	 , 'archive_end, archive_start, item_id'))
		BEGIN
			SET @sql = 'CREATE INDEX IX_' + @nameArchive + '_item_id_archive_start_archive_end ON ' + @nameArchive + '(item_id, archive_start, archive_end)'
			EXEC (@sql)
		END

	IF NOT EXISTS (SELECT * FROM #archive_indexes WHERE index_keys = 'item_id')
		BEGIN
			SET @sql = 'CREATE INDEX IX_' + @nameArchive + '_item_id ON ' + @nameArchive + '(item_id)'
			EXEC (@sql)
		END

	DROP TABLE #archive_indexes

END
GO