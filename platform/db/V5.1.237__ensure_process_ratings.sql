GO
ALTER PROCEDURE [dbo].[items_AddColumn]
	@TableName NVARCHAR(255), @ColumnName NVARCHAR(50), @DataType TINYINT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @DataTypeName NVARCHAR(255)
	IF @DataType = 0 BEGIN
		SET @DataTypeName = 'NVARCHAR(255)'
	END ELSE IF @DataType = 1 BEGIN
		SET @DataTypeName = 'INT'
	END ELSE IF @DataType = 2 BEGIN
		SET @DataTypeName = 'FLOAT'
	END ELSE IF @DataType = 3 BEGIN
		SET @DataTypeName = 'DATE'
	END ELSE IF @DataType = 4 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 5 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 6 BEGIN
		SET @DataTypeName = 'INT'
	END ELSE IF @DataType = 7 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 10 BEGIN
		SET @DataTypeName = 'NVARCHAR(255)'
	END ELSE IF @DataType = 13 BEGIN
		SET @DataTypeName = 'DATETIME'
	END ELSE IF @DataType = 14 BEGIN
		SET @DataTypeName = 'FLOAT'
	END
	
	DECLARE @Sql NVARCHAR(MAX) = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataTypeName
	PRINT @Sql
	EXEC sp_executesql @Sql 

	exec app_ensure_archive @TableName
END

GO
CREATE PROCEDURE [dbo].[app_ensure_process_process_ratings]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'process_ratings'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_process @instance, @process, @process, 0

    EXEC dbo.app_ensure_column @instance, @process, 'value', 0, 2, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'user_item_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'process_item_id', 0, 1, 1, 0, 0, NULL, 0, 0

  END

GO
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor
FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
BEGIN
  exec app_ensure_process_process_ratings @instance
END
FETCH NEXT FROM instanceCursor INTO @instance
CLOSE instanceCursor
DEALLOCATE instanceCursor
