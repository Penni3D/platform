ALTER TRIGGER [dbo].[item_data_process_selections_AFTER_UPDATE]
    ON [dbo].[item_data_process_selections]
    AFTER UPDATE
    AS

    DECLARE @user_id int;
select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

IF @user_id > -1 BEGIN
    UPDATE [dbo].[item_data_process_selections]
    SET archive_start= getutcdate(),
        archive_userid=  @user_id
    WHERE [item_column_id]+[item_id]+[selected_item_id] IN (SELECT DISTINCT [item_column_id]+[item_id]+[selected_item_id] FROM Inserted)
    
    INSERT INTO archive_item_data_process_selections([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],archive_end, archive_type)
    SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],
           archive_end = getutcdate(),
           archive_type = 1
    FROM deleted
END

GO

ALTER TRIGGER [dbo].[item_data_process_selections_DELETE]
    ON [dbo].[item_data_process_selections]
    FOR DELETE
    AS
    INSERT INTO archive_item_data_process_selections([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],archive_end, archive_type)
    SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start],
           archive_end = getutcdate(),
           archive_type = 1
    FROM deleted

DECLARE @user_id int;
select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

IF @user_id > -1 BEGIN
    INSERT INTO archive_item_data_process_selections([item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_start],archive_end, archive_type, archive_userid)
    SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_start],
           archive_end = getutcdate(),
           archive_type = 2,
           archive_userid = @user_id
    FROM deleted
END