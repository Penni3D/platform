ALTER PROCEDURE [dbo].[app_ensure_tab]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @group_id INT, @tab_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_tabs WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @tab_id = process_tab_id FROM process_tabs WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		DECLARE @langVar NVARCHAR (50) = 'TAB_' + @process + '_' + @variable

		INSERT INTO process_tabs (instance, process, variable, order_no) VALUES (@instance, @process, @langVar, 1);
		SELECT @tab_id = @@IDENTITY; --SCOPE_IDENTITY();

		INSERT INTO process_group_tabs (process_group_id, process_tab_id, order_no) VALUES (@group_id, @tab_id, 1);
		
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND process = @process AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, UPPER(@langVar), @variable, 'en-GB')
		END
		
	END
END