IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_list_bar_icons'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_list_bar_icons] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_list_bar_icons]
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'list_bar_icons'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	DECLARE @tableName NVARCHAR(50) = ''
	SET @tableName = '_' + @instance + '_' + @process
	DECLARE @sql NVARCHAR(MAX) = ''
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_list @instance, @process, @process, 1

	UPDATE processes SET list_order = 'list_item' WHERE process = @process

	EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0

	INSERT INTO items (instance, process) VALUES (@instance, @process);
	DECLARE @itemId INT = @@identity;

	SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''None'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
	EXEC (@sql)
	
	
END;
GO
