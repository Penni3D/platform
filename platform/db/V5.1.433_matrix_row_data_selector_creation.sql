DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
	BEGIN
	
		IF(SELECT COUNT(*) FROM process_portfolios WHERE process = 'matrix_process_row_data' AND instance = @instance) = 0
	
		BEGIN
	
		DECLARE @process NVARCHAR(50) = 'matrix_process_row_data';
		DECLARE @portfolioId INT;
		DECLARE @conditionId INT;


		EXEC app_ensure_portfolio @instance, @process,'MATRIX_PROCESS_ROW_DATA_SELECTOR', @portfolio_id = @portfolioId OUTPUT
		UPDATE process_portfolios SET process_portfolio_type = 10 WHERE process_portfolio_id = @portfolioId
		INSERT INTO process_portfolio_columns(process_portfolio_id, report_column, item_column_id)
		(SELECT @portfolioId, 1, item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name IN ('item_column_id', 'owner_item_id', 'question_item_id', 'cell_item_id', 'score'))

	
		EXEC app_ensure_condition @instance, @process, 0, 0, 'MATRIX_PROCESS_ROW_DATA_SELECTOR_READ', 'null', @condition_id = @conditionId OUTPUT
		DELETE FROM condition_features WHERE condition_id = @conditionId

		INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')
		INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'portfolio')
		INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, @portfolioId)
		END

		ELSE 
		BEGIN
	
		PRINT('ELSE')
		END

		FETCH  NEXT FROM instance_cursor INTO @instance
	END
	