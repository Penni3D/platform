IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_business_2'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_business_2] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROCEDURE [dbo].[app_ensure_process_business_2]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'business_2'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, 'business_2', 'business_2', 2
	
	EXEC dbo.app_ensure_column @instance,'business_2','idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','evaluation_of_benefits_1', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','evaluation_of_benefits_2', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','evaluation_of_benefits_3', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','evaluation_of_benefits_4', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','risk_assessment_1', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','risk_assessment_2', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','evaluation_bet_1', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','evaluation_bet_2', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,'business_2','evaluation_bet_3', 0, 0, 1, 0, 0, NULL, 0, 0
END
GO

