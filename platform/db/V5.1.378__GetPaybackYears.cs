using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.db {
	class GetPaybackYears : CsMigration {
		public override bool Execute(IConnections db) {

		string instance = Conv.ToStr(db.ExecuteScalar("SELECT TOP 1 instance FROM instances"));

			string archiveEnd = "(@archiveDate)";
			string startPart = @"SET ANSI_NULLS ON 

			GO

			SET QUOTED_IDENTIFIER ON
			GO

			CREATE FUNCTION[dbo].[GetPaybackYears]
			(
				@process_item_id int,
				@positives nvarchar(25),
				@negatives nvarchar(25),
				@type nvarchar(25),
				@mode int,
				@archiveDate date
			)
			RETURNS FLOAT
			AS
			BEGIN

			DECLARE @paybackDaTa TABLE(row_index int, amount float, item_id int, date date, selected_item_id int)

			";
			string middlePart = "";
			middlePart += " IF @archiveDate IS NULL BEGIN ";
			middlePart += " INSERT INTO @paybackDaTa SELECT  ROW_NUMBER() OVER(";
			middlePart += " PARTITION BY budget_item.project_item_id";

			middlePart += " ORDER BY budget_item.date";
			middlePart += " ) row_index,budget_item.amount,budget_item.item_id,budget_item.date,selection.selected_item_id FROM _" + instance + "_budget_item budget_item ";
			middlePart += " LEFT JOIN item_data_process_selections selection ON budget_item.item_id=selection.item_id";
			middlePart += " LEFT JOIN _" + instance + "_list_budget_item_type item_type ON item_type.item_id=selection.item_column_id";
			middlePart += " LEFT JOIN _" + instance + "_list_budget_category category ON category.item_id=selection.selected_item_id";

			middlePart += " where project_item_id=@process_item_id AND ',' + @type + ',' LIKE '%,' + CAST(selection.item_column_id AS nvarchar(10)) + ',%' AND amount!=0";
			middlePart += " order by date ";
			middlePart += " END ";

			middlePart += " ELSE BEGIN";

			middlePart += " INSERT INTO @paybackDaTa SELECT  ROW_NUMBER() OVER(";
			middlePart += " PARTITION BY budget_item.project_item_id";

			middlePart += " ORDER BY budget_item.date";
			middlePart += " ) row_index,budget_item.amount,budget_item.item_id,budget_item.date,selection.selected_item_id FROM get__" + instance + "_budget_item" + archiveEnd + " budget_item ";
			middlePart += " LEFT JOIN item_data_process_selections selection ON budget_item.item_id=selection.item_id";
			middlePart += " LEFT JOIN get__" + instance + "_list_budget_item_type" + archiveEnd + " item_type ON item_type.item_id=selection.item_column_id";
			middlePart += " LEFT JOIN get__" + instance + "_list_budget_category" + archiveEnd + " category ON category.item_id=selection.selected_item_id";

			middlePart += " where project_item_id=@process_item_id AND ',' + @type + ',' LIKE '%,' + CAST(selection.item_column_id AS nvarchar(10)) + ',%' AND amount!=0";
			middlePart += " order by date ";
			middlePart += " END ";


			string endpart = @"DECLARE @numberOfRows INT
			SET @numberOfRows = (SELECT COUNT(*) FROM @paybackDaTa)

			DECLARE @run INT
			DECLARE @sum FLOAT
			DECLARE @tempD DATE
			DECLARE @startD DATE
			DECLARE @endD DATE
			DECLARE @count INT

			DECLARE @negative FLOAT
			DECLARE @positive FLOAT

			SET @run = 1
			SET @sum = 0
			SET @count = 0
			WHILE(@run<@numberOfRows+1)
			BEGIN

			SET @negative=0
			set @positive = 0
			SET @count = 0

			SET @tempD = (SELECT date FROM @paybackDaTa WHERE row_index=@run)

			IF @tempD IS NOT NULL AND @startD IS NULL BEGIN SET @startD = @tempD END
			SET @count = (SELECT COUNT(item_id) FROM @paybackDaTa WHERE date=@tempD)

			SET @negative=(SELECT SUM(amount) FROM @paybackDaTa WHERE ',' + @negatives + ',' LIKE '%,' + CAST(selected_item_id AS nvarchar(10)) + ',%' AND date=@tempD)
			SET @positive=(SELECT SUM(amount) FROM @paybackDaTa WHERE ',' + @positives + ',' LIKE '%,' + CAST(selected_item_id AS nvarchar(10)) + ',%' AND date=@tempD)

			IF @negative IS null BEGIN SET @negative=0 END
			IF @positive IS null BEGIN SET @positive=0 END

			SET @sum = @sum + @positive - @negative
	  		IF @count=0 BEGIN
	  			SET @count=1
	  		END
			SET @run  = @run + @count

			IF @sum=0 OR @sum>0 BEGIN
				SET @run = @numberOfRows+1
				SET @endD = @tempD
			END
			END

			IF @startD IS NOT NULL AND @endD IS NOT NULL BEGIN
				IF @mode=0 BEGIN
				return datediff(YEAR, @startD, @endD)
				END
				IF @mode=1 BEGIN
				return CAST(datediff(QUARTER, @startD, @endD) AS float)/4
				END
				IF @mode=2 BEGIN
				return CAST(datediff(MONTH, @startD, @endD) AS float)/12
				END
			END

			return null
			END
			GO
			";
			string sql = startPart + middlePart + endpart;

			db.ExecuteDeploymentScript(sql);
			return true;
		}
	}
}