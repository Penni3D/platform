DECLARE @calendar_id INT
DECLARE calCursor CURSOR FOR SELECT calendar_id from calendars c WHERE c.item_id IS NOT NULL
OPEN calCursor
FETCH NEXT FROM calCursor INTO @calendar_id
WHILE @@fetch_status = 0 BEGIN
    EXEC app_set_archive_user_id 0
    EXEC dbo.Calendar_CheckWeekends @calendar_id,'2018-01-01','2025-01-01'
    FETCH  NEXT FROM calCursor INTO @calendar_id
END
CLOSE calCursor
DEALLOCATE calCursor