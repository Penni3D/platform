EXEC app_set_archive_user_id 1

DECLARE @sql NVARCHAR(MAX)
DECLARE @name_ic INT
SELECT @name_ic = item_column_id FROM item_columns WHERE process='notification' AND name = 'name_variable'
DECLARE @title_ic INT
SELECT @title_ic = item_column_id FROM item_columns WHERE process='notification' AND name = 'title_variable'
DECLARE @body_ic INT
SELECT @body_ic = item_column_id FROM item_columns WHERE process='notification' AND name = 'body_variable'

DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
  SET @sql = 'UPDATE process_translations	SET process_translations.variable = ''variable_name_variable_'' + CAST(item_id AS NVARCHAR) + ''_' + CAST(@name_ic AS NVARCHAR) + ''' FROM _' + @instance + '_notification INNER JOIN process_translations ON _' + @instance + '_notification.name_variable = process_translations.variable'
  EXEC (@sql)

  SET @sql = 'UPDATE process_translations SET process_translations.variable = ''variable_title_variable_'' + CAST(item_id AS NVARCHAR) + ''_' + CAST(@title_ic AS NVARCHAR) + ''' FROM _' + @instance + '_notification INNER JOIN process_translations ON _' + @instance + '_notification.title_variable = process_translations.variable'
  EXEC (@sql)

  SET @sql = 'UPDATE process_translations SET process_translations.variable = ''variable_body_variable_'' + CAST(item_id AS NVARCHAR) + ''_' + CAST(@body_ic AS NVARCHAR) + ''' FROM _' + @instance + '_notification INNER JOIN process_translations ON _' + @instance + '_notification.body_variable = process_translations.variable'
  EXEC (@sql)
   	
  SET @sql = 'UPDATE _' + @instance + '_notification SET name_variable = ''variable_name_variable_'' + CAST(item_id AS NVARCHAR) + ''_' + CAST(@name_ic AS NVARCHAR) + ''',	title_variable = ''variable_title_variable_'' + CAST(item_id AS NVARCHAR) + ''_' + CAST(@title_ic AS NVARCHAR) + ''',	body_variable = ''variable_body_variable_'' + CAST(item_id AS NVARCHAR) + ''_' + CAST(@body_ic AS NVARCHAR) + ''''
  EXEC (@sql)

  FETCH  NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor





