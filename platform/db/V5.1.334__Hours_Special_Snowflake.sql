ALTER TRIGGER [dbo].[items_AfterInsertRow] ON [dbo].[items] AFTER INSERT AS
    DECLARE @user_id int;
select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
DECLARE @sql nvarchar(max) =
    (
        SELECT
            CASE WHEN it.process = 'worktime_tracking_hours' THEN
                         ';INSERT INTO ' + it.name + ' (user_item_id, item_id, order_no) VALUES (' + CAST(@user_id AS VARCHAR) + ',' + CAST(i.item_id AS VARCHAR) + ',' +
                         '(SELECT CASE WHEN ' + CAST(it.ordering AS NVARCHAR) + '=1 THEN ISNULL(dbo.getOrderBetween((SELECT TOP 1 ISNULL(order_no,''U'') FROM ' + it.name + ' ORDER BY order_no COLLATE Latin1_general_bin DESC),null),''U'') ELSE ''U'' END)' + ')'
                 ELSE
                         ';INSERT INTO ' + it.name + ' (item_id, order_no) VALUES (' + CAST(i.item_id AS VARCHAR) + ',' +
                         '(SELECT CASE WHEN ' + CAST(it.ordering AS NVARCHAR) + '=1 THEN ISNULL(dbo.getOrderBetween((SELECT TOP 1 ISNULL(order_no,''U'') FROM ' + it.name + ' ORDER BY order_no COLLATE Latin1_general_bin DESC),null),''U'') ELSE ''U'' END)' + ')'
                END
        FROM item_tables it
                 INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
        FOR XML PATH('')
    )

EXEC sp_executesql @sql