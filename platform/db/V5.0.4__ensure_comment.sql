SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_comment'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_comment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_comment]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'comment'
	DECLARE @tableName NVARCHAR(50) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create conditions
	EXEC app_ensure_condition @instance, @process, 0, 0
	
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'COMMENTS', @portfolio_id = @p1 OUTPUT

	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'comment_owner', 0, 5, 1, 1, 0, null,  0, 0, 'user', null, @item_column_id = @i1 OUTPUT
	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'comment_published', 0, 1, 1, 1, 0, null,  0, 0, null, null, @item_column_id = @i2 OUTPUT

	EXEC app_ensure_column @instance, @process, 'comment_text', 0, 0, 1, 1, 0, null,  0, 0

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'comment_created_date', 0, 13, 1, 1, 0, null,  0, 0,  null, null, @item_column_id = @i3 OUTPUT
	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'comment_published_date', 0, 13, 1, 1, 0, null,  0, 0, null, null, @item_column_id = @i4 OUTPUT
	
	--Create conditions
	DECLARE @c1 AS NVARCHAR(MAX)

	DECLARE @cId1 INT
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i2 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'READ', @c1, @condition_id = @cId1 OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId1, 'read')

	DECLARE @cId2 INT
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":3,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"UserColumnTypeId":2,"ComparisonTypeId":3},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i2 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'WRITE', @c1, @condition_id = @cId2 OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId2, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId2, 'write')
	
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'CommentsPortfolioId', @p1, 0, null

	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 

	--Create actions
	DECLARE @actionId INT
	exec app_ensure_action @instance, @process, 'NEW_ROW', @process_action_id = @actionId OUTPUT
	exec app_ensure_action_row @actionId, 0, @i2, 0, '0'
	exec app_ensure_action_row @actionId, 0, @i1, 2, null
	exec app_ensure_action_row @actionId, 0, @i3, 1, null

	update processes set new_row_action_id = @actionId WHERE instance = @instance and process = @process

	DECLARE @actionId2 INT
	exec app_ensure_action @instance, @process, 'PUBLISH_COMMENT', @process_action_id = @actionId2 OUTPUT
	exec app_ensure_action_row @actionId2, 0, @i2, 0, '1'
	exec app_ensure_action_row @actionId2, 0, @i4, 1, null

	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'CommentsPublishActionId', @actionId2, 0, null
END;
GO
