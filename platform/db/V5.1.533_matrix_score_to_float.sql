DECLARE @Process NVARCHAR(MAX)
DECLARE @sqlCommand NVARCHAR(MAX)
DECLARE @sqlCommand2 NVARCHAR(MAX)
DECLARE @Instance NVARCHAR(MAX)
DECLARE @ItemColumnId int;
DECLARE @ItemColumnId2 int;

DECLARE cursor_instance CURSOR

FOR SELECT instance from instances

OPEN cursor_instance

FETCH NEXT FROM cursor_instance INTO @Instance
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sqlCommand2 = 'ALTER TABLE _' + @Instance + '_matrix_process_row_data ALTER COLUMN score float;' 
		SET @ItemColumnId2 = (SELECT item_column_id FROM item_columns where name = 'score' AND process = 'matrix_process_row_data' AND instance = @Instance)
		EXEC (@sqlCommand2)
		UPDATE item_columns SET data_type = 2 WHERE item_column_id = @ItemColumnId2


	DECLARE cursor_process CURSOR
		FOR SELECT process FROM processes WHERE process_type = 4

		OPEN cursor_process

	FETCH NEXT FROM cursor_process INTO @Process
	WHILE @@FETCH_STATUS = 0
    BEGIN
		SET @sqlCommand = 'ALTER TABLE _' + @Instance + '_' + @Process + ' ALTER COLUMN score float;' 
		SET @ItemColumnId = (SELECT item_column_id FROM item_columns where name = 'score' AND process = @Process)
		UPDATE item_columns SET data_type = 2 WHERE item_column_id = @ItemColumnId
	
		EXEC (@sqlCommand)
        FETCH NEXT FROM cursor_process INTO 
            @Process
					
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;

   FETCH NEXT FROM cursor_instance INTO 
            @Instance

END;
CLOSE cursor_instance;
DEALLOCATE cursor_instance;


