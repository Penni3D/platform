ALTER PROCEDURE [dbo].[app_ensure_process_worktime_tracking_basic_tasks]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'worktime_tracking_basic_tasks'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_list @instance, @process, @process, 1

    UPDATE processes SET list_order = 'list_item' WHERE process = @process

    EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'approval_required', 0, 1, 0, 0, 0, NULL, 0, 0

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    DECLARE @itemId INT = @@identity;
    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Sick Day'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    SET @itemId = @@identity;
    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Travel'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    SET @itemId = @@identity;
    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Company Meeting'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)
  END

GO

-- EXECUTE PROCEDURE FOR ALL INSTANCES
BEGIN
  DECLARE @instanceName NVARCHAR(10)
  DECLARE @process NVARCHAR(50) = 'worktime_tracking_basic_tasks'
  DECLARE Timetracking_Cursor CURSOR FOR SELECT instance FROM processes WHERE process='worktime_tracking_basic_tasks'

  OPEN Timetracking_Cursor

  FETCH NEXT FROM Timetracking_Cursor INTO @instanceName

  WHILE @@fetch_status = 0
    BEGIN

      EXEC dbo.app_ensure_column @instanceName, @process, 'approval_required', 0, 1, 0, 0, 0, NULL, 0, 0
      FETCH  NEXT FROM Timetracking_Cursor INTO @instanceName
    END
  CLOSE Timetracking_Cursor
  DEALLOCATE Timetracking_Cursor
END