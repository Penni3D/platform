SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  TRIGGER [dbo].[processes_fk_delete]
    ON [dbo].[processes]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.parent_process = d.process
	DELETE dd FROM item_columns dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM item_columns dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.data_additional = d.process
	UPDATE item_columns SET link_process = NULL WHERE instance = (SELECT instance FROM deleted) AND link_process = (SELECT process FROM deleted)
	DELETE dd FROM item_tables dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM items dd						INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_groups dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_tabs dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_containers dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_portfolios dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_tabs_subprocesses dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM conditions dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_actions dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_dashboard_charts dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM instance_menu dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.params  LIKE '"process":"' + d.process + '"'
	DELETE dd FROM processes dd					INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[item_columns_fk_delete]
	ON [dbo].[item_columns]
	INSTEAD OF DELETE
AS
	DELETE dd FROM process_container_columns dd		INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	DELETE dd FROM process_portfolio_columns dd		INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	DELETE dd FROM attachments dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	DELETE dd FROM process_tab_columns dd			INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	DELETE dd FROM state_rights dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	DELETE dd FROM state_rights dd					INNER JOIN deleted d ON dd.user_column_id = d.item_column_id
	DELETE dd FROM item_column_equations dd			INNER JOIN deleted d ON dd.parent_item_column_id = d.item_column_id
	DELETE dd FROM item_column_equations dd			INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	UPDATE item_columns SET relative_column_id = NULL WHERE instance = (SELECT instance FROM deleted) AND relative_column_id = (SELECT item_column_id FROM deleted)
	DELETE dd FROM item_columns dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id