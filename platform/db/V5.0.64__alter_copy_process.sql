IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_copy_process'))
   exec('CREATE PROCEDURE [dbo].[app_copy_process] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_copy_process]	-- Add the parameters for the stored procedure here
	@source_instance nvarchar(10),
	@source_process nvarchar(50),
	@target_instance nvarchar(10),
	@target_process nvarchar(50),
	@is_instance_copy int = 0,
	@copy_data int = 0
	AS
BEGIN

	DECLARE @sql NVARCHAR(MAX) = '';
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @colName NVARCHAR(MAX);
	DECLARE @colName2 NVARCHAR(MAX);

	DECLARE @pk_tables TABLE ( table_name nvarchar(max) )
	DECLARE @all_tables TABLE ( level int, table_name nvarchar(max) )

	--DECLARE @sql_table TABLE ( sql nvarchar(max) )

	DECLARE @GetTables CURSOR
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	WITH rec AS (
                SELECT level=0, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
				 where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = 'processes' AND CONSTRAINT_TYPE = 'PRIMARY KEY')
                UNION ALL 
                    SELECT r.level+1, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                    FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
                     INNER JOIN rec r ON r.fk_table = tc_pk.TABLE_NAME 
					  where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = r.fk_table AND CONSTRAINT_TYPE = 'PRIMARY KEY')  and fk_table <> pk_table and fk_table <> 'processes'
					  )
					  select distinct pk_tables.table2 as table_name FROM 
                 ( SELECT fk_table, count(fk_table) as cnt
				  FROM rec 
				 group by fk_table  ) cnt,
( select tc.table_name table1, tc2.table_name table2 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc   
   inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc on tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
    inner join INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 on rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME ) pk_tables
	where cnt.fk_table = pk_tables.table2 AND pk_tables.table2 not in ('processes', 'conditions', 'item_tables', 'item_data', 'items', 'item_subitems', 'instance_menu_rights', 'process_portfolio_user_groups', 'attachments', 'item_data_process_selections',  'instance_user_configurations', 'instance_integration_keys', 'instance_user_configurations', 'instance_user_configurations_saved', 'worktime_tracking_users_tasks', 'worktime_tracking_hours', 'instance_comments')

	set @sql = ' CREATE  PROCEDURE [dbo].[app_copy_process_temp]	-- Add the parameters for the stored procedure here
						@source_instance nvarchar(10),
						@source_process nvarchar(50),
						@target_instance nvarchar(10),
						@target_process nvarchar(50),
						@copy_data int = 0 
						AS BEGIN
						exec app_set_archive_user_id 0 
			
			IF @source_instance = @target_instance		
			BEGIN
				INSERT INTO processes (instance, process, process_type, new_row_action_id, variable, list_process) (SELECT @target_instance, @target_process, process_type, new_row_action_id, variable, list_process FROM processes WHERE instance = @source_instance AND process = @source_process); 		
			END
			ELSE			
			BEGIN
				INSERT INTO processes (instance, process, process_type, new_row_action_id, variable, list_process) (SELECT @target_instance, @target_process, process_type, new_row_action_id, @target_process, list_process FROM processes WHERE instance = @source_instance AND process = @source_process); 
			END
			'
	
	--insert into @sql_table (sql) values (@sql)
	--print @sql

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

	WHILE (@@FETCH_STATUS=0) BEGIN
		--set @sql = ''
	
		INSERT INTO @pk_tables (table_name) values(@TableName)

		DECLARE @GetColumns CURSOR
		SET @GetColumns = CURSOR FAST_FORWARD LOCAL FOR 
		select column_name  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		inner join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		where tc.table_name = @TableName AND CONSTRAINT_TYPE = 'PRIMARY KEY'

		-- print 'pks for ' + @TableName
		set @sql = @sql + ' DECLARE @' + @TableName + ' TABLE ( '
		--item_column_id int,  new_item_column_id int ) '

		OPEN @GetColumns
		FETCH NEXT FROM @GetColumns INTO @colName
			set @sql = @sql + '' + @colName + ' int, new_' + @colName + ' int'
		WHILE (@@FETCH_STATUS=0) BEGIN
		FETCH NEXT FROM @GetColumns INTO @colName
		END
		CLOSE @GetColumns
		set @sql = @sql + ' ) ' + CHAR(13)+CHAR(10) + ' DECLARE '

		set @sql = @sql + '@' + @colName + ' int, @new_' + @colName + ' int;'
		set @sql = @sql + CHAR(13)+CHAR(10)


		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
			set @sql = @sql + 'disable trigger ' + @TableName + '_INSTEAD_OF_INSERT on ' + @TableName + ';'  + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
			set @sql = @sql + 'disable trigger ' + @TableName + '_AFTER_UPDATE on ' + @TableName + ';'  + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		set @sql = @sql + ' MERGE ' + @TableName + ' AS t USING ( SELECT ' + @colName

		DECLARE @GetColumns2 CURSOR
		SET @GetColumns2 = CURSOR FAST_FORWARD LOCAL FOR
		select column_name  from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid', @colName)
		
		DECLARE @colNames AS NVARCHAR(MAX) = ''
		OPEN @GetColumns2
		FETCH NEXT FROM @GetColumns2 INTO @colName2
		WHILE (@@FETCH_STATUS=0) BEGIN
			-- set @sql = @sql + ', ' + @colName2  + CHAR(13)+CHAR(10)
			set @colNames = @colNames + ' ' + @colName2 + ','
		FETCH NEXT FROM @GetColumns2 INTO @colName2
		END

		set @colNames = substring(@colNames, 0, len(@colNames))

		DECLARE @colCnt AS INTEGER = 0
		DECLARE @pks1 AS NVARCHAR(MAX) = ''
		DECLARE @pks2 AS NVARCHAR(MAX) = ''
		DECLARE @pks3 AS NVARCHAR(MAX) = ''

		SELECT @colCnt = count(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName AND COLUMN_NAME = 'process'
		IF @colCnt > 0 
		BEGIN
			set @pks1 = ',@target_instance as instance, @target_process as process '
			set @pks2 = 'instance = @source_instance AND process = @source_process '
			set @pks3 = 'instance, process, '
		END
		
		IF @tableName = 'calendar_events' OR @tableName = 'calendar_data' OR @tableName = 'calendar_data_attributes' OR @tableName = 'calendar_static_holidays' 
		BEGIN 
			set @pks2 = ' calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @source_instance ) '
		END

		IF @tableName = 'calendars' OR @tableName = 'instance_integration_keys'
		BEGIN 
			set @pks1 = ',@target_instance as instance '
			set @pks2 = ' instance = @source_instance '
			set @pks3 = 'instance,  '
		END

		--IF @tableName = 'task_parents'
		--BEGIN 
		--	set @pks1 = ''
		--	set @pks2 = ' process_id IN (SELECT item_id FROM items WHERE instance = @source_instance and process = @source_process ) '
		--	set @pks3 = ''
		--END


		set @sql = @sql + ' ' + @pks1 + ', ' + @colNames + ' FROM ' + @tableName + ' WHERE 1=1 AND ' + @pks2 + ' ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( ' + CHAR(13)+CHAR(10)
		set @sql = @sql + @pks3 + @colNames + ' ) VALUES ( ' + CHAR(13)+CHAR(10)
		set @sql = @sql + @pks3 + @colNames + ' )  ' + CHAR(13)+CHAR(10)
		 set @sql = @sql + ' OUTPUT s.' + @colName + ', inserted.' + @colName + ' INTO @' + @TableName + CHAR(13)+CHAR(10) + ';'
		-- set @sql = @sql + ' OUTPUT s.' + @colName + ', @@identity INTO @' + @TableName + CHAR(13)+CHAR(10) + ';'


		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
			set @sql = @sql + ' enable trigger ' + @TableName + '_INSTEAD_OF_INSERT on ' + @TableName +' ;' + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
			set @sql = @sql + ' enable trigger ' + @TableName + '_AFTER_UPDATE on ' + @TableName +' ;' + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END
		
		FETCH NEXT FROM @GetTables INTO @TableName
	END;


	WITH rec2 AS (
                SELECT level=0, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
				 where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = 'processes' AND CONSTRAINT_TYPE = 'PRIMARY KEY')
                UNION ALL 
                    SELECT r.level+1, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                    FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
                     INNER JOIN rec2 r ON r.fk_table = tc_pk.TABLE_NAME 
					  where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = r.fk_table AND CONSTRAINT_TYPE = 'PRIMARY KEY') and fk_table <> pk_table and fk_table <> 'processes'
					  )
                  INSERT INTO @all_tables (level, table_name) SELECT count(fk_table) as cnt, fk_table
				  FROM rec2 
				 WHERE fk_table NOT IN (SELECT table_name FROM @pk_tables) AND fk_table not like 'condition%' 
				and fk_table NOT IN 
				('processes', 'item_tables','item_data','items','item_subitems', 'instance_menu_rights', 'process_portfolio_user_groups', 'attachments', 'item_data_process_selections', 				'allocation_durations', 'allocation_comments', 'task_durations', 'instance_user_configurations', 'instance_integration_keys', 'instance_user_configurations', 'instance_user_configurations_saved', 'worktime_tracking_users_tasks', 'worktime_tracking_hours', 'instance_comments') 
				group by fk_table  


	DECLARE @GetTables2 CURSOR
	SET @GetTables2 = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT table_name FROM @all_tables order by level

	OPEN @GetTables2
	FETCH NEXT FROM @GetTables2 INTO @tableName
	WHILE (@@FETCH_STATUS=0) BEGIN
		--set @sql = ''

		DECLARE @tableCnt AS INTEGER = 0
		
		select 
		@tableCnt = count(tc.table_name)
		--tc2.TABLE_NAME, ccu.COLUMN_NAME 
		from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 ON rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME
		where tc.TABLE_NAME = @tableName AND tc.CONSTRAINT_TYPE = 'FOREIGN KEY' AND tc2.table_name in (select table_name from @pk_tables)

		IF @tableCnt > 0
			BEGIN
				SET @sql = @sql + ' INSERT INTO ' + @tableName + ' (' + CHAR(13)+CHAR(10)

				DECLARE @tName NVARCHAR(MAX)
				DECLARE @cName NVARCHAR(MAX)
				DECLARE @cName2 NVARCHAR(MAX)
				DECLARE @GetCols CURSOR
				SET @GetCols = CURSOR FAST_FORWARD LOCAL FOR 
				select  tc2.TABLE_NAME, ccu.COLUMN_NAME  as column_name,  ccu2.COLUMN_NAME  as column_name2
				from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
				INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 ON rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu2 ON rc.UNIQUE_CONSTRAINT_NAME = ccu2.CONSTRAINT_NAME
				where tc.TABLE_NAME = @tableName AND tc.CONSTRAINT_TYPE = 'FOREIGN KEY' AND tc2.table_name in (select table_name from @pk_tables)

				DECLARE @fks TABLE ( colname nvarchar(MAX) )
				DECLARE @fks_cols AS NVARCHAR(MAX) = ''
				DECLARE @fks_ins AS NVARCHAR(MAX) = ''
				DECLARE @fks_where AS NVARCHAR(MAX) = '1=0 '

				OPEN @GetCols
				FETCH NEXT FROM @GetCols INTO @tName, @cName, @cName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					INSERT INTO @fks (colname) values (@cName)
					SET @fks_cols = @fks_cols + @cName + ','
					SET @fks_ins = @fks_ins + ' (SELECT new_' + @cName2 + ' FROM @' + @tName + ' WHERE ' + @cName2 + ' = ' + @TableName + '.' + @cName + '),'
					SET @fks_where = @fks_where +' OR '  + @cName + ' IN (SELECT ' + @cName2 + ' FROM @' + @tName + ') '  + CHAR(13)+CHAR(10)
				FETCH NEXT FROM @GetCols INTO @tName, @cName, @cName2
				END
				CLOSE @GetCols


				DECLARE @GetColumns3 CURSOR
				SET @GetColumns3 = CURSOR FAST_FORWARD LOCAL FOR
				select  column_name from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid') and column_name not in (select colname from @fks)
				and column_name not in ( select column_name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME=ccu.CONSTRAINT_NAME where tc.TABLE_NAME = @TableName and CONSTRAINT_TYPE = 'PRIMARY KEY' )
				and column_name not in ('process_tab_container_id', 'process_group_tab_id', 'process_portfolio_column_id')
		
				DECLARE @colNames2 AS NVARCHAR(MAX) = ''
				OPEN @GetColumns3
				FETCH NEXT FROM @GetColumns3 INTO @colName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					--set @sql = @sql + ' ' + @colName2  + ', ' + CHAR(13)+CHAR(10)
					set @colNames2 = @colNames2 + ' ' + @colName2 + ','
				FETCH NEXT FROM @GetColumns3 INTO @colName2
				END

				if @TableName = 'process_group_features'
				BEGIN
					set @colNames2 = @colNames2 + ' feature,' 
				END
				
				set @colNames2 = substring(@colNames2, 0, len(@colNames2)  )

				if LEN(@colNames2) = 0 
				BEGIN
					set @fks_cols = substring(@fks_cols, 0, len(@fks_cols)  )
					set @fks_ins = substring(@fks_ins, 0, len(@fks_ins)  )
				END

				set @sql = @sql + @fks_cols + @colNames2 + ') SELECT ' + @fks_ins + ' ' +@colNames2 + ' FROM ' + @TableName + ' WHERE ' + @fks_where 

				--update @sql_table set sql = concat(sql, @sql)
				--print @sql
			END
		ELSE
			BEGIN
				
				DECLARE @GetColumns4 CURSOR
				SET @GetColumns4 = CURSOR FAST_FORWARD LOCAL FOR
				select column_name from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid') and column_name not in (select colname from @fks)
				and column_name not in ( select column_name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME=ccu.CONSTRAINT_NAME where tc.TABLE_NAME = @TableName and CONSTRAINT_TYPE = 'PRIMARY KEY' )
		
				DECLARE @colNames3 AS NVARCHAR(MAX) = ''
				OPEN @GetColumns4
				FETCH NEXT FROM @GetColumns4 INTO @colName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					--set @sql = @sql + ' ' + @colName2  + ', ' + CHAR(13)+CHAR(10)
					set @colNames3 = @colNames3 + ' ' + @colName2 + ', '
				FETCH NEXT FROM @GetColumns4 INTO @colName2
				END			

				--if @TableName = 'process_translations'
				--BEGIN
				--	set @colNames3 = @colNames3 + ' code, variable,' 
				--END
				if @TableName = 'process_subprocesses'
				BEGIN
					set @colNames3 = @colNames3 + ' parent_process,' 
				END

				if @is_instance_copy = 1 AND @TableName <> 'process_subprocesses'
				BEGIN 
				if @TableName <> 'task_parents' and @TableName <> 'instance_user_configurations'
				BEGIN
					SET @sql = @sql + ' INSERT INTO ' + @tableName + ' (' + CHAR(13)+CHAR(10)
					set @sql = @sql + @colNames3 + ' instance, process) ( SELECT ' + @colNames3 + ' @target_instance, @target_process FROM ' + @tableName + ' WHERE instance = @source_instance AND process = @source_process )' + CHAR(13)+CHAR(10)
				END
				END

				--update @sql_table set sql = concat(sql, @sql)
				--print @sql
			END

		--INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no, read_only)
		--( SELECT 
		-- (SELECT new_process_container_id FROM @containers WHERE process_container_id = process_container_columns.process_container_id ),
		-- (SELECT new_item_column_id FROM @item_columns WHERE item_column_id = process_container_columns.item_column_id ),
		-- placeholder, validation, required, order_no, read_only 
		-- FROM process_container_columns 
		-- WHERE process_container_id IN (SELECT process_container_id FROM @containers) 
		-- OR  item_column_id IN (SELECT item_column_id FROM @item_columns)
		-- )

	FETCH NEXT FROM @GetTables2 INTO @tableName
	END
	
	--update @sql_table set sql = concat(sql, ' END ')

	--select top 1 @sql =sql from @sql_table
	--PRINT LEN(@sql)

	--set @sql = @sql + '
	--	 INSERT INTO process_group_features (process_group_id, feature, state) 
	--	 ( SELECT  distinct (SELECT new_process_group_id FROM @process_groups WHERE process_group_id = process_group_features.process_group_id),
	--	  feature, state
	--	FROM process_group_features
	--	WHERE process_group_id IN (SELECT process_group_id FROM @process_groups) ) '

	set @sql = @sql + '
		DECLARE @conditions TABLE ( condition_id int, new_condition_id int ) 
		 DECLARE @condition_id int, @new_condition_id int;
		
		disable trigger conditions_AFTER_UPDATE on conditions;
		disable trigger conditions_INSTEAD_OF_INSERT on conditions;

		 MERGE conditions AS t USING ( SELECT condition_id ,@target_instance as instance, @target_process as process ,  variable, condition_json FROM conditions WHERE instance = @source_instance AND process = @source_process  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
		instance, process,  variable, condition_json ) VALUES ( 
		instance, process,  variable, condition_json )  
		 OUTPUT s.condition_id, inserted.condition_id INTO @conditions; 
		 
		 enable trigger conditions_AFTER_UPDATE on conditions ;	
		 enable trigger conditions_INSTEAD_OF_INSERT on conditions ;	
		 '
		 
		 
	set @sql = @sql + '
		 INSERT INTO condition_containers (condition_id, process_container_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_containers.condition_id),
		  (SELECT new_process_container_id FROM @process_containers WHERE process_container_id = condition_containers.process_container_id)
		 FROM condition_containers
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_container_id IN (SELECT process_container_id FROM @process_containers) )  '
 		 
	set @sql = @sql + '
		 INSERT INTO condition_features (condition_id, feature) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_features.condition_id),
		  feature
		FROM condition_features
		WHERE condition_id IN (SELECT condition_id FROM @conditions) ) '

		 	set @sql = @sql + '
			INSERT INTO condition_groups (condition_id, process_group_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_groups.condition_id),
		  (SELECT new_process_group_id FROM @process_groups WHERE process_group_id = condition_groups.process_group_id)
		 FROM condition_groups
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_group_id IN (SELECT process_group_id FROM @process_groups) ) '

		 	set @sql = @sql + '
		 INSERT INTO condition_portfolios (condition_id, process_portfolio_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_portfolios.condition_id),
		  (SELECT new_process_portfolio_id FROM @process_portfolios WHERE process_portfolio_id = condition_portfolios.process_portfolio_id)
		 FROM condition_portfolios
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_portfolio_id IN (SELECT process_portfolio_id FROM @process_portfolios) ) '

		set @sql = @sql + '
		 INSERT INTO condition_states (condition_id, state) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_states.condition_id),
		  state
		 FROM condition_states
		 WHERE condition_id IN (SELECT condition_id FROM @conditions)  ) '

		set @sql = @sql + '
		 INSERT INTO condition_tabs (condition_id, process_tab_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_tabs.condition_id),
		  (SELECT new_process_tab_id FROM @process_tabs WHERE process_tab_id = condition_tabs.process_tab_id)
		 FROM condition_tabs
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_tab_id IN (SELECT process_tab_id FROM @process_tabs) ) '


		set @sql = @sql + '
		IF @source_instance = @target_instance
			BEGIN
			 INSERT INTO condition_user_groups (condition_id, item_id) 
			 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_user_groups.condition_id),
			  item_id
			 FROM condition_user_groups
			 WHERE condition_id IN (SELECT condition_id FROM @conditions)  ) 
		 END
		 '

		 set @sql = @sql + '
					UPDATE process_translations set variable = @target_process WHERE instance = @target_instance AND process = @target_process and variable = @source_process ;
		'

		 set @sql = @sql + '

 				DECLARE @ItemCols CURSOR
				SET @ItemCols = CURSOR FAST_FORWARD LOCAL FOR
				SELECT item_column_id, new_item_column_id FROM @item_columns

				OPEN @ItemCols
				FETCH NEXT FROM @ItemCols INTO @item_column_id, @new_item_column_id
				WHILE (@@FETCH_STATUS=0) BEGIN
					UPDATE conditions set condition_json = replace(condition_json, ''"ItemColumnId":'' + CAST(@item_column_id AS NVARCHAR) + '','', ''"ItemColumnId":'' + CAST(@new_item_column_id AS NVARCHAR) + '','') WHERE instance = @target_instance AND process = @target_process
					UPDATE conditions set condition_json = replace(condition_json, ''"ItemColumnId":'' + CAST(@item_column_id AS NVARCHAR) + ''}'', ''"ItemColumnId":'' + CAST(@new_item_column_id AS NVARCHAR) + ''}'') WHERE instance = @target_instance AND process = @target_process
					-- UPDATE process_translations set variable = substring(replace(variable, ''ic_'' + CAST(@item_column_id AS NVARCHAR) + ''_'', ''ic_'' + CAST(@new_item_column_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
					
					UPDATE item_columns 
						SET 
						-- variable = substring(replace(variable, ''ic_'' + CAST(@item_column_id AS NVARCHAR) + ''_'', ''ic_'' + CAST(@new_item_column_id AS NVARCHAR) + ''_''), 1, 50),
						relative_column_id = (select new_item_column_id FROM @item_columns WHERE item_column_id = relative_column_id)
					WHERE instance = @target_instance AND process = @target_process

					--UPDATE item_columns 
					--SET 
					--data_additional2 = (select new_item_column_id FROM @item_columns WHERE item_column_id = CAST(data_additional2 as int))
					--WHERE instance = @target_instance AND process = @target_process and data_type = 11
					
					UPDATE process_portfolio_columns 
					SET 
					archive_item_column_id = (select new_item_column_id FROM @item_columns WHERE item_column_id = archive_item_column_id)
					WHERE process_portfolio_id IN (select process_portfolio_id FROM process_portfolios WHERE instance = @target_instance AND process = @target_process)

				FETCH NEXT FROM  @ItemCols INTO @item_column_id, @new_item_column_id
				END		 '


		 set @sql = @sql + '
 				DECLARE @Conds CURSOR
				SET @Conds = CURSOR FAST_FORWARD LOCAL FOR
				SELECT condition_id, new_condition_id FROM @conditions

				OPEN @Conds
				FETCH NEXT FROM @Conds INTO @condition_id, @new_condition_id
				WHILE (@@FETCH_STATUS=0) BEGIN
					UPDATE conditions set condition_json = replace(condition_json, ''"ConditionId":'' + CAST(@condition_id AS NVARCHAR) + '','', ''"ConditionId":'' + CAST(@new_condition_id AS NVARCHAR) + '','') WHERE instance = @target_instance AND process = @target_process
					UPDATE conditions set condition_json = replace(condition_json, ''"ConditionId":'' + CAST(@condition_id AS NVARCHAR) + ''}'', ''"ConditionId":'' + CAST(@new_condition_id AS NVARCHAR) + ''}'') WHERE instance = @target_instance AND process = @target_process
					-- UPDATE process_translations set variable = substring(replace(variable, ''condition_'' + CAST(@condition_id AS NVARCHAR) + ''_'', ''condition_'' + CAST(@new_condition_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
					-- UPDATE conditions set variable = substring(replace(variable, ''condition_'' + CAST(@condition_id AS NVARCHAR) + ''_'', ''condition_'' + CAST(@new_condition_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
				FETCH NEXT FROM  @Conds INTO @condition_id, @new_condition_id
				END		 '

		 --set @sql = @sql + '
 		--		DECLARE @groups CURSOR
			--	SET @groups = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_group_id, new_process_group_id FROM @process_groups

			--	OPEN @groups
			--	FETCH NEXT FROM @groups INTO @process_group_id, @new_process_group_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''group_'' + CAST(@process_group_id AS NVARCHAR) + ''_'', ''group_:'' + CAST(@new_process_group_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_groups set variable = substring(replace(variable, ''group_'' + CAST(@process_group_id AS NVARCHAR) + ''_'', ''group_:'' + CAST(@new_process_group_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @groups INTO @process_group_id, @new_process_group_id
			--	END		 '

		 --set @sql = @sql + '
 		--		DECLARE @tabs CURSOR
			--	SET @tabs = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_tab_id, new_process_tab_id FROM @process_tabs

			--	OPEN @tabs
			--	FETCH NEXT FROM @tabs INTO @process_tab_id, @new_process_tab_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''tab_'' + CAST(@process_tab_id AS NVARCHAR) + ''_'', ''tab_'' + CAST(@new_process_tab_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_tabs set variable = substring(replace(variable, ''tab_'' + CAST(@process_tab_id AS NVARCHAR) + ''_'', ''tab_'' + CAST(@new_process_tab_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @tabs INTO @process_tab_id, @new_process_tab_id
			--	END		 '

		 --set @sql = @sql + '
 		--		DECLARE @pp CURSOR
			--	SET @pp = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_portfolio_id, new_process_portfolio_id FROM @process_portfolios

			--	OPEN @pp
			--	FETCH NEXT FROM @pp INTO @process_portfolio_id, @new_process_portfolio_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''port_'' + CAST(@process_portfolio_id AS NVARCHAR) + ''_'', ''port_'' + CAST(@new_process_portfolio_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_portfolios set variable = substring(replace(variable, ''port_'' + CAST(@process_portfolio_id AS NVARCHAR) + ''_'', ''port_'' + CAST(@new_process_portfolio_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @pp INTO @process_portfolio_id, @new_process_portfolio_id
			--	END	'

		 --set @sql = @sql + '
 		--		DECLARE @cc CURSOR
			--	SET @cc = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_container_id, new_process_container_id FROM @process_containers

			--	OPEN @cc
			--	FETCH NEXT FROM @cc INTO @process_container_id, @new_process_container_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''cont_'' + CAST(@process_container_id AS NVARCHAR) + ''_'', ''cont_'' + CAST(@new_process_container_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_containers set variable = substring(replace(variable, ''cont_'' + CAST(@process_container_id AS NVARCHAR) + ''_'', ''cont_'' + CAST(@new_process_container_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @cc INTO @process_container_id, @new_process_container_id
			--	END	
		 --'

		 set @sql = @sql + '
			if @copy_data = 1 
			begin

				 DECLARE @items TABLE ( item_id int, new_item_id int ) 
				 DECLARE @item_id int, @new_item_id int;
				
				 disable trigger items_BeforeInsertRow on items;
				-- disable trigger items_AFTER_UPDATE on items;

				 MERGE items AS t USING ( SELECT item_id,  @target_instance as instance, @target_process as process, deleted, order_no FROM items WHERE instance = @source_instance AND process = @source_process) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
				 instance, process, deleted, order_no ) VALUES ( 
				 instance, process, deleted, order_no )  
				 OUTPUT s.item_id, inserted.item_id INTO @items;'
				 
				 set @sql = @sql + '
 				DECLARE @tableCols CURSOR
				SET @tableCols = CURSOR FAST_FORWARD LOCAL FOR
				SELECT name FROM item_columns WHERE instance = @source_instance AND process = @source_process and data_type in (0,1,2,3,4, 7) AND len(name) > 0

				DECLARE @tableColNames NVARCHAR(MAX) = '+char(39)+''+char(39)+'
				DECLARE @tableColName NVARCHAR(MAX)

				OPEN @tableCols
				FETCH NEXT FROM @tableCols INTO @tableColName
				WHILE (@@FETCH_STATUS=0) BEGIN
					set @tableColNames = @tableColNames + '+char(39)+'u.'+char(39)+' + @tableColName + '+char(39)+' = s.'+char(39)+' + @tableColName + '+char(39)+','+char(39)+'
				FETCH NEXT FROM  @tableCols INTO @tableColName
				END					 
				
				set @tableColNames = substring(@tableColNames, 0, len(@tableColNames)  ) '


				 set @sql = @sql + '
				 declare @sql2 nvarchar(max) = '+char(39)+''+char(39)+'
				 set @sql2 = @sql2 + '+char(39)+'
				update u
				set '+char(39)+' + @tableColNames + '+char(39)+'
				from _'+char(39)+' + @target_instance + '+char(39)+'_'+char(39)+' + @target_process + '+char(39)+' u 
				inner join #NewItems2 i on u.item_id = i.new_item_id
				inner join _'+char(39)+' + @source_instance + '+char(39)+'_'+char(39)+' + @source_process + '+char(39)+' s on i.item_id = s.item_id '+char(39)+' ;

				IF OBJECT_ID('+char(39)+'tempdb..#NewItems2'+char(39)+') IS NOT NULL DROP TABLE #NewItems2 ;
				CREATE table #NewItems2 (item_id int, new_item_id int ) ;
				
				INSERT INTO #NewItems2 (item_id, new_item_id) SELECT item_id, new_item_id FROM @items ;

				IF LEN(@tableColNames) > 0 EXEC (@sql2) ;
				'

				 set @sql = @sql + '
				 enable trigger items_BeforeInsertRow on items ;
				-- enable trigger items_AFTER_UPDATE on items ;

				 if @source_instance = @target_instance
				 begin 
					INSERT INTO item_subitems (parent_item_id, item_id, favourite) (SELECT (SELECT new_item_id FROM @items WHERE item_id = i.parent_item_id), item_id, favourite FROM item_subitems i WHERE parent_item_id IN (SELECT item_id FROM @items) )
					INSERT INTO item_subitems (parent_item_id, item_id, favourite) (SELECT parent_item_id, (SELECT new_item_id FROM @items WHERE item_id = i.item_id), favourite FROM item_subitems i WHERE item_id IN (SELECT item_id FROM @items) )

					INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) 
					(SELECT 
						(SELECT new_item_id FROM @items WHERE item_id = i.item_id), 
						(SELECT new_item_column_id FROM @item_columns WHERE item_column_id = i.item_column_id),
						selected_item_id 
						FROM item_data_process_selections i WHERE item_id IN (SELECT item_id FROM @items)
					)

					INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) 
					(SELECT 
						item_id,
						(SELECT new_item_column_id FROM @item_columns WHERE item_column_id = i.item_column_id),
						(SELECT new_item_id FROM @items WHERE item_id = i.selected_item_id )
						FROM item_data_process_selections i WHERE selected_item_id IN (SELECT item_id FROM @items)
					)
						
				 end



			end
		 '
		 
		 if @is_instance_copy = 1
		 BEGIN 
			set @sql = @sql + ' INSERT INTO #NewItemColumns (item_column_id, new_item_column_id) (SELECT item_column_id, new_item_column_id FROM @item_columns)'
			set @sql = @sql + ' INSERT INTO #NewItems (item_id, new_item_id) (SELECT item_id, new_item_id FROM @items)'
			set @sql = @sql + ' INSERT INTO #NewPortfolios (process_portfolio_id, new_process_portfolio_id) (SELECT process_portfolio_id, new_process_portfolio_id FROM @process_portfolios)'
			set @sql = @sql + ' INSERT INTO #NewTabs (process_tab_id, new_process_tab_id) (SELECT process_tab_id, new_process_tab_id FROM @process_tabs)'
			set @sql = @sql + ' INSERT INTO #NewConditions (condition_id, new_condition_id) (SELECT condition_id, new_condition_id FROM @conditions)'
		 END

		 set @sql = @sql + '
		 END
		 '

	--set @sql = @sql + ' END '
	--PRINT ' END '

	print LEN(@sql)

	
	--DECLARE @CurrentEnd BIGINT; /* track the length of the next substring */
	--DECLARE @offset tinyint; /*tracks the amount of offset needed */
	--set @sql = replace(  replace(@sql, char(13) + char(10), char(10))   , char(13), char(10))
	
	--WHILE LEN(@sql) > 1
	--BEGIN
	--	IF CHARINDEX(CHAR(10), @sql) between 1 AND 4000
	--	BEGIN
	--		   SET @CurrentEnd =  CHARINDEX(char(10), @sql) -1
	--		   set @offset = 2
	--	END
	--	ELSE
	--	BEGIN
	--		   SET @CurrentEnd = 4000
	--			set @offset = 1
	--	END   
	--	PRINT SUBSTRING(@sql, 1, @CurrentEnd) 
	--	set @sql = SUBSTRING(@sql, @CurrentEnd+@offset, LEN(@sql))   
	--END

	exec (@sql)

	set @sql = ' exec app_copy_process_temp ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @source_process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @target_process +char(39)+',' + cast(@copy_data as nvarchar)
	exec (@sql)

	if @is_instance_copy <> 1 
	BEGIN 
		SET @sql = 'drop procedure app_copy_process_temp'
		exec (@sql)
	END

END
