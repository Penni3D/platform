IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_remove_archive'))
   exec('CREATE PROCEDURE [dbo].[app_remove_archive] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_remove_archive]
	@TableName NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @GetDFS CURSOR
	DECLARE @ConstraintName NVARCHAR(MAX);
	DECLARE @colsCount INT;

	SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = 'archive_'+@TableName
	IF @colsCount > 0
		BEGIN
			print 'remove archive for ' + @TableName
			SET @sql = 'DROP table archive_'+@TableName
			print @sql
			EXEC (@sql)
		END

	SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_AFTER_UPDATE'
	IF @colsCount > 0
		BEGIN
			SET @sql = 'DROP TRIGGER '+@TableName+'_AFTER_UPDATE'
			print @sql
			EXEC (@sql)
		END

	SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_DELETE'
	IF @colsCount > 0
		BEGIN
			SET @sql = 'DROP TRIGGER '+@TableName+'_DELETE'
			print @sql
			EXEC (@sql)
		END

	SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_INSTEAD_OF_INSERT'
	IF @colsCount > 0
		BEGIN
			SET @sql = 'DROP TRIGGER '+@TableName+'_INSTEAD_OF_INSERT'
			print @sql
			EXEC (@sql)
		END

	SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = 'get_'+@TableName
	IF @colsCount > 0
		BEGIN
			SET @sql = 'DROP FUNCTION get_'+@TableName
			print @sql
			EXEC (@sql)
		END

	SELECT
		@ConstraintName = default_constraints.name
	FROM
		sys.all_columns
		INNER JOIN
		sys.tables
			ON all_columns.object_id = tables.object_id
		INNER JOIN
		sys.schemas
			ON tables.schema_id = schemas.schema_id
		INNER JOIN
		sys.default_constraints
			ON all_columns.default_object_id = default_constraints.object_id
	WHERE
		schemas.name = 'dbo'
		AND tables.name = @TableName
		AND all_columns.name = 'archive_userid'

	SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @ConstraintName
	IF @colsCount > 0
		BEGIN
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP CONSTRAINT ' + @ConstraintName
			print @sql
			EXEC (@sql)
		END

	SELECT @colsCount=COUNT(column_name) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TableName AND column_name = 'archive_userid'
	IF @colsCount > 0
		BEGIN
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP COLUMN archive_userid'
			print @sql
			EXEC (@sql)
		END



	SELECT
		@ConstraintName = default_constraints.name
	FROM
		sys.all_columns
		INNER JOIN
		sys.tables
			ON all_columns.object_id = tables.object_id
		INNER JOIN
		sys.schemas
			ON tables.schema_id = schemas.schema_id
		INNER JOIN
		sys.default_constraints
			ON all_columns.default_object_id = default_constraints.object_id
	WHERE
		schemas.name = 'dbo'
		AND tables.name = @TableName
		AND all_columns.name = 'archive_start'

	SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @ConstraintName
	IF @colsCount > 0
		BEGIN
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP CONSTRAINT ' + @ConstraintName
			print @sql
			EXEC (@sql)
		END

	SELECT @colsCount=COUNT(column_name) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TableName AND column_name = 'archive_start'
	IF @colsCount > 0
		BEGIN
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP COLUMN archive_start'
			print @sql
			EXEC (@sql)
		END

END

GO

ALTER PROCEDURE [dbo].[app_ensure_archive_all]
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @GetTables CURSOR

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT table_name FROM INFORMATION_SCHEMA.TABLES  WHERE table_type = 'BASE TABLE' and substring(table_name, 0, 9) <> 'archive_'  and table_name <> 'sysdiagrams'
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

	WHILE (@@FETCH_STATUS=0) BEGIN
		IF @TableName not in ('attachments_data', 'instance_configurations', 'items', 'instance_user_configurations', 'instance_user_configurations_saved', 'links') BEGIN
			print 'ensure archive for ' + @TableName
			SET @sql = 'EXEC app_ensure_archive '''+@TableName+''''
			print @sql
			EXEC (@sql)
		END
			FETCH NEXT FROM @GetTables INTO @TableName
	END
	CLOSE @GetTables

END
GO
exec app_remove_archive 'instance_configurations'
GO
exec app_remove_archive 'instance_user_configurations'
GO
exec app_remove_archive 'instance_user_configurations_saved'
GO
exec app_remove_archive 'links'
