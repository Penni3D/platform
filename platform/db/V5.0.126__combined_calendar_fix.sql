﻿ALTER FUNCTION [dbo].[GetCombinedCalendar](@base_calendar_id INT, @calendar_id INT, @start DATE, @end DATE)
RETURNS TABLE 
AS
RETURN 
(

SELECT 
calendar_id as calendar_id,
datadate AS event_date,
dayoff = ISNULL(dayoff1,0),
holiday = ISNULL(holiday1,0),
notwork = ISNULL(notwork2,notwork1),
calendar_event_id = ISNULL(calendar_event_id2,calendar_event_id1) ,
calendar_data_id = ISNULL(calendar_data_id2,calendar_data_id1) 
FROM 
(
	SELECT 
	@calendar_id as calendar_id,
	event_date AS datadate,
	MAX(dayoff1) AS dayoff1,
	MAX(holiday1) AS holiday1,
	MAX(notwork1) AS notwork1,
	MAX(notwork2) AS notwork2,
	MAX(calendar_event_id1) AS calendar_event_id1,
	MAX(calendar_event_id2) AS calendar_event_id2, 
	MAX(calendar_data_id1) AS calendar_data_id1,
	MAX(calendar_data_id2) AS calendar_data_id2 
	FROM (
		SELECT 
		event_date,
		cd1.calendar_event_id AS calendar_event_id1,
		NULL AS calendar_event_id2,
		cd1.calendar_data_id AS calendar_data_id1,
		NULL AS calendar_data_id2,
		cd1.dayoff AS dayoff1,
		cd1.holiday AS holiday1,
		cd1.notwork AS notwork1,
		NULL AS notwork2 
		FROM calendar_data cd1 
		WHERE 
			cd1.calendar_id = @base_calendar_id AND 
			--YEAR(cd1.event_date) = @year
			cd1.event_date BETWEEN @start AND @end	
		UNION 
		
		SELECT 
		event_date,
		NULL AS calendar_event_id1,
		cd2.calendar_event_id AS calendar_event_id2,
		NULL AS calendar_data_id1,
		cd2.calendar_data_id AS calendar_data_id2,
		NULL AS dayoff1,NULL AS holiday1,
		NULL AS notwork1,
		cd2.notwork AS notwork2 
		FROM calendar_data cd2 
		WHERE cd2.calendar_id = @calendar_id AND 
		--YEAR(cd2.event_date) = @year
		cd2.event_date BETWEEN @start AND @end
		) inner_query 
	GROUP BY event_date
) outer_query 
)