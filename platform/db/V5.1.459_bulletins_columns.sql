DECLARE @PortfolioId INT;
DECLARE @StartId INT = (SELECT item_column_id FROM item_columns where process = 'notification' AND name = 'start')
DECLARE @UserItemId INT = (SELECT item_column_id FROM item_columns where process = 'notification' AND name = 'user_item_id')
DECLARE @Ug INT = (SELECT item_column_id FROM item_columns where process = 'notification' AND name = 'user_groups')

DECLARE cursor_process CURSOR
FOR SELECT process_portfolio_id FROM process_portfolios WHERE process = 'notification'

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @PortfolioId
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
	  IF(SELECT COUNT (*) FROM process_portfolio_columns WHERE process_portfolio_id = @PortfolioId AND item_column_id = @StartId) = 0
	  BEGIN

		EXEC app_set_archive_user_id 1
	  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_filter,is_default)
	  VALUES (@PortfolioId, @StartId, 'xxx', 0, 0)
	  END

	  IF(SELECT COUNT (*) FROM process_portfolio_columns WHERE process_portfolio_id = @PortfolioId AND item_column_id = @UserItemId) = 0
	  BEGIN
	  EXEC app_set_archive_user_id 1
	  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_filter,is_default)
	  VALUES (@PortfolioId, @UserItemId, 'xxz', 0, 0)
	  END

	  IF(SELECT COUNT (*) FROM process_portfolio_columns WHERE process_portfolio_id = @PortfolioId AND item_column_id = @Ug) = 0
	  BEGIN
	  EXEC app_set_archive_user_id 1
	  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_filter,is_default)
	  VALUES (@PortfolioId, @Ug, 'xxy', 0, 0)
	  END
		
        FETCH NEXT FROM cursor_process INTO 
            @PortfolioId
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
