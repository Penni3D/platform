EXEC app_set_archive_user_id 1
DECLARE @instance NVARCHAR(10)
DECLARE @item_column_id INT
DECLARE @condition_id INT
DECLARE @condition_id2 INT
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@FETCH_STATUS = 0 BEGIN
	SELECT @item_column_id = item_column_id FROM item_columns WHERE instance = @instance AND process = 'notification' AND [name] = 'user_item_id'

	SELECT @condition_id = condition_id FROM conditions
	WHERE instance = @instance AND process = 'notification' AND variable = 'condition_notification_NOTIFICATIONS'
	AND condition_json = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":53,"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1,"Disabled":false}]'

	SELECT @condition_id2 = condition_id FROM conditions
	WHERE instance = @instance AND process = 'notification' AND variable = 'condition_notification_acces_container'
	AND (condition_json IS NULL OR condition_json  = 'null')

	IF @item_column_id > 0 AND @condition_id > 0 AND @condition_id2 > 0 BEGIN
		UPDATE item_columns SET in_conditions = 1 WHERE item_column_id = @item_column_id
	
		UPDATE conditions
		SET condition_json = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":53,"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":3,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@item_column_id AS NVARCHAR) + ',"UserColumnId":null,"Value":"","UserColumnTypeId":2},"OperatorId":1}]},"OperatorId":1,"Disabled":false}]'
		WHERE condition_id = @condition_id

		UPDATE conditions
		SET condition_json = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":6,"ItemColumnId":53,"Value":"1"},"OperatorId":null}]},"OperatorId":1,"Disabled":false}]'
		WHERE condition_id = @condition_id2
	END

	FETCH NEXT FROM instance_cursor INTO @instance
END
CLOSE instance_cursor
DEALLOCATE instance_cursor