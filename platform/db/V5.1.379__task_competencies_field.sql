﻿DECLARE @instance NVARCHAR(10)
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
    DECLARE @link_process NVARCHAR(50) = 'task_competency_effort'
    DECLARE @ContainerId INT
    DECLARE @LanguageVariableAC NVARCHAR(255) = 'cont_' + @link_process + '_right_container_1';
    DECLARE @portfolio_id NVARCHAR(MAX) = (SELECT '{"showAll":false,"portfolioId":' + CAST(process_portfolio_id AS NVARCHAR(255)) +',"canOpen":false,"table":true,"advancedFilter":false,"luceneScore":0,"luceneMaxCount":10}' FROM process_portfolios WHERE process = 'competency' AND process_portfolio_type = 10)
    
    --Create Process
    EXEC app_ensure_process @instance, @link_process, @link_process, 3

    --Create Access Container
    INSERT INTO process_containers (instance, process) VALUES(@INSTANCE, @link_process)
    SET @ContainerId = @@IDENTITY
    UPDATE process_containers SET variable = @LanguageVariableAC, right_container = 1 WHERE instance = @instance AND process = @link_process AND process_container_id = @ContainerId

    --Create Columns
    EXEC app_ensure_column @instance, @link_process, 'effort'     , @ContainerId, 2, 0, 1, 0, null, 0, 0
    EXEC app_ensure_column @instance, @link_process, 'cost_center', @ContainerId, 6, 0, 1, 0, null, 0, 'list_cost_center', 'list_cost_center'
    DECLARE @item_column_id INT = (SELECT TOP 1 item_column_id FROM item_columns WHERE process = @link_process ORDER BY item_column_id DESC)
    UPDATE item_columns SET ic_validation = '{"Precision":null,"Max":1,"Rows":null,"Spellcheck":null,"Suffix":null,"ParentId":null,"DateRange":null,"CalendarConnection":null,"AfterMonths":null,"BeforeMonths":null,"Unselectable":null,"Required":false,"ReadOnly":false,"Label":"{\"en-GB\":\"\"}"}'
    WHERE item_column_id = @item_column_id

    --Create Task Column
    EXEC app_ensure_column @instance, 'task', 'competencies', 0, 5, 0, 1, 0, null, 0, 'competency', @portfolio_id
    SET @item_column_id = (SELECT TOP 1 item_column_id FROM item_columns WHERE process = 'task' ORDER BY item_column_id DESC)
    UPDATE item_columns SET input_method = 0, input_name = 0, link_process = @link_process, ic_validation = '{"Precision":null,"Max":null,"Rows":null,"Spellcheck":null,"Suffix":null,"ParentId":null,"DateRange":null,"CalendarConnection":null,"AfterMonths":null,"BeforeMonths":null,"Unselectable":null,"Required":null,"ReadOnly":null,"Label":null}'
    WHERE item_column_id = @item_column_id
    

    FETCH  NEXT FROM instance_cursor INTO @instance
END
CLOSE instance_cursor
DEALLOCATE instance_cursor