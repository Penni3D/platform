DECLARE @instance NVARCHAR(10)
DECLARE @process NVARCHAR(50) = 'instance_tags'
DECLARE @portfolioId INT
DECLARE @conditionId INT
DECLARE @groupId INT
DECLARE @tabId INT
DECLARE @containerId INT
DECLARE @groupProcess NVARCHAR(50) = 'instance_tags_groups'


DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances

OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
	EXEC app_set_archive_user_id 0
    EXEC app_ensure_portfolio @instance, @process, 'instance_tags_all_tags', @portfolio_id = @portfolioId OUTPUT

 	EXEC app_ensure_condition @instance, @process, 0, @portfolioId, 'condition_all_tags', 'null', @condition_id = @conditionId OUTPUT 
	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')

	INSERT INTO process_portfolio_columns(process_portfolio_id, item_column_id) SELECT @portfolioId, item_column_id FROM item_columns WHERE instance = @instance AND process = @process
	DECLARE @params NVARCHAR(MAX) = '{"tagsPortfolioId":' + CAST(@portfolioId AS NVARCHAR(MAX)) + '}'

	IF (SELECT COUNT(*) FROM instance_menu WHERE default_state = 'tags' AND instance = @instance AND link_type = 3) > 0 
		UPDATE instance_menu 
		SET params = @params 
		WHERE default_state = 'tags' AND instance = @instance AND link_type = 3
	ELSE
		INSERT INTO instance_menu(instance, variable, order_no, default_state, params, link_type)
		VALUES(@instance, 'instancemenu_tags', 5, 'tags', @params, 3)


	EXEC app_ensure_group @instance, @groupProcess, 'GROUP', @group_id = @groupId OUTPUT
	EXEC app_ensure_tab @instance, @groupProcess, 'GROUP_TAB', @groupId, @tab_id = @tabId OUTPUT
	EXEC app_ensure_container @instance, @groupProcess, 'GROUP_CONTAINER', @tabId, 1, @container_id = @containerId OUTPUT

	EXEC app_ensure_condition @instance, @groupProcess, @groupId, 0, 'condition_read_meta', 'null', @condition_id = @conditionId OUTPUT 
	INSERT INTO condition_states(condition_id, state) VALUES (@conditionId, 'read')
	INSERT INTO condition_states(condition_id, state) VALUES (@conditionId, 'write')

	INSERT INTO process_container_columns(process_container_id, item_column_id) SELECT @containerId, item_column_id FROM item_columns WHERE instance = @instance AND process = @groupProcess 


    FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor