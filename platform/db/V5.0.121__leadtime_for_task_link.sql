﻿EXEC app_set_archive_user_id 0
SET NOCOUNT ON;

DECLARE @sql NVARCHAR(MAX);
DECLARE @TableName NVARCHAR(MAX);
DECLARE @GetTables CURSOR

--Get Tables
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
SELECT instance FROM instances
	
OPEN @GetTables
FETCH NEXT FROM @GetTables INTO @TableName
WHILE (@@FETCH_STATUS=0) BEGIN
	--SELECT @TableName;
	SET @sql = '
		IF NOT EXISTS(
			SELECT * FROM sys.columns WHERE name = ''leadtime'' AND object_id = OBJECT_ID(N''[dbo].[_'+ @TableName + '_task_link_type]'')
			)
			ALTER TABLE _'+ @TableName + '_task_link_type
				ADD leadtime INT
			'
	--print @sql
	exec (@sql)
	FETCH NEXT FROM @GetTables INTO @TableName
END
CLOSE @GetTables
