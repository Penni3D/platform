﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[instance_menu]') AND name = 'link_type') 
    ALTER TABLE instance_menu
          ADD link_type int NOT NULL 
          CONSTRAINT im_link_type_df DEFAULT 0

IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[instance_menu]') AND name = 'page') 
    ALTER TABLE instance_menu
          ADD page nvarchar(255) NULL



exec app_ensure_archive 'instance_menu'


CREATE TABLE [dbo].[instance_menu_states]( 
  [instance_menu_id] [int]  NOT NULL, 
  [item_id] [int]  NOT NULL, 
  [state] [nvarchar](255) NOT NULL 
) 

GO

ALTER TABLE [dbo].[instance_menu_states]  WITH CHECK ADD  CONSTRAINT [FK_instance_menu_states] FOREIGN KEY([item_id])
REFERENCES [dbo].[items] ([item_id])

GO

ALTER TABLE [dbo].[instance_menu_states]  WITH CHECK ADD  CONSTRAINT [FK_instance_menu_states2] FOREIGN KEY([instance_menu_id])
REFERENCES [dbo].[instance_menu] ([instance_menu_id])
  ON DELETE CASCADE
   ON UPDATE CASCADE

GO
alter table [dbo].[instance_menu_states]  add primary key (instance_menu_id, item_id,state)



exec app_set_archive_user_id 0

UPDATE instance_menu set page = 'newUser', link_type=0 WHERE default_state = 'process.user.new' AND page is null 
UPDATE instance_menu set page = 'newUserGroup', link_type=0 WHERE default_state = 'process.userGroup.new' AND page is null 
UPDATE instance_menu set page = 'CUSTOMER_PROCESSES', link_type=0 WHERE default_state = 'processes' AND page is null 
UPDATE instance_menu set page = 'SUPPORT_PROCESSES', link_type=0 WHERE default_state = 'processes' AND page is null 
UPDATE instance_menu set page = 'DATA_PROCESSES', link_type=0 WHERE default_state = 'processes' AND page is null 
UPDATE instance_menu set page = 'LINK_PROCESSES', link_type=0 WHERE default_state = 'processes' AND page is null 
UPDATE instance_menu set page = 'documentation', link_type=0 WHERE default_state = 'documentation' AND page is null 
UPDATE instance_menu set page = 'configuration.client', link_type=0 WHERE default_state = 'configuration.client' AND page is null 
UPDATE instance_menu set page = 'eventLog', link_type=0 WHERE default_state = 'eventLog' AND page is null 
UPDATE instance_menu set page = 'Calendar', link_type=0 WHERE default_state = 'calendar' AND page is null 
UPDATE instance_menu set page = 'Notifications', link_type=0 WHERE default_state = 'notifications' AND page is null 
UPDATE instance_menu set page = 'Emails', link_type=0 WHERE default_state = 'emails' AND page is null 
UPDATE instance_menu set page = 'Bulletins', link_type=0 WHERE default_state = 'bulletins' AND page is null 
UPDATE instance_menu set page = 'Show Bulletins', link_type=0 WHERE default_state = 'showBulletins' AND page is null 
UPDATE instance_menu set page = 'Lists', link_type=0 WHERE default_state = 'lists' AND page is null 
UPDATE instance_menu set page = 'Notification tags', link_type=0 WHERE default_state = 'notificationtags' AND page is null 
UPDATE instance_menu set page = 'roadmap', link_type=0 WHERE default_state = 'roadmap' AND page is null 
UPDATE instance_menu set page = 'Translation Editor', link_type=0 WHERE default_state = 'translationEditor' AND page is null 
UPDATE instance_menu set page = 'InstanceCompare', link_type=0 WHERE default_state = 'InstanceCompare' AND page is null 
UPDATE instance_menu set page = 'Page View Statistics', link_type=0 WHERE default_state = 'statspage' AND page is null 
UPDATE instance_menu set page = 'DataGenerator', link_type=0 WHERE default_state = 'dataGenerator' AND page is null 
UPDATE instance_menu set page = 'Organisation Management', link_type=0 WHERE default_state = 'organisationManagement' AND page is null 
UPDATE instance_menu set page = 'Organisation Management - Function', link_type=0 WHERE default_state = 'organisationManagement.function' AND page is null 
UPDATE instance_menu set page = 'Organisation Management - Location', link_type=0 WHERE default_state = 'organisationManagement.location' AND page is null 
UPDATE instance_menu set page = 'Organisation Management - Cost center', link_type=0 WHERE default_state = 'organisationManagement.costCenters' AND page is null 
UPDATE instance_menu set page = 'Organisation Management - Cost center owner', link_type=0 WHERE default_state = 'organisationManagement.costCentersOwners' AND page is null 
UPDATE instance_menu set page = 'Organisation Management - Competencies', link_type=0 WHERE default_state = 'organisationManagement.competencies' AND page is null 
UPDATE instance_menu set page = 'Translation Client', link_type=0 WHERE default_state = 'translationclient' AND page is null 
UPDATE instance_menu set page = 'Workload Report', link_type=0 WHERE default_state = 'workloadReport' AND page is null 
UPDATE instance_menu set page = 'Worktime Tracking', link_type=0 WHERE default_state = 'worktimeTracking' AND page is null 
UPDATE instance_menu set page = 'Worktime Hour Approval', link_type=0 WHERE default_state = 'worktimeHourApproval' AND page is null 
UPDATE instance_menu set page = 'workTimeReport', link_type=0 WHERE default_state = 'workTimeReport' AND page is null 
UPDATE instance_menu set page = 'Integration', link_type=0 WHERE default_state = 'integration' AND page is null 
UPDATE instance_menu set page = 'KtnExcelImport', link_type=0 WHERE default_state = 'ktnExcelImport' AND page is null 
UPDATE instance_menu set page = 'projects_allocations', link_type=0 WHERE default_state = 'projectsAllocations' AND page is null 
UPDATE instance_menu set page = 'resource_allocation', link_type=0 WHERE default_state = 'resourceAllocation' AND page is null 
UPDATE instance_menu set page = 'resources_allocations', link_type=0 WHERE default_state = 'resourcesAllocations' AND page is null 
UPDATE instance_menu set page = 'instanceMenu', link_type=0 WHERE default_state = 'instanceMenu' AND page is null 

UPDATE instance_menu set link_type=1 WHERE default_state = 'portfolio' AND page is null 
UPDATE instance_menu set link_type=2 WHERE default_state = 'process.new' AND page is null 

