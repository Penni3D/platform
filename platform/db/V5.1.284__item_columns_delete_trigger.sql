ALTER TRIGGER [dbo].[item_columns_fk_delete]

    ON [dbo].[item_columns]

    INSTEAD OF DELETE

AS

    DELETE dd FROM process_container_columns dd            INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

    DELETE dd FROM process_portfolio_columns dd    INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

    DELETE dd FROM attachments dd                    INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

    DELETE dd FROM item_data_process_selections dd                    INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

    DELETE dd FROM process_tab_columns dd                    INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

 

    DELETE dd FROM state_rights dd                    INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

    DELETE dd FROM state_rights dd                    INNER JOIN deleted d ON dd.user_column_id = d.item_column_id

    DELETE dd FROM item_column_equations dd                    INNER JOIN deleted d ON dd.parent_item_column_id = d.item_column_id

    DELETE dd FROM item_column_equations dd                    INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

      DELETE dd FROM item_columns dd                    INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
	  
	  
	  
GO
    
ALTER PROCEDURE [dbo].[items_Reset]

AS

BEGIN

    SET NOCOUNT ON;

    DELETE FROM item_tables

    DELETE FROM items

    DELETE FROM item_columns

    DELETE FROM processes

    DELETE FROM instances

END





GO





ALTER PROCEDURE [dbo].[items_ChangeColumnType]

    @ItemColumnId INT, @OldDataType INT, @NewDataType TINYINT

AS

BEGIN

    SET NOCOUNT ON;





    IF (@OldDataType = -1) BEGIN 

        SELECT @OldDataType = data_type FROM item_columns WHERE item_column_id = @ItemColumnId

    END





    IF (@OldDataType <> @NewDataType) BEGIN

        DECLARE @ColumnName NVARCHAR(50)

        DECLARE @TableName NVARCHAR(50)

        DECLARE @instance NVARCHAR(10)

        DECLARE @process NVARCHAR(50)





        SELECT @ColumnName = name, @instance = instance, @process = process FROM item_columns WHERE item_column_id = @ItemColumnId

        SELECT @tableName = name FROM item_tables WHERE instance = @instance AND process = @process





        --Change Datatype

        UPDATE item_columns SET data_type = @NewDataType WHERE item_column_id = @ItemColumnId

        EXEC items_DropColumn @TableName, @ColumnName

        EXEC items_AddColumn @TableName, @ColumnName, @NewDataType

    END

END