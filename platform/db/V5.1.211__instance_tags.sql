﻿CREATE PROCEDURE [dbo].[app_ensure_process_tags]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'instance_tags'
    DECLARE @process2 NVARCHAR(50) = 'instance_tags_groups'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create tags groups process
    EXEC app_ensure_process @instance, @process2, @process2, 0

    DECLARE @i2 INT
    EXEC dbo.app_ensure_column @instance,@process2, 'tag_group', 0, 0, 1, 0, 0, NULL, 0, 0,  @item_column_id = @i2 OUTPUT
    
    update item_columns SET unique_col = 1 WHERE instance = @instance AND process = @process2 AND item_column_id = @i2

    --Create tags process
    EXEC app_ensure_process @instance, @process, @process, 0

    DECLARE @i1 INT
    EXEC dbo.app_ensure_column @instance,@process, 'tag_name', 0, 0, 1, 0, 0, NULL, 0, 0,  @item_column_id = @i1 OUTPUT
    EXEC dbo.app_ensure_column @instance,@process, 'tag_group', 0, 5, 1, 0, 0, NULL, 0, 0, @process2
    
    update item_columns SET status_column = 1 WHERE instance = @instance AND process = @process
    update item_columns SET unique_col = 1 WHERE instance = @instance AND process = @process AND item_column_id = @i1
  END
GO



DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
    exec app_ensure_process_tags @instance
    FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor
