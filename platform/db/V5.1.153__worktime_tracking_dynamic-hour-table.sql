IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_worktime_tracking_hours'))
  exec('CREATE PROCEDURE [dbo].[app_ensure_process_worktime_tracking_hours] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_worktime_tracking_hours]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'worktime_tracking_hours'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_process @instance, @process, @process, 1

    EXEC app_ensure_column @instance, @process, 'user_item_id', 0, 1, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'task_item_id', 0, 1, 1, 1, 0, null,  0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'process', 0, 4, 1, 1, 0, NULL, 0, 0
    EXEC app_ensure_column @instance, @process, 'hours', 0, 2, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'additional_hours', 0, 2, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'date', 0, 3, 1, 1, 0, null,  0, 0
    
    EXEC app_ensure_column @instance, @process, 'line_manager_status', 0, 1, 0, 0, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'line_manager_user_id', 0, 1, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'project_manager_status', 0, 1, 0, 0, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'project_manager_user_id', 0, 1, 1, 1, 0, null,  0, 0
    
    EXEC app_ensure_column @instance, @process, 'project_manager_approved_at', 0, 3, 1, 1, 0, null,  0, 0
    EXEC app_ensure_column @instance, @process, 'line_manager_approved_at', 0, 3, 1, 1, 0, null,  0, 0
    
    EXEC app_ensure_column @instance, @process, 'created_at', 0, 3, 1, 1, 0, null,  0, 0
  END
GO

EXEC app_execute_procedure_for_all_instances 'app_ensure_process_worktime_tracking_hours'
GO 

ALTER TABLE worktime_tracking_hours_breakdown DROP CONSTRAINT FK_worktime_tracking_hours_item

GO 

 
DECLARE @origItemId INT
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR 
SELECT wth.id, (select instance from items i where i.item_id = wth.user_item_id) FROM worktime_tracking_hours wth
DECLARE @sql NVARCHAR(MAX) = ''
DECLARE @tableName NVARCHAR(MAX) = ''

OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @origItemId, @instance

WHILE @@fetch_status = 0
  BEGIN
    DECLARE @table NVARCHAR(MAX) = ''
    SET @table = 'worktime_tracking_hours'
    DECLARE @orderNo NVARCHAR(MAX) = 'e'
    INSERT INTO items (instance, process) VALUES (@instance, @table);
    DECLARE @itemId INT = @@identity;
    
    SET @tableName = '_' + @instance + '_' + @table
    
    
    SET @sql = 'UPDATE '+@tableName+' ' + 
    'SET user_item_id = org.user_item_id, task_item_id = org.task_item_id, process = org.process, date = org.date, hours = org.hours, line_manager_status = org.line_manager_status, line_manager_user_id = org.line_manager_user_id, project_manager_status = org.project_manager_status, project_manager_user_id = org.project_manager_user_id, additional_hours = org.additional_hours, line_manager_approved_at = org.line_manager_approved_at, project_manager_approved_at = org.project_manager_approved_at ' + 
    'FROM worktime_tracking_hours org WHERE item_id = ' + CAST (@itemId AS nvarchar) + ' and org.id = ' + CAST (@origItemId AS nvarchar) 
     
    EXEC (@sql)
    
    SET @sql = 'UPDATE worktime_tracking_hours_breakdown SET worktime_tracking_hour_id =  ' + CAST (@itemId AS nvarchar)  + ' WHERE worktime_tracking_hour_id = ' + CAST (@origItemId AS nvarchar)   
    EXEC (@sql)

    FETCH  NEXT FROM instanceCursor INTO
      @origItemId, @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor