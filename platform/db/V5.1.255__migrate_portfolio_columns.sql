CREATE FUNCTION dbo.udf_SplitVariable
(
   @List varchar(8000),
   @SplitOn varchar(5) = ','
)

RETURNS @RtnValue TABLE
(
   Id INT IDENTITY(1,1),
   Value VARCHAR(8000)
)

AS
BEGIN

--Account for ticks
SET @List = (REPLACE(@List, '''', ''))

--Account for 'emptynull'
IF LTRIM(RTRIM(@List)) = 'emptynull'
BEGIN
   SET @List = ''
END

--Loop through all of the items in the string and add records for each item
WHILE (CHARINDEX(@SplitOn,@List)>0)
BEGIN
   INSERT INTO @RtnValue (value)
   SELECT Value = LTRIM(RTRIM(SUBSTRING(@List, 1, CHARINDEX(@SplitOn, @List)-1)))  

   SET @List = SUBSTRING(@List, CHARINDEX(@SplitOn,@List) + LEN(@SplitOn), LEN(@List))
END

INSERT INTO @RtnValue (Value)
SELECT Value = LTRIM(RTRIM(@List))

RETURN

END
GO

DECLARE @var NVARCHAR(MAX)
DECLARE @var2 NVARCHAR(MAX)
DECLARE @var3 NVARCHAR(MAX)
DECLARE @var4 NVARCHAR(MAX)

DECLARE @startPos INT = 0
DECLARE @startPos2 INT = 0
DECLARE @sql NVARCHAR(MAX);
DECLARE @GetTables CURSOR
DECLARE @GetTables2 CURSOR
DECLARE @first int = 0

DECLARE @identifier int = 0
DECLARE @instance nvarchar(10)
DECLARE @userId int = 0

    --Get Tables
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR  select value, identifier, instance, user_id from instance_user_configurations
where identifier > 0 and [group] = 'portfolios'
OPEN @GetTables
    FETCH NEXT FROM @GetTables INTO @var, @identifier, @instance, @userId

    WHILE (@@FETCH_STATUS=0) BEGIN
       -- print @identifier
        set @startPos = charindex('columns":', @var)
        set @startPos2 = charindex(']', @var, @startPos)

		print @var
		print @startpos + 9
		print @startpos2
		print @startpos2 - @startpos - 9

        set @var2 = replace(replace(replace(substring(@var, @startpos+9, @startpos2-@startpos-9), '"', ''), ' ', ''), char(10), '')

       -- print @var2


        SET @GetTables2 = CURSOR FAST_FORWARD LOCAL FOR select '"' + name + '"' from process_portfolio_columns ppc
        inner join item_columns ic
        on ppc.item_column_id = ic.item_column_id
        where process_portfolio_id = @identifier and name not in (select value from udf_SplitVariable(@var2, ',')) and name is not null

        set @var4 = 'columns:['

        set @first = 0

             OPEN @GetTables2
            FETCH NEXT FROM @GetTables2 INTO @var3

            WHILE (@@FETCH_STATUS=0) BEGIN
                --print 'process 2'
                    --print @var3
                if @first <> 0
                BEGIN
                    set @var3 = ',' + @var3    
                END
                set @first = 1
                --print @var3
                --print @var4
                
                set @var4 = @var4 + @var3
            
        --print 'var33'
        --print @var3
        --print 'var44'    
        --print @var4    
                FETCH NEXT FROM @GetTables2 INTO @var3
            END
            
        set @var4 = @var4 + ']'
        --print 'var4'
        --print @var4

       -- print substring(@var, 0, @startpos) + @var4 + substring(@var, @startpos2, len(@var) - @startpos2 )
        
        --update set instance_user_configurations set value = substring(@var, 0, @startpos) + @var4 + substring(@var, @startpos2, len(@var) - @startpos2 ) where instance = @instance and user_id = @userId and identifier = @identifier

        FETCH NEXT FROM @GetTables INTO @var, @identifier, @instance, @userId
    END
    CLOSE @GetTables
	
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR  select value, identifier, instance, user_id from instance_user_configurations_saved
where identifier > 0 and [group] = 'portfolios'
OPEN @GetTables
FETCH NEXT FROM @GetTables INTO @var, @identifier, @instance, @userId

WHILE (@@FETCH_STATUS=0) BEGIN
	--print @identifier
	set @startPos = charindex('columns":', @var)
	set @startPos2 = charindex(']', @var, @startPos)

	set @var2 = replace(replace(replace(substring(@var, @startpos+9, @startpos2-@startpos-9), '"', ''), ' ', ''), char(10), '')

	--print @var2


	SET @GetTables2 = CURSOR FAST_FORWARD LOCAL FOR select '"' + name + '"' from process_portfolio_columns ppc
	inner join item_columns ic
	on ppc.item_column_id = ic.item_column_id
	where process_portfolio_id = @identifier and name not in (select value from udf_SplitVariable(@var2, ',')) and name is not null

	set @var4 = 'columns:['

	set @first = 0

		 OPEN @GetTables2
		FETCH NEXT FROM @GetTables2 INTO @var3

		WHILE (@@FETCH_STATUS=0) BEGIN
			--print 'process 2'
				--print @var3
			if @first <> 0
			BEGIN
				set @var3 = ',' + @var3    
			END
			set @first = 1
			--print @var3
			--print @var4
			
			set @var4 = @var4 + @var3
		
	--print 'var33'
	--print @var3
	--print 'var44'    
	--print @var4    
			FETCH NEXT FROM @GetTables2 INTO @var3
		END
		
	set @var4 = @var4 + ']'
	--print 'var4'
	--print @var4

	--print substring(@var, 0, @startpos) + @var4 + substring(@var, @startpos2, len(@var) - @startpos2 )
	
	--update set instance_user_configurations set value = substring(@var, 0, @startpos) + @var4 + substring(@var, @startpos2, len(@var) - @startpos2 ) where instance = @instance and user_id = @userId and identifier = @identifier

	FETCH NEXT FROM @GetTables INTO @var, @identifier, @instance, @userId
END
CLOSE @GetTables

GO
	
DROP FUNCTION udf_SplitVariable;  
