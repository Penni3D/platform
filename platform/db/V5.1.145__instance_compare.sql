SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[list_GetId]
(
 @instance nvarchar(10),
 @process nvarchar(50),
 @listItemValue nvarchar(max),
 @id int OUTPUT
)
AS
BEGIN
	DECLARE @sql nvarchar(max)

	if not exists (select process from processes where instance = @instance and process = @process)
begin
	set @id = 0
end
else
begin
	set @sql = 'SELECT @id = item_id FROM _' + @instance + '_' + @process + ' WHERE list_item = ''' + @listItemValue + '''' 
	exec sp_executesql @sql, N'@id nvarchar(max) out', @id out
end
END
GO



SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[conditions_replace]
(
@sourceInstance nvarchar(10),
@targetInstance nvarchar(10)
)
AS
BEGIN
	DECLARE @condition_id INT
	DECLARE @instance nvarchar(10)
	DECLARE @process nvarchar(50)
	DECLARE @VARIABLE nvarchar(255)
	DECLARE @condition_json nvarchar(max)

	DECLARE @conditionCursor CURSOR 
	SET @conditionCursor = CURSOR  FAST_FORWARD LOCAL FOR 
	SELECT condition_id, instance, process, variable, condition_json FROM conditions WHERE instance IN (@sourceInstance, @targetInstance)

	OPEN @conditionCursor
	FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	WHILE (@@FETCH_STATUS=0) BEGIN
		declare @json as nvarchar(max)
		declare @json2 as nvarchar(max)
		exec dbo.condition_IdReplaceProc @condition_json,  @replaced_json = @json output
		exec dbo.condition_VarReplaceProc @sourceInstance, @targetInstance, @json,  @replaced_json = @json2 output
		--print '@json1'
	--	print @json
		--print '@json2'
	--	print @json2
		insert into #conditions (condition_id, instance, process, variable, condition_json, replaced_condition_json) values (@condition_id, @instance, @process, @variable, @json, @json2)
		FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	END
END
GO


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
alter  PROCEDURE [dbo].[app_copy_instance]	-- Add the parameters for the stored procedure here
						@source_instance nvarchar(10),
						@target_instance nvarchar(10),
						@copy_support_data as int = 0,
						@copy_data as int = 0
						AS BEGIN
						exec app_set_archive_user_id 0 

DECLARE @sql NVARCHAR(MAX)

			INSERT INTO instances (instance) VALUES (@target_instance);  DECLARE @calendar_events TABLE ( calendar_event_id int, new_calendar_event_id int ) 

 DECLARE @calendars TABLE ( calendar_id int, new_calendar_id int ) 
 DECLARE @calendar_id int, @new_calendar_id int;
disable trigger calendars_INSTEAD_OF_INSERT on calendars;

disable trigger calendars_AFTER_UPDATE on calendars;

 MERGE calendars AS t USING ( SELECT calendar_id ,@target_instance as instance ,  item_id, variable, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, calendar_name, base_calendar FROM calendars WHERE 1=1 AND  instance = @source_instance  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
instance,   item_id, variable, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, calendar_name, base_calendar ) VALUES ( 
instance,   item_id, variable, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, calendar_name, base_calendar )  
 OUTPUT s.calendar_id, inserted.calendar_id INTO @calendars
; enable trigger calendars_INSTEAD_OF_INSERT on calendars ;

 enable trigger calendars_AFTER_UPDATE on calendars ;



 DECLARE @calendar_event_id int, @new_calendar_event_id int;
disable trigger calendar_events_INSTEAD_OF_INSERT on calendar_events;

disable trigger calendar_events_AFTER_UPDATE on calendar_events;

 MERGE calendar_events AS t USING ( SELECT calendar_event_id ,  calendar_id, name, event_start, event_end FROM calendar_events WHERE 1=1 AND  calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @source_instance )  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
 calendar_id, name, event_start, event_end ) VALUES ( 
 calendar_id, name, event_start, event_end )  
 OUTPUT s.calendar_event_id, inserted.calendar_event_id INTO @calendar_events
; enable trigger calendar_events_INSTEAD_OF_INSERT on calendar_events ;

 enable trigger calendar_events_AFTER_UPDATE on calendar_events ;



 DECLARE @calendar_static_holidays TABLE ( calendar_static_holiday_id int, new_calendar_static_holiday_id int ) 
 DECLARE @calendar_static_holiday_id int, @new_calendar_static_holiday_id int;
disable trigger calendar_static_holidays_INSTEAD_OF_INSERT on calendar_static_holidays;

disable trigger calendar_static_holidays_AFTER_UPDATE on calendar_static_holidays;

 MERGE calendar_static_holidays AS t USING ( SELECT calendar_static_holiday_id ,  calendar_id, name, month, day FROM calendar_static_holidays WHERE 1=1 AND  calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @source_instance )  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
 calendar_id, name, month, day ) VALUES ( 
 calendar_id, name, month, day )  
 OUTPUT s.calendar_static_holiday_id, inserted.calendar_static_holiday_id INTO @calendar_static_holidays
; enable trigger calendar_static_holidays_INSTEAD_OF_INSERT on calendar_static_holidays ;

 enable trigger calendar_static_holidays_AFTER_UPDATE on calendar_static_holidays ;




disable trigger instance_languages_INSTEAD_OF_INSERT on instance_languages;

disable trigger instance_languages_AFTER_UPDATE on instance_languages;

 INSERT INTO instance_languages (instance,  code, name, abbr, icon_url ) 
 (SELECT @target_instance as instance ,  code, name, abbr, icon_url FROM instance_languages WHERE  instance = @source_instance   )

; enable trigger instance_languages_INSTEAD_OF_INSERT on instance_languages ;

 enable trigger instance_languages_AFTER_UPDATE on instance_languages ;



 DECLARE @instance_menu TABLE ( instance_menu_id int, new_instance_menu_id int ) 
 DECLARE @instance_menu_id int, @new_instance_menu_id int;
disable trigger instance_menu_INSTEAD_OF_INSERT on instance_menu;

disable trigger instance_menu_AFTER_UPDATE on instance_menu;

 MERGE instance_menu AS t USING ( SELECT instance_menu_id ,@target_instance as instance ,  parent_id, variable, order_no, default_state, params FROM instance_menu WHERE 1=1 AND instance = @source_instance   ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
instance,  parent_id, variable, order_no, default_state, params ) VALUES ( 
instance,  parent_id, variable, order_no, default_state, params )  
 OUTPUT s.instance_menu_id, inserted.instance_menu_id INTO @instance_menu
; enable trigger instance_menu_INSTEAD_OF_INSERT on instance_menu ;

 enable trigger instance_menu_AFTER_UPDATE on instance_menu ;

 insert into instance_configurations   ([instance]
      ,[process]
      ,[group_name]
      ,[category]
      ,[name]
      ,[value]
      ,[server]) ( SELECT       @target_instance
      ,[process]
      ,[group_name]
      ,[category]
      ,[name]
      ,[value]
      ,[server] FROM instance_configurations WHERE instance = @source_instance and value is not null and process in (select process from processes where 		instance = @target_instance))

 INSERT INTO calendar_data_attributes (
calendar_id, name, type_id, event_date) SELECT  (SELECT new_calendar_id FROM @calendars WHERE calendar_id = calendar_data_attributes.calendar_id),  name, type_id, event_date FROM calendar_data_attributes WHERE 1=0  OR calendar_id IN (SELECT calendar_id FROM @calendars) 

 --INSERT INTO instance_hosts (
 --instance, host) ( SELECT  @target_instance, host FROM instance_hosts WHERE instance = @source_instance )


 INSERT INTO instance_translations (instance,code, variable, translation) SELECT @target_instance, code, variable, translation from instance_translations where instance = @source_instance


 INSERT INTO calendar_data (
calendar_event_id,calendar_static_holiday_id,calendar_id, event_date, dayoff, holiday, notwork) SELECT  (SELECT new_calendar_event_id FROM @calendar_events WHERE calendar_event_id = calendar_data.calendar_event_id), (SELECT new_calendar_static_holiday_id FROM @calendar_static_holidays WHERE calendar_static_holiday_id = calendar_data.calendar_static_holiday_id), (SELECT new_calendar_id FROM @calendars WHERE calendar_id = calendar_data.calendar_id),  event_date, dayoff, holiday, notwork FROM calendar_data WHERE 1=0  OR calendar_event_id IN (SELECT calendar_event_id FROM @calendar_events) 
 OR calendar_static_holiday_id IN (SELECT calendar_static_holiday_id FROM @calendar_static_holidays) 
 OR calendar_id IN (SELECT calendar_id FROM @calendars) 

 		DECLARE @GetProcesses CURSOR
		SET @GetProcesses = CURSOR FAST_FORWARD LOCAL FOR
		select process, process_type from processes where instance = @source_instance 
		
		IF OBJECT_ID('tempdb..#NewItemColumns') IS NOT NULL DROP TABLE #NewItemColumns
		CREATE table #NewItemColumns (item_column_id int, new_item_column_id int )

		IF OBJECT_ID('tempdb..#NewItems') IS NOT NULL DROP TABLE #NewItems
		CREATE table #NewItems (item_id int, new_item_id int )

		IF OBJECT_ID('tempdb..#NewPortfolios') IS NOT NULL DROP TABLE #NewPortfolios
		CREATE table #NewPortfolios (process_portfolio_id int, new_process_portfolio_id int )

		IF OBJECT_ID('tempdb..#NewTabs') IS NOT NULL DROP TABLE #NewTabs
		CREATE table #NewTabs (process_tab_id int, new_process_tab_id int )

		IF OBJECT_ID('tempdb..#NewConditions') IS NOT NULL DROP TABLE #NewConditions
		CREATE table #NewConditions (condition_id int, new_condition_id int )

		DECLARE @counter as int = 0

		DECLARE @process AS NVARCHAR(MAX) = ''
		DECLARE @processType AS int

		OPEN @GetProcesses
		FETCH NEXT FROM @GetProcesses INTO @process, @processType
		WHILE (@@FETCH_STATUS=0) BEGIN
			print	'copy ' + @process
			IF @process <> 'task' and @process <> 'task_link_type'
			BEGIN

				if @counter = 0 
					begin
						set @sql = ' exec app_copy_process ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @process +char(39) + ', 1'
					end
				else 
					begin
						set @sql = ' exec app_copy_process_temp ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @process +char(39)
					end

				--set @sql = ' exec app_copy_process_2 ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @process +char(39) + ', 1'			
			
				if @processType = 0 AND @copy_data = 1 set @sql = @sql + ',1'
				if @processType = 1 AND @copy_support_data = 1 set @sql = @sql + ',1'
				if @processType = 2 AND @copy_support_data = 1 set @sql = @sql + ',1'

				print	@sql

				exec (@sql)

				set @counter = @counter +1

			END
			ELSE
			BEGIN
			IF @process <> 'task'
			BEGIN 
				exec app_ensure_process_task @target_instance
				END
			END
		FETCH NEXT FROM @GetProcesses INTO @process, @processType
		END

		SET @sql = 'drop procedure app_copy_process_temp'
		exec (@sql)

		set @sql = 'disable trigger _' + @target_instance +'_task_AFTER_UPDATE on _' + @target_instance + '_task';
		exec (@sql)

	--	if @copy_support_data = 1 and @copy_data = 0
	--	begin
			
			--insert into item_data_process_selections (item_id, item_column_id, selected_item_id) 
			--(
			--	select 
			--	(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
			--	(select new_item_column_id from #NewItemColumns WHERE item_column_id = idps.item_column_id ),
			--	(select new_item_id from #NewItems WHERE item_id = idps.selected_item_id )

			--	from item_data_process_selections idps
			--	inner join items i1 on idps.item_id = i1.item_id 
			--	inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
			--	inner join items i2 on idps.selected_item_id = i1.item_id 
			--	inner join processes p2 on  i2.instance = p2.instance and i2.process = p2.process
			--	 WHERE idps.item_id in (select item_id from #NewItems) OR selected_item_id in (select item_id from #NewItems) and p1.process_type > 0 and p2.process_type > 0
			--	-- and idps.item_id <> idps.selected_item_id
			--)

			--insert into item_subitems (parent_item_id, item_id, favourite) 
			--(
			--	select 
			--	(select new_item_id from #NewItems WHERE item_id = idps.parent_item_id ),
			--	(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
			--	favourite

			--	from item_subitems idps
			--	inner join items i1 on idps.parent_item_id = i1.item_id 
			--	inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
			--	inner join items i2 on idps.item_id = i1.item_id 
			--	inner join processes p2 on  i2.instance = p2.instance and i2.process = p2.process
			--	 WHERE idps.parent_item_id in (select item_id from #NewItems) OR idps.item_id in (select item_id from #NewItems) and p1.process_type > 0 and p2.process_type > 0
			--)


			--INSERT INTO instance_menu_rights (instance_menu_id, item_id) 
			--(
			--	SELECT 
			--	(select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = im.instance_menu_id),
			--	(select new_item_id FROM #NewItems WHERE item_id = im.item_id) FROM instance_menu_rights im
			--	WHERE im.instance_menu_id in (select instance_menu_id from @instance_menu) )
			--	UPDATE instance_menu set parent_id = (select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = parent_id) WHERE instance = @target_instance

	--	end
		if @copy_data = 1
		begin
			insert into item_data_process_selections (item_id, item_column_id, selected_item_id) 
			(
				select 
				(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
				(select new_item_column_id from #NewItemColumns WHERE item_column_id = idps.item_column_id ),
				(select new_item_id from #NewItems WHERE item_id = idps.selected_item_id )

				from item_data_process_selections idps
				inner join items i1 on idps.item_id = i1.item_id 
				inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
				 WHERE idps.item_id in (select item_id from #NewItems) OR selected_item_id in (select item_id from #NewItems)
			)
			insert into item_subitems (parent_item_id, item_id, favourite) 
			(
				select 
				(select new_item_id from #NewItems WHERE item_id = idps.parent_item_id ),
				(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
				favourite

				from item_subitems idps
				inner join items i1 on idps.parent_item_id = i1.item_id 
				inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
				inner join items i2 on idps.item_id = i2.item_id 
				inner join processes p2 on  i2.instance = p2.instance and i2.process = p2.process
				 WHERE idps.parent_item_id in (select item_id from #NewItems) OR idps.item_id in (select item_id from #NewItems) 
			)
			INSERT INTO instance_menu_rights (instance_menu_id, item_id) 
			(
				SELECT 
				(select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = im.instance_menu_id),
				(select new_item_id FROM #NewItems WHERE item_id = im.item_id) FROM instance_menu_rights im
				WHERE im.instance_menu_id in (select instance_menu_id from @instance_menu) )

			UPDATE instance_menu set parent_id = (select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = parent_id) WHERE instance = @target_instance

			INSERT INTO allocation_durations (allocation_item_id, date, hours) 
			(select  
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.allocation_item_id ),
				date, hours from allocation_durations i where allocation_item_id in (SELECT item_id FROM #NewItems)
			)

			INSERT INTO allocation_comments (allocation_item_id, comment, created_at, author_item_id) 
			(select  
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.allocation_item_id ),
				comment, created_at, (SELECT new_item_id FROM #NewItems WHERE item_id = i.author_item_id ) 
			from allocation_comments i where allocation_item_id in (SELECT item_id FROM #NewItems)
			)

			INSERT INTO task_durations (task_item_id, resource_item_id, date, hours) 
			(select  
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.task_item_id ),
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.resource_item_id ),
				date, hours from task_durations i where task_item_id in (SELECT item_id FROM #NewItems)
			)
		end


		INSERT INTO process_subprocesses (parent_process, instance, process) ( SELECT  parent_process, @target_instance, process FROM process_subprocesses WHERE instance = @source_instance)


		
 		DECLARE @GetPortfolios CURSOR
		SET @GetPortfolios = CURSOR FAST_FORWARD LOCAL FOR
		select process_portfolio_id, new_process_portfolio_id from #NewPortfolios
		
		DECLARE @process_portfolio_id int, @new_process_portfolio_id int

		OPEN @GetPortfolios
		FETCH NEXT FROM @GetPortfolios INTO @process_portfolio_id, @new_process_portfolio_id
		WHILE (@@FETCH_STATUS=0) BEGIN
			UPDATE instance_menu set params = replace(params, 'portfolioId":' + CAST(@process_portfolio_id as nvarchar) + '}', 'portfolioId":' + CAST(@new_process_portfolio_id as nvarchar) + '}') where instance = @target_instance			
			
			UPDATE instance_configurations SET value = COALESCE(REPLACE(value, 'portfolioId":[' + CAST(@process_portfolio_id as nvarchar) + ']', 'portfolioId":[' + CAST(@new_process_portfolio_id as nvarchar) + ']'), '')
			WHERE instance = @target_instance AND category = 'Main application' and name = 'Frontpage layout'

			FETCH NEXT FROM @GetPortfolios INTO @process_portfolio_id, @new_process_portfolio_id
		END

		UPDATE item_columns 
		SET 
		data_additional2 = (select new_item_column_id from #NewItemColumns WHERE item_column_id = CAST(data_additional2 as int) )
		WHERE instance = @target_instance and data_type = 11

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_item_column_id, '') FROM #NewItemColumns WHERE item_column_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'FavouriteColumnId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'User portfolio ID'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'BacklogPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'SprintsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'GanttPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'KanbanPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'EmailsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'NotificationsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'BulletinsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'SendedEmailsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'UnreadNotificationsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'AllNotificationsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_tab_id, '') FROM #NewTabs WHERE process_tab_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Evaluation' and name = 'EvaluationIdeaTabId'

 		DECLARE @ItemCols CURSOR
		SET @ItemCols = CURSOR FAST_FORWARD LOCAL FOR
		SELECT item_column_id, new_item_column_id FROM #NewItemColumns

		DECLARE @item_column_id int, @new_item_column_id int

		OPEN @ItemCols
		FETCH NEXT FROM @ItemCols INTO @item_column_id, @new_item_column_id
		WHILE (@@FETCH_STATUS=0) BEGIN
			UPDATE conditions set condition_json = replace(condition_json, '"ItemColumnId":' + CAST(@item_column_id AS NVARCHAR) + ',', '"ItemColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + ',') WHERE instance = @target_instance 
			UPDATE conditions set condition_json = replace(condition_json, '"ItemColumnId":' + CAST(@item_column_id AS NVARCHAR) + '}', '"ItemColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + '}') WHERE instance = @target_instance
			UPDATE conditions set condition_json = replace(condition_json, '"UserColumnId":' + CAST(@item_column_id AS NVARCHAR) + ',', '"UserColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + ',') WHERE instance = @target_instance 
			UPDATE conditions set condition_json = replace(condition_json, '"UserColumnId":' + CAST(@item_column_id AS NVARCHAR) + '}', '"UserColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + '}') WHERE instance = @target_instance
		FETCH NEXT FROM  @ItemCols INTO @item_column_id, @new_item_column_id
		END	


 		DECLARE @Items CURSOR
		SET @Items = CURSOR FAST_FORWARD LOCAL FOR
		SELECT item_id, new_item_id FROM #NewItems

		DECLARE @item_id int, @new_item_id int

		OPEN @Items
		FETCH NEXT FROM @Items INTO @item_id, @new_item_id
		WHILE (@@FETCH_STATUS=0) BEGIN
			UPDATE conditions set condition_json = replace(condition_json, '"Value":' + CAST(@item_id AS NVARCHAR) + ',', '"Value":' + CAST(@new_item_id AS NVARCHAR) + ',') WHERE instance = @target_instance 
			UPDATE conditions set condition_json = replace(condition_json, '"Value":' + CAST(@item_id AS NVARCHAR) + '}', '"Value":' + CAST(@new_item_id AS NVARCHAR) + '}') WHERE instance = @target_instance
		FETCH NEXT FROM  @Items INTO @item_id, @new_item_id
		END	
		

		
		--set @sql = 'UPDATE _' + @target_instance + '_task  set parent_item_id = (select new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_task.parent_item_id)  WHERE parent_item_id IS NOT NULL AND item_id IN ( SELECT item_id FROM items WHERE instance = ''' + @target_instance + ''' AND process = ''task'') ' 
		--exec (@sql)




		insert into task_order (order_set, owner_id, task_id, order_no)
		 ( select 
		 order_set, owner_id,
		 (select new_item_id from #NewItems where item_id = task_id), 
		 order_no
		from task_order
		where 
		task_id in (select item_id from #NewItems )
		)


	 
 		IF OBJECT_ID('tempdb..#task_status') IS NOT NULL DROP TABLE #task_status
		CREATE table #task_status (status_id int, new_status_id int )

		 DECLARE @status_id int, @new_status_id int;

		disable trigger task_status_INSTEAD_OF_INSERT on task_status;
		disable trigger task_status_AFTER_UPDATE on task_status;

		 MERGE task_status AS t USING (
		  select status_id,
		 (select new_item_id from #NewItems where item_id = parent_item_id) as parent_item_id, 
		 title, description, is_done, order_no
		from task_status
		where 
		parent_item_id in (select item_id from #NewItems )
		  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
		 parent_item_id, title, description, is_done, order_no ) VALUES ( 
		 parent_item_id, title, description, is_done, order_no  )  
		 OUTPUT s.status_id, inserted.status_id INTO #task_status;
		 
		 enable trigger task_status_INSTEAD_OF_INSERT on task_status ;
		 enable trigger task_status_AFTER_UPDATE on task_status ;

		 set @sql = ' UPDATE _' + @target_instance + '_task  SET status_id = (SELECT new_status_id FROM #task_status WHERE status_id = _' + @target_instance + '_task.status_id)';
		 exec (@sql)

IF (select count(process) from processes where instance = @target_instance AND process = 'evalution') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_evaluation SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_evaluation.parent_item_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_evaluation SET creator_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_evaluation.creator_item_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_evaluation SET evaluator_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_evaluation.evaluator_item_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'idea_matrix') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_idea_matrix SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_idea_matrix.parent_item_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'notification_tag_links') > 0
BEGIN

		 set @sql = ' UPDATE _' + @target_instance + '_notification_tag_links SET json_tag_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_notification_tag_links.json_tag_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_notification_tag_links SET item_column_id = (SELECT new_item_column_id FROM #NewItemColumns WHERE item_column_id = _' + @target_instance + '_notification_tag_links.item_column_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'request_more_info') > 0
BEGIN

		 set @sql = ' UPDATE _' + @target_instance + '_request_more_info SET parent_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_request_more_info.parent_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_request_more_info SET request_user_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_request_more_info.request_user_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_request_more_info SET request_submitter = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_request_more_info.request_submitter) ';
		 exec (@sql)

END

IF (select count(process) from processes where instance = @target_instance AND process = 'synpulse_rf_finance') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_finance SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_finance.parent_item_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'synpulse_rf_milestone') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_milestone SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_milestone.parent_item_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_milestone SET list_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_milestone.list_item_id) ';
		 exec (@sql)

		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_milestones SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_milestones.parent_item_id) ';
		 exec (@sql)
END

		 INSERT INTO condition_user_groups (condition_id, item_id) (select 
		 (select new_condition_id from #NewConditions WHERE condition_id = condition_user_groups.condition_id ),
		 (select new_item_id from #NewItems WHERE item_id = condition_user_groups.item_id )
		  from condition_user_groups where condition_id in (select condition_id from #NewConditions) 
		  and (select new_item_id from #NewItems WHERE item_id = condition_user_groups.item_id ) is not null
		  )

		set @sql = 'enable trigger _' + @target_instance +'_task_AFTER_UPDATE on _' + @target_instance + '_task';
		exec (@sql)
END
GO



SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_copy_process]	-- Add the parameters for the stored procedure here
	@source_instance nvarchar(10),
	@source_process nvarchar(50),
	@target_instance nvarchar(10),
	@target_process nvarchar(50),
	@is_instance_copy int = 0,
	@copy_data int = 0
	AS
BEGIN

	DECLARE @sql NVARCHAR(MAX) = '';
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @colName NVARCHAR(MAX);
	DECLARE @colName2 NVARCHAR(MAX);

	DECLARE @pk_tables TABLE ( table_name nvarchar(max) )
	DECLARE @all_tables TABLE ( level int, table_name nvarchar(max) )

	--DECLARE @sql_table TABLE ( sql nvarchar(max) )

	DECLARE @GetTables CURSOR
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	WITH rec AS (
                SELECT level=0, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
				 where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = 'processes' AND CONSTRAINT_TYPE = 'PRIMARY KEY')
                UNION ALL 
                    SELECT r.level+1, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                    FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
                     INNER JOIN rec r ON r.fk_table = tc_pk.TABLE_NAME 
					  where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = r.fk_table AND CONSTRAINT_TYPE = 'PRIMARY KEY')  and fk_table <> pk_table and fk_table <> 'processes'
					  )
					  select distinct pk_tables.table2 as table_name FROM 
                 ( SELECT fk_table, count(fk_table) as cnt
				  FROM rec 
				 group by fk_table  ) cnt,
( select tc.table_name table1, tc2.table_name table2 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc   
   inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc on tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
    inner join INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 on rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME ) pk_tables
	where cnt.fk_table = pk_tables.table2 AND pk_tables.table2 not in ('processes', 'conditions', 'item_tables', 'item_data', 'items', 'item_subitems', 'instance_menu_rights', 'instance_menu_states', 'process_portfolio_user_groups', 'attachments', 'item_data_process_selections',  'instance_user_configurations', 'instance_integration_keys', 'instance_user_configurations', 'instance_user_configurations_saved', 'worktime_tracking_users_tasks', 'worktime_tracking_hours', 'instance_comments', 'worktime_tracking_hours_breakdown')
	and substring(cnt.fk_table, 1, 1) <> '_'

	set @sql = ' CREATE  PROCEDURE [dbo].[app_copy_process_temp]	-- Add the parameters for the stored procedure here
						@source_instance nvarchar(10),
						@source_process nvarchar(50),
						@target_instance nvarchar(10),
						@target_process nvarchar(50),
						@copy_data int = 0 
						AS BEGIN
						exec app_set_archive_user_id 0 
			
			IF @source_instance = @target_instance		
			BEGIN
				INSERT INTO processes (instance, process, process_type, new_row_action_id, variable, list_process) (SELECT @target_instance, @target_process, process_type, new_row_action_id, variable, list_process FROM processes WHERE instance = @source_instance AND process = @source_process); 		
			END
			ELSE			
			BEGIN
				INSERT INTO processes (instance, process, process_type, new_row_action_id, variable, list_process) (SELECT @target_instance, @target_process, process_type, new_row_action_id, @target_process, list_process FROM processes WHERE instance = @source_instance AND process = @source_process); 
			END
			'
	
	--insert into @sql_table (sql) values (@sql)
	--print @sql

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

	WHILE (@@FETCH_STATUS=0) BEGIN
		--set @sql = ''
	
		INSERT INTO @pk_tables (table_name) values(@TableName)

		DECLARE @GetColumns CURSOR
		SET @GetColumns = CURSOR FAST_FORWARD LOCAL FOR 
		select column_name  FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		inner join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		where tc.table_name = @TableName AND CONSTRAINT_TYPE = 'PRIMARY KEY'

		-- print 'pks for ' + @TableName
		set @sql = @sql + ' DECLARE @' + @TableName + ' TABLE ( '
		--item_column_id int,  new_item_column_id int ) '

		OPEN @GetColumns
		FETCH NEXT FROM @GetColumns INTO @colName
			set @sql = @sql + '' + @colName + ' int, new_' + @colName + ' int'
		WHILE (@@FETCH_STATUS=0) BEGIN
		FETCH NEXT FROM @GetColumns INTO @colName
		END
		CLOSE @GetColumns
		set @sql = @sql + ' ) ' + CHAR(13)+CHAR(10) + ' DECLARE '

		set @sql = @sql + '@' + @colName + ' int, @new_' + @colName + ' int;'
		set @sql = @sql + CHAR(13)+CHAR(10)


		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
			set @sql = @sql + 'disable trigger ' + @TableName + '_INSTEAD_OF_INSERT on ' + @TableName + ';'  + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
			set @sql = @sql + 'disable trigger ' + @TableName + '_AFTER_UPDATE on ' + @TableName + ';'  + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		set @sql = @sql + ' MERGE ' + @TableName + ' AS t USING ( SELECT ' + @colName

		DECLARE @GetColumns2 CURSOR
		SET @GetColumns2 = CURSOR FAST_FORWARD LOCAL FOR
		select column_name  from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid', @colName)
		
		DECLARE @colNames AS NVARCHAR(MAX) = ''
		OPEN @GetColumns2
		FETCH NEXT FROM @GetColumns2 INTO @colName2
		WHILE (@@FETCH_STATUS=0) BEGIN
			-- set @sql = @sql + ', ' + @colName2  + CHAR(13)+CHAR(10)
			set @colNames = @colNames + ' ' + @colName2 + ','
		FETCH NEXT FROM @GetColumns2 INTO @colName2
		END

		set @colNames = substring(@colNames, 0, len(@colNames))

		DECLARE @colCnt AS INTEGER = 0
		DECLARE @pks1 AS NVARCHAR(MAX) = ''
		DECLARE @pks2 AS NVARCHAR(MAX) = ''
		DECLARE @pks3 AS NVARCHAR(MAX) = ''

		SELECT @colCnt = count(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName AND COLUMN_NAME = 'process'
		IF @colCnt > 0 
		BEGIN
			set @pks1 = ',@target_instance as instance, @target_process as process '
			set @pks2 = 'instance = @source_instance AND process = @source_process '
			set @pks3 = 'instance, process, '
		END
		
		IF @tableName = 'calendar_events' OR @tableName = 'calendar_data' OR @tableName = 'calendar_data_attributes' OR @tableName = 'calendar_static_holidays' 
		BEGIN 
			set @pks2 = ' calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @source_instance ) '
		END

		IF @tableName = 'calendars' OR @tableName = 'instance_integration_keys'
		BEGIN 
			set @pks1 = ',@target_instance as instance '
			set @pks2 = ' instance = @source_instance '
			set @pks3 = 'instance,  '
		END

		--IF @tableName = 'task_parents'
		--BEGIN 
		--	set @pks1 = ''
		--	set @pks2 = ' process_id IN (SELECT item_id FROM items WHERE instance = @source_instance and process = @source_process ) '
		--	set @pks3 = ''
		--END


		set @sql = @sql + ' ' + @pks1 + ', ' + @colNames + ' FROM ' + @tableName + ' WHERE 1=1 AND ' + @pks2 + ' ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( ' + CHAR(13)+CHAR(10)
		set @sql = @sql + @pks3 + @colNames + ' ) VALUES ( ' + CHAR(13)+CHAR(10)
		set @sql = @sql + @pks3 + @colNames + ' )  ' + CHAR(13)+CHAR(10)
		 set @sql = @sql + ' OUTPUT s.' + @colName + ', inserted.' + @colName + ' INTO @' + @TableName + CHAR(13)+CHAR(10) + ';'
		-- set @sql = @sql + ' OUTPUT s.' + @colName + ', @@identity INTO @' + @TableName + CHAR(13)+CHAR(10) + ';'


		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
			set @sql = @sql + ' enable trigger ' + @TableName + '_INSTEAD_OF_INSERT on ' + @TableName +' ;' + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
			set @sql = @sql + ' enable trigger ' + @TableName + '_AFTER_UPDATE on ' + @TableName +' ;' + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END
		
		FETCH NEXT FROM @GetTables INTO @TableName
	END;


	WITH rec2 AS (
                SELECT level=0, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
				 where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = 'processes' AND CONSTRAINT_TYPE = 'PRIMARY KEY')
                UNION ALL 
                    SELECT r.level+1, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                    FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
                     INNER JOIN rec2 r ON r.fk_table = tc_pk.TABLE_NAME 
					  where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = r.fk_table AND CONSTRAINT_TYPE = 'PRIMARY KEY') and fk_table <> pk_table and fk_table <> 'processes'
					  )
                  INSERT INTO @all_tables (level, table_name) SELECT count(fk_table) as cnt, fk_table
				  FROM rec2 
				 WHERE fk_table NOT IN (SELECT table_name FROM @pk_tables) AND fk_table not like 'condition%' 
				and fk_table NOT IN 
				('processes', 'item_tables','item_data','items','item_subitems', 'instance_menu_rights',  'instance_menu_states', 'process_portfolio_user_groups', 'attachments', 'item_data_process_selections', 				'allocation_durations', 'allocation_comments', 'task_durations', 'instance_user_configurations', 'instance_integration_keys', 'instance_user_configurations', 'instance_user_configurations_saved', 'worktime_tracking_users_tasks', 'worktime_tracking_hours', 'instance_comments', 'worktime_tracking_hours_breakdown') 
				and substring(fk_table, 1,1) <> '_' 
				group by fk_table  


	DECLARE @GetTables2 CURSOR
	SET @GetTables2 = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT table_name FROM @all_tables order by level

	OPEN @GetTables2
	FETCH NEXT FROM @GetTables2 INTO @tableName
	WHILE (@@FETCH_STATUS=0) BEGIN
		--set @sql = ''

		DECLARE @tableCnt AS INTEGER = 0
		
		select 
		@tableCnt = count(tc.table_name)
		--tc2.TABLE_NAME, ccu.COLUMN_NAME 
		from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 ON rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME
		where tc.TABLE_NAME = @tableName AND tc.CONSTRAINT_TYPE = 'FOREIGN KEY' AND tc2.table_name in (select table_name from @pk_tables)

		IF @tableCnt > 0
			BEGIN
				SET @sql = @sql + ' INSERT INTO ' + @tableName + ' (' + CHAR(13)+CHAR(10)

				DECLARE @tName NVARCHAR(MAX)
				DECLARE @cName NVARCHAR(MAX)
				DECLARE @cName2 NVARCHAR(MAX)
				DECLARE @GetCols CURSOR
				SET @GetCols = CURSOR FAST_FORWARD LOCAL FOR 
				select  tc2.TABLE_NAME, ccu.COLUMN_NAME  as column_name,  ccu2.COLUMN_NAME  as column_name2
				from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
				INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 ON rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu2 ON rc.UNIQUE_CONSTRAINT_NAME = ccu2.CONSTRAINT_NAME
				where tc.TABLE_NAME = @tableName AND tc.CONSTRAINT_TYPE = 'FOREIGN KEY' AND tc2.table_name in (select table_name from @pk_tables)

				DECLARE @fks TABLE ( colname nvarchar(MAX) )
				DECLARE @fks_cols AS NVARCHAR(MAX) = ''
				DECLARE @fks_ins AS NVARCHAR(MAX) = ''
				DECLARE @fks_where AS NVARCHAR(MAX) = '1=0 '
				DECLARE @fks_where2 AS NVARCHAR(MAX) = '1=1 '

				OPEN @GetCols
				FETCH NEXT FROM @GetCols INTO @tName, @cName, @cName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					INSERT INTO @fks (colname) values (@cName)
					SET @fks_cols = @fks_cols + @cName + ','
					SET @fks_ins = @fks_ins + ' (SELECT new_' + @cName2 + ' FROM @' + @tName + ' WHERE ' + @cName2 + ' = ' + @TableName + '.' + @cName + '),'
					SET @fks_where = @fks_where +' OR ( '  + @cName + ' IN (SELECT ' + @cName2 + ' FROM @' + @tName + ')     ) '  + CHAR(13)+CHAR(10)
					SET @fks_where2 = @fks_where2 +' AND (  (SELECT new_' + @cName2 + ' FROM @' + @tName + ' WHERE ' + @cName2 + ' = ' + @TableName + '.' + @cName + ') is not null   ) '  + CHAR(13)+CHAR(10)
				FETCH NEXT FROM @GetCols INTO @tName, @cName, @cName2
				END
				CLOSE @GetCols


				DECLARE @GetColumns3 CURSOR
				SET @GetColumns3 = CURSOR FAST_FORWARD LOCAL FOR
				select  column_name from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid') and column_name not in (select colname from @fks)
				and column_name not in ( select column_name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME=ccu.CONSTRAINT_NAME where tc.TABLE_NAME = @TableName and CONSTRAINT_TYPE = 'PRIMARY KEY' )
				and column_name not in ('process_tab_container_id', 'process_group_tab_id', 'process_portfolio_column_id')
		
				DECLARE @colNames2 AS NVARCHAR(MAX) = ''
				OPEN @GetColumns3
				FETCH NEXT FROM @GetColumns3 INTO @colName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					--set @sql = @sql + ' ' + @colName2  + ', ' + CHAR(13)+CHAR(10)
					set @colNames2 = @colNames2 + ' ' + @colName2 + ','
				FETCH NEXT FROM @GetColumns3 INTO @colName2
				END

				if @TableName = 'process_group_features'
				BEGIN
					set @colNames2 = @colNames2 + ' feature,' 
				END
				
				set @colNames2 = substring(@colNames2, 0, len(@colNames2)  )

				if LEN(@colNames2) = 0 
				BEGIN
					set @fks_cols = substring(@fks_cols, 0, len(@fks_cols)  )
					set @fks_ins = substring(@fks_ins, 0, len(@fks_ins)  )
				END

				set @sql = @sql + @fks_cols + @colNames2 + ') SELECT ' + @fks_ins + ' ' +@colNames2 + ' FROM ' + @TableName + ' WHERE (' + @fks_where + ' ) AND (' + @fks_where2 + ')'

				--update @sql_table set sql = concat(sql, @sql)
				--print @sql
			END
		ELSE
			BEGIN
				
				DECLARE @GetColumns4 CURSOR
				SET @GetColumns4 = CURSOR FAST_FORWARD LOCAL FOR
				select column_name from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid') and column_name not in (select colname from @fks)
				and column_name not in ( select column_name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME=ccu.CONSTRAINT_NAME where tc.TABLE_NAME = @TableName and CONSTRAINT_TYPE = 'PRIMARY KEY' )
		
				DECLARE @colNames3 AS NVARCHAR(MAX) = ''
				OPEN @GetColumns4
				FETCH NEXT FROM @GetColumns4 INTO @colName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					--set @sql = @sql + ' ' + @colName2  + ', ' + CHAR(13)+CHAR(10)
					set @colNames3 = @colNames3 + ' ' + @colName2 + ', '
				FETCH NEXT FROM @GetColumns4 INTO @colName2
				END			

				--if @TableName = 'process_translations'
				--BEGIN
				--	set @colNames3 = @colNames3 + ' code, variable,' 
				--END
				if @TableName = 'process_subprocesses'
				BEGIN
					set @colNames3 = @colNames3 + ' parent_process,' 
				END

				if @is_instance_copy = 1 AND @TableName <> 'process_subprocesses'
				BEGIN 
				if @TableName <> 'task_parents' and @TableName <> 'instance_user_configurations'
				BEGIN
					SET @sql = @sql + ' INSERT INTO ' + @tableName + ' (' + CHAR(13)+CHAR(10)
					set @sql = @sql + @colNames3 + ' instance, process) ( SELECT ' + @colNames3 + ' @target_instance, @target_process FROM ' + @tableName + ' WHERE instance = @source_instance AND process = @source_process )' + CHAR(13)+CHAR(10)
				END
				END

				--update @sql_table set sql = concat(sql, @sql)
				--print @sql
			END

		--INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no, read_only)
		--( SELECT 
		-- (SELECT new_process_container_id FROM @containers WHERE process_container_id = process_container_columns.process_container_id ),
		-- (SELECT new_item_column_id FROM @item_columns WHERE item_column_id = process_container_columns.item_column_id ),
		-- placeholder, validation, required, order_no, read_only 
		-- FROM process_container_columns 
		-- WHERE process_container_id IN (SELECT process_container_id FROM @containers) 
		-- OR  item_column_id IN (SELECT item_column_id FROM @item_columns)
		-- )

	FETCH NEXT FROM @GetTables2 INTO @tableName
	END
	
	--update @sql_table set sql = concat(sql, ' END ')

	--select top 1 @sql =sql from @sql_table
	--PRINT LEN(@sql)

	--set @sql = @sql + '
	--	 INSERT INTO process_group_features (process_group_id, feature, state) 
	--	 ( SELECT  distinct (SELECT new_process_group_id FROM @process_groups WHERE process_group_id = process_group_features.process_group_id),
	--	  feature, state
	--	FROM process_group_features
	--	WHERE process_group_id IN (SELECT process_group_id FROM @process_groups) ) '

	set @sql = @sql + '
		DECLARE @conditions TABLE ( condition_id int, new_condition_id int ) 
		 DECLARE @condition_id int, @new_condition_id int;
		
		disable trigger conditions_AFTER_UPDATE on conditions;
		disable trigger conditions_INSTEAD_OF_INSERT on conditions;

		 MERGE conditions AS t USING ( SELECT condition_id ,@target_instance as instance, @target_process as process ,  variable, condition_json FROM conditions WHERE instance = @source_instance AND process = @source_process  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
		instance, process,  variable, condition_json ) VALUES ( 
		instance, process,  variable, condition_json )  
		 OUTPUT s.condition_id, inserted.condition_id INTO @conditions; 
		 
		 enable trigger conditions_AFTER_UPDATE on conditions ;	
		 enable trigger conditions_INSTEAD_OF_INSERT on conditions ;	
		 '
		 
		 
	set @sql = @sql + '
		 INSERT INTO condition_containers (condition_id, process_container_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_containers.condition_id),
		  (SELECT new_process_container_id FROM @process_containers WHERE process_container_id = condition_containers.process_container_id)
		 FROM condition_containers
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_container_id IN (SELECT process_container_id FROM @process_containers) )  '
 		 
	set @sql = @sql + '
		 INSERT INTO condition_features (condition_id, feature) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_features.condition_id),
		  feature
		FROM condition_features
		WHERE condition_id IN (SELECT condition_id FROM @conditions) ) '

		 	set @sql = @sql + '
			INSERT INTO condition_groups (condition_id, process_group_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_groups.condition_id),
		  (SELECT new_process_group_id FROM @process_groups WHERE process_group_id = condition_groups.process_group_id)
		 FROM condition_groups
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_group_id IN (SELECT process_group_id FROM @process_groups) ) '

		 	set @sql = @sql + '
		 INSERT INTO condition_portfolios (condition_id, process_portfolio_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_portfolios.condition_id),
		  (SELECT new_process_portfolio_id FROM @process_portfolios WHERE process_portfolio_id = condition_portfolios.process_portfolio_id)
		 FROM condition_portfolios
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_portfolio_id IN (SELECT process_portfolio_id FROM @process_portfolios) ) '

		set @sql = @sql + '
		 INSERT INTO condition_states (condition_id, state) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_states.condition_id),
		  state
		 FROM condition_states
		 WHERE condition_id IN (SELECT condition_id FROM @conditions)  ) '

		set @sql = @sql + '
		 INSERT INTO condition_tabs (condition_id, process_tab_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_tabs.condition_id),
		  (SELECT new_process_tab_id FROM @process_tabs WHERE process_tab_id = condition_tabs.process_tab_id)
		 FROM condition_tabs
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_tab_id IN (SELECT process_tab_id FROM @process_tabs) ) '


		set @sql = @sql + '
		IF @source_instance = @target_instance
			BEGIN
			 INSERT INTO condition_user_groups (condition_id, item_id) 
			 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_user_groups.condition_id),
			  item_id
			 FROM condition_user_groups
			 WHERE condition_id IN (SELECT condition_id FROM @conditions)  ) 
		 END
		 '

		 set @sql = @sql + '
					UPDATE process_translations set variable = @target_process WHERE instance = @target_instance AND process = @target_process and variable = @source_process ;
		'

		 set @sql = @sql + '

 				DECLARE @ItemCols CURSOR
				SET @ItemCols = CURSOR FAST_FORWARD LOCAL FOR
				SELECT item_column_id, new_item_column_id FROM @item_columns

				OPEN @ItemCols
				FETCH NEXT FROM @ItemCols INTO @item_column_id, @new_item_column_id
				WHILE (@@FETCH_STATUS=0) BEGIN
					UPDATE conditions set condition_json = replace(condition_json, ''"ItemColumnId":'' + CAST(@item_column_id AS NVARCHAR) + '','', ''"ItemColumnId":'' + CAST(@new_item_column_id AS NVARCHAR) + '','') WHERE instance = @target_instance AND process = @target_process
					UPDATE conditions set condition_json = replace(condition_json, ''"ItemColumnId":'' + CAST(@item_column_id AS NVARCHAR) + ''}'', ''"ItemColumnId":'' + CAST(@new_item_column_id AS NVARCHAR) + ''}'') WHERE instance = @target_instance AND process = @target_process
					-- UPDATE process_translations set variable = substring(replace(variable, ''ic_'' + CAST(@item_column_id AS NVARCHAR) + ''_'', ''ic_'' + CAST(@new_item_column_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
					
					UPDATE item_columns 
						SET 
						-- variable = substring(replace(variable, ''ic_'' + CAST(@item_column_id AS NVARCHAR) + ''_'', ''ic_'' + CAST(@new_item_column_id AS NVARCHAR) + ''_''), 1, 50),
						relative_column_id = (select new_item_column_id FROM @item_columns WHERE item_column_id = relative_column_id)
					WHERE instance = @target_instance AND process = @target_process

					--UPDATE item_columns 
					--SET 
					--data_additional2 = (select new_item_column_id FROM @item_columns WHERE item_column_id = CAST(data_additional2 as int))
					--WHERE instance = @target_instance AND process = @target_process and data_type = 11
					
					UPDATE process_portfolio_columns 
					SET 
					archive_item_column_id = (select new_item_column_id FROM @item_columns WHERE item_column_id = archive_item_column_id)
					WHERE process_portfolio_id IN (select process_portfolio_id FROM process_portfolios WHERE instance = @target_instance AND process = @target_process)

				FETCH NEXT FROM  @ItemCols INTO @item_column_id, @new_item_column_id
				END		 '


		 set @sql = @sql + '
 				DECLARE @Conds CURSOR
				SET @Conds = CURSOR FAST_FORWARD LOCAL FOR
				SELECT condition_id, new_condition_id FROM @conditions

				OPEN @Conds
				FETCH NEXT FROM @Conds INTO @condition_id, @new_condition_id
				WHILE (@@FETCH_STATUS=0) BEGIN
					UPDATE conditions set condition_json = replace(condition_json, ''"ConditionId":'' + CAST(@condition_id AS NVARCHAR) + '','', ''"ConditionId":'' + CAST(@new_condition_id AS NVARCHAR) + '','') WHERE instance = @target_instance AND process = @target_process
					UPDATE conditions set condition_json = replace(condition_json, ''"ConditionId":'' + CAST(@condition_id AS NVARCHAR) + ''}'', ''"ConditionId":'' + CAST(@new_condition_id AS NVARCHAR) + ''}'') WHERE instance = @target_instance AND process = @target_process
					-- UPDATE process_translations set variable = substring(replace(variable, ''condition_'' + CAST(@condition_id AS NVARCHAR) + ''_'', ''condition_'' + CAST(@new_condition_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
					-- UPDATE conditions set variable = substring(replace(variable, ''condition_'' + CAST(@condition_id AS NVARCHAR) + ''_'', ''condition_'' + CAST(@new_condition_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
				FETCH NEXT FROM  @Conds INTO @condition_id, @new_condition_id
				END		 '

		 --set @sql = @sql + '
 		--		DECLARE @groups CURSOR
			--	SET @groups = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_group_id, new_process_group_id FROM @process_groups

			--	OPEN @groups
			--	FETCH NEXT FROM @groups INTO @process_group_id, @new_process_group_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''group_'' + CAST(@process_group_id AS NVARCHAR) + ''_'', ''group_:'' + CAST(@new_process_group_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_groups set variable = substring(replace(variable, ''group_'' + CAST(@process_group_id AS NVARCHAR) + ''_'', ''group_:'' + CAST(@new_process_group_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @groups INTO @process_group_id, @new_process_group_id
			--	END		 '

		 --set @sql = @sql + '
 		--		DECLARE @tabs CURSOR
			--	SET @tabs = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_tab_id, new_process_tab_id FROM @process_tabs

			--	OPEN @tabs
			--	FETCH NEXT FROM @tabs INTO @process_tab_id, @new_process_tab_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''tab_'' + CAST(@process_tab_id AS NVARCHAR) + ''_'', ''tab_'' + CAST(@new_process_tab_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_tabs set variable = substring(replace(variable, ''tab_'' + CAST(@process_tab_id AS NVARCHAR) + ''_'', ''tab_'' + CAST(@new_process_tab_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @tabs INTO @process_tab_id, @new_process_tab_id
			--	END		 '

		 --set @sql = @sql + '
 		--		DECLARE @pp CURSOR
			--	SET @pp = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_portfolio_id, new_process_portfolio_id FROM @process_portfolios

			--	OPEN @pp
			--	FETCH NEXT FROM @pp INTO @process_portfolio_id, @new_process_portfolio_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''port_'' + CAST(@process_portfolio_id AS NVARCHAR) + ''_'', ''port_'' + CAST(@new_process_portfolio_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_portfolios set variable = substring(replace(variable, ''port_'' + CAST(@process_portfolio_id AS NVARCHAR) + ''_'', ''port_'' + CAST(@new_process_portfolio_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @pp INTO @process_portfolio_id, @new_process_portfolio_id
			--	END	'

		 --set @sql = @sql + '
 		--		DECLARE @cc CURSOR
			--	SET @cc = CURSOR FAST_FORWARD LOCAL FOR
			--	SELECT process_container_id, new_process_container_id FROM @process_containers

			--	OPEN @cc
			--	FETCH NEXT FROM @cc INTO @process_container_id, @new_process_container_id
			--	WHILE (@@FETCH_STATUS=0) BEGIN
			--		UPDATE process_translations set variable = substring(replace(variable, ''cont_'' + CAST(@process_container_id AS NVARCHAR) + ''_'', ''cont_'' + CAST(@new_process_container_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--		UPDATE process_containers set variable = substring(replace(variable, ''cont_'' + CAST(@process_container_id AS NVARCHAR) + ''_'', ''cont_'' + CAST(@new_process_container_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
			--	FETCH NEXT FROM  @cc INTO @process_container_id, @new_process_container_id
			--	END	
		 --'

		 set @sql = @sql + '
			if @copy_data = 1 
			begin

				 DECLARE @items TABLE ( item_id int, new_item_id int ) 
				 DECLARE @item_id int, @new_item_id int;
				
				 disable trigger items_BeforeInsertRow on items;
				-- disable trigger items_AFTER_UPDATE on items;

				 MERGE items AS t USING ( SELECT item_id,  @target_instance as instance, @target_process as process, deleted, order_no FROM items WHERE instance = @source_instance AND process = @source_process) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
				 instance, process, deleted, order_no ) VALUES ( 
				 instance, process, deleted, order_no )  
				 OUTPUT s.item_id, inserted.item_id INTO @items;'
				 
				 set @sql = @sql + '
 				DECLARE @tableCols CURSOR
				SET @tableCols = CURSOR FAST_FORWARD LOCAL FOR
				SELECT name FROM item_columns WHERE instance = @source_instance AND process = @source_process and data_type in (0,1,2,3,4, 7) AND len(name) > 0

				DECLARE @tableColNames NVARCHAR(MAX) = '+char(39)+''+char(39)+'
				DECLARE @tableColName NVARCHAR(MAX)

				OPEN @tableCols
				FETCH NEXT FROM @tableCols INTO @tableColName
				WHILE (@@FETCH_STATUS=0) BEGIN
					set @tableColNames = @tableColNames + '+char(39)+'u.'+char(39)+' + @tableColName + '+char(39)+' = s.'+char(39)+' + @tableColName + '+char(39)+','+char(39)+'
				FETCH NEXT FROM  @tableCols INTO @tableColName
				END					 
				
				set @tableColNames = substring(@tableColNames, 0, len(@tableColNames)  ) '


				 set @sql = @sql + '
				 declare @sql2 nvarchar(max) = '+char(39)+''+char(39)+'
				 set @sql2 = @sql2 + '+char(39)+'
				update u
				set '+char(39)+' + @tableColNames + '+char(39)+'
				from _'+char(39)+' + @target_instance + '+char(39)+'_'+char(39)+' + @target_process + '+char(39)+' u 
				inner join #NewItems2 i on u.item_id = i.new_item_id
				inner join _'+char(39)+' + @source_instance + '+char(39)+'_'+char(39)+' + @source_process + '+char(39)+' s on i.item_id = s.item_id '+char(39)+' ;

				IF OBJECT_ID('+char(39)+'tempdb..#NewItems2'+char(39)+') IS NOT NULL DROP TABLE #NewItems2 ;
				CREATE table #NewItems2 (item_id int, new_item_id int ) ;
				
				INSERT INTO #NewItems2 (item_id, new_item_id) SELECT item_id, new_item_id FROM @items ;

				IF LEN(@tableColNames) > 0 EXEC (@sql2) ;
				'

				 set @sql = @sql + '
				 enable trigger items_BeforeInsertRow on items ;
				-- enable trigger items_AFTER_UPDATE on items ;

				 if @source_instance = @target_instance
				 begin 
					INSERT INTO item_subitems (parent_item_id, item_id, favourite) (SELECT (SELECT new_item_id FROM @items WHERE item_id = i.parent_item_id), item_id, favourite FROM item_subitems i WHERE parent_item_id IN (SELECT item_id FROM @items) )
					INSERT INTO item_subitems (parent_item_id, item_id, favourite) (SELECT parent_item_id, (SELECT new_item_id FROM @items WHERE item_id = i.item_id), favourite FROM item_subitems i WHERE item_id IN (SELECT item_id FROM @items) )

					INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) 
					(SELECT 
						(SELECT new_item_id FROM @items WHERE item_id = i.item_id), 
						(SELECT new_item_column_id FROM @item_columns WHERE item_column_id = i.item_column_id),
						selected_item_id 
						FROM item_data_process_selections i WHERE item_id IN (SELECT item_id FROM @items)
					)

					INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) 
					(SELECT 
						item_id,
						(SELECT new_item_column_id FROM @item_columns WHERE item_column_id = i.item_column_id),
						(SELECT new_item_id FROM @items WHERE item_id = i.selected_item_id )
						FROM item_data_process_selections i WHERE selected_item_id IN (SELECT item_id FROM @items)
					)
						
				 end



			end
		 '
		 
		 if @is_instance_copy = 1
		 BEGIN 
			set @sql = @sql + ' INSERT INTO #NewItemColumns (item_column_id, new_item_column_id) (SELECT item_column_id, new_item_column_id FROM @item_columns)'
			set @sql = @sql + ' INSERT INTO #NewItems (item_id, new_item_id) (SELECT item_id, new_item_id FROM @items)'
			set @sql = @sql + ' INSERT INTO #NewPortfolios (process_portfolio_id, new_process_portfolio_id) (SELECT process_portfolio_id, new_process_portfolio_id FROM @process_portfolios)'
			set @sql = @sql + ' INSERT INTO #NewTabs (process_tab_id, new_process_tab_id) (SELECT process_tab_id, new_process_tab_id FROM @process_tabs)'
			set @sql = @sql + ' INSERT INTO #NewConditions (condition_id, new_condition_id) (SELECT condition_id, new_condition_id FROM @conditions)'
		 END

		 set @sql = @sql + '
		 END
		 '

	--set @sql = @sql + ' END '
	--PRINT ' END '

	print LEN(@sql)

	
	--DECLARE @CurrentEnd BIGINT; /* track the length of the next substring */
	--DECLARE @offset tinyint; /*tracks the amount of offset needed */
	--set @sql = replace(  replace(@sql, char(13) + char(10), char(10))   , char(13), char(10))
	
	--WHILE LEN(@sql) > 1
	--BEGIN
	--	IF CHARINDEX(CHAR(10), @sql) between 1 AND 4000
	--	BEGIN
	--		   SET @CurrentEnd =  CHARINDEX(char(10), @sql) -1
	--		   set @offset = 2
	--	END
	--	ELSE
	--	BEGIN
	--		   SET @CurrentEnd = 4000
	--			set @offset = 1
	--	END   
	--	PRINT SUBSTRING(@sql, 1, @CurrentEnd) 
	--	set @sql = SUBSTRING(@sql, @CurrentEnd+@offset, LEN(@sql))   
	--END

	exec (@sql)

	set @sql = ' exec app_copy_process_temp ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @source_process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @target_process +char(39)+',' + cast(@copy_data as nvarchar)
	exec (@sql)
	--print @sql


	if @is_instance_copy <> 1 
	BEGIN 
		SET @sql = 'drop procedure app_copy_process_temp'
		exec (@sql)
	END

END
GO



ALTER TABLE [dbo].[processes]  WITH CHECK ADD  CONSTRAINT [FK_process_instance] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])



GO 


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[condition_VarReplaceProc]
(
 @sourceInstance NVARCHAR(10),
 @targetInstance NVARCHAR(10),
 @condition_json NVARCHAR(MAX),
 @replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		
			if @var4 is not null 
			begin
						SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)
			end

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
				END

			IF dbo.IsListColumn(dbo.itemColumn_GetId(@sourceInstance, @var3))  > 0
				BEGIN					
					DECLARE @idCursor CURSOR 
					DECLARE @listItemId INT 
					DECLARE @value nvarchar(max)
					DECLARE @id int
					DECLARE @process nvarchar(50)

					SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
					SELECT item_id FROM dbo.get_ListColumnIds(dbo.itemColumn_GetId(@sourceInstance, @var3));

					select @process = data_additional FROM item_columns WHERE instance = @sourceInstance and item_column_id = dbo.itemColumn_GetId(@sourceInstance, @var3)

					OPEN @idCursor
					FETCH NEXT FROM @idCursor INTO @listItemId
					WHILE (@@FETCH_STATUS=0) BEGIN

						print 'get id'
						print @listItemId

						EXEC list_GetValue @listItemId, @value = @value output
						print @targetInstance
						print @process
						print @value
										
						print 'EXEC list_GetId ' + @targetInstance + ', ' + @process + ', ' + @value + ', ' + ' @id = @id output '
						EXEC list_GetId @targetInstance, @process, @value, @id = @id output
						set @condition_json = replace(@condition_json, '"Value":"' + @value + '"}', '"Value":"' + CAST(@id as nvarchar) + '"}')

						FETCH NEXT FROM @idCursor INTO @listItemId
					END
				END
		  CONTINUE  
	END



	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
				END

		  CONTINUE  
	END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)

			SET @var4 = dbo.condition_GetId(@targetInstance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'ConditionId":' + @var4)

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2+LEN(@var4))
				END

		  CONTINUE  
	END
 SET @replaced_json = @condition_json
END
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[conditions_replace]
(
@sourceInstance nvarchar(10)
--,@targetInstance nvarchar(10)
)
AS
BEGIN
	DECLARE @condition_id INT
	DECLARE @instance nvarchar(10)
	DECLARE @process nvarchar(50)
	DECLARE @VARIABLE nvarchar(255)
	DECLARE @condition_json nvarchar(max)

	DECLARE @conditionCursor CURSOR 
	SET @conditionCursor = CURSOR  FAST_FORWARD LOCAL FOR 
	SELECT condition_id, instance, process, variable, condition_json FROM conditions
	-- WHERE instance IN (@sourceInstance, @targetInstance)
	WHERE instance = @sourceInstance

	OPEN @conditionCursor
	FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	WHILE (@@FETCH_STATUS=0) BEGIN
		declare @json as nvarchar(max)
		declare @json2 as nvarchar(max)
		exec dbo.condition_IdReplaceProc @condition_json,  @replaced_json = @json output
		exec dbo.condition_VarReplaceProc @sourceInstance, @sourceInstance, @json,  @replaced_json = @json2 output
		--print '@json1'
	--	print @json
		--print '@json2'
	--	print @json2
		insert into #conditions (condition_id, instance, process, variable, condition_json, replaced_condition_json) values (@condition_id, @instance, @process, @variable, @json, @json2)
		FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	END
END
GO

