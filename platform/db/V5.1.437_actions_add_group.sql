--ALTER TABLE dbo.archive_process_action_rows
--ADD action_group_id INTEGER

--ALTER TABLE dbo.archive_process_action_rows
--ADD order_no [nvarchar](255) NULL 

ALTER TABLE dbo.process_action_rows
ADD action_group_id INTEGER

ALTER TABLE dbo.process_action_rows
ADD order_no [nvarchar](255) NULL
  
ALTER TABLE dbo.process_action_rows
  DROP CONSTRAINT DF_process_action_rows_value_type
  
ALTER TABLE dbo.process_action_rows
  ALTER COLUMN value_type INT NULL

--ALTER TABLE dbo.archive_process_action_rows
  --DROP CONSTRAINT DF__archive_p__value__3A6282ED
  
--ALTER TABLE dbo.archive_process_action_rows
  --ALTER COLUMN value_type INT NULL

CREATE TABLE [dbo].[process_action_rows_groups]( 
  [process_action_group_id] [int] IDENTITY(1,1) NOT NULL, 
--   [process_action_id] [int] NOT NULL,
--   FOREIGN KEY(process_action_id) REFERENCES process_actions(process_action_id),
  [type] [int] NOT NULL,
  [instance] [nvarchar](10) NOT NULL, 
  [process] [nvarchar](50) NOT NULL, 
  [variable] [nvarchar](255) NULL,
  [process_in_group] [nvarchar](255) NULL,
  [in_use] [int] NULL,
  [order_no] [nvarchar](255) NULL, 

  CONSTRAINT [PK_process_action_groups] PRIMARY KEY CLUSTERED  
( 
  [process_action_group_id] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
GO


EXEC app_remove_archive 'process_action_rows'

