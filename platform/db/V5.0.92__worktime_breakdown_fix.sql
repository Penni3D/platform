DROP TABLE [dbo].[worktime_tracking_hours_breakdown]
GO

BEGIN
    CREATE TABLE [dbo].[worktime_tracking_hours_breakdown](
        [id] [int] NOT NULL IDENTITY (1,1),
        [worktime_tracking_hour_id] [int] NOT NULL,
        [hours] [float] NULL DEFAULT 0,
        [additional_hours] [float] NULL DEFAULT 0,
        [type] [int] NOT NULL,
        [list_item_id] [int] NULL,
        CONSTRAINT [PK_worktime_tracking_hours_breakdown] UNIQUE CLUSTERED ([worktime_tracking_hour_id], [list_item_id]),
        CONSTRAINT [FK_worktime_tracking_hours_item] FOREIGN KEY ([worktime_tracking_hour_id]) REFERENCES [dbo].[worktime_tracking_hours] (id)
    )
END