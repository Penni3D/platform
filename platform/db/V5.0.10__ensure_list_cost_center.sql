IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_list_cost_center'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_list_cost_center] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_list_cost_center]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'list_cost_center'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;
	DECLARE @tableName NVARCHAR(50) = ''
	SET @tableName = '_' + @instance + '_' + @process
	DECLARE @sql NVARCHAR(MAX) = ''


	--Create process
	EXEC app_ensure_list @instance, @process, @process, 0

	EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'owner', 0, 6, 1, 1, 0, null,  0, 0, 'user'
	EXEC dbo.app_ensure_column @instance,@process, 'location_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'function_id', 0, 1, 1, 0, 0, NULL, 0, 0	


	--New User process Tab
	DECLARE @g2 INT
	SELECT @g2 = process_group_id FROM process_groups WHERE instance = @instance AND process = 'user' AND variable = 'USER';

	DECLARE @c1 INT
	SELECT @c1 = ISNULL(process_container_id, 0) FROM process_containers WHERE instance = @instance AND process = 'user' AND variable = 'ORGANISATION_MANAGEMENT';

	IF (@c1 IS NULL)
		BEGIN
			--Create new User process tab
			DECLARE @t2 INT
			EXEC app_ensure_tab @instance,'user','USER_TAB2', @g2, @tab_id = @t2 OUTPUT

			--Create new User process container	
			EXEC app_ensure_container @instance,'user','ORGANISATION_MANAGEMENT', @t2, 1, @container_id = @c1 OUTPUT
		END

	--Create cost center column
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, 'user', 'cost_center', @c1, 6, 1, 1, 0, null,  0, 0, 'list_cost_center', @item_column_id = @i1 OUTPUT

	-- User process new process fields
	DECLARE @c2 INT
	SELECT @c2 = ISNULL(process_container_id, 0) FROM process_containers WHERE instance = @instance AND process = 'user' AND variable = 'USER_NEW_PROCESS_2';

	IF (@c2 IS NULL)
	BEGIN

		--Create new User process container
		EXEC app_ensure_container @instance,'user','USER_NEW_PROCESS_2', 0, 1, @container_id = @c2 OUTPUT

		-- Assign container to new process dialog
		INSERT INTO process_portfolio_dialogs (instance, process, process_container_id, order_no, variable) VALUES (@instance, 'user', @c2, 'W', null)

	END


	-- Assing Cost center field to new process container
	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no) VALUES (@c2, @i1, null, '{"max":"1"}', 1, 'W');


	DECLARE @function_id NVARCHAR(50)
	DECLARE @location_id NVARCHAR(50)
	SELECT @function_id = item_id FROM items WHERE process = 'list_function'
	SELECT @location_id = item_id FROM items WHERE process = 'list_location'


	INSERT INTO items (instance, process) VALUES (@instance, @process);
	DECLARE @itemId INT = @@identity;
	SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Default cost center'', location_id = ' + @location_id + ', function_id = ' + @function_id + ' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
	EXEC (@sql)
END
