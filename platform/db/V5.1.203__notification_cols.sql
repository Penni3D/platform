﻿DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
    exec app_ensure_column @instance, 'notification', 'httprequest_type', 0, 1, 0, 0, '', '', 0, 0
    exec app_ensure_column @instance, 'notification', 'httprequest_url', 0, 0, 0, 0, '', '', 0, 0
    exec app_ensure_column @instance, 'notification', 'httprequest_header', 0, 4, 0, 0, '', '', 0, 0
    exec app_ensure_column @instance, 'notification', 'httprequest_body', 0, 4, 0, 0, '', '', 0, 0
    FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor




CREATE TABLE [dbo].[instance_http_requests] (
  [instance_http_request_id] int  IDENTITY(1,1) NOT NULL,
  [instance] nvarchar(10) NULL,
  [request_date] datetime NULL,
  [user_item_id] INT NULL,
  [process_item_id] INT NULL,
  [process_action_row_id] INT NULL,
  [httprequest_item_id] INT NULL,
  [httprequest_type] nvarchar(6) NULL,  
  [httprequest_status_code] nvarchar(max) NULL,
  [url] nvarchar(max) NULL,
  [header] nvarchar(max) NULL,
  [body] nvarchar(max) NULL,
  [response] nvarchar(max) NULL
)
GO

ALTER TABLE [dbo].[instance_http_requests] ADD CONSTRAINT [instance_http_requests_instance] FOREIGN KEY ([instance]) REFERENCES [dbo].[instances] ([instance]) ON DELETE CASCADE ON UPDATE NO ACTION
GO