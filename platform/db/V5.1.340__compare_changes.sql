SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processActionRows_ValueIdReplace]
(
	@item_column_id INT,
	@process_action_type INT,
	@value_type INT,
	@type INT,
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @result NVARCHAR(MAX)
	DECLARE @data_type INT
	DECLARE @item_id INT
	DECLARE @guid uniqueidentifier
	DECLARE @tab_id INT
	DECLARE @variable NVARCHAR(255)
	DECLARE @column_id INT
	DECLARE @column_name NVARCHAR(50)

	SET @result = @value

	IF @process_action_type IN (0, 8) AND @value_type = 0 AND @type = 1 BEGIN
		SELECT @data_type = data_type FROM item_columns WHERE item_column_id = @item_column_id
		IF @data_type IN (5, 6) BEGIN
			IF SUBSTRING(@value, 1, 1) = '[' BEGIN
				IF ISNUMERIC(SUBSTRING(@value, 2, LEN(@value) - 2)) = 1 BEGIN
					SET @item_id = CAST(SUBSTRING(@value, 2, LEN(@value) - 2) AS INT)
					SELECT @guid = guid FROM items WHERE item_id = @item_id
					IF @guid IS NOT NULL BEGIN
						SET @result = @guid
					END
					ELSE BEGIN
						SET @result = '[]'
					END
				END
			END
		END
		
		ELSE BEGIN
			SELECT @column_name = name FROM item_columns WHERE item_column_id = @item_column_id
			IF @column_name = 'current_state' BEGIN
				IF SUBSTRING(@value, 1, 4) = 'tab_' BEGIN
					IF ISNUMERIC(SUBSTRING(@value, 5, LEN(@value) - 4)) = 1 BEGIN
						SET @tab_id = CAST(SUBSTRING(@value, 5, LEN(@value) - 4) AS INT)
						SET @variable = dbo.processTab_GetVariable(@tab_id)
						IF @variable IS NOT NULL BEGIN
							SET @result = @variable
						END
						ELSE BEGIN
							SET @result = ''
						END
					END
				END
			END
		END
	END

	IF @process_action_type = 5	BEGIN
		IF @type = 1 BEGIN
			IF ISNUMERIC(@value) = 1 AND NOT @value = '0' BEGIN 
				SET @item_id = CAST(@value AS INT)
				SELECT @guid = guid FROM items WHERE item_id = @item_id
				IF @guid IS NOT NULL BEGIN
					SET @result = @guid
				END
				ELSE BEGIN
					SET @result = '0'
				END
			END
		END
		
		ELSE IF @type = 2 BEGIN
			IF SUBSTRING(@value, 1, 1) = '[' BEGIN
				IF ISNUMERIC(SUBSTRING(@value, 2, LEN(@value) - 2)) = 1 BEGIN
					SET @column_id = CAST(SUBSTRING(@value, 2, LEN(@value) - 2) AS INT)
					SET @variable = dbo.itemColumn_GetVariable(@column_id)
					IF @variable IS NOT NULL BEGIN
						SET @result = @variable
					END
					ELSE BEGIN
						SET @result = 'null'
					END
				END
			END
		END

		ELSE IF @type = 3 BEGIN
			IF SUBSTRING(@value, 1, 12) = 'process.tab_' BEGIN
				IF ISNUMERIC(SUBSTRING(@value, 13, LEN(@value) - 12)) = 1 BEGIN
					SET @tab_id = CAST(SUBSTRING(@value, 13, LEN(@value) - 12) AS INT)
					SET @variable = dbo.processTab_GetVariable(@tab_id)
					IF @variable IS NOT NULL BEGIN
						SET @result = @variable
					END
					ELSE BEGIN
						SET @result = ''
					END
				END
			END
		END
	END

	RETURN @result
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processActionRows_ValueVarReplace]
(
	@instance NVARCHAR(MAX),
	@item_column_id INT,
	@process_action_type INT,
	@value_type INT,
	@type INT,
	@value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @result NVARCHAR(MAX)
	DECLARE @data_type INT
	DECLARE @item_id INT
	DECLARE @guid uniqueidentifier
	DECLARE @tab_id INT
	DECLARE @variable NVARCHAR(255)
	DECLARE @column_id INT
	DECLARE @column_name NVARCHAR(50)

	SET @result = @value

	IF @process_action_type IN (0, 8) AND @value_type = 0 AND @type = 1 BEGIN
		SELECT @data_type = data_type FROM item_columns WHERE item_column_id = @item_column_id
		IF @data_type IN (5, 6) BEGIN
			IF @value IS NOT NULL AND NOT SUBSTRING(@value, 1, 1) = '[' BEGIN
				SET @guid = @value
				SELECT @item_id = item_id FROM items WHERE guid = @guid
				IF @item_id IS NOT NULL BEGIN
					SET @result = '[' + CAST(@item_id AS NVARCHAR) + ']'
				END
				ELSE BEGIN
					SET @result = '[]'
				END
			END
		END

		ELSE BEGIN
			SELECT @column_name = name FROM item_columns WHERE item_column_id = @item_column_id
			IF @column_name = 'current_state' BEGIN
				IF NOT @value = '' BEGIN
					SET @tab_id = dbo.processTab_GetId(@instance, @value)
					IF @tab_id IS NOT NULL BEGIN
						SET @result = 'tab_' + CAST(@tab_id AS NVARCHAR)
					END
					ELSE BEGIN
						SET @result = ''
					END
				END
			END
		END
	END

	IF @process_action_type = 5	BEGIN
		IF @type = 1 BEGIN
			IF @value IS NOT NULL AND NOT @value = '0' BEGIN
				SET @guid = @value
				SELECT @item_id = item_id FROM items WHERE guid = @guid
				IF @item_id IS NOT NULL BEGIN
					SET @result = CAST(@item_id AS NVARCHAR)
				END
				ELSE BEGIN
					SET @result = '0'
				END
			END
		END
		
		ELSE IF @type = 2 BEGIN
			IF NOT @value = 'null' BEGIN
				SET @column_id = dbo.itemColumn_GetId(@instance, @value)
				IF @column_id IS NOT NULL BEGIN
					SET @result = '[' + CAST(@column_id AS NVARCHAR) + ']'
				END
				ELSE BEGIN
					SET @result = 'null'
				END
			END
		END

		ELSE IF @type = 3 BEGIN
			IF NOT @value = '' BEGIN
				SET @tab_id = dbo.processTab_GetId(@instance, @value)
				IF @tab_id IS NOT NULL BEGIN
					SET @result = 'process.tab_' + CAST(@tab_id AS NVARCHAR)
				END
				ELSE BEGIN
					SET @result = ''
				END
			END
		END
	END

	RETURN @result
END