DELETE FROM process_container_columns WHERE process_container_column_id IN (
SELECT pcc.process_container_column_id FROM process_container_columns pcc
LEFT JOIN item_columns as ic ON ic.item_column_id = pcc.item_column_id
LEFT JOIN process_containers pc ON pc.process_container_id = pcc.process_container_id
WHERE ic.process <> pc.process)