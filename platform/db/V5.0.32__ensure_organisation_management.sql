IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_organisation_management'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_organisation_management] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_organisation_management]  -- Add the parameters for the stored procedure here
    @instance NVARCHAR(10)
AS
  BEGIN

    --Archive user is 0
    DECLARE @BinVar VARBINARY(128);
    SET @BinVar = CONVERT(VARBINARY(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_process_list_function @instance
    EXEC app_ensure_process_list_location @instance
    EXEC app_ensure_process_list_cost_center @instance
    EXEC app_ensure_process_organisation @instance
    EXEC app_ensure_process_competency @instance
  END
