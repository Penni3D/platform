BEGIN

 DECLARE @ContainerId INT 

   
 SET @ContainerId = (SELECT process_container_id from process_containers
  WHERE variable = 'cont_user_right_container_1' and process = 'user')
  
  
   DECLARE @FieldId1 INT  = ( SELECT item_column_id from item_columns where process = 'user' AND name = 'first_day_of_week')
  DECLARE @FieldId2 INT = ( SELECT item_column_id from item_columns where process = 'user' AND name = 'date_format')
   DECLARE @FieldId3 INT = ( SELECT item_column_id from item_columns where process = 'user' AND name = 'locale_id')
   
   DECLARE @FirstExists INT = ( SELECT process_container_column_id from process_container_columns WHERE item_column_id = @FieldId1 AND process_container_id = @ContainerId )
   DECLARE @SecondExists INT =( SELECT process_container_column_id from process_container_columns WHERE item_column_id = @FieldId2 AND process_container_id = @ContainerId )
     DECLARE @ThirdExists INT =( SELECT process_container_column_id from process_container_columns WHERE item_column_id = @FieldId3 AND process_container_id = @ContainerId )
   
   
					IF @FirstExists IS NULL
                    BEGIN
						INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, order_no) VALUES(@ContainerId, @FieldId1, '', '{"ReadOnly":0,"Required":0}', 'U')
					END
					
					IF @SecondExists IS NULL
                    BEGIN
						INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, order_no) VALUES(@ContainerId, @FieldId2, '', '{"ReadOnly":0,"Required":0}', 'W')
					END
  
					IF @ThirdExists IS NULL
                    BEGIN
						INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, order_no) VALUES(@ContainerId, @FieldId3, '', '{"ReadOnly":0,"Required":0}', 'X')
					END
  
  
	
END