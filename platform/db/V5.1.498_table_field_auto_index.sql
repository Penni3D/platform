DECLARE @ItemColumnId INT
DECLARE @Instance NVARCHAR(255)
DECLARE @Process  NVARCHAR(255)
DECLARE @DataAdditional2  NVARCHAR(MAX)
DECLARE @sql NVARCHAR(MAX)
DECLARE @StartPos INT
DECLARE @EndPos INT
DECLARE @ValueLen INT
DECLARE @Value NVARCHAR(255)
DECLARE @TabName NVARCHAR(255)
DECLARE @ColName NVARCHAR(255)
CREATE TABLE #indexes (index_name SYSNAME, index_description VARCHAR(210), index_keys NVARCHAR(2078))
DECLARE table_cursor CURSOR FOR SELECT item_column_id, instance, process, data_additional2 FROM item_columns WHERE data_type IN (18, 21)
OPEN table_cursor
FETCH NEXT FROM table_cursor INTO @ItemColumnId, @Instance, @process, @DataAdditional2
WHILE @@fetch_status = 0
	BEGIN
		IF @DataAdditional2 IS NOT NULL
			BEGIN
				SET @StartPos = CHARINDEX('"itemColumnId"', @DataAdditional2)
				IF @StartPos = 0
					BEGIN
						SET @StartPos = CHARINDEX('"ownerColumnId"', @DataAdditional2) + 1
					END
				IF @StartPos > 1
					BEGIN
						SET @StartPos = @startPos + 15
						SET @EndPos = CHARINDEX(',', @DataAdditional2, @startPos)
						IF @EndPos = 0
							BEGIN
								SET @EndPos = CHARINDEX('}', @DataAdditional2, @startPos)
							END
						SET @Value = SUBSTRING(@DataAdditional2, @startPos, @EndPos - @startPos)
						IF ISNUMERIC(@VALUE) = 1
							BEGIN
								IF (SELECT COUNT(*) FROM item_columns WHERE data_type < 5 AND item_column_id = CAST(@Value AS INT)) > 0
									BEGIN
										SELECT @TabName = it.[name], @ColName = ic.[name] FROM item_columns ic INNER JOIN item_tables it ON it.instance = ic.instance AND it.process = ic.process WHERE ic.item_column_id = CAST(@Value AS INT)
										INSERT INTO #indexes EXEC sp_helpindex @TabName
										IF NOT EXISTS (SELECT * FROM #indexes WHERE index_keys = @ColName)
											AND NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TabName + '_' + @ColName + '_nonunique')
											AND @ColName IS NOT NULL
											AND @ColName <> ''
											BEGIN
												UPDATE item_columns SET unique_col = 2 WHERE item_column_id = CAST(@Value AS INT)
											END
										TRUNCATE TABLE #indexes
									END
							END
					END

				SET @StartPos = CHARINDEX('"parentFieldName"', @DataAdditional2)
				IF @StartPos > 0
					BEGIN
					SET @StartPos = @startPos + 19
						SET @EndPos = CHARINDEX('"', @DataAdditional2, @startPos)
						IF @EndPos = 0
							BEGIN
								SET @EndPos = CHARINDEX('}', @DataAdditional2, @startPos)
							END
						SET @Value = SUBSTRING(@DataAdditional2, @startPos, @EndPos - @startPos)
						IF @Value <> ''
							BEGIN
								IF (SELECT COUNT(*) FROM item_columns WHERE data_type < 5 AND instance = @Instance AND process = @Process AND [name] = @Value) > 0
									BEGIN
										SELECT @TabName = it.[name], @ColName = @Value FROM item_columns ic INNER JOIN item_tables it ON it.instance = ic.instance AND it.process = ic.process WHERE ic.instance = @Instance AND ic.process = @Process AND  ic.[name] = @Value
										INSERT INTO #indexes EXEC sp_helpindex @TabName
										IF NOT EXISTS (SELECT * FROM #indexes WHERE index_keys = @ColName)
											AND NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TabName + '_' + @ColName + '_nonunique')
											AND @ColName IS NOT NULL
											AND @ColName <> ''
											BEGIN
												UPDATE item_columns SET unique_col = 2 WHERE instance = @Instance AND process = @Process AND  [name] = @Value
											END
										TRUNCATE TABLE #indexes
									END
							END
					END
			END

		FETCH NEXT FROM table_cursor INTO @ItemColumnId, @Instance, @process, @DataAdditional2
	END

DROP TABLE #indexes
DEALLOCATE table_cursor



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[AfterUpdateColumn] ON [dbo].[item_columns] AFTER UPDATE AS
DECLARE @GetChanges CURSOR
DECLARE @TableName NVARCHAR(255)
DECLARE @OldColumnName NVARCHAR(255)
DECLARE @ColumnName NVARCHAR(50)
DECLARE @TableColumnName NVARCHAR(MAX)
DECLARE @DataType TINYINT
DECLARE @UseFk TINYINT
DECLARE @Unique TINYINT
DECLARE @sql NVARCHAR(MAX)
DECLARE @DataAdditional2 NVARCHAR(MAX)
DECLARE @Instance NVARCHAR(MAX)
DECLARE @Process NVARCHAR(MAX)
DECLARE @StartPos INT
DECLARE @EndPos INT
DECLARE @ValueLen INT
DECLARE @Value NVARCHAR(255)
DECLARE @TabName NVARCHAR(255)
DECLARE @ColName NVARCHAR(255)

CREATE TABLE #indexes (index_name SYSNAME, index_description VARCHAR(210), index_keys NVARCHAR(2078))

SET @GetChanges = CURSOR FAST_FORWARD LOCAL FOR
	SELECT it.[name] AS TableName, i.[name] AS ColumnName, i.data_type, d.[name] AS OldColumnName, i.use_fk, i.unique_col, i.data_additional2, i.instance, i.process
	FROM item_tables it
	INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
	INNER JOIN deleted d ON i.item_column_id = d.item_column_id

OPEN @GetChanges
FETCH NEXT FROM @GetChanges INTO @TableName, @ColumnName, @DataType, @OldColumnName, @UseFk, @Unique, @DataAdditional2, @Instance, @Process
WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @ColumnName IS NOT NULL AND @OldColumnName IS NULL AND LEN(@ColumnName) > 0
			BEGIN
				EXEC dbo.items_AddColumn @TableName, @ColumnName, @DataType
			END
		ELSE
			BEGIN
				IF @ColumnName <> @OldColumnName
					BEGIN
						SET @TableColumnName = @TableName + '.' + @OldColumnName
						EXEC sp_rename @TableColumnName, @ColumnName, 'COLUMN'
					END
			END

		IF @UseFk = 1
			BEGIN
				IF NOT EXISTS(SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']')) AND @ColumnName IS NOT NULL AND @ColumnName <> ''
					BEGIN
						SET @sql = 'ALTER TABLE ' + @TableName + ' WITH CHECK ADD  CONSTRAINT FK_' + @TableName+ '_' + @ColumnName + ' FOREIGN KEY(' + @ColumnName + ') REFERENCES [dbo].[items] ([item_id])'
						EXEC (@sql)
					END
			END
		ELSE
			BEGIN
				IF EXISTS(SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @ColumnName + ']'))
					BEGIN
						SET @sql = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT FK_' + @TableName+ '_' + @ColumnName
						EXEC (@sql)
					END
			END

		IF @Unique = 1
			BEGIN
				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_nonunique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_nonunique ON ' + @TableName
						EXEC (@sql)
					END

				IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_unique') AND @ColumnName IS NOT NULL AND @ColumnName <> ''
					BEGIN
						SET @sql = 'CREATE UNIQUE INDEX IX_' + @TableName + '_' + @ColumnName + '_unique ON ' + @TableName + '(' + @ColumnName + ')'
						EXEC (@sql)
					END
			END
		ELSE IF @Unique = 2
			BEGIN
				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_unique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_unique ON ' + @TableName
						EXEC (@sql)
					END

				IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_nonunique') AND @ColumnName IS NOT NULL AND @ColumnName <> ''
					BEGIN
						SET @sql = 'CREATE INDEX IX_' + @TableName + '_' + @ColumnName + '_nonunique ON ' + @TableName + '(' + @ColumnName + ')'
						EXEC (@sql)
					END
			END
		ELSE
			BEGIN
				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_unique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_unique ON ' +  @TableName
						EXEC (@sql)
					END

				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @ColumnName + '_nonunique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @ColumnName + '_nonunique ON ' + @TableName
						EXEC (@sql)
					END
			END

		IF @ColumnName <> @OldColumnName
			BEGIN
				IF EXISTS(SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('[dbo].[FK_' + @TableName+ '_' + @OldColumnName + ']'))
					BEGIN
						SET @sql = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT FK_' + @TableName+ '_' + @OldColumnName
						EXEC (@sql)
					END

				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @OldColumnName + '_unique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @OldColumnName + '_unique ON ' + @TableName
						EXEC (@sql)
					END

				IF EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TableName + '_' + @OldColumnName + '_nonunique')
					BEGIN
						SET @sql = 'DROP INDEX IX_' + @TableName + '_' + @OldColumnName + '_nonunique ON ' + @TableName
						EXEC (@sql)
					END
			END

		IF @DataType IN (18, 21)
			BEGIN
				SET @StartPos = CHARINDEX('"itemColumnId"', @DataAdditional2)
				IF @StartPos = 0
					BEGIN
						SET @StartPos = CHARINDEX('"ownerColumnId"', @DataAdditional2) + 1
					END
				IF @StartPos > 1
					BEGIN
						SET @StartPos = @startPos + 15
						SET @EndPos = CHARINDEX(',', @DataAdditional2, @startPos)
						IF @EndPos = 0
							BEGIN
								SET @EndPos = CHARINDEX('}', @DataAdditional2, @startPos)
							END
						SET @Value = SUBSTRING(@DataAdditional2, @startPos, @EndPos - @startPos)
						IF ISNUMERIC(@VALUE) = 1
							BEGIN
								IF (SELECT COUNT(*) FROM item_columns WHERE data_type < 5 AND item_column_id = CAST(@Value AS INT)) > 0
									BEGIN
										SELECT @TabName = it.[name], @ColName = ic.[name] FROM item_columns ic INNER JOIN item_tables it ON it.instance = ic.instance AND it.process = ic.process WHERE ic.item_column_id = CAST(@Value AS INT)
										INSERT INTO #indexes EXEC sp_helpindex @TabName
										IF NOT EXISTS (SELECT * FROM #indexes WHERE index_keys = @ColName)
											AND NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TabName + '_' + @ColName + '_nonunique')
											AND @ColName IS NOT NULL
											AND @ColName <> ''
											BEGIN
												UPDATE item_columns SET unique_col = 2 WHERE item_column_id = CAST(@Value AS INT)
												SET @sql = 'CREATE INDEX IX_' + @TabName + '_' + @ColName + '_nonunique ON ' + @TabName + '(' + @ColName + ')'
												EXEC (@sql)
											END
										TRUNCATE TABLE #indexes
									END
							END
					END

				SET @StartPos = CHARINDEX('"parentFieldName"', @DataAdditional2)
				IF @StartPos > 0
					BEGIN
					SET @StartPos = @startPos + 19
						SET @EndPos = CHARINDEX('"', @DataAdditional2, @startPos)
						IF @EndPos = 0
							BEGIN
								SET @EndPos = CHARINDEX('}', @DataAdditional2, @startPos)
							END
						SET @Value = SUBSTRING(@DataAdditional2, @startPos, @EndPos - @startPos)
						IF @Value <> ''
							BEGIN
								IF (SELECT COUNT(*) FROM item_columns WHERE data_type < 5 AND instance = @Instance AND process = @Process AND [name] = @Value) > 0
									BEGIN
										SELECT @TabName = it.[name], @ColName = @Value FROM item_columns ic INNER JOIN item_tables it ON it.instance = ic.instance AND it.process = ic.process WHERE ic.instance = @Instance AND ic.process = @Process AND  ic.[name] = @Value
										INSERT INTO #indexes EXEC sp_helpindex @TabName
										IF NOT EXISTS (SELECT * FROM #indexes WHERE index_keys = @ColName)
											AND NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_' + @TabName + '_' + @ColName + '_nonunique')
											AND @ColName IS NOT NULL
											AND @ColName <> ''
											BEGIN
												UPDATE item_columns SET unique_col = 2 WHERE instance = @Instance AND process = @Process AND  [name] = @Value
												SET @sql = 'CREATE INDEX IX_' + @TabName + '_' + @ColName + '_nonunique ON ' + @TabName + '(' + @ColName + ')'
												EXEC (@sql)
											END
										TRUNCATE TABLE #indexes
									END
							END
					END
			END

		FETCH NEXT FROM @GetChanges INTO @TableName, @ColumnName, @DataType, @OldColumnName, @UseFk, @Unique, @DataAdditional2, @Instance, @Process
	END
DROP TABLE #indexes