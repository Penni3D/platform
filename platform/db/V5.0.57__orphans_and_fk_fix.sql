EXEC app_set_archive_user_id 1
DELETE FROM process_comments WHERE instance NOT IN (SELECT instance FROM instances)
DELETE FROM process_comments WHERE process NOT IN (SELECT process FROM processes)
DELETE FROM process_comments WHERE sender NOT IN (SELECT item_id FROM items WHERE process = 'user')
DELETE FROM process_comments WHERE item_id NOT IN (SELECT item_id FROM items)

DELETE FROM instance_comments WHERE instance NOT IN (SELECT instance FROM instances)
DELETE FROM instance_comments WHERE sender NOT IN (SELECT item_id FROM items WHERE process = 'user')

DELETE FROM instance_configurations WHERE instance NOT IN (SELECT instance FROM instances)
DELETE FROM instance_configurations WHERE process NOT IN (SELECT process FROM processes)

DELETE FROM instance_user_configurations WHERE instance NOT IN (SELECT instance FROM instances)
DELETE FROM instance_user_configurations WHERE user_id NOT IN (SELECT item_id FROM items WHERE process = 'user')

DELETE FROM instance_user_configurations_saved WHERE instance NOT IN (SELECT instance FROM instances)
DELETE FROM instance_user_configurations WHERE user_id NOT IN (SELECT item_id FROM items WHERE process = 'user')

DECLARE @sql NVARCHAR(MAX);

SELECT @sql = 'ALTER TABLE instance_user_configurations_saved ' + 'DROP CONSTRAINT ' + name + ';'
    FROM sys.key_constraints
    WHERE [type] = 'PK'
    AND [parent_object_id] = OBJECT_ID('instance_user_configurations_saved');

EXEC sp_executeSQL @sql;

ALTER TABLE instance_user_configurations_saved
ADD user_configuration_id INT IDENTITY(1, 1) PRIMARY KEY

ALTER TABLE instance_configurations
ADD PRIMARY KEY (instance_configuration_id)

ALTER TABLE instance_user_configurations_saved
ADD FOREIGN KEY (user_id)
REFERENCES items (item_id)

ALTER TABLE instance_comments
ADD FOREIGN KEY (instance)
REFERENCES instances (instance)

ALTER TABLE instance_comments
ADD FOREIGN KEY (sender)
REFERENCES items (item_id)

ALTER TABLE instance_configurations
ADD FOREIGN KEY (instance)
REFERENCES instances (instance)

ALTER TABLE instance_configurations
ADD FOREIGN KEY (process, instance)
REFERENCES processes (process, instance)

ALTER TABLE process_comments
ADD FOREIGN KEY (instance)
REFERENCES instances (instance)

ALTER TABLE process_comments
ADD FOREIGN KEY (instance)
REFERENCES instances (instance)

ALTER TABLE process_comments
ALTER COLUMN process nvarchar(50) NOT NULL

ALTER TABLE process_comments
ADD FOREIGN KEY (process, instance)
REFERENCES processes (process, instance)

ALTER TABLE process_comments
ADD FOREIGN KEY (item_id)
REFERENCES items (item_id)

ALTER TABLE process_comments
ADD FOREIGN KEY (sender)
REFERENCES items (item_id)