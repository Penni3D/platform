IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_copy_container'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_copy_container] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_copy_container]
	@instance NVARCHAR(10),	@process NVARCHAR(50), @from_process_container_id INT, @to_process_tab_id INT, @to_container_side INT, @new_container_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @i int = 0
	
	DECLARE @numrows INT = (SELECT COUNT(*) FROM process_container_columns WHERE process_container_id = @from_process_container_id)
	IF @numrows > 0 
	BEGIN		
		-- Create new container
		INSERT INTO process_containers (instance, process, rows, cols) VALUES (@instance, @process, 1, 1)
		SET @new_container_id = @@identity

		-- Insert new container to tab
		DECLARE @new_container_order_no NVARCHAR(MAX)
		SELECT @new_container_order_no = order_no FROM process_tab_containers WHERE process_container_id = @from_process_container_id ORDER BY order_no COLLATE Latin1_General_bin ASC
		SET @new_container_order_no = @new_container_order_no + 'V'
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@to_process_tab_id, @new_container_id, @to_container_side, @new_container_order_no)

		-- Insert old container columns to new container
		DECLARE @item_column_id INT, @placeholder NVARCHAR(255), @validation NVARCHAR(MAX), @required INT, @order_no NVARCHAR(MAX), @read_only TINYINT
		
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no, read_only)
		SELECT @new_container_id, item_column_id, placeholder, validation, required, order_no, read_only FROM process_container_columns WHERE process_container_id = @from_process_container_id ORDER BY order_no COLLATE Latin1_General_bin ASC
    END
END
