CREATE PROCEDURE [dbo].[app_ensure_table_process]
  @instance nvarchar(10), @Process nvarchar(50)
AS
BEGIN
 
 DECLARE @LanguageVariable NVARCHAR(255)
 DECLARE @LanguageVariable2 NVARCHAR(255)
 DECLARE @LanguageVariable3 NVARCHAR(255)
 DECLARE @ContainerId INT
 
 SET @ContainerId = (SELECT TOP 1 process_container_id FROM process_containers WHERE process = @Process AND instance = @instance ORDER BY process_container_id DESC)
 SET @LanguageVariable = 'ic_' + @Process + '_parent_item_id_rc'
 SET @LanguageVariable2 = 'ic_' + @Process + '_owner_item_id_rc'
  SET @LanguageVariable3 = 'ic_' + @Process + '_task_type_rc'
  
  
	INSERT INTO item_columns (instance, process, name, data_type, system_column, variable, input_method, input_name, data_additional, data_additional2) 
		values (@instance, @process, 'parent_item_id', 1, 0, @LanguageVariable, null, null, null, null)
		
	DECLARE @Ic INT = @@identity;	
		
	INSERT INTO process_container_columns (process_container_id, validation, item_column_id, order_no) VALUES(@ContainerId, '{"ReadOnly":0,"Required":0}', @Ic, 0)


	INSERT INTO item_columns (instance, process, name, data_type, system_column, variable, input_method, input_name, data_additional, data_additional2) 
		values (@instance, @process, 'owner_item_id', 1, 0, @LanguageVariable2, null, null, null, null)	
		
	DECLARE @Ic2 INT = @@identity;
   INSERT INTO process_container_columns (process_container_id, validation, item_column_id, order_no) VALUES(@ContainerId, '{"ReadOnly":0,"Required":0}', @Ic2, 0)
   
   	INSERT INTO item_columns (instance, process, name, data_type, system_column, variable, input_method, input_name, data_additional, data_additional2) 
		values (@instance, @process, 'task_type', 1, 0, @LanguageVariable3, null, null, null, null)
   	DECLARE @Ic3 INT = @@identity;
 
  INSERT INTO process_container_columns (process_container_id, validation, item_column_id, order_no) VALUES(@ContainerId, '{"ReadOnly":0,"Required":0}', @Ic3, 0)
  
  
  DECLARE @Language NVARCHAR(10);

		DECLARE language_c CURSOR 
		FOR SELECT 
			code from instance_languages

			OPEN language_c
		
			FETCH NEXT FROM language_c INTO @Language
				WHILE @@FETCH_STATUS = 0
				BEGIN
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @Process, @Language, @LanguageVariable, 'parent_item_id')
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @Process, @Language, @LanguageVariable2, 'owner_item_id')
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @Process, @Language, @LanguageVariable3, 'task_type')
				FETCH NEXT FROM language_c INTO
			@Language
				END;
			CLOSE language_c;
			DEALLOCATE language_c;
			
			
END