﻿ALTER TABLE items
    ADD created_on DATETIME DEFAULT(GetUTCDate())
GO
ALTER TABLE items
    ADD created_by INT DEFAULT(0)
GO
ALTER TRIGGER [dbo].[items_BeforeInsertRow] ON [dbo].[items] INSTEAD OF INSERT AS
    BEGIN
        INSERT INTO items (instance, process, deleted, created_by)
        SELECT instance, process, deleted, convert(int,convert(varbinary(4),CONTEXT_INFO()))
        FROM Inserted
    END
GO
exec app_set_archive_user_id 1
delete from process_translations where variable = 'NOTIFICATION_SENDER'