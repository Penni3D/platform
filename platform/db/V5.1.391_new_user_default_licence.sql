DECLARE @Sql NVARCHAR(255);
DECLARE @Sql2 NVARCHAR(255);
DECLARE @Instance NVARCHAR(255); 
DECLARE @YesValue INT
DECLARE @NoValue INT
DECLARE @ParmDefinition nvarchar(500);
DECLARE @ParmDefinition2 nvarchar(500);
SET @Instance = (SELECT instance from instances)
DECLARE @Sql3 NVARCHAR(MAX);


SET @Sql3 = '
EXEC app_set_archive_user_id 1
DECLARE @YesValue INT = (SELECT item_id from _' + @Instance + '_list_user_licence_required where list_item = ''Yes'');
DECLARE @NoValue INT = (SELECT item_id from _' + @Instance + '_list_user_licence_required where list_item = ''No'');
declare @licence_column_ID INT = (SELECT item_column_id FROM item_columns WHERE process = ''user'' AND name = ''licence_required'');
SELECT @licence_column_ID;



IF  (SELECT COUNT(process_action_id) from process_action_rows where process_action_id = (
	SELECT after_new_row_action_id FROM processes WHERE process = ''user''
		) AND item_column_id = (SELECT item_column_id AS LRid
	FROM item_columns WHERE process = ''user'' AND
	name = ''licence_required'' )) > 0
BEGIN
		
		INSERT INTO item_data_process_selections (item_id, item_column_id , selected_item_id) 
		SELECT item_id,@licence_column_ID, @YesValue FROM _' + @instance + '_user WHERE item_id != 1 AND email NOT LIKE ''%@keto%'' AND (SELECT COUNT(item_id) FROM item_data_process_selections 
		idps WHERE item_id = _'+ @instance + '_user.item_id AND item_column_id = @licence_column_ID) = 0
		
		INSERT INTO item_data_process_selections (item_id, item_column_id , selected_item_id) 
		SELECT item_id,@licence_column_ID, @NoValue FROM _' + @instance + '_user WHERE item_id = 1 AND (SELECT COUNT(item_id) FROM item_data_process_selections 
		idps WHERE item_id = _' + @instance + '_user.item_id AND item_column_id = @licence_column_ID) = 0
		OR email LIKE ''%@keto%'' AND (SELECT COUNT(item_id) FROM item_data_process_selections 
		idps WHERE item_id = _' + @instance + '_user.item_id AND item_column_id = @licence_column_ID) = 0
		
		RETURN;
END	
	IF (SELECT after_new_row_action_id FROM processes WHERE process = ''user'' ) IS NULL
	BEGIN
		INSERT INTO process_actions (instance, process, variable)
		VALUES (''' + @instance + ''', ''user'', ''action_user_new_user'')
		SELECT @@identity
		UPDATE processes 
		SET after_new_row_action_id = @@identity
		WHERE process = ''user''
	END
		INSERT INTO process_action_rows (process_action_id, process_action_type, item_column_id,
		value_type, value)
		VALUES ( (SELECT after_new_row_action_id FROM processes WHERE process = ''user''),
				 0,
				 (SELECT item_column_id from item_columns where process = ''user'' AND variable = ''IC_USER_LICENCE_REQUIRED'' AND data_type = 6 AND data_additional = ''list_user_licence_required''),
				 0,
				 ''['' + (SELECT CAST(item_id AS nvarchar(max)) from _' + @instance + '_list_user_licence_required where list_item = ''Yes'')  + '']''); 
		
		INSERT INTO item_data_process_selections (item_id, item_column_id , selected_item_id) 
		SELECT item_id, @licence_column_ID, @YesValue FROM _' + @instance + '_user WHERE item_id != 1 AND email NOT LIKE ''%@keto%'' AND (SELECT COUNT(item_id) FROM item_data_process_selections 
		idps WHERE item_id = _' + @instance + '_user.item_id AND item_column_id = @licence_column_ID) = 0

		INSERT INTO item_data_process_selections (item_id, item_column_id , selected_item_id) 
		SELECT item_id, @licence_column_ID, @NoValue FROM _' + @instance + '_user WHERE item_id = 1 AND (SELECT COUNT(item_id) FROM item_data_process_selections 
		idps WHERE item_id = _' + @instance + '_user.item_id AND item_column_id = @licence_column_ID) = 0
		OR email LIKE ''%@keto%'' AND (SELECT COUNT(item_id) FROM item_data_process_selections 
		idps WHERE item_id = _' + @instance + '_user.item_id AND item_column_id = @licence_column_ID) = 0 
';

EXECUTE SP_EXECUTESQL @Sql3

