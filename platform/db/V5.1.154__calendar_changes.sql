ALTER TABLE calendar_events
ALTER COLUMN event_start DATE NULL

ALTER TABLE calendar_events
ALTER COLUMN event_end DATE NULL

ALTER TABLE calendars
ADD parent_calendar_id INT NULL FOREIGN KEY REFERENCES calendars(calendar_id)

ALTER TABLE calendars
ADD ensure_date DATE NULL

ALTER TABLE calendars
ADD DEFAULT 0 FOR base_calendar

ALTER TABLE calendars
ADD use_base_calendar TINYINT DEFAULT 1

ALTER TABLE calendar_events
ADD event_type INT NULL DEFAULT 0

ALTER TABLE calendar_data
ALTER COLUMN event_date DATE NOT NULL

ALTER TABLE calendar_static_holidays
ALTER COLUMN name NVARCHAR(255) NULL

ALTER TABLE calendar_static_holidays
ALTER COLUMN month TINYINT NULL

ALTER TABLE calendar_static_holidays
ALTER COLUMN day TINYINT NULL



GO



DROP TABLE calendar_data_attributes
DROP TABLE archive_calendar_data_attributes

UPDATE calendars SET use_base_calendar = 0 WHERE base_calendar = 1
UPDATE calendars SET use_base_calendar = 1 WHERE base_calendar = 0

EXEC app_remove_archive calendar_events
EXEC app_ensure_archive calendar_events

EXEC app_remove_archive calendars
EXEC app_ensure_archive calendars

EXEC app_remove_archive calendar_data

EXEC app_remove_archive calendar_static_holidays
EXEC app_ensure_archive calendar_static_holidays



GO



ALTER PROCEDURE Calendar_ModifyEvent
	@calendarId INT, @calendarEventId INT, @dayOff TINYINT = 0, @holiday TINYINT = 0, @notWork TINYINT = 1
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @eventStartDate DATE
	DECLARE @eventEndDate DATE
	DECLARE @currentDate DATE

	DELETE FROM calendar_data WHERE calendar_event_id = @calendarEventId
	SELECT @eventStartDate = event_start, @eventEndDate = event_end FROM calendar_events WHERE calendar_event_id = @calendarEventId

	SET @currentDate = @eventStartDate
	WHILE (@currentDate <= @eventEndDate)
	BEGIN
		INSERT INTO calendar_data(calendar_id, calendar_event_id, event_date, dayoff, holiday, notwork) 
		VALUES(@calendarId, @calendarEventId, @currentDate, @dayOff, @holiday, @notWork)
		
		SET @currentDate = DATEADD(DAY, 1, @currentDate)
	END	
END;



GO



CREATE PROCEDURE Calendar_ModifyStaticHoliday
	@calendarId INT, @calendarStaticHolidayId INT, @startDate DATE, @endDate DATE
AS
BEGIN
	DECLARE @name VARCHAR(MAX)
	DECLARE @month INT
	DECLARE @day INT
	DECLARE @yearStart INT
	DECLARE @yearEnd INT
	DECLARE @holidayToAdd DATE

	DELETE FROM calendar_data WHERE calendar_static_holiday_id = @calendarStaticHolidayId AND event_date BETWEEN @startDate AND @endDate	
	SELECT @day = [day], @month = [month] FROM calendar_static_holidays WHERE calendar_static_holiday_id = @calendarStaticHolidayId
	
	SET @yearStart = YEAR(@startDate)
	SET @yearEnd = YEAR(@endDate)

	WHILE @yearStart <= @yearEnd
	BEGIN
		SET @holidayToAdd =
			CAST(CAST(@yearStart AS VARCHAR(4)) +
			RIGHT('0' + CAST(@month AS VARCHAR(2)), 2) +
			RIGHT('0' + CAST(@day AS VARCHAR(2)), 2) 
		AS DATE)

		IF(@holidayToAdd >= @startDate AND @holidayToAdd <= @endDate)
		BEGIN
			INSERT INTO calendar_data(calendar_id, calendar_static_holiday_id, event_date, dayoff, holiday, notwork) VALUES(@calendarId, @calendarStaticHolidayId, @holidayToAdd, 0, 1, 1)
		END
		SET @yearStart = @yearStart + 1
	END
END



GO



CREATE PROCEDURE Calendar_CheckWeekends
	@calendar_id INT, @startDate DATE, @endDate DATE
AS
BEGIN
	DECLARE @RC INT
	BEGIN TRANSACTION

	Exec @RC = sp_getapplock @Resource='CheckDayOffs', @LockMode='Exclusive', @LockOwner='Transaction', @LockTimeout = 15000
				
	DECLARE @weekday_str VARCHAR(15) = NULL
	SET @weekday_str =			
		(SELECT RIGHT(CASE def_str WHEN '' THEN NULL ELSE def_str END, len(CASE def_str WHEN '' THEN NULL ELSE def_str END)-1) FROM (
		SELECT def_str = 
			CASE WHEN def_1 = 1 THEN ',1' ELSE '' END 
			+ CASE WHEN def_2 = 1 THEN ',2' ELSE '' END
			+ CASE WHEN def_3 = 1 THEN ',3' ELSE '' END
			+ CASE WHEN def_4 = 1 THEN ',4' ELSE '' END
			+ CASE WHEN def_5 = 1 THEN ',5' ELSE '' END
			+ CASE WHEN def_6 = 1 THEN ',6' ELSE '' END
			+ CASE WHEN def_7 = 1 THEN ',7' ELSE '' END
			FROM calendars
			WHERE calendar_id = @calendar_id AND use_base_calendar <> 1
	) AS q)

	IF @weekday_str IS NOT NULL
	BEGIN
		EXEC Calendar_AddWeekends @calendar_id, @startDate, @endDate, @weekday_str
	END
	
	UPDATE calendars SET ensure_date = @endDate WHERE calendar_id = @calendar_id
	COMMIT TRANSACTION
END



GO



CREATE PROCEDURE Calendar_CheckCalendar
	@calendarId INT, @startDate DATE = NULL, @endDate DATE = NULL
AS
BEGIN
	IF @startDate IS NULL
	BEGIN
		SELECT @startDate = MIN(event_date) FROM calendar_data;

		IF @startDate IS NULL
		BEGIN
			SET @startDate = DATEADD(YEAR, -3, GETDATE())
		END
	END

	EXEC Calendar_CheckStaticHolidays @calendarId, @startDate, @endDate
	EXEC Calendar_CheckWeekends @calendarId, @startDate, @endDate
END



GO



ALTER PROCEDURE Calendar_CheckStaticHolidays
	@calendarId INT, @startDate DATE, @endDate DATE
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @calendarStaticHolidayId INT

	DECLARE GetStaticHolidays CURSOR FAST_FORWARD LOCAL FOR 
		SELECT calendar_static_holiday_id FROM calendar_static_holidays WHERE calendar_id = @calendarId

	OPEN GetStaticHolidays
	FETCH NEXT FROM GetStaticHolidays INTO @calendarStaticHolidayId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC Calendar_ModifyStaticHoliday @calendarId, @calendarStaticHolidayId, @startDate, @endDate

		FETCH NEXT FROM GetStaticHolidays INTO @calendarStaticHolidayId
	END
	
	CLOSE GetStaticHolidays
END;



GO



CREATE PROCEDURE Calendar_ApplyToPastWeekends
	@calendar_id INT
AS
BEGIN
	DECLARE @RC INT
	BEGIN TRANSACTION
	DECLARE @startDate DATE = GETDATE();
	DECLARE @endDate DATE = GETDATE();
	DECLARE @useBase INT = 1;

	EXEC @RC = sp_getapplock @Resource='CheckDayOffs', @LockMode='Exclusive', @LockOwner='Transaction', @LockTimeout = 15000
	
	SELECT @useBase = use_base_calendar FROM calendars WHERE calendar_id = @calendar_id;
	IF @useBase = 0
	BEGIN
		SELECT @startDate = MIN(event_date) FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL;
		DELETE FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL AND event_date < @endDate;
		EXEC Calendar_CheckWeekends @calendar_id, @startDate, @endDate
	END
	ELSE
	BEGIN
		DELETE FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL AND event_date < @endDate;
	END
	
	COMMIT TRANSACTION
END



GO



CREATE PROCEDURE Calendar_ApplyToFutureWeekends
	@calendar_id INT
AS
BEGIN
	DECLARE @RC INT
	BEGIN TRANSACTION
	DECLARE @startDate DATE = GETDATE();

	EXEC @RC = sp_getapplock @Resource='CheckDayOffs', @LockMode='Exclusive', @LockOwner='Transaction', @LockTimeout = 15000

	DELETE FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL AND event_date >= @startDate;		
	UPDATE calendars SET ensure_date = @startDate WHERE calendar_id = @calendar_id		
	COMMIT TRANSACTION
END



GO



CREATE PROCEDURE Calendar_AddWeekends
	@calendar_id INT, @start_date DATE, @end_date DATE, @weekdays VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @weekday INT 
	DECLARE @day INT
	DECLARE @i INT;	DECLARE @p INT
	DECLARE @n INT;	DECLARE @l INT
	DECLARE @d DATE;
	
	SET @weekday = DATEPART(dw, @start_date)-1;IF @weekday = 0 SET @weekday = 7
	SET @p = 0;SET @n = 1
	
	DELETE FROM calendar_data WHERE calendar_event_id IS NULL AND calendar_static_holiday_id IS NULL AND calendar_id = @calendar_id AND event_date >= @start_date AND event_date <= @end_date

	IF len(@weekdays) > 0
	BEGIN 
		WHILE (@n > 0)
		BEGIN
			/* Split weekdays string and loop through */
			SET @n = CHARINDEX(',', @weekdays, @p + 1)
			SET @l = CASE WHEN @n > 0 THEN @n ELSE LEN(@weekdays) + 1 END - @p - 1
			SET @day = CONVERT(INT, SUBSTRING(@weekdays, @p + 1, @l))
			SET @p = @n;
			SET @i = 0;
			
			/* Calculate difference */
			SET @day = ABS((@weekday - @day) - 7)
			IF @day > 6 SET @day = @day - 7
			 
			/* While days to add, insert */
			WHILE (@end_date >= DATEADD(day, @i * 7 + @day, @start_date))
			BEGIN
				SET @d = DATEADD(day, @day + (@i * 7), @start_date)
				INSERT INTO calendar_data(calendar_id, event_date, dayoff, holiday, notWork) VALUES(@calendar_id, @d, 1, 0, 1)
				SET @i = @i + 1
			END
		END
	END
END;



GO



CREATE FUNCTION GetCombinedCalendarForUser
	(@user_item_id INT, @start DATE, @end DATE)
RETURNS TABLE 
AS
RETURN 
(
	SELECT * FROM GetCombinedCalendar((SELECT parent_calendar_id FROM calendars WHERE item_id = @user_item_id),
		(SELECT calendar_id FROM calendars WHERE item_id = @user_item_id), @start, @end)
)



GO



CREATE TRIGGER calendar_events_AFTER_UPDATE_ENSURE_EVENTS ON calendar_events
AFTER UPDATE
AS
	DECLARE @startDate DATE
	DECLARE @endDate DATE
	DECLARE @calendarEventId INT
	DECLARE @calendarId INT
	DECLARE @eventType INT;

	DECLARE @dayOff INT;
	DECLARE @holiday INT;
	DECLARE @notWork INT;

	DECLARE calendarCursor CURSOR FAST_FORWARD LOCAL FOR
	SELECT i.calendar_event_id, i.calendar_id, i.event_type FROM inserted i INNER JOIN deleted d ON d.calendar_event_id = i.calendar_event_id WHERE (i.event_start <> d.event_start) OR (i.event_end <> d.event_end)

	OPEN calendarCursor
	FETCH NEXT FROM calendarCursor INTO @calendarEventId, @calendarId, @eventType
	WHILE @@fetch_status = 0
	BEGIN
		SET @dayOff = 
			CASE
				WHEN @eventType = 2 THEN 1 ELSE 0
			END

		SET @holiday = 
			CASE
				WHEN @eventType = 0 THEN 1 ELSE 0
			END

		SET @notWork = 
			CASE
				WHEN @eventType IN (0, 2) THEN 1 ELSE 0
			END

		EXEC Calendar_ModifyEvent @calendarId, @calendarEventId, @dayOff, @holiday, @notWork
		FETCH NEXT FROM calendarCursor INTO @calendarEventId, @calendarId, @eventType
	END
	CLOSE calendarCursor
	DEALLOCATE calendarCursor

	
	
GO



CREATE TRIGGER calendar_static_holidays_AFTER_UPDATE_ENSURE_HOLIDAYS ON calendar_static_holidays
AFTER UPDATE
AS
	DECLARE @calendarStaticHolidayId INT
	DECLARE @calendarId INT
	DECLARE @startDate DATE
	DECLARE @endDate DATE

	DECLARE calendarCursor CURSOR FAST_FORWARD LOCAL FOR
	SELECT i.calendar_static_holiday_id, i.calendar_id FROM inserted i INNER JOIN deleted d ON d.calendar_static_holiday_id = i.calendar_static_holiday_id WHERE (i.day <> d.day) OR (i.month <> d.month)

	OPEN calendarCursor
	FETCH NEXT FROM calendarCursor INTO @calendarStaticHolidayId, @calendarId
	WHILE @@fetch_status = 0
	BEGIN
		SELECT @startDate = MIN(event_date), @endDate = MAX(event_date) FROM calendar_data WHERE calendar_static_holiday_id = @calendarStaticHolidayId
		EXEC Calendar_ModifyStaticHoliday @calendarId, @calendarStaticHolidayId, @startDate, @endDate
		FETCH NEXT FROM calendarCursor INTO @calendarStaticHolidayId, @calendarId
	END
	CLOSE calendarCursor
	DEALLOCATE calendarCursor

	

GO



DECLARE @instanceName NVARCHAR(MAX)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor 
FETCH NEXT FROM instanceCursor INTO @instanceName
WHILE @@fetch_status = 0
    BEGIN
	DECLARE @tableName NVARCHAR(MAX) = '_' + @instanceName + '_user'
	DECLARE @sql NVARCHAR(MAX)

	SET @sql = 'UPDATE calendars SET item_id = (SELECT item_id FROM ' + @tableName + ' WHERE calendar_id = calendars.calendar_id) WHERE (SELECT item_id FROM ' + @tableName + ' WHERE calendar_id = calendars.calendar_id) IS NOT NULL'
	EXEC(@sql)
	
	SET @sql = 'UPDATE calendars SET parent_calendar_id = (SELECT base_calendar_id FROM ' + @tableName + ' WHERE calendar_id = calendars.calendar_id) WHERE (SELECT base_calendar_id FROM ' + @tableName + ' WHERE calendar_id = calendars.calendar_id) IS NOT NULL'
	EXEC(@sql)
	
	SET @sql = 'UPDATE calendars SET description = NULL, calendar_name = ''My calendar'' WHERE (SELECT calendar_id FROM ' + @tableName + ' WHERE calendar_id = calendars.calendar_id) IS NOT NULL'
	EXEC(@sql)

    FETCH NEXT FROM instanceCursor INTO @instanceName
    END
CLOSE instanceCursor
DEALLOCATE instanceCursor



GO



ALTER FUNCTION GetCombinedCalendar
	(@base_calendar_id INT, @calendar_id INT, @start DATE, @end DATE)
RETURNS TABLE
AS
RETURN
(
	SELECT
		calendar_id AS calendar_id,
		datadate AS event_date,
		dayoff = ISNULL(dayoff2, dayoff1),
		holiday = ISNULL(holiday2, holiday1),
		notwork = ISNULL(notwork2, notwork1),
		calendar_event_id = ISNULL(calendar_event_id2, calendar_event_id1),
		calendar_data_id = ISNULL(calendar_data_id2, calendar_data_id1),
		calendar_static_holiday_id = ISNULL(calendar_holiday_id2, calendar_holiday_id1)
	FROM
	(
		SELECT
			@calendar_id as calendar_id,
			event_date AS datadate,
			MAX(dayoff1) AS dayoff1,
			MAX(dayoff2) AS dayoff2,
			MAX(holiday1) AS holiday1,
			MAX(holiday2) AS holiday2,
			MAX(notwork1) AS notwork1,
			MAX(notwork2) AS notwork2,
			MAX(calendar_event_id1) AS calendar_event_id1,
			MAX(calendar_event_id2) AS calendar_event_id2,
			MAX(calendar_data_id1) AS calendar_data_id1,
			MAX(calendar_data_id2) AS calendar_data_id2,
			MAX(calendar_holiday_id1) AS calendar_holiday_id1,
			MAX(calendar_holiday_id2) AS calendar_holiday_id2
		FROM (
			SELECT
				event_date,
				cd1.calendar_event_id AS calendar_event_id1,
				NULL AS calendar_event_id2,
				cd1.calendar_data_id AS calendar_data_id1,
				NULL AS calendar_data_id2,
				cd1.calendar_static_holiday_id AS calendar_holiday_id1,
				NULL AS calendar_holiday_id2,
				cd1.dayoff AS dayoff1,
				NULL AS dayoff2,
				cd1.holiday AS holiday1,
				NULL AS holiday2,
				cd1.notwork AS notwork1,
				NULL AS notwork2
			FROM calendar_data cd1
			WHERE cd1.calendar_id = 
				(SELECT CASE use_base_calendar WHEN 0 THEN 0 ELSE @base_calendar_id END FROM calendars WHERE calendar_id = @calendar_id) AND cd1.event_date BETWEEN @start AND @end
			UNION
			
			SELECT
				event_date,
				NULL AS calendar_data_id1,
				cd2.calendar_data_id AS calendar_data_id2,
				NULL AS calendar_event_id1,
				cd2.calendar_event_id AS calendar_event_id2,
				NULL AS calendar_holiday_id1,
				cd2.calendar_static_holiday_id AS calendar_holiday_id2,
				NULL AS dayoff1,
				cd2.dayoff AS dayoff2,
				NULL AS holiday1,
				cd2.holiday AS holiday2,
				NULL AS notwork1,
				cd2.notwork AS notwork2
			FROM calendar_data cd2
			WHERE cd2.calendar_id = @calendar_id AND cd2.event_date BETWEEN @start AND @end
			) inner_query
		GROUP BY event_date
	) outer_query
)



GO



DELETE FROM item_columns WHERE name IN ('base_calendar_id', 'calendar_id') AND process = 'user';
DELETE FROM calendar_events
DELETE FROM calendar_data
DELETE FROM calendar_static_holidays

DROP PROCEDURE Calendar_AddDateRange
DROP PROCEDURE Calendar_GetCalendarsByUserIDs
DROP PROCEDURE Calendar_RemoveEvent
DROP PROCEDURE Calendar_CheckDayOffs
DROP PROCEDURE Calendar_AddDaysOff