ALTER PROCEDURE [dbo].[app_ensure_process_allocation]	-- Add the parameters for the stored procedure here
  @instance nvarchar(10)
AS
BEGIN
  DECLARE @process NVARCHAR(50) = 'allocation'

  --Archive user is 0
  DECLARE @BinVar varbinary(128);
  SET @BinVar = CONVERT(varbinary(128), 0);
  SET CONTEXT_INFO @BinVar;

  --Create process
  EXEC app_ensure_process @instance, @process, @process, 1

  --Create columns
  EXEC app_ensure_column @instance, @process, 'status', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'resource_item_id', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'process_item_id', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'cost_center'    , 1, 1, 1, 1, 0, NULL, 0, 0
  EXEC app_ensure_column @instance, @process, 'creator_item_id', 1, 1, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 1, 0, null,  0, 0
  EXEC app_ensure_column @instance, @process, 'task_item_id', 1, 0, 1, 1, 0, null, 0, 0
  EXEC app_ensure_column @instance, @process, 'resource_type', 0, 0, 1, 1, 0, null,  0, 0
END;
GO

-- Apply changes for all instances that have this process
DECLARE @instanceName NVARCHAR(10)
DECLARE @process NVARCHAR(255) = 'allocation'
DECLARE Allocation_Cursor CURSOR FOR
  SELECT instance FROM processes WHERE variable = @process

OPEN Allocation_Cursor

FETCH NEXT FROM Allocation_Cursor INTO
  @instanceName

WHILE @@fetch_status = 0
  BEGIN
    EXEC app_ensure_process_allocation @instanceName

    FETCH  NEXT FROM Allocation_Cursor INTO
      @instanceName
  END
CLOSE Allocation_Cursor
DEALLOCATE Allocation_Cursor