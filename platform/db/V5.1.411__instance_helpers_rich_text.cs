using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;

namespace Keto5.x.platform.db
{
	class ServerMigrationInstanceHelpers : CsMigration
	{
		public override bool Execute(IConnections db) {

			db.ExecuteScalar("ALTER TABLE instance_helpers ALTER COLUMN desc_variable NVARCHAR(4000)");

			var defaultLanguage = Conv.ToStr(db.ExecuteScalar("select top 1 (default_locale_id) from instances"));

				string platform_helps =
					"SELECT instance_helper_id, desc_variable FROM instance_helpers WHERE platform = 1";

				string other_helps =
					"SELECT instance_helper_id, desc_variable FROM instance_helpers WHERE platform = 0";

				string languagesSql = "SELECT code FROM instance_languages";

				DataTable languagesDt = db.GetDatatable(languagesSql);

				var allLanguagesFiles = new Dictionary<string, Dictionary<string,string>>();

				foreach (DataRow language in languagesDt.Rows) {
					var l = new LanguageFile();
					var result = l.GetLanguageKeyCollectionFromJSON("keto", Conv.ToStr(language["code"]));
					allLanguagesFiles.Add(Conv.ToStr(language["code"]), result);
				}

				DataTable platformHelps = db.GetDatatable(platform_helps);
				DataTable otherHelps = db.GetDatatable(other_helps);


				foreach (DataRow helpRow in otherHelps.Rows) {

					var descTranslation = Conv.ToStr(helpRow["desc_variable"]);

					var str =
						"{\""+ defaultLanguage +"\":{\"html\":\"" + descTranslation + "\",\"delta\":{\"ops\":[{\"insert\":\"" + descTranslation + "\"}]}}}";

					var helperId = Conv.ToInt(helpRow["instance_helper_id"]);

					string _updateNames = "UPDATE instance_helpers SET desc_variable = " + "'" + str + "'" + " WHERE instance_helper_id = " +
					                      helperId;
					db.ExecuteUpdateQuery(_updateNames,null);
				}

				var s = "";

				foreach (DataRow confRow in platformHelps.Rows) {

					s = "";

					var descVariable = Conv.ToStr(confRow["desc_variable"]);
					foreach (var VARIABLE in allLanguagesFiles) {
						foreach (var x in VARIABLE.Value) {

							if (x.Key == descVariable) {
								var p = "\""+ VARIABLE.Key +"\":{\"html\":\"" + x.Value  + "\",\"delta\":{\"ops\":[{\"insert\":\"" + x.Value  + "\"}]}},";
								s = String.Concat(s, p);
							}
						}
					}

					s = s.Remove(s.Length - 1);
					s = s + "}";
					s = s.Insert(0, "{");

					var ihi = Conv.ToInt(confRow["instance_helper_id"]);

					string updateNames = "UPDATE instance_helpers SET desc_variable = " + "'" + s + "'" + " WHERE instance_helper_id = " +
					                     ihi;
					db.ExecuteUpdateQuery(updateNames,null);

				}

			return true;
		}
	}
}