ALTER TABLE conditions
  ADD [disabled] TINYINT DEFAULT 0
GO
EXEC app_remove_archive 'conditions'
EXEC app_ensure_archive 'conditions'
GO
UPDATE conditions SET [disabled] = 0
