

  SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[instance_schedule_groups](
	[group_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[schedule_json] [nvarchar](MAX) NULL,
	[order_no] [nvarchar](MAX) NULL,
	[disabled] [tinyint] NULL,
	[variable] [nvarchar](255) NULL, CONSTRAINT [PK_instance_schedule_groups] PRIMARY KEY CLUSTERED  
( 
  group_id ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
GO

ALTER TABLE instance_schedules
ADD order_no NVARCHAR (MAX)
ALTER TABLE instance_schedules
ADD [group_id] INT NULL FOREIGN KEY REFERENCES instance_schedule_groups(group_id)


GO

DECLARE @SID INT;
DECLARE cursor_process CURSOR
FOR SELECT schedule_id FROM instance_schedules 

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @SID
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
		UPDATE instance_schedules
		SET order_no = ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM instance_schedules 
		ORDER BY order_no COLLATE Latin1_general_bin DESC),null)),'U')
		WHERE schedule_id = @SID
		
        FETCH NEXT FROM cursor_process INTO 
            @SID
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
