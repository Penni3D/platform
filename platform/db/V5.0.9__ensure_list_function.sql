-- Stored Procedure

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_list_function'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_list_function] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_list_function]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'list_function'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	DECLARE @tableName NVARCHAR(50) = ''
	SET @tableName = '_' + @instance + '_' + @process
	DECLARE @sql NVARCHAR(MAX) = ''
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_list @instance, @process, @process, 0

	UPDATE processes SET list_order = 'list_item' WHERE process = @process

	EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'owner', 0, 5, 1, 1, 0, null,  0, 0, 'user'

	INSERT INTO items (instance, process) VALUES (@instance, @process);
	DECLARE @itemId INT = @@identity;

	SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Default function'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
	EXEC (@sql)
END
