SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_restore_item_recursive]
	@item_id INT,
	@parent_item_id INT,
	@archive_id INT = NULL,
	@archive_start DATETIME = NULL,
	@restore_mode TINYINT = 0,
	@restored_items_in NVARCHAR(MAX) = NULL,
	@restored_items_out NVARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	BEGIN TRY
		DECLARE @user_id INT
		SELECT @user_id = CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
		IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1

		DECLARE @sql NVARCHAR(MAX)
		DECLARE @instance NVARCHAR(10)
		DECLARE @process NVARCHAR(50)
		DECLARE @archive INT
		DECLARE @msg NVARCHAR(2048) = ''
		DECLARE @restored_items NVARCHAR(MAX) = ''
		DECLARE @guid UNIQUEIDENTIFIER
		DECLARE @columns NVARCHAR(MAX)
		DECLARE @deleted_by INT
		DECLARE @deleted_at DATETIME
		DECLARE @mode INT = -1
		DECLARE @item_column_id INT
		DECLARE @data_additional NVARCHAR(MAX)
		DECLARE @data_additional2 NVARCHAR(MAX)
		DECLARE @start_pos INT
		DECLARE @end_pos INT

		DECLARE @child_items_cursor CURSOR
		DECLARE @child_items_item_id INT

		DECLARE @parent_items_cursor CURSOR
		DECLARE @parent_items_item_id INT

		DECLARE @table_columns_cursor CURSOR
		DECLARE @table_items_cursor CURSOR
		DECLARE @table_item_column_id NVARCHAR(255)
		DECLARE @table_instance NVARCHAR(10)
		DECLARE @table_process NVARCHAR(50)
		DECLARE @table_column_name NVARCHAR(50)
		DECLARE @table_data_type INT
		DECLARE @table_item_id INT
		CREATE TABLE #table_items (item_id INT)
		CREATE TABLE #table_item_column_ids (item_column_id INT)
	
		DECLARE @constraints_cursor CURSOR
		DECLARE @constraints_items_cursor CURSOR
		DECLARE @constraints_table_name NVARCHAR(50)
		DECLARE @constraints_column_name NVARCHAR(50)
		DECLARE @constraints_item_id INT

		DECLARE @ac_cursor CURSOR
		DECLARE @ac_allocation_comment_id INT
		DECLARE @ac_allocation_item_id INT
		DECLARE @ac_comment NVARCHAR(255)
		DECLARE @ac_created_at DATETIME
		DECLARE @ac_author_item_id INT

		DECLARE @iucs_cursor CURSOR
		DECLARE @iucs_instance NVARCHAR(10)
		DECLARE @iucs_user_id INT
		DECLARE @iucs_group NVARCHAR(255)
		DECLARE @iucs_identifier NVARCHAR(255)
		DECLARE @iucs_value NVARCHAR(MAX)
		DECLARE @iucs_public TINYINT
		DECLARE @iucs_name NVARCHAR(MAX)
		DECLARE @iucs_show_in_menu TINYINT
		DECLARE @iucs_user_configuration_id INT
		DECLARE @iucs_visible_for_filter NVARCHAR(MAX)
		DECLARE @iucs_visible_for_usergroup NVARCHAR(MAX)
		DECLARE @iucs_portfolio_default_view INT
		DECLARE @iucs_dashboard_id INT

		DECLARE @pb_cursor CURSOR
		DECLARE @pb_process_baseline_id INT
		DECLARE @pb_instance NVARCHAR(10)
		DECLARE @pb_process NVARCHAR(50)
		DECLARE @pb_process_item_id INT
		DECLARE @pb_process_group_id INT
		DECLARE @pb_baseline_date DATETIME
		DECLARE @pb_creator_user_item_id INT
		DECLARE @pb_description NVARCHAR(50)
		DECLARE @pb_control_name NVARCHAR(255)

		SET @restored_items_out = ''

		-- Discover archive_id and archive_start and mode
		IF @archive_id IS NOT NULL OR @archive_start IS NOT NULL BEGIN
			SELECT @instance = instance, @process = process FROM items WHERE item_id = @item_id

			IF @instance IS NULL OR @process IS NULL BEGIN
				SELECT @instance = instance, @process = process, @archive_start = deleted_date, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

				IF @instance IS NOT NULL AND @process IS NOT NULL SET @mode = 1 -- Restore deleted item with historic values
			END
			ELSE BEGIN
				SET @mode = 0 -- Restore item with historic values
			END

			IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
				IF @archive_id IS NOT NULL BEGIN
					SET @sql = 'SELECT @archive_start = archive_start
								FROM archive__' + @instance + '_' + @process + ' 
								WHERE archive_id = @archive_id'
					EXEC sp_executesql @sql, N'@archive_id INT, @archive_start DATETIME OUTPUT', @archive_id, @archive_start OUTPUT
				END
				ELSE IF @archive_start IS NOT NULL BEGIN
					SET @sql = 'SELECT @archive_id = archive_id
								FROM get__' + @instance + '_' + @process + '_with_id(@item_id, @archive_start) '
					EXEC sp_executesql @sql, N'@item_id INT, @archive_start DATETIME, @archive_id INT OUTPUT', @item_id, @archive_start, @archive_id OUTPUT
				END
			END
		END
		ELSE BEGIN
			SELECT @instance = instance, @process = process, @archive_start = deleted_date, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

			IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
				SET @mode = 1 -- Restore deleted item with latest values

				SET @sql = 'SELECT TOP 1 @archive_id = archive_id
							FROM archive__' + @instance + '_' + @process + ' 
							WHERE archive_type = 2 AND item_id = @item_id 
							ORDER BY archive_id DESC'
				EXEC sp_executesql @sql, N'@item_id INT, @archive_id INT OUTPUT', @item_id, @archive_id OUTPUT
			END
		END

		IF @mode = 1 AND @archive_start IS NOT NULL AND @archive_start <= '2021-06-07' SET @msg = 'Cannot restore. Item is too old to restore'

		IF @archive_id IS NULL OR @archive_start IS NULL BEGIN
			 SET @msg = 'Cannot restore. No archive found'
		END

		IF @instance IS NOT NULL AND @process IS NOT NULL AND @archive_id IS NOT NULL AND @archive_start IS NOT NULL BEGIN
			SET @sql = 'SELECT @archive = archive FROM item_tables WHERE name = ''_' + @instance + '_' + @process + ''''
			EXEC sp_executesql @sql, N'@archive INT OUTPUT', @archive OUTPUT
			IF ISNULL(@archive, 1) = 0 SET @msg = 'Cannot restore. Process ' + @process + ' doesn''t use archive'
		END

		IF @instance IS NULL OR @process IS NULL BEGIN
			SET @msg = 'Item not found'
		END

		IF @restored_items_in IS NOT NULL AND @instance IS NOT NULL AND @process IS NOT NULL AND @archive_id IS NOT NULL AND @archive_start IS NOT NULL BEGIN
			IF @archive_id = 0 BEGIN
				SET @sql = 'SELECT @deleted_at = archive_start, @deleted_by = archive_userid
							FROM _' + @instance + '_' + @process + ' 
							WHERE item_id = @item_id'
				EXEC sp_executesql @sql, N'@item_id INT, @deleted_at DATETIME OUTPUT, @deleted_by INT OUTPUT', @item_id, @deleted_at OUTPUT, @deleted_by OUTPUT
			END
			ELSE BEGIN
				SET @sql = 'SELECT @deleted_at = archive_start, @deleted_by = archive_userid
							FROM archive__' + @instance + '_' + @process + ' 
							WHERE archive_id = @archive_id'
				EXEC sp_executesql @sql, N'@archive_id INT, @deleted_at DATETIME OUTPUT, @deleted_by INT OUTPUT', @archive_id, @deleted_at OUTPUT, @deleted_by OUTPUT
			END
			SET @restored_items = N',{"item_id":' + CAST(@item_id AS NVARCHAR) + ',"parent_item_id":' + CAST(@parent_item_id AS NVARCHAR) + ',"instance":"' + @instance + '","process":"' + @process + '","restored_by":' + CAST(@deleted_by AS NVARCHAR) + ',"restored_at":"' + FORMAT(@deleted_at,'yyyy-MM-ddTHH:mm:ss.fff') + '","deleted":' + CAST(@mode AS NVARCHAR)
		END

		IF @msg = '' AND @instance IS NOT NULL AND @process IS NOT NULL AND @archive_id IS NOT NULL AND @archive_start IS NOT NULL BEGIN
			IF @restore_mode < 3 BEGIN
				-- Remove indications that item is deleted
				IF @mode = 1 BEGIN
					DELETE FROM items_deleted WHERE item_id = @item_id
				END

				-- Restore item
				IF @mode = 1 BEGIN
					EXEC sp_set_session_context @key = N'restore_item_id', @value = @item_id
					EXEC sp_set_session_context @key = N'restore_guid', @value = @guid
					INSERT INTO items (instance, process, deleted) VALUES (@instance, @process, 0)
				END

				-- Restore item's data in row
				SELECT @columns = STUFF((SELECT ', t.' + COLUMN_NAME + ' = a.' + COLUMN_NAME 
										 FROM INFORMATION_SCHEMA.COLUMNS 
										 WHERE TABLE_NAME = '_' + @instance + '_' + @process AND COLUMN_NAME NOT IN ('item_id', 'order_no', 'archive_userid', 'archive_start') 
										 GROUP BY COLUMN_NAME 
										 FOR XML PATH('')), 1, 2, '')
				IF @columns <> '' BEGIN
					SET @sql = 'UPDATE t SET ' + @columns + ' 
								FROM _' + @instance + '_' + @process + ' t 
								INNER JOIN archive__' + @instance + '_' + @process + ' a ON a.item_id = t.item_id AND a.archive_id = @archive_id
								WHERE t.item_id = @item_id'
					EXEC sp_executesql @sql, N'@item_id INT, @archive_id INT', @item_id, @archive_id
				END
			END
		
			-- Restore by constraints
			SET @constraints_cursor = CURSOR FAST_FORWARD LOCAL FOR
			SELECT ccu.TABLE_NAME, ccu.COLUMN_NAME 
			FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
			INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
			WHERE UNIQUE_CONSTRAINT_NAME = 'PK_items' AND SUBSTRING(ccu.TABLE_NAME, 1, 1) = '_'
			OPEN @constraints_cursor
			FETCH NEXT FROM @constraints_cursor INTO @constraints_table_name, @constraints_column_name
			WHILE @@fetch_status = 0 BEGIN
				SET @sql = 'SET @cursor = CURSOR FAST_FORWARD LOCAL FOR
							SELECT item_id FROM get_' + @constraints_table_name + '(@archive_start) WHERE ' + @constraints_column_name + ' = @item_id
							OPEN @cursor'
				EXEC sp_executesql @sql, N'@item_id INT, @archive_start DATETIME, @cursor CURSOR OUTPUT', @item_id, @archive_start, @constraints_items_cursor OUTPUT
				FETCH NEXT FROM @constraints_items_cursor INTO @constraints_item_id
				WHILE @@fetch_status = 0 BEGIN
					EXEC app_restore_item_recursive @constraints_item_id, @item_id, NULL, @archive_start, @restore_mode, @restored_items_out, @restored_items_out OUTPUT
			
					FETCH NEXT FROM @constraints_items_cursor INTO @constraints_item_id
				END

				DEALLOCATE @constraints_items_cursor
			
				FETCH NEXT FROM @constraints_cursor INTO @constraints_table_name, @constraints_column_name
			END
		
			-- Restore child items by item_columns.use_fk = 2
			SET @child_items_cursor = CURSOR FOR SELECT selected_item_id FROM get_item_data_process_selections_with_id(@archive_start, @item_id) WHERE item_column_id IN (SELECT item_column_id FROM item_columns WHERE use_fk = 2)
			OPEN @child_items_cursor
			FETCH NEXT FROM @child_items_cursor INTO @child_items_item_id
			WHILE @@fetch_status = 0 BEGIN
				EXEC app_restore_item_recursive @child_items_item_id, @item_id, NULL, @archive_start, @restore_mode, @restored_items_out, @restored_items_out OUTPUT
			
				FETCH NEXT FROM @child_items_cursor INTO @child_items_item_id
			END

			-- Restore parent items by item_columns.use_fk = 3
			SET @parent_items_cursor = CURSOR FOR SELECT item_id FROM get_item_data_process_selections_with_sel_id(@archive_start, @item_id) WHERE item_column_id IN (SELECT item_column_id FROM item_columns WHERE use_fk = 3)
			OPEN @parent_items_cursor
			FETCH NEXT FROM @parent_items_cursor INTO @parent_items_item_id
			WHILE @@fetch_status = 0 BEGIN
				EXEC app_restore_item_recursive @parent_items_item_id, @item_id, NULL, @archive_start, @restore_mode, @restored_items_out, @restored_items_out OUTPUT
			
				FETCH NEXT FROM @parent_items_cursor INTO @parent_items_item_id
			END
			
			IF @restore_mode < 3 BEGIN
				-- Restore item_data_process_selections
				IF @mode = 0 DELETE FROM item_data_process_selections WHERE item_id = @item_id OR selected_item_id = @item_id OR link_item_id = @item_id 

				INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id)
				SELECT idps.item_id, idps.item_column_id, idps.selected_item_id, idps.link_item_id
				FROM get_item_data_process_selections_own(@archive_start, @item_id) idps
				LEFT JOIN items_deleted i_id ON i_id.item_id = idps.item_id
				LEFT JOIN item_columns ic ON ic.item_column_id = idps.item_column_id
				LEFT JOIN items_deleted s_id ON s_id.item_id = idps.selected_item_id
				LEFT JOIN items_deleted l_id ON l_id.item_id = idps.link_item_id
				WHERE i_id.item_id IS NULL
				AND ic.item_column_id IS NOT NULL
				AND s_id.item_id IS NULL
				AND l_id.item_id IS NULL
			END

			-- Restore table columns data (18, 21) if parent process column is int/process/list and table process column is item_id
			SET @table_columns_cursor = CURSOR FOR SELECT data_additional2 FROM item_columns WHERE instance = @instance AND process = @process AND data_type in (18, 21) AND CHARINDEX('"parentFieldName"', data_additional2) = 0 
			OPEN @table_columns_cursor
			FETCH NEXT FROM @table_columns_cursor INTO @data_additional2
			WHILE @@fetch_status = 0 BEGIN
				SET @start_pos = CHARINDEX('"itemColumnId"', @data_additional2)
				IF @start_pos = 0 BEGIN
					SET @start_pos = CHARINDEX('"ownerColumnId"', @data_additional2) + 1
				END
				IF @start_pos > 1 BEGIN
					SET @start_pos = @start_pos + 15
					SET @end_pos = CHARINDEX(',', @data_additional2, @start_pos)
					IF @end_pos = 0 BEGIN
						SET @end_pos = CHARINDEX('}', @data_additional2, @start_pos)
					END
					SET @table_item_column_id = SUBSTRING(@data_additional2, @start_pos, @end_pos - @start_pos)
					IF (SELECT COUNT (*) FROM #table_item_column_ids WHERE item_column_id = @table_item_column_id) = 0 BEGIN
						INSERT INTO #table_item_column_ids (item_column_id) VALUES (@table_item_column_id)
						IF ISNUMERIC(@table_item_column_id) = 1 BEGIN
							SELECT @table_instance = instance, @table_process = process, @table_column_name = [name], @table_data_type = data_type, @data_additional = data_additional FROM item_columns WHERE item_column_id = @table_item_column_id
							IF @table_data_type = 1 BEGIN
								SET @sql = 'INSERT INTO #table_items SELECT item_id FROM get__' + @table_instance + '_' + @table_process + '(@archive_start) WHERE ' + @table_column_name + ' = @item_id'
								EXEC sp_executesql @sql, N'@archive_start DATETIME, @item_id INT', @archive_start, @item_id
								SET @table_items_cursor = CURSOR FOR SELECT item_id FROM #table_items
								OPEN @table_items_cursor
								FETCH NEXT FROM @table_items_cursor INTO @table_item_id
								WHILE @@fetch_status = 0 BEGIN
									EXEC app_restore_item_recursive @table_item_id, @item_id, NULL, @archive_start, @restore_mode, @restored_items_out, @restored_items_out OUTPUT
								
									FETCH NEXT FROM @table_items_cursor INTO @table_item_id
								END
								DELETE FROM #table_items
							END
							ELSE IF @table_data_type IN (5, 6) AND @data_additional = @process BEGIN
								IF @restore_mode < 3 AND @mode = 0 DELETE FROM item_data_process_selections WHERE item_column_id = @table_item_column_id AND selected_item_id = @item_id
							
								INSERT INTO #table_items SELECT item_id FROM get_item_data_process_selections_with_sel_ids(@archive_start, @table_item_column_id, @item_id)
								SET @table_items_cursor = CURSOR FOR SELECT item_id FROM #table_items
								OPEN @table_items_cursor
								FETCH NEXT FROM @table_items_cursor INTO @table_item_id
								WHILE @@fetch_status = 0 BEGIN
									EXEC app_restore_item_recursive @table_item_id, @item_id, NULL, @archive_start, @restore_mode, @restored_items_out, @restored_items_out OUTPUT
							
									FETCH NEXT FROM @table_items_cursor INTO @table_item_id
								END
								DELETE FROM #table_items
							END
						END
					END
					FETCH NEXT FROM @table_columns_cursor INTO @data_additional2
				END
			END

			IF @restore_mode < 3 BEGIN
				-- Restore item_subitems
				IF @mode = 0 DELETE FROM item_subitems WHERE parent_item_id = @item_id OR item_id = @item_id

				INSERT INTO item_subitems (parent_item_id, item_id, favourite)
				SELECT parent_item_id, item_id, favourite FROM get_item_subitems(@archive_start)
				WHERE parent_item_id = @item_id OR item_id = @item_id

				-- Restore process_tabs_subprocess_data
				IF @mode = 0 DELETE FROM process_tabs_subprocess_data WHERE item_id = @item_id

				INSERT INTO process_tabs_subprocess_data (process_tab_subprocess_id, parent_process_item_id, item_id)
				SELECT process_tab_subprocess_id, parent_process_item_id, item_id FROM get_process_tabs_subprocess_data(@archive_start)
				WHERE item_id = @item_id

				-- Restore task_durations
				IF @mode = 0 DELETE FROM task_durations WHERE task_item_id = @item_id OR resource_item_id = @item_id

				INSERT INTO task_durations (task_item_id, resource_item_id, [date], [hours])
				SELECT task_item_id, resource_item_id, [date], [hours] FROM get_task_durations(@archive_start)
				WHERE task_item_id = @item_id OR resource_item_id = @item_id

				-- Restore allocation_durations
				IF @mode = 0 DELETE FROM allocation_durations WHERE allocation_item_id = @item_id

				INSERT INTO allocation_durations (allocation_item_id, [date], [hours])
				SELECT allocation_item_id, [date], [hours] FROM get_allocation_durations(@archive_start)
				WHERE allocation_item_id = @item_id

				-- Restore allocation_comments
				IF @mode = 0 DELETE FROM allocation_comments WHERE allocation_item_id = @item_id OR author_item_id = @item_id

				SET @ac_cursor = CURSOR FOR 
				SELECT allocation_comment_id, allocation_item_id, comment, created_at, author_item_id 
				FROM get_allocation_comments(@archive_start)
				WHERE allocation_item_id = @item_id OR author_item_id = @item_id
				OPEN @ac_cursor
				FETCH NEXT FROM @ac_cursor INTO @ac_allocation_comment_id, @ac_allocation_item_id, @ac_comment, @ac_created_at, @ac_author_item_id 
				WHILE @@fetch_status = 0 BEGIN
					EXEC sp_set_session_context @key = N'restore_allocation_comment_id', @value = @ac_allocation_comment_id

					INSERT INTO allocation_comments (allocation_item_id, comment, created_at, author_item_id)
					VALUES (@ac_allocation_item_id, @ac_comment, @ac_created_at, @ac_author_item_id)
			
					FETCH NEXT FROM @ac_cursor INTO @ac_allocation_comment_id, @ac_allocation_item_id, @ac_comment, @ac_created_at, @ac_author_item_id
				END

				-- Restore instance_user_configurations_saved
				IF @mode = 0 DELETE FROM instance_user_configurations_saved WHERE [user_id] = @item_id

				SET @iucs_cursor = CURSOR FOR 
				SELECT instance, [user_id], [group], identifier, [value], [public], [name], show_in_menu, user_configuration_id, visible_for_filter, visible_for_usergroup, portfolio_default_view, dashboard_id
				FROM get_instance_user_configurations_saved(@archive_start)
				WHERE [user_id] = @item_id
				OPEN @iucs_cursor
				FETCH NEXT FROM @iucs_cursor INTO @iucs_instance, @iucs_user_id, @iucs_group, @iucs_identifier, @iucs_value, @iucs_public, @iucs_name, @iucs_show_in_menu, @iucs_user_configuration_id, @iucs_visible_for_filter, @iucs_visible_for_usergroup, @iucs_portfolio_default_view, @iucs_dashboard_id
				WHILE @@fetch_status = 0 BEGIN
					EXEC sp_set_session_context @key = N'restore_user_configuration_id', @value = @iucs_user_configuration_id

					INSERT INTO instance_user_configurations_saved (instance, [user_id], [group], identifier, [value], [public], [name], show_in_menu, visible_for_filter, visible_for_usergroup, portfolio_default_view, dashboard_id)
					VALUES (@iucs_instance, @iucs_user_id, @iucs_group, @iucs_identifier, @iucs_value, @iucs_public, @iucs_name, @iucs_show_in_menu, @iucs_visible_for_filter, @iucs_visible_for_usergroup, @iucs_portfolio_default_view, @iucs_dashboard_id)
			
					FETCH NEXT FROM @iucs_cursor INTO @iucs_instance, @iucs_user_id, @iucs_group, @iucs_identifier, @iucs_value, @iucs_public, @iucs_name, @iucs_show_in_menu, @iucs_user_configuration_id, @iucs_visible_for_filter, @iucs_visible_for_usergroup, @iucs_portfolio_default_view, @iucs_dashboard_id
				END

				-- Restore instance_user_configurations
				IF @mode = 0 DELETE FROM instance_user_configurations WHERE [user_id] = @item_id

				INSERT INTO instance_user_configurations (instance, [user_id], [group], [value], identifier)
				SELECT instance, [user_id], [group], [value], identifier FROM get_instance_user_configurations(@archive_start)
				WHERE [user_id] = @item_id

				-- Restore condition_user_groups
				IF @mode = 0 DELETE FROM condition_user_groups WHERE item_id = @item_id

				INSERT INTO condition_user_groups (condition_id, item_id)
				SELECT condition_id, item_id FROM get_condition_user_groups(@archive_start)
				WHERE item_id = @item_id

				-- Restore process_baseline
				IF @mode = 0 DELETE FROM process_baseline WHERE process_item_id = @item_id

				SET @pb_cursor = CURSOR FOR 
				SELECT process_baseline_id, instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id, [description], control_name
				FROM get_process_baseline(@archive_start)
				WHERE process_item_id = @item_id
				OPEN @pb_cursor
				FETCH NEXT FROM @pb_cursor INTO @pb_process_baseline_id, @pb_instance, @pb_process, @pb_process_item_id, @pb_process_group_id, @pb_baseline_date, @pb_creator_user_item_id, @pb_description, @pb_control_name
				WHILE @@fetch_status = 0 BEGIN
					EXEC sp_set_session_context @key = N'restore_process_baseline_id', @value = @pb_process_baseline_id

					INSERT INTO process_baseline (instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id, [description], control_name)
					VALUES (@pb_instance, @pb_process, @pb_process_item_id, @pb_process_group_id, @pb_baseline_date, @pb_creator_user_item_id, @pb_description, @pb_control_name)
			
					FETCH NEXT FROM @pb_cursor INTO @pb_process_baseline_id, @pb_instance, @pb_process, @pb_process_item_id, @pb_process_group_id, @pb_baseline_date, @pb_creator_user_item_id, @pb_description, @pb_control_name
				END
			END

			IF @restored_items_in IS NOT NULL BEGIN
				SET @restored_items_out = @restored_items_in + @restored_items + ',"error":0,"error_msg":""}' + @restored_items_out
			END
		END

		IF @msg <> '' BEGIN
			IF @restored_items_in IS NOT NULL BEGIN
				IF @restored_items = '' BEGIN
					SET @restored_items = N',{"item_id":' + CAST(@item_id AS NVARCHAR) + ',"parent_item_id":' + CAST(@parent_item_id AS NVARCHAR) + ',"instance":"' + ISNULL(@instance, '') + '","process":"' + ISNULL(@process, '') + '","restored_by":' + CAST(ISNULL(@deleted_by, '0') AS NVARCHAR) + ',"restored_at":"' + FORMAT(ISNULL(@deleted_at, ''), 'yyyy-MM-ddTHH:mm:ss.fff') + '","deleted":' + CAST(@mode AS NVARCHAR)
				END
				SET @restored_items_out = @restored_items_in + @restored_items + ',"error":1,"error_msg":"' + REPLACE(@msg, '"', '\"') + '"}' + @restored_items_out
			END
			ELSE BEGIN;
				THROW 51000, @msg, 1
			END
		END
	END TRY
	BEGIN CATCH
		IF @restored_items_in IS NOT NULL BEGIN
			IF @restored_items = '' BEGIN
				SET @restored_items = N',{"item_id":' + CAST(@item_id AS NVARCHAR) + ',"parent_item_id":' + CAST(@parent_item_id AS NVARCHAR) + ',"instance":"' + ISNULL(@instance, '') + '","process":"' + ISNULL(@process, '') + '","restored_by":' + CAST(ISNULL(@deleted_by, '0') AS NVARCHAR) + ',"restored_at":"' + FORMAT(ISNULL(@deleted_at, ''), 'yyyy-MM-ddTHH:mm:ss.fff') + '","deleted":' + CAST(@mode AS NVARCHAR)
			END
			SET @restored_items_out = @restored_items_in + @restored_items + ',"error":1,"error_msg":"' + REPLACE(ERROR_MESSAGE(), '"', '\"') + '"}' + @restored_items_out
		END
		ELSE BEGIN
			SET @msg = 'ROLLBACK - ' + ERROR_MESSAGE();
			THROW 51000, @msg, 1
		END
	END CATCH
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_restore_item]
	@item_id INT,
	@archive_id INT = NULL,
	@archive_start DATETIME = NULL,
	@restore_mode TINYINT = 0,
	@restored_items_out NVARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION
		DECLARE @restored_items_in NVARCHAR(MAX) = NULL
		IF @restore_mode > 0 SET @restored_items_in = ''
		EXEC app_restore_item_recursive @item_id, 0, @archive_id, @archive_start, @restore_mode, @restored_items_in, @restored_items_out OUTPUT
		IF @restored_items_out IS NOT NULL SET @restored_items_out = '[' + SUBSTRING(@restored_items_out, 2, LEN(@restored_items_out)) + ']'
			
		IF @restore_mode >= 2 BEGIN
			IF @@TRANCOUNT > 0 ROLLBACK TRAN
		END
		ELSE BEGIN
			IF (SELECT MAX(ERROR) FROM OPENJSON (@restored_items_out) WITH (error TINYINT '$.error')) = 0 BEGIN
				COMMIT TRAN
			END
			ELSE BEGIN
				IF @@TRANCOUNT > 0 ROLLBACK TRAN
			END
		END
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK TRAN
		DECLARE @msg NVARCHAR(2048) = 'ROLLBACK - ' + ERROR_MESSAGE();
		THROW 51000, @msg, 1
	END CATCH
END