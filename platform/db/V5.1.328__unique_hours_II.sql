EXEC app_set_archive_user_id 1

DECLARE @sql NVARCHAR(MAX)
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance
                                  FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
    if @instance = 'nokiantyre'
        BEGIN
            DELETE
            FROM items
            WHERE item_id IN (
                select item_id
                from (
                         select user_item_id,
                                task_item_id,
                                date,
                                COUNT(ISNULL(hours, 0)) AS records,
                                MIN(item_id)            AS item_id
                         from _nokiantyre_worktime_tracking_hours
                         group by user_item_id, task_item_id, date
                     ) iq
                where iq.records > 1
            )
        END

    SET @sql = 'DELETE FROM items WHERE item_id IN (SELECT item_id FROM _' + @instance +
               '_worktime_tracking_hours WHERE hours IS null AND additional_hours IS null)'
    EXEC (@sql)

    IF NOT EXISTS(SELECT *
              FROM sys.indexes
              WHERE name = 'user_task_date')
        BEGIN
            SET @sql = 'ALTER TABLE _' + @instance + '_worktime_tracking_hours ALTER COLUMN process NVARCHAR(50)'
            EXEC (@sql)

            SET @sql = 'ALTER TABLE archive__' + @instance + '_worktime_tracking_hours ALTER COLUMN process NVARCHAR(50)'
            EXEC (@sql)
            
            SET @sql = 'ALTER TABLE _' + @instance +
                       '_worktime_tracking_hours ADD CONSTRAINT user_task_date UNIQUE (user_item_id, task_item_id, date, process)'
            EXEC (@sql)        
        END

    FETCH NEXT FROM instanceCursor INTO @instance
END
CLOSE instanceCursor
DEALLOCATE instanceCursor