EXEC app_set_archive_user_id 0

ALTER TABLE process_action_rows
ADD condition_id INT

GO 

  ALTER TABLE process_action_rows
ADD FOREIGN KEY (condition_id)
REFERENCES conditions (condition_id) ON DELETE CASCADE 