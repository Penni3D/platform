EXEC app_set_archive_user_id 1
DECLARE @Instance NVARCHAR(255);
DECLARE cursor_process CURSOR FOR SELECT instance FROM instances
OPEN cursor_process
FETCH NEXT FROM cursor_process INTO @Instance
WHILE @@FETCH_STATUS = 0
    BEGIN
        EXEC app_ensure_column @instance, 'worktime_tracking_hours', 'owner_item_id', 0, 1, 1, 1, 0, null, 0, 0, 0
        DECLARE @Sql1 NVARCHAR(MAX) = 'UPDATE _' + @instance + '_worktime_tracking_hours SET owner_item_id = ISNULL((CASE WHEN process = ''task'' THEN (SELECT owner_item_id FROM _' + @instance + '_task WHERE item_id = task_item_id) ELSE 0 END),0)'
        EXEC sp_executesql @Sql1
        FETCH NEXT FROM cursor_process INTO @Instance
    END;
CLOSE cursor_process;
DEALLOCATE cursor_process;