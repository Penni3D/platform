SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[item_subitems_INSTEAD_OF_INSERT]
	ON [dbo].[item_subitems]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[item_subitems] ([parent_item_id],[item_id],[favourite],[archive_start], archive_userid) 
	SELECT [parent_item_id],[item_id],[favourite],[archive_start] 
	, @user_id
	FROM inserted

	INSERT INTO archive_item_subitems([parent_item_id],[item_id],[favourite],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [parent_item_id],[item_id],[favourite], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted



GO



INSERT INTO archive_item_subitems (parent_item_id, item_id, favourite, archive_userid, archive_start, archive_end, archive_type)
SELECT isi.parent_item_id, isi.item_id, isi.favourite, isi.archive_userid, DATEADD(s, -1, isi.archive_start), isi.archive_start, 1
FROM item_subitems isi
LEFT JOIN archive_item_subitems aisi ON aisi.item_id = isi.item_id
AND aisi.parent_item_id = isi.parent_item_id
AND aisi.favourite = isi.favourite
WHERE aisi.item_id IS NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[allocation_durations_INSTEAD_OF_INSERT]
	ON [dbo].[allocation_durations]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[allocation_durations] ([allocation_item_id],[date],[hours],[archive_start], archive_userid) 
	SELECT [allocation_item_id],[date],[hours],[archive_start] 
	, @user_id
	FROM inserted

	INSERT INTO archive_allocation_durations([allocation_item_id],[date],[hours],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [allocation_item_id],[date],[hours], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted



GO



INSERT INTO archive_allocation_durations (allocation_item_id, [date], [hours], archive_userid, archive_start, archive_end, archive_type)
SELECT ad.allocation_item_id, ad.[date], ad.[hours], ad.archive_userid, DATEADD(s, -1, ad.archive_start), ad.archive_start, 1
FROM allocation_durations ad
LEFT JOIN archive_allocation_durations aad ON aad.allocation_item_id = ad.allocation_item_id
AND aad.[date] = ad.[date]
WHERE aad.allocation_item_id IS NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[allocation_comments_INSTEAD_OF_INSERT]
	ON [dbo].[allocation_comments]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[allocation_comments] ([allocation_comment_id],[allocation_item_id],[comment],[created_at],[author_item_id],[archive_start], archive_userid) 
	SELECT [allocation_comment_id],[allocation_item_id],[comment],[created_at],[author_item_id],[archive_start] 
	, @user_id
	FROM inserted

	INSERT INTO archive_allocation_comments([allocation_comment_id],[allocation_item_id],[comment],[created_at],[author_item_id],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [allocation_comment_id],[allocation_item_id],[comment],[created_at],[author_item_id], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted



GO



INSERT INTO archive_allocation_comments (allocation_comment_id, comment, created_at, author_item_id, archive_userid, archive_start, archive_end, archive_type)
SELECT ac.allocation_comment_id, ac.comment, ac.created_at, ac.author_item_id, ac.archive_userid, DATEADD(s, -1, ac.archive_start), ac.archive_start, 1
FROM allocation_comments ac
LEFT JOIN archive_allocation_comments aac ON aac.allocation_comment_id = ac.allocation_comment_id
WHERE aac.allocation_comment_id IS NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[process_tabs_subprocess_data_INSTEAD_OF_INSERT]
	ON [dbo].[process_tabs_subprocess_data]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[process_tabs_subprocess_data] ([process_tab_subprocess_id],[parent_process_item_id],[item_id],[archive_start], archive_userid) 
	SELECT [process_tab_subprocess_id],[parent_process_item_id],[item_id],[archive_start] 
	, @user_id
	FROM inserted

	INSERT INTO archive_process_tabs_subprocess_data([process_tab_subprocess_id],[parent_process_item_id],[item_id],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [process_tab_subprocess_id],[parent_process_item_id],[item_id], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted



GO



INSERT INTO archive_process_tabs_subprocess_data (process_tab_subprocess_id, parent_process_item_id, item_id, archive_userid, archive_start, archive_end, archive_type)
SELECT ptsd.process_tab_subprocess_id, ptsd.parent_process_item_id, ptsd.item_id, ptsd.archive_userid, DATEADD(s, -1, ptsd.archive_start), ptsd.archive_start, 1
FROM process_tabs_subprocess_data ptsd
LEFT JOIN archive_process_tabs_subprocess_data aptsd ON aptsd.process_tab_subprocess_id = ptsd.process_tab_subprocess_id
AND aptsd.parent_process_item_id = ptsd.parent_process_item_id
AND aptsd.item_id = ptsd.item_id
WHERE aptsd.process_tab_subprocess_id IS NULL



GO



ALTER TABLE archive_instance_user_configurations_saved ADD portfolio_default_view INT
ALTER TABLE archive_instance_user_configurations_saved ADD dashboard_id INT



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[instance_user_configurations_saved_INSTEAD_OF_INSERT]
	ON [dbo].[instance_user_configurations_saved]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[instance_user_configurations_saved] ([instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id], archive_userid) 
	SELECT [instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id]
	, @user_id
	FROM inserted
	
	INSERT INTO archive_instance_user_configurations_saved([instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_userid],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id],archive_end, archive_type)
	SELECT [instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter], @user_id, DATEADD(s, -1, [archive_start]),[visible_for_usergroup],[portfolio_default_view],[dashboard_id], [archive_start], 1 FROM inserted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[instance_user_configurations_saved_AFTER_UPDATE]
	ON [dbo].[instance_user_configurations_saved]
	AFTER UPDATE
	AS

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF (SELECT archive FROM item_tables WHERE name = 'instance_user_configurations_saved') = 1 BEGIN
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;
    
	UPDATE [dbo].[instance_user_configurations_saved]
    SET archive_start= getutcdate(),
    archive_userid=  @user_id
    WHERE [user_configuration_id] IN (SELECT DISTINCT [user_configuration_id] FROM Inserted)

	INSERT INTO archive_instance_user_configurations_saved([instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_userid],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id],archive_end, archive_type)
	SELECT [instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_userid],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id],
		archive_end = getutcdate(),
		archive_type = 1
	FROM deleted	
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[instance_user_configurations_saved_DELETE]
	ON [dbo].[instance_user_configurations_saved]
	FOR DELETE
	AS	

	IF (SELECT archive FROM item_tables WHERE name = 'instance_user_configurations_saved') = 1 BEGIN
		INSERT INTO archive_instance_user_configurations_saved([instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_userid],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id],archive_end, archive_type)
		SELECT [instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_userid],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id],
			archive_end = getutcdate(),
			archive_type = 1
		FROM deleted
			
		DECLARE @user_id int;
		select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
		IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

		INSERT INTO archive_instance_user_configurations_saved([instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id],archive_end, archive_type, archive_userid)
		SELECT [instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id],
			archive_end = getutcdate(),
			archive_type = 2,
			archive_userid = @user_id
		FROM deleted
	END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[get_instance_user_configurations_saved]( @d datetime )
	RETURNS @rtab TABLE (archive_id INT, [instance] nvarchar(10) NOT NULL ,[user_id] int NOT NULL ,[group] nvarchar(255) NOT NULL ,[identifier] nvarchar(255) NULL ,[value] nvarchar(MAX) NOT NULL ,[public] tinyint NULL DEFAULT ((0)),[name] nvarchar(MAX) NULL ,[show_in_menu] tinyint NOT NULL DEFAULT ((0)),[user_configuration_id] int NOT NULL ,[visible_for_filter] nvarchar(255) NULL ,[archive_userid] int NOT NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()),[visible_for_usergroup] nvarchar(255) NULL,[portfolio_default_view] INT NULL,[dashboard_id] INT NULL)
	AS
	BEGIN	
		INSERT @rtab
			SELECT archive_id, [instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_userid],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id]
			FROM archive_instance_user_configurations_saved WHERE archive_id IN (
				SELECT MAX(archive_id)
				FROM archive_instance_user_configurations_saved
				WHERE archive_start <= @d AND archive_end > @d AND [user_configuration_id] NOT IN
				(SELECT [user_configuration_id] FROM  instance_user_configurations_saved  WHERE archive_start <= @d  )
				GROUP BY [user_configuration_id]
				)
			UNION 
			SELECT 0, [instance],[user_id],[group],[identifier],[value],[public],[name],[show_in_menu],[user_configuration_id],[visible_for_filter],[archive_userid],[archive_start],[visible_for_usergroup],[portfolio_default_view],[dashboard_id]
			FROM instance_user_configurations_saved 
			WHERE archive_start <= @d  
		RETURN
	END
			


GO



INSERT INTO archive_instance_user_configurations_saved (user_configuration_id, instance, [user_id], [group], identifier, [value], [public], [name], show_in_menu, visible_for_filter, visible_for_usergroup, portfolio_default_view, dashboard_id, archive_userid, archive_start, archive_end, archive_type)
SELECT iucs.user_configuration_id, iucs.instance, iucs.[user_id], iucs.[group], iucs.identifier, iucs.[value], iucs.[public], iucs.[name], iucs.show_in_menu, iucs.visible_for_filter, iucs.visible_for_usergroup, iucs.portfolio_default_view, iucs.dashboard_id, iucs.archive_userid, DATEADD(s, -1, iucs.archive_start), iucs.archive_start, 1
FROM instance_user_configurations_saved iucs
LEFT JOIN archive_instance_user_configurations_saved aiucs ON aiucs.user_configuration_id = iucs.user_configuration_id
WHERE aiucs.user_configuration_id IS NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[condition_user_groups_INSTEAD_OF_INSERT]
	ON [dbo].[condition_user_groups]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[condition_user_groups] ([condition_id],[item_id],[archive_start], archive_userid) 
	SELECT [condition_id],[item_id],[archive_start] 
	, @user_id
	FROM inserted

	INSERT INTO archive_condition_user_groups([condition_id],[item_id],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [condition_id],[item_id], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted



GO



INSERT INTO archive_condition_user_groups (condition_id, item_id, archive_userid, archive_start, archive_end, archive_type)
SELECT cug.condition_id, cug.item_id, cug.archive_userid, DATEADD(s, -1, cug.archive_start), cug.archive_start, 1
FROM condition_user_groups cug
LEFT JOIN archive_condition_user_groups acug ON acug.condition_id = cug.condition_id
AND acug.item_id = cug.item_id
WHERE acug.condition_id IS NULL



GO



CREATE TABLE [dbo].[archive_instance_user_configurations](
	[archive_id] [int] IDENTITY(1,1) NOT NULL,
	[archive_end] [datetime] NULL,
	[archive_type] [tinyint] NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[user_id] [int] NOT NULL,
	[group] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[identifier] [nvarchar](255) NULL,
	[archive_userid] [int] NOT NULL,
	[archive_start] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



GO



ALTER TABLE instance_user_configurations ADD archive_userid INT NOT NULL DEFAULT ((0))
ALTER TABLE instance_user_configurations ADD archive_start DATETIME NULL DEFAULT (GetUtcDate())
GO
UPDATE instance_user_configurations SET archive_userid = 1, archive_start = GetUtcDate()



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[instance_user_configurations_INSTEAD_OF_INSERT]
	ON [dbo].[instance_user_configurations]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[instance_user_configurations] ([instance],[user_id],[group],[value],[identifier],[archive_start],archive_userid) 
	SELECT [instance],[user_id],[group],[value],[identifier],[archive_start], @user_id FROM inserted
	
	INSERT INTO archive_instance_user_configurations([instance],[user_id],[group],[value],[identifier],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [instance],[user_id],[group],[value],[identifier], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[instance_user_configurations_DELETE]
	ON [dbo].[instance_user_configurations]
	FOR DELETE AS

	INSERT INTO archive_instance_user_configurations([instance],[user_id],[group],[value],[identifier],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [instance],[user_id],[group],[value],[identifier],[archive_userid],[archive_start], archive_end = getutcdate(), archive_type = 1 FROM deleted
			
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO archive_instance_user_configurations([instance],[user_id],[group],[value],[identifier],[archive_start],archive_end, archive_type, archive_userid)
	SELECT [instance],[user_id],[group],[value],[identifier],[archive_start], archive_end = getutcdate(), archive_type = 2, archive_userid = @user_id FROM deleted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[instance_user_configurations_AFTER_UPDATE]
	ON [dbo].[instance_user_configurations]
	AFTER UPDATE AS

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;
    
	UPDATE [dbo].[instance_user_configurations]
	SET archive_start = getutcdate(),
	archive_userid = @user_id
	WHERE CAST([instance] AS NVARCHAR) + CAST([user_id] AS NVARCHAR) + CAST([group] AS NVARCHAR) + CAST([identifier] AS NVARCHAR)  IN (SELECT DISTINCT CAST([instance] AS NVARCHAR) + CAST([user_id] AS NVARCHAR) + CAST([group] AS NVARCHAR) + CAST([identifier] AS NVARCHAR) FROM Inserted)
		
	INSERT INTO archive_instance_user_configurations([instance],[user_id],[group],[value],[identifier],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [instance],[user_id],[group],[value],[identifier],[archive_userid],[archive_start], archive_end = getutcdate(), archive_type = 1 FROM deleted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[get_instance_user_configurations]( @d datetime )
	RETURNS @rtab TABLE (archive_id INT, [instance] nvarchar(10) NOT NULL ,[user_id] int NOT NULL ,[group] nvarchar(255) NOT NULL ,[value] nvarchar(MAX) NOT NULL ,[identifier] nvarchar(255) NULL  ,[archive_userid] int NOT NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
	AS
	BEGIN	
		INSERT @rtab
			SELECT archive_id, [instance],[user_id],[group],[value],[identifier],[archive_userid],[archive_start]
			FROM archive_instance_user_configurations WHERE archive_id IN (
				SELECT MAX(archive_id)
				FROM archive_instance_user_configurations
				WHERE archive_start <= @d AND archive_end > @d
				AND CAST([instance] AS NVARCHAR) + CAST([user_id] AS NVARCHAR) + CAST([group] AS NVARCHAR) + CAST([identifier] AS NVARCHAR) NOT IN
				(SELECT CAST([instance] AS NVARCHAR) + CAST([user_id] AS NVARCHAR) + CAST([group] AS NVARCHAR) + CAST([identifier] AS NVARCHAR) FROM instance_user_configurations  WHERE archive_start <= @d)
				GROUP BY [instance], [user_id], [group], [identifier])
			UNION 
			SELECT 0, [instance],[user_id],[group],[value],[identifier],[archive_userid],[archive_start]
			FROM instance_user_configurations 
			WHERE archive_start <= @d  
		RETURN
	END



GO



INSERT INTO archive_instance_user_configurations (instance, [user_id], [group], [value], identifier, archive_userid, archive_start, archive_end, archive_type)
SELECT iuc.instance, iuc.[user_id], iuc.[group], iuc.[value], iuc.identifier, iuc.archive_userid, DATEADD(s, -1, iuc.archive_start), iuc.archive_start, 1
FROM instance_user_configurations iuc
LEFT JOIN archive_instance_user_configurations aiuc ON aiuc.instance = iuc.instance
AND aiuc.[user_id] = iuc.[user_id]
AND aiuc.[group] = iuc.[group]
AND aiuc.identifier = iuc.identifier
WHERE aiuc.instance IS NULL



GO



CREATE TABLE [dbo].[archive_process_baseline](
	[archive_id] [int] IDENTITY(1,1) NOT NULL,
	[archive_end] [datetime] NULL,
	[archive_type] [tinyint] NOT NULL,
	[process_baseline_id] [int] NOT NULL,
	[instance] [nvarchar](10) NULL,
	[process] [nvarchar](50) NULL,
	[process_item_id] [int] NULL,
	[process_group_id] [int] NULL,
	[baseline_date] [datetime] NOT NULL,
	[creator_user_item_id] [int] NULL,
	[description] [nvarchar](50) NULL,
	[archive_userid] [int] NOT NULL,
	[archive_start] [datetime] NULL,
	[control_name] [nvarchar](255) NULL
)



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_baseline_INSTEAD_OF_INSERT]
	ON [dbo].[process_baseline]
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO [dbo].[process_baseline] ([process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start]) 
	SELECT [process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name], @user_id, [archive_start] FROM inserted
	
	INSERT INTO archive_process_baseline([process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name], @user_id, DATEADD(s, -1, [archive_start]), [archive_start], 1 FROM inserted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_baseline_DELETE]
	ON [dbo].[process_baseline]
	FOR DELETE AS

	INSERT INTO archive_process_baseline([process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start], archive_end = getutcdate(), archive_type = 1 FROM deleted
			
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1;

	INSERT INTO archive_process_baseline([process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_start],archive_end, archive_type, archive_userid)
	SELECT [process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_start], archive_end = getutcdate(), archive_type = 2, archive_userid = @user_id FROM deleted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_baseline_AFTER_UPDATE]
	ON [dbo].[process_baseline]
	AFTER UPDATE AS
	
	INSERT INTO archive_process_baseline([process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start],archive_end, archive_type)
	SELECT [process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start], archive_end = getutcdate(), archive_type = 1 FROM deleted



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[get_process_baseline]( @d datetime )
	RETURNS @rtab TABLE (archive_id INT, [process_baseline_id] int NOT NULL ,[instance] NVARCHAR(10) NULL, [process] NVARCHAR(50), [process_item_id] INT NULL, [process_group_id] INT NULL, [baseline_date] DATETIME NOT NULL, [creator_user_item_id] INT NULL, [description] NVARCHAR(50), [control_name] NVARCHAR(255) ,[archive_userid] int NOT NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
	AS
	BEGIN	
		INSERT @rtab
			SELECT archive_id, [process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start]
			FROM archive_process_baseline WHERE archive_id IN (
				SELECT MAX(archive_id)
				FROM archive_process_baseline
				WHERE archive_start <= @d AND archive_end > @d AND [process_baseline_id] NOT IN
				(SELECT [process_baseline_id] FROM process_baseline WHERE archive_start < @d  )
				GROUP BY [process_baseline_id]
				)
			UNION 
			SELECT 0, [process_baseline_id],[instance],[process],[process_item_id],[process_group_id],[baseline_date],[creator_user_item_id],[description],[control_name],[archive_userid],[archive_start] 
			FROM process_baseline
			WHERE archive_start < @d  
		RETURN
	END



GO



INSERT INTO archive_process_baseline (process_baseline_id, instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id, [description], control_name, archive_userid, archive_start, archive_end, archive_type)
SELECT pb.process_baseline_id, pb.instance, pb.process, pb.process_item_id, pb.process_group_id, pb.baseline_date, pb.creator_user_item_id, pb.[description], pb.control_name, pb.archive_userid, DATEADD(s, -1, pb.archive_start), pb.archive_start, 1
FROM process_baseline pb
LEFT JOIN archive_process_baseline apb ON apb.process_baseline_id = pb.process_baseline_id
WHERE apb.process_baseline_id IS NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[get_item_data_process_selections_own](@d DATETIME, @itemId INT)
RETURNS @rtab TABLE ([item_id] int NOT NULL ,[item_column_id] int NOT NULL ,[selected_item_id] int NOT NULL ,[link_item_id] int NULL ,[archive_userid] int NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
AS
BEGIN	
	INSERT @rtab
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start]
		FROM archive_item_data_process_selections WHERE archive_id IN (
			SELECT MAX(archive_id)
			FROM archive_item_data_process_selections
			WHERE (item_id = @itemId OR selected_item_id = @itemId OR link_item_id = @itemId) AND archive_start <= @d AND archive_end > @d AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN
			(SELECT CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) FROM item_data_process_selections WHERE (item_id = @itemId OR selected_item_id = @itemId OR link_item_id = @itemId) AND archive_start <= @d)
			GROUP BY [item_column_id],[item_id],[selected_item_id]
			)
		UNION 
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start] 
		FROM item_data_process_selections 
		WHERE (item_id = @itemId OR selected_item_id = @itemId OR link_item_id = @itemId) AND archive_start <= @d
	RETURN
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_restore_item_recursive]
	@item_id INT,
	@archive_id INT = NULL,
	@archive_start DATETIME = NULL,
	@restored_items_in NVARCHAR(MAX) = NULL,
	@restored_items_out NVARCHAR(MAX) OUTPUT
AS
BEGIN
	DECLARE @user_id INT
	SELECT @user_id = CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
	IF NOT (@user_id >= 0 OR @user_id = -1) OR @user_id IS NULL THROW 51000, 'Archive User Id has not been set', 1

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @instance NVARCHAR(10)
	DECLARE @process NVARCHAR(50)
	DECLARE @archive INT
	DECLARE @msg NVARCHAR(MAX)
	DECLARE @guid UNIQUEIDENTIFIER
	DECLARE @columns NVARCHAR(MAX)
	DECLARE @deleted_by INT
	DECLARE @deleted_at DATETIME
	DECLARE @mode TINYINT
	DECLARE @item_column_id INT
	DECLARE @data_additional NVARCHAR(MAX)
	DECLARE @data_additional2 NVARCHAR(MAX)
	DECLARE @start_pos INT
	DECLARE @end_pos INT

	DECLARE @child_items_cursor CURSOR
	DECLARE @child_items_item_id INT

	DECLARE @parent_items_cursor CURSOR
	DECLARE @parent_items_item_id INT

	DECLARE @table_columns_cursor CURSOR
	DECLARE @table_items_cursor CURSOR
	DECLARE @table_item_column_id NVARCHAR(255)
	DECLARE @table_instance NVARCHAR(10)
	DECLARE @table_process NVARCHAR(50)
	DECLARE @table_column_name NVARCHAR(50)
	DECLARE @table_data_type INT
	DECLARE @table_item_id INT
	CREATE TABLE #table_items (item_id INT)
	CREATE TABLE #table_item_column_ids (item_column_id INT)
	
	DECLARE @constraints_cursor CURSOR
	DECLARE @constraints_table_name NVARCHAR(50)
	DECLARE @constraints_column_name NVARCHAR(50)
	DECLARE @constraints_item_id INT

	-- Discover archive_id and archive_start and mode
	IF @archive_id IS NOT NULL OR @archive_start IS NOT NULL BEGIN
		SELECT @instance = instance, @process = process FROM items WHERE item_id = @item_id

		IF @instance IS NULL OR @process IS NULL BEGIN
			SELECT @instance = instance, @process = process, @archive_start = deleted_date, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

			SET @mode = 1 -- Restore deleted item with historic values
		END
		ELSE BEGIN
			SET @mode = 0 -- Restore item with historic values
		END

		IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
			IF @archive_id IS NOT NULL BEGIN
				SET @sql = 'SELECT @archive_start = archive_start
							FROM archive__' + @instance + '_' + @process + ' 
							WHERE archive_id = @archive_id'
				EXEC sp_executesql @sql, N'@archive_id INT, @archive_start DATETIME OUTPUT', @archive_id, @archive_start OUTPUT
			END
			ELSE IF @archive_start IS NOT NULL BEGIN
				SET @sql = 'SELECT @archive_id = archive_id
							FROM get__' + @instance + '_' + @process + '_with_id(@item_id, @archive_start) '
				EXEC sp_executesql @sql, N'@item_id INT, @archive_start DATETIME, @archive_id INT OUTPUT', @item_id, @archive_start, @archive_id OUTPUT
			END
		END
	END
	ELSE BEGIN
		SET @mode = 1 -- Restore deleted item with latest values

		SELECT @instance = instance, @process = process, @archive_start = deleted_date, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

		IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
			SET @sql = 'SELECT TOP 1 @archive_id = archive_id
						FROM archive__' + @instance + '_' + @process + ' 
						WHERE archive_type = 2 AND item_id = @item_id 
						ORDER BY archive_id DESC'
			EXEC sp_executesql @sql, N'@item_id INT, @archive_id INT OUTPUT', @item_id, @archive_id OUTPUT
		END
	END

	IF @mode = 1 AND @archive_start IS NOT NULL AND @archive_start <= '2021-06-07' THROW 51000, 'Cannot restore. Item is too old to restore', 1

	IF @instance IS NOT NULL AND @process IS NOT NULL AND @archive_id IS NOT NULL AND @archive_start IS NOT NULL BEGIN
		SET @sql = 'SELECT @archive = archive FROM item_tables WHERE name = ''_' + @instance + '_' + @process + ''''
		EXEC sp_executesql @sql, N'@archive INT OUTPUT', @archive OUTPUT
		SET @msg = 'Cannot restore. Process ' + @process + ' doesn''t use archive'
		IF ISNULL(@archive, 1) = 0 THROW 51000, @msg, 1

		IF @restored_items_in IS NOT NULL BEGIN
			IF @archive_id = 0 BEGIN
				SET @sql = 'SELECT @deleted_at = archive_start, @deleted_by = archive_userid
							FROM _' + @instance + '_' + @process + ' 
							WHERE item_id = @item_id'
				EXEC sp_executesql @sql, N'@item_id INT, @deleted_at DATETIME OUTPUT, @deleted_by INT OUTPUT', @item_id, @deleted_at OUTPUT, @deleted_by OUTPUT
			END
			ELSE BEGIN
				SET @sql = 'SELECT @deleted_at = archive_start, @deleted_by = archive_userid
							FROM archive__' + @instance + '_' + @process + ' 
							WHERE archive_id = @archive_id'
				EXEC sp_executesql @sql, N'@archive_id INT, @deleted_at DATETIME OUTPUT, @deleted_by INT OUTPUT', @archive_id, @deleted_at OUTPUT, @deleted_by OUTPUT
			END
			SET @restored_items_out = @restored_items_in + N',{"item_id":' + CAST(@item_id AS NVARCHAR) + ',"instance":"' + @instance + '","process":"' + @process + '","restored_by":' + CAST(@deleted_by AS NVARCHAR) + ',"restored_at":"' + FORMAT(@deleted_at,'yyyy-MM-ddTHH:mm:ss.fff') + '","deleted":' + CAST(@mode AS NVARCHAR) + '}'
		END

		-- Remove indications that item is deleted
		IF @mode = 1 BEGIN
			DELETE FROM items_deleted WHERE item_id = @item_id
		END

		-- Restore item
		IF @mode = 1 BEGIN
			EXEC sp_set_session_context @key = N'restore_item_id', @value = @item_id
			EXEC sp_set_session_context @key = N'restore_guid', @value = @guid
			INSERT INTO items (instance, process, deleted) VALUES (@instance, @process, 0)
		END

		-- Restore item's data in row
		SELECT @columns = STUFF((SELECT ', t.' + COLUMN_NAME + ' = a.' + COLUMN_NAME 
								 FROM INFORMATION_SCHEMA.COLUMNS 
								 WHERE TABLE_NAME = '_' + @instance + '_' + @process AND COLUMN_NAME NOT IN ('item_id', 'order_no', 'archive_userid', 'archive_start') 
								 GROUP BY COLUMN_NAME 
								 FOR XML PATH('')), 1, 2, '')
		IF @columns <> '' BEGIN
			SET @sql = 'UPDATE t SET ' + @columns + ' 
						FROM _' + @instance + '_' + @process + ' t 
						INNER JOIN archive__' + @instance + '_' + @process + ' a ON a.item_id = t.item_id AND a.archive_id = @archive_id
						WHERE t.item_id = @item_id'
			EXEC sp_executesql @sql, N'@item_id INT, @archive_id INT', @item_id, @archive_id
		END
		
		-- Restore by constraints
		SET @constraints_cursor = CURSOR FAST_FORWARD LOCAL FOR
		SELECT ccu.TABLE_NAME, ccu.COLUMN_NAME 
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
		INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		WHERE UNIQUE_CONSTRAINT_NAME = 'PK_items' AND SUBSTRING(ccu.TABLE_NAME, 1, 1) = '_'
		OPEN @constraints_cursor
		FETCH NEXT FROM @constraints_cursor INTO @constraints_table_name, @constraints_column_name
		WHILE @@fetch_status = 0 BEGIN
			SET @sql = 'SELECT @constraints_item_id = item_id FROM get_' + @constraints_table_name + '(@archive_start) WHERE ' + @constraints_column_name + ' = @item_id'
			EXEC sp_executesql @sql, N'@item_id INT, @archive_start DATETIME, @constraints_item_id INT OUTPUT', @item_id, @archive_start, @constraints_item_id OUTPUT
			
			IF @constraints_item_id > 0 EXEC app_restore_item_recursive @constraints_item_id, NULL, @archive_start, @restored_items_out, @restored_items_out OUTPUT
			
			FETCH NEXT FROM @constraints_cursor INTO @constraints_table_name, @constraints_column_name
		END
		
		-- Restore child items by item_columns.use_fk = 2
		SET @child_items_cursor = CURSOR FOR SELECT selected_item_id FROM get_item_data_process_selections_with_id(@archive_start, @item_id) WHERE item_column_id IN (SELECT item_column_id FROM item_columns WHERE use_fk = 2)
		OPEN @child_items_cursor
		FETCH NEXT FROM @child_items_cursor INTO @child_items_item_id
		WHILE @@fetch_status = 0 BEGIN
			EXEC app_restore_item_recursive @child_items_item_id, NULL, @archive_start, @restored_items_out, @restored_items_out OUTPUT
			
			FETCH NEXT FROM @child_items_cursor INTO @child_items_item_id
		END

		-- Restore parent items by item_columns.use_fk = 3
		SET @parent_items_cursor = CURSOR FOR SELECT item_id FROM get_item_data_process_selections_with_sel_id(@archive_start, @item_id) WHERE item_column_id IN (SELECT item_column_id FROM item_columns WHERE use_fk = 3)
		OPEN @parent_items_cursor
		FETCH NEXT FROM @parent_items_cursor INTO @parent_items_item_id
		WHILE @@fetch_status = 0 BEGIN
			EXEC app_restore_item_recursive @parent_items_item_id, NULL, @archive_start, @restored_items_out, @restored_items_out OUTPUT
			
			FETCH NEXT FROM @parent_items_cursor INTO @parent_items_item_id
		END
				
		-- Restore item_data_process_selections
		IF @mode = 0 DELETE FROM item_data_process_selections WHERE item_id = @item_id OR selected_item_id = @item_id OR link_item_id = @item_id 

		INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id)
		SELECT idps.item_id, idps.item_column_id, idps.selected_item_id, idps.link_item_id
		FROM get_item_data_process_selections_own(@archive_start, @item_id) idps
		LEFT JOIN items_deleted i_id ON i_id.item_id = idps.item_id
		LEFT JOIN item_columns ic ON ic.item_column_id = idps.item_column_id
		LEFT JOIN items_deleted s_id ON s_id.item_id = idps.selected_item_id
		LEFT JOIN items_deleted l_id ON l_id.item_id = idps.link_item_id
		WHERE i_id.item_id IS NULL
		AND ic.item_column_id IS NOT NULL
		AND s_id.item_id IS NULL
		AND l_id.item_id IS NULL

		-- Restore table columns data (18, 21) if parent process column is int/process/list and table process column is item_id
		SET @table_columns_cursor = CURSOR FOR SELECT data_additional2 FROM item_columns WHERE instance = @instance AND process = @process AND data_type in (18, 21) AND CHARINDEX('"parentFieldName"', data_additional2) = 0 
		OPEN @table_columns_cursor
		FETCH NEXT FROM @table_columns_cursor INTO @data_additional2
		WHILE @@fetch_status = 0 BEGIN
			SET @start_pos = CHARINDEX('"itemColumnId"', @data_additional2)
			IF @start_pos = 0 BEGIN
				SET @start_pos = CHARINDEX('"ownerColumnId"', @data_additional2) + 1
			END
			IF @start_pos > 1 BEGIN
				SET @start_pos = @start_pos + 15
				SET @end_pos = CHARINDEX(',', @data_additional2, @start_pos)
				IF @end_pos = 0 BEGIN
					SET @end_pos = CHARINDEX('}', @data_additional2, @start_pos)
				END
				SET @table_item_column_id = SUBSTRING(@data_additional2, @start_pos, @end_pos - @start_pos)
				IF (SELECT COUNT (*) FROM #table_item_column_ids WHERE item_column_id = @table_item_column_id) = 0 BEGIN
					INSERT INTO #table_item_column_ids (item_column_id) VALUES (@table_item_column_id)
					IF ISNUMERIC(@table_item_column_id) = 1 BEGIN
						SELECT @table_instance = instance, @table_process = process, @table_column_name = [name], @table_data_type = data_type, @data_additional = data_additional FROM item_columns WHERE item_column_id = @table_item_column_id
						IF @table_data_type = 1 BEGIN
							SET @sql = 'INSERT INTO #table_items SELECT item_id FROM get__' + @table_instance + '_' + @table_process + '(@archive_start) WHERE ' + @table_column_name + ' = @item_id'
							EXEC sp_executesql @sql, N'@archive_start DATETIME, @item_id INT', @archive_start, @item_id
							SET @table_items_cursor = CURSOR FOR SELECT item_id FROM #table_items
							OPEN @table_items_cursor
							FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							WHILE @@fetch_status = 0 BEGIN
								EXEC app_restore_item_recursive @table_item_id, NULL, @archive_start, @restored_items_out, @restored_items_out OUTPUT
								
								FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							END
							DELETE FROM #table_items
						END
						ELSE IF @table_data_type IN (5, 6) AND @data_additional = @process BEGIN
							DELETE FROM item_data_process_selections WHERE item_column_id = @table_item_column_id AND selected_item_id = @item_id
							
							INSERT INTO #table_items SELECT item_id FROM get_item_data_process_selections_with_sel_ids(@archive_start, @table_item_column_id, @item_id)
							SET @table_items_cursor = CURSOR FOR SELECT item_id FROM #table_items
							OPEN @table_items_cursor
							FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							WHILE @@fetch_status = 0 BEGIN
								EXEC app_restore_item_recursive @table_item_id, NULL, @archive_start, @restored_items_out, @restored_items_out OUTPUT
							
								FETCH NEXT FROM @table_items_cursor INTO @table_item_id
							END
							DELETE FROM #table_items
						END
					END
				END
				FETCH NEXT FROM @table_columns_cursor INTO @data_additional2
			END
		END

		-- Restore item_subitems
		IF @mode = 0 DELETE FROM item_subitems WHERE parent_item_id = @item_id OR item_id = @item_id

		INSERT INTO item_subitems (parent_item_id, item_id, favourite)
		SELECT parent_item_id, item_id, favourite FROM get_item_subitems(@archive_start)
		WHERE parent_item_id = @item_id OR item_id = @item_id

		-- Restore process_tabs_subprocess_data
		IF @mode = 0 DELETE FROM process_tabs_subprocess_data WHERE item_id = @item_id

		INSERT INTO process_tabs_subprocess_data (process_tab_subprocess_id, parent_process_item_id, item_id)
		SELECT process_tab_subprocess_id, parent_process_item_id, item_id FROM get_process_tabs_subprocess_data(@archive_start)
		WHERE item_id = @item_id

		-- Restore task_durations
		IF @mode = 0 DELETE FROM task_durations WHERE task_item_id = @item_id OR resource_item_id = @item_id

		INSERT INTO task_durations (task_item_id, resource_item_id, [date], [hours])
		SELECT task_item_id, resource_item_id, [date], [hours] FROM get_task_durations(@archive_start)
		WHERE task_item_id = @item_id OR resource_item_id = @item_id

		-- Restore allocation_durations
		IF @mode = 0 DELETE FROM allocation_durations WHERE allocation_item_id = @item_id

		INSERT INTO allocation_durations (allocation_item_id, [date], [hours])
		SELECT allocation_item_id, [date], [hours] FROM get_allocation_durations(@archive_start)
		WHERE allocation_item_id = @item_id

		-- Restore allocation_comments
		IF @mode = 0 DELETE FROM allocation_comments WHERE allocation_item_id = @item_id OR author_item_id = @item_id

		SET IDENTITY_INSERT allocation_comments ON

		INSERT INTO allocation_comments (allocation_comment_id, allocation_item_id, comment, created_at, author_item_id)
		SELECT allocation_comment_id, allocation_item_id, comment, created_at, author_item_id FROM get_allocation_comments(@archive_start)
		WHERE allocation_item_id = @item_id OR author_item_id = @item_id
		
		SET IDENTITY_INSERT allocation_comments OFF

		-- Restore instance_user_configurations_saved
		IF @mode = 0 DELETE FROM instance_user_configurations_saved WHERE [user_id] = @item_id

		SET IDENTITY_INSERT instance_user_configurations_saved ON

		INSERT INTO instance_user_configurations_saved (instance, [user_id], [group], identifier, [value], [public], [name], show_in_menu, user_configuration_id, visible_for_filter, visible_for_usergroup, portfolio_default_view, dashboard_id)
		SELECT instance, [user_id], [group], identifier, [value], [public], [name], show_in_menu, user_configuration_id, visible_for_filter, visible_for_usergroup, portfolio_default_view, dashboard_id FROM get_instance_user_configurations_saved(@archive_start)
		WHERE [user_id] = @item_id
		
		SET IDENTITY_INSERT instance_user_configurations_saved OFF

		-- Restore instance_user_configurations
		IF @mode = 0 DELETE FROM instance_user_configurations WHERE [user_id] = @item_id

		INSERT INTO instance_user_configurations (instance, [user_id], [group], [value], identifier)
		SELECT instance, [user_id], [group], [value], identifier FROM get_instance_user_configurations(@archive_start)
		WHERE [user_id] = @item_id

		-- Restore condition_user_groups
		IF @mode = 0 DELETE FROM condition_user_groups WHERE item_id = @item_id

		INSERT INTO condition_user_groups (condition_id, item_id)
		SELECT condition_id, item_id FROM get_condition_user_groups(@archive_start)
		WHERE item_id = @item_id

		-- Restore process_baseline
		IF @mode = 0 DELETE FROM process_baseline WHERE process_item_id = @item_id

		SET IDENTITY_INSERT process_baseline ON

		INSERT INTO process_baseline (process_baseline_id, instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id, [description], control_name)
		SELECT process_baseline_id, instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id, [description], control_name FROM get_process_baseline(@archive_start)
		WHERE process_item_id = @item_id

		SET IDENTITY_INSERT process_baseline OFF
	END
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_restore_item]
	@item_id INT,
	@archive_id INT = NULL,
	@archive_start DATETIME = NULL,
	@get_restored_items TINYINT = 0,
	@restored_items_out NVARCHAR(MAX) OUTPUT
AS
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION
			DECLARE @restored_items_in NVARCHAR(MAX) = NULL
			IF @get_restored_items = 1 SET @restored_items_in = ''
			EXEC app_restore_item_recursive @item_id, @archive_id, @archive_start, @restored_items_in, @restored_items_out OUTPUT
			IF @restored_items_out IS NOT NULL SET @restored_items_out = '[' + SUBSTRING(@restored_items_out, 2, LEN(@restored_items_out)) + ']'
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK TRAN
		DECLARE @msg NVARCHAR(2048) = 'ROLLBACK - ' + ERROR_MESSAGE();
		THROW 51000, @msg, 1
	END CATCH
END



GO



DROP PROCEDURE [dbo].[app_restore_item_transaction]