﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[get_process_dashboard_chart_map]( @d datetime )
		RETURNS @rtab TABLE (archive_id INT, [process_dashboard_chart_id] int NOT NULL ,[process_dashboard_id] int NOT NULL ,[order_no] nvarchar(255) NOT NULL ,[archive_userid] int NOT NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
		AS
		BEGIN	
			INSERT @rtab
				SELECT archive_id, [process_dashboard_chart_id],[process_dashboard_id],[order_no],[archive_userid],[archive_start]
				FROM archive_process_dashboard_chart_map WHERE archive_id IN (
					SELECT MAX(archive_id)
					FROM archive_process_dashboard_chart_map
					WHERE archive_start <= @d AND archive_end > @d AND [process_dashboard_chart_id] NOT IN
					(SELECT [process_dashboard_chart_id] FROM  process_dashboard_chart_map  WHERE archive_start < @d  )
					GROUP BY [process_dashboard_chart_id]
					)
				UNION 
				SELECT 0, [process_dashboard_chart_id],[process_dashboard_id],[order_no],[archive_userid],[archive_start] 
				FROM process_dashboard_chart_map
				WHERE archive_start < @d  
			RETURN
		END