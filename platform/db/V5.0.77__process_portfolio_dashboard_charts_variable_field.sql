ALTER TABLE process_dashboard_charts ADD variable NVARCHAR(50)

GO

DECLARE @instance NVARCHAR(50) = ''
DECLARE @process NVARCHAR(50) = ''
DECLARE @variable NVARCHAR(50) = ''
DECLARE @translation NVARCHAR(50) = ''
DECLARE @duplicateCount INT = 0

DECLARE Dashboard CURSOR FOR SELECT PAD.name as translation, P.instance, PAD.process FROM process_dashboard_charts pad LEFT JOIN processes p ON p.process = pad.process WHERE P.INSTANCE IS NOT NULL
OPEN Dashboard
FETCH NEXT FROM Dashboard into @translation, @instance, @process

WHILE @@FETCH_STATUS = 0
   BEGIN
  		SET @variable = LOWER(@translation)
  		SET @variable = SUBSTRING(@variable, 0, 50)
  		SET @variable = REPLACE(@variable, SUBSTRING(@translation,PATINDEX('%[^a-z0-9]%',@variable), 1), '_')
  		SET @variable = REPLACE(@variable, '=', '_')
  		SET @variable = REPLACE(@variable, '(', '_')
  		SET @variable = REPLACE(@variable, ')', '_')
  		SET @variable = REPLACE(@variable, '__', '_')
  		SET @variable = 'CHART_' + @variable
  		
		WHILE (SELECT COUNT(*) AS count FROM process_translations WHERE variable = @variable) > 0
  		BEGIN
	  		SET @duplicateCount = @duplicateCount + 1
	  		
	  		IF (LEN(@variable) > 48)
  			BEGIN
				SET @variable = (SELECT SUBSTRING(@variable, 0, 48)) + '_' +  CAST(@duplicateCount AS nvarchar)
  			END
  			ELSE
	  		BEGIN
				IF (try_cast(RIGHT(@variable, 1) as int) >= 0 )
					BEGIN
						SET @variable = REPLACE(@variable, RIGHT(@variable, 2), '')
					END
					ELSE IF (try_cast(RIGHT(@variable, 2) as int) >= 0)
					BEGIN
						SET @variable = REPLACE(@variable, RIGHT(@variable, 2), '')
					END
					
					SET @variable = @variable + '_' +  CAST(@duplicateCount AS nvarchar)
  			END
  			
  		END
		 
  		EXEC app_set_archive_user_id 1	
    	UPDATE process_dashboard_charts SET variable = @variable WHERE name = @translation AND process = @process AND instance = @instance
  		INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@instance, @process, 'en-GB', @variable, @translation)

  		SELECT @variable
  		SET @duplicateCount = 0
      	FETCH NEXT FROM Dashboard into @translation, @instance, @process
   END
CLOSE Dashboard
DEALLOCATE Dashboard

EXEC app_ensure_archive 'process_dashboard_charts'
GO
