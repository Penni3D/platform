﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[process_portfolio_columns]') AND name = 'is_default') 
    ALTER TABLE process_portfolio_columns
          ADD is_default tinyint NOT NULL
          CONSTRAINT ppc_is_default_df DEFAULT 1
    WITH VALUES
