ALTER PROCEDURE [dbo].[items_AddColumn]
	@TableName NVARCHAR(255), @ColumnName NVARCHAR(50), @DataType TINYINT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @DataTypeName NVARCHAR(255)
	IF @DataType = 0 BEGIN
		SET @DataTypeName = 'NVARCHAR(255)'
	END ELSE IF @DataType = 1 BEGIN
		SET @DataTypeName = 'INT'
	END ELSE IF @DataType = 2 BEGIN
		SET @DataTypeName = 'FLOAT'
	END ELSE IF @DataType = 3 BEGIN
		SET @DataTypeName = 'DATE'
	END ELSE IF @DataType = 4 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 5 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 6 BEGIN
		SET @DataTypeName = 'INT'
	END ELSE IF @DataType = 7 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 10 BEGIN
		SET @DataTypeName = 'NVARCHAR(255)'
	END ELSE IF @DataType = 13 BEGIN
		SET @DataTypeName = 'DATETIME'
	END ELSE IF @DataType = 14 BEGIN
		SET @DataTypeName = 'FLOAT'
	END ELSE IF @DataType = 23 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END
	
	DECLARE @Sql NVARCHAR(MAX) = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataTypeName
	PRINT @Sql
	EXEC sp_executesql @Sql 

	exec app_ensure_archive @TableName
END