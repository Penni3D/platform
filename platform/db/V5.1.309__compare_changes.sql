UPDATE processes SET compare = 2 WHERE process = 'notification_tags'



GO



ALTER TABLE process_action_rows ADD [guid] UNIQUEIDENTIFIER DEFAULT newsequentialid() NOT NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processActionRows_GetId]
(
 @instance NVARCHAR(10),
 @guid uniqueidentifier
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 
 SELECT TOP 1 @id = par.process_action_row_id 
 FROM process_action_rows par 
 WHERE (SELECT TOP 1 pa.instance FROM process_actions pa WHERE pa.process_action_id = par.process_action_id) = @instance 
 AND par.[guid] = @guid
 
 RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processActionRows_GetGuid]
(
 @processActionRowId int
)
RETURNS uniqueidentifier
AS
BEGIN
 DECLARE @var uniqueidentifier
 SELECT TOP 1 @var = [guid] FROM process_action_rows WHERE process_action_row_id = @processActionRowId
 RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[conditionsGroups_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = condition_group_id FROM conditions_groups WHERE conditions_groups.instance = @instance and variable = @variable
 RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[conditionsGroups_GetVariable]
(
 @conditionGroupId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 SELECT TOP 1 @var = variable FROM conditions_groups WHERE condition_group_id = @conditionGroupId
 RETURN @var
END