DECLARE @instance NVARCHAR(10)
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
DECLARE @container_id INT
DECLARE @ic_id INT
DECLARE @code NVARCHAR(5)
DECLARE @selected_item_id INT
DECLARE @sql NVARCHAR(255)
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0 BEGIN
    SELECT TOP 1 @container_id = process_container_id FROM process_containers WHERE instance = @instance AND process = 'user_group' ORDER BY process_container_id ASC
    EXEC app_ensure_column @instance, 'user_group', 'compare', @container_id, 6, 1, 0, null, 0, 0, 'list_generic_yes_no', null, 0, @item_column_id = @ic_id OUTPUT

    UPDATE process_translations SET translation = 'Compare' WHERE instance = @instance AND process = 'user_group' AND variable = 'IC_USER_GROUP_COMPARE' AND code = 'en-GB'

    SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'fi-FI'
    IF @code = 'fi-FI' BEGIN
        INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'IC_USER_GROUP_COMPARE', 'Vertaile', 'fi-FI')
    END

    SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'en-US'
    IF @code = 'en-US' BEGIN
        INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'IC_USER_GROUP_COMPARE', 'Compare', 'en-US')
    END

    SELECT @code = code FROM process_translations WHERE instance = @instance AND code = 'de-DE'
    IF @code = 'de-DE' BEGIN
        INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group', 'IC_USER_GROUPE_COMPARE', 'Compare', 'de-DE')
    END

    SET @sql = 'SELECT TOP 1 @selected_item_id = item_id FROM _' + @instance + '_list_generic_yes_no WHERE value = 0 ORDER BY item_id ASC'
    EXECUTE sp_executesql @sql, N'@selected_item_id NVARCHAR(255) OUTPUT', @selected_item_id = @selected_item_id OUTPUT
    EXEC('INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) SELECT item_id, ' + @ic_id + ', '  + @selected_item_id + ' FROM _' + @instance + '_user_group')

    FETCH  NEXT FROM instance_cursor INTO @instance
END