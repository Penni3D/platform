SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[conditions_replace]
(
	@sourceInstance nvarchar(10)
	,@targetInstance nvarchar(10)
)
AS
BEGIN
	DECLARE @condition_id INT
	DECLARE @instance nvarchar(10)
	DECLARE @process nvarchar(50)
	DECLARE @VARIABLE nvarchar(255)
	DECLARE @condition_json nvarchar(max)
	DECLARE @order_no nvarchar(max)
	DECLARE @disabled tinyint

	DECLARE @conditionCursor CURSOR 
	SET @conditionCursor = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT condition_id, instance, process, variable, condition_json, order_no, [disabled] FROM conditions WHERE instance = @sourceInstance

	OPEN @conditionCursor
	FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json, @order_no, @disabled
	WHILE (@@FETCH_STATUS = 0) BEGIN
		declare @json as nvarchar(max)
		declare @json2 as nvarchar(max)
		exec dbo.condition_IdReplaceProc @condition_json,  @replaced_json = @json output
		exec dbo.condition_VarReplaceProc @sourceInstance, @sourceInstance, @json, @replaced_json = @json2 output
		insert into #conditions (condition_id, instance, process, variable, condition_json, replaced_condition_json, order_no, [disabled]) values (@condition_id, @targetInstance, @process, @variable, @json, @json2, @order_no, @disabled)
		FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json, @order_no, @disabled
	END
END