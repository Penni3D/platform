EXEC app_set_archive_user_id 1
DECLARE @instance NVARCHAR(10)
DECLARE @sql NVARCHAR(MAX)
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
DECLARE @process_container_id INT
DECLARE @item_column_id INT
DECLARE @selected_item_id INT
DECLARE @process_container_column_id INT
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@FETCH_STATUS = 0 BEGIN
	SET @item_column_id = 0
	SELECT @item_column_id = item_column_id FROM item_columns WHERE instance = @instance AND process = 'user_group' AND [name] = 'group_type'

	---	

	SET @selected_item_id = 0
	SET @sql = 'SELECT @selected_item_id = item_id FROM _' + @instance + '_user_group_type WHERE is_admin_group IS NULL OR is_admin_group <> 1'
	EXEC sp_executesql @sql, N'@selected_item_id INT OUTPUT', @selected_item_id OUTPUT
	
	IF @item_column_id > 0 AND @selected_item_id > 0 BEGIN
		SET @sql = 'INSERT INTO item_data_process_selections (item_column_id, item_id, selected_item_id) 
					SELECT @item_column_id, item_id, @selected_item_id FROM _' + @instance + '_user_group WHERE item_id NOT IN 
					(SELECT item_id FROM item_data_process_selections WHERE item_column_id = @item_column_id)'
		EXEC sp_executesql @sql, N'@item_column_id INT, @selected_item_id INT', @item_column_id, @selected_item_id
	END

	---

	SET @process_container_id = 0
	SELECT @process_container_id = process_container_id FROM process_portfolio_dialogs WHERE instance = @instance AND process = 'user_group'
	
	SET @process_container_column_id = NULL
	SELECT @process_container_column_id = process_container_column_id FROM process_container_columns WHERE process_container_id = @process_container_id AND item_column_id = @item_column_id

	IF @process_container_column_id IS NULL AND @process_container_id > 0 AND @item_column_id > 0 BEGIN
		INSERT INTO process_container_columns (process_container_id, item_column_id, [validation], order_no) 
		VALUES (@process_container_id, @item_column_id, '{"Precision":null,"Min":null,"Max":null,"Rows":null,"Spellcheck":null,"Suffix":null,"ParentId":null,"DateRange":null,"CalendarConnection":null,"AfterMonths":null,"BeforeMonths":null,"Unselectable":null,"Required":true,"ReadOnly":null,"Label":"{}"}', 'zzz')
	END
	
	FETCH NEXT FROM instance_cursor INTO @instance
END
CLOSE instance_cursor
DEALLOCATE instance_cursor