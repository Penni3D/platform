IF (OBJECT_ID('app_ensure_process_ah_costs') IS NOT NULL) DROP PROCEDURE app_ensure_process_ah_costs
GO
IF (OBJECT_ID('app_ensure_process_benefits') IS NOT NULL) DROP PROCEDURE app_ensure_process_benefits
GO
IF (OBJECT_ID('app_ensure_process_costsKTN') IS NOT NULL) DROP PROCEDURE app_ensure_process_costsKTN
GO
IF (OBJECT_ID('app_ensure_process_deliverables') IS NOT NULL) DROP PROCEDURE app_ensure_process_deliverables
GO
IF (OBJECT_ID('app_ensure_process_deliverables_dependencies') IS NOT NULL) DROP PROCEDURE app_ensure_process_deliverables_dependencies
GO
IF (OBJECT_ID('app_ensure_process_effort_matrix') IS NOT NULL) DROP PROCEDURE app_ensure_process_effort_matrix
GO
IF (OBJECT_ID('app_ensure_process_evaluation_matrix') IS NOT NULL) DROP PROCEDURE app_ensure_process_evaluation_matrix
GO
IF (OBJECT_ID('app_ensure_process_hel_costs') IS NOT NULL) DROP PROCEDURE app_ensure_process_hel_costs
GO
IF (OBJECT_ID('app_ensure_process_hel_resources') IS NOT NULL) DROP PROCEDURE app_ensure_process_hel_resources
GO
IF (OBJECT_ID('app_ensure_process_idea_matrix') IS NOT NULL) DROP PROCEDURE app_ensure_process_idea_matrix
GO
IF (OBJECT_ID('app_ensure_process_issues') IS NOT NULL) DROP PROCEDURE app_ensure_process_issues
GO
IF (OBJECT_ID('app_ensure_process_kesko_matrix') IS NOT NULL) DROP PROCEDURE app_ensure_process_kesko_matrix
GO
IF (OBJECT_ID('app_ensure_process_waltham_costs') IS NOT NULL) DROP PROCEDURE app_ensure_process_waltham_costs
GO
IF (OBJECT_ID('app_ensure_process_risks') IS NOT NULL) DROP PROCEDURE app_ensure_process_risks
GO