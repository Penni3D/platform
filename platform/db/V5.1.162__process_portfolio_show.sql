﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[processes]') AND name = 'portfolio_show_Type') 
    ALTER TABLE processes
          ADD portfolio_show_Type  tinyint NOT NULL
          CONSTRAINT process_portfolio_show_Type_df DEFAULT 0


    ALTER TABLE process_portfolio_columns
          ALTER COLUMN show_Type  tinyint NULL

UPDATE process_portfolio_columns SET show_type = NULL WHERE show_type = 0

    ALTER TABLE process_portfolio_columns
          ADD report_column  tinyint NOT NULL
          CONSTRAINT portfolio_report_column_df DEFAULT 0

    ALTER TABLE archive_process_portfolio_columns
          ALTER COLUMN show_Type  tinyint NULL