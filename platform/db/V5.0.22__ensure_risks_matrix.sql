IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_risks_matrix'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_risks_matrix] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_risks_matrix]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'risks_matrix'
	DECLARE @process2 NVARCHAR(50) = 'list_risks_matrix'
	DECLARE @process3 NVARCHAR(50) = 'list_risks_matrix_status'



	DECLARE @tableName NVARCHAR(50)
	SET @tableName = '_' + @instance + '_' + @process2

	DECLARE @tableName2 NVARCHAR(50)
	SET @tableName2 = '_' + @instance + '_' + @process3

	DECLARE @itemId INT
	DECLARE @sql NVARCHAR(MAX)


	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;


	EXEC app_ensure_list @instance, @process2, @process2, 1

	EXEC dbo.app_ensure_column @instance,@process2, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process2, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process2, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process2, 'value', 0, 0, 1, 0, 0, NULL, 0, 0

	INSERT INTO items (instance, process) VALUES (@instance, @process2);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName +' SET list_item = ''Small'', value = 1 WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)


	INSERT INTO items (instance, process) VALUES (@instance, @process2);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName +' SET list_item = ''Minor'', value = 2 WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)


	INSERT INTO items (instance, process) VALUES (@instance, @process2);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName +' SET list_item = ''Medium'', value = 3 WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)

	INSERT INTO items (instance, process) VALUES (@instance, @process2);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName +' SET list_item = ''Substancial'', value = 4 WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)

	INSERT INTO items (instance, process) VALUES (@instance, @process2);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName +' SET list_item = ''Large'', value = 5 WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)

	EXEC app_ensure_list @instance, @process3, @process3, 1

	EXEC dbo.app_ensure_column @instance,@process3, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process3, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process3, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0

	INSERT INTO items (instance, process) VALUES (@instance, @process3);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName2 +' SET list_item = ''No danger'' WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)

	INSERT INTO items (instance, process) VALUES (@instance, @process3);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName2 +' SET list_item = ''Risk of realizing'' WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)

	INSERT INTO items (instance, process) VALUES (@instance, @process3);
	SET @itemId = @@identity;
	SET @sql = 	'UPDATE ' + @tableName2 +' SET list_item = ''Realized'' WHERE item_id = ' + CAST(@itemId AS NVARCHAR)
	EXEC (@sql)

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2
	EXEC dbo.app_ensure_column @instance,@process,'idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'type', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'description', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'probability', 0, 6, 1, 0, 0, NULL, 0, 0, 'list_risks_matrix'
	EXEC dbo.app_ensure_column @instance,@process,'seriousness', 0, 6, 1, 0, 0, NULL, 0, 0, 'list_risks_matrix'
	EXEC dbo.app_ensure_column @instance,@process,'current_score', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'status', 0, 6, 1, 0, 0, NULL, 0, 0, 'list_risks_matrix_status'
	EXEC dbo.app_ensure_column @instance,@process,'actions', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'prev', 0, 1, 1, 0, 0, NULL, 0, 0

END
GO

