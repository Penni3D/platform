CREATE TABLE worktime_tracking_users_tasks
(
  user_item_id int NOT NULL,
  task_item_id int NOT NULL,
  added_by int NOT NULL,
  description NVARCHAR(MAX) NULL,
  created_at DATETIME NOT NULL DEFAULT getDate(),
  CONSTRAINT FK_timetracker_user FOREIGN KEY (user_item_id)
  REFERENCES items (item_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
GO

ALTER TABLE worktime_tracking_users_tasks
  ADD CONSTRAINT PK_timetracker_no_duplicates UNIQUE(user_item_id, task_item_id)
GO

CREATE TABLE worktime_tracking_hours
(
  id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  user_item_id INT NOT NULL,
  task_item_id INT NOT NULL,
  process NVARCHAR(50) NOT NULL,
  date DATE NOT NULL,
  hours FLOAT NOT null,
  created_at DATETIME NOT NULL DEFAULT GETDATE(),
  CONSTRAINT FK_timetracking_hours FOREIGN KEY (user_item_id)
  REFERENCES items (item_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_worktime_tracking_basic_tasks'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_worktime_tracking_basic_tasks] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_worktime_tracking_basic_tasks]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'worktime_tracking_basic_tasks'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_list @instance, @process, @process, 1

    UPDATE processes SET list_order = 'list_item' WHERE process = @process

    EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    DECLARE @itemId INT = @@identity;
    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Sick Day'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    SET @itemId = @@identity;
    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Travel'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    SET @itemId = @@identity;
    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Company Meeting'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)
  END

GO

-- EXECUTE PROCEDURE FOR ALL INSTANCES
BEGIN
  DECLARE @instanceName NVARCHAR(10)
  DECLARE Timetracking_Cursor CURSOR FOR SELECT instance FROM instances

  OPEN Timetracking_Cursor

  FETCH NEXT FROM Timetracking_Cursor INTO @instanceName

  WHILE @@fetch_status = 0
    BEGIN

      EXEC app_ensure_process_worktime_tracking_basic_tasks @instanceName

      FETCH  NEXT FROM Timetracking_Cursor INTO
        @instanceName
    END
  CLOSE Timetracking_Cursor
  DEALLOCATE Timetracking_Cursor
END
