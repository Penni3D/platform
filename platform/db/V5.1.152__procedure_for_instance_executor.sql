IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_execute_procedure_for_all_instances'))
  exec('CREATE PROCEDURE [dbo].[app_execute_procedure_for_all_instances] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_execute_procedure_for_all_instances]
    @procedureName nvarchar(max )
AS
BEGIN
  DECLARE @instanceName NVARCHAR(10)
  DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
  DECLARE @sql NVARCHAR(MAX) = ''
  
  OPEN instanceCursor

  FETCH NEXT FROM instanceCursor INTO @instanceName

  WHILE @@fetch_status = 0
    BEGIN     
      SET @sql = @procedureName + ' ' + @instanceName 
      exec (@sql)
      
      FETCH  NEXT FROM instanceCursor INTO
        @instanceName
    END
  CLOSE instanceCursor
  DEALLOCATE instanceCursor
  END