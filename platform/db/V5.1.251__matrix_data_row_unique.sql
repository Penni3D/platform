EXEC app_set_archive_user_id 0

DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
	DECLARE @sql NVARCHAR(MAX);
	SET @sql = 'CREATE UNIQUE INDEX UNIQUE_MATRIX_CELL_SELECTIONS ON _' + CAST(@instance AS NVARCHAR(MAX)) + '_matrix_process_row_data(item_column_id, owner_item_id, question_item_id) WHERE item_column_id IS NOT NULL AND owner_item_id IS NOT NULL AND question_item_id IS NOT NULL';
	
	EXECUTE sp_executesql @sql

    FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor