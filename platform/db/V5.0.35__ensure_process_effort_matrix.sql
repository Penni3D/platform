IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_effort_matrix'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_effort_matrix] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_effort_matrix]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'effort_matrix'
	DECLARE @tableName NVARCHAR(50) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2

	--Create conditions
	EXEC app_ensure_condition @instance, @process, 0, 0

	--Create columns
	EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'status', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'matrix_points_1', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'matrix_points_2', 0, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'matrix_points_3', 0, 1, 1, 1, 0, null,  0, 0

	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 
END;
