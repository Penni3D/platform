CREATE TABLE process_comments (
	comment_id int IDENTITY(1, 1) PRIMARY KEY,
	instance nvarchar(10) NOT NULL,
	process nvarchar(255) NOT NULL,
	item_id int NOT NULL,
	identifier nvarchar(255) NOT NULL,
	sender int NOT NULL,
	comment nvarchar(MAX),
	[date] DATE NOT NULL DEFAULT GETDATE(),
);

CREATE TABLE instance_comments (
	comment_id int IDENTITY(1, 1) PRIMARY KEY,
	instance nvarchar(10) NOT NULL,
	identifier nvarchar(255) NOT NULL,
	sender int NOT NULL,
	comment nvarchar(MAX),
	[date] DATE NOT NULL DEFAULT GETDATE(),
);