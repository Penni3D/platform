EXEC app_set_archive_user_id 0
DECLARE @conf NVARCHAR(MAX)

DELETE FROM instance_configurations WHERE name = 'mode'
DELETE FROM instance_configurations WHERE name = 'financial'
DELETE FROM instance_configurations WHERE name = 'MyTasksPortfolioId'
DELETE FROM instance_configurations WHERE name = 'UserTasksPortfolioId'
DELETE FROM instance_configurations WHERE name = 'ItemTypeItemId'
DELETE FROM instance_configurations WHERE name = 'HourBreakdown'
DELETE FROM instance_configurations WHERE name = 'AdditionalHours'
DELETE FROM instance_configurations WHERE name = 'RequireFullWeek'
DELETE FROM instance_configurations WHERE name = 'DailyHoursCapped'
DELETE FROM instance_configurations WHERE name = 'DailyMaximumAdditionalHours'
DELETE FROM instance_configurations WHERE name = 'ApprovalRequiredFieldInProcesses'
DELETE FROM instance_configurations WHERE name = 'AddOnlyTasksFromProcessWithStatusColumn'
DELETE FROM instance_configurations WHERE name = 'AddOnlyTasksFromProcessWithStatusValueIn'
DELETE FROM instance_configurations WHERE name = 'AllowBasicHoursOnWeekends'
DELETE FROM instance_configurations WHERE name = 'User portfolio ID'
DELETE FROM instance_configurations WHERE name = 'FavouriteColumnId'
DELETE FROM instance_configurations WHERE name = 'BacklogPortfolioId'
DELETE FROM instance_configurations WHERE name = 'SprintsPortfolioId'
DELETE FROM instance_configurations WHERE name = 'GanttPortfolioId'
DELETE FROM instance_configurations WHERE name = 'KanbanPortfolioId'
DELETE FROM instance_configurations WHERE name = 'Bulletins portfolio ID'
DELETE FROM instance_configurations WHERE name = 'CommentsPortfolioId'
DELETE FROM instance_configurations WHERE name = 'CommentsPublishActionId'

-- COPY MODEL COPY
SET @conf = 'CopyModelPortfolioId';
UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"modelPortfolioId":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE default_state = 'new.process' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
DELETE FROM instance_configurations WHERE name = @conf

SET @conf = 'CopyModelProcess';
UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"modelProcess":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE default_state = 'new.process' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
DELETE FROM instance_configurations WHERE name = @conf

SET @conf = 'CopyModelStartDate';
UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"modelStartDate":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE default_state = 'new.process' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
DELETE FROM instance_configurations WHERE name = @conf

SET @conf = 'CopyModelEndDate';
UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"modelEndDate":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE default_state = 'new.process' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
DELETE FROM instance_configurations WHERE name = @conf

SET @conf = 'CopyModelItemColumn';
UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"modelItemColumn":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE default_state = 'new.process' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
DELETE FROM instance_configurations WHERE name = @conf


-- NOTIFICATION COPY
DELETE FROM instance_menu WHERE page = 'notification'
DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
	BEGIN
	
	INSERT INTO instance_menu(instance, variable, params, link_type, page) VALUES(@instance, 'instancemenu_notification_' + @instance, '{}', 3, 'notification');

	SET @conf = 'EmailsPortfolioId';
	UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + '"emailsPortfolioId":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE page = 'notification' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
	DELETE FROM instance_configurations WHERE name = @conf

	SET @conf = 'NotificationsPortfolioId';
	UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"notificationsPortfolioId":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE page = 'notification' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
	DELETE FROM instance_configurations WHERE name = @conf

	SET @conf = 'BulletinsPortfolioId';
	UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"bulletinsPortfolioId":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE page = 'notification' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
	DELETE FROM instance_configurations WHERE name = @conf

	SET @conf = 'SendedEmailsPortfolioId';
	UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"sendedEmailsPortfolioId":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE page = 'notification' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
	DELETE FROM instance_configurations WHERE name = @conf

	SET @conf = 'UnreadNotificationsPortfolioId';
	UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"unreadNotificationsPortfolioId":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE page = 'notification' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
	DELETE FROM instance_configurations WHERE name = @conf

	SET @conf = 'AllNotificationsPortfolioId';
	UPDATE instance_menu SET params = LEFT(params, LEN(params) - 1) + ',"allNotificationsPortfolioId":"' + (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) + '"}' WHERE page = 'notification' AND (SELECT TOP 1 value FROM instance_configurations WHERE name = @conf) IS NOT NULL AND params NOT LIKE '%' + @conf +'%'
	DELETE FROM instance_configurations WHERE name = @conf
	FETCH  NEXT FROM instance_cursor INTO @instance
END