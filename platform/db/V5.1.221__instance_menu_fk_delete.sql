CREATE  TRIGGER [dbo].[instance_menu_fk_delete]
    ON [dbo].[instance_menu]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM instance_menu
	 dd	INNER JOIN deleted d ON dd.parent_id = d.instance_menu_id

	DELETE dd FROM instance_menu
	 dd	INNER JOIN deleted d ON dd.instance_menu_id = d.instance_menu_id