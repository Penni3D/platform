SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									IF @type = 0 
										BEGIN
											SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
											SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
										END
							
									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
											SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
										END
				
									IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)

									IF @type = 0 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 as int ) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
																END
														END
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @endChar NVARCHAR(1)
													DECLARE @listVal NVARCHAR(max)
													DECLARE @guid uniqueidentifier

													SET @endChar = ''
													IF charindex(']', @var3) > 0
														BEGIN
															SET @endChar = ']'
														END
						
													SET @var3 = replace(@var3, '[', '')
													SET @var3 = replace(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
								  					WHILE (@@FETCH_STATUS=0) 
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															DECLARE @gu_id NVARCHAR(max)
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = 'NULL' 
																END
															SET @var4 = replace(@var4,  @listVal , CAST(@gu_id AS NVARCHAR(max)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													
													SET @dataAdditional2 = replace(@dataAdditional2, '@' + @var2, @strToFound + '":[' + @var4 + @endChar)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
		
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)
				
									IF @type = 0 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
																END
														END
												END
										END

									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processPortfolio_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
																END
														END
												END
										END

									IF @type = 2 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
																END
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
		
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) -2 )

									IF @type = 0 
										BEGIN
											SET @var3 =  replace(replace(@var3, 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
																END
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processActionDialog_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
																END
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0),
						('@TaskEffortResponsiblesCol', 0),
						('@BudgetItemTypeValues', 1),
						('@BudgetItemCategoryIncomeValues', 1),
						('@BudgetItemCategoryExpenseValues', 1),
						('@NPVDiscountValueIntField', 0),
						('@NPVStartYearIntField', 0),
						('@NPVNumberOfYearsIntField', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									IF @type = 0 
										BEGIN
											SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
											SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
										END

									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
											SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
										END
				
									IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @endChar NVARCHAR(1)
													DECLARE @listVal NVARCHAR(max)
													DECLARE @listItemId int
													
													SET @endChar = ''
													IF charindex(']', @var3) > 0
														BEGIN
															SET @endChar = ']'
														END

													SET @var3 = replace(@var3, '[', '')
													SET @var3 = replace(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = replace(@var4,  @listVal, CAST(@listItemId AS NVARCHAR(max)))

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = replace(@dataAdditional2, '@' + @var2, '[' + @strToFound + '":[' + @var4 + @endChar)
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END

									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END

									IF @type = 2 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							IF @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)
	
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":"tab_' + @var4 + '"')
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END
												END
										END
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END