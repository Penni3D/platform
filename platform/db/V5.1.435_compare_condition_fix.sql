SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[condition_IdReplaceProc]
(
	@condition_json NVARCHAR(MAX),
	@replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = CHARINDEX('ItemColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.itemColumn_GetVariable(@var3)
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'ItemColumnId":' + @var4)
						END

					DECLARE @endPos INT
					SET @endPos = @endPos1 - LEN(@var2) + LEN(@var4) + 14
					DECLARE @data_type INT
					SELECT @data_type = data_type FROM item_columns WHERE item_column_id = @var3
					IF @data_type IN (5, 6) AND @endPos2 > @endPos1 AND SUBSTRING(@condition_json, @endPos, 9) = ',"Value":'
						BEGIN
							DECLARE @endPos3 INT
							DECLARE @endPos4 INT
							DECLARE @listItemId AS NVARCHAR(MAX)
							DECLARE @listItemId2 AS NVARCHAR(MAX)
							SET @endPos3 = CHARINDEX(',', @condition_json, @endPos + 1)
							SET @endPos4 = CHARINDEX('}', @condition_json, @endPos)
							IF @endPos4 > @endPos3
								BEGIN
									SET @listItemId = SUBSTRING(@condition_json, @endPos + 9, @endPos3 - @endPos - 9)
								END
							ELSE
								BEGIN
									SET @listItemId = SUBSTRING(@condition_json, @endPos + 9, @endPos4 - @endPos - 9)
								END

							SET @listItemId2 = REPLACE(@listItemId, '"', '')

							IF ISNUMERIC(@listItemId2) = 1 AND @listItemId2 <> '0'
								BEGIN
									DECLARE @guid NVARCHAR(MAX)
									SELECT @guid = CAST(dbo.item_GetGuid(CAST(@listItemId2 AS INT)) AS NVARCHAR(MAX))
									IF @guid IS NULL
										BEGIN
											SET @guid = 'null'
										END
									SET @condition_json = STUFF(@condition_json, @endPos + 9, LEN(@listItemId), @guid)
								END
							ELSE
								BEGIN
									SET @condition_json = STUFF(@condition_json, @endPos + 9, LEN(@listItemId), 'null')
								END
						END
				END
			ELSE
				BEGIN
					SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'ItemColumnId":0')
				END

			SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @startPos + 1)
		END

	SET @startPos = CHARINDEX('LinkColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.itemColumn_GetVariable(@var3)
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'LinkColumnId":' + @var4)
						END
				END
			ELSE
				BEGIN
					SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'LinkColumnId":0')
				END

			SET @startPos = CHARINDEX('LinkColumnId', @condition_json, @startPos + 1)
		END

	SET @startPos = CHARINDEX('UserColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN 
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)

			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.itemColumn_GetVariable(@var3)
					IF @var4 IS NOT NULL 
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'UserColumnId":' + @var4)
						END
				END
			ELSE
				BEGIN
					SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'UserColumnId":0')
				END

			SET @startPos = CHARINDEX('UserColumnId', @condition_json, @startPos + 1)
		END

	SET @startPos = CHARINDEX('ConditionId', @condition_json)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.condition_GetVariable(@var3)
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'ConditionId":' + @var4)
						END
				END
			ELSE
				BEGIN
					SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'ConditionId":0')
				END

			SET @startPos = CHARINDEX('ConditionId', @condition_json, @startPos + 1)
		END

	SET @replaced_json = @condition_json
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[condition_VarReplaceProc]
(
	@targetInstance NVARCHAR(10),
	@condition_json NVARCHAR(MAX),
	@replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = CHARINDEX('ItemColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'ItemColumnId":' + @var4)
						END
						
					DECLARE @endPos INT
					SET @endPos = @endPos1 - LEN(@var2) + LEN(@var4) + 14
					DECLARE @data_type INT
					SELECT @data_type = data_type FROM item_columns WHERE item_column_id = @var4
					IF @data_type IN (5, 6) AND @endPos2 > @endPos1 AND SUBSTRING(@condition_json, @endPos, 9) = ',"Value":'
						BEGIN
							DECLARE @endPos3 INT
							DECLARE @endPos4 INT
							DECLARE @guid AS NVARCHAR(MAX)
							SET @endPos3 = CHARINDEX(',', @condition_json, @endPos + 1)
							SET @endPos4 = CHARINDEX('}', @condition_json, @endPos)
							IF @endPos4 > @endPos3
								BEGIN
									SET @guid = SUBSTRING(@condition_json, @endPos + 9, @endPos3 - @endPos - 9)
								END
							ELSE
								BEGIN
									SET @guid = SUBSTRING(@condition_json, @endPos + 9, @endPos4 - @endPos - 9)
								END

							SET @guid = REPLACE(@guid, '"', '')

							IF ISNUMERIC(@guid) = 0 AND @guid <> '' AND @guid <> 'null'
								BEGIN
									DECLARE @listItemId INT
									SELECT @listItemId = dbo.item_GetId(CAST(@guid AS UNIQUEIDENTIFIER))
									IF @listItemId IS NOT NULL
										BEGIN
											SET @condition_json = STUFF(@condition_json, @endPos + 9, LEN(@guid), @listItemId)
										END
									ELSE
										BEGIN
											SET @condition_json = STUFF(@condition_json, @endPos + 9, LEN(@guid), 'null')
										END
								END
							ELSE
								BEGIN
									SET @condition_json = STUFF(@condition_json, @endPos + 9, LEN(@guid), 'null')
								END
						END
				END

			SET @startPos = CHARINDEX('ItemColumnId', @condition_json, @startPos + 1)
		END

	SET @startPos = CHARINDEX('LinkColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'LinkColumnId":' + @var4)
						END
				END

			SET @startPos = CHARINDEX('LinkColumnId', @condition_json, @startPos + 1)
		END

	SET @startPos = CHARINDEX('UserColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)
					IF @var4 IS NOT NULL
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'UserColumnId":' + @var4)
						END
				END

			SET @startPos = CHARINDEX('UserColumnId', @condition_json, @startPos + 1)
		END

	SET @startPos = CHARINDEX('ConditionId', @condition_json)
	WHILE @startPos > 0
		BEGIN
			SET @endPos1 = CHARINDEX(',', @condition_json, @startPos)
			SET @endPos2 = CHARINDEX('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)
			
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.condition_GetId(@targetInstance, @var3)
					IF @var4 IS NOT NULL 
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'ConditionId":' + @var4)
						END
					ELSE
						BEGIN
							SET @condition_json = STUFF(@condition_json, CHARINDEX(@var2, @condition_json, @startPos), LEN(@var2), 'ConditionId":null')
						END
				END

			SET @startPos = CHARINDEX('ConditionId', @condition_json, @startPos + 1)
		END
 
	SET @replaced_json = @condition_json
END