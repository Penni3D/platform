SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processPortfolioColumnGroup_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = portfolio_column_group_id FROM process_portfolio_columns_groups WHERE instance = @instance and variable = @variable
 RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processPortfolioColumnGroup_GetVariable]
(
 @portfolioColumnGroupId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 SELECT TOP 1 @var = variable FROM process_portfolio_columns_groups WHERE portfolio_column_group_id = @portfolioColumnGroupId
 RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processColumns_ValidationIdReplace]
(
 @validation NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int

	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)

	IF @validation IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				  ('ParentId', 0)
		
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS=0) 
				BEGIN
					DECLARE @startPos INT = charindex(@strToFound, @validation)
					WHILE @startPos > 0
						BEGIN  
							IF @type = 0 
								BEGIN
									SET @endPos1 = charindex(',', @validation, @startPos)
									SET @endPos2 = charindex('}', @validation, @startPos)
								END
							IF @type = 1 
								BEGIN
									set @startPos = @startPos + 1
									SET @endPos1 = charindex(']', @validation, @startPos)
									SET @endPos2 = charindex(',', @validation, @startPos) 
								END
				
							IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos1-@startPos)
									SET @startPos = charindex(@strToFound, @validation, @endPos1)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos2-@startPos)
									SET @startPos = charindex(@strToFound, @validation, @endPos2)
								END
				
							SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				

							IF @type = 0 
								BEGIN
									SET @var3 = TRIM('"' FROM @var3)
									IF ISNUMERIC(@var3) = 1 
										BEGIN
											IF CAST(@var3 as int ) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetVariable(@var3)		
													SET @validation = replace(@validation, @var2, @strToFound +'":' + @var4)
					
													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @validation, @endPos1+LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @validation, @endPos2+LEN(@var4))
														END
												END
										END
								END

							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> ']'
										BEGIN
											DECLARE @listVal NVARCHAR(max)
											DECLARE @guid uniqueidentifier
						
											SET @var3 = replace(@var3, '[', '')
											SET @var3 = replace(@var3, ']', '')
											SET @var4 = @var3

											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
								
								  			WHILE (@@FETCH_STATUS=0) 
												BEGIN
													SELECT @guid = guid FROM items WHERE item_id = @listVal
													DECLARE @gu_id NVARCHAR(max)
													SET @gu_id = @guid
													IF @gu_id IS NULL
														BEGIN
															SET @gu_id = 'NULL' 
														END
													SET @var4 = replace(@var4,  @listVal , CAST(@gu_id as nvarchar(max) ) )

													FETCH NEXT FROM @delimited INTO @listVal
												END
											CLOSE @delimited
											SET @validation = replace(@validation, @var3, @var4)
										END
								END

							CONTINUE  
						END

						FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
				CLOSE @getStrs
		END
		RETURN @validation
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processColumns_ValidationVarReplace]
(
 @instance NVARCHAR(MAX),
 @validation NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int

	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)

	IF @validation IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				 ('ParentId', 0)
		
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS=0) 
				BEGIN
					DECLARE @startPos INT = charindex(@strToFound, @validation)
					WHILE @startPos > 0
						BEGIN  
							IF @type = 0 
								BEGIN
									SET @endPos1 = charindex(',', @validation, @startPos)
									SET @endPos2 = charindex('}', @validation, @startPos)
								END
							IF @type = 1 
								BEGIN
									set @startPos = @startPos + 1
									SET @endPos1 = charindex(']', @validation, @startPos)
									SET @endPos2 = charindex(',', @validation, @startPos) 
								END
				
							IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos1-@startPos)
									SET @startPos = charindex(@strToFound, @validation, @endPos1)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos2-@startPos)
									SET @startPos = charindex(@strToFound, @validation, @endPos2)
								END
				
							SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
							IF @type = 0 
								BEGIN
									IF LEN(@var3)  > 0
										BEGIN
											SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
											IF @var4 IS NOT NULL
												BEGIN
													SET @validation = replace(@validation, @var2, @strToFound +'":' + @var4)
												END

											IF @endPos2 > @endPos1
												BEGIN
													SET @startPos = charindex(@strToFound, @validation, @endPos1+LEN(@var4))
												END
											ELSE
												BEGIN
													SET @startPos = charindex(@strToFound, @validation, @endPos2+LEN(@var4))
												END
										END
								END

							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0 AND @var3 <> ']'
										BEGIN
											DECLARE @listVal NVARCHAR(max)
											DECLARE @listItemId int
											SET @var3 = replace(@var3, '[', '')
											SET @var3 = replace(@var3, ']', '')
											SET @var4 = @var3

											DECLARE @delimited CURSOR
											SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
											SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
											OPEN @delimited
											FETCH NEXT FROM @delimited INTO @listVal
								
											WHILE (@@FETCH_STATUS=0) 
												BEGIN
													IF @listVal = 'NULL' 
														BEGIN
															SET @listItemId = 0
														END
													ELSE 
														BEGIN
															SELECT @listItemId = item_id FROM items WHERE guid = @listVal
															IF @listItemId IS NULL
																BEGIN
																	SET @listItemId = 0
																END
														END
														SET @var4 = replace(@var4,  @listVal ,  CAST(@listItemId as nvarchar(max) ) )

														FETCH NEXT FROM @delimited INTO @listVal
												END
												CLOSE @delimited
												SET @validation = replace(@validation, @var3, @var4)
										END
								END
								
							CONTINUE  
						END
						FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
				CLOSE @getStrs
		END
		RETURN @validation
END