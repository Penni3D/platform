ALTER TABLE instance_helpers
ADD order_no NVARCHAR(MAX)

GO


DECLARE @instance NVARCHAR(10) = ''
DECLARE @process NVARCHAR(50) = ''

--Archive user is 0
DECLARE @BinVar varbinary(128);
SET @BinVar = CONVERT(varbinary(128), 0);
SET CONTEXT_INFO @BinVar;

DECLARE @lastInstance NVARCHAR(10) = ''
DECLARE @lastProcess NVARCHAR(50) = ''
DECLARE @orderNo NVARCHAR(MAX) = ''

DECLARE @condition_group_id INT
DECLARE ConditionId_Cursor CURSOR FOR
  SELECT condition_group_id, instance, process FROM conditions_groups ORDER BY instance, process

OPEN ConditionId_Cursor

FETCH NEXT FROM ConditionId_Cursor INTO @condition_group_id, @instance, @process

WHILE @@fetch_status = 0
    BEGIN
		IF (@process != @lastProcess OR @instance != @lastInstance)
			BEGIN
				SET @orderNo = 'U'
			END
			
		SET @orderNo = dbo.getOrderBetween(@orderNo, null)
		
		SET @lastProcess = @process
		SET @lastInstance = @instance
		
		UPDATE conditions_groups SET order_no = @orderNo WHERE condition_group_id = @condition_group_id AND instance = @instance AND process = @process
		
		FETCH NEXT FROM ConditionId_Cursor INTO @condition_group_id, @instance, @process
	END
	
CLOSE ConditionId_Cursor
DEALLOCATE ConditionId_Cursor
GO
---------------------------------------------------------

DECLARE @instance NVARCHAR(10) = ''
DECLARE @identifier NVARCHAR(50) = ''

--Archive user is 0
DECLARE @BinVar varbinary(128);
SET @BinVar = CONVERT(varbinary(128), 0);
SET CONTEXT_INFO @BinVar;

DECLARE @lastInstance NVARCHAR(10) = ''
DECLARE @lastIdentifier NVARCHAR(50) = ''
DECLARE @orderNo NVARCHAR(MAX) = ''

DECLARE @instance_helper_id INT
DECLARE cursor2 CURSOR FOR
  SELECT instance_helper_id, instance, identifier FROM instance_helpers ORDER BY instance, identifier

OPEN cursor2

FETCH NEXT FROM cursor2 INTO @instance_helper_id, @instance, @identifier

WHILE @@fetch_status = 0
    BEGIN
		IF (@identifier != @lastIdentifier OR @instance != @lastInstance)
			BEGIN
				SET @orderNo = 'U'
			END
			
		SET @orderNo = dbo.getOrderBetween(@orderNo, null)
		
		SET @lastIdentifier = @identifier
		SET @lastInstance = @instance
		
		UPDATE instance_helpers SET order_no = @orderNo WHERE instance_helper_id = @instance_helper_id AND instance = @instance AND identifier = @identifier
		
		FETCH NEXT FROM cursor2 INTO @instance_helper_id, @instance, @identifier
	END
	
CLOSE cursor2
DEALLOCATE cursor2
GO


