  exec app_remove_archive instance_hosts
  GO
  ALTER TABLE instance_hosts ADD uptime_monitor_id INT;
  ALTER TABLE instance_hosts ADD uptime_monitor_start DATETIME;