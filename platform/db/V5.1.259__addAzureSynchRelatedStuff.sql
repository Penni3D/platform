

DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
	BEGIN
   
   ----list
EXEC app_ensure_list @instance, 'list_azure_ad_groups', 'azure_ad_groups', 0


EXEC dbo.app_ensure_column @instance,'list_azure_ad_groups', 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_azure_ad_groups', 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_azure_ad_groups', 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0

EXEC dbo.app_ensure_column @instance,'list_azure_ad_groups', 'list_code', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_azure_ad_groups', 'list_description', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_azure_ad_groups', 'list_domain', 0, 0, 1, 0, 0, NULL, 0, 0

exec app_set_archive_user_id 0 
update processes set variable='list_azure_ad_groups'  where process='list_azure_ad_groups'


----user
EXEC dbo.app_ensure_column @instance,'user', 'integration_update', 0, 1, 0, 0, 0, NULL, 0, 0
exec app_set_archive_user_id 0 

declare @userT nvarchar(255);
set @userT = '_'+ @instance + '_user';

EXEC ('update ' + @userT + ' set integration_update=0')

exec app_set_archive_user_id 0
update item_columns set status_column=1 where name='integration_update'

----user group
DECLARE @container_id_v int = 0;
set @container_id_v = (SELECT  c.process_container_id FROM process_tabs left join process_tab_containers cont ON process_tabs.process_tab_id=cont.process_tab_id left join process_containers c ON c.process_container_id = cont.process_container_id where process_tabs.process='user_group' AND c.variable='cont_user_group_admin_container');
set @container_id_v =  (SELECT CASE when @container_id_v is null then 0 else @container_id_v END);


EXEC dbo.app_ensure_column @instance,'user_group', 'azure_ad_groups', @container_id_v, 6, 0, 0, 0, NULL, 0, 0,'list_azure_ad_groups'

DELETE FROM process_translations WHERE variable IN('IC_LIST_AZURE_AD_GROUPS_LIST_SYMBOL_FILL','IC_LIST_AZURE_AD_GROUPS_LIST_SYMBOL','IC_LIST_AZURE_AD_GROUPS_LIST_ITEM','IC_LIST_AZURE_AD_GROUPS_LIST_CODE','IC_LIST_AZURE_AD_GROUPS_LIST_DESCRIPTION','IC_LIST_AZURE_AD_GROUPS_LIST_DOMAIN','IC_USER_INTEGRATION_UPDATE','IC_USER_GROUP_AZURE_AD_GROUPS')

FETCH  NEXT FROM instance_cursor INTO @instance
END
