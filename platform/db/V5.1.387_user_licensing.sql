
DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
	BEGIN
   
EXEC app_ensure_list @instance, 'list_user_licences', 'user_licences', 0

EXEC dbo.app_ensure_column @instance,'list_user_licences', 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licences', 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licences', 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0

EXEC dbo.app_ensure_column @instance,'list_user_licences', 'cost', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licences', 'licence_group', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licences', 'in_use', 0, 1, 1, 0, 0, NULL, 0, 0


EXEC app_ensure_list @instance, 'list_user_licence_required', 'licence_required', 0

EXEC dbo.app_ensure_column @instance,'list_user_licence_required', 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licence_required', 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licence_required', 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licence_required', 'code', 0, 1, 1, 0, 0, NULL, 0, 0
EXEC dbo.app_ensure_column @instance,'list_user_licence_required', 'in_use', 0, 1, 1, 0, 0, NULL, 0, 0

DECLARE @sql NVARCHAR(MAX) = ''

INSERT INTO items (instance, process) VALUES (@instance, 'list_user_licence_required');
DECLARE @itemId INT = @@identity;

SET @sql = 'UPDATE _' + @instance + '_list_user_licence_required SET list_item = ''Yes'', code = 1 WHERE item_id = ' +  CAST(@itemId AS nvarchar)
EXEC (@sql)

INSERT INTO items (instance, process) VALUES (@instance, 'list_user_licence_required');
SET @itemId = @@identity;

SET @sql = 'UPDATE _' + @instance + '_list_user_licence_required SET list_item = ''No'', code = 0 WHERE item_id = ' +  CAST(@itemId AS nvarchar)
EXEC (@sql)

EXEC dbo.app_ensure_column @instance,'user', 'licence_required', 0, 6, 1, 0, 0, NULL, 0, 'list_user_licence_required'
EXEC dbo.app_ensure_column @instance,'user_group', 'licence_categories', 0, 6, 1, 0, 0, NULL, 0, 'list_user_licences'

FETCH  NEXT FROM instance_cursor INTO @instance
END
