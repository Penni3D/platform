IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_preparation_phase_data'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_preparation_phase_data] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_preparation_phase_data]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'preparation_phase_data'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2
	
	EXEC dbo.app_ensure_column @instance,@process,'idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'preparation_progress', 0, 6, 1, 0, 0, NULL, 0, 0, 'list_preparation_progress'
	EXEC dbo.app_ensure_column @instance,@process,'most_critical_area', 0, 6, 1, 0, 0, NULL, 0, 0, 'list_most_critical_area'
	EXEC dbo.app_ensure_column @instance,@process,'projectclass', 0, 6, 1, 0, 0, NULL, 0, 0, 'list_project_class'
	EXEC dbo.app_ensure_column @instance,@process,'cost_type', 0, 6, 1, 0, 0, NULL, 0, 0, 'list_cost_type'
	EXEC dbo.app_ensure_column @instance,@process,'steering_group', 0, 5, 1, 0, 0, NULL, 0, 0, 'user'
	EXEC dbo.app_ensure_column @instance,@process,'project_requirements_and_dependencies', 0, 4, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'the_projects_most_significant_risks', 0, 4, 1, 0, 0, NULL, 0, 0


END
GO

