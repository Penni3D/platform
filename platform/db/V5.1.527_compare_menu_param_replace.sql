EXEC app_set_archive_user_id 1
UPDATE instance_menu SET params = REPLACE(params, '"flatGroupOwnerItemId"', '"flatGroupOwnerColumnId"') WHERE default_state = 'portfolio'



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processColumns_ValidationVarReplace]
(
	@instance NVARCHAR(MAX),
	@validation NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(MAX) 
	DECLARE @type INT
	DECLARE @strToFoundLength INT

	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)

	IF @validation IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('ParentId', 0)
			
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
			
			WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					DECLARE @startPos INT = CHARINDEX(@strToFound, @validation)
					IF @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 2

							IF @type = 0 
								BEGIN
									SET @endPos1 = CHARINDEX(',', @validation, @startPos)
									SET @endPos2 = CHARINDEX('}', @validation, @startPos)
								END
							
							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@validation, @startPos, @endPos2 - @startPos)
								END
							
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
							
							IF @type = 0 
								BEGIN
									IF @var3 = '""'
										BEGIN
											SET @validation = STUFF(@validation, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, '""')
										END
									ELSE IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
											IF @var4 IS NOT NULL
												BEGIN
													SET @validation = STUFF(@validation, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, '"' + @var4 + '"')
												END
											ELSE
												BEGIN
													SET @validation = STUFF(@validation, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, 'null')
												END
										END
								END
						END
						FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
				CLOSE @getStrs
		END
		RETURN @validation
END