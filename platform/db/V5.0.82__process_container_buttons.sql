	DECLARE @process_tab_id int;
	DECLARE @process_container_id int;
	DECLARE @process nvarchar(50);
	DECLARE @instance nvarchar(50);
	DECLARE @GetInstance CURSOR

	DECLARE @container_var nvarchar(255);

	--Get Tables
	SET @GetInstance = CURSOR FAST_FORWARD LOCAL FOR 
	select distinct ptc.process_tab_id, process, instance from process_tab_columns ptc inner join process_tabs pt on ptc.process_tab_id = pt.process_tab_id

	OPEN @GetInstance
	FETCH NEXT FROM @GetInstance INTO @process_tab_id, @process,@instance

	WHILE (@@FETCH_STATUS=0) BEGIN	
			set @container_var = CAST(@process_tab_id as nvarchar)  + '_BUTTONS'
			exec app_ensure_container @instance, @process, @container_var, @process_tab_id, 3, @container_id = @process_container_id OUTPUT
			insert into process_container_columns (process_container_id, item_column_id) ( select @process_container_id, item_column_id FROM process_tab_columns WHERE process_tab_id = @process_tab_id )
			FETCH NEXT FROM @GetInstance INTO @process_tab_id, @process, @instance
	END
	CLOSE @GetInstance
