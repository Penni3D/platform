ALTER TABLE notification_receivers
ADD CONSTRAINT notification_receivers_fk_name
FOREIGN KEY (process_action_row_id)
REFERENCES process_action_rows (process_action_row_id)
 ON DELETE CASCADE; 