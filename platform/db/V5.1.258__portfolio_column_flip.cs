using System;
using System.Data;
using System.Xml;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.db {
	class ServerMigrationPortfolioColumnFlip : CsMigration {
		public override bool Execute(IConnections db) {
			try {
				string icNamesSubquery =
					",(SELECT name FROM item_columns ic INNER JOIN process_portfolio_columns ppc ON ic.item_column_id = ppc.item_column_id WHERE ppc.process_portfolio_id LIKE identifier FOR XML PATH('')) AS portfolio_ic_names";
				string selectConfigurationsSql =
					"SELECT instance, [group], value, identifier, user_id, (SELECT 0) AS user_configuration_id, (SELECT 'false') AS saved" +
					icNamesSubquery + " FROM instance_user_configurations ";
				string selectSavedConfigurationSql =
					"SELECT instance, [group], value, identifier, user_id, user_configuration_id, (SELECT 'true') AS saved" +
					icNamesSubquery + " FROM instance_user_configurations_saved ";
				string whereSql = "WHERE [group] = 'portfolios' AND identifier <> 'process.gantt'";
				DataTable oldConfigurationsDt =
					db.GetDatatable(
						selectConfigurationsSql + whereSql + " UNION " + selectSavedConfigurationSql + whereSql, null);
				DataTable newConfigurationsDt = DefineNewConfigurationDt();
				DataTable newSavedConfigurationsDt = DefineNewConfigurationDt();
				foreach (DataRow confRow in oldConfigurationsDt.Rows) {
					if (Convert.ToString(confRow["saved"]) == "false") {
						DataRow dr = getNewConfigRow(newConfigurationsDt, confRow);
						newConfigurationsDt.Rows.Add(dr);
					} else if (Convert.ToString(confRow["saved"]) == "true") {
						DataRow dr = getNewConfigRow(newSavedConfigurationsDt, confRow);
						newSavedConfigurationsDt.Rows.Add(dr);
					}
				}

				if (newConfigurationsDt.Rows.Count > 0) {
					updateConfigValues(newConfigurationsDt, db, "instance_user_configurations");
				}

				if (newSavedConfigurationsDt.Rows.Count > 0) {
					updateConfigValues(newSavedConfigurationsDt, db, "instance_user_configurations_saved");
				}
			} catch (Exception ex) {
				message = ex.Message;
				return false;
			}

			return true;
		}

		private DataTable DefineNewConfigurationDt() {
			DataTable dt = new DataTable();
			DataColumn dcInstance = new DataColumn("instance");
			DataColumn dcGroup = new DataColumn("group");
			DataColumn dcOldValue = new DataColumn("old_value");
			DataColumn dcNewValue = new DataColumn("new_value");
			DataColumn dcIdentifier = new DataColumn("identifier");
			DataColumn dcUserId = new DataColumn("user_id");
			DataColumn dcUserConfigurationId = new DataColumn("user_configuration_id");
			dt.Columns.Add(dcInstance);
			dt.Columns.Add(dcGroup);
			dt.Columns.Add(dcOldValue);
			dt.Columns.Add(dcNewValue);
			dt.Columns.Add(dcIdentifier);
			dt.Columns.Add(dcUserId);
			dt.Columns.Add(dcUserConfigurationId);
			return dt;
		}

		private DataRow getNewConfigRow(DataTable dt, DataRow oldRow) {
			DataRow dr = dt.NewRow();
			dr["instance"] = Convert.ToString(oldRow["instance"]);
			dr["group"] = Convert.ToString(oldRow["group"]);
			dr["old_value"] = Convert.ToString(oldRow["value"]);

			var x = GetIcFlipValuesToConfigValue(Convert.ToString(oldRow["value"]),
				Convert.ToString(oldRow["portfolio_ic_names"]));
			if (x != null) {
				dr["new_value"] = x;
			} else {
				dr["new_value"] = dr["old_value"];
			}


			dr["user_id"] = Convert.ToInt32(oldRow["user_id"]);
			dr["identifier"] = Convert.ToString(oldRow["identifier"]);
			dr["user_configuration_id"] = Convert.ToString(oldRow["user_configuration_id"]);
			return dr;
		}

		private string GetIcFlipValuesToConfigValue(string oldValue, string portfolioIcNamesSql) {
			var doc = new XmlDocument();
			var xmlData = "<root>" + portfolioIcNamesSql + "</root>";
			doc.LoadXml(xmlData);
			string icNames = "";
			string newValue = "";
			string oldColumns = "";
			if (portfolioIcNamesSql == "") return null;
			//resolve portfolioIcNamesSql json array
			JObject o;
			try {
				o = JObject.Parse(JsonConvert.SerializeXmlNode(doc));
			} catch (Exception e) {
				Console.WriteLine(e.Message);
				throw;
			}

			foreach (var x in o) {
				if (x.Key == "root") {
					JObject i;
					try {
						i = JObject.Parse(x.Value.ToString());
					} catch (Exception e) {
						Console.WriteLine(e.Message);
						throw;
					}


					foreach (var x2 in i) {
						if (x2.Key == "name") icNames = x2.Value.ToString();
					}
				}
			}

			if (icNames == "") {
				Diag.LogToConsole("Unable to resolve GetIcFlipValuesToConfigValue() function. Error code 1.",
					Diag.LogSeverities.Error);
				throw new Exception();
			}

			//Resolve old visible columns in json
			try {
				o = JObject.Parse(oldValue);
			} catch (Exception e) {
				Console.WriteLine("Cannot resolve columns: " + e.Message);
				return null;
			}


			foreach (var x in o) {
				if (x.Key == "filters") {
					JObject p;
					try {
						p = JObject.Parse(x.Value.ToString());
					} catch (Exception e) {
						Console.WriteLine(e.Message);
						throw;
					}

					foreach (var x2 in p) {
						//oldColumns = x2.First.ToString(); //to-do: First korvattava, että kutsuu oikealla key:llä. tämä on epävarmaa.
						if (x2.Key == "columns") {
							oldColumns = x2.Value.ToString();
						}
					}
				}
			}

			if (oldColumns == "") {
				Diag.LogToConsole("Unable to resolve GetIcFlipValuesToConfigValue() function. Error code 2.",
					Diag.LogSeverities.Error);
				throw new Exception();
			}

			newValue = null;
			if (icNames.Contains("[")) newValue = flipConfigJsonFilterColumns(oldColumns, oldValue, icNames);
			return newValue;
		}

		private string flipConfigJsonFilterColumns(string oldVisibleColumns, string oldConfigJson,
			string portfolioItemColumns) {
			string newConfigJson = oldConfigJson;
			string hideColumns = "[";
			if (portfolioItemColumns == "") return "";
			if (oldVisibleColumns == "") return "";

			try {
				foreach (var x in JToken.Parse(portfolioItemColumns)) {
					string val = x.ToString();
					bool hideColumn = true;
					foreach (var x2 in JToken.Parse(oldVisibleColumns)) {
						if (x2.ToString() == val) {
							hideColumn = false;
						}
					}

					if (hideColumn == true) {
						hideColumns = hideColumns + "\"" + val + "\",";
					}
				}
			} catch (Exception e) {
				Console.WriteLine(e.Message);
				throw;
			}


			if (hideColumns.EndsWith(",") == true) {
				hideColumns = hideColumns.Substring(0, hideColumns.Length - 1); //remove comma
			}

			hideColumns = hideColumns + "]";

			JObject a;
			try {
				a = JObject.Parse(oldConfigJson);
			} catch (Exception e) {
				Console.WriteLine(e.Message);
				throw;
			}

			JObject newConfigObject = a;
			bool configFlipSuccess = false;
			string returnStr = "";
			foreach (var x in newConfigObject) {
				if (x.Key == "filters") {
					foreach (var x2 in JToken.Parse(x.Value.ToString())) {
						if (x2.First.Path == "columns") {
							newConfigObject.SelectToken(x.Key).SelectToken(x2.First.Path)
								.Replace(JToken.Parse(hideColumns));
							returnStr = newConfigObject.ToString();
							configFlipSuccess = true;
						}
					}
				}
			}

			if (configFlipSuccess == false) {
				Diag.LogToConsole("Unable to resolve flipConfigJsonFilterColumns() function. Error code 3.",
					Diag.LogSeverities.Error);
				throw new Exception();
			}


			return returnStr;
		}

		private void updateConfigValues(DataTable newDt, IConnections db, string targetDtInDb) {
			bool savedDtTarget = targetDtInDb == "instance_configurations_saved" == true;

			foreach (DataRow dr in newDt.Rows) {
				int userId = Convert.ToInt32(dr["user_id"]);
				string instance = Convert.ToString(dr["instance"]);
				string group = Convert.ToString(dr["group"]);
				string identifier = Convert.ToString(dr["identifier"]);
				string oldValue = Convert.ToString(dr["old_value"]);
				string newValue = Convert.ToString(dr["new_value"]);
				string sqlUpdateAndSet = "UPDATE " + targetDtInDb + " SET value = '" + newValue + "'";
				string whereSql = "WHERE instance = '" + instance + "'";
				whereSql += " AND [group] = '" + group + "'";
				whereSql += " AND user_id = " + userId;
				whereSql += " AND identifier = '" + identifier + "'";
				whereSql += " AND value = '" + oldValue + "'";
				if (savedDtTarget == true) {
					whereSql += " AND user_configuration_id = " + Convert.ToInt32(dr["user_configuration_id"]);
				}

				db.ExecuteUpdateQuery(sqlUpdateAndSet + " " + whereSql, null);
			}
		}
	}
}