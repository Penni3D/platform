DECLARE @ItemColumnId INT;
DECLARE @ContainerColumnId INT;
DECLARE current_state_cursor CURSOR
    FOR SELECT item_column_id
        FROM item_columns
        WHERE [name] = 'current_state'

OPEN current_state_cursor

FETCH NEXT FROM current_state_cursor INTO @ItemColumnId
WHILE @@FETCH_STATUS = 0
    BEGIN

        UPDATE item_columns SET status_column = 1 WHERE item_column_id = @ItemColumnId

        SET @ContainerColumnId =
                (SELECT process_container_column_id FROM process_container_columns WHERE item_column_id = @ItemColumnId)

        DELETE FROM process_container_columns WHERE process_container_column_id = @ContainerColumnId

        FETCH NEXT FROM current_state_cursor INTO
            @ItemColumnId

    END
CLOSE current_state_cursor;
DEALLOCATE current_state_cursor;
GO
DELETE
FROM [instance_user_configurations]
where identifier = '4'
  AND (SELECT COUNT(*) FROM [process_portfolios] WHERE process_portfolio_id = 4 and process = 'task') > 0