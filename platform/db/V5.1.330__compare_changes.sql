ALTER TABLE instance_union_rows ADD [guid] UNIQUEIDENTIFIER DEFAULT newsequentialid() NOT NULL



GO



ALTER TABLE instance_union_processes ADD [guid] UNIQUEIDENTIFIER DEFAULT newsequentialid() NOT NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceUnion_GetId]
(
	@instance NVARCHAR(10),
	@variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
	DECLARE @id int
	SELECT TOP 1 @id = instance_union_id FROM instance_unions WHERE instance = @instance AND variable = @variable
	RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceUnion_GetVariable]
(
	@instance_union_id int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	SELECT TOP 1 @var = variable FROM instance_unions WHERE instance_union_id = @instance_union_id
	RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceUnionRow_GetId]
(
	@guid uniqueidentifier
)
RETURNS int
AS
BEGIN
	DECLARE @id int
	SELECT TOP 1 @id = instance_union_row_id FROM instance_union_rows WHERE [guid] = @guid
	RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceUnionRow_GetGuid]
(
	@instance_union_row_id int
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @var uniqueidentifier
	SELECT TOP 1 @var = [guid] FROM instance_union_rows WHERE instance_union_row_id = @instance_union_row_id
	RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceUnionProcess_GetId]
(
	@guid uniqueidentifier
)
RETURNS int
AS
BEGIN
	DECLARE @id int
	SELECT TOP 1 @id = instance_union_process_id FROM instance_union_processes WHERE [guid] = @guid
	RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceUnionProcess_GetGuid]
(
	@instance_union_process_id int
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @var uniqueidentifier
	SELECT TOP 1 @var = [guid] FROM instance_union_processes WHERE instance_union_process_id = @instance_union_process_id
	RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									IF @type = 0 
										BEGIN
											SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
											SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
										END

									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
											SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
										END
				
									IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @listVal NVARCHAR(max)
													DECLARE @listItemId int
													SET @var3 = replace(@var3, '[', '')
													SET @var3 = replace(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = replace(@var4,  @listVal, CAST(@listItemId AS NVARCHAR(max) ) )

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = replace(@dataAdditional2, @var3, @var4)
												END
										END
									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									IF @type = 2 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
	
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":"tab_' + @var4 + '"')
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END