DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
	BEGIN
	DECLARE @process nvarchar (50) = 'user_group'
	DECLARE @process2 nvarchar (50) = 'user_group_type'
	DECLARE @t1 int
	
	DECLARE @itemId1 int
	DECLARE @itemId2 int
		
	    --Create process
  EXEC app_ensure_list @instance, @process2, @process2, 0

  EXEC dbo.app_ensure_column @instance,@process2, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
  EXEC dbo.app_ensure_column @instance,@process2, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
  EXEC dbo.app_ensure_column @instance,@process2, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
    
   DECLARE @sql nvarchar(max)
    DECLARE @group_name nvarchar(max)
   
   SET @group_name = 'Admin'
   INSERT INTO items (instance, process) VALUES (@instance, @process2)
    select top 1  @itemId1 = item_id from items where instance = @instance and process = @process2 order BY item_id DESC 
    
    SET @sql = 'update _' + @instance + '_' + @process2 + ' set list_item = ''' + @group_name + '''  WHERE item_id = ' + CAST (@itemId1 AS nvarchar(max))
    EXEC (@sql)   
  
   
   SET @group_name = 'Normal'
   INSERT INTO items (instance, process) VALUES (@instance, @process2)
    select top 1  @itemId2 = item_id from items where instance = @instance and process = @process2 order BY item_id DESC 
    
    SET @sql = 'update _' + @instance + '_' + @process2 + ' set list_item = ''' + @group_name + ''' WHERE item_id = ' +  CAST (@itemId2 AS nvarchar(max))
    EXEC (@sql)
    
       
 select top 1  @t1 = process_tab_id from process_tabs where instance = @instance and process = @process

	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'admin_container', @t1, 1, @container_id = @c1 OUTPUT
	
	--Create fields
	DECLARE @i1 INT
	EXEC dbo.app_ensure_column @instance, @process, 'group_type', @c1, 6, 1, 1, 0, null,  0, 0, @process2,  @item_column_id = @i1 OUTPUT

	
	SET @group_name = 'KETO Administrators'
  SET @sql = 'INSERT INTO item_data_process_selections  (item_id, item_column_id, selected_item_id) SELECT item_id, ' + CAST(@i1 AS nvarchar(max)) + ', ' + CAST (@itemId2 AS nvarchar(max)) + ' FROM _' + @instance + '_' + @process + ' WHERE usergroup_name <> ''' + @group_name + ''''
        EXEC (@sql)
        
  SET @group_name = 'KETO Administrators'
  SET @sql = 'INSERT INTO item_data_process_selections  (item_id, item_column_id, selected_item_id) SELECT item_id, ' + CAST(@i1 AS nvarchar(max)) + ', ' + CAST (@itemId1 AS nvarchar(max)) + ' FROM _' + @instance + '_' + @process + ' WHERE usergroup_name = ''' + @group_name + ''''
     EXEC (@sql)
 
 DECLARE @cond1 int 
 
 SELECT @cond1 = condition_id FROM conditions WHERE instance = @instance AND process = @process
 
 UPDATE conditions SET condition_json = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@i1 AS nvarchar(max)) + ',"Value":' + CAST (@itemId2 AS nvarchar(max)) + '},"OperatorId":null}]},"OperatorId":1}]'
 WHERE condition_id = @cond1
 
 
 
 	DECLARE @cId INT
 	EXEC app_ensure_condition @instance, @process, 0, 0, 'ADMIN_CONTAINER', 'null', @condition_id = @cId OUTPUT
 	
 	DELETE FROM condition_features WHERE condition_id = @cId
 	DELETE FROM condition_states WHERE condition_id = @cId
 	
	INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'meta')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
		INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')
	
	INSERT INTO condition_containers (condition_id, process_container_id) VALUES (@cId, @c1)
	
	  SET @group_name = 'KETO Administrators'
	SET @sql = 'INSERT INTO condition_user_groups  (condition_id, item_id) SELECT ' + CAST(@cId AS nvarchar(max)) + ', item_id FROM _' + @instance + '_' + @process + ' WHERE usergroup_name = ''' + @group_name + ''''
     EXEC (@sql)
 
	
	
	EXEC app_ensure_condition @instance, @process, 0, 0, 'ADMIN_GROUPS', 'null', @condition_id = @cId OUTPUT
 	
 	DELETE FROM condition_features WHERE condition_id = @cId
 	DELETE FROM condition_states WHERE condition_id = @cId
 	
	INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')
	
	
	
 UPDATE conditions SET condition_json = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@i1 AS nvarchar(max)) + ',"Value":' + CAST (@itemId1 AS nvarchar(max)) + '},"OperatorId":null}]},"OperatorId":1}]'
 WHERE condition_id = @cId
 
 	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'ADMIN_GROUPS', @portfolio_id = @p1 OUTPUT

	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@cId, @p1)
	
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) SELECT @p1, item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND data_type <> 5
	
	FETCH  NEXT FROM instance_cursor INTO @instance
END