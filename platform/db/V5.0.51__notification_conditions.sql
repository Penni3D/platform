ALTER TABLE notification_conditions
ADD in_use tinyint not null
CONSTRAINT nc_in_use_default DEFAULT 0
GO
