
  DECLARE @time INT
  DECLARE @json NVARCHAR(MAX)
  DECLARE @newJson NVARCHAR(MAX)
  DECLARE @scheduleId INT

  DECLARE cursor_schedule CURSOR
  FOR SELECT schedule_id FROM instance_schedules
	OPEN cursor_schedule
	FETCH NEXT FROM cursor_schedule INTO @scheduleId
		WHILE @@FETCH_STATUS = 0
    BEGIN
       SET @time = CAST(SUBSTRING((SELECT schedule_json FROM instance_schedules WHERE schedule_id = @scheduleId), 19, 1) AS INT);
  SET @json = (SELECT schedule_json FROM instance_schedules WHERE schedule_id = @scheduleId)
  IF @time < 10
  BEGIN

  SET @newJson = (SELECT STUFF(@json, 19,0, '0'));

  UPDATE instance_schedules SET schedule_json = @newJson
  WHERE schedule_id = @scheduleId


  END
		
        FETCH NEXT FROM cursor_schedule INTO 
            @scheduleId
    END;
	
CLOSE cursor_schedule;
DEALLOCATE cursor_schedule;