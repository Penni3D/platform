SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[itemColumns_DaIdReplace]
(
	@data_type INT,
	@dataAdditional NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	IF @dataAdditional IS NOT NULL
		BEGIN
			IF @data_type = 25
				BEGIN
					IF ISNUMERIC(@dataAdditional) = 1
						BEGIN
							DECLARE @variable NVARCHAR(MAX)
							SELECT @variable = variable FROM process_dashboard_charts WHERE process_dashboard_chart_id = CAST(@dataAdditional AS INT)
							IF @variable IS NOT NULL
								BEGIN
									SET @dataAdditional = @variable
								END
						END
				END
		END
	RETURN @dataAdditional
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[itemColumns_DaVarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	IF @dataAdditional IS NOT NULL
		BEGIN
			IF @data_type = 25
				BEGIN
					IF @dataAdditional IS NOT NULL
						BEGIN
							DECLARE @process_dashboard_chart_id INT
							SELECT @process_dashboard_chart_id = process_dashboard_chart_id FROM process_dashboard_charts WHERE variable = @dataAdditional AND instance = @instance
							IF @process_dashboard_chart_id IS NOT NULL
								BEGIN
									SET @dataAdditional = @process_dashboard_chart_id
								END
							ELSE
								BEGIN
									SET @dataAdditional = NULL
								END
						END
				END
		END
	RETURN @dataAdditional
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processDashboardChart_GetIds]
(
	@instance NVARCHAR(10),
	@variables NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @ids NVARCHAR(MAX)
	SET @ids = STUFF((SELECT ',' + CAST(process_dashboard_chart_id AS NVARCHAR) FROM process_dashboard_charts WHERE variable IN (SELECT value FROM STRING_SPLIT(@variables, ',')) AND instance = @instance ORDER BY variable ASC FOR XML PATH('')), 1, 1, '')
	IF @ids IS NULL	BEGIN
		SET @ids = ''
	END
	RETURN @ids
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processDashboardChart_GetVariables]
(
	@process_dashboard_chart_ids NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @variables NVARCHAR(MAX)
	SET @variables = STUFF((SELECT ',' + variable FROM process_dashboard_charts WHERE process_dashboard_chart_id IN (SELECT value FROM STRING_SPLIT(@process_dashboard_chart_ids, ',')) FOR XML PATH('')), 1, 1, '')
	IF @variables IS NULL	BEGIN
		SET @variables = ''
	END
	RETURN @variables
END