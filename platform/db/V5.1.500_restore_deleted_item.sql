ALTER TABLE items_deleted ADD [guid] UNIQUEIDENTIFIER NULL



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
    AS
    SET XACT_ABORT OFF

DECLARE @ids NVARCHAR(MAX) = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM deleted FOR XML PATH(''))
DECLARE @sql NVARCHAR(MAX)
DECLARE @tName NVARCHAR(MAX)
DECLARE @cName NVARCHAR(MAX)
DECLARE @lvl AS INT = 0

IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
CREATE TABLE #DeleteItems (item_id INT, ord INT)

DECLARE @GetTables CURSOR
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
    SELECT ccu.TABLE_NAME, ccu.COLUMN_NAME 
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
    INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
    WHERE UNIQUE_CONSTRAINT_NAME = 'PK_items' AND SUBSTRING(ccu.TABLE_NAME, 1, 1) = '_'

DECLARE @idps_childs NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 2 FOR XML PATH(''))
DECLARE @idps_parents NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 3 FOR XML PATH(''))

WHILE @ids IS NOT NULL BEGIN
	IF OBJECT_ID('tempdb..#LoopItems') IS NOT NULL DROP TABLE #LoopItems
	CREATE TABLE #LoopItems (item_id INT, ord INT)

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @sql = ' WITH rec AS ( 
			 SELECT t.item_id, t.' + @cName + ', ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			 FROM ' + @tName + ' t 
			 WHERE t.' + @cName + ' IN (-1' + @ids + ')
			 ) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
		EXECUTE (@sql)
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

	SET @sql = ' WITH rec AS ( 
			SELECT t.selected_item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_childs + ') AND item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT selected_item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @sql = ' WITH rec AS ( 
			SELECT t.item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_parents + ') AND selected_item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @ids = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems) FOR XML PATH(''))

	INSERT INTO #DeleteItems (item_id, ord) SELECT item_id, ord FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems)

	SET @lvl = @lvl + 1
END

DELETE  FROM item_subitems 				  WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_tabs_subprocess_data WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM condition_user_groups WHERE  item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_baseline WHERE  process_item_id IN (SELECT item_id FROM #DeleteItems)

DECLARE @delId AS INT
DECLARE @GetIds CURSOR

SET @GetIds = CURSOR FOR SELECT item_id FROM #DeleteItems GROUP BY item_id, ord ORDER BY ord DESC

OPEN @GetIds
FETCH NEXT FROM @GetIds INTO @delId

WHILE @@FETCH_STATUS = 0 BEGIN
	INSERT INTO items_deleted (item_id, instance, process, deleted_date, [guid]) SELECT item_id, instance, process, getUTCdate(), [guid] FROM items WHERE item_id = @delId
    DELETE FROM items WHERE item_id = @delId
    FETCH NEXT FROM @GetIds INTO @delId
END
CLOSE @GetIds

DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
UPDATE dd SET dd.link_item_id = NULL FROM item_data_process_selections dd INNER JOIN deleted d ON dd.link_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id

DELETE dd FROM instance_user_configurations_saved dd INNER JOIN deleted d ON dd.user_id = d.item_id
DELETE dd FROM instance_user_configurations dd INNER JOIN deleted d ON dd.user_id = d.item_id

DELETE dd FROM condition_user_groups dd INNER JOIN deleted d ON dd.item_id = d.item_id

DELETE dd FROM process_baseline dd INNER JOIN deleted d ON dd.process_item_id = d.item_id

--Insert into deleted items
INSERT INTO items_deleted (item_id, instance, process, deleted_date, [guid]) SELECT item_id, instance, process, getUTCdate(), [guid] FROM deleted

--Finally delete the item
DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[items_BeforeInsertRow] ON [dbo].[items] INSTEAD OF INSERT AS
    BEGIN
        IF SESSION_CONTEXT(N'restore_item_id') IS NULL BEGIN -- New item
			INSERT INTO items (instance, process, deleted, created_by)
			SELECT instance, process, deleted, CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
			FROM Inserted
		END ELSE BEGIN -- Restore deleted item
			SET IDENTITY_INSERT items ON
			IF SESSION_CONTEXT(N'restore_guid') IS NULL BEGIN -- No GUID
				INSERT INTO items (item_id, instance, process, deleted, created_by)
				SELECT CAST(SESSION_CONTEXT(N'restore_item_id') AS INT), instance, process, deleted, CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
				FROM Inserted
			END ELSE BEGIN -- Has GUID
				INSERT INTO items (item_id, [guid], instance, process, deleted, created_by)
				SELECT CAST(SESSION_CONTEXT(N'restore_item_id') AS INT), CAST(SESSION_CONTEXT(N'restore_guid') AS UNIQUEIDENTIFIER), instance, process, deleted, CONVERT(INT, CONVERT(VARBINARY(4), CONTEXT_INFO()))
				FROM Inserted
			END
			SET IDENTITY_INSERT items OFF
			EXEC sp_set_session_context @key = N'restore_item_id', @value = NULL
			EXEC sp_set_session_context @key = N'restore_guid', @value = NULL
		END
    END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[get_item_data_process_selections_with_id](@d datetime, @i int)
RETURNS @rtab TABLE ([item_id] int NOT NULL ,[item_column_id] int NOT NULL ,[selected_item_id] int NOT NULL ,[link_item_id] int NULL ,[archive_userid] int NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
AS
BEGIN	
	INSERT @rtab
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start]
		FROM archive_item_data_process_selections WHERE archive_id IN (
			SELECT MAX(archive_id)
			FROM archive_item_data_process_selections
			WHERE item_id = @i AND archive_start <= @d AND archive_end > @d AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN
			(SELECT CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) FROM  item_data_process_selections WHERE item_id = @i AND archive_start <= @d)
			GROUP BY [item_column_id],[item_id],[selected_item_id]
			)
		UNION 
		SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start] 
		FROM item_data_process_selections 
		WHERE archive_start <= @d AND item_id = @i
	RETURN
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_restore_item]
	@item_id INT,
	@archive_id INT = NULL,
	@archive_start DATETIME = NULL
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @instance NVARCHAR(10)
	DECLARE @process NVARCHAR(50)
	DECLARE @guid UNIQUEIDENTIFIER
	DECLARE @columns NVARCHAR(MAX)
	DECLARE @mode TINYINT

	IF @archive_id IS NOT NULL OR @archive_start IS NOT NULL BEGIN
		SELECT @instance = instance, @process = process FROM items WHERE item_id = @item_id

		IF @instance IS NULL OR @process IS NULL BEGIN
			SELECT @instance = instance, @process = process, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

			SET @mode = 1 -- Restore deleted item with historic values
		END
		ELSE BEGIN
			SET @mode = 2 -- Restore item with historic values
		END

		IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
			IF @archive_id IS NOT NULL BEGIN
				SET @sql = 'SELECT @archive_start = archive_start 
							FROM archive__' + @instance + '_' + @process + ' 
							WHERE archive_id = ' + CAST(@archive_id AS NVARCHAR)
				EXEC sp_executesql @sql, N'@archive_start DATETIME OUTPUT', @archive_start OUTPUT
			END
			ELSE IF @archive_start IS NOT NULL BEGIN
				SET @sql = 'SELECT TOP 1 @archive_id = archive_id 
							FROM archive__' + @instance + '_' + @process + ' 
							WHERE item_id = ' + CAST(@item_id AS NVARCHAR) + ' 
							AND archive_start <= ''' + CONVERT(NVARCHAR, @archive_start, 121) + ''' 
							AND archive_end > ''' + CONVERT(NVARCHAR, @archive_start, 121) + ''' 
							ORDER BY archive_id DESC'
				EXEC sp_executesql @sql, N'@archive_id INT OUTPUT', @archive_id OUTPUT
			END
		END
	END
	ELSE BEGIN
		SET @mode = 1 -- Restore deleted item with latest values

		SELECT @instance = instance, @process = process, @guid = [guid] FROM items_deleted WHERE item_id = @item_id

		IF @instance IS NOT NULL AND @process IS NOT NULL BEGIN
			SET @sql = 'SELECT TOP 1 @archive_id = archive_id, @archive_start = archive_start  
						FROM archive__' + @instance + '_' + @process + ' 
						WHERE archive_type = 2 AND item_id = ' + CAST(@item_id AS NVARCHAR) + ' 
						ORDER BY archive_id DESC'
			EXEC sp_executesql @sql, N'@archive_id INT OUTPUT, @archive_start DATETIME OUTPUT', @archive_id OUTPUT, @archive_start OUTPUT
		END
	END

	IF @instance IS NOT NULL AND @process IS NOT NULL AND @archive_id IS NOT NULL AND @archive_start IS NOT NULL BEGIN
		IF @mode = 1 BEGIN
			EXEC sp_set_session_context @key = N'restore_item_id', @value = @item_id
			EXEC sp_set_session_context @key = N'restore_guid', @value = @guid
			INSERT INTO items (instance, process, deleted) VALUES (@instance, @process, 0)
		END
		ELSE IF @mode = 2 BEGIN
			DELETE FROM item_data_process_selections WHERE item_id = @item_id
		END

		SELECT @columns = STUFF((SELECT ', t.' + COLUMN_NAME + ' = a.' + COLUMN_NAME 
								 FROM INFORMATION_SCHEMA.COLUMNS 
								 WHERE TABLE_NAME = '_' + @instance + '_' + @process AND COLUMN_NAME NOT IN ('item_id', 'order_no', 'archive_userid', 'archive_start') 
								 GROUP BY COLUMN_NAME 
								 FOR XML PATH('')), 1, 2, '')

		IF @columns <> '' BEGIN
			SET @sql = 'UPDATE t SET ' + @columns + ' 
						FROM _' + @instance + '_' + @process + ' t 
						INNER JOIN archive__' + @instance + '_' + @process + ' a ON a.item_id = t.item_id AND a.archive_id = ' + CAST(@archive_id AS NVARCHAR) + '  
						WHERE t.item_id = ' + CAST(@item_id AS NVARCHAR)
			EXEC(@sql)
		END

		INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id)
		SELECT idps.item_id, idps.item_column_id, idps.selected_item_id, idps.link_item_id
		FROM get_item_data_process_selections_with_id(@archive_start, @item_id) idps
		LEFT JOIN item_columns ic ON ic.item_column_id = idps.item_column_id
		LEFT JOIN items_deleted s_id ON s_id.item_id = idps.selected_item_id
		LEFT JOIN items_deleted l_id ON l_id.item_id = idps.link_item_id
		WHERE ic.item_column_id IS NOT NULL AND s_id.item_id IS NULL AND l_id.item_id IS NULL

		IF @mode = 1 BEGIN
			SET @sql = 'DELETE FROM archive__' + @instance + '_' + @process + ' WHERE archive_type = 2 AND item_id = ' + CAST(@item_id AS NVARCHAR)
			EXEC(@sql)

			DELETE FROM items_deleted WHERE item_id = @item_id
		END
	END
END