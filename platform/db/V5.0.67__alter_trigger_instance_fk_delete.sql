ALTER TRIGGER [dbo].[instance_fk_delete]
    ON [dbo].[instances]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM attachments dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM calendars dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_translations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_languages dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_logs dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_hosts dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_menu dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations_saved dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM processes dd	INNER JOIN deleted d ON dd.instance = d.instance

	DELETE dd FROM instance_comments dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations_saved dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM process_comments dd	INNER JOIN deleted d ON dd.instance = d.instance

	DELETE dd FROM instances dd	INNER JOIN deleted d ON dd.instance = d.instance
	