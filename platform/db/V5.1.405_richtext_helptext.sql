
ALTER TABLE process_container_columns
ALTER COLUMN help_text NVARCHAR(MAX);


  DECLARE @Helptext NVARCHAR (MAX);
  DECLARE @NewBody NVARCHAR (MAX);
  DECLARE @Code NVARCHAR (255);
  DECLARE @Translation NVARCHAR (MAX);
  DECLARE @ProcessContainerColumnId INT;
  DECLARE cursor_process CURSOR

  FOR SELECT help_text, process_container_column_id from process_container_columns where help_text IS NOT NULL AND help_text <> ''

	OPEN cursor_process

	FETCH NEXT FROM cursor_process INTO @Helptext, @ProcessContainerColumnId 

		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			DECLARE cursor_help CURSOR

				FOR SELECT translation,code FROM process_translations WHERE variable = @Helptext
				SET @NewBody = '{';
					OPEN cursor_help	
						FETCH NEXT FROM cursor_help INTO @Translation, @Code
							WHILE @@FETCH_STATUS = 0
								BEGIN

								SET @NewBody = (SELECT CONCAT(@NewBody,'"'+@Code+'":{"html":"'+@Translation+'","delta":{"ops":[{"insert":"' + @Translation+'"}]}},'))
								
								FETCH NEXT FROM cursor_help INTO @Translation, @Code
								END;

								CLOSE cursor_help
								DEALLOCATE cursor_help
								SET @NewBody = left(@NewBody + ',,', charindex(',,', @NewBody+ ',,') - 1)
								SET @NewBody = (SELECT CONCAT(@NewBody, '}'))
								UPDATE process_container_columns SET help_text = @NewBody WHERE process_container_column_id = @ProcessContainerColumnId
							

			FETCH NEXT FROM cursor_process INTO 
				@Helptext,@ProcessContainerColumnId
		END;
	
	CLOSE cursor_process;
	DEALLOCATE cursor_process;
