SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceSchedule_OptionsIdReplace]
(
	@configOptions NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @configOptions IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('@ProcessPortfolioId', 0), 
				('@ProcessActionId', 1)
		
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @configOptions)
					IF @startPos > 0
						BEGIN  
							SET @strToFoundLength = LEN(@strToFound) + 2
									
							SET @endPos1 = CHARINDEX(',', @configOptions, @startPos)
							SET @endPos2 = CHARINDEX('}', @configOptions, @startPos)

							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@configOptions, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@configOptions, @startPos, @endPos2 - @startPos)
								END
				
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
				
							IF @type = 0
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetVariable(@var3)
													IF @var4 IS NOT NULL
														BEGIN
															SET @configOptions = STUFF(@configOptions, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
								END

							IF @type = 1
								BEGIN
									IF ISNUMERIC(@var3) = 1
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetVariable(@var3)
													IF @var4 IS NOT NULL
														BEGIN
															SET @configOptions = STUFF(@configOptions, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
								END
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END							
		END
	RETURN @configOptions
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[instanceSchedule_OptionsVarReplace]
(
	@instance NVARCHAR(MAX),
	@configOptions NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @configOptions IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('@ProcessPortfolioId', 0), 
				('@ProcessActionId', 1)
					
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
					
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
			WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @configOptions)
					IF @startPos > 0
						BEGIN
							SET @strToFoundLength = LEN(@strToFound) + 2
									
							SET @endPos1 = CHARINDEX(',', @configOptions, @startPos)
							SET @endPos2 = CHARINDEX('}', @configOptions, @startPos)

							IF @endPos2 > @endPos1 AND @endPos1 > 0
								BEGIN
									SET @var2 = SUBSTRING(@configOptions, @startPos, @endPos1 - @startPos)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@configOptions, @startPos, @endPos2 - @startPos)
								END
									
							SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

							IF @type = 0
								BEGIN
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)
											IF @var4 IS NULL
												BEGIN
													SET @var4 = 0
												END
											SET @configOptions = STUFF(@configOptions, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
									
							IF @type = 1
								BEGIN
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.processAction_GetId(@instance, @var3)
											IF @var4 IS NULL
												BEGIN
													SET @var4 = '0'
												END
											SET @configOptions = STUFF(@configOptions, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
										END
								END
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END							
		END
	RETURN @configOptions
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[itemColumns_Da2SubIdReplace]
(
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX),
	@dataAdditional2Sub NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2Sub IS NOT NULL
		BEGIN
			IF @data_type = 20
				BEGIN
					IF @dataAdditional2 = '5'
						BEGIN
							INSERT INTO @strTable (str_to_found, strtype) 
							VALUES
								('portfolioId', 0) 
		
							SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
							SELECT str_to_found, strtype FROM @strTable 
			
							OPEN @getStrs
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  					WHILE (@@FETCH_STATUS = 0) 
								BEGIN
									SET @startPos = CHARINDEX(@strToFound, @dataAdditional2Sub)
									IF @startPos > 0
										BEGIN  
											SET @strToFoundLength = LEN(@strToFound) + 2
									
											SET @endPos1 = CHARINDEX(',', @dataAdditional2Sub, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2Sub, @startPos)

											IF @endPos2 > @endPos1 AND @endPos1 > 0
												BEGIN
													SET @var2 = SUBSTRING(@dataAdditional2Sub, @startPos, @endPos1 - @startPos)
												END
											ELSE
												BEGIN
													SET @var2 = SUBSTRING(@dataAdditional2Sub, @startPos, @endPos2 - @startPos)
												END
				
											SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)
				
											IF @type = 0
												BEGIN
													IF ISNUMERIC(@var3) = 1
														BEGIN
															IF CAST(@var3 AS INT) > 0
																BEGIN
																	SET @var4 = dbo.processPortfolio_GetVariable(@var3)
																	IF @var4 IS NOT NULL
																		BEGIN
																			SET @dataAdditional2Sub = STUFF(@dataAdditional2Sub, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
																		END
																END
														END
												END
										END
									FETCH NEXT FROM @getStrs INTO @strToFound, @type
								END							
						END
				END
		END
	RETURN @dataAdditional2Sub
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[itemColumns_Da2SubVarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX),
	@dataAdditional2Sub NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound NVARCHAR(MAX)
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @strToFoundLength INT

	IF @dataAdditional2Sub IS NOT NULL
		BEGIN
			IF @data_type = 20
				BEGIN
					IF @dataAdditional2 = '5'
						BEGIN
							INSERT INTO @strTable (str_to_found, strtype) 
							VALUES
								('portfolioId', 0)
					
							SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
							SELECT str_to_found, strtype FROM @strTable 
					
							OPEN @getStrs
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
					
							WHILE (@@FETCH_STATUS = 0) 
								BEGIN
									SET @startPos = CHARINDEX(@strToFound, @dataAdditional2Sub)
									IF @startPos > 0
										BEGIN
											SET @strToFoundLength = LEN(@strToFound) + 2
									
											SET @endPos1 = CHARINDEX(',', @dataAdditional2Sub, @startPos)
											SET @endPos2 = CHARINDEX('}', @dataAdditional2Sub, @startPos)

											IF @endPos2 > @endPos1 AND @endPos1 > 0
												BEGIN
													SET @var2 = SUBSTRING(@dataAdditional2Sub, @startPos, @endPos1 - @startPos)
												END
											ELSE
												BEGIN
													SET @var2 = SUBSTRING(@dataAdditional2Sub, @startPos, @endPos2 - @startPos)
												END
									
											SET @var3 = SUBSTRING(@var2, @strToFoundLength + 1, LEN(@var2) - @strToFoundLength)

											IF @type = 0
												BEGIN
													IF LEN(@var3) > 0
														BEGIN
															SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)
															IF @var4 IS NULL
																BEGIN
																	SET @var4 = 0
																END
															SET @dataAdditional2Sub = STUFF(@dataAdditional2Sub, @startPos + @strToFoundLength, LEN(@var2) - @strToFoundLength, @var4)
														END
												END
										END
									FETCH NEXT FROM @getStrs INTO @strToFound, @type
								END							
						END
				END
		END
	RETURN @dataAdditional2Sub
END