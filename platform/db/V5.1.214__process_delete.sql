﻿ALTER  TRIGGER [dbo].[processes_fk_delete]
    ON [dbo].[processes]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.parent_process = d.process
	DELETE dd FROM item_columns dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM item_tables dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM items dd						INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_groups	dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_tabs dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_containers dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_portfolios dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_tabs_subprocesses dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM conditions dd				
	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_actions dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_dashboard_charts dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process

	DELETE dd FROM processes dd					INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process

	DELETE dd FROM instance_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process


GO 


exec app_set_archive_user_id 1
update processes set list_process = 0, process_type = 1 where process = 'process_comments'