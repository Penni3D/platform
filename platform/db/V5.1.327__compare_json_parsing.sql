SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[condition_IdReplaceProc]
(
	@condition_json NVARCHAR(MAX),
	@replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;  
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.itemColumn_GetVariable(@var3)				
					IF @var4 is not null 
						BEGIN
							SELECT  @condition_json = stuff(@condition_json, charindex(@var2, @condition_json), len(@var2), 'ItemColumnId":' + @var4)
						END

					DECLARE @replaced AS NVARCHAR (max)
					IF dbo.IsListColumn(@var3)  > 0
						BEGIN					
							DECLARE @idCursor CURSOR 
							DECLARE @listItemId INT 
							DECLARE @value nvarchar(max)

							SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
							SELECT item_id FROM dbo.get_ListColumnIds(@var3);

							OPEN @idCursor
							FETCH NEXT FROM @idCursor INTO @listItemId
							WHILE (@@FETCH_STATUS = 0) 
								BEGIN
									SELECT @value = cast(dbo.item_GetGuid(@listItemId) AS NVARCHAR(max))
									IF @listItemId is not null and @value is not null 
										BEGIN
											SET @replaced = replace(@condition_json, '"Value":' + CAST(@listItemId AS NVARCHAR) + '}', '"Value":"' + @value + '"}')
											IF @replaced is not null
												BEGIN
													SET @condition_json = @replaced
												END
										END

									FETCH NEXT FROM @idCursor INTO @listItemId
								END
						END

						IF @endPos2 > @endPos1
							BEGIN
								SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1 + LEN(@var4))
							END
						ELSE
							BEGIN
								SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2 + LEN(@var4))
							END
				END
			ELSE
				BEGIN
					set @replaced = ''
				END

			CONTINUE  
		END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  	
			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)

			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.itemColumn_GetVariable(@var3)		
					IF @var4 is not null 
						BEGIN
							SELECT  @condition_json = stuff(@condition_json, charindex(@var2, @condition_json), len(@var2), 'UserColumnId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = charindex('UserColumnId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = charindex('UserColumnId', @condition_json, @endPos2 + LEN(@var4))
						END
					END
				ELSE
					BEGIN
						IF @var3 <> 'null'
							BEGIN
								SET @condition_json = NULL
							END
					END
			CONTINUE  
		END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)

			IF ISNUMERIC(@var3) = 1
				BEGIN
					SET @var4 = dbo.condition_GetVariable(@var3)		
					IF @var4 is not null 
						BEGIN
							SELECT  @condition_json = stuff(@condition_json, charindex(@var2, @condition_json), len(@var2), 'ConditionId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = charindex('ConditionId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = charindex('ConditionId', @condition_json, @endPos2 + LEN(@var4))
						END
				END
			ELSE
				BEGIN
					IF @var3 <> 'null'
						BEGIN
							SET @condition_json = NULL
						END
				END

			CONTINUE  
		END

	SET @replaced_json = @condition_json
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[condition_VarReplaceProc]
(
	@sourceInstance NVARCHAR(10),
	@targetInstance NVARCHAR(10),
	@condition_json NVARCHAR(MAX),
	@replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		
					IF @var4 is not null 
						BEGIN
							SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)
						END

					IF dbo.IsListColumn(dbo.itemColumn_GetId(@sourceInstance, @var3)) > 0
						BEGIN					
							DECLARE @idCursor CURSOR 
							DECLARE @listItemId INT 
							DECLARE @value NVARCHAR(max)
							DECLARE @id INT
	
							SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
							SELECT item_id FROM dbo.get_ListColumnIds(dbo.itemColumn_GetId(@sourceInstance, @var3));
	
							OPEN @idCursor
							FETCH NEXT FROM @idCursor INTO @listItemId
							WHILE (@@FETCH_STATUS = 0) 
								BEGIN
									SELECT @value = cast(dbo.item_GetGuid(@listItemId) AS NVARCHAR(max))
									IF @listItemId is not null and @value is not null 
										BEGIN
											SET @condition_json = replace(@condition_json, '"Value":"' + @value + '"}', '"Value":' + CAST(@listItemId AS NVARCHAR) + '}')
										END

									FETCH NEXT FROM @idCursor INTO @listItemId
								END
						END
				END

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1 + LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2 + LEN(@var4))
				END

			 CONTINUE  
		END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2) - 14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)
					IF @var4 is not null 
						BEGIN
							SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = charindex('UserColumnId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = charindex('UserColumnId', @condition_json, @endPos2 + LEN(@var4))
						END
				END

			CONTINUE  
		END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
		BEGIN  
			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1 - @startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2 - @startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2) - 13)
			
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
				BEGIN
					SET @var4 = dbo.condition_GetId(@targetInstance, @var3)		
					IF @var4 is not null 
						BEGIN
							SET @condition_json = replace(@condition_json, @var2, 'ConditionId":' + @var4)
						END

					IF @endPos2 > @endPos1
						BEGIN
							SET @startPos = charindex('ConditionId', @condition_json, @endPos1 + LEN(@var4))
						END
					ELSE
						BEGIN
							SET @startPos = charindex('ConditionId', @condition_json, @endPos2 + LEN(@var4))
						END
				END

			CONTINUE  
		END
 
	SET @replaced_json = @condition_json
END



GO



IF object_id(N'condition_IdReplace', N'FN') IS NOT NULL 
	DROP FUNCTION condition_IdReplace



GO



IF object_id(N'condition_VarReplace', N'FN') IS NOT NULL 
	DROP FUNCTION condition_VarReplace



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS=0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
		
									IF @type = 0 
										BEGIN
											SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
											SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
										END
							
									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
											SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
										END
				
									IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1-@startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2-@startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
									IF @type = 0 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 as int ) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
																END
					
															IF @endPos2 > @endPos1
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
																END
															ELSE
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
																END
														END
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @listVal NVARCHAR(max)
													DECLARE @guid uniqueidentifier
						
													SET @var3 = replace(@var3, '[', '')
													SET @var3 = replace(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
					
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
								  					WHILE (@@FETCH_STATUS=0) 
														BEGIN
															SELECT @guid = guid FROM items WHERE item_id = @listVal
															DECLARE @gu_id NVARCHAR(max)
															SET @gu_id = @guid
															IF @gu_id IS NULL
																BEGIN
																	SET @gu_id = 'NULL' 
																END
															SET @var4 = replace(@var4,  @listVal , CAST(@gu_id AS NVARCHAR(max) ) )

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = replace(@dataAdditional2, @var3, @var4)
												END
										END
									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS=0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
		
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1-@startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2-@startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
									IF @type = 0 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.itemColumn_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
																END
					
															IF @endPos2 > @endPos1
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
																END
															ELSE
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
																END
														END
												END
										END

									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processPortfolio_GetVariable(@var3)		
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
																END
					
															IF @endPos2 > @endPos1
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
																END
															ELSE
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
																END
														END
												END
										END

									IF @type = 2 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processAction_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
																END
					
															IF @endPos2 > @endPos1
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
																END
															ELSE
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
																END
														END
												END
										END

									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS=0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
		
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1-@startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2-@startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )

									IF @type = 0 
										BEGIN
											SET @var3 =  replace(replace(@var3, 'tab_', ''), '"', '')
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processTab_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
																END
					
															IF @endPos2 > @endPos1
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
																END
															ELSE
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
																END
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF ISNUMERIC(@var3) = 1 
												BEGIN
													IF CAST(@var3 AS int ) > 0
														BEGIN
															SET @var4 = dbo.processActionDialog_GetVariable(@var3)
															IF @var4 IS NOT NULL
																BEGIN
																	SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
																END
					
															IF @endPos2 > @endPos1
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
																END
															ELSE
																BEGIN
																	SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
																END
														END
												END
										END

									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
	@instance NVARCHAR(MAX),
	@data_type INT,
	@dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found nvarchar(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT

	IF @dataAdditional2 IS NOT NULL
		BEGIN

			IF @data_type = 16
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('@SourceProcessId', 0), 
						('@SourceColumn', 0), 
						('@OptionalList1Id', 0), 
						('@OptionalList1Val', 1),
						('@OptionalList2Id', 0), 
						('@OptionalList2Val', 1),
						('@OptionalOrderColumn', 0)
		
					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									IF @type = 0 
										BEGIN
											SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
											SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
										END

									IF @type = 1 
										BEGIN
											set @startPos = @startPos + 1
											SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
											SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
										END
				
									IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									IF @type = 1
										BEGIN
											IF LEN(@var3) > 0 AND @var3 <> ']'
												BEGIN
													DECLARE @listVal NVARCHAR(max)
													DECLARE @listItemId int
													SET @var3 = replace(@var3, '[', '')
													SET @var3 = replace(@var3, ']', '')
													SET @var4 = @var3

													DECLARE @delimited CURSOR
													SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
													SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
													OPEN @delimited
													FETCH NEXT FROM @delimited INTO @listVal
								
													WHILE (@@FETCH_STATUS = 0) 
														BEGIN
															IF @listVal = 'NULL' 
																BEGIN
																	SET @listItemId = 0
																END
															ELSE 
																BEGIN
																	SELECT @listItemId = item_id FROM items WHERE guid = @listVal
																	IF @listItemId IS NULL
																		BEGIN
																			SET @listItemId = 0
																		END
																END
															SET @var4 = replace(@var4,  @listVal, CAST(@listItemId AS NVARCHAR(max) ) )

															FETCH NEXT FROM @delimited INTO @listVal
														END
													CLOSE @delimited
													SET @dataAdditional2 = replace(@dataAdditional2, @var3, @var4)
												END
										END
									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type IN (5, 18, 21, 22, 65)
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('itemColumnId', 0), 
						('portfolioId', 1), 
						('actionId', 2),
						('endDateColumnId', 0),
						('milestoneColumnId', 0),
						('descriptionColumnId', 0),
						('ownerColumnId', 0)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									IF @type = 2 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processAction_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

			IF @data_type = 8
				BEGIN
					INSERT INTO @strTable (str_to_found, strtype) 
					VALUES
						('initialState', 0),
						('currentState', 0),
						('optionalDoneDialog', 1)

					SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
					SELECT str_to_found, strtype FROM @strTable 
			
					OPEN @getStrs
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  			WHILE (@@FETCH_STATUS = 0) 
						BEGIN
							SET @startPos = charindex(@strToFound, @dataAdditional2)
							WHILE @startPos > 0
								BEGIN  
									SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
									SET @endPos2 = charindex('}', @dataAdditional2, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2 - @startPos)
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
	
									IF @type = 0 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processTab_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":tab_' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END
										
									IF @type = 1 
										BEGIN
											IF LEN(@var3) > 0
												BEGIN
													SET @var4 = dbo.processActionDialog_GetId(@instance, @var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound + '":' + @var4)
														END

													IF @endPos2 > @endPos1
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1 + LEN(@var4))
														END
													ELSE
														BEGIN
															SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2 + LEN(@var4))
														END
												END
										END

									CONTINUE  
								END
							FETCH NEXT FROM @getStrs INTO @strToFound, @type
						END
				END

		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @dataAdditional2
END