DECLARE @instance NVARCHAR(10);
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor

FETCH NEXT FROM instance_cursor INTO @instance
WHILE @@fetch_status = 0
	BEGIN
        EXEC app_ensure_list @instance, 'list_wtr_location', 'wtr_location', 0
        EXEC dbo.app_ensure_column @instance,'user', 'default_location', 0, 6, 1, 0, 0, NULL, 0, 'list_wtr_location'
FETCH  NEXT FROM instance_cursor INTO @instance
END