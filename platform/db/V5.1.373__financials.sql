﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_list_budget_category]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'list_budget_category'

	--Archive user is 0
	DECLARE @BinVar VARBINARY(128);
	SET @BinVar = CONVERT(VARBINARY(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_list @instance, @process, @process, 0

	EXEC dbo.app_ensure_column @instance, @process, 'list_item', 0, 0, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'list_symbol', 0, 0, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'list_symbol_fill', 0, 0, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'income', 0, 1, 0, 0, NULL, 0, 0
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_list_budget_item_type]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'list_budget_item_type'

	--Archive user is 0
	DECLARE @BinVar VARBINARY(128);
	SET @BinVar = CONVERT(VARBINARY(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_list @instance, @process, @process, 0

	EXEC dbo.app_ensure_column @instance, @process, 'list_item', 0, 0, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'list_symbol', 0, 0, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'list_symbol_fill', 0, 0, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance, @process, 'type', 0, 1, 0, 0, NULL, 0, 0
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_budget_row]	-- Add the parameters for the stored procedure here
	@instance NVARCHAR(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'budget_row'

	--Archive user is 0
	DECLARE @BinVar VARBINARY(128);
	SET @BinVar = CONVERT(VARBINARY(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2

	EXEC app_ensure_column @instance, @process, 'project_item_id', 0, 1, 0, 0, NULL,  0, 0
	EXEC app_ensure_column @instance,@process, 'description', 0, 4, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'category_item_id', 0, 6, 0, 0, NULL,  0, 0, 'list_budget_category'
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_budget_item]	-- Add the parameters for the stored procedure here
	@instance NVARCHAR(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'budget_item'

	--Archive user is 0
	DECLARE @BinVar VARBINARY(128);
	SET @BinVar = CONVERT(VARBINARY(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 2

	--Create columns
	EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 5, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'type_item_id', 0, 6, 0, 0, NULL, 0, 0, 'list_budget_item_type'
	EXEC app_ensure_column @instance, @process, 'date', 0, 3, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'amount', 0, 2, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'project_item_id', 0, 1, 0, 0, NULL, 0, 0
	EXEC app_ensure_column @instance, @process, 'category_item_id', 0, 6, 0, 0, NULL, 0, 0, 'list_budget_category'

	-- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_item_id', 0, 6, 0, 0, NULL, 0, 0, 'budget_currency'
	-- add later when necessary: EXEC app_ensure_column @instance, @process, 'currency_rate', 0, 2, 0, 0, NULL, 0, 0
	-- add later when necessary: EXEC app_ensure_column @instance, @process, 'description', 0, 4, 0, 0, NULL,  0, 0
END



GO



BEGIN
	--Archive user is 0
	DECLARE @BinVar VARBINARY(128);
	SET @BinVar = CONVERT(VARBINARY(128), 0);
	SET CONTEXT_INFO @BinVar;

	DECLARE @instanceName NVARCHAR(10)
	DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''
	DECLARE @table NVARCHAR(MAX) = ''
  
	OPEN instance_cursor
	FETCH NEXT FROM instance_cursor INTO @instanceName
	WHILE @@fetch_status = 0 BEGIN
		SET @table = 'list_budget_category'
		SET @tableName = '_' + @instanceName + '_' + @table
		IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = @tableName)) BEGIN
			EXEC app_ensure_process_list_budget_category @instanceName

			INSERT INTO items (instance, process) VALUES (@instanceName, @table);
			DECLARE @itemId INT = @@identity;
			SET @sql = 'UPDATE ' + @tableName + ' SET list_item =''Cost'', income = 0 WHERE item_id = ' + CAST(@itemId AS nvarchar)
			EXEC (@sql)

			INSERT INTO items (instance, process) VALUES (@instanceName, @table);
			SET @itemId = @@identity;
			SET @sql = 'UPDATE ' + @tableName + ' SET list_item =''Income'', income = 1 WHERE item_id = ' + CAST(@itemId AS nvarchar)
			EXEC (@sql)
		END

		SET @table = 'list_budget_item_type'
		SET @tableName = '_' + @instanceName + '_' + @table
		IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = @tableName)) BEGIN
			EXEC app_ensure_process_list_budget_item_type @instanceName
      
			INSERT INTO items (instance, process) VALUES (@instanceName, @table);
			SET @itemId = @@identity;
			SET @sql = 'UPDATE ' + @tableName + ' SET list_item =''Planned'', type = 1 WHERE item_id = ' + CAST(@itemId AS nvarchar)
			EXEC (@sql)
      
			INSERT INTO items (instance, process) VALUES (@instanceName, @table);
			SET @itemId = @@identity;
			SET @sql = 'UPDATE ' + @tableName + ' SET list_item =''Estimated'', type = 2 WHERE item_id = ' + CAST(@itemId AS nvarchar)
			EXEC (@sql)

			INSERT INTO items (instance, process) VALUES (@instanceName, @table);
			SET @itemId = @@identity;
			SET @sql = 'UPDATE ' + @tableName + ' SET list_item =''Committed'', type = 3 WHERE item_id = ' + CAST(@itemId AS nvarchar)
			EXEC (@sql)

			INSERT INTO items (instance, process) VALUES (@instanceName, @table);
			SET @itemId = @@identity;
			SET @sql = 'UPDATE ' + @tableName + ' SET list_item =''Actual'', type = 4 WHERE item_id = ' + CAST(@itemId AS nvarchar)
			EXEC (@sql)
		END
		ELSE IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'type' AND Object_ID = Object_ID(N'dbo.' + @tableName)) BEGIN
			EXEC dbo.app_ensure_column @instanceName, @table, 'type', 0, 1, 0, 0, NULL, 0, 0
		END

		IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = '_' + @instanceName + '_budget_row')) BEGIN
			EXEC app_ensure_process_budget_row @instanceName
		END

		IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = '_' + @instanceName + '_budget_item')) BEGIN
			EXEC app_ensure_process_budget_item @instanceName
		END
      
		FETCH  NEXT FROM instance_cursor INTO
		@instanceName
	END
	CLOSE instance_cursor
	DEALLOCATE instance_cursor
END