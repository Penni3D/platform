SET NOCOUNT ON;

	DECLARE @process NVARCHAR(50) = 'notification_tag_links'
	DECLARE @instance NVARCHAR(MAX);
	DECLARE @GetTables CURSOR

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT instance FROM instances
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @instance

	exec app_set_archive_user_id 0 

	WHILE (@@FETCH_STATUS=0) BEGIN
		  EXEC dbo.app_ensure_column @instance,@process,'view_state', 0, 0, 1, 0, 0, NULL, 0, 0
    	EXEC dbo.app_ensure_column @instance,@process,'link_param', 0, 0, 1, 0, 0, NULL, 0, 0
		FETCH NEXT FROM @GetTables INTO @instance
	END
	CLOSE @GetTables

