SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[process_dashboard_chart_links](
	[process_dashboard_chart_link_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[dashboard_id] [int] NOT NULL,
	[source_field] [nvarchar](50) NULL,
	[link_process] [nvarchar](50) NULL,
	[link_process_field] [nvarchar](50) NULL,
	[chart_ids] [nvarchar](max) NULL
 CONSTRAINT [PK_process_dashboard_linked_charts] PRIMARY KEY CLUSTERED 
(
	[process_dashboard_chart_link_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

EXEC app_ensure_archive 'process_dashboard_chart_links'