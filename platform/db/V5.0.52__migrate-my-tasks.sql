DECLARE @instance NVARCHAR(10)

DECLARE loopCursor CURSOR LOCAL FAST_FORWARD FOR
    SELECT instance
    FROM instances
OPEN loopCursor
FETCH NEXT FROM loopCursor INTO @instance
WHILE @@FETCH_STATUS = 0 BEGIN
	DECLARE @i9 INT
	DECLARE @i9_old INT
	DECLARE @i19 INT
	DECLARE @i18 INT
	DECLARE @p2 INT
	DECLARE @con18 INT
	DECLARE @process NVARCHAR(50) = 'task'
	DECLARE @consql NVARCHAR(MAX)

IF EXISTS (SELECT * FROM processes WHERE instance = @instance AND process = @process )

	EXEC app_set_archive_user_id 0

	SELECT @i9 = item_column_id FROM item_columns WHERE process = 'task' AND instance = @instance AND name = 'responsibles'

	SELECT @i9_old = item_column_id FROM item_columns WHERE process = 'task' AND instance = @instance AND name = 'responsible_id'

	SELECT @i18 = item_column_id FROM item_columns WHERE process = 'task' AND instance = @instance AND name = 'task_type'
	SELECT @i19 = item_column_id FROM item_columns WHERE process = 'task' AND instance = @instance AND name = 'parent_item_id'

	IF @i9 IS NULL
	BEGIN 
		EXEC app_ensure_column @instance, @process, 'responsibles', 0, 5, 0, 1, 0, null,  0, 0, 'user', @item_column_id = @i9 OUTPUT
	END
	
	IF @i18 IS NULL
	BEGIN 
		EXEC app_ensure_column @instance, @process, 'task_type', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i18 OUTPUT
	END
	
	IF @i19 IS NULL
	BEGIN 
		EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i19 OUTPUT
	END

	IF @i9_old IS NOT NULL
	BEGIN
		print 'DELETE old resbonsible_id column'
	
		UPDATE conditions SET condition_json = replace(condition_json, '"ItemColumnId":' + CAST(@i9_old AS NVARCHAR) , '"ItemColumnId":' + CAST(@i9 AS NVARCHAR) ) from conditions where condition_json like '%"ItemColumnId":' +  CAST(@i9_old AS NVARCHAR) + '%'
		UPDATE conditions SET condition_json = replace(condition_json, '"UserColumnId":' + CAST(@i9_old AS NVARCHAR) , '"UserColumnId":' + CAST(@i9 AS NVARCHAR) ) from conditions where condition_json like '%"UserColumnId":' +  CAST(@i9_old AS NVARCHAR) + '%'
	
		DELETE FROM item_columns WHERE item_column_id = @i9_old
	END

	BEGIN
		
		EXEC app_ensure_portfolio @instance, @process,'TASK_MY_TASKS', @portfolio_id = @p2 OUTPUT
		
	
		INSERT INTO process_portfolio_columns(process_portfolio_id, item_column_id) (SELECT @p2, item_column_id FROM item_columns WHERE instance = @instance AND process = @process and name <> 'bar_icons')

			SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":3,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@i9 AS NVARCHAR) +' ,"Value":"","UserColumnTypeId":2},"OperatorId":null}]},"OperatorId":1},{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":'+ CAST(@i18 AS NVARCHAR) +  ',"Value":"21"},"OperatorId":null},{"ConditionTypeId":4,"Condition":{"ComparisonTypeId":6,"ParentItemColumnId":' + CAST(@i19 AS NVARCHAR) + ',"ItemColumnId":' + CAST(@i18 AS NVARCHAR) + ',"Value":"21"},"OperatorId":2}]},"OperatorId":1}]'

			EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_MY_TASKS', @consql, @condition_id = @con18 OUTPUT

			INSERT INTO condition_states (condition_id, state) VALUES (@con18, 'write'), (@con18, 'read')
			
			DELETE FROM condition_features WHERE condition_id = @con18 AND feature = 'meta'
			INSERT INTO condition_features (condition_id, feature) VALUES (@con18, 'portfolio')
			INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con18, @p2)
			EXEC app_ensure_configuration @instance, null, null, 'Tasks', 'MyTasksPortfolioId', @p2, 0, null
			
		
	END
   FETCH NEXT FROM loopCursor INTO @instance
END

CLOSE loopCursor
DEALLOCATE loopCursor
