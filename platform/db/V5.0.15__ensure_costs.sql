IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_costs'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_costs] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_costs]  -- Add the parameters for the stored procedure here
    @instance NVARCHAR(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'costs'

    --Archive user is 0
    DECLARE @BinVar VARBINARY(128);
    SET @BinVar = CONVERT(VARBINARY(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_process @instance, @process, @process, 2

    EXEC dbo.app_ensure_column @instance, @process, 'idea_id', 0, 1, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'year', 0, 1, 1, 0, 0, NULL, 0, 0
        EXEC dbo.app_ensure_column @instance, @process, 'month', 0, 1, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'external_workservices_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'external_workservices_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'external_workservices_actual', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'internal_work_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'internal_work_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'internal_work_actual', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'machinesequipments_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'machinesequipments_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'machinesequipments_actual', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'other_internal_costs_budget', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'other_internal_costs_estimated', 0, 2, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance, @process, 'other_internal_costs_actual', 0, 2, 1, 0, 0, NULL, 0, 0

  END
GO

