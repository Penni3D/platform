ALTER TABLE process_portfolios
ADD order_item_column_id INT NULL FOREIGN KEY REFERENCES item_columns(item_column_id)

ALTER TABLE process_portfolios
ADD order_direction  TINYINT NOT NULL DEFAULT 0
