IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_container'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_container] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_container]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @tab_id INT, @grid TINYINT =0, @container_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @container_id = process_container_id FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		INSERT INTO process_containers (instance, process, variable) VALUES (@instance, @process, @variable);
		SELECT @container_id = @@IDENTITY;
		IF (@tab_id > 0)
			BEGIN
				INSERT INTO process_tab_containers (process_tab_id, process_container_id, order_no, container_side) VALUES (@tab_id, @container_id, 1, @grid);
			END
	END
END
