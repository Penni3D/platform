ALTER PROCEDURE [dbo].[app_ensure_recursive_list]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10), 	@process nvarchar(50)
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	--DECLARE @process NVARCHAR(50) = 'user'
	DECLARE @combine TINYINT = 0
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	SET @tableName = '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

			--Create group
			DECLARE @g1 INT
			EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g1 OUTPUT
		
			--Create tabs
			DECLARE @t1 INT
			EXEC app_ensure_tab @instance,@process,'TAB', @g1, @tab_id = @t1 OUTPUT
		
			--Create portfolios
			DECLARE @p1 INT
			EXEC app_ensure_portfolio @instance,@process,'PORTFOLIO', @portfolio_id = @p1 OUTPUT
			
			update process_portfolios set process_portfolio_type = 1 where process_portfolio_id = @p1
			
			--Create conditions
			DECLARE @cId INT	
			EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT

			INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')

			insert into process_portfolio_columns (process_portfolio_id, item_column_id)  select @p1, item_column_id from item_columns where instance = @instance and process = @process and item_column_id NOT IN (select item_column_id from process_portfolio_columns where process_portfolio_id = @p1 )

			exec app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 0, 0, '', 0, 0, null, null
	        
			update item_columns set status_column = 1 where instance = @instance and process = @process and name = 'parent_item_id'


			set @sql = 'update ' +  @tableName + ' set parent_item_id = 0 '
			exec(@sql)
END
GO
