--Archive user is 0
DECLARE @BinVar varbinary(128);
SET @BinVar = CONVERT(varbinary(128), 0);
SET CONTEXT_INFO @BinVar;

DECLARE @process NVARCHAR(50) = 'flex_time_tracking'

-- Apply changes for all instances that have this process
DECLARE @instanceName NVARCHAR(10)
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instanceName
WHILE @@fetch_status = 0 BEGIN
	--Create process
	EXEC app_ensure_process @instanceName, @process, '', 2
		
	UPDATE process_translations SET translation = 'Work time tracking' WHERE instance = @instanceName AND process = @process AND variable = 'PROCESS_' + @process + '_' AND code = 'en-GB'
	INSERT INTO process_translations (instance, process, variable, code, translation) VALUES (@instanceName, @process, 'PROCESS_' + @process + '_', 'fi-FI', N'Ty�ajanseuranta')

	DECLARE @cId INT

	EXEC dbo.app_ensure_container @instanceName, @process, 'AccessContainer', 0, 0, @container_id = @cId OUTPUT

	UPDATE process_translations SET translation = 'Access Container' WHERE instance = @instanceName AND process = @process AND variable = 'CONT_' + @process + '_ACCESSCONTAINER' AND code = 'en-GB'
	INSERT INTO process_translations (instance, process, variable, code, translation) VALUES (@instanceName, @process, 'CONT_' + @process + '_ACCESSCONTAINER', 'fi-FI', 'Access Container')

	UPDATE process_containers SET right_container = 1 WHERE process_container_id = @cId
		
	EXEC dbo.app_ensure_column @instanceName, @process, 'type', @cId, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instanceName, @process, 'user_item_id', @cId, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instanceName, @process, 'start_date', @cId, 13, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instanceName, @process, 'end_date', @cId, 13, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instanceName, @process, 'lunch_break', @cId, 2, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instanceName, @process, 'work_hours', @cId, 2, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instanceName, @process, 'special', @cId, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instanceName, @process, 'comment', @cId, 4, 0, 0, NULL, 0, 0

	EXEC app_ensure_condition @instanceName, @process, 0, 0, 'Rights', 'null', @condition_id = @cId OUTPUT

	INSERT INTO process_translations (instance, process, variable, code, translation) VALUES (@instanceName, @process, 'CONDITION_' + @process + '_RIGHTS', 'fi-FI', 'Oikeudet')

	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'delete')

	DECLARE @code NVARCHAR(5)
	DECLARE lang_cursor CURSOR FOR SELECT code FROM instance_languages WHERE code NOT IN ('en-GB', 'fi-FI')
	OPEN lang_cursor
	FETCH NEXT FROM lang_cursor INTO @code
	WHILE @@fetch_status = 0 BEGIN
		INSERT INTO process_translations (instance, process, variable, code, translation) VALUES (@instanceName, @process, 'PROCESS_' + @process + '_', @code, 'Work time tracking')

		INSERT INTO process_translations (instance, process, variable, code, translation) VALUES (@instanceName, @process, 'CONT_' + @process + '_ACCESSCONTAINER', @code, 'Access Container')
		
		INSERT INTO process_translations (instance, process, variable, code, translation) VALUES (@instanceName, @process, 'CONDITION_' + @process + '_RIGHTS', @code, 'Rights')
	
		FETCH NEXT FROM lang_cursor INTO @code
	END
	CLOSE lang_cursor
	DEALLOCATE lang_cursor

	FETCH NEXT FROM instance_cursor INTO @instanceName
END
CLOSE instance_cursor
DEALLOCATE instance_cursor