﻿UPDATE instance_requests SET path = LEFT(path,255) WHERE LEN(path) > 255
GO
ALTER TABLE instance_requests DROP COLUMN client
GO
DECLARE @sql nvarchar(max)
DECLARE @TwoPartTableNameQuoted nvarchar(500) = '[dbo].[instance_requests]',
    @ColumnNameUnQuoted sysname = 'agent',
    @DynSQL NVARCHAR(MAX);

SELECT @DynSQL =
       'ALTER TABLE ' + @TwoPartTableNameQuoted + ' DROP' +
       ISNULL(' CONSTRAINT ' + QUOTENAME(OBJECT_NAME(c.default_object_id)) + ',','') +
       ISNULL(check_constraints,'') +
       '  COLUMN ' + QUOTENAME(@ColumnNameUnQuoted)
FROM   sys.columns c
           CROSS APPLY (SELECT ' CONSTRAINT ' + QUOTENAME(OBJECT_NAME(referencing_id)) + ','
                        FROM   sys.sql_expression_dependencies
                        WHERE  referenced_id = c.object_id
                          AND referenced_minor_id = c.column_id
                          AND OBJECTPROPERTYEX(referencing_id, 'BaseType') = 'C'
                        FOR XML PATH('')) ck(check_constraints)
WHERE  c.object_id = object_id(@TwoPartTableNameQuoted) AND c.name = @ColumnNameUnQuoted;
EXEC (@DynSQL);
GO
ALTER TABLE instance_requests ALTER COLUMN [path] NVARCHAR(255) NULL
GO
ALTER TABLE instance_requests ADD compressed TINYINT DEFAULT (0)