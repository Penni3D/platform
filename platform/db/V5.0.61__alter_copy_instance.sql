IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_copy_instance'))
   exec('CREATE PROCEDURE [dbo].[app_copy_instance] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER  PROCEDURE [dbo].[app_copy_instance]	-- Add the parameters for the stored procedure here
						@source_instance nvarchar(10),
						@target_instance nvarchar(10),
						@copy_support_data as int = 0,
						@copy_data as int = 0
						AS BEGIN
						exec app_set_archive_user_id 0 

DECLARE @sql NVARCHAR(MAX)
						
			INSERT INTO instances (instance) VALUES (@target_instance);  DECLARE @calendar_events TABLE ( calendar_event_id int, new_calendar_event_id int ) 


 DECLARE @calendars TABLE ( calendar_id int, new_calendar_id int ) 
 DECLARE @calendar_id int, @new_calendar_id int;
disable trigger calendars_INSTEAD_OF_INSERT on calendars;

disable trigger calendars_AFTER_UPDATE on calendars;

 MERGE calendars AS t USING ( SELECT calendar_id ,@target_instance as instance ,  item_id, variable, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default FROM calendars WHERE 1=1 AND  instance = @source_instance  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
instance,   item_id, variable, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default ) VALUES ( 
instance,   item_id, variable, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default )  
 OUTPUT s.calendar_id, inserted.calendar_id INTO @calendars
; enable trigger calendars_INSTEAD_OF_INSERT on calendars ;

 enable trigger calendars_AFTER_UPDATE on calendars ;



 DECLARE @calendar_event_id int, @new_calendar_event_id int;
disable trigger calendar_events_INSTEAD_OF_INSERT on calendar_events;

disable trigger calendar_events_AFTER_UPDATE on calendar_events;

 MERGE calendar_events AS t USING ( SELECT calendar_event_id ,  calendar_id, name, event_start, event_end FROM calendar_events WHERE 1=1 AND  calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @source_instance )  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
 calendar_id, name, event_start, event_end ) VALUES ( 
 calendar_id, name, event_start, event_end )  
 OUTPUT s.calendar_event_id, inserted.calendar_event_id INTO @calendar_events
; enable trigger calendar_events_INSTEAD_OF_INSERT on calendar_events ;

 enable trigger calendar_events_AFTER_UPDATE on calendar_events ;



 DECLARE @calendar_static_holidays TABLE ( calendar_static_holiday_id int, new_calendar_static_holiday_id int ) 
 DECLARE @calendar_static_holiday_id int, @new_calendar_static_holiday_id int;
disable trigger calendar_static_holidays_INSTEAD_OF_INSERT on calendar_static_holidays;

disable trigger calendar_static_holidays_AFTER_UPDATE on calendar_static_holidays;

 MERGE calendar_static_holidays AS t USING ( SELECT calendar_static_holiday_id ,  calendar_id, name, month, day FROM calendar_static_holidays WHERE 1=1 AND  calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @source_instance )  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
 calendar_id, name, month, day ) VALUES ( 
 calendar_id, name, month, day )  
 OUTPUT s.calendar_static_holiday_id, inserted.calendar_static_holiday_id INTO @calendar_static_holidays
; enable trigger calendar_static_holidays_INSTEAD_OF_INSERT on calendar_static_holidays ;

 enable trigger calendar_static_holidays_AFTER_UPDATE on calendar_static_holidays ;




disable trigger instance_languages_INSTEAD_OF_INSERT on instance_languages;

disable trigger instance_languages_AFTER_UPDATE on instance_languages;

 INSERT INTO instance_languages (instance,  code, name, abbr, icon_url ) 
 (SELECT @target_instance as instance ,  code, name, abbr, icon_url FROM instance_languages WHERE  instance = @source_instance   )

; enable trigger instance_languages_INSTEAD_OF_INSERT on instance_languages ;

 enable trigger instance_languages_AFTER_UPDATE on instance_languages ;



 DECLARE @instance_menu TABLE ( instance_menu_id int, new_instance_menu_id int ) 
 DECLARE @instance_menu_id int, @new_instance_menu_id int;
disable trigger instance_menu_INSTEAD_OF_INSERT on instance_menu;

disable trigger instance_menu_AFTER_UPDATE on instance_menu;

 MERGE instance_menu AS t USING ( SELECT instance_menu_id ,@target_instance as instance ,  parent_id, variable, order_no, default_state, params FROM instance_menu WHERE 1=1 AND instance = @source_instance   ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
instance,  parent_id, variable, order_no, default_state, params ) VALUES ( 
instance,  parent_id, variable, order_no, default_state, params )  
 OUTPUT s.instance_menu_id, inserted.instance_menu_id INTO @instance_menu
; enable trigger instance_menu_INSTEAD_OF_INSERT on instance_menu ;

 enable trigger instance_menu_AFTER_UPDATE on instance_menu ;

 
 insert into instance_configurations   ([instance]
      ,[process]
      ,[group_name]
      ,[category]
      ,[name]
      ,[value]
      ,[server]) ( SELECT       @target_instance
      ,[process]
      ,[group_name]
      ,[category]
      ,[name]
      ,[value]
      ,[server] FROM instance_configurations WHERE instance = @source_instance and value is not null)


 INSERT INTO calendar_data_attributes (
calendar_id, name, type_id, event_date) SELECT  (SELECT new_calendar_id FROM @calendars WHERE calendar_id = calendar_data_attributes.calendar_id),  name, type_id, event_date FROM calendar_data_attributes WHERE 1=0  OR calendar_id IN (SELECT calendar_id FROM @calendars) 

 --INSERT INTO instance_hosts (
 --instance, host) ( SELECT  @target_instance, host FROM instance_hosts WHERE instance = @source_instance )


 INSERT INTO instance_translations (instance,code, variable, translation) SELECT @target_instance, code, variable, translation from instance_translations where instance = @source_instance


 INSERT INTO calendar_data (
calendar_event_id,calendar_static_holiday_id,calendar_id, event_date, dayoff, holiday, notwork) SELECT  (SELECT new_calendar_event_id FROM @calendar_events WHERE calendar_event_id = calendar_data.calendar_event_id), (SELECT new_calendar_static_holiday_id FROM @calendar_static_holidays WHERE calendar_static_holiday_id = calendar_data.calendar_static_holiday_id), (SELECT new_calendar_id FROM @calendars WHERE calendar_id = calendar_data.calendar_id),  event_date, dayoff, holiday, notwork FROM calendar_data WHERE 1=0  OR calendar_event_id IN (SELECT calendar_event_id FROM @calendar_events) 
 OR calendar_static_holiday_id IN (SELECT calendar_static_holiday_id FROM @calendar_static_holidays) 
 OR calendar_id IN (SELECT calendar_id FROM @calendars) 

 		DECLARE @GetProcesses CURSOR
		SET @GetProcesses = CURSOR FAST_FORWARD LOCAL FOR
		select process, process_type from processes where instance = @source_instance
		
		IF OBJECT_ID('tempdb..#NewItemColumns') IS NOT NULL DROP TABLE #NewItemColumns
		CREATE table #NewItemColumns (item_column_id int, new_item_column_id int )

		IF OBJECT_ID('tempdb..#NewItems') IS NOT NULL DROP TABLE #NewItems
		CREATE table #NewItems (item_id int, new_item_id int )

		IF OBJECT_ID('tempdb..#NewPortfolios') IS NOT NULL DROP TABLE #NewPortfolios
		CREATE table #NewPortfolios (process_portfolio_id int, new_process_portfolio_id int )

		IF OBJECT_ID('tempdb..#NewTabs') IS NOT NULL DROP TABLE #NewTabs
		CREATE table #NewTabs (process_tab_id int, new_process_tab_id int )

		IF OBJECT_ID('tempdb..#NewConditions') IS NOT NULL DROP TABLE #NewConditions
		CREATE table #NewConditions (condition_id int, new_condition_id int )

		DECLARE @counter as int = 0

		DECLARE @process AS NVARCHAR(MAX) = ''
		DECLARE @processType AS int

		OPEN @GetProcesses
		FETCH NEXT FROM @GetProcesses INTO @process, @processType
		WHILE (@@FETCH_STATUS=0) BEGIN
			print	'copy ' + @process
			IF @process <> 'task' and @process <> 'task_link_type'
			BEGIN

				if @counter = 0 
					begin
						set @sql = ' exec app_copy_process ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @process +char(39) + ', 1'
					end
				else 
					begin
						set @sql = ' exec app_copy_process_temp ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @process +char(39)
					end

				--set @sql = ' exec app_copy_process_2 ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @process +char(39) + ', 1'			
			
				if @processType = 0 AND @copy_data = 1 set @sql = @sql + ',1'
				if @processType = 1 AND @copy_support_data = 1 set @sql = @sql + ',1'
				if @processType = 2 AND @copy_support_data = 1 set @sql = @sql + ',1'

				print	@sql

				exec (@sql)

				set @counter = @counter +1

			END
			ELSE
			BEGIN
			IF @process <> 'task'
			BEGIN 
				exec app_ensure_process_task @target_instance
				END
			END
		FETCH NEXT FROM @GetProcesses INTO @process, @processType
		END

		SET @sql = 'drop procedure app_copy_process_temp'
		exec (@sql)

		set @sql = 'disable trigger _' + @target_instance +'_task_AFTER_UPDATE on _' + @target_instance + '_task';
		exec (@sql)

		if @copy_support_data = 1 and @copy_data = 0
		begin
			
			--insert into item_data_process_selections (item_id, item_column_id, selected_item_id) 
			--(
			--	select 
			--	(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
			--	(select new_item_column_id from #NewItemColumns WHERE item_column_id = idps.item_column_id ),
			--	(select new_item_id from #NewItems WHERE item_id = idps.selected_item_id )

			--	from item_data_process_selections idps
			--	inner join items i1 on idps.item_id = i1.item_id 
			--	inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
			--	inner join items i2 on idps.selected_item_id = i1.item_id 
			--	inner join processes p2 on  i2.instance = p2.instance and i2.process = p2.process
			--	 WHERE idps.item_id in (select item_id from #NewItems) OR selected_item_id in (select item_id from #NewItems) and p1.process_type > 0 and p2.process_type > 0
			--	-- and idps.item_id <> idps.selected_item_id
			--)

			--insert into item_subitems (parent_item_id, item_id, favourite) 
			--(
			--	select 
			--	(select new_item_id from #NewItems WHERE item_id = idps.parent_item_id ),
			--	(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
			--	favourite

			--	from item_subitems idps
			--	inner join items i1 on idps.parent_item_id = i1.item_id 
			--	inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
			--	inner join items i2 on idps.item_id = i1.item_id 
			--	inner join processes p2 on  i2.instance = p2.instance and i2.process = p2.process
			--	 WHERE idps.parent_item_id in (select item_id from #NewItems) OR idps.item_id in (select item_id from #NewItems) and p1.process_type > 0 and p2.process_type > 0
			--)


			INSERT INTO instance_menu_rights (instance_menu_id, item_id) 
			(
				SELECT 
				(select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = im.instance_menu_id),
				(select new_item_id FROM #NewItems WHERE item_id = im.item_id) FROM instance_menu_rights im
				WHERE im.instance_menu_id in (select instance_menu_id from @instance_menu) )
				UPDATE instance_menu set parent_id = (select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = parent_id) WHERE instance = @target_instance
		end
		if @copy_data = 1
		begin
			insert into item_data_process_selections (item_id, item_column_id, selected_item_id) 
			(
				select 
				(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
				(select new_item_column_id from #NewItemColumns WHERE item_column_id = idps.item_column_id ),
				(select new_item_id from #NewItems WHERE item_id = idps.selected_item_id )

				from item_data_process_selections idps
				inner join items i1 on idps.item_id = i1.item_id 
				inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
				 WHERE idps.item_id in (select item_id from #NewItems) OR selected_item_id in (select item_id from #NewItems)
			)
			insert into item_subitems (parent_item_id, item_id, favourite) 
			(
				select 
				(select new_item_id from #NewItems WHERE item_id = idps.parent_item_id ),
				(select new_item_id from #NewItems WHERE item_id = idps.item_id ),
				favourite

				from item_subitems idps
				inner join items i1 on idps.parent_item_id = i1.item_id 
				inner join processes p1 on  i1.instance = p1.instance and i1.process = p1.process
				inner join items i2 on idps.item_id = i2.item_id 
				inner join processes p2 on  i2.instance = p2.instance and i2.process = p2.process
				 WHERE idps.parent_item_id in (select item_id from #NewItems) OR idps.item_id in (select item_id from #NewItems) 
			)
			INSERT INTO instance_menu_rights (instance_menu_id, item_id) 
			(
				SELECT 
				(select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = im.instance_menu_id),
				(select new_item_id FROM #NewItems WHERE item_id = im.item_id) FROM instance_menu_rights im
				WHERE im.instance_menu_id in (select instance_menu_id from @instance_menu) )

			UPDATE instance_menu set parent_id = (select new_instance_menu_id FROM @instance_menu WHERE instance_menu_id = parent_id) WHERE instance = @target_instance

			INSERT INTO allocation_durations (allocation_item_id, date, hours) 
			(select  
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.allocation_item_id ),
				date, hours from allocation_durations i where allocation_item_id in (SELECT item_id FROM #NewItems)
			)

			INSERT INTO allocation_comments (allocation_item_id, comment, created_at, author_item_id) 
			(select  
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.allocation_item_id ),
				comment, created_at, (SELECT new_item_id FROM #NewItems WHERE item_id = i.author_item_id ) 
			from allocation_comments i where allocation_item_id in (SELECT item_id FROM #NewItems)
			)

			INSERT INTO task_durations (task_item_id, resource_item_id, date, hours) 
			(select  
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.task_item_id ),
				(SELECT new_item_id FROM #NewItems WHERE item_id = i.resource_item_id ),
				date, hours from task_durations i where task_item_id in (SELECT item_id FROM #NewItems)
			)
		end


		INSERT INTO process_subprocesses (parent_process, instance, process) ( SELECT  parent_process, @target_instance, process FROM process_subprocesses WHERE instance = @source_instance)


		
 		DECLARE @GetPortfolios CURSOR
		SET @GetPortfolios = CURSOR FAST_FORWARD LOCAL FOR
		select process_portfolio_id, new_process_portfolio_id from #NewPortfolios
		
		DECLARE @process_portfolio_id int, @new_process_portfolio_id int

		OPEN @GetPortfolios
		FETCH NEXT FROM @GetPortfolios INTO @process_portfolio_id, @new_process_portfolio_id
		WHILE (@@FETCH_STATUS=0) BEGIN
			UPDATE instance_menu set params = replace(params, 'portfolioId":' + CAST(@process_portfolio_id as nvarchar) + '}', 'portfolioId":' + CAST(@new_process_portfolio_id as nvarchar) + '}') where instance = @target_instance			
			
			UPDATE instance_configurations SET value = COALESCE(REPLACE(value, 'portfolioId":[' + CAST(@process_portfolio_id as nvarchar) + ']', 'portfolioId":[' + CAST(@new_process_portfolio_id as nvarchar) + ']'), '')
			WHERE instance = @target_instance AND category = 'Main application' and name = 'Frontpage layout'

			FETCH NEXT FROM @GetPortfolios INTO @process_portfolio_id, @new_process_portfolio_id
		END

		UPDATE item_columns 
		SET 
		data_additional2 = (select new_item_column_id from #NewItemColumns WHERE item_column_id = CAST(data_additional2 as int) )
		WHERE instance = @target_instance and data_type = 11

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_item_column_id, '') FROM #NewItemColumns WHERE item_column_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'FavouriteColumnId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'User portfolio ID'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'BacklogPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'SprintsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'GanttPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'KanbanPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'EmailsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'NotificationsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'BulletinsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'SendedEmailsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'UnreadNotificationsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_portfolio_id, '') FROM #NewPortfolios WHERE process_portfolio_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Main application' and name = 'AllNotificationsPortfolioId'

		UPDATE instance_configurations SET value = COALESCE((SELECT COALESCE(new_process_tab_id, '') FROM #NewTabs WHERE process_tab_id =  CAST(value as int) ), '')
		WHERE instance = @target_instance AND category = 'Evaluation' and name = 'EvaluationIdeaTabId'

 		DECLARE @ItemCols CURSOR
		SET @ItemCols = CURSOR FAST_FORWARD LOCAL FOR
		SELECT item_column_id, new_item_column_id FROM #NewItemColumns

		DECLARE @item_column_id int, @new_item_column_id int

		OPEN @ItemCols
		FETCH NEXT FROM @ItemCols INTO @item_column_id, @new_item_column_id
		WHILE (@@FETCH_STATUS=0) BEGIN
			UPDATE conditions set condition_json = replace(condition_json, '"ItemColumnId":' + CAST(@item_column_id AS NVARCHAR) + ',', '"ItemColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + ',') WHERE instance = @target_instance 
			UPDATE conditions set condition_json = replace(condition_json, '"ItemColumnId":' + CAST(@item_column_id AS NVARCHAR) + '}', '"ItemColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + '}') WHERE instance = @target_instance
			UPDATE conditions set condition_json = replace(condition_json, '"UserColumnId":' + CAST(@item_column_id AS NVARCHAR) + ',', '"UserColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + ',') WHERE instance = @target_instance 
			UPDATE conditions set condition_json = replace(condition_json, '"UserColumnId":' + CAST(@item_column_id AS NVARCHAR) + '}', '"UserColumnId":' + CAST(@new_item_column_id AS NVARCHAR) + '}') WHERE instance = @target_instance
		FETCH NEXT FROM  @ItemCols INTO @item_column_id, @new_item_column_id
		END	


 		DECLARE @Items CURSOR
		SET @Items = CURSOR FAST_FORWARD LOCAL FOR
		SELECT item_id, new_item_id FROM #NewItems

		DECLARE @item_id int, @new_item_id int

		OPEN @Items
		FETCH NEXT FROM @Items INTO @item_id, @new_item_id
		WHILE (@@FETCH_STATUS=0) BEGIN
			UPDATE conditions set condition_json = replace(condition_json, '"Value":' + CAST(@item_id AS NVARCHAR) + ',', '"Value":' + CAST(@new_item_id AS NVARCHAR) + ',') WHERE instance = @target_instance 
			UPDATE conditions set condition_json = replace(condition_json, '"Value":' + CAST(@item_id AS NVARCHAR) + '}', '"Value":' + CAST(@new_item_id AS NVARCHAR) + '}') WHERE instance = @target_instance
		FETCH NEXT FROM  @Items INTO @item_id, @new_item_id
		END	
		

		
		--set @sql = 'UPDATE _' + @target_instance + '_task  set parent_item_id = (select new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_task.parent_item_id)  WHERE parent_item_id IS NOT NULL AND item_id IN ( SELECT item_id FROM items WHERE instance = ''' + @target_instance + ''' AND process = ''task'') ' 
		--exec (@sql)




		insert into task_order (order_set, owner_id, task_id, order_no)
		 ( select 
		 order_set, owner_id,
		 (select new_item_id from #NewItems where item_id = task_id), 
		 order_no
		from task_order
		where 
		task_id in (select item_id from #NewItems )
		)


	 
 		IF OBJECT_ID('tempdb..#task_status') IS NOT NULL DROP TABLE #task_status
		CREATE table #task_status (status_id int, new_status_id int )

		 DECLARE @status_id int, @new_status_id int;

		disable trigger task_status_INSTEAD_OF_INSERT on task_status;
		disable trigger task_status_AFTER_UPDATE on task_status;

		 MERGE task_status AS t USING (
		  select status_id,
		 (select new_item_id from #NewItems where item_id = parent_item_id) as parent_item_id, 
		 title, description, is_done, order_no
		from task_status
		where 
		parent_item_id in (select item_id from #NewItems )
		  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
		 parent_item_id, title, description, is_done, order_no ) VALUES ( 
		 parent_item_id, title, description, is_done, order_no  )  
		 OUTPUT s.status_id, inserted.status_id INTO #task_status;
		 
		 enable trigger task_status_INSTEAD_OF_INSERT on task_status ;
		 enable trigger task_status_AFTER_UPDATE on task_status ;

		 set @sql = ' UPDATE _' + @target_instance + '_task  SET status_id = (SELECT new_status_id FROM #task_status WHERE status_id = _' + @target_instance + '_task.status_id)';
		 exec (@sql)

IF (select count(process) from processes where instance = @target_instance AND process = 'evalution') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_evaluation SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_evaluation.parent_item_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_evaluation SET creator_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_evaluation.creator_item_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_evaluation SET evaluator_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_evaluation.evaluator_item_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'idea_matrix') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_idea_matrix SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_idea_matrix.parent_item_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'notification_tag_links') > 0
BEGIN

		 set @sql = ' UPDATE _' + @target_instance + '_notification_tag_links SET json_tag_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_notification_tag_links.json_tag_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_notification_tag_links SET item_column_id = (SELECT new_item_column_id FROM #NewItemColumns WHERE item_column_id = _' + @target_instance + '_notification_tag_links.item_column_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'request_more_info') > 0
BEGIN

		 set @sql = ' UPDATE _' + @target_instance + '_request_more_info SET parent_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_request_more_info.parent_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_request_more_info SET request_user_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_request_more_info.request_user_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_request_more_info SET request_submitter = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_request_more_info.request_submitter) ';
		 exec (@sql)

END

IF (select count(process) from processes where instance = @target_instance AND process = 'synpulse_rf_finance') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_finance SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_finance.parent_item_id) ';
		 exec (@sql)
END

IF (select count(process) from processes where instance = @target_instance AND process = 'synpulse_rf_milestone') > 0
BEGIN
		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_milestone SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_milestone.parent_item_id) ';
		 exec (@sql)
		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_milestone SET list_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_milestone.list_item_id) ';
		 exec (@sql)

		 set @sql = ' UPDATE _' + @target_instance + '_synpulse_rf_milestones SET parent_item_id = (SELECT new_item_id FROM #NewItems WHERE item_id = _' + @target_instance + '_synpulse_rf_milestones.parent_item_id) ';
		 exec (@sql)
END

		 INSERT INTO condition_user_groups (condition_id, item_id) (select 
		 (select new_condition_id from #NewConditions WHERE condition_id = condition_user_groups.condition_id ),
		 (select new_item_id from #NewItems WHERE item_id = condition_user_groups.item_id )
		  from condition_user_groups where condition_id in (select condition_id from #NewConditions) 
		  and (select new_item_id from #NewItems WHERE item_id = condition_user_groups.item_id ) is not null
		  )

		set @sql = 'enable trigger _' + @target_instance +'_task_AFTER_UPDATE on _' + @target_instance + '_task';
		exec (@sql)
END
