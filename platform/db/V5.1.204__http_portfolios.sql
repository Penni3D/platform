﻿DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
DECLARE @process NVARCHAR(50) = 'notification'

OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
   	DECLARE @p1 INT
	  EXEC app_ensure_portfolio @instance,@process,'HTTP_REQUESTS', @portfolio_id = @p1 OUTPUT
	  
	  insert into process_portfolio_columns(process_portfolio_id, item_column_id) select @p1, item_column_id from item_columns where instance = @instance and process = @process
	  
	  	DECLARE @i1 INT
	  	select @i1 = item_column_id from item_columns  where instance = @instance and process = @process and name = 'type'
	  	
	  DECLARE @c1 AS NVARCHAR(MAX)
    DECLARE @con1 AS INT
    
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR(max)) + ',"ComparisonTypeId":3,"Value":"3"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'HTTP_REQUESTS', @c1, @condition_id = @con1 OUTPUT
	INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'read')
  DELETE from condition_features WHERE feature = 'meta' and condition_id = @con1
   
   declare @params nvarchar (MAX)
   declare @menuId INT 
   
  select @menuId = instance_menu_id, @params = params from instance_menu  where instance = @instance  and default_state = 'notification'

  IF @menuId > 0 
  BEGIN
      update instance_menu set params = substring( @params, 1, LEN( @params) -1) + ',"httpRequestsPortfolioId":' + CAST(@p1 AS nvarchar(max)) + '}' WHERE instance_menu_id = @menuId
  END
   
   
    FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor
