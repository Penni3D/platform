SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processPortfolio_DownloadOptionsIdReplace]
(
	@downloadOptions NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(MAX) 
	DECLARE @type INT
	DECLARE @strTable TABLE (str_to_found NVARCHAR(MAX), strtype  INT)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @reverseStartPos INT
	DECLARE @len INT

	IF @downloadOptions IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('":{', 0), --ItemColumnId
				('PortfolioId', 1)
		
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @downloadOptions)
					WHILE @startPos > 0
						BEGIN  
							IF @type = 0
								BEGIN
									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @reverseStartPos = LEN(@downloadOptions) - @startPos + 2

									SET @endPos1 = CHARINDEX('"', @downloadOptions, @reverseStartPos)
									SET @endPos2 = CHARINDEX('{', @downloadOptions, @reverseStartPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos1 - @reverseStartPos + 4)
											SET @len = @endPos1 - @reverseStartPos
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos2 - @reverseStartPos + 4)
											SET @len = @endPos2 - @reverseStartPos
										END

									SET @var3 = SUBSTRING(@var2, 4, LEN(@var2) - 4)
				
									IF ISNUMERIC(@var3) = 1 
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SET @var4 = dbo.itemColumn_GetVariable(REVERSE(@var3))
													IF @var4 IS NOT NULL
														BEGIN
															SET @downloadOptions = STUFF(@downloadOptions, @reverseStartPos, @len, REVERSE(@var4))
														END
												END
										END

									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @startPos = LEN(@downloadOptions) - @reverseStartPos + 2
								END

							IF @type = 1
								BEGIN
									SET @endPos1 = CHARINDEX(',', @downloadOptions, @startPos)
									SET @endPos2 = CHARINDEX('}', @downloadOptions, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos1 - @startPos)
											SET @len = @endPos1 - @startPos - LEN(@strToFound) - 2
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos2 - @startPos)
											SET @len = @endPos2 - @startPos - LEN(@strToFound) - 2
										END

									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)

									SET @startPos = @startPos + LEN(@strToFound) + 2

									IF ISNUMERIC(@var3) = 1 
										BEGIN
											IF CAST(@var3 AS INT) > 0
												BEGIN
													SET @var4 = dbo.processPortfolio_GetVariable(@var3)		
													IF @var4 IS NOT NULL
														BEGIN
															SET @downloadOptions = STUFF(@downloadOptions, @startPos, @len, @var4)
														END
												END
										END
								END
							SET @startPos = CHARINDEX(@strToFound, @downloadOptions, @startPos + 1)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @downloadOptions
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processPortfolio_DownloadOptionsVarReplace]
(
	@instance NVARCHAR(MAX),
	@downloadOptions NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT
	DECLARE @strToFound nvarchar(max) 
	DECLARE @type int
	DECLARE @strTable TABLE (str_to_found NVARCHAR(max), strtype  int)
	DECLARE @getStrs CURSOR
	DECLARE @startPos INT
	DECLARE @reverseStartPos INT
	DECLARE @len INT

	IF @downloadOptions IS NOT NULL
		BEGIN
			INSERT INTO @strTable (str_to_found, strtype) 
			VALUES
				('":{', 0), --ItemColumnId 
				('PortfolioId', 1)

			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET @startPos = CHARINDEX(@strToFound, @downloadOptions)
					WHILE @startPos > 0
						BEGIN  
							IF @type = 0
								BEGIN  
									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @reverseStartPos = LEN(@downloadOptions) - @startPos + 2

									SET @endPos1 = CHARINDEX('"', @downloadOptions, @reverseStartPos)
									SET @endPos2 = CHARINDEX('{', @downloadOptions, @reverseStartPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos1 - @reverseStartPos + 4)
											SET @len = @endPos1 - @reverseStartPos
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @reverseStartPos - 3, @endPos2 - @reverseStartPos + 4)
											SET @len = @endPos2 - @reverseStartPos
										END

									SET @var3 = SUBSTRING(@var2, 4, LEN(@var2) - 4)
				
									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.itemColumn_GetId(@instance, REVERSE(@var3))	
											IF @var4 IS NOT NULL
												BEGIN
													SET @downloadOptions = STUFF(@downloadOptions, @reverseStartPos, @len, REVERSE(@var4))
												END
										END

									SET @downloadOptions = REVERSE(@downloadOptions)
									SET @startPos = LEN(@downloadOptions) - @reverseStartPos + 2
								END

							IF @type = 1
								BEGIN  
									SET @endPos1 = CHARINDEX(',', @downloadOptions, @startPos)
									SET @endPos2 = CHARINDEX('}', @downloadOptions, @startPos)

									IF @endPos2 > @endPos1 AND @endPos1 > 0
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos1 - @startPos)
											SET @len = @endPos1 - @startPos - LEN(@strToFound) - 2
										END
									ELSE
										BEGIN
											SET @var2 = SUBSTRING(@downloadOptions, @startPos, @endPos2 - @startPos)
											SET @len = @endPos2 - @startPos - LEN(@strToFound) - 2
										END
				
									SET @var3 = SUBSTRING(@var2, LEN(@strToFound) + 3, LEN(@var2) - LEN(@strToFound) - 2)

									SET @startPos = @startPos + LEN(@strToFound) + 2

									IF LEN(@var3) > 0
										BEGIN
											SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		
											IF @var4 IS NOT NULL
												BEGIN
													SET @downloadOptions = STUFF(@downloadOptions, @startPos, @len, @var4)
												END
										END
								END

							SET @startPos = CHARINDEX(@strToFound, @downloadOptions, @startPos + 1)
						END
					FETCH NEXT FROM @getStrs INTO @strToFound, @type
				END
		END
	IF (SELECT CURSOR_STATUS('global', '@getStrs')) >= -1
		BEGIN
			DEALLOCATE @getStrs
		END
	RETURN @downloadOptions
END