﻿DECLARE @sql nvarchar(max)
DECLARE @instance nvarchar(10)
DECLARE instanceCursor CURSOR FOR SELECT instance FROM instances
OPEN instanceCursor

FETCH NEXT FROM instanceCursor INTO @instance

WHILE @@fetch_status = 0
  BEGIN
      SET @sql = ' CREATE NONCLUSTERED INDEX _' + @instance + '_archive_task_idx ON [dbo].[archive__' + @instance + '_task] ([item_id])'
      exec (@sql)
      FETCH  NEXT FROM instanceCursor INTO @instance
  END
CLOSE instanceCursor
DEALLOCATE instanceCursor
