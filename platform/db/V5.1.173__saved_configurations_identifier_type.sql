ALTER TABLE instance_user_configurations_saved
ALTER COLUMN identifier NVARCHAR(255) NULL

UPDATE instance_user_configurations_saved
SET identifier = 'sticky' WHERE [group] = 'navigation'