SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[condition_VarReplaceProc]
(
 @sourceInstance NVARCHAR(10),
 @targetInstance NVARCHAR(10),
 @condition_json NVARCHAR(MAX),
 @replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
			BEGIN

				SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		
				if @var4 is not null 
				begin
							SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)
				end
	
	
				IF dbo.IsListColumn(dbo.itemColumn_GetId(@sourceInstance, @var3))  > 0
					BEGIN					
						DECLARE @idCursor CURSOR 
						DECLARE @listItemId INT 
						DECLARE @value nvarchar(max)
						DECLARE @id int
						DECLARE @process nvarchar(50)
	
						SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
						SELECT item_id FROM dbo.get_ListColumnIds(dbo.itemColumn_GetId(@sourceInstance, @var3));
	
						select @process = data_additional FROM item_columns WHERE instance = @sourceInstance and item_column_id = dbo.itemColumn_GetId(@sourceInstance, @var3)
	
						OPEN @idCursor
						FETCH NEXT FROM @idCursor INTO @listItemId
						WHILE (@@FETCH_STATUS=0) BEGIN
	
---							EXEC list_GetValue @listItemId, @value = @value output
--							EXEC list_GetId @targetInstance, @process, @value, @id = @id output
							select @id = dbo.item_GetId(@value)
							if @id is not null 
							begin
								set @condition_json = replace(@condition_json, '"Value":"' + @value + '"}', '"Value":"' + CAST(@id as nvarchar) + '"}')
							end 

							FETCH NEXT FROM @idCursor INTO @listItemId
						END
					END
				END

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
					END

		  CONTINUE  
	END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
			BEGIN


				SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)
	
				SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)
	
				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
					END
			END

		  CONTINUE  
	END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)
			
			IF ISNUMERIC(@var3) = 0 AND @var3 <> 'null'
			BEGIN

			SET @var4 = dbo.condition_GetId(@targetInstance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'ConditionId":' + @var4)

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2+LEN(@var4))
				END
			END

		  CONTINUE  
	END
 SET @replaced_json = @condition_json
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processActionDialog_GetVariable]
(
 @processActionDialogId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 SELECT TOP 1 @var = variable FROM process_actions_dialog WHERE process_action_dialog_id = @processActionDialogId
 RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[processActionDialog_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_action_dialog_id FROM process_actions_dialog WHERE instance = @instance and variable = @variable
 RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	ALTER FUNCTION [dbo].[itemColumns_Da2VarReplace]
(
 @instance NVARCHAR(MAX),
 @data_type INT,
 @dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json
	declare @strToFound nvarchar(max) 
	declare @type int

	declare @strTable TABLE (str_to_found nvarchar(max), strtype  int)

	IF @data_type = 16 AND @dataAdditional2 IS NOT NULL
		BEGIN
			insert into @strTable (str_to_found, strtype) 
			VALUES
				 ('@SourceProcessId', 0), 
				('@SourceColumn', 0), 
				('@OptionalList1Id', 0), 
				('@OptionalList1Val', 1),
				('@OptionalList2Id', 0), 
				('@OptionalList2Val', 1),
				('@OptionalOrderColumn', 0)
		
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS=0) BEGIN
					DECLARE @startPos INT = charindex(@strToFound, @dataAdditional2)
					WHILE @startPos > 0
					BEGIN  
		
						IF @type = 0 
							BEGIN
								SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
								SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
							END
						IF @type = 1 
							BEGIN
								set @startPos = @startPos + 1
								SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
								SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
							END
				
							IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
								END
				
							SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
				IF @type = 0 
						BEGIN
							IF LEN(@var3)  > 0
							BEGIN
								SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		
								IF @var4 IS NOT NULL
									BEGIN
										SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
								END

								IF @endPos2 > @endPos1
									BEGIN
										SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
									END
								ELSE
									BEGIN
										SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
									END
							END
						END
				IF @type = 1
						BEGIN
							IF LEN(@var3) > 0 AND @var3 <> ']'
							BEGIN
								DECLARE @listVal NVARCHAR(max)
								DECLARE @listItemId int
								SET @var3 = replace(@var3, '[', '')
								SET @var3 = replace(@var3, ']', '')
								SET @var4 = @var3

								DECLARE @delimited CURSOR
								SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
								SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
								OPEN @delimited
								FETCH NEXT FROM @delimited INTO @listVal
								
								WHILE (@@FETCH_STATUS=0) BEGIN
									IF @listVal = 'NULL' BEGIN
										SET @listItemId = 0
									END
									ELSE BEGIN
										SELECT @listItemId = item_id FROM items WHERE guid = @listVal
										IF @listItemId IS NULL
										BEGIN
											SET @listItemId = 0
										END
									END
									SET @var4 = replace(@var4,  @listVal ,  CAST(@listItemId as nvarchar(max) ) )

									FETCH NEXT FROM @delimited INTO @listVal
								END
								CLOSE @delimited
								SET @dataAdditional2 = replace(@dataAdditional2, @var3, @var4)
							END
						END
						  CONTINUE  
					END
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		  END
		  CLOSE @getStrs
	END
 RETURN @dataAdditional2
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[itemColumns_Da2IdReplace]
(
 @data_type INT,
 @dataAdditional2 NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	declare @strToFound nvarchar(max) 
	declare @type int

	declare @strTable TABLE (str_to_found nvarchar(max), strtype  int)

	IF @data_type = 16 AND @dataAdditional2 IS NOT NULL
		BEGIN
			insert into @strTable (str_to_found, strtype) 
			VALUES
				 ('@SourceProcessId', 0), 
				('@SourceColumn', 0), 
				('@OptionalList1Id', 0), 
				('@OptionalList1Val', 1),
				('@OptionalList2Id', 0), 
				('@OptionalList2Val', 1),
				('@OptionalOrderColumn', 0)
		
			DECLARE @getStrs CURSOR
			SET @getStrs = CURSOR FAST_FORWARD LOCAL FOR 
			SELECT str_to_found, strtype FROM @strTable 
			
			OPEN @getStrs
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		
		  	WHILE (@@FETCH_STATUS=0) BEGIN
					DECLARE @startPos INT = charindex(@strToFound, @dataAdditional2)
					WHILE @startPos > 0
					BEGIN  
		
						IF @type = 0 
							BEGIN
								SET @endPos1 = charindex(',', @dataAdditional2, @startPos)
								SET @endPos2 = charindex('}', @dataAdditional2, @startPos)
							END
						IF @type = 1 
							BEGIN
								set @startPos = @startPos + 1
								SET @endPos1 = charindex(']', @dataAdditional2, @startPos)
								SET @endPos2 = charindex(',', @dataAdditional2, @startPos) 
							END
				
							IF (@type = 0 AND @endPos2 > @endPos1 AND @endPos1 > 0) OR (@type = 1 AND @endPos2 < @endPos1 AND @endPos1 > 0)
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos1-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1)
								END
							ELSE
								BEGIN
									SET @var2 = SUBSTRING(@dataAdditional2, @startPos, @endPos2-@startPos)
									SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2)
								END
				
							SET @var3 = SUBSTRING(@var2, LEN(@strToFound)+3, LEN(@var2)-LEN(@strToFound) -2 )
				
				IF @type = 0 
						BEGIN
							IF ISNUMERIC(@var3) = 1 
								BEGIN
								IF CAST(@var3 as int ) > 0
								BEGIN
									SET @var4 = dbo.itemColumn_GetVariable(@var3)		
									SET @dataAdditional2 = replace(@dataAdditional2, @var2, @strToFound +'":' + @var4)
					
									IF @endPos2 > @endPos1
										BEGIN
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos1+LEN(@var4))
										END
									ELSE
										BEGIN
											SET @startPos = charindex(@strToFound, @dataAdditional2, @endPos2+LEN(@var4))
										END
								END
							END
						END
				IF @type = 1
						BEGIN
							IF LEN(@var3) > 0 AND @var3 <> ']'
							BEGIN
									DECLARE @listVal NVARCHAR(max)
									DECLARE @guid uniqueidentifier
						
									SET @var3 = replace(@var3, '[', '')
									SET @var3 = replace(@var3, ']', '')
									SET @var4 = @var3

									DECLARE @delimited CURSOR
									SET @delimited = CURSOR FAST_FORWARD LOCAL FOR 
									SELECT * FROM dbo.DelimitedStringToTable (@var3, ',') 
									
					
									OPEN @delimited
									FETCH NEXT FROM @delimited INTO @listVal
								
								  	WHILE (@@FETCH_STATUS=0) BEGIN

											SELECT @guid = guid FROM items WHERE item_id = @listVal
											DECLARE @gu_id NVARCHAR(max)
											set @gu_id = @guid
											IF @gu_id IS NULL
											BEGIN
												SET @gu_id = 'NULL' 
											END
											SET @var4 = replace(@var4,  @listVal , CAST(@gu_id as nvarchar(max) ) )

										FETCH NEXT FROM @delimited INTO @listVal

									  END
									  CLOSE @delimited
									SET @dataAdditional2 = replace(@dataAdditional2, @var3, @var4)
							END
						END
						  CONTINUE  
					END
			FETCH NEXT FROM @getStrs INTO @strToFound, @type
		  END
		  CLOSE @getStrs
	END
 RETURN @dataAdditional2
END