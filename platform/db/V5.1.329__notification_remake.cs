using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.db
{
	class ServerMigrationNotificationRemake : CsMigration
	{
		public override bool Execute(IConnections db) {
		//	try {
				string sql =
					"SELECT LEFT(translation, CHARINDEX ('delta', translation) - 3) + '}' as t, fulltranslation, variable, code, process_translation_id FROM ( " +
					"SELECT LEFT (translation, CHARINDEX ('ops', translation)) AS translation, translation AS fulltranslation, variable, code, process_translation_id from process_translations where variable LIKE ('variable_body_variable%') AND translation LIKE '%delta%' " +
					"	) iq";

				DataTable oldConfigurationsDt = db.GetDatatable(sql);
				foreach (DataRow confRow in oldConfigurationsDt.Rows) {

					var langvar = Conv.ToStr(confRow["variable"]);
					dynamic stuff = JsonConvert.DeserializeObject(Conv.ToStr(confRow["fulltranslation"]));

					var endPartJson = Conv.ToStr(stuff.delta.ops);

					var firstPartJson = Conv.ToStr(stuff.html);

					var code = Conv.ToStr(confRow["code"]);

					var newContent = Conv.ToStr(confRow["fulltranslation"]);

					var pattern = @"\[(.*?)\]";

					var replacingTags = Regex.Matches(endPartJson, pattern);
					var oldIds = Regex.Matches(firstPartJson, pattern);
					var theseReplace = new List<string>();
					var toBeReplaced = new List<string>();

					foreach (Match m8 in oldIds) {
						toBeReplaced.Add(Conv.ToStr(m8.Groups[1]));
					}
					foreach (Match m in replacingTags) {
						theseReplace.Add(Conv.ToStr(m.Groups[1]));
					}

					int count = 0;
					try {

						foreach (string element in toBeReplaced) {
							if (theseReplace[count] != null) {
								newContent = newContent.Replace(element, theseReplace[count]);
							}
							count++;
						}

						newContent = newContent.Replace("'", "''");
						string sqlUpdateAndSet = "UPDATE process_translations SET translation = " + "'" + newContent +
						                         "'" +
						                         "WHERE variable = " + "'" + langvar + "'" + "AND code = '" + code +
						                         "'";
						db.ExecuteUpdateQuery(sqlUpdateAndSet, null);

					} catch (Exception e) {
						Diag.SupressException(e);
					}
				}

			return true;
		}
	}
}