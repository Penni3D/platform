ALTER PROCEDURE [dbo].[app_ensure_copy_tab]
	@instance NVARCHAR(10),	@process NVARCHAR(50), @from_tab_id INT, @to_group_id INT, @new_tab_id INT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;
	
	DECLARE @i int = 0
	
	DECLARE  @process_container_id INT
	
	DECLARE container CURSOR FOR 
	SELECT process_container_id FROM process_tab_containers WHERE process_tab_id = @from_tab_id

	OPEN container
	FETCH NEXT FROM container into @process_container_id
	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @process_container_id
		
		
		INSERT INTO process_containers (instance, process, rows, cols) VALUES (@instance, @process, 1, 1)
		DECLARE @new_container_id INT = @@identity
		
		SELECT @new_container_id

		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no)
		SELECT @new_tab_id, @new_container_id, container_side, order_no FROM process_tab_containers WHERE process_tab_id = @from_tab_id AND process_container_id = @process_container_id ORDER BY order_no COLLATE Latin1_General_bin ASC
		
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, order_no)
		SELECT @new_container_id, process_container_columns.item_column_id, process_container_columns.placeholder, process_container_columns.validation, process_container_columns.order_no FROM process_container_columns LEFT JOIN process_tab_containers ON process_tab_containers.process_container_id = process_container_columns.process_container_id WHERE process_tab_containers.process_tab_id = @from_tab_id AND process_tab_containers.process_container_id = @process_container_id 
	
		FETCH NEXT FROM container into @process_container_id
	END
	
	CLOSE container
	DEALLOCATE container   
END