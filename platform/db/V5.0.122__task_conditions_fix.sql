﻿exec app_set_archive_user_id 0

update c
 set c.condition_json = (select condition_json from conditions where variable = 'COMPLETE_TAB_READ' AND instance = c.instance)
from conditions c
where condition_json is null and variable = 'COMPLETE_TAB_WRITE'