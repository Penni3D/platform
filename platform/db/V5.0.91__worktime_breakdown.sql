IF NOT exists(select * from sysobjects WHERE name = 'worktime_tracking_hours_breakdown' AND xType= 'U')
  BEGIN 
    CREATE TABLE [dbo].[worktime_tracking_hours_breakdown](
      [ıd] [int] NOT NULL IDENTITY (1,1),
      [worktime_tracking_hour_id] [int] NOT NULL,
      [hours] [float] NOT NULL,
      [type] [int] NOT NULL,
      [list_item_id] [int] NULL,
      CONSTRAINT [PK_worktime_tracking_hours_breakdown] UNIQUE CLUSTERED ([worktime_tracking_hour_id], [type]),
      CONSTRAINT [FK_worktime_tracking_hours_item] FOREIGN KEY ([worktime_tracking_hour_id]) REFERENCES [dbo].[worktime_tracking_hours] (id)      
    )
  END

GO
ALTER TABLE [dbo].[worktime_tracking_hours]
    ADD additional_hours FLOAT
GO
ALTER TABLE [dbo].[worktime_tracking_hours]
  ALTER COLUMN hours INT NULL
GO

CREATE PROCEDURE [dbo].[app_ensure_process_worktime_tracking_breakdown_types]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'worktime_tracking_breakdown_types'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_list @instance, @process, @process, 1

    UPDATE processes SET list_order = 'list_item' WHERE process = @process

    EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0

    INSERT INTO items (instance, process) VALUES (@instance, @process);
    DECLARE @itemId INT = @@identity;
    SET @sql = 'UPDATE ' + @tableName + ' SET list_item = ''Default'' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
    EXEC (@sql)
  END

GO

-- Apply changes for all instances that have this process
DECLARE @instanceName NVARCHAR(10)
DECLARE @process NVARCHAR(255) = 'worktime_tracking_breakdown_types'
DECLARE instance_cursor CURSOR FOR
  SELECT instance FROM instances

OPEN instance_cursor

FETCH NEXT FROM 
instance_cursor INTO
  @instanceName

WHILE @@fetch_status = 0
  BEGIN
    EXEC app_ensure_process_worktime_tracking_breakdown_types @instanceName

    FETCH  NEXT FROM instance_cursor INTO
      @instanceName
  END
CLOSE instance_cursor
DEALLOCATE instance_cursor