UPDATE instance_user_configurations SET identifier = 'resources' WHERE identifier = '1' AND [group] = 'allocation'
UPDATE instance_user_configurations SET identifier = 'projects' WHERE identifier = '0' AND [group] = 'allocation'

UPDATE instance_user_configurations_saved SET identifier = 'resources' WHERE identifier = '1' AND [group] = 'allocation'
UPDATE instance_user_configurations_saved SET identifier = 'projects' WHERE identifier = '0' AND [group] = 'allocation'