EXEC app_ensure_archive 'process_group_tabs'
ALTER TABLE process_group_tabs
 ADD tag NVARCHAR(255)

EXEC app_ensure_archive 'process_tabs'
ALTER TABLE process_tabs
 ADD maintenance_name NVARCHAR(255)

EXEC app_ensure_archive 'process_groups'
ALTER TABLE process_groups
ADD maintenance_name NVARCHAR(255)

EXEC app_ensure_archive 'process_containers'
ALTER TABLE process_containers
ADD maintenance_name NVARCHAR(255)