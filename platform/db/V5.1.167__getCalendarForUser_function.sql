CREATE FUNCTION GetCalendarForUser(@userId int)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
		work_hours,
		timesheet_hours,
		def_1,
		def_2,
		def_3,
		def_4,
		def_5,
		def_6,
		def_7
	FROM (
		SELECT 
			(SELECT CASE WHEN c.work_hours = -1 THEN pc.work_hours ELSE c.work_hours END) AS work_hours,
			(SELECT CASE WHEN c.timesheet_hours = -1 THEN pc.timesheet_hours ELSE c.timesheet_hours END) AS timesheet_hours,
			(SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_1 ELSE c.def_1 END) AS def_1,
			(SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_2 ELSE c.def_2 END) AS def_2,
			(SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_3 ELSE c.def_3 END) AS def_3,
			(SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_4 ELSE c.def_4 END) AS def_4,
			(SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_5 ELSE c.def_5 END) AS def_5,
			(SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_6 ELSE c.def_6 END) AS def_6,
			(SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_7 ELSE c.def_7 END) AS def_7
		FROM calendars c 
		LEFT JOIN calendars pc ON pc.calendar_id = c.parent_calendar_id WHERE c.item_id = @userId
	) outer_query 
)