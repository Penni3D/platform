ALTER TRIGGER [dbo].[items_DropTable] ON [dbo].[item_tables] AFTER DELETE AS
DECLARE @GetDeleted CURSOR
DECLARE @TableName NVARCHAR(255)

SET @GetDeleted = CURSOR FAST_FORWARD LOCAL FOR 
SELECT name FROM deleted

OPEN @GetDeleted
FETCH NEXT FROM @GetDeleted INTO @TableName

WHILE (@@FETCH_STATUS=0) BEGIN
	DECLARE @DropSql NVARCHAR(MAX) = 'DROP TABLE ' + @TableName + ';'
	EXEC sp_executesql @DropSql

  	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[archive_'+@TableName+']')) BEGIN
		SET @DropSql  = 'DROP TABLE archive_' + @TableName + ';'
		EXEC sp_executesql @DropSql

		SET @DropSql = 'DROP FUNCTION get_' + @TableName + ';'
		EXEC sp_executesql @DropSql
	END

	FETCH NEXT FROM @GetDeleted INTO @TableName
END

CLOSE @GetDeleted
DEALLOCATE @GetDeleted