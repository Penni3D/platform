SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROCEDURE [dbo].[app_ensure_process]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @process_type INT,
	@colour nvarchar(50) = null, @listProcess int = 0, @listOrder nvarchar(50) = null, @newRowActionId int = null
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT COUNT(*) FROM processes WHERE instance = @instance AND process = @process) = 0)
	BEGIN
		INSERT INTO processes (instance, process, variable, process_type, colour, list_process, list_order, new_row_action_id) VALUES (@instance, @process, @variable, @process_type, @colour, @listProcess, @listOrder, @newRowActionId);
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND process = @process AND variable = @variable) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, @variable, @variable, 'en-GB')
		END

	END
END
GO






SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_customer'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_customer] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_customer]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10), 	@process nvarchar(50), @processType as int, @colour nvarchar(50), @listProcess int,  @listOrder nvarchar(50), @newRowActionId int
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	--DECLARE @process NVARCHAR(50) = 'user'
	DECLARE @combine TINYINT = 0
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	SET @tableName = '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF NOT EXISTS (SELECT * FROM processes WHERE instance = @instance and process = @process)
	BEGIN
	
		--Create process
		EXEC app_ensure_process @instance, @process, @process, @processType, @colour, @listProcess, @listOrder, @newRowActionId
	
		IF @processType = 0 
		BEGIN
	
			--Create group
			DECLARE @g1 INT
			EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g1 OUTPUT
		
			--Create tabs
			DECLARE @t1 INT
			EXEC app_ensure_tab @instance,@process,'TAB', @g1, @tab_id = @t1 OUTPUT
		
			--Create containers
			DECLARE @c1 INT
			EXEC app_ensure_container @instance,@process,'CONTAINER', @t1, 1, @container_id = @c1 OUTPUT
		
			--Create portfolios
			DECLARE @p1 INT
			EXEC app_ensure_portfolio @instance,@process,'PORTFOLIO', @portfolio_id = @p1 OUTPUT
			
			--Create conditions
			DECLARE @cId INT	
			EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT

			INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
			INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')

			--Create columns
			EXEC app_ensure_column @instance, @process, 'title', @c1, 0, 1, 1, 0, null, @p1, 0, 0

			--Create menu
			DECLARE @instanceMenuId INT, 
			@instanceMenuId2 INT

			INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, null, @process, '0', null, '{}')
			SET @instanceMenuId = @@IDENTITY

			INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'NEW', '0', 'process.new', '{"process":"' + @process + '"}')
			SET @instanceMenuId2 = @@IDENTITY
			SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
			EXEC (@sql)
		
			INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'PORTFOLIO', '0', 'portfolio', '{"process":"' + @process + '","portfolioId":' + cast(@p1 as nvarchar(max)) + '}')
			SET @instanceMenuId2 = @@IDENTITY
			SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
			EXEC (@sql)
		END

		END
		
	END
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_container'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_container] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROCEDURE [dbo].[app_ensure_container]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @tab_id INT, @grid TINYINT =0, @container_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @container_id = process_container_id FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		INSERT INTO process_containers (instance, process, variable) VALUES (@instance, @process, @variable);
		SELECT @container_id = @@IDENTITY;
		
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, order_no, container_side) VALUES (@tab_id, @container_id, 1, @grid);
	END
END
GO
