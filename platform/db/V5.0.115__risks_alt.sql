SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_risks]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	exec app_set_archive_user_id 0
	declare @process nvarchar(50) = 'risks'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	----Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--create condition
	EXEC app_ensure_condition @instance, @process, 0, 0

	--Create group
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'RISKS_PHASE', @group_id = @g1 OUTPUT
		
	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'RISKS_TAB', @g1, @tab_id = @t1 OUTPUT
		
	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'RISKS_CONTAINER', @t1, 1, @container_id = @c1 OUTPUT
		
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'RISKS_PORTFOLIO', @portfolio_id = @p1 OUTPUT

	--Create conditions
	DECLARE @cId INT
	EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT
	INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')

	--Create owner item id for row manager
	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 1, 1, 1, 0, null, 0, 0, 0
	EXEC app_ensure_column @instance, @process, 'task_type', 0, 1, 1, 1, 0, null, 0, 0, 0

	--Create list_risks_probability -list
	DECLARE @processListProbability NVARCHAR(50) = 'list_risks_probability'
	DECLARE @tableNameProbability NVARCHAR(50) = ''
	SET @tableNameProbability = '_' + @instance + '_' + @processListProbability
	DECLARE @sqlProbability NVARCHAR(MAX) = ''

	--Archive user is 0
	DECLARE @BinVar1 varbinary(128);
	SET @BinVar1 = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar1;

	--Create process
	DECLARE @ListProbabilityId INT
	EXEC app_ensure_list @instance, @processListProbability, @processListProbability, 0

	UPDATE processes SET list_order = 'value' WHERE process = @processListProbability

	EXEC dbo.app_ensure_column @instance,@processListProbability, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProbability, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProbability, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListProbability, 'value', 0, 0, 1, 0, 0, NULL, 0, 0


	INSERT INTO items (instance, process) VALUES (@instance, @processListProbability);
	DECLARE @itemIdProbability INT = @@identity;
	SET @sqlProbability = 'UPDATE ' + @tableNameProbability + ' SET list_item = ''Minor'', value = 1 WHERE item_id = ' +  CAST(@itemIdProbability AS nvarchar)
	EXEC (@sqlProbability)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProbability);
	SET @itemIdProbability = @@identity;
	SET @sqlProbability = 'UPDATE ' + @tableNameProbability + ' SET list_item = ''Small'', value = 2 WHERE item_id = ' +  CAST(@itemIdProbability AS nvarchar)
	EXEC (@sqlProbability)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProbability);
	SET @itemIdProbability = @@identity;
	SET @sqlProbability = 'UPDATE ' + @tableNameProbability + ' SET list_item = ''Medium'', value = 3 WHERE item_id = ' +  CAST(@itemIdProbability AS nvarchar)
	EXEC (@sqlProbability)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProbability);
	SET @itemIdProbability = @@identity;
	SET @sqlProbability = 'UPDATE ' + @tableNameProbability + ' SET list_item = ''Substantial'', value = 4 WHERE item_id = ' +  CAST(@itemIdProbability AS nvarchar)
	EXEC (@sqlProbability)
	INSERT INTO items (instance, process) VALUES (@instance, @processListProbability);
	SET @itemIdProbability = @@identity;
	SET @sqlProbability = 'UPDATE ' + @tableNameProbability + ' SET list_item = ''Large'', value = 5 WHERE item_id = ' +  CAST(@itemIdProbability AS nvarchar)
	EXEC (@sqlProbability)


	--Create list_risks_seriousness -list
	DECLARE @processListseriousness NVARCHAR(50) = 'list_risks_seriousness'
	DECLARE @tableNameSeriousness NVARCHAR(50) = ''
	SET @tableNameSeriousness = '_' + @instance + '_' + @processListSeriousness
	DECLARE @sqlSeriousness NVARCHAR(MAX) = ''

	--Archive user is 0
	DECLARE @BinVar2 varbinary(128);
	SET @BinVar2 = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar2;

	--Create process
	DECLARE @ListSeriousnessId INT
	EXEC app_ensure_list @instance, @processListSeriousness, @processListSeriousness, 0

	UPDATE processes SET list_order = 'value' WHERE process = @processListSeriousness

	EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@processListSeriousness, 'value', 0, 0, 1, 0, 0, NULL, 0, 0


	INSERT INTO items (instance, process) VALUES (@instance, @processListSeriousness);
	DECLARE @itemIdSeriousness INT = @@identity;
	SET @sqlSeriousness = 'UPDATE ' + @tableNameSeriousness + ' SET list_item = ''Minor'', value = 1 WHERE item_id = ' +  CAST(@itemIdSeriousness AS nvarchar)
	EXEC (@sqlSeriousness)
	INSERT INTO items (instance, process) VALUES (@instance, @processListSeriousness);
	SET @itemIdSeriousness = @@identity;
	SET @sqlSeriousness = 'UPDATE ' + @tableNameSeriousness + ' SET list_item = ''Small'', value = 2 WHERE item_id = ' +  CAST(@itemIdSeriousness AS nvarchar)
	EXEC (@sqlSeriousness)
	INSERT INTO items (instance, process) VALUES (@instance, @processListSeriousness);
	SET @itemIdSeriousness = @@identity;
	SET @sqlSeriousness = 'UPDATE ' + @tableNameSeriousness + ' SET list_item = ''Medium'', value = 3 WHERE item_id = ' +  CAST(@itemIdSeriousness AS nvarchar)
	EXEC (@sqlSeriousness)
	INSERT INTO items (instance, process) VALUES (@instance, @processListSeriousness);
	SET @itemIdSeriousness = @@identity;
	SET @sqlSeriousness = 'UPDATE ' + @tableNameSeriousness + ' SET list_item = ''Substantial'', value = 4 WHERE item_id = ' +  CAST(@itemIdSeriousness AS nvarchar)
	EXEC (@sqlSeriousness)
	INSERT INTO items (instance, process) VALUES (@instance, @processListSeriousness);
	SET @itemIdSeriousness = @@identity;
	SET @sqlSeriousness = 'UPDATE ' + @tableNameSeriousness + ' SET list_item = ''Large'', value = 5 WHERE item_id = ' +  CAST(@itemIdSeriousness AS nvarchar)
	EXEC (@sqlSeriousness)

	--Create Portfolio columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'title', @c1, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into porfolio columns
	EXEC app_ensure_column @instance, @process, 'probability', @c1, 6, 1, 1, 0, null,  0, 0, @processListProbability , null, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into portfolio columns
	EXEC app_ensure_column @instance, @process, 'seriousness', @c1, 6, 1, 1, 0, null,  0, 0, @processListSeriousness , null, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into portfolio columns
	EXEC app_ensure_column @instance, @process, 'risk_description', @c1, 4, 1, 0, 0, null, 0, 0, 0 --Put description long text into the container
	DECLARE @formula NVARCHAR(MAX) = 'getvalue("probability","value") * getvalue("seriousness","value")'
	EXEC app_ensure_column @instance, @process, 'risk_value', @c1, 14, 1, 1, 0, null,  0, 0, @formula, null, 0, @item_column_id = @i1 OUTPUT --Is also a portfolio column
	INSERT INTO process_portfolio_columns (process_portfolio_id,item_column_id,use_in_select_result) VALUES (@p1,@i1,1) --Put into portfolio columns

	EXEC app_ensure_column @instance, @process, 'probabilityalt', @c1, 6, 1, 1, 0, null,  0, 0, @processListProbability , null, 0
	EXEC app_ensure_column @instance, @process, 'seriousnessalt', @c1, 6, 1, 1, 0, null,  0, 0, @processListSeriousness , null, 0

	--change feature columns to not to be system columns so they can be configured
	UPDATE item_columns SET system_column = 0 WHERE process = @process AND instance = @instance
	--change feature columns to be status columns so every information is loaded to portfolio regardless of visible columns
	UPDATE item_columns set status_column = 1 where instance = @instance and process = @process
END;
GO