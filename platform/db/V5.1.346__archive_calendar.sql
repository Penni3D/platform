CREATE FUNCTION GetCalendarForUserArchive(@userId int, @archiveDate DateTime)
    RETURNS TABLE
        AS
        RETURN
            (
                SELECT
                    work_hours,
                    timesheet_hours,
                    weekly_hours,
                    def_1,
                    def_2,
                    def_3,
                    def_4,
                    def_5,
                    def_6,
                    def_7
                FROM (
                         SELECT
                             (SELECT CASE WHEN c.work_hours = -1 THEN pc.work_hours ELSE c.work_hours END) AS work_hours,
                             (SELECT CASE WHEN c.timesheet_hours = -1 THEN pc.timesheet_hours ELSE c.timesheet_hours END) AS timesheet_hours,
                             (SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_1 ELSE c.def_1 END) AS def_1,
                             (SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_2 ELSE c.def_2 END) AS def_2,
                             (SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_3 ELSE c.def_3 END) AS def_3,
                             (SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_4 ELSE c.def_4 END) AS def_4,
                             (SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_5 ELSE c.def_5 END) AS def_5,
                             (SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_6 ELSE c.def_6 END) AS def_6,
                             (SELECT CASE WHEN c.use_base_calendar = 1 THEN pc.def_7 ELSE c.def_7 END) AS def_7,
                             (SELECT CASE
                                         WHEN c.weekly_hours = -2 THEN (
                                             SELECT CASE
                                                        WHEN c.use_base_calendar = 1
                                                            THEN (7 - pc.def_1 - pc.def_2 - pc.def_3 - pc.def_4 - pc.def_5 - pc.def_6 - pc.def_7) * (SELECT CASE WHEN c.timesheet_hours = -1 THEN pc.timesheet_hours ELSE c.timesheet_hours END)
                                                        ELSE
                                                                (7 - c.def_1 - c.def_2 - c.def_3 - c.def_4 - c.def_5 - c.def_6 - c.def_7) * (SELECT CASE WHEN c.timesheet_hours = -1 THEN pc.timesheet_hours ELSE c.timesheet_hours END)
                                                        END)
                                         WHEN c.weekly_hours = -1 THEN (
                                             SELECT CASE
                                                        WHEN pc.weekly_hours = -2
                                                            THEN (7 - pc.def_1 - pc.def_2 - pc.def_3 - pc.def_4 - pc.def_5 - pc.def_6 - pc.def_7) * pc.timesheet_hours
                                                        ELSE
                                                            pc.weekly_hours
                                                        END)
                                         ELSE c.weekly_hours END) AS weekly_hours
                         FROM dbo.get_calendars(@archiveDate) c
                                  LEFT JOIN dbo.get_calendars(@archiveDate) pc ON pc.calendar_id = c.parent_calendar_id WHERE c.item_id = @userId
                     ) outer_query
            )
GO
ALTER FUNCTION  [dbo].[get_calendars]( @d datetime )
    RETURNS @rtab TABLE (archive_id INT, [calendar_id] int NOT NULL ,[instance] nvarchar(10) NOT NULL ,[item_id] int NULL ,[variable] nvarchar(255) NOT NULL DEFAULT (''),[work_hours] decimal(18,2) NOT NULL DEFAULT ((-1)),[def_1] tinyint NOT NULL DEFAULT ((0)),[def_2] tinyint NOT NULL DEFAULT ((0)),[def_3] tinyint NOT NULL DEFAULT ((0)),[def_4] tinyint NOT NULL DEFAULT ((0)),[def_5] tinyint NOT NULL DEFAULT ((0)),[def_6] tinyint NOT NULL DEFAULT ((0)),[def_7] tinyint NOT NULL DEFAULT ((0)),[is_default] tinyint NOT NULL DEFAULT ((0)),[base_calendar] tinyint NULL DEFAULT ((0)),[calendar_name] nvarchar(50) NULL ,[description] nvarchar(255) NULL ,[timesheet_hours] decimal(18,2) NOT NULL DEFAULT ((-1)),[parent_calendar_id] int NULL ,[ensure_date] date NULL ,[use_base_calendar] tinyint NULL DEFAULT ((1)),[weekly_hours] decimal(18,2) NULL DEFAULT ((-2)),[archive_userid] int NOT NULL DEFAULT ((0)),[archive_start] datetime NULL DEFAULT (getutcdate()))
    AS
    BEGIN
        INSERT @rtab
        SELECT archive_id, [calendar_id],[instance],[item_id],[variable],[work_hours],[def_1],[def_2],[def_3],[def_4],[def_5],[def_6],[def_7],[is_default],[base_calendar],[calendar_name],[description],[timesheet_hours],[parent_calendar_id],[ensure_date],[use_base_calendar],[weekly_hours],[archive_userid],[archive_start]
        FROM archive_calendars WHERE archive_id IN (
            SELECT MAX(archive_id)
            FROM archive_calendars
            WHERE archive_start <= @d AND archive_end > @d AND [calendar_id] NOT IN
                                                               (SELECT [calendar_id] FROM  calendars  WHERE archive_start <= @d  )
            GROUP BY [calendar_id]
        )
        UNION
        SELECT 0, [calendar_id],[instance],[item_id],[variable],[work_hours],[def_1],[def_2],[def_3],[def_4],[def_5],[def_6],[def_7],[is_default],[base_calendar],[calendar_name],[description],[timesheet_hours],[parent_calendar_id],[ensure_date],[use_base_calendar],[weekly_hours],[archive_userid],[archive_start]
        FROM calendars
        WHERE archive_start <= @d
        RETURN
    END
GO
ALTER TABLE archive_calendars ALTER COLUMN work_hours decimal(18, 2) NOT NULL
ALTER TABLE archive_calendars ALTER COLUMN timesheet_hours decimal(18, 2) NOT NULL
ALTER TABLE archive_calendars ALTER COLUMN weekly_hours decimal(18, 2) NOT NULL 
