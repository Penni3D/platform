﻿EXEC app_set_archive_user_id 0
UPDATE process_translations
SET translation = 'List item'
WHERE variable = 'LIST_ITEM'

EXEC app_set_archive_user_id 0
UPDATE process_translations
SET translation = 'List symbol'
WHERE variable = 'LIST_SYMBOL'

EXEC app_set_archive_user_id 0
UPDATE process_translations
SET translation = 'List symbol fill'
WHERE variable = 'LIST_SYMBOL_FILL'
