﻿DECLARE @MatrixProcessName NVARCHAR(255);
DECLARE @Instance NVARCHAR(255); 
SET @Instance = (SELECT TOP 1 instance FROM instances)
DECLARE @DefaultLanguage NVARCHAR(255);
SET @DefaultLanguage = (SELECT TOP 1 default_locale_id FROM instances)
DECLARE @ItemId INT;
DECLARE @ItemColumnId INT;
DECLARE @OldName NVARCHAR(255);

DECLARE @Sql NVARCHAR(255);

EXEC app_set_archive_user_id 1
DECLARE cursor_process CURSOR
FOR SELECT process FROM processes WHERE process_type = 4

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @MatrixProcessName

	PRINT @MatrixProcessName
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
		SET @ItemColumnId = (SELECT item_column_id FROM item_columns WHERE process = @MatrixProcessName AND name = 'name')
		UPDATE item_columns SET data_type = 24 WHERE process = @MatrixProcessName AND name = 'name'


			
		DECLARE cursor_matrix CURSOR
			FOR SELECT item_id FROM items WHERE process = @MatrixProcessName
		OPEN cursor_matrix
			FETCH NEXT FROM cursor_matrix INTO @ItemId
				WHILE @@FETCH_STATUS = 0
			BEGIN
			
			SET @Sql = 'SELECT @GetName = name FROM _' + @Instance + '_' + @MatrixProcessName + ' WHERE item_id = @SetItemId'
			EXECUTE SP_EXECUTESQL @Sql, N'@SetItemId INT,@GetName NVARCHAR(255) OUTPUT',@SetItemId = @ItemId,@GetName = @OldName OUTPUT
			
			DECLARE @Variable NVARCHAR(MAX)
			SET @Variable = 'Variable_' + @MatrixProcessName + '_' + CAST(@ItemId as nvarchar(255)) + '_name'


			INSERT INTO process_translations (instance, process, code, variable, translation) 
			VALUES(@Instance, @MatrixProcessName, @DefaultLanguage, @Variable, @OldName) 
		
			SET @Sql = 'UPDATE _' + @Instance + '_' + @MatrixProcessName + ' SET name = '''+ @Variable + ''' WHERE item_id = ' + CAST(@ItemId as nvarchar(255))
			EXECUTE SP_EXECUTESQL @Sql

			FETCH NEXT FROM cursor_matrix INTO 
				@ItemId
			END;
	
		CLOSE cursor_matrix;
		DEALLOCATE cursor_matrix;

        FETCH NEXT FROM cursor_process INTO 
            @MatrixProcessName
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;


