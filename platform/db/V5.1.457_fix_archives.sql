CREATE PROCEDURE [dbo].[app_ensure_archive_dynamic]
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @sql NVARCHAR(MAX);
    DECLARE @TableName NVARCHAR(MAX);
    DECLARE @GetTables CURSOR

    --Get Tables
    SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
        SELECT name FROM item_tables

    OPEN @GetTables
    FETCH NEXT FROM @GetTables INTO @TableName

    WHILE (@@FETCH_STATUS=0) BEGIN
        IF @TableName not in (
                              'attachments_data',
                              'database_migrations',
                              'instance_configurations',
                              'items',
                              'instance_user_configurations',
                              'instance_user_configurations_saved',
                              'links'
            ) BEGIN
            print 'ensure archive for ' + @TableName
            SET @sql = 'EXEC app_ensure_archive '''+@TableName+''''
            print @sql
            EXEC (@sql)
        END
        FETCH NEXT FROM @GetTables INTO @TableName
    END
    CLOSE @GetTables
END