﻿ALTER PROCEDURE [dbo].[app_ensure_process_list_budget_category]	-- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
  BEGIN
    DECLARE @process NVARCHAR(50) = 'list_budget_category'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;
    DECLARE @tableName NVARCHAR(50) = ''
    SET @tableName = '_' + @instance + '_' + @process
    DECLARE @sql NVARCHAR(MAX) = ''


    --Create process
    EXEC app_ensure_list @instance, @process, @process, 0

    EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'income', 0, 0, 1, 0, 0, NULL, 0, 0
  END

GO

-- EXECUTE PROCEDURE FOR ALL INSTANCES
BEGIN
  DECLARE @instanceName NVARCHAR(10)
  DECLARE Budget_category_Cursor CURSOR FOR SELECT instance FROM processes WHERE process='list_budget_category'
  
  OPEN Budget_category_Cursor

  FETCH NEXT FROM Budget_category_Cursor INTO @instanceName

  WHILE @@fetch_status = 0
    BEGIN    
      EXEC app_ensure_process_list_budget_category @instanceName
      
      FETCH  NEXT FROM Budget_category_Cursor INTO @instanceName
    END
  CLOSE Budget_category_Cursor
  DEALLOCATE Budget_category_Cursor
END
