DECLARE @sql NVARCHAR(MAX);
DECLARE @instance NVARCHAR(MAX);
DECLARE @GetInstances CURSOR
DECLARE @i1 INT;

--Get Tables
SET @GetInstances = CURSOR FAST_FORWARD LOCAL FOR
    select instance
    from instances

OPEN @GetInstances
FETCH NEXT FROM @GetInstances INTO @instance

WHILE (@@FETCH_STATUS = 0) BEGIN
    DECLARE @process NVARCHAR(50) = 'action_history'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create Table Process
    EXEC app_ensure_process @instance, @process, @process, 2
    EXEC app_ensure_create_right_container @instance, @process
    EXEC app_ensure_table_process @instance, @process

    --Create Access Container
    DECLARE @ContainerId INT
    SET @ContainerId = (SELECT TOP 1 process_container_id
                        FROM process_containers
                        WHERE process = @Process
                          AND instance = @instance
                        ORDER BY process_container_id DESC)

    --Create Columns
    EXEC app_ensure_column @instance, @process, 'action_id', @ContainerId, 1, 0, 1, 0, null, 0, 0
    EXEC app_ensure_column @instance, @process, 'action_json', @ContainerId, 4, 0, 0, 0, NULL, 0, 0
    
    FETCH NEXT FROM @GetInstances INTO @instance
END
CLOSE @GetInstances
GO
alter table process_actions add use_action_history INT NOT NULL DEFAULT (0)
GO
alter table process_actions add action_history_item_column_id INT NOT NULL DEFAULT (0)
GO
alter table process_actions add action_history_text NVARCHAR(MAX)