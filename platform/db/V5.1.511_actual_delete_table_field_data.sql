SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
    AS
    SET XACT_ABORT OFF

DECLARE @delete_timestamp DATETIME = getUTCdate()
DECLARE @ids NVARCHAR(MAX) = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM deleted FOR XML PATH(''))
DECLARE @sql NVARCHAR(MAX)
DECLARE @tName NVARCHAR(MAX)
DECLARE @cName NVARCHAR(MAX)
DECLARE @lvl AS INT = 0

IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
CREATE TABLE #DeleteItems (item_id INT, ord INT)

DECLARE @GetTables CURSOR
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
    SELECT ccu.TABLE_NAME, ccu.COLUMN_NAME 
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
    INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
    WHERE UNIQUE_CONSTRAINT_NAME = 'PK_items' AND SUBSTRING(ccu.TABLE_NAME, 1, 1) = '_'

DECLARE @idps_childs NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 2 FOR XML PATH(''))
DECLARE @idps_parents NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 3 FOR XML PATH(''))

DECLARE @start_pos INT
DECLARE @end_pos INT
DECLARE @data_additional NVARCHAR(MAX)
DECLARE @data_additional2 NVARCHAR(MAX)
DECLARE @process NVARCHAR(50)
DECLARE @table_columns_cursor CURSOR
DECLARE @table_item_column_id NVARCHAR(255)
DECLARE @table_instance NVARCHAR(10)
DECLARE @table_process NVARCHAR(50)
DECLARE @table_column_name NVARCHAR(50)
DECLARE @table_data_type INT
CREATE TABLE #table_columns_table (process NVARCHAR(50), data_additional2 NVARCHAR(MAX))
CREATE TABLE #table_item_column_ids (item_column_id INT)

WHILE @ids IS NOT NULL BEGIN
	IF OBJECT_ID('tempdb..#LoopItems') IS NOT NULL DROP TABLE #LoopItems
	CREATE TABLE #LoopItems (item_id INT, ord INT)

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @sql = ' WITH rec AS ( 
			 SELECT t.item_id, t.' + @cName + ', ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			 FROM ' + @tName + ' t 
			 WHERE t.' + @cName + ' IN (-1' + @ids + ')
			 ) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
		EXECUTE (@sql)
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

	SET @sql = ' WITH rec AS ( 
			SELECT t.selected_item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_childs + ') AND item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT selected_item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @sql = ' WITH rec AS ( 
			SELECT t.item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_parents + ') AND selected_item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @sql = 'INSERT INTO #table_columns_table (process, data_additional2) SELECT process, data_additional2 FROM item_columns WHERE instance + ''_'' + process IN (SELECT instance + ''_'' + process FROM items WHERE item_id IN (-1' + @ids + ')) AND data_type in (18, 21) AND CHARINDEX(''"parentFieldName"'', data_additional2) = 0'
	EXEC(@sql)
	SET @table_columns_cursor = CURSOR FAST_FORWARD LOCAL FOR SELECT process, data_additional2 FROM #table_columns_table
	OPEN @table_columns_cursor
	FETCH NEXT FROM @table_columns_cursor INTO @process, @data_additional2
	WHILE @@fetch_status = 0 BEGIN
		SET @start_pos = CHARINDEX('"itemColumnId"', @data_additional2)
		IF @start_pos = 0 BEGIN
			SET @start_pos = CHARINDEX('"ownerColumnId"', @data_additional2) + 1
		END
		IF @start_pos > 1 BEGIN
			SET @start_pos = @start_pos + 15
			SET @end_pos = CHARINDEX(',', @data_additional2, @start_pos)
			IF @end_pos = 0 BEGIN
				SET @end_pos = CHARINDEX('}', @data_additional2, @start_pos)
			END
			SET @table_item_column_id = SUBSTRING(@data_additional2, @start_pos, @end_pos - @start_pos)
			IF (SELECT COUNT (*) FROM #table_item_column_ids WHERE item_column_id = @table_item_column_id) = 0 BEGIN
				INSERT INTO #table_item_column_ids (item_column_id) VALUES (@table_item_column_id)
				IF ISNUMERIC(@table_item_column_id) = 1 BEGIN
					SELECT @table_instance = instance, @table_process = process, @table_column_name = [name], @table_data_type = data_type, @data_additional = data_additional FROM item_columns WHERE item_column_id = @table_item_column_id
					IF @table_data_type = 1 BEGIN
						SET @sql = 'INSERT INTO #LoopItems (item_id, ord) SELECT item_id, @lvl FROM _' + @table_instance + '_' + @table_process + ' WHERE ' + @table_column_name + ' IN (-1' + @ids + ')'
						EXEC sp_executesql @sql, N'@lvl INT', @lvl
					END
					ELSE IF @table_data_type IN (5, 6) AND @data_additional = @process BEGIN
						SET @sql = 'INSERT INTO #LoopItems (item_id, ord) SELECT item_id, @lvl FROM item_data_process_selections WHERE item_id IN (-1' + @ids + ') AND item_column_id = @table_item_column_id'
						EXEC sp_executesql @sql, N'@table_item_column_id INT, @lvl INT', @table_item_column_id, @lvl
					END
				END
			END
			FETCH NEXT FROM @table_columns_cursor INTO @process, @data_additional2
		END
	END
	DEALLOCATE @table_columns_cursor

	SET @ids = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems) FOR XML PATH(''))

	INSERT INTO #DeleteItems (item_id, ord) SELECT item_id, ord FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems)

	SET @lvl = @lvl + 1
END

DELETE  FROM item_subitems 				  WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_tabs_subprocess_data WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM condition_user_groups WHERE  item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_baseline WHERE  process_item_id IN (SELECT item_id FROM #DeleteItems)

DECLARE @delId AS INT
DECLARE @GetIds CURSOR

SET @GetIds = CURSOR FOR SELECT item_id FROM #DeleteItems GROUP BY item_id, ord ORDER BY ord DESC

OPEN @GetIds
FETCH NEXT FROM @GetIds INTO @delId

WHILE @@FETCH_STATUS = 0 BEGIN
	INSERT INTO items_deleted (item_id, instance, process, deleted_date, [guid]) SELECT item_id, instance, process, @delete_timestamp, [guid] FROM items WHERE item_id = @delId
    DELETE FROM items WHERE item_id = @delId
    FETCH NEXT FROM @GetIds INTO @delId
END
CLOSE @GetIds

DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
UPDATE dd SET dd.link_item_id = NULL FROM item_data_process_selections dd INNER JOIN deleted d ON dd.link_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id

DELETE dd FROM instance_user_configurations_saved dd INNER JOIN deleted d ON dd.user_id = d.item_id
DELETE dd FROM instance_user_configurations dd INNER JOIN deleted d ON dd.user_id = d.item_id

DELETE dd FROM condition_user_groups dd INNER JOIN deleted d ON dd.item_id = d.item_id

DELETE dd FROM process_baseline dd INNER JOIN deleted d ON dd.process_item_id = d.item_id

--Insert into deleted items
INSERT INTO items_deleted (item_id, instance, process, deleted_date, [guid]) SELECT item_id, instance, process, @delete_timestamp, [guid] FROM deleted

--Finally delete the item
DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id