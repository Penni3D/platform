ALTER TABLE calendars 
ADD timesheet_hours DECIMAL(18,2) NOT NULL DEFAULT ((-1))
GO
UPDATE calendars SET timesheet_hours = work_hours