
ALTER TABLE instance_integration_keys  
DROP CONSTRAINT PK__instance__5DEF6B6197A38FED;   
GO
ALTER TABLE instance_integration_keys ALTER COLUMN user_item_id INTEGER NULL
GO
ALTER TABLE instance_integration_keys ADD instance_integration_key_id INT IDENTITY(1,1)


ALTER TABLE instance_integration_tokens DROP
   CONSTRAINT FK_Keys;

ALTER TABLE instance_integration_tokens ADD
   CONSTRAINT FK_Keys
      FOREIGN KEY ([key])
      REFERENCES instance_integration_keys ([key])
      ON DELETE CASCADE
      ON UPDATE CASCADE;


ALTER TABLE instance_integration_tokens ALTER COLUMN token nvarchar(255) NULL