EXEC app_set_archive_user_id 0
DELETE FROM instance_menu WHERE default_state = 'notification' and link_type = 3

GO

UPDATE item_columns SET data_type = 24 WHERE use_translation = 1

GO

UPDATE item_columns SET data_type = 24 WHERE name IN ('name_variable', 'title_variable', 'body_variable')

GO

ALTER TABLE item_columns
DROP CONSTRAINT ic_use_translation_df

GO

ALTER TABLE item_columns
DROP COLUMN use_translation

GO

EXEC app_ensure_archive 'item_columns'
