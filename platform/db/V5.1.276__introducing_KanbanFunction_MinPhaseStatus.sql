EXEC app_set_archive_user_id 0;

SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @GetTables CURSOR

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT instance FROM instances
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

  WHILE (@@FETCH_STATUS=0) BEGIN
    SET @sql = 
'
CREATE FUNCTION f_' + @TableName + '_KanbanFunction_MinPhaseStatus
(
	 @phase_item_id INT
	,@status_date DATETIME
	,@phase_task_status_column_id INT
	,@sprint_item_default_status_id INT
) RETURNS INT
AS
BEGIN

	DECLARE @function_return_value INT;
	--DECLARE @status_date DATETIME = GETUTCDATE();
	--DECLARE @phase_item_id INT = 2391; 
	--DECLARE @phase_task_status_column_id INT = (SELECT ic.item_column_id icid FROM item_columns ic WHERE ic.instance = ''keto'' AND ic.process = ''task'' AND ic.data_additional = ''list_task_status''); --225;
	--DECLARE @sprint_item_default_status_id INT = (SELECT TOP 1 item_id FROM _keto_list_task_status ORDER BY order_no ASC); --160;

	WITH phase_descendants AS
	  (
		SELECT tsk.item_id this_id, 0 parent, tsk.task_type, 1 level, NULL this_status
		  FROM get__' + @TableName + '_task(@status_date) tsk WHERE tsk.item_id = @phase_item_id

	  UNION ALL
		SELECT cld.item_id, cld.parent_item_id, cld.task_type, d.level + 1,

		-- 1 = vaiheen alainen teht�v�, teht�v�ll� on oma statuksensa, mutta // markus: ei saa olla null vaan pienin
		CASE WHEN cld.task_type = 1 AND (SELECT phase.item_id FROM get__' + @TableName + '_task(@status_date) phase WHERE phase.item_id = cld.parent_item_id AND phase.task_type = 2) > 0 THEN
			ISNULL(
				(SELECT idps.selected_item_id FROM get_item_data_process_selections(@status_date) idps WHERE idps.item_id = cld.item_id AND idps.item_column_id = @phase_task_status_column_id)
			,@sprint_item_default_status_id)

		-- 21 = sprintti, tila tutkitaan alla olevista korteista (funktiolla) tai defaultataan
		ELSE CASE WHEN cld.task_type = 21 THEN
			ISNULL(
				(SELECT dbo.f_' + @TableName + '_KanbanFunction_MinSprintStatusByCards(cld.item_id, @status_date))
			,@sprint_item_default_status_id)

		-- 1 = sprintin alainen teht�v�, tila tutkitaan alla olevista korteista (funktiolla) tai defaultataan
		ELSE CASE WHEN cld.task_type = 1 AND (SELECT sprint.item_id FROM get__' + @TableName + '_task(@status_date) sprint WHERE sprint.item_id = cld.parent_item_id AND sprint.task_type = 21) > 0 THEN
			ISNULL(
				(SELECT dbo.f_' + @TableName + '_KanbanFunction_MinTaskStatusByCards(cld.item_id, @status_date))
			,@sprint_item_default_status_id)
			
		-- 0 = sprintin alaisen teht�v�n kortti
		ELSE CASE WHEN cld.task_type = 0 THEN
			ISNULL(
				(SELECT status_list_item_id FROM get_task_status(@status_date) WHERE status_id = cld.status_id)
			,@sprint_item_default_status_id)
			
		ELSE
			NULL

		END END END END

		  FROM phase_descendants as d JOIN get__' + @TableName + '_task(@status_date) cld ON d.this_id = cld.parent_item_id
	  )


	--SELECT * FROM phase_descendants WHERE pd.this_id <> @phase_item_id;
	--SELECT @function_return_value = MIN(this_status) FROM phase_descendants;
	SELECT @function_return_value = x.this_status FROM (
		SELECT TOP 1 pd.*, gbls.kanban_status 
			FROM phase_descendants pd 
			LEFT JOIN get__' + @TableName + '_list_task_status(@status_date) gbls ON gbls.item_id = pd.this_status 
		WHERE pd.this_id <> @phase_item_id -- AND pd.this_status IS NOT NULL
		ORDER BY gbls.kanban_status ASC
	) x;

	RETURN @function_return_value;
END
'
    --PRINT @sql
	--EXEC ('DROP FUNCTION _' + @TableName + '_BudgetItemYearNPV');
    EXEC (@sql);
    FETCH NEXT FROM @GetTables INTO @TableName
  END
  CLOSE @GetTables