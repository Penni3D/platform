IF object_id('release_history') is null
CREATE TABLE release_history (
    release_version   NVARCHAR(20),
    release_date      NVARCHAR(20),
    installation_date DATETIME,
    repository        NVARCHAR(50)
)