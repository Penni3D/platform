
CREATE TABLE [dbo].[instance_currencies]( 
  [currency_id] [int] NOT NULL IDENTITY (1,1),
  [instance] [nvarchar](10) NOT NULL, 
  [name] [nvarchar](50) NOT NULL, 
  [abbr] [nvarchar](10) NULL, 
  [baseline_value] [tinyint]
 CONSTRAINT [PK_instance_currencies] PRIMARY KEY CLUSTERED  
( 
  [currency_id] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
GO

ALTER TABLE instance_currencies
  ADD FOREIGN KEY (instance)
REFERENCES instances (instance) ON DELETE CASCADE ON UPDATE NO ACTION
GO
 
CREATE TABLE [dbo].[instance_currencies_values]( 
  [instance] [nvarchar](10) NOT NULL, 
  [currency_id] [int],
  [exchange_rate] [float],
  [current_default] [tinyint],
  [date] [date])
  
GO

ALTER TABLE instance_currencies_values
  ADD FOREIGN KEY (currency_id)
REFERENCES instance_currencies (currency_id) ON DELETE CASCADE ON UPDATE NO ACTION

GO


DECLARE @Instance NVARCHAR(MAX);
DECLARE @sqlCommand varchar(MAX)
DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances 

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
	  IF(SELECT COUNT(*) FROM item_columns where name = 'currency' AND instance = @instance AND process = 'user') = 0 BEGIN
	  
		EXEC app_ensure_list @Instance, 'list_system_currencies', 'system_currencies', 0
		EXEC dbo.app_ensure_column @Instance, 'list_system_currencies', 'list_item', 0, 0, 0, 0, NULL, 0, 0
		EXEC dbo.app_ensure_column @Instance, 'list_system_currencies', 'list_symbol', 0, 0, 0, 0, NULL, 0, 0
		EXEC dbo.app_ensure_column @Instance, 'list_system_currencies', 'list_symbol_fill', 0, 0, 0, 0, NULL, 0, 0
		
		INSERT INTO items (process, instance) 
		VALUES ('list_system_currencies', @Instance)
		
		DECLARE @Code NVARCHAR(50) = (SELECT default_locale_id FROM instances WHERE instance = @Instance)
		
		INSERT INTO instance_translations (instance,code,variable,translation)
		VALUES (@Instance, @Code, 'variable_currency_eur', 'euro')
		
		
		DECLARE @NewItemId int = (SELECT TOP 1 item_id FROM items ORDER BY item_id DESC)

		
		
		SET @sqlCommand = 'UPDATE _' + @Instance + '_list_system_currencies SET list_item = ''eur'' WHERE item_id =' + CAST(@NewItemId AS NVARCHAR)
		EXEC (@sqlCommand)	
		
		INSERT INTO instance_currencies (instance, name, abbr,baseline_value) VALUES (@Instance, 'variable_currency_eur', 'eur', 1)
		DECLARE @EurId int = (SELECT TOP 1 currency_id FROM instance_currencies ORDER BY currency_id DESC)
		
		INSERT INTO instance_currencies_values (instance, currency_id,exchange_rate,current_default,date)
		VALUES (@Instance, @EurId, 1, 1, GETDATE())
	   
	  INSERT INTO item_columns (instance, process, name, variable, data_type, data_additional)
	  VALUES (@Instance, 'user','currency', 'USER_CURRENCY', 6, 'list_system_currencies')
	  
	  INSERT INTO process_translations (instance, process, code, variable, translation)
	  VALUES (@Instance, 'user', (SELECT default_locale_id FROM instances where instance = @Instance), 'USER_CURRENCY', 'Currency')
	  
	  INSERT INTO conditions (instance,process,variable,condition_json, order_no,disabled)
	  VALUES (@Instance, 'list_system_currencies', 'condition_list_system_currencies_delete', 'null', 'x', 0)
	  
	  DECLARE @condid int = (SELECT TOP 1 condition_id FROM conditions ORDER BY condition_id desc)
	  
	  INSERT INTO condition_features (condition_id,feature) VALUES (@condid, 'meta')
	  
	    INSERT INTO condition_states (condition_id,state) VALUES (@condid, 'meta.delete')
	  END
	  
		
        FETCH NEXT FROM cursor_process INTO 
            @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;

GO

ALTER TABLE instances
ADD default_currency NVARCHAR(50)
