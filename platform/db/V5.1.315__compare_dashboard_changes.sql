SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processDashboard_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_dashboard_id FROM process_dashboards WHERE instance = @instance AND variable = @variable
 RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processDashboard_GetVariable]
(
	@process_dashboard_id int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	SELECT TOP 1 @var = variable FROM process_dashboards WHERE process_dashboard_id = @process_dashboard_id
	RETURN @var
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processDashboardChart_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_dashboard_chart_id FROM process_dashboard_charts WHERE instance = @instance AND variable = @variable
 RETURN @id
END



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[processDashboardChart_GetVariable]
(
	@process_dashboard_chart_id int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	SELECT TOP 1 @var = variable FROM process_dashboard_charts WHERE process_dashboard_chart_id = @process_dashboard_chart_id
	RETURN @var
END