  ALTER TABLE process_containers ADD right_container INT 

GO

DECLARE @MyCursor CURSOR;
DECLARE @Process NVARCHAR(50);
DECLARE @INSTANCE NVARCHAR(10);
DECLARE @LanguageVariableAC NVARCHAR(255);
DECLARE @ContainerId INT;
DECLARE cursor_process CURSOR
FOR SELECT
process, instance FROM processes where list_process = 0 

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Process, @INSTANCE
	WHILE @@FETCH_STATUS = 0
    BEGIN
      
		SET @LanguageVariableAC = 'cont_' + @Process + '_right_container_1';
		EXEC app_set_archive_user_id 0
		INSERT INTO process_containers (instance, process) VALUES(@INSTANCE, @Process)
		
		SET @ContainerId = @@IDENTITY

		UPDATE process_containers SET variable = @LanguageVariableAC, right_container = 1 WHERE instance = @INSTANCE AND process = @Process AND process_container_id = @ContainerId
	
		DECLARE @ConditionId INT;
		INSERT INTO conditions (instance, process, condition_json, order_no, variable) VALUES(@INSTANCE, @Process, 'null', 'U', 'condition_' + @Process + '_acces_container')

		SET @ConditionId = (SELECT TOP 1 condition_id from conditions ORDER BY condition_id desc)

		INSERT INTO conditions_groups_condition (condition_group_id, condition_id) VALUES(0, @ConditionId)
		INSERT INTO condition_containers (condition_id, process_container_id) VALUES (@ConditionId, @ContainerId)
		INSERT INTO condition_features (condition_id, feature) VALUES (@ConditionId, 'meta')
		INSERT INTO condition_states (condition_id, state) VALUES (@ConditionId, 'write')
		INSERT INTO condition_states (condition_id, state) VALUES (@ConditionId, 'read')
		
		DECLARE @Language NVARCHAR(10);

		DECLARE language_c CURSOR 
		FOR SELECT 
			code from instance_languages

			OPEN language_c
		
			FETCH NEXT FROM language_c INTO @Language
				WHILE @@FETCH_STATUS = 0
				BEGIN
				PRINT(@Language)
				PRINT(@Process)
				PRINT(@LanguageVariableAC)
				PRINT('condition_' + @Process + '_acces_container')
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@INSTANCE, @Process, @Language, 'condition_' + @Process + '_acces_container', 'Access Container condition')
				INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@INSTANCE, @Process, @Language, @LanguageVariableAC, 'Access Container')
				FETCH NEXT FROM language_c INTO
			@Language
				END;
			CLOSE language_c;
			DEALLOCATE language_c;

		DECLARE @ItemColumnId1 INT;
		DECLARE item_column_cursor1 CURSOR
		
		FOR SELECT
		item_column_id FROM item_columns where status_column = 1 AND instance = @INSTANCE AND process = @Process

		OPEN item_column_cursor1
		FETCH NEXT FROM item_column_cursor1 INTO @ItemColumnId1
			WHILE @@FETCH_STATUS = 0
			BEGIN

			INSERT INTO process_container_columns (process_container_id, validation, item_column_id, order_no) VALUES(@ContainerId, '{"ReadOnly":0,"Required":0}', @ItemColumnId1, 0)
			UPDATE item_columns SET status_column = 0 WHERE item_column_id = @ItemColumnId1

			FETCH NEXT FROM item_column_cursor1 INTO
			@ItemColumnId1

			END;
			CLOSE item_column_cursor1;
			DEALLOCATE item_column_cursor1;



		DECLARE @ItemColumnId INT;

		DECLARE item_column_cursor CURSOR
		FOR SELECT
		ic.item_column_id FROM item_columns ic 
		LEFT JOIN process_container_columns pcc ON pcc.item_column_id = ic.item_column_id
		WHERE process = @PROCESS and status_column = 1 and pcc.process_container_id IS NULL

		OPEN item_column_cursor
			FETCH NEXT FROM item_column_cursor INTO @ItemColumnId
			WHILE @@FETCH_STATUS = 0
			BEGIN

			INSERT INTO process_container_columns (process_container_id, validation, item_column_id, order_no) VALUES(@ContainerId, '{"ReadOnly":0,"Required":0}', @ItemColumnId, 0)
			UPDATE item_columns SET status_column = 0 WHERE item_column_id = @ItemColumnId

			FETCH NEXT FROM item_column_cursor INTO
			@ItemColumnId

			END;
			CLOSE item_column_cursor;
			DEALLOCATE item_column_cursor;

        FETCH NEXT FROM cursor_process INTO 
            @Process,
			@INSTANCE

    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
