exec app_set_archive_user_id 1
update processes set process_type = 1 where process in ('instance_tags', 'instance_tags_groups', 'process_ratings')
GO 

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_action]
	@instance nvarchar(10),
	@process nvarchar(50),
	@name NVARCHAR(MAX),
	@process_action_id INT = 0 OUTPUT
AS
BEGIN
	INSERT INTO process_actions (instance, process) VALUES(@instance, @process)
	SET @process_action_id = @@IDENTITY
	DECLARE @langVar NVARCHAR (50) = 'ACTION_' + @process + '_' + @name
	UPDATE process_actions SET variable = @langVar WHERE instance = @instance AND process = @process

	INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, upper(@langVar), @name, 'en-GB')
END;
GO


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_column]
	@instance NVARCHAR(10), @process NVARCHAR(50), @column_name NVARCHAR(50), @container_id INT, 
	@data_type TINYINT, @system_column TINYINT, @required INT, @inputMethod INT, @inputName NVARCHAR(255), @portfolio_id INT, @use_in_select_result TINYINT, @dataAdditional1 NVARCHAR(MAX) = null, @dataAdditional2 NVARCHAR(MAX) = null, @use_in_filter INT = 0, @item_column_id INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @item_column_id = 0
	SELECT @item_column_id = item_column_id FROM item_columns WHERE instance = @instance AND name = @column_name AND process = @process
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF (@item_column_id = 0)
	BEGIN
			DECLARE @langVar NVARCHAR (50) = 'IC_' + @process + '_' + @column_name

		INSERT INTO item_columns (instance, process, name, data_type, system_column, variable, input_method, input_name, data_additional, data_additional2) 
		values (@instance, @process, @column_name, @data_type, @system_column, UPPER(@langVar), @inputMethod, @inputName, @dataAdditional1, @dataAdditional2)
		SELECT @item_column_id = @@IDENTITY
		

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @column_name, 'en-GB')
		END

		IF (@container_id > 0) BEGIN
			INSERT INTO process_container_columns (process_container_id, item_column_id, required) values(@container_id, @item_column_id, @required)
		END
	END

	IF (@portfolio_id > 0)
	BEGIN
		IF (SELECT COUNT(item_column_id) FROM process_portfolio_columns WHERE process_portfolio_id = @portfolio_id AND item_column_id = @item_column_id) = 0 
		BEGIN
			INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter)
			VALUES(
				@portfolio_id,
				@item_column_id,
				dbo.items_GetOrderEnd(@instance, @process),
				@use_in_select_result,
				@use_in_filter
			)
		END
	END
END
GO


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_condition]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10),
	@process nvarchar(50),
	@process_group_id INT,
	@process_portfolio_id INT,
	@name NVARCHAR(MAX) = 'ALL',
	@condition_json NVARCHAR(MAX) = 'null',
	@condition_id INT = 0 OUTPUT
AS
BEGIN
	DECLARE @conditionId INT
		DECLARE @langVar NVARCHAR (50) = 'CONDITION_' + @process + '_' + @name

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, @process, @langVar, @condition_json)
	SET @conditionId = @@IDENTITY
	set @condition_id = @conditionId
	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	--INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write')
	--INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')


		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @name, 'en-GB')
		END

	IF @process_group_id > 0 BEGIN
		INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, @process_group_id)
	END
	IF @process_portfolio_id > 0 BEGIN
		INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, @process_portfolio_id)
	END
END;
GO


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_conditions]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @conditionId INT

		DECLARE @langVar NVARCHAR (50) = 'CONDITION_' + 'user' + '_' + 'ALL'

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, 'user', 'ALL', 'null')
	SET @conditionId = @@IDENTITY

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user',UPPER(@langVar), 'all', 'en-GB')
		END

	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, (SELECT TOP 1 process_group_id FROM process_groups WHERE instance=@instance AND process = 'user'))
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, (SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE instance=@instance AND process = 'user'))

		SET @langVar = 'CONDITION_' + 'user_group' + '_' + 'ALL'

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, 'user_group', 'ALL', 'null')
	SET @conditionId = @@IDENTITY

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, 'user_group',UPPER(@langVar), 'all', 'en-GB')
		END

	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, (SELECT TOP 1 process_group_id FROM process_groups WHERE instance=@instance AND  process = 'user_group'))
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, (SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE instance=@instance AND process = 'user_group'))
END;
GO


SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_container]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @tab_id INT, @grid TINYINT =0, @container_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @container_id = process_container_id FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		DECLARE @langVar NVARCHAR (50) = 'CONT_' + @process + '_' + @variable

		INSERT INTO process_containers (instance, process, variable) VALUES (@instance, @process, UPPER(@langVar));
		SELECT @container_id = @@IDENTITY;

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @variable, 'en-GB')
		END

		IF (@tab_id > 0)
			BEGIN
				INSERT INTO process_tab_containers (process_tab_id, process_container_id, order_no, container_side) VALUES (@tab_id, @container_id, 1, @grid);
			END
	END
END
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_group]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @group_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_groups WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @group_id = process_group_id FROM process_groups WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		DECLARE @langVar NVARCHAR (50) = 'GROUP_' + @process + '_' + @variable

		INSERT INTO process_groups (instance, process, variable, order_no) VALUES (@instance, @process, @langVar, 1);
		SELECT @group_id = @@IDENTITY; --SCOPE_IDENTITY();

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @variable, 'en-GB')
		END

	END
END
GO

SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_list]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @system_process INT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT COUNT(*) FROM processes WHERE instance = @instance AND process = @process) = 0)
	BEGIN
		DECLARE @langVar NVARCHAR (50) = 'LIST_' + @process 

		INSERT INTO processes (instance, process, variable, process_type, list_process) VALUES (@instance, @process, @langVar , @system_process, 1);

		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @process, 'en-GB')
		END
	END
END
GO



SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_portfolio]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @portfolio_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_portfolios WHERE instance = @instance AND process = @process AND variable = @variable) = 0) BEGIN
		DECLARE @langVar NVARCHAR (50) = 'PORT_' + @process + '_' + @variable

		INSERT INTO process_portfolios (instance, process, variable) VALUES (@instance, @process, @langVar);
		SET @portfolio_id = @@IDENTITY;
		
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = UPPER(@langVar)) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@langVar), @variable, 'en-GB')
		END
		
	END ELSE BEGIN
		SELECT @portfolio_id = process_portfolio_id FROM process_portfolios WHERE instance = @instance AND process = @process AND variable = @variable;
	END
END
GO



SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @process_type INT,
	@colour nvarchar(50) = null, @listProcess int = 0, @listOrder nvarchar(50) = null, @newRowActionId int = null
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT COUNT(*) FROM processes WHERE instance = @instance AND process = @process) = 0)
	BEGIN
			DECLARE @langVar NVARCHAR (50) = 'PROCESS_' + @process + '_' + @variable

		INSERT INTO processes (instance, process, variable, process_type, colour, list_process, list_order, new_row_action_id) 
		VALUES (@instance, @process, @langVar, @process_type, @colour, @listProcess, @listOrder, @newRowActionId);
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND process = @process AND variable = @langVar) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, @langVar, @variable, 'en-GB')
		END

	END
END
GO




SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_tab]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @group_id INT, @tab_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_tabs WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @tab_id = process_tab_id FROM process_tabs WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		DECLARE @langVar NVARCHAR (50) = 'TAB_' + @process + '_' + @variable

		INSERT INTO process_tabs (instance, process, variable, order_no) VALUES (@instance, @process, @langVar, 1);
		SELECT @tab_id = @@IDENTITY; --SCOPE_IDENTITY();

		INSERT INTO process_group_tabs (process_group_id, process_tab_id, order_no) VALUES (@group_id, @tab_id, 1);
		
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND process = @process AND variable = @variable) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, @variable, @variable, 'en-GB')
		END
		
	END
END
GO