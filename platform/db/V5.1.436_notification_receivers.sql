DECLARE @Instance NVARCHAR(255);
DECLARE @RightContainerId INT;
DECLARE @SelectorId INT;
DECLARE cursor_process CURSOR

FOR SELECT instance FROM instances 

OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN
      

	  SET @RightContainerId = (SELECT process_container_id FROM process_containers WHERE right_container = 1 AND instance = @Instance AND process = 'notification')
	 -- SET @SelectorId = (SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE process_portfolio_type = 10 AND instance = @Instance AND process = 'matrix_process_row_data')
	
	EXEC dbo.app_ensure_column @Instance,'notification','user_item_id',@RightContainerId,5,0,0,null,null,0, 'user'
	EXEC dbo.app_ensure_column @Instance,'notification','user_groups',@RightContainerId,5,0,0,null,null,0, 'user_group'
  
	 -- SET @scrpt = N'ALTER TABLE _' + @Instance + '_matrix_process_row_data ADD [comment] NVARCHAR(MAX)'
	  
	 -- EXECUTE SP_EXECUTESQL @scrpt
		
        FETCH NEXT FROM cursor_process INTO 
            @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
	