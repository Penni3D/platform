DECLARE @MyCursor CURSOR;
DECLARE @Process NVARCHAR(50);
DECLARE @INSTANCE NVARCHAR(10);
DECLARE @LanguageVariable NVARCHAR(50);
DECLARE @ContainerId INT;

EXEC App_set_archive_user_id
     0

DECLARE cursor_process CURSOR FOR
    SELECT process,
           instance
    FROM processes
    WHERE list_process = 0

OPEN cursor_process

FETCH next FROM cursor_process INTO @Process, @INSTANCE

WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE @OwnerItemId INT = (SELECT item_column_id
                                    FROM item_columns
                                    WHERE process = @Process
                                      AND [name] = 'owner_item_id'
                                      AND instance = @Instance)
        DECLARE @TaskType INT = (SELECT item_column_id
                                 FROM item_columns
                                 WHERE process = @Process
                                   AND [name] = 'task_type'
                                   AND instance = @Instance)
        DECLARE @ParentItemId INT = (SELECT item_column_id
                                     FROM item_columns
                                     WHERE process = @Process
                                       AND [name] = 'parent_item_id'
                                       AND instance = @Instance)
        DECLARE @PortfolioId INT;
        DECLARE portfolio_cursor CURSOR FOR
            SELECT process_portfolio_id
            FROM process_portfolios
            WHERE process = @Process
              AND instance = @Instance

        OPEN portfolio_cursor

        FETCH next FROM portfolio_cursor INTO @PortfolioId

        WHILE @@FETCH_STATUS = 0
            BEGIN
                IF @OwnerItemId IS NOT NULL
                    BEGIN
                        IF (SELECT Count(*)
                            FROM process_portfolio_columns
                            WHERE item_column_id = @OwnerItemId
                              AND process_portfolio_id = @PortfolioId) = 0
                            BEGIN
                                INSERT INTO process_portfolio_columns
                                (process_portfolio_id,
                                 item_column_id,
                                 report_column, is_default, use_in_search)
                                VALUES (@PortfolioId,
                                        @OwnerItemId,
                                        1,0,0)
                            END
                    END

                IF @TaskType IS NOT NULL
                    BEGIN
                        IF (SELECT Count(*)
                            FROM process_portfolio_columns
                            WHERE item_column_id = @TaskType
                              AND process_portfolio_id = @PortfolioId) = 0
                            BEGIN
                                INSERT INTO process_portfolio_columns
                                (process_portfolio_id,
                                 item_column_id,
                                 report_column, is_default, use_in_search)
                                VALUES (@PortfolioId,
                                        @TaskType,
                                        1,0,0)
                            END
                    END

                IF @ParentItemId IS NOT NULL
                    BEGIN
                        IF (SELECT Count(*)
                            FROM process_portfolio_columns
                            WHERE item_column_id = @ParentItemId
                              AND process_portfolio_id = @PortfolioId) = 0
                            BEGIN
                                INSERT INTO process_portfolio_columns
                                (process_portfolio_id,
                                 item_column_id,
                                 report_column, is_default, use_in_search)
                                VALUES (@PortfolioId,
                                        @ParentItemId,
                                        1,0,0)
                            END
                    END

                FETCH next FROM portfolio_cursor INTO @PortfolioId
            END

        CLOSE portfolio_cursor

        DEALLOCATE portfolio_cursor

        FETCH next FROM cursor_process INTO @Process, @INSTANCE
    END;

CLOSE cursor_process;

DEALLOCATE cursor_process; 