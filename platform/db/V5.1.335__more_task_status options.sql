DECLARE @instance NVARCHAR(20);
DECLARE @tableName NVARCHAR(50) = '';
DECLARE instance_cursor CURSOR FOR SELECT instance FROM instances 
OPEN instance_cursor
FETCH NEXT FROM instance_cursor INTO @instance

DECLARE @sql NVARCHAR(MAX) = ''

SET @tableName = '_' + @instance + '_list_task_status';

exec app_set_archive_user_id 0
INSERT INTO items (instance, process) VALUES (@instance, 'list_task_status');
SET @sql = 'update '+@tableName+' set order_no=''Y'',kanban_status=3,list_item=''Approaching'',list_symbol=''lens'',list_symbol_fill=''yellow'' WHERE item_id='+CAST(@@identity AS nvarchar);
exec (@sql)

INSERT INTO items (instance, process) VALUES (@instance, 'list_task_status');
SET @sql = 'update '+@tableName+' set order_no=''Z'',kanban_status=4,list_item=''Late'',list_symbol=''lens'',list_symbol_fill=''red'' WHERE item_id='+CAST(@@identity AS nvarchar);
exec (@sql)