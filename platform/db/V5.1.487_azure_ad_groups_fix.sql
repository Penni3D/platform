DECLARE @Instance NVARCHAR(255);
DECLARE cursor_process CURSOR
FOR SELECT instance FROM instances 
OPEN cursor_process

FETCH NEXT FROM cursor_process INTO @Instance
	WHILE @@FETCH_STATUS = 0
    BEGIN 
        DECLARE @sql NVARCHAR(MAX)
        SET @sql = 'ALTER TABLE _' + @instance + '_list_azure_ad_groups ALTER COLUMN list_description nvarchar(MAX)'
	    EXECUTE SP_EXECUTESQL @sql

        SET @sql = 'ALTER TABLE archive__' + @instance + '_list_azure_ad_groups ALTER COLUMN list_description nvarchar(MAX)'
        EXECUTE SP_EXECUTESQL @sql

        FETCH NEXT FROM cursor_process INTO @Instance
    END;
	
CLOSE cursor_process;
DEALLOCATE cursor_process;
	