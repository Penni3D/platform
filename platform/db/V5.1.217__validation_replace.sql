﻿exec app_set_archive_user_id 1

UPDATE process_container_columns SET validation = NULL WHERE validation is not null  AND validation = 'null'
UPDATE process_container_columns SET validation = REPLACE(validation, '"precision"', '"Precision"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"rows"', '"Rows"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"max"', '"Max"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"spellcheck"', '"Spellcheck"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"suffix"', '"Suffix"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"parentId"', '"ParentId"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"dateRange"', '"DateRange"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"afterMonths"', '"AfterMonths"') WHERE validation is not null 
UPDATE process_container_columns SET validation = REPLACE(validation, '"beforeMonths"', '"BeforeMonths"') WHERE validation is not null 

UPDATE process_portfolio_columns SET validation = NULL WHERE validation is not null  AND validation = 'null'
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"precision"', '"Precision"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"rows"', '"Rows"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"max"', '"Max"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"spellcheck"', '"Spellcheck"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"suffix"', '"Suffix"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"parentId"', '"ParentId"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"dateRange"', '"DateRange"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"afterMonths"', '"AfterMonths"') WHERE validation is not null 
UPDATE process_portfolio_columns SET validation = REPLACE(validation, '"beforeMonths"', '"BeforeMonths"') WHERE validation is not null 

DELETE FROM instance_configurations WHERE category = 'notifications' and NAME = 'emails'