CREATE NONCLUSTERED INDEX calendar_id_index
ON [dbo].[calendar_data] ([event_date])
INCLUDE ([calendar_id])

GO
CREATE NONCLUSTERED INDEX calendar_event_index
ON [dbo].[calendar_data] ([calendar_id],[event_date])

GO
CREATE NONCLUSTERED INDEX calendar_ids_index
ON [dbo].[calendar_data] ([event_date])
INCLUDE ([calendar_id],[calendar_event_id],[calendar_static_holiday_id])