DECLARE @instance NVARCHAR (MAX);
DECLARE @process NVARCHAR (MAX);
DECLARE cursor_process CURSOR

FOR SELECT instance, process FROM processes WHERE list_process = 1
OPEN cursor_process
FETCH NEXT FROM cursor_process INTO @instance, @process
    WHILE @@FETCH_STATUS = 0
    BEGIN
        EXEC app_ensure_column @instance, @process, 'user_group', 0, 0, 0, 0, null, 0, 0, 'user_group',null,0,0
        FETCH NEXT FROM cursor_process INTO @instance, @process
    END;
CLOSE cursor_process;
DEALLOCATE cursor_process;