SET NOCOUNT ON;

DECLARE @instance NVARCHAR(MAX);
DECLARE @GetTables CURSOR
DECLARE @sql NVARCHAR(MAX)
--Get Tables
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
    SELECT instance FROM instances

OPEN @GetTables
FETCH NEXT FROM @GetTables INTO @instance

exec app_set_archive_user_id 0
UPDATE item_tables SET ordering = 0 WHERE process = 'allocation'

WHILE (@@FETCH_STATUS=0) BEGIN
    SET @sql = 'UPDATE _' + @instance + '_allocation SET order_no = LEFT(order_no,1)'
    EXEC (@sql)
    
    FETCH NEXT FROM @GetTables INTO @instance
END
CLOSE @GetTables

