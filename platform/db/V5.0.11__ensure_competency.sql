-- Stored Procedure
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('app_ensure_process_competency'))
   exec('CREATE PROCEDURE [dbo].[app_ensure_process_competency] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[app_ensure_process_competency]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'competency'
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''
	DECLARE @sql2 NVARCHAR(MAX) = ''
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC dbo.app_ensure_process @instance, @process, @process, 1

	--Create phase
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'competency', @group_id = @g1 OUTPUT

	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'competency', @g1, @tab_id = @t1 OUTPUT

	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'competency', @t1, 1, @container_id = @c1 OUTPUT

	DECLARE @c2 INT
	EXEC app_ensure_container @instance,@process,'Description', @t1, 2, @container_id = @c2 OUTPUT

	DECLARE @c3 INT
	EXEC app_ensure_container @instance,@process,'New Competency', 0, 1, @container_id = @c3 OUTPUT

	--Create fields
	DECLARE @i1 INT
	DECLARE @i2 INT
	DECLARE @i3 INT

	EXEC dbo.app_ensure_column @instance, @process, 'name', @c1, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT
	EXEC dbo.app_ensure_column @instance, @process, 'avg_hour_rate', @c1, 2, 1, 1, 0, null,  0, 0, @item_column_id = @i3 OUTPUT
	EXEC dbo.app_ensure_column @instance, @process, 'cost_center', @c1, 6, 1, 1, 0, null,  0, 0, 'list_cost_center', @item_column_id = @i2 OUTPUT
	EXEC dbo.app_ensure_column @instance, @process, 'description', @c2, 4, 1, 0, 0, null,  0, 0

	--New project dialog container
	INSERT INTO process_container_columns (process_container_id, item_column_id, required) values(@c3, @i1, 1);
	INSERT INTO process_container_columns (process_container_id, item_column_id, required) values(@c3, @i3, 1);
	INSERT INTO process_container_columns (process_container_id, item_column_id, required) values(@c3, @i2, 1);

	UPDATE process_container_columns SET validation = '{"max":"0"}' WHERE item_column_id = @i2


	--Create selector portfolio
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'Competency', @portfolio_id = @p1 OUTPUT
	SET @sql2 = 'UPDATE process_portfolios SET process_portfolio_type = ''1'' WHERE process_portfolio_id = ' +  CAST(@p1 AS nvarchar) 
	EXEC (@sql2)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter) VALUES (@p1, @i1, 'V', 1, 1);

	--Create process portfolio
	DECLARE @p2 INT
	EXEC app_ensure_portfolio @instance,@process,'Competency portfolio', @portfolio_id = @p2 OUTPUT
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter) VALUES (@p2, @i1, 'V', 1, 1);
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter) VALUES (@p2, @i3, 'W', 1, 1);
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter) VALUES (@p2, @i2, 'X', 1, 1);


	--Create conditions
	DECLARE @con1 INT
	EXEC app_ensure_condition @instance, @process, 0, 0, 'all', @condition_id = @con1 OUTPUT
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'read');
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write');
	INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio');
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con1, @p1);
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con1, @p2);
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con1, @g1);

	--New process container
	INSERT INTO process_portfolio_dialogs (process, instance, process_container_id, order_no) VALUES (@process, @instance, @c3, 'V');



	--New User process Tab

	DECLARE @g2 INT
	SELECT @g2 = process_group_id FROM process_groups WHERE instance = @instance AND process = 'user' AND variable = 'USER';

	DECLARE @c4 INT
	SELECT @c4 = ISNULL(process_container_id, 0) FROM process_containers WHERE instance = @instance AND process = 'user' AND variable = 'organisation_management';

	IF (@c4 IS NULL)
		BEGIN
			--Create new User process tab

			DECLARE @t2 INT
			EXEC app_ensure_tab @instance,'user','USER_TAB2', @g2, @tab_id = @t2 OUTPUT

			--Create new User process container		
			EXEC app_ensure_container @instance,'user','organisation_management', @t2, 1, @container_id = @c4 OUTPUT
		END

	--Create competency column
	EXEC app_ensure_column @instance, 'user', 'competency', @c4, 5, 1, 1, 0, null,  0, 0, 'competency'

END;


GO

