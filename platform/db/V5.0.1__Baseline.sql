
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[calendar_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = calendar_id FROM calendars WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[calendar_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION [dbo].[calendar_GetVariable]
(
 @calendarId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM calendars WHERE calendar_id = @calendarId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[condition_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[condition_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = condition_id FROM conditions WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[condition_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[condition_GetVariable]
(
 @conditionId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM conditions WHERE condition_id = @conditionId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[condition_IdReplace]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[condition_IdReplace]
(
 @condition_json NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.itemColumn_GetVariable(@var3)		

				SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)

				IF @endPos2 > @endPos1 AND @endPos1 > 0
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
					END
			END

		  CONTINUE  
	END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.itemColumn_GetVariable(@var3)		

				SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)

				IF @endPos2 > @endPos1 AND @endPos1 > 0
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
					END
			END

		  CONTINUE  
	END
 RETURN @condition_json
END

GO
/****** Object:  UserDefinedFunction [dbo].[condition_VarReplace]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[condition_VarReplace]
(
 @instance NVARCHAR(10),
 @condition_json NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)

			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
				END

		  CONTINUE  
	END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			SET @var4 = dbo.itemColumn_GetId(@instance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)

			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
				END

		  CONTINUE  
	END
 RETURN @condition_json
END

GO
/****** Object:  UserDefinedFunction [dbo].[ContainerRows]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ContainerRows]
(
	@containerId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @retVal INT
	DECLARE @dataType INT
	DECLARE @dataAdditional NVARCHAR(255)
	DECLARE @rowCount INT

	SET @retVal = 0
	DECLARE dataCursor CURSOR FOR 
		SELECT data_type, data_additional FROM process_container_columns pcc INNER JOIN item_columns ic ON pcc.item_column_id = ic.item_column_id WHERE process_container_id = @containerId
		OPEN dataCursor

		FETCH NEXT FROM dataCursor INTO  @dataType, @DataAdditional
		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			IF @dataType = 6
			BEGIN
			SET @rowCount = 1
			WHILE (@rowCount > 0)
			BEGIN 
				SELECT @rowCount = COUNT(*) FROM process_subprocesses WHERE process=@DataAdditional
				SELECT @DataAdditional = parent_process FROM process_subprocesses WHERE process=@DataAdditional
				SET @retVal = @retVal + 1
			END
			END ELSE
			BEGIN
				SET @retVal = @retVal + 1
			END
			FETCH NEXT FROM dataCursor INTO  @dataType, @DataAdditional
		END 
  
	RETURN @retVal
END

GO
/****** Object:  UserDefinedFunction [dbo].[FunctionExists]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @table_name VARCHAR(15) = 'Projects'

--drop table if exists 'projects'
--EXEC sp_executesql('DROP TABLE IF EXISTS ' + @table_name)

CREATE FUNCTION [dbo].[FunctionExists]
(
    @FunctionName VARCHAR(200)
)
    RETURNS BIT
AS
BEGIN
    If Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].[' + @FunctionName + ']')) BEGIN
        RETURN 1
	END
    RETURN 0
END

GO
/****** Object:  UserDefinedFunction [dbo].[get_ListColumnIds]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function  [dbo].[get_ListColumnIds]( @itemColumnId int )
		returns @rtab table ([item_id] int not null)
		as
		begin	
		declare @instance nvarchar(10)
		declare @data_additional as nvarchar(max)
		declare @sql nvarchar(max)

		select @instance = instance, @data_additional = data_additional from item_columns where item_column_id = @itemColumnId

		insert @rtab select item_id from items where instance = @instance and process = @data_additional
			return
		end


--CREATE PROCEDURE dbo.get_ListColumnIds   
--   @itemColumnId int, @idCursor CURSOR VARYING OUTPUT  
--AS  
--		declare @instance nvarchar(10)
--		declare @data_additional as nvarchar(max)
--		declare @sql nvarchar(max)

--		select @instance = instance, @data_additional = data_additional from item_columns where item_column_id = @itemColumnId

--		IF OBJECT_ID('tempdb..#ids') IS NOT NULL DROP TABLE #ids 
--		CREATE table #ids (id int) 

--		set @sql = 'insert into #ids (id) (select item_id from _' + @instance + '_' + @data_additional + ')'
--		exec (@sql)

--		SET NOCOUNT ON;  
--		SET @idCursor = CURSOR  
--		FORWARD_ONLY STATIC FOR  
--		  SELECT id FROM #ids;  

--    OPEN @idCursor;  
--GO 

GO
/****** Object:  UserDefinedFunction [dbo].[GetCombinedCalendarCount]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetCombinedCalendarCount](@base_calendar_id INT, @calendar_id INT, @start_date DATETIME, @end_date DATETIME)
RETURNS INT
AS
BEGIN
	DECLARE @result INT
	IF (@calendar_id = 0) BEGIN
		SET @result =  
		(SELECT COUNT(event_date)
		FROM calendar_data cd1 
		WHERE cd1.calendar_id = @base_calendar_id AND cd1.event_date BETWEEN @start_date AND @end_date)
	END ELSE BEGIN
		SET @result =  
		(SELECT COUNT(event_date) as cnt FROM (
		SELECT event_date FROM (
			SELECT event_date
			FROM calendar_data cd1 
			WHERE 
				cd1.calendar_id = @base_calendar_id AND 
				cd1.event_date BETWEEN @start_date AND @end_date
		
			UNION 
		
			SELECT 
			event_date
			FROM calendar_data cd2 
			WHERE cd2.calendar_id = @calendar_id AND 
			cd2.event_date BETWEEN @start_date AND @end_date
			) inner_query 
		GROUP BY event_date) outer_query)
	END

	RETURN @result
END


GO
/****** Object:  UserDefinedFunction [dbo].[GetDefaultBaseCalendar]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetDefaultBaseCalendar]()
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result INT;

	-- Add the T-SQL statements to compute the return value here
	SET @result = (SELECT ISNULL(MIN(calendar_id),0) FROM calendars WHERE calendars.is_default = 1);

	-- Return the result of the function
	RETURN @result;

END


GO
/****** Object:  UserDefinedFunction [dbo].[GetLoadPercentage]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetLoadPercentage]
(
	-- Add the parameters for the function here
	@base_calendar_id INT, @user_calendar_id INT, @start_date DATETIME, @end_date DATETIME, @duration FLOAT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @WorkingDays INT
	SET @WorkingDays = (SELECT dbo.GetWorkingDaysForDateRange(@base_calendar_id,@user_calendar_id,@start_date,@end_date))

	DECLARE @WorkHours FLOAT
	IF (@user_calendar_id > 0) BEGIN
		SET @WorkHours = (SELECT CASE WHEN work_hours = -1 THEN (SELECT work_hours FROM calendars WHERE calendar_id = @base_calendar_id) ELSE work_hours END FROM calendars WHERE calendar_id = @user_calendar_id)
	END ELSE BEGIN
		SET @WorkHours = (SELECT work_hours FROM calendars WHERE calendar_id = @base_calendar_id)
	END
	
	IF ((@WorkingDays * @WorkHours) = 0) BEGIN
		RETURN 0
	END
	
	RETURN (@duration / (@WorkingDays * @WorkHours)) * 100
END


GO
/****** Object:  UserDefinedFunction [dbo].[getOrderBetween]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getOrderBetween](@order1 NVARCHAR(MAX), @order2 NVARCHAR(MAX))
RETURNS NVARCHAR(MAX)
AS BEGIN
	DECLARE @charList NVARCHAR(MAX) = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	DECLARE @minC NVARCHAR(1) = LEFT(@charList,1)
	DECLARE @midC NVARCHAR(1) = SUBSTRING(@charList, FLOOR(LEN(@charList)/2)+1,1);
	DECLARE @order1Int int = CHARINDEX(RIGHT(@order1,1),@charList COLLATE Latin1_General_CS_AS);
	DECLARE @order2Int int = CHARINDEX(RIGHT(@order2,1),@charList COLLATE Latin1_General_CS_AS);
	DECLARE @order1Core NVARCHAR(MAX) = LEFT(@order1,LEN(@order1)-1)
	DECLARE @order2Core NVARCHAR(MAX) = LEFT(@order2,LEN(@order2)-1)
	DECLARE @answer NVARCHAR(MAX)

	IF @order1Core = @order2Core AND @order1Int+1 = @order2Int SELECT @answer = @order1+@midC 
	ELSE IF LEN(@order1)>isnull(LEN(@order2),0) BEGIN
		if @order1Int <LEN(@charList) SELECT @answer = @order1Core+SUBSTRING(@charList,@order1Int+1, 1)
			ELSE SELECT @answer = @order1+@midC
	END ELSE BEGIN
		if @order2Int >2 SELECT @answer = @order2Core+SUBSTRING(@charList,@order2Int-1, 1)
			ELSE SELECT @answer = @order2Core+@minC+@midC	
	END 
	RETURN @answer
END 

GO
/****** Object:  UserDefinedFunction [dbo].[GetSample]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 CREATE FUNCTION [dbo].[GetSample]
(
	@sampleType TINYINT
)
RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @result NVARCHAR(255)
	SET @result = (SELECT TOP 1 sample_data FROM sample_data WHERE sample_type = @sampleType ORDER BY (select new_id from getNewID))
	RETURN @result
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetUserOrTeamWorkHoursForDateRange]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetUserOrTeamWorkHoursForDateRange](@owner_id INT, @owner_type_id TINYINT,@start_date DATETIME, @end_date DATETIME)
RETURNS FLOAT
AS
BEGIN
	DECLARE @result FLOAT
	
IF (@owner_type_id = 0)
BEGIN
	SET @result = 
	(SELECT dbo.GetUserWorkHours(item_id) * (DATEDIFF(dd,@start_date,@end_date) + 1 - dbo.GetCombinedCalendarCount(base_calendar_id, calendar_id, @start_date, @end_date)) FROM (SELECT DISTINCT u.item_id,u.base_calendar_id AS base_calendar_id,ISNULL(c.calendar_id,0) as calendar_id FROM users u LEFT JOIN calendars c ON u.item_id = c.item_id WHERE u.item_id = @owner_id) as q)
END

IF (@owner_type_id = 1)
BEGIN
	SET @result = (
	SELECT SUM(sum) AS total FROM 
	(SELECT (dbo.GetUserWorkHours(item_id) * (DATEDIFF(dd,@start_date,@end_date) + 1 - dbo.GetCombinedCalendarCount(base_calendar_id, calendar_id, @start_date, @end_date))) AS sum FROM 
	(
		SELECT DISTINCT u.item_id,u.base_calendar_id AS base_calendar_id,ISNULL(c.calendar_id,0) as calendar_id 
		FROM users u 
		LEFT JOIN calendars c ON u.item_id = c.item_id 
		WHERE u.item_id IN 
		(
			SELECT item_id FROM users_teams WHERE team_id = @owner_id
		)
	) AS inner_q
	) AS outer_q)
END

RETURN @result
END




GO
/****** Object:  UserDefinedFunction [dbo].[GetUserWorkHours]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetUserWorkHours](@UserId INT) RETURNS FLOAT
AS
BEGIN
	IF (@UserId = 0) BEGIN RETURN 0 END
	DECLARE @userworkhours FLOAT, @baseworkhours FLOAT, @result FLOAT
	SET @userworkhours = ISNULL((SELECT work_hours FROM calendars WHERE item_id = @UserId),-1)
	IF (@userworkhours = -1) 
		BEGIN
			DECLARE @workhours_multiplier FLOAT
			SET @workhours_multiplier = ISNULL((SELECT workhours_multiplier FROM users WHERE item_id = @UserId),0)
			SET @baseworkhours = (SELECT work_hours FROM calendars WHERE calendar_id = (SELECT base_calendar_id FROM users WHERE item_id = @UserId))
			SET @result = @baseworkhours * @workhours_multiplier
		END 
	ELSE
		SET @result = @userworkhours
	RETURN ISNULL(@Result,0)
END


GO
/****** Object:  UserDefinedFunction [dbo].[GetWorkingDaysForDateRange]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetWorkingDaysForDateRange]
(
	-- Add the parameters for the function here
	@base_calendar_id INT, @user_calendar_id INT, @start_date DATETIME, @end_date DATETIME
)
RETURNS INT
AS
BEGIN
	RETURN (SELECT (DATEDIFF(day,@start_date,@end_date) + 1) - (SELECT COUNT(*) FROM dbo.GetCombinedCalendar (@base_calendar_id,@user_calendar_id,@start_date,@end_date) WHERE notwork = 1))
END


GO
/****** Object:  UserDefinedFunction [dbo].[InstanceExists]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[InstanceExists]
(
    @instance VARCHAR(200)
)
    RETURNS BIT
AS
BEGIN
    If (SELECT COUNT(instance) FROM instances WHERE instance = @instance) > 0 BEGIN
        RETURN 1
	END
    RETURN 0
END

GO
/****** Object:  UserDefinedFunction [dbo].[InstanceLanguageExists]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[InstanceLanguageExists]
(
    @instance VARCHAR(200), @code VARCHAR(200)
)
    RETURNS BIT
AS
BEGIN
    If (SELECT COUNT(code) FROM instance_languages WHERE instance = @instance AND code = @code) > 0 BEGIN
        RETURN 1
	END
    RETURN 0
END

GO
/****** Object:  UserDefinedFunction [dbo].[IsListColumn]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[IsListColumn]
(
    @itemColumnId int
)
    RETURNS BIT
AS
BEGIN
declare @instance nvarchar(10)
declare @data_type int
declare @data_additional as nvarchar(max)

select @instance = instance, @data_type = data_type, @data_additional = data_additional from item_columns where item_column_id = @itemColumnId
  if @data_type = 6 and len(@data_additional) > 0 
  begin
	IF (SELECT COUNT(process) FROM processes WHERE instance = @instance and process = @data_additional and list_process = 1) > 0 BEGIN
		return 1
	END
  END
  RETURN 0
END


GO
/****** Object:  UserDefinedFunction [dbo].[itemColumn_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[itemColumn_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = item_column_id FROM item_columns WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[itemColumn_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[itemColumn_GetVariable]
(
 @itemColumnId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM item_columns WHERE item_column_id = @itemColumnId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[items_GetOrderBetween]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[items_GetOrderBetween]
(
	@prev_item_id INT, @next_item_id INT
)
RETURNS NVARCHAR(MAX) 
AS
BEGIN
	DECLARE @prev NVARCHAR(MAX) 
	DECLARE @next NVARCHAR(MAX) 
	SELECT @prev = order_no FROM items WHERE item_id = @prev_item_id
	SELECT @next = order_no FROM items WHERE item_id = @next_item_id
	RETURN  dbo.getOrderBetween(@prev, @next)
END

GO
/****** Object:  UserDefinedFunction [dbo].[items_GetOrderEnd]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[items_GetOrderEnd]
(
 @instance NVARCHAR(10), @process NVARCHAR(50)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @last NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @last = order_no FROM items WHERE instance = @instance AND process = @process ORDER BY order_no COLLATE Latin1_general_bin DESC
 IF (@last IS null) OR (LEN(@last) = 0) BEGIN 
  RETURN 'U';
 END
 RETURN dbo.getOrderBetween(@last,null);
END

GO
/****** Object:  UserDefinedFunction [dbo].[items_GetOrderStart]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[items_GetOrderStart]
(
	@instance NVARCHAR(10), @process NVARCHAR(50)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @last NVARCHAR(MAX)
	SELECT @last = order_no FROM items WHERE instance = @instance AND process = @process ORDER BY order_no COLLATE Latin1_general_bin ASC
	IF (@last IS null) BEGIN 
		RETURN 'U';
	END
	RETURN dbo.getOrderBetween(null,@last);
END

GO
/****** Object:  UserDefinedFunction [dbo].[menu_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[menu_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = instance_menu_id FROM instance_menu WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[menu_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[menu_GetVariable]
(
 @instanceMenuId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM instance_menu WHERE instance_menu_id = @instanceMenuId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[menu_IdReplace]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[menu_IdReplace]
(
 @params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('portfolioId', @params)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @params, @startPos)
			SET @endPos2 = charindex('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.processPortfolio_GetVariable(@var3)		

				SET @params = replace(@params, @var2, 'portfolioId":' + @var4)

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('portfolioId', @params, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('portfolioId', @params, @endPos2+LEN(@var4))
					END
			END

		  CONTINUE  
	END

 RETURN @params
END

GO
/****** Object:  UserDefinedFunction [dbo].[menu_VarReplace]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[menu_VarReplace]
(
 @instance NVARCHAR(10),
 @params NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('portfolioId', @params)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @params, @startPos)
			SET @endPos2 = charindex('}', @params, @startPos)
			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@params, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('portfolioId', @params, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)

			SET @var4 = dbo.processPortfolio_GetId(@instance, @var3)		

			SET @params = replace(@params, @var2, 'portfolioId":' + @var4)

			IF @endPos2 > @endPos1 AND @endPos1 > 0
				BEGIN
					SET @startPos = charindex('portfolioId', @params, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('portfolioId', @params, @endPos2+LEN(@var4))
				END

		  CONTINUE  
	END

 RETURN @params
END

GO
/****** Object:  UserDefinedFunction [dbo].[notificationCondition_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[notificationCondition_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = notification_condition_id FROM notification_conditions WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[notificationCondition_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[notificationCondition_GetVariable]
(
 @notificationConditionId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM notification_conditions WHERE notification_condition_id = @notificationConditionId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[PortfolioValue]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[PortfolioValue]
(
	@Instance VARCHAR(10),
    @PortfolioId INT,
	@ItemId INT,
	@ItemColumnId INT
)
    RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @Retval NVARCHAR(255)
	DECLARE @DataType INT
	DECLARE @DataNvarchar NVARCHAR(255)
	DECLARE @DataInt INT
	DECLARE @DataFloat FLOAT
	DECLARE @DataDate DATETIME
	DECLARE @DataText NVARCHAR(MAX)
	DECLARE @CurrentItemId INT
	DECLARE @OldItemId INT
	DECLARE @rowCount INT
	DECLARE @rowNumber INT


	SELECT @rowCount = COUNT(id.item_id)
	FROM item_data_process_selections idps
	INNER JOIN item_data id ON idps.selected_item_id = id.item_id
	INNER JOIN process_portfolio_columns ppc ON id.item_column_id = ppc.item_column_id
	INNER JOIN item_columns ic ON id.item_column_id = ic.item_column_id
	WHERE idps.item_id = @ItemId AND idps.item_column_id = @ItemColumnId AND use_in_select_result = 1

	SET @rowNumber = 0
	DECLARE dataCursor CURSOR FOR 
		SELECT id.item_id, data_type, data_nvarchar, data_int, data_float, data_date, data_text
		FROM item_data_process_selections idps
		INNER JOIN item_data id ON idps.selected_item_id = id.item_id
		INNER JOIN process_portfolio_columns ppc ON id.item_column_id = ppc.item_column_id
		INNER JOIN item_columns ic ON id.item_column_id = ic.item_column_id
		WHERE idps.item_id = @ItemId AND idps.item_column_id = @ItemColumnId AND use_in_select_result = 1 order by selected_item_id, order_no

		SET @RetVal = ''
		OPEN dataCursor

		FETCH NEXT FROM dataCursor INTO  @CurrentItemId, @DataType, @DataNvarchar, @DataInt, @DataFloat, @DataDate, @DataText
		SET @OldItemId = @CurrentItemId
		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			SET @rowNumber = @rowNumber + 1
			IF @CurrentItemId <> @OldItemId BEGIN
				 SET @Retval = SUBSTRING(@Retval, 0, LEN(@Retval) +1)
				SET @Retval = @Retval + ', '
			END						
			IF @DataType = 0 BEGIN
				SET @Retval = @Retval + @DataNvarchar
			END ELSE IF @DataType = 1 BEGIN
				SET @Retval = @Retval + @DataInt
			END ELSE IF @DataType = 2 BEGIN 
				SET @Retval = @Retval + @DataFloat
			END ELSE IF @DataType = 3 BEGIN
				SET @Retval = @Retval + @DataDate
			END ELSE IF @DataType = 4 BEGIN
				SET @Retval = @Retval + @DataText
			END
			IF @RowNumber < @RowCount BEGIN
				SET @Retval = @Retval + ' '
			END
			SET @OldItemId = @CurrentItemId
			FETCH NEXT FROM dataCursor INTO  @CurrentItemId, @DataType, @DataNvarchar, @DataInt, @DataFloat, @DataDate, @DataText
		END 
  

    RETURN @Retval
END


GO
/****** Object:  UserDefinedFunction [dbo].[processAction_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processAction_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_action_id FROM process_actions WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[processAction_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processAction_GetVariable]
(
 @processActionKId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM process_actions WHERE process_action_id = @processActionKId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[processActionDialog_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processActionDialog_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_action_dialog_id FROM process_actions_dialog WHERE instance = @instance and name = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[processActionDialog_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processActionDialog_GetVariable]
(
 @processActionDialogId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = name FROM process_actions_dialog WHERE process_action_dialog_id = @processActionDialogId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[processContainer_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processContainer_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_container_id FROM process_containers WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[processContainer_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processContainer_GetVariable]
(
 @processContainerId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM process_containers WHERE process_container_id = @processContainerId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[processGroup_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processGroup_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_group_id FROM process_groups WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[processGroup_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processGroup_GetVariable]
(
 @processGroupId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM process_groups WHERE process_group_id = @processGroupId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[processPortfolio_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processPortfolio_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_portfolio_id FROM process_portfolios WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[processPortfolio_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processPortfolio_GetVariable]
(
 @processPortfolioId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM process_portfolios WHERE process_portfolio_id = @processPortfolioId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[processTab_GetId]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processTab_GetId]
(
 @instance NVARCHAR(10),
 @variable NVARCHAR(255)
)
RETURNS int
AS
BEGIN
 DECLARE @id int
 SELECT TOP 1 @id = process_tab_id FROM process_tabs WHERE instance = @instance and variable = @variable
 RETURN @id
END

GO
/****** Object:  UserDefinedFunction [dbo].[processTab_GetVariable]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[processTab_GetVariable]
(
 @processTabId int
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
 DECLARE @var NVARCHAR(MAX)
 --DECLARE @max FLOAT = 9*10E99
 SELECT TOP 1 @var = variable FROM process_tabs WHERE process_tab_id = @processTabId
 RETURN @var
END

GO
/****** Object:  UserDefinedFunction [dbo].[TableColumnExists]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @table_name VARCHAR(15) = 'Projects'

--drop table if exists 'projects'
--EXEC sp_executesql('DROP TABLE IF EXISTS ' + @table_name)

CREATE FUNCTION [dbo].[TableColumnExists]
(
    @TableName VARCHAR(200),
	@ColumnName VARCHAR(200)
)
    RETURNS BIT
AS
BEGIN
IF EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = @TableName AND COLUMN_NAME = @ColumnName)  BEGIN
        RETURN 1
	END
    RETURN 0
END

GO
/****** Object:  UserDefinedFunction [dbo].[TableExists]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @table_name VARCHAR(15) = 'Projects'

--drop table if exists 'projects'
--EXEC sp_executesql('DROP TABLE IF EXISTS ' + @table_name)

CREATE FUNCTION [dbo].[TableExists]
(
    @TableName VARCHAR(200)
)
    RETURNS BIT
AS
BEGIN
    If Exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = @TableName) BEGIN
        RETURN 1
	END
    RETURN 0
END

GO
/****** Object:  UserDefinedFunction [dbo].[WorkdaysBetween]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[WorkdaysBetween](@StartDate datetime,@EndDate datetime, @Holidays nvarchar(4000))
RETURNS bigint
AS
BEGIN
    DECLARE @result bigint
	SET @result = DATEDIFF(d,@StartDate,@EndDate) + 1
	IF @result < LEN(@Holidays)/11
	BEGIN
		SET @result = 0
		DECLARE @Process_date datetime
		SET @Process_date = @StartDate
			WHILE @Process_date <=  @EndDate
			BEGIN
	       		IF PATINDEX('%'+LEFT(CONVERT(varchar,@Process_date,120),10)+'%',@Holidays) = 0
	       			SET @result = @result + 1
				SET @Process_date = DATEADD(d,1,@Process_date)
			END
	END
	ELSE
	BEGIN
		DECLARE @offset bigint
		SET @offset = 1
		DECLARE @d_temp datetime
		WHILE LEN(@Holidays) > @offset
		BEGIN
			SET @d_temp = CONVERT(datetime,SUBSTRING(@Holidays,@offset,10))
			SET @offset = @offset + 11
			IF @d_temp >= @StartDate AND @d_temp <= @EndDate
				SET @result = @result - 1
		END	
	END
	IF @result > 0
		RETURN @result
	RETURN NULL

END



GO
/****** Object:  Table [dbo].[attachments]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attachments](
	[attachment_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[item_id] [int] NULL,
	[item_column_id] [int] NULL,
	[parent_id] [int] NULL,
	[control_name] [nvarchar](max) NULL,
	[type] [tinyint] NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[url] [nvarchar](max) NULL,
 CONSTRAINT [PK_attachments] PRIMARY KEY CLUSTERED 
(
	[attachment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[attachments_data]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attachments_data](
	[attachment_id] [int] NOT NULL,
	[filename] [nvarchar](max) NOT NULL,
	[content_type] [nvarchar](max) NOT NULL,
	[filesize] [int] NOT NULL,
	[filedata] [image] NULL,
	[thumbnail] [image] NULL,
	[thumbnail_small] [image] NULL,
	[thumbnail_tiny] [image] NULL,
 CONSTRAINT [PK_attachments_data] PRIMARY KEY CLUSTERED 
(
	[attachment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[calendar_data]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calendar_data](
	[calendar_data_id] [int] IDENTITY(1,1) NOT NULL,
	[calendar_id] [int] NOT NULL,
	[calendar_event_id] [int] NULL,
	[calendar_static_holiday_id] [int] NULL,
	[event_date] [datetime] NULL,
	[dayoff] [tinyint] NOT NULL,
	[holiday] [tinyint] NOT NULL,
	[notwork] [tinyint] NOT NULL,
 CONSTRAINT [PK_calendar_data] PRIMARY KEY CLUSTERED 
(
	[calendar_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[calendar_data_attributes]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calendar_data_attributes](
	[calendar_data_attribute_id] [int] IDENTITY(1,1) NOT NULL,
	[calendar_id] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[type_id] [tinyint] NOT NULL,
	[event_date] [datetime] NOT NULL,
 CONSTRAINT [PK_calendar_data_attributes] PRIMARY KEY CLUSTERED 
(
	[calendar_data_attribute_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[calendar_events]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calendar_events](
	[calendar_event_id] [int] IDENTITY(1,1) NOT NULL,
	[calendar_id] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[event_start] [datetime] NOT NULL,
	[event_end] [datetime] NOT NULL,
 CONSTRAINT [PK_calendar_events] PRIMARY KEY CLUSTERED 
(
	[calendar_event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[calendar_static_holidays]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calendar_static_holidays](
	[calendar_static_holiday_id] [int] IDENTITY(1,1) NOT NULL,
	[calendar_id] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[month] [tinyint] NOT NULL,
	[day] [tinyint] NOT NULL,
 CONSTRAINT [PK_calendar_static_holidays] PRIMARY KEY CLUSTERED 
(
	[calendar_static_holiday_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[calendars]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calendars](
	[calendar_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[item_id] [int] NULL,
	[variable] [nvarchar](50) NOT NULL,
	[calendar_name] [nvarchar](255) NOT NULL,
	[work_hours] [decimal](18, 2) NOT NULL,
	[def_1] [tinyint] NOT NULL,
	[def_2] [tinyint] NOT NULL,
	[def_3] [tinyint] NOT NULL,
	[def_4] [tinyint] NOT NULL,
	[def_5] [tinyint] NOT NULL,
	[def_6] [tinyint] NOT NULL,
	[def_7] [tinyint] NOT NULL,
	[is_default] [tinyint] NOT NULL,
	[base_calendar] [tinyint] NOT NULL,
	[description] [nvarchar](MAX),
	
 CONSTRAINT [PK_calendars] PRIMARY KEY CLUSTERED 
(
	[calendar_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[condition_containers]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condition_containers](
	[condition_id] [int] NOT NULL,
	[process_container_id] [int] NOT NULL,
 CONSTRAINT [PK_condition_containers] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC,
	[process_container_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[condition_features]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condition_features](
	[condition_id] [int] NOT NULL,
	[feature] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_condition_features] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC,
	[feature] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[condition_groups]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condition_groups](
	[condition_id] [int] NOT NULL,
	[process_group_id] [int] NOT NULL,
 CONSTRAINT [PK_condition_groups] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC,
	[process_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[condition_portfolios]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condition_portfolios](
	[condition_id] [int] NOT NULL,
	[process_portfolio_id] [int] NOT NULL,
 CONSTRAINT [PK_condition_portfolios] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC,
	[process_portfolio_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[condition_states]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condition_states](
	[condition_id] [int] NOT NULL,
	[state] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_condition_states] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC,
	[state] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[condition_tabs]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condition_tabs](
	[condition_id] [int] NOT NULL,
	[process_tab_id] [int] NOT NULL,
 CONSTRAINT [PK_condition_tabs] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC,
	[process_tab_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[condition_user_groups]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condition_user_groups](
	[condition_id] [int] NOT NULL,
	[item_id] [int] NOT NULL,
 CONSTRAINT [PK_condition_user_groups] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC,
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[conditions]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[conditions](
	[condition_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[variable] [nvarchar](255) NULL,
	[condition_json] [nvarchar](max) NULL,
 CONSTRAINT [PK_conditions] PRIMARY KEY CLUSTERED 
(
	[condition_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_configurations]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_configurations](
	[instance_configuration_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NULL,
	[group_name] [nvarchar](255) NULL,
	[category] [nvarchar](255) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[server] [tinyint] NOT NULL,
	[domain] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_hosts]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_hosts](
	[instance] [nvarchar](10) NOT NULL,
	[host] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_instance_hosts] PRIMARY KEY CLUSTERED 
(
	[instance] ASC,
	[host] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__instance__7F309E8F44CA3770] UNIQUE NONCLUSTERED 
(
	[host] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_languages]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_languages](
	[instance] [nvarchar](10) NOT NULL,
	[code] [nvarchar](5) NOT NULL,
	[name] [nvarchar](50) NULL,
	[abbr] [nvarchar](10) NULL,
	[icon_url] [nvarchar](255) NULL,
 CONSTRAINT [PK_instance_languages] PRIMARY KEY CLUSTERED 
(
	[instance] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_logs]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_logs](
	[instance_log_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[message] [nvarchar](max) NOT NULL CONSTRAINT [DF_instance_logs_message]  DEFAULT (''),
	[stack] [nvarchar](max) NOT NULL CONSTRAINT [DF_instance_logs_stack]  DEFAULT (''),
	[occurence] [datetime] NULL CONSTRAINT [DF_instance_logs_occurence]  DEFAULT (getdate()),
	[user_item_id] [int] NOT NULL CONSTRAINT [DF_instance_logs_user_item_id]  DEFAULT ((0)),
	[log_type] [tinyint] NOT NULL CONSTRAINT [DF_instance_logs_log_type]  DEFAULT ((0)),
	[log_severity] [tinyint] NOT NULL CONSTRAINT [DF_instance_logs_log_severity]  DEFAULT ((0)),
	[log_category] [tinyint] NOT NULL CONSTRAINT [DF_instance_logs_group]  DEFAULT ((0)),
	[server_id] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_instance_logs] PRIMARY KEY CLUSTERED 
(
	[instance_log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_menu]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_menu](
	[instance_menu_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[parent_id] [int] NULL,
	[variable] [nvarchar](255) NULL,
	[order_no] [nvarchar](255) NOT NULL,
	[default_state] [nvarchar](255) NULL,
	[params] [nvarchar](255) NULL,
	[icon] [nvarchar](50) NULL,
 CONSTRAINT [PK_instance_menu] PRIMARY KEY CLUSTERED 
(
	[instance_menu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_menu_rights]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_menu_rights](
	[instance_menu_id] [int] NOT NULL,
	[item_id] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_translations]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_translations](
	[instance] [nvarchar](10) NOT NULL,
	[code] [nvarchar](5) NOT NULL,
	[variable] [nvarchar](50) NOT NULL,
	[translation] [nvarchar](max) NULL,
 CONSTRAINT [PK_instance_translations] PRIMARY KEY CLUSTERED 
(
	[instance] ASC,
	[code] ASC,
	[variable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_user_configurations]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_user_configurations](
	[instance] [nvarchar](10) NOT NULL,
	[user_id] [int] NOT NULL,
	[group] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_user_configurations_saved]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_user_configurations_saved](
	[instance] [nvarchar](10) NOT NULL,
	[user_id] [int] NOT NULL,
	[group] [nvarchar](255) NOT NULL,
	[id] [int] NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[public] [tinyint] NULL,
	[name] [nvarchar](255) NULL,
	[show_in_menu] [tinyint] NOT NULL,
 CONSTRAINT [PK_instance_user_configurations_saved] PRIMARY KEY CLUSTERED 
(
	[instance] ASC,
	[group] ASC,
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instance_visit_stats]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instance_visit_stats](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[view] [nvarchar](50) NOT NULL,
	[process_item_id] [int] NULL,
	[user_item_id] [int] NOT NULL,
	[date] [datetime] NOT NULL,
	[tab_id] [int] NULL,
	[feature] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[instances]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[instances](
	[instance] [nvarchar](10) NOT NULL,
	[variable] [nvarchar](255) NOT NULL,
	[default_locale_id] [nvarchar](5) NOT NULL,
	[default_date_format] [nvarchar](10) NOT NULL,
	[default_first_day_of_week] [tinyint] NOT NULL,
	[default_timezone] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_instances] PRIMARY KEY CLUSTERED 
(
	[instance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_column_joins]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_column_joins](
	[item_column_join_id] [int] IDENTITY(1,1) NOT NULL,
	[item_column_id] [int] NOT NULL,
	[parent_column_type] [int] NOT NULL,
	[parent_item_column_id] [int] NULL,
	[parent_additional_join] [nvarchar](max) NULL,
	[sub_column_type] [int] NOT NULL,
	[sub_item_column_id] [int] NULL,
	[sub_additional_join] [nvarchar](max) NULL,
 CONSTRAINT [PK_process_portfolio_join_columns] PRIMARY KEY CLUSTERED 
(
	[item_column_join_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_columns]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_columns](
	[item_column_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[variable] [nvarchar](255) NULL,
	[data_type] [tinyint] NOT NULL CONSTRAINT [DF_business_process_columns_data_type]  DEFAULT ((0)),
	[system_column] [tinyint] NOT NULL CONSTRAINT [DF_extension_columns_system_column]  DEFAULT ((0)),
	[data_additional] [nvarchar](max) NULL,
	[data_additional2] [nvarchar](255) NULL,
	[input_method] [tinyint] NULL,
	[input_name] [nvarchar](50) NULL,
	[status_column] [tinyint] NOT NULL CONSTRAINT [DF_item_columns_system_column1]  DEFAULT ((0)),
	[check_conditions] [tinyint] NOT NULL CONSTRAINT [DF_item_columns_check_conditions]  DEFAULT ((0)),
	[relative_column_id] [int] NULL,
	[process_dialog_id] [int] NULL,
	[process_action_id] [int] NULL,
	[link_process] [nvarchar](50) NULL,
 CONSTRAINT [PK_item_columns] PRIMARY KEY CLUSTERED 
(
	[item_column_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_data]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_data](
	[item_id] [int] NOT NULL,
	[item_column_id] [int] NOT NULL,
	[data_nvarchar] [nvarchar](255) NULL,
	[data_int] [int] NULL,
	[data_float] [float] NULL,
	[data_date] [datetime] NULL,
	[data_text] [nvarchar](max) NULL,
	[last_saved_user_item_id] [int] NULL,
	[last_saved_date] [datetime] NULL,
 CONSTRAINT [PK_item_data] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC,
	[item_column_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_data_process_selections]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_data_process_selections](
	[item_id] [int] NOT NULL,
	[item_column_id] [int] NOT NULL,
	[selected_item_id] [int] NOT NULL,
	[link_item_id] [int] NULL,
 CONSTRAINT [PK_item_data_process_selections] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC,
	[item_column_id] ASC,
	[selected_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_subitems]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_subitems](
	[parent_item_id] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[favourite] [tinyint] NOT NULL,
 CONSTRAINT [PK_item_subitems] PRIMARY KEY CLUSTERED 
(
	[parent_item_id] ASC,
	[item_id] ASC,
	[favourite] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[item_tables]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item_tables](
	[item_table_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_item_tables] PRIMARY KEY CLUSTERED 
(
	[item_table_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[items]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[items](
	[item_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[deleted] [int] NOT NULL,
	[order_no] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_items] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[links]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[links](
	[url] [nvarchar](36) NOT NULL,
	[params] [nvarchar](max) NOT NULL,
	[created] [datetime] NULL,
	[expiration] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[notification_conditions]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notification_conditions](
	[notification_condition_id] [int] IDENTITY(1,1) NOT NULL,
	[variable] [nvarchar](255) NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[item_id] [int] NULL,
	[before_condition_id] [int] NULL,
	[after_condition_id] [int] NULL,
	[view_id] [nvarchar](255) NULL,
 CONSTRAINT [PK_notification_conditions] PRIMARY KEY CLUSTERED 
(
	[notification_condition_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[notification_receivers]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notification_receivers](
	[notification_condition_id] [int] NOT NULL,
	[item_column_id] [int] NOT NULL,
	[list_process] [nvarchar](50) NULL,
	[list_item_column_id] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_action_rows]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_action_rows](
	[process_action_row_id] [int] IDENTITY(1,1) NOT NULL,
	[process_action_id] [int] NOT NULL,
	[process_action_type] [tinyint] NOT NULL,
	[item_column_id] [int] NULL,
	[value_type] [tinyint] NOT NULL,
	[value] [nvarchar](max) NULL,

 CONSTRAINT [PK_process_action_rows] PRIMARY KEY CLUSTERED 
(
	[process_action_row_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_actions]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_actions](
	[process_action_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NULL,
	[variable] [nvarchar](255) NULL,
 CONSTRAINT [PK_process_actions] PRIMARY KEY CLUSTERED 
(
	[process_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_actions_dialog]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_actions_dialog](
	[process_action_dialog_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NULL,
	[name] [nvarchar](255) NULL,
	[type] [int] NULL,
	[dialog_title_variable] [nvarchar](255) NULL,
	[dialog_text_variable] [nvarchar](255) NULL,
	[cancel_button_variable] [nvarchar](255) NULL,
	[submit_button_variable] [nvarchar](255) NULL,
 CONSTRAINT [PK_process_actions_dialog] PRIMARY KEY CLUSTERED 
(
	[process_action_dialog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_container_columns]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_container_columns](
	[process_container_column_id] [int] IDENTITY(1,1) NOT NULL,
	[process_container_id] [int] NOT NULL,
	[item_column_id] [int] NULL,
	[placeholder] [nvarchar](50) NULL,
	[validation] [nvarchar](50) NULL,
	[required] [int] NULL,
	[order_no] [nvarchar](255) NOT NULL,
	[read_only] [tinyint] NOT NULL,
 CONSTRAINT [PK_process_container_columns] PRIMARY KEY CLUSTERED 
(
	[process_container_column_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_containers]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_containers](
	[process_container_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[variable] [nvarchar](255) NULL,
	[rows] [int] NOT NULL,
	[cols] [int] NOT NULL,
 CONSTRAINT [PK_process_containers_1] PRIMARY KEY CLUSTERED 
(
	[process_container_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_dashboard_chart_map]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_dashboard_chart_map](
	[process_dashboard_chart_id] [int] NOT NULL,
	[process_dashboard_id] [int] NOT NULL,
	[order_no] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_dashboard_charts]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_dashboard_charts](
	[process_dashboard_chart_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[json_data] [nvarchar](max) NOT NULL,
	[process_portfolio_id] [int] NOT NULL,
 CONSTRAINT [PK_process_dashboards_charts] PRIMARY KEY CLUSTERED 
(
	[process_dashboard_chart_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_dashboards]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_dashboards](
	[process_dashboard_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[process_portfolio_id] [int] NOT NULL,
 CONSTRAINT [PK_process_dashboards] PRIMARY KEY CLUSTERED 
(
	[process_dashboard_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_group_features]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_group_features](
	[process_group_id] [int] NOT NULL,
	[feature] [nvarchar](255) NOT NULL,
	[state] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_process_group_features] PRIMARY KEY CLUSTERED 
(
	[process_group_id] ASC,
	[feature] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_group_navigation]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_group_navigation](
	[process_navigation_id] [int] IDENTITY(1,1) NOT NULL,
	[process_group_id] [int] NOT NULL,
	[variable] [nvarchar](50) NOT NULL,
	[process_tab_id] [int] NOT NULL,
	[default_state] [nvarchar](50) NOT NULL,
	[order_no] [nvarchar](max) NOT NULL,
	[type_id] [int] NOT NULL,
 CONSTRAINT [PK_process_group_navigation] PRIMARY KEY CLUSTERED 
(
	[process_navigation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_group_tabs]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_group_tabs](
	[process_group_tab_id] [int] IDENTITY(1,1) NOT NULL,
	[process_group_id] [int] NOT NULL,
	[process_tab_id] [int] NOT NULL,
	[order_no] [nvarchar](255) NULL,
 CONSTRAINT [PK_process_group_tabs] PRIMARY KEY CLUSTERED 
(
	[process_group_id] ASC,
	[process_tab_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_groups]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_groups](
	[process_group_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[variable] [nvarchar](255) NULL,
	[short_variable] [nvarchar](255) NULL,
	[order_no] [nvarchar](255) NULL,
	[type] [int] NOT NULL,
 CONSTRAINT [PK_process_groups] PRIMARY KEY CLUSTERED 
(
	[process_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_portfolio_columns]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_portfolio_columns](
	[process_portfolio_column_id] [int] IDENTITY(1,1) NOT NULL,
	[process_portfolio_id] [int] NOT NULL,
	[item_column_id] [int] NOT NULL,
	[order_no] [nvarchar](255) NOT NULL,
	[use_in_select_result] [tinyint] NOT NULL,
	[show_type] [tinyint] NOT NULL,
	[use_in_filter] [tinyint] NOT NULL,
	[archive_item_column_id] [int] NULL,
	[width] [int] NULL,
	[priority] [int] NULL,
	[sum_column] [tinyint] NOT NULL,
 CONSTRAINT [PK_process_portfolio_columns] PRIMARY KEY CLUSTERED 
(
	[process_portfolio_id] ASC,
	[item_column_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_portfolio_dialogs]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_portfolio_dialogs](
	[process_portfolio_dialog_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](50) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[process_container_id] [int] NOT NULL,
	[order_no] [nvarchar](max) NOT NULL,
	[variable] [nvarchar](50) NULL,
 CONSTRAINT [PK_process_portfolio_dialogs] PRIMARY KEY CLUSTERED 
(
	[process_portfolio_dialog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_portfolio_groups]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_portfolio_groups](
	[process_portfolio_id] [int] NOT NULL,
	[process_group_id] [int] NOT NULL,
 CONSTRAINT [PK_process_portfolio_groups] PRIMARY KEY CLUSTERED 
(
	[process_portfolio_id] ASC,
	[process_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_portfolio_user_groups]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_portfolio_user_groups](
	[process_portfolio_id] [int] NOT NULL,
	[item_id] [int] NOT NULL,
 CONSTRAINT [PK_process_portfolio_user_groups] PRIMARY KEY CLUSTERED 
(
	[process_portfolio_id] ASC,
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_portfolios]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_portfolios](
	[process_portfolio_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[variable] [nvarchar](255) NULL,
	[process_portfolio_type] [int] NOT NULL,
	[calendar_type] [int] NULL,
	[calendar_start_id] [int] NULL,
	[calendar_end_id] [int] NULL,
	[show_progress_arrows] [tinyint] NOT NULL,
	[open_split] [bit] NOT NULL,
	[default_state] [nvarchar](255) NULL,
	[recursive] [int] NULL,
 CONSTRAINT [PK_procet_portfolios] PRIMARY KEY CLUSTERED 
(
	[process_portfolio_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_subprocesses]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_subprocesses](
	[instance] [nvarchar](10) NOT NULL,
	[parent_process] [nvarchar](50) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_process_subprocesses] PRIMARY KEY CLUSTERED 
(
	[instance] ASC,
	[parent_process] ASC,
	[process] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_tab_columns]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_tab_columns](
	[process_tab_column_id] [int] IDENTITY(1,1) NOT NULL,
	[process_tab_id] [int] NOT NULL,
	[item_column_id] [int] NOT NULL,
	[order_no] [nvarchar](255) NULL,
 CONSTRAINT [PK_process_tab_columns] PRIMARY KEY CLUSTERED 
(
	[process_tab_column_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_tab_containers]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_tab_containers](
	[process_tab_container_id] [int] IDENTITY(1,1) NOT NULL,
	[process_tab_id] [int] NOT NULL,
	[process_container_id] [int] NOT NULL,
	[container_side] [tinyint] NOT NULL,
	[order_no] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_process_tab_containers_1] PRIMARY KEY CLUSTERED 
(
	[process_tab_id] ASC,
	[process_container_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_tabs]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_tabs](
	[process_tab_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[variable] [nvarchar](255) NULL,
	[order_no] [nvarchar](255) NULL,
	[use_in_new_item] [tinyint] NOT NULL,
	[archive_enabled] [tinyint] NOT NULL,
	[archive_item_column_id] [int] NULL,
 CONSTRAINT [PK_process_tabs_1] PRIMARY KEY CLUSTERED 
(
	[process_tab_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_tabs_subprocess_data]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_tabs_subprocess_data](
	[process_tab_subprocess_id] [int] NOT NULL,
	[parent_process_item_id] [int] NOT NULL,
	[item_id] [int] NOT NULL,
 CONSTRAINT [PK_process_tabs_subprocess_data] PRIMARY KEY CLUSTERED 
(
	[process_tab_subprocess_id] ASC,
	[parent_process_item_id] ASC,
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_tabs_subprocesses]    Script Date: 01/08/2017 14.18.25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_tabs_subprocesses](
	[process_tab_subprocess_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NOT NULL,
	[process_tab_id] [int] NOT NULL,
	[order_no] [nvarchar](255) NOT NULL,
	[process_side] [int] NOT NULL,
 CONSTRAINT [PK_process_tabs_subprocesses] PRIMARY KEY CLUSTERED 
(
	[process_tab_subprocess_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[process_translations]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[process_translations](
	[process_translation_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[process] [nvarchar](50) NULL,
	[code] [nvarchar](5) NOT NULL,
	[variable] [nvarchar](255) NOT NULL,
	[translation] [nvarchar](max) NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[process_translation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[processes]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[processes](
	[process] [nvarchar](50) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[variable] [nvarchar](255) NULL,
	[list_process] [tinyint] NOT NULL,
	[process_type] [tinyint] NOT NULL,
	[new_row_action_id] [int] NULL,
	[colour] [nvarchar](50) NULL,
	[list_order] [nvarchar](50) NULL,
 CONSTRAINT [PK_processes] PRIMARY KEY CLUSTERED 
(
	[process] ASC,
	[instance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sample_data]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sample_data](
	[sample_type] [tinyint] NOT NULL,
	[sample_data] [nvarchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[state_rights]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state_rights](
	[state_right_id] [int] IDENTITY(1,1) NOT NULL,
	[instance] [nvarchar](10) NOT NULL,
	[user_group_item_id] [int] NOT NULL,
	[process] [nvarchar](10) NOT NULL,
	[item_column_id] [int] NOT NULL,
	[feature] [nvarchar](255) NOT NULL,
	[state] [nvarchar](255) NOT NULL,
	[comparison_type_id] [int] NOT NULL,
	[user_column_type] [tinyint] NOT NULL,
	[user_column_id] [int] NULL,
 CONSTRAINT [PK_state_rights] PRIMARY KEY CLUSTERED 
(
	[state_right_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[task_order]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[task_order](
	[order_id] [int] IDENTITY(1,1) NOT NULL,
	[order_set] [int] NOT NULL,
	[owner_id] [int] NOT NULL,
	[task_id] [int] NOT NULL,
	[order_no] [nvarchar](max) NOT NULL CONSTRAINT [DF__task_orde__order__48DABF76]  DEFAULT (''),
 CONSTRAINT [PK_task_order] PRIMARY KEY CLUSTERED 
(
	[order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[task_parents]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[task_parents](
	[task_parent_id] [int] IDENTITY(1,1) NOT NULL,
	[process_id] [int] NOT NULL,
	[task_id] [int] NOT NULL,
	[type_id] [tinyint] NOT NULL,
 CONSTRAINT [PK_task_parents] PRIMARY KEY CLUSTERED 
(
	[task_parent_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[task_status]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[task_status](
	[status_id] [int] IDENTITY(1,1) NOT NULL,
	[parent_item_id] [int] NOT NULL,
	[title] [nvarchar](255) NULL,
	[description] [nvarchar](max) NULL,
	[is_done] [tinyint] NOT NULL CONSTRAINT [DF_task_status_is_done]  DEFAULT ((0)),
	[order_no] [nvarchar](max) NULL CONSTRAINT [DF_task_status_order_no]  DEFAULT (''),
 CONSTRAINT [PK_task_status] PRIMARY KEY CLUSTERED 
(
	[status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tasks]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tasks](
	[task_id] [int] IDENTITY(1,1) NOT NULL,
	[status_id] [int] NULL,
	[parent_id] [int] NULL,
	[title] [nvarchar](255) NULL,
	[description] [nvarchar](max) NULL,
	[color] [int] NOT NULL,
	[start_date] [datetime] NULL,
	[end_date] [datetime] NULL,
	[task_type] [int] NOT NULL,
	[responsible_id] [int] NOT NULL,
 CONSTRAINT [PK_tasks] PRIMARY KEY CLUSTERED 
(
	[task_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  UserDefinedFunction [dbo].[GetCombinedCalendar]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetCombinedCalendar](@base_calendar_id INT, @calendar_id INT, @start DATE, @end DATE)
RETURNS TABLE 
AS
RETURN 
(

SELECT 
calendar_id as calendar_id,
datadate AS event_date,
dayoff = ISNULL(dayoff1,0),
holiday = ISNULL(holiday1,0),
notwork = ISNULL(notwork2,notwork1),
calendar_event_id = ISNULL(calendar_event_id2,calendar_event_id1) ,
calendar_data_id = ISNULL(calendar_data_id2,calendar_data_id1) 
FROM 
(
	SELECT 
	calendar_id as calendar_id,
	event_date AS datadate,
	MAX(dayoff1) AS dayoff1,
	MAX(holiday1) AS holiday1,
	MAX(notwork1) AS notwork1,
	MAX(notwork2) AS notwork2,
	MAX(calendar_event_id1) AS calendar_event_id1,
	MAX(calendar_event_id2) AS calendar_event_id2, 
	MAX(calendar_data_id1) AS calendar_data_id1,
	MAX(calendar_data_id2) AS calendar_data_id2 
	FROM (
		SELECT 
		@calendar_id as calendar_id,
		event_date,
		cd1.calendar_event_id AS calendar_event_id1,
		NULL AS calendar_event_id2,
		cd1.calendar_data_id AS calendar_data_id1,
		NULL AS calendar_data_id2,
		cd1.dayoff AS dayoff1,
		cd1.holiday AS holiday1,
		cd1.notwork AS notwork1,
		NULL AS notwork2 
		FROM calendar_data cd1 
		WHERE 
			cd1.calendar_id = @base_calendar_id AND 
			--YEAR(cd1.event_date) = @year
			cd1.event_date BETWEEN @start AND @end	
		UNION 
		
		SELECT 
		calendar_id as calendar_id,
		event_date,
		NULL AS calendar_event_id1,
		cd2.calendar_event_id AS calendar_event_id2,
		NULL AS calendar_data_id1,
		cd2.calendar_data_id AS calendar_data_id2,
		NULL AS dayoff1,NULL AS holiday1,
		NULL AS notwork1,
		cd2.notwork AS notwork2 
		FROM calendar_data cd2 
		WHERE cd2.calendar_id = @calendar_id AND 
		--YEAR(cd2.event_date) = @year
		cd2.event_date BETWEEN @start AND @end
		) inner_query 
	GROUP BY calendar_id, event_date
) outer_query 

)



GO
/****** Object:  UserDefinedFunction [dbo].[GetCombinedCalendarForMonth]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetCombinedCalendarForMonth](@base_calendar_id INT, @calendar_id INT, @year INT, @month INT)
RETURNS TABLE 
AS
RETURN 
(

SELECT 
datadate AS event_date,
dayoff = ISNULL(dayoff1,0),
holiday = ISNULL(holiday1,0),
notwork = ISNULL(notwork2,notwork1),
calendar_event_id = ISNULL(calendar_event_id2,calendar_event_id1) ,
calendar_data_id = ISNULL(calendar_data_id2,calendar_data_id1) 
FROM 
(
	SELECT 
	event_date AS datadate,
	MAX(dayoff1) AS dayoff1,
	MAX(holiday1) AS holiday1,
	MAX(notwork1) AS notwork1,
	MAX(notwork2) AS notwork2,
	MAX(calendar_event_id1) AS calendar_event_id1,
	MAX(calendar_event_id2) AS calendar_event_id2, 
	MAX(calendar_data_id1) AS calendar_data_id1,
	MAX(calendar_data_id2) AS calendar_data_id2 
	FROM (
		SELECT 
		event_date,
		cd1.calendar_event_id AS calendar_event_id1,
		NULL AS calendar_event_id2,
		cd1.calendar_data_id AS calendar_data_id1,
		NULL AS calendar_data_id2,
		cd1.dayoff AS dayoff1,
		cd1.holiday AS holiday1,
		cd1.notwork AS notwork1,
		NULL AS notwork2 
		FROM calendar_data cd1 
		WHERE 
			cd1.calendar_id = @base_calendar_id AND 
			(YEAR(cd1.event_date) = @year AND MONTH(cd1.event_date) = @month)
		
		UNION 
		
		SELECT 
		event_date,
		NULL AS calendar_event_id1,
		cd2.calendar_event_id AS calendar_event_id2,
		NULL AS calendar_data_id1,
		cd2.calendar_data_id AS calendar_data_id2,
		NULL AS dayoff1,NULL AS holiday1,
		NULL AS notwork1,
		cd2.notwork AS notwork2 
		FROM calendar_data cd2 
		WHERE cd2.calendar_id = @calendar_id AND 
		(YEAR(cd2.event_date) = @year AND MONTH(cd2.event_date) = @month)
		) inner_query 
	GROUP BY event_date
) outer_query 

)




GO
/****** Object:  View [dbo].[getNewID]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[getNewID] as select newid() as new_id

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [business_process]    Script Date: 01/08/2017 14.18.26 ******/
CREATE NONCLUSTERED INDEX [business_process] ON [dbo].[item_columns]
(
	[process] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_item_columns]    Script Date: 01/08/2017 14.18.26 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_item_columns] ON [dbo].[item_columns]
(
	[instance] ASC,
	[process] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20170226-104218]    Script Date: 01/08/2017 14.18.26 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170226-104218] ON [dbo].[item_data_process_selections]
(
	[selected_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20170226-104232]    Script Date: 01/08/2017 14.18.26 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170226-104232] ON [dbo].[item_data_process_selections]
(
	[item_column_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20170226-104254]    Script Date: 01/08/2017 14.18.26 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170226-104254] ON [dbo].[item_data_process_selections]
(
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [business_process]    Script Date: 01/08/2017 14.18.26 ******/
CREATE NONCLUSTERED INDEX [business_process] ON [dbo].[items]
(
	[process] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_notification_conditions]    Script Date: 01/08/2017 14.18.26 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_notification_conditions] ON [dbo].[notification_conditions]
(
	[instance] ASC,
	[variable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NonClusteredIndex-20170515-160125]    Script Date: 01/08/2017 14.18.26 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20170515-160125] ON [dbo].[process_translations]
(
	[instance] ASC,
	[variable] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[attachments] ADD  CONSTRAINT [DF_attachments_type]  DEFAULT ((0)) FOR [type]
GO
ALTER TABLE [dbo].[attachments] ADD  CONSTRAINT [DF_attachments_name]  DEFAULT ('') FOR [name]
GO
ALTER TABLE [dbo].[calendar_data] ADD  CONSTRAINT [DF_calendar_data_calendar_id]  DEFAULT ((0)) FOR [calendar_id]
GO
ALTER TABLE [dbo].[calendar_data] ADD  CONSTRAINT [DF_calendar_events_data_dayoff]  DEFAULT ((0)) FOR [dayoff]
GO
ALTER TABLE [dbo].[calendar_data] ADD  CONSTRAINT [DF_calendar_events_data_holiday]  DEFAULT ((0)) FOR [holiday]
GO
ALTER TABLE [dbo].[calendar_data] ADD  CONSTRAINT [DF_calendar_events_data_other]  DEFAULT ((1)) FOR [notwork]
GO
ALTER TABLE [dbo].[calendar_data_attributes] ADD  CONSTRAINT [DF_calendar_data_attributes_calendar_id]  DEFAULT ((0)) FOR [calendar_id]
GO
ALTER TABLE [dbo].[calendar_data_attributes] ADD  CONSTRAINT [DF_calendar_data_attributes_name]  DEFAULT ('') FOR [name]
GO
ALTER TABLE [dbo].[calendar_events] ADD  CONSTRAINT [DF_calendar_events_calendar_id]  DEFAULT ((0)) FOR [calendar_id]
GO
ALTER TABLE [dbo].[calendar_events] ADD  CONSTRAINT [DF_calendar_events_name]  DEFAULT ('') FOR [name]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_calendar_name]  DEFAULT ('') FOR [variable]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_work_hours]  DEFAULT ((-1)) FOR [work_hours]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_def_monday]  DEFAULT ((0)) FOR [def_1]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_def_tue]  DEFAULT ((0)) FOR [def_2]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_def_wed]  DEFAULT ((0)) FOR [def_3]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_def_thu]  DEFAULT ((0)) FOR [def_4]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_def_fri]  DEFAULT ((0)) FOR [def_5]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_def_sat]  DEFAULT ((0)) FOR [def_6]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_def_sun]  DEFAULT ((0)) FOR [def_7]
GO
ALTER TABLE [dbo].[calendars] ADD  CONSTRAINT [DF_calendars_isdefault]  DEFAULT ((0)) FOR [is_default]
GO
ALTER TABLE [dbo].[instance_menu] ADD  CONSTRAINT [DF__instance___order__6BEEF189]  DEFAULT ((0)) FOR [order_no]
GO
ALTER TABLE [dbo].[instance_user_configurations_saved] ADD  CONSTRAINT [DF_instance_user_configurations_saved_public]  DEFAULT ((0)) FOR [public]
GO
ALTER TABLE [dbo].[instance_user_configurations_saved] ADD  CONSTRAINT [DF_instance_user_configurations_saved_show_in_menu]  DEFAULT ((0)) FOR [show_in_menu]
GO
ALTER TABLE [dbo].[instances] ADD  CONSTRAINT [DF_instances_name]  DEFAULT ('') FOR [variable]
GO
ALTER TABLE [dbo].[instances] ADD  CONSTRAINT [DF_instances_default_locale_id]  DEFAULT (N'en-GB') FOR [default_locale_id]
GO
ALTER TABLE [dbo].[instances] ADD  CONSTRAINT [DF_instances_default_date_format]  DEFAULT (N'dd/mm/yy') FOR [default_date_format]
GO
ALTER TABLE [dbo].[instances] ADD  CONSTRAINT [DF_instances_default_first_day_of_week]  DEFAULT ((1)) FOR [default_first_day_of_week]
GO
ALTER TABLE [dbo].[instances] ADD  CONSTRAINT [DF_instances_default_timezone]  DEFAULT (N'GMT Standard Time') FOR [default_timezone]
GO
ALTER TABLE [dbo].[item_subitems] ADD  CONSTRAINT [DF_item_subitems_favourite]  DEFAULT ((0)) FOR [favourite]
GO
ALTER TABLE [dbo].[items] ADD  CONSTRAINT [DF_items_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[items] ADD  CONSTRAINT [DF_items_order_no]  DEFAULT ((0)) FOR [order_no]
GO
ALTER TABLE [dbo].[process_action_rows] ADD  CONSTRAINT [DF_process_action_rows_process_action_type]  DEFAULT ((0)) FOR [process_action_type]
GO
ALTER TABLE [dbo].[process_action_rows] ADD  CONSTRAINT [DF_process_action_rows_value_type]  DEFAULT ((0)) FOR [value_type]
GO
ALTER TABLE [dbo].[process_container_columns] ADD  CONSTRAINT [DF_process_container_columns_order_no]  DEFAULT ((0)) FOR [order_no]
GO
ALTER TABLE [dbo].[process_container_columns] ADD  CONSTRAINT [DF_process_container_columns_read_only]  DEFAULT ((0)) FOR [read_only]
GO
ALTER TABLE [dbo].[process_containers] ADD  CONSTRAINT [DF_process_containers_rows]  DEFAULT ((1)) FOR [rows]
GO
ALTER TABLE [dbo].[process_containers] ADD  CONSTRAINT [DF_process_containers_cols]  DEFAULT ((1)) FOR [cols]
GO
ALTER TABLE [dbo].[process_groups] ADD  CONSTRAINT [DF_process_groups_type]  DEFAULT ((0)) FOR [type]
GO
ALTER TABLE [dbo].[process_portfolio_columns] ADD  CONSTRAINT [DF_process_portfolio_columns_order_no]  DEFAULT ((1)) FOR [order_no]
GO
ALTER TABLE [dbo].[process_portfolio_columns] ADD  CONSTRAINT [DF_process_portfolio_columns_use_in_select_result]  DEFAULT ((0)) FOR [use_in_select_result]
GO
ALTER TABLE [dbo].[process_portfolio_columns] ADD  CONSTRAINT [DF_process_portfolio_columns_show_list_item]  DEFAULT ((0)) FOR [show_type]
GO
ALTER TABLE [dbo].[process_portfolio_columns] ADD  CONSTRAINT [DF_process_portfolio_columns_use_in_filter]  DEFAULT ((0)) FOR [use_in_filter]
GO
ALTER TABLE [dbo].[process_portfolio_columns] ADD  CONSTRAINT [DF_process_portfolio_columns_sum_column]  DEFAULT ((0)) FOR [sum_column]
GO
ALTER TABLE [dbo].[process_portfolios] ADD  CONSTRAINT [DF_process_portfolios_process_portfolio_type]  DEFAULT ((0)) FOR [process_portfolio_type]
GO
ALTER TABLE [dbo].[process_portfolios] ADD  CONSTRAINT [DF_process_portfolios_sum_column]  DEFAULT ((0)) FOR [show_progress_arrows]
GO
ALTER TABLE [dbo].[process_portfolios] ADD  CONSTRAINT [DF_process_portfolios_open_split]  DEFAULT ((0)) FOR [open_split]
GO
ALTER TABLE [dbo].[process_portfolios] ADD  CONSTRAINT [DF_process_portfolios_recursive]  DEFAULT ((0)) FOR [recursive]
GO
ALTER TABLE [dbo].[process_tab_containers] ADD  CONSTRAINT [DF_process_tab_containers_container_side]  DEFAULT ((1)) FOR [container_side]
GO
ALTER TABLE [dbo].[process_tabs] ADD  CONSTRAINT [DF_process_tabs_use_in_new_item]  DEFAULT ((0)) FOR [use_in_new_item]
GO
ALTER TABLE [dbo].[process_tabs] ADD  CONSTRAINT [DF_process_tabs_archive_enabled]  DEFAULT ((0)) FOR [archive_enabled]
GO
ALTER TABLE [dbo].[process_tabs_subprocesses] ADD  CONSTRAINT [DF_process_tabs_subprocesses_cols]  DEFAULT ((1)) FOR [process_side]
GO
ALTER TABLE [dbo].[processes] ADD  CONSTRAINT [DF_processes_instance]  DEFAULT ('') FOR [instance]
GO
ALTER TABLE [dbo].[processes] ADD  CONSTRAINT [DF_processes_name]  DEFAULT ('') FOR [variable]
GO
ALTER TABLE [dbo].[processes] ADD  CONSTRAINT [DF_processes_list_process]  DEFAULT ((0)) FOR [list_process]
GO
ALTER TABLE [dbo].[processes] ADD  CONSTRAINT [DF_processes_process_type]  DEFAULT ((0)) FOR [process_type]
GO
ALTER TABLE [dbo].[state_rights] ADD  CONSTRAINT [DF_state_rights_user_column_type]  DEFAULT ((0)) FOR [user_column_type]
GO
ALTER TABLE [dbo].[tasks] ADD  CONSTRAINT [DF_tasks_color]  DEFAULT ((0)) FOR [color]
GO
ALTER TABLE [dbo].[tasks] ADD  CONSTRAINT [DF_tasks_task_type]  DEFAULT ((0)) FOR [task_type]
GO
ALTER TABLE [dbo].[tasks] ADD  CONSTRAINT [DF_tasks_responsible_id]  DEFAULT ((0)) FOR [responsible_id]
GO
ALTER TABLE [dbo].[attachments]  WITH CHECK ADD  CONSTRAINT [FK_attachments_instances] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[attachments] CHECK CONSTRAINT [FK_attachments_instances]
GO
ALTER TABLE [dbo].[attachments]  WITH CHECK ADD  CONSTRAINT [FK_attachments_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[attachments] CHECK CONSTRAINT [FK_attachments_item_columns]
GO
ALTER TABLE [dbo].[attachments]  WITH CHECK ADD  CONSTRAINT [FK_attachments_items] FOREIGN KEY([item_id])
REFERENCES [dbo].[items] ([item_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[attachments] CHECK CONSTRAINT [FK_attachments_items]
GO
ALTER TABLE [dbo].[calendar_data]  WITH CHECK ADD  CONSTRAINT [FK_calendar_data_calendar_events] FOREIGN KEY([calendar_event_id])
REFERENCES [dbo].[calendar_events] ([calendar_event_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[calendar_data] CHECK CONSTRAINT [FK_calendar_data_calendar_events]
GO
ALTER TABLE [dbo].[calendar_data]  WITH CHECK ADD  CONSTRAINT [FK_calendar_data_calendar_static_holidays] FOREIGN KEY([calendar_static_holiday_id])
REFERENCES [dbo].[calendar_static_holidays] ([calendar_static_holiday_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[calendar_data] CHECK CONSTRAINT [FK_calendar_data_calendar_static_holidays]
GO
ALTER TABLE [dbo].[calendar_data]  WITH CHECK ADD  CONSTRAINT [FK_calendar_data_calendars] FOREIGN KEY([calendar_id])
REFERENCES [dbo].[calendars] ([calendar_id])
GO
ALTER TABLE [dbo].[calendar_data] CHECK CONSTRAINT [FK_calendar_data_calendars]
GO
ALTER TABLE [dbo].[calendar_data_attributes]  WITH CHECK ADD  CONSTRAINT [FK_calendar_data_attributes_calendars] FOREIGN KEY([calendar_id])
REFERENCES [dbo].[calendars] ([calendar_id])
GO
ALTER TABLE [dbo].[calendar_data_attributes] CHECK CONSTRAINT [FK_calendar_data_attributes_calendars]
GO
ALTER TABLE [dbo].[calendar_events]  WITH CHECK ADD  CONSTRAINT [FK_calendar_events_calendars] FOREIGN KEY([calendar_id])
REFERENCES [dbo].[calendars] ([calendar_id])
GO
ALTER TABLE [dbo].[calendar_events] CHECK CONSTRAINT [FK_calendar_events_calendars]
GO
ALTER TABLE [dbo].[calendar_static_holidays]  WITH CHECK ADD  CONSTRAINT [FK_calendar_static_holidays_calendars] FOREIGN KEY([calendar_id])
REFERENCES [dbo].[calendars] ([calendar_id])
GO
ALTER TABLE [dbo].[calendar_static_holidays] CHECK CONSTRAINT [FK_calendar_static_holidays_calendars]
GO
ALTER TABLE [dbo].[calendars]  WITH CHECK ADD  CONSTRAINT [FK_calendars_instances] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[calendars] CHECK CONSTRAINT [FK_calendars_instances]
GO
ALTER TABLE [dbo].[calendars]  WITH CHECK ADD  CONSTRAINT [FK_calendars_items] FOREIGN KEY([item_id])
REFERENCES [dbo].[items] ([item_id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[calendars] CHECK CONSTRAINT [FK_calendars_items]
GO
ALTER TABLE [dbo].[condition_containers]  WITH CHECK ADD  CONSTRAINT [FK_condition_containers_conditions] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_containers] CHECK CONSTRAINT [FK_condition_containers_conditions]
GO
ALTER TABLE [dbo].[condition_containers]  WITH CHECK ADD  CONSTRAINT [FK_condition_containers_process_containers] FOREIGN KEY([process_container_id])
REFERENCES [dbo].[process_containers] ([process_container_id])
GO
ALTER TABLE [dbo].[condition_containers] CHECK CONSTRAINT [FK_condition_containers_process_containers]
GO
ALTER TABLE [dbo].[condition_features]  WITH CHECK ADD  CONSTRAINT [FK_condition_features_conditions] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_features] CHECK CONSTRAINT [FK_condition_features_conditions]
GO
ALTER TABLE [dbo].[condition_groups]  WITH CHECK ADD  CONSTRAINT [FK_condition_groups_conditions] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_groups] CHECK CONSTRAINT [FK_condition_groups_conditions]
GO
ALTER TABLE [dbo].[condition_groups]  WITH CHECK ADD  CONSTRAINT [FK_condition_groups_process_groups] FOREIGN KEY([process_group_id])
REFERENCES [dbo].[process_groups] ([process_group_id])
GO
ALTER TABLE [dbo].[condition_groups] CHECK CONSTRAINT [FK_condition_groups_process_groups]
GO
ALTER TABLE [dbo].[condition_portfolios]  WITH CHECK ADD  CONSTRAINT [FK_condition_portfolios_conditions] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_portfolios] CHECK CONSTRAINT [FK_condition_portfolios_conditions]
GO
ALTER TABLE [dbo].[condition_portfolios]  WITH CHECK ADD  CONSTRAINT [FK_condition_portfolios_process_portfolios] FOREIGN KEY([process_portfolio_id])
REFERENCES [dbo].[process_portfolios] ([process_portfolio_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_portfolios] CHECK CONSTRAINT [FK_condition_portfolios_process_portfolios]
GO
ALTER TABLE [dbo].[condition_states]  WITH CHECK ADD  CONSTRAINT [FK_condition_states_conditions] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_states] CHECK CONSTRAINT [FK_condition_states_conditions]
GO
ALTER TABLE [dbo].[condition_tabs]  WITH CHECK ADD  CONSTRAINT [FK_condition_tabs_conditions] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_tabs] CHECK CONSTRAINT [FK_condition_tabs_conditions]
GO
ALTER TABLE [dbo].[condition_tabs]  WITH CHECK ADD  CONSTRAINT [FK_condition_tabs_process_tabs] FOREIGN KEY([process_tab_id])
REFERENCES [dbo].[process_tabs] ([process_tab_id])
GO
ALTER TABLE [dbo].[condition_tabs] CHECK CONSTRAINT [FK_condition_tabs_process_tabs]
GO
ALTER TABLE [dbo].[condition_user_groups]  WITH CHECK ADD  CONSTRAINT [FK_condition_user_groups_conditions] FOREIGN KEY([condition_id])
REFERENCES [dbo].[conditions] ([condition_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condition_user_groups] CHECK CONSTRAINT [FK_condition_user_groups_conditions]
GO
ALTER TABLE [dbo].[conditions]  WITH CHECK ADD  CONSTRAINT [FK_conditions_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[conditions] CHECK CONSTRAINT [FK_conditions_processes]
GO
ALTER TABLE [dbo].[instance_hosts]  WITH CHECK ADD  CONSTRAINT [FK_instance_hosts_instances] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[instance_hosts] CHECK CONSTRAINT [FK_instance_hosts_instances]
GO
ALTER TABLE [dbo].[instance_languages]  WITH CHECK ADD  CONSTRAINT [FK_instance_languages_instances] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[instance_languages] CHECK CONSTRAINT [FK_instance_languages_instances]
GO
ALTER TABLE [dbo].[instance_menu]  WITH CHECK ADD  CONSTRAINT [FK_instance_menu_instance_menu] FOREIGN KEY([parent_id])
REFERENCES [dbo].[instance_menu] ([instance_menu_id])
GO
ALTER TABLE [dbo].[instance_menu] CHECK CONSTRAINT [FK_instance_menu_instance_menu]
GO
ALTER TABLE [dbo].[instance_menu]  WITH CHECK ADD  CONSTRAINT [FK_instance_menu_instances] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[instance_menu] CHECK CONSTRAINT [FK_instance_menu_instances]
GO
ALTER TABLE [dbo].[instance_menu_rights]  WITH CHECK ADD  CONSTRAINT [FK_instance_pages_items] FOREIGN KEY([item_id])
REFERENCES [dbo].[items] ([item_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[instance_menu_rights] CHECK CONSTRAINT [FK_instance_pages_items]
GO
ALTER TABLE [dbo].[instance_translations]  WITH CHECK ADD  CONSTRAINT [FK_instance_translations_instance_languages1] FOREIGN KEY([instance], [code])
REFERENCES [dbo].[instance_languages] ([instance], [code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[instance_translations] CHECK CONSTRAINT [FK_instance_translations_instance_languages1]
GO
ALTER TABLE [dbo].[instance_user_configurations]  WITH CHECK ADD  CONSTRAINT [FK_instance_user_configurations_instances] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[instance_user_configurations] CHECK CONSTRAINT [FK_instance_user_configurations_instances]
GO
ALTER TABLE [dbo].[instance_user_configurations]  WITH CHECK ADD  CONSTRAINT [FK_instance_user_configurations_items] FOREIGN KEY([user_id])
REFERENCES [dbo].[items] ([item_id])
GO
ALTER TABLE [dbo].[instance_user_configurations] CHECK CONSTRAINT [FK_instance_user_configurations_items]
GO
ALTER TABLE [dbo].[instance_user_configurations_saved]  WITH CHECK ADD  CONSTRAINT [FK_instance_user_configurations_saved_instance_user_configurations_saved] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[instance_user_configurations_saved] CHECK CONSTRAINT [FK_instance_user_configurations_saved_instance_user_configurations_saved]
GO
ALTER TABLE [dbo].[instances]  WITH CHECK ADD  CONSTRAINT [FK_instances_instances] FOREIGN KEY([instance])
REFERENCES [dbo].[instances] ([instance])
GO
ALTER TABLE [dbo].[instances] CHECK CONSTRAINT [FK_instances_instances]
GO
ALTER TABLE [dbo].[item_columns]  WITH CHECK ADD  CONSTRAINT [FK_item_columns_item_columns] FOREIGN KEY([relative_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[item_columns] CHECK CONSTRAINT [FK_item_columns_item_columns]
GO
ALTER TABLE [dbo].[item_columns]  WITH CHECK ADD  CONSTRAINT [FK_item_columns_item_columns1] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[item_columns] CHECK CONSTRAINT [FK_item_columns_item_columns1]
GO
ALTER TABLE [dbo].[item_columns]  WITH CHECK ADD  CONSTRAINT [FK_item_columns_process_actions] FOREIGN KEY([process_action_id])
REFERENCES [dbo].[process_actions] ([process_action_id])
GO
ALTER TABLE [dbo].[item_columns] CHECK CONSTRAINT [FK_item_columns_process_actions]
GO
ALTER TABLE [dbo].[item_columns]  WITH CHECK ADD  CONSTRAINT [FK_item_columns_process_actions_dialog] FOREIGN KEY([process_dialog_id])
REFERENCES [dbo].[process_actions_dialog] ([process_action_dialog_id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[item_columns] CHECK CONSTRAINT [FK_item_columns_process_actions_dialog]
GO
ALTER TABLE [dbo].[item_columns]  WITH CHECK ADD  CONSTRAINT [FK_item_columns_processes] FOREIGN KEY([link_process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[item_columns] CHECK CONSTRAINT [FK_item_columns_processes]
GO
ALTER TABLE [dbo].[item_data]  WITH CHECK ADD  CONSTRAINT [FK_item_data_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[item_data] CHECK CONSTRAINT [FK_item_data_item_columns]
GO
ALTER TABLE [dbo].[item_data_process_selections]  WITH CHECK ADD  CONSTRAINT [FK_item_data_process_selections_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[item_data_process_selections] CHECK CONSTRAINT [FK_item_data_process_selections_item_columns]
GO
ALTER TABLE [dbo].[item_data_process_selections]  WITH CHECK ADD  CONSTRAINT [FK_item_data_process_selections_items] FOREIGN KEY([item_id])
REFERENCES [dbo].[items] ([item_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[item_data_process_selections] CHECK CONSTRAINT [FK_item_data_process_selections_items]
GO
ALTER TABLE [dbo].[item_data_process_selections]  WITH CHECK ADD  CONSTRAINT [FK_item_data_process_selections_items1] FOREIGN KEY([selected_item_id])
REFERENCES [dbo].[items] ([item_id])
GO
ALTER TABLE [dbo].[item_data_process_selections] CHECK CONSTRAINT [FK_item_data_process_selections_items1]
GO
ALTER TABLE [dbo].[item_data_process_selections]  WITH CHECK ADD  CONSTRAINT [FK_item_data_process_selections_items2] FOREIGN KEY([link_item_id])
REFERENCES [dbo].[items] ([item_id])
GO
ALTER TABLE [dbo].[item_data_process_selections] CHECK CONSTRAINT [FK_item_data_process_selections_items2]
GO
ALTER TABLE [dbo].[item_subitems]  WITH CHECK ADD  CONSTRAINT [FK_item_subitems_items] FOREIGN KEY([item_id])
REFERENCES [dbo].[items] ([item_id])
GO
ALTER TABLE [dbo].[item_subitems] CHECK CONSTRAINT [FK_item_subitems_items]
GO
ALTER TABLE [dbo].[item_subitems]  WITH CHECK ADD  CONSTRAINT [FK_item_subitems_items1] FOREIGN KEY([parent_item_id])
REFERENCES [dbo].[items] ([item_id])
GO
ALTER TABLE [dbo].[item_subitems] CHECK CONSTRAINT [FK_item_subitems_items1]
GO
ALTER TABLE [dbo].[item_tables]  WITH CHECK ADD  CONSTRAINT [FK_item_tables_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[item_tables] CHECK CONSTRAINT [FK_item_tables_processes]
GO
ALTER TABLE [dbo].[items]  WITH CHECK ADD  CONSTRAINT [FK_items_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[items] CHECK CONSTRAINT [FK_items_processes]
GO
ALTER TABLE [dbo].[process_action_rows]  WITH CHECK ADD  CONSTRAINT [FK_process_action_rows_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_action_rows] CHECK CONSTRAINT [FK_process_action_rows_item_columns]
GO
ALTER TABLE [dbo].[process_action_rows]  WITH CHECK ADD  CONSTRAINT [FK_process_action_rows_process_actions] FOREIGN KEY([process_action_id])
REFERENCES [dbo].[process_actions] ([process_action_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_action_rows] CHECK CONSTRAINT [FK_process_action_rows_process_actions]
GO
ALTER TABLE [dbo].[process_actions]  WITH CHECK ADD  CONSTRAINT [FK_process_actions_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_actions] CHECK CONSTRAINT [FK_process_actions_processes]
GO
ALTER TABLE [dbo].[process_container_columns]  WITH CHECK ADD  CONSTRAINT [FK_process_container_columns_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[process_container_columns] CHECK CONSTRAINT [FK_process_container_columns_item_columns]
GO
ALTER TABLE [dbo].[process_container_columns]  WITH CHECK ADD  CONSTRAINT [FK_process_container_columns_process_containers] FOREIGN KEY([process_container_id])
REFERENCES [dbo].[process_containers] ([process_container_id])
GO
ALTER TABLE [dbo].[process_container_columns] CHECK CONSTRAINT [FK_process_container_columns_process_containers]
GO
ALTER TABLE [dbo].[process_containers]  WITH CHECK ADD  CONSTRAINT [FK_process_containers_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_containers] CHECK CONSTRAINT [FK_process_containers_processes]
GO
ALTER TABLE [dbo].[process_dashboard_chart_map]  WITH CHECK ADD  CONSTRAINT [FK_process_dashboard_chart_map_process_dashboard_charts] FOREIGN KEY([process_dashboard_chart_id])
REFERENCES [dbo].[process_dashboard_charts] ([process_dashboard_chart_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_dashboard_chart_map] CHECK CONSTRAINT [FK_process_dashboard_chart_map_process_dashboard_charts]
GO
ALTER TABLE [dbo].[process_dashboard_chart_map]  WITH CHECK ADD  CONSTRAINT [FK_process_dashboard_chart_map_process_dashboards] FOREIGN KEY([process_dashboard_id])
REFERENCES [dbo].[process_dashboards] ([process_dashboard_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_dashboard_chart_map] CHECK CONSTRAINT [FK_process_dashboard_chart_map_process_dashboards]
GO
ALTER TABLE [dbo].[process_dashboard_charts]  WITH CHECK ADD  CONSTRAINT [FK_process_dashboard_charts_process_dashboard_charts] FOREIGN KEY([process_dashboard_chart_id])
REFERENCES [dbo].[process_dashboard_charts] ([process_dashboard_chart_id])
GO
ALTER TABLE [dbo].[process_dashboard_charts] CHECK CONSTRAINT [FK_process_dashboard_charts_process_dashboard_charts]
GO
ALTER TABLE [dbo].[process_dashboard_charts]  WITH CHECK ADD  CONSTRAINT [FK_process_dashboard_charts_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_dashboard_charts] CHECK CONSTRAINT [FK_process_dashboard_charts_processes]
GO
ALTER TABLE [dbo].[process_dashboards]  WITH CHECK ADD  CONSTRAINT [FK_process_dashboards_process_portfolios] FOREIGN KEY([process_portfolio_id])
REFERENCES [dbo].[process_portfolios] ([process_portfolio_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_dashboards] CHECK CONSTRAINT [FK_process_dashboards_process_portfolios]
GO
ALTER TABLE [dbo].[process_dashboards]  WITH CHECK ADD  CONSTRAINT [FK_process_dashboards_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_dashboards] CHECK CONSTRAINT [FK_process_dashboards_processes]
GO
ALTER TABLE [dbo].[process_group_features]  WITH CHECK ADD  CONSTRAINT [FK_process_group_features_process_groups] FOREIGN KEY([process_group_id])
REFERENCES [dbo].[process_groups] ([process_group_id])
GO
ALTER TABLE [dbo].[process_group_features] CHECK CONSTRAINT [FK_process_group_features_process_groups]
GO
ALTER TABLE [dbo].[process_group_tabs]  WITH CHECK ADD  CONSTRAINT [FK_process_group_tabs_process_groups] FOREIGN KEY([process_group_id])
REFERENCES [dbo].[process_groups] ([process_group_id])
GO
ALTER TABLE [dbo].[process_group_tabs] CHECK CONSTRAINT [FK_process_group_tabs_process_groups]
GO
ALTER TABLE [dbo].[process_group_tabs]  WITH CHECK ADD  CONSTRAINT [FK_process_group_tabs_process_tabs] FOREIGN KEY([process_tab_id])
REFERENCES [dbo].[process_tabs] ([process_tab_id])
GO
ALTER TABLE [dbo].[process_group_tabs] CHECK CONSTRAINT [FK_process_group_tabs_process_tabs]
GO
ALTER TABLE [dbo].[process_groups]  WITH CHECK ADD  CONSTRAINT [FK_process_groups_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_groups] CHECK CONSTRAINT [FK_process_groups_processes]
GO
ALTER TABLE [dbo].[process_portfolio_columns]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolio_columns_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[process_portfolio_columns] CHECK CONSTRAINT [FK_process_portfolio_columns_item_columns]
GO
ALTER TABLE [dbo].[process_portfolio_columns]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolio_columns_item_columns1] FOREIGN KEY([archive_item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[process_portfolio_columns] CHECK CONSTRAINT [FK_process_portfolio_columns_item_columns1]
GO
ALTER TABLE [dbo].[process_portfolio_columns]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolio_columns_process_portfolios] FOREIGN KEY([process_portfolio_id])
REFERENCES [dbo].[process_portfolios] ([process_portfolio_id])
GO
ALTER TABLE [dbo].[process_portfolio_columns] CHECK CONSTRAINT [FK_process_portfolio_columns_process_portfolios]
GO
ALTER TABLE [dbo].[process_portfolio_groups]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolio_groups_process_groups] FOREIGN KEY([process_group_id])
REFERENCES [dbo].[process_groups] ([process_group_id])
GO
ALTER TABLE [dbo].[process_portfolio_groups] CHECK CONSTRAINT [FK_process_portfolio_groups_process_groups]
GO
ALTER TABLE [dbo].[process_portfolio_groups]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolio_groups_process_portfolios] FOREIGN KEY([process_portfolio_id])
REFERENCES [dbo].[process_portfolios] ([process_portfolio_id])
GO
ALTER TABLE [dbo].[process_portfolio_groups] CHECK CONSTRAINT [FK_process_portfolio_groups_process_portfolios]
GO
ALTER TABLE [dbo].[process_portfolio_user_groups]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolio_user_groups_items] FOREIGN KEY([item_id])
REFERENCES [dbo].[items] ([item_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_portfolio_user_groups] CHECK CONSTRAINT [FK_process_portfolio_user_groups_items]
GO
ALTER TABLE [dbo].[process_portfolio_user_groups]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolio_user_groups_process_portfolios] FOREIGN KEY([process_portfolio_id])
REFERENCES [dbo].[process_portfolios] ([process_portfolio_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_portfolio_user_groups] CHECK CONSTRAINT [FK_process_portfolio_user_groups_process_portfolios]
GO
ALTER TABLE [dbo].[process_portfolios]  WITH CHECK ADD  CONSTRAINT [FK_process_portfolios_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_portfolios] CHECK CONSTRAINT [FK_process_portfolios_processes]
GO
ALTER TABLE [dbo].[process_subprocesses]  WITH CHECK ADD  CONSTRAINT [FK_process_subprocesses_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_subprocesses] CHECK CONSTRAINT [FK_process_subprocesses_processes]
GO
ALTER TABLE [dbo].[process_subprocesses]  WITH CHECK ADD  CONSTRAINT [FK_process_subprocesses_processes1] FOREIGN KEY([parent_process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_subprocesses] CHECK CONSTRAINT [FK_process_subprocesses_processes1]
GO
ALTER TABLE [dbo].[process_tab_columns]  WITH CHECK ADD  CONSTRAINT [FK_process_tab_columns_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[process_tab_columns] CHECK CONSTRAINT [FK_process_tab_columns_item_columns]
GO
ALTER TABLE [dbo].[process_tab_columns]  WITH CHECK ADD  CONSTRAINT [FK_process_tab_columns_process_tabs] FOREIGN KEY([process_tab_id])
REFERENCES [dbo].[process_tabs] ([process_tab_id])
GO
ALTER TABLE [dbo].[process_tab_columns] CHECK CONSTRAINT [FK_process_tab_columns_process_tabs]
GO
ALTER TABLE [dbo].[process_tab_containers]  WITH CHECK ADD  CONSTRAINT [FK_process_tab_containers_process_containers] FOREIGN KEY([process_container_id])
REFERENCES [dbo].[process_containers] ([process_container_id])
GO
ALTER TABLE [dbo].[process_tab_containers] CHECK CONSTRAINT [FK_process_tab_containers_process_containers]
GO
ALTER TABLE [dbo].[process_tab_containers]  WITH CHECK ADD  CONSTRAINT [FK_process_tab_containers_process_tabs] FOREIGN KEY([process_tab_id])
REFERENCES [dbo].[process_tabs] ([process_tab_id])
GO
ALTER TABLE [dbo].[process_tab_containers] CHECK CONSTRAINT [FK_process_tab_containers_process_tabs]
GO
ALTER TABLE [dbo].[process_tabs]  WITH CHECK ADD  CONSTRAINT [FK_process_tabs_item_columns] FOREIGN KEY([archive_item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[process_tabs] CHECK CONSTRAINT [FK_process_tabs_item_columns]
GO
ALTER TABLE [dbo].[process_tabs]  WITH CHECK ADD  CONSTRAINT [FK_process_tabs_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_tabs] CHECK CONSTRAINT [FK_process_tabs_processes]
GO
ALTER TABLE [dbo].[process_tabs_subprocess_data]  WITH CHECK ADD  CONSTRAINT [FK_process_tabs_subprocess_data_process_tabs_subprocesses] FOREIGN KEY([process_tab_subprocess_id])
REFERENCES [dbo].[process_tabs_subprocesses] ([process_tab_subprocess_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_tabs_subprocess_data] CHECK CONSTRAINT [FK_process_tabs_subprocess_data_process_tabs_subprocesses]
GO
ALTER TABLE [dbo].[process_tabs_subprocesses]  WITH CHECK ADD  CONSTRAINT [FK_process_tabs_subprocesses_process_tabs] FOREIGN KEY([process_tab_id])
REFERENCES [dbo].[process_tabs] ([process_tab_id])
GO
ALTER TABLE [dbo].[process_tabs_subprocesses] CHECK CONSTRAINT [FK_process_tabs_subprocesses_process_tabs]
GO
ALTER TABLE [dbo].[process_tabs_subprocesses]  WITH CHECK ADD  CONSTRAINT [FK_process_tabs_subprocesses_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
GO
ALTER TABLE [dbo].[process_tabs_subprocesses] CHECK CONSTRAINT [FK_process_tabs_subprocesses_processes]
GO
ALTER TABLE [dbo].[process_translations]  WITH CHECK ADD  CONSTRAINT [FK_process_translations_instance_languages] FOREIGN KEY([instance], [code])
REFERENCES [dbo].[instance_languages] ([instance], [code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_translations] CHECK CONSTRAINT [FK_process_translations_instance_languages]
GO
ALTER TABLE [dbo].[process_translations]  WITH CHECK ADD  CONSTRAINT [FK_process_translations_processes] FOREIGN KEY([process], [instance])
REFERENCES [dbo].[processes] ([process], [instance])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[process_translations] CHECK CONSTRAINT [FK_process_translations_processes]
GO
ALTER TABLE [dbo].[processes]  WITH CHECK ADD  CONSTRAINT [FK_processes_process_actions] FOREIGN KEY([new_row_action_id])
REFERENCES [dbo].[process_actions] ([process_action_id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[processes] CHECK CONSTRAINT [FK_processes_process_actions]
GO
ALTER TABLE [dbo].[state_rights]  WITH CHECK ADD  CONSTRAINT [FK_state_rights_item_columns] FOREIGN KEY([item_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[state_rights] CHECK CONSTRAINT [FK_state_rights_item_columns]
GO
ALTER TABLE [dbo].[state_rights]  WITH CHECK ADD  CONSTRAINT [FK_state_rights_item_columns1] FOREIGN KEY([user_column_id])
REFERENCES [dbo].[item_columns] ([item_column_id])
GO
ALTER TABLE [dbo].[state_rights] CHECK CONSTRAINT [FK_state_rights_item_columns1]
GO
ALTER TABLE [dbo].[task_parents]  WITH CHECK ADD  CONSTRAINT [FK_task_parents_items] FOREIGN KEY([process_id])
REFERENCES [dbo].[items] ([item_id])
GO
ALTER TABLE [dbo].[task_parents] CHECK CONSTRAINT [FK_task_parents_items]
GO
ALTER TABLE [dbo].[task_parents]  WITH CHECK ADD  CONSTRAINT [FK_task_parents_items1] FOREIGN KEY([task_id])
REFERENCES [dbo].[items] ([item_id])
GO
ALTER TABLE [dbo].[task_parents] CHECK CONSTRAINT [FK_task_parents_items1]
GO

/****** Object:  StoredProcedure [dbo].[app_copy_process]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_copy_process]	-- Add the parameters for the stored procedure here
	@source_instance nvarchar(10),
	@source_process nvarchar(50),
	@target_instance nvarchar(10),
	@target_process nvarchar(50),
	@is_instance_copy int = 0,
	@copy_data int = 0
	AS
BEGIN

	DECLARE @sql NVARCHAR(MAX) = '';
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @colName NVARCHAR(MAX);
	DECLARE @colName2 NVARCHAR(MAX);

	DECLARE @pk_tables TABLE ( table_name nvarchar(max) )
	DECLARE @all_tables TABLE ( level int, table_name nvarchar(max) )

	--DECLARE @sql_table TABLE ( sql nvarchar(max) )

	DECLARE @GetTables CURSOR
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	WITH rec AS (
                SELECT level=0, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
				 where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = 'processes' AND CONSTRAINT_TYPE = 'PRIMARY KEY')
                UNION ALL 
                    SELECT r.level+1, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                    FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
                     INNER JOIN rec r ON r.fk_table = tc_pk.TABLE_NAME 
					  where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = r.fk_table AND CONSTRAINT_TYPE = 'PRIMARY KEY')  and fk_table <> pk_table and fk_table <> 'processes'
					  )
					  select distinct pk_tables.table2 as table_name FROM 
                 ( SELECT fk_table, count(fk_table) as cnt
				  FROM rec 
				 group by fk_table  ) cnt,
( select tc.table_name table1, tc2.table_name table2 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc   
   inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc on tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
    inner join INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 on rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME ) pk_tables
	where cnt.fk_table = pk_tables.table2 AND pk_tables.table2 not in ('processes', 'conditions', 'item_tables', 'item_data', 'items', 'item_subitems', 'instance_menu_rights', 'process_portfolio_user_groups', 'attachments', 'item_data_process_selections')

	set @sql = ' CREATE  PROCEDURE [dbo].[app_copy_process_temp]	-- Add the parameters for the stored procedure here
						@source_instance nvarchar(10),
						@source_process nvarchar(50),
						@target_instance nvarchar(10),
						@target_process nvarchar(50),
						@copy_data int = 0 
						AS BEGIN
						exec app_set_archive_user_id 0 
			
			IF @source_instance = @target_instance		
			BEGIN
				INSERT INTO processes (instance, process, process_type, new_row_action_id, variable, list_process) (SELECT @target_instance, @target_process, process_type, new_row_action_id, variable, list_process FROM processes WHERE instance = @source_instance AND process = @source_process); 		
			END
			ELSE			
			BEGIN
				INSERT INTO processes (instance, process, process_type, new_row_action_id, variable, list_process) (SELECT @target_instance, @target_process, process_type, new_row_action_id, @target_process, list_process FROM processes WHERE instance = @source_instance AND process = @source_process); 
			END
			'
	
	--insert into @sql_table (sql) values (@sql)
	--print @sql

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

	WHILE (@@FETCH_STATUS=0) BEGIN
		--set @sql = ''
	
		INSERT INTO @pk_tables (table_name) values(@TableName)

		DECLARE @GetColumns CURSOR
		SET @GetColumns = CURSOR FAST_FORWARD LOCAL FOR 
		select column_name FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		inner join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		where tc.table_name = @TableName AND CONSTRAINT_TYPE = 'PRIMARY KEY'

		-- print 'pks for ' + @TableName
		set @sql = @sql + ' DECLARE @' + @TableName + ' TABLE ( '
		--item_column_id int,  new_item_column_id int ) '

		OPEN @GetColumns
		FETCH NEXT FROM @GetColumns INTO @colName
			set @sql = @sql + '' + @colName + ' int, new_' + @colName + ' int'
		WHILE (@@FETCH_STATUS=0) BEGIN
		FETCH NEXT FROM @GetColumns INTO @colName
		END
		CLOSE @GetColumns
		set @sql = @sql + ' ) ' + CHAR(13)+CHAR(10) + ' DECLARE '

		set @sql = @sql + '@' + @colName + ' int, @new_' + @colName + ' int;'
		set @sql = @sql + CHAR(13)+CHAR(10)


		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
			set @sql = @sql + 'disable trigger ' + @TableName + '_INSTEAD_OF_INSERT on ' + @TableName + ';'  + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
			set @sql = @sql + 'disable trigger ' + @TableName + '_AFTER_UPDATE on ' + @TableName + ';'  + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		set @sql = @sql + ' MERGE ' + @TableName + ' AS t USING ( SELECT ' + @colName

		DECLARE @GetColumns2 CURSOR
		SET @GetColumns2 = CURSOR FAST_FORWARD LOCAL FOR
		select column_name from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid', @colName)
		
		DECLARE @colNames AS NVARCHAR(MAX) = ''
		OPEN @GetColumns2
		FETCH NEXT FROM @GetColumns2 INTO @colName2
		WHILE (@@FETCH_STATUS=0) BEGIN
			-- set @sql = @sql + ', ' + @colName2  + CHAR(13)+CHAR(10)
			set @colNames = @colNames + ' ' + @colName2 + ','
		FETCH NEXT FROM @GetColumns2 INTO @colName2
		END

		set @colNames = substring(@colNames, 0, len(@colNames))

		DECLARE @colCnt AS INTEGER = 0
		DECLARE @pks1 AS NVARCHAR(MAX) = ''
		DECLARE @pks2 AS NVARCHAR(MAX) = ''
		DECLARE @pks3 AS NVARCHAR(MAX) = ''

		SELECT @colCnt = count(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName AND COLUMN_NAME = 'process'
		IF @colCnt > 0 
		BEGIN
			set @pks1 = ',@target_instance as instance, @target_process as process '
			set @pks2 = 'instance = @source_instance AND process = @source_process '
			set @pks3 = 'instance, process, '
		END
		
		IF @tableName = 'calendar_events' OR @tableName = 'calendar_data' OR @tableName = 'calendar_data_attributes' OR @tableName = 'calendar_static_holidays' 
		BEGIN 
			set @pks2 = ' calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @source_instance ) '
		END

		IF @tableName = 'calendars' 
		BEGIN 
			set @pks1 = ',@target_instance as instance '
			set @pks2 = ' instance = @source_instance '
			set @pks3 = 'instance,  '
		END

		--IF @tableName = 'task_parents'
		--BEGIN 
		--	set @pks1 = ''
		--	set @pks2 = ' process_id IN (SELECT item_id FROM items WHERE instance = @source_instance and process = @source_process ) '
		--	set @pks3 = ''
		--END


		set @sql = @sql + ' ' + @pks1 + ', ' + @colNames + ' FROM ' + @tableName + ' WHERE 1=1 AND ' + @pks2 + ' ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( ' + CHAR(13)+CHAR(10)
		set @sql = @sql + @pks3 + @colNames + ' ) VALUES ( ' + CHAR(13)+CHAR(10)
		set @sql = @sql + @pks3 + @colNames + ' )  ' + CHAR(13)+CHAR(10)
		 set @sql = @sql + ' OUTPUT s.' + @colName + ', inserted.' + @colName + ' INTO @' + @TableName + CHAR(13)+CHAR(10) + ';'
		-- set @sql = @sql + ' OUTPUT s.' + @colName + ', @@identity INTO @' + @TableName + CHAR(13)+CHAR(10) + ';'


		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
			set @sql = @sql + ' enable trigger ' + @TableName + '_INSTEAD_OF_INSERT on ' + @TableName +' ;' + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
			set @sql = @sql + ' enable trigger ' + @TableName + '_AFTER_UPDATE on ' + @TableName +' ;' + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10)
		END
		
		FETCH NEXT FROM @GetTables INTO @TableName
	END;


	WITH rec2 AS (
                SELECT level=0, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
				inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
				 where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = 'processes' AND CONSTRAINT_TYPE = 'PRIMARY KEY')
                UNION ALL 
                    SELECT r.level+1, rc.CONSTRAINT_NAME, tc_pk.TABLE_NAME as pk_table, tc_fk.TABLE_NAME as fk_table
                    FROM  INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc 
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_fk ON rc.CONSTRAINT_NAME = tc_fk.CONSTRAINT_NAME  
					inner join  INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc_pk ON rc.UNIQUE_CONSTRAINT_NAME = tc_pk.CONSTRAINT_NAME  
                     INNER JOIN rec2 r ON r.fk_table = tc_pk.TABLE_NAME 
					  where UNIQUE_CONSTRAINT_NAME = (select CONSTRAINT_NAME  	from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk  where table_name = r.fk_table AND CONSTRAINT_TYPE = 'PRIMARY KEY') and fk_table <> pk_table and fk_table <> 'processes'
					  )
                  INSERT INTO @all_tables (level, table_name) SELECT count(fk_table) as cnt, fk_table
				  FROM rec2 
				 WHERE fk_table NOT IN (SELECT table_name FROM @pk_tables) AND fk_table not like 'condition%' and fk_table NOT IN ('processes', 'item_tables','item_data','items','item_subitems', 'instance_menu_rights', 'process_portfolio_user_groups', 'attachments', 'item_data_process_selections') group by fk_table  


	DECLARE @GetTables2 CURSOR
	SET @GetTables2 = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT table_name FROM @all_tables order by level

	OPEN @GetTables2
	FETCH NEXT FROM @GetTables2 INTO @tableName
	WHILE (@@FETCH_STATUS=0) BEGIN
		--set @sql = ''

		DECLARE @tableCnt AS INTEGER = 0
		
		select 
		@tableCnt = count(tc.table_name)
		--tc2.TABLE_NAME, ccu.COLUMN_NAME 
		from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 ON rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME
		where tc.TABLE_NAME = @tableName AND tc.CONSTRAINT_TYPE = 'FOREIGN KEY' AND tc2.table_name in (select table_name from @pk_tables)

		IF @tableCnt > 0
			BEGIN
				SET @sql = @sql + ' INSERT INTO ' + @tableName + ' (' + CHAR(13)+CHAR(10)

				DECLARE @tName NVARCHAR(MAX)
				DECLARE @cName NVARCHAR(MAX)
				DECLARE @cName2 NVARCHAR(MAX)
				DECLARE @GetCols CURSOR
				SET @GetCols = CURSOR FAST_FORWARD LOCAL FOR 
				select  tc2.TABLE_NAME, ccu.COLUMN_NAME as column_name, ccu2.COLUMN_NAME as column_name2
				from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
				INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc ON tc.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 ON rc.UNIQUE_CONSTRAINT_NAME = tc2.CONSTRAINT_NAME
				INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu2 ON rc.UNIQUE_CONSTRAINT_NAME = ccu2.CONSTRAINT_NAME
				where tc.TABLE_NAME = @tableName AND tc.CONSTRAINT_TYPE = 'FOREIGN KEY' AND tc2.table_name in (select table_name from @pk_tables)

				DECLARE @fks TABLE ( colname nvarchar(MAX) )
				DECLARE @fks_cols AS NVARCHAR(MAX) = ''
				DECLARE @fks_ins AS NVARCHAR(MAX) = ''
				DECLARE @fks_where AS NVARCHAR(MAX) = '1=0 '

				OPEN @GetCols
				FETCH NEXT FROM @GetCols INTO @tName, @cName, @cName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					INSERT INTO @fks (colname) values (@cName)
					SET @fks_cols = @fks_cols + @cName + ','
					SET @fks_ins = @fks_ins + ' (SELECT new_' + @cName2 + ' FROM @' + @tName + ' WHERE ' + @cName2 + ' = ' + @TableName + '.' + @cName + '),'
					SET @fks_where = @fks_where +' OR '  + @cName + ' IN (SELECT ' + @cName2 + ' FROM @' + @tName + ') '  + CHAR(13)+CHAR(10)
				FETCH NEXT FROM @GetCols INTO @tName, @cName, @cName2
				END
				CLOSE @GetCols


				DECLARE @GetColumns3 CURSOR
				SET @GetColumns3 = CURSOR FAST_FORWARD LOCAL FOR
				select column_name from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid') and column_name not in (select colname from @fks)
				and column_name not in ( select column_name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME=ccu.CONSTRAINT_NAME where tc.TABLE_NAME = @TableName and CONSTRAINT_TYPE = 'PRIMARY KEY' )
				and column_name not in ('process_tab_container_id', 'process_group_tab_id', 'process_portfolio_column_id')
		
				DECLARE @colNames2 AS NVARCHAR(MAX) = ''
				OPEN @GetColumns3
				FETCH NEXT FROM @GetColumns3 INTO @colName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					--set @sql = @sql + ' ' + @colName2  + ', ' + CHAR(13)+CHAR(10)
					set @colNames2 = @colNames2 + ' ' + @colName2 + ','
				FETCH NEXT FROM @GetColumns3 INTO @colName2
				END

				if @TableName = 'process_group_features'
				BEGIN
					set @colNames2 = @colNames2 + ' feature,' 
				END
				
				set @colNames2 = substring(@colNames2, 0, len(@colNames2)  )

				if LEN(@colNames2) = 0 
				BEGIN
					set @fks_cols = substring(@fks_cols, 0, len(@fks_cols)  )
					set @fks_ins = substring(@fks_ins, 0, len(@fks_ins)  )
				END

				set @sql = @sql + @fks_cols + @colNames2 + ') SELECT ' + @fks_ins + ' ' +@colNames2 + ' FROM ' + @TableName + ' WHERE ' + @fks_where 

				--update @sql_table set sql = concat(sql, @sql)
				--print @sql
			END
		ELSE
			BEGIN
				
				DECLARE @GetColumns4 CURSOR
				SET @GetColumns4 = CURSOR FAST_FORWARD LOCAL FOR
				select column_name from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName and column_name not in ('instance', 'process', 'archive_start', 'archive_userid') and column_name not in (select colname from @fks)
				and column_name not in ( select column_name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu on tc.CONSTRAINT_NAME=ccu.CONSTRAINT_NAME where tc.TABLE_NAME = @TableName and CONSTRAINT_TYPE = 'PRIMARY KEY' )
		
				DECLARE @colNames3 AS NVARCHAR(MAX) = ''
				OPEN @GetColumns4
				FETCH NEXT FROM @GetColumns4 INTO @colName2
				WHILE (@@FETCH_STATUS=0) BEGIN
					--set @sql = @sql + ' ' + @colName2  + ', ' + CHAR(13)+CHAR(10)
					set @colNames3 = @colNames3 + ' ' + @colName2 + ', '
				FETCH NEXT FROM @GetColumns4 INTO @colName2
				END			

				--if @TableName = 'process_translations'
				--BEGIN
				--	set @colNames3 = @colNames3 + ' code, variable,' 
				--END
				if @TableName = 'process_subprocesses'
				BEGIN
					set @colNames3 = @colNames3 + ' parent_process,' 
				END

				if @is_instance_copy = 1 AND @TableName <> 'process_subprocesses'
				BEGIN 
				if @TableName <> 'task_parents' and @TableName <> 'instance_user_configurations'
				BEGIN
					SET @sql = @sql + ' INSERT INTO ' + @tableName + ' (' + CHAR(13)+CHAR(10)
					set @sql = @sql + @colNames3 + ' instance, process) ( SELECT ' + @colNames3 + ' @target_instance, @target_process FROM ' + @tableName + ' WHERE instance = @source_instance AND process = @source_process )' + CHAR(13)+CHAR(10)
				END
				END
			END

	FETCH NEXT FROM @GetTables2 INTO @tableName
	END

	set @sql = @sql + '
		DECLARE @conditions TABLE ( condition_id int, new_condition_id int ) 
		 DECLARE @condition_id int, @new_condition_id int;
		
		disable trigger conditions_AFTER_UPDATE on conditions;
		disable trigger conditions_INSTEAD_OF_INSERT on conditions;

		 MERGE conditions AS t USING ( SELECT condition_id ,@target_instance as instance, @target_process as process ,  variable, condition_json FROM conditions WHERE instance = @source_instance AND process = @source_process  ) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
		instance, process,  variable, condition_json ) VALUES ( 
		instance, process,  variable, condition_json )  
		 OUTPUT s.condition_id, inserted.condition_id INTO @conditions; 
		 
		 enable trigger conditions_AFTER_UPDATE on conditions ;	
		 enable trigger conditions_INSTEAD_OF_INSERT on conditions ;	
		 '
		 
		 
	set @sql = @sql + '
		 INSERT INTO condition_containers (condition_id, process_container_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_containers.condition_id),
		  (SELECT new_process_container_id FROM @process_containers WHERE process_container_id = condition_containers.process_container_id)
		 FROM condition_containers
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_container_id IN (SELECT process_container_id FROM @process_containers) )  '
 		 
	set @sql = @sql + '
		 INSERT INTO condition_features (condition_id, feature) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_features.condition_id),
		  feature
		FROM condition_features
		WHERE condition_id IN (SELECT condition_id FROM @conditions) ) '

		 	set @sql = @sql + '
			INSERT INTO condition_groups (condition_id, process_group_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_groups.condition_id),
		  (SELECT new_process_group_id FROM @process_groups WHERE process_group_id = condition_groups.process_group_id)
		 FROM condition_groups
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_group_id IN (SELECT process_group_id FROM @process_groups) ) '

		 	set @sql = @sql + '
		 INSERT INTO condition_portfolios (condition_id, process_portfolio_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_portfolios.condition_id),
		  (SELECT new_process_portfolio_id FROM @process_portfolios WHERE process_portfolio_id = condition_portfolios.process_portfolio_id)
		 FROM condition_portfolios
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_portfolio_id IN (SELECT process_portfolio_id FROM @process_portfolios) ) '

		set @sql = @sql + '
		 INSERT INTO condition_states (condition_id, state) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_states.condition_id),
		  state
		 FROM condition_states
		 WHERE condition_id IN (SELECT condition_id FROM @conditions)  ) '

		set @sql = @sql + '
		 INSERT INTO condition_tabs (condition_id, process_tab_id) 
		 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_tabs.condition_id),
		  (SELECT new_process_tab_id FROM @process_tabs WHERE process_tab_id = condition_tabs.process_tab_id)
		 FROM condition_tabs
		 WHERE condition_id IN (SELECT condition_id FROM @conditions) 
		 OR process_tab_id IN (SELECT process_tab_id FROM @process_tabs) ) '


		set @sql = @sql + '
		IF @source_instance = @target_instance
			BEGIN
			 INSERT INTO condition_user_groups (condition_id, item_id) 
			 ( SELECT  (SELECT new_condition_id FROM @conditions WHERE condition_id = condition_user_groups.condition_id),
			  item_id
			 FROM condition_user_groups
			 WHERE condition_id IN (SELECT condition_id FROM @conditions)  ) 
		 END
		 '

		 set @sql = @sql + '
					UPDATE process_translations set variable = @target_process WHERE instance = @target_instance AND process = @target_process and variable = @source_process ;
		'

		 set @sql = @sql + '

 				DECLARE @ItemCols CURSOR
				SET @ItemCols = CURSOR FAST_FORWARD LOCAL FOR
				SELECT item_column_id, new_item_column_id FROM @item_columns

				OPEN @ItemCols
				FETCH NEXT FROM @ItemCols INTO @item_column_id, @new_item_column_id
				WHILE (@@FETCH_STATUS=0) BEGIN
					UPDATE conditions set condition_json = replace(condition_json, ''"ItemColumnId":'' + CAST(@item_column_id AS NVARCHAR) + '','', ''"ItemColumnId":'' + CAST(@new_item_column_id AS NVARCHAR) + '','') WHERE instance = @target_instance AND process = @target_process
					UPDATE conditions set condition_json = replace(condition_json, ''"ItemColumnId":'' + CAST(@item_column_id AS NVARCHAR) + ''}'', ''"ItemColumnId":'' + CAST(@new_item_column_id AS NVARCHAR) + ''}'') WHERE instance = @target_instance AND process = @target_process
					-- UPDATE process_translations set variable = substring(replace(variable, ''ic_'' + CAST(@item_column_id AS NVARCHAR) + ''_'', ''ic_'' + CAST(@new_item_column_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
					
					UPDATE item_columns 
						SET 
						-- variable = substring(replace(variable, ''ic_'' + CAST(@item_column_id AS NVARCHAR) + ''_'', ''ic_'' + CAST(@new_item_column_id AS NVARCHAR) + ''_''), 1, 50),
						relative_column_id = (select new_item_column_id FROM @item_columns WHERE item_column_id = relative_column_id)
					WHERE instance = @target_instance AND process = @target_process

					--UPDATE item_columns 
					--SET 
					--data_additional2 = (select new_item_column_id FROM @item_columns WHERE item_column_id = CAST(data_additional2 as int))
					--WHERE instance = @target_instance AND process = @target_process and data_type = 11
					
					UPDATE process_portfolio_columns 
					SET 
					archive_item_column_id = (select new_item_column_id FROM @item_columns WHERE item_column_id = archive_item_column_id)
					WHERE process_portfolio_id IN (select process_portfolio_id FROM process_portfolios WHERE instance = @target_instance AND process = @target_process)

				FETCH NEXT FROM  @ItemCols INTO @item_column_id, @new_item_column_id
				END		 '


		 set @sql = @sql + '
 				DECLARE @Conds CURSOR
				SET @Conds = CURSOR FAST_FORWARD LOCAL FOR
				SELECT condition_id, new_condition_id FROM @conditions

				OPEN @Conds
				FETCH NEXT FROM @Conds INTO @condition_id, @new_condition_id
				WHILE (@@FETCH_STATUS=0) BEGIN
					UPDATE conditions set condition_json = replace(condition_json, ''"ConditionId":'' + CAST(@condition_id AS NVARCHAR) + '','', ''"ConditionId":'' + CAST(@new_condition_id AS NVARCHAR) + '','') WHERE instance = @target_instance AND process = @target_process
					UPDATE conditions set condition_json = replace(condition_json, ''"ConditionId":'' + CAST(@condition_id AS NVARCHAR) + ''}'', ''"ConditionId":'' + CAST(@new_condition_id AS NVARCHAR) + ''}'') WHERE instance = @target_instance AND process = @target_process
					-- UPDATE process_translations set variable = substring(replace(variable, ''condition_'' + CAST(@condition_id AS NVARCHAR) + ''_'', ''condition_'' + CAST(@new_condition_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
					-- UPDATE conditions set variable = substring(replace(variable, ''condition_'' + CAST(@condition_id AS NVARCHAR) + ''_'', ''condition_'' + CAST(@new_condition_id AS NVARCHAR) + ''_''), 1, 50) WHERE instance = @target_instance AND process = @target_process
				FETCH NEXT FROM  @Conds INTO @condition_id, @new_condition_id
				END		 '

		 set @sql = @sql + '
			if @copy_data = 1 
			begin

				 DECLARE @items TABLE ( item_id int, new_item_id int ) 
				 DECLARE @item_id int, @new_item_id int;
				
				 disable trigger items_BeforeInsertRow on items;
				 disable trigger items_AFTER_UPDATE on items;

				 MERGE items AS t USING ( SELECT item_id,  @target_instance as instance, @target_process as process, deleted, order_no FROM items WHERE instance = @source_instance AND process = @source_process) AS s ON (1=0)  WHEN NOT MATCHED THEN INSERT ( 
				 instance, process, deleted, order_no ) VALUES ( 
				 instance, process, deleted, order_no )  
				 OUTPUT s.item_id, inserted.item_id INTO @items;'
				 
				 set @sql = @sql + '
 				DECLARE @tableCols CURSOR
				SET @tableCols = CURSOR FAST_FORWARD LOCAL FOR
				SELECT name FROM item_columns WHERE instance = @source_instance AND process = @source_process and data_type in (0,1,2,3,4, 7) AND len(name) > 0

				DECLARE @tableColNames NVARCHAR(MAX) = '+char(39)+''+char(39)+'
				DECLARE @tableColName NVARCHAR(MAX)

				OPEN @tableCols
				FETCH NEXT FROM @tableCols INTO @tableColName
				WHILE (@@FETCH_STATUS=0) BEGIN
					set @tableColNames = @tableColNames + '+char(39)+'u.'+char(39)+' + @tableColName + '+char(39)+' = s.'+char(39)+' + @tableColName + '+char(39)+','+char(39)+'
				FETCH NEXT FROM  @tableCols INTO @tableColName
				END					 
				
				set @tableColNames = substring(@tableColNames, 0, len(@tableColNames)  ) '


				 set @sql = @sql + '
				 declare @sql2 nvarchar(max) = '+char(39)+''+char(39)+'
				 set @sql2 = @sql2 + '+char(39)+'
				update u
				set '+char(39)+' + @tableColNames + '+char(39)+'
				from _'+char(39)+' + @target_instance + '+char(39)+'_'+char(39)+' + @target_process + '+char(39)+' u 
				inner join #NewItems2 i on u.item_id = i.new_item_id
				inner join _'+char(39)+' + @source_instance + '+char(39)+'_'+char(39)+' + @source_process + '+char(39)+' s on i.item_id = s.item_id '+char(39)+' ;

				IF OBJECT_ID('+char(39)+'tempdb..#NewItems2'+char(39)+') IS NOT NULL DROP TABLE #NewItems2 ;
				CREATE table #NewItems2 (item_id int, new_item_id int ) ;
				
				INSERT INTO #NewItems2 (item_id, new_item_id) SELECT item_id, new_item_id FROM @items ;

				IF LEN(@tableColNames) > 0 EXEC (@sql2) ;
				'

				 set @sql = @sql + '
				 enable trigger items_BeforeInsertRow on items ;
				 enable trigger items_AFTER_UPDATE on items ;

				 if @source_instance = @target_instance
				 begin 
					INSERT INTO item_subitems (parent_item_id, item_id, favourite) (SELECT (SELECT new_item_id FROM @items WHERE item_id = i.parent_item_id), item_id, favourite FROM item_subitems i WHERE parent_item_id IN (SELECT item_id FROM @items) )
					INSERT INTO item_subitems (parent_item_id, item_id, favourite) (SELECT parent_item_id, (SELECT new_item_id FROM @items WHERE item_id = i.item_id), favourite FROM item_subitems i WHERE item_id IN (SELECT item_id FROM @items) )

					INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) 
					(SELECT 
						(SELECT new_item_id FROM @items WHERE item_id = i.item_id), 
						(SELECT new_item_column_id FROM @item_columns WHERE item_column_id = i.item_column_id),
						selected_item_id 
						FROM item_data_process_selections i WHERE item_id IN (SELECT item_id FROM @items)
					)

					INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) 
					(SELECT 
						item_id,
						(SELECT new_item_column_id FROM @item_columns WHERE item_column_id = i.item_column_id),
						(SELECT new_item_id FROM @items WHERE item_id = i.selected_item_id )
						FROM item_data_process_selections i WHERE selected_item_id IN (SELECT item_id FROM @items)
					)
				 end
			end
		 '
		 
		 if @is_instance_copy = 1
		 BEGIN 
			set @sql = @sql + ' INSERT INTO #NewItemColumns (item_column_id, new_item_column_id) (SELECT item_column_id, new_item_column_id FROM @item_columns)'
			set @sql = @sql + ' INSERT INTO #NewItems (item_id, new_item_id) (SELECT item_id, new_item_id FROM @items)'
			set @sql = @sql + ' INSERT INTO #NewPortfolios (process_portfolio_id, new_process_portfolio_id) (SELECT process_portfolio_id, new_process_portfolio_id FROM @process_portfolios)'
			set @sql = @sql + ' INSERT INTO #NewTabs (process_tab_id, new_process_tab_id) (SELECT process_tab_id, new_process_tab_id FROM @process_tabs)'
			set @sql = @sql + ' INSERT INTO #NewConditions (condition_id, new_condition_id) (SELECT condition_id, new_condition_id FROM @conditions)'
		 END

		 set @sql = @sql + '
		 END
		 '

	print LEN(@sql)


	exec (@sql)

	set @sql = ' exec app_copy_process_temp ' + char(39) + @source_instance +char(39)+ ',' +char(39)+ @source_process + char(39)+',' +char(39)+ @target_instance + char(39)+','+char(39)+ @target_process +char(39)+',' + cast(@copy_data as nvarchar)
	exec (@sql)

	if @is_instance_copy <> 1 
	BEGIN 
		SET @sql = 'drop procedure app_copy_process_temp'
		exec (@sql)
	END

END

GO
/****** Object:  StoredProcedure [dbo].[app_create_user]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_create_user]
	@instance NVARCHAR(50), @process NVARCHAR(50), @first_name NVARCHAR(255), @last_name NVARCHAR(255), @login NVARCHAR(255), @email NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;
 	
	DECLARE @sql NVARCHAR(MAX) = ''
	DECLARE @itemId INT
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @locale_id NVARCHAR(5)
	DECLARE @date_format NVARCHAR(10)
	DECLARE @first_day_of_week TINYINT
	DECLARE @timezone NVARCHAR(255)

    --Archive user is 0
    DECLARE @BinVar VARBINARY(128);
    SET @BinVar = CONVERT(VARBINARY(128), 0);
    SET CONTEXT_INFO @BinVar;
    
	SET @tableName = '_' + @instance + '_' + @process

	SELECT @locale_id = default_locale_id, @date_format = default_date_format, @first_day_of_week = default_first_day_of_week, @timezone = default_timezone FROM instances WHERE instance = @instance
 
	INSERT INTO items (instance, process) VALUES (@instance, @process); SET @itemId = @@identity;
	SET @sql = 'UPDATE ' + @tableName + ' SET login = '' + @login + '', first_name='' + @first_name + '', last_name='' + @last_name + '', email = '' + @email + '', date_format = '' + @date_format + '', first_day_of_week = '' + @first_day_of_week + '', timezone = '' + @timezone + '', locale_id = '' + @locale_id + '', active = 1 WHERE item_id = ' +  CAST(@itemId AS nvarchar) 
	EXEC (@sql)
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_action]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_action]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10),
	@process nvarchar(50),
	@name NVARCHAR(MAX),
	@process_action_id INT = 0 OUTPUT
AS
BEGIN
	INSERT INTO process_actions (instance, process) VALUES(@instance, @process)
	SET @process_action_id = @@IDENTITY
	DECLARE @langVar NVARCHAR (50) = 'ACTION_' + CAST(@process_action_id AS nvarchar) + '_' + @name
	UPDATE process_actions SET variable = @langVar WHERE instance = @instance AND process = @process

	INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, @langVar, @name, 'en-GB')
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_action_row]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_action_row]	-- Add the parameters for the stored procedure here
	@process_action_id int,
	@process_action_type tinyint,
	@item_column_id int,
	@value_type tinyint,
	@value nvarchar(max)
AS
BEGIN
	INSERT INTO process_action_rows (process_action_id, process_action_type, item_column_id, value_type, value) VALUES(@process_action_id, @process_action_type, @item_column_id, @value_type, @value)
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_archive]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[app_ensure_archive]
	@tableName NVARCHAR(200)
AS
BEGIN
	-- Declaring variables	
	DECLARE @colNames NVARCHAR(MAX);
	DECLARE @colNamesInsert NVARCHAR(MAX);
	DECLARE @colTypes NVARCHAR(MAX);
	DECLARE @colDefs NVARCHAR(MAX);
	DECLARE @pkCols NVARCHAR(MAX);
	DECLARE @pkCols2 NVARCHAR(MAX);

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @viewIndexSql nvarchar(MAX);

	-- Setting action variable
	DECLARE @action nvarchar(20)
	SET @action ='CREATE'
	
	-- Setting table names
	DECLARE @nameArchive NVARCHAR(200);
	DECLARE @nameExpired NVARCHAR(200);
	DECLARE @nameHistoryFunction NVARCHAR(100);

	SET @nameArchive = 'archive_'+@tableName;
	SET @nameExpired = 'old_'+@tableName;
	SET @nameHistoryFunction = 'get_'+@tableName;
	
	-- Setting column names
	DECLARE @archiveId NVARCHAR(100);
	DECLARE @archiveUserId NVARCHAR(100);
	DECLARE @archiveStart NVARCHAR(100);
	DECLARE @archiveEnd NVARCHAR(100);
	DECLARE @archiveType NVARCHAR(100);
		

	SET @archiveId = 'archive_id';
	SET @archiveStart = 'archive_start';
	SET @archiveEnd = 'archive_end';
	SET @archiveUserId = 'archive_userid';
	SET @archiveType = 'archive_type';
		

	DECLARE @defaultValue NVARCHAR(255);
	SET @defaultValue = null;

	DECLARE @archiveColsCount int;
	
	-- Getting list of columns in original table, 
	--	colNames is list of cols, 
	--	colTypes is list of names with datatypes
	--	colDefs is list of colnames encapsulated with isnull and column default value for insert	
		
	IF COL_LENGTH(@tableName,@archiveUserId) IS NULL
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ADD '+@archiveUserId+' int  DEFAULT 0'
			EXEC (@sql)
		END
	ELSE
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN '+@archiveUserId+' int NOT NULL'
			EXEC (@sql)
			
			SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveUserId
			IF @defaultValue IS NULL
			BEGIN
				SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT 0 FOR '+@archiveUserId
				EXEC (@sql)
			END

			EXEC (@sql)
		END

	IF COL_LENGTH(@tableName,@archiveStart) IS NULL
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ADD  '+@archiveStart+' [datetime] DEFAULT getdate()'
			EXEC (@sql)
		END
	ELSE
		BEGIN
			SET @sql = 'ALTER TABLE ['+@tableName+'] ALTER COLUMN  '+@archiveStart+' [datetime]'
			EXEC (@sql)

			SELECT @defaultValue = COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME = @archiveStart
			IF @defaultValue IS NULL
			BEGIN
				SET @sql = 'ALTER TABLE ['+@tableName+'] ADD DEFAULT getdate() FOR '+@archiveStart
				EXEC (@sql)
			END
	END

	SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = @nameArchive
	IF @archiveColsCount = 0 
	BEGIN
			SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
					@colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' + 
					data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
					( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' + 
					COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
				@colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
			FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE table_name = @tablename 

			-- Creating archive table
			SET @sql = '
				CREATE TABLE dbo.'+@nameArchive+'(
					['+@archiveId+'] int IDENTITY(1,1) NOT NULL,
					['+@archiveEnd+'] [datetime] NULL,
					['+@archiveType+'] tinyint NOT NULL,
					'+@colTypes+')  ON [PRIMARY]'
			 EXEC (@sql)

		END
	ELSE
		BEGIN
			SELECT @archiveColsCount=COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)
			IF @archiveColsCount > 0 
				BEGIN		
					SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
							@colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' + 
							data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
							( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' + 
							COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
						@colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
					FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE table_name = @tablename AND column_name NOT IN (SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @nameArchive)
			
					SET @sql = 'ALTER TABLE dbo.'+@nameArchive+' ADD '+@colTypes
					EXEC (@sql)
				END

		SET @action ='ALTER'

		--Drop old archive TRIGGERS
		--IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
		--	SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_UPDATE]'
		--	EXEC (@sql)
		--END

		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_INSERT]')) BEGIN			
			SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_AFTER_INSERT]'
			EXEC (@sql)
		END
		IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_Delete]')) BEGIN			
			SET @sql = 'DROP TRIGGER  [dbo].[' + @tableName + '_Delete]'
			EXEC (@sql)
		END
		--[item_columns_AFTER_UPDATE]
		--[item_columns_AFTER_INSERT]
		--[item_columns_Delete]
	END

		SET @colNames = null
		SET @colTypes = null
		SET @colDefs = null

		SELECT @colNames = COALESCE(@colNames+',','')+'['+column_name+']',
				@colTypes = COALESCE(@colTypes+',','')+'['+column_name+'] ' + 
				data_type + COALESCE('('+CASE WHEN character_maximum_length=-1 THEN 'MAX' ELSE cast(character_maximum_length AS VARCHAR) END+')','') + ' ' +
				( CASE WHEN IS_NULLABLE = 'No' THEN'NOT ' ELSE '' END ) + 'NULL ' + 
				COALESCE('DEFAULT '+COLUMN_DEFAULT,''),
			@colDefs = COALESCE(@colDefs+',','')+ISNULL('ISNULL(['+column_name+'],'+COLUMN_DEFAULT+')','['+column_name+']')
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = @tablename 

		SELECT @colNamesInsert = COALESCE(@colNamesInsert+',','')+'['+column_name+']'
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = @tablename AND (select columnproperty(object_id(@tablename),column_name,'IsIdentity')) = 0 AND column_name <> @archiveUserId

		SELECT @pkCols = COALESCE(@pkCols+',','')+'['+column_name+']'
		from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
			INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
			where 	pk.TABLE_NAME = @tableName
			and	CONSTRAINT_TYPE = 'PRIMARY KEY'
			and	c.TABLE_NAME = pk.TABLE_NAME
			and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
			and pk.table_catalog = (SELECT db_name())

		SELECT @pkCols2 = COALESCE(@pkCols2+'+','')+'['+column_name+']'
		from 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
			INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
			where 	pk.TABLE_NAME = @tableName
			and	CONSTRAINT_TYPE = 'PRIMARY KEY'
			and	c.TABLE_NAME = pk.TABLE_NAME
			and	c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
			and pk.table_catalog = (SELECT db_name())

	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_INSTEAD_OF_INSERT]')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END

	SET @sql =  @action + ' TRIGGER [dbo].['+@tableName+'_INSTEAD_OF_INSERT]
	ON [dbo].['+@tableName+']
	INSTEAD OF INSERT AS
	
	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

	INSERT INTO [dbo].['+@tableName+'] ('+@colNamesInsert+', '+@archiveUserId+') 
	SELECT '+@colNamesInsert+' 
	, @user_id
	FROM inserted
	'
	EXEC (@sql)


	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_AFTER_UPDATE]')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END

	-- Creating UPDATE triggers
	SET @sql = @action + ' TRIGGER [dbo].['+@tableName+'_AFTER_UPDATE]
	ON [dbo].['+@tableName+']
	AFTER UPDATE
	AS

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

    UPDATE [dbo].['+@tableName+']
    SET ' + @archiveStart + '= GETDATE(),
    ' + @archiveUserId + '=  @user_id
    WHERE ' + @pkCols2 + ' IN (SELECT DISTINCT ' + @pkCols2 + ' FROM Inserted)

	INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
	SELECT '+@colNames+',
		'+@archiveEnd+' = getdate(),
		'+@archiveType+' = 1
	FROM deleted	
	'
	EXEC (@sql)

	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@tableName+'_DELETE]')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END
		
	-- Creating Delete Trigger 
	SET @sql =  @action +' TRIGGER [dbo].['+@tableName+'_DELETE]
		ON [dbo].['+@tableName+']
		FOR DELETE
		AS		
			INSERT INTO '+@nameArchive+'('+@colNames+','+@archiveEnd+', '+@archiveType+')
			SELECT '+@colNames+',
				'+@archiveEnd+' = getdate(),
				'+@archiveType+' = 1
			FROM deleted
			
			DECLARE @user_id int;
			select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));

			INSERT INTO '+@nameArchive+'('+REPLACE(@colNames,'['+@archiveUserId+'],','')+','+@archiveEnd+', '+@archiveType+', '+@archiveUserId+')
			SELECT '+REPLACE(@colNames,'['+@archiveUserId+'],','')+',
				'+@archiveEnd+' = getdate(),
				'+@archiveType+' = 2,
				'+@archiveUserId+' = @user_id
			FROM deleted
			'
	EXEC (@sql)


	-- Creating or altering history function, depends if it exist

	IF Exists(select * from dbo.sysobjects where id = OBJECT_ID('[dbo].['+@nameHistoryFunction+']')) BEGIN			
		SET @action ='ALTER'
	END
	ELSE begin
		SET @action ='CREATE'
	END

	SET @sql = @action +' FUNCTION  [dbo].['+@nameHistoryFunction+']( @d datetime )
		RETURNS @rtab TABLE ('+@colTypes+')
		AS
		BEGIN	
			INSERT @rtab
				SELECT '+@colNames+'
				FROM '+@nameArchive+' WHERE '+@archiveId+' IN (
					SELECT MAX('+@archiveId+')
					FROM '+@nameArchive+'
					WHERE '+@archiveStart+' < @d AND archive_end > @d AND ' + @pkCols2 + ' NOT IN
					(SELECT ' + @pkCols2 + ' FROM  '+@tableName+'  WHERE '+@archiveStart+' < @d  )
					GROUP BY ' + @pkCols + '
					)
				UNION 
				SELECT '+@colNames+' 
				FROM '+@tableName+' 
				WHERE '+@archiveStart+' < @d  
			RETURN
		END'	
	EXEC (@sql)		
END




GO
/****** Object:  StoredProcedure [dbo].[app_ensure_archive_all]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_archive_all]
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @GetTables CURSOR

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT table_name FROM INFORMATION_SCHEMA.TABLES  WHERE table_type = 'BASE TABLE' and substring(table_name, 0, 9) <> 'archive_'  and table_name <> 'sysdiagrams'
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

	WHILE (@@FETCH_STATUS=0) BEGIN
		IF @TableName not in ('attachments_data', 'instance_configurations', 'items') BEGIN
			print 'ensure archive for ' + @TableName
			SET @sql = 'EXEC app_ensure_archive '''+@TableName+''''
			print @sql
			EXEC (@sql)
		END
			FETCH NEXT FROM @GetTables INTO @TableName
	END
	CLOSE @GetTables

END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_column]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[app_ensure_column]
	@instance NVARCHAR(10), @process NVARCHAR(50), @column_name NVARCHAR(50), @container_id INT, 
	@data_type TINYINT, @system_column TINYINT, @required INT, @inputMethod INT, @inputName NVARCHAR(255), @portfolio_id INT, @use_in_select_result TINYINT, @dataAdditional1 NVARCHAR(MAX) = null, @dataAdditional2 NVARCHAR(MAX) = null, @use_in_filter INT = 0, @item_column_id INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @item_column_id = 0
	SELECT @item_column_id = item_column_id FROM item_columns WHERE instance = @instance AND name = @column_name AND process = @process
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF (@item_column_id = 0)
	BEGIN
		INSERT INTO item_columns (instance, process, name, data_type, system_column, variable, input_method, input_name, data_additional, data_additional2) values (@instance, @process, @column_name, @data_type, @system_column, UPPER(@process + '_' + @column_name), @inputMethod, @inputName, @dataAdditional1, @dataAdditional2)
		SELECT @item_column_id = @@IDENTITY
		
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND variable = @column_name) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process,UPPER(@column_name), @column_name, 'en-GB')
		END

		IF (@container_id > 0) BEGIN
			INSERT INTO process_container_columns (process_container_id, item_column_id, required) values(@container_id, @item_column_id, @required)
		END
	END

	IF (@portfolio_id > 0)
	BEGIN
		IF (SELECT COUNT(item_column_id) FROM process_portfolio_columns WHERE process_portfolio_id = @portfolio_id AND item_column_id = @item_column_id) = 0 
		BEGIN
			INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, use_in_filter)
			VALUES(
				@portfolio_id,
				@item_column_id,
				dbo.items_GetOrderEnd(@instance, @process),
				@use_in_select_result,
				@use_in_filter
			)
		END
	END
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_condition]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_condition]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10),
	@process nvarchar(50),
	@process_group_id INT,
	@process_portfolio_id INT,
	@name NVARCHAR(MAX) = 'ALL',
	@condition_json NVARCHAR(MAX) = 'null',
	@condition_id INT = 0 OUTPUT
AS
BEGIN
	DECLARE @conditionId INT

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, @process, @name, @condition_json)
	SET @conditionId = @@IDENTITY
	set @condition_id = @conditionId
	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	--INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write')
	--INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')

	IF @process_group_id > 0 BEGIN
		INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, @process_group_id)
	END
	IF @process_portfolio_id > 0 BEGIN
		INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, @process_portfolio_id)
	END
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_conditions]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_conditions]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @conditionId INT

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, 'user', 'ALL', 'null')
	SET @conditionId = @@IDENTITY
	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, (SELECT TOP 1 process_group_id FROM process_groups WHERE instance=@instance AND process = 'user'))
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, (SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE instance=@instance AND process = 'user'))

	INSERT INTO conditions (instance, process, variable, condition_json) VALUES(@instance, 'user_group', 'ALL', 'null')
	SET @conditionId = @@IDENTITY
	INSERT INTO condition_features (condition_id, feature) VALUES (@conditionId, 'meta')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@conditionId, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@conditionId, (SELECT TOP 1 process_group_id FROM process_groups WHERE instance=@instance AND  process = 'user_group'))
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@conditionId, (SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE instance=@instance AND process = 'user_group'))
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_configuration]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_configuration]
	@instance NVARCHAR(10), @process NVARCHAR(50), @group_name NVARCHAR(255), @category NVARCHAR(255), @name NVARCHAR(255), @value NVARCHAR(255), @server INT, @domain NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	DECLARE @instance_configuration_id INT;
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM instance_configurations WHERE instance = @instance AND process = @process AND group_name = @group_name AND category = @category AND name = @name) > 0) BEGIN
		SELECT @instance_configuration_id = instance_configuration_id FROM instance_configurations  WHERE instance = @instance AND process = @process AND group_name = @group_name AND category = @category AND name = @name
		UPDATE instance_configurations SET value = @value, server=@server, domain=@domain WHERE instance_configuration_id = @instance_configuration_id 
	END ELSE BEGIN
		INSERT INTO instance_configurations (instance, process, group_name, category, name, value, server, domain) VALUES (@instance, @process, @group_name, @category, @name, @value, @server, @domain);
		SELECT @instance_configuration_id = @@IDENTITY; --SCOPE_IDENTITY();
	END
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_container]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_container]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @tab_id INT, @grid TINYINT, @container_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @container_id = process_container_id FROM process_containers WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		INSERT INTO process_containers (instance, process, variable) VALUES (@instance, @process, @variable);
		SELECT @container_id = @@IDENTITY;
		IF (@tab_id > 0)
		BEGIN
			INSERT INTO process_tab_containers (process_tab_id, process_container_id, order_no, container_side) VALUES (@tab_id, @container_id, 1, @grid);
		END
	END
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_group]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_group]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @group_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_groups WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @group_id = process_group_id FROM process_groups WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		INSERT INTO process_groups (instance, process, variable, order_no) VALUES (@instance, @process, @variable, 1);
		SELECT @group_id = @@IDENTITY; --SCOPE_IDENTITY();
	END
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_instance_menu]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_instance_menu]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @instanceMenuId INT, 
	@instanceMenuId2 INT, 
	@userPortfolioId INT,
	@userGroupPortfolioId INT,
	@sql AS NVARCHAR(MAX)

	SELECT TOP 1 @userPortfolioId = process_portfolio_id FROM process_portfolios WHERE instance = @instance AND process = 'user'
	SELECT TOP 1 @userGroupPortfolioId = process_portfolio_id  FROM process_portfolios WHERE instance = @instance AND process = 'user_group'
	
	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, null, 'SETTINGS', 'V', null, '{}')
	SET @instanceMenuId = @@IDENTITY

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'CONFIGURATION', 'V', 'configuration.client', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'MENU', 'W', 'instanceMenu', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)
	
	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'EVENT_LOG', 'X', 'eventLog', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)
	
	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'CALENDARS', 'Y', 'calendar', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'LISTS', 'Z', 'lists', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'DOCUMENTATION', '0', 'documentation', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, null, 'PROCESSES', 'W', null, '{}')
	SET @instanceMenuId = @@IDENTITY

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'CUSTOMER_PROCESSES', 'V', 'processes', '{"processType":0}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)
 
	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'SUPPORT_PROCESSES', 'W', 'processes', '{"processType":1}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'DATA_PROCESSES', 'X', 'processes', '{"processType":2}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)
	
	
	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, null, 'USERS_AND_USER_GROUPS', 'X', null, '{}')
	SET @instanceMenuId = @@IDENTITY

    INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'NEW_USER', 'V', 'process.user.new', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)
	
	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'USERS', 'W', 'portfolio', '{"process":"user","portfolioId":' + CAST(@userPortfolioId AS nvarchar) + '}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'USER_GROUPS', 'X', 'portfolio', '{"process":"user_group","portfolioId":' + CAST(@userGroupPortfolioId AS nvarchar) + '}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

	INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'NEW_USER_GROUP', 'Y', 'process.userGroup.new', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)
	
	
    INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, null, 'EMAILS_AND_NOTIFICATIONS', 'Y', null, '{}')
	SET @instanceMenuId = @@IDENTITY

    INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'EMAILS', 'V', 'emails', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

    INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'NOTIFICATIONS', 'W', 'notifications', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)

    INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, @instanceMenuId, 'BULLETINS', 'X', 'bulletins', '{}')
	SET @instanceMenuId2 = @@IDENTITY
	SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group WHERE usergroup_name = ''Administrators'')); '
	EXEC (@sql)
	
	--{"processType":0}	
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_list]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_list]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @system_process INT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT COUNT(*) FROM processes WHERE instance = @instance AND process = @process) = 0)
	BEGIN
		INSERT INTO processes (instance, process, variable, process_type, list_process) VALUES (@instance, @process, @variable, @system_process, 1);
	END
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_portfolio]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_portfolio]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @portfolio_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_portfolios WHERE instance = @instance AND process = @process AND variable = @variable) = 0) BEGIN
		INSERT INTO process_portfolios (instance, process, variable) VALUES (@instance, @process, @variable);
		SET @portfolio_id = @@IDENTITY;
	END ELSE BEGIN
		SELECT @portfolio_id = process_portfolio_id FROM process_portfolios WHERE instance = @instance AND process = @process AND variable = @variable;
	END
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @process_type INT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT COUNT(*) FROM processes WHERE instance = @instance AND process = @process) = 0)
	BEGIN
		INSERT INTO processes (instance, process, variable, process_type) VALUES (@instance, @process, @variable, @process_type);
		IF ((SELECT COUNT(*) FROM process_translations WHERE instance = @instance AND process = @process AND variable = @variable) = 0)
		BEGIN
			INSERT INTO process_translations (instance, process, variable, translation, code) VALUES (@instance, @process, @variable, @variable, 'en-GB')
		END

	END
END

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_allocation]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_allocation]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'allocation'
	DECLARE @tableName NVARCHAR(50) =  '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create columns
	EXEC app_ensure_column @instance, @process, 'status', 1, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'process_item_id', 1, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'creator_item_id', 1, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'task_item_id', 1, 0, 1, 1, 0, null, 0, 0
END;


GO

/****** Object:  StoredProcedure [dbo].[app_ensure_process_comment]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[app_ensure_process_comment]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'comment'
	DECLARE @tableName NVARCHAR(50) = ''

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create conditions
	EXEC app_ensure_condition @instance, @process, 0, 0
	
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'COMMENTS', @portfolio_id = @p1 OUTPUT

	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'comment_owner', 0, 5, 1, 1, 0, null,  0, 0, 'user', null, @item_column_id = @i1 OUTPUT
	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'comment_published', 1, 1, 1, 1, 0, null,  0, 0, null, null, @item_column_id = @i2 OUTPUT

	EXEC app_ensure_column @instance, @process, 'comment_text', 0, 0, 1, 1, 0, null,  0, 0

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'comment_created_date', 0, 13, 1, 1, 0, null,  0, 0,  null, null, @item_column_id = @i3 OUTPUT
	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'comment_published_date', 0, 13, 1, 1, 0, null,  0, 0, null, null, @item_column_id = @i4 OUTPUT
	
	--Create conditions
	DECLARE @c1 AS NVARCHAR(MAX)

	DECLARE @cId1 INT
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i2 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'READ', @c1, @condition_id = @cId1 OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId1, 'read')

	DECLARE @cId2 INT
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":3,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"UserColumnTypeId":2,"ComparisonTypeId":3},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i2 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'WRITE', @c1, @condition_id = @cId2 OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId2, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId2, 'write')
	
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'CommentsPortfolioId', @p1, 0, null

	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 

	--Create actions
	DECLARE @actionId INT
	exec app_ensure_action @instance, @process, 'NEW_ROW', @process_action_id = @actionId OUTPUT
	exec app_ensure_action_row @actionId, 0, @i2, 0, '0'
	exec app_ensure_action_row @actionId, 0, @i1, 2, null
	exec app_ensure_action_row @actionId, 0, @i3, 1, null

	update processes set new_row_action_id = @actionId WHERE instance = @instance and process = @process

	DECLARE @actionId2 INT
	exec app_ensure_action @instance, @process, 'PUBLISH_COMMENT', @process_action_id = @actionId2 OUTPUT
	exec app_ensure_action_row @actionId2, 0, @i2, 0, '1'
	exec app_ensure_action_row @actionId2, 0, @i4, 1, null

	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'CommentsPublishActionId', @actionId2, 0, null
END;

GO

/****** Object:  StoredProcedure [dbo].[app_ensure_process_notification]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_notification]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'notification'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1
	
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'EMAILS', @portfolio_id = @p1 OUTPUT

	DECLARE @p2 INT
	EXEC app_ensure_portfolio @instance,@process,'NOTIFICATONS', @portfolio_id = @p2 OUTPUT

	DECLARE @p3 INT
	EXEC app_ensure_portfolio @instance,@process,'BULLETINS', @portfolio_id = @p3 OUTPUT

	--Create configurations
	EXEC app_ensure_configuration @instance, null, null, 'Notifications', 'EmailsPortfolioId', @p1, 0, null
	EXEC app_ensure_configuration @instance, null, null, 'Notifications', 'NotificationsPortfolioId', @p2, 0, null
	EXEC app_ensure_configuration @instance, null, null, 'Notifications', 'BulletinsPortfolioId', @p3, 0, null

	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'type',			  0, 1, 1, 1, 0, null, @p1, 0, @item_column_id = @i1 OUTPUT

	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'name_variable',  0, 0, 1, 1, 0, null, @p1, 0, @item_column_id = @i2 OUTPUT

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'title_variable', 0, 0, 1, 1, 0, null, @p1, 0, @item_column_id = @i3 OUTPUT

	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'body_variable',  0, 0, 1, 1, 0, null, @p1, 0, @item_column_id = @i4 OUTPUT

	--bulletins cols
	EXEC app_ensure_column @instance, @process, 'sender',	0, 5, 1, 1, 0, null, @p3, 0, 'user'
	EXEC app_ensure_column @instance, @process, 'created',	0, 3, 1, 1, 0, null,  @p3, 0
	EXEC app_ensure_column @instance, @process, 'language', 0, 0, 1, 1, 0, null,  @p3, 0
	EXEC app_ensure_column @instance, @process, 'notify',	0, 1, 1, 1, 0, null,  @p3, 0
	EXEC app_ensure_column @instance, @process, 'expiration', 0, 3, 1, 1, 0, null,  @p3, 0
	EXEC app_ensure_column @instance, @process, 'modified',	0, 3, 1, 0, 0, null,  @p3, 0
	EXEC app_ensure_column @instance, @process, 'status',	0, 1, 1, 1, 0, null,  @p3, 0
	EXEC app_ensure_column @instance, @process, 'delta',	0, 4, 1, 1, 0, null, @p3, 0
	
	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 

	--Create conditions
	DECLARE @c1 AS NVARCHAR(MAX)
    DECLARE @con1 AS INT
	
	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'EMAILS', @c1, @condition_id = @con1 OUTPUT
	INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'read')
    INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write')


	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p2, 'NOTIFICATIONS', @c1, @condition_id = @con1 OUTPUT
    INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'read')
    INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write')
	

	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p3, 'BULLETINS', @c1, @condition_id = @con1 OUTPUT
    INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'read')
    INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write')
	

	--Portfolio columns
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i1)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i2)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i3)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i4)

	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i1)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i2)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i3)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i4)
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_notification_history]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_notification_history]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'notification_history'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1
	
	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'SENDED_EMAILS', @portfolio_id = @p1 OUTPUT

	DECLARE @p2 INT
	EXEC app_ensure_portfolio @instance,@process,'UNREAD_NOTIFICATONS', @portfolio_id = @p2 OUTPUT

	DECLARE @p3 INT
	EXEC app_ensure_portfolio @instance,@process,'ALL_NOTIFICATIONS', @portfolio_id = @p3 OUTPUT

	--Create configurations
	EXEC app_ensure_configuration @instance, null, null, 'Notifications', 'SendedEmailsPortfolioId', @p1, 0, null
	EXEC app_ensure_configuration @instance, null, null, 'Notifications', 'UnreadNotificationsPortfolioId', @p2, 0, null
	EXEC app_ensure_configuration @instance, null, null, 'Notifications', 'AllNotificationsPortfolioId', @p3, 0, null

	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'type',			  0, 1, 1, 1, 0, null, @p1, 0, @item_column_id = @i1 OUTPUT

	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'notification_item_id',  0, 1, 1, 1, 0, null, @p1, 0, @item_column_id = @i2 OUTPUT

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'title', 0, 4, 1, 1, 0, null, @p1, 0, @item_column_id = @i3 OUTPUT

	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'body',  0, 4, 1, 1, 0, null, @p1, 0, @item_column_id = @i4 OUTPUT
	
	DECLARE @i5 INT
	EXEC app_ensure_column @instance, @process, 'created',  0, 3, 1, 1, 0, null, @p1, 0, @item_column_id = @i5 OUTPUT

	DECLARE @i6 INT
	EXEC app_ensure_column @instance, @process, 'is_read',  0, 1, 1, 1, 0, null, @p1, 0, @item_column_id = @i6 OUTPUT

	DECLARE @i7 INT
	EXEC app_ensure_column @instance, @process, 'receiver_item_id',  0, 1, 1, 1, 0, null, @p1, 0, @item_column_id = @i7 OUTPUT

	DECLARE @i8 INT
	EXEC app_ensure_column @instance, @process, 'from_address',  0, 0, 1, 1, 0, null, @p1, 0, @item_column_id = @i8 OUTPUT

	DECLARE @i9 INT
	EXEC app_ensure_column @instance, @process, 'to_address',  0, 0, 1, 1, 0, null, @p1, 0, @item_column_id = @i9 OUTPUT

	DECLARE @i10 INT
	EXEC app_ensure_column @instance, @process, 'cc_address',  0, 0, 1, 1, 0, null, @p1, 0, @item_column_id = @i10 OUTPUT

	update item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process 

	--Create conditions
	DECLARE @c1 AS NVARCHAR(MAX)

	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p1, 'SENDED_EMAILS', @c1

	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}, {"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i6 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}, {"ConditionTypeId":3,"Condition":{"ItemColumnId":' + CAST(@i7 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1","UserColumnTypeId":2},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p2, 'UNREAD_NOTIFICATONS', @c1

	SET @c1 = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i1 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}, {"ConditionTypeId":3,"Condition":{"ItemColumnId":' + CAST(@i7 AS NVARCHAR) + ',"ComparisonTypeId":3,"Value":"1","UserColumnTypeId":2},"OperatorId":1}]},"OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, @p3, 'ALL_NOTIFICATIONS', @c1

	--Portfolio columns
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i1)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i2)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i3)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i4)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i5)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i6)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i7)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i8)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i9)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p2, @i10)

	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i1)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i2)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i3)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i4)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i5)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i6)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i7)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i8)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i9)
	INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id) VALUES (@p3, @i10)
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_notification_seen]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_notification_seen]    -- Add the parameters for the stored procedure here
    @instance nvarchar(10)
AS
BEGIN
    DECLARE @process NVARCHAR(50) = 'notification_seen'

    --Archive user is 0
    DECLARE @BinVar varbinary(128);
    SET @BinVar = CONVERT(varbinary(128), 0);
    SET CONTEXT_INFO @BinVar;

    --Create process
    EXEC app_ensure_process @instance, @process, @process, 1
    
    EXEC dbo.app_ensure_column @instance,@process, 'notification_item_id', 0, 0, 1, 0, 0, NULL, 0, 0
    EXEC dbo.app_ensure_column @instance,@process, 'user_id', 0, 0, 1, 0, 0, NULL, 0, 0
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_notification_tag_links]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_notification_tag_links]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'notification_tag_links'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1
	
	EXEC dbo.app_ensure_column @instance,@process,'json_tag_id', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'item_column_id', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'tag_type', 0, 1, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'process_name', 0, 0, 1, 0, 0, NULL, 0, 0
		EXEC dbo.app_ensure_column @instance,@process,'view_state', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process,'link_param', 0, 0, 1, 0, 0, NULL, 0, 0
END;

GO
/****** Object:  StoredProcedure [dbo].[app_ensure_process_notification_tags]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_notification_tags]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'notification_tags'

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_list @instance, @process, @process, 1
	
	EXEC dbo.app_ensure_column @instance,@process, 'list_item', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol', 0, 0, 1, 0, 0, NULL, 0, 0
	EXEC dbo.app_ensure_column @instance,@process, 'list_symbol_fill', 0, 0, 1, 0, 0, NULL, 0, 0
END;

GO

/****** Object:  StoredProcedure [dbo].[app_ensure_process_task_status]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_task_status]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	DECLARE @process NVARCHAR(10) = 'task_status'
	DECLARE @combine TINYINT = 0

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create columns
	EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 1, 1, 0, null, 0, 0
	EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'description', 0, 0, 1, 0, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'is_done', 0, 1, 1, 1, 0, null,  0, 1
END;

GO

/****** Object:  StoredProcedure [dbo].[app_ensure_tab]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_tab]
	@instance NVARCHAR(10), @process NVARCHAR(50), @variable NVARCHAR(255), @group_id INT, @tab_id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF ((SELECT count(*) FROM process_tabs WHERE instance = @instance AND process = @process AND variable = @variable) > 0) BEGIN
		SELECT @tab_id = process_tab_id FROM process_tabs WHERE instance = @instance AND process = @process AND variable = @variable
	END ELSE BEGIN
		INSERT INTO process_tabs (instance, process, variable, order_no) VALUES (@instance, @process, @variable, 1);
		SELECT @tab_id = @@IDENTITY; --SCOPE_IDENTITY();

		INSERT INTO process_group_tabs (process_group_id, process_tab_id, order_no) VALUES (@group_id, @tab_id, 1);
	END
END

GO


/****** Object:  StoredProcedure [dbo].[app_remove_archive]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_remove_archive]
	@TableName NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @GetTables CURSOR
	DECLARE @GetDFS CURSOR
	DECLARE @ConstraintName NVARCHAR(MAX);
	DECLARE @colsCount INT;

		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = 'archive_'+@TableName
		IF @colsCount > 0 
		BEGIN		
			print 'remove archive for ' + @TableName
			SET @sql = 'DROP table archive_'+@TableName
			print @sql
			EXEC (@sql)
		END

		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_AFTER_UPDATE'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP TRIGGER '+@TableName+'_AFTER_UPDATE'
			print @sql
			EXEC (@sql)
		END
		
		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_DELETE'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP TRIGGER '+@TableName+'_DELETE'
			print @sql
			EXEC (@sql)
		END
		
		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_INSTEAD_OF_INSERT'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP TRIGGER '+@TableName+'_INSTEAD_OF_INSERT'
			print @sql
			EXEC (@sql)
		END
		
		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = 'get_'+@TableName
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP FUNCTION get_'+@TableName
			print @sql
			EXEC (@sql)
		END

		SELECT
			@ConstraintName = default_constraints.name
		FROM 
			sys.all_columns
				INNER JOIN
			sys.tables
				ON all_columns.object_id = tables.object_id
				INNER JOIN 
			sys.schemas
				ON tables.schema_id = schemas.schema_id
				INNER JOIN
			sys.default_constraints
				ON all_columns.default_object_id = default_constraints.object_id
		WHERE 
				schemas.name = 'dbo'
			AND tables.name = @TableName
			AND all_columns.name = 'archive_userid'

		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @ConstraintName
		IF @colsCount > 0 
		BEGIN		
				SET @sql = 'ALTER TABLE '+@TableName+'  DROP CONSTRAINT ' + @ConstraintName
				print @sql
				EXEC (@sql)
		END

		SELECT @colsCount=COUNT(column_name) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TableName AND column_name = 'archive_userid'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP COLUMN archive_userid'
			print @sql
			EXEC (@sql)
		END



		SELECT
			@ConstraintName = default_constraints.name
		FROM 
			sys.all_columns
				INNER JOIN
			sys.tables
				ON all_columns.object_id = tables.object_id
				INNER JOIN 
			sys.schemas
				ON tables.schema_id = schemas.schema_id
				INNER JOIN
			sys.default_constraints
				ON all_columns.default_object_id = default_constraints.object_id
		WHERE 
				schemas.name = 'dbo'
			AND tables.name = @TableName
			AND all_columns.name = 'archive_start'

		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @ConstraintName
		IF @colsCount > 0 
		BEGIN		
				SET @sql = 'ALTER TABLE '+@TableName+'  DROP CONSTRAINT ' + @ConstraintName
				print @sql
				EXEC (@sql)
		END

		SELECT @colsCount=COUNT(column_name) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TableName AND column_name = 'archive_start'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP COLUMN archive_start'
			print @sql
			EXEC (@sql)
		END

		FETCH NEXT FROM @GetTables INTO @TableName
	END
	CLOSE @GetTables


GO
/****** Object:  StoredProcedure [dbo].[app_remove_archive_all]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_remove_archive_all]
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @GetTables CURSOR
	DECLARE @GetDFS CURSOR
	DECLARE @ConstraintName NVARCHAR(MAX);
	DECLARE @colsCount INT;

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT table_name FROM INFORMATION_SCHEMA.TABLES  WHERE table_type = 'BASE TABLE' and substring(table_name, 0, 9) <> 'archive_'  and table_name <> 'sysdiagrams'
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

	WHILE (@@FETCH_STATUS=0) BEGIN
		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = 'archive_'+@TableName
		IF @colsCount > 0 
		BEGIN		
			print 'remove archive for ' + @TableName
			SET @sql = 'DROP table archive_'+@TableName
			print @sql
			EXEC (@sql)
		END

		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_AFTER_UPDATE'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP TRIGGER '+@TableName+'_AFTER_UPDATE'
			print @sql
			EXEC (@sql)
		END
		
		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_DELETE'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP TRIGGER '+@TableName+'_DELETE'
			print @sql
			EXEC (@sql)
		END
		
		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @TableName+'_INSTEAD_OF_INSERT'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP TRIGGER '+@TableName+'_INSTEAD_OF_INSERT'
			print @sql
			EXEC (@sql)
		END
		
		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = 'get_'+@TableName
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'DROP FUNCTION get_'+@TableName
			print @sql
			EXEC (@sql)
		END

		SELECT
			@ConstraintName = default_constraints.name
		FROM 
			sys.all_columns
				INNER JOIN
			sys.tables
				ON all_columns.object_id = tables.object_id
				INNER JOIN 
			sys.schemas
				ON tables.schema_id = schemas.schema_id
				INNER JOIN
			sys.default_constraints
				ON all_columns.default_object_id = default_constraints.object_id
		WHERE 
				schemas.name = 'dbo'
			AND tables.name = @TableName
			AND all_columns.name = 'archive_userid'

		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @ConstraintName
		IF @colsCount > 0 
		BEGIN		
				SET @sql = 'ALTER TABLE '+@TableName+'  DROP CONSTRAINT ' + @ConstraintName
				print @sql
				EXEC (@sql)
		END

		SELECT @colsCount=COUNT(column_name) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TableName AND column_name = 'archive_userid'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP COLUMN archive_userid'
			print @sql
			EXEC (@sql)
		END



		SELECT
			@ConstraintName = default_constraints.name
		FROM 
			sys.all_columns
				INNER JOIN
			sys.tables
				ON all_columns.object_id = tables.object_id
				INNER JOIN 
			sys.schemas
				ON tables.schema_id = schemas.schema_id
				INNER JOIN
			sys.default_constraints
				ON all_columns.default_object_id = default_constraints.object_id
		WHERE 
				schemas.name = 'dbo'
			AND tables.name = @TableName
			AND all_columns.name = 'archive_start'

		SELECT @colsCount=COUNT(name) FROM sysobjects WHERE name = @ConstraintName
		IF @colsCount > 0 
		BEGIN		
				SET @sql = 'ALTER TABLE '+@TableName+'  DROP CONSTRAINT ' + @ConstraintName
				print @sql
				EXEC (@sql)
		END

		SELECT @colsCount=COUNT(column_name) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TableName AND column_name = 'archive_start'
		IF @colsCount > 0 
		BEGIN		
			SET @sql = 'ALTER TABLE '+@TableName+'  DROP COLUMN archive_start'
			print @sql
			EXEC (@sql)
		END

		FETCH NEXT FROM @GetTables INTO @TableName
	END
	CLOSE @GetTables

END

GO
/****** Object:  StoredProcedure [dbo].[app_replace_dates]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[app_replace_dates]
	
as
begin
	set nocount on;

	declare @sql nvarchar(max);
	declare @gettables cursor

	--get tables
	set @gettables = cursor fast_forward local for 
	select replace(lower(replace(replace(replace(m.definition, 'alter procedure', 'alter procedure'), 'alter function', 'alter function'), 'alter trigger', 'alter trigger')), 'getutcdate', 'getutcdate') as sql from sys.objects o
	inner join sys.sql_modules m  on o.object_id=m.object_id
	-- where o.type_desc = 'sql_trigger' and 
	where lower(m.definition) like '%getutcdate%' -- order by name
	
	open @gettables
	fetch next from @gettables into @sql

	while (@@fetch_status=0) begin
		print @sql
		exec (@sql)
		fetch next from @gettables into @sql
	end
	close @gettables

end

GO
/****** Object:  StoredProcedure [dbo].[app_set_archive_user_id]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[app_set_archive_user_id]
	@UserId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), @UserId);
	SET CONTEXT_INFO @BinVar;
END

GO
/****** Object:  StoredProcedure [dbo].[Calendar_AddDateRange]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calendar_AddDateRange] 
	(
	@calendar_id INT, @calendar_event_id INT = 0, @start_date DATETIME, @end_date DATETIME, @typeid TINYINT, @name NVARCHAR(50) = ''
	)
AS

BEGIN
	DECLARE @i INT
	DECLARE @l INT
	DECLARE @d DATETIME
	SET @i = DATEDIFF(DAY,@start_date,@end_date)
	SET @l = 0
	
	WHILE (@l <= @i)
	BEGIN
		SET @d = DATEADD(DAY,@l,@start_date);
		
		/* Add National Holiday */
		IF (@typeid = 1)
		BEGIN
			UPDATE calendar_data SET holiday=1 WHERE calendar_id = @calendar_id AND event_date = @d
			IF @@ROWCOUNT = 0 INSERT INTO calendar_data (calendar_id, calendar_event_id, event_date, holiday) VALUES (@calendar_id, 0, @d, 1)

			UPDATE calendar_data_attributes SET name=@name WHERE event_date = @d AND type_id = 1 AND calendar_id = @calendar_id
			IF @@ROWCOUNT = 0 INSERT INTO calendar_data_attributes (calendar_id,event_date, type_id, name) VALUES (@calendar_id,@d,1,@name)
		END
		
		/* Add Other Holiday */
		ELSE IF  (@typeid = 2)
		BEGIN
			UPDATE calendar_data SET notwork=1, calendar_event_id=@calendar_event_id WHERE calendar_id = @calendar_id AND event_date = @d
			IF @@ROWCOUNT = 0 INSERT INTO calendar_data (calendar_id, calendar_event_id, event_date) VALUES (@calendar_id, @calendar_event_id, @d)
		END
		
		/* Add Work */
		ELSE IF (@typeid = 3)
		BEGIN
			UPDATE calendar_data SET notwork=0 WHERE calendar_id = @calendar_id AND event_date = @d
			IF @@ROWCOUNT = 0 INSERT INTO calendar_data (calendar_id, calendar_event_id, event_date, notwork) VALUES (@calendar_id, 0, @d, 0)
		
			UPDATE calendar_data_attributes SET name=@name WHERE event_date = @d AND type_id = 3 AND calendar_id = @calendar_id
			IF @@ROWCOUNT = 0 INSERT INTO calendar_data_attributes (calendar_id,event_date, type_id, name) VALUES (@calendar_id,@d,3,@name)					
		END
		
		SET @l = @l + 1
	END
END;

GO
/****** Object:  StoredProcedure [dbo].[Calendar_AddDaysOff]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calendar_AddDaysOff]
	(
	@calendar_id INT, @start_date DATETIME, @end_date DATETIME, 
	@weekdays VARCHAR(15)
	)
AS

BEGIN
	SET NOCOUNT ON;
	DECLARE @weekday INT 
	DECLARE @day INT
	DECLARE @i INT;	DECLARE @p INT
	DECLARE @n INT;	DECLARE @l INT
	DECLARE @d DATETIME;
	
	SET @weekday = DATEPART(dw, @start_date)-1;IF @weekday = 0 SET @weekday = 7
	SET @p = 0;SET @n = 1
	
	/* Remove daysoff that do not have an event or national holiday */
	DELETE FROM calendar_data WHERE calendar_event_id is null AND holiday = 0 AND notwork = 1 AND calendar_id = @calendar_id AND event_date >= @start_date AND event_date <= @end_date
	 
	/* Mark other days to not have dayoff */
	UPDATE calendar_data SET dayoff = 0 WHERE calendar_id = @calendar_id AND event_date >= @start_date AND event_date <= @end_date
	
	IF len(@weekdays) > 0
	BEGIN 
		WHILE (@n > 0)
		BEGIN
			/* Split weekdays string and loop through */
			SET @n = CHARINDEX(',', @weekdays, @p + 1)
			SET @l = CASE WHEN @n > 0 THEN @n ELSE LEN(@weekdays) + 1 END - @p - 1
			SET @day = CONVERT(INT,SUBSTRING(@weekdays, @p + 1, @l))
			SET @p = @n;
			SET @i = 0;
			
			/* Calculate difference */
			SET @day = ABS((@weekday - @day) - 7)
			IF @day > 6 SET @day = @day - 7
			 
			/* While days to add, insert */
			WHILE (@end_date >= DATEADD(day,@i * 7 + @day ,@start_date))
			BEGIN
				SET @d = DATEADD(day,@day+(@i*7),@start_date)
				UPDATE calendar_data SET dayoff = 1 WHERE calendar_id = @calendar_id AND event_date = @d
				IF @@ROWCOUNT = 0 INSERT INTO calendar_data (calendar_id, event_date, dayoff) VALUES(@calendar_id,@d,1)
				SET @i = @i + 1
			END
		END
	END
END;

GO
/****** Object:  StoredProcedure [dbo].[Calendar_CheckDayOffs]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calendar_CheckDayOffs]
	@check_start DATETIME, @check_end DATETIME, @create_start DATETIME, @create_end DATETIME, @calendar_id INT
AS
BEGIN
	--SET NOCOUNT ON;
 DECLARE @RC INT
 BEGIN TRANSACTION

 Exec @RC =sp_getapplock @Resource='CheckDayOffs', @LockMode='Exclusive'
            , @LockOwner='Transaction', @LockTimeout = 15000

--	DECLARE @number_of_we INT
--	SET @number_of_we = (SELECT COUNT(calendar_data_id) FROM calendar_data WHERE event_date BETWEEN @check_start AND @check_end AND calendar_id = @calendar_id AND dayoff = 1)

--	IF (@number_of_we = 0)
--BEGIN
		DECLARE @min_req TINYINT
		SET @min_req = (SELECT (def_1 + def_2 + def_3 + def_4 + def_5 + def_6 + def_7) FROM calendars WHERE calendar_id = @calendar_id)
		
		IF (@min_req > 0)
		BEGIN
			DECLARE @weekday_str VARCHAR(15)
			
			SET @weekday_str =			
			(SELECT RIGHT(def_str, len(def_str)-1) FROM (
					  SELECT def_str = 
					  CASE WHEN def_1 = 1 THEN ',1' ELSE '' END 
					+ CASE WHEN def_2 = 1 THEN ',2' ELSE '' END
					+ CASE WHEN def_3 = 1 THEN ',3' ELSE '' END
					+ CASE WHEN def_4 = 1 THEN ',4' ELSE '' END
					+ CASE WHEN def_5 = 1 THEN ',5' ELSE '' END
					+ CASE WHEN def_6 = 1 THEN ',6' ELSE '' END
					+ CASE WHEN def_7 = 1 THEN ',7' ELSE '' END
					FROM calendars
					WHERE calendar_id = @calendar_id
			) AS q)
			EXEC dbo.Calendar_AddDaysOff @calendar_id, @create_start,@create_end, @weekday_str
			SELECT  @weekday_str
		END
--	END

  -- release lock
  COMMIT TRANSACTION

END


GO
/****** Object:  StoredProcedure [dbo].[Calendar_CheckStaticHolidays]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calendar_CheckStaticHolidays]
	 @create_start DATETIME, @create_end DATETIME, @calendar_id INT
AS
BEGIN
		DELETE FROM calendar_data WHERE calendar_id = @calendar_id AND calendar_static_holiday_id <> null AND event_date BETWEEN @create_start AND @create_end	
		DECLARE @GetStaticHolidays CURSOR
		SET @GetStaticHolidays = CURSOR FAST_FORWARD LOCAL FOR 
		SELECT calendar_static_holiday_id, name, month, day FROM calendar_static_holidays WHERE calendar_id = @calendar_id

		DECLARE @name VARCHAR(MAX)
		DECLARE @month INT
		DECLARE @day INT
		DECLARE @calendarStaticHolidayId INT

		OPEN @GetStaticHolidays
		FETCH NEXT FROM @GetStaticHolidays INTO @calendarStaticHolidayId, @name, @month,@day

		WHILE (@@FETCH_STATUS=0) BEGIN
			DECLARE @yearStart INT = YEAR(@create_start)
			DECLARE @yearEnd INT = YEAR(@create_end)
			DECLARE @holidayToAdd DATETIME
			WHILE(@yearStart<=@yearEnd) BEGIN
				SET @holidayToAdd =
				CAST(CAST(@yearStart AS VARCHAR(4)) +
				RIGHT('0' + CAST(@month AS VARCHAR(2)), 2) +
				RIGHT('0' + CAST(@day AS VARCHAR(2)), 2) 
				AS DATETIME)

				IF(@holidayToAdd >= @create_start AND @holidayToAdd <= @create_end) BEGIN
					UPDATE calendar_data SET dayoff=1, holiday=1, calendar_static_holiday_id = @calendarStaticHolidayId WHERE calendar_id = @calendar_id AND event_date = @holidayToAdd
					IF @@ROWCOUNT = 0 INSERT INTO calendar_data (calendar_id, calendar_static_holiday_id, event_date, dayoff, holiday, notwork) VALUES(@calendar_id, @calendarStaticHolidayId, @holidayToAdd, 0, 1, 1)
					IF((SELECT COUNT(*) FROM calendar_data_attributes WHERE calendar_id = @calendar_id AND event_date=@holidayToAdd AND name = @name AND type_id = 1) = 0) BEGIN
						INSERT INTO calendar_data_attributes (calendar_id, event_date, name, type_id) VALUES(@calendar_id, @holidayToAdd, @name, 1)
					END
				END
				SET @yearStart = @yearStart + 1
			END
			FETCH NEXT FROM @GetStaticHolidays INTO @calendarStaticHolidayId, @name, @month,@day
		END
		CLOSE @GetStaticHolidays
END

GO
/****** Object:  StoredProcedure [dbo].[Calendar_GetCalendarsByUserIDs]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Calendar_GetCalendarsByUserIDs](@user_ids VARCHAR(max),@start_date DATETIME, @end_date DATETIME, @CalendarsOnly TINYINT)
AS
BEGIN
	
	DECLARE @ResultTable TABLE (user_id INT,base_calendar_id INT, user_calendar_id INT,event_date DATETIME, dayoff TINYINT, holiday TINYINT, notwork TINYINT, calendar_event_id INT)
	
	IF (@user_ids <> '')
	BEGIN
		DECLARE @params NVARCHAR(max) SET @params = '@GetUsers CURSOR OUTPUT'
		DECLARE @GetUsers CURSOR
		DECLARE @sql NVARCHAR(MAX) 
		
		IF (@CalendarsOnly = 0)
		BEGIN
			SET @sql = 'SET @GetUsers = CURSOR FAST_FORWARD LOCAL FOR SELECT u.user_id,u.base_calendar_id AS base_calendar_id,ISNULL(c.calendar_id,0) AS calendar_id FROM users u LEFT JOIN calendars c ON c.user_id = u.user_id WHERE u.user_id IN (' + @user_ids + ');OPEN @GetUsers'
		END ELSE
			SET @sql = 'SET @GetUsers = CURSOR FAST_FORWARD LOCAL FOR SELECT DISTINCT 0,u.base_calendar_id AS base_calendar_id,ISNULL(c.calendar_id,0) as calendar_id FROM users u LEFT JOIN calendars c ON u.user_id = c.user_id WHERE u.user_id IN (' + @user_ids + ');OPEN @GetUsers'
		
		DECLARE @UserID INT, @BaseCalendarID INT, @UserCalendarID INT
		EXEC sp_executesql @sql, @params, @GetUsers OUTPUT

		FETCH NEXT FROM @GetUsers INTO @UserID, @BaseCalendarID, @UserCalendarID

		--Loop through until no record is fetched
		WHILE (@@FETCH_STATUS=0)
		BEGIN 
			--Get Events
			INSERT INTO @ResultTable SELECT @UserID,@BaseCalendarId,@UserCalendarId,event_date, dayoff, holiday, notwork, calendar_event_id FROM dbo.GetCombinedCalendar(@BaseCalendarID,@UserCalendarID,@start_date,@end_date)
			
			--Get Next Item
			FETCH NEXT FROM @GetUsers INTO @UserID, @BaseCalendarID, @UserCalendarID
		END

		CLOSE @GetUsers
		DEALLOCATE @GetUsers
	END
--Return
SET NOCOUNT OFF
SELECT * FROM @ResultTable ORDER BY user_id ASC, event_date ASC

END


GO
/****** Object:  StoredProcedure [dbo].[Calendar_ModifyEvent]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Calendar_ModifyEvent]
	(
	@calendar_id INT, @calendar_event_id INT, @NewStartDate DATETIME, @NewEndDate DATETIME
	)
AS

BEGIN
	SET NOCOUNT ON;
	/* Remove all records outside of the new range */
	DELETE FROM calendar_data WHERE calendar_event_id = @calendar_event_id AND holiday = 0 AND dayoff = 0 AND (event_date < @NewStartDate OR event_date > @NewEndDate)
	UPDATE calendar_data SET calendar_event_id = 0 WHERE calendar_event_id = @calendar_event_id AND (event_date < @NewStartDate OR event_date > @NewEndDate)
	
	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = @NewStartDate
		
	/* Create Event inside the new range */
	WHILE (@CurrentDate <= @NewEndDate)
	BEGIN	
		UPDATE calendar_data SET calendar_event_id=@calendar_event_id WHERE calendar_id = @calendar_id AND event_date = @CurrentDate
		IF @@ROWCOUNT = 0 INSERT INTO calendar_data (calendar_id, calendar_event_id, event_date) VALUES (@calendar_id, @calendar_event_id, @CurrentDate)
		SET @CurrentDate = DATEADD(DAY,1,@CurrentDate)
	END	
END;


GO
/****** Object:  StoredProcedure [dbo].[Calendar_RemoveEvent]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Calendar_RemoveEvent]
	(
	@calendar_event_id INT
	)
AS

BEGIN
	SET NOCOUNT ON;
	/* Remove all records that are not daysoff or national holidays */
	DELETE FROM calendar_data WHERE calendar_event_id = @calendar_event_id AND holiday = 0 AND dayoff = 0
	
	/* Update remaining records to not be in this event */
	UPDATE calendar_data SET calendar_event_id = 0 WHERE calendar_event_id = @calendar_event_id
END;


GO

/****** Object:  StoredProcedure [dbo].[list_GetValue]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[list_GetValue]
(
 @listItemId int,
 @value NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @instance nvarchar(10)
	DECLARE @process nvarchar(50)
	DECLARE @sql nvarchar(max)

	SELECT @instance = instance, @process = process FROM items WHERE item_id = @listItemId
	
	set @sql = 'SELECT @value = list_item FROM _' + @instance + '_' + @process + ' where item_id = ' + cast(@listItemId as nvarchar)
	exec sp_executesql @sql, N'@value nvarchar(max) out', @value out
END 

GO

/****** Object:  StoredProcedure [dbo].[condition_IdReplaceProc]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[condition_IdReplaceProc]
(
 @condition_json NVARCHAR(MAX),
 @replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;  
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.itemColumn_GetVariable(@var3)		

				SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
					END
				IF dbo.IsListColumn(@var3)  > 0
				BEGIN					
					DECLARE @idCursor CURSOR 
					DECLARE @listItemId INT 
					DECLARE @value nvarchar(max)

					SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
					SELECT item_id FROM dbo.get_ListColumnIds(@var3);

					OPEN @idCursor
					FETCH NEXT FROM @idCursor INTO @listItemId
					WHILE (@@FETCH_STATUS=0) BEGIN
						print @listItemId
						EXEC list_GetValue @listItemId, @value = @value output
						set @condition_json = replace(@condition_json, '"Value":"' + CAST(@listItemId AS nvarchar) + '"}', '"Value":"' + @value + '"}')
						FETCH NEXT FROM @idCursor INTO @listItemId
					END
				END
			END

		  CONTINUE  
	END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.itemColumn_GetVariable(@var3)		

				SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
					END
			END

		  CONTINUE  
	END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)

			IF ISNUMERIC(@var3) = 1
			BEGIN
				SET @var4 = dbo.condition_GetVariable(@var3)		

				SET @condition_json = replace(@condition_json, @var2, 'ConditionId":' + @var4)

				IF @endPos2 > @endPos1
					BEGIN
						SET @startPos = charindex('ConditionId', @condition_json, @endPos1+LEN(@var4))
					END
				ELSE
					BEGIN
						SET @startPos = charindex('ConditionId', @condition_json, @endPos2+LEN(@var4))
					END
			END

		  CONTINUE  
	END
 SET @replaced_json = @condition_json
END

GO

/****** Object:  StoredProcedure [dbo].[list_GetId]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[list_GetId]
(
 @instance nvarchar(10),
 @process nvarchar(50),
 @listItemValue nvarchar(max),
 @id int OUTPUT
)
AS
BEGIN
	DECLARE @sql nvarchar(max)

	set @sql = 'SELECT @id = item_id FROM _' + @instance + '_' + @process + ' WHERE list_item = ''' + @listItemValue + '''' 
	exec sp_executesql @sql, N'@id nvarchar(max) out', @id out
END 

GO

/****** Object:  StoredProcedure [dbo].[condition_VarReplaceProc]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[condition_VarReplaceProc]
(
 @sourceInstance NVARCHAR(10),
 @targetInstance NVARCHAR(10),
 @condition_json NVARCHAR(MAX),
 @replaced_json NVARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @var NVARCHAR(MAX)
	DECLARE @var2 NVARCHAR(MAX)
	DECLARE @var3 NVARCHAR(MAX)
	DECLARE @var4 NVARCHAR(MAX)
	--DECLARE @var5 NVARCHAR(MAX)
	DECLARE @endPos1 INT
	DECLARE @endPos2 INT

	--SET @var5 = @condition_json

	DECLARE @startPos INT = charindex('ItemColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'ItemColumnId":' + @var4)

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ItemColumnId', @condition_json, @endPos2+LEN(@var4))
				END

			IF dbo.IsListColumn(dbo.itemColumn_GetId(@sourceInstance, @var3))  > 0
				BEGIN					
					DECLARE @idCursor CURSOR 
					DECLARE @listItemId INT 
					DECLARE @value nvarchar(max)
					DECLARE @id int
					DECLARE @process nvarchar(50)

					SET @idCursor = CURSOR  FAST_FORWARD LOCAL FOR 
					SELECT item_id FROM dbo.get_ListColumnIds(dbo.itemColumn_GetId(@sourceInstance, @var3));

					select @process = data_additional FROM item_columns WHERE instance = @sourceInstance and item_column_id = dbo.itemColumn_GetId(@sourceInstance, @var3)

					OPEN @idCursor
					FETCH NEXT FROM @idCursor INTO @listItemId
					WHILE (@@FETCH_STATUS=0) BEGIN

						print 'get id'
						print @listItemId

						EXEC list_GetValue @listItemId, @value = @value output
						print @targetInstance
						print @process
						print @value
										
						print 'EXEC list_GetId ' + @targetInstance + ', ' + @process + ', ' + @value + ', ' + ' @id = @id output '
						EXEC list_GetId @targetInstance, @process, @value, @id = @id output
						set @condition_json = replace(@condition_json, '"Value":"' + @value + '"}', '"Value":"' + CAST(@id as nvarchar) + '"}')
						FETCH NEXT FROM @idCursor INTO @listItemId
					END
				END
		  CONTINUE  
	END

	SET @startPos = charindex('UserColumnId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 15, LEN(@var2)-14)

			SET @var4 = dbo.itemColumn_GetId(@targetInstance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'UserColumnId":' + @var4)

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('UserColumnId', @condition_json, @endPos2+LEN(@var4))
				END

		  CONTINUE  
	END

	SET @startPos = charindex('ConditionId', @condition_json)
	WHILE @startPos > 0
	BEGIN  

			SET @endPos1 = charindex(',', @condition_json, @startPos)
			SET @endPos2 = charindex('}', @condition_json, @startPos)
			IF @endPos2 > @endPos1
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos1-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1)
				END
			ELSE
				BEGIN
					SET @var2 = SUBSTRING(@condition_json, @startPos, @endPos2-@startPos)
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2)
				END
			SET @var3 = SUBSTRING(@var2, 14, LEN(@var2)-13)

			SET @var4 = dbo.condition_GetId(@targetInstance, @var3)		

			SET @condition_json = replace(@condition_json, @var2, 'ConditionId":' + @var4)

			IF @endPos2 > @endPos1
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos1+LEN(@var4))
				END
			ELSE
				BEGIN
					SET @startPos = charindex('ConditionId', @condition_json, @endPos2+LEN(@var4))
				END

		  CONTINUE  
	END
 SET @replaced_json = @condition_json
END

GO
/****** Object:  StoredProcedure [dbo].[conditions_replace]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[conditions_replace]
(
@sourceInstance nvarchar(10),
@targetInstance nvarchar(10)
)
AS
BEGIN
	DECLARE @condition_id INT
	DECLARE @instance nvarchar(10)
	DECLARE @process nvarchar(50)
	DECLARE @VARIABLE nvarchar(255)
	DECLARE @condition_json nvarchar(max)

	DECLARE @conditionCursor CURSOR 
	SET @conditionCursor = CURSOR  FAST_FORWARD LOCAL FOR 
	SELECT condition_id, instance, process, variable, condition_json FROM conditions WHERE instance IN (@sourceInstance, @targetInstance)

	OPEN @conditionCursor
	FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	WHILE (@@FETCH_STATUS=0) BEGIN
		declare @json as nvarchar(max)
		declare @json2 as nvarchar(max)
		exec dbo.condition_IdReplaceProc @condition_json,  @replaced_json = @json output
		exec dbo.condition_VarReplaceProc @sourceInstance, @targetInstance, @json,  @replaced_json = @json2 output
		print '@json1'
		print @json
		print '@json2'
		print @json2
		insert into #conditions (condition_id, instance, process, variable, condition_json, replaced_condition_json) values (@condition_id, @instance, @process, @variable, @json, @json2)
		FETCH NEXT FROM @conditionCursor INTO @condition_id, @instance, @process, @variable, @condition_json
	END
END

GO
/****** Object:  StoredProcedure [dbo].[items_AddColumn]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[items_AddColumn]
	@TableName NVARCHAR(255), @ColumnName NVARCHAR(50), @DataType TINYINT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @DataTypeName NVARCHAR(255)
	IF @DataType = 0 BEGIN
		SET @DataTypeName = 'NVARCHAR(255)'
	END ELSE IF @DataType = 1 BEGIN
		SET @DataTypeName = 'INT'
	END ELSE IF @DataType = 2 BEGIN
		SET @DataTypeName = 'FLOAT'
	END ELSE IF @DataType = 3 BEGIN
		SET @DataTypeName = 'DATE'
	END ELSE IF @DataType = 4 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 5 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 6 BEGIN
		SET @DataTypeName = 'INT'
	END ELSE IF @DataType = 7 BEGIN
		SET @DataTypeName = 'NVARCHAR(MAX)'
	END ELSE IF @DataType = 10 BEGIN
		SET @DataTypeName = 'NVARCHAR(255)'
	END ELSE IF @DataType = 13 BEGIN
		SET @DataTypeName = 'DATETIME'
	END ELSE IF @DataType = 14 BEGIN
		SET @DataTypeName = 'FLOAT'
	END
	
	DECLARE @Sql NVARCHAR(MAX) = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataTypeName
	PRINT @Sql
	EXEC sp_executesql @Sql 

	exec app_ensure_archive @TableName
END

GO
/****** Object:  StoredProcedure [dbo].[items_BuildTable]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[items_BuildTable]
	@Instance NVARCHAR(10),@Process NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ColId NVARCHAR(50)
	DECLARE @ColName NVARCHAR(50)
	DECLARE @DataType TINYINT
	DECLARE @D_item_id INT
	DECLARE @D_order_no NVARCHAR(MAX)
	DECLARE @D_item_column_id INT
	DECLARE @D_nvarchar NVARCHAR(255)
	DECLARE @D_int INT
	DECLARE @D_float FLOAT
	DECLARE @D_date DATE
	DECLARE @D_text NVARCHAR(MAX)
	DECLARE @ColNames NVARCHAR(MAX) = ''
	DECLARE @TableName NVARCHAR(200)
	DECLARE @GetColumns CURSOR
	DECLARE @GetData CURSOR
	DECLARE @CreateSql NVARCHAR(MAX) = ''
	DECLARE @FillSql NVARCHAR(MAX) = ''

	SET @TableName = '_' + @instance + '_' + @Process
	
	--Drop table if it exists // Trigger will actually drop the table
	DELETE FROM item_tables WHERE name = @TableName
	
	--Double Check
	IF dbo.TableExists(@TableName) = 1 BEGIN
		DECLARE @DropSql NVARCHAR(MAX) = 'DROP TABLE ' + @TableName
		EXEC sp_executesql @DropSql
	END

	--Get Columns
	SET @GetColumns = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT name,data_type FROM item_columns WHERE process = @Process ANd instance = @Instance

	OPEN @GetColumns
	FETCH NEXT FROM @GetColumns INTO @ColName,@DataType

	WHILE (@@FETCH_STATUS=0) BEGIN
		--Add Column name
		SET @CreateSql = @CreateSql + ',' + @ColName

		--Add data type
		IF @DataType = 0 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(255)'
		END ELSE IF @DataType = 1 BEGIN
			SET @CreateSql = @CreateSql + ' INT'
		END ELSE IF @DataType = 2 BEGIN
			SET @CreateSql = @CreateSql + ' FLOAT'
		END ELSE IF @DataType = 3 BEGIN
			SET @CreateSql = @CreateSql + ' DATETIME'
		END ELSE IF @DataType = 4 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(MAX)'
		END ELSE IF @DataType = 5 BEGIN
			SET @CreateSql = @CreateSql + ' NVARCHAR(MAX)'
		END ELSE IF @DataType = 6 BEGIN
			SET @CreateSql = @CreateSql + ' INT'
		END

		FETCH NEXT FROM @GetColumns INTO @ColName,@DataType
	END
	CLOSE @GetColumns

	--Create Table
	SET @CreateSql = 'CREATE TABLE ' + @TableName + '(item_id INT PRIMARY KEY, order_no NVARCHAR(MAX)' + @CreateSql + ')'
	SET @ColNames = 'data_id,order_no' + @ColNames
	EXEC sp_executesql @CreateSql
	INSERT INTO item_tables (instance,process,name) VALUES (@Instance,@Process,@tablename)

	--Create Archive Table
	EXEC app_ensure_archive @TableName

	--Get Data
	SET @GetData = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT id.item_id,i.order_no,id.item_column_id,id.data_nvarchar,id.data_int,id.data_float,id.data_date,id.data_text,ic.name, ic.data_type FROM item_data id
	LEFT JOIN item_columns ic ON ic.item_column_id = id.item_column_id
	LEFT JOIN items i ON i.item_id = id.item_id
	WHERE ic.process = @Process AND ic.instance = @Instance
	ORDER BY i.order_no ASC, id.item_id ASC

	OPEN @GetData
	FETCH NEXT FROM @GetData INTO @D_item_id, @D_order_no, @D_item_column_id, @D_nvarchar, @D_int, @D_float, @D_date, @D_text, @ColName, @DataType

	DECLARE @CurrentID INT = 0
	WHILE (@@FETCH_STATUS=0) BEGIN
		IF @D_item_id <> @CurrentID BEGIN
			--Id changed -- commit
			IF @CurrentID <> 0 BEGIN
				SET @FillSql = 'INSERT INTO ' + @TableName + ' (' + @ColNames + ') VALUES (' + CAST(@CurrentID AS NVARCHAR) + ',' + CAST(@D_order_no AS NVARCHAR) + @FillSql + ')'
				EXEC sp_executesql @FillSql
			END
			SET @FillSql = ''
			SET @CurrentID = @D_item_id
			SET @ColNames = 'item_id, order_no'
		END
	
		--Add data
		SET @ColNames = @ColNames + ',' + @ColName
		IF @DataType = 0 BEGIN
			SET @FillSql = @FillSql + ',' + CHAR(39) + REPLACE(ISNULL(@D_nvarchar,''),CHAR(39),CHAR(39)+CHAR(39)) + CHAR(39)
		END ELSE IF @DataType = 1 BEGIN
			SET @FillSql = @FillSql + ',' + CHAR(39) + CAST(ISNULL(@D_int,0) AS nvarchar) + CHAR(39)
		END ELSE IF @DataType = 2 BEGIN
			SET @FillSql = @FillSql + ',' + CHAR(39) + CAST(ISNULL(@D_float,0) AS nvarchar) + CHAR(39)
		END ELSE IF @DataType = 3 BEGIN
			SET @FillSql = @FillSql + ',' + CHAR(39) + CAST(ISNULL(@D_date,'') AS nvarchar) + CHAR(39)
		END ELSE IF @DataType = 4 BEGIN
			SET @FillSql = @FillSql + ',' + CHAR(39) + REPLACE(ISNULL(@D_text,''),CHAR(39),CHAR(39)+CHAR(39)) + CHAR(39)
		END ELSE IF @DataType = 5 BEGIN
			SET @FillSql = @FillSql + ',' + CHAR(39) + REPLACE(ISNULL(@D_text,''),CHAR(39),CHAR(39)+CHAR(39)) + CHAR(39)
		END ELSE IF @DataType = 6 BEGIN
			SET @FillSql = @FillSql + ',' + CHAR(39) + CAST(ISNULL(@D_int,0) AS nvarchar) + CHAR(39)
		END

		FETCH NEXT FROM @GetData INTO @D_item_id, @D_order_no, @D_item_column_id, @D_nvarchar, @D_int, @D_float, @D_date, @D_text, @ColName, @DataType
	END

	--Insert Last Row
	IF @CurrentID <> 0 BEGIN
		SET @FillSql = 'INSERT INTO ' + @TableName + ' (' + @ColNames + ') VALUES (' + CAST(@CurrentID AS NVARCHAR) + ',' + CAST(@D_order_no AS NVARCHAR) + @FillSql + ')'
		EXEC sp_executesql @FillSql
	END
	CLOSE @GetData
	DEALLOCATE @GetData

	--Insert Empty Rows
	DECLARE @EmptySql NVARCHAR(MAX) = 
	'INSERT INTO ' + @TableName +' (item_id, order_no)
	SELECT i.item_id, i.order_no FROM items i 
	LEFT JOIN item_data id ON id.item_id = i.item_id
	LEFT JOIN item_columns ic ON ic.item_column_id = id.item_column_id 
	WHERE i.instance = ' + CHAR(39) + @Instance + CHAR(39) + ' AND i.process = ' + CHAR(39) + @Process + CHAR(39) + ' AND id.item_column_id IS NULL'
	EXEC sp_executesql @EmptySql
END

GO

/****** Object:  StoredProcedure [dbo].[items_DropColumn]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[items_DropColumn]
	@TableName NVARCHAR(255), @ColumnName NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @colsCount INT
	SELECT @colsCount=COUNT(column_name) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName AND COLUMN_NAME = @ColumnName
	IF @colsCount > 0 
	BEGIN
		DECLARE @Sql NVARCHAR(MAX) = 'ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @ColumnName
		EXEC sp_executesql @Sql 

		SET @Sql = 'ALTER TABLE archive_' + @TableName + ' DROP COLUMN ' + @ColumnName
		EXEC sp_executesql @Sql 

		exec app_ensure_archive @TableName
	END
END

GO

/****** Object:  StoredProcedure [dbo].[items_ChangeColumnType]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[items_ChangeColumnType]
	@ItemColumnId INT, @OldDataType INT, @NewDataType TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	IF (@OldDataType = -1) BEGIN 
		SELECT @OldDataType = data_type FROM item_columns WHERE item_column_id = @ItemColumnId
	END

	IF (@OldDataType <> @NewDataType) BEGIN
		DECLARE @ColumnName NVARCHAR(50)
		DECLARE @TableName NVARCHAR(50)
		DECLARE @instance NVARCHAR(10)
		DECLARE @process NVARCHAR(50)

		SELECT @ColumnName = name, @instance = instance, @process = process FROM item_columns WHERE item_column_id = @ItemColumnId
		SELECT @tableName = name FROM item_tables WHERE instance = @instance AND process = @process
		
		--Remove existing data
		DELETE FROM item_data WHERE item_column_id = @ItemColumnId

		--Change Datatype
		UPDATE item_columns SET data_type = @NewDataType WHERE item_column_id = @ItemColumnId
		EXEC items_DropColumn @TableName, @ColumnName
		EXEC items_AddColumn @TableName, @ColumnName, @NewDataType
	END
END

GO

/****** Object:  StoredProcedure [dbo].[items_RebuildOrderNumbers]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[items_RebuildOrderNumbers]
(
	@instance NVARCHAR(10), @process NVARCHAR(50)
)AS
BEGIN
	DECLARE @len int;
	SELECT @len = COUNT(item_id) FROM items WHERE instance = @instance AND process = @process 
	DECLARE @temp TABLE(order_no nvarchar(max))
	DECLARE @charList NVARCHAR(MAX) = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

	DECLARE @step float = CAST(LEN(@charList) as float) / CAST(@len as float);
	IF @step<1 SET @step = 1
	DECLARE @i float=1;
	WHILE @i<LEN(@charList) BEGIN
		SET @i = @i+@step;
		INSERT INTO @temp (order_no) VALUES (SUBSTRING(@charList,CAST(@i AS INT), 1))
	END

	DECLARE @runner INT= 1;
	DECLARE @f nvarchar(max);
	DECLARE @l nvarchar(max);
	DECLARE @s nvarchar(max);

	WHILE @i<@len BEGIN
		SELECT ROW_NUMBER() OVER(ORDER BY order_no COLLATE Latin1_General_bin ASC) AS Row, order_no 
		INTO #sorted
		FROM @temp
		SELECT @f = order_no FROM #sorted WHERE Row = @runner
		SELECT @l = order_no FROM #sorted WHERE Row = @runner+1
		SET @i = @i+1;
		if @runner+2<@i  SET @runner = @runner+2;
			ELSE SET @runner =1;
		DROP TABLE #sorted
		SELECT @s = dbo.getOrderBetween(@f,@l);	
		INSERT INTO @temp (order_no) VALUES(@s)
	END
	exec app_set_archive_user_id 0

	UPDATE items
	SET order_no = i.order_no
	FROM(
		SELECT s1.item_id, s2.order_no
		FROM (
			SELECT row = ROW_NUMBER() OVER(ORDER BY order_no COLLATE Latin1_General_bin ASC), item_id
			FROM items
			WHERE instance = @instance AND process = @process
		) s1
		INNER JOIN (
			SELECT row = ROW_NUMBER() OVER(ORDER BY order_no COLLATE Latin1_General_bin ASC), order_no FROM @temp
		) s2 ON s1.row = s2.row
	) i
	WHERE items.item_id = i.item_id

	--ORDER BY i.order_no COLLATE Latin1_General_bin  

 END;

GO
/****** Object:  StoredProcedure [dbo].[items_Reset]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[items_Reset]
	
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM item_tables
	DELETE FROM items
	DELETE FROM item_columns
	DELETE FROM processes
	DELETE FROM instances
	DELETE FROM item_data
END

GO


/****** Object:  Trigger [dbo].[calendar_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[calendar_fk_delete]
    ON [dbo].[calendars]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM calendar_data dd	INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
	DELETE dd FROM calendar_data_attributes dd	INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
    	DELETE dd FROM calendar_events dd			INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
	DELETE dd FROM calendar_static_holidays dd	INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
	DELETE dd FROM calendars dd	INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
    --DELETE dd FROM calendars dd					INNER JOIN deleted d ON dd.calendar_id = d.calendar_id
    

GO
/****** Object:  Trigger [dbo].[instance_configurations_INSTEAD_INSERT]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[instance_configurations_INSTEAD_INSERT]
ON [dbo].[instance_configurations] 
INSTEAD OF INSERT
AS

BEGIN tran
	IF EXISTS (SELECT * FROM instance_configurations iC
		INNER JOIN INSERTED i ON iC.instance = i.instance AND (iC.process = i.process OR (iC.process IS NULL AND i.process IS NULL)) AND iC.category = i.category AND iC.name = i.name)
		BEGIN
			PRINT 'FOUND';
			UPDATE instance_configurations SET value = i.value, process = i.process, group_name = i.group_name
			FROM (SELECT instance, process, group_name, category, name, value FROM inserted) i 
			WHERE instance_configurations.instance = i.instance AND (instance_configurations.process = i.process OR (instance_configurations.process IS NULL AND i.process IS NULL)) AND instance_configurations.category = i.category AND instance_configurations.name = i.name
		END
	ELSE
		BEGIN
		PRINT 'NOPE';
			INSERT INTO instance_configurations(instance, process, group_name, category, name, value, [server])
			SELECT i.instance, i.process, i.group_name, i.category, i.name, i.value, i.[server] FROM inserted i
		END
COMMIT tran

GO
/****** Object:  Trigger [dbo].[instance_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[instance_fk_delete]
    ON [dbo].[instances]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM attachments dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM calendars dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_translations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_languages dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_logs dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_hosts dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_menu dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instance_user_configurations_saved dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM processes dd	INNER JOIN deleted d ON dd.instance = d.instance
	DELETE dd FROM instances dd	INNER JOIN deleted d ON dd.instance = d.instance

GO
/****** Object:  Trigger [dbo].[AfterDeleteColumn]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[AfterDeleteColumn] ON [dbo].[item_columns] AFTER delete AS
DECLARE @GetChanges CURSOR
DECLARE @TableName NVARCHAR(255)
DECLARE @ColumnName NVARCHAR(50)

SET @GetChanges = CURSOR FAST_FORWARD LOCAL FOR 
SELECT it.name AS TableName, i.name AS ColumnName FROM item_tables it
INNER JOIN deleted i ON i.instance = it.instance AND i.process = it.process

OPEN @GetChanges
FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName
WHILE (@@FETCH_STATUS=0) BEGIN
	IF dbo.TableColumnExists(@TableName, @ColumnName) = 1 BEGIN
		EXEC dbo.items_DropColumn @TableName,@ColumnName
	END
	FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName
END

GO
/****** Object:  Trigger [dbo].[AfterInsertColumn]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[AfterInsertColumn] ON [dbo].[item_columns] AFTER insert AS
DECLARE @GetChanges CURSOR
DECLARE @TableName NVARCHAR(255)
DECLARE @ColumnName NVARCHAR(50)
DECLARE @DataType TINYINT

SET @GetChanges = CURSOR FAST_FORWARD LOCAL FOR 
SELECT it.name AS TableName, i.name AS ColumnName, i.data_type FROM item_tables it
INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process

OPEN @GetChanges
FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType
WHILE (@@FETCH_STATUS=0) BEGIN
	IF @ColumnName IS NOT NULL AND LEN(@ColumnName) > 0
		BEGIN
			EXEC dbo.items_AddColumn @TableName,@ColumnName,@DataType
		END
	FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType
END

GO
/****** Object:  Trigger [dbo].[AfterUpdateColumn]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[AfterUpdateColumn] ON [dbo].[item_columns] AFTER update AS
DECLARE @GetChanges CURSOR
DECLARE @TableName NVARCHAR(255)
DECLARE @OldColumnName NVARCHAR(255)
DECLARE @ColumnName NVARCHAR(50)
DECLARE @TableColumnName NVARCHAR(MAX)
DECLARE @DataType TINYINT

SET @GetChanges = CURSOR FAST_FORWARD LOCAL FOR 
SELECT it.name AS TableName, i.name AS ColumnName, i.data_type, d.name As OldColumnName FROM item_tables it
INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
INNER JOIN deleted d ON i.item_column_id = d.item_column_id

OPEN @GetChanges
FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType,@OldColumnName
WHILE (@@FETCH_STATUS=0) 
BEGIN
	IF @ColumnName IS NOT NULL AND @OldColumnName IS NULL AND LEN(@ColumnName) > 0
		BEGIN
			EXEC dbo.items_AddColumn @TableName,@ColumnName,@DataType
		END
	ELSE 
		BEGIN
			IF @ColumnName <> @OldColumnName
				BEGIN
					SET @TableColumnName = @TableName + '.' + @OldColumnName
					EXEC sp_rename @TableColumnName, @ColumnName, 'COLUMN'
				END
		END

	FETCH NEXT FROM @GetChanges INTO @TableName,@ColumnName,@DataType, @OldColumnName
END

GO
/****** Object:  Trigger [dbo].[item_columns_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[item_columns_fk_delete]
    ON [dbo].[item_columns]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM process_container_columns dd			INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM process_portfolio_columns dd	INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM attachments dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM item_data_process_selections dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM process_tab_columns dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM item_data dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM state_rights dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id
    DELETE dd FROM state_rights dd					INNER JOIN deleted d ON dd.user_column_id = d.item_column_id
	DELETE dd FROM item_columns dd					INNER JOIN deleted d ON dd.item_column_id = d.item_column_id

GO
/****** Object:  Trigger [dbo].[item_UpdateTables]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[item_UpdateTables] ON [dbo].[item_data] AFTER insert,update AS
	DECLARE @updateCount INT
	DECLARE @listItemColumnName INT
	DECLARE @colCount INT
	DECLARE @instance NVARCHAR(10)
	DECLARE @process NVARCHAR(10)
	DECLARE @data NVARCHAR(255)
	DECLARE @itemId INT
	DECLARE @itemColumnId INT
	DECLARE @portfolioId INT
	DECLARE @SQL NVARCHAR(MAX) = (
		SELECT 
			';UPDATE ' + it.name + ' SET ' + ic.name + ' = (SELECT item_data.' + 
			CASE 
				WHEN ic.data_type = 0 THEN 'data_nvarchar'
				WHEN ic.data_type = 1 THEN 'data_int'
				WHEN ic.data_type = 2 THEN 'data_float'
				WHEN ic.data_type = 3 THEN 'data_date'
				WHEN ic.data_type = 4 THEN 'data_text'
				WHEN ic.data_type = 5 THEN 'data_nvarchar'
				WHEN ic.data_type = 6 THEN 'data_int'
			END + 
			' FROM item_data WHERE item_data.item_id = ' + CAST(id.item_id AS nvarchar) + ' AND item_data.item_column_id = ' + CAST(id.item_column_id AS nvarchar) + ') ' + 
			' WHERE item_id = ' + CAST(id.item_id AS nvarchar)
		FROM item_data id
		INNER JOIN inserted i ON i.item_id = id.item_id AND i.item_column_id = id.item_column_id
		INNER JOIN item_columns ic ON ic.item_column_id = id.item_column_id
		INNER JOIN item_tables it ON it.instance = ic.instance AND it.process = ic.process
		FOR XML PATH('')
	)

	SELECT @instance = p.instance, @process = p.process FROM item_columns ic 
	inner join processes p ON ic.instance = p.instance AND ic.process = p.process
	INNER JOIN inserted i ON i.item_column_id = ic.item_column_id
	WHERE ic.item_column_id = i.item_column_id 

	SELECT @updateCount = count(item_column_id) FROM item_columns WHERE instance = @instance AND data_additional = @process AND data_type = 5

	 IF @updateCount > 0
	 BEGIN

	/*UPDATE item_data SET data_nvarchar = [dbo].[PortfolioValue](@instance, @portfolioId, id.item_id, id.item_column_id) 
	FROM item_data id INNER JOIN item_columns ic ON ic.item_column_id = id.item_column_id
	WHERE item_id = @itemId AND item_column_id = @itemColumnId*/


			DECLARE columnsCursor CURSOR FAST_FORWARD FOR 
			
			SELECT item_id, ic.item_column_id, data_additional2 FROM item_columns ic
			INNER JOIN item_data id ON ic.item_column_id = id.item_column_id
			WHERE instance = @instance AND data_additional = @process AND data_type = 5

			OPEN columnsCursor

			FETCH NEXT FROM columnsCursor INTO  @itemId, @itemColumnId, @portfolioId
			WHILE (@@FETCH_STATUS = 0) 
			BEGIN
				UPDATE item_data SET data_nvarchar = [dbo].[PortfolioValue](@instance, @portfolioId, @itemId, @itemColumnId) WHERE item_id = @itemId AND item_column_id = @itemColumnId
				FETCH NEXT FROM columnsCursor INTO  @itemId, @itemColumnId, @portfolioId
			END
			CLOSE columnsCursor
			DEALLOCATE columnsCursor
	 END  

	--print @SQL 
	EXEC sp_executesql @SQL

GO
/****** Object:  Trigger [dbo].[items_DropTable]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[items_DropTable] ON [dbo].[item_tables] AFTER DELETE AS
DECLARE @GetDeleted CURSOR
DECLARE @TableName NVARCHAR(255)

SET @GetDeleted = CURSOR FAST_FORWARD LOCAL FOR 
SELECT name FROM deleted

OPEN @GetDeleted
FETCH NEXT FROM @GetDeleted INTO @TableName

WHILE (@@FETCH_STATUS=0) BEGIN
	DECLARE @DropSql NVARCHAR(MAX) = 'DROP TABLE ' + @TableName + ';'
	EXEC sp_executesql @DropSql
	FETCH NEXT FROM @GetDeleted INTO @TableName
END

CLOSE @GetDeleted
DEALLOCATE @GetDeleted

GO
/****** Object:  Trigger [dbo].[items_AfterDeleteRow]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[items_AfterDeleteRow] ON [dbo].[items] AFTER DELETE AS
DECLARE @sql nvarchar(max) = 
(
SELECT ';DELETE FROM ' + it.name + ' WHERE item_id = (' + CAST(i.item_id AS VARCHAR) + ')' 
FROM item_tables it 
INNER JOIN deleted i ON i.instance = it.instance AND i.process = it.process
FOR XML PATH('')
)
EXEC sp_executesql @SQL

GO
/****** Object:  Trigger [dbo].[items_AfterInsertRow]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[items_AfterInsertRow] ON [dbo].[items] AFTER INSERT AS
	DECLARE @sql nvarchar(max) = 
	(
	SELECT ';INSERT INTO ' + it.name + ' (item_id, order_no) VALUES (' + CAST(i.item_id AS VARCHAR) + ',''' + CAST(i.order_no AS VARCHAR) + ''')' 
	FROM item_tables it 
	INNER JOIN inserted i ON i.instance = it.instance AND i.process = it.process
	FOR XML PATH('')
	)
EXEC sp_executesql @SQL

GO
/****** Object:  Trigger [dbo].[items_AfterUpdateRow]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[items_AfterUpdateRow] ON [dbo].[items] AFTER UPDATE AS
DECLARE @oldVal INT
DECLARE @newVal INT
DECLARE @tableName nvarchar(max)
DECLARE @itemId INT
DECLARE @instance nvarchar(max)
DECLARE @process nvarchar(max)
DECLARE @colNames nvarchar(max) = ''
DECLARE @colName nvarchar(max)
DECLARE @colData nvarchar(max) = ''
DECLARE @dataType INT
DECLARE @D_nvarchar NVARCHAR(255)
DECLARE @D_int INT
DECLARE @D_float FLOAT
DECLARE @D_date DATE
DECLARE @D_text NVARCHAR(MAX)
DECLARE @GetColumns CURSOR

SELECT @oldVal = deleted FROM deleted
SELECT @newVal = deleted, @itemId = item_id, @instance = instance, @process = process FROM inserted
SELECT @tableName = name FROM inserted i INNER JOIN item_tables it on i.instance = it.instance AND i.process = it.process

IF @oldVal <> @newVal AND @newVal = 1 BEGIN
	DECLARE @sql nvarchar(max) = 
	(
	SELECT '; DELETE FROM ' + @tableName + ' WHERE item_id = ' + CAST(@itemId  AS VARCHAR)
	FOR XML PATH('')
	)
	EXEC sp_executesql @SQL
	UPDATE items SET deleted = 1 FROM process_tabs_subprocess_data subdata INNER JOIN items ON subdata.item_id = items.item_id WHERE parent_process_item_id = @itemId
END
IF @oldVal <> @newVal AND @newVal = 0 BEGIN
	--Get Columns
	SET @GetColumns = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT name, data_type, id.data_nvarchar, id.data_int, id.data_float, id.data_date, id.data_text FROM item_columns ic INNER JOIN item_data id on ic.item_column_id = id.item_column_id WHERE process = @Process AND instance = @Instance AND item_id = @itemId ORDER BY name

	OPEN @GetColumns
	FETCH NEXT FROM @GetColumns INTO @ColName, @dataType, @D_nvarchar, @D_int, @D_float, @D_date, @D_text

	WHILE (@@FETCH_STATUS=0) BEGIN
		--Add Column name
		SET @ColNames = @ColNames + ',' + @ColName
		IF @dataType = 0 BEGIN
			SET @ColData = @ColData + ',' + char(39) + @D_nvarchar + char(39)
		END ELSE IF @DataType = 1 BEGIN
			SET @ColData = @ColData + ',' + char(39) + CAST(@D_int AS NVARCHAR) + char(39)
		END ELSE IF @DataType = 2 BEGIN
			SET @ColData = @ColData + ',' + char(39) + CAST(@D_float AS NVARCHAR) + char(39)
		END ELSE IF @DataType = 3 BEGIN
			SET @ColData = @ColData + ',' + char(39) + CAST(@D_date AS NVARCHAR) + char(39)
		END ELSE IF @DataType = 4 BEGIN
			SET @ColData = @ColData + ',' + char(39) + @D_text+ char(39)
		END
		FETCH NEXT FROM @GetColumns INTO @ColName, @dataType, @D_nvarchar, @D_int, @D_float, @D_date, @D_text
	END
	DECLARE @sql2 nvarchar(max) =
	(
	SELECT  '; INSERT INTO ' + @tableName + ' (item_id' + @ColNames + ') VALUES (' + CAST(@itemId AS NVARCHAR) + @colData + ')'
		FOR XML PATH('')
	)
	EXEC sp_executesql @SQL2
	UPDATE items SET deleted = 0 FROM process_tabs_subprocess_data subdata INNER JOIN items ON subdata.item_id = items.item_id WHERE parent_process_item_id = @itemId
END

--Update Order Numbers
DECLARE @SQL3 NVARCHAR(MAX) = (
	SELECT 
		';UPDATE ' + @tableName + ' SET order_no = ''' + CAST(i.order_no AS NVARCHAR) + ''' WHERE item_id = ' + CAST(items.item_id AS nvarchar)
	FROM items
	INNER JOIN inserted i ON i.item_id = items.item_id
	FOR XML PATH('')
)
--print @SQL 
EXEC sp_executesql @SQL3

GO
/****** Object:  Trigger [dbo].[items_BeforeInsertRow]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[items_BeforeInsertRow] ON [dbo].[items] INSTEAD OF INSERT AS
BEGIN
    INSERT INTO items (instance, process, deleted, order_no)
    SELECT instance, process, deleted, CASE WHEN order_no = 0 THEN dbo.items_GetOrderEnd(instance,process) ELSE order_no END
    FROM   Inserted
END

GO
/****** Object:  Trigger [dbo].[items_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
    DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data dd						INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
	DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.process_id = d.item_id
	DELETE dd FROM task_parents dd	INNER JOIN deleted d ON dd.task_id = d.item_id
	--DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
	--DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.process_item_id = d.item_id
	--DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_id = d.item_id
	--DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_id = d.item_id
	--DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id
	
	--Finally delete the item
	DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id


GO
/****** Object:  Trigger [dbo].[process_containers_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_containers_fk_delete]
    ON [dbo].[process_containers]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM condition_containers dd			INNER JOIN deleted d ON dd.process_container_id = d.process_container_id
    DELETE dd FROM process_container_columns dd	INNER JOIN deleted d ON dd.process_container_id = d.process_container_id
    DELETE dd FROM process_tab_containers dd					INNER JOIN deleted d ON dd.process_container_id = d.process_container_id
	DELETE dd FROM process_containers dd					INNER JOIN deleted d ON dd.process_container_id = d.process_container_id

GO
/****** Object:  Trigger [dbo].[process_groups_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_groups_fk_delete]
    ON [dbo].[process_groups]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM condition_groups dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
    DELETE dd FROM process_group_tabs dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
    DELETE dd FROM process_portfolio_groups dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
	DELETE dd FROM process_group_features dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id
	DELETE dd FROM process_groups dd			INNER JOIN deleted d ON dd.process_group_id = d.process_group_id

GO
/****** Object:  Trigger [dbo].[process_portfolios_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_portfolios_fk_delete]
    ON [dbo].[process_portfolios]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM process_portfolio_columns dd			INNER JOIN deleted d ON dd.process_portfolio_id = d.process_portfolio_id
    DELETE dd FROM process_portfolio_groups dd			INNER JOIN deleted d ON dd.process_portfolio_id = d.process_portfolio_id
    DELETE dd FROM process_portfolio_user_groups dd			INNER JOIN deleted d ON dd.process_portfolio_id = d.process_portfolio_id
    DELETE dd FROM process_portfolios dd				INNER JOIN deleted d ON dd.process_portfolio_id = d.process_portfolio_id

GO
/****** Object:  Trigger [dbo].[process_tabs_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_tabs_fk_delete]
    ON [dbo].[process_tabs]
    INSTEAD OF DELETE
AS 
    DELETE dd FROM condition_tabs dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_group_tabs dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tab_columns dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tab_containers dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tabs_subprocesses dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id
    DELETE dd FROM process_tabs dd			INNER JOIN deleted d ON dd.process_tab_id = d.process_tab_id

GO
/****** Object:  Trigger [dbo].[process_translations_AfterUpdateRow]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[process_translations_AfterUpdateRow] ON [dbo].[process_translations] AFTER UPDATE  AS

UPDATE upd SET upd.variable = i.variable FROM conditions upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM instance_menu upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id where d.process = null

UPDATE upd SET upd.variable = i.variable FROM instances upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id where d.process = null

UPDATE upd SET upd.variable = i.variable FROM item_columns upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM notification_conditions upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM process_actions upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.dialog_title_variable = i.variable FROM process_actions_dialog upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.dialog_title_variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id
UPDATE upd SET upd.dialog_text_variable = i.variable FROM process_actions_dialog upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.dialog_text_variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id
UPDATE upd SET upd.cancel_button_variable = i.variable FROM process_actions_dialog upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.cancel_button_variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id
UPDATE upd SET upd.submit_button_variable = i.variable FROM process_actions_dialog upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.submit_button_variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM process_containers upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM process_groups upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id
UPDATE upd SET upd.short_variable = i.variable FROM process_groups upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.short_variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM process_portfolios upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM process_tabs upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

UPDATE upd SET upd.variable = i.variable FROM processes upd
INNER JOIN deleted d ON upd.instance = d.instance AND upd.process = d.process AND upd.variable = d.variable 
INNER JOIN inserted i ON d.process_translation_id = i.process_translation_id

GO
/****** Object:  Trigger [dbo].[item_AddProcess]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[item_AddProcess] ON [dbo].[processes] AFTER insert AS
DECLARE @instance NVARCHAR(10)
DECLARE @process NVARCHAR(50)

select @instance = instance, @process = process from inserted
	exec items_buildTable @instance, @process

GO
/****** Object:  Trigger [dbo].[processes_fk_delete]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  TRIGGER [dbo].[processes_fk_delete]
    ON [dbo].[processes]
    INSTEAD OF DELETE
AS 
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_subprocesses dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.parent_process = d.process
	DELETE dd FROM item_columns dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM item_tables dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM items dd						INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_groups	dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_tabs dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_containers dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
    DELETE dd FROM process_portfolios dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_tabs_subprocesses dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_translations dd		INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM conditions dd				INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_actions dd			INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process
	DELETE dd FROM process_dashboard_charts dd	INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process

	DELETE dd FROM processes dd					INNER JOIN deleted d ON dd.instance = d.instance AND dd.process = d.process

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Is not Dayoff; 1 = Is dayoff' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'calendar_data', @level2type=N'COLUMN',@level2name=N'dayoff'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Is not national holiday; 1 = Is national holiday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'calendar_data', @level2type=N'COLUMN',@level2name=N'holiday'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Work; 1 = NotWork' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'calendar_data', @level2type=N'COLUMN',@level2name=N'notwork'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = server message; 1= server exception; 2 = client message; 3 = client exception' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'instance_logs', @level2type=N'COLUMN',@level2name=N'log_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = information; 1 = warning; 2 = error; 3 = data loss' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'instance_logs', @level2type=N'COLUMN',@level2name=N'log_severity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=item_id, 1=column, 2=date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'item_column_joins', @level2type=N'COLUMN',@level2name=N'parent_column_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=item_id, 1=column, 2=date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'item_column_joins', @level2type=N'COLUMN',@level2name=N'sub_column_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=NVARCHAR(255), 1=INT, 2=FLOAT, 3=DATE, 4=NVARCHAR(MAX)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'item_columns', @level2type=N'COLUMN',@level2name=N'data_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=not deleted, 1=deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'items', @level2type=N'COLUMN',@level2name=N'deleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=customer, 1=support, 2=data' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'processes', @level2type=N'COLUMN',@level2name=N'process_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=itemColumn, 1=staticColumn (eg. item_id)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'state_rights', @level2type=N'COLUMN',@level2name=N'user_column_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = backlog; 1 = sprints' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'task_parents', @level2type=N'COLUMN',@level2name=N'type_id'
GO

/****** Object:  StoredProcedure [dbo].[app_ensure_process_bulletins]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_bulletins]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	DECLARE @process NVARCHAR(50) = 'bulletins'
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	SET @tableName = '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create group
	DECLARE @g INT
	EXEC app_ensure_group @instance,@process,'BULLETINS', @group_id = @g OUTPUT

	--Create tabs
	DECLARE @t INT
	EXEC app_ensure_tab @instance,@process,'BULLETIN', @g, @tab_id = @t OUTPUT

	--Create containers
	DECLARE @c INT
	EXEC app_ensure_container @instance,@process,'BULLETIN_CONT', @t, 1, @container_id = @c OUTPUT

	--Create portfolios
	DECLARE @p INT
	EXEC app_ensure_portfolio @instance,@process,'BULLETINS', @portfolio_id = @p OUTPUT
	
	PRINT 'PORTFOLIO_ID'
	PRINT @p

	--Create configuration
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'Bulletins portfolio ID', @p, 0, null

	--Create conditions
	EXEC app_ensure_condition @instance, @process, @g, @p

	--Create columns
	EXEC app_ensure_column @instance, @process, 'title',	@c, 0, 1, 1, 0, null, @p, 0
	EXEC app_ensure_column @instance, @process, 'message',	@c, 4, 1, 1, 0, null, 0, 0
	EXEC app_ensure_column @instance, @process, 'sender',	@c, 5, 1, 1, 0, null, @p, 0, 'user'
	EXEC app_ensure_column @instance, @process, 'created',	@c, 3, 1, 1, 0, null,  @p, 0

	EXEC app_ensure_column @instance, @process, 'language', @c, 0, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'notify',	@c, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'expiration', @c, 3, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'modified',	@c, 3, 1, 0, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'status',	@c, 1, 1, 1, 0, null,  0, 0
	EXEC app_ensure_column @instance, @process, 'delta',	@c, 4, 1, 1, 0, null, 0, 0

END;

GO

/****** Object:  StoredProcedure [dbo].[app_ensure_process_task]    Script Date: 01/08/2017 14.18.26 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[app_ensure_process_task]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'task'
	DECLARE @link_process NVARCHAR(50) = 'task_link_type'
	DECLARE @tableName NVARCHAR(50) =  '_' + @instance + '_' + @process
	DECLARE @link_process2 NVARCHAR(50) = 'task_user_effort'


	--Archive user is 0
	DECLARE @BinVar varbinary(128)
	SET @BinVar = CONVERT(varbinary(128), 0)
	SET CONTEXT_INFO @BinVar;

	--Create task process
	EXEC app_ensure_process @instance, @process, @process, 1
	
	-- CREATE link process and columns
	EXEC app_ensure_process @instance, @link_process, @link_process, 4
	DECLARE @li1 INT
	EXEC app_ensure_column @instance, @link_process, 'link_type', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @li1 OUTPUT
	UPDATE item_columns SET status_column = 1 WHERE item_column_id = @li1
	

	-- CREATE link process for user effort and columns
	EXEC app_ensure_process @instance, @link_process2, @link_process2, 4
	DECLARE @li2 INT
	EXEC app_ensure_column @instance, @link_process2, 'effort', 0, 2, 1, 1, 0, null,  0, 0, @item_column_id = @li2 OUTPUT
	UPDATE item_columns SET status_column = 1 WHERE item_column_id = @li2
	

	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance, @process,'TASK', @portfolio_id = @p1 OUTPUT


	--Create configurations
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'BacklogPortfolioId', @p1, 0, null
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'SprintsPortfolioId', @p1, 0, null
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'GanttPortfolioId', @p1, 0, null
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'KanbanPortfolioId', @p1, 0, null


	--Create phases
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'BACKLOG', @group_id = @g1 OUTPUT

	DECLARE @g2 INT
	EXEC app_ensure_group @instance,@process,'SPRINT', @group_id = @g2 OUTPUT

	DECLARE @g3 INT
	EXEC app_ensure_group @instance,@process,'PHASE_TASK', @group_id = @g3 OUTPUT

	DECLARE @g4 INT
	EXEC app_ensure_group @instance,@process,'KANBAN', @group_id = @g4 OUTPUT

	DECLARE @g5 INT
	EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g5 OUTPUT

	DECLARE @g6 INT
	EXEC app_ensure_group @instance,@process,'SPRINT_TASK', @group_id = @g6 OUTPUT

	DECLARE @g7 INT
	EXEC app_ensure_group @instance,@process,'PHASE_TASK_SIGN', @group_id = @g7 OUTPUT
	
	DECLARE @g8 INT
	EXEC app_ensure_group @instance,@process,'PHASE_TASK_UNSIGN', @group_id = @g8 OUTPUT
	
	
	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'BACKLOG_BASIC_INFO', @g1, @tab_id = @t1 OUTPUT

	DECLARE @t2 INT
	EXEC app_ensure_tab @instance,@process,'SPRINT_BASIC_INFO', @g2, @tab_id = @t2 OUTPUT

	DECLARE @t3 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_TASK_BASIC_INFO', @g3, @tab_id = @t3 OUTPUT

	DECLARE @t4 INT
	EXEC app_ensure_tab @instance,@process,'KANBAN_BASIC_INFO', @g4, @tab_id = @t4 OUTPUT

	DECLARE @t5 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_BASIC_INFO', @g5, @tab_id = @t5 OUTPUT

	DECLARE @t6 INT
	EXEC app_ensure_tab @instance,@process,'SPRINT_TASK_BASIC_INFO', @g6, @tab_id = @t6 OUTPUT
	
	DECLARE @t7 INT
	EXEC app_ensure_tab @instance,@process,'SPRINT_COMPLETE', @g2, @tab_id = @t7 OUTPUT

	DECLARE @t8 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_TASK_SIGN', @g7, @tab_id = @t8 OUTPUT

	DECLARE @t9 INT
	EXEC app_ensure_tab @instance,@process,'PHASE_TASK_UNSIGN', @g8, @tab_id = @t9 OUTPUT


	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'BACKLOG_BASIC_INFO', @t1, 1, @container_id = @c1 OUTPUT

	DECLARE @c2 INT
	EXEC app_ensure_container @instance,@process,'SPRINT_BASIC_INFO', @t2, 1, @container_id = @c2 OUTPUT

	DECLARE @c3 INT
	EXEC app_ensure_container @instance,@process,'PHASE_TASK_BASIC_INFO', @t3, 1, @container_id = @c3 OUTPUT

	DECLARE @c4 INT
	EXEC app_ensure_container @instance,@process,'KANBAN_BASIC_INFO', @t4, 1, @container_id = @c4 OUTPUT

	DECLARE @c5 INT
	EXEC app_ensure_container @instance,@process,'PHASE_BASIC_INFO', @t5, 1, @container_id = @c5 OUTPUT

	DECLARE @c6 INT
	EXEC app_ensure_container @instance,@process,'SPRINT_TASK_BASIC_INFO', @t6, 1, @container_id = @c6 OUTPUT

	DECLARE @c7 INT
	EXEC app_ensure_container @instance,@process,'DESCRIPTION', 0, 1, @container_id = @c7 OUTPUT

	DECLARE @c8 INT
	EXEC app_ensure_container @instance,@process,'DESCRIPTION_WO_ATTACHMENT', 0, 1, @container_id = @c8 OUTPUT

	DECLARE @c9 INT
	EXEC app_ensure_container @instance,@process,'SIGN', @t8, 1, @container_id = @c9 OUTPUT

	DECLARE @c10 INT
	EXEC app_ensure_container @instance,@process,'UNSIGN', @t9, 1, @container_id = @c10 OUTPUT

	DECLARE @c11 INT
	EXEC app_ensure_container @instance,@process,'COMPLETE_SPRINT', @t7, 1, @container_id = @c11 OUTPUT

	DECLARE @c12 INT
	EXEC app_ensure_container @instance,@process,'SPRINT_TASK_BASIC_INFO', @t6, 1, @container_id = @c12 OUTPUT


	--Create columns
	EXEC app_ensure_column @instance, @process, 'status_id', 0, 1, 1, 1, 0, null,  0, 0
	
  	DECLARE @i19 INT
	EXEC app_ensure_column @instance, @process, 'parent_item_id', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i19 OUTPUT
	EXEC app_ensure_column @instance, @process, 'owner_item_id', 0, 0, 1, 1, 0, null,  0, 0
	
	
	
	DECLARE @i20 INT
	EXEC app_ensure_column @instance, @process, 'linked_tasks', 0, 6, 1, 1, 0, null,  0, 0, 'task', @item_column_id = @i20 OUTPUT
	UPDATE item_columns SET link_process = @link_process WHERE item_column_id = @i20
	
 	DECLARE @i17 INT
	EXEC app_ensure_column @instance, @process, 'owner_item_type', 0, 0, 1, 1, 0, null,  0, 0, @item_column_id = @i17 OUTPUT

	DECLARE @i6 INT
	EXEC app_ensure_column @instance, @process, 'title', 0, 0, 1, 1, 0, null, 0, 0, @item_column_id = @i6 OUTPUT

	DECLARE @i7 INT
	EXEC app_ensure_column @instance, @process, 'description', 0, 4, 1, 1, 0, null, 0, 0, @item_column_id = @i7 OUTPUT

	EXEC app_ensure_column @instance, @process, 'color', 0, 0, 1, 1, 0, null,  0, 0

  	DECLARE @i18 INT
	EXEC app_ensure_column @instance, @process, 'task_type', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i18 OUTPUT

	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'effort', 0, 16, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT

	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'start_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i2 OUTPUT

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'end_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i3 OUTPUT

	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'description', 0, 4, 1, 1, 0, null,  0, 0, @item_column_id = @i4 OUTPUT

	DECLARE @i5 INT
	EXEC app_ensure_column @instance, @process, 'status', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i5 OUTPUT

	DECLARE @i8 INT
	EXEC app_ensure_column @instance, @process, 'actual_effort', 0, 1, 1, 1, 0, null,  0, 0, @item_column_id = @i8 OUTPUT

	DECLARE @i9 INT
	EXEC app_ensure_column @instance, @process, 'responsibles', 0, 5, 0, 1, 0, null,  0, 0, 'user', @item_column_id = @i9 OUTPUT
	UPDATE item_columns SET link_process = @link_process2 WHERE item_column_id = @i9


	DECLARE @i10 INT
	EXEC app_ensure_column @instance, @process, 'actual_start_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i10 OUTPUT

	DECLARE @i11 INT
	EXEC app_ensure_column @instance, @process, 'actual_end_date', 0, 3, 1, 1, 0, null,  0, 0, @item_column_id = @i11 OUTPUT

	DECLARE @i12 INT
	EXEC app_ensure_column @instance, @process, 'result', 0, 4, 1, 1, 0, null,  0, 0, @item_column_id = @i12 OUTPUT

	DECLARE @i13 INT
	EXEC app_ensure_column @instance, @process, 'attachments', 0, 10, 1, 1, 0, null,  0, 0, @item_column_id = @i13 OUTPUT

	DECLARE @i14 INT
	EXEC app_ensure_column @instance, @process, 'complete_button', 0, 8, 0, 1, 0, null,  0, 0, null, @item_column_id = @i14 OUTPUT

	DECLARE @i15 INT
	EXEC app_ensure_column @instance, @process, 'sign_button', 0, 8, 0, 1, 0, null,  0, 0, null, @item_column_id = @i15 OUTPUT

	DECLARE @i16 INT
	EXEC app_ensure_column @instance, @process, 'unsign_button', 0, 8, 0, 1, 0, null,  0, 0, null, @item_column_id = @i16 OUTPUT

    EXEC app_ensure_column @instance, @process, 'bar_icon', 0, 6, 1, 1, 0, null, 0, 0, 'list_bar_icons' 

	DECLARE @i21 INT
	EXEC app_ensure_column @instance, @process, 'costs', 0, 16, 1, 1, 0, null,  0, 0, @item_column_id = @i1 OUTPUT
	
	UPDATE item_columns SET data_additional2 = 'close;close' WHERE item_column_id = @i14
	UPDATE item_columns SET data_additional2 = 'close;close' WHERE item_column_id = @i15
	UPDATE item_columns SET data_additional2 = 'close;close' WHERE item_column_id = @i16


	UPDATE item_columns SET data_additional = 'TaskEffort', data_additional2 = '{"@TaskEffortResponsiblesCol":' + CAST(@i19 as nvarchar) + '}' WHERE item_column_id = @i1
	UPDATE item_columns SET data_additional = 'TaskCost', data_additional2 = '"@TaskCostsResponsiblesCol":' + CAST(@i19 as nvarchar) + '}' WHERE item_column_id = @i21

	-- Assign tabs to phases
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i6, 1, 'V')
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i9, 2, 'W')
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i2, 3, 'X')
  INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, order_no)  VALUES (@p1, @i3, 4, 'Y')


	
	-- Assign containers to tabs

	-- BACKLOG
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t1, @c7, 2, 'W')

	-- SPRINT
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t2, @c7, 2, 'W')

	-- GANTT
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t3, @c7, 2, 'W')


	-- KANBAN
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t4, @c7, 2, 'W')

	-- PHASE
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t5, @c8, 2, 'W')

	-- SPRINT TASK
		INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@t6, @c7, 2, 'W')

		
	-- Assign fields to containers

	-- BACKLOG
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c1, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c1, @i1, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c1, @i21, null, null, 1)


	-- SPRINT
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i2, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"start"}', 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i3, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"end"}', 1)
   		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c2, @i9, null, null, 1)


	-- PHASE TASK
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i6, 'name', NULL , 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i2, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"start"}', 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i3, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"end"}', 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i1, null, null, 1)
  		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i9, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c3, @i21, null, null, 1)

	-- SPRINT TASK
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c12, @i6, 'name', NULL , 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c12, @i1, null, null, 1)
		
	-- KANBAN
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i1, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i8, null, null, 1)
	 	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i9, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c4, @i21, null, null, 1)

	-- PHASE
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c5, @i6, 'name', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c5, @i2, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"start"}', 0, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c5, @i3, null, '{"parentId":"' + CAST(@i3 AS nvarchar) + '","dateRange":"end"}', 0, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c5, @i1, null, null, 0, 1)


	-- DESCRIPTION
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c7, @i7, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c7, @i13, null, null, 1)

	-- DESCRIPTION w/o ATTACHMENT
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c8, @i7, null, null, 1)


	-- SPRINT COMPLETE
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c11, @i12, null, null, 1)

	-- SIGN
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i10, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"start"}', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i11, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"end"}', null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i8, null, null, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required) VALUES (@c9, @i12, null, null, 1)

	-- UNSIGN
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i10, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"start"}', null, 1, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i11, '{"parentId":"' + CAST(@i11 AS nvarchar) + '","dateRange":"end"}', null, 1, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i8, null, null, 1, 1)
		INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, read_only) VALUES (@c10, @i12, null, null, 1, 1)


	-- CREATE PROCESS ACTION

	-- COMPLETE
		DECLARE @pa1 INT
		EXEC app_ensure_action @instance, @process, 'ACTION_COMPLETE', @process_action_id = @pa1 OUTPUT
		EXEC app_ensure_action_row @pa1, 0, @i5, 0, 1
		UPDATE item_columns SET process_action_id = @pa1 WHERE item_column_id = CAST(@i14 AS nvarchar) OR item_column_id = CAST(@i15 AS nvarchar)


	-- UNCOMPLETE
		DECLARE @pa2 INT
		EXEC app_ensure_action @instance, @process, 'ACTION_UNCOMPLETE', @process_action_id = @pa2 OUTPUT
		EXEC app_ensure_action_row @pa2, 0, @i5, 0, 0
		UPDATE item_columns SET process_action_id = @pa2 WHERE item_column_id = CAST(@i16 AS nvarchar)


	  -- PROCESS ACTION DIALOGS
	  INSERT INTO process_actions_dialog(instance, process, name, type, dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable) VALUES (@instance, @process, 'SPRINT_COMPLETE', 2, 'ACIONDG_COMPLETE_SPRINT_TITLE', 'ACIONDG_COMPLETE_SPRINT_TEXT_', 'ACIONDG_COMPLETE_SPRINT_CANCEL', 'ACIONDG_COMPLETE_SPRINT_CONFIRM')
	  DECLARE @pad1 INT = @@identity
	
	
	  INSERT INTO process_actions_dialog(instance, process, name, type, dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable) VALUES (@instance, @process, 'GANTT_SIGN', 2, 'ACIONDG_GANTT_SIGN_TITLE', 'ACIONDG_GANTT_SIGN_TEXT_', 'ACIONDG_GANTT_SIGN_CANCEL', 'ACIONDG_GANTT_SIGN_CONFIRM')
	  DECLARE @pad2 INT = @@identity
	
	
	  INSERT INTO process_actions_dialog(instance, process, name, type, dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable) VALUES (@instance, @process, 'GANTT_UNSIGN', 2, 'ACIONDG_GANTT_UNSIGN_TITLE', 'ACIONDG_GANTT_UNSIGN_TEXT_', 'ACIONDG_GANTT_UNSIGN_CANCEL', 'ACIONDG_GANTT_UNSIGN_CONFIRM')
	  DECLARE @pad3 INT = @@identity


	--CONDITIONS

	--Create conditions
  	DECLARE @con13 INT
	EXEC app_ensure_condition @instance, @process, 0, 0, 'Portfolio', @condition_id = @con13 OUTPUT
  	INSERT INTO condition_features (condition_id, feature) VALUES (@con13, 'portfolio')
    DELETE FROM condition_features WHERE condition_id = @con13 AND feature = 'meta'

  	INSERT INTO condition_states (condition_id, state) VALUES (@con13, 'read')
  	INSERT INTO condition_states (condition_id, state) VALUES (@con13, 'write')
  	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con13, @p1)

	-- BACKLOG
	-- WRITE
  DECLARE @con1 INT
  DECLARE @consql NVARCHAR(MAX)
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"backlog"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_BACKLOG_WRITE', @consql, @condition_id = @con1 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con1, @g1)
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con1, @t1)

  
	-- READ
  DECLARE @con2 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"backlog"},"OperatorId":1}]},"OperatorId":1}]'

  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_BACKLOG_READ', @consql, @condition_id = @con2 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con2, 'read')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con2, @g1)
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con2, @t1)


	-- SPRINT
	-- WRITE
  DECLARE @con3 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_WRITE', @consql, @condition_id = @con3 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con3, 'write')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con3, @g2)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con3, @t2)

	-- READ
  DECLARE @con4 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_READ', @consql, @condition_id = @con4 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con4, 'read')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con4, @g2)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con4, @t2)


	-- PHASE TASK UNSIGN
	-- WRITE
  DECLARE @con5 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_UNSIGN_WRITE', @consql, @condition_id = @con5 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con5, 'write')
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con5, @t9)
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con5, @g8)

	-- PHASE TASK SIGN
	-- WRITE
  DECLARE @con7 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_SIGN_WRITE', @consql, @condition_id = @con7 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con7, 'write')
  INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con7, @t8)
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con7, @g7)

  -- KANBAN
	-- WRITE
  DECLARE @con9 INT
  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"0"},"OperatorId":1}]},"OperatorId":1}]'
  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_KANBAN_WRITE', @consql, @condition_id = @con9 OUTPUT
  INSERT INTO condition_states (condition_id, state) VALUES (@con9, 'write')
  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con9, @g4)
INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con9, @t4)

	-- PHASE
	-- WRITE
	  DECLARE @con11 INT
	  SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1}]},"OperatorId":1}]'
	  EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_WRITE', @consql, @condition_id = @con11 OUTPUT
	  INSERT INTO condition_states (condition_id, state) VALUES (@con11, 'write')
	  INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con11, @g5)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con11, @t5)

	-- SPRINT TASK BASIC INFO
	--WRITE
	DECLARE @con14 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_TASK_BASIC_INFO_WRITE', @consql, @condition_id = @con14 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con14, 'write')
  	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con14, @g6)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con14, @t6)

	--READ
	DECLARE @con15 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"21"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_SPRINT_TASK_BASIC_INFO_READ', @consql, @condition_id = @con15 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con15, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con15, @g6)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con15, @t6)


	-- PHASE TASK BASIC INFO
	--WRITE
	DECLARE @con16 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":6,"Value":"1"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_BASIC_INFO_WRITE', @consql, @condition_id = @con16 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con16, 'write')
  	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con16, @g3)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con16, @t3)

	--READ
	DECLARE @con17 INT
	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i17 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"gantt"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1},{"ConditionTypeId":4,"Condition":{"ParentItemColumnId": ' + CAST(@i19 AS nvarchar) + ', "ItemColumnId":' + CAST(@i18 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"2"},"OperatorId":1},{"ConditionTypeId":1,"Condition":{"ItemColumnId":' + CAST(@i5 AS nvarchar) + ',"ComparisonTypeId":3,"Value":"1"},"OperatorId":1}]}, "OperatorId":1}]'
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_TASK_PHASE_TASK_BASIC_INFO_READ', @consql, @condition_id = @con17 OUTPUT
  	INSERT INTO condition_states (condition_id, state) VALUES (@con17, 'read')
	INSERT INTO condition_groups (condition_id, process_group_id) VALUES (@con17, @g3)
	INSERT INTO condition_tabs (condition_id, process_tab_id) VALUES (@con17, @t3)


	UPDATE item_columns SET status_column = 1 WHERE @instance = @instance AND process = @process


	  -- INSERT DIALOG TO BUTTON
	UPDATE item_columns SET process_dialog_id = @pad1, process_action_id = @pa1, status_column = 0 WHERE instance = @instance AND process = @process AND item_column_id = @i14
	UPDATE item_columns SET process_dialog_id = @pad2, process_action_id = @pa1, status_column = 0 WHERE instance = @instance AND process = @process AND item_column_id = @i15
	UPDATE item_columns SET process_dialog_id = @pad3, process_action_id = @pa2, status_column = 0 WHERE instance = @instance AND process = @process AND item_column_id = @i16


	-- ASSIGN BUTTONS TO PHASES
	INSERT INTO process_tab_columns (process_tab_id, item_column_id, order_no) VALUES (@t7, @i14, 'V')
	INSERT INTO process_tab_columns (process_tab_id, item_column_id, order_no) VALUES (@t8, @i15, 'V')
	INSERT INTO process_tab_columns (process_tab_id, item_column_id, order_no) VALUES (@t9, @i16, 'V')

	DECLARE @sql nvarchar(MAX)
	
	SET @sql = 'ALTER TRIGGER ' + @tableName + '_AFTER_UPDATE 
	ON ' + @tableName + '
	AFTER UPDATE
	AS

	DECLARE @user_id int;
	select @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()));
    /*
    UPDATE [dbo].[_test_task]
    SET archive_start= GETDATE(),
    archive_userid=  @user_id
    WHERE [item_id] IN (SELECT DISTINCT [item_id] FROM Inserted)

	INSERT INTO archive__test_task([item_id],[order_no],[archive_userid],[archive_start],[status_id],[parent_item_id],[owner_item_id],[linked_tasks],[owner_item_type],[title],[description],[color],[task_type],[effort],[start_date],[end_date],[status],[actual_effort],[responsible_id],[actual_start_date],[actual_end_date],[result],[attachments],archive_end, archive_type)
	SELECT [item_id],[order_no],[archive_userid],[archive_start],[status_id],[parent_item_id],[owner_item_id],[linked_tasks],[owner_item_type],[title],[description],[color],[task_type],[effort],[start_date],[end_date],[status],[actual_effort],[responsible_id],[actual_start_date],[actual_end_date],[result],[attachments],
		archive_end = getdate(),
		archive_type = 1
	FROM deleted */'

	EXEC (@sql)
	
	SET @sql = 'CREATE TRIGGER ' + @tableName + '_AFTER_UPDATE_OWNER 
				ON ' + @tableName + '
				AFTER UPDATE
				AS

				DECLARE @user_id int;
				SELECT @user_id = convert(int,convert(varbinary(4),CONTEXT_INFO()))	

				DECLARE @owner_item_id int, @owner_item_type nvarchar(255)
				SELECT @owner_item_id = owner_item_id, @owner_item_type = owner_item_type FROM inserted;
	
				BEGIN
					WITH rec AS (
						SELECT t.item_id,(SELECT owner_item_id FROM _' + @instance + '_task t2 WHERE t.parent_item_id = t2.item_id) AS owner_item_id, 
							(SELECT owner_item_type FROM _' + @instance + '_task t2 WHERE t.parent_item_id = t2.item_id) AS owner_item_type
						FROM [dbo].[_' + @instance + '_task] t
						WHERE t.parent_item_id IN (SELECT item_id FROM inserted)
						UNION ALL
							SELECT t.item_id, r.owner_item_id AS owner_item_id, r.owner_item_type
							FROM [dbo].[_' + @instance + '_task] t
							INNER JOIN rec r ON t.parent_item_id = r.item_id
						)
					UPDATE st
					SET st.owner_item_id = rec.owner_item_id, st.owner_item_type = rec.owner_item_type
					FROM _' + @instance + '_task st
					INNER JOIN rec
					ON rec.item_id = st.item_id WHERE rec.item_id != (SELECT item_id FROM inserted)
				END'

	EXEC (@sql)

	SET @sql = 'create FUNCTION  [dbo].[get_' + @instance + '_recursive_tasks](@itemId int )
		RETURNS @rtab TABLE ([item_id] int NOT NULL )
		AS
		BEGIN	
				WITH rec AS (
			             SELECT t.*
			             FROM _' + @instance + '_task t
			             WHERE t.item_id = @itemId
			             UNION ALL 
			             SELECT t.* 
			             FROM _' + @instance + '_task t 
			             INNER JOIN rec r ON r.item_id = t.parent_item_id) 

			INSERT @rtab
				
			             SELECT item_id FROM rec
			RETURN
		END' 

		EXEC (@sql)

	DECLARE @p2 INT
	DECLARE @con18 INT

	EXEC app_ensure_portfolio @instance, @process,'TASK_MY_TASKS', @portfolio_id = @p2 OUTPUT
	INSERT INTO process_portfolio_columns(process_portfolio_id, item_column_id) (SELECT @p2, item_column_id FROM item_columns WHERE instance = @instance AND process = @process and name <> 'bar_icons')

	SET @consql = '[{"OperationGroup":{"Operations":[{"ConditionTypeId":3,"Condition":{"ComparisonTypeId":3,"ItemColumnId":' + CAST(@i9 AS NVARCHAR) +' ,"Value":"","UserColumnTypeId":2},"OperatorId":null}]},"OperatorId":1},{"OperationGroup":{"Operations":[{"ConditionTypeId":1,"Condition":{"ComparisonTypeId":3,"ItemColumnId":'+ CAST(@i18 AS NVARCHAR) +  ',"Value":"21"},"OperatorId":null},{"ConditionTypeId":4,"Condition":{"ComparisonTypeId":6,"ParentItemColumnId":' + CAST(@i19 AS NVARCHAR) + ',"ItemColumnId":' + CAST(@i18 AS NVARCHAR) + ',"Value":"21"},"OperatorId":2}]},"OperatorId":1}]'
	
	EXEC app_ensure_condition @instance, @process, 0, 0, 'CONDITION_MY_TASKS', @consql, @condition_id = @con18 OUTPUT

	INSERT INTO condition_states (condition_id, state) VALUES (@con18, 'write'), (@con18, 'read')
    DELETE FROM condition_features WHERE condition_id = @con18 AND feature = 'meta'

	INSERT INTO condition_features (condition_id, feature) VALUES (@con18, 'portfolio')
	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con18, @p2)

	EXEC app_ensure_configuration @instance, null, null, 'Tasks', 'MyTasksPortfolioId', @p2, 0, null
	
	EXEC app_ensure_process_list_bar_icons @instance
	
END

GO

/****** Object:  StoredProcedure [dbo].[app_ensure_process_user]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_user]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	DECLARE @process NVARCHAR(50) = 'user'
	DECLARE @combine TINYINT = 0
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	SET @tableName = '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	--Create group
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'USER', @group_id = @g1 OUTPUT

	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'USER_TAB1', @g1, @tab_id = @t1 OUTPUT

	DECLARE @t2 INT
	EXEC app_ensure_tab @instance,@process,'ORGANISATION_MANAGEMENT', @g1, @tab_id = @t2 OUTPUT

	INSERT INTO process_group_features (process_group_id, feature, state) VALUES (@g1, 'USER_ACCOUNT_INFORMATION', 'process.user')

	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'USER_CONT1', 0, 1, @container_id = @c1 OUTPUT

	DECLARE @c2 INT
	EXEC app_ensure_container @instance,@process,'USER_CONT2', @t1, 1, @container_id = @c2 OUTPUT

	DECLARE @c3 INT
	EXEC app_ensure_container @instance,@process,'USER_CONT3', @t1, 2, @container_id = @c3 OUTPUT

	DECLARE @c4 INT
	EXEC app_ensure_container @instance,@process,'USER_CONT4', 0, 2, @container_id = @c4 OUTPUT

	DECLARE @c5 INT
	EXEC app_ensure_container @instance,@process,'USER_NEW_PROCESS_1', 0, 2, @container_id = @c5 OUTPUT

	DECLARE @c6 INT
	EXEC app_ensure_container @instance,@process,'ORGANISATION_MANAGEMENT', @t2, 1, @container_id = @c6 OUTPUT

	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'USER', @portfolio_id = @p1 OUTPUT

	--Create configuration
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'User portfolio ID', @p1, 0, null
	
	
	  --Create conditions
	DECLARE @con1 INT
 	EXEC app_ensure_condition @instance, @process, @g1, @p1, 'Porfolio', 'null', @condition_id = @con1 OUTPUT 
	INSERT INTO condition_features (condition_id, feature) VALUES (@con1, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'read')
	INSERT INTO condition_states (condition_id, state) VALUES (@con1, 'write')

	DECLARE @con2 INT
 	EXEC app_ensure_condition @instance, @process, 0, null, 'Password change admin', 'null', @condition_id = @con2 OUTPUT 
	INSERT INTO condition_containers (condition_id, process_container_id) VALUES (@con2, @c1)
	INSERT INTO condition_states (condition_id, state) VALUES (@con2, 'password')
    INSERT INTO condition_user_groups (condition_id, item_id) VALUES (@con2, 2)
		
	DECLARE @con3 INT
 	EXEC app_ensure_condition @instance, @process, null, null, 'User group change', 'null', @condition_id = @con3 OUTPUT 
    INSERT INTO condition_features (condition_id, feature) VALUES (@con3, 'USER_ACCOUNT_INFORMATION')
	INSERT INTO condition_states (condition_id, state) VALUES (@con3, 'read')
	INSERT INTO condition_states (condition_id, state) VALUES (@con3, 'write')
	INSERT INTO condition_user_groups (condition_id, item_id) VALUES (@con3, 2)
	
	--Create columns
	DECLARE @i1 INT
	EXEC app_ensure_column @instance, @process, 'last_name', @c2, 0, 1, 1, 0, null,  @p1, 1, 1, @item_column_id = @i1 OUTPUT

	DECLARE @i2 INT
	EXEC app_ensure_column @instance, @process, 'first_name', @c2, 0, 1, 1, 0, null,  @p1, 1, 1, @item_column_id = @i2 OUTPUT

	DECLARE @i3 INT
	EXEC app_ensure_column @instance, @process, 'email', @c2, 0, 1, 0, 0, null, @p1, 1, 1, @item_column_id = @i3 OUTPUT

	EXEC app_ensure_column @instance, @process, 'domain', 0, 0, 1, 1, 0, null, @p1, 0, 0

	DECLARE @i6 INT
	EXEC app_ensure_column @instance, @process, 'login', @c1, 0, 1, 1, 0, null, 0, 0, 0, @item_column_id = @i6 OUTPUT

	DECLARE @i4 INT
	EXEC app_ensure_column @instance, @process, 'active', @c1, 0, 1, 1, 0, null,  @p1, 0, 0, @item_column_id = @i4 OUTPUT

	DECLARE @i5 INT
	EXEC app_ensure_column @instance, @process, 'password', @c1, 7, 1, 0, 0, null,  0, 0, 0, @item_column_id = @i5 OUTPUT

	DECLARE @i7 INT
	EXEC app_ensure_column @instance, @process, 'password_changed', 0, 1, 1, 1, 0, null,  0, 0, 0, @item_column_id = @i7 OUTPUT
	EXEC app_ensure_column @instance, @process, 'token', 0, 0, 1, 0, 0, null, 0, 0, 0

	EXEC app_ensure_column @instance, @process, 'favourites', 0, 5, 1, 0, 0, null, 0, 0, 'user'
	EXEC app_ensure_configuration @instance, null, null, 'Main application', 'FavouriteColumnId', @i1, 0, null
	
	EXEC app_ensure_column @instance, @process, 'date_format', @c3, 0, 1, 0, 1, 'DATE_FORMATS',  0, 0, 0
	EXEC app_ensure_column @instance, @process, 'first_day_of_week', @c3, 1,  1, 0, 1, 'FIRST_DAY_OF_WEEK', 0, 0, 0
	EXEC app_ensure_column @instance, @process, 'timezone', @c3, 0, 1, 0, 1, 'TIMEZONES', 0, 0, 0
	EXEC app_ensure_column @instance, @process, 'locale_id', @c3, 0, 1, 0,  1, 'LANGUAGES', 0, 0, 0

	EXEC app_ensure_column @instance, @process, 'base_calendar_id', @c6, 1, 1, 1, 0, null,  0, 0, 0
	EXEC app_ensure_column @instance, @process, 'calendar_id', @c6, 1, 1, 1, 0, null,  0, 0, 0

	-- Assign fields to containers (new process)
	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no) VALUES (@c5, @i2, null, 0, 1, 'V');
	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no) VALUES (@c5, @i1, null, 0, 1, 'W');
	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no) VALUES (@c5, @i3, null, 0, 1, 'X');
	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no) VALUES (@c5, @i6, null, 0, 1, 'Y');
	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no) VALUES (@c5, @i5, null, 0, 1, 'Z');
	INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, required, order_no) VALUES (@c5, @i4, null, 0, 1, 'ZV');


	-- Assign container to new process dialog
	--INSERT INTO process_portfolio_dialogs (instance, process, process_container_id, order_no, variable) VALUES (@instance, @process, @c5, 'V', NULL)

	
    DECLARE @process_group_id INT
    SELECT @process_group_id = process_group_id FROM process_groups WHERE instance = @instance AND process = 'user' AND variable = 'USER';

    DECLARE @process_container_id INT
    SELECT @process_container_id = ISNULL(process_container_id, 0) FROM process_containers WHERE instance = @instance AND process = 'user' AND variable = 'WORKHOUR_APPROVAL';

    IF (@process_container_id IS NULL)
        BEGIN
            --Create new User process tab

            DECLARE @new_tab_id INT
            EXEC app_ensure_tab @instance,'user','USER_TAB3', @process_group_id, @tab_id = @new_tab_id OUTPUT

            --Create new User process container		
            EXEC app_ensure_container @instance,'user','WORKHOUR_APPROVAL', @new_tab_id, 1, @container_id = @process_container_id OUTPUT
        END

    --Create competency column
    EXEC app_ensure_column @instance, 'user', 'hour_approval_users', @process_container_id, 5, 1, 1, 0, null,  0, 0, 'user'

    INSERT INTO process_translations
    ([instance], process, code, variable, [translation])
    VALUES(@instance, 'user', 'en-GB', 'USER_TAB3', 'Worktime Approval')

    INSERT INTO process_translations
    ([instance], process, code, variable, [translation])
    VALUES(@instance, 'user', 'en-GB', 'WORKHOUR_APPROVAL', 'Worktime Approval')

    INSERT INTO process_translations
    ([instance], process, code, variable, [translation])
    VALUES(@instance, 'user', 'en-GB', 'USER_HOUR_APPROVAL_USERS', 'Supervised users')
    

	-- Portfolio selector
	INSERT INTO process_portfolios (instance, process, variable, process_portfolio_type, calendar_type, calendar_start_id, calendar_end_id, default_state) VALUES (@instance, @process, 'USER_SELECTOR', 1, 0, 0, 0, null)
	
	DECLARE @selectorId INT = @@identity;
	INSERT into process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result) VALUES (@selectorId, @i1,1)
	INSERT into process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result) VALUES (@selectorId, @i2,1)
	INSERT into process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result) VALUES (@selectorId, @i3,2)
	
  	INSERT INTO condition_portfolios (condition_id, process_portfolio_id) VALUES (@con1, @selectorId)

	-- CREATE NEW PROCESS ACTION
	DECLARE @pa1 INT
	EXEC app_ensure_action @instance, @process, 'NEW_USER', @process_action_id = @pa1 OUTPUT
	EXEC app_ensure_action_row @pa1, 0, @i5, 0, 1
	UPDATE processes SET new_row_action_id = @pa1 WHERE process = 'user'

	--Create First User
	DECLARE @loginColumnId INT
	DECLARE @passwordColumnId INT
	DECLARE @itemId INT

	SELECT @loginColumnId = item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = 'login';
	SELECT @passwordColumnId = item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = 'password';

	--IF ((SELECT count(*) FROM item_data WHERE item_column_id = @loginColumnId AND data_nvarchar = 'admin') = 0) BEGIN
		INSERT INTO items (instance, process) VALUES (@instance, @process);
		SET @itemId = @@identity;
	--	INSERT INTO item_data (item_id, item_column_id, data_nvarchar) VALUES (@itemId, @loginColumnId, 'admin');
	--	INSERT INTO item_data (item_id, item_column_id, data_nvarchar) VALUES (@itemId, @passwordColumnId, '246111281122');
		SET @sql = 'UPDATE ' + @tableName + ' SET login = ''admin'', password = ''32000,18,x4yZLtdFDNE5W4NGAKGmpibfZlELmT09,xf+a7j3YcHdSBmXT03vbvGNs'', first_name=''Admin'', last_name=''Admin'', locale_id=''en-GB'', active = 1 WHERE item_id = ' +  CAST(@itemId AS nvarchar)
		EXEC (@sql)

	--END

	-- Create calendar
	INSERT INTO calendars (instance, calendar_name, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, base_calendar) VALUES (@instance, 'Default calendar', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
	DECLARE @baseCalendarId INT = @@identity;

	INSERT INTO calendars (instance, calendar_name, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, base_calendar) VALUES (@instance, 'Admin Admin', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	DECLARE @calendarId INT = @@identity;

	SET @sql = 'UPDATE ' + @tableName + ' SET base_calendar_id = '  +  CAST(@baseCalendarId AS nvarchar)  + ', calendar_id = '  +  CAST(@calendarId AS nvarchar)  + ' WHERE item_id = ' +  CAST(@itemId AS nvarchar)
	EXEC (@sql)

END

GO

/****** Object:  StoredProcedure [dbo].[app_ensure_process_user_group]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_process_user_group]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @tab_id INT, @item_column_id INT
	DECLARE @process NVARCHAR(50) = 'user_group'
	DECLARE @combine TINYINT = 0
	DECLARE @tableName NVARCHAR(50) = ''
	DECLARE @sql NVARCHAR(MAX) = ''

	SET @tableName = '_' + @instance + '_' + @process

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	--Create process
	EXEC app_ensure_process @instance, @process, @process, 1

	
	--Create group
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'USER_GROUP', @group_id = @g1 OUTPUT

	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'USER_GROUP_TAB1', @g1, @tab_id = @t1 OUTPUT

	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'USER_CONT1', @t1, 1, @container_id = @c1 OUTPUT

	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'USER_GROUPS', @portfolio_id = @p1 OUTPUT
	
	--Create conditions
	EXEC app_ensure_condition @instance, @process, @g1, @p1

	--Create columns
	EXEC app_ensure_column @instance, @process, 'usergroup_name', @c1, 0, 1, 1, 0, null, @p1, 0
	EXEC app_ensure_column @instance, @process, 'user_id', @c1, 5, 1, 1, 0, null, @p1, 0, 'user'

	--Create First User group
	DECLARE @nameColumnId INT
	DECLARE @userColumnId INT
	DECLARE @itemId INT

	SELECT @nameColumnId = item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = 'usergroup_name';
	SELECT @userColumnId = item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = 'user_id';


		INSERT INTO items (instance, process) VALUES (@instance, @process);
		SET @itemId = @@identity;

		SET @sql = 'UPDATE ' + @tableName + ' SET usergroup_name = ''Administrators'', user_id = ''Admin'' WHERE item_id = ' +  CAST(@itemId AS nvarchar) 
		EXEC (@sql)
		SET @sql = 'INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES(' + CAST(@itemId AS nvarchar)  + ',' + CAST(@userColumnId AS nvarchar) + ', (SELECT item_id FROM _' + @instance + '_user WHERE login=''admin''))'
		EXEC (@sql)

	-- END

END;
GO

/****** Object:  StoredProcedure [dbo].[app_ensure_instance]    Script Date: 01/08/2017 14.18.26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[app_ensure_instance]
	@instance NVARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	--Archive user is 0
	DECLARE @BinVar varbinary(128);
	SET @BinVar = CONVERT(varbinary(128), 0);
	SET CONTEXT_INFO @BinVar;

	IF [dbo].[InstanceExists](@instance) = 0 BEGIN
		INSERT INTO instances (instance) VALUES(@instance)
	END
	IF [dbo].[InstanceLanguageExists](@instance, 'en-GB') = 0 BEGIN
		INSERT INTO instance_languages (instance, code, name, abbr) VALUES(@instance, 'en-GB', 'english', 'eng')
	END
	IF [dbo].[InstanceLanguageExists](@instance, 'fi-FI') = 0 BEGIN
		INSERT INTO instance_languages (instance, code, name, abbr) VALUES(@instance, 'fi-FI', 'finnish', 'fi')
	END
	exec app_ensure_process_user @instance
	exec app_ensure_process_user_group @instance
	exec app_ensure_instance_menu @instance
	exec app_ensure_process_task @instance
	exec app_ensure_process_bulletins @instance
	exec app_ensure_process_notification @instance
	exec app_ensure_process_notification_history @instance
	exec app_ensure_process_notification_tag_links @instance
	exec app_ensure_process_notification_tags @instance
	exec app_ensure_process_comment @instance
	exec app_ensure_process_allocation @instance
	
	--exec app_ensure_conditions @instance
END

GO
