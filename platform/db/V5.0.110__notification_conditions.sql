﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[notification_conditions]') AND name = 'condition_type') 
    ALTER TABLE notification_conditions
          ADD condition_type tinyint NOT NULL
          CONSTRAINT nc_condition_type_df DEFAULT 0
    WITH VALUES
