EXEC app_set_archive_user_id 0

SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(MAX);
	DECLARE @TableName NVARCHAR(MAX);
	DECLARE @GetTables CURSOR

	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT instance FROM instances
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @TableName

DECLARE @cnt INT = 0;



	WHILE (@@FETCH_STATUS=0) BEGIN
		--select * from _waltham_task where parent_item_id not in (select item_id from items)
		SET @sql = 'delete from items where item_id in (select item_id from _' + @TableName + '_task where parent_item_id not in (select item_id from items)) '
		print @sql
		
		SET @cnt = 0
		
		WHILE @cnt < 15
    BEGIN
      SET @cnt = @cnt + 1;
		    exec (@sql)
    END;

		FETCH NEXT FROM @GetTables INTO @TableName
	END
	CLOSE @GetTables



UPDATE item_columns SET use_fk = 1 WHERE NAME = 'parent_item_id' AND process='task'
