SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_customer]	-- Add the parameters for the stored procedure here
  @instance nvarchar(10), 	@process nvarchar(50), @processType as int, @colour nvarchar(50), @listProcess int,  @listOrder nvarchar(50), @newRowActionId int
AS
BEGIN
  DECLARE @tab_id INT, @item_column_id INT
  --DECLARE @process NVARCHAR(50) = 'user'
  DECLARE @combine TINYINT = 0
  DECLARE @tableName NVARCHAR(50) = ''
  DECLARE @sql NVARCHAR(MAX) = ''

  SET @tableName = '_' + @instance + '_' + @process

  --Archive user is 0
  DECLARE @BinVar varbinary(128);
  SET @BinVar = CONVERT(varbinary(128), 0);
  SET CONTEXT_INFO @BinVar;

  IF NOT EXISTS (SELECT * FROM processes WHERE instance = @instance and process = @process)
    BEGIN

      --Create process
      EXEC app_ensure_process @instance, @process, @process, @processType, @colour, @listProcess, @listOrder, @newRowActionId

      IF @processType = 0
        BEGIN

          --Create group
          DECLARE @g1 INT
          DECLARE @var nvarchar(255) = 'PHASE_' + @process + '_PHASE'
          EXEC app_ensure_group @instance,@process, @var, @group_id = @g1 OUTPUT

          --Create tabs
          DECLARE @t1 INT
          SET @var = 'TAB_' + @process + '_TAB'
          EXEC app_ensure_tab @instance,@process, @var, @g1, @tab_id = @t1 OUTPUT

          --Create containers
          DECLARE @c1 INT
          SET @var = '_CONTAINER' + @process + '_CONTAINER'
          EXEC app_ensure_container @instance,@process, @var, @t1, 1, @container_id = @c1 OUTPUT

          --Create portfolios
          DECLARE @p1 INT
          SET @var = 'PORTFOLIO_' + @process + '_PORTFOLIO'
          EXEC app_ensure_portfolio @instance,@process,@var, @portfolio_id = @p1 OUTPUT

          --Create conditions
          DECLARE @cId INT
          SET @var = 'CONDITION_' + @process + '_ALL'
          EXEC app_ensure_condition @instance, @process, @g1, @p1, @var, 'null', @condition_id = @cId OUTPUT

          INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
          INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
          INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')

          --Create columns
          EXEC app_ensure_column @instance, @process, 'title', @c1, 0, 1, 1, 0, null, @p1, 0, 0

 		   DECLARE @i1 INT	
			DECLARE @historyDateColName as nvarchar(max) = 'g_' + CAST(@g1 AS NVARCHAR) +  '_date'	
          EXEC app_ensure_column @instance, @process, @historyDateColName, 0, 13, 1, 1, 0, null, 0, 0, 0, @item_column_id = @i1 OUTPUT

			update process_groups set history_date_col = @i1 WHERE process_group_id = @g1

          --Create menu
          DECLARE @instanceMenuId INT,
          @instanceMenuId2 INT

          INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params) VALUES(@instance, null, @process, '0', null, '{}')
          SET @instanceMenuId = @@IDENTITY

          INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, link_type) VALUES(@instance, @instanceMenuId, 'NEW', '0', 'new.process', '{"process":"' + @process + '"}', 2)
          SET @instanceMenuId2 = @@IDENTITY
          SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group ORDER BY item_id ASC)); '
          EXEC (@sql)

          INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, link_type) VALUES(@instance, @instanceMenuId, 'PORTFOLIO', '0', 'portfolio', '{"process":"' + @process + '","portfolioId":' + cast(@p1 as nvarchar(max)) + '}', 1)
          SET @instanceMenuId2 = @@IDENTITY
          SET @sql = ' INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(' + CAST(@instanceMenuId2 AS NVARCHAR) + ', (SELECT TOP 1 item_id FROM _' + @instance + '_user_group ORDER BY item_id ASC)); '
          EXEC (@sql)
        END

    END

END
GO