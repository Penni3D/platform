SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[app_ensure_process_waltham_costs]	-- Add the parameters for the stored procedure here
	@instance nvarchar(10)
AS
BEGIN
	DECLARE @process NVARCHAR(50) = 'waltham_costs'

	exec app_set_archive_user_id 0

	EXEC app_ensure_process @instance, @process, @process, 1
	
	--Create group
	DECLARE @g1 INT
	EXEC app_ensure_group @instance,@process,'PHASE', @group_id = @g1 OUTPUT

	--Create tabs
	DECLARE @t1 INT
	EXEC app_ensure_tab @instance,@process,'TAB', @g1, @tab_id = @t1 OUTPUT

	--Create containers
	DECLARE @c1 INT
	EXEC app_ensure_container @instance,@process,'CONTAINER', @t1, 1, @container_id = @c1 OUTPUT

	--Create portfolios
	DECLARE @p1 INT
	EXEC app_ensure_portfolio @instance,@process,'PORTFOLIO', @portfolio_id = @p1 OUTPUT
	
	update process_portfolios set process_portfolio_type = 1 where instance = @instance and process = @process and process_portfolio_id = @p1

	--Create conditions
	DECLARE @cId INT	
	EXEC app_ensure_condition @instance, @process, @g1, @p1, 'ALL', 'null', @condition_id = @cId OUTPUT

	INSERT INTO condition_features (condition_id, feature) VALUES (@cId, 'portfolio')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'write')
	INSERT INTO condition_states (condition_id, state) VALUES (@cId, 'read')

	--Create columns
	EXEC app_ensure_column @instance, @process, 'owner_item_id', @c1, 1, 1, 1, 0, null, 0, 0, 0
	EXEC app_ensure_column @instance, @process, 'owner_item_type', @c1, 1, 1, 1, 0, null, 0, 0, 0

	EXEC app_ensure_column @instance, @process, 'title', @c1, 0, 1, 1, 0, null, @p1, 0, 0
	EXEC app_ensure_column @instance, @process, 'value', @c1, 2, 1, 1, 0, null, @p1, 0, 0
	
	EXEC app_ensure_column @instance, @process, 'desc', @c1, 0, 1, 1, 0, null, @p1, 0, 0
	
	update item_columns set status_column = 1 where instance = @instance and process = @process	

END;
GO

	DECLARE @GetTables CURSOR
  DECLARE @instance NVARCHAR (10)
  DECLARE @process NVARCHAR(50) = 'waltham_costs'
  DECLARE @c1 INT = 0 
  DECLARE @p1 INT = 0 
    
	--Get Tables
	SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR 
	SELECT instance FROM processes WHERE process = @process 
	
	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @instance

	WHILE (@@FETCH_STATUS=0) BEGIN
	  SELECT top 1 @c1=process_container_id FROM process_containers WHERE instance = @instance AND process = @process 
	    SELECT top 1 @p1=process_portfolio_id FROM process_portfolios WHERE instance = @instance AND process = @process 
	  EXEC app_ensure_column @instance, @process, 'description', @c1, 0, 1, 1, 0, null, @p1, 0, 0

		FETCH NEXT FROM @GetTables INTO @instance
	END
	CLOSE @GetTables

