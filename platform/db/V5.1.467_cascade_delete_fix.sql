UPDATE item_columns SET use_fk = 0 WHERE use_fk = 1 AND data_type <> 1



GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  TRIGGER [dbo].[items_fk_delete]
    ON [dbo].[items]
    INSTEAD OF DELETE
    AS
    SET XACT_ABORT OFF

DECLARE @ids NVARCHAR(MAX) = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM deleted FOR XML PATH(''))
DECLARE @sql NVARCHAR(MAX)
DECLARE @tName NVARCHAR(MAX)
DECLARE @cName NVARCHAR(MAX)
DECLARE @lvl AS INT = 0

IF OBJECT_ID('tempdb..#DeleteItems') IS NOT NULL DROP TABLE #DeleteItems
CREATE TABLE #DeleteItems (item_id INT, ord INT)

DECLARE @GetTables CURSOR
SET @GetTables = CURSOR FAST_FORWARD LOCAL FOR
    SELECT ccu.TABLE_NAME, ccu.COLUMN_NAME 
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
    INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON rc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
    WHERE UNIQUE_CONSTRAINT_NAME = 'PK_items' AND SUBSTRING(ccu.TABLE_NAME, 1, 1) = '_'

DECLARE @idps_childs NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 2 FOR XML PATH(''))
DECLARE @idps_parents NVARCHAR(MAX) = (SELECT ',' + CAST(item_column_id AS NVARCHAR) FROM item_columns WHERE use_fk = 3 FOR XML PATH(''))

WHILE @ids IS NOT NULL BEGIN
	IF OBJECT_ID('tempdb..#LoopItems') IS NOT NULL DROP TABLE #LoopItems
	CREATE TABLE #LoopItems (item_id INT, ord INT)

	OPEN @GetTables
	FETCH NEXT FROM @GetTables INTO @tName, @cName
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @sql = ' WITH rec AS ( 
			 SELECT t.item_id, t.' + @cName + ', ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			 FROM ' + @tName + ' t 
			 WHERE t.' + @cName + ' IN (-1' + @ids + ')
			 ) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
		EXECUTE (@sql)
		FETCH NEXT FROM @GetTables INTO @tName, @cName
	END
	CLOSE @GetTables

	SET @sql = ' WITH rec AS ( 
			SELECT t.selected_item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_childs + ') AND item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT selected_item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @sql = ' WITH rec AS ( 
			SELECT t.item_id, ' + CAST(@lvl AS NVARCHAR) + ' AS lvl
			FROM item_data_process_selections t 
			WHERE t.item_column_id IN (-1' + @idps_parents + ') AND selected_item_id IN (-1' + @ids + ')
			) INSERT INTO #LoopItems (item_id, ord) SELECT item_id, lvl FROM rec '
	EXECUTE (@sql)

	SET @ids = (SELECT ',' + CAST(item_id AS NVARCHAR) FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems) FOR XML PATH(''))

	INSERT INTO #DeleteItems (item_id, ord) SELECT item_id, ord FROM #LoopItems WHERE item_id NOT IN (SELECT item_id FROM #DeleteItems)

	SET @lvl = @lvl + 1
END

DELETE  FROM item_subitems 				  WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_tabs_subprocess_data WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE selected_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM item_data_process_selections WHERE item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations  WHERE task_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM task_durations WHERE  resource_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_durations WHERE  allocation_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM allocation_comments WHERE  author_item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM condition_user_groups WHERE  item_id IN (SELECT item_id FROM #DeleteItems)
DELETE  FROM process_baseline WHERE  process_item_id IN (SELECT item_id FROM #DeleteItems)

DECLARE @delId AS INT
DECLARE @GetIds CURSOR

SET @GetIds = CURSOR FOR SELECT item_id FROM #DeleteItems GROUP BY item_id, ord ORDER BY ord DESC

OPEN @GetIds
FETCH NEXT FROM @GetIds INTO @delId

WHILE @@FETCH_STATUS = 0 BEGIN
	INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM items WHERE item_id = @delId
    DELETE FROM items WHERE item_id = @delId
    FETCH NEXT FROM @GetIds INTO @delId
END
CLOSE @GetIds

DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.parent_item_id = d.item_id
DELETE dd FROM item_subitems dd					INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM process_tabs_subprocess_data dd	INNER JOIN deleted d ON dd.item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.selected_item_id = d.item_id
DELETE dd FROM item_data_process_selections dd	INNER JOIN deleted d ON dd.item_id = d.item_id
UPDATE dd SET dd.link_item_id = NULL FROM item_data_process_selections dd INNER JOIN deleted d ON dd.link_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.task_item_id = d.item_id
DELETE dd FROM task_durations dd INNER JOIN deleted d ON dd.resource_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_durations dd INNER JOIN deleted d ON dd.allocation_item_id = d.item_id
DELETE dd FROM allocation_comments dd INNER JOIN deleted d ON dd.author_item_id = d.item_id

DELETE dd FROM instance_user_configurations_saved dd INNER JOIN deleted d ON dd.user_id = d.item_id
DELETE dd FROM instance_user_configurations dd INNER JOIN deleted d ON dd.user_id = d.item_id

DELETE dd FROM condition_user_groups dd INNER JOIN deleted d ON dd.item_id = d.item_id

DELETE dd FROM process_baseline dd INNER JOIN deleted d ON dd.process_item_id = d.item_id

--Insert into deleted items
INSERT INTO items_deleted (item_id, instance, process, deleted_date) SELECT item_id, instance, process, getUTCdate() FROM deleted

--Finally delete the item
DELETE dd FROM items dd							INNER JOIN deleted d ON dd.item_id = d.item_id