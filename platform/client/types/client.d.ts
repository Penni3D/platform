namespace Client {
    export interface ClientInformationModel {
        locale_id: string,
        default_locale_id: string,
        date_format: string,
        first_day_of_week: number,
        timezone: string,
        item_id: number,
        anonymous: boolean,
        fullname: string,
        token: string,
        version: string,
        versiondate: string,
	    delegator: string
    }
    export interface ProcessTab {
        ArchiveEnabled?: boolean,
        ArchiveItemColumnId?: number,
        Background?: string,
        Columns?: any,
        LanguageTranslation?: any,
        MaintenanceName?: string,
        OrderNo?: string,
        ProcessTabGroups?: any,
        ProcessTabId?: number,
        Tag?: string,
        UseInNewItem?: boolean,
        Variable?: string,
        grids?: any
    }

    export interface ContainerColumn {

        HelpTextTranslation?: string,
        Instance?: string,
        ItemColumn?: Client.ItemColumn,
        Label?: string,
        OrderNo?: string,
        Placeholder?: string,
        Process?: string,
        ProcessContainerColumnId?: number,
        ProcessContainerId?: number,
        ReadOnly?: boolean,
        Required?: boolean,
        Validation?: any
    }

    export interface ItemColumn {
        CheckConditions?: boolean,
        DataAdditional?: any,
        DataAdditional2?: any,
        DataType: number,
        Equations?: string,
        InputMethod?: number,
        InputName?: string,
        ItemColumnId: number,
        ItemDataProcessSelection?: any,
        ItemDataProcessSelections?: any,
        LanguageTranslation: any,
        LanguageTranslationRaw: any,
        LinkProcess?: string,
        Name?: string,
        OrderNo: string,
        ParentSelectType?: number,
        Process: string,
        ProcessActionId?: number,
        ProcessContainerIds: Array<number>,
        ProcessDialogId?: number,
        RelativeColumnId?: number,
        StatusColumn?: boolean,
        SubDataAdditional?: string,
        SubDataType?: number,
        SystemColumn?: boolean,
        UniqueCol?: number,
        UseFk?: number,
        UseInPortfolio?: boolean,
        UseInSelect?: boolean,
        UseLucene?: boolean,
        Variable: string,
        Translation?: string, //nämä eivät tule serveriltä, mutta tarvitaan clientillä. Miten toimia?
        TranslationRaw?: string,
        InConditions:boolean
    }

    export interface ProcessContainer {
        LanguageTranslation?: any,
        MaintenanceName?: string,
        ProcessContainerId?: number,
        Variable?: string,
        RightContainer?: number,
		tcId?: number,
	    Id?: number,
	    fields?: Array<any>
    }
    export interface TabContainer {
        OrderNo: string,
        ProcessContainer: Client.ProcessContainer,
        ProcessContainerSide: number,
        ProcessTab: Client.ProcessTab,
        ProcessTabContainerId?: number,
    }

    export interface ProcessDialog {
        LanguageTranslation?: any,
        ProcessActionDialogId: number,
        Variable?: string,
        Type: number,
        Translation?: string
    }

    export interface Condition {
        ConditionGroupId: number,
        ConditionId: number,
        ConditionJSON: any,
        Disabled: boolean,
        Features: List<string>,
        ItemIds: List<number>,
        LanguageTranslation: any,
        OrderNo: string,
        Process: string,
        ProcessContainerIds: List<number>,
        ProcessGroupIds: List<number>,
        ProcessPortfolioIds: List<number>,
        ProcessTabIds: List<number>,
        States: List<string>,
        Variable: string,
        Translation: string
    }
    
    export interface ProcessActionRow {
        ProcessActionRowId : number,
        ProcessActionId : number,
        ProcessActionGroupId : number,
        ItemColumnId: number,
        ValueType: number,
        Value: number,
        UserItemColumnId: number,
        LinkItemColumnId: number,
        Value2: number,
        Value3: number,
        ItemColumnIds: List<number>,
        UserGroupIds:  List<number>,
        ConditionId: number,
        ListItemColumnIds: List<number>,
        ListProcess: string
    }
}