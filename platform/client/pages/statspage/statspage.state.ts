﻿(function () {
    'use strict';

    angular
        .module('core.pages')
        .config(StatsPageConfig)
        .controller('StatsPageController', StatsPageController)
        .service('StatsPageService', StatsPageService);

    StatsPageConfig.$inject = ['ViewsProvider'];
    function StatsPageConfig(ViewsProvider) {
        ViewsProvider.addView('statspage', {
            controller: 'StatsPageController',
            template: 'pages/statspage/statspage.html',
            resolve: {
                timerangeSetup: function () {
                    return {
                        day: {
                            name: 'Day',
                            columns: 31,            // number of visible columns when this is selected
                            formatter: 'D/M/YYYY',  // formatter for momentjs
                            qualifier: 'day'        // qualifier for momentjs
                        },
                        week: {
                            name: 'Week',
                            columns: 24,
                            formatter: 'W/YYYY',
                            qualifier: 'week'
                        },
                        year: {
                            name: 'Year',
                            columns: 10,
                            formatter: 'YYYY',
                            qualifier: 'year'
                        },
                        month: {
                            name: 'Month',
                            columns: 24,
                            formatter: 'M/YYYY',
                            qualifier: 'month'
                        }
                    };
                }
            }
        });

		ViewsProvider.addPage('statspage', "PAGE_STATISTICS", ['read', 'write']);
	}

    StatsPageController.$inject = ['$scope', 'StatsPageService', 'timerangeSetup'];
    function StatsPageController($scope, StatsPageService, timerangeSetup) {
        var pageViewStats = [];

        $scope.parameters = {
            timerange: timerangeSetup.day.qualifier,
            uniqueViews: 'all'
        };
        $scope.parameters.startDate = getDefaultStartDate();

        $scope.header = {
            title: 'Page View Statistics'
        };

        $scope.chartOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }]
            }
        };

        $scope.options = {
            timeRanges: [
                { name: timerangeSetup.day.name,   value: timerangeSetup.day.qualifier },
                { name: timerangeSetup.week.name,  value: timerangeSetup.week.qualifier },
                { name: timerangeSetup.month.name, value: timerangeSetup.month.qualifier },
                { name: timerangeSetup.year.name,  value: timerangeSetup.year.qualifier }],
            views: [],
            uniqueViews: [
                { name: 'Number of unique views', value: 'unique' },
                { name: 'Number of page loads',   value: 'all' }
                    ]
        };

        $scope.changeValue = function () {
            loadStats();
        };

        function getColumnCount() {
            return timerangeSetup[$scope.parameters.timerange].columns;
        }

        function getDefaultStartDate() {
            return moment().subtract(getColumnCount() - 1, 'days').toDate();
        }

        function setYAxisStep(value) {
            $scope.chartOptions.scales.yAxes[0].ticks.stepSize = value;
        }

        /**
         * Creates blank array with proper date key.
         * Date key is 1/2/2017 when day selected, 2/2017 when month, 2017 when year and 15/2017 when week
         */
        function getEmptyColumns() {
            var columnObjects = [];
            var setup = timerangeSetup[$scope.parameters.timerange];
            var startDate = moment($scope.parameters.startDate);

            for (var i = 0; i < getColumnCount(); i++) {
                columnObjects.push({
                    viewCount: 0,
                    date: startDate.format(setup.formatter)
                });

                startDate.add(1, setup.qualifier);
            }
            return columnObjects;
        }

        function getKeyForItem(item) {
            var key = '' + item.year;
            if (item.week) {
                key = item.week + '/' + key;
            } else {
                if (item.month) {
                    key = item.month + '/' + key;
                }
                if (item.day) {
                    key = item.day + '/' + key;
                }
            }
            return key;
        }

        function setViewCountToScope() {

            var viewsForScope = [];
            var labelsForScope = [];
            var maxValue = 0;

            var columns = getEmptyColumns();
            _.each(pageViewStats, function (item) {
                var key = getKeyForItem(item);
                var exists = _.find(columns, { date: key });

                if (exists) {
                    exists.viewCount = item.viewCount;
                }

                if (maxValue < item.viewCount) {
                    maxValue = item.viewCount;
                }
            });
            _.each(columns, function (item) {
                labelsForScope.push(item.date);
                viewsForScope.push(item.viewCount);
            });
            $scope.labels = labelsForScope;
            $scope.views = [viewsForScope];

            // fix charts step size to match data
            if (maxValue < 10) {
                setYAxisStep(1);
            } else {
                var rounded = Math.floor(maxValue / 10) * 5;
                setYAxisStep(rounded);
            }
        }

        function loadStats() {
            StatsPageService.getStatsPage(
                $scope.parameters.view,
                $scope.parameters.timerange,
                $scope.parameters.uniqueViews,
                $scope.parameters.startDate)
                .then(function (data) {
                    pageViewStats = data;
                    setViewCountToScope();
                });
        }

        function loadOptions() {
            StatsPageService.getOptions()
                .then(function (response) {
                    $scope.options.views = _.map(response.views, function (item) {
                        return { name: item, value: item };
                    });
                });
        }

        function initData() {
            loadOptions();
            loadStats();
        }

        initData();
    }

    StatsPageService.$inject = ['ApiService'];

    function StatsPageService(ApiService) {
        var self = this;
        var StatsPage = ApiService.v1api('features/statspage');
        var dateFormatter = 'YYYY-MM-DD';
        var queryOptions = {
            QUERY: 'query',
            OPTIONS: 'options'
        };

        function convertToSpaceIfNull(value) {
            if (_.isEmpty(value)) {
                value = ' ';
            }
            return value;
        }


        /**
         * Returns page view statistics starting from given date
         *
         * @param  {string=} view         name of the view (optional)
         * @param  {string=} timerange    'day', 'week', 'month' or 'year' (optional)
         * @param  {string=} uniqueViews  'all' or 'unique' (optional)
         * @param  {Date}    startDate    Start date for the query
         * @return {Object[]}             Views in array
         */
        self.getStatsPage = function (view, timerange, uniqueViews, startDate) {
            view = convertToSpaceIfNull(view);
            timerange = convertToSpaceIfNull(timerange);
            uniqueViews = convertToSpaceIfNull(uniqueViews);
            if (!startDate) {
                throw new Error('StartDate is required');
            }

            var formattedDate = moment(startDate).format(dateFormatter);
            return StatsPage.get([queryOptions.QUERY, view, timerange, uniqueViews, formattedDate]);
        };


        /**
         * Returns options for view stats page selectors (view names for example)
         *
         * @return {Object}  Object filled with arrays of possible selectors
         */
        self.getOptions = function () {
            return StatsPage.get(queryOptions.OPTIONS);
        };
    }
})();
