﻿(function () {
	'use strict';

	angular
        .module('core.pages')
		.service('EventLogService', EventLogService);

	EventLogService.$inject = ['$rootScope', 'ConnectorService', 'ApiService'];
	function EventLogService($rootScope, ConnectorService, ApiService) {
	    let self = this;

		let Logs = ApiService.v1api('diagnostics/instancelogs');

		self.getLogs = function (filters) {
			  return Logs.new([], filters).then(function(response){
				return response
			});
		}
	}
})();