﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(EventLogConfig)
		.controller('EventLogController', EventLogController);

	EventLogConfig.$inject = ['ViewsProvider'];

	function EventLogConfig(ViewsProvider) {
		ViewsProvider.addView('eventLog', {
			controller: 'EventLogController',
			template: 'pages/eventLog/eventLog.html',
			resolve: {
				InitialEventLogs: function (EventLogService) {

					let f = {
						SearchText: '',
						MinRow: 0,
						MaxRow: 50,
						LogType: null,
						UserItemId: null,
						StartOccurence: null,
						EndOccurence: null,
						OrderBy: null
					};
					return EventLogService.getLogs(f);
				},
				Rights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('eventLog');
				},
			}
		});

		ViewsProvider.addPage('eventLog', "PAGE_EVENT_LOG", ['read', 'stack_trace']);
	}

	EventLogController.$inject = ['$scope', 'EventLogService','MessageService', 'InitialEventLogs', 'CalendarService', 'ViewService', 'StateParameters', 'Rights'];

	function EventLogController($scope, EventLogService, MessageService, InitialEventLogs, CalendarService, ViewService, StateParameters, Rights) {
		$scope.data = InitialEventLogs;

		let maxNumber = 0;
		let orderBy = null;
		let needsNewSearch = false;

		$scope.stackRight = Rights.includes('stack_trace');

		if($scope.data.length > 0) maxNumber = $scope.data[0].MaxRows;

		$scope.expandedRows = [];

		$scope.expandMessage = function (i) {
			let content = {
				title: 'Message',
				message: $scope.data[i].Message,
				buttons: [{ type: 'primary', text: 'Close' }],
			};
			MessageService.dialog(content);
		};

		$scope.expandStack = function(i) {
			let content = {
				title: 'Stack Trace',
				message: $scope.data[i].Stack,
				buttons: [{ type: 'primary', text: 'Close'}],
			};
			MessageService.dialog(content);
		};
		
		$scope.getOccurenceFunc = function(i) {
			return CalendarService.formatDateTime($scope.data[i].Occurence, false, false, false, false, false);
		};
		
		$scope.paging = {
			currentPage: 0,
			size: 50,
			max: maxNumber
		};

		let orderCollection = {
			"message": "DESC",
			"last_name": "DESC",
			"first_name": "DESC",
			"login": "DESC",
			"occurence": "DESC",
			"instance_log_id": "DESC",
			"log_category": "DESC",
			"log_type": "DESC",
			"log_severity": "DESC",
		};

		let currentFloor = 0;
		let currentCeiling = 50;
		let oldCurrentPage = 0;

		$scope.onPageChange = function(){
			if(oldCurrentPage > $scope.paging.currentPage){
				currentCeiling = currentFloor;
				currentFloor = currentFloor - $scope.paging.size;
			} else {
				currentFloor = currentCeiling;
				currentCeiling = currentFloor + $scope.paging.size;
			}
			oldCurrentPage = $scope.paging.currentPage;
			$scope.doSearch(true);
		};

		$scope.formatDates = function(date){
			return CalendarService.formatDate(date);
		};

		$scope.selectOrderByColumn = function(columnName) {
			if (orderCollection[columnName] == "ASC") {
				orderCollection[columnName] = "DESC";
			} else if (orderCollection[columnName] == "DESC") {
				orderCollection[columnName] = "ASC";
			}

			orderBy = columnName;
			$scope.doSearch();
		};

		$scope.filterVisibility = false;
		$scope.search = "";
		$scope.logType = null;
		$scope.startOccu = null;
		$scope.endOccu = null;

		$scope.logTypes = [{'name': 'ServerMessage', 'value': 0},
			{'name': 'ServerException', 'value': 1},
			{'name': 'ClientAngular', 'value': 2},
			{'name': 'ClientJavaScript ', 'value': 3}
		];

		$scope.logCategories = [
			{'name': 'System', 'value': 0},
			{'name': 'Authentication', 'value': 1},
			{'name': 'Database', 'value': 2},
			{'name': 'Email', 'value': 3},
			{'name': 'Angular', 'value': 4},
			{'name': 'RegularJs', 'value': 5},
			{'name': 'Integration', 'value': 6},
			{'name': 'BackgroundTask', 'value': 7}
		];

		$scope.logSeverities = [{ 'name': 'Info', 'value': 0 },
			{ 'name': 'Warning', 'value': 1 },
			{ 'name': 'Error', 'value': 2 },
			{ 'name': 'SQL Error', 'value': 6 }
		];
		$scope.logSeveritiesC = {0: 'Info', 1: 'Warning', 2: 'Error', 6: 'SQL Error'}

		$scope.clearFilters = function(){

			$scope.search = "";
			$scope.idSearch = null;
			currentFloor = 0;
			currentCeiling = 50;
			$scope.logType = null;
			$scope.userForSearch = "";
			$scope.startOccu = null;
			$scope.endOccu = null;
			$scope.logCategory = null;
			$scope.logSeverity = null;
			orderBy = null;

			if(needsNewSearch == true){
				$scope.doSearch();
			}
		};
		
		$scope.header = {
			title: 'PAGE_EVENT_LOG',
			icons: [{
				icon: 'filter_list',
				onClick: function () {
					$scope.filterVisibility = !$scope.filterVisibility;
				}
			}]
		};

		let searchTimer

		$scope.doSearch = function (optionalParams?) {
			if (typeof optionalParams != 'undefined' && optionalParams.hasOwnProperty('fromText') && $scope.search.length < 4  && $scope.idSearch == null ) {
				return;
			}

			if (typeof optionalParams == 'undefined') {
				currentFloor = 0;
				currentCeiling = $scope.paging.size;
			}

			let userId = null;
			
			if ($scope.userForSearch.length > 0 ) userId = $scope.userForSearch[0];

			let f = {
				SearchText: $scope.search,
				IdSearch: $scope.idSearch,
				MinRow: currentFloor,
				MaxRow: currentCeiling,
				LogType: $scope.logType,
				UserItemId: userId,
				StartOccurence: $scope.startOccu,
				EndOccurence: $scope.endOccu,
				LogCategory: $scope.logCategory,
				LogSeverity: $scope.logSeverity,
				OrderBy: [orderBy, orderCollection[orderBy]]
			};

			if (orderBy == null || orderCollection[orderBy] == null) f.OrderBy = null;
			clearTimeout(searchTimer);
			searchTimer = setTimeout(function () {
				ViewService.lock(StateParameters.ViewId);
				EventLogService.getLogs(f).then(function (newRows) {
					ViewService.unlock(StateParameters.ViewId);
					needsNewSearch = true;
					$scope.data = newRows;
					if($scope.data.length > 0) {
						$scope.paging.max = $scope.data[0].MaxRows;
					} else {
						$scope.paging.max = 0;
					}
				});
			}, 500);
		}

		$scope.getLogCategory = function (id) {
			return $scope.logCategories.find(x => x.value === id).name;
		};

		$scope.getLogType = function (id) {
			return $scope.logTypes.find(x => x.value === id).name;
		};
		
		$scope.getLogSeverity = function (id) {
			try {
				return $scope.logSeverities.find(x => x.value === id).name;
			} catch (error) {
				console.error(error);
				return "";
			}
		};
	}
})();