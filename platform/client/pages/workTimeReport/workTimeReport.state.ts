(function () {
    'use strict';

    angular
        .module('core.pages')
        .config(WorkTimeReportConfig)
        .controller('WorkTimeReportController', WorkTimeReportController)
        .service('WorkTimeReportService', WorkTimeReportService)
        .directive('workTimeReportDirective', WorkTimeReportDirective);

    WorkTimeReportConfig.$inject = ['ViewsProvider'];

    function WorkTimeReportConfig(ViewsProvider) {
        ViewsProvider.addView('workTimeReport', {
            controller: 'WorkTimeReportController',
            template: 'pages/workTimeReport/workTimeReport.html',
            resolve: {
                WtrLists: function (WorkTimeReportService, $q, Translator, MessageService) {
                    let lists = [];
                    let promises = [];

                    let resolutionList = [{
                        list_item: "WORKTIME_REPORT_32_D",
                        value: "day"
                    }, {list_item: "WORKTIME_REPORT_33_W", value: "week"}, {
                        list_item: "WORKTIME_REPORT_34_M",
                        value: "month"
                    }];
                    lists.push({SelectReportResolution: resolutionList});
                    let groupingList = [{
                        list_item: "WORKTIME_REPORT_38_T",
                        value: "none"
                    }, {list_item: "WORKTIME_REPORT_39_U", value: "users"}];
                    lists.push({SelectGrouping: groupingList});
                    let scopeList = [{
                        list_item: "WORKTIME_REPORT_38_T",
                        value: "target"
                    }, {list_item: "WORKTIME_REPORT_44_TASK", value: "task"}];
                    lists.push({SelectScope: scopeList});

                    let p = WorkTimeReportService.getProcessesWithHoursList().then(function (processList) {
                        let targetList = [{
                            process: "worktime_tracking_basic_tasks",
                            process_name_column: "list_item",
                            parameter: Translator.translate("worktime_tracking_basic_tasks")
                        }];
                        let errorMsg = "";
                        let taskProcessErrorIsInMessage = false;
                        let hasErrors = false;
                        processList.data.forEach(function (listObj, i) {
                            targetList.push(listObj);
                            if (listObj.parameter == null || listObj.process_name_column == null) { //missing entity selectors = true.
                                if (hasErrors == false) {
                                    errorMsg = errorMsg + Translator.translate("WORKTIME_REPORT_40_MES") + "\n" + "\n" + Translator.translate("WORKTIME_REPORT_43_TARGET_PCM") + "\n";
                                    hasErrors = true;
                                }
                                if (listObj.parameter == null && taskProcessErrorIsInMessage == false) {
                                    errorMsg = errorMsg + Translator.translate("WORKTIME_REPORT_41_TASK_PCM") + "\n";
                                    taskProcessErrorIsInMessage = true;
                                }
                                if (listObj.process_name_column == null) {
                                    errorMsg = errorMsg + '-"' + listObj.process + '" ' + Translator.translate("WORKTIME_REPORT_42_TARGET_PCM") + "\n";
                                }
                            }
                        });
                        lists.push({SelectProcess: targetList});
                        if (hasErrors) {
                            MessageService.msgBox(errorMsg);
                        }
                    });
                    promises.push(p);
                    return $q.all(promises).then(function () {
                        return lists;
                    });
                }
            }
        });

        ViewsProvider.addPage('workTimeReport', "PAGE_WORKTIME_REPORT", ['read', 'write']);
    }

    WorkTimeReportDirective.$inject = ['$timeout'];

    function WorkTimeReportDirective($timeout) {
        return {
            controller: function ($scope, $element) {
                let scrollPosition = $("#WtrTableArea").scrollLeft();
                let curYPos = 0;
                let curXPos = 0;
                $scope.scrollingReport = false;
                if ($element[0].id === "WtrTableArea") { //Control for report table scrolling
                    $scope.resetScroll = function () {
                        $($element[0]).scrollLeft(0);
                    }

                    $element.bind("mousedown", function (e) {
                        e.preventDefault(); //Prevents dragging the row
                        scrollPosition = $("#WtrTableArea").scrollLeft();
                        $scope.scrollingReport = true;
                        $scope.cursorInTableArea = true;
                        curYPos = e.pageY;
                        curXPos = e.pageX;
                    });

                    $element.bind("mouseleave", function (e) {
                        $scope.scrollingReport = false;
                    });

                    $element.bind("mouseup", function (e) {
                        $scope.scrollingReport = false;
                    });

                    $element.bind("mousemove", function (e) {
                        if ($scope.scrollingReport == true) {
                            $("#WtrTableArea").scrollLeft(scrollPosition + (curXPos - e.pageX));
                        }
                    });

                }
            }
        }
    }

    WorkTimeReportController.$inject = ['$scope', 'Translator', '$timeout', '$q', '$filter', 'StateParameters', 'WorkTimeReportService', 'WtrLists', 'ViewService', 'Common', 'MessageService', 'CalendarService'];

    function WorkTimeReportController($scope, Translator, $timeout, $q, $filter, StateParameters, WorkTimeReportService, WtrLists, ViewService, Common, MessageService, CalendarService) {
        $scope.header = {
            title: 'WORKTIMEREPORT',
            icons: [
                {
                    icon: 'filter_list',
                    onClick: function () {
                        $scope.showOptions = !$scope.showOptions;
                    }
                }
            ]
        };

        WtrLists.forEach(function (list, i) {
            if (Object.keys(list)[0] === 'SelectReportResolution') {
                $scope.listSelectReportResolution = list;
                if (list["SelectReportResolution"].length > 0) {
                    $scope.valueSelectReportResolution = [list["SelectReportResolution"][0].value];
                }
            }
            if (Object.keys(list)[0] === 'SelectGrouping') {
                $scope.listGrouping = list;
                if (list["SelectGrouping"].length > 0) {
                    $scope.valueGrouping = [list["SelectGrouping"][0].value];
                }
            }
            if (Object.keys(list)[0] === 'SelectScope') {
                $scope.listScope = list;
                if (list["SelectScope"].length > 0) {
                    $scope.valueScope = [list["SelectScope"][0].value];
                }
            }
            if (Object.keys(list)[0] === 'SelectProcess') {
                list["SelectProcess"].forEach(function (listRow, i) {
                    listRow.processTranslation = Translator.translate(listRow.process); // Add a translation value to listRow based on listRow process value
                });
                $scope.listSelectProcess = list;
                if (list["SelectProcess"].length > 0) {
                    $scope.valueReportProcess = [[list["SelectProcess"][0].process][0]];
                    $scope.processSelectHeader = [list["SelectProcess"][0].processTranslation][0];
                    $scope.SearchProcessName = [list["SelectProcess"][0].process][0];
                    $scope.targetProcessNameColumn = [list["SelectProcess"][0].process_name_column][0];
                    $scope.parameter = [list["SelectProcess"][0].parameter][0]; //parameter is either task name column from entity selector or translation value of "worktime_tracking_basic_tasks".
                }
            }
        });

        $scope.optionsProcesses = [];
        $scope.reportTargetChanged = function (newTarget) {
            $scope.listSelectProcess["SelectProcess"].forEach(function (listRow, i) {
                if (listRow.process == newTarget[0]) {
                    if ($scope.processSelectHeader.toLowerCase() !== listRow.processTranslation) {
                        $scope.optionsProcesses = []; //reset process selector input options
                    }
                    $scope.processSelectHeader = listRow.processTranslation;
                    $scope.SearchProcessName = listRow.process;
                    $scope.targetProcessNameColumn = listRow.process_name_column;
                    $scope.parameter = listRow.parameter; //parameter is either task name column from entity selector or translation value of "worktime_tracking_basic_tasks".
                }

            });
        }

		let sd = new Date();
		sd.setUTCHours(0, 0, 0, 0);
		sd.setUTCDate(1);
		sd.setFullYear(sd.getFullYear());
		$scope.optionStartDate = sd;
		let ed = new Date();
		ed.setUTCHours(0, 0, 0, 0);
		ed.setUTCDate(ed.getDate() + 1);
		$scope.optionEndDate = ed;

        $scope.optionsUsers = [];
        $scope.hourIndicator = "h";
        $scope.reportCreated = false;
        $scope.loading = false;
        $scope.editOptions = true;

        if ($scope.reportCreated == false) {
            $scope.showOptions = true;
        }
        $scope.closeReport = function () {
            //ViewService.refreshView(StateParameters.ViewId); //Refresh process view to a logical state
            $scope.reportCreated = false;
            $scope.resetScroll(); // set report table area scrollLeft to zero with directive
            //Reset values to lessen browser burden
            $scope.reportObjects = [];
            $scope.daysArray = [];
            $scope.reportUsers = [];
            $scope.reportProcesses = [];
            $scope.dateHeaders = [];
            $scope.reportGroupUsers = [];
            $scope.reportCreated = false;
            $scope.editOptions = true;
        }
        $scope.openReport = function () {
            $scope.loading = true;
            $scope.reportCreated = false;
            $scope.showOptions = false;
            let startDate = moment($scope.optionStartDate);
            let endDate = moment($scope.optionEndDate);
            let amountOfDays = endDate.diff(startDate, 'days');
            $scope.daysArray = new Array(+amountOfDays);
            $scope.reportUsers = angular.copy($scope.optionsUsers);
            $scope.reportProcesses = angular.copy($scope.optionsProcesses);
            $scope.reportStartDate = CalendarService.formatDate(angular.copy($scope.optionStartDate)); //Change from date to text
            $scope.reportEndDate = CalendarService.formatDate(angular.copy($scope.optionEndDate)); //Change from date to text
            let paramScope = $scope.valueScope[0];
            if (paramScope == "task") {
                $scope.reportRowHeader = Translator.translate("WORKTIME_REPORT_45_TASKS");
                $scope.scopeIsTask = true;
            } else {
                $scope.reportRowHeader = $scope.processSelectHeader;
                $scope.scopeIsTask = false;
            }
            if ($scope.valueGrouping[0] === "users") {
                $scope.userGrouping = true;
                $scope.reportRowHeader = Translator.translate("WORKTIME_REPORT_39_U");
            } else {
                $scope.userGrouping = false;
            }
            $scope.reportBasicTasks = false;
            let basicTasks = "false";
            if ($scope.valueReportProcess == "worktime_tracking_basic_tasks") {
                basicTasks = "true";
                $scope.reportBasicTasks = true;
            }
            let users = []
            let procesesses = []
            if ($scope.optionsUsers.length > 0) {
                $scope.optionsUsers.forEach(function (userId, i) {
                    users.push({user: userId});
                });
                //users = angular.copy($scope.optionsUsers);
            } else {
                users.push({user: "all"});
            }
            if ($scope.optionsProcesses.length > 0) {
                $scope.optionsProcesses.forEach(function (processId, i) {
                    procesesses.push({process: processId});
                });
                //procesesses = angular.copy($scope.optionsProcesses);
            } else {
                procesesses.push({process: "all"});
			}
            let reportParameters = {
                resolution: $scope.valueSelectReportResolution["0"],
                timeframeStart: $scope.optionStartDate,
                timeframeEnd: $scope.optionEndDate,
                grouping: $scope.valueGrouping[0],
                reportScope: paramScope,
                basicHours: basicTasks,
                reportUsers: users,
                reportProcesses: procesesses,
                targetEntityName: $scope.targetProcessNameColumn,
                lmApprovedHours: false
            };
            let tasksParentProcess = angular.copy($scope.SearchProcessName);
            let processParameter = $scope.parameter; //parameter is either task name column from entity selector or translation value of "worktime_tracking_basic_tasks".
            WorkTimeReportService.getWorkTimeReport(tasksParentProcess, processParameter, reportParameters).then(function (reportData) {
                    $scope.dateHeaders = []; //For table header repeater
                    $scope.reportObjects = reportData.data;
                    if (reportParameters.resolution == "day") {
                        setupDayResolution($scope.reportObjects);
                        $scope.reportResolutionType = "days";
                        if ($scope.dateHeaders.length > 0) {
                            setSumOfDayColumns($scope.reportObjects);
                        }
                    } else if (reportParameters.resolution == "week") {
                        setupWeekResolution($scope.reportObjects);
                        $scope.reportResolutionType = "weeks";
                        if ($scope.dateHeaders.length > 0) {
                            setSumOfWeekColumns($scope.reportObjects);
                        }
                    } else if (reportParameters.resolution == "month") {
                        setupMonthResolution($scope.reportObjects);
                        $scope.reportResolutionType = "months";
                        if ($scope.dateHeaders.length > 0) {
                            setSumOfMonthColumns($scope.reportObjects);
                        }
                    }
                    if ($scope.dateHeaders.length > 0) {
                        $scope.dataFound = true;
                        $scope.reportCreated = true;
                        $scope.editOptions = false;
                        $scope.loading = false;
                    } else {
                        $scope.dataFound = false;
                        $scope.reportCreated = true;
                        $scope.showOptions = true; //Prompt the user to choose different options
                        $scope.editOptions = true;
                        $scope.loading = false;
                    }
                }, function () {
                    //Server did not return report data
                    $scope.reportCreated = false;
                    $scope.loading = false;
                }
            );
        }

        let setSumOfDayColumns = function (reportData) {
            $scope.dateHeaders.forEach(function (dateCol, i) {
                dateCol.hourSum = 0;
                reportData.forEach(function (reportRow, i2) {
                    if (reportRow.days_and_hours[i].hours > 0) {
                        dateCol.hourSum = dateCol.hourSum + reportRow.days_and_hours[i].hours;
                        reportRow.days_and_hours[i].hours = reportRow.days_and_hours[i].hours + " " + $scope.hourIndicator;
                    }
                });
            });
            $scope.reportRows.forEach(function (row, i) {
                if (row.row_type == 1) {
                    row.days_and_hours.forEach(function (dah, i) {
                        if (dah.hours > 0 && $scope.userGrouping == true) {
                            dah.hours = dah.hours + " " + $scope.hourIndicator;
                        }
                    });
                } else if (row.row_type == 2) {
                    row.days_and_hours.forEach(function (dah, i) {
                        if (dah.hours > 0 && $scope.userGrouping == true && $scope.scopeIsTask == true) {
                            dah.hours = dah.hours + " " + $scope.hourIndicator;
                        }
                    });
                }
            });
        }

        let setSumOfWeekColumns = function (reportData) {
            $scope.dateHeaders.forEach(function (weekCol, i) {
                weekCol.hourSum = 0;
                reportData.forEach(function (reportRow, i2) {
                    if (reportRow.weeks_and_hours[i].hours > 0) {
                        weekCol.hourSum = weekCol.hourSum + reportRow.weeks_and_hours[i].hours;
                        reportRow.weeks_and_hours[i].hours = reportRow.weeks_and_hours[i].hours + " " + $scope.hourIndicator;
                    }
                });
            });
            $scope.reportRows.forEach(function (row, i) {
                if (row.row_type == 1) {
                    row.weeks_and_hours.forEach(function (wah, i) {
                        if (wah.hours > 0 && $scope.userGrouping == true) {
                            wah.hours = wah.hours + " " + $scope.hourIndicator;
                        }
                    });
                } else if (row.row_type == 2) {
                    row.weeks_and_hours.forEach(function (wah, i) {
                        if (wah.hours > 0 && $scope.userGrouping == true && $scope.scopeIsTask == true) {
                            wah.hours = wah.hours + " " + $scope.hourIndicator;
                        }
                    });
                }
            });
        }

        let setSumOfMonthColumns = function (reportData) {
            $scope.dateHeaders.forEach(function (monthCol, i) {
                monthCol.hourSum = 0;
                reportData.forEach(function (reportRow, i2) {
                    if (reportRow.months_and_hours[i].hours > 0) {
                        monthCol.hourSum = monthCol.hourSum + reportRow.months_and_hours[i].hours;
                        reportRow.months_and_hours[i].hours = reportRow.months_and_hours[i].hours + " " + $scope.hourIndicator;
                    }
                });
            });
            $scope.reportRows.forEach(function (row, i) {
                if (row.row_type == 1) {
                    row.months_and_hours.forEach(function (mah, i) {
                        if (mah.hours > 0 && $scope.userGrouping == true) {
                            mah.hours = mah.hours + " " + $scope.hourIndicator;
                        }
                    });
                } else if (row.row_type == 2) {
                    row.months_and_hours.forEach(function (mah, i) {
                        if (mah.hours > 0 && $scope.userGrouping == true && $scope.scopeIsTask == true) {
                            mah.hours = mah.hours + " " + $scope.hourIndicator;
                        }
                    });
                }
            });
        }

        function isObject(item) {
            return (typeof item === "object" && !Array.isArray(item) && item !== null);
        }

        function getMondayOfTheWeek(d) {
            d = new Date(d);
            let day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
            return new Date(d.setDate(diff));
        }

        function setupDayResolution(reportData) {
            $scope.TotalHours = 0;
            $scope.reportRows = [];

            let dayNames = Common.getLocalShortDaysOfWeek();
            let startDate = angular.copy($scope.optionStartDate);
            let userIds = [];
            let userRow = {
                rowsCollection: undefined,
                user_item_id: undefined,
                responsible_name: undefined,
                row_type: undefined,
                caret_val: undefined,
                hasRows: undefined,
                hours_sum: undefined,
                days_and_hours: [],
                hasParentRow: undefined
            };
            let newUserRow = true;
            let ownerIds = [];
            let ownerRow = {days_and_hours: []};
            let newOwnerRow = true;
            let rowCollectionId = 0;

            reportData.forEach(function (obj, i) {
                let serverDaysAndHoursFound = JSON.parse(obj.days_and_hours); //Temporary save server days data
                serverDaysAndHoursFound = serverDaysAndHoursFound.root.wth2; //Extra object handling because of server XML conversion
                if (isObject(serverDaysAndHoursFound) == true) {
                    serverDaysAndHoursFound = [serverDaysAndHoursFound];
                } //if single object only, change it to an array of single object
                let i2 = 0;
                obj.days_and_hours = []; //Reset object and fill it with empty values
                if ($scope.userGrouping == true) {
                    if (userIds.indexOf(obj.user_item_id) == -1) {
                        ownerIds = []; //Reset ownerIds array because each user will have owner rows as child rows
                        newUserRow = true;
                        userIds.push(obj.user_item_id);
                        if ($scope.scopeIsTask == true) {
                            rowCollectionId = rowCollectionId + 1;
                        } //rowCollection scope for 3 level hierachy is a new user
                        userRow = {
                            rowsCollection: rowCollectionId,
                            user_item_id: obj.user_item_id,
                            responsible_name: obj.responsible_name,
                            row_type: 1,
                            caret_val: true,
                            hasRows: true,
                            hours_sum: 0,
                            days_and_hours: [],
                            hasParentRow: false
                        };
                        $scope.reportRows.push(userRow);
                    } else {
                        newUserRow = false;
                    }
                }
                if ($scope.scopeIsTask == true) {
                    if (ownerIds.indexOf(obj.owner_item_id) == -1) {
                        newOwnerRow = true;
                        ownerIds.push(obj.owner_item_id);
                        ownerRow = {
                            rowsCollection: rowCollectionId,
                            owner_item_id: obj.owner_item_id,
                            owner_name: obj.owner_name,
                            user_item_id: obj.user_item_id,
                            row_type: 2,
                            caret_val: true,
                            hasRows: true,
                            hours_sum: 0,
                            days_and_hours: [],
                            hasParentRow: false,
                            responsible_name: undefined
                        };
                        
                        if ($scope.userGrouping == true) {
                            ownerRow.hasParentRow = true;
                            ownerRow.user_item_id = obj.user_item_id;
                            ownerRow.responsible_name = obj.responsible_name;
                        } else {
                            ownerRow.hasParentRow = false;
                        }
                        $scope.reportRows.push(ownerRow);
                    } else {
                        newOwnerRow = false;
                    }
                    obj.row_type = 3; //Task row
                } else {
                    obj.row_type = 2; //Task owner process row
                }
                if ($scope.userGrouping == true || $scope.scopeIsTask == true) {
                    obj.hasParentRow = true;
                } else {
                    obj.hasParentRow = false;
                }
                obj.hasRows = false;
                obj.caret_val = true;
                obj.rowsCollection = rowCollectionId;

                while (i2 < $scope.daysArray.length + 1) {
                    let nextDayInLoop = new Date(startDate);
                    let numberOfDaysToAdd = i2;
                    let hours;
                    nextDayInLoop.setDate(nextDayInLoop.getDate() + numberOfDaysToAdd);
                    serverDaysAndHoursFound.forEach(function (obj3, i3) {
                        if (CalendarService.formatDate(obj3["@date"]) == CalendarService.formatDate(nextDayInLoop)) { //Data was found in server for the day
                            hours = parseFloat(obj3["@hours"]);
                        }
                    });
                    obj.days_and_hours.push({date: nextDayInLoop, hours: hours});
                    if ($scope.userGrouping == true) {
                        //Group for users stuff starts
                        if (newUserRow == true) {
                            userRow.days_and_hours.push({date: nextDayInLoop, hours: hours});
                        } else {
                            for (let i3 = 0; i3 < userRow.days_and_hours.length; i3++) {
                                if (userRow.days_and_hours[i3]["date"].toDateString() === nextDayInLoop.toDateString() && hours > 0) {
                                    if (userRow.days_and_hours[i3]["hours"] > 0) {
                                        userRow.days_and_hours[i3]["hours"] = (parseFloat(userRow.days_and_hours[i3]["hours"]) + parseFloat(hours));
                                    } else {
                                        userRow.days_and_hours[i3]["hours"] = parseFloat(hours);
                                    }
                                }
                            }
                        }
                    }
                    //Group for users stuff ends
                    if ($scope.scopeIsTask == true) {
                        //owner/project row that has child rows starts
                        if (newOwnerRow == true) {
                            ownerRow.days_and_hours.push({date: nextDayInLoop, hours: hours});
                        } else {
                            for (let i3 = 0; i3 < ownerRow.days_and_hours.length; i3++) {
                                if (ownerRow.days_and_hours[i3]["date"].toDateString() === nextDayInLoop.toDateString() && hours > 0) {
                                    if (ownerRow.days_and_hours[i3]["hours"] > 0) {
                                        ownerRow.days_and_hours[i3]["hours"] = (parseFloat(ownerRow.days_and_hours[i3]["hours"]) + parseFloat(hours));
                                    } else {
                                        ownerRow.days_and_hours[i3]["hours"] = parseFloat(hours);
                                    }
                                }
                            }
                        }
                        //owner/project row that has child rows ends
                    }
                    if (i == 0) { //Only one loop round required for day object headers
                        let dayNum = nextDayInLoop.getDay() - 1;
                        if (dayNum < 0) {
                            dayNum = 6;
                        } //6 is sunday. 
                        let dateHeaderObj = {
                            date: CalendarService.formatDate(nextDayInLoop),
                            dayName: dayNames[dayNum]
                        };
                        $scope.dateHeaders.push(dateHeaderObj);
                    }
                    i2++;
                }
                if ($scope.scopeIsTask == true) {
                    ownerRow.hours_sum = ownerRow.hours_sum + obj.hours_sum;
                } //scope is task
                if ($scope.userGrouping == true) {
                    userRow.hours_sum = userRow.hours_sum + obj.hours_sum;
                } //Group for users stuff
                $scope.TotalHours = $scope.TotalHours + obj.hours_sum;

                $scope.reportRows.push(obj);
            });
        }
        
        let getDiffInWeeks = (fromDate, toDate) => {
            let requestedOffset = 1
            let diff = toDate.diff(fromDate);
            let diffInWeeks = moment.duration(diff).as('weeks')
            return Math.ceil(diffInWeeks) + requestedOffset
        }

        function setupWeekResolution(reportData) {
            $scope.TotalHours = 0;
            $scope.reportRows = [];

			let startDate = new Date(angular.copy($scope.optionStartDate));
			let endDate = new Date(angular.copy($scope.optionEndDate));

            let weeksCount = getDiffInWeeks(moment(startDate), moment(endDate));

            let userIds = [];
            let userRow = {
                rowsCollection: undefined,
                user_item_id: undefined,
                responsible_name: undefined,
                row_type: undefined,
                caret_val: undefined,
                hasRows: undefined,
                hours_sum: undefined,
                weeks_and_hours: [],
                hasParentRow: undefined
            };
            let newUserRow = true;
            let ownerIds = [];
            let ownerRow = {
                rowsCollection: undefined,
                owner_item_id: undefined,
                owner_name: undefined,
                user_item_id: undefined,
                row_type: undefined,
                caret_val: undefined,
                hasRows: undefined,
                hours_sum: undefined,
                weeks_and_hours: [],
                hasParentRow: undefined,
                responsible_name: undefined
            };
            let newOwnerRow = true;
            let rowCollectionId = 0;
            reportData.forEach(function (obj, i) {
				let serverWeeksAndHoursFound = JSON.parse(obj.weeks_and_hours); //Temporary save server days data
                serverWeeksAndHoursFound = serverWeeksAndHoursFound.root.wth2; //Extra object handling because of server XML conversion 
                if (isObject(serverWeeksAndHoursFound) == true) {
                    serverWeeksAndHoursFound = [serverWeeksAndHoursFound];
                } //if single object only, change it to an array of single object
                let i2 = 0;
                let getWeekNumByDate = function (d) {
					let weekNum = getWeekNum(d);
					let yearNum = d.getFullYear();
                    return {year: yearNum, week: weekNum};
                };
                obj.weeks_and_hours = [];

                if ($scope.userGrouping == true) {
                    if (userIds.indexOf(obj.user_item_id) == -1) {
                        ownerIds = []; //Reset ownerIds array because each user will have owner rows as child rows
                        newUserRow = true;
                        userIds.push(obj.user_item_id);
                        if ($scope.scopeIsTask == true) {
                            rowCollectionId = rowCollectionId + 1;
                        } //rowCollection scope for 3 level hierachy is a new user
                        userRow = {
                            rowsCollection: rowCollectionId,
                            user_item_id: obj.user_item_id,
                            responsible_name: obj.responsible_name,
                            row_type: 1,
                            caret_val: true,
                            hasRows: true,
                            hours_sum: 0,
                            weeks_and_hours: [],
                            hasParentRow: false
                        };
                        $scope.reportRows.push(userRow);
                    } else {
                        newUserRow = false;
                    }
                }

                if ($scope.scopeIsTask == true) {
                    if (ownerIds.indexOf(obj.owner_item_id) == -1) {
                        newOwnerRow = true;
						ownerIds.push(obj.owner_item_id);
                        ownerRow = {
                            rowsCollection: rowCollectionId,
                            owner_item_id: obj.owner_item_id,
                            owner_name: obj.owner_name,
                            user_item_id: obj.user_item_id,
                            row_type: 2,
                            caret_val: true,
                            hasRows: true,
                            hours_sum: 0,
                            weeks_and_hours: [],
                            hasParentRow: false,
                            responsible_name: undefined
                        };
                        if ($scope.userGrouping == true) {
                            ownerRow.hasParentRow = true;
                            ownerRow.user_item_id = obj.user_item_id;
                            ownerRow.responsible_name = obj.responsible_name;
                        } else {
                            ownerRow.hasParentRow = false;
                        }
                        $scope.reportRows.push(ownerRow);
                    } else {
                        newOwnerRow = false;
                    }
                    obj.row_type = 3; //Task row
                } else {
                    obj.row_type = 2; //Task owner process row
                }
                if ($scope.userGrouping == true || $scope.scopeIsTask == true) {
                    obj.hasParentRow = true;
                } else {
                    obj.hasParentRow = false;
                }
                obj.hasRows = false;
                obj.caret_val = true;
                obj.rowsCollection = rowCollectionId;

                while (i2 < weeksCount) {
                    let nextWeekInLoop = new Date(getMondayOfTheWeek(startDate));
                    let numberOfWeeksToAdd = i2;
                    let hours = "";
                    nextWeekInLoop.setDate(nextWeekInLoop.getDate() + (numberOfWeeksToAdd * 7));
                    if ($scope.getDayNameByDate(nextWeekInLoop) == Translator.translate("CAL_DAYS_MIN_0")) { //handle american sunday calendar standard
                        nextWeekInLoop.setDate(nextWeekInLoop.getDate() - 1); //Change sunday to saturday so that the week number logic works in european standard.
                    }

                    let year_and_week_numbers = getWeekNumByDate(nextWeekInLoop);

                    serverWeeksAndHoursFound.forEach(function (obj3, i3) {
                        if (parseInt(obj3["@year"]) == parseInt(year_and_week_numbers.year) && parseInt(obj3["@week"]) - 1 == parseInt(year_and_week_numbers.week)) { //Data was found in server for the week
                            hours = parseFloat(obj3["@hours"]);
                        }
                    });

                    obj.weeks_and_hours.push({
                        year: year_and_week_numbers.year,
                        week: year_and_week_numbers.week,
                        hours: hours
                    });
                    if ($scope.userGrouping == true) {
                        //Group for users stuff starts
                        if (newUserRow == true) {
                            userRow.weeks_and_hours.push({
                                year: year_and_week_numbers.year,
                                week: year_and_week_numbers.week,
                                hours: hours
                            });
						} else {
							for (let i3 = 0; i3 < userRow.weeks_and_hours.length; i3++) {
                                if (parseInt(userRow.weeks_and_hours[i3]["year"]) == parseInt(year_and_week_numbers.year) && parseInt(userRow.weeks_and_hours[i3]["week"]) == year_and_week_numbers.week && hours > 0) {
                                    if (userRow.weeks_and_hours[i3]["hours"] > 0) {
                                        userRow.weeks_and_hours[i3]["hours"] = (parseFloat(userRow.weeks_and_hours[i3]["hours"]) + parseFloat(hours));
                                    } else {
                                        userRow.weeks_and_hours[i3]["hours"] = parseFloat(hours);
                                    }
                                }
                            }
                        }
                    }
                    //Group for users stuff ends
                    if ($scope.scopeIsTask == true) {
                        //owner/project row that has child rows starts
						if (newOwnerRow == true) { //if (!ownerRow.weeks_and_hours.find(function (find_obj) { return find_obj.week === year_and_week_numbers.week && find_obj.year === year_and_week_numbers.year; })) {
							ownerRow.weeks_and_hours.push({
								year: year_and_week_numbers.year,
								week: year_and_week_numbers.week,
								hours: hours
							});
						} else {
							for (let i3 = 0; i3 < ownerRow.weeks_and_hours.length; i3++) {
								if (parseInt(ownerRow.weeks_and_hours[i3]["year"]) == parseInt(year_and_week_numbers.year) && parseInt(ownerRow.weeks_and_hours[i3]["week"]) == year_and_week_numbers.week && hours > 0) {
									if (ownerRow.weeks_and_hours[i3]["hours"] > 0) {
										ownerRow.weeks_and_hours[i3]["hours"] = (parseFloat(ownerRow.weeks_and_hours[i3]["hours"]) + parseFloat(hours));
									} else {
										ownerRow.weeks_and_hours[i3]["hours"] = parseFloat(hours);
									}
								}
							}
						}
                        //owner/project row that has child rows ends
                    }
                    if (i == 0) { //header column amount is available in the first round
                        let dateHeaderObj = {year: year_and_week_numbers.year, week: year_and_week_numbers.week};
                        $scope.dateHeaders.push(dateHeaderObj);
                    }
                    i2++;
                }
                if ($scope.scopeIsTask == true) {
                    ownerRow.hours_sum = ownerRow.hours_sum + obj.hours_sum;
                } //scope is task
                if ($scope.userGrouping == true) {
                    userRow.hours_sum = userRow.hours_sum + obj.hours_sum;
                } //Group for users stuff
                $scope.TotalHours = $scope.TotalHours + obj.hours_sum;

                $scope.reportRows.push(obj);
            });
        }

        function setupMonthResolution(reportData) {
            $scope.TotalHours = 0;
            $scope.reportRows = [];

			let startDate = new Date(angular.copy($scope.optionStartDate));
			let endDate = new Date(angular.copy($scope.optionEndDate));
			let startMonth = startDate.getMonth(); //parseInt($filter('date')(startDate, 'M'), 10);
			let startYear = startDate.getFullYear(); //parseInt($filter('date')(startDate, 'yyyy'), 10);
			let endMonth = endDate.getMonth(); //parseInt($filter('date')(endDate, 'M'), 10); //ParseInt used because month number is sometimes in form 01 - 09
			let endYear = endDate.getFullYear(); //parseInt($filter('date')(endDate, 'yyyy'), 10);

			let monthNames = Common.getLocalShortMonthsOfYear();
            let yearsSubstract = endYear - startYear;
            let monthsCount = ((yearsSubstract * 12) + endMonth - startMonth) + 1; //always atleast one month even if start and end are at the same month
            let getMonthNumByDate = function (d) {
				let monthNum = d.getMonth() + 1; //$filter('date')(d, 'M');
				//                           ^-- getMonth() is zero based where db has actual month number
				let yearNum = d.getFullYear(); //$filter('date')(d, 'yyyy');
                return {year: yearNum, month: monthNum};
            }
            //Group for users stuff starts
            $scope.reportGroupUsers = [];
            let userIds = [];
            let userRow = {
                rowsCollection: undefined,
                user_item_id: undefined,
                responsible_name: undefined,
                row_type: undefined,
                caret_val: undefined,
                hasRows: undefined,
                hours_sum: undefined,
                months_and_hours: [],
                hasParentRow: undefined
            };
            let newUserRow = true;
            let ownerIds = [];
            let ownerRow = {
                rowsCollection: undefined,
                owner_item_id: undefined,
                owner_name: undefined,
                user_item_id: undefined,
                row_type: undefined,
                caret_val: undefined,
                hasRows: undefined,
                hours_sum: undefined,
                hasParentRow: undefined,
                responsible_name: undefined,
                months_and_hours: []
            };
            let newOwnerRow = true;
            let rowCollectionId = 0;
            //Group for users stuff ends
            reportData.forEach(function (obj, i) {
                let serverMonthsAndHoursFound = JSON.parse(obj.months_and_hours); //Temporary save server months data
                serverMonthsAndHoursFound = serverMonthsAndHoursFound.root.wth2; //Extra object handling because of server XML conversion 
                if (isObject(serverMonthsAndHoursFound) == true) {
                    serverMonthsAndHoursFound = [serverMonthsAndHoursFound];
                } //if single object only, change it to an array of single object
                let i2 = 0;
                obj.months_and_hours = []; //Reset object and fill it with empty values
                if ($scope.userGrouping == true) {
                    if (userIds.indexOf(obj.user_item_id) == -1) {
                        ownerIds = []; //Reset ownerIds array because each user will have owner rows as child rows
                        newUserRow = true;
                        userIds.push(obj.user_item_id);
                        if ($scope.scopeIsTask == true) {
                            rowCollectionId = rowCollectionId + 1;
                        } //rowCollection scope for 3 level hierachy is a new user
                        userRow = {
                            rowsCollection: rowCollectionId,
                            user_item_id: obj.user_item_id,
                            responsible_name: obj.responsible_name,
                            row_type: 1,
                            caret_val: true,
                            hasRows: true,
                            hours_sum: 0,
                            months_and_hours: [],
                            hasParentRow: false
                        };
                        $scope.reportRows.push(userRow);
                    } else {
                        newUserRow = false;
                    }
                }
                if ($scope.scopeIsTask == true) {
                    if (ownerIds.indexOf(obj.owner_item_id) == -1) {
                        newOwnerRow = true;
                        ownerIds.push(obj.owner_item_id);
                        ownerRow = {
                            rowsCollection: rowCollectionId,
                            owner_item_id: obj.owner_item_id,
                            owner_name: obj.owner_name,
                            user_item_id: obj.user_item_id,
                            row_type: 2,
                            caret_val: true,
                            hasRows: true,
                            hours_sum: 0,
                            months_and_hours: [],
                            hasParentRow: false,
                            responsible_name: undefined
                        };
                        if ($scope.userGrouping == true) {
                            ownerRow.hasParentRow = true;
                            ownerRow.user_item_id = obj.user_item_id
                            ownerRow.responsible_name = obj.responsible_name
                        } else {
                            ownerRow.hasParentRow = false;
                        }
                        $scope.reportRows.push(ownerRow);
                    } else {
                        newOwnerRow = false;
                    }

                    obj.row_type = 3; //Task row
                } else {
                    obj.row_type = 2; //Task owner process row
                }
                if ($scope.userGrouping == true || $scope.scopeIsTask == true) {
                    obj.hasParentRow = true;
                } else {
                    obj.hasParentRow = false;
                }
                obj.hasRows = false;
                obj.caret_val = true;
                obj.rowsCollection = rowCollectionId;

                while (i2 < monthsCount) {
                    let nextMonthInLoop = new Date(startDate.setDate(1));
                    let numberOfMonthsToAdd = i2;
                    let hours = "";
                    nextMonthInLoop.setMonth(nextMonthInLoop.getMonth() + numberOfMonthsToAdd);
                    let year_and_month_numbers = getMonthNumByDate(nextMonthInLoop);

                    serverMonthsAndHoursFound.forEach(function (obj3, i3) {
                        if (parseInt(obj3["@month"]) == parseInt(year_and_month_numbers.month) && parseInt(obj3["@year"]) == parseInt(year_and_month_numbers.year)) { //Data was found in server for the month
                            hours = parseFloat(obj3["@hours"]);
                        }
                    });

                    obj.months_and_hours.push({
                        year: year_and_month_numbers.year,
                        month: year_and_month_numbers.month,
                        hours: hours
                    });
                    if ($scope.userGrouping == true) {
                        if (newUserRow == true) {
                            userRow.months_and_hours.push({
                                year: year_and_month_numbers.year,
                                month: year_and_month_numbers.month,
                                hours: hours
                            });
                        } else {
                            for (let i3 = 0; i3 < userRow.months_and_hours.length; i3++) {
                                if (parseInt(userRow.months_and_hours[i3]["year"]) == parseInt(year_and_month_numbers.year) && parseInt(userRow.months_and_hours[i3]["month"]) == parseInt(year_and_month_numbers.month) && hours > 0) {
                                    if (userRow.months_and_hours[i3]["hours"] > 0) {
                                        userRow.months_and_hours[i3]["hours"] = (parseFloat(userRow.months_and_hours[i3]["hours"]) + parseFloat(hours));
                                    } else {
                                        userRow.months_and_hours[i3]["hours"] = parseFloat(hours);
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.scopeIsTask == true) {
                        //owner/project row that has child rows starts
                        if (newOwnerRow == true) {
                            ownerRow.months_and_hours.push({
                                year: year_and_month_numbers.year,
                                month: year_and_month_numbers.month,
                                hours: hours
                            });
                        } else {
                            for (let i3 = 0; i3 < ownerRow.months_and_hours.length; i3++) {
                                if (parseInt(ownerRow.months_and_hours[i3]["year"]) == parseInt(year_and_month_numbers.year) && parseInt(ownerRow.months_and_hours[i3]["month"]) == parseInt(year_and_month_numbers.month) && hours > 0) {
                                    if (ownerRow.months_and_hours[i3]["hours"] > 0) {
                                        ownerRow.months_and_hours[i3]["hours"] = (parseFloat(ownerRow.months_and_hours[i3]["hours"]) + parseFloat(hours));
                                    } else {
                                        ownerRow.months_and_hours[i3]["hours"] = parseFloat(hours);
                                    }
                                }
                            }
                        }
                        //owner/project row that has child rows ends
                    }
                    if (i == 0) { //header column amount is available in the first round
                        let dateHeaderObj = {
                            year: year_and_month_numbers.year,
                            month: monthNames[year_and_month_numbers.month - 1]
                        };
                        $scope.dateHeaders.push(dateHeaderObj);
                    }
                    i2++;
                }
                if ($scope.scopeIsTask == true) {
                    ownerRow.hours_sum = ownerRow.hours_sum + obj.hours_sum;
                } //scope is task
                if ($scope.userGrouping == true) {
                    userRow.hours_sum = userRow.hours_sum + obj.hours_sum;
                } //Group for users stuff
                $scope.TotalHours = $scope.TotalHours + obj.hours_sum;

                $scope.reportRows.push(obj);
            });
        }

        $scope.getDayNameByDate = function (dateParam) {
            let jsDate = new Date(dateParam);
            let dayNames = Common.getLocalShortDaysOfWeek();
            let dayNum = jsDate.getDay() - 1;
            if (dayNum < 0) {
                dayNum = 6;
            } //6 is sunday. 
            return dayNames[dayNum];
        }

        $scope.getNextDayInLoop = function (startDate, index) {
            let nextDayInLoop = new Date(angular.copy(startDate));
            let numberOfDaysToAdd = index;
            nextDayInLoop.setDate(nextDayInLoop.getDate() + numberOfDaysToAdd);
            let returnDate = CalendarService.formatDate(nextDayInLoop);
            return returnDate;
        }

        $scope.getNextDayNameInLoop = function (startDate, index) {
            let dayNames = Common.getLocalShortDaysOfWeek()
            let nextDayInLoop = new Date(angular.copy(startDate));
            let numberOfDaysToAdd = index;
            nextDayInLoop.setDate(nextDayInLoop.getDate() + numberOfDaysToAdd);
            return dayNames[nextDayInLoop.getDay()];
        }

        $scope.toggleUserRows = function (parentRow) {
            if (parentRow.caret_val == true) {
                parentRow.caret_val = false;
                parentRow.revert = true;
                $scope.reportRows.forEach(function (row, i) {
                    if (parentRow.user_item_id == row.user_item_id && row.row_type == 2) {
                        if (row.hasRows == true) {
                            $scope.toggleOwnerRows(row, 2);
                        }
                        row.caret_val = false;
                    }
                });
            } else {
                parentRow.caret_val = true;
                parentRow.revert = false;
                $scope.reportRows.forEach(function (row, i) {
                    if (parentRow.user_item_id == row.user_item_id && row.row_type == 2) {
                        if (row.hasRows == true) {
                            $scope.toggleOwnerRows(row, 3);
                        }
                        row.caret_val = true;
                    }
                });
            }
        }
        $scope.toggleOwnerRows = function (parentRow, toggleType) {
            if (parentRow.row_type == 2 && toggleType == 1 && parentRow.ignoreCaretVal !== true) {
                parentRow.ignoreCaretVal = true;
            } else {
                parentRow.ignoreCaretVal = false;
            }
            if (parentRow.caret_val == true) {
                parentRow.caret_val = false;
                parentRow.revert = true;
                $scope.reportRows.forEach(function (row, i) {
                    if (row.owner_item_id == parentRow.owner_item_id && row.row_type == 3 && row.rowsCollection == parentRow.rowsCollection) {
                        if (toggleType == 3) {
                            row.caret_val = true;
                        } else {
                            row.caret_val = false;
                        }

                    }
                });
            } else {
                parentRow.caret_val = true;
                parentRow.revert = false;
                $scope.reportRows.forEach(function (row, i) {
                    if (parentRow.owner_item_id == row.owner_item_id && row.row_type == 3 && row.rowsCollection == parentRow.rowsCollection) {
                        row.caret_val = true;
                        if (toggleType == 2) {
                            row.caret_val = false;
                        } else {
                            row.caret_val = true;
                        }

                    }
                });
            }
        }

    }

    WorkTimeReportService.$inject = ['ApiService', '$q'];

    function WorkTimeReportService(ApiService, $q) {
        let self = this;
        let WorkTimeReport = ApiService.v1api('time/workTimeReport');
        let items = ApiService.v1api('meta/items');

        self.getWorkTimeReport = function (ProcessName, ProcessParameter, Parameters) {
            let d = $q.defer();
            WorkTimeReport.new([ProcessName, ProcessParameter], Parameters).then(function (data) {
                d.resolve(data)
            }, function () {
                d.reject();
            });
            return d.promise;
        };

        self.getListByName = function (ListName) {
            return items.get([ListName]);
        };

        self.getAccumulationExcel = function(itemId, startDate, endDate) {
	        return WorkTimeReport.get(itemId +"/"+ startDate +"/"+ endDate);
        };
        
        self.getProcessesWithHoursList = function () {
            return WorkTimeReport.get(["WtrProcesses"]);
        };
	}

	var getWeekNum = function (d) {
        return moment(d).format('W');
	};

})();