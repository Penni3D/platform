(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(EmailLogConfig)
		.controller('EmailLogController', EmailLogController);

	EmailLogConfig.$inject = ['ViewsProvider'];

	function EmailLogConfig(ViewsProvider) {
		ViewsProvider.addView('emailLog', {
			controller: 'EmailLogController',
			template: 'pages/emailLog/emailLog.html',
			resolve: {
				InitialEmailLogs: function (EventLogService, ApiService, $rootScope) {
					let f = {
						SearchText: '',
						MinRow: 0,
						MaxRow: 50,
						OrderBy: null
					};
					return ApiService.v1api('settings/NotificationsData').new(["emaillogs", $rootScope.me.instance], f).then(function(response){
						return response
					});
				}
			}
		});

		ViewsProvider.addPage('emailLog', "PAGE_EMAIL_LOG", ['read']);
	}

	EmailLogController.$inject = ['$scope', 'EventLogService', 'InitialEmailLogs', 'CalendarService', 'ViewService', 'StateParameters', 'ApiService', '$rootScope', 'Translator'];

	function EmailLogController($scope, EventLogService, InitialEmailLogs, CalendarService, ViewService, StateParameters, ApiService, $rootScope, Translator) {
		$scope.data = InitialEmailLogs;

		let maxNumber = 0;
		let orderBy = null;
		let needsNewSearch = false;

		if($scope.data.length > 0) maxNumber = $scope.data[0].MaxRows;

		$scope.paging = {
			currentPage: 0,
			size: 50,
			max: maxNumber
		};


		let orderCollection = {
			"message": "DESC",
			"last_name": "DESC",
			"first_name": "DESC",
			"login": "DESC",
			"occurence": "DESC"
		};

		let currentFloor = 0;
		let currentCeiling = 50;
		let oldCurrentPage = 0;

		$scope.onPageChange = function(){
			if(oldCurrentPage > $scope.paging.currentPage){
				currentCeiling = currentFloor;
				currentFloor = currentFloor - $scope.paging.size;
			} else {
				currentFloor = currentCeiling;
				currentCeiling = currentFloor + $scope.paging.size;
			}
			oldCurrentPage = $scope.paging.currentPage;
			$scope.doSearch(true);
		};
		/*
				$scope.selectOrderByColumn = function(columnName){

					if(orderCollection[columnName] == "ASC"){
						orderCollection[columnName] = "DESC";
					} else if(orderCollection[columnName] == "DESC"){
						orderCollection[columnName] = "ASC";
					}
					orderBy = columnName;
					$scope.doSearch();
				};*/
		$scope.formatDate = function(unformattedString){
			return moment(unformattedString).format('l');
		};

		$scope.search = "";
		/*$scope.logTypes = [{'name': 'Message', 'value': 0},
			{'name': 'Warning', 'value': 1},
			{'name': 'Error', 'value': 2},
			{'name': 'DataLoss ', 'value': 3},
			{'name': 'SuccessMessage ', 'value': 4},
			{'name': 'Performance ', 'value': 5},
			{'name': 'Sql ', 'value': 6}
		];*/

		$scope.header = {
			title: "INSTANCE_MAIL_LOG",
			icons: []
		};

		$scope.opened = false;

		$scope.header.icons.push({
			icon: 'filter_list',
			onClick: function () {
				$scope.opened = !$scope.opened;
			}
		});

		$scope.clearFilters = function(){

			$scope.search = "";
			currentFloor = 0;
			currentCeiling = 50;
			orderBy = null;

			if(needsNewSearch == true){
				$scope.doSearch();
			}
		};

		$scope.getFormattedDateTime = function (time) {
			return CalendarService.formatDateTime(time);
		};

		let searchTimer
		$scope.doSearch = function (optionalParams?) {
			if(typeof optionalParams != 'undefined' && optionalParams.hasOwnProperty('fromText') && $scope.search.length < 4){
				return;
			}

			if(typeof optionalParams == 'undefined'){
				currentFloor = 0;
				currentCeiling = $scope.paging.size;
			}

			let f = {
				SearchText: $scope.search,
				MinRow: currentFloor,
				MaxRow: currentCeiling,
				OrderBy: null
			};

			if(orderBy == null || orderCollection[orderBy] == null) f.OrderBy = null;
			clearTimeout(searchTimer);
			searchTimer = setTimeout(function () {
				ViewService.lock(StateParameters.ViewId);

				return ApiService.v1api('settings/NotificationsData').new(["emaillogs", $rootScope.me.instance], f).then(function(newRows){
					$scope.paging.max = newRows.length;
					ViewService.unlock(StateParameters.ViewId);
					needsNewSearch = true;
					$scope.data = newRows;

					if($scope.data.length > 0) {
						$scope.paging.max = $scope.data[0].MaxRows;
					} else {
						$scope.paging.max = 0;
					}

				});
			}, 1500);
		}

		$scope.GetStatus = function (status) {
			if (status == 0)
				return Translator.translate('MAIL_STATUS_ERROR');
			else if (status == 1)
				return Translator.translate('MAIL_STATUS_SENT');
			else
				return Translator.translate('MAIL_STATUS_UNKNOWN');
		};
	}
})();