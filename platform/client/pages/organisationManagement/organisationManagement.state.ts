﻿(function () {
    'use strict';

    angular
        .module('core.pages')
        .config(OrganisationManagementConfig)
        .controller('OrganisationManagementController', OrganisationManagementController)
        .controller('OrganisationManagementOrganisationController', OrganisationManagementOrganisationController)
        .controller('OrganisationManagementFunctionController', OrganisationManagementFunctionController)
        .controller('OrganisationManagementLocationController', OrganisationManagementLocationController)
        .controller('OrganisationManagementCostCentersController', OrganisationManagementCostCentersController)
        .controller('OrganisationManagementCostCentersOwnersController', OrganisationManagementCostCentersOwnersController)
        .controller('OrganisationManagementCompetencyController', OrganisationManagementCompetencyController);

    OrganisationManagementConfig.$inject = ['ViewsProvider'];
    function OrganisationManagementConfig(ViewsProvider) {
        ViewsProvider.addView('organisationManagement', {
            controller: 'OrganisationManagementController',
            template: 'pages/organisationManagement/organisationManagement.html',
            resolve: {
                Organisations: function (OrganisationManagementService) {
                    return OrganisationManagementService.getUsers();
                },
                CompetencyPortfolio: function (OrganisationManagementService) {
                    return OrganisationManagementService.getPortfolioId('competency', 'Competency portfolio');
                }
            }
        });

        ViewsProvider.addView('organisationManagement.organisation', {
            controller: 'OrganisationManagementOrganisationController',
            template: 'pages/organisationManagement/organisationManagementOrganisation.html',
            resolve: {
                Organisation: function (OrganisationManagementService, StateParameters) {
                    return OrganisationManagementService.getItems('organisation', StateParameters.item_id);
                },

                Users: function (OrganisationManagementService, StateParameters) {
                    return OrganisationManagementService.getUsers(StateParameters.item_id);
                },

                Competencies: function (OrganisationManagementService) {
					return OrganisationManagementService.getPortfolioId('competency', 'Competency portfolio').then(function (portfolioId) {
						return OrganisationManagementService.getUsers('competency', portfolioId[0].ProcessPortfolioId);
					});
                },
                ProcessTabId: function (SettingsService) {
                    return SettingsService.ProcessGroups('competency').then(function (data) {
                        if (data.length > 0) {
                            return SettingsService.ProcessGroupTabs('competency', data[0].ProcessGroupId).then(function (tabs) {
                                return tabs[0].ProcessTab.ProcessTabId;
                            });
                        }

                    });
                }
            }
        });

        ViewsProvider.addView('organisationManagement.function', {
            controller: 'OrganisationManagementFunctionController',
            template: 'pages/organisationManagement/organisationManagementFunctions.html',
            resolve: {
                Functions: function (OrganisationManagementService) {
                    return OrganisationManagementService.getListItems('list_function');
                }
            }
        });


        ViewsProvider.addView('organisationManagement.location', {
            controller: 'OrganisationManagementLocationController',
            template: 'pages/organisationManagement/organisationManagementLocations.html',
            resolve: {
                Locations: function (OrganisationManagementService) {
                    return OrganisationManagementService.getListItems('list_location');
                }
            }
        });

        ViewsProvider.addView('organisationManagement.costCenters', {
            controller: 'OrganisationManagementCostCentersController',
            template: 'pages/organisationManagement/organisationManagementCostCenters.html',
            resolve: {
                Functions: function (OrganisationManagementService) {
                    return OrganisationManagementService.getListItems('list_function');
                },
                Locations: function (OrganisationManagementService) {
                    return OrganisationManagementService.getListItems('list_location');
                },
                CostCenters: function (OrganisationManagementService) {
                    return OrganisationManagementService.getCostCenters();
                }
            }
        });

        ViewsProvider.addView('organisationManagement.costCentersOwners', {
            controller: 'OrganisationManagementCostCentersOwnersController',
            template: 'pages/organisationManagement/organisationManagementCostCentersOwners.html',
            resolve: {
                CostCenters: function (OrganisationManagementService) {
                    return OrganisationManagementService.getListItems('list_cost_center');
                }
            }
        });

        ViewsProvider.addView('organisationManagement.competencies', {
            controller: 'OrganisationManagementCompetencyController',
            template: 'pages/organisationManagement/organisationManagementCompetencies.html',
            resolve: {
				PortfolioRows: function (PortfolioService, StateParameters, OrganisationManagementService) {
					var filters = PortfolioService.getActiveSettings(StateParameters.portfolioId).filters;
					filters.limit = 999;
					return OrganisationManagementService.getPortfolioId('competency', 'Competency portfolio').then(function (portfolioId) {
						return PortfolioService.getPortfolioData(
						    'competency',
                            portfolioId[0].ProcessPortfolioId,
                            {local: true},
                            {limit: 999}
                        );
					});
                },
                CompetencyPortfolio: function (OrganisationManagementService) {
                    return OrganisationManagementService.getPortfolioId('competency', 'Competency portfolio');
                },
                ProcessTabId: function (SettingsService) {
                    return SettingsService.ProcessGroups('competency').then(function (data) {
                        if (data.length > 0) {
                            return SettingsService.ProcessGroupTabs('competency', data[0].ProcessGroupId).then(function (tabs) {
                                return tabs[0].ProcessTab.ProcessTabId;
                            });
                        }

                    });
                }
            }
        });

		ViewsProvider.addPage('organisationManagement', "PAGE_ORGANISATION_MANAGEMENT", ['read', 'write']);
		ViewsProvider.addPage('organisationManagement.function', "PAGE_ORGANISATION_MANAGEMENT_FUNC", ['read', 'write']);
		ViewsProvider.addPage('organisationManagement.location', "PAGE_ORGANISATION_MANAGEMENT_LOCATION", ['read', 'write']);
		ViewsProvider.addPage('organisationManagement.costCenters', "PAGE_ORGANISATION_MANAGEMENT_CC", ['read', 'write']);
		ViewsProvider.addPage('organisationManagement.costCentersOwners', "PAGE_ORGANISATION_MANAGEMENT_CC_OWNER", ['read', 'write']);
		ViewsProvider.addPage('organisationManagement.competencies', "PAGE_ORGANISATION_MANAGEMENT_COMPETENCIES", ['read', 'write']);
    }


    OrganisationManagementController.$inject = ['$scope', 'OrganisationManagementService', 'MessageService', 'Common', 'ViewService', 'StateParameters', 'Organisations', 'CompetencyPortfolio'];
    function OrganisationManagementController($scope, OrganisationManagementService, MessageService, Common, ViewService, StateParameters, Organisations, CompetencyPortfolio) {

        var params = StateParameters;
        params.process = 'competency';
        params.portfolioId = CompetencyPortfolio[0].ProcessPortfolioId;
        
        $scope.selectedIndex = null;
        $scope.selectedContractors = [];
        $scope.selectedPartners = [];
        $scope.edit = true;

        var data = {};

        $scope.header = {
            title: 'Organisation Management',
            menu: [
                {
                    name: 'Delete',
                    onClick: function () {
                        if ($scope.selectedRows.length > 0) {
                            MessageService.confirm("Are you sure you want to delete this?", function () {
                                $scope.deleteOrganisation();
                            });
                        }
                    }
                }
            ]
        };

        $scope.data = Organisations;

        $scope.addOrganisation = function (type) {
            data = { Translation: null, type: type };

            var save = function () {
                addOrganisationSave();
                data = {};
            };

            var content = {
                buttons: [{ text: 'Discard' }, { type: 'primary', onClick: save, text: 'Save' }],
                inputs: [{ type: 'string', label: 'NAME', model: 'name' }],
                title: "Add new organisation"
            };
            MessageService.dialog(content, data)
        };

        var addOrganisationSave = function () {
            OrganisationManagementService.addItems('organisation', data).then(function (response) {
                var type;
                if (response.type == 1) type = 'subcontractors';
                else if (response.type == 2) type = 'partners';

                $scope.data[type].push(response);
            });
        };

        $scope.showOrganisation = function (id, index, type) {
            $scope.selectedIndex = index;
            $scope.selectedType = type;
            StateParameters.item_id = id;
            ViewService.view('organisationManagement.organisation', { params: StateParameters }, { split: true });
        };

        $scope.deleteRows = function (type) {
            var selectedRows;
            if (type == 'contractors')
                selectedRows = $scope.selectedContractors;
            else if (type == 'partners')
                selectedRows = $scope.selectedPartners;

            var deleteIds = Common.convertArraytoString(selectedRows, 'item_id');

            OrganisationManagementService.deleteItems('organisation', deleteIds).then(function () {
                angular.forEach(selectedRows, function (key) {
                    var index = Common.getIndexOf($scope.data[key.type], key.item_id, 'item_id');
                    $scope.data[key.type].splice(index, 1);

                    if (type == 'contractors')
                        $scope.selectedContractors = [];
                    else if (type == 'partners')
                        $scope.selectedPartners = [];
                });
            });
        }
    }

    OrganisationManagementOrganisationController.$inject = ['$scope', 'OrganisationManagementService', 'MessageService', 'SaveService', 'Common', 'ViewService', 'StateParameters', 'Organisation', 'Users', 'Competencies', 'UserService', 'ProcessTabId', 'ProcessService'];
    function OrganisationManagementOrganisationController($scope, OrganisationManagementService, MessageService, SaveService, Common, ViewService, StateParameters, Organisation, Users, Competencies, UserService, ProcessTabId,ProcessService) {
        var changedValues = [];

        $scope.organisation = Organisation;
        $scope.users = Users;
        $scope.competencies = Competencies;

        $scope.selectedIndex = null;
        $scope.selectedRows = [];

        $scope.header = {
            title: 'Organisation Management - Resource',
            menu: []
        };

        SaveService.subscribeSave($scope, function () {
            return saveItems();
        });

        var saveItems = function () {
            return OrganisationManagementService.saveItems('organisation', [$scope.organisation]).then(function () {
                changedValues = [];
            });
        };

        $scope.showCompetency = function (id, index, type) {
            $scope.selectedIndex = index;
            $scope.selectedType = type;
            StateParameters.item_id = id;
            StateParameters.process = 'competency';
            ViewService.view(ProcessTabId, { params: StateParameters }, { split: true });
        };

        $scope.changeValue = function () {
            SaveService.tick();
        };

        $scope.addnewUser = function () {
            var params = StateParameters;
            params.process = 'user';
            params.Data = {};
            params.Data.organisation = [params.item_id];
            ViewService.view('process.user.new', { params: params});
        };

        $scope.cancel = function () {
            MessageService.closeDialog();
        };

        $scope.insertUser = function () {
            var users = [];
            angular.forEach($scope.users, function (key) {
                users.push({ item_id: key, selected_item_id: StateParameters.item_id });
            });
            var message = "";
            OrganisationManagementService.insertUser(StateParameters.item_id, users).then(function (data) {
                if (data.length > 0) {

                    angular.forEach(data, function (key) {
                        UserService.getUser(key.item_id).then(function (data) {
                            message += data.name + ' - Already exist another organisation<br>';
                            var index = _.findIndex($scope.users, key.item_id);
                            if (angular.isDefined(index))
                                $scope.users.splice(index, 1);
                        });
                    });
                    if (message != "") {
                        setTimeout(function () {
                            MessageService.msgBox(message);
                        }, 500);
                    }
                }
            }).then(function () {
                if (message == "") {
                    OrganisationManagementService.getUsers('competency', StateParameters.item_id).then(function (data) {
                        $scope.competencies = data;
                    });
                }

            });
        };

        ProcessService.subscribeNewItem($scope, function (itemId) {
            $scope.users.push(itemId);
        }, 'user');
    }

    OrganisationManagementFunctionController.$inject = ['$scope', 'OrganisationManagementService', 'ViewService', 'MessageService', 'StateParameters','SaveService', 'Common', 'Functions'];
    function OrganisationManagementFunctionController($scope, OrganisationManagementService, ViewService, MessageService, StateParameters, SaveService, Common, Functions) {

        $scope.selectedIndex = null;
        $scope.selectedRows = [];
        $scope.edit = true;

        ViewService.footbar(StateParameters.ViewId, {template: 'pages/organisationManagement/functionsFootbar.html'});
        
        var newValuesArray = [];
        var changedItems = [];

        var data = {};

        $scope.header = {
            title: 'Organisation Management - Functions',
            menu: []
        };

        $scope.data = Functions;
        $scope.paging = { size: 25, currentPage: 0 };

        $scope.addFunction = function () {
            data = { Translation: null };

            var save = function () {
                OrganisationManagementService.addItems('list_function', data).then(function (response) {
                    //$scope.data.push(response);
                    OrganisationManagementService.getListItems('list_function').then(function(o){
                        $scope.data = o
                    });
                    OrganisationManagementService.getCostCenters();
                });
            };

            var content = {
                buttons: [{ text: 'Discard' }, { type: 'primary', onClick: save, text: 'Save' }],
                inputs: [{ type: 'string', label: 'NAME', model: 'list_item' }],
                title: "Add new function"
            };
            MessageService.dialog(content, data)
        };

        SaveService.subscribeSave($scope, function () {
            if (changedItems.length > 0) {
                saveList();
                changedItems = [];
            }
        });

        var saveList = function () {
            return OrganisationManagementService.saveItems('list_function', $scope.data);
        };

        $scope.changeValue = function (params) {
            newValuesArray.push({ id: params.item_id });
            var noDuplicateObjects = {};

            var l = newValuesArray.length;

            for (var i = 0; i < l; i++) {
                var item = newValuesArray[i];
                noDuplicateObjects[item.id] = item;
            }

            var nonDuplicateArray = [];

            for (var item in noDuplicateObjects) {
                nonDuplicateArray.push(noDuplicateObjects[item]);
            }
            changedItems = nonDuplicateArray;
        };

        $scope.deleteRows = function () {
            MessageService.confirm("Are you sure you want to delete this?", function () {
                var deleteIds = Common.convertArraytoString($scope.selectedRows, 'item_id');
                OrganisationManagementService.deleteItems('list_function', deleteIds).then(function () {
                    angular.forEach($scope.selectedRows, function (key) {
                        var index = Common.getIndexOf($scope.data, key.item_id, 'item_id');
                        $scope.data.splice(index, 1);
                    });
                    OrganisationManagementService.deleteListItems('function', deleteIds);

                    $scope.selectedRows = [];
                });
            });
        }
    }

    OrganisationManagementLocationController.$inject = ['$scope', 'OrganisationManagementService', 'MessageService', 'StateParameters', 'ViewService', 'SaveService', 'Common', 'Locations'];
    function OrganisationManagementLocationController($scope, OrganisationManagementService, MessageService, StateParameters, ViewService, SaveService, Common, Locations) {

        $scope.selectedRows = [];
        $scope.edit = true;

        var newValuesArray = [];
        var changedItems = [];

        var data = {};
    
        ViewService.footbar(StateParameters.ViewId, {template: 'pages/organisationManagement/locationsFootbar.html'});

        $scope.paging = { size: 25, currentPage: 0 };
        

        $scope.header = {
            title: 'Organisation Management - Locations',
            menu: []
        };

        $scope.data = Locations;
        $scope.addLocation = function () {
            data = { Translation: null };

            var save = function () {
                OrganisationManagementService.addItems('list_location', data).then(function (response) {
                    //$scope.data.push(response);
                    OrganisationManagementService.getListItems('list_location').then(function(o){
                        $scope.data = o
                    });
                    OrganisationManagementService.getCostCenters();
                });
            };

            var content = {
                buttons: [{ text: 'Discard' }, { type: 'primary', onClick: save, text: 'Save' }],
                inputs: [{ type: 'string', label: 'NAME', model: 'list_item' }],
                title: "Add new location"
            };
            MessageService.dialog(content, data)
        };

        SaveService.subscribeSave($scope, function () {
            if (changedItems.length > 0) {
                saveList();
                changedItems = [];
            }
        });

        var saveList = function () {
            return OrganisationManagementService.saveItems('list_location', $scope.data);
        };

        $scope.changeValue = function (params) {
            newValuesArray.push({ id: params.item_id });
            var noDuplicateObjects = {};

            var l = newValuesArray.length;

            for (var i = 0; i < l; i++) {
                var item = newValuesArray[i];
                noDuplicateObjects[item.id] = item;
            }

            var nonDuplicateArray = [];

            for (var item in noDuplicateObjects) {
                nonDuplicateArray.push(noDuplicateObjects[item]);
            }
            changedItems = nonDuplicateArray;
        };

        $scope.deleteRows = function () {
            MessageService.confirm("Are you sure you want to delete this?", function () {
                var deleteIds = Common.convertArraytoString($scope.selectedRows, 'item_id');
                OrganisationManagementService.deleteItems('list_location', deleteIds).then(function () {
                    angular.forEach($scope.selectedRows, function (key) {
                        var index = Common.getIndexOf($scope.data, key.item_id, 'item_id');
                        $scope.data.splice(index, 1);
                    });
                    OrganisationManagementService.deleteListItems('location', deleteIds);
                    $scope.selectedRows = [];
                });
            });
        }

    }

    OrganisationManagementCostCentersController.$inject = ['$scope', 'OrganisationManagementService', 'SaveService', 'Locations', 'Functions', 'CostCenters', 'StateParameters', 'ViewService'];
    function OrganisationManagementCostCentersController($scope, OrganisationManagementService, SaveService, Locations, Functions, CostCenters, StateParameters, ViewService) {

        $scope.locations = Locations;
        $scope.functions = Functions;
        $scope.costCenters = CostCenters;

        $scope.header = {
            title: 'Organisation Management - Cost centers',
            menu: []
        };

        var newValuesArray = [];
        var changedItems = [];

        var saveCostCenters = function () {
            var Items = [];

            angular.forEach(changedItems, function (key) {
                Items.push($scope.costCenters[key.location_id][key.function_id]);
            });

            return OrganisationManagementService.saveItems('list_cost_center', Items).then(function () {
                changedItems = [];
            });

        };

        SaveService.subscribeSave($scope, function () {
            if (changedItems.length > 0) {
                return saveCostCenters();
            }
        });

        $scope.changeValue = function (params) {
            newValuesArray.push({ cost_center_id: params.cost_center_id, location_id: params.location_id, function_id: params.function_id });
            var noDuplicateObjects = {};

            var l = newValuesArray.length;

            for (var i = 0; i < l; i++) {
                var item = newValuesArray[i];
                noDuplicateObjects[item.cost_center_id] = item;
            }

            var nonDuplicateArray = [];

            for (var item in noDuplicateObjects) {
                nonDuplicateArray.push(noDuplicateObjects[item]);
            }
            changedItems = nonDuplicateArray;
        };
    }

    OrganisationManagementCostCentersOwnersController.$inject = ['$q', '$scope', 'OrganisationManagementService', 'SaveService', 'Common', 'CostCenters', 'StateParameters', 'ViewService'];
    function OrganisationManagementCostCentersOwnersController($q, $scope, OrganisationManagementService, SaveService, Common, CostCenters, StateParameters, ViewService) {

        $scope.costCenters = CostCenters;

        $scope.header = {
            title: 'Organisation Management - Cost Centers owner',
            menu: []
        };

        ViewService.footbar(StateParameters.ViewId, {template: 'pages/organisationManagement/costCenterFootbar.html'});

        $scope.paging = { size: 25, currentPage: 0 };


        var newValuesArray = [];
        var changedItems = [];

        var saveCostCenters = function () {
            var Items = [];

            angular.forEach(changedItems, function (key) {
                var index = Common.getIndexOf($scope.costCenters, key.item_id, 'item_id');
                Items.push($scope.costCenters[index]);
            });

            return OrganisationManagementService.saveItems('list_cost_center', Items).then(function () {
                changedItems = [];
            });

        };

        SaveService.subscribeSave($scope, function () {
            if (changedItems.length > 0) {
                saveCostCenters();
            }
        });

        $scope.changeValue = function (params) {
            newValuesArray.push({ item_id: params.item_id });
            var noDuplicateObjects = {};

            var l = newValuesArray.length;

            for (var i = 0; i < l; i++) {
                var item = newValuesArray[i];
                noDuplicateObjects[item.item_id] = item;
            }

            var nonDuplicateArray = [];

            for (var item in noDuplicateObjects) {
                nonDuplicateArray.push(noDuplicateObjects[item]);
            }
            changedItems = nonDuplicateArray;
        };
    }


    OrganisationManagementCompetencyController.$inject = ['$scope', 'OrganisationManagementService', 'MessageService', 'Common', 'ViewService', 'StateParameters', 'PortfolioRows', 'CompetencyPortfolio', 'ProcessTabId', 'ProcessService'];
    function OrganisationManagementCompetencyController($scope, OrganisationManagementService, MessageService, Common, ViewService, StateParameters, PortfolioRows, CompetencyPortfolio, ProcessTabId, ProcessService) {

        $scope.competencies = PortfolioRows.rowdata;

        ViewService.footbar(StateParameters.ViewId, {template: 'pages/organisationManagement/competencyFootbar.html'});

        $scope.paging = { size: 25, currentPage: 0 };

        $scope.openRow = function (item_id) {
            var toState = {};
            toState.itemId = item_id;
            toState.process = 'competency';
            toState.tabId = ProcessTabId;

            ViewService.view('process.tab', { params: toState });
        };

        $scope.selectedRows = [];

        $scope.deleteRows = function () {

            var deleteIds = Common.convertArraytoString($scope.selectedRows, 'item_id');

            OrganisationManagementService.deleteItems('competency', deleteIds).then(function () {
                angular.forEach($scope.selectedRows, function (key) {
                    var index = Common.getIndexOf($scope.competencies, key.item_id, 'item_id');
                    $scope.competencies.splice(index, 1);
                });

                $scope.selectedRows = [];
            });
        };


        $scope.header = {
            title: 'Organisation Management - Competencies',
            menu: [
                {
                    name: 'Delete',
                    onClick: function () {
                        if ($scope.selectedRows.length > 0) {
                            MessageService.confirm("Are you sure you want to delete this?", function () {
                                $scope.deleteRows();
                            });
                        }
                    }
                }
            ]
        };

        $scope.newCompetency = function () {
            var params = StateParameters;
            params.process = 'competency';
            params.portfolioId = CompetencyPortfolio[0].ProcessPortfolioId;
            params.closeAfterCreate = true;
            ViewService.view('new.process', { params: params });
        };
    }
})();