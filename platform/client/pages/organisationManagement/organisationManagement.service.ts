﻿(function () {
    'use strict';

    angular
        .module('core.pages')
        .service('OrganisationManagementService', OrganisationManagementService);

    OrganisationManagementService.$inject = ['ApiService'];
    function OrganisationManagementService(ApiService) {
        var self = this;

        self.Items = ApiService.v1api('meta/Items');
        self.ItemColumns = ApiService.v1api('meta/ItemColumns');
        self.ProcessPortfolios = ApiService.v1api('meta/ProcessPortfolios');
        self.listProcesses = ApiService.v1api('meta/listProcesses');
        self.OrganisationManagement = ApiService.v1api('settings/OrganisationManagement');
        self.OrganisationManagementCostCenter = ApiService.v1api('settings/OrganisationManagementCostCenter');


        self.getItemColumns = function (process) {
            return self.ItemColumns.get([process]);
        };

        self.getCostCenters = function () {
            return self.OrganisationManagementCostCenter.get([]);
        };

        self.getPortfolioId = function (process, portfolio) {
            return self.ProcessPortfolios.get([process, portfolio]);
        };

        self.addItems = function (ProcessName, data) {
            return self.Items.new([ProcessName], data);
        };

        self.getUsers = function (selected_item_id, type) {
            return self.OrganisationManagement.get([selected_item_id, type]);
        };

        self.insertUser = function (selected_item_id, data) {
            return self.OrganisationManagement.new([selected_item_id], data);
        };
        self.getItems = function (ProcessName, item_id) {
            return self.Items.get([ProcessName, item_id]);
        };

        self.getListItems = function (ProcessName) {
            return self.OrganisationManagement.get([ProcessName, 'order', 'list_item']);
        };


        self.newItems = function (ProcessName, data) {
            return self.Items.new([ProcessName], data);
        };

        self.saveItems = function (ProcessName, data) {
            return self.listProcesses.save([ProcessName], data);
        };

        self.deleteItems = function (ProcessName, deleteIds) {
            return self.Items.delete([ProcessName, deleteIds]);
        };

        self.deleteListItems = function (type, deleteIds) {
            return self.OrganisationManagement.delete([type, deleteIds]);
        };


        
    }

})();