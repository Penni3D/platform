﻿(function () {
    'use strict';

    angular
        .module('core.pages')
        .config(IntegrationConfig)
        .controller('IntegrationController', IntegrationController);

	IntegrationConfig.$inject = ['ViewsProvider'];
    function IntegrationConfig(ViewsProvider) {
        ViewsProvider.addView('integration', {
            controller: 'IntegrationController',
            template: 'pages/integration/integration.html',
            resolve: {
                Keys: function (ApiService) {
	                return ApiService.v1api('integration/Config/keys').get().then(function (data) {
		                angular.forEach(data, function (d) {
		                	if (d.user_item_id != null) {
				                d.userArr = [d.user_item_id];
			                } else {
				                d.userArr = [];
			                }
		                });
		                return data;
	                });
                }
            }
        });

		ViewsProvider.addPage('integration', "PAGE_INTEGRATION", ['read', 'write']);
	}

	IntegrationController.$inject = ['$scope', 'Keys', 'SaveService', '$q', 'ApiService','ViewService','StateParameters'];
    function IntegrationController($scope, Keys, SaveService, $q, ApiService,ViewService,StateParameters) {
        $scope.keys = Keys;
	    $scope.selectedRows = [];
	    $scope.selectedRows2 = [];
	    $scope.edit = true;
	    $scope.tokens = [];
	    $scope.activeKey = "";
	    $scope.keySelected = false;
	    
	    let changedIds = [];
	    let changedIds2 = [];
	    
	    $scope.deleteSelections = function () {
		    let ids1 = [];
		    angular.forEach($scope.selectedRows, function (d) {
		    	ids1.push(d.id);
		    	let index = -1;
		    	for(let i=0; i<$scope.keys.length; i++) {
		    		if ($scope.keys[i].instance_integration_key_id == d.id) {
		    			index = i;
				    }
			    }
			    if (index > -1) $scope.keys.splice(index, 1);
		    });
		    let ids2 = [];
		    angular.forEach($scope.selectedRows2, function (d) {
			    ids2.push(d.id);
			    let index = -1;
			    for(let i=0; i<$scope.tokens.length; i++) {
				    if ($scope.tokens[i].instance_integration_token_id == d.id) {
					    index = i;
				    }
			    }
			    if (index > -1) $scope.tokens.splice(index, 1);
		    });
		    if (ids1.length > 0) ApiService.v1api('integration/Config/keys/delete').new([], ids1).then(function () {} );
		    if (ids2.length > 0) ApiService.v1api('integration/Config/tokens/delete').new([], ids2).then(function () {});
	    };

	    SaveService.subscribeSave($scope, function () {
		    let d = $q.defer();
		    let promises = [];
		    if (changedIds.length > 0) {
			    let changedData = [];
			    
			    angular.forEach($scope.keys, function (d) {
				    if (changedIds.indexOf(d.instance_integration_key_id) > -1) {
					    changedData.push(d);
				    } 
			    });
				promises.push(ApiService.v1api('integration/Config/keys').save([], changedData).then(function (keys) {
					angular.forEach(keys, function (k) {
						if (k.not_unique) {
							angular.forEach($scope.keys, function (d, index) {
								if (k.instance_integration_key_id == d.instance_integration_key_id) {
									$scope.keys[index].not_unique = true;
								}
							});
						}
					});
				}));
				changedIds = [];
		    }
		    if (changedIds2.length > 0) {
			    let changedData2 = [];
			    angular.forEach($scope.tokens, function (d) {
				    if (changedIds2.indexOf(d.instance_integration_token_id) > -1) {
					    changedData2.push(d);
				    }
			    });
				promises.push(ApiService.v1api('integration/Config/tokens').save([], changedData2));
				changedIds2 = [];
		    }
		    
		    $q.all(promises).then(function () {
			    d.resolve();
		    });
		    
		    return d.promise;
	    });	    
	    
	    $scope.header = { title: 'INTEGRATION_PAGE' };
	    ViewService.footbar(StateParameters.ViewId, { template: 'pages/integration/integrationFootbar.html' });
	    
	    $scope.changeValue = function (p) {
	    	if (changedIds.indexOf(p.id) === -1) changedIds.push(p.id);
	    };

	    $scope.changeValue2 = function (p) {
		    if (changedIds2.indexOf(p.id) === -1) changedIds2.push(p.id);
	    };

	    $scope.showTokens = function (key) {
	    	if (!key && key.key != "") return;
		    $scope.activeKey = key.key;
		    $scope.keySelected = true;
		    ApiService.v1api('integration/Config/tokens').get([key.key]).then(function (tokens) {
		    	$scope.tokens = tokens;
		    });
	    };
	    
	    $scope.addKey = function ()  {
		    let newKey = [{}];
		    ApiService.v1api('integration/Config/keys').new([], newKey).then(function (keys) {
			    angular.forEach(keys, function (d) {
			    	$scope.keys.push(d);
			    });
		    });
	    };

	    $scope.addToken = function ()  {
	    	let newToken = [{"valid_until": new Date()}];
		    ApiService.v1api('integration/Config/tokens').new([$scope.activeKey], newToken).then(function (tokens) {
			    angular.forEach(tokens, function (d) {
				    $scope.tokens.push(d);
			    });
		    });
		};
    }
})();