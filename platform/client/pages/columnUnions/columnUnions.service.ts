﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.service('InstanceUnionsService', InstanceUnionsService);

	InstanceUnionsService.$inject = ['$q', 'ApiService', '$rootScope', 'Common'];

	function InstanceUnionsService($q, ApiService, $rootScope, Common) {
		let self = this;

		let columnUnions = ApiService.v1api('settings/InstanceUnions');

		self.getUnions = function () {
			return columnUnions.get();
		}

		self.getUnion = function (instanceUnionId) {
			return columnUnions.get([instanceUnionId]);
		}

		self.addUnion = function (data) {
			return columnUnions.new([], data);
		}

		self.addProcessToUnion = function (instanceUnionId, process) {
			let data = {
				InstanceUnionId: instanceUnionId,
				Process: process
			}

			return columnUnions.new(['addProcess'], data);
		}

		self.saveProcesses = function (processes) {
			return columnUnions.save(['saveProcesses'], processes);
		}

		self.getUnionProcesses = function (instanceUnionId) {
			return columnUnions.get(['getUnionProcesses', instanceUnionId]);
		}

		self.addUnionRow = function (instanceUnionId) {
			let data = {
				InstanceUnionId: instanceUnionId
			}

			return columnUnions.new(['addUnionRow'], data);
		}

		self.getUnionRows = function (instanceUnionId) {
			return columnUnions.get(['getUnionRows', instanceUnionId]);
		}

		self.saveUnionColumnsAndRows = function (unions, rows) {
			return columnUnions.save(['saveUnionColumnsAndRows'], {Item1: unions, Item2: rows});
		}

		self.getUnionColumns = function (instanceUnionId) {
			return columnUnions.get(['getUnionColumns', instanceUnionId]);
		}

		self.deleteUnionProcess = function (unionProcessId) {
			return columnUnions.delete(['deleteUnionProcess', unionProcessId]);
		}

		self.deleteUnionRow = function (unionRowId) {
			return columnUnions.delete(['deleteUnionRow', unionRowId]);
		}

		self.deleteUnion = function (unionId) {
			return columnUnions.delete(['deleteUnion', unionId]);
		}

		self.beautifyItemColumns = function (cols, portfolioId, allColumns, datatypeColumns) {
			angular.forEach(cols, function (col) {
				if ([7, 8, 9, 11, 12, 14, 15, 17, 18, 19, 21, 22, 60, 65].indexOf(col.DataType) > -1) return;
				col.LanguageTranslation[$rootScope.clientInformation.locale_id] += " (" + Common.getDataTypeTranslation(col.DataType) + ")";
				allColumns[col.ItemColumnId] = col;
				if (angular.isUndefined(datatypeColumns[portfolioId])) {
					datatypeColumns[portfolioId] = {};
				}
				if (angular.isUndefined(datatypeColumns[portfolioId][col.DataType])) {
					if (col.DataType == 5 || col.DataType == 6) {
						datatypeColumns[portfolioId][col.DataType] = {};
					} else {
						datatypeColumns[portfolioId][col.DataType] = [];
					}
				}
				if (col.DataType == 5 || col.DataType == 6) {
					if (angular.isUndefined(datatypeColumns[portfolioId][col.DataType][col.DataAdditional])) {
						datatypeColumns[portfolioId][col.DataType][col.DataAdditional] = [];
					}
					datatypeColumns[portfolioId][col.DataType][col.DataAdditional].push(col);
				} else {
					datatypeColumns[portfolioId][col.DataType].push(col);
				}
			});
		}

	}

})();