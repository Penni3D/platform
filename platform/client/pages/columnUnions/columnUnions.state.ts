﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(InstanceUnionsConfig)
		.controller('InstanceUnionsController', InstanceUnionsController)
		.controller('AddUnionDialogController', AddUnionDialogController)
		.controller('InstanceUnionController', InstanceUnionController);

	InstanceUnionsConfig.$inject = ['ViewsProvider'];

	function InstanceUnionsConfig(ViewsProvider) {
		ViewsProvider.addView('InstanceUnions', {
			controller: 'InstanceUnionsController',
			template: 'pages/columnUnions/columnUnions.html',
			resolve: {
				Unions: function (InstanceUnionsService) {
					return InstanceUnionsService.getUnions();
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				}
			}
		});

		ViewsProvider.addView('InstanceUnion', {
			controller: 'InstanceUnionController',
			template: 'pages/columnUnions/columnUnion.html',
			resolve: {
				Union: function (InstanceUnionsService, StateParameters) {
					return InstanceUnionsService.getUnion(StateParameters.InstanceUnionId);
				},
				UnionProcesses: function (InstanceUnionsService, StateParameters) {
					return InstanceUnionsService.getUnionProcesses(StateParameters.InstanceUnionId);
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				},
				UnionRows: function (InstanceUnionsService, StateParameters) {
					return InstanceUnionsService.getUnionRows(StateParameters.InstanceUnionId);
				},
				UnionColumns: function (InstanceUnionsService, StateParameters) {
					return InstanceUnionsService.getUnionColumns(StateParameters.InstanceUnionId);
				},
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				}
			},
			afterResolve: {
				ItemColumns: function (ProcessService, UnionProcesses, $q, $rootScope, Common, PortfolioService, InstanceUnionsService) {
					let promises = [];
					let iCols = {};

					angular.forEach(UnionProcesses, function (p) {
						if (angular.isUndefined(iCols[p.Process])) {
							let ppp = PortfolioService.getPortfolioColumns(p.Process, p.PortfolioId).then(function(cols){
								let colts = [];
								_.each(cols, function(col){
									colts.push(col.ItemColumn);
								})
								iCols[p.PortfolioId] = colts;
							})
							promises.push(ppp);
						}
					});
					return $q.all(promises).then(function () {
							let allCols = {};
							let dataTypes = {};
							angular.forEach(iCols, function (cols, p) {
								InstanceUnionsService.beautifyItemColumns(cols, p, allCols, dataTypes);
							});

							return {cols: iCols, types: dataTypes, allCols: allCols};
						}
					)
				},
				ProcessDict: function (Processes) {
					let processDict = {};
					angular.forEach(Processes, function (p) {
						processDict[p.ProcessName] = p;
					});
					return processDict;
				}
			}
		});

		ViewsProvider.addPage('InstanceUnions', "PAGE_INSTANCE_UNIONS", ['read', 'write']);
	}

	InstanceUnionsController.$inject = ['$scope', 'InstanceUnionsService', 'StateParameters', 'MessageService', 'Unions', 'Processes', 'ViewService'];

	function InstanceUnionsController($scope, InstanceUnionsService, StateParameters, MessageService, Unions, Processes, ViewService) {
		$scope.unions = Unions;

		let addUnion = function () {
			let params = {
				template: 'pages/columnUnions/addUnion.html',
				controller: 'AddUnionDialogController',
				locals: {
					Unions: Unions
				}
			};

			MessageService.dialogAdvanced(params);
		}

		$scope.header = {
			title: "INSTANCE_UNIONS",
			icons: [
				{
					icon: 'add',
					onClick: addUnion
				}
			]
		};

		$scope.showUnion = function (i, p) {
			let params = {
				InstanceUnionId: p.InstanceUnionId,
				UnionProcess: p.Process
			};
			ViewService.view('InstanceUnion',
				{
					params: params
				},
				{
					split: false,
					storeInCookies: false
				});
		}

		$scope.deleteUnion = function (index, p) {
			MessageService.confirm('INSTANCE_UNION_DELETE', function () {
				InstanceUnionsService.deleteUnion(p).then(function (d) {
					for (let i = 0; i < $scope.unions.length; i++) {
						if ($scope.unions[i].InstanceUnionId == p) {
							$scope.unions.splice(i, 1);
						}
					}
				});
			})
		}
	}

	AddUnionDialogController.$inject = ['$scope', 'InstanceUnionsService', 'MessageService', 'ProcessesService', 'Translator', 'Unions', 'Common'];

	function AddUnionDialogController($scope, InstanceUnionsService, MessageService, ProcessesService, Translator, Unions, Common) {
		let tickTimeout;
		$scope.disabled = true;
		$scope.buttonText = 'SAVE';
		$scope.languageTranslation = {};
		$scope.variable = '';

		function checkVariableUniqueness() {
			if ($scope.variable.length > 50) {
				$scope.variable = $scope.variable.substr(0, 50);
			}

			clearTimeout(tickTimeout);
			tickTimeout = setTimeout(function () {
				ProcessesService.isUniqueVariable($scope.variable).then(function (isUnique) {
					if (isUnique) {
						$scope.disabled = false;
						$scope.buttonText = Translator.translate('SAVE');
					} else {
						$scope.buttonText = Translator.translate('ALREADY_IN_USE');
					}
				});
			}, 1500);
		}

		$scope.variableChange = function () {
			$scope.disabled = true;

			if ($scope.variable) {
				$scope.variable = Common.formatVariableName($scope.variable);
				checkVariableUniqueness();
			}
		};

		$scope.translationChange = function () {
			$scope.disabled = true;
			let translation = Translator.translation($scope.languageTranslation);
			if (translation) {
				$scope.variable = Common.formatVariableName(translation);
				checkVariableUniqueness();
			}
		};

		$scope.save = function () {

			let data = {
				Variable: $scope.variable,
				LanguageTranslation: $scope.languageTranslation,
			};

			InstanceUnionsService.addUnion(data).then(function (u) {
				Unions.push(u);
				MessageService.closeDialog();
			}, function () {
				$scope.disabled = false;
			});
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};
	}

	InstanceUnionController.$inject = ['$scope', 'InstanceUnionsService', 'MessageService', 'ProcessesService',
		'Translator', 'Union', 'UnionProcesses', 'StateParameters', 'Processes',
		'UnionRows', 'ItemColumns', 'UnionColumns', 'SaveService', 'ProcessService', 'ProcessDict', '$timeout', 'SettingsService', 'PortfolioService', 'ViewService', 'Portfolios', '$q'];

	function InstanceUnionController($scope, InstanceUnionsService, MessageService, ProcessesService,
	                                 Translator, Union, UnionProcesses, StateParameters, Processes,
	                                 UnionRows, ItemColumns, UnionColumns, SaveService, ProcessService, ProcessDict, $timeout, SettingsService, PortfolioService, ViewService, Portfolios, $q) {

		$scope.header = {
			title: Translator.translation(Union.LanguageTranslation)
		};

		$scope.unionProcesses = UnionProcesses;
		$scope.unionRows = UnionRows;
		$scope.unionPortfolios = [];
		$scope.itemColumns = ItemColumns.cols;
		$scope.types = ItemColumns.types;
		$scope.allCols = ItemColumns.allCols;
		$scope.columnUnions = UnionColumns;
		$scope.processDict = ProcessDict;

		$scope.portfolios = {};
		_.each($scope.processDict, function (p) {
			$scope.portfolios[p.ProcessName] = _.filter(Portfolios, function (o) {
				return o.Process == p.ProcessName;
			});
		});

		$scope.optionsInitialized = false;

		$timeout(function () {
			PortfolioService.getPortfolios(StateParameters.UnionProcess).then(function (portfolios) {
				$scope.unionPortfolios = portfolios;
				$scope.optionsInitialized = true;
			});

		}, 100);

		$scope.addUnionProcess = function () {
			let model = {
				process: ''
			};
			MessageService.dialog({
				title: 'ADD_PROCESS',
				inputs: [{
					type: 'select',
					model: 'process',
					label: 'SELECT_PROCESS',
					options: Processes,
					optionName: 'LanguageTranslation',
					optionValue: 'ProcessName'
				}],
				buttons: [
					{
						text: 'MSG_CANCEL'
					},
					{
						type: 'primary',
						text: 'ADD',
						onClick: function () {
							let uProcess = model.process[0];
							$scope.optionsInitialized = false;
							return InstanceUnionsService.addProcessToUnion(StateParameters.InstanceUnionId, uProcess).then(function (u) {
								$scope.unionProcesses.push(u);
								MessageService.closeDialog();
							});
						}
					}
				]
			}, model);
		}

		let data;
		$scope.addUnionPortfolio = function () {

			data = {LanguageTranslation: {}, type: 0};

			let discard = function () {
			};

			let save = function () {
				let portfolio = {
					ProcessPortfolioType: 0,
					ProcessPortfolioId: 0,
					LanguageTranslation: data.LanguageTranslation,
					IgnoreRights: true
				};
				SettingsService.ProcessPortfoliosAdd(StateParameters.UnionProcess, portfolio).then(function (portfolio) {

					let condition = {
						LanguageTranslation: data.LanguageTranslation,
						Process: StateParameters.UnionProcess,
						States: ['portfolio.read'],
						Features: ['portfolio'],
						ConditionId: 0,
						Translation: {},
						OrderNo: 'U',
						ConditionGroupId: 0,
						ProcessPortfolioIds: [portfolio.ProcessPortfolioId]
					};
					SettingsService.ConditionsAdd(StateParameters.UnionProcess, condition);

					$scope.unionPortfolios.push(portfolio);
					data = {};
				});
			};

			let content = {
				buttons: [{onClick: discard, text: 'Discard'}, {type: 'primary', onClick: save, text: 'Save'}],
				inputs: [{type: 'translation', label: 'PORTFOLIO_NAME', model: 'LanguageTranslation'}],
				title: 'ADD_NEW_PORTFOLIO'
			};
			MessageService.dialog(content, data);
		}

		$scope.deleteUnionPortfolio = function (portfolioId) {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				SettingsService.ProcessPortfoliosDelete(StateParameters.UnionProcess, portfolioId.toString()).then(function () {
					PortfolioService.getPortfolios(StateParameters.UnionProcess).then(function (portfolios) {
						$scope.unionPortfolios = portfolios;
					});
				});
			});
		}

		$scope.showPortfolio = function (portfolioId) {
			let params = StateParameters;
			params.process = StateParameters.UnionProcess;
			params.limited = true;
			params.ProcessPortfolioId = portfolioId;
			params.type = 'portfolios';

			ViewService.view('settings.portfolios.fields', {params: params}, {split: true, size: 'wider'});
		}

		$scope.addUnionRow = function () {
			InstanceUnionsService.addUnionRow(StateParameters.InstanceUnionId).then(function (ur) {
				InstanceUnionsService.getUnionRows(StateParameters.InstanceUnionId).then(function (unions) {
					$scope.unionRows = unions;
				});
			})
		}

		$scope.changeColumn = function (index) {
			SaveService.tick();
			if (index == 0) {
				$scope.optionsInitialized = false;
				$timeout(function () {
					$scope.optionsInitialized = true
				}, 100);
			}
		}

		let getPortfolioColumns = function(portfolioId, process){
			if(!$scope.itemColumns[portfolioId]){
				PortfolioService.getPortfolioColumns(process, portfolioId).then(function(portColumns){
					let c = [];
					_.each(portColumns, function(pc){
						c.push(pc.ItemColumn);
					})
					InstanceUnionsService.beautifyItemColumns(c, portfolioId, $scope.allCols, $scope.types);
					$scope.itemColumns[portfolioId] = c;
					$scope.optionsInitialized = true;
				})
			}
		}

		$scope.changeProcess = function (process) {
			getPortfolioColumns(process.PortfolioId, process.Process);
			SaveService.tick();
		}

		$scope.checkTableVisibilities = function(){
			return $scope.unionProcesses.length > 1 && _.filter($scope.unionProcesses, function(o) { return o.PortfolioId == 0; }).length == 0;
		}

		SaveService.subscribeSave($scope, function () {
			let promises = [
				InstanceUnionsService.saveUnionColumnsAndRows($scope.columnUnions, $scope.unionRows),
				InstanceUnionsService.saveProcesses($scope.unionProcesses)
			];
			return $q.all(promises);
		});

		$scope.deleteUnionProcess = function (unionProcessId) {
			MessageService.confirm('INSTANCE_UNION_PROCESS_DELETE', function () {
				return InstanceUnionsService.deleteUnionProcess(unionProcessId).then(function (d) {
					for (let i = 0; i < $scope.unionProcesses.length; i++) {
						if ($scope.unionProcesses[i].InstanceUnionProcessId == unionProcessId) {
							$scope.unionProcesses.splice(i, 1);
						}
					}
				});
			})
		}

		$scope.deleteUnionRow = function (unionRowId) {
			MessageService.confirm('INSTANCE_UNION_ROW_DELETE', function () {
				return InstanceUnionsService.deleteUnionRow(unionRowId).then(function (d) {
					for (let i = 0; i < $scope.unionRows.length; i++) {
						if ($scope.unionRows[i].InstanceUnionRowId == unionRowId) {
							$scope.unionRows.splice(i, 1);
						}
					}
				});
			})
		}
	}

})();