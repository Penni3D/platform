(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(UserGroupInfoConfig)
		.controller('UserGroupInfoController', UserGroupInfoController);

	UserGroupInfoConfig.$inject = ['ViewsProvider'];

	function UserGroupInfoConfig(ViewsProvider) {
		ViewsProvider.addView('userGroupInfo', {
			controller: 'UserGroupInfoController',
			template: 'pages/userGroupInfo/userGroupInfo.html',
			resolve: {
				UserGroups: function (ApiService) {
					return ApiService.v1api('userGroupInfo/userGroups').get(['all', 0]).then(function (userGroups) {
						return userGroups;
					});
				},
				Access: function (ViewConfigurator, StateParameters) {
					return ViewConfigurator.getRights('userGroupInfo', StateParameters.NavigationMenuId).then(function (states) {
						return states.indexOf("read") > -1;
					});
				}
			}
		});

		ViewsProvider.addPage('userGroupInfo', "PAGE_USER_GROUP_INFO", ['read', 'conditions', 'admin']);
	}

	UserGroupInfoController.$inject = ['$scope', 'UserGroups', 'Access', 'Translator'];

	function UserGroupInfoController($scope, UserGroups, Access, Translator) {
		if (Access) $scope.userGroups = UserGroups;
		
		$scope.header.title = 'PAGE_USER_GROUP_INFO';

		$scope.titles = [
			{id: 0, value: 'USER_GROUP_INFO_TITLE0'},
			{id: 1, value: 'USER_GROUP_INFO_TITLE1'},
			{id: 2, value: 'USER_GROUP_INFO_TITLE2'},
			{id: 3, value: 'USER_GROUP_INFO_TITLE3'},
			{id: 4, value: 'USER_GROUP_INFO_TITLE4'}
		];

		let StateRead = Translator.translate("STATE_READ");
		$scope.parseStates = function (states) {
			let result = '';
			angular.forEach(states.split(','), function (state) {
				if (result.length > 0) result += ', ';
				if (state.indexOf('.') == -1) {
					result += state.toString().trim() == "" ? StateRead : Translator.translate("STATE_" + state.toString().trim().toUpperCase());
				} else {
					let parts = state.split('.');
					result += Translator.translate("CONDITION_" + parts[1].toUpperCase()) + ' (' + Translator.translate("FEATURE_" + parts[0].toUpperCase()) + ')'	
				}
			});
			return result;
		};

		
		$scope.initOpen = $scope.userGroups.length == 1;
	}
})();