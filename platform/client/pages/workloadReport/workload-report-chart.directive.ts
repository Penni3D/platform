(function () {
	'use strict';

	angular
		.module('core.allocation')
		.directive('workloadReportChart', workloadReportChart);

	workloadReportChart.$inject = ['WorkloadReportService', 'Translator', 'ChartColourService', 'AllocationService'];

	function workloadReportChart(WorkloadReportService, Translator, ChartColourService, AllocationService) {
		return {
			scope: {
				selections: '=',
				refresh: '='
			},
			template: '<div><canvas id="workload-report-chart" class="chart chart-bar" chart-data="chartData.values" chart-labels="chartData.labels" ' +
				'chart-series="chartData.series" ' +
				'chart-options="chartOptions" chart-colors="chartColors" chart-dataset-override="chartDatasetOverride"></canvas></div>',
			replace: true,
			transclude: true,
			controller: function ($scope) {
				var colors = {};
				colors['actual_effort'] = ChartColourService.getColours(['blue']);
				colors['effort'] = ChartColourService.getColours(['orange']);

				$scope.refresh = function (){
					$scope.updateChart();
				};

				function splitRangeToDates(from, to, effort, prefix){
					var dateCount = moment(to).diff(from,  'days') + 1;
					var dates = [];
					var effortPerDay = 0;
					if (dateCount > 0) {
						effortPerDay = effort / dateCount;
						var startDate = moment(from);
						while (dateCount > 0) {
							dates.push(startDate.clone());
							dateCount--;
						}
					}

					if (prefix){
						var returnObject = {};
						returnObject[prefix + 'Dates'] = dates;
						returnObject[prefix + 'EffortPerDay'] = effortPerDay;
						return returnObject;
					} else {
						return {
							dates: dates,
							effortPerDay: effortPerDay
						};
					}
				}

				function keyGrapperRecursive(obj, key, parent, parentObjects){

					var keys = [];
					var current = {};
					var include = false;
					var subIds = [];
					_.each(obj,  function (item, subKey) {
						if (_.isNumber(+subKey) && !_.isNaN(+subKey)) {
							subIds.push({
								item: item,
								subKey: subKey
							});
						}
					});


					if (key){
						current.key = key;
					}

					if (_.isEmpty(subIds)) {
						if (obj.task_type === 1) {
							var lastSprint;
							var from, to, actualFrom, actualTo;
							if (lastSprint){
								from = lastSprint.from;
								to = lastSprint.to;
								actualFrom = lastSprint.actual_from;
								actualTo = lastSprint.actual_to;
							}
							if (!from && !to){
								from = obj.from;
								to = obj.to;
							}
							if (!actualFrom && !actualTo){
								actualFrom = obj.actual_from;
								actualTo = obj.actual_to;
							}

							if (obj.effort > 0 && from && to) {
								current.effort = obj.effort;
								current.from = from;
								current.to = to;
								var splitEffort = splitRangeToDates(from, to, obj.effort);
								if (splitEffort.dates.length > 0) {
									_.extend(current, splitEffort);
									include = true;
								}
							}

							if (obj.actual_effort > 0 && actualFrom && actualTo) {
								current.actual_effort = obj.actual_effort;
								current.actual_from = actualFrom;
								current.actual_to = actualTo;
								var splitActualEffort = splitRangeToDates(actualFrom, actualTo, obj.actual_effort, 'actual');
								if (splitActualEffort.actualDates.length > 0) {
									_.extend(current, splitActualEffort);
									include = true;
								}
							}
						}
					}
					if (_.has(current,  'key') && include){

						keys.push(current);
					}

					var objectRoute = _.compact(_.concat(parentObjects, obj));
					_.each(subIds,  function (sub) {
						var subItems = keyGrapperRecursive(sub.item,  sub.subKey, obj, objectRoute);
						keys = _.concat(keys,  subItems);
					});
					return keys;
				}

				function flattenAndReverseRelations(relations) {
					var flatRelations = {};
					_.each(relations,  function (rel, key) {
						flatRelations[key] = keyGrapperRecursive(rel, null, null, []);
					});
					return flatRelations;
				}

				function convertDateToWantedFormat(date, resolution) {
					if (resolution === 0) {
						return date.format('W/Y');
					} else if (resolution === 1) {
						return date.format('M/Y');
					}
				}

				function updateProcessData(data) {
					var dateValues = [];
					var fromDate = moment(data.options.from);
					var toDate = moment(data.options.to);
					var resolution = data.options.resolution;
					var distinctItemIds = [];
					var flatRelations = flattenAndReverseRelations(data.taskDurations.relations);
					var effortsByGroup = {};
					var actualEffortsByGroup = {};
					var groupedRows = _.groupBy(data.taskDurations.processRows, '_groupByColumn');

					_.each(groupedRows,  function (items, groupKey) {
						_.each(items, function (item) {
							var itemId = _.get(item, 'item_id');
							var rel = flatRelations[itemId];
							_.each(rel,  function (relation) {
								if (_.has(relation, 'dates')) {
									effortsByGroup[groupKey] = effortsByGroup[groupKey] || {};
									var dates = _.get(relation,  'dates');
									_.each(dates, function (d) {
										var dateTargetKey = convertDateToWantedFormat(d, data.options.resolution);
										effortsByGroup[groupKey][dateTargetKey] = effortsByGroup[groupKey][dateTargetKey] || 0;
										effortsByGroup[groupKey][dateTargetKey] += +relation.effortPerDay;
									});
								}

								if (_.has(relation, 'actualDates')) {
									actualEffortsByGroup[groupKey] = actualEffortsByGroup[groupKey] || {};
									var actualDates = _.get(relation,  'actualDates');
									_.each(actualDates, function (d) {
										var dateTargetKey = convertDateToWantedFormat(d, data.options.resolution);
										actualEffortsByGroup[groupKey][dateTargetKey] = actualEffortsByGroup[groupKey][dateTargetKey] || 0;
										actualEffortsByGroup[groupKey][dateTargetKey] += +relation.actualEffortPerDay;
									});
								}
							});
						});
					});

					while (fromDate.isBefore(toDate)){
						var shortening = resolution === 0 ?
							fromDate.isoWeek() + "/" + fromDate.year()
							: (fromDate.month() + 1) + "/" + fromDate.year();
						dateValues.push(shortening);
						if (resolution === 0){
							fromDate.add(7, 'days');
						} else {
							fromDate.add(1, 'month');
						}
					}

					var daatta = [];
					var stackNamesForIndexes = {};
					var colorsForIndex = {};
					var effortTranslation = Translator.translate('WORKLOAD_REPORT_EFFORT');
					var actualEffortTranslation = Translator.translate('WORKLOAD_REPORT_ACTUAL_EFFORT');
					_.each(effortsByGroup, function (item, key) {
						var arr = [];
						var serieName = _.find(data.taskDurations.listItems, {item_id: +key});
						daatta.push(arr);
						stackNamesForIndexes[daatta.length - 1] = effortTranslation;
						colorsForIndex[daatta.length -1] = 'effort';
						distinctItemIds.push(serieName && effortTranslation + ' - ' + serieName.list_item || effortTranslation);
						_.each(dateValues, function (dateVal) {
							if (_.has(item,  dateVal)){
								var rounded = Math.round(item[dateVal]*100) / 100;
								arr.push(rounded);
							} else {
								arr.push(0);
							}
						});
					});

					_.each(actualEffortsByGroup, function (item, key) {
						var arr = [];
						var serieName = _.find(data.taskDurations.listItems, {item_id: +key});
						daatta.push(arr);
						stackNamesForIndexes[daatta.length - 1] = 'actual_effort';
						colorsForIndex[daatta.length -1] = 'actual_effort';
						distinctItemIds.push(serieName && actualEffortTranslation + ' - ' + serieName.list_item || actualEffortTranslation);
						_.each(dateValues, function (dateVal) {
							if (_.has(item,  dateVal)){
								var rounded = Math.round(item[dateVal]*100) / 100;
								arr.push(rounded);
							} else {
								arr.push(0);
							}
						});
					});

					$scope.chartData ={
						values: daatta,
						series: distinctItemIds,
						labels: dateValues
					};

					$scope.chartOptions= {
						legend: { display: true },
						title: { display: true },
						scales: {
							yAxes: [{
								stacked: true,
								ticks: {
									beginAtZero: true
								}
							}],
							xAxes: [{
								labels:true,
								stacked: true
							}]
						}
					};
					var i = 0;
					var chartColors = [];
					$scope.chartDatasetOverride = _.map(daatta,  function (data, index) {
						chartColors.push(colors[colorsForIndex[index]][i++]);
						return {
							// todo: colors no good, do something
							borderColor: colors[colorsForIndex[index]][i],
							backgroundColor: colors[colorsForIndex[index]][i],
							stack: stackNamesForIndexes[index]
						};
					});
					$scope.chartColors = chartColors;
				}

				function updateApprovalData(data) {
					var showLegend = true;
					var statuses = {};
					var items = {};
					var dataSets = {};
					_.each(data.values,  function (item) {
						if (_.isNumber(item.status)) {
							var date = item.year + '/' + ('0' + item.subValue).slice(-2);
							items[date] = items[date] || {};
							var itemsForStatus = items[date];
							itemsForStatus[item.status] = item;
							statuses[item.status] = true;
						}
					});

					var sortedDates = _.sortBy(data.dates);
					var foundStatuses = _.sortBy(_.keys(statuses));
					_.each(sortedDates,  function (d) {
						_.each(foundStatuses, function (status) {
							dataSets[status] = dataSets[status] || [];
							var stuffForDateAndStatus = items[d] && items[d][status];

							if (stuffForDateAndStatus) {
								dataSets[status].push(stuffForDateAndStatus.hours);
							} else {
								dataSets[status].push(0);
							}
						});
					});

					var values = [];
					var series = [];
					var colors = [];

					_.each(dataSets,  function (val, key) {
						var translationKey = data.translations[key];
						colors.push(AllocationService.getCellColourHex('allocation', key));
						if (!_.isEmpty(translationKey)){
							series.push(translationKey);
							values.push(val);
						}
					});

					if (_.isEmpty(values)){
						values = [0];
						showLegend = false;
					}

					$scope.chartData ={
						values: values,
						series: series,
						labels: sortedDates
					};
					$scope.chartColors = colors;
					$scope.chartOptions= {
						legend: { display: showLegend },
						title: { display: true },
						scales: {
							yAxes: [{
								stacked: true,
								ticks: {
									beginAtZero: true
								}
							}],
							xAxes: [{
								stacked: true
							}]
						}
					};
					$scope.chartDatasetOverride = _.map(colors,  function (color) {
						return {
							backgroundColor: color
						}
					});
				}

				$scope.updateChart = function () {
					WorkloadReportService.getData($scope.selections)
						.then(function (data) {
							if (data.processData) {
								updateProcessData(data);
							} else {
								updateApprovalData(data);
							}

						});
				};
			}
		};
	}
})();