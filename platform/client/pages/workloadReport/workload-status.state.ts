(function () {
	'use strict';


	angular
		.module('core.pages')
		.config(WorkLoadApprovalConfig)
		.service('WorkLoadStatusService', WorkLoadStatusService)
		.controller('WorkLoadStatusController', WorkLoadStatusController);

	WorkLoadApprovalConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function WorkLoadApprovalConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('WorkLoadStatus', {
			controller: 'WorkLoadStatusController',
			template: 'pages/workloadReport/workload-status.html',
			resolve: {
				WorkLoadRights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('WorkLoadStatus').then(function (states) {
						return {
							read: _.includes(states, 'read'),
						};
					});
				},
				ViewMode: function () {
					return "statuspage";
				},
				PortfolioColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioId) {
						return PortfolioService.getPortfolioColumns('user', StateParameters.FilteringPortfolioId);
					} else {
						return [];
					}
				},
				Sublists: function (ProcessService, StateParameters) {
					return ProcessService.getSublists('user', StateParameters.FilteringPortfolioId);
				},
				SavedSettings: function (AllocationService) {
					return AllocationService.getSavedSettings('priorisation').then(function (o) {
						return o;
					});
				},
				ActiveSettings: function (AllocationService) {
					return AllocationService.getActiveSettings('priorisation');
				},

				UserData: function (PortfolioService, StateParameters) {
					return PortfolioService
						.getPortfolioData('user', StateParameters.FilteringPortfolioId, { local: true }, { limit: 9999999 }).then(function (stuff) {
							return stuff.rowdata;
						});
				},
			}
		});
		ViewsProvider.addView('process.WorkLoadStatus', {
			controller: 'WorkLoadStatusController',
			template: 'pages/workloadReport/workload-status.html',
			resolve: {
				WorkLoadRights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('WorkLoadStatus').then(function (states) {
						return {
							read: _.includes(states, 'read'),
						};
					});
				},
				ViewMode: function () {
					return "widget";
				},
				PortfolioColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioId) {
						return PortfolioService.getPortfolioColumns('user', StateParameters.FilteringPortfolioId);
					} else {
						return [];
					}
				},
				Sublists: function (ProcessService, StateParameters) {
					return ProcessService.getSublists('user', StateParameters.FilteringPortfolioId);
				},
			}
		});
		ViewsProvider.addPage('WorkLoadStatus', 'PAGE_WORKLOAD_STATUS', ['read']);
		ViewsProvider.addFeature('process.WorkLoadStatus', 'MENU_WORKLOAD_STATUS', 'FEATURE_WORKLOAD_STATUS', ['read']);
		ViewConfiguratorProvider.addConfiguration('WorkLoadStatus', {
			global: true,
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('user');
				},
				ItemColumns: function (ProcessService) {
					return ProcessService.getColumns('worktime_tracking_hours');
				},
				PortfolioColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioId) {
						return PortfolioService.getPortfolioColumns('user', StateParameters.FilteringPortfolioId).then(function (cols) {
							let x = [];
							_.each(cols, function (col) {
								x.push(col.ItemColumn)
							});
							return x;
						});
					} else {
						return [];
					}
				}
			},
			tabs: {
				default: {
					FilteringPortfolioId: function (resolves) {
						let portfolios = _.filter(resolves.Portfolios, { ProcessPortfolioType: 0 });
						return {
							label: 'WT_STATUS_FILTERING_PORTFOLIO',
							type: 'select',
							options: portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					EnabledModes: function () {
						return {
							label: 'Enabled Modes',
							type: 'multiselect',
							options: [
								{ name: "Unallocated Hours (h)", value: 1 },
								{ name: "Unallocated Hours (%)", value: 11 },
								{ name: "Contracted vs Spent Hours", value: 2 },
								{ name: "Project vs Basic Hours", value: 3 },
								{ name: "Allocation for Competencies", value: 4 },
								{ name: "Spent vs Quota", value: 5 }
							],
							optionValue: 'value',
							optionName: 'name'
						};
					},

					optionalColumnId: function (resolves) {
						return {
							type: 'multiselect',
							label: 'ADDITIONAL_COLUMNS_WORKLOAD',
							options: resolves.PortfolioColumns,
							optionName: 'LanguageTranslation',
							optionValue: 'ItemColumnId'
						};
					}
				}
			}
		});
	}

	WorkLoadStatusController.$inject = [
		'$scope',
		'$rootScope',
		'Translator',
		'WorkLoadStatusService',
		'Common',
		'CalendarService',
		'ViewService',
		'StateParameters',
		'$compile',
		'Configuration',
		'MessageService',
		'UserService',
		'WorkLoadRights',
		'PortfolioColumns',
		'ViewMode',
		'PortfolioService',
		'Colors',
		'LocalStorageService',
		'ChartColourService',
		'FileSaver',
		'ApiService',
		'AttachmentService',
		'AllocationService',
		'SavedSettings',
		'SidenavService',
		'ActiveSettings',
		'UserData',
		'ProcessService',
		'$q'
	];

	function WorkLoadStatusController(
		$scope,
		$rootScope,
		Translator,
		WorkLoadStatusService,
		Common,
		CalendarService,
		ViewService,
		StateParameters,
		$compile,
		Configuration,
		MessageService,
		UserService,
		WorkLoadRights,
		PortfolioColumns,
		ViewMode,
		PortfolioService,
		Colors,
		LocalStorageService,
		ChartColourService,
		FileSaver,
		ApiService,
		AttachmentService,
		AllocationService,
		SavedSettings,
		SidenavService,
		ActiveSettings,
		UserData,
		ProcessService,
		$q

	) {

		$scope.navigationMenuId = StateParameters.NavigationMenuId;
		$scope.portfolioId = StateParameters.FilteringPortfolioId;
		$scope.portfolioColumns = PortfolioColumns;
		$scope.rights = WorkLoadRights;
		$scope.localeWeekSelector = 'week';
		$scope.users = [];
		$scope.data = [];
		$scope.weeks = [];

		let currentFilters = PortfolioService.getActiveSettings($scope.portfolioId).filters

		$scope.additionalColumnNames = [];

		_.each($scope.portfolioColumns, function (pc) {
			if (_.includes(StateParameters.optionalColumnId, pc.ItemColumn.ItemColumnId)) {
				let n = {};
				n['name'] = pc.ItemColumn.Name
				n['translation'] = pc.ItemColumn.LanguageTranslation[$rootScope.me.locale_id];
				n['datatype'] = pc.ItemColumn.DataType;
				n['data_additional'] = pc.ItemColumn.DataAdditional;
				$scope.additionalColumnNames.push(n)
			}
		})

		$scope.filterSave = function () {
			ViewService.view('configure.allocation', {
				params: {
					isNew: true,
					view: 'priorisation',
					public: true
				},
				locals: {
					CurrentSettings: {
						Name: Translator.translate('ALLOCATION_SAVED_NEW'),
						Value: { 'filters': currentFilters }
					},
					ReloadNavigation: reloadNavigation
				}
			});
		}

		function reloadNavigation() {
			AllocationService
				.getSavedSettings('priorisation')
				.then(function (newSettings) {
					setNavigation(newSettings);
				});
		}
		let activeTab = {
			$$active: undefined,
			activeTab: undefined
		};


		let setFiltersFinally = function () {
			ActiveSettings.filters = PortfolioService.getActiveSettings($scope.portfolioId);
			AllocationService.saveActiveSettings('priorisation')
		}

		let setActiveTab = function (s) {
			activeTab.$$active = false;
			activeTab = t;
			activeTab.$$active = true;

			if (s.Value.filters) {
				angular.forEach(s.Value.filters, function (value, key) {
					if (!ActiveSettings.hasOwnProperty('filters')) {
						ActiveSettings.filters = { 'filters': {} }
					}
					ActiveSettings.filters.filters[key] = angular.copy(value);
				});
			} else {
				PortfolioService.clearActiveSettings($scope.portfolioId);
			}

			setFiltersFinally();
			$scope.getRows({ filters: s.Value.filters });
			$scope.header.extTitle = " - " + Translator.translation(s.Name);
		}

		function setNavigation(settings) {
			let sidenav = [];
			let savedTabs = [];
			angular.forEach(settings, function (s) {
				s.Name = angular.fromJson(s.Name);
				let t = {
					hideSplit: true,
					name: Translator.translation(s.Name),
					onClick: function () {
						setActiveTab(s);
					},
					menu: [{
						icon: 'settings',
						name: 'CONFIGURE',
						onClick: function () {
							ViewService.view('configure.allocation', {
								params: {
									portfolioId: StateParameters.portfolioId,
									isNew: false,
									view: 'priorisation'
								},
								locals: {
									CurrentSettings: s,
									ReloadNavigation: reloadNavigation
								}
							});
						}
					}]
				};

				savedTabs.push(t);
			});

			if (savedTabs.length) {
				sidenav.push({ name: 'ALLOCATION_SAVED', tabs: savedTabs, showGroup: true });
			}

			SidenavService.navigation(sidenav);
		}

		setNavigation(SavedSettings)

		let downloadExcel = function () {
			_.each(tableValues.Rows, function (r) {
				_.each(r.Cells, function (cell) {
					if (typeof cell.Value != 'undefined' && cell.Value.includes("−")) {
						cell.Value = cell.Value.replace("−", "-");
					}
				})
			})
			AttachmentService.getExcel($scope.header.title + " - Excel", [], tableValues)
		}

		ViewService.addDownloadable('PORTFOLIO_EE', function () {
			$scope.header.extTitle = '';
			downloadExcel();
		}, StateParameters.ViewId);

		//Check first from stateparameters
		let f;
		let t;
		if (typeof StateParameters.fromDate === 'undefined' && typeof StateParameters.toDate === 'undefined') {
			//Then Check from service
			if (typeof WorkLoadStatusService.persistentSettings.fromDate === 'undefined' && typeof WorkLoadStatusService.persistentSettings.toDate === 'undefined') {
				//Not set anywhere
				f = moment().startOf('month').add(-1, 'M');
				t = moment().endOf('month').add(2, 'M');
			} else {
				f = moment(WorkLoadStatusService.persistentSettings.fromDate);
				t = moment(WorkLoadStatusService.persistentSettings.toDate);
			}
		} else {
			f = moment(StateParameters.fromDate);
			t = moment(StateParameters.toDate);
		}

		$scope.selections = {
			dateRange: {
				from: f,
				to: t
			},
			excelRange: {
				from: f,
				to: t
			},
			resolution: 1,
			filterValue: -1,
			selectValue: 0
		};

		$scope.isWidget = ViewMode == 'widget';
		WorkLoadStatusService.persistentSettings.fromDate = f;
		WorkLoadStatusService.persistentSettings.toDate = t;

		$scope.selectValue = 0;

		function cleanInvisibleSelections() {
			let userIdsToSpare = {};
			_.each($scope.users, function (user) {
				if ($scope.getVisibility(user.UserItemId)) {
					userIdsToSpare[user.UserItemId] = true;
				}
			});
			_.each(_.keys($scope.emailSelections), function (id) {
				if (!userIdsToSpare[id]) {
					delete $scope.emailSelections[id];
				}
			});
		}

		$scope.getRows = function (filters_?) {
			storeSettings();
			setWeeks();
			setTimeout(function () {
				$scope.users = [];
				loadData(filters_);
			}, 1);
		};

		$scope.filterRows = function () {
			storeSettings();
			cleanInvisibleSelections();
		};

		$scope.getVisibility = function () {
			return true;
		};

		$scope.jumpTo = function (model) {
			$scope.selections.dateRange.from = moment(model.d1);
			$scope.selections.dateRange.to = moment(model.d2);
			$scope.selections.excelRange.from = moment(model.d1);
			$scope.selections.excelRange.to = moment(model.d2);
			WorkLoadStatusService.persistentSettings.fromDate = moment(model.d1);
			WorkLoadStatusService.persistentSettings.toDate = moment(model.d2);
			$scope.getRows();
		};

		$scope.changeMode = function () {
			storeSettings();
			$scope.loading = true;
			setTimeout(function () {
				setWeeks();
				$scope.users = [];
				$scope.chosenMode = $scope.selections.selectedMode;
				setData($scope.data);
			}, 1);
		};

		$scope.showFilter = false;
		let filterList = {
			color: PortfolioService.hasActiveFilters($scope.portfolioId) ?
				Colors.getColor('accent').rgb : '',
			icon: 'filter_list',
			onClick: function () {
				$scope.showFilter = !$scope.showFilter;

				if ($scope.showFilter) {
					ViewService.scrollTop(StateParameters.ViewId);
				}
			}
		};

		let dialogModel = { d1: $scope.selections.dateRange.from.toDate(), d2: $scope.selections.dateRange.to.toDate() };
		$scope.header = {
			title: Translator.translate("WORKLOADSTATUS") + " " + CalendarService.formatDate($scope.selections.dateRange.from) + " - " + CalendarService.formatDate($scope.selections.dateRange.to),
			icons: [
				{
					icon: 'date_range', onClick: function () {
						let model = dialogModel;
						let scope = $rootScope.$new();

						let params = {
							scope: scope,
							title: 'WORKINGTIME_TRACKING_CHANGE_DATESPAN',
							inputs: [
								{
									type: 'date',
									label: 'WORKINGTIME_TRACKING_DATE_START',
									model: 'd1'
								},
								{
									type: 'date',
									label: 'WORKINGTIME_TRACKING_DATE_END',
									model: 'd2'
								}],
							buttons: [
								{
									type: 'secondary',
									text: 'MSG_CANCEL'
								},
								{
									type: 'primary',
									text: 'WORKINGTIME_TRACKING_SELECT',
									onClick: function () {
										$scope.jumpTo(model);
									}
								}]
						};
						MessageService.dialog(params, model, function () {
							if (model.d2 < model.d1) {
								model.d2 = model.d1;
							}
						});
					}
				},
				filterList
			]
		};


		//let $loading = $compile('<div id="worktime-backdrop"><loader class="contrast"></loader></div>')($scope);

		$scope.loading = false;

		function storeSettings() {
			LocalStorageService.store('KETO_WORKLOAD_STATUS_SHEET', $scope.selections);
		}

		function fetchSettings() {
			if (ViewMode == 'widget') {
				$scope.chosenMode = 4;
				$scope.selections.selectedMode = 4;
				return;
			}

			let selections = LocalStorageService.get('KETO_WORKLOAD_STATUS_SHEET');
			$scope.selections = _.extend({}, $scope.selections, selections);
			$scope.selections.dateRange.from = moment($scope.selections.dateRange.from);
			$scope.selections.dateRange.to = moment($scope.selections.dateRange.to);
			if (!$scope.selections.selectedMode) $scope.selections.selectedMode = 1;
			$scope.chosenMode = $scope.selections.selectedMode;
		}

		function setWeeks() {
			$scope.loading = true;
			$scope.header.title = "Resource Utilisation " + CalendarService.formatDate($scope.selections.dateRange.from) + " - " + CalendarService.formatDate($scope.selections.dateRange.to);
			$scope.weeks = [];

			let l = $scope.selections.dateRange.to.utc().endOf('month').diff($scope.selections.dateRange.from.utc().startOf('month'), 'month') + 1;
			let start = $scope.selections.dateRange.from.clone().utc().startOf('month');

			for (let w = 0; w < l; w++) {
				$scope.weeks.push({ m: start.month(), y: start.year(), d: start.format("MM"), o: start.clone().utc() });
				start.add(1, 'months');
			}
		}

		// init and refresh
		$scope.changeSelectedCompetencies = function (id) {
			$scope.setCompetencyData($scope.competencyResponse);
		};

		let getUtilisationIndex = 0;
		let getCompetenciesIndex = 0;

		$scope.competencySelections = [];
		function loadData(filtersParams?) {
			$scope.limitEnd = 50;
			$scope.limitStart = 0;
			let activeSelections = typeof filtersParams == 'undefined' ? PortfolioService.getActiveSettings($scope.portfolioId) : filtersParams;
			getUtilisationIndex++;
			WorkLoadStatusService.getUtilisation(
				getDayMonthYearFormatFromMoment($scope.selections.dateRange.from),
				getDayMonthYearFormatFromMoment($scope.selections.dateRange.to),
				$scope.portfolioId,
				activeSelections,
				getUtilisationIndex
			).then(function (data) {
				if (data[0].Month == getUtilisationIndex) {
					data.splice(0, 1);

					let competencyIds = [];
					_.each(data, function (row) {
						_.each(row.CompetencyIds, function (competencyId) {
							if (competencyIds.indexOf(competencyId) == -1) competencyIds.push(competencyId);
						});
					});
					loadCompetencyData(competencyIds);

					setData(data);
					cleanInvisibleSelections();
				}
			});
		}

		function loadCompetencyData(competencyIds) {
			let item_id = 0;
			if (ViewMode === 'widget') {
				item_id = StateParameters.itemId;
			}
			getCompetenciesIndex++;
			WorkLoadStatusService.getCompetencies(getDayMonthYearFormatFromMoment($scope.selections.dateRange.from),
				getDayMonthYearFormatFromMoment($scope.selections.dateRange.to), item_id, competencyIds, getCompetenciesIndex).then(function (response) {
					if (response[0].Month == getCompetenciesIndex) {
						response.splice(0, 1);

						_.each(response, function (row) {
							if (!$scope.competencySelections[row.ItemId] && (row.CompetencyHours > 0 || row.UserHours > 0)) {
								$scope.competencySelections[row.ItemId] = true;
							}
						});

						$scope.setCompetencyData(response);
					}
				});
		}

		$scope.availableColors = [];
		$scope.white = {
			backgroundColor: '#F5F5F5',
			pointBackgroundColor: '#F5F5F5',
			hoverBackgroundColor: '#F5F5F5',
			borderColor: '#F5F5F5',
			fill: true
		};

		_.each(ChartColourService.getColours(), function (c) {
			$scope.availableColors.push({
				backgroundColor: c,
				pointBackgroundColor: c,
				hoverBackgroundColor: c,
				fill: true
			});
		});

		$scope.setCompetencyData = function (response) {
			$scope.competencyData = [];
			$scope.competencyCapa = [];
			$scope.competencyLabels = [];
			$scope.competencyOverride = [];
			$scope.competencyOptions = {
				maintainAspectratio: true,
				responsive: true,
				scales: {
					yAxes: [
						{
							id: 'y-axis-1',
							display: true,
							position: 'left',
							gridLines: { display: true, drawBorder: true },
							stacked: true,
							ticks: {
								fontFamily: 'Roboto', fontSize: 12
							}
						}
					],
					xAxes: [
						{
							id: 'x-axis-1',
							display: true,
							position: 'left',
							gridLines: { display: false, drawBorder: true },
							stacked: true,
							ticks: {
								fontFamily: 'Roboto', fontSize: 12
							}
						}
					]
				}
			};
			$scope.competencySeries = [];
			$scope.competencyColoring = [];
			$scope.competencyResponse = [];
			$scope.competencyResponseLabels = [];
			$scope.competencyResponse = response;

			let labels = _.uniqBy(response, "ItemId");
			$scope.competencyResponseLabels = labels;

			_.each(labels, function (label) {
				if ($scope.competencySelections[label.ItemId]) {
					$scope.competencyData.push([]);
					$scope.competencyData.push([]);
					$scope.competencyData.push([]);

					$scope.competencySeries.push(label.Name);
					$scope.competencySeries.push('Remaining ');
					$scope.competencySeries.push('Overflow ');
					$scope.competencyOverride.push({ stack: label.ItemId });
					$scope.competencyOverride.push({ stack: label.ItemId });
					$scope.competencyOverride.push({ stack: label.ItemId });
				}
			});

			_.each($scope.weeks, function (month) {
				$scope.competencyLabels.push(month.d + '/' + month.y);

				let i = 0;
				_.each(labels, function (label, index) {
					let hoursColor = $scope.availableColors[index];
					let capacColor = angular.copy($scope.white);
					capacColor.borderColor = hoursColor.backgroundColor;

					let overfColor = angular.copy(hoursColor);
					overfColor.borderColor = '#000000';

					$scope.competencyColoring.push(overfColor);
					$scope.competencyColoring.push(capacColor);
					$scope.competencyColoring.push(hoursColor);


					if ($scope.competencySelections[label.ItemId]) {
						let r = _.find(response, function (o) {
							return o.Month === (month.m + 1) && o.Year === month.y && o.ItemId === label.ItemId;
						});
						let hours = 0;
						let capacity = 0;
						if (r) {
							hours = r.CompetencyHours + r.UserHours;
							capacity = r.Capacity;
						}

						let c = 0;
						if (capacity >= hours) {
							$scope.competencyData[i].push(hours);
							c = capacity - hours;
							if (c < 0) c = 0;
							$scope.competencyData[i + 1].push(c);
							$scope.competencyData[i + 2].push(0);
						} else {
							$scope.competencyData[i].push(capacity);
							c = hours - capacity;
							if (c < 0) c = 0;
							$scope.competencyData[i + 1].push(0);
							$scope.competencyData[i + 2].push(c);
						}

						i += 3;
					}
				});
			});
		};

		$scope.getCompColor = function (index) {
			return $scope.availableColors[index].backgroundColor;
		};
		$scope.getCompValue = function (type, id, year, month) {
			let r = _.find($scope.competencyResponse, function (o) {
				return month === o.Month - 1 && year === o.Year && o.ItemId === id;
			});
			let hours = 0;
			let capacity = 0;
			if (r) {
				capacity = r.Capacity;
				hours = r.CompetencyHours + r.UserHours;
			}
			if (type == 1 || type == 11) return hours;
			return capacity;
		};
		$scope.getCompTitle = function (id, year, month) {
			let r = _.find($scope.competencyResponse, function (o) {
				return month === o.Month - 1 && year === o.Year && o.ItemId === id;
			});
			let hours = 0;
			let capacity = 0;
			if (r) {
				capacity = r.Capacity;
				hours = r.CompetencyHours + r.UserHours;
			}
			return Common.formatNumber(hours) + ' / ' + Common.formatNumber(capacity);
		};

		$scope.limitStart = 0;
		$scope.limitEnd = 50;
		$scope.loadMore = function () {
			$scope.limitStart = $scope.limitEnd;
			$scope.limitEnd += 50;
			setData($scope.data);
		};
		$scope.canLoadMore = true;

		function setDataForOptColumns() {
			_.each($scope.users, function (user) {
				let matchingDataRow = _.find(UserData, function (o) { return o.item_id == user.UserItemId; });
				let promises = []
				if (typeof matchingDataRow != 'undefined') {
					_.each($scope.additionalColumnNames, function (ad, index) {
						user['$$property' + (index + 1)] = matchingDataRow[$scope.additionalColumnNames[index].name];
						if (($scope.additionalColumnNames[index].datatype == 5 || $scope.additionalColumnNames[index].datatype == 6) && (matchingDataRow[$scope.additionalColumnNames[index].name] != null && matchingDataRow[$scope.additionalColumnNames[index].name].length && matchingDataRow[$scope.additionalColumnNames[index].name][0].length != 0)) {
							promises.push(ProcessService.getItem($scope.additionalColumnNames[index].data_additional, matchingDataRow[$scope.additionalColumnNames[index].name][0]).then(function (h) {
								user['$$translatedProperty' + (index + 1)] = h.primary;
							}))
						} else {
							user['$$translatedProperty' + (index + 1)] = matchingDataRow[$scope.additionalColumnNames[index].name].length ? matchingDataRow[$scope.additionalColumnNames[index].name] : "";
						}
					})
				}
				$q.all(promises).then(function () {
					formExcel();
				})
			})
		}

		function setData(data) {
			let users = _.orderBy(_.uniqBy(data, "UserItemId"), 'Name');
			$scope.data = data;

			if (ViewMode == 'widget') {
				let ut = 0;
				_.each(users, function (item) {
					if ($scope.getVisibility(item.UserItemId)) ut += 1;
				});

				let p = ViewService.getView(StateParameters.ViewId);
				if (typeof p.parent !== 'undefined' && typeof p.parent.scope.setIcon === 'function')
					p.parent.scope.setIcon(StateParameters.ViewId, ut);
				$scope.users = users;
				if ($scope.additionalColumnNames.length) {
					setDataForOptColumns();
				} else {
					formExcel();
				}

				$scope.canLoadMore = false;
			} else {
				if ($scope.limitEnd > users.length) $scope.limitEnd = users.length;
				for (let i = $scope.limitStart; i < $scope.limitEnd; i++) {
					$scope.users.push(users[i]);
				}
				if ($scope.additionalColumnNames.length) {
					setDataForOptColumns();
				} else {
					formExcel();
				}
				$scope.canLoadMore = !($scope.users.length == users.length);
			}

			$scope.loading = false;

		}

		$scope.modeOptions = [];

		for (let m of StateParameters.EnabledModes) {
			if (m == 1) $scope.modeOptions.push({ name: Translator.translate("WORK_LOAD_STATUS_MODE_1"), value: 1 });
			if (m == 11) $scope.modeOptions.push({ name: Translator.translate("WORK_LOAD_STATUS_MODE_11"), value: 11 });
			if (m == 2) $scope.modeOptions.push({ name: Translator.translate("WORK_LOAD_STATUS_MODE_2"), value: 2 });
			if (m == 3) $scope.modeOptions.push({ name: Translator.translate("WORK_LOAD_STATUS_MODE_3"), value: 3 });
			if (m == 4) $scope.modeOptions.push({ name: Translator.translate("WORK_LOAD_STATUS_MODE_4"), value: 4 });
			if (m == 5) $scope.modeOptions.push({ name: Translator.translate("WORK_LOAD_STATUS_MODE_5"), value: 5 });
		}

		$scope.getItem = function (r, v) {
			if ($scope.chosenMode === 2) {
				if (v === 1) {
					if (r["ActualTaskHours"] < r["PlannedHours"]) {
						if (r["PlannedHours"] == 0) return 0;
						return r["ActualTaskHours"] / r["PlannedHours"] * 100;
					}
					if (r["ActualTaskHours"] == 0) return 0;
					return (1 - r["PlannedHours"] / r["ActualTaskHours"] + 1) * 100;
				}
				if (v === 2) {
					return 100;
				}
			} else if ($scope.chosenMode === 3) {
				if (v === 1) return r["ActualTaskHours"];
				if (v === 2) return r["ActualTaskHours"] + r["ActualOtherHours"];
			} else if ($scope.chosenMode === 5) {
				if (v === 2) return r["Capacity"];
				if (v === 1) return r["ActualTaskHours"] + r["ActualOtherHours"];
			}
			if (v === 1) return r["PlannedHours"];
			if (v === 2) return r["Capacity"];
		};

		$scope.getTitle = function (d, user_id) {
			let r = _.find($scope.data, function (o) {
				return o.Month === (d.m + 1) && o.Year === d.y && o.UserItemId === user_id;
			});

			if (r) {
				let v1 = $scope.getItem(r, 1);
				let v2 = $scope.getItem(r, 2);
				if (v1 == 0 && v2 == 0) return "";
				if ($scope.chosenMode === 2) {
					return Common.formatNumber(r["ActualTaskHours"], 1) + ' / ' + Common.formatNumber(r["PlannedHours"], 1);
				} else if ($scope.chosenMode === 3) {
					return Common.formatNumber(v1 / (v2) * 100, 1) + "%";
				} else if ($scope.chosenMode === 5) {
					return Common.formatNumber(r["ActualTaskHours"] + r["ActualOtherHours"], 1) + ' / ' + Common.formatNumber(r["Capacity"], 1);
				} else if ($scope.chosenMode === 11) {
					return Common.formatNumber(v1 / (v2) * 100, 0) + "%";
				}
				return Common.formatNumber(v2 - v1, 1);
			}
			return "";
		};

		let tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };

		let addCategoryColumns = function (tableValues_, firstCategory, secondCategory) {
			tableValues_.Rows[1].Cells.push({ 'Value': Translator.translate(firstCategory) })
			if (secondCategory != "") {
				tableValues_.Rows[1].Cells.push({ 'Value': Translator.translate(secondCategory) })
			}
		}

		let formExcel = function () {
			let customMode = $scope.selections.selectedMode == 5 || $scope.selections.selectedMode == 3 || $scope.selections.selectedMode == 2
			let incrementer = !customMode ? 1 : 2;

			tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };
			tableValues.Rows.push({ 'Cells': [] })
			tableValues.Rows[0].Cells.push({ 'Value': Translator.translate('FLEX_TIME_TRACKING_USER') })

			if ($scope.additionalColumnNames.length == 1) {
				tableValues.Rows[0].Cells.push({ 'Value': $scope.additionalColumnNames[0].translation })
			} else if ($scope.additionalColumnNames.length == 2) {
				tableValues.Rows[0].Cells.push({ 'Value': $scope.additionalColumnNames[0].translation })
				tableValues.Rows[0].Cells.push({ 'Value': $scope.additionalColumnNames[1].translation })
			}

			if ($scope.selections.selectedMode == 5) {
				tableValues.Rows.push({ 'Cells': [] })
				tableValues.Rows[1].Cells.push({ 'Value': "" })
				if ($scope.additionalColumnNames.length) {
					_.each($scope.additionalColumnNames, function () {
						tableValues.Rows[1].Cells.push({ 'Value': "" })
					})
				}
				_.each($scope.weeks, function () {
					addCategoryColumns(tableValues, 'PRIORISATION_SPENT', 'PRIORISATION_QUOTA')
				})
			} else if ($scope.selections.selectedMode == 3) {
				tableValues.Rows.push({ 'Cells': [] })
				tableValues.Rows[1].Cells.push({ 'Value': "" })
				if ($scope.additionalColumnNames.length) {
					_.each($scope.additionalColumnNames, function () {
						tableValues.Rows[1].Cells.push({ 'Value': "" })
					})
				}
				_.each($scope.weeks, function () {
					addCategoryColumns(tableValues, 'PRIORISATION_PROJECT', 'PRIORISATION_BH')
				})
			} else if ($scope.selections.selectedMode == 2) {
				tableValues.Rows.push({ 'Cells': [] })
				tableValues.Rows[1].Cells.push({ 'Value': "" })
				if ($scope.additionalColumnNames.length) {
					_.each($scope.additionalColumnNames, function (o) {
						tableValues.Rows[1].Cells.push({ 'Value': "" })
					})
				}
				_.each($scope.weeks, function () {
					addCategoryColumns(tableValues, 'PRIORISATION_CONTRACTED', 'PRIORISATION_SH')
				})
			}
			_.each($scope.users, function (user, index) {
				tableValues.Rows.push({ 'Cells': [] })
				tableValues.Rows[index + incrementer].Cells.push({ 'Value': user.Name })

				if ($scope.additionalColumnNames.length == 1) {
					tableValues.Rows[index + incrementer].Cells.push({ 'Value': user['$$translatedProperty1'] })
				} else if ($scope.additionalColumnNames.length == 2) {
					tableValues.Rows[index + incrementer].Cells.push({ 'Value': user['$$translatedProperty1'] })
					tableValues.Rows[index + incrementer].Cells.push({ 'Value': user['$$translatedProperty2'] })
				}

				_.each($scope.weeks, function (week) {
					let setValue = $scope.getTitle(week, user.UserItemId)
					if ($scope.selections.selectedMode == 2 || $scope.selections.selectedMode == 5) {
						_.each(setValue.split("/"), function (valueToExcel) {
							if (valueToExcel.includes(",")) {
								valueToExcel = valueToExcel.replace(",", ".");
							}
							tableValues.Rows[index + incrementer].Cells.push({ 'Value': valueToExcel, NValue: valueToExcel, DataType: 2 });
						})
					} else {
						if (setValue.includes(",")) {
							setValue = setValue.replace(",", ".");
						}
						tableValues.Rows[index + incrementer].Cells.push({ 'Value': setValue, NValue: setValue, DataType: 2 })
					}
				})
			})
			_.each($scope.weeks, function (week) {
				tableValues.Rows[0].Cells.push({ 'Value': week.y + "/" + week.d })
				if (customMode) tableValues.Rows[0].Cells.push({ 'Value': "" })
			})
		}

		$scope.getValue = function (v, d, user_id) {
			let r = _.find($scope.data, function (o) {
				return o.Month === (d.m + 1) && o.Year === d.y && o.UserItemId === user_id;
			});

			if (r) return $scope.getItem(r, v);
		};

		if (ViewMode != 'widget') {
			ViewService.footbar(StateParameters.ViewId, { template: 'pages/workloadReport/workload-status-footbar.html' });
		}
		fetchSettings();
		setWeeks();
		loadData();
	}

	function getDayMonthYearFormatFromMoment(momentObject) {
		return momentObject.format('DDMMYYYY');
	}

	WorkLoadStatusService.$inject = ['ApiService'];

	function WorkLoadStatusService(ApiService) {
		let persistentSettings = {};

		let Workload = ApiService.v1api('resources/WorkloadReport');
		let getCompetencies = function (from, to, item_id, competencyIds, queryIndex) {
			return Workload.new(['competencies', from, to, item_id, queryIndex], competencyIds);
		};
		let getUtilisation = function (from, to, portfolioId, filterSelections, queryIndex) {

			if (portfolioId && filterSelections && filterSelections.filters) {
				let filterString = _.chain(filterSelections.filters.select)
					.map(function (filter, key) {
						if (!_.isEmpty(filter)) {
							return key + '@' + _.join(filter, '-');
						}
					})
					.filter(function (val) {
						return !_.isEmpty(val);
					})
					.join(',')
					.value();
				if (_.isEmpty(filterString)) filterString = '%20';
				let filterText = filterSelections.filters.text;
				if (_.isEmpty(filterText)) {
					filterText = '%20';
				}
				return Workload.get([from, to, portfolioId, filterText, filterString, queryIndex]);
			}
			return Workload.get([from, to, queryIndex]);
		};

		return {
			getUtilisation: getUtilisation,
			persistentSettings: persistentSettings,
			getCompetencies: getCompetencies
		}
	}
})();