(function(){
	'use strict';
	angular
		.module('core.pages')
		.config(WorkloadReportConfig)
		.controller('WorkloadReportController', WorkloadReportController)
		.service('WorkloadReportService', WorkloadReportService);

	WorkloadReportConfig.$inject = ['ViewsProvider'];
    function WorkloadReportConfig(ViewsProvider) {

	    ViewsProvider.addView('workloadReport', {
		    controller: 'WorkloadReportController',
		    template: 'pages/workloadReport/workload-report.html',
		    resolve: {
		    	Options: function (WorkloadReportService) {
				    return WorkloadReportService.getOptions();
			    }
		    }
	    });

		ViewsProvider.addPage('workloadReport', 'PAGE_WORKLOAD_REPORT', ['read', 'write']);
	}
	
	WorkloadReportController.$inject = ['$scope', 'WorkloadReportService', 'Options', 'ProcessService', 'Translator'];
    function WorkloadReportController($scope, WorkloadReportService, Options, ProcessService, Translator) {
	    $scope.header = {
		    title: 'Workload Report'
	    };

	    $scope.$watch('selections', function (newVal) {
	    	WorkloadReportService.storeSelections(newVal);
		    $scope.formErrors = [];
	    }, true);
	    
	    $scope.processSelected = function () {
		    var selectedProcess = _.get($scope, 'selections.process');
	    	if (selectedProcess){
	    		ProcessService.getColumns(selectedProcess)
				    .then(function (response) {
					    $scope.itemColumns = _.filter(response, function (item) {
						    return item.DataType === 6 && item.DataAdditional;
					    });
				    });
		    }
	    };
    
	    	    
	    function isFormOk() {
		    var valid = true;
		    var messages = [];
		    if ($scope.selections.mode === $scope.options.modes[1].Value) {
		    	if (!$scope.selections.process) {
				    valid = false;
				    messages.push(Translator.translate('WORKLOAD_REPORT_SELECT_PROCESS'));
			    }
		    }
		    
		    return {
		    	messages: messages,
		    	valid: valid
		    };
	    }
	    
	    $scope.updateChart = function () {
		    var formValidation = isFormOk();
		    $scope.formErrors = formValidation.messages;
	    	if (formValidation.valid) {
			    $scope.refreshChart();
		    }
	    };
	    
	    
	    function initController(){
		    $scope.options =_.extend({}, Options, {
			    resolutions: [
				    {Value: 0, Variable: 'WEEK'},
				    {Value: 1, Variable: 'MONTH'}
			    ],
			    modes: [
				    {
					    Value: 'ResourceApproval',
						Variable: 'WORKLOAD_REPORT_RESOURCE_APPROVAL'
				    },
				    {
					    Value: 2,
						Variable: 'WORKLOAD_REPORT_PROCESS_DATA'
				    }]
		    });

		    var previousSelections = WorkloadReportService.fetchSelections();
		    $scope.selections = _.extend(
			    {
				    from: moment().add(-1, 'months').toDate(),
				    to: moment().add(2, 'months').toDate(),
				    resolution: 1,
				    mode: 2,
				    process: null
			    },
			    previousSelections
		    );
		    $scope.selections.processColumn = null;
		    $scope.processSelected();
	    }
	    
	    initController();
    }
    
    WorkloadReportService.$inject = ['ApiService', 'LocalStorageService'];
    function WorkloadReportService(ApiService, LocalStorageService){
	    
    	var WorkLoad = ApiService.v1api('resources/WorkloadReport');
    	this.getOptions = function () {
		    return WorkLoad.get('options');
	    };
    	
	    this.getData = function (options) {
		    return WorkLoad.new('', options)
	    };

	    function getStorageKey() {
		    return "KETO_WORKLOAD_REPORT_SELECTIONS";
	    }
	    this.storeSelections = function(selections) {
			LocalStorageService.store(getStorageKey(), selections);
	    };

	    this.fetchSelections = function() {
	    	return LocalStorageService.get(getStorageKey());
	    };
	    
    }
})();