﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(RecycleBinConfig)
		.controller('RecycleBinController', RecycleBinController)
		.controller('RecycleBinDialogController', RecycleBinDialogController)

	RecycleBinConfig.$inject = ['ViewsProvider'];

	function RecycleBinConfig(ViewsProvider) {
		ViewsProvider.addView('RecycleBin', {
			controller: 'RecycleBinController',
			template: 'pages/recycleBin/recycleBin.html',
			resolve: {
				Recycled: function (RecycleBinService) {
					return RecycleBinService.getData();
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				}
			}
		});

		ViewsProvider.addPage('RecycleBin', "PAGE_RECYCLE_BIN", ['read', 'write']);
	}

	RecycleBinController.$inject = ['$scope', 'RecycleBinService', 'StateParameters', 'MessageService', 'Recycled', 'CalendarService', 'ViewService', 'Processes', '$q', 'UserService'];

	function RecycleBinController($scope, RecycleBinService, StateParameters, MessageService, Recycled, CalendarService, ViewService, Processes, $q, UserService) {
		$scope.processes = Processes;
		$scope.loading = 0;
		$scope.selectedProcess = "";

		let getData = function (Recycled) {
			$scope.recycled = [];
			let promises = [];
			for (let u of Recycled) {
				promises.push(UserService.getUser(u.DeletedBy).then(function (result) {
					u.user = result.primary;
				}));
			}
			$q.all(promises).then(function () {
				$scope.recycled = Recycled;
			});
		}
		getData(Recycled);

		$scope.processChanged = function () {
			RecycleBinService.getData($scope.selectedProcess).then(function (result) {
				getData(result);
			});
		}
		$scope.fd = function (d) {
			return CalendarService.formatDateTime(d, false, true, true);
		}

		$scope.restoreItem = function (item_id, archive_id) {
			$scope.loading = item_id;
			RecycleBinService.restoreItem(item_id, archive_id).then(function (result) {
				$scope.loading = 0;
				let params = {
					template: 'pages/recycleBin/recycleBin.dialog.html',
					controller: 'RecycleBinDialogController',
					locals: {
						Callback: function () {
							ViewService.refresh(StateParameters.ViewId);
						},
						RestoreItem: {ItemId: item_id, ArchiveId: archive_id},
						RestoreItems: result,
						StateParameters: StateParameters
					}
				};

				MessageService.dialogAdvanced(params);

			});
		}

		$scope.header = {
			title: "PAGE_RECYCLE_BIN"
		};
		ViewService.footbar(StateParameters.ViewId, {template: 'pages/recycleBin/recycleBinFootbar.html'});
	}

	RecycleBinDialogController.$inject = ['$scope', 'StateParameters', 'RestoreItem', 'RestoreItems', 'MessageService', 'CalendarService', 'UserService', '$q', 'RecycleBinService', 'Callback'];

	function RecycleBinDialogController($scope, StateParameters, RestoreItem, RestoreItems, MessageService, CalendarService, UserService, $q, RecycleBinService, Callback) {
		$scope.items = [];
		let promises = [];
		for (let u of RestoreItems) {
			promises.push(UserService.getUser(u.LastModifiedBy).then(function (result) {
				u.user = result;
			}));
		}
		$q.all(promises).then(function () {
			$scope.items = RestoreItems;
			checkItems($scope.items);
		});

		$scope.cancel = function () {
			MessageService.closeDialog();
		}

		$scope.fd = function (d) {
			return CalendarService.formatDateTime(d, false, true, true);
		}

		$scope.hasErrors = false;

		function checkItems(items) {
			$scope.hasErrors = false;
			for (let i of items) {
				if (i.Error) {
					$scope.hasErrors = true;
					break;
				}
			}
		}

		$scope.simulating = false;
		$scope.simulated = false
		$scope.simulate = function () {
			$scope.simulating = true;
			RecycleBinService.simulate(RestoreItem).then(function (result) {
				$scope.simulating = false;
				$scope.simulated = true;
				$scope.items = result;
				checkItems($scope.items);
			});
		}

		$scope.restoring = false;
		$scope.restore = function () {
			$scope.restoring = true;
			RecycleBinService.restore(RestoreItem).then(function (result) {
				Callback();
				MessageService.closeDialog();
			});
		}

		$scope.lastChange = function (r) {
			if (!r) return '';
			let msg = r.Error ? ' - ' + r.ErrorMessage : '';
			return (r.user ? r.user.primary : '') + " - " + $scope.fd(r.LastModifiedOn) + msg;
		}
	}

})();