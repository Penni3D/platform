﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.service('RecycleBinService', RecycleBinService);

	RecycleBinService.$inject = ['$q', 'ApiService'];

	function RecycleBinService($q, ApiService) {
		let self = this;
		let recycleBin = ApiService.v1api('settings/RecycleBin');

		self.getData = function (process) {
			return recycleBin.get(process);
		}

		self.restoreItem = function (item_id, archive_id) {
			return recycleBin.new(['restore'], {ItemId: item_id, ArchiveId: archive_id});
		}
		self.simulate = function (item) {
			return recycleBin.new(['simulate'], item);
		}
		self.restore = function (item) {
			return recycleBin.new(['commit'], item);
		}
	}
})();