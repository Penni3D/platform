﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(InstanceCompareConfig)
		.controller('InstanceCompareController', InstanceCompareController);

	InstanceCompareConfig.$inject = ['ViewsProvider'];

	function InstanceCompareConfig(ViewsProvider) {
		ViewsProvider.addView('InstanceCompare', {
			controller: 'InstanceCompareController',
			template: 'pages/instanceCompare/instanceCompare.html',
			resolve: {
				SourceDatabases: function (InstanceCompareService) {
					return InstanceCompareService.getDb();
				},
				SourceInstances: function (InstanceCompareService) {
					return InstanceCompareService.getSourceInstances();
				},
				Databases: function (InstanceCompareService) {
					return InstanceCompareService.getDbList();
				}
			}
		});

		ViewsProvider.addPage('InstanceCompare', "PAGE_INSTANCE_COMPARE", ['read', 'write']);
	}

	InstanceCompareController.$inject = ['$scope', '$rootScope', 'InstanceCompareService', 'SourceInstances', '$filter', 'Databases', 'Common', 'Translator', 'MessageService', 'SourceDatabases', 'ProcessService', 'Configuration', 'ViewService', 'StateParameters'];

	function InstanceCompareController($scope, $rootScope, InstanceCompareService, SourceInstances, $filter, Databases, Common, Translator, MessageService, SourceDatabases, ProcessService, Configuration, ViewService, StateParameters) {
		$rootScope.me.locale_id = '';

		$scope.sourceDbs = SourceDatabases;
		$scope.sourceDb = SourceDatabases[0]['database'];
		$scope.sourceInstances = SourceInstances;
		$scope.databases = Databases;
		$scope.targetInstancesLoaded = false;
		$scope.processesLoaded = false;

		$scope.edit = true;
		$scope.ready = false;
		$scope.working = '';
		$scope.status = '';
		$scope.error = '';
		$scope.header.title = Translator.translation('PAGE_INSTANCE_COMPARE');
		$scope.skipProcessPortfoliosDownloadTemplates = false;
		$scope.skipHelps = false;
		$scope.skipConfigs = false;
		$scope.skipRequests = false;
		$scope.skipCompare = true;
		$scope.progress = {};
		$scope.state = 0;

		$scope.changeSourceInstance = function () {
			$scope.processesLoaded = false;
			delete $scope.$$childTail.selectedProcesses;
			delete $scope.selectedProcesses;
			delete $scope.sourceProcesses;
			InstanceCompareService.GetProcesses($scope.sourceInstance).then(function (sourceProcesses) {
				$scope.sourceProcesses = sourceProcesses;
				$scope.checkReady();

				if ($scope.loading_settings == 1) {
					$scope.LoadSettings2();
				}
			});
		}

		$scope.changeTargetDb = function () {
			$scope.processesLoaded = false;
			$scope.targetInstancesLoaded = false;
			delete $scope.$$childTail.targetInstance;
			delete $scope.targetInstance;
			InstanceCompareService.getTargetInstances($scope.targetDb).then(function (targetInstances) {
				$scope.targetInstances = targetInstances;
				$scope.targetInstancesLoaded = true;
				if (targetInstances.length == 1) {
					$scope.targetInstance = targetInstances[0]['instance'];
					$scope.$$childTail.targetInstance = targetInstances[0]['instance'];
				} else {
					delete $scope.$$childTail.targetInstance;
				}

				if ($scope.loading_settings == 2) {
					$scope.LoadSettings3();
				}

				$scope.checkReady();
			});

			$scope.checkReady();
			$scope.guid_cache = {};
		}

		if (SourceInstances.length == 1) {
			$scope.sourceInstance = SourceInstances[0]['instance'];
			$scope.changeSourceInstance();
		}

		$scope.changeTargetInstance = function () {
			if ($scope.loading_settings == 3) {
				$scope.LoadSettings4();
			} else {
				$scope.targetInstance = $scope.$$childTail.targetInstance;
			}
			$scope.checkReady();
		}

		$scope.checkReady = function () {
			if ($scope.sourceInstance && $scope.targetDb && $scope.$$childTail.targetInstance) {
				$scope.ready = true;
				$scope.processesLoaded = true;
			} else {
				$scope.ready = false;
			}
		}

		$scope.___duplicates = 0;
		$scope.instanceConfigurationsL1 = 0;
		$scope.instanceConfigurationsL2 = 0;
		$scope.instanceConfigurationsL3 = 0;
		$scope.languagesL1 = 0;
		$scope.languagesL2 = 0;
		$scope.languagesL3 = 0;
		$scope.translationsL1 = 0;
		$scope.translationsL2 = 0;
		$scope.translationsL3 = 0;
		//$scope.calendarsL1 = 0; $scope.calendarsL2 = 0; $scope.calendarsL3 = 0;
		//$scope.calendarEventsL1 = 0; $scope.calendarEventsL2 = 0; $scope.calendarEventsL3 = 0;
		//$scope.calendarStaticHolidaysL1 = 0; $scope.calendarStaticHolidaysL2 = 0; $scope.calendarStaticHolidaysL3 = 0;
		$scope.processesL1 = 0;
		$scope.processesL2 = 0;
		$scope.processesL3 = 0;
		$scope.processSubprocessesL2 = 0;
		$scope.processSubprocessesL3 = 0;
		$scope.listItemsL1 = [];
		$scope.listItemsL2 = [];
		$scope.listItemsL3 = [];
		$scope.matrixesL1 = [];
		$scope.matrixesL2 = [];
		$scope.matrixesL3 = [];
		$scope.userGroupL2 = 0;
		$scope.itemColumnsL1 = 0;
		$scope.itemColumnsL2 = 0;
		$scope.itemColumnsL3 = 0;
		$scope.containersL1 = 0;
		$scope.containersL2 = 0;
		$scope.containersL3 = 0;
		$scope.groupsL1 = 0;
		$scope.groupsL2 = 0;
		$scope.groupsL3 = 0;
		$scope.portfoliosL1 = 0;
		$scope.portfoliosL2 = 0;
		$scope.portfoliosL3 = 0;
		$scope.portfoliosDownloadTemplatesL1 = 0;
		$scope.portfoliosDownloadTemplatesL2 = 0;
		$scope.portfoliosDownloadTemplatesL3 = 0;
		$scope.tabsL1 = 0;
		$scope.tabsL2 = 0;
		$scope.tabsL3 = 0;
		$scope.actionsL1 = 0;
		$scope.actionsL2 = 0;
		$scope.actionsL3 = 0;
		$scope.actionsDialogL1 = 0;
		$scope.actionsDialogL2 = 0;
		$scope.actionsDialogL3 = 0;
		$scope.containerColumnsL1 = 0;
		$scope.containerColumnsL2 = 0;
		$scope.containerColumnsL3 = 0;
		$scope.portfolioColumnGroupsL1 = 0;
		$scope.portfolioColumnGroupsL2 = 0;
		$scope.portfolioColumnGroupsL3 = 0;
		$scope.portfolioColumnsL1 = 0;
		$scope.portfolioColumnsL2 = 0;
		$scope.portfolioColumnsL3 = 0;
		$scope.processPortfolioDialogsL1 = 0;
		$scope.processPortfolioDialogsL2 = 0;
		$scope.processPortfolioDialogsL3 = 0;
		$scope.processPortfolioActionsL1 = 0;
		$scope.processPortfolioActionsL2 = 0;
		$scope.processPortfolioActionsL3 = 0;
		$scope.processPortfolioGroupsL1 = 0;
		$scope.processPortfolioGroupsL2 = 0;
		$scope.processPortfolioGroupsL3 = 0;
		$scope.processPortfolioUserGroupsL1 = 0;
		$scope.processPortfolioUserGroupsL2 = 0;
		$scope.processPortfolioUserGroupsL3 = 0;
		$scope.processPortfolioTimemachineSavedL1 = 0;
		$scope.processPortfolioTimemachineSavedL2 = 0;
		$scope.processPortfolioTimemachineSavedL3 = 0;
		$scope.tabContainersL1 = 0;
		$scope.tabContainersL2 = 0;
		$scope.tabContainersL3 = 0;
		$scope.actionRowsGroupsL1 = 0;
		$scope.actionRowsGroupsL2 = 0;
		$scope.actionRowsGroupsL3 = 0;
		$scope.actionRowsL1 = 0;
		$scope.actionRowsL2 = 0;
		$scope.actionRowsL3 = 0;
		$scope.itemColumnJoinsL1 = 0;
		$scope.itemColumnJoinsL2 = 0;
		$scope.itemColumnJoinsL3 = 0;
		$scope.itemColumnEquationsL1 = 0;
		$scope.itemColumnEquationsL2 = 0;
		$scope.itemColumnEquationsL3 = 0;
		$scope.groupFeaturesL1 = 0;
		$scope.groupFeaturesL2 = 0;
		$scope.groupFeaturesL3 = 0;
		$scope.groupTabsL1 = 0;
		$scope.groupTabsL2 = 0;
		$scope.groupTabsL3 = 0;
		$scope.groupNavigationL1 = 0;
		$scope.groupNavigationL2 = 0;
		$scope.groupNavigationL3 = 0;
		$scope.tabColumnsL1 = 0;
		$scope.tabColumnsL2 = 0;
		$scope.tabColumnsL3 = 0;
		$scope.processTabsSubprocessesL1 = 0;
		$scope.processTabsSubprocessesL2 = 0;
		$scope.processTabsSubprocessesL3 = 0;
		$scope.processTabsSubprocessDataL1 = 0;
		$scope.processTabsSubprocessDataL2 = 0;
		$scope.processTabsSubprocessDataL3 = 0;
		$scope.processDashboardsL1 = 0;
		$scope.processDashboardsL2 = 0;
		$scope.processDashboardsL3 = 0;
		$scope.processDashboardChartsL1 = 0;
		$scope.processDashboardChartsL2 = 0;
		$scope.processDashboardChartsL3 = 0;
		$scope.processDashboardChartMapL1 = 0;
		$scope.processDashboardChartMapL2 = 0;
		$scope.processDashboardChartMapL3 = 0;
		$scope.processDashboardChartLinksL1 = 0;
		$scope.processDashboardChartLinksL2 = 0;
		$scope.processDashboardChartLinksL3 = 0;
		$scope.processDashboardUserGroupsL1 = 0;
		$scope.processDashboardUserGroupsL2 = 0;
		$scope.processDashboardUserGroupsL3 = 0;
		$scope.conditionsL1 = 0;
		$scope.conditionsL2 = 0;
		$scope.conditionsL3 = 0;
		$scope.conditionContainersL1 = 0;
		$scope.conditionContainersL2 = 0;
		$scope.conditionContainersL3 = 0;
		$scope.conditionFeaturesL1 = 0;
		$scope.conditionFeaturesL2 = 0;
		$scope.conditionFeaturesL3 = 0;
		$scope.conditionGroupsL1 = 0;
		$scope.conditionGroupsL2 = 0;
		$scope.conditionGroupsL3 = 0;
		$scope.conditionPortfoliosL1 = 0;
		$scope.conditionPortfoliosL2 = 0;
		$scope.conditionPortfoliosL3 = 0;
		$scope.conditionStatesL1 = 0;
		$scope.conditionStatesL2 = 0;
		$scope.conditionStatesL3 = 0;
		$scope.conditionTabsL1 = 0;
		$scope.conditionTabsL2 = 0;
		$scope.conditionTabsL3 = 0;
		$scope.conditionUserGroupsL1 = 0;
		$scope.conditionUserGroupsL2 = 0;
		$scope.conditionUserGroupsL3 = 0;
		$scope.conditionsGroupsL1 = 0;
		$scope.conditionsGroupsL2 = 0;
		$scope.conditionsGroupsL3 = 0;
		$scope.conditionsGroupsConditionL1 = 0;
		$scope.conditionsGroupsConditionL2 = 0;
		$scope.conditionsGroupsConditionL3 = 0;
		$scope.menuL1 = 0;
		$scope.menuL2 = 0;
		$scope.menuL3 = 0;
		$scope.menuRightsL1 = 0;
		$scope.menuRightsL2 = 0;
		$scope.menuRightsL3 = 0;
		$scope.menuStatesL1 = 0;
		$scope.menuStatesL2 = 0;
		$scope.menuStatesL3 = 0;
		$scope.notificationL1 = 0;
		$scope.notificationL2 = 0;
		$scope.notificationL3 = 0;
		$scope.notificationTagLinksL1 = 0;
		$scope.notificationTagLinksL2 = 0;
		$scope.notificationTagLinksL3 = 0;
		$scope.notificationConditionsL1 = 0;
		$scope.notificationConditionsL2 = 0;
		$scope.notificationConditionsL3 = 0;
		$scope.notificationReceiversL1 = 0;
		$scope.notificationReceiversL2 = 0;
		$scope.notificationReceiversL3 = 0;
		$scope.instanceUnionsL1 = 0;
		$scope.instanceUnionsL2 = 0;
		$scope.instanceUnionsL3 = 0;
		$scope.instanceUnionRowsL1 = 0;
		$scope.instanceUnionRowsL2 = 0;
		$scope.instanceUnionRowsL3 = 0;
		$scope.instanceUnionProcessesL1 = 0;
		$scope.instanceUnionProcessesL2 = 0;
		$scope.instanceUnionProcessesL3 = 0;
		$scope.instanceUnionColumnsL1 = 0;
		$scope.instanceUnionColumnsL2 = 0;
		$scope.instanceUnionColumnsL3 = 0;
		$scope.instanceSchedulesL1 = 0;
		$scope.instanceSchedulesL2 = 0;
		$scope.instanceSchedulesL3 = 0;
		$scope.instanceScheduleGroupsL1 = 0;
		$scope.instanceScheduleGroupsL2 = 0;
		$scope.instanceScheduleGroupsL3 = 0;
		$scope.instanceHelpersL1 = 0;
		$scope.instanceHelpersL2 = 0;
		$scope.instanceHelpersL3 = 0;
		$scope.instanceTranslationsL1 = 0;
		$scope.instanceTranslationsL2 = 0;
		$scope.instanceTranslationsL3 = 0;
		$scope.frontpageImagesL1 = 0;
		$scope.frontpageImagesL2 = 0;
		$scope.frontpageImagesL3 = 0;
		$scope.instanceCurrenciesL1 = 0;
		$scope.instanceCurrenciesL2 = 0;
		$scope.instanceCurrenciesL3 = 0;

		clearTimeout($rootScope.me.DeployStatusTimer);
		$rootScope.me.DeployStatusTimer = setInterval(function () {
			InstanceCompareService.getProgress().then(function (result) {
				$scope.progress = result;
				$scope.checkState();
			});
		}, 500);

		$scope.checkState = function () {
			$scope.state = 0;
			if ($scope.progress.PhaseProgress == "Comparing") $scope.state = 1;
			if ($scope.progress.PhaseProgress == "Deploying") $scope.state = 2;
			if ($scope.progress.PhaseProgress == "Done") $scope.state = 3;
			if ($scope.progress.DeployCount == 0) $scope.progress.DeployCount = 100;
		}

		$scope.outcome = '';

		$scope.reset = function () {
			if (!$scope.edit) return;
			$scope.outcome = '';
			InstanceCompareService.init().then(function () {
				$scope.edit = true;
			});
		}

		ViewService.footbar(StateParameters.ViewId, {template: 'pages/instanceCompare/instanceCompareFootbar.html'});
		$scope.compare = function (delayed_working, delayed_status, fromDeploy = false) {
			$scope.SaveSettings();
			$scope.edit = false;
			$scope.outcome = '';

			if (delayed_working == '') {
				$scope.working = 'Comparing...';
				$scope.status = '';
				$scope.error = '';
			}

			var selectedProcesses = $scope.$$childTail.selectedProcesses;
			if (angular.isUndefined(selectedProcesses) || selectedProcesses == null || selectedProcesses == [] || selectedProcesses == '') {
				selectedProcesses = '___none___';
			}

			let f = function (outcome) {
				$scope.parseReady = false;
				InstanceCompareService.compareAll($scope.sourceInstance, $scope.targetInstance, $scope.targetDb, selectedProcesses, $scope.skipProcessPortfoliosDownloadTemplates, $scope.skipHelps, $scope.skipConfigs, $scope.skipRequests).then(function (d) {
					if (delayed_working == '') {
						$scope.working = 'Compare done';
						$scope.status = '';
						$scope.error = '';
					} else {
						$scope.working = delayed_working;
						$scope.status = delayed_status;
						$scope.error = '';
					}

					setTimeout(function () {


						$scope.outcome = outcome;
						$scope.data = d;


						$scope.___duplicates = $filter('filter')(d.___duplicates, {id: ''}).length;

						$scope.instanceConfigurationsL1 = $filter('filter')(d.instanceConfigurations, {id2: ''}).length;
						$scope.instanceConfigurationsL2 = $filter('filter')(d.instanceConfigurations, {id2: null}).length;
						$scope.instanceConfigurationsL3 = $filter('filter')(d.instanceConfigurationsDeletes, {id2: null}).length;

						$scope.languagesL1 = $filter('filter')(d.languages, {id2: ''}).length;
						$scope.languagesL2 = $filter('filter')(d.languages, {id2: null}).length;
						$scope.languagesL3 = $filter('filter')(d.languagesDeletes, {id2: null}).length;

						$scope.translationsL1 = $filter('filter')(d.translations, {id2: ''}).length;
						$scope.translationsL2 = $filter('filter')(d.translations, {id2: null}).length;
						//$scope.translationsL3 = $filter('filter')(d.translationsDeletes, { id2: null }).length;

						//$scope.calendarsL1 = $filter('filter')(d.calendars, { id2: '' }).length;
						//$scope.calendarsL2 = $filter('filter')(d.calendars, { id2: null }).length;
						//$scope.calendarsL3 = $filter('filter')(d.calendarsDeletes, { id2: null }).length;

						//$scope.calendarEventsL1 = $filter('filter')(d.calendarEvents, { calendar_id: '' }).length;
						//$scope.calendarEventsL2 = $filter('filter')(d.calendarEvents, { calendar_id: null }).length;
						//$scope.calendarEventsL3 = $filter('filter')(d.calendarEventsDeletes, { calendar_id: null }).length;

						//$scope.calendarStaticHolidaysL1 = $filter('filter')(d.calendarStaticHolidays, { calendar_id: '' }).length;
						//$scope.calendarStaticHolidaysL2 = $filter('filter')(d.calendarStaticHolidays, { calendar_id: null }).length;
						//$scope.calendarStaticHolidaysL3 = $filter('filter')(d.calendarStaticHolidaysDeletes, { calendar_id: null }).length;

						$scope.processesL1 = $filter('filter')(d.processes, {id2: ''}).length;
						$scope.processesL2 = $filter('filter')(d.processes, {id2: null}).length;
						$scope.processesL3 = $filter('filter')(d.processesDeletes, {id2: null}).length;

						$scope.processSubprocessesL2 = $filter('filter')(d.processSubprocesses, {id2: null}).length;
						$scope.processSubprocessesL3 = $filter('filter')(d.processSubprocessesDeletes, {id2: null}).length;

						for (var i = 0; i < d.___listItems.length; i++) {
							if (d.___listItems[i].compare >= 2) {
								$scope.listItemsL1[d.___listItems[i].list] = $filter('filter')(d[d.___listItems[i].list], {guid2: ''}).length;
							}
							$scope.listItemsL2[d.___listItems[i].list] = $filter('filter')(d[d.___listItems[i].list], {guid2: null}).length;
							if (d.___listItems[i].compare == 3) {
								$scope.listItemsL3[d.___listItems[i].list] = $filter('filter')(d[d.___listItems[i].list + 'Deletes'], {guid2: null}).length;
							}
						}

						for (var i = 0; i < d.___matrixes.length; i++) {
							if (d.___matrixes[i].compare >= 2) {
								$scope.matrixesL1[d.___matrixes[i].process] = $filter('filter')(d['___matrix_' + d.___matrixes[i].process], {guid2: ''}).length;
							}
							$scope.matrixesL2[d.___matrixes[i].process] = $filter('filter')(d['___matrix_' + d.___matrixes[i].process], {guid2: null}).length;
							if (d.___matrixes[i].compare == 3) {
								$scope.matrixesL3[d.___matrixes[i].process] = $filter('filter')(d['___matrix_' + d.___matrixes[i].process + 'Deletes'], {guid2: null}).length;
							}
						}

						$scope.userGroupL2 = $filter('filter')(d.userGroup, {guid2: null}).length;

						$scope.itemColumnsL1 = $filter('filter')(d.itemColumns, {id2: ''}).length;
						$scope.itemColumnsL2 = $filter('filter')(d.itemColumns, {id2: null}).length;
						$scope.itemColumnsL3 = $filter('filter')(d.itemColumnsDeletes, {id2: null}).length;

						$scope.containersL1 = $filter('filter')(d.containers, {id2: ''}).length;
						$scope.containersL2 = $filter('filter')(d.containers, {id2: null}).length;
						$scope.containersL3 = $filter('filter')(d.containersDeletes, {id2: null}).length;

						$scope.groupsL1 = $filter('filter')(d.groups, {id2: ''}).length;
						$scope.groupsL2 = $filter('filter')(d.groups, {id2: null}).length;
						$scope.groupsL3 = $filter('filter')(d.groupsDeletes, {id2: null}).length;

						$scope.portfoliosL1 = $filter('filter')(d.portfolios, {id2: ''}).length;
						$scope.portfoliosL2 = $filter('filter')(d.portfolios, {id2: null}).length;
						$scope.portfoliosL3 = $filter('filter')(d.portfoliosDeletes, {id2: null}).length;

						$scope.portfoliosDownloadTemplatesL1 = $filter('filter')(d.portfoliosDownloadTemplates, {id2: ''}).length;
						$scope.portfoliosDownloadTemplatesL2 = $filter('filter')(d.portfoliosDownloadTemplates, {id2: null}).length;
						$scope.portfoliosDownloadTemplatesL3 = $filter('filter')(d.portfoliosDownloadTemplatesDeletes, {id2: null}).length;

						$scope.tabsL1 = $filter('filter')(d.tabs, {id2: ''}).length;
						$scope.tabsL2 = $filter('filter')(d.tabs, {id2: null}).length;
						$scope.tabsL3 = $filter('filter')(d.tabsDeletes, {id2: null}).length;

						$scope.actionsL1 = $filter('filter')(d.actions, {id2: ''}).length;
						$scope.actionsL2 = $filter('filter')(d.actions, {id2: null}).length;
						$scope.actionsL3 = $filter('filter')(d.actionsDeletes, {id2: null}).length;

						$scope.actionsDialogL1 = $filter('filter')(d.actionsDialog, {id2: ''}).length;
						$scope.actionsDialogL2 = $filter('filter')(d.actionsDialog, {id2: null}).length;
						$scope.actionsDialogL3 = $filter('filter')(d.actionsDialogDeletes, {id2: null}).length;

						$scope.containerColumnsL1 = $filter('filter')(d.containerColumns, {process_container_id2: ''}).length;
						$scope.containerColumnsL2 = $filter('filter')(d.containerColumns, {process_container_id2: null}).length;
						$scope.containerColumnsL3 = $filter('filter')(d.containerColumnsDeletes, {process_container_id2: null}).length;

						$scope.portfolioColumnGroupsL1 = $filter('filter')(d.portfolioColumnGroups, {id2: ''}).length;
						$scope.portfolioColumnGroupsL2 = $filter('filter')(d.portfolioColumnGroups, {id2: null}).length;
						$scope.portfolioColumnGroupsL3 = $filter('filter')(d.portfolioColumnGroupsDeletes, {id2: null}).length;

						$scope.portfolioColumnsL1 = $filter('filter')(d.portfolioColumns, {process_portfolio_id2: ''}).length;
						$scope.portfolioColumnsL2 = $filter('filter')(d.portfolioColumns, {process_portfolio_id2: null}).length;
						$scope.portfolioColumnsL3 = $filter('filter')(d.portfolioColumnsDeletes, {process_portfolio_id2: null}).length;

						$scope.processPortfolioDialogsL1 = $filter('filter')(d.processPortfolioDialogs, {id2: ''}).length;
						$scope.processPortfolioDialogsL2 = $filter('filter')(d.processPortfolioDialogs, {id2: null}).length;
						$scope.processPortfolioDialogsL3 = $filter('filter')(d.processPortfolioDialogsDeletes, {id2: null}).length;

						$scope.processPortfolioActionsL1 = $filter('filter')(d.processPortfolioActions, {id2: ''}).length;
						$scope.processPortfolioActionsL2 = $filter('filter')(d.processPortfolioActions, {id2: null}).length;
						$scope.processPortfolioActionsL3 = $filter('filter')(d.processPortfolioActionsDeletes, {id2: null}).length;

						$scope.processPortfolioGroupsL1 = $filter('filter')(d.processPortfolioGroups, {process_portfolio_id2: ''}).length;
						$scope.processPortfolioGroupsL2 = $filter('filter')(d.processPortfolioGroups, {process_portfolio_id2: null}).length;
						$scope.processPortfolioGroupsL3 = $filter('filter')(d.processPortfolioGroupsDeletes, {process_portfolio_id2: null}).length;

						$scope.processPortfolioUserGroupsL1 = $filter('filter')(d.processPortfolioUserGroups, {process_portfolio_id2: ''}).length;
						$scope.processPortfolioUserGroupsL2 = $filter('filter')(d.processPortfolioUserGroups, {process_portfolio_id2: null}).length;
						$scope.processPortfolioUserGroupsL3 = $filter('filter')(d.processPortfolioUserGroupsDeletes, {process_portfolio_id2: null}).length;

						$scope.processPortfolioTimemachineSavedL1 = $filter('filter')(d.processPortfolioTimemachineSaved, {save_id2: ''}).length;
						$scope.processPortfolioTimemachineSavedL2 = $filter('filter')(d.processPortfolioTimemachineSaved, {save_id2: null}).length;
						$scope.processPortfolioTimemachineSavedL3 = $filter('filter')(d.processPortfolioTimemachineSavedDeletes, {save_id2: null}).length;

						$scope.tabContainersL1 = $filter('filter')(d.tabContainers, {process_tab_id2: ''}).length;
						$scope.tabContainersL2 = $filter('filter')(d.tabContainers, {process_tab_id2: null}).length;
						$scope.tabContainersL3 = $filter('filter')(d.tabContainersDeletes, {process_tab_id2: null}).length;

						$scope.actionRowsGroupsL1 = $filter('filter')(d.actionRowsGroups, {id2: ''}).length;
						$scope.actionRowsGroupsL2 = $filter('filter')(d.actionRowsGroups, {id2: null}).length;
						$scope.actionRowsGroupsL3 = $filter('filter')(d.actionRowsGroupsDeletes, {id2: null}).length;

						$scope.actionRowsL1 = $filter('filter')(d.actionRows, {process_action_id2: ''}).length;
						$scope.actionRowsL2 = $filter('filter')(d.actionRows, {process_action_id2: null}).length;
						$scope.actionRowsL3 = $filter('filter')(d.actionRowsDeletes, {process_action_id2: null}).length;

						$scope.itemColumnJoinsL1 = $filter('filter')(d.itemColumnJoins, {item_column_id2: ''}).length;
						$scope.itemColumnJoinsL2 = $filter('filter')(d.itemColumnJoins, {item_column_id2: null}).length;
						$scope.itemColumnJoinsL3 = $filter('filter')(d.itemColumnJoinsDeletes, {item_column_id2: null}).length;

						$scope.itemColumnEquationsL1 = $filter('filter')(d.itemColumnEquations, {id2: ''}).length;
						$scope.itemColumnEquationsL2 = $filter('filter')(d.itemColumnEquations, {id2: null}).length;
						$scope.itemColumnEquationsL3 = $filter('filter')(d.itemColumnEquationsDeletes, {id2: null}).length;

						$scope.groupFeaturesL1 = $filter('filter')(d.groupFeatures, {process_group_id2: ''}).length;
						$scope.groupFeaturesL2 = $filter('filter')(d.groupFeatures, {process_group_id2: null}).length;
						$scope.groupFeaturesL3 = $filter('filter')(d.groupFeaturesDeletes, {process_group_id2: null}).length;

						$scope.groupTabsL1 = $filter('filter')(d.groupTabs, {process_group_id2: ''}).length;
						$scope.groupTabsL2 = $filter('filter')(d.groupTabs, {process_group_id2: null}).length;
						$scope.groupTabsL3 = $filter('filter')(d.groupTabsDeletes, {process_group_id2: null}).length;

						$scope.groupNavigationL1 = $filter('filter')(d.groupNavigation, {variable2: ''}).length;
						$scope.groupNavigationL2 = $filter('filter')(d.groupNavigation, {variable2: null}).length;
						$scope.groupNavigationL3 = $filter('filter')(d.groupNavigationDeletes, {variable2: null}).length;

						$scope.tabColumnsL1 = $filter('filter')(d.tabColumns, {process_tab_id2: ''}).length;
						$scope.tabColumnsL2 = $filter('filter')(d.tabColumns, {process_tab_id2: null}).length;
						$scope.tabColumnsL3 = $filter('filter')(d.tabColumnsDeletes, {process_tab_id2: null}).length;

						$scope.processTabsSubprocessesL1 = $filter('filter')(d.processTabsSubprocesses, {id2: ''}).length;
						$scope.processTabsSubprocessesL2 = $filter('filter')(d.processTabsSubprocesses, {id2: null}).length;
						$scope.processTabsSubprocessesL3 = $filter('filter')(d.processTabsSubprocessesDeletes, {id2: null}).length;

						$scope.processTabsSubprocessDataL1 = $filter('filter')(d.processTabsSubprocessData, {process_tab_subprocess_id2: ''}).length;
						$scope.processTabsSubprocessDataL2 = $filter('filter')(d.processTabsSubprocessData, {process_tab_subprocess_id2: null}).length;
						$scope.processTabsSubprocessDataL3 = $filter('filter')(d.processTabsSubprocessDataDeletes, {process_tab_subprocess_id2: null}).length;

						$scope.processDashboardsL1 = $filter('filter')(d.processDashboards, {id2: ''}).length;
						$scope.processDashboardsL2 = $filter('filter')(d.processDashboards, {id2: null}).length;
						$scope.processDashboardsL3 = $filter('filter')(d.processDashboardsDeletes, {id2: null}).length;

						$scope.processDashboardChartsL1 = $filter('filter')(d.processDashboardCharts, {id2: ''}).length;
						$scope.processDashboardChartsL2 = $filter('filter')(d.processDashboardCharts, {id2: null}).length;
						$scope.processDashboardChartsL3 = $filter('filter')(d.processDashboardChartsDeletes, {id2: null}).length;

						$scope.processDashboardChartMapL1 = $filter('filter')(d.processDashboardChartMap, {id2: ''}).length;
						$scope.processDashboardChartMapL2 = $filter('filter')(d.processDashboardChartMap, {id2: null}).length;
						$scope.processDashboardChartMapL3 = $filter('filter')(d.processDashboardChartMapDeletes, {id2: null}).length;

						$scope.processDashboardChartLinksL1 = $filter('filter')(d.processDashboardChartLinks, {id2: ''}).length;
						$scope.processDashboardChartLinksL2 = $filter('filter')(d.processDashboardChartLinks, {id2: null}).length;
						$scope.processDashboardChartLinksL3 = $filter('filter')(d.processDashboardChartLinksDeletes, {id2: null}).length;

						$scope.processDashboardUserGroupsL1 = $filter('filter')(d.processDashboardUserGroups, {dashboard_id2: ''}).length;
						$scope.processDashboardUserGroupsL2 = $filter('filter')(d.processDashboardUserGroups, {dashboard_id2: null}).length;
						$scope.processDashboardUserGroupsL3 = $filter('filter')(d.processDashboardUserGroupsDeletes, {dashboard_id2: null}).length;

						$scope.conditionsL1 = $filter('filter')(d.conditions, {id2: ''}).length;
						$scope.conditionsL2 = $filter('filter')(d.conditions, {id2: null}).length;
						$scope.conditionsL3 = $filter('filter')(d.conditionsDeletes, {id2: null}).length;

						$scope.conditionContainersL1 = $filter('filter')(d.conditionContainers, {condition_id2: ''}).length;
						$scope.conditionContainersL2 = $filter('filter')(d.conditionContainers, {condition_id2: null}).length;
						$scope.conditionContainersL3 = $filter('filter')(d.conditionContainersDeletes, {condition_id2: null}).length;

						$scope.conditionFeaturesL1 = $filter('filter')(d.conditionFeatures, {condition_id2: ''}).length;
						$scope.conditionFeaturesL2 = $filter('filter')(d.conditionFeatures, {condition_id2: null}).length;
						$scope.conditionFeaturesL3 = $filter('filter')(d.conditionFeaturesDeletes, {condition_id2: null}).length;

						$scope.conditionGroupsL1 = $filter('filter')(d.conditionGroups, {condition_id2: ''}).length;
						$scope.conditionGroupsL2 = $filter('filter')(d.conditionGroups, {condition_id2: null}).length;
						$scope.conditionGroupsL3 = $filter('filter')(d.conditionGroupsDeletes, {condition_id2: null}).length;

						$scope.conditionPortfoliosL1 = $filter('filter')(d.conditionPortfolios, {condition_id2: ''}).length;
						$scope.conditionPortfoliosL2 = $filter('filter')(d.conditionPortfolios, {condition_id2: null}).length;
						$scope.conditionPortfoliosL3 = $filter('filter')(d.conditionPortfoliosDeletes, {condition_id2: null}).length;

						$scope.conditionStatesL1 = $filter('filter')(d.conditionStates, {condition_id2: ''}).length;
						$scope.conditionStatesL2 = $filter('filter')(d.conditionStates, {condition_id2: null}).length;
						$scope.conditionStatesL3 = $filter('filter')(d.conditionStatesDeletes, {condition_id2: null}).length;

						$scope.conditionTabsL1 = $filter('filter')(d.conditionTabs, {condition_id2: ''}).length;
						$scope.conditionTabsL2 = $filter('filter')(d.conditionTabs, {condition_id2: null}).length;
						$scope.conditionTabsL3 = $filter('filter')(d.conditionTabsDeletes, {condition_id2: null}).length;

						$scope.conditionUserGroupsL1 = $filter('filter')(d.conditionUserGroups, {condition_id2: ''}).length;
						$scope.conditionUserGroupsL2 = $filter('filter')(d.conditionUserGroups, {condition_id2: null}).length;
						$scope.conditionUserGroupsL3 = $filter('filter')(d.conditionUserGroupsDeletes, {condition_id2: null}).length;

						$scope.conditionsGroupsL1 = $filter('filter')(d.conditionsGroups, {variable2: ''}).length;
						$scope.conditionsGroupsL2 = $filter('filter')(d.conditionsGroups, {variable2: null}).length;
						$scope.conditionsGroupsL3 = $filter('filter')(d.conditionsGroupsDeletes, {variable2: null}).length;

						$scope.conditionsGroupsConditionL1 = $filter('filter')(d.conditionsGroupsCondition, {condition_id2: ''}).length;
						$scope.conditionsGroupsConditionL2 = $filter('filter')(d.conditionsGroupsCondition, {condition_id2: null}).length;
						$scope.conditionsGroupsConditionL3 = $filter('filter')(d.conditionsGroupsConditionDeletes, {condition_id2: null}).length;

						$scope.menuL1 = $filter('filter')(d.menu, {id2: ''}).length;
						$scope.menuL2 = $filter('filter')(d.menu, {id2: null}).length;
						$scope.menuL3 = $filter('filter')(d.menuDeletes, {id2: null}).length;

						$scope.menuRightsL1 = $filter('filter')(d.menuRights, {instance_menu_id2: ''}).length;
						$scope.menuRightsL2 = $filter('filter')(d.menuRights, {instance_menu_id2: null}).length;
						$scope.menuRightsL3 = $filter('filter')(d.menuRightsDeletes, {instance_menu_id2: null}).length;

						$scope.menuStatesL1 = $filter('filter')(d.menuStates, {instance_menu_id2: ''}).length;
						$scope.menuStatesL2 = $filter('filter')(d.menuStates, {instance_menu_id2: null}).length;
						$scope.menuStatesL3 = $filter('filter')(d.menuStatesDeletes, {instance_menu_id2: null}).length;

						$scope.notificationL1 = $filter('filter')(d.notification, {guid2: ''}).length;
						$scope.notificationL2 = $filter('filter')(d.notification, {guid2: null}).length;
						$scope.notificationL3 = $filter('filter')(d.notificationDeletes, {guid2: null}).length;

						$scope.notificationTagLinksL1 = $filter('filter')(d.notificationTagLinks, {guid2: ''}).length;
						$scope.notificationTagLinksL2 = $filter('filter')(d.notificationTagLinks, {guid2: null}).length;
						$scope.notificationTagLinksL3 = $filter('filter')(d.notificationTagLinksDeletes, {guid2: null}).length;

						$scope.notificationConditionsL1 = $filter('filter')(d.notificationConditions, {id2: ''}).length;
						$scope.notificationConditionsL2 = $filter('filter')(d.notificationConditions, {id2: null}).length;
						$scope.notificationConditionsL3 = $filter('filter')(d.notificationConditionsDeletes, {id2: null}).length;

						$scope.notificationReceiversL1 = $filter('filter')(d.notificationReceivers, {notification_condition_id2: ''}).length;
						$scope.notificationReceiversL2 = $filter('filter')(d.notificationReceivers, {notification_condition_id2: null}).length;
						$scope.notificationReceiversL3 = $filter('filter')(d.notificationReceiversDeletes, {notification_condition_id2: null}).length;

						$scope.instanceUnionsL1 = $filter('filter')(d.instanceUnions, {id2: ''}).length;
						$scope.instanceUnionsL2 = $filter('filter')(d.instanceUnions, {id2: null}).length;
						$scope.instanceUnionsL3 = $filter('filter')(d.instanceUnionsDeletes, {id2: null}).length;

						$scope.instanceUnionRowsL1 = $filter('filter')(d.instanceUnionRows, {id2: ''}).length;
						$scope.instanceUnionRowsL2 = $filter('filter')(d.instanceUnionRows, {id2: null}).length;
						$scope.instanceUnionRowsL3 = $filter('filter')(d.instanceUnionRowsDeletes, {id2: null}).length;

						$scope.instanceUnionProcessesL1 = $filter('filter')(d.instanceUnionProcesses, {id2: ''}).length;
						$scope.instanceUnionProcessesL2 = $filter('filter')(d.instanceUnionProcesses, {id2: null}).length;
						$scope.instanceUnionProcessesL3 = $filter('filter')(d.instanceUnionProcessesDeletes, {id2: null}).length;

						$scope.instanceUnionColumnsL1 = $filter('filter')(d.instanceUnionColumns, {id2: ''}).length;
						$scope.instanceUnionColumnsL2 = $filter('filter')(d.instanceUnionColumns, {id2: null}).length;
						$scope.instanceUnionColumnsL3 = $filter('filter')(d.instanceUnionColumnsDeletes, {id2: null}).length;

						$scope.instanceSchedulesL1 = $filter('filter')(d.instanceSchedules, {guid2: ''}).length;
						$scope.instanceSchedulesL2 = $filter('filter')(d.instanceSchedules, {guid2: null}).length;
						//$scope.instanceSchedulesL3 = $filter('filter')(d.instanceSchedulesDeletes, { guid2: null }).length;

						$scope.instanceScheduleGroupsL1 = $filter('filter')(d.instanceScheduleGroups, { group_id2: '' }).length;
						$scope.instanceScheduleGroupsL2 = $filter('filter')(d.instanceScheduleGroups, { group_id2: null }).length;
						//$scope.instanceScheduleGroupsL3 = $filter('filter')(d.instanceScheduleGroupsDeletes, { group_id2: null }).length;

						$scope.instanceHelpersL1 = $filter('filter')(d.instanceHelpers, {id2: ''}).length;
						$scope.instanceHelpersL2 = $filter('filter')(d.instanceHelpers, {id2: null}).length;
						$scope.instanceHelpersL3 = $filter('filter')(d.instanceHelpersDeletes, {id2: null}).length;

						$scope.instanceTranslationsL1 = $filter('filter')(d.instanceTranslations, {variable2: ''}).length;
						$scope.instanceTranslationsL2 = $filter('filter')(d.instanceTranslations, {variable2: null}).length;
						$scope.instanceTranslationsL3 = $filter('filter')(d.instanceTranslationsDeletes, {variable2: null}).length;

						$scope.frontpageImagesL1 = $filter('filter')(d.frontpageImages, {id2: ''}).length;
						$scope.frontpageImagesL2 = $filter('filter')(d.frontpageImages, {id2: null}).length;
						$scope.frontpageImagesL3 = $filter('filter')(d.frontpageImagesDeletes, {id2: null}).length;

						$scope.instanceCurrenciesL1 = $filter('filter')(d.instanceCurrencies, {id2: ''}).length;
						$scope.instanceCurrenciesL2 = $filter('filter')(d.instanceCurrencies, {id2: null}).length;
						$scope.instanceCurrenciesL3 = $filter('filter')(d.instanceCurrenciesDeletes, {id2: null}).length;

						var updates = [];
						var oldOrd = '';
						var newOrd = '';
						for (var i = 0; i < d.updates.length; i++) {
							if (i == 0) {
								newOrd = Common.getOrderNoBetween('a');
							} else {
								newOrd = Common.getOrderNoBetween(oldOrd);
							}
							oldOrd = newOrd;
							updates.push({skip: false, sql: d.updates[i], old_order_no: i, order_no: newOrd});
						}
						$scope.updates = updates;
						$scope.edit = true;
						$scope.parseReady = true;
					}, 100);
					$("div.select_all_sql").removeClass('aria-detect');
				})
					.catch(function (data) {
						$scope.edit = true;
						$scope.working = 'ERROR!!!';
						$scope.status = '';
						$scope.error = data.data.Info.description;
						$scope.outcome = 'fail';
						$scope.parseReady = true;
					});
			}

			if (!fromDeploy) {
				InstanceCompareService.init().then(function () {
					f('compared');
				});
			} else {
				if (delayed_status && delayed_status.length == 2 && delayed_status[0] == "OK") {
					f('deployed');
				} else if (delayed_status && delayed_status.length > 0) {
					$scope.error = "Error";
					$scope.status = delayed_status;
					$scope.outcome = 'fail';
					$scope.parseReady = true;
					$scope.edit = true;
				} else {
					f('deployed');
				}
			}

		};

		$scope.deploy = function () {
			MessageService.confirm('Do you really want to deploy?', function () {
				var source_version = '';
				for (var i = 0; i < $scope.sourceDbs.length; i++) {
					if ($scope.sourceDbs[i].database == $scope.sourceDb) {
						source_version = $scope.sourceDbs[i].name;
						source_version = source_version.substr(source_version.indexOf("---"))
						var at_start = source_version.indexOf("@");
						var at_end = source_version.indexOf("---", at_start);
						source_version = source_version.replace(source_version.substring(at_start, at_end), '');
					}
				}

				var target_version = '';
				for (var i = 0; i < $scope.databases.length; i++) {
					if ($scope.databases[i].database == $scope.targetDb) {
						target_version = $scope.databases[i].name;
						target_version = target_version.substr(target_version.indexOf("---"))
						var at_start = target_version.indexOf("@");
						var at_end = target_version.indexOf("---", at_start);
						target_version = target_version.replace(target_version.substring(at_start, at_end), '');
					}
				}

				if (source_version.toLowerCase() != target_version.toLowerCase()) {
					MessageService.confirm('Instances have different versions. Are you really sure that you want to deploy?', function () {
						$scope.do_deploy();
					});
				} else {
					$scope.do_deploy();
				}
			});
		}

		$scope.do_deploy = function () {
			$scope.SaveSettings();
			$scope.edit = false;
			$scope.working = 'Deploying...';
			$scope.status = '';
			$scope.error = '';
			$scope.outcome = '';

			var selectedProcesses = $scope.$$childTail.selectedProcesses;
			if (angular.isUndefined(selectedProcesses) || selectedProcesses == null || selectedProcesses == [] || selectedProcesses == '') {
				selectedProcesses = '___none___';
			}

			InstanceCompareService.deployChangesWithSkipAndOrder($scope.sourceInstance, $scope.targetInstance, $scope.targetDb, selectedProcesses, $scope.skipProcessPortfoliosDownloadTemplates, $scope.skipHelps, $scope.skipConfigs, $scope.skipRequests)
				.then(function (d) {
					$scope.compare('Deploy done', d, true);
				})
				.catch(function (data) {
					$scope.edit = true;
					$scope.working = 'ERROR!!!';
					$scope.status = '';
					$scope.error = 'Error: ' + data.data.Info.description;
					$scope.parseReady = true;
				});
		}

		$scope.selectSql = function () {
			SelectText('sql');
		}

		function SelectText(node) {
			node = document.getElementById(node);
			if (document.body.createTextRange) {
				const range = document.body.createTextRange();
				range.moveToElementText(node);
				range.select();
			} else if (window.getSelection) {
				const selection = window.getSelection();
				const range = document.createRange();
				range.selectNodeContents(node);
				selection.removeAllRanges();
				selection.addRange(range);
			} else {
				console.warn("Could not select text in node: Unsupported browser.");
			}
		}

		$scope.orderSqls = function (current, prev, next) {
			var beforeOrderNo;
			var afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.order_no;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.order_no;
			}

			current.order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
		};

		$scope.scrollToSql = function () {
			$("content").animate({
				scrollTop: $("#sql_div").offset().top - 110
			}, 300, "easeOutCubic");
		}

		//$scope.restart = function () {
		//	MessageService.confirm('Do you really want to restart application?', function () {
		//		RuntimeWidgetService.restart().then(function () {
		//			document.location.reload();
		//		});
		//	});
		//};

		//$(document).ready(function () {
		//	$("div.restart").removeClass('aria-detect');
		//});

		$scope.guid = '';
		$scope.guid_infos = '';
		$scope.guid_cache = {};
		$scope.guid_show = false;

		$scope.discoverGuid = function (guid) {
			$scope.guid_show = false;
			if (guid == undefined) {
				guid = $scope.guid;
			}
			guid = guid.trim();
			if (guid.length == 36) {
				if ($scope.guid_cache[guid] == undefined) {
					InstanceCompareService.discoverGUID(guid, $scope.targetDb).then(function (guid_infos) {
						$scope.guid_infos = guid_infos;
						$scope.guid_cache[guid] = guid_infos;
						$scope.guid_show = true;
					});
				} else {
					$scope.guid_infos = $scope.guid_cache[guid];
					$scope.guid_show = true;
				}
			}
		};

		$scope.getProcessType = function (process_type) {
			switch (process_type) {
				case 0:
					return Translator.translation('CUSTOMER_PROCESSES');
				case 1:
					return Translator.translation('SUPPORT_PROCESSES');
				case 2:
					return Translator.translation('DATA_PROCESSES');
				case 3:
					return Translator.translation('LINK_PROCESSES');
				case 4:
					return Translator.translation('MATRIX_PROCESSES');
				case 5:
					return Translator.translation('TABLE_PROCESSES');
			}
			return '';
		};

		$scope.list_types = {};
		ProcessService.getListItems('list_admin_types').then(function (list_types) {
			angular.forEach(list_types, function (item) {
				$scope.list_types[item.item_id] = item.list_item;
			});
		});

		$scope.getListType = function (list_type) {
			if (list_type > 0) {
				return $scope.list_types[list_type];
			}
			return '';
		};

		$scope.CheckEdit = function () {
			$("label").each(function () {
				$(this).html($(this).html().replace('[red]', '<span style="color:red;">').replace('[/red]', '</span>'));
				$(this).attr("tooltip", $(this).attr("tooltip").replace('[red]', '').replace('[/red]', ''));
			});
			return $scope.edit;
		}

		Array.prototype.rotate = function (n) {
			var len = this.length;
			var res = new Array(this.length);
			if (n % len === 0) {
				return this.slice();
			} else {
				for (var i = 0; i < len; i++) {
					res[i] = this[(i + (len + n % len)) % len];
				}
			}
			return res;
		};

		String.prototype.diff = function (s) {
			function getMatchingSubstring(s, l, m) {
				var i = 0;
				var slen = s.length;
				var match = false;
				var o = {fis: slen, mtc: m, sbs: ''};
				while (i < slen) {
					l[i] === s[i] ? match ? o.sbs += s[i] : (match = true, o.fis = i, o.sbs = s[i]) : match && (i = slen);
					++i;
				}
				return o;
			}

			function getChanges(t, s, m) {
				var isThisLonger = t.length >= s.length ? true : false;
				var [longer, shorter] = isThisLonger ? [t, s] : [s, t];
				var bi = 0;

				while (shorter[bi] === longer[bi] && bi < shorter.length) {
					++bi;
				}
				longer = longer.split('').slice(bi);
				shorter = shorter.slice(bi);

				var len = longer.length;
				var cd = {
					fis: shorter.length,
					fil: len,
					sbs: '',
					mtc: m + s.slice(0, bi)
				};
				var sub = {sbs: ''};

				if (shorter !== '') {
					for (var rc = 0; rc < len && sub.sbs.length < len; rc++) {
						sub = getMatchingSubstring(shorter, longer.rotate(rc), cd.mtc);
						sub.fil = rc < len - sub.fis ? sub.fis + rc : sub.fis - len + rc;
						sub.sbs.length > cd.sbs.length && (cd = sub);
					}
				}
				[cd.del, cd.ins] = isThisLonger ? [longer.slice(0, cd.fil).join(''), shorter.slice(0, cd.fis)] : [shorter.slice(0, cd.fis), longer.slice(0, cd.fil).join('')];
				return cd.del.indexOf(' ') == -1 ||
				cd.ins.indexOf(' ') == -1 ||
				cd.del === '' ||
				cd.ins === '' ||
				cd.sbs === '' ? cd : getChanges(cd.del, cd.ins, cd.mtc);
			}

			var changeData =
					getChanges(this, s, ''),
				nextS = s.slice(changeData.mtc.length + changeData.ins.length + changeData.sbs.length),
				nextThis = this.slice(changeData.mtc.length + changeData.del.length + changeData.sbs.length),
				result = '';
			changeData.del.length > 0 && (changeData.del = '<span style="color:white; background-color:#f44335;">' + changeData.del + '</span>');
			changeData.ins.length > 0 && (changeData.ins = '');
			result = changeData.mtc + changeData.del + changeData.ins + changeData.sbs;
			result += (nextThis !== '' || nextS !== '') ? nextThis.diff(nextS) : '';
			return result;
		};

		$scope.GetDiff = function (prefix, id, from_str, to_str) {
			if (!$scope.skipCompare) {
				$("#" + prefix + "_" + id).html(from_str);
				return '';
			}

			if (from_str == null) from_str = '';
			if (to_str == null) to_str = '';
			$("#" + prefix + "_" + id).html(from_str.diff(to_str));
			return '';
		};

		$scope.SqlOpen = function () {
			return $("accordion.sql pane").hasClass("expanded");
		};

		$scope.SaveSettings = function () {
			var settings = {
				"sourceInstance": $scope.sourceInstance,
				"targetDb": $scope.targetDb,
				"targetInstance": $scope.targetInstance,
				"processes": $scope.$$childTail.selectedProcesses,
				"skipProcessPortfoliosDownloadTemplates": $scope.skipProcessPortfoliosDownloadTemplates,
				"skipHelps": $scope.skipHelps,
				"skipConfigs": $scope.skipConfigs,
				"skipRequests": $scope.skipRequests,
				"skipCompare": $scope.skipCompare
			};

			Configuration.setChangedPreferences('compare', ['compare']);
			Configuration.setActivePreferences('compare', {'compare': settings});
			Configuration.saveActivePreferences('compare', 'compare');
		};

		$scope.loading_settings = 0;
		$scope.load_settings = {};

		$scope.LoadSettings = function () {
			var settings = Configuration.getActivePreferences('compare', 'compare');
			if (Object.keys(settings).length > 0) {
				$scope.loading_settings = 1;
				$scope.load_settings = settings;

				$scope.sourceInstance = settings.sourceInstance;
				$scope.changeSourceInstance();
			} else if ($scope.databases.length == 1) {
				$scope.targetDb = $scope.databases[0]["database"];
				$scope.changeTargetDb();
			}
		};

		$scope.LoadSettings2 = function () {
			$scope.loading_settings = 2;

			$scope.targetDb = $scope.load_settings.targetDb;
			$scope.changeTargetDb();
		};

		$scope.LoadSettings3 = function () {
			$scope.loading_settings = 3;

			$scope.targetInstance = $scope.load_settings.targetInstance;
			$scope.$$childTail.targetInstance = $scope.load_settings.targetInstance;
			$scope.changeTargetInstance();
		};

		$scope.LoadSettings4 = function () {
			$scope.selectedProcesses = $scope.load_settings.processes;
			$scope.$$childTail.selectedProcesses = $scope.load_settings.processes;
			$scope.skipProcessPortfoliosDownloadTemplates = $scope.load_settings.skipProcessPortfoliosDownloadTemplates;
			$scope.skipHelps = $scope.load_settings.skipHelps;
			$scope.skipConfigs = $scope.load_settings.skipConfigs;
			$scope.skipRequests = $scope.load_settings.skipRequests;
			$scope.skipCompare = $scope.load_settings.skipCompare;

			$scope.loading_settings = 0;
		};

		$scope.LoadSettings();
	}
})();