﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.service('InstanceCompareService', InstanceCompareService);

	InstanceCompareService.$inject = ['$q', 'ApiService'];
	function InstanceCompareService($q, ApiService) {
		var self = this;

		var InstanceCompare = ApiService.v1api('deploy/InstanceCompare');

		self.getSourceInstances = function () {
			return InstanceCompare.get(['instances']);
		};

		self.getTargetInstances = function (targetDb) {
			return InstanceCompare.get(['instances', targetDb]);
		};

		self.getDb = function () {
			return InstanceCompare.get(['getDb']);
		};

		self.init = function() {
			return InstanceCompare.get(['init']);
		}
		
		self.getProgress = function() {
			return InstanceCompare.get(['progress']);
		}

		self.getDbList = function () {
			return InstanceCompare.get(['getDbList']);
		};

		self.GetProcesses = function (sourceInstance) {
			return InstanceCompare.get(['processes', sourceInstance]);
		};

		self.compareAll = function (sourceInstance, targetInstance, targetDb, processes, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests) {
			return InstanceCompare.get(['compareAll', sourceInstance, targetInstance, targetDb, processes, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests]);
		};

		self.deployChangesWithSkipAndOrder = function (sourceInstance, targetInstance, targetDb, processes, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests) {
			return InstanceCompare.save(['deployChanges', sourceInstance, targetInstance, targetDb, processes, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests]);
		};

		self.discoverGUID = function (guid, target_db) {
			return InstanceCompare.get(['discoverGUID/' + guid + '/' + target_db]);
		};
	}
})();