(function () {
	'use strict';

	angular
		.module('core')
		.config(BackgroundProcessConfig)
		.service('BackgroundProcessesService', BackgroundProcessesService)
		.controller('BackgroundProcessConfigController', BackgroundProcessConfigController)
		.controller('BackgroundProcessLogsController', BackgroundProcessLogsController)
		.controller('BackgroundProcessTimeSettingsController', BackgroundProcessTimeSettingsController)
		.controller('BackgroundProcessesController', BackgroundProcessesController);

	BackgroundProcessesService.$inject = ['ApiService', 'ConnectorService', '$rootScope', 'MessageService'];
	function BackgroundProcessesService(ApiService, ConnectorService, $rootScope, MessageService) {
		let self = this;
		let InstanceSchedulesRest = ApiService.v1api('settings/BackgroundProcessSettings');
		let BackgroundProcesses = ApiService.v1api('settings/RegisteredBackgroundProcesses');
		let Items = ApiService.v1api('meta/Items');

		self.getBackgroundProcessRows = function () {
			return InstanceSchedulesRest.get();
		};

		self.addBackgroundProcess = function (groupId) {
			return InstanceSchedulesRest.new(groupId);
		}

		self.saveBackgroundProcess = function (changed) {
			return InstanceSchedulesRest.save("", changed);
		}

		self.getBackgroundProcesses = function () {
			return BackgroundProcesses.get();
		}

		self.deleteBackgroundProcess = function (scheduleId) {
			return InstanceSchedulesRest.delete([scheduleId])
		}

		self.forceExecute = function (schedule) {
			return InstanceSchedulesRest.new(["async_with_schedule"], schedule).then(function () {
			});
		}

		self.forceExecuteGroup = function (schedules) {
			return InstanceSchedulesRest.new(["async_with_schedule_group"], schedules).then(function () {
			});
		}

		self.saveInstanceDefaultUser = function (defaults) {
			return InstanceSchedulesRest.save(["instance_default_user"], defaults);
		}

		self.getInstanceDefaults = function () {
			return InstanceSchedulesRest.get(["instance_defaults"]);
		}

		self.getLogs = function (id) {
			return InstanceSchedulesRest.get(["logs", id]);
		}

		self.getOutcomes = function () {
			return InstanceSchedulesRest.get(["outcomes"]);
		}

		self.executeOnDemand = function (bgProcessName, settings, msg = 'BG_PROCESS_STARTED') {
			MessageService.toast(msg);
			return Items.new(["async_no_schedule", bgProcessName], settings);
		}

		self.getGroups = function(){
			return InstanceSchedulesRest.get(["groups"]);
		}

		self.addGroup = function(){
			return InstanceSchedulesRest.new(["addgroup"]);
		}

		self.saveGroup = function(group){
			return InstanceSchedulesRest.save(["group"], group);
		}

		self.deleteGroups = function(groups){
			return InstanceSchedulesRest.new(["deletegroups"], groups);
		}
	}

	BackgroundProcessConfig.$inject = ['ViewsProvider'];

	function BackgroundProcessConfig(ViewsProvider) {
		ViewsProvider.addView('backgroundProcesses', {
			controller: 'BackgroundProcessesController',
			template: 'pages/backgroundProcesses/backgroundProcesses.html',
			resolve: {
				BackgroundProcesses: function (BackgroundProcessesService) {
					return BackgroundProcessesService.getBackgroundProcesses().then(function (processes) {
						_.each(processes, function (process, key) {
							if (process.NotSchedulable == 1) {
								delete processes[key];
							}
						})
						return processes
					});
				},
				InstanceDefaults: function (BackgroundProcessesService) {
					return BackgroundProcessesService.getInstanceDefaults().then(function (defaults) {
						return defaults;
					});
				},
				Outcomes: function (BackgroundProcessesService) {
					return BackgroundProcessesService.getOutcomes();
				},
				BackgroundProcessGroups: function(BackgroundProcessesService){
					return BackgroundProcessesService.getGroups().then(function(groups){
						const ungroupedGroup = {
							GroupId: 0,
							Translation: {'en-GB': 'Ungrouped', 'fi-FI': 'Ei-ryhmitelty', 'de-DE': 'Nicht gruppiert'},
							OrderNo: '1',
							Schedules: [],
							Instance: ""
						}
						groups.unshift(ungroupedGroup);
						return groups;
					});
				}
			}, afterResolve: {
				BackgroundProcessRows: function (BackgroundProcessesService, BackgroundProcessGroups) {
					return BackgroundProcessesService.getBackgroundProcessRows().then(function(rows){
						_.each(rows, function(row){
							if(row.GroupId == 0) BackgroundProcessGroups[0].Schedules.push(row);
						})
						return rows;
					});
				},
			}
		});

		ViewsProvider.addPage('backgroundProcesses', 'PAGE_BG_PROCESSES', ['read', 'write']);
	}

	BackgroundProcessesController.$inject = [
		'$scope',
		'BackgroundProcessRows',
		'BackgroundProcessesService',
		'SaveService',
		'BackgroundProcesses',
		'Translator',
		'MessageService',
		'StateParameters',
		'InstanceDefaults',
		'Outcomes',
		'CalendarService',
		'ViewService',
		'Common',
		'BackgroundProcessGroups'
	];

	function BackgroundProcessesController(
		$scope,
		BackgroundProcessRows,
		BackgroundProcessesService,
		SaveService,
		BackgroundProcesses,
		Translator,
		MessageService,
		StateParameters,
		InstanceDefaults,
		Outcomes,
		CalendarService,
		ViewService,
		Common,
		BackgroundProcessGroups
	) {

		$scope.types = [];
		if(InstanceDefaults.InstanceDefaultUser == 0) InstanceDefaults.InstanceDefaultUser = undefined;

		$scope.instanceDefaults = InstanceDefaults;

		$scope.onSort = function(current, prev, next){
			let beforeOrderNo;
			let afterOrderNo;
			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}
			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}
			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			$scope.edit(current);
		}

		_.each(BackgroundProcesses, function (bgProcess) {
			bgProcess.$$translatedName = Translator.translate(bgProcess.LanguageVariable);
			if (bgProcess.$$translatedName.length == 0) bgProcess.$$translatedName = bgProcess.LanguageVariable;
			$scope.types.push(bgProcess);
		})

		$scope.types = _.sortBy($scope.types, [function(o) { return o.Name; }]);

		$scope.activeRow = 0;

		$scope.toggleLogs = function (row) {
			BackgroundProcessesService.getLogs(row.ScheduleId).then(function(logs){
				let t = _.find(BackgroundProcesses, function(o) { return o.Name == row.Name; });
				let nicknamePart = row.Nickname.length ? " (" + row.Nickname + ")" : "";
				let dialParameters = {
					'ScheduleId': row.ScheduleId,
					'Logs': logs,
					'Outcomes': Outcomes,
					'Name': typeof t != 'undefined' ? Translator.translate(t.LanguageVariable) + nicknamePart : "name cannot be found"
				};

				let params = {
					template: 'pages/backgroundProcesses/backgroundProcessesLogs.html',
					controller: 'BackgroundProcessLogsController',
					locals: {
						DialogParameters: dialParameters,
					},
					wider: true
				};
				MessageService.dialogAdvanced(params);
			})
		}

		let forceRefresh = function(){
			ViewService.refresh(StateParameters.ViewId);
		}

		$scope.editTimeSettings = function (group) {
			let dialParameters = {
				'group': group,
				'timeSlotOptions': $scope.timeSlotOptions,
				'timeOptions': $scope.timeOptions,
				'weekDays': $scope.weekDays
			};
			let params = {
				template: 'pages/backgroundProcesses/backgroundProcessesTimeSettings.html',
				controller: 'BackgroundProcessTimeSettingsController',
				locals: {
					DialogParameters: dialParameters,
					ForceRefresh: forceRefresh
				},
			};
			MessageService.dialogAdvanced(params);
		}

		if (typeof InstanceDefaults.InstanceDefaultUser != 'undefined') {
			$scope.instanceDefaults.InstanceDefaultUser = [InstanceDefaults.InstanceDefaultUser];
		}

		$scope.header = {
			title: 'SETUP_BACKGROUNDTASKS'
		}

		$scope.timeSlotOptions = [{name: 'DAILY', value: 1}, {name: 'ONCE_A_WEEK', value: 2}];

		$scope.weekDays = [{name: 'CAL_DAYS_LONG_1', value: 'monday'}, {
			name: 'CAL_DAYS_LONG_2',
			value: 'tuesday'
		}, {name: 'CAL_DAYS_LONG_3', value: 'wednesday'}, {
			name: 'CAL_DAYS_LONG_4',
			value: 'thursday'
		}, {name: 'CAL_DAYS_LONG_5', value: 'friday'}, {
			name: 'CAL_DAYS_LONG_6',
			value: 'saturday'
		}, {name: 'CAL_DAYS_LONG_0', value: 'sunday'}]

		$scope.timeOptions = [];
		for (let i = 1; i < 25; i++) {
			if (i <= 9) {
				$scope.timeOptions.push("0" + i + ":00");
			} else {
				$scope.timeOptions.push(i + ":00");
			}
		}

		$scope.compareOptions = [
			{ value: 0, name: Translator.translate('LIST_COMPARE0') },
			{ value: 1, name: Translator.translate('LIST_COMPARE1') },
			{ value: 2, name: Translator.translate('LIST_COMPARE2') }];

		let changedItems = [];
		$scope.timeSlots = {};
		$scope.groupTimeSlots = {};
		$scope.backgroundProcessRows = BackgroundProcessRows;

		let checkIfConfigurable = function(name){
			if(name == null || typeof  name == 'undefined') return false;
			if(name.length == 0) return false;
			let rootProcess = _.find(BackgroundProcesses, function(o) { return o.Name == name; })
			if(rootProcess && rootProcess.ConfigValues.length) return true;
			return false;
		}

		_.each(BackgroundProcessGroups, function(g){
			_.each(g.Schedules, function(s){
				s.$$configurable =  checkIfConfigurable(s.Name);
			})
		})

		let setSavedTimeSlots = function () {
			_.each($scope.backgroundProcessRows, function (iSchedule) {
				$scope.timeSlots[iSchedule.ScheduleId] = iSchedule.ScheduleOptions;
			});

			_.each(BackgroundProcessGroups, function(grp){
				$scope.groupTimeSlots[grp.GroupId] = grp.ScheduleOptions;
			});
		}
		setSavedTimeSlots();
		$scope.addBackgroundProcess = function (index, groupId) {
			BackgroundProcessesService.addBackgroundProcess(groupId).then(function (newSchedule) {
				newSchedule.$$configurable = false;
				$scope.groups[index].Schedules.push(newSchedule);
			});
		}

		let saveGroup = function(group){
			BackgroundProcessesService.saveGroup(group)
		}

		SaveService.subscribeSave($scope, function () {
			if (changedItems.length > 0) {
				BackgroundProcessesService.saveBackgroundProcess(changedItems).then(function () {
					changedItems = [];
				});
			}
		});

		$scope.toggleDisable = function(row){
			if(row.Disabled == 1) row.Disabled = 0;
			else if(row.Disabled == 0) row.Disabled = 1;
			$scope.edit(row);
		}

		$scope.toggleDisableGroup = function(group){
			if(group.Disabled == 1) group.Disabled = 0;
			else if(group.Disabled == 0) group.Disabled = 1;
			saveGroup(group);
		}

		$scope.groups = BackgroundProcessGroups;

		$scope.addGroup = function(){
			BackgroundProcessesService.addGroup().then(function(g){
				$scope.groups.push(g);
			})
		}

		$scope.forceExecuteGroup = function(group){
			BackgroundProcessesService.forceExecuteGroup(group.Schedules);
		}

		$scope.edit = function (changed) {
			changed.ScheduleOptions = $scope.timeSlots[changed.ScheduleId];
			if (_.findIndex(changedItems, function (o) {
				return o.ScheduleId == changed.ScheduleId;
			}) == -1) {
				changedItems.push(changed);
			}
			changed.$$configurable = checkIfConfigurable(changed.Name);

			if(changed.Name == "AzureADBackgroundProcess" && changed.ConfigOptions == null){
				changed.ConfigOptions = "{\"@AzureAdDomain\":\"default\"}";
			}

			SaveService.tick();
		}
		$scope.deleteBackgroundProcess = function (schedule, index, group) {
			MessageService.confirm("CONFIRM_DELETE", function () {
				BackgroundProcessesService.deleteBackgroundProcess(schedule.ScheduleId).then(function () {
					let grpIndex = _.findIndex($scope.groups, function(o) { return o.GroupId == group.GroupId; });
					$scope.groups[grpIndex].Schedules.splice(index,1)
				});
			}, 'warn');
		}

		let isExecuting = false;
		let executionTimer;
		$scope.forceExecute = function (schedule) {
			if (!schedule || schedule.Name == null || schedule.Name == '') return;

			isExecuting = true;
			executionTimer = setTimeout(function () {
				if (isExecuting) ViewService.lock(StateParameters.ViewId);
			}, 200);

			BackgroundProcessesService.forceExecute(schedule).then(function (o) {
				ViewService.unlock(StateParameters.ViewId);
				isExecuting = false;
			});
		}

		$scope.editConfig = function (row) {

			let dialParameters = {
				'Row': row,
			};

			let params = {
				template: 'pages/backgroundProcesses/backgroundProcessConfig.html',
				controller: 'BackgroundProcessConfigController',
				locals: {
					DialogParameters: dialParameters,
					StateParameters: StateParameters,
					BackgroundProcesses: BackgroundProcesses,
					ConfCollection: row.ConfigOptions != null && row.ConfigOptions.length ? JSON.parse(row.ConfigOptions) : {}
				}
			};
			MessageService.dialogAdvanced(params);
		}

		SaveService.subscribeSave($scope, function () {
			let c = angular.copy($scope.instanceDefaults);
			c.InstanceDefaultUser = c.InstanceDefaultUser[0];
			BackgroundProcessesService.saveInstanceDefaultUser(c);
		});

		$scope.changeDefaultUser = function () {
			SaveService.tick();
		}

		ViewService.footbar(StateParameters.ViewId, {template: 'pages/backgroundProcesses/backgroundProcessFooter.html'});
	}

	BackgroundProcessConfigController.$inject = [
		'$scope',
		'DialogParameters',
		'BackgroundProcesses',
		'MessageService',
		'SaveService',
		'BackgroundProcessesService',
		'ConfCollection',
		'$rootScope',
		'NotificationService'
	];

	function BackgroundProcessConfigController(
		$scope,
		DialogParameters,
		BackgroundProcesses,
		MessageService,
		SaveService,
		BackgroundProcessesService,
		ConfCollection,
		$rootScope,
		NotificationService
	) {
		$scope.userLanguage = $rootScope.clientInformation.locale_id;
		$scope.languages = [];
		$scope.thisValue = BackgroundProcesses[DialogParameters.Row.Name];
		$scope.showLangugeSelection = false;
		NotificationService.getLanguages().then(function(b){
			$scope.languages = b;
		});

		for(let i = 0; i < $scope.thisValue.ConfigValues.length; i++){
			if($scope.thisValue.ConfigValues[i].IgnoreRichText == false && $scope.thisValue.ConfigValues[i].OptionType == 8){
				$scope.showLangugeSelection = true;
				break;
			}
		}

		$scope.row = DialogParameters.Row;
		let setTooltips = function () {
			_.each($scope.thisValue.ConfigValues, function (configRow) {
				if (!configRow.HelpText.length) configRow.HelpText = configRow.Name;
			})
		}
		setTooltips();

		let findIfRichText = function(configPropertyName){
			let confValue = _.find($scope.thisValue.ConfigValues, function(o) { return o.Name == configPropertyName; });
			if(confValue.OptionType == 8 && confValue.IgnoreRichText == false){
				return true
			}
			return false;
		}

		$scope.initializeUserLanguage = function(){
			_.each(ConfCollection, function(confValue, confName){
				if(findIfRichText(confName) != false && !confValue.hasOwnProperty($scope.userLanguage)){
					confValue[$scope.userLanguage] = {};
				}
			})
		}

		$scope.confCollection = ConfCollection;

		//FALLBACK FOR OLD SYNTAX BEFORE THIS RICH TEXT WAS IMPLEMENTED
		if(!_.isEmpty($scope.confCollection)){
			_.each($scope.confCollection, function(value, key){
				if(key == '@HeaderText' || key == '@FooterText') {
					_.each(value, function (value2, key2) {
						if (typeof value2 == 'string') {
							$scope.confCollection[key][key2] = {html: value2, delta: {ops: [{'insert': value2}]}}
						}
					})
				}
			})
		}

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.buildConfig = function () {
			$scope.row.ConfigOptions = JSON.stringify($scope.confCollection);
			BackgroundProcessesService.saveBackgroundProcess([$scope.row]).then(function () {
				MessageService.closeDialog();
			});
		}

		/*Jos meillä on collection, josta halutaan hakea suodatettuna prosessilla, tämä funktio palauttaa ne*/
		$scope.returnOptionValues = function (c) {
			if (!c.UseFilter) {
				return c.Values;
			} else {
				return c.Values[$scope.confCollection["@Process"]];
			}
		}
	}


	BackgroundProcessLogsController.$inject = [
		'$scope',
		'DialogParameters',
		'BackgroundProcessesService',
		'MessageService',
		'CalendarService',
		'Translator'
	];

	function BackgroundProcessLogsController(
		$scope,
		DialogParameters,
		BackgroundProcessesService,
		MessageService,
		CalendarService,
		Translator
	) {

		$scope.formatDate = function(date) {
			return CalendarService.formatDateTime(date);
		}

		$scope.logs = DialogParameters.Logs;
		$scope.headerText = Translator.translate('LAST_25_BG_LOGS') + ' ' + Translator.translate('LAST_25_BG_LOGS_2') + ' //' + DialogParameters.Name;

		$scope.outcomes = DialogParameters.Outcomes;

		$scope.cancel = function() {
			MessageService.closeDialog();
		}
	}


	BackgroundProcessTimeSettingsController.$inject = [
		'$scope',
		'DialogParameters',
		'BackgroundProcessesService',
		'MessageService',
		'ForceRefresh'
	];

	function BackgroundProcessTimeSettingsController(
		$scope,
		DialogParameters,
		BackgroundProcessesService,
		MessageService,
		ForceRefresh
	) {
		$scope.group = DialogParameters.group;
		$scope.timeSlotOptions = DialogParameters.timeSlotOptions;
		$scope.timeOptions = DialogParameters.timeOptions;
		$scope.weekDays = DialogParameters.weekDays;

		$scope.save = function(){
			if($scope.group.Slot == 1 && $scope.group.hasOwnProperty('Weekday')) $scope.group.Weekday = null;
			BackgroundProcessesService.saveGroup($scope.group).then(function(){
				ForceRefresh();
				MessageService.closeDialog();
			});
		}
		$scope.cancel = function(){
			MessageService.closeDialog();
		}

		$scope.delete = function () {
			MessageService.confirm('DELETE_SCHEDULE_GROUP', function () {
				BackgroundProcessesService.deleteGroups([$scope.group]).then(function(){
					ForceRefresh();
				});
			});
		};
	}

})();