﻿(function () {
    'use strict';

    angular
        .module('core.pages')
        .service('ExcelImportService', ExcelImportService);

    ExcelImportService.$inject = ['ApiService', 'AttachmentService'];
    function ExcelImportService(ApiService, AttachmentService) {
        let self = this;
        let ImportExcel = ApiService.v1api('files/itemsExcel');
	    let ImportExcelUpdate = ApiService.v1api('files/itemsExcel/update');
      
        self.getFiles = function(process) {
			return AttachmentService.getControlAttachments(process, 'Excel');
        };

        self.getFile = function (process, group) {
            return AttachmentService.getControlAttachments(process, group);
        };

        self.importFile = function(process, fileId) {
			return ImportExcel.get([process, fileId]);
		};

	    self.importUpdateFile = function(process, fileId) {
		    return ImportExcelUpdate.get([process, fileId]);
	    };

	    self.exportExcelWithPortfolioData = function(process, portfolioId){
		    window.open("files/ItemsExcel/" + process + "/" + portfolioId + "/data");
	    }
    }

})();