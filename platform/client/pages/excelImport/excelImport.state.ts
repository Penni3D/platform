﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(ExcelImportConfig)
		.controller('ExcelImportController', ExcelImportController);

	ExcelImportConfig.$inject = ['ViewsProvider'];

	function ExcelImportConfig(ViewsProvider) {
		ViewsProvider.addView('ExcelImport', {
			controller: 'ExcelImportController',
			template: 'pages/excelImport/excelImport.html',
			resolve: {
				ImportFiles: function (ExcelImportService, CalendarService, $filter, StateParameters) {
					return ExcelImportService.getFiles(StateParameters.process).then(function (files) {
						let filesRetval = [];
						for (let i = 0; i < files.length; i++) {
							files[i].Name = files[i].Name + ' (' + CalendarService.formatDate(files[i].ArchiveStart) + ')';
							filesRetval.push(files[i]);
						}
						filesRetval = $filter('orderBy')(filesRetval, 'ArchiveStart', true);

						return filesRetval;
					});
				}
			}
		});

		ViewsProvider.addPage('ExcelImport', "EXCEL_EXPORT", ['read', 'write']);
	}


	ExcelImportController.$inject = ['$scope', 'ExcelImportService', 'ImportFiles', 'StateParameters', 'ApiService', 'BackgroundProcessesService'];

	function ExcelImportController($scope, ExcelImportService, ImportFiles, StateParameters, ApiService, BackgroundProcessesService) {
		$scope.importFiles = ImportFiles;
		$scope.fileId = 0;
		$scope.filesLoaded = true;
		let Items = ApiService.v1api('meta/Items');

		$scope.process = StateParameters.process;

		$scope.exportTemplate = function () {
			window.open("files/ItemsExcel/" + StateParameters.process);
		};
		$scope.exportTemplateWithD = function(){

			let bgParams = {};
			bgParams["PortfolioId"] = 0;
			bgParams["FileName"] = StateParameters.process + ' - Excel';
			bgParams["Process"] = StateParameters.process;
			bgParams["StrictMode"] = false;
			bgParams["Type"] = 1;

			BackgroundProcessesService.executeOnDemand("GetDataAsExcel", bgParams, 'BG_PROCESS_STARTED');

		};

		$scope.header = {
			title: "Import / Export"
		};

		$scope.filesChanged = function () {
			$scope.filesLoaded = false;
			let oldIds = [];
			for (let i = 0; i < $scope.importFiles.length; i++) {
				oldIds.push($scope.importFiles[i].AttachmentId);
			}

			return ExcelImportService.getFiles(StateParameters.process).then(function (f) {
				let newIds = [];
				for (let i2 = 0; i2 < f.length; i2++) {
					if (oldIds.indexOf(f[i2].AttachmentId) == -1) {
						newIds.push(f[i2].AttachmentId);
					}
				}

				$scope.importFiles = f;
				$scope.filesLoaded = true;

			});
		};

		$scope.importTemplate = function (action) {
			if ($scope.fileId) {
				if (action == "insert") {

					let bgParams = {'q': {}};
					bgParams["PortfolioId"] = 0;
					bgParams["Process"] = StateParameters.process;
					bgParams["FileName"] = StateParameters.process + " portfolio excel";
					bgParams["Type"] = 2;
					bgParams["Insert"] = true;
					bgParams['FileId'] = $scope.fileId;

					BackgroundProcessesService.executeOnDemand("SetDataWithExcel", bgParams, 'BG_PROCESS_STARTED_2');

				}

				if (action == "update") {
					let bgParams = {'Type': 2};
					bgParams['FileId'] = $scope.fileId;
					bgParams['Process'] = StateParameters.process;
					bgParams['PortfolioId'] = 0;
					bgParams['q'] = {}
					bgParams["FileName"] = StateParameters.process + " portfolio excel";
					bgParams["Update"] = true;

					BackgroundProcessesService.executeOnDemand("SetDataWithExcel", bgParams, 'BG_PROCESS_STARTED_2');

				}
			}
		}
	}
})();