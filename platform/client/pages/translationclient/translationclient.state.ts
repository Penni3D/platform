﻿(function () {
    'use strict';

    angular
        .module('core.pages')
        .config(TranslationclientConfig)
        .controller('TranslationclientController', TranslationclientController)
        .controller('InstanceTranslationController', InstanceTranslationController)
        .service('TranslationclientService', TranslationclientService)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start;
                return input.slice(start);
            }
        });

    TranslationclientConfig.$inject = ['ViewsProvider'];
    function TranslationclientConfig(ViewsProvider) {
        ViewsProvider.addView('translationclient', {
            controller: 'TranslationclientController',
            template: 'pages/translationclient/translationclient.html',
            resolve: {
                Translations: function (TranslationclientService) {
                    return TranslationclientService.getTranslations();
                },

                Processes: function (ProcessesService) {
                    return ProcessesService.get();
                },

                InstanceTranslations: function(TranslationclientService){
                    return TranslationclientService.getInstanceTranslations();
                }
            },
                afterResolve: {
                    InitialData: function (Processes, TranslationclientService) {
                        
                        return TranslationclientService.getProcessTranslations(Processes[0].ProcessName);
                    }
                }
        });

        ViewsProvider.addPage('translationclient', 'PAGE_TRANSLATION_EDITOR_CLIENT', ['read', 'write']);
    }

    TranslationclientController.$inject = ['$scope', 'InitialData', 'Processes', 'Translator', 'Common', 'TranslationclientService', 'Translations', 'MessageService', 'ViewService', 'StateParameters', 'SaveService', 'InstanceTranslations'];
    function TranslationclientController($scope, InitialData, Processes, Translator, Common, TranslationclientService, Translations, MessageService, ViewService, StateParameters, SaveService, InstanceTranslations) {

        ViewService.footbar(StateParameters.ViewId, { template: 'pages/translationclient/translationClientFootbar.html', scope: $scope });
        var languageVariables = [];
        $scope.data = Translations;

        $scope.languageVariableArray = [];
        $scope.selectedRows = [];
        $scope.paging = { size: 50, currentPage: 0 };
        $scope.pageSize = 50;
        $scope.currentPage = 0;
        $scope.resultsParPage = Translator.translate('RESULTS_PER_PAGE');


        $scope.numberOfPages = function () {
            return Math.ceil($scope.languageVariableArray.length / $scope.pageSize);
        };



        $scope.header = {
            title: 'TRANSLATION_CLIENT',
            menu: [{
                name: 'SAVE_NEW_LANG_FILE',
                onClick: function () {
                    var newFileSave = function () {
                        TranslationclientService.saveTranslation($scope.data);
                    };

                    MessageService.confirm('LANGUAGE_VARIABLE_SAVE', newFileSave);
                }
            },
            {
                name: 'DELETE',
                onClick: function () {
                    var listValuesForDelete = function () {
                        angular.forEach($scope.selectedRows, function (translation) {
                            deleteLanguageVar(translation);
                        });
                    };

                    var deleteLanguageVar = function (key) {
                        angular.forEach(Translations, function (value) {
                            delete value[key];
                        });

                        newArrayAfterDelete(key);
                        $scope.selectedRows = [];
                    };

                    MessageService.confirm('LANGUAGE_VARIABLE_DELETE', listValuesForDelete);
                }
            }]
        };




        $scope.searchChange = function () {
            $scope.paging.currentPage = 0;
            customFilter();
            $scope.checkSelectedTab(1);
            $scope.processSearchChange();
        };

        $scope.newEmptyRow = function () {
            var data = {
                check: ''
            };

            var modifications = {
                create: false,
                cancel: true
            };

            var onChange = function () {
                modifications.create = !isDuplicateVariable(data.check)
            };

            var content = {
                buttons: [
                    {
                        text: 'CANCEL',
                        modify: 'cancel'

                    },
                    {
                        type: 'primary',
                        modify: 'create',
                        text: 'SAVE',
                        onClick: function () {
                            data.check = data.check.split(' ').join('_');
                            $scope.languageVariableArray.unshift(data.check.toUpperCase());
                        }
                    }
                ],
                inputs: [{ type: 'string', label: 'NEW_LANGUAGE_VARIABLE', model: 'check' }],
                title: 'NEW_LANGUAGE_VARIABLE'
            };

            MessageService.dialog(content, data, onChange, modifications);
        };

        $scope.saveNewRow = function () {
            $scope.languageVariableArray.unshift($scope.newLngVariable.toUpperCase());
            $scope.selectedRow = $scope.languageVariableArray[0];
        };

        $scope.variableChangeDialog = function (keyToChange) {
            var data = {
                check: keyToChange
            };

            var modifications = {
                create: false,
                cancel: true
            };

            var onChange = function () {
                modifications.create = !isDuplicateVariable(data.check)
            };

            var content = {
                buttons: [
                    {
                        text: 'CANCEL',
                        modify: 'cancel'
                    },
                    {
                        type: 'primary',
                        modify: 'create',
                        text: 'CHANGE_VAR_BUTTON',
                        onClick: function () {
                            changeVariableName(keyToChange, data.check.toUpperCase());
                        }
                    }
                ],
                inputs: [{ type: 'string', label: 'NEW_VARIABLE', model: 'check' }],
                title: 'CHANGE_LANGUAGE_VARIABLE'
            };

            MessageService.dialog(content, data, onChange, modifications);
        };

        function isDuplicateVariable(variable) {
            return $scope.languageVariableArray.indexOf(variable.toUpperCase()) > -1
        }

        function changeVariableName(key, newKey) {
            angular.forEach(Translations, function (value) {
                value[newKey] = value[key];
                delete value[key];
            });

            var indexOfKey = $scope.languageVariableArray.indexOf(key);
            $scope.languageVariableArray[indexOfKey] = newKey;
        }

        function newArrayAfterDelete(indexKey) {
            var index = $scope.languageVariableArray.indexOf(indexKey);
            $scope.languageVariableArray.splice(index, 1);
        }

        function updateKeys() {
            var uniqueKeysObject = {};
            angular.forEach(Translations, function (translation) {
                angular.forEach(translation, function (t, key) {
                    uniqueKeysObject[key] = true;
                });
            });

            $scope.languageVariableArray = _.keys(uniqueKeysObject).sort();
            languageVariables = $scope.languageVariableArray;
        }

        function customFilter() {
            if (_.size($scope.search) < 3) {
                $scope.languageVariableArray = languageVariables;
                return;
            }

            var keysInObject = {};
            var searchTextToUpperCase = $scope.search.toUpperCase();
            angular.forEach(Translations, function (translation) {
                angular.forEach(translation, function (value, key) {
                    if (value && value.toUpperCase().indexOf(searchTextToUpperCase) > -1 || key.indexOf(searchTextToUpperCase) > -1) {
                        keysInObject[key] = true;
                    }
                });
            });
            $scope.languageVariableArray = _.keys(keysInObject).sort();
        }
        updateKeys();

        //___________________________________________________________________________________PROCESS_______________________________________________________________________________________________________
        //Process Translation
        $scope.variableTranslationPairs = {};
        $scope.processLanguageVariables = [];
        $scope.processOptions = Processes;
        $scope.processData = InitialData;
        $scope.filterSearch = $scope.processOptions[0].ProcessName;
        //Process Translation variables
        var dataToUpdate = [];
        var newTranslationArray = [];
        
        
        // Sets the languages for the table headers.
        var setLanguages = function () {
            
            _.each($scope.data, function (rows, languageKey) {
                var newStr = languageKey.replace(/_/g, "-");
            });
        };
        setLanguages();

        //Select page contents by selecting a Process from the input                                      
        $scope.processFilter = function () {
            $scope.paging.currentPage = 0;
            processFilterCustom();
        };

        //Filter for PROCESS select input | Gets the processTranslations using filterSearch as a parameter for the query, then next function                                                     
        function processFilterCustom() {
            return TranslationclientService.getProcessTranslations($scope.filterSearch).then(function (processTranslations) {
                $scope.processData = processTranslations;
                setDataVariables();
            });
        }
        //Checks whether the selected tab is the Translations or Process translations and updates the Rows/page accordingly.
        $scope.checkSelectedTab = function (id) {
            if (id == 1) {

                $scope.usedArray = $scope.languageVariableArray.length;
            }
            else if (id == 2) {
                $scope.usedArray = $scope.processLanguageVariables.length;
            }
        }

        //Loops through the data from server and places it into needed variables.                       
        function setDataVariables() {
            $scope.variableTranslationPairs = {};
            $scope.processLanguageVariables = [];
            setLanguages(); 
            angular.forEach($scope.processData, function (variables, language) {
                if (!$scope.variableTranslationPairs[language]) {
                    $scope.variableTranslationPairs[language] = {};
                }
                _.each(variables, function (v) {
                    var newVar = v.Variable;
                    $scope.variableTranslationPairs[language][newVar] = v.Translation;
                    $scope.processLanguageVariables.push(newVar);
                });
            });
            $scope.processLanguageVariables = _.uniq($scope.processLanguageVariables);
            $scope.checkSelectedTab(2);
        }
        setDataVariables();

        //Search tool OnChange | Sets the paging to 0 and next func.
        $scope.processSearchChange = function () {
            $scope.paging.currentPage = 0;
            processSearchFilter();
        };
        //Filters the content using the $scope.search value.
        function processSearchFilter() {
            //Resets the search filter if below 3
            if (_.size($scope.search) < 3) {
                    setDataVariables();
            }
            //the filter
            else {  
                var keysInObject = {};
                var searchText = $scope.search;
                angular.forEach($scope.variableTranslationPairs, function (processTranslation) {
                    angular.forEach(processTranslation, function (value, key) {
                        if ((value && value.toUpperCase().indexOf(searchText) > -1 )|| (key.indexOf(searchText) > -1 )|| key.indexOf(searchText.toUpperCase()) > -1) {
                            keysInObject[key] = true;
                        }
                    });
                });
                $scope.processLanguageVariables = _.keys(keysInObject).sort();
                $scope.checkSelectedTab(2);
            }
        }
        //on-change for saving | first checks if the translation is completely new or just edit
        $scope.changeValue = function (params) {
            var g = checkIfVariable(params.language, params.langVariable);
            params.process = findProcess(params.langVariable);

            if (params.process != 'undefined') {
                var id = findVariableId(params.language, params.langVariable);
            }
            // Update translation
            if (g != -1) {
                  var changed = {
                    'Id': 0,
                    'Translation': ""
                };
                var idOfChanged = id;
                changed['Id'] = id;
                changed['Translation'] = params.translation;
                var index = Common.getIndexOf(dataToUpdate, idOfChanged, 'Id');
                if (typeof index == 'undefined') {
                    dataToUpdate.push(changed);
                } else {
                    dataToUpdate[index] = changed;
                }
                SaveService.tick();
            }
            // New translation
            else { 
                var newTranslationObjects = {
                    'langVariable': "", 
                    'language': "",
                    'translation': "",
                    'process': ""
                };

                newTranslationObjects['langVariable'] = params.langVariable;
                newTranslationObjects['language'] = params.language;
                newTranslationObjects['translation'] = params.translation
                newTranslationObjects['process'] = params.process;
                var index = Common.getIndexOf(newTranslationArray, params.langVariable, 'langVariable');
                if (typeof index == 'undefined') {
                    newTranslationArray.push(newTranslationObjects);
                } else {
                    newTranslationArray[index] = newTranslationObjects;
                }
                SaveService.tick();
            }
        };
        // returns if the processData contains a variable at the desired location
        var checkIfVariable = function (language, variable) {
            return _.findIndex($scope.processData[language], function (o) { return o.Variable == variable; });
        }
        //Match the process with the language variable
        var findProcess = function (languageVariable) {
            var result;
            angular.forEach($scope.processData, function (value, key) {
                angular.forEach(value, function (langRow) {
                    if (langRow.Variable == languageVariable) {
                        result = langRow.Process;
                    }
                });
            });
            return result;
        }
        //Gets the id of the variable.
        var findVariableId = function (language, languageVariable) {
            var id;
            angular.forEach($scope.processData[language], function (rows) {
                if (rows.Variable == languageVariable) {
                    id = rows.ProcessTranslationId;
                }
            });
            return id;
        };
        // Saves new translations or edits to the table.
        SaveService.subscribeSave($scope, function () {
            //Save/Set
            if (dataToUpdate.length > 0) {
                TranslationclientService.saveProcessTranslation(dataToUpdate).then(function () {
                    dataToUpdate = [];
                });
            }
            //New Insert
            if (newTranslationArray.length > 0) {
                TranslationclientService.newProcessTranslation(newTranslationArray).then(function (data) {
                    insertFunction(data);
                });
                var insertFunction = function (data2) {
                    angular.forEach(data2, function (value) {
                        $scope.processData[value.Language].push(value);
                    });
                    newTranslationArray = [];
                };
            }
        });

        let refreshTranslations = function(){
            TranslationclientService.getInstanceTranslations().then(function(trans){
                InstanceTranslations = trans;
            });
        };

        $scope.editProcessTranslation = function (languageVariable) {

            StateParameters.$$languageVariable = languageVariable;

            let params = {
                template: 'pages/translationclient/instanceTranslation.html',
                controller: 'InstanceTranslationController',
                locals: {
                    StateParameters: StateParameters,
                    InstanceTranslations: InstanceTranslations,
                    RefreshTranslations: refreshTranslations
                }
            };

            MessageService.dialogAdvanced(params);
        };
}

    InstanceTranslationController.$inject = ['$scope', 'StateParameters', 'InstanceTranslations', 'MessageService', '$rootScope', 'TranslationclientService', 'RefreshTranslations'];
    function InstanceTranslationController($scope, StateParameters, InstanceTranslations, MessageService, $rootScope, TranslationclientService, RefreshTranslations) {

        $scope.tkey = StateParameters.$$languageVariable;
        let changedTranslations = [];

        $scope.languages = $rootScope.languages;

        $scope.data = [];

        _.each($scope.languages, function(lang){

            let t = _.find(InstanceTranslations, function(xp){
                return xp.Code == lang.value && xp.Variable == $scope.tkey;
            });

            if(typeof t != 'undefined'){
                $scope.data.push(t);
            } else {
                $scope.data.push({Variable: $scope.tkey, Translation: "", Code: lang.value})
            }
        });

        $scope.changeTranslation = function(d){
            let index = _.findIndex(changedTranslations, function(c) { return c.Code == d.Code; });
            if(index == -1){
                changedTranslations.push(d);
            }
        };

        $scope.instanceTranslations = InstanceTranslations;

        $scope.cancel = function () {
            MessageService.closeDialog();
        };

        $scope.save = function(){
            if(changedTranslations.length > 0){
                TranslationclientService.saveInstanceTranslations(changedTranslations).then(function(){
                    changedTranslations = [];
                    MessageService.closeDialog();
                    RefreshTranslations();
                });
            }

        }
    }

    TranslationclientService.$inject = ['ApiService'];
    function TranslationclientService(ApiService) {
        var self = this;
        var Translations = ApiService.v1api('language/TranslationClientRest');
        //save
        self.saveProcessTranslation = function (saveData) {            
            return Translations.save(['save'], saveData);
        };

        self.saveInstanceTranslations = function(saveData){
            return Translations.save("", saveData);
        };

        //new insertion, also returns the new process_translation_id for future edits.
        self.newProcessTranslation = function (newTranslation) {
            var dataArray = [];
            angular.forEach(newTranslation, function (key, value) {
            var dataToSave = {};
                dataToSave["langVariable"] = key.langVariable;
                dataToSave["language"] = key.language;
                dataToSave["translation"] = key.translation;
                dataToSave["process"] = key.process
                dataArray.push(dataToSave);
            });
            return Translations.new(['new'], dataArray).then(function (newData) { return newData.data;  });
        };


        self.saveTranslation = function(data){
            return Translations.new("",data).then(function(t){
               return t.data;
            });
        };
        //PROCESS get
        self.getProcessTranslations = function (process) {
            return Translations.get([process]).then(function (t) {
                
                return t.data;
            });
        };
        //TRANSLATIONS get
        self.getTranslations = function () {
            return Translations.get().then(function (t) {
                return t.data;
            });
        };

        self.getInstanceTranslations = function () {
            return Translations.get('instance').then(function (t) {
                return t.data;
            });
        };
    }
})();