(function(){
	'use strict';
	angular
		.module('core.pages')
		.config(SimpleAllocationsConfig)
		.controller('SimpleAllocationsController', SimpleAllocationsController)
		.service('SimpleAllocationsService', SimpleAllocationsService);

	SimpleAllocationsConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
    function SimpleAllocationsConfig(ViewsProvider, ViewConfiguratorProvider) {

        ViewsProvider.addView('simple_allocations', {
		    controller: 'SimpleAllocationsController',
            template: 'pages/simpleAllocations/simpleAllocations.html',
            resolve: {
                States: function (ViewConfigurator, StateParameters) {
                    return ViewConfigurator.getRights('simple_allocations', StateParameters.NavigationMenuId);
                },
		        WorkCategoryList: function (ListsService, StateParameters, $q) {
		            if (StateParameters.WorkCategoryList) {
                        return ListsService.getProcessItems(StateParameters.WorkCategoryList, 'order');
                    }
                    else {
		                return  $q.when([]);
                    }
                },
                Sums: function (SimpleAllocationsService, StateParameters) {
                    return SimpleAllocationsService.getSums(0, new Date().getFullYear(),'_', '0');
                },
                SavedSettings: function (PortfolioService, StateParameters) {
                    return PortfolioService.getSavedSettings('SimpleAllocations');
                }
            }
	    });
	    
        ViewsProvider.addPage('simple_allocations', 'PAGE_SIMPLE_ALLOCATIONS', ['read', 'write', 'delete', 'admin']);
		
        ViewConfiguratorProvider.addConfiguration('simple_allocations', {
            global: true,
            onSetup: {
                Processes: function (ProcessesService) {
                    return ProcessesService.get();
                },
                Lists: function (SettingsService) {
                    return SettingsService.ListProcesses();
                },
                BaseCalendars: function (CalendarService) {
                    return CalendarService.getBaseCalendars();
                },
                ProcessPortfolios: function (PortfolioService, StateParameters) {
                    return PortfolioService.getPortfolios(StateParameters.Process);
                },
                UserPortfolios: function (PortfolioService) {
                    return PortfolioService.getPortfolios('user');
                },
                ComptencyPortfolios: function (PortfolioService) {
                    return PortfolioService.getPortfolios('competency');
                }
            },
            defaults: {
                UserMode: 0,
                DateMode: 0,
                SumMode: 0,
                ShowWorkCategoryTotals: 0,
                LockHistory: 0,
                AllowMove: 0
            },
            tabs: {
                default: {
                    Process: function (resolves) {
                        return {
                            label: 'SIMPLE_ALLOCATIONS_PARENT_PROCESS',
                            type: 'select',
                            options: resolves.Processes,
                            optionValue: 'ProcessName',
                            optionName: 'LanguageTranslation'
                        };
                    },
                    ProcessTitle: function () {
                        return {
                            label: 'SIMPLE_ALLOCATIONS_PARENT_PROCESS_TITLE',
                            type: 'translation'
                        };
                    },
                    ProcessPortfolioId: function (resolves) {
                        return {
                            label: 'SIMPLE_ALLOCATIONS_PROCESS_PORTFOLIO',
                            type: 'select',
                            options: resolves.ProcessPortfolios,
                            optionValue: 'ProcessPortfolioId',
                            optionName: 'LanguageTranslation'
                        };
                    },
                    WorkCategoryList: function (resolves) {
                        return {
                            label: 'WORK_CATEGORY_LIST',
                            type: 'select',
                            options: resolves.Lists,
                            optionValue: 'ProcessName',
							optionName: 'LanguageTranslation'
                        };
                    },
                    UserMode: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_COMPETENCY', value: 0 }, { name: 'SIMPLE_ALLOCATION_USER', value: 1 }],
                            label: 'SIMPLE_ALLOCATION_USERMODE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    UserPortfolioId: function (resolves) {
                        return {
                            label: 'SIMPLE_ALLOCATIONS_USER_PORTFOLIO',
                            type: 'select',
                            options: resolves.UserPortfolios,
                            optionValue: 'ProcessPortfolioId',
                            optionName: 'LanguageTranslation'
                        };
                    },
                    CompetencyPortfolio: function (resolves) {
                        return {
                            label: 'SIMPLE_ALLOCATIONS_COMPETENCY_PORTFOLIO',
                            type: 'select',
                            options: resolves.CompetencyPortfolios,
                            optionValue: 'ProcessPortfolioId',
                            optionName: 'LanguageTranslation'
                        };
                    },
                    BaseCalendarId: function (resolves) {
                        return {
                            label: 'SIMPLE_ALLOCATION_BASECALENDAR',
                            type: 'select',
                            options: resolves.BaseCalendars,
                            optionValue: 'CalendarId',
                            optionName: 'CalendarName'
                        };
                    },
                    DateMode: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_MONTH_MODE', value: 0 }, { name: 'SIMPLE_ALLOCATION_WEEK_MODE', value: 1 }],
                            label: 'SIMPLE_ALLOCATION_DATE_MODE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    SumMode: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_SUM_BOTH', value: 0 },
                            { name: 'SIMPLE_ALLOCATION_SUM_TOTAL', value: 1 },
                            { name: 'SIMPLE_ALLOCATION_SUM_YEAR', value: 2 },
                            { name: 'SIMPLE_ALLOCATION_SUM_NONE', value: 3 }],
                            label: 'SIMPLE_ALLOCATION_SUM_MODE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    ShowWorkCategoryTotals: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_SHOW_WORK_CATEGORY_TOTALS_YES', value: 0 },
                                { name: 'SIMPLE_ALLOCATION_SHOW_WORK_CATEGORY_TOTALS_NO', value: 1 }],
                            label: 'SIMPLE_ALLOCATION_SHOW_WORK_CATEGORY_TOTALS',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    LockHistory: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'FALSE', value: 0 }, { name: 'TRUE', value: 1 }],
                            label: 'ALLOCATION_SIMPLE_LOCK_HISTORY',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    AllowMove: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'ALLOCATION_SIMPLE_ALLOW_MOVE_0', value: 0 }
                                    , { name: 'ALLOCATION_SIMPLE_ALLOW_MOVE_1', value: 1 }
                                    , { name: 'ALLOCATION_SIMPLE_ALLOW_MOVE_2', value: 2 }],
                            label: 'ALLOCATION_SIMPLE_ALLOW_MOVE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    processWidth: function () {
                        return {
                            type: 'integer',
                            label: "SIMPLE_ALLOCATIONS_PARENT_PROCESS_WIDTH"
                        };
                    },
                    titleWidth: function () {
                        return {
                            type: 'integer',
                            label: "SIMPLE_ALLOCATION_TITLE_WIDTH"
                        };
                    },
                    workCategoryWidth: function () {
                        return {
                            type: 'integer',
                            label: "SIMPLE_ALLOCATION_WORK_CATEGORY_WIDTH"
                        };
                    }
                }
            }
        });
	}
	
    SimpleAllocationsController.$inject = ['$scope', 'States', 'SimpleAllocationsService', 'SaveService', 'Common', 'MessageService', 'StateParameters', 'WorkCategoryList', 'CalendarService', '$q', 'Sums', 'ViewService', 'Translator', 'UserService', 'PortfolioService', 'SavedSettings', 'SidenavService', 'ProcessService'];
    function SimpleAllocationsController($scope, States, SimpleAllocationsService, SaveService, Common, MessageService, StateParameters, WorkCategoryList, CalendarService, $q, Sums, ViewService, Translator, UserService, PortfolioService, SavedSettings, SidenavService, ProcessService) {
        $scope.WriteRights = false;
        if (States.indexOf("write") > -1) {
            $scope.WriteRights = true;
        }

        $scope.DeleteRights = false;
        if (States.indexOf("delete") > -1) {
            $scope.DeleteRights = true;
        }

        $scope.AdminRights = false;
        if (States.indexOf("admin") > -1) {
            $scope.AdminRights = true;
        }

        $scope.parentProcess = StateParameters.Process;
        $scope.parentProcessTitle = Translator.translation(StateParameters.ProcessTitle);

        $scope.process_portfolio_id = StateParameters.ProcessPortfolioId;
        $scope.user_portfolio_id = StateParameters.UserPortfolioId;
        $scope.competency_portfolio_id = StateParameters.CompetencyPortfolio;

        $scope.workCategories = WorkCategoryList;
        $scope.rows = {};
        $scope.year = new Date().getFullYear();
        $scope.selectedRowsAll = [];
        $scope.selectedRows = [];
        $scope.months = [];
        $scope.allowYearChange = true;
        $scope.sums = Sums;

        $scope.filter_item_ids = [];
        $scope.filter_item_ids_loaded = false

        $scope.resource_name = [];
        $scope.process_name = [];

        if (angular.isDefined(StateParameters.UserMode)) {
            $scope.usermode = StateParameters.UserMode;
        }
        else {
            $scope.usermode = 0;
        }

        if ($scope.usermode == 0) {
            $scope.del_resource_tooltip = Translator.translation('ALLOCATION_SIMPLE_DELETE_COMPETENCY');
        }
        else {
            $scope.del_resource_tooltip = Translator.translation('ALLOCATION_SIMPLE_DELETE_USER');
		}

        if (angular.isDefined(StateParameters.BaseCalendarId)) {
            $scope.baseCalendar = StateParameters.BaseCalendarId;
        }
        else {
            $scope.baseCalendar = 1;
        }

        if (angular.isDefined(StateParameters.DateMode)) {
            $scope.dateMode = StateParameters.DateMode;
        }
        else {
            $scope.dateMode = 0;
        }
        
        if (angular.isDefined(StateParameters.SumMode)) {
            $scope.sumMode = StateParameters.SumMode;
        }
        else {
            $scope.sumMode = 0;
        }

        if (angular.isDefined(StateParameters.ShowWorkCategoryTotals) && StateParameters.ShowWorkCategoryTotals == 1) {
            $scope.ShowWorkCategoryTotals = false;
        }
        else {
            $scope.ShowWorkCategoryTotals = true;
        }

        if (angular.isDefined(StateParameters.LockHistory)) {
            $scope.lockHistory = StateParameters.LockHistory;
        }
        else {
            $scope.lockHistory = 0;
        }

        if (angular.isDefined(StateParameters.AllowMove)) {
            $scope.allowMove = StateParameters.AllowMove;
        }
        else {
            $scope.allowMove = 0;
        }

        if (angular.isDefined(StateParameters.processWidth)) {
            $scope.processWidth = StateParameters.processWidth;
        }
        else {
            $scope.processWidth = 200;
        }

        if (angular.isDefined(StateParameters.titleWidth)) {
            $scope.titleWidth = StateParameters.titleWidth;
        }
        else {
            $scope.titleWidth = 150;
        }

        if (angular.isDefined(StateParameters.workCategoryWidth)) {
            $scope.workCategoryWidth = StateParameters.workCategoryWidth;
        }
        else {
            $scope.workCategoryWidth = 150;
        }

        $scope.checkBoxVisible = function (edit) {
            if (edit && ($scope.DeleteRights || ($scope.WriteRights && $scope.allowMove > 0))) {
                return true;
            }
            else {
                return false;
            }
        };

        $scope.lock_month = 0;

        let checkMonthLock = function () {
            if ($scope.lockHistory === 1) {
                if (StateParameters.DateMode == 1) {
                    if ($scope.year < new Date().getUTCFullYear()) {
                        $scope.lock_month = 53;
                    }
                    else if ($scope.year === new Date().getUTCFullYear()) {
                        $scope.lock_month = moment().isoWeek() - 1;
                    }
                    else {
                        $scope.lock_month = 0;
                    }
                }
                else {
                    if ($scope.year < new Date().getUTCFullYear()) {
                        $scope.lock_month = 12;
                    }
                    else if ($scope.year === new Date().getUTCFullYear()) {
                        $scope.lock_month = new Date().getUTCMonth();
                    }
                    else {
                        $scope.lock_month = 0;
                    }
				}
            }
        }
        checkMonthLock();

        let fetchedYears = [];
        $scope.workingDays = {};
        $scope.workingDaysInMonth = {};
        let changedItems = [];
        let newValuesArray = [];
        let changedRows = [];
        
        $scope.quarter = moment().quarter();

        let initData = function() {
            $scope.months = [];

            if (StateParameters.DateMode == 1) {
                let quarter_start;
                let quarter_end;
                if ($scope.quarter == 1) {
                    quarter_start = moment($scope.year + '-01-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-03-31', "YYYY-MM-DD");
                }
                else if ($scope.quarter == 2) {
                    quarter_start = moment($scope.year + '-04-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-06-30', "YYYY-MM-DD");
				}
                else if ($scope.quarter == 3) {
                    quarter_start = moment($scope.year + '-07-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-09-30', "YYYY-MM-DD");
                }
                else {
                    quarter_start = moment($scope.year + '-10-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-12-31', "YYYY-MM-DD");
                }
                quarter_start.startOf('isoWeek').add(3, 'days');
                quarter_end.startOf('isoWeek').add(3, 'days');

                let quarter_date = quarter_start;
                while (quarter_date <= quarter_end) {
                    if (quarter_date.quarter() == $scope.quarter) {
                        let week = quarter_date.isoWeek()
                        $scope.months.push(week);
                    }
                    quarter_date.add(7, 'days');
				}
            }
            else {
                for (let i = 1; i <= 12; i++) {
                    $scope.months.push(i);
                }
            }
        }

        let initWorkDays = function (user_item_id) {
            if (StateParameters.DateMode == 1) {
                let quarter_start = moment($scope.year + '-01-01', "YYYY-MM-DD").startOf('isoWeek').add(3, 'days');
                let quarter_end = moment($scope.year + '-12-31', "YYYY-MM-DD").startOf('isoWeek').add(3, 'days');
                while (quarter_start <= quarter_end) {
                    let week = quarter_start.isoWeek();
                    if (typeof $scope.workingDaysInMonth[user_item_id] === 'undefined') {
                        $scope.workingDaysInMonth[user_item_id] = {};
                    }
                    if (typeof $scope.workingDaysInMonth[user_item_id][$scope.year] === 'undefined') {
                        $scope.workingDaysInMonth[user_item_id][$scope.year] = {};
                    }
                    $scope.workingDaysInMonth[user_item_id][$scope.year][week] = getWeekWorkDays(user_item_id, week).length;
                    quarter_start.add(7, 'days');
                }
            }
            else {
                for (let m = 1; m <= 12; m++) {
                    if ($scope.workingDays) {
                        if (typeof $scope.workingDaysInMonth[user_item_id] === 'undefined') {
                            $scope.workingDaysInMonth[user_item_id] = {};
                        }
                        if (typeof $scope.workingDaysInMonth[user_item_id][$scope.year] === 'undefined') {
                            $scope.workingDaysInMonth[user_item_id][$scope.year] = {};
                        }
                        $scope.workingDaysInMonth[user_item_id][$scope.year][m] = $scope.workingDays[user_item_id][$scope.year][m].length;
                    }
                }
            }
        }

        let getAllocations = function (resource_item_ids) {
            let filter_item_ids = '0';
            if (resource_item_ids == null) {
                if ($scope.AdminRights) {
                    if (!$scope.filter_item_ids_loaded) {
                        let settings = PortfolioService.getActiveSettings('SimpleAllocations');
                        if (settings.simple_allocations_filter_item_ids) {
                            $scope.filter_item_ids = settings.simple_allocations_filter_item_ids;
                        }
                    }
                    angular.forEach($scope.filter_item_ids, function (value) {
                        filter_item_ids += ',' + value;
                    });
                }
                else {
                    filter_item_ids = UserService.getCurrentUserId();
                    $scope.filter_item_ids = [filter_item_ids];
			    }

                $scope.rows[$scope.year] = {};
            }
            else {
                angular.forEach(resource_item_ids, function (value) {
                    filter_item_ids += ',' + value;
                });
			}

            SimpleAllocationsService.getAllocations(0, $scope.year, StateParameters.DateMode, StateParameters.Process, filter_item_ids, StateParameters.ProcessPortfolioId).then(function (response) {
                angular.forEach($scope.filter_item_ids, function (filter_item_id) {
                    if (typeof $scope.rows[$scope.year][filter_item_id] === 'undefined') {
                        $scope.rows[$scope.year][filter_item_id] = [];
                    }

                    if (typeof $scope.resource_name[filter_item_id] === 'undefined') {
                        ProcessService.getProcess(filter_item_id).then(function (process) {
                            ProcessService.getItem(process, filter_item_id).then(function (item) {
                                $scope.resource_name[filter_item_id] = item['primary'];
                            });
                        });
                    }
                });

                angular.forEach(response.allocations, function (key) {
                    if (key.resource_item_id > 0) {
                        key.resource_item_id = [key.resource_item_id];
                    }
                    else {
                        key.resource_item_id = [];
                    }

                    if (typeof $scope.rows[$scope.year][key.resource_item_id] === 'undefined') {
                        $scope.rows[$scope.year][key.resource_item_id] = [];
                    }

                    $scope.rows[$scope.year][key.resource_item_id].push(key);

                    if (typeof $scope.process_name[key.process_item_id] === 'undefined') {
                        ProcessService.getProcess(key.process_item_id).then(function (process) {
                            ProcessService.getItem(process, key.process_item_id).then(function (item) {
                                $scope.process_name[key.process_item_id] = item['primary'];
                            });
                        });
                    }
                });

                $scope.calculateMonthTotal();

                getCalendarEvents();
            });
        }
        
        let getCalendarEvents = function () {
            let year_start = $scope.year;
            let year_end = $scope.year;
            if (StateParameters.DateMode == 1) {
                year_start--;
                year_end++;
			}
            if (fetchedYears.indexOf('0_' + $scope.year) === -1) {
                let sd = new Date(year_start + '-01-01');
                let ed = new Date(year_end + '-12-31');
                fetchedYears.push('0_' + $scope.year);

                CalendarService.getBaseCalendar($scope.baseCalendar, sd, ed).then(function (dates) {
                    if (typeof $scope.workingDays[0] === 'undefined') {
                        $scope.workingDays[0] = {};
                    }

                    for (let year = year_start; year <= year_end; year++) {
                        let yearCalendar = dates[year];

                        if (typeof $scope.workingDays[0][year] === 'undefined') {
                            $scope.workingDays[0][year] = {};

                            angular.forEach(yearCalendar, function (month, m) {
                                if (typeof $scope.workingDays[0][year][m] === 'undefined') {
                                    $scope.workingDays[0][year][m] = [];

                                    angular.forEach(month, function (day, d) {
                                        if (Common.isFalse(day.notWork)) {
                                            $scope.workingDays[0][year][m].push({ index: d });
                                        }
                                    });
                                }
                            });
                        }
                    }

                    initWorkDays(0);
                });
            }

            if ($scope.usermode === 1) {
                angular.forEach($scope.rows[$scope.year], function (row, user_item_id) {
                    if (fetchedYears.indexOf(user_item_id + '_' + $scope.year) === -1) {
                        let sd = new Date(year_start + '-01-01');
                        let ed = new Date(year_end + '-12-31');
                        fetchedYears.push(user_item_id + '_' + $scope.year);

                        CalendarService.getUserCalendar(user_item_id, sd, ed).then(function (dates) {
                            if (typeof $scope.workingDays[user_item_id] === 'undefined') {
                                $scope.workingDays[user_item_id] = {};
                            }

                            for (let year = year_start; year <= year_end; year++) {
                                let yearCalendar = dates[year];

                                if (typeof $scope.workingDays[user_item_id][year] === 'undefined') {
                                    $scope.workingDays[user_item_id][year] = {};

                                    angular.forEach(yearCalendar, function (month, m) {
                                        if (typeof $scope.workingDays[user_item_id][year][m] === 'undefined') {
                                            $scope.workingDays[user_item_id][year][m] = [];

                                            angular.forEach(month, function (day, d) {
                                                if (Common.isFalse(day.notWork)) {
                                                    $scope.workingDays[user_item_id][year][m].push({ index: d });
                                                }
                                            });
                                        }
                                    });
                                }
                            }

                            initWorkDays(user_item_id);
                        });
                    }
                });
            }
        };

        $scope.totals = {};

        $scope.selectAllRows = function (resource_item_id) {
            _.each($scope.rows[$scope.year][resource_item_id], function (row) {
                if (row.edit) {
                    if (typeof $scope.selectedRowsAll[resource_item_id] === 'undefined') {
                        $scope.selectedRowsAll[resource_item_id] = [];
                    }
                    if (typeof $scope.selectedRows[resource_item_id] === 'undefined') {
                        $scope.selectedRows[resource_item_id] = [];
                    }

                    if ($scope.selectedRowsAll[resource_item_id]) {
                        $scope.selectedRows[resource_item_id][row.item_id] = $scope.selectedRowsAll[resource_item_id];
                    } else {
                        delete $scope.selectedRows[resource_item_id][row.item_id];
                    }
                }
            });
        };

        let addAllocation_data = { resource_item_id: 0, process_item_id: [] };

        $scope.addAllocation = function (resource_item_id) {
            let content = {
                title: 'ALLOCATION_SIMPLE_ADD_TITLE',
                inputs: [{ type: 'process', label: $scope.parentProcessTitle, model: 'process_item_id', process: $scope.parentProcess, max: 1 }],
                buttons: [{ text: 'CANCEL' }, { type: 'primary', onClick: addAllocation, text: 'ALLOCATION_SIMPLE_ADD_BUTTON' }]
            };

            addAllocation_data = { resource_item_id: resource_item_id, process_item_id: [] };

            MessageService.dialog(content, addAllocation_data);
        };

        let addAllocation = function () {
            if (addAllocation_data.process_item_id.length == 0) {
                MessageService.msgBox('ALLOCATION_SIMPLE_ADD_ERROR_NO_PROCESS').then(function () {
                    $scope.addAllocation(addAllocation_data.resource_item_id);
                });
            }
            else {
                let process_item_id = addAllocation_data.process_item_id[0]; 

                if (angular.isUndefined($scope.rows[$scope.year])) {
                    $scope.rows[$scope.year] = {};
                }

                if (angular.isUndefined($scope.totals[addAllocation_data.resource_item_id]['item'][$scope.year])) {
                    $scope.totals[addAllocation_data.resource_item_id]['item'][$scope.year] = {};
                }

                let allocation = {
                    process_item_id: process_item_id,
                    title: 'New allocation',
                    resource_type: '',
                    resource_item_id: addAllocation_data.resource_item_id,
                    user_item_id: 0
                };

                if ($scope.usermode == 0) {
                    allocation.resource_type = 'competency';
                }
                else {
                    allocation.resource_type = 'user';
                }

                return SimpleAllocationsService.insertAllocation(allocation).then(function (new_data) {
                    if (new_data.resource_item_id > 0) {
                        new_data.resource_item_id = [new_data.resource_item_id];
                    }
                    else {
                        new_data.resource_item_id = [];
                    }

                    if (typeof $scope.rows[$scope.year][new_data.resource_item_id] === 'undefined') {
                        $scope.rows[$scope.year][new_data.resource_item_id] = [];
                    }

                    $scope.rows[$scope.year][new_data.resource_item_id].push(new_data);

                    $scope.process_name[new_data.process_item_id] = new_data.project_name;
                });
            }
        }

        $scope.moveAllocation = function () {
            let move = function () {
                let shifts = [];
                angular.forEach($scope.filter_item_ids, function (filter_item_id) {
                    angular.forEach($scope.selectedRows[filter_item_id], function (value, item_id) {
                        if (value) {
                            let index = -1;
                            angular.forEach($scope.rows[$scope.year][filter_item_id], function (row, ind) {
                                if (row.item_id == item_id) {
                                    index = ind;
                                }
                            });
                            let shift = {
                                process_item_id: $scope.rows[$scope.year][filter_item_id][index].process_item_id,
                                date_mode: $scope.dateMode,
                                shift: data.shift,
                                move_history: data.history[0],
                                allocation_item_id: item_id
                            };
                            shifts.push(shift);
                        }
                    });
                });
                let filter_item_ids = '0';
                angular.forEach($scope.filter_item_ids, function (value) {
                    filter_item_ids += ',' + value;
                });
                SimpleAllocationsService.moveAllocations(0, $scope.year, StateParameters.DateMode, StateParameters.Process, filter_item_ids, StateParameters.ProcessPortfolioId, shifts).then(function (response) {
                    angular.forEach(response.allocations, function (key) {
                        if (key.resource_item_id > 0) {
                            key.resource_item_id = [key.resource_item_id];
                        }
                        else {
                            key.resource_item_id = [];
                        }

                        let index = -1;
                        angular.forEach($scope.rows[$scope.year][key.resource_item_id], function (row, ind) {
                            if (row.item_id == key.item_id) {
                                index = ind;
                            }
                        });

                        $scope.rows[$scope.year][key.resource_item_id][index] = key;
                    });

                    $scope.calculateMonthTotal();

                    getCalendarEvents();
                });
            };

            let shift_label = '';
            if ($scope.dateMode === 0) {
                shift_label = 'ALLOCATION_SIMPLE_MOVE_MONTHS';
            }
            else {
                shift_label = 'ALLOCATION_SIMPLE_MOVE_WEEKS';
            }

            let content = {
                title: 'ALLOCATION_SIMPLE_MOVE_TITLE',
                inputs: [{ type: 'integer', label: shift_label, model: 'shift' }],
                buttons: [{ text: 'CANCEL' }, { type: 'primary', onClick: move, text: 'ALLOCATION_SIMPLE_MOVE_BUTTON' }]
            };

            let history_value = [0];

            if ($scope.allowMove === 1) {
                history_value = [1];
                let history_button = { type: 'select', label: 'ALLOCATION_SIMPLE_MOVE_HISTORY', model: 'history', options: [{ name: 'FALSE', value: 0 }, { name: 'TRUE', value: 1 }], optionName: 'name', optionValue: 'value' };
                content.inputs.push(history_button);
            }

            let data = { shift: 0, history: history_value };

            MessageService.dialog(content, data);
        };

        $scope.deleteAllocation = function () {
            MessageService.confirm('ALLOCATION_SIMPLE_DELETE_CONFIRM', function () {
                let ids = [];
                angular.forEach($scope.filter_item_ids, function (filter_item_id) {
                    angular.forEach($scope.selectedRows[filter_item_id], function (value, item_id) {
                        if (value) ids.push(item_id);
                    });
                });
                let deleteIds = Common.convertArraytoString(ids);
                SimpleAllocationsService.deleteAllocation(deleteIds).then(function () {
                    angular.forEach($scope.filter_item_ids, function (filter_item_id) {
                        angular.forEach($scope.selectedRows[filter_item_id], function (value, item_id) {
                            if (value) {
                                angular.forEach($scope.rows[$scope.year][filter_item_id], function (row, index) {
                                    if (row.item_id == item_id) {
                                        $scope.rows[$scope.year][filter_item_id].splice(index, 1);
									}
                                });
                            }
                        });
                        delete $scope.selectedRowsAll[filter_item_id];
                        $scope.selectedRows[filter_item_id] = [];
                    });
                    $scope.calculateMonthTotal();
                });
            });
        };

        $scope.hasSelectedRows = function () {
            let result = false;
            if ($scope.DeleteRights || ($scope.WriteRights && $scope.allowMove > 0)) {
                angular.forEach($scope.filter_item_ids, function (filter_item_id) {
                    angular.forEach($scope.selectedRows[filter_item_id], function (value) {
                        if (value) {
                            result = true;
                        }
                    });
                });
            }
            return result;
        };

	    $scope.changeYear = function (type) {
	        if (!$scope.allowYearChange) {
	            return;
            }
            if (type == '-') {
                $scope.year -= 1;
            }
            else if (type == '+') {
                $scope.year += 1;
            }

            if (StateParameters.DateMode == 1 || $scope.months.length == 0) {
                initData();
            }

            getAllocations(null);

            if (StateParameters.DateMode == 1) {
                let filter_item_ids = '0';
                angular.forEach($scope.filter_item_ids, function (value) {
                    filter_item_ids += ',' + value;
                });
                SimpleAllocationsService.getSums(0, $scope.year, StateParameters.Process, filter_item_ids).then(function (sums) {
                    $scope.sums = sums;
                });
            }

			getCalendarEvents();

            checkMonthLock();

            $scope.calculateMonthTotal();
        };

        $scope.changeQuarter = function (type) {
            if (!$scope.allowYearChange) {
                return;
            }
            if (type == '-') {
                $scope.quarter -= 1;
            }
            else if (type == '+') {
                $scope.quarter += 1;
            }

            let year_changed = false;
            if ($scope.quarter == 0) {
                $scope.year--;
                $scope.quarter = 4;
                year_changed = true;
            }
            if ($scope.quarter == 5) {
                $scope.year++;
                $scope.quarter = 1;
                year_changed = true;
            }

            initData();
            
            if (year_changed) {
                getAllocations(null);

                if (StateParameters.DateMode == 1) {
                    let filter_item_ids = '0';
                    angular.forEach($scope.filter_item_ids, function (value) {
                        filter_item_ids += ',' + value;
                    });
                    SimpleAllocationsService.getSums(0, $scope.year, StateParameters.Process, filter_item_ids).then(function (sums) {
                        $scope.sums = sums;
                    });
                }
            }

            getCalendarEvents();

            checkMonthLock();

            $scope.calculateMonthTotal();
        };

        $scope.workCategoriesSelected = [];

        $scope.calculateMonthTotal = function (params) {
            $scope.allowYearChange = false;

            $scope.workCategoriesSelected = [];
            $scope.workCategoryTotals = [];
            $scope.workCategoryTotals2 = [];

            angular.forEach($scope.filter_item_ids, function (filter_item_id) {
                if (angular.isUndefined($scope.totals[filter_item_id])) $scope.totals[filter_item_id] = {};

                $scope.totals[filter_item_id]['item'] = {};
                $scope.totals[filter_item_id]['total'] = {};
                $scope.totals[filter_item_id]['total2'] = {};

                let workCategoryTotals = {};
                let workCategoryTotals2 = {};

                $scope.workCategoriesSelected[filter_item_id] = [];

                angular.forEach($scope.rows[$scope.year][filter_item_id], function (key) {

                    if (angular.isUndefined($scope.totals[filter_item_id]['item'][$scope.year])) $scope.totals[filter_item_id]['item'][$scope.year] = {};
                    if (angular.isUndefined($scope.totals[filter_item_id]['item'][$scope.year][key.item_id])) $scope.totals[filter_item_id]['item'][$scope.year][key.item_id] = 0;
                    if (angular.isUndefined($scope.totals[filter_item_id]['total'][$scope.year])) $scope.totals[filter_item_id]['total'][$scope.year] = {};
                    if (angular.isUndefined($scope.totals[filter_item_id]['total'][$scope.year]['total'])) $scope.totals[filter_item_id]['total'][$scope.year]['total'] = 0;
                    if (angular.isUndefined($scope.totals[filter_item_id]['total2'][$scope.year])) $scope.totals[filter_item_id]['total2'][$scope.year] = {};
                    if (angular.isUndefined($scope.totals[filter_item_id]['total2'][$scope.year]['total'])) $scope.totals[filter_item_id]['total2'][$scope.year]['total'] = 0;

                    if (angular.isDefined(key['month'])) {
                        angular.forEach(key['month'], function (key2, value2) {
                            if (angular.isUndefined($scope.totals[filter_item_id]['total'][$scope.year][value2])) $scope.totals[filter_item_id]['total'][$scope.year][value2] = 0;
                            if (angular.isUndefined($scope.totals[filter_item_id]['total2'][$scope.year][value2])) $scope.totals[filter_item_id]['total2'][$scope.year][value2] = 0;

                            $scope.totals[filter_item_id]['total'][$scope.year][value2] += parseFloat(key2);
                            $scope.totals[filter_item_id]['total'][$scope.year]['total'] += parseFloat(key2);
                            $scope.totals[filter_item_id]['item'][$scope.year][key.item_id] += parseFloat(key2);

                            if (angular.isDefined(key.work_category_item_id) && key.work_category_item_id > 0) {
                                $scope.totals[filter_item_id]['total2'][$scope.year][value2] += parseFloat(key2);
                                $scope.totals[filter_item_id]['total2'][$scope.year]['total'] += parseFloat(key2);
                            }
                        });
                    }

                    if ($scope.workCategories.length > 0 && angular.isDefined(key.work_category_item_id)) {
                        if (angular.isUndefined(workCategoryTotals[key.work_category_item_id])) {
                            workCategoryTotals[key.work_category_item_id] = 0;
                        }
                        if (angular.isUndefined(workCategoryTotals2[key.work_category_item_id])) {
                            workCategoryTotals2[key.work_category_item_id] = {};
                        }

                        angular.forEach(key['month'], function (key2, value2) {
                            if (angular.isUndefined(workCategoryTotals2[key.work_category_item_id][value2])) {
                                workCategoryTotals2[key.work_category_item_id][value2] = 0;
                            }
                            workCategoryTotals2[key.work_category_item_id][value2] += key2;
                        });

                        workCategoryTotals[key.work_category_item_id] += $scope.totals[filter_item_id]['item'][$scope.year][key.item_id];
                    }

                    if (key.work_category_item_id > 0 && $scope.workCategoriesSelected[filter_item_id].indexOf(key.work_category_item_id) == -1) {
                        $scope.workCategoriesSelected[filter_item_id].push(key.work_category_item_id);
                    }
                });
                if (typeof params !== 'undefined') {
                    $scope.changeValue(params);
                }

                $scope.workCategoryTotals[filter_item_id] = workCategoryTotals;
                $scope.workCategoryTotals2[filter_item_id] = workCategoryTotals2;
            });

            $scope.allowYearChange = true;
        }
        
        $scope.formatNumber = function(value) {
            if (typeof value === 'undefined') value = 0;
            return Common.formatNumber(value);
        }
        
        SaveService.subscribeSave($scope, function () {
            saveData();
        });

        let saveData = function () {
            let promises = [];

            if (changedItems.length > 0) {
                let saveArray = [];
                for(let i = 0; i < changedItems.length; i++) {
                    let row = changedItems[i];
                    if (StateParameters.DateMode == 1) {
                        let monthsArray = {};
                        angular.forEach(row.months, function (value, week) {
                            let weekWorkDays = getWeekWorkDays(row.user_item_id, week);
                            let dailyEffort = value / weekWorkDays.length;
                            if (angular.isUndefined(monthsArray[row.item_id])) {
                                monthsArray[row.item_id] = {};
                            }
                            angular.forEach(weekWorkDays, function (weekDate) {
                                let datesArr = weekDate.split('-');
                                if (angular.isUndefined(monthsArray[row.item_id][datesArr[1]])) {
                                    monthsArray[row.item_id][datesArr[1]] = [];
                                }
                                monthsArray[row.item_id][datesArr[1]].push({ year: datesArr[0], day: datesArr[2], hours: dailyEffort });
                            });
                        });
                        angular.forEach(monthsArray, function (month_data, allocation_item_id) {
                            angular.forEach(month_data, function (day_data, month) {
                                saveArray.push({ allocation_item_id: allocation_item_id, year: $scope.year, month: month, dates: day_data });
                            });
                        });
                    }
                    else {
                        angular.forEach(row['months'], function (key, value) {
                            let datesArray = [];
                            for (let j = 0; j < $scope.workingDays[row.user_item_id][row.year][value].length; j++) {
                                let hour = (key / $scope.workingDays[row.user_item_id][row.year][value].length);
                                datesArray.push({ year: $scope.year, day: $scope.workingDays[row.user_item_id][row.year][value][j].index, hours: hour });
                            }
                            saveArray.push({ allocation_item_id: row.item_id, year: row.year, month: value, dates: datesArray });
                        });
                    }
                }

                let p = SimpleAllocationsService.saveAllocationHours(StateParameters.DateMode, saveArray).then(function() {
                    changedItems = [];
                    newValuesArray = [];
                });     
                promises.push(p);
            }

            if (changedRows.length > 0) {
                let p2 = SimpleAllocationsService.updateAllocation(changedRows).then(function() {
                    changedRows = [];
                });
                promises.push(p2);
            }
            return $q.all(promises).then(function(allPromisesResolved) {
                let filter_item_ids = '0';
                angular.forEach($scope.filter_item_ids, function (value) {
                    filter_item_ids += ',' + value;
                });
                return SimpleAllocationsService.getSums(0, $scope.year, StateParameters.Process, filter_item_ids).then(function (sums) {
                    $scope.sums = sums;
                    $scope.allowYearChange = true;
                });
            });
        }
        
        let getWeekWorkDays = function (user_item_id, week) {
            let weekDays = [];
            let week_start = moment().year($scope.year).isoWeek(week).startOf('isoWeek');
            let week_end = moment().year($scope.year).isoWeek(week).endOf('isoWeek');
            while (week_start <= week_end) {
                if ($scope.workingDays[user_item_id]) {
                    if ($scope.workingDays[user_item_id][week_start.year()]) {
                        if ($scope.workingDays[user_item_id][week_start.year()][week_start.month() + 1]) {
                            if ($scope.workingDays[user_item_id][week_start.year()][week_start.month() + 1].filter(x => x.index == week_start.date()).length > 0) {
                                weekDays.push(week_start.year() + '-' + (week_start.month() + 1) + '-' + (week_start.date()));
                            }
                        }
                    }
                }
                week_start.add(1, 'days');
            }
            return weekDays;
        }

        $scope.changeValue = function (params) {
            newValuesArray.push({ item_id: params.item_id, months: params.months, year: $scope.year, user_item_id: params.user_item_id });
            let noDuplicateObjects = {};
            let l = newValuesArray.length;

            for (let i = 0; i < l; i++) {
                let new_item = newValuesArray[i];
                noDuplicateObjects[new_item.item_id] = new_item;
            }

            let nonDuplicateArray = [];

            for (let item in noDuplicateObjects) {
                nonDuplicateArray.push(noDuplicateObjects[item]);
            }

            changedItems = nonDuplicateArray;
        };

        $scope.changeAllocation = function (data) {
            let row = angular.copy(data);
            if (typeof row !== 'undefined') {
                if (angular.isDefined(row.resource_item_id) && row.resource_item_id.length > 0) {

                    if ($scope.usermode == 0) {
                        row.resource_type = 'competency';
                        row.user_item_id = 0;
                        data.user_item_id = 0;
                    }
                    else {
                        row.resource_type = 'user';
                        row.user_item_id = row.resource_item_id[0];
                        data.user_item_id = row.user_item_id;
                    }
                    row.resource_item_id = row.resource_item_id[0];
                }
                else {
                    row.resource_item_id = 0;
                    row.user_item_id = 0;
                    data.user_item_id = 0;
                }
                let index = _.findIndex(changedRows, { item_id: row.item_id });
                if (index != -1)
                    changedRows[index] = row;
                else
                    changedRows.push(row);

                SaveService.tick();
            }

            $scope.calculateMonthTotal(data.item_id, data.month, row.user_item_id);

            getCalendarEvents();
        };
        
        $scope.changeYear();

        let workCategoriesDict = {};
        angular.forEach($scope.workCategories, function (key, value) {
            workCategoriesDict[key.item_id] = key;
        });
        $scope.workCategoriesDict = workCategoriesDict;

        let addResource_data = { resource_item_ids: [] };

        let addResource = function () {
            let content;
            if ($scope.usermode == 0) {
                content = {
                    title: 'ALLOCATION_SIMPLE_ADD_COMPETENCY_TITLE',
                    inputs: [{
                        type: 'process',
                        label: 'ALLOCATION_SIMPLE_ADD_COMPETENCY_FIELD',
                        model: 'resource_item_ids',
                        process: 'competency',
                        max: 0
                    }],
                    buttons: [{ text: 'CANCEL' }, { type: 'primary', onClick: doAddResource, text: 'ALLOCATION_SIMPLE_ADD_BUTTON' }]
                };
            }
            else {
                content = {
                    title: 'ALLOCATION_SIMPLE_ADD_USER_TITLE',
                    inputs: [{
                        type: 'process',
                        label: 'ALLOCATION_SIMPLE_ADD_USER_FIELD',
                        model: 'resource_item_ids',
                        process: 'user',
                        max: 0
                    }],
                    buttons: [{ text: 'CANCEL' }, { type: 'primary', onClick: doAddResource, text: 'ALLOCATION_SIMPLE_ADD_BUTTON' }]
                };
			}

            addResource_data = { resource_item_ids: [] };

            MessageService.dialog(content, addResource_data);
        };

        let doAddResource = function () {
            angular.forEach(addResource_data.resource_item_ids, function (resource_item_id) {
                let found = false;
                angular.forEach($scope.filter_item_ids, function (filter_item_id) {
                    if (resource_item_id == filter_item_id) {
                        found = true;
					}
                });
                if (!found) {
                    $scope.filter_item_ids.push(resource_item_id);
				}
            });

            SaveReourceFilter();

            getAllocations(addResource_data.resource_item_ids);
        };

        $scope.delResource = function (resource_item_id) {
            let confirm = '';
            if ($scope.usermode == 0) {
                confirm = 'ALLOCATION_SIMPLE_DELETE_COMPETENCY_CONFIRM';
            }
            else {
                confirm = 'ALLOCATION_SIMPLE_DELETE_USER_CONFIRM';
            }
            MessageService.confirm(confirm, function () {
                angular.forEach($scope.filter_item_ids, function (filter_item_id, index) {
                    if (filter_item_id == resource_item_id) {
                        $scope.filter_item_ids.splice(index, 1);
                    }
                });

                delete $scope.rows[$scope.year][resource_item_id];

                SaveReourceFilter();
            });
        };

        let SaveReourceFilter = function () {
            let settings = PortfolioService.getActiveSettings('SimpleAllocations');
            settings.simple_allocations_filter_item_ids = $scope.filter_item_ids;
            PortfolioService.saveActiveSettings('SimpleAllocations');
		}

        let portfolioSettings = PortfolioService.getActiveSettings('SimpleAllocations');

        let saveFilters = function () {
            ViewService.view('configure.portfolioSimple', {
                params: {
                    NavigationMenuId: StateParameters.NavigationMenuId,
                    portfolioId: 'SimpleAllocations',
                    isNew: true
                },
                locals: {
                    CurrentSettings: {
                        Name: Translator.translate('PORTFOLIO_NEW'),
                        Value: portfolioSettings
                    },
                    ReloadNavigation: function () {
                        getAllocations(null);
                        $scope.ReloadNavigation();
                    },
                    RefreshParent: function(){
                        return;
                    }
                }
            });
        };

        $scope.header = {
            title: StateParameters.title,
            icons: []
        };

        if ($scope.AdminRights) {
            let save_filters_button = {
                icon: 'backup',
                onClick: saveFilters,
                name: 'ALLOCATION_SIMPLE_SAVE_FILTERS'
            };
            $scope.header.icons.push(save_filters_button);

            let add_resource_button = {
                icon: 'add',
                onClick: addResource,
                name: $scope.usermode == 0 ? 'ALLOCATION_SIMPLE_ADD_COMPETENCY' : 'ALLOCATION_SIMPLE_ADD_USER'
            };
            $scope.header.icons.push(add_resource_button);
        }

        setNavigation(SavedSettings);

        function setNavigation(settings) {
            let sidenav = [];
            let publicTabs = [];
            let myTabs = [];
            angular.forEach(settings, function (s) {
                if (s.Value.showInMenu) {
                    let t = {
                        hideSplit: true,
                        name: Translator.translation(s.Name),
                        menu: undefined,
                        $$activeTab: undefined,
                        onClick: function () {
                            $scope.filter_item_ids = s.Value.simple_allocations_filter_item_ids;
                            SaveReourceFilter();
                            getAllocations(null);
                        }
                    };

                    if (s.UserId == UserService.getCurrentUserId()) {
                        t.menu = [{
                            icon: 'settings',
                            name: 'CONFIGURE',
                            onClick: function () {
                                ViewService.view('configure.portfolioSimple', {
                                    params: {
                                        NavigationMenuId: StateParameters.navigationMenuId,
                                        portfolioId: 'SimpleAllocations',
                                        isNew: false
                                    },
                                    locals: {
                                        CurrentSettings: s,
                                        ReloadNavigation: function () {
                                            getAllocations(null);
                                            $scope.ReloadNavigation();
                                        },
                                        RefreshParent: function(){
                                            return;
                                        }
                                    }
                                });
                            }
                        }];

                        myTabs.push(t);
                    } else {
                        publicTabs.push(t);
                    }
                }
            });

            if (myTabs.length || publicTabs.length) {
                sidenav.push({
                    tabs: [{
                        icon: ' ',
                        name: 'ALLOCATION_SIMPLE_CLEAR_FILTERS',
                        onClick: function () {
                            $scope.filter_item_ids = [];
                            SaveReourceFilter();
                            getAllocations(null);
                        }
                    }]
                });
			}

            if (myTabs.length) {
                sidenav.push({ name: 'PORTFOLIO_SAVED', showGroup: true, tabs: myTabs, expand: true });
            }

            if (publicTabs.length) {
                sidenav.push({ name: 'PORTFOLIO_SAVED_PUBLIC', showGroup: true, tabs: publicTabs, expand: true });
            }

            SidenavService.navigation(sidenav);
        }

        $scope.ReloadNavigation = function () {
            PortfolioService.getSavedSettings('SimpleAllocations').then(function (s) {
                setNavigation(s);
            });
        };

        ViewService.footbar(StateParameters.ViewId, { template: 'pages/simpleAllocations/simpleAllocationsFooter.html' });
    }

    SimpleAllocationsService.$inject = ['ApiService'];
    function SimpleAllocationsService(ApiService) {
        let api = ApiService.v1api('resources/simple/allocations');

        this.getAllocations = function (processItemId, year, mode, process, filter_item_ids, portfolio_id) {
            return api.get([processItemId, year, mode, process, filter_item_ids, portfolio_id]);
	    };
    	
    	this.insertAllocation = function (allocationItem) {
            return api.new('', allocationItem).then(function(data) {
                return data;
            });
	    };
        
        this.updateAllocation = function (allocationItem) {
            return api.save('', allocationItem).then(function(data) {
                return data;
            });
        };

        this.deleteAllocation = function (allocationItemIds) {
            return api.delete(allocationItemIds);
	    };

        this.saveAllocationHours = function (mode, allocationHours) {
            return api.save(['hours', mode], allocationHours);
        };

        this.getSums = function (itemId, year, process, filter_item_ids) {
            return api.get(['sum', itemId, year, process, filter_item_ids]);
        };

        this.moveAllocations = function (processItemId, year, mode, process, filter_item_ids, portfolio_id, shifts) {
            return api.save(['move', processItemId, year, mode, process, filter_item_ids, portfolio_id], shifts);
        };
    }
})();