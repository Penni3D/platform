(function () {
	'use strict';


	angular
		.module('core.pages')
		.service('WorktimeTrackingService', WorktimeTrackingService);

	WorktimeTrackingService.$inject = ['ApiService', '$q', 'Common', 'Configuration', 'CalendarService', 'SaveService', 'ProcessService'];

	function WorktimeTrackingService(ApiService, $q, Common, Configuration, CalendarService, SaveService, ProcessService) {
		let pendingChanges = [];

		/**
		 * API HANDLING FUNCTIONS - START
		 */
		let WorkTimeTracking = ApiService.v1api('time/worktimetracking');

		let getListOfMyTasksFromDateRangeFromDateRange = function (from, to, selectedUserId, currentUserId, isAdmin, AdditionalHours, AllowBasicHoursOnWeekends, isMobile) {
			return WorkTimeTracking.get([selectedUserId, 'myitems',
				getDayMonthYearFormatFromMoment(from),
				getDayMonthYearFormatFromMoment(to)])
				.then(function (data) {
					return buildUIDataObject(selectedUserId, currentUserId, isAdmin, data, from, to, AdditionalHours, AllowBasicHoursOnWeekends, isMobile);
				});
		};

		let addTaskForMyList = function (userItemId, taskId) {
			let payload = {
				TaskItemId: taskId
			};

			return WorkTimeTracking.new(['myitems', userItemId], payload);
		};

		let removeTaskFromMyList = function (taskId, userItemId) {
			return WorkTimeTracking.delete(['myitems', userItemId, taskId]);
		};

		let getMyOpenTasks = function (userItemId) {
			return WorkTimeTracking.get(['myopentasks', userItemId]);
		};

		let saveChanges = function (userItemId) {
			let promises = [];
			if (_.size(pendingChanges)) {
				promises.push(WorkTimeTracking.new(['hours', userItemId], pendingChanges));
			}

			pendingChanges = [];

			return $q.all(promises);
		};

		let updateDescription = function (taskId, description) {
			return WorkTimeTracking.new(['myitems', taskId, 'description'], {description: description});
		};

		let approveTasks = function (userId, tasks) {
			return SaveService.bypassTick().then(function () {
				return WorkTimeTracking.new(['approve', userId], tasks);
			});
		};

		let rejectTasks = function (userId, tasks) {
			return SaveService.bypassTick().then(function () {
				return WorkTimeTracking.new(['reject', userId], tasks);
			});
		};

		let submitTasks = function (userId, tasks, partial) {
			let target = partial ? 'submitpartial' : 'submit';
			return SaveService.bypassTick().then(function () {
				return WorkTimeTracking.new([target, userId], tasks);
			});
		};

		let revertTasks = function (userId, tasks, status = 0) {
			return SaveService.bypassTick().then(function () {
				return WorkTimeTracking.new(['revert', userId, status], tasks);
			});

		};

		let copyTasks = function (userId, CopyFromStart, CopyFromEnd, OffSet) {
			return WorkTimeTracking.new(['myitems', 'copy'], {
				UserItemId: userId,
				CopyFromStart: CopyFromStart,
				CopyFromEnd: CopyFromEnd,
				OffSet: OffSet
			});
		};

		/**
		 * API HANDLING FUNCTIONS - END
		 */

		/**
		 * DATA MANIPULATING FUNCTIONS - START
		 */

		function buildParentTree(items) {
			let keys = _.keys(items);
			_.each(keys, function (key) {
				let val = items[key];
				if (val && val.parent_item_id && items[val.parent_item_id]) {
					val.$parent = items[val.parent_item_id];
				}
			});
			let parentPhases = {};

			angular.forEach(keys, function (key) {
				let val = items[key];
				let titles = [val.parent_title + ' > '];
				let parent = val.$parent;

				let c = 0;
				while (parent) {
					c++;
					titles.push(parent.parent_title + ' > ');
					parent = parent.$parent;
					if (typeof (parent) !== 'undefined' && val.parent_item_id === parent.parent_item_id) {
						titles.push('Circular Dependency Error > ');
						console.log('Tasks are forming a circular dependency. Check task ' + val.parent_item_id + ' and it\'s parent');
						break;
					}
				}
				parentPhases[key] = _.join(titles, '');
			});
			return parentPhases;
		}

		function addValue(task, value, primary, date = false) {
			let delimiter = ">";
			if (date) {
				value = CalendarService.formatDate(value);
				delimiter = "-";
			}

			if (primary == "primary") {
				if (task.title == "") {
					task.title = value;
				} else {
					task.title += ' ' + delimiter + ' ' + value;
				}
			} else {
				if (task.titleSecondary == "") {
					task.titleSecondary = value;
				} else {
					task.titleSecondary += ' ' + delimiter + ' ' + value;
				}
			}
		}

		function removeCTRLChars(str) {
			return str.replace(/[\x00-\x1F\x7F-\x9F]/g, "");
		}

		function buildUIDataObject(selectedUser, currentUser, isAdmin, receivedData, from, to, AdditionalHours, AllowBasicHoursOnWeekends, isMobile) {
			let additionalHoursInUse = Common.isTrue(AdditionalHours);
			let allowBasicHoursOnWeekends = Common.isTrue(AllowBasicHoursOnWeekends);

			let dataObject = {
				parentPhases: undefined,
				edit: false,
				from: undefined,
				to: undefined,
				canConfirm: false,
				canConfirmPartial: false,
				isPartial: false,
				isSubmitted: false,
				canRevert: false,
				dateHeaderColumns: [],
				dateValueColumns: [],
				datesForColumns: [],
				hours: {},
				parentsOfTasks: {},
				myTasks: [],
				basicTasks: undefined,
				breakdownTypes: undefined,
				sheetStatusMessage: undefined,
				originalHours: undefined,
				weeklyHourTotal: receivedData.calendarData[selectedUser] ? receivedData.calendarData[selectedUser].WeeklyHours : 0
			};

			dataObject.parentPhases = buildParentTree(receivedData.parentPhases);
			dataObject.edit = true;
			dataObject.from = from;
			dataObject.to = to;

			if (selectedUser !== currentUser && isAdmin !== true) dataObject.edit = false;

			dataObject.canConfirm = true;
			dataObject.canRevert = false;
			dataObject.canConfirmPartial = true;

			dataObject.dateHeaderColumns = [];
			dataObject.dateValueColumns = [];
			dataObject.datesForColumns = [];
			dataObject.hours = {};
			let myTasks = _.orderBy(receivedData.tasks, ['owner_name', 'title']);
			dataObject.parentsOfTasks = {};
			dataObject.myTasks = [];
			dataObject.basicTasks = receivedData.basicTasks;
			dataObject.breakdownTypes = receivedData.breakdownTypes;

			let currentOwnerName = '';
			let promises = [];
			let promiseMap = {};

			_.each(myTasks, function (task) {
				let taskOwnerDuration = _.get(receivedData.parentStuff, task.owner_type + '.' + task.owner_item_id);

				if (taskOwnerDuration) {
					task.fromDate = taskOwnerDuration.from && moment(taskOwnerDuration.from).format('YYYYMMDD');
					task.toDate = taskOwnerDuration.to && moment(taskOwnerDuration.to).format('YYYYMMDD');
				}

				//Process Owner
				if (currentOwnerName !== task.owner_name) {
					let t = "";
					if (task.owner_name != "") t = JSON.parse(removeCTRLChars(task.owner_name));
					let name = "";
					let sname = "";
					let add = [];

					_.each(t, function (item) {
						if (item.process && item.item_id) {
							add.push(item.item_id);

							if (!promiseMap[item.item_id]) {
								promiseMap[item.item_id] = true;
								if (item.item_id != 0) promises.push(ProcessService.getItem(item.process, item.item_id).then(function (p) {
									return {name: p.primary, item_id: item.item_id}
								}));
							}

							return;
						} else if (item.name) {
							if (item.primary == 'secondary') {
								sname += item.name;
							} else {
								name += item.name;
							}

							return;
						}
					});

					dataObject.myTasks.push({
						header: true,
						itemId: task.owner_item_id,
						title: task.owner_name,
						fromDate: task.fromDate,
						toDate: task.toDate,
						ownerTitle: name,
						ownerSecondary: sname,
						add: add
					});
				}


				dataObject.myTasks.push(task);
				currentOwnerName = task.owner_name;
				dataObject.parentsOfTasks[task.task_item_id] = currentOwnerName;

				//Process Task
				let t = "";
				if (task.title != "") t = JSON.parse(removeCTRLChars(task.title));
				task.title = "";
				task.titleSecondary = "";
				let add = [];

				_.each(t, function (item) {
					if (item.process && item.item_id) {
						add.push(item.item_id);

						if (!promiseMap[item.item_id]) {
							promiseMap[item.item_id] = true;
							if (item.item_id != 0) promises.push(ProcessService.getItem(item.process, item.item_id).then(function (p) {
								return {name: p.primary, item_id: item.item_id}
							}));
						}

						return;
					} else if (item.date) {
						addValue(task, item.date, item.primary, true);
						return;
					} else if (item.name) {
						addValue(task, item.name, item.primary, false);
						return;
					}
				});
				task.add = add;


			});

			return $q.all(promises).then(function () {
				_.each(dataObject.myTasks, function (task) {
					if (task.header) {
						_.each(task.add, function (id) {
							let p = _.find(promises, function (promise) {
								return promise.$$state.value.item_id == id;
							});

							if (p && p.$$state.value.name) {
								if (task.ownerSecondary != "") {
									task.ownerSecondary += ", " + p.$$state.value.name;
								} else {
									task.ownerSecondary = p.$$state.value.name;
								}
							}
						});
					} else {
						_.each(task.add, function (id) {
							let p = _.find(promises, function (promise) {
								return promise.$$state.value.item_id == id;
							});

							if (p && p.$$state.value.name) {
								if (task.titleSecondary != "") {
									task.titleSecondary += ", " + p.$$state.value.name;
								} else {
									task.titleSecondary = p.$$state.value.name;
								}
							}
						});
					}

				});

				let dateCursor = from.clone().startOf('day');
				let ind = 0;
				while (dateCursor.isBefore(to)) {
					let date = getDayMonthYearFormatFromMoment(dateCursor);
					dataObject.dateHeaderColumns.push(date);

					let columnClass = dateCursor.isSame(new Date(), 'day') ? 'today' : ind == 1 || ind == 3 || ind >= 5 ? 'weekend' : '';
					let addDateColumn = false;

					if ((ind < 5 || allowBasicHoursOnWeekends) || !additionalHoursInUse) {
						if (!isMobile) {
							dataObject.dateValueColumns.push({
								date: date,
								date2: dateCursor.format('YYYYMMDD'),
								field: 'hours',
								index: ind,
								class: columnClass
							});

							addDateColumn = true;
						}
					}

					if (additionalHoursInUse) {
						if (!isMobile) {
							dataObject.dateValueColumns.push({
								date: date,
								date2: dateCursor.format('YYYYMMDD'),
								field: 'additionalHours',
								index: ind,
								class: columnClass
							});

							addDateColumn = true;
						}
					}

					if (addDateColumn) {
						dataObject.datesForColumns.push({
							date: CalendarService.formatDate(dateCursor.toDate(), true),
							class: columnClass
						});
					}

					ind++;
					dateCursor.add(1, 'days');
				}
				let totalStatus = 100;
				let approvedAt;
				let approver;
				let partial = false;
				_.each(receivedData.hours, function (r) {
					let taskId = r.task_item_id;
					let process = r.process;

					
					if (r.line_manager_status === 0.5) partial = true;
					if (totalStatus >= r.line_manager_status) {
						totalStatus = r.line_manager_status;
					}

					if (r.line_manager_status === 2) {
						let d = r.line_manager_approved_at && CalendarService.formatDate(moment(r.line_manager_approved_at).toDate());
						if (!approver) {
							if (r.line_manager_user_id === r.user_item_id) {
								approvedAt = d;
							} else {
								approvedAt = d;
								approver = r.line_manager;
							}
						}
					}

					dataObject.hours[process] = dataObject.hours[process] || {};
					dataObject.hours[process][taskId] = dataObject.hours[process][taskId] || {};
					if (r.line_manager_status == 0.5) dataObject.canConfirmPartial = false;

					if ((!(r.line_manager_status == 0 || r.line_manager_status == 0.5)) && !_.isNull(r.line_manager_status)) {
						dataObject.edit = false;
						dataObject.canConfirm = false;

						if (r.line_manager_status === 1 || r.line_manager_status === 0.5)
							dataObject.canRevert = true;
					}

					dataObject.hours[process][taskId][getDayMonthYearFormatFromMoment(moment(r.date))] = {
						hours: r.hours,
						itemId: r.item_id,
						breakdownHours: r.breakdownHours,
						additionalHours: r.additional_hours,
						taskId: taskId
					};
				});

				if (totalStatus === 1 || (totalStatus === 2 && !approvedAt)) {
					dataObject.sheetStatusMessage = 'This sheet has been submitted';
					dataObject.isPartial = false;
					dataObject.isSubmitted = true;
				} else if (totalStatus === 2 && !approver) {
					dataObject.sheetStatusMessage = 'This sheet has been submitted ' + approvedAt;
					dataObject.isPartial = false;
					dataObject.isSubmitted = true;
				} else if (totalStatus === 2) {
					dataObject.sheetStatusMessage = 'This sheet has been approved ' + approvedAt + ' by ' + approver;
					dataObject.isSubmitted = true;
					dataObject.isPartial = false;
				} 
				
				if (partial) {
					dataObject.sheetStatusMessage = 'This sheet has been submitted partially';
					dataObject.isSubmitted = false;
					dataObject.isPartial = true;
				}

				calculateTotalHours(dataObject);
				dataObject.originalHours = receivedData.hours;
				return dataObject;
			});
		}

		function calculateTotalHours(dataObject) {
			let weeklyTotal = 0;
			
			dataObject.totalHours = {};
			dataObject.sumOfAll = {};
			dataObject.totalProjectHours = {};
			angular.forEach(dataObject.hours, function (tasks, process) {
				dataObject.totalHours[process] = {};
				angular.forEach(tasks, function (task) {
					let dateCursor = dataObject.from.clone().startOf('day');
					do {
						let day = getDayMonthYearFormatFromMoment(dateCursor);

						let taskDate = task[day];
						if (taskDate) {
							let parentName = dataObject.parentsOfTasks[taskDate.taskId];
							dataObject.totalHours[process][day] = dataObject.totalHours[process][day] || {
								hours: 0,
								additionalHours: 0
							};
							dataObject.sumOfAll[day] = dataObject.sumOfAll[day] || {hours: 0, additionalHours: 0};

							dataObject.totalHours[process][day].hours += taskDate.hours || 0;
							dataObject.totalHours[process][day].additionalHours += taskDate.additionalHours || 0;
							dataObject.sumOfAll[day].hours += taskDate.hours || 0;
							dataObject.sumOfAll[day].additionalHours += taskDate.additionalHours || 0;

							if (parentName) {
								dataObject.totalProjectHours[parentName] = dataObject.totalProjectHours[parentName] || {};
								dataObject.totalProjectHours[parentName][day] = dataObject.totalProjectHours[parentName][day] || {
									hours: 0,
									additionalHours: 0
								};
								dataObject.totalProjectHours[parentName][day].hours += taskDate.hours || 0;
								dataObject.totalProjectHours[parentName][day].additionalHours += taskDate.additionalHours || 0;
							}

							weeklyTotal += (taskDate.hours || 0);
						}

						dateCursor.add(1, 'days');
					} while (dateCursor.isBefore(dataObject.to))
				});
			});
			dataObject.weeklyTotal = weeklyTotal;
		}

		function getDayMonthYearFormatFromMoment(momentObject) {
			return momentObject.format('DDMMYYYY');
		}

		function changeHours(item, dataObject) {
			let existingItem = _.find(pendingChanges, function (change) {
				return change.process === item.process && change.date === item.column.date &&
					item.taskId === change.taskId;
			});

			let propertyName = item.column.field;
			let h = dataObject.hours[item.process][item.taskId][item.column.date];

			if (!h || !_.has(h, propertyName) || isNaN(h[propertyName])) {
				return;
			}

			if (!existingItem) {
				let c = {
					itemId: h.itemId,
					taskId: item.taskId,
					process: item.process,
					date: item.column.date
				};
				pendingChanges.push(c);
				existingItem = c;
			}

			existingItem.hours = h.hours;
			existingItem.additionalHours = h.additionalHours;
			existingItem.breakdownHours = h.breakdownHours;
			calculateTotalHours(dataObject);
		}

		function buildColumnArrays(stateParameters, shortDaysOfWeek, isMobile) {
			let additionalHoursInUse = Common.isTrue(stateParameters.AdditionalHours);
			let allowBasicHoursOnWeekends = Common.isTrue(stateParameters.AllowBasicHoursOnWeekends);
			let splitWeekDays = [];
			let weekDays = _.map(shortDaysOfWeek, function (day, ind) {
				let isWeekendDay = (parseInt(ind) >= 5);
				let defaultColCount = additionalHoursInUse ? 2 : 1;
				let colsInWeekend = defaultColCount;
				if (!allowBasicHoursOnWeekends || !additionalHoursInUse) {
					colsInWeekend = 1;
				}

				let returnObject = {
					value: day,
					colSpan: isWeekendDay ? colsInWeekend : defaultColCount,
					class: undefined
				};
				let daySplitObject = {
					class: returnObject.class,
					variable: 'WORKINGTIME_TRACKING_HOURS'
				};

				let showNormalHourCol = allowBasicHoursOnWeekends && isWeekendDay || !isWeekendDay;

				if (showNormalHourCol) {
					splitWeekDays.push(daySplitObject);
				}
				if (additionalHoursInUse) {
					splitWeekDays.push(_.extend({}, daySplitObject, {variable: 'WORKINGTIME_TRACKING_ADDITIONAL_HOURS_ABBR'}));
				}
				if (!(showNormalHourCol) && !additionalHoursInUse) {
					splitWeekDays.push(_.extend({}, daySplitObject, {variable: ''}));
				}
				return returnObject;
			});

			return {
				weekDays: isMobile ? [] : weekDays,
				splitWeekDays: isMobile ? [] : splitWeekDays
			};
		}

		/**
		 * DATA MANIPULATING FUNCTIONS - END
		 */

		return {
			getListOfMyTasksFromDateRangeFromDateRange: getListOfMyTasksFromDateRangeFromDateRange,
			addTasksForMyList: addTaskForMyList,
			removeTaskFromMyList: removeTaskFromMyList,
			getMyOpenTasks: getMyOpenTasks,
			saveChanges: saveChanges,
			updateDescription: updateDescription,
			approveTasks: approveTasks,
			rejectTasks: rejectTasks,
			submitTasks: submitTasks,
			revertTasks: revertTasks,
			copyTasks: copyTasks,

			getDayMonthYearFormatFromMoment: getDayMonthYearFormatFromMoment,
			buildUIDataObject: buildUIDataObject,
			calculateTotalHours: calculateTotalHours,
			changeHours: changeHours,
			buildColumnArrays: buildColumnArrays,

			isChangesPending: function () {
				return _.size(pendingChanges) > 0;
			}
		};
	}
})
();