(function () {
	'use strict';


	angular
		.module('core.pages')
		.config(WorktimeTrackingConfig)
		.controller('WorktimeTrackingController', WorktimeTrackingController);

	WorktimeTrackingConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider', 'ConfigurationProvider'];

	function WorktimeTrackingConfig(ViewsProvider, ViewConfiguratorProvider, ConfigurationProvider) {
		ConfigurationProvider.addConfiguration('Page', {
			ApprovalRequiredFieldInProcesses: {
				format: 'string'
			},
			AddOnlyTasksFromProcessWithStatusColumn: {
				format: 'string'
			},
			AddOnlyTasksFromProcessWithStatusValueIn: {
				format: 'string'
			},
			TaskStartDate: {
				format: 'string'
			},
			TaskEndDate: {
				format: 'string'
			}
		});

		ViewsProvider.addView('worktimeTracking', {
			controller: 'WorktimeTrackingController',
			template: 'pages/workTimeTracking/worktime-tracking-week-view.html',
			resolve: {
				WorkTimeRights: function (ViewConfigurator) {
					return ViewConfigurator
						.getRights('worktimeTracking')
						.then(function (states) {
							return {
								admin: _.includes(states, 'admin') || _.includes(states, 'limitedAdmin'),
								limitedAdmin: _.includes(states, 'limitedAdmin'),
								own: _.includes(states, 'own')
							};
						});
				},
				CommentOptions: function (ProcessService, StateParameters) {
					if (StateParameters.CommentMode == 2 && StateParameters.CommentList) {
						return ProcessService.getListItems(StateParameters.CommentList);
					} else {
						return [];
					}

				},
				Processes: function (ProcessesService) {
					return ProcessesService.get().then(function (processes) {
						return processes;
					});
				},
				WorktimeLimit: function (WorktimeHourStatusService) {
					return WorktimeHourStatusService.getRebaseDate();
				}
			}
		});

		ViewsProvider.addPage('worktimeTracking', 'PAGE_WORKTIME_TRACKING', ['own', 'limitedAdmin', 'admin']);
		ViewConfiguratorProvider.addConfiguration('worktimeTracking', {
			global: true,
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('task');
				},
				Lists: function (ListsService) {
					return ListsService.getListProcesses();
				},
				UserLists: function (ProcessActionsService) {
					return ProcessActionsService.getItemColumns('user').then(function (result) {
						let listColumns = [];
						for (let lc of result) {
							if (lc.DataType == 6) listColumns.push(lc);
						}
						return listColumns;
					});
				},
				TaskColumns: function () {
					return [];
				},
				Features: function (Views) {
					return Views.getFeatures();
				}
			},
			tabs: {
				BASIC_SETTINGS: {
					TaskSelectionPortfolioId: function (resolves) {
						return {
							label: 'WT_HOUR_TASKSELECTIONPORTFOLIO',
							type: 'select',
							options: resolves.Portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					TaskPortfolioId: function (resolves) {
						return {
							label: 'WT_HOUR_TASKPORTFOLIO',
							type: 'select',
							options: resolves.Portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					HourBreakdown: function () {
						return {
							label: 'WT_HOUR_BREAKDOWN',
							type: 'boolean'
						};
					},
					AdditionalHours: function () {
						return {
							label: 'WT_ADDITIONAL_HOURS',
							type: 'boolean'
						};
					}, PartialSubmissionsInUse: function () {
						return {
							label: 'WT_PARTIAL_IN_USE',
							type: 'boolean'
						};
					}, HourApprovalInUse: function () {
						return {
							label: 'WT_APPROVAL_IN_USE',
							type: 'boolean'
						};
					}, AllowCopy: function () {
						return {
							label: 'WT_COPY_IN_USE',
							type: 'boolean'
						};
					}
				},
				WT_RESTRICTIONS: {
					RequireFullWeek: function () {
						return {
							label: 'WT_REQUIRE_FULL_WEEK',
							type: 'boolean'
						};
					},
					DailyHoursCapped: function () { // From KTN-case: If additionalHours is in use, then regular hours are capped to persons daily hours
						return {
							label: 'WT_DAILY_HOURS_CAPPED',
							type: 'boolean'
						};
					},
					DailyMaximumAdditionalHours: function () { // From KTN-case: AdditionalHours are capped to this value
						return {
							label: 'WT_DAILY_MAX_ADDITIONAL_HOURS',
							type: 'decimal'
						};
					},
					AllowBasicHoursOnWeekends: function () {
						return {
							label: 'WT_ALLOW_ON_WEEKEND',
							type: 'boolean'
						};
					},
					AllowOutsideOfTasks: function () {
						return {
							label: 'WT_TASK_START_END',
							type: 'boolean'
						};
					}
				},
				WT_COMMENTS: {
					CommentMode: function () {
						return {
							label: 'WT_USE_COMMENTS',
							options: [{name: 'WT_NO_COMMENT', value: 0}, {
								name: 'WT_TEXT_COMMENT',
								value: 1
							}, {name: 'WT_LIST_COMMENT', value: 2}],
							type: 'select'
						};
					},
					CommentList: function (resolves) {
						return {
							label: 'WT_COMMENT_LIST',
							type: 'select',
							options: resolves.Lists,
							optionValue: 'ProcessName',
							optionName: 'LanguageTranslation'
						};
					},
					CommentDefaultList: function (resolves) {
						return {
							label: 'WT_COMMENT_DEFAULT_LIST',
							type: 'select',
							options: resolves.UserLists,
							optionValue: 'Name',
							optionName: 'Variable'
						};
					},
					RequireComment: function () {
						return {
							label: 'WT_REQUIRE_COMMENT',
							type: 'boolean'
						};
					}
				},
				SPLIT_OPTIONS: {
					SplitFeature: function (resolves) {
						return {
							label: 'FEATURE_TO_OPEN',
							type: 'select',
							options: resolves.Features,
							optionValue: 'view',
							optionName: 'variable'
						}
					},
					CustomWidth: function () {
						return {
							label: 'WIDTH',
							type: 'select',
							options: [{name: 'VIEW_SPLIT_EVEN', value: 'even'}, {
								name: 'VIEW_SPLIT_WIDER',
								value: 'wider'
							}, {name: 'VIEW_SPLIT_NARROWER', value: 'narrower'},
								{name: 'VIEW_SPLIT_FULL', value: 'full'}
							],
							optionValue: 'value',
							optionName: 'name'
						}
					}
				}
			},
			defaults: {
				HourBreakdown: 'false',
				CommentMode: 0,
				RequireComment: 'false',
				AdditionalHours: 'false',
				RequireFullWeek: 'false',
				DailyHoursCapped: 'false',
				DailyMaximumAdditionalHours: 0,
				AllowBasicHoursOnWeekends: 'true',
				HourApprovalInUse: 'true',
				PartialSubmissionsInUse: 'false',
				AllowOutsideOfTasks: 'false',
				AllowCopy: 'false',
				SplitFeature: 'process.hourAccumulation',
				CustomWidth: 'wider'
			}
		});
	}

	/*
	 WORKTIME TRACKING CONTROLLER
	 */
	WorktimeTrackingController.$inject = [
		'$q',
		'$scope',
		'$rootScope',
		'$compile',
		'WorktimeTrackingService',
		'ViewService',
		'StateParameters',
		'Common',
		'SaveService',
		'MessageService',
		'UserService',
		'CalendarService',
		'Configuration',
		'WorkTimeRights',
		'Processes',
		'LocalStorageService',
		'CommentOptions',
		'Translator',
		'ProcessService',
		'WorktimeLimit',
		'SettingsService'
	];

	function WorktimeTrackingController(
		$q,
		$scope,
		$rootScope,
		$compile,
		WorktimeTrackingService,
		ViewService,
		StateParameters,
		Common,
		SaveService,
		MessageService,
		UserService,
		CalendarService,
		Configuration,
		WorkTimeRights,
		Processes,
		LocalStorageService,
		CommentOptions,
		Translator,
		ProcessService,
		WorktimeLimit,
		SettingsService
	) {
		$scope.collapsedGroups = {};
		let existingConf = LocalStorageService.get('worktime_tracking_collapsed_items');
		try {
			_.extend($scope.collapsedGroups, existingConf);
		} catch (e) {
			$scope.collapsedGroups = {};
		}

		$scope.processes = _.groupBy(Processes, 'ProcessName');

		$scope.isMobile = window.innerWidth < 1024;
		$scope.vacations = {};
		$scope.hourbreakdownIsOn = Common.isTrue(StateParameters.HourBreakdown);
		$scope.additionalHoursInUse = Common.isTrue(StateParameters.AdditionalHours);
		$scope.fullWeekRequired = Common.isTrue(StateParameters.RequireFullWeek);
		$scope.allowBasicHoursOnWeekends = Common.isTrue(StateParameters.AllowBasicHoursOnWeekends);
		$scope.isHourApprovalInUse = Common.isTrue(StateParameters.HourApprovalInUse);
		$scope.partialSubmissionsInUse = Common.isTrue(StateParameters.PartialSubmissionsInUse);
		$scope.AllowOutsideOfTasks = Common.isTrue(StateParameters.AllowOutsideOfTasks);
		$scope.AllowCopy = Common.isTrue(StateParameters.AllowCopy);
		$scope.worktimeLimit = moment(WorktimeLimit);
		$scope.commentOptions = CommentOptions;
		$scope.commentMode = Number(StateParameters.CommentMode);
		$scope.requireComment = Common.isTrue(StateParameters.RequireComment);
		$scope.commentDefaultList = StateParameters.CommentDefaultList;
		$scope.defaultCommentValue = undefined;
		if ($scope.commentMode === 2 && CommentOptions.length === 0) {
			$scope.commentMode = 1;
		}

		//Selected user
		$scope.rights = WorkTimeRights;
		if (typeof StateParameters.activeSettings !== 'undefined') {
			$scope.selectedUserId = StateParameters.activeSettings.selectedUserId;
			$scope.selectedUserName = StateParameters.activeSettings.selectedUserName;
		} else if (typeof StateParameters.userId === 'undefined') {
			$scope.selectedUserId = $rootScope.me.itemId;
			$scope.selectedUserName = $rootScope.me.name;
			$scope.rights.jump = false;
		} else {
			$scope.selectedUserId = StateParameters.userId;
			$scope.selectedUserName = StateParameters.userName;
			$scope.rights.jump = $rootScope.me.itemId !== StateParameters.userId;
			$scope.rights.canReject = StateParameters.status === 1 || StateParameters.status === 2;
			$scope.rights.canApprove = StateParameters.status === 1 || StateParameters.status === 2 || StateParameters.status === 3;
			$scope.calendars = {};
		}


		ProcessService.getItem('user', $scope.selectedUserId).then(function (user) {
			if (user[$scope.commentDefaultList] && user[$scope.commentDefaultList].length > 0)
				$scope.defaultCommentValue = user[$scope.commentDefaultList][0];
		});

		let pendingComments = [];
		$scope.localeWeekSelector = 'week';
		if (+$rootScope.clientInformation.first_day_of_week === 1) {
			$scope.localeWeekSelector = 'isoWeek';
		}

		let selections = {
			dateRange: {
				to: undefined,
				from: typeof StateParameters.fromDate === 'undefined' ? moment() : moment(moment(StateParameters.fromDate).format('DDMMYYYY'), 'DDMMYYYY')
			},
			resolution: 1
		};

		$scope.isCurrentWeek = function () {
			return selections.dateRange.from.isSame(moment().startOf('isoWeek'), 'date');
		};

		$scope.isBeforeCurrentWeek = function () {
			return selections.dateRange.to < moment().startOf('isoWeek');
		};

		$scope.printing = false;
		$scope.OnBeforeCapture = function () {
			$scope.printing = true;
		}

		$scope.OnAfterCapture = function () {
			$scope.printing = false;
		}

		//Set Initial Dates
		if (!$scope.isMobile)
			selections.dateRange.from = selections.dateRange.from.startOf($scope.localeWeekSelector);
		selections.dateRange.to = $scope.isMobile ?
			selections.dateRange.from.clone() : selections.dateRange.from.clone().endOf($scope.localeWeekSelector).endOf('day');

		function calculateWeeklyHours() {
			$scope.weeklyHours = CalendarService.getUserWorkHours($scope.selectedUserId).weeklyHours;
		}

		function loadCalendars() {
			let thisYear = new Date().getFullYear();
			let start = new Date(thisYear, 0, 1);
			let end = new Date(thisYear + 1, 0, 1);
			return CalendarService.getUserCalendar($scope.selectedUserId, start, end).then(function (calendarData) {
				$scope.calendarData = calendarData;
				calculateWeeklyHours();
			});
		}

		if (!StateParameters.userId || $scope.selectedUserId === StateParameters.userId) {
			ViewService.footbar(StateParameters.ViewId, {template: 'pages/workTimeTracking/worktime-tracking-footbar.html'});
		}

		$scope.selectedRows = [];
		$scope.formattedDateRange = {};

		let columns = WorktimeTrackingService.buildColumnArrays(StateParameters, Common.getLocalShortDaysOfWeek(), $scope.isMobile);
		$scope.weekDays = columns.weekDays;
		$scope.splitWeekDays = columns.splitWeekDays;

		function getDaysBetweenDates(d0, d1) {

			let msPerDay = 8.64e7;

			// Copy dates so don't mess them up
			let x0 = new Date(d0);
			let x1 = new Date(d1);

			// Set to noon - avoid DST errors
			x0.setHours(12, 0, 0);
			x1.setHours(12, 0, 0);

			// Round to remove daylight saving errors
			return Math.round((x1 - x0) / msPerDay);
		}

		$scope.copyToToday = function () {
			if ($scope.isBeforeCurrentWeek()) {
				MessageService.confirm("WT_COPY_CONFIRM", function () {
					let date1 = CalendarService.getUTCDate(moment().startOf('isoWeek').toDate());
					let date2 = CalendarService.getUTCDate(selections.dateRange.from.startOf('day').toDate());
					let dateFrom = CalendarService.getUTCDate(moment(selections.dateRange.from).toDate())
					let dateTo = CalendarService.getUTCDate(moment(selections.dateRange.to).toDate())
					let diff = Math.abs(getDaysBetweenDates(date1, date2));

					WorktimeTrackingService.copyTasks(
						$scope.selectedUserId,
						moment(dateFrom).format('YYYY-MM-DD'),
						moment(dateTo).format('YYYY-MM-DD'),
						diff
					).then(function (result) {
						if (result)
							MessageService.msgBox("WT_COPY_DONE");
						else
							MessageService.msgBox("WT_SUBMITTED_ERROR");
					});
				});
			} else
				MessageService.msgBox("WT_SAMEWEEK_ERROR");
		};

		SaveService.subscribeSave($scope, function () {
			return WorktimeTrackingService.saveChanges($scope.selectedUserId, pendingComments, $scope.data).then(loadData);
		});
		let iconset = [
			{
				icon: 'date_range',
				onClick: function () {
					let model = {Date: null};
					let params = {
						title: 'WORKINGTIME_TRACKING_CHANGE_DATE',
						inputs: [
							{
								type: 'date',
								label: 'WORKINGTIME_TRACKING_DATE',
								model: 'Date'
							}],
						buttons: [
							{
								type: 'secondary',
								text: 'MSG_CANCEL'
							},
							{
								type: 'primary',
								text: 'WORKINGTIME_TRACKING_JUMP',
								onClick: function () {
									$scope.jumpTo(model.Date);
								}
							}]
					};
					MessageService.dialog(params, model);
				}
			},
			{
				icon: 'add',
				onClick: function () {
					let model = {
						tasks: []
					};

					let params = {
						title: 'WORKINGTIME_TRACKING_ADD_TASKS',
						inputs: [
							{
								type: 'process',
								process: 'task',
								model: 'tasks',
								label: 'TASKS',
								portfolioId: StateParameters.TaskSelectionPortfolioId
							}
						],
						buttons: [
							{
								type: 'secondary',
								text: 'WORKINGTIME_TRACKING_ADD_MY_TASKS',
								onClick: function () {
									WorktimeTrackingService.getMyOpenTasks($scope.selectedUserId)
										.then(function (data) {
											$scope.onDrop('task', _.map(data, 'item_id'));
										});
								}
							},
							{
								type: 'secondary',
								text: 'MSG_CANCEL'
							},
							{
								type: 'primary',
								text: 'ADD',
								onClick: function () {
									let i, l = model.tasks.length;
									for (i = 0; i < l; i++) {
										$scope.onDrop('task', model.tasks[i]);
									}
								}
							}
						]
					};

					MessageService.dialog(params, model);
				}
			}];
		if ($scope.AllowCopy) {
			iconset.unshift({
				icon: 'playlist_add',
				onClick: function () {
					$scope.copyToToday();
				}
			});
		}
		if ($scope.rights.admin) {
			iconset.unshift({
				icon: 'account_circle', onClick: function () {
					let model = {
						users: []
					};

					let params = {
						title: 'WORKINGTIME_TRACKING_CHANGE_USER',
						inputs: [
							{
								type: 'process',
								process: 'user',
								model: 'users',
								label: 'WORKINGTIME_TRACKING_USER',
								max: 1
							}],
						buttons: [
							{
								type: 'secondary',
								text: 'MSG_CANCEL'
							},
							{
								type: 'primary',
								text: 'WORKINGTIME_TRACKING_CHANGE',
								onClick: function () {
									if (model.users.length > 0) $scope.changeUser(model.users[0]);
								}
							}]
					};
					MessageService.dialog(params, model);
				}
			});
		}

		if ($scope.rights.jump) {
			iconset.push({
				icon: 'flip_to_back',
				onClick: function () {
					$scope.returnToStatusSheet();
				}
			});
		}

		//if ($scope.rights.jump) iconset = [];
		$scope.header = {
			title: $scope.selectedUserName,
			icons: iconset
		};

		// init and refresh
		function loadData(hideSpinner?, message?) {
			!hideSpinner && ViewService.lock(StateParameters.ViewId);
			return WorktimeTrackingService
				.getListOfMyTasksFromDateRangeFromDateRange(
					selections.dateRange.from,
					selections.dateRange.to,
					$scope.selectedUserId,
					$rootScope.me.itemId,
					$scope.rights.admin,
					StateParameters.AdditionalHours,
					StateParameters.AllowBasicHoursOnWeekends,
					$scope.isMobile)
				.then(function (data) {
					!hideSpinner && ViewService.unlock(StateParameters.ViewId);
					$scope.data = data;
					calculateWeeklyHours();
					checkIfHoursCanBeConfirmed();
					data.weekRights = decideRightsForWeek(data.originalHours);
					determineFootbarButtons();
					$scope.weeklyHours = data.weeklyHourTotal;
					if (message) $scope.data.sheetStatusMessage = message;
					$scope.enabledCache = {};
				});
		}

		function decideRightsForWeek(hours) {
			let foundStatuses = {};
			let status = 0;
			let submitted = false;
			let decided = false;
			let partiallySubmitted = false;
			_.each(hours, function (h) {
				if (h.line_manager_status === 0) {
					submitted = false;
				} else if (h.line_manager_status === 0.5) {
					partiallySubmitted = true;
				} else if (h.line_manager_status === 1 || !h.line_manager_status) {
					status = 1;
					foundStatuses[1] = true;
					submitted = true;
					decided = false;
				}
				if (h.line_manager_status === 2 && status === 0) {
					status = 2;
					foundStatuses[2] = true;
					decided = true;
				}
				if (h.line_manager_status === 3 && status === 0) {
					status = 3;
					foundStatuses[3] = true;
					decided = true;
				}
			});

			let rights = {
				isSubmitted: false,
				canReject: false,
				canApprove: false,
				isDecided: false,
				partiallySubmitted: partiallySubmitted
			};

			if (!(selections.dateRange.to.isAfter($scope.worktimeLimit) ||
				selections.dateRange.from.isAfter($scope.worktimeLimit))) return rights;

			rights.isSubmitted = submitted;
			rights.isDecided = decided;

			if (status === 0) {
				rights.canReject = false;
				rights.canApprove = false;
			} else if (status === 1) {
				rights.canReject = $scope.rights.canReject;
				rights.canApprove = $scope.rights.canApprove;
			} else if (status === 2) {
				rights.canReject = $scope.rights.canReject;
				rights.canApprove = false;
			}
			return rights;
		}

		function setFormattedDateRange() {
			$scope.formattedDateRange.from = CalendarService.formatDate(selections.dateRange.from);
			$scope.formattedDateRange.to = CalendarService.formatDate(selections.dateRange.to.clone().startOf('day'));
			$scope.header.title = $scope.selectedUserName + " (" + $scope.formattedDateRange.from + " - " + $scope.formattedDateRange.to + ")";
			StateParameters.activeTitle = $scope.selectedUserName;
			StateParameters.activeSettings = {};
			StateParameters.activeSettings.selectedUserId = $scope.selectedUserId;
			StateParameters.activeSettings.selectedUserName = $scope.selectedUserName;
		}

		function getActiveTasks(partial = false) {
			let payload = [];
			angular.forEach($scope.data.hours, function (tasks) {
				angular.forEach(tasks, function (task, taskId) {
					let o = {
						TaskId: taskId,
						UserId: $scope.selectedUserId,
						From: selections.dateRange.from.format('YYYY-MM-DD'),
						To: selections.dateRange.to.clone().startOf('day').format('YYYY-MM-DD')
					};

					if (partial && isPartial(moment(Object.keys(task)[0], 'DDMMYYYY')) || !partial)
						payload.push(o);
				});
			});
			return payload;
		}

		function getSplitActiveTasks() {
			let left = [];
			let right = [];
			let map = getPartialDayMap();
			angular.forEach($scope.data.hours, function (tasks) {
				angular.forEach(tasks, function (task, taskId) {
					left.push({
						TaskId: taskId,
						UserId: $scope.selectedUserId,
						From: map.leftStart.format('YYYY-MM-DD'),
						To: map.leftEnd.format('YYYY-MM-DD')
					});

					right.push({
						TaskId: taskId,
						UserId: $scope.selectedUserId,
						From: map.rightStart.format('YYYY-MM-DD'),
						To: map.rightEnd.format('YYYY-MM-DD')
					});
				});
			});
			return {left: left, right: right}
		}

		// scope functions


		$scope.returnToStatusSheet = function () {
			ViewService.view('worktimeHourStatus');
		};

		$scope.addTask = function (taskId) {
			WorktimeTrackingService.addTasksForMyList($scope.selectedUserId, taskId)
				.then(function () {
					loadData(true);
				});
		};

		$scope.changeUser = function (userItemId) {
			WorktimeTrackingService.saveChanges($scope.selectedUserId, pendingComments, $scope.data).then(function () {
				UserService.getUser(userItemId).then(function (user) {
					$scope.selectedUserId = user.item_id;
					$scope.selectedUserName = user.name;
					//$scope.header.title = user.name + " - " + $scope.formattedDateRange.from + " - " + $scope.formattedDateRange.from;
					setFormattedDateRange();
					$scope.calendars = {};
					$scope.vacations = {};
					loadCalendars().then(function () {
						loadData();
					});
				});
			});
		};

		$scope.submit = function (partial = false) {
			let customMessage = "";
			let t = "";
			_.each(getActiveTasks(partial), function (task) {
				let id = task.TaskId;
				_.each($scope.data.basicTasks, function (basicTask) {
					if (basicTask.item_id == id && basicTask.submission_warning != null) {
						customMessage = " " + basicTask.submission_warning;
						t = "warn";
						return;
					}
				});
				if (customMessage !== '') return;
			});

			if ($scope.data.weeklyHoursError && !partial) {
				customMessage = " " + Translator.translate("WORKINGTIME_TRACKING_SUBMISSION_QUOTA");
				t = 'warn';
			}

			let msg = Translator.translate('WORKINGTIME_TRACKING_CONFIRM_DATES') + ' ' + $scope.formattedDateRange.from + ' - ' + $scope.formattedDateRange.to + "?" + customMessage;

			if (partial)
				msg = Translator.translate('WORKINGTIME_TRACKING_PARTIAL_DATES') + ' ' + $scope.formattedDateRange.from + ' - ' + CalendarService.formatDate(selections.dateRange.from.clone().endOf('month').toDate()) + "?" + customMessage;


			MessageService.confirm(
				msg,
				function () {
					$scope.data.edit = false;
					$scope.data.canConfirm = false;
					$scope.data.canRevert = false;

					return WorktimeTrackingService.submitTasks($scope.selectedUserId, getActiveTasks(partial), partial)
						.then(function () {
							loadData(true);
						});
				}, t
			);
		};

		$scope.revert = function () {
			MessageService.confirm(
				Translator.translate('WORKINGTIME_TRACKING_REVERT_DATES') + ' ' + $scope.formattedDateRange.from + ' - ' + $scope.formattedDateRange.to + "?",
				function () {
					$scope.data.edit = false;
					$scope.data.canConfirm = false;
					$scope.data.canRevert = false;

					if ($scope.partialSubmissionsInUse && !$scope.data.weekRights.partiallySubmitted && isPartialWeek()) {
						//Two reverts
						let activeTasks = getSplitActiveTasks();
						let promises = [];

						promises.push(WorktimeTrackingService.revertTasks($scope.selectedUserId, activeTasks.left, 0.5));
						promises.push(WorktimeTrackingService.revertTasks($scope.selectedUserId, activeTasks.right, 0));

						return $q.all(promises).then(function () {
							loadData(true);
						});
					} else {
						//Full revert
						return WorktimeTrackingService.revertTasks($scope.selectedUserId, getActiveTasks(false))
							.then(function () {
								loadData(true);
							});
					}
				}
			);
		};

		$scope.approve = function () {
			let m = 'WORKINGTIME_TRACKING_APPROVE_DATES';
			let t = undefined;
			if ($scope.data.weeklyHoursError) {
				m = 'WORKINGTIME_TRACKING_APPROVE_DATES_WARNING';
				t = 'warn';
			}

			MessageService.confirm(
				Translator.translate(m) + ' ' + $scope.formattedDateRange.from + ' - ' + $scope.formattedDateRange.to + "?",
				function () {
					return WorktimeTrackingService.approveTasks($scope.selectedUserId, getActiveTasks())
						.then(function () {
							loadData(true);
						});
				}, t);
		};

		$scope.reject = function () {
			MessageService.confirm(
				Translator.translate('WORKINGTIME_TRACKING_REJECT_DATES') + ' ' + $scope.formattedDateRange.from + ' - ' + $scope.formattedDateRange.to + "?",
				function () {
					return WorktimeTrackingService.rejectTasks($scope.selectedUserId, getActiveTasks())
						.then(function () {
							loadData(true, Translator.translate('TIMESHEET_REJECTED'));
						});
				});
		};

		//Check that timeframe has end of month on mon - thu
		$scope.partialDays = 0;

		function isPartialWeek() {
			let now = selections.dateRange.from.clone();
			let endOf = selections.dateRange.from.clone().endOf('month');
			let days = 0;
			while (now.isSameOrBefore(selections.dateRange.to)) {
				days += 1;
				if (now.isSame(endOf, 'day') && [0, 5, 6].indexOf(now.day()) == -1) {
					$scope.partialDays = days;
					return true;
				}
				now.add(1, 'days');
			}
			return false;
		}

		function getPartialDayMap() {
			let now = selections.dateRange.from.clone();
			let endOf = selections.dateRange.from.clone().endOf('month');
			let isLeft = true;
			let r = {
				leftStart: undefined,
				leftEnd: undefined,
				rightStart: undefined,
				rightEnd: undefined
			}
			while (now.isSameOrBefore(selections.dateRange.to)) {
				if (!r.leftStart && isLeft) r.leftStart = now.clone();
				if (isLeft) r.leftEnd = now.clone();
				if (!r.rightStart && !isLeft) r.rightStart = now.clone();
				if (!isLeft) r.rightEnd = now.clone();

				if (now.isSame(endOf, 'day') && [0, 5, 6].indexOf(now.day()) == -1)
					isLeft = false;

				now.add(1, 'days');
			}
			return r;
		}

		function partialWeekTotal() {
			let result = 0;
			angular.forEach($scope.data.totalHours, function (task, i) {
				for (let t of Object.keys(task)) {
					if (isPartial(moment(t, 'DDMMYYYY'))) result += task[t].hours;
				}
			});
			return result;
		}

		function isPartial(m) {
			let endOf = selections.dateRange.from.clone().endOf('month');
			return m.isSameOrBefore(endOf);
		}

		function checkIfHoursCanBeConfirmed() {
			$scope.validation = [];

			let doneHours = $scope.data.weeklyTotal;
			calculateWeeklyHours();
			if ($scope.fullWeekRequired && doneHours < $scope.data.weeklyHourTotal) {
				if (!$scope.rights.admin) $scope.data.canConfirm = false;
				if ($scope.rights.limitedAdmin) $scope.data.canConfirm = false;
				$scope.data.weeklyHoursError = true;
				$scope.validation.push('WORKINGTIME_TRACKING_MISSING_WEEKLY_HOURS');
			} else if ($scope.fullWeekRequired && doneHours > $scope.data.weeklyHourTotal) {
				if (!$scope.rights.admin) $scope.data.canConfirm = false;
				if ($scope.rights.limitedAdmin) $scope.data.canConfirm = false;
				$scope.data.weeklyHoursError = true;
				$scope.validation.push('WORKINGTIME_TRACKING_TOOMANY_WEEKLY_HOURS');
			} else {
				$scope.data.weeklyHoursError = false;
			}

			$scope.data.canConfirmPartial = isPartialWeek();

			let dailyHoursCapped = Common.isTrue(StateParameters.DailyHoursCapped);
			let dailyMaximumAdditionalHours = StateParameters.DailyMaximumAdditionalHours;

			let errorsFound = false;
			if (dailyHoursCapped) {
				_.each($scope.data.sumOfAll, function (item) {
					item.error = {};
					let ho = CalendarService.getUserWorkHours($scope.selectedUserId);
					item.error.hours = item.hours > ho.timesheetHours;
					if (dailyMaximumAdditionalHours > 0) {
						item.error.additionalHours = item.additionalHours > dailyMaximumAdditionalHours;
					}
					if (item.error.hours) {
						errorsFound = true;
						$scope.data.canConfirmPartial = false;
						$scope.validation.push('WORKINGTIME_TRACKING_DAILY_HOURS_EXCEEDED');
					}
					if (item.error.additionalHours) {
						errorsFound = true;
						$scope.data.canConfirmPartial = false;
						$scope.validation.push('WORKINGTIME_TRACKING_DAILY_ADDITIONAL_HOURS_EXCEEDED');
					}
					if ($scope.data.canConfirmPartial && (ho.weeklyHours / ho.timesheetHours >= 5)) {
						if (partialWeekTotal() != ho.timesheetHours * $scope.partialDays) {
							$scope.data.canConfirmPartial = false;
							$scope.validation.push('WORKINGTIME_TRACKING_PARTIAL_HOURS_DONOTMATCH');
						}
					}
				});
			}
			if (errorsFound) $scope.data.canConfirm = false;


			$scope.validation = _.uniq($scope.validation);
		}

		$scope.hoursChanged = function (item) {
			WorktimeTrackingService.changeHours(item, $scope.data);
			checkIfHoursCanBeConfirmed();
			SaveService.tick();
		};

		$scope.back = function () {
			moveDates($scope.isMobile ? -1 : -7);
		};

		$scope.forward = function () {
			moveDates($scope.isMobile ? 1 : 7);
		};

		$scope.jumpTo = function (jumpto) {
			if (WorktimeTrackingService.isChangesPending()) {
				return;
			}

			selections.dateRange.from = moment(jumpto);
			selections.dateRange.to = moment(jumpto);

			if (!$scope.isMobile) {
				selections.dateRange.from.startOf($scope.localeWeekSelector);
				selections.dateRange.to.endOf($scope.localeWeekSelector)
			}

			setFormattedDateRange();
			loadData();
		};

		$scope.isWeekDay = function (weekend) {
			if ($scope.allowBasicHoursOnWeekends == true) return true;
			let d = moment(weekend);
			return !(d.day() == 0 || d.day() == 6);
		};

		function moveDates(days) {
			if (WorktimeTrackingService.isChangesPending()) {
				return;
			}

			selections.dateRange.from.add(days, 'days');
			selections.dateRange.to.add(days, 'days');
			setFormattedDateRange();
			loadData();
		}

		$scope.changedComments = function (itemId) {
			if (pendingComments.indexOf(itemId) === -1) {
				pendingComments.push(itemId);
			}
			SaveService.tick();
		};

		$scope.taskRemovable = function (task) {
			return !($scope.data.hours.task && $scope.data.hours.task[task.task_item_id])
		};

		$scope.removeTask = function (task) {
			if ($scope.taskRemovable(task)) {
				MessageService.confirm(Translator.translate("REMOVE_TASK").replace("@@", task.title.trimStart(" ")), function () {
					WorktimeTrackingService.removeTaskFromMyList(task.task_item_id, $scope.selectedUserId).then(function () {
						let i = $scope.data.myTasks.length;
						let parentId = 0;
						let parentInUse = false;
						while (i--) {
							if ($scope.data.myTasks[i].task_item_id === task.task_item_id) {
								parentId = $scope.data.myTasks[i].owner_item_id;
								$scope.data.myTasks.splice(i, 1);
								break;
							}
						}

						i = $scope.data.myTasks.length;
						while (i--) {
							if ($scope.data.myTasks[i].owner_item_id == parentId) {
								parentInUse = true;
								break;
							}
						}

						if (!parentInUse) {
							i = $scope.data.myTasks.length;
							while (i--) {
								if ($scope.data.myTasks[i].header == true) {
									if ($scope.data.myTasks[i].itemId == parentId) {
										$scope.data.myTasks.splice(i, 1);
										break;
									}
								}
							}
						}
					});
				});
			}
		};

		$scope.editTaskOnDialog = function (task, event, isBasic) {
			if (!$scope.isMobile || !$scope.data.edit) {
				return;
			}

			let column = {
				date: selections.dateRange.from.format('DDMMYYYY'),
				date2: selections.dateRange.from.format('YYYYMMDD'),
				field: 'hours'
			};

			let hourInput = {
				label: 'WORKINGTIME_TRACKING_BREAKDOWN_VALUE',
				type: 'decimal',
				max: 24,
				min: 0,
				step: '0.25',
				model: 'hours'
			};

			let process = isBasic ? 'worktime_tracking_basic_tasks' : task.process;
			let taskId = isBasic ? task.item_id : task.task_item_id;
			let dataPath = process + '.' + String(taskId) + '.' + column.date + '.' + column.field;

			let model = {
				hours: _.get($scope.data.hours, dataPath)
			};

			function onChange() {
				_.set($scope.data.hours, dataPath, model.hours);

				$scope.hoursChanged({
					taskId: taskId,
					column: column,
					process: process
				});
			}

			if (isBasic) {
				if ($scope.isWeekDay(column.date2)) {
					MessageService.dialog({
						title: task.list_item,
						inputs: [hourInput],
						buttons: [
							{text: 'CANCEL'},
							{
								text: 'SAVE',
								type: 'primary',
								onClick: onChange
							}
						]
					}, model);
				} else {
					MessageService.toast('WORKTIMETRACKING_WEEKEND_NOT_ALLOWED');
				}
			} else {
				if ($scope.isTaskEnabledOnThisDay(task, column)) {
					if ($scope.hourbreakdownIsOn) {
						if ((column.field === 'hours' && $scope.isWeekDay(column.date2)) ||
							column.field === 'additionalHours'
						) {
							$scope.clicked(task.process, task.task_item_id, column);
						} else {
							MessageService.toast('WORKTIMETRACKING_TASK_NOT_ALLOWED');
						}
					} else {
						MessageService.dialog({
							title: task.title,
							inputs: [hourInput],
							buttons: [
								{text: 'CANCEL'},
								{
									text: 'SAVE',
									type: 'primary',
									onClick: onChange
								}
							]
						}, model);
					}
				} else {
					MessageService.toast('WORKTIMETRACKING_TASK_NOT_ENABLED');
				}
			}
		};

		$scope.taskLockTitles = {};
		$scope.taskLockTitle = function (task) {
			return $scope.taskLockTitles[task.task_item_id];
		};
		$scope.isTaskLocked = function (task, dateValueColumns): boolean {
			let t: boolean = false;
			_.each(dateValueColumns, function (col) {
				let result: boolean = $scope.isTaskEnabledOnThisDay(task, col);
				if (result) t = true;
			});

			return t;
		};
		$scope.isBasicHourEditableOnThisDay = function (editable, col): boolean {
			let result = undefined;
			if ($scope.enabledCache[col.date2 + 0])
				result = $scope.enabledCache[col.date2 + 0];
			else {
				result = determineIfBasicHourEditableOnThisDay(editable, col);
				$scope.enabledCache[col.date2 + 0] = result;
			}
			return result;
		};

		$scope.enabledCache = {};
		$scope.isTaskEnabledOnThisDay = function (task, col): boolean {
			let result = undefined;
			if (typeof $scope.enabledCache[col.date2 + task.task_item_id] !== 'undefined') {
				result = $scope.enabledCache[col.date2 + task.task_item_id];
			} else {
				result = determineIfTaskIsEnabledOnThisDay(task, col);
				$scope.enabledCache[col.date2 + task.task_item_id] = result;
			}
			return result;
		};

		function determineIfBasicHourEditableOnThisDay(editable, col) {
			if (!(selections.dateRange.to.isAfter($scope.worktimeLimit) ||
				selections.dateRange.from.isAfter($scope.worktimeLimit))) return false;
			if ($scope.data.isPartial && isPartial(moment(col.date2))) return false;
			return editable;
		}

		function determineIfTaskIsEnabledOnThisDay(task, col) {
			const ps = task.fromDate; //Process Start
			const pe = task.toDate; //Process End
			const ts = task.start_date; //Task Start
			const te = task.end_date; //Task End
			let cd; //Current Day


			let tr = false;
			let pr = false;

			if (task.deleted === 1) {
				$scope.taskLockTitles[task.task_item_id] = Translator.translate("WORKINGTIME_TRACKING_TASK_REMOVED");
				return false;
			}

			if (task.locked === 1) {
				$scope.taskLockTitles[task.task_item_id] = Translator.translate("WORKINGTIME_TRACKING_TASK_PREVENTED");
				return false;
			}

			if (!(selections.dateRange.to.isAfter($scope.worktimeLimit) ||
				selections.dateRange.from.isAfter($scope.worktimeLimit))) return false;

			if ($scope.data.isPartial && isPartial(moment(col.date2))) return false;

			//Check task
			if (!$scope.AllowOutsideOfTasks) {
				cd = moment(col.date2);
				if (ts && te) tr = cd.isBetween(ts, te, null, '[]');
				if (ts && !te) tr = cd.isSameOrAfter(ts);
				if (!ts && te) tr = cd.isSameOrBefore(te);
			} else {
				tr = true;
			}

			//Check process
			cd = col.date2;
			if (ps && pe) pr = (ps <= cd && cd <= pe);
			if (ps && !pe) pr = (ps <= cd);
			if (!ps && pe) pr = (cd <= pe);
			if (!ps && !pe) pr = true;

			$scope.taskLockTitles[task.task_item_id] = Translator.translate("PROJECT") + ": " + CalendarService.formatDate(dateFromFormat(ps)) + " - " + CalendarService.formatDate(dateFromFormat(pe))
				+ ", " + Translator.translate("HOURACCUMULATION_TASK") + ": " + CalendarService.formatDate(ts) + " - " + CalendarService.formatDate(te);

			return pr && tr;
		}

		function dateFromFormat(f) {
			return moment(f).toDate();
		}

		// Drop magic
		$scope.$on('DragProcessStart', function (event, process) {
			if (process === 'task') {
				ViewService.backdrop(StateParameters.ViewId, {
					template: '<div droppable-process="onDrop" id="worktime-backdrop"><i class="material-icons" aria-hidden="true">keyboard_arrow_down</i><h1>Drop here</h1></div>'
				});
			}
		});

		$scope.$on('DragProcessStop', function (event, process) {
			if (process === 'task') {
				ViewService.removeBackdrop(StateParameters.ViewId);
			}
		});

		$scope.onDrop = function (process, itemIds) {
			let arrIds = _.isArray(itemIds) ? itemIds : [itemIds];
			let tasksToAdd = [];
			_.each(arrIds, function (itemId) {
				if (!_.find($scope.data.myTasks, {task_item_id: itemId})) {
					tasksToAdd.push(WorktimeTrackingService.addTasksForMyList($scope.selectedUserId, itemId));
				}
			});
			if (_.isEmpty(tasksToAdd)) {
				MessageService.toast('WORKINGTIME_TRACKING_TASK_EXISTS');
			} else {
				$q.all(tasksToAdd).then(loadData);
			}
		};

		$scope.toggleTaskGroup = function (task) {
			$scope.collapsedGroups[task.itemId] = !$scope.collapsedGroups[task.itemId];
		};
		$scope.allOpen = function () {
			if (!($scope.data && $scope.data.myTasks)) return false;
			let dir = false;
			for (let task of $scope.data.myTasks) {
				if ($scope.collapsedGroups[task.itemId] == true) {
					dir = true;
					break;
				}
			}
			return !dir;
		};
		$scope.toggleAll = function () {
			let dir = $scope.allOpen();
			for (let task of $scope.data.myTasks) $scope.collapsedGroups[task.itemId] = dir;
		};

		$scope.openAnalysis = function (row, type) {
			ProcessService.getProcess(type == 1 ? row.itemId : row.task_item_id).then(function (process) {
				SettingsService.getProcessFeatures(process).then(function (processFeatures) {
					let featureFound = _.findIndex(processFeatures, function (o) {
						return o.state == StateParameters.SplitFeature;
					});
					let p = {
						params: {
							itemId: type == 1 ? row.itemId : row.task_item_id,
							process: process
						}
					};
					let fallBackFeature = type == 1 ? 'process.hourAccumulation' : 'process.tab';
					ViewService.view(featureFound != -1 ? StateParameters.SplitFeature : fallBackFeature, p, {
						split: StateParameters.CustomWidth != 'full' ? true : false,
						size: StateParameters.CustomWidth
					});
				})
			});
		}

		$scope.$watch('collapsedGroups', function (newVal, oldVal) {
			LocalStorageService.store('worktime_tracking_collapsed_items', newVal);
		}, true);

		$scope.clicked = function (process, itemId, column) {
			$scope.data.hours[process] = $scope.data.hours[process] || {};
			$scope.data.hours[process][itemId] = $scope.data.hours[process][itemId] || {};
			$scope.data.hours[process][itemId][column.date] = $scope.data.hours[process][itemId][column.date] || {};
			let task = _.find($scope.data.myTasks, function (t) {
				return itemId === t.task_item_id;
			});
			$scope.selected = {
				taskName: task && task.title,
				column: column,
				taskId: itemId,
				process: process,
				editable: $scope.isTaskEnabledOnThisDay(task, column),
				breakdownHours: $scope.data.hours[process][itemId][column.date].breakdownHours,
				itemId: $scope.data.hours[process][itemId][column.date].itemId,
				weekDay: moment(column.date2).format('DD.MM.YYYY'),
				currentTotal: $scope.data.weeklyTotal,
				dailyTotal: $scope.data.sumOfAll[column.date],
				allowBasicHoursOnWeekends: $scope.allowBasicHoursOnWeekends
			};

			let params = {
				template: $scope.isMobile ?
					'pages/workTimeTracking/worktime-tracking-hour-breakdown-dialog-mobile.html' :
					'pages/workTimeTracking/worktime-tracking-hour-breakdown-dialog.html',
				controller: 'WorktimeTrackingHourBreakdownDialogController',
				locals: {
					saveCallback: saveBreakdown,
					ParentScope: $scope
				}
			};
			MessageService.dialogAdvanced(params);
		};

		function calculateHoursBybreakdownHours(process, taskId, date) {

			let item = _.get($scope.data.hours, process + '.' + taskId + '.' + date);
			let hours = 0;
			let additionalHours = 0;
			if (item) {
				_.each(item.breakdownHours, function (breakdownHoursItem) {
					hours += (+breakdownHoursItem.Hours) || 0;
					additionalHours += (+breakdownHoursItem.AdditionalHours) || 0;
				});
			}
			item.hours = hours;
			item.additionalHours = additionalHours;
		}

		function saveBreakdown() {
			let selected = $scope.selected;
			// clean array
			selected.breakdownHours = _.filter(selected.breakdownHours, function (row) {
				return row.Hours === 0 || row.Hours > 0 || row.AdditionalHours === 0 || row.AdditionalHours > 0;
			});

			_.set($scope.data.hours,
				selected.process + '.' + selected.taskId + '.' + selected.column.date + '.breakdownHours',
				selected.breakdownHours);

			//$scope.data.hours[selected.process][selected.itemId][selected.column.date].breakdownHours = selected.breakdownHours;
			calculateHoursBybreakdownHours(selected.process, selected.taskId, selected.column.date);
			$scope.hoursChanged({
				taskId: selected.taskId,
				column: selected.column,
				process: selected.process,
				breakdownHours: selected.breakdownHours,
				id: selected.itemId
			});
			$scope.selected = null;
		}

		setFormattedDateRange();
		loadCalendars().then(function () {
			loadData();
		});

		function determineFootbarButtons() {
			$scope.buttonOptions = [];
			if (!$scope.rights.jump && $scope.rights.admin && $scope.data.weekRights.isSubmitted) {
				$scope.buttonOptions.push({
					name: 'REVERT',
					type: 'warn',
					onClick: $scope.revert
				});
			}

			if (!$scope.rights.jump && $scope.data.canConfirm) {
				$scope.buttonOptions.push({
					name: 'SUBMIT',
					type: 'primary',
					onClick: $scope.submit
				});
			}

			if ($scope.rights.jump && $scope.data.weekRights.canReject) {
				$scope.buttonOptions.push({
					name: 'REJECT',
					type: 'warn',
					onClick: $scope.reject
				});
			}

			if ($scope.rights.jump && $scope.data.weekRights.canApprove) {
				$scope.buttonOptions.push({
					name: 'APPROVE',
					type: 'primary',
					onClick: $scope.approve
				});
			}
		}
	}
})
();