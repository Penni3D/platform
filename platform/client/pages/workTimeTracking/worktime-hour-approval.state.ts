(function () {
	'use strict';


	angular
		.module('core.pages')
		.config(WorktimeHourApprovalConfig)
		.service('WorktimeHourApprovalService', WorktimeHourApprovalService)
		.controller('WorktimeHourApprovalController', WorktimeHourApprovalController);
		
	WorktimeHourApprovalConfig.$inject = ['ViewsProvider'];
	function WorktimeHourApprovalConfig(ViewsProvider){
		ViewsProvider.addView('worktimeHourApproval', {
			controller: 'WorktimeHourApprovalController',
			template: 'pages/workTimeTracking/worktime-hour-approval.html'
		});

		ViewsProvider.addPage('worktimeHourApproval', 'PAGE_WORKTIME_HOUR_APPROVAL', ['read', 'write']);
	}

	WorktimeHourApprovalController.$inject = [
		'$scope',
		'Translator',
		'WorktimeHourApprovalService',
		'Common',
		'CalendarService',
		'ViewService',
		'StateParameters',
		'$compile',
		'Configuration',
		'UserService'
	];
	function WorktimeHourApprovalController(
		$scope,
		Translator,
		WorktimeHourApprovalService,
	    Common,
		CalendarService,
		ViewService,
		StateParameters,
		$compile,
		Configuration,
		UserService
	) {
		$scope.hourbreakdownIsOn = Common.isTrue(StateParameters.HourBreakdown);
		$scope.additionalHoursInUse = Common.isTrue(StateParameters.AdditionalHours);
		$scope.weekDays = Common.getLocalShortDaysOfWeek();

		let selections = {
			dateRange: {
				from: moment.utc().startOf('isoWeek'),
				to: moment.utc().endOf('isoWeek')
			}
		};

		$scope.header = {
			title: 'Worktime Hour Approval'
		};

		$scope.formattedDateRange = {};

		function setFormattedDateRange() {
			$scope.formattedDateRange.from = CalendarService.formatDate(selections.dateRange.from);
			$scope.formattedDateRange.to = CalendarService.formatDate(selections.dateRange.to);
		}

		$scope.back = function () {
			selections.dateRange.from.add(-7, 'days');
			selections.dateRange.to.add(-7, 'days');
			setFormattedDateRange();
			loadData();
		};

		$scope.forward = function () {
			selections.dateRange.from.add(7, 'days');
			selections.dateRange.to.add(7, 'days');
			setFormattedDateRange();
			loadData();
		};
		
		$scope.changeStatus = function (item, method) {
			let itemToSend = [{
				TaskId: +item.task_item_id || 0,
				UserId: +item.user_item_id,
				From: _.first($scope.dates).itemKey,
				To: _.last($scope.dates).itemKey
			}];
			WorktimeHourApprovalService
				.changeStatus(method, itemToSend)
				.then(loadData);
		};

		setFormattedDateRange();

		function updateDatesInView(){
			let from = selections.dateRange.from;
			let to = selections.dateRange.to;
			let listOfDates = [];
			let dateCursor = from.clone().startOf('day');
			while (dateCursor.isBefore(to)){
				listOfDates.push({
					key: getDayAndMonthFromMoment(dateCursor),
					itemKey: dateCursor.format('YYYY-MM-DD')
				});
				dateCursor.add(1, 'days');
			}
			$scope.dates = listOfDates;
		}

		function getDayAndMonthFromMoment(momentObject){
			return momentObject.format('DDMMYYYY');
		}
		
		$scope.approveAllForUser = function (userId) {
			let items = $scope.itemsByUsers[userId];
			if (_.size(items) === 0){
				return;
			}
			let userIds = [userId];
			let firstDate = _.first(items).date;
			let lastDate = _.last(items).date;
			let taskIds = _.chain(items)
				.filter({line_manager_status: 1})
				.map('task_item_id')
				.uniq()
				.value();
			WorktimeHourApprovalService.approve(taskIds,  userIds,  firstDate, lastDate)
				.then(loadData)
		};
		
		function setData(data){
			updateDatesInView();
			
			_.each(data.approvable, function(item){
				item.$date = moment(item.date).format('DDMMYYYY');
			});
			_.each(data.approvedForCurrentWeek, function(item){
				item.$date = moment(item.date).format('DDMMYYYY');
			});
			let approvableHoursByUserId = _.groupBy(data.approvable, 'user_item_id');
			let approvedHoursByUserId = _.groupBy(data.approvedForCurrentWeek, 'user_item_id');

			let userIds = _.uniq(_.concat(_.keys(approvableHoursByUserId), _.keys(approvedHoursByUserId)));
						
			let onlyHeadersFound = true;
			let usersById = _.groupBy(data.users, 'item_id');

			$scope.rowitems = [];
						
			_.each(userIds, function (userId) {
				let userName = _.get(usersById, userId + '.0.first_name') + ' ' + _.get(usersById, userId + '.0.last_name');
				$scope.rowitems.push({
					header: true,
					user_item_id: userId,
					user_name: userName
				});
				let rowItem = {
					header: false,
					user_item_id: userId,
					user: userId,
					user_name: userName
				};
				let datesForApproved = [];
				let datesForRejected = [];
				let datesForApprovable =[];
				let addApproved = false, addRejected = false, addReady = false;
				_.each($scope.dates, function (date) {
					let sumForApprovable = 0; let sumForApproved = 0, sumForRejected = 0;
					_.each(approvableHoursByUserId[userId], function (dayItem) {
						if (date.key === dayItem.$date) {
							let hours = dayItem.hours || 0;
							let additionalHours = dayItem.additional_hours || 0;
							sumForApprovable = sumForApprovable + hours + additionalHours;
							addReady = true;
						}
					});
					
					_.each(approvedHoursByUserId[userId], function (dayItem) {
						if (date.key === dayItem.$date){
							let hours = dayItem.hours || 0;
							let additionalHours = dayItem.additional_hours || 0;
							if (dayItem.line_manager_status === 2){
								sumForApproved = sumForApproved + hours + additionalHours;
								addApproved = true;
							} else if(dayItem.line_manager_status === 3){
								sumForRejected = sumForRejected + hours + additionalHours;
								addRejected = true;
							}
						}
					});
					datesForApprovable.push(
						{
							hours:sumForApprovable,
							line_manager_status: 1
						});
					datesForApproved.push(
						{
							hours:sumForApproved,
							line_manager_status: 2
						});
					datesForRejected.push({
						hours:sumForRejected,
						line_manager_status: 3
					});
				});
				if (addReady) {
					$scope.rowitems.push(
						_.extend(
							{},
							rowItem,
							{rejectable: true, approvable: true, revertable: false, dates: datesForApprovable}
						));
				}
				if (addApproved) {
					$scope.rowitems.push(
						_.extend(
							{},
							rowItem,
							{rejectable: true, approvable: false, revertable: true,dates: datesForApproved}
						));
				}
				if (addRejected) {
					$scope.rowitems.push(
						_.extend(
							{},
							rowItem,
							{rejectable: false, approvable: true, revertable: true, dates: datesForRejected}
						));
				}
				if (addRejected || addApproved || addReady){
					onlyHeadersFound = false;
				}
			});
			if (onlyHeadersFound){
				$scope.rowitems = [{
					header: true,
					user_name: Translator.translate('Nothing to show')
				}];
			}
		}

		$scope.showUser = function(rowuser) {
            UserService.getUser(rowuser.user_item_id).then(function (user) {
                ViewService.view('worktimeTracking',
                    {
                        params: {
                            userId: user.item_id,
                            userCalendarId: user.calendar_id,
                            userName: user.name,
                            fromDate: selections.dateRange.from,
                            toDate: selections.dateRange.to
                        }
                    },
                    {
                        split: true
                    });
            });
		};

		// init and refresh
		function loadData() {
			ViewService.lock(StateParameters.ViewId);
			WorktimeHourApprovalService.getListOfTasksToApprove(
				getDayAndMonthFromMoment(selections.dateRange.from),
				getDayAndMonthFromMoment(selections.dateRange.to))
				.then(function(data) {
					ViewService.unlock(StateParameters.ViewId);
					setData(data);
				});
		}

		loadData();
	}

	WorktimeHourApprovalService.$inject = ['ApiService'];
	function WorktimeHourApprovalService(ApiService){
		let WorkTimeTracking = ApiService.v1api('time/worktimetracking/linemanager');
		
		let getApprovalObject = function(taskId, userId, from, to){
			return {
				TaskId: taskId,
				UserId: userId,
				from: from, 
				to: to
			};
		};
		
		let buildApprovalObjectList = function (method, taskIds, userIds, from, to) {
			let itemsToSend = [];
			_.each(taskIds,  function (taskId) {
				_.each(userIds, function (userId) {
					itemsToSend.push(getApprovalObject(taskId,  userId,  from,  to));
				});
			});
			return changeStatus(method,  itemsToSend);
		};
				
		let approve = function (taskIds, userIds, from, to) {
			return buildApprovalObjectList('approve', taskIds,  userIds,  from,  to);
		};

		let reject = function (taskIds, userIds, from, to) {
			return buildApprovalObjectList('reject', taskIds,  userIds,  from,  to);
		};

		let restoreToReady = function (taskIds, userIds, from, to) {
			return buildApprovalObjectList('restoreconfirmed', taskIds,  userIds,  from,  to);
		};
		
		let getListOfTasksToApprove = function (from, to) {
			return WorkTimeTracking.get(['approvable', from,  to]);
		};
		
		let changeStatus = function (method, items) {
			return WorkTimeTracking.new([method], items);
		};
		
		
		return {
			approve: approve,
			reject: reject,
			restoreToReady: restoreToReady, 
			getListOfTasksToApprove: getListOfTasksToApprove,
			changeStatus: changeStatus 
		}
	}
})();