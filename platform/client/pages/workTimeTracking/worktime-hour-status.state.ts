(function () {
	'use strict';


	angular
		.module('core.pages')
		.config(WorktimeHourApprovalConfig)
		.service('WorktimeHourStatusService', WorktimeHourStatusService)
		.controller('WorktimeHourStatusController', WorktimeHourStatusController);

	WorktimeHourApprovalConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function WorktimeHourApprovalConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('worktimeHourStatus', {
			controller: 'WorktimeHourStatusController',
			template: 'pages/workTimeTracking/worktime-hour-status.html',
			resolve: {
				WorkTimeRights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('worktimeHourStatus').then(function (states) {
						return {
							download: _.includes(states, 'download'),
							email: _.includes(states, 'email'),
							approveAll: _.includes(states, 'approveAll'),
							rebase: _.includes(states, 'rebase'),
							rebaseAll: _.includes(states, 'rebaseAll'),
							viewall: _.includes(states, 'viewall')
						};
					});
				},
				ViewMode: function () {
					return "statuspage";
				},
				WidgetShowAll: function () {
					return false;
				},
				PortfolioColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioId) {
						return PortfolioService.getPortfolioColumns('user', StateParameters.FilteringPortfolioId);
					} else {
						return [];
					}
				},
				Sublists: function (ProcessService, StateParameters) {
					return ProcessService.getSublists('user', StateParameters.FilteringPortfolioId);
				}
			}
		});

		ViewsProvider.addPage('worktimeHourStatus', 'PAGE_WORKTIME_HOUR_STATUS', ['download', 'email', 'approveAll', 'rebase', 'rebaseAll', 'viewall']);
		ViewConfiguratorProvider.addConfiguration('worktimeHourStatus', {
			global: true,
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('user');
				},
				ItemColumns: function (ProcessService) {
					return ProcessService.getColumns('worktime_tracking_hours');
				}
			},
			tabs: {
				default: {
					FilteringPortfolioId: function (resolves) {
						let portfolios = _.filter(resolves.Portfolios, {ProcessPortfolioType: 0});
						return {
							label: 'WT_STATUS_FILTERING_PORTFOLIO',
							type: 'select',
							options: portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					additionalColumnId: function (resolves) {
						return {
							type: 'multiselect',
							label: 'ADDITIONAL_COLUMNS',
							options: resolves.ItemColumns,
							optionName: 'LanguageTranslation',
							optionValue: 'ItemColumnId'
						};
					}
				}
			}
		});
	}

	WorktimeHourStatusController.$inject = [
		'$scope',
		'$rootScope',
		'Translator',
		'WorktimeHourStatusService',
		'Common',
		'CalendarService',
		'ViewService',
		'StateParameters',
		'$compile',
		'Configuration',
		'MessageService',
		'UserService',
		'WorkTimeRights',
		'PortfolioColumns',
		'ViewMode',
		'PortfolioService',
		'LocalStorageService',
		'SidenavService',
		'WidgetShowAll',
		'ApiService'
	];

	function WorktimeHourStatusController(
		$scope,
		$rootScope,
		Translator,
		WorktimeHourStatusService,
		Common,
		CalendarService,
		ViewService,
		StateParameters,
		$compile,
		Configuration,
		MessageService,
		UserService,
		WorkTimeRights,
		PortfolioColumns,
		ViewMode,
		PortfolioService,
		LocalStorageService,
		SidenavService,
		WidgetShowAll,
		ApiService
	) {
		$scope.rights = WorkTimeRights;
		$scope.getRows = function () {
			loadData();
		};
		$scope.showStatus1 = function () {
			return Translator.translate("TIMESHEET_STATUS_1") != "Disabled";
		};
		$scope.showStatus05 = function () {
			return Translator.translate("TIMESHEET_STATUS_01") != "Disabled";
		};

		$scope.userFilterOptions = [
			{name: 'My team', value: 0},
			{name: 'Own', value: 1}
		];
		if ($scope.rights.viewall) $scope.userFilterOptions.push({name: 'All', value: 2});

		if (StateParameters.FilteringPortfolioId && $scope.rights.viewall) {
			$scope.userFilterOptions.push({name: 'Filtered', value: 3});
		} else if (_.get($scope.selections, 'selectValue') === 3) {
			$scope.selections.selectValue = 1;
			$scope.portfolioFilterHidden = true;
		}

		$scope.statusFilterOptions = [];
		$scope.statusFilterOptions.push({name: 'All', value: -1});
		$scope.statusFilterOptions.push({name: 'TIMESHEET_STATUS_0', value: 0, icon: 'lens', color: 'grey'});
		if ($scope.showStatus05()) $scope.statusFilterOptions.push({
			name: 'TIMESHEET_STATUS_01',
			value: -2,
			icon: 'lens',
			color: '#616161'
		});
		if ($scope.showStatus1()) $scope.statusFilterOptions.push({
			name: 'TIMESHEET_STATUS_1',
			value: 1,
			icon: 'lens',
			color: 'yellow'
		});
		$scope.statusFilterOptions.push({name: 'TIMESHEET_STATUS_2', value: 2, icon: 'lens', color: 'green'});


		$scope.navigationMenuId = StateParameters.NavigationMenuId;
		$scope.portfolioId = StateParameters.FilteringPortfolioId;
		$scope.portfolioColumns = PortfolioColumns;

		$scope.localeWeekSelector = 'week';
		$scope.users = [];
		$scope.data = [];
		$scope.weeks = [];
		$scope.emailSelections = {};
		let myTeamUserIds = [];

		if (+$rootScope.clientInformation.first_day_of_week === 1) {
			$scope.localeWeekSelector = 'isoWeek';
		}

		//Check first from stateparameters
		let f;
		let t;
		if (typeof StateParameters.fromDate === 'undefined' && typeof StateParameters.toDate === 'undefined') {
			//Then Check from service
			if (typeof WorktimeHourStatusService.persistentSettings.fromDate === 'undefined' && typeof WorktimeHourStatusService.persistentSettings.toDate === 'undefined') {
				//Not set anywhere
				f = moment().startOf('month').add(-1, 'M');
				t = moment().endOf('month');
			} else {
				f = moment(WorktimeHourStatusService.persistentSettings.fromDate);
				t = moment(WorktimeHourStatusService.persistentSettings.toDate);
			}
		} else {
			f = moment(StateParameters.fromDate);
			t = moment(StateParameters.toDate);
		}


		if (ViewMode == 'widget') {
			f = moment().startOf('month').add(-1, 'M');
			t = moment().endOf('month');
		}

		$scope.selections = {
			dateRange: {
				from: f,
				to: t
			},
			excelRange: {
				from: f,
				to: t
			},
			resolution: 1,
			filterValue: -1,
			selectValue: 0
		};

		if (ViewMode == 'widget') $scope.selections.filterValue = 1;
		$scope.isWidget = ViewMode == 'widget';
		$scope.widgetShowAll = WidgetShowAll;
		WorktimeHourStatusService.persistentSettings.fromDate = f;
		WorktimeHourStatusService.persistentSettings.toDate = t;

		$scope.selectValue = 0;
		$scope.selectUser = function () {
			_.each($scope.users, function (user) {
				if ($scope.getVisibility(user.UserItemId) && $scope.emailSelectionsAll) {
					$scope.emailSelections[user.UserItemId] = $scope.emailSelectionsAll;
				} else {
					delete $scope.emailSelections[user.UserItemId];
				}
			});
		};

		$scope.rebaseAllTimesheets = function () {
			MessageService.confirm("TIMESHEET_REBASE_ALL", function () {
				WorktimeHourStatusService.setRebaseDate(new Date()).then(function (result) {
					MessageService.MsgBox("TIMESHEET_REBASE_ALL_DONE");
				});
			});
		};

		$scope.rebaseTimesheets = function () {
			let a = [];
			angular.forEach($scope.emailSelections, function (email, user_item_id) {
				a.push(user_item_id);
			});

			if (a.length > 0) {
				MessageService.confirm("TIMESHEET_REBASE", function () {
					WorktimeHourStatusService.rebase(a).then(function (result) {
						if (result == true) loadData();
					});
				});
			} else {
				MessageService.msgBox("TIMESHEET_NOSELECTIONS");
			}
		};

		$scope.sendEmail = function () {
			let model = {text: Translator.translate("WORKINGTIME_TRACKING_EMAIL_DEFAULT")};
			let scope = $rootScope.$new();

			let params = {
				scope: scope,
				title: 'WORKINGTIME_TRACKING_EMAIL_TITLE',
				inputs: [
					{
						type: 'text',
						label: 'WORKINGTIME_TRACKING_EMAIL_BODY',
						placeholder: Translator.translate('WORKINGTIME_TRACKING_EMAIL_DEFAULT'),
						model: 'text'
					}],
				buttons: [
					{
						type: 'secondary',
						text: 'MSG_CANCEL'
					},
					{
						type: 'primary',
						text: Translator.translate("WORKINGTIME_TRACKING_EMAIL_SEND_LM"),
						onClick: function () {
							let a = [];
							angular.forEach($scope.emailSelections, function (email, user_item_id) {
								if (email) a.push(user_item_id);
							});

							WorktimeHourStatusService.sendEmail(model.text, a, true).then(function (result) {
								if (result == false) {
									MessageService.toast("WORKINGTIME_TRACKING_EMAIL_FAIL");
								} else {
									MessageService.toast("WORKINGTIME_TRACKING_EMAIL_OK");
								}
							}, function (data) {
								MessageService.toast("WORKINGTIME_TRACKING_EMAIL_FAIL");
							});
						}
					},
					{
						type: 'primary',
						text: Translator.translate("WORKINGTIME_TRACKING_EMAIL_SEND"),
						onClick: function () {
							let a = [];
							angular.forEach($scope.emailSelections, function (email, user_item_id) {
								if (email) a.push(user_item_id);
							});
							WorktimeHourStatusService.sendEmail(model.text, a, false).then(function (result) {
								MessageService.msgBox(result);
							});
						}
					}]
			};
			MessageService.dialog(params, model);
		};

		function cleanInvisibleSelections() {
			let userIdsToSpare = {};
			_.each($scope.users, function (user) {
				if ($scope.getVisibility(user.UserItemId)) {
					userIdsToSpare[user.UserItemId] = true;
				}
			});
			_.each(_.keys($scope.emailSelections), function (id) {
				if (!userIdsToSpare[id]) {
					delete $scope.emailSelections[id];
				}
			});
		}

		$scope.reloadNavigation = function () {
			decideIfFiltersAreShown();
		};

		function decideIfFiltersAreShown() {
			if ($scope.selections && $scope.selections.selectValue === 3) {
				$scope.showPortfolioFilters = true;
				$scope.activeFilters = PortfolioService.getActiveSettings($scope.portfolioId);
				PortfolioService.getSavedSettings($scope.portfolioId).then(function (s) {
					setNavigation(s, $scope.portfolioId);
				});
			} else {
				$scope.showPortfolioFilters = false;
			}
		}

		function setNavigation(settings, pid) {
			let sidenav = [];
			let publicTabs = [];
			let myTabs = [];
			angular.forEach(settings, function (s) {
				if (s.Value.showInMenu) {
					let t = {
						hideSplit: true,
						name: Translator.translation(s.Name),
						menu: undefined,
						$$activeTab: undefined,
						onClick: function () {
							angular.forEach(s.Value.filters, function (value, key) {
								$scope.activeFilters.filters[key] = angular.copy(value);
							});
							$scope.changeScope(true);
						}
					};

					if (s.UserId == UserService.getCurrentUserId()) {
						t.menu = [{
							icon: 'settings',
							name: 'CONFIGURE',
							onClick: function () {
								ViewService.view('configure.portfolioSimple', {
									params: {
										NavigationMenuId: $scope.navigationMenuId,
										portfolioId: pid,
										isNew: false
									},
									locals: {
										CurrentSettings: s,
										ReloadNavigation: $scope.reloadNavigation,
										RefreshParent: function(){
											return;
										}
									}
								});
							}
						}];

						myTabs.push(t);
					} else {
						publicTabs.push(t);
					}
				}
			});

			if (myTabs.length) {
				sidenav.push({name: 'PORTFOLIO_SAVED', showGroup: true, tabs: myTabs, expand: true});
			}

			if (publicTabs.length) {
				sidenav.push({name: 'PORTFOLIO_SAVED_PUBLIC', showGroup: true, tabs: publicTabs, expand: true});
			}

			SidenavService.navigation(sidenav);
		}

		let Items = ApiService.v1api('meta/Items');

		$scope.changeScope = function (forceFilters = false) {
			if (forceFilters) {
				$scope.selections.selectValue = 3;
			}

			storeSettings();
			decideIfFiltersAreShown();
			loadData();
		};

		fetchSettings();
		decideIfFiltersAreShown();
		$scope.filterRows = function () {
			storeSettings();
			cleanInvisibleSelections();
		};

		$scope.getVisibility = function (user_item_id) {
			if ($scope.widgetShowAll) return true;
			if ($scope.selections.filterValue === -1) return true;

			if ($scope.selections.filterValue === 0) {
				let rows = _.filter($scope.data, function (row) {
					return row.UserItemId === user_item_id && row.Status !== 0;
				});
				return rows.length !== $scope.weeks.length;
			}

			return _.find($scope.data, function (row) {
				return row.UserItemId === user_item_id && row.Status === $scope.selections.filterValue;
			});
		};

		let dialogModel = {d1: $scope.selections.dateRange.from.toDate(), d2: $scope.selections.dateRange.to.toDate()};
		$scope.header = {
			title: "Timesheet status " + CalendarService.formatDate($scope.selections.dateRange.from) + " - " + CalendarService.formatDate($scope.selections.dateRange.to),
			icons: [
				{
					icon: 'date_range', onClick: function () {
						let model = dialogModel;
						let scope = $rootScope.$new();

						let params = {
							scope: scope,
							title: 'WORKINGTIME_TRACKING_CHANGE_DATESPAN',
							inputs: [
								{
									type: 'date',
									label: 'WORKINGTIME_TRACKING_DATE_START',
									model: 'd1'
								},
								{
									type: 'date',
									label: 'WORKINGTIME_TRACKING_DATE_END',
									model: 'd2'
								}],
							buttons: [
								{
									type: 'secondary',
									text: 'MSG_CANCEL'
								},
								{
									type: 'primary',
									text: 'WORKINGTIME_TRACKING_SELECT',
									onClick: function () {
										$scope.jumpTo(model);
									}
								}]
						};
						MessageService.dialog(params, model, function () {
							if (model.d2 < model.d1) {
								model.d2 = model.d1;
							}
						});
					}
				}
			]
		};

		if ($scope.rights.download) {
			ViewService.addDownloadable("HOURACCUMULATION_DOWNLOAD", function () {
				if (typeof $scope.selections.excelRange.from == 'string') $scope.selections.excelRange.from = moment($scope.selections.excelRange.from);
				if (typeof $scope.selections.excelRange.to == 'string') $scope.selections.excelRange.to = moment($scope.selections.excelRange.to);

				if ($scope.selections.selectValue == 3) {
					let a = [];
					angular.forEach($scope.emailSelections, function (email, user_item_id) {
						a.push({UserItemId: user_item_id});
					});
					let b = a.length > 0 ? a : $scope.users;
					let c = "";
					_.each(b, function (user) {
						c += user.UserItemId + ",";
					});
					if (c != "") {
						c = c.substring(0, c.length - 1);
						let bgParams = {};
						bgParams["FileName"] = "Timesheet status";
						bgParams["Process"] = "worktime_tracking";
						bgParams["v"] = $scope.selections.selectValue;
						bgParams["To"] = $scope.selections.excelRange.to.format('YYYY-MM-DD');
						bgParams["From"] = $scope.selections.excelRange.from.format('YYYY-MM-DD');
						bgParams["Ids"] = c;

						return Items.new(["async_no_schedule", "WorktimeExcelBg"], bgParams);

						/*window.open("files/WorktimeExcel/" +
							$scope.selections.excelRange.from.format('YYYY-MM-DD') + "/" +
							$scope.selections.excelRange.to.format('YYYY-MM-DD') + "/" +
							$scope.selections.selectValue + "/" + c);*/
					}
				} else {

					let bgParams = {};
					bgParams["FileName"] = "Timesheet status";
					bgParams["Process"] = "worktime_tracking";
					bgParams["v"] = $scope.selections.selectValue;
					bgParams["To"] = $scope.selections.excelRange.to.format('YYYY-MM-DD');
					bgParams["From"] = $scope.selections.excelRange.from.format('YYYY-MM-DD');

					return Items.new(["async_no_schedule", "WorktimeExcelBg"], bgParams);

					/*	window.open("files/WorktimeExcel/" +
							$scope.selections.excelRange.from.format('YYYY-MM-DD') + "/" +
							$scope.selections.excelRange.to.format('YYYY-MM-DD') + "/" +
							$scope.selections.selectValue);*/
				}
			}, StateParameters.ViewId);
		}

		$scope.jumpTo = function (model) {
			$scope.selections.dateRange.from = moment(model.d1);
			$scope.selections.dateRange.to = moment(model.d2);
			$scope.selections.excelRange.from = moment(model.d1);
			$scope.selections.excelRange.to = moment(model.d2);
			WorktimeHourStatusService.persistentSettings.fromDate = moment(model.d1);
			WorktimeHourStatusService.persistentSettings.toDate = moment(model.d2);
			storeSettings();
			loadData();
		};

		$scope.openTimesheet = function (week, user_item_id) {
			if (!WorkTimeRights.approveAll &&
				myTeamUserIds.indexOf(user_item_id) === -1 &&
				user_item_id !== $rootScope.me.item_id) {
				return;
			}

			let r = _.find($scope.data, function (o) {
				return o.Week === week.w && o.UserItemId === user_item_id;
			});
			UserService.getUser(user_item_id).then(function (user) {
				ViewService.view('worktimeTracking',
					{
						params: {
							userId: user.item_id,
							userCalendarId: user.calendar_id,
							userName: user.name,
							fromDate: week.o,
							toDate: week.o.clone().add(7, 'days'),
							status: r && r.Status || 0
						}
					},
					{
						split: false
					});
			});
		};

		function storeSettings() {
			LocalStorageService.store('KETO_WORKTIME_STATUS_SHEET', $scope.selections);
		}

		function fetchSettings() {
			if (ViewMode == 'widget') return;
			let selections = LocalStorageService.get('KETO_WORKTIME_STATUS_SHEET');
			$scope.selections = _.extend({}, $scope.selections, selections);
			$scope.selections.dateRange.from = moment($scope.selections.dateRange.from);
			$scope.selections.dateRange.to = moment($scope.selections.dateRange.to);
		}


// init and refresh
		function loadData() {
			if (ViewMode != 'widget') ViewService.lock(StateParameters.ViewId);
			$scope.header.title = "Timesheet status " + CalendarService.formatDate($scope.selections.dateRange.from) + " - " + CalendarService.formatDate($scope.selections.dateRange.to);

			let l = $scope.selections.dateRange.to.utc().endOf('isoWeek').diff($scope.selections.dateRange.from.utc().startOf('isoWeek'), 'week') + 1;
			let start = $scope.selections.dateRange.from.clone().utc().startOf('isoWeek');
			let activeSelections = ($scope.selections.selectValue === 3) ? PortfolioService.getActiveSettings($scope.portfolioId) : null;

			WorktimeHourStatusService.getStatus(
				getDayMonthYearFormatFromMoment($scope.selections.dateRange.from),
				getDayMonthYearFormatFromMoment($scope.selections.dateRange.to),
				$scope.selections.selectValue,
				$scope.portfolioId,
				activeSelections
			)
				.then(function (data) {
					$scope.weeks = [];
					for (let w = 0; w < l; w++) {
						$scope.weeks.push({w: start.isoWeek(), d: start.format("DD/MM"), o: start.clone().utc()});
						start.add(7, 'days');
					}

					if (ViewMode != 'widget') ViewService.unlock(StateParameters.ViewId);
					setData(data);
					cleanInvisibleSelections();
				});
		}

		function setData(data) {
			$scope.users = _.uniqBy(data.items, "UserItemId");
			$scope.data = data.items;
			myTeamUserIds = data.myTeam;

			if (ViewMode == 'widget') {

				let ut = 0;
				_.each($scope.users, function (item) {
					if ($scope.getVisibility(item.UserItemId) && $scope.widgetShowAll == false) ut += 1;
					if ($scope.widgetShowAll == true) ut += 1;
				});

				let p = ViewService.getView(StateParameters.ParentViewId);
				if (typeof p !== 'undefined' && typeof p.scope.setIcon === 'function') {
					p.scope.setIcon(StateParameters.ViewId, ut);
				}
			}
		}

		$scope.getTitle = function (week, user_id) {
			let r = _.find($scope.data, function (o) {
				return o.Week === week && o.UserItemId === user_id;
			});

			if (r) {
				return 'Hours: ' + r.Hours + ', Additional Hours: ' + r.AdditionalHours;
			}
			return "";
		};

		$scope.getStatus = function (week, user_id) {
			let r = _.find($scope.data, function (o) {
				return o.Week === week && o.UserItemId === user_id;
			});
			if (r) {
				if (r.Status === 0) return "inprogress";
				if (r.Status === -2) return "partial";
				if (r.Status === 1) return "submitted";
				if (r.Status === 2) return "approved";
				if (r.Status === 3) return "rejected";
			}
			return "inprogress";
		};

		if (ViewMode != 'widget') {
			ViewService.footbar(StateParameters.ViewId, {template: 'pages/workTimeTracking/worktime-hour-status-footbar.html'});
		}

		loadData();
	}

	function getDayMonthYearFormatFromMoment(momentObject) {
		return momentObject.format('DDMMYYYY');
	}

	WorktimeHourStatusService.$inject = ['ApiService'];

	function WorktimeHourStatusService(ApiService) {
		let persistentSettings = {};

		let WorkTimeTracking = ApiService.v1api('time/worktimetracking');
		let getStatus = function (from, to, all, portfolioId, filterSelections) {
			if (filterSelections) {
				let filterString = _.chain(filterSelections.filters.select)
					.map(function (filter, key) {
						if (!_.isEmpty(filter)) {
							return key + '@' + _.join(filter, '-');
						}
					})
					.filter(function (val) {
						return !_.isEmpty(val);
					})
					.join(',')
					.value();
				if (_.isEmpty(filterString)) filterString = '%20';
				let filterText = filterSelections.filters.text;
				if (_.isEmpty(filterText)) {
					filterText = '%20';
				}

				return WorkTimeTracking.get(['status', from, to, all, portfolioId, filterText, filterString]);
			}
			return WorkTimeTracking.get(['status', from, to, all]);
		};

		let getWorkLoadDashboardData = function (startTime, endTime) {
			return WorkTimeTracking.get(['dashboard', startTime, endTime])
		};

		let getWidgetData = function (from, to) {
			return WorkTimeTracking.get(['widget', from, to]);
		};
		let getHourTotal = function (from, to) {
			return WorkTimeTracking.get(['total', from, to]);
		};

		let rebase = function (users) {
			return WorkTimeTracking.new('rebase', {text: '', users: users, linemanager: false});
		};

		let getRebaseDate = function () {
			return WorkTimeTracking.get('rebaseDate');
		};

		let setRebaseDate = function (d: Date) {
			return WorkTimeTracking.new('rebaseDate', d);
		};

		let sendEmail = function (text, users, tolm) {
			return WorkTimeTracking.new('sendemail', {text: text, users: users, linemanager: tolm});
		};
		return {
			getStatus: getStatus,
			sendEmail: sendEmail,
			getWidgetData: getWidgetData,
			getHourTotal: getHourTotal,
			getWorkLoadDashboardData: getWorkLoadDashboardData,
			persistentSettings: persistentSettings,
			rebase: rebase,
			getRebaseDate: getRebaseDate,
			setRebaseDate: setRebaseDate
		}
	}
})
();