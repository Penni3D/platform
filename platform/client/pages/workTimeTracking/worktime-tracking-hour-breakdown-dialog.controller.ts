(function () {
	'use strict';
	
	angular
		.module('core.pages')
		.controller('WorktimeTrackingHourBreakdownDialogController', WorktimeTrackingHourBreakdownDialogController)
		.controller('WorktimeTrackingHourBreakdownDialogCardController', WorktimeTrackingHourBreakdownDialogCardController);

	WorktimeTrackingHourBreakdownDialogController.$inject = ['$scope', 'MessageService', 'saveCallback', 'ParentScope', 'Common', 'Colors', 'ProcessService'];
	function WorktimeTrackingHourBreakdownDialogController($scope, MessageService, saveCallback, ParentScope, Common, Colors, ProcessService) {
		let totalWhenDialogWasOpened = ParentScope.selected.currentTotal;
		let dailyTotalsWithoutThisDay = {
			reg: 0,
			add: 0
		};
		if (ParentScope.selected.dailyTotal) {
			dailyTotalsWithoutThisDay.reg = ParentScope.selected.dailyTotal.hours;
			dailyTotalsWithoutThisDay.add = ParentScope.selected.dailyTotal.additionalHours;
		}

		_.each(ParentScope.selected.breakdownHours, function (item) {
			dailyTotalsWithoutThisDay.reg = dailyTotalsWithoutThisDay.reg - (+item.Hours || 0);
			dailyTotalsWithoutThisDay.add = dailyTotalsWithoutThisDay.add - (+item.AdditionalHours || 0);
		});

		let hourSumForBreakdown = 0;

		function calculateHoursForBreakdown() {
			hourSumForBreakdown = 0;
			_.each(ParentScope.selected.breakdownHours, function (item) {
				hourSumForBreakdown += (+item.Hours || 0);
			});

		}

		calculateHoursForBreakdown();
		let hoursWithoutThese = totalWhenDialogWasOpened - hourSumForBreakdown;
		$scope.additionalHoursInUse = ParentScope.additionalHoursInUse;
		$scope.commentMode = ParentScope.commentMode;
		$scope.commentOptions = ParentScope.commentOptions;
		$scope.redColor = Colors.getColor('alert').rgb;
		$scope.requireComment = ParentScope.requireComment;
		$scope.selected = ParentScope.selected;
		$scope.data = ParentScope.data;
		$scope.modify = $scope.data.edit && $scope.selected.editable;
		$scope.hourTotal = 0;
		$scope.additionalHourTotal = 0;
		$scope.taskAdditionalTotal = 0;
		$scope.taskTotal = 0;
		$scope.defaultCommentValue = ParentScope.defaultCommentValue;

		$scope.hasHours = function () {
			if (ParentScope.selected.allowBasicHoursOnWeekends) return true;
			let d = moment(ParentScope.selected.column.date2);
			return !(d.day() == 6 || d.day() == 0);
		};

		$scope.showRowMenu = function (row, event) {
			let title = '';
			let i = $scope.data.breakdownTypes.length;
			while (i--) {
				if (row.ListItemId == $scope.data.breakdownTypes[i].item_id) {
					title = $scope.data.breakdownTypes[i].list_item;
					break;
				}
			}

			MessageService.menu([
				{
					name: 'EDIT',
					onClick: function (event) {
						showCard(event, row, title, $scope.hasHours, $scope.additionalHoursInUse);
					}
				},
				{
					name: 'REMOVE',
					onClick: function () {
						$scope.deleteRow(row);
					}
				}
			], event.currentTarget);
		};

		$scope.editComment = function (event, row) {
			showCard(event, row, undefined, false, false);
		};

		function showCard(event, row, title, showHours, showAdditionalHours) {
			let p = {
				template: 'pages/workTimeTracking/worktime-tracking-hour-breakdown-dialog-card.html',
				controller: 'WorktimeTrackingHourBreakdownDialogCardController',
				locals: {
					Row: row,
					BreakdownChange: $scope.breakdownHoursChanged,
					Title: title,
					Configs: {
						edit: $scope.data.edit,
						commentMode: $scope.commentMode,
						commentOptions: $scope.commentOptions,
						requireComment: $scope.requireComment,
						showHours: showHours,
						showAdditional: showAdditionalHours
					}
				}
			};

			MessageService.card(p, event.currentTarget);
		}

		function refreshTotals() {
			let breakdownSum = _.sumBy(ParentScope.selected.breakdownHours, 'Hours');
			if (typeof breakdownSum === 'undefined') {
				breakdownSum = 0;
			}

			let additionalSum = _.sumBy(ParentScope.selected.breakdownHours, 'AdditionalHours');
			if (typeof additionalSum === 'undefined') {
				additionalSum = 0;
			}

			$scope.hourTotal = Common.formatNumber(dailyTotalsWithoutThisDay.reg + breakdownSum, 2);
			$scope.additionalHourTotal = Common.formatNumber(dailyTotalsWithoutThisDay.add + additionalSum, 2);
			$scope.taskTotal = Common.formatNumber(breakdownSum, 2);
			$scope.taskAdditionalTotal = Common.formatNumber(additionalSum, 2);

			calculateHoursForBreakdown();
			$scope.weeklyTotalAfterChanges = hoursWithoutThese + hourSumForBreakdown;
		}

		$scope.cancelHours = [];
		$scope.cancel = function () {
			_.forEach(ParentScope.selected.breakdownHours, function (row) {
				if (row.deleted) {
					let r = $scope.cancelHours[row.ListItemId];
					row.deleted = false;
					row.Hours = r.Hours;
					row.AdditionalHours = r.AdditionalHours;
				}
			});
			MessageService.closeDialog();
		};

		$scope.canSave = function () {
			let error = false;
			_.forEach(ParentScope.selected.breakdownHours, function (row) {
				if (row.deleted) {
					return;
				}

				if (row.ListItemId === 0 || !row.ListItemId) {
					error = true;
				}

				if ($scope.requireComment && (
					($scope.commentMode == 1 && !row.Comment) ||
					($scope.commentMode == 2 && !row.CommentListItemId)
				)) {
					error = true;
				}
			});

			return !error;
		};

		$scope.saveBreakdown = function () {
			// sum similar items
			let groupedByListItem = _.groupBy(ParentScope.selected.breakdownHours, 'ListItemId');
			let grouper = $scope.commentMode == 1 ? 'Comment' : 'CommentListItemId';

			ParentScope.selected.breakdownHours = _.flatten(_.map(groupedByListItem, function (listItems, key) {
				let combinedItems = [];

				let commentsSplit = _.partition(listItems, function (i) {
					return i[grouper];
				});

				let itemsWithComments = _.head(commentsSplit);
				let itemsWithoutComments = _.last(commentsSplit);

				//Items without any comments
				if (itemsWithoutComments.length) {
					combinedItems.push({
						Hours: _.sumBy(itemsWithoutComments, 'Hours'),
						AdditionalHours: _.sumBy(itemsWithoutComments, 'AdditionalHours'),
						ListItemId: +key
					});
				}

				//Items with comments
				_.forEach(_.groupBy(itemsWithComments, grouper), function (items, comment) {
					let i = {
						Hours: _.sumBy(items, 'Hours'),
						AdditionalHours: _.sumBy(items, 'AdditionalHours'),
						ListItemId: +key,
					};

					i[grouper] = comment;

					combinedItems.push(i);
				});

				return combinedItems;
			}));

			saveCallback();
			$scope.cancelHours = [];

			MessageService.closeDialog();
		};

		$scope.addRow = function () {
			if (ParentScope.selected) {
				ParentScope.selected.breakdownHours = ParentScope.selected.breakdownHours || [];
				ParentScope.selected.breakdownHours.push({CommentListItemId: $scope.defaultCommentValue});
			}
		};

		$scope.deleteRow = function (row) {
			$scope.cancelHours[row.ListItemId] = angular.copy(row);
			row.deleted = true;
			row.Hours = 0;
			row.AdditionalHours = 0;
			refreshTotals();

		};

		$scope.breakdownHoursChanged = function () {
			refreshTotals();
		};

		if (_.size(ParentScope.selected.breakdownHours) === 0) {
			$scope.addRow();
		}
		refreshTotals();
	}

	WorktimeTrackingHourBreakdownDialogCardController.$inject = ['$scope', 'Row', 'MessageService', 'BreakdownChange', 'Configs', 'Title'];
	function WorktimeTrackingHourBreakdownDialogCardController($scope, Row, MessageService, BreakdownChange, Configs, Title) {
		$scope.title = Title;
		$scope.config = Configs;
		$scope.row = Row;
		$scope.closeCard = function () {
			MessageService.closeCard();
		};

		$scope.breakdownChange = BreakdownChange;
	}
})
();