(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(ConditionResultConfig)
		.config(ConditionResultDialogConfig)
		.controller('ConditionResultController', ConditionResultController)
		.controller('ConditionResultsDialogController', ConditionResultsDialogController)
		.service('ConditionResultService', ConditionResultService);


	ConditionResultConfig.$inject = ['ViewsProvider'];

	function ConditionResultConfig(ViewsProvider) {
		ViewsProvider.addView('conditionResult', {
			controller: 'ConditionResultController',
			template: 'pages/checkConditionResult/checkConditionResult.html',
			resolve: {
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				}
			}
		});

		ViewsProvider.addPage('conditionResult', 'CONDITION_C_8', ['read', 'write']);
	}

	ConditionResultDialogConfig.$inject = ['ViewsProvider'];

	function ConditionResultDialogConfig(ViewsProvider) {
		ViewsProvider.addDialog('conditionResultDialog', {
			controller: 'ConditionResultsDialogController',
			template: 'pages/checkConditionResult/conditionResultsDialog.html',
			resolve: {
				Conditions: function(SettingsService, StateParameters){
					if(!StateParameters.Process) return [];
					return SettingsService.Conditions(StateParameters.Process);
				}
			}
		});

	}


	ConditionResultsDialogController.$inject = ['$scope',
		'MessageService',
		'Conditions',
		'Translator',
		'ConditionResultService',
		'StateParameters',
		'$rootScope',
		'UserService'];

	function ConditionResultsDialogController($scope,
	                                          MessageService,
	                                          Conditions,
	                                          Translator,
	                                          ConditionResultService,
	                                          StateParameters,
	                                          $rootScope,
	                                          UserService){
		let userLanguage = $rootScope.me.locale_id;
		$scope.conditions = Conditions;
		$scope.dialogTranslation = Translator.translate('CONDITION_C_8');
		$scope.cancel = function(){
			MessageService.closeDialog();
		}

		$scope.selectedUser = [$rootScope.me.item_id];
		$scope.showConditionResult = function () {
			return ConditionResultService.getConditionResult(StateParameters.Process, $scope.condition, StateParameters.ItemId, $scope.selectedUser).then(function (result) {
				UserService.getUser($scope.selectedUser).then(function(user){
					formResultString(result, user);
					$scope.conditionResult = result;
					$scope.resultReady = true;
				})
			});
		};

		let formResultString = function(result: boolean, user: object){
			let userName = user.hasOwnProperty('primary') ? user['primary'] : "Unknown";
			let selectedCondition = _.find($scope.conditions, function(c){
				return c.ConditionId == $scope.condition;
			})
			$scope.string1 = Translator.translate('CONDITION');
			$scope.string11 = " " + selectedCondition.LanguageTranslation[userLanguage]
			$scope.string2 = Translator.translate('CONDITION_C_11');
			$scope.string21 = " " + userName;

		}
	}

	ConditionResultController.$inject = ['$scope', 'Translator', 'ConditionResultService', 'Processes', 'ViewService', 'StateParameters', 'ProcessService', 'SettingsService', '$rootScope'];

	function ConditionResultController($scope, Translator, ConditionResultService, Processes, ViewService, StateParameters, ProcessService, SettingsService, $rootScope) {

		var userLanguage = $rootScope.me.locale_id;

		$scope.processes = Processes;
		$scope.selectedProcess;
		$scope.selectedRow;
		$scope.selectedCondition;
		$scope.conditions;
		$scope.conditionResult;


		$scope.conditionTranslation;
		$scope.rowTranslation;


		$scope.processRows;

		$scope.header = {
			title: 'CONDITION_C_8',
		};

		$scope.resultReady = false;
		$scope.buttonGreyed = true;

		$scope.resultNotReady = function () {
			$scope.resultReady = false;
			$scope.isButtonReady();
		};

		$scope.isButtonReady = function(){
			if($scope.selectedProcess != 'undefined' && $scope.selectedCondition.length != 0 && $scope.selectedRow.length != 0){

				$scope.buttonGreyed = false;
			}
		};

		$scope.getProcessConditions = function (process) {
			return SettingsService.Conditions(process).then(function (conditions) {
				$scope.conditions = conditions;
			});
		};

		$scope.returnConditionTranslation = function (conditionId) {
			_.each($scope.conditions, function (condition) {
				if (conditionId == condition.ConditionId) {

					$scope.conditionTranslation = condition.LanguageTranslation[userLanguage];
				}
			});
		};

		$scope.returnRowTranslation = function (itemId) {
			_.each($scope.processRows, function (row) {
				if (row.title && row.item_id == itemId) {
					$scope.rowTranslation = row.title
				} else if (!row.title && row.item_id == itemId) {
					$scope.rowTranslation = 'no title' + '( Item Id:' + row.item_id + ' )';
				}
			});
		};


		$scope.getProcessRows = function (params) {
			$scope.resultReady = false;
			return ProcessService.getItems(params.process[0]).then(function (rows) {
				$scope.processRows = rows;
				return $scope.getProcessConditions(params.process[0]);
			});
		};

		$scope.showConditionResult = function () {
			return ConditionResultService.getConditionResult($scope.selectedProcess[0], $scope.selectedCondition[0], $scope.selectedRow[0], $scope.selectedUser).then(function (result) {
				$scope.conditionResult = result;
				$scope.resultReady = true;
			});
		};

	}

	ConditionResultService.$inject = ['ApiService'];

	function ConditionResultService(ApiService) {
		var self = this;
		var States = ApiService.v1api('navigation/States');

		let resultCache = {'UserItemId': 0, 'ItemId': 0, 'Process': "0"}
		self.getConditionResult = function (process, conditionId, processRowId, userItemId) {
			return States.get(['getConditionsResult', process, conditionId, processRowId, userItemId]).then(function (result) {
				return result;
			});
		};

		self.setResultCache = function(userItemId, itemId, process){
			resultCache['UserItemId'] = userItemId;
			resultCache['ItemId'] = itemId;
			resultCache["Process"] = process;
		}

		self.getResultCache = function(){
			return resultCache;
		}
	}
})();