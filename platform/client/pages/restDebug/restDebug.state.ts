(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(RestDebugConfig)
		.controller('RestDebugController', RestDebugController);


	RestDebugConfig.$inject = ['ViewsProvider'];
	function RestDebugConfig(ViewsProvider) {
		ViewsProvider.addView('restDebug', {
			controller: 'RestDebugController',
			template: 'pages/restDebug/restDebug.html',
			resolve: {
				ActiveSettings: function (RestDebugService) {
					return RestDebugService.getActiveSettings();
				},
				SavedSettings: function (RestDebugService) {
					return RestDebugService.getSavedSettings();
				},
				Rests: function (RestDebugService) {
					return RestDebugService.getRESTs();
				}
			}
		});

		ViewsProvider.addPage('restDebug', 'PAGE_REST_DEBUG', ['read']);
	}

	RestDebugController.$inject = [
		'$scope',
		'$http',
		'SavedSettings',
		'ActiveSettings',
		'RestDebugService',
		'SidenavService',
		'Translator',
		'MessageService',
		'StateParameters',
		'Rests'
	];
	function RestDebugController(
		$scope,
		$http,
		SavedSettings,
		ActiveSettings,
		RestDebugService,
		SidenavService,
		Translator,
		MessageService,
		StateParameters,
		Rests
	) {
		setNavigation();

		$scope.conf = ActiveSettings;
		$scope.saveable = true;

		$scope.header = {
			title: StateParameters.title
		};

		$scope.save = function () {
			$scope.saveable = false;
			RestDebugService.setSavedSettings($scope.conf.path, $scope.conf).then(function (settings) {
				SavedSettings.push(settings);
				setNavigation();
				$scope.saveable = true;
			});
		};

		$scope.delete = function () {
			handlePromises($http.delete($scope.conf.path));
		};

		$scope.post = function () {
			handlePromises($http.post($scope.conf.path, $scope.conf.payload));
		};

		$scope.put = function () {
			handlePromises($http.put($scope.conf.path, $scope.conf.payload))
		};

		$scope.get = function () {
			handlePromises($http.get($scope.conf.path));
		};

		function handlePromises(promise) {
			RestDebugService.saveActiveSettings();
			$scope.loading = true;

			promise.then(function (response) {
				$scope.loading = false;
				$scope.result = response;
			}, function (error) {
				$scope.loading = false;
				$scope.result = error;
			});
		}

		function setNavigation() {
			var sidenav = [];
			var tabs = [];

			angular.forEach(SavedSettings, function (s, key) {
				var t = {
					hideSplit: true,
					name: Translator.translation(s.Name),
					onClick: function () {
						angular.forEach(s.Value, function (value, key) {
							ActiveSettings[key] = value;
						});

						RestDebugService.saveActiveSettings();
					},
					menu: [{
						icon: 'delete',
						name: 'REMOVE',
						onClick: function () {
							MessageService.confirm('REST_DEBUG_DELETE', function () {
								return RestDebugService.removeSavedSettings(s.ConfigurationId).then(function () {
									SavedSettings.splice(key, 1);
									setNavigation();
								});
							});
						}
					}]
				};

				tabs.push(t);
			});

			if (tabs.length) {
				sidenav.push({ name: 'REST_DEBUG_SAVED', showGroup: true, tabs: tabs });
			}

			SidenavService.navigation(sidenav);
		}
	}
})();