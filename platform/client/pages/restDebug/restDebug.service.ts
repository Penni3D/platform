(function () {
	'use strict';

	angular
		.module('core.pages')
		.service('RestDebugService', RestDebugService)

	RestDebugService.$inject = ['Configuration', 'ApiService'];
	function RestDebugService(Configuration, ApiService) {
		let self = this;

		self.getRESTs = function () {
			let a = ApiService.v1api('diagnostics/rests');
			return a.get();
		}

		self.getSavedSettings = function () {
			return Configuration.getSavedPreferences('rest', 'debug');
		};

		self.setSavedSettings = function (name, settings) {
			return Configuration.newPreferences('rest', 'debug', name, false, settings);
		};

		self.removeSavedSettings = function (configurationId) {
			return Configuration.deleteSavedPreferences('rest', 'debug', configurationId);
		};

		self.saveActiveSettings = function () {
			return Configuration.saveActivePreferences('rest', 'debug');
		};

		self.getActiveSettings = function () {
			let settings = Configuration.getActivePreferences('rest');

			if (typeof settings.debug === 'undefined') {
				settings.debug = {
					path: 'model/',
					payload: '{}'
				};
			}

			return settings.debug;
		};
	}


})();