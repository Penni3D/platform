(function (){
	'use strict';
	angular
		.module('core.pages')
		.config(FlexTimeTrackingCorrectionsConfig)
		.controller('FlexTimeTrackingCorrectionsController', FlexTimeTrackingCorrectionsController)
		.service('FlexTimeTrackingCorrectionsService', FlexTimeTrackingCorrectionsService);

	FlexTimeTrackingCorrectionsConfig.$inject = ['ViewsProvider'];
	function FlexTimeTrackingCorrectionsConfig(ViewsProvider) {

		ViewsProvider.addView('FlexTimeTrackingCorrections', {
			controller: 'FlexTimeTrackingCorrectionsController',
			template: 'pages/flexTimeTracking/flexTimeTrackingCorrections.html',
			resolve: {
				States: function (ViewConfigurator, StateParameters) {
					return ViewConfigurator.getRights('FlexTimeTrackingCorrections', StateParameters.NavigationMenuId);
				},
				CorrectionsData: function (FlexTimeTrackingCorrectionsService) {
					return FlexTimeTrackingCorrectionsService.getCorrections(null);
				}
			}
		});
		ViewsProvider.addPage('FlexTimeTrackingCorrections', 'FLEX_TIME_TRACKING_CORRECTIONS_TITLE', ['read', 'admin']);
	}

	FlexTimeTrackingCorrectionsController.$inject = ['$scope', 'FlexTimeTrackingCorrectionsService', 'CorrectionsData', 'States', 'MessageService'];
	function FlexTimeTrackingCorrectionsController($scope, FlexTimeTrackingCorrectionsService, CorrectionsData, States, MessageService) {
		$scope.AdminRights = false;
		if (States.indexOf("admin") > -1) {
			$scope.AdminRights = true;
		}

		$scope.corrections = CorrectionsData;

		$scope.GetCorrections = function (user_item_id) {
			FlexTimeTrackingCorrectionsService.getCorrections(user_item_id).then(function (corrections) {
				$scope.corrections = corrections;
			});
		};

		$scope.Changed = function () {
			$scope.GetCorrections($scope.corrections.user_item_id[0]);
		};

		$scope.header = {
			title: 'FLEX_TIME_TRACKING_CORRECTIONS_TITLE'
		};

		$scope.Add = function () {
			var add = function () {
				data.hours = ParseInt(data.hours);
				data.minutes = ParseInt(data.minutes);

				if (data.date == null) {
					MessageService.msgBox('FLEX_TIME_TRACKING_CORRECTIONS_ERROR_NO_DATE').then(function () {
						MessageService.dialog(content, data);
					});
					return;
				}
				else if (data.hours == 0 && data.minutes == 0) {
					MessageService.msgBox('FLEX_TIME_TRACKING_CORRECTIONS_ERROR_NO_TIME').then(function () {
						MessageService.dialog(content, data);
					});
					return;
				}

				data.hours = ParseInt(data.hours);
				data.minutes = ParseInt(data.minutes);

				var flex_day = $scope.corrections.new_correction;
				flex_day.selected_date = ParseDate(data.date);
				if (data.sign[0] == 1) {
					if (data.hours > 0) {
						flex_day.flex_rows[0].work.hours = '-' + data.hours;
						flex_day.flex_rows[0].work.minutes = '-' + data.minutes;
					}
					else {
						flex_day.flex_rows[0].work.hours = '0';
						flex_day.flex_rows[0].work.minutes = '-' + data.minutes;
					}
				}
				else {
					flex_day.flex_rows[0].work.hours = data.hours;
					flex_day.flex_rows[0].work.minutes = data.minutes;
				}
				flex_day.flex_rows[0].comment = data.comment;

				FlexTimeTrackingCorrectionsService.addCorrection(flex_day).then(function () {
					$scope.GetCorrections($scope.corrections.user_item_id[0]);
				});
			};

			var content = {
				title: 'FLEX_TIME_TRACKING_CORRECTIONS_ADD',
				inputs: [{ type: 'date', label: 'FLEX_TIME_TRACKING_DATE', model: 'date' },
					{ type: 'select', label: 'FLEX_TIME_TRACKING_SIGN', model: 'sign', options: [{ name: '+', value: 0 }, { name: '-', value: 1 }], optionName: 'name', optionValue: 'value' },
					{ type: 'integer', label: 'FLEX_TIME_TRACKING_HOURS', model: 'hours', min: 0, max: 999 },
					{ type: 'integer', label: 'FLEX_TIME_TRACKING_MINUTES', model: 'minutes', min: 0, max: 59 },
					{ type: 'text', label: 'FLEX_TIME_TRACKING_COMMENT', model: 'comment' }],
				buttons: [{ text: 'DISCARD' }, { type: 'primary', onClick: add, text: 'ADD' }]
			};

			var data = { date: new Date(), sign: [0], hours: 0, minutes: 0, comment: '' };

			MessageService.dialog(content, data);
		};

		$scope.Delete = function (flex_row_item_id) {
			MessageService.confirm('FLEX_TIME_TRACKING_CORRECTIONS_DELETE_CONFIRM', function () {
				FlexTimeTrackingCorrectionsService.deleteCorrection(flex_row_item_id);

				for (var d = 0; d < $scope.corrections.flex_days.length; d++) {
					var flex_day = $scope.corrections.flex_days[d];
					for (var r = 0; r < flex_day.flex_rows.length; r++) {
						var flex_row = flex_day.flex_rows[r];
						if (flex_row.flex_row_item_id == flex_row_item_id) {
							flex_day.flex_rows.splice(r, 1);
							return;
						}
					}
				}
			});
		};

		function ParseDate(d) {
			return d.getFullYear() + '-' + LeadingZero(d.getMonth() + 1) + '-' + LeadingZero(d.getDate()) + 'T00:00:00Z';
		}

		function LeadingZero(num) {
			if (num < 10) {
				return '0' + num;
			}
			return num
		}

		function ParseInt(value) {
			if (value == null) {
				return 0;
			}
			return value;
		}
	}

	FlexTimeTrackingCorrectionsService.$inject = ['ApiService'];
	function FlexTimeTrackingCorrectionsService(ApiService) {
		var self = this;
		var flextimetracking = ApiService.v1api('time/flextimetracking');

		self.getCorrections = function (user_item_id) {
			return flextimetracking.get(["corrections/" + user_item_id]);
		};

		self.addCorrection = function (flexDay) {
			return flextimetracking.new([2], flexDay);
		};

		self.deleteCorrection = function (flex_row_item_id) {
			return flextimetracking.delete([flex_row_item_id]);
		};
	}
})();