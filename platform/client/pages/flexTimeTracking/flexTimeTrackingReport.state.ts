(function (){
	'use strict';
	angular
		.module('core.pages')
		.config(FlexTimeTrackingReportConfig)
		.controller('FlexTimeTrackingReportController', FlexTimeTrackingReportController)
		.service('FlexTimeTrackingReportService', FlexTimeTrackingReportService);

	FlexTimeTrackingReportConfig.$inject = ['ViewsProvider'];
	function FlexTimeTrackingReportConfig(ViewsProvider) {

		ViewsProvider.addView('FlexTimeTrackingReport', {
			controller: 'FlexTimeTrackingReportController',
			template: 'pages/flexTimeTracking/flexTimeTrackingReport.html',
			resolve: {
				States: function (ViewConfigurator, StateParameters) {
					return ViewConfigurator.getRights('FlexTimeTrackingReport', StateParameters.NavigationMenuId);
				},
				ReportData: function (FlexTimeTrackingReportService) {
					return FlexTimeTrackingReportService.getReport(null, null, null);
				}
			}
		});
		ViewsProvider.addPage('FlexTimeTrackingReport', 'FLEX_TIME_TRACKING_REPORT_TITLE', ['read', 'admin']);
	}

	FlexTimeTrackingReportController.$inject = ['$scope', 'FlexTimeTrackingReportService', 'ReportData', 'States', '$sce', 'SaveService'];
	function FlexTimeTrackingReportController($scope, FlexTimeTrackingReportService, ReportData, States, $sce, SaveService) {
		$scope.AdminRights = false;
		if (States.indexOf("admin") > -1) {
			$scope.AdminRights = true;
		}

		$scope.report = ReportData;

		$scope.GetReport = function (user_item_id, year, month) {
			FlexTimeTrackingReportService.getReport(user_item_id, year, month).then(function (report) {
				$scope.report = report;
			});
		};

		$scope.Changed = function () {
			SaveService.tick(true);
			$scope.GetReport($scope.report.user_item_id[0], $scope.report.year, $scope.report.month);
		};

		$scope.PrevYear = function () {
			$scope.GetReport($scope.report.user_item_id[0], $scope.report.year - 1, $scope.report.month);
		};

		$scope.NextYear = function () {
			$scope.GetReport($scope.report.user_item_id[0], $scope.report.year + 1, $scope.report.month);
		};

		$scope.PrevMonth = function () {
			var year = $scope.report.year;
			var month = $scope.report.month - 1;
			if (month == 0) {
				year--;
				month = 12;
			}
			$scope.GetReport($scope.report.user_item_id[0], year, month);
		};

		$scope.NextMonth = function () {
			var year = $scope.report.year;
			var month = $scope.report.month + 1;
			if (month == 13) {
				year++;
				month = 1;
			}
			$scope.GetReport($scope.report.user_item_id[0], year, month);
		};

		$scope.header = {
			title: 'FLEX_TIME_TRACKING_REPORT_TITLE'
		};

		function isNumeric(num) {
			return !isNaN(parseFloat(num)) && isFinite(num);
		}

		$scope.GetDiff = function (flex_day) {
			var html = '';

			var hours = 0;
			var minutes = 0;
			var sick_leave_hours = 0;
			var sick_leave_minutes = 0;
			for (var r = 0; r < flex_day.flex_rows.length; r++) {
				if ((flex_day.flex_rows[r].type == 0 || flex_day.flex_rows[r].type == 1) && isNumeric(flex_day.flex_rows[r].work.hours) && isNumeric(flex_day.flex_rows[r].work.minutes)) {
					if (flex_day.flex_rows[r].special == 0 || flex_day.flex_rows[r].special == 1) {
						hours += parseInt(flex_day.flex_rows[r].work.hours);
						minutes += parseInt(flex_day.flex_rows[r].work.minutes);
					}
					else if (flex_day.flex_rows[r].special == 2) {
						sick_leave_hours += parseInt(flex_day.flex_rows[r].work.hours);
						sick_leave_minutes += parseInt(flex_day.flex_rows[r].work.minutes);
					}
				}
			}

			flex_day.day_hours.hours = hours + Math.floor(minutes / 60);
			flex_day.day_hours.minutes = Math.floor(minutes % 60);

			flex_day.day_sick_leave.hours = sick_leave_hours + Math.floor(sick_leave_minutes / 60);
			flex_day.day_sick_leave.minutes = Math.floor(sick_leave_minutes % 60);

			var daily_hours = parseInt(flex_day.daily_hours.hours) - parseInt(flex_day.day_sick_leave.hours);
			var daily_minutes = parseInt(flex_day.daily_hours.minutes) - parseInt(flex_day.day_sick_leave.minutes);
			daily_hours += Math.floor(daily_minutes / 60);
			daily_minutes = Math.floor(daily_minutes % 60);

			var diff_hours = 0;
			var diff_minutes = 0;
			if (parseInt(flex_day.day_hours.hours) > daily_hours || (parseInt(flex_day.day_hours.hours) == daily_hours && parseInt(flex_day.day_hours.minutes) > daily_minutes)) {
				diff_hours = parseInt(flex_day.day_hours.hours) - daily_hours;
				diff_minutes = parseInt(flex_day.day_hours.minutes) - daily_minutes;
				if (diff_minutes < 0) {
					diff_hours -= 1;
					diff_minutes += 60;
				}
				html += '+';
			}
			else if (parseInt(flex_day.day_hours.hours) < daily_hours || (parseInt(flex_day.day_hours.hours) == daily_hours && parseInt(flex_day.day_hours.minutes) < daily_minutes)) {
				diff_hours = daily_hours - parseInt(flex_day.day_hours.hours);
				diff_minutes = daily_minutes - parseInt(flex_day.day_hours.minutes);
				if (diff_minutes < 0) {
					diff_hours -= 1;
					diff_minutes += 60;
				}
				html += '-';
			}

			if (diff_hours > 0) {
				html += diff_hours + ' <span>h</span> ';
			}
			if (diff_minutes > 0) {
				html += diff_minutes + ' <span>m</span>';
			}

			return $sce.trustAsHtml(html);
		};
	}

	FlexTimeTrackingReportService.$inject = ['ApiService'];
	function FlexTimeTrackingReportService(ApiService) {
		var self = this;
		var flextimetracking = ApiService.v1api('time/flextimetracking');

		self.getReport = function (user_item_id, year, month) {
			return flextimetracking.get(["report", user_item_id + "/" + year + "/" + month]);
		};
	}
})();