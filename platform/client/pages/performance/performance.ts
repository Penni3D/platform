(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(PerformanceConfig)
		.controller('PerformanceController', PerformanceController)
		.service('PerformanceService', PerformanceService);


	PerformanceConfig.$inject = ['ViewsProvider'];

	function PerformanceConfig(ViewsProvider) {
		ViewsProvider.addView('Performance', {
			controller: 'PerformanceController',
			template: 'pages/performance/performance.html',
			resolve: {}
		});

		ViewsProvider.addPage('Performance', 'PAGE_PERFORMANCE', ['read']);
	}

	PerformanceController.$inject = ['$scope', 'Translator', 'PerformanceService', 'ViewService', 'StateParameters', 'Common'];

	function PerformanceController($scope, Translator, PerformanceService, ViewService, StateParameters, Common) {
		$scope.header = {
			title: StateParameters.title
		};
		$scope.sqColumns = [];
		$scope.coColumns = [];
		$scope.sqLoading = true;
		$scope.coLoading = true;
		$scope.consent = false;

		$scope.status = function (a, b) {
			if (!a && !b) return 1;
			if (a) return 2;
			if (b) return  3;
			return 0;
		};

		$scope.fn = function (n) {
			return Common.formatNumber(n, 0);
		};
		
		$scope.toggleConsent = function() {
			$scope.consent = true;
			PerformanceService.getSQPerformance().then(function (result) {
				$scope.sqLoading = false;
				$scope.sqColumns = result;

				PerformanceService.getCOPerformance().then(function (result) {
					$scope.coLoading = false;
					$scope.coColumns = result;
				});
			});
		}
	}

	PerformanceService.$inject = ['ApiService'];

	function PerformanceService(ApiService) {
		let self = this;

		self.getSQPerformance = function () {
			return ApiService.v1api('settings/RegisteredSubQueries/testall').get().then(function (result) {
				return result;
			});
		};

		self.getCOPerformance = function () {
			return ApiService.v1api('settings/conditions/testall').get().then(function (result) {
				return result;
			});
		};
	}
})();