﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.service('ListsService', ListsService);

	ListsService.$inject = ['ApiService', 'Common', 'MessageService', 'SettingsService', '$q'];

	function ListsService(ApiService, Common, MessageService, SettingsService, $q) {
		let self = this;

		self.listProcesses = ApiService.v1api('meta/listProcesses');
		self.Processes = ApiService.v1api('settings/processes');
		self.ItemColumns = ApiService.v1api('meta/itemColumns');
		self.ProcessItems = ApiService.v1api('meta/Items');
		self.SystemList = ApiService.v1api('meta/SystemList');
		self.ItemsDataProcessSelections = ApiService.v1api('meta/ItemsDataProcessSelections');
		self.ItemsData = ApiService.v1api('meta/ItemsData');
		self.DuplicateVariable = ApiService.v1api('settings/DuplicateVariable');

		self.getAllInstanceListProcesses = function () {
			return self.listProcesses.get();
		};

		self.saveItemsDataProcessSelections = function (ProcessName, itemId, data) {
			return self.ItemsDataProcessSelections.save([ProcessName, itemId], data);
		};

		self.getListProcesses = function (process) {
			return self.listProcesses.get(process);
		};

		self.getListProcessesByType = function (type) {
			return self.listProcesses.get(["all", type]);
		};

		self.getItemsData = function (process, itemId) {
			return self.ItemsData.get([process, itemId]);
		};

		self.getListProcessesData = function (type, processName) {
			return self.listProcesses.get([type, processName]);
		};

		self.getListProcessesDataNew = function (type, processName) {
			return self.listProcesses.get([type, processName, "new"]);
		};

		self.getListHierarchy = function (pageType) {
			return self.listProcesses.get(["hierarchy", pageType]);
		};

		self.getRecursiveHierarchy = function (order?) {
			if (!order) order = "item_id";
			return self.listProcesses.get(["recursive", order]);
		};

		self.getSystemLists = function (list) {
			return self.SystemList.get(list);
		};

		self.getItemColumns = function (processType, itemColumns) {
			return self.ItemColumns.get([processType, itemColumns]);
		};

		self.getItemColumnsData = function (processType) {
			return self.ItemColumns.get(processType);
		};

		self.getProcessItems = function (ProcessName, directive, column?) {
			if (angular.isUndefined(directive) || column == '' || column == null) column = 'order_no';

			return self.ProcessItems.get([ProcessName, directive, column]);
		};

		self.getFilledProcessItems = function (ProcessName, directive, columns, order?) {
			if (angular.isUndefined(directive) || order == '' || order == null) order = 'order_no';
			return self.ProcessItems.new(['fill', ProcessName, directive, order], columns);
		};

		self.getChildProcessItems = function (ProcessName, type, parentProcessName) {
			return self.ProcessItems.get([ProcessName, type, parentProcessName]);
		};

		self.ProcessDelete = function (deleteIds) {
			return self.Processes.delete(deleteIds);
		};

		self.ItemColumnsSave = function (ProcessName, ItemColumnArray) {
			return self.ItemColumns.new([ProcessName], ItemColumnArray);
		};

		self.ItemColumnsSaveMany = function (ProcessName, ItemColumnArray) {
			return self.ItemColumns.new([ProcessName, "many"], ItemColumnArray);
		};

		self.ItemColumnsPut = function (ProcessName, ItemColumnArray) {
			return self.ItemColumns.save([ProcessName], ItemColumnArray);
		};

		self.ItemColumnsDelete = function (ProcessName, itemColumns) {
			return self.ItemColumns.delete([ProcessName, itemColumns]);
		};

		self.ListsProcessesDelete = function (subProcesses) {
			return self.listProcesses.delete(subProcesses);
		};

		self.ListProcessesSave = function (process, ListProcesses) {
			return self.listProcesses.new([process], ListProcesses);
		};

		self.ListProcessesPut = function (process, ListProcesses) {
			return self.listProcesses.save([process], ListProcesses);
		};

		self.ListProcessesSubItemSave = function (ProcessName, parentItemId, isRecursive, Process) {
			return self.listProcesses.new([ProcessName, parentItemId, isRecursive], Process);
		};

		self.ListProcessesSubItemPut = function (ProcessName, parentItemId, Process) {
			return self.listProcesses.save([ProcessName, parentItemId], Process);
		};

		self.ProcessItemsDelete = function (ProcessName, ids: string) {
			return self.ProcessItems.delete([ProcessName, ids]);
		};

		self.ProcessItemsSave = function (ProcessName, parentProcesses) {
			return self.ProcessItems.new([ProcessName, parentProcesses]);
		};

		self.addSelectorColumns = function(processName, itemColumnIds){
			return self.listProcesses.save([processName, "selectorcolumns"], itemColumnIds);
		}

		self.addNewList = function (parentProcess, userLanguage) {
			let newListData = {
				LanguageTranslation: null,
				LanguageTranslationRaw: null,
				ListProcess: 1,
				SystemProcess: 0,
				Variable: null,
				ParentProcess: parentProcess,
				ListType: null,
				Compare: 0
			};


			let modifications = {
				create: false,
				cancel: true
			};

			let onChange = function (model) {
				if (model == 'LanguageTranslationRaw') {
					newListData.Variable = Common.formatVariableName(newListData.LanguageTranslationRaw[userLanguage]);
				} else {
					newListData.Variable = Common.formatVariableName(newListData.Variable);
				}
				SettingsService.checkDuplicateVariable('process', 'list_' + newListData.Variable).then(function (result) {
					if (newListData.Variable.length > 0 && newListData.LanguageTranslationRaw[userLanguage].length > 0) {
						modifications.create = result != '';
					} else {
						modifications.create = false;
					}
				});
			};

			let save = function () {
				newListData.Variable = 'list_' + newListData.Variable;
				newListData.LanguageTranslation = newListData.LanguageTranslationRaw;
				return self.ListProcessesSave('', newListData).then(function (response) {
					return response;
				});
			};

			let content = {
				buttons: [{text: 'Discard'}, {onClick: save, text: 'Save', type: 'primary', modify: 'create'}],
				inputs: [{type: 'translation', label: 'Translation', model: 'LanguageTranslationRaw', params: '1'},
					{type: 'string', label: 'Variable', model: 'Variable'}
				],
				title: 'ADD_LIST'
			};
			return MessageService.dialog(content, newListData, onChange, modifications);
		};
	}

})();