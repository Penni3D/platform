(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(ListsColumnsAddConfig)
		.controller('ListColumnsAddController', ListColumnsAddController);


	ListsColumnsAddConfig.$inject = ['ViewsProvider'];

	function ListsColumnsAddConfig(ViewsProvider) {
		ViewsProvider.addView('listColumnsAdd', {
			controller: 'ListColumnsAddController',
			template: 'pages/lists/listsColumnsAdd.html',
		});
	}

	ListColumnsAddController.$inject = ['$scope', '$rootScope', 'DataTypes', 'Translator', 'ListsService', 'StateParameters', 'ListColumns', 'MessageService', 'Lists', 'ListColumnItems', 'Processes'];

	function ListColumnsAddController($scope, $rootScope, DataTypes, Translator, ListsService, StateParameters, ListColumns, MessageService, Lists, ListColumnItems, Processes) {

		$scope.dataTypes = DataTypes;
		$scope.newColumn = { LanguageTranslationRaw: null, DataType: 0, DataAdditionalProcess: null, DataAdditionalList: null, Max: 1};
		$scope.activeLanguage = $rootScope.me.locale_id;
		$scope.lists = Lists;
		$scope.processes = Processes;
		$scope.activeListName = "";

		$scope.newListColumnSave = function () {
			if (Object.keys($scope.newColumn.LanguageTranslationRaw).length == 0
				|| typeof $scope.newColumn.LanguageTranslationRaw[$scope.activeLanguage] === 'undefined'
				|| $scope.newColumn.LanguageTranslationRaw[$scope.activeLanguage] == '') {
				MessageService.msgBox('ADD_LIST_COLUMN_NO_NAME');
				return;
			}

			if ($scope.newColumn.DataType == 5) {
				if ($scope.newColumn.DataAdditionalProcess === null) {
					MessageService.msgBox('ADD_LIST_COLUMN_NO_PROCESS');
					return;
				}
				$scope.newColumn.DataAdditional = $scope.newColumn.DataAdditionalProcess
			}
			else if ($scope.newColumn.DataType == 6) {
				if ($scope.newColumn.DataAdditionalList === null) {
					MessageService.msgBox('ADD_LIST_COLUMN_NO_LIST');
					return;
				}
				$scope.newColumn.DataAdditional = $scope.newColumn.DataAdditionalList
			}

			ListsService.ItemColumnsSave(StateParameters.process, {
				LanguageTranslationRaw: $scope.newColumn.LanguageTranslationRaw,
				DataType: $scope.newColumn.DataType,
				Variable: $scope.newColumn.LanguageTranslationRaw[$scope.activeLanguage],
				DataAdditional: $scope.newColumn.DataAdditional,
				Validation: { Max: $scope.newColumn.Max, Suffix: '', ParentId: '', DateRange: '', Label: '' }
			}).then(function (newColumn) {
				ListColumns.push(newColumn);
				if(newColumn.DataType == 6){
					ListColumnItems[newColumn.DataAdditional] = [];
					ListsService.getProcessItems(newColumn.DataAdditional, 'order').then(function(items){
						ListColumnItems[newColumn.DataAdditional] = items;
						MessageService.closeDialog();
					})
				} else {
					MessageService.closeDialog();
				}
			});
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};
	}
})();