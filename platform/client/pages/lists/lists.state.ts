(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(ListsConfig)
		.controller('ListsController', ListsController)
		.controller('ListsEditController', ListsEditController)
		.controller('ListsSublistsController', ListsSublistsController)
		.controller('ListsItemsController', ListsItemsController);

	ListsConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function ListsConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('lists', {
			controller: 'ListsController',
			template: 'pages/lists/lists.html',
			resolve: {
				States: function (ViewConfigurator, StateParameters) {
					return ViewConfigurator.getRights('lists', StateParameters.NavigationMenuId);
				},

				ListOptions: function (ListsService) {
					return ListsService.getProcessItems('list_admin_types', 'order');
				},

				PageType: function (ListsService, StateParameters) {
					return ListsService.getProcessItems('list_admin_types', 'order').then(function (items) {
						return _.some(items, {'system_item': 1, 'item_id': StateParameters.PageTypeItemId});
					});
				},

				Hierarchy: function (ListsService, StateParameters, ViewService) {

					let duplicateCheck = [];

					if (!StateParameters.PageTypeItemId) return ViewService.fail('List Page Type has not been set. Go to menu editor and set the list page type value');
					return ListsService.getListHierarchy(StateParameters.PageTypeItemId).then(function (lists) {

						let news = [];

						_.each(lists, function (list) {
							if (!_.includes(duplicateCheck, list.Process.ProcessName)) {
								duplicateCheck.push(list.Process.ProcessName);
								news.push(list);
							}

						});

						return news;
					});
				}
			},
		});

		ViewsProvider.addView('listsEdit', {
			controller: 'ListsEditController',
			template: 'pages/lists/listsEdit.html',
			options: {
				replace: 'listsEdit',
				split: true
			}
		});

		ViewsProvider.addView('listsItems', {
			controller: 'ListsItemsController',
			template: 'pages/lists/listsItems.html',
			resolve: {
				States: function (ViewConfigurator, StateParameters) {
					return ViewConfigurator.getRights('lists', StateParameters.NavigationMenuId);
				},
				List: function (ListsService, StateParameters, $q) {
					let d = $q.defer();
					let promises = [];
					let data = {};
					let process = StateParameters.process;
					ListsService.getListProcesses(process).then(function (processData) {
						if(StateParameters.Recursive == 1){
							promises.push(ListsService.getRecursiveHierarchy(processData.ListOrder))
						}
						ListsService.getItemColumnsData(process).then(function (columns) {
							ListsService.getFilledProcessItems(process, 'order', columns, processData.ListOrder).then(function (items) {
								angular.forEach(columns, function (itemColumn) {
									if (itemColumn.Name == "user_group") {
										angular.forEach(items, function (processItem) {
											if (processItem.user_group) {
												let regex = /\d+/g;
												let matches = processItem.user_group.match(regex);
												processItem.user_group = [];
												for (let i of matches){
													processItem.user_group.push(parseInt(i));
												}

											}
										});

									} else if (itemColumn.DataType == 5 || itemColumn.DataType == 6) {
										angular.forEach(items, function (processItem) {
											if (angular.isUndefined(data[processItem['item_id']])) {
												data[processItem['item_id']] = {};
											}
											data[processItem['item_id']][itemColumn.ItemColumnId] = processItem[itemColumn.Name];
										});
									}
								});

								$q.all(promises).then(function (result) {
									d.resolve({
										ListColumns: columns,
										ListItems: items,
										ProcessSelections: data,
										RecursiveHierarchy: result[0]
									});
								});
							});
						});

					});
					return d.promise;
				},
				Symbols: function (ListsService) {
					return ListsService.getSystemLists('SYMBOLS').then(function (symbols) {
						let sym = [];
						angular.forEach(symbols, function (s) {
							s.hasIcon = true;
							s.icon = s.value;
							s.iconColor = 'black';
							sym.push(s);
						});
						return sym;
					});
				},
				Lists: function (ListsService) {
					return ListsService.getAllInstanceListProcesses();
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				}
			}
		});

		ViewsProvider.addView('sublists', {
			controller: 'ListsSublistsController',
			template: 'pages/lists/listsSublists.html',
			resolve: {
				CurrentSubLists: function (ListsService, StateParameters) {
					return ListsService.getListProcessesData(1, StateParameters.process).then(function (processes) {
						return processes;
					});
				}
			}
		});

		ViewsProvider.addPage('lists', 'PAGE_LISTS', ['read', 'write', 'admin']);

		ViewConfiguratorProvider.addConfiguration('lists', {
			onSetup: {
				List: function (ListsService) {
					return ListsService.getProcessItems('list_admin_types', 'order');
				}
			},

			tabs: {
				interface: {
					PageTypeItemId: function (resolves) {
						return {
							type: 'select',
							options: resolves.List,
							label: 'LIST_PAGE_TYPE',
							optionValue: 'item_id',
							optionName: 'list_item'
						};
					},
				}
			}
		});
	}

	ListsController.$inject = ['$q', '$rootScope', '$scope', 'Translator', 'ListOptions', '$filter', 'ListsService', 'MessageService', 'SaveService', 'Common', 'ViewService', 'StateParameters', 'Hierarchy', 'SettingsService', 'PageType', 'States'];

	function ListsController($q, $rootScope, $scope, Translator, ListOptions, $filter, ListsService, MessageService, SaveService, Common, ViewService, StateParameters, Hierarchy, SettingsService, PageType, States) {

		$scope.selectedRows = [];
		$scope.lists = Hierarchy;
		$scope.filteredLists = $scope.lists;
		$scope.adminPage = PageType;
		$scope.listOptions = ListOptions;
		$scope.orderBys = {};

		$scope.RightWrite = (States.indexOf("write") > -1);
		if (States.indexOf("admin") > -1) $scope.RightWrite = true;


		let processOptionValues = [];
		$scope.processOptions = [];
		_.each($scope.lists, function (list) {
			$scope.orderBys[list.Process.ProcessName] = angular.copy(list.Process.ItemColumns);
			$scope.orderBys[list.Process.ProcessName].splice(0, 0, {LanguageTranslation: 'ORDER_NO', Name: "order_no"});

			for (let s of list.Process.ListProcesses.split(",")) {
				let p = s.toString().trim();
				if (processOptionValues.indexOf(p) == -1 && p != "") processOptionValues.push(p);
			}
		});
		for (let v of processOptionValues) {
			$scope.processOptions.push({name: v, value: v});
		}

		$scope.header = {
			title: 'LISTS'
		};

		let changedLists = [];

		$scope.selectedFilter;
		$scope.textFilter = "";
		$scope.filterLists = function () {
			let tempLists = [];
			if (!$scope.selectedFilter || $scope.selectedFilter == "") {
				tempLists = $scope.lists;
			} else {
				tempLists = [];
				for (let p of $scope.lists) {
					for (let s of p.Process.ListProcesses.split(",")) {
						let value = s.toString().trim();
						if ($scope.selectedFilter == value) {
							tempLists.push(p);
							break;
						}
					}
				}
			}

			$scope.filteredLists = [];
			if ($scope.textFilter != "") {
				for (let l of tempLists) {
					let name = l.Process.LanguageTranslation[$rootScope.clientInformation.locale_id];
					if (name.toLowerCase().indexOf($scope.textFilter.toLowerCase()) > -1) $scope.filteredLists.push(l);
				}
			} else {
				$scope.filteredLists = tempLists;
			}
		}

		$scope.addList = function () {
			ListsService.addNewList(null, $rootScope.me.locale_id).then(function (newList) {
				if (newList) {
					let nl = {Level: 0, Process: ""};
					nl.Level = 0;
					nl.Process = newList;
					$scope.lists.push(nl);

					StateParameters.Recursive = 0;
					StateParameters.process = newList.ProcessName;
					StateParameters.thisList = nl;
					StateParameters.adminPage = $scope.adminPage;
					
					ViewService.view('listsEdit', {params: StateParameters}, {split: true});
				}
			});
		};

		let findIndex = function (_variable) {
			let retVal = null;
			_.each($scope.lists, function (list, key) {
				if (list.Process.Variable == _variable) {
					retVal = key;
				}
			});
			return retVal;
		};

		$scope.deleteList = function () {
			MessageService.confirm('CONFIRM_DELETE', function () {
				let allowedToDelete = [];
				let notAllowedToDelete = [];
				angular.forEach($scope.selectedRows, function (value) {

					let listToDelete = $scope.lists[value.index];

					if (listToDelete.Process.ListProcesses == '' || listToDelete.Process.ListProcesses == null) {
						allowedToDelete.push(listToDelete.Process);
					} else {
						notAllowedToDelete.push(listToDelete.Process);
					}
				});

				if (allowedToDelete.length > 0) {
					ListsService.ProcessDelete(Common.convertArraytoString(allowedToDelete, 'ProcessName')).then(function () {
						angular.forEach(allowedToDelete, function (deleted) {
							changedLists = [];
							$scope.selectedRows = [];
							MessageService.deleteSuccess();
							$scope.lists.splice(findIndex(deleted.Variable), 1);
						});
					});
				}
				if (notAllowedToDelete.length > 0) {
					let message = Translator.translate('LIST_DELETE_NOT_ALLOWED') + '' + '\n';
					angular.forEach(notAllowedToDelete, function (value2) {
						message += $scope.lists[findIndex(value2.Variable)].Process.LanguageTranslation[$rootScope.me.locale_id];
					});
					setTimeout(function () {
						MessageService.msgBox(message);
					}, 500);
				}
			});
		};

		$scope.selectorColumns = function (process, ProcessPortfolioId) {
			let params = StateParameters;
			params.process = process;
			params.ProcessPortfolioId = ProcessPortfolioId;
			params.type = 'selector';
			ViewService.view('settings.summary', {params: {processType: 0, compare: 0, process: process}});
		};


		$scope.setAsSelector = function (variable) {
			let index = findIndex(variable);
			if ($scope.lists[index].Process.Selector == true) {
				let portfolio = {
					ProcessPortfolioId: 0,
					ProcessPortfolioType: 1,
					LanguageTranslation: $scope.lists[index].Process.LanguageTranslation,
					CalendarType: 0,
					CalendarStartId: 0,
					CalendarEndId: 0,
					DefaultState: '',
				};

				SettingsService.ProcessPortfoliosAdd($scope.lists[index].Process.ProcessName, portfolio).then(function (portfolio) {
					$scope.lists[index].Process.ProcessPortfolioId = portfolio.ProcessPortfolioId;

					let condition = {
						LanguageTranslation: {},
						Process: $scope.lists[index].Process.ProcessName,
						Features: [],
						ConditionId: 0,
						OrderNo: 'V'
					};
					condition.LanguageTranslation[$rootScope.me.locale_id] = 'read';
					SettingsService.ConditionsAdd($scope.lists[index].Process.ProcessName, condition).then(function (data) {
						data.Features = ['meta', 'portfolio'];
						data.States = ['meta.read', 'portfolio.read'];
						data.ProcessPortfolioIds = [portfolio.ProcessPortfolioId];
						SettingsService.ConditionsSave($scope.lists[index].Process.ProcessName, [data]);
					});
				});
			} else if ($scope.lists[index].Process.Selector == false) {
				SettingsService.ProcessPortfoliosDelete($scope.lists[index].Process.ProcessName, $scope.lists[index].Process.ProcessPortfolioId).then(function () {
					$scope.lists[index].Process.ProcessPortfolioId = null;
					SettingsService.Conditions($scope.lists[index].Process.ProcessName).then(function (data) {
						let deleteIds = Common.convertArraytoString(data, 'ConditionId');
						SettingsService.ConditionsDelete($scope.lists[index].Process.ProcessName, deleteIds);
					});
				});
			}
		};

		let findSubProcesses = function () {
			_.each($scope.lists, function (list) {
				if (list.Level > 0) {
					list.Process.isSublist = true;
				}
			});
		};
		findSubProcesses();

		$scope.listTypes = [
			{name: 'ONE_LIST', value: 0},
			{name: 'RECURSIVE', value: 1}
		];

		$scope.showTypeOptions = [
			{value: 0, name: Translator.translate('DEFAULT')},
			{value: 1, name: Translator.translate('TEXT')},
			{value: 2, name: Translator.translate('SYMBOL')},
			{value: 3, name: Translator.translate('BOTH')}];

		$scope.compareOptions = [
			{value: 0, name: Translator.translate('LIST_COMPARE0')},
			{value: 1, name: Translator.translate('LIST_COMPARE1')},
			{value: 2, name: Translator.translate('LIST_COMPARE2')},
			{value: 3, name: Translator.translate('LIST_COMPARE3')}];

		let findThisList = function (nameToFind) {
			return _.find($scope.lists, function (l) {
				return l.Process.ProcessName == nameToFind;
			});
		};

		$scope.changeValue = function (params) {
			if (typeof Common.getIndexOf(changedLists, params.List.Process.ProcessName, 'ProcessName') == 'undefined') {
				params.List.Process.SubProcesses = [];
				changedLists.push(params.List.Process);
			}
		};

		SaveService.subscribeSave($scope, function () {
			if (changedLists.length > 0) {
				ListsService.ListProcessesPut('', changedLists).then(function () {
					changedLists = [];
				});
			}
		});

		$scope.editList = function (listParams) {
			StateParameters.Recursive = listParams.ProcessType;
			StateParameters.process = listParams.ProcessName;
			StateParameters.thisList = findThisList(listParams.ProcessName);
			StateParameters.adminPage = $scope.adminPage;
			StateParameters.orderable = listParams.ListOrder;
			ViewService.view('listsEdit', {params: StateParameters}, {split: true});
		};
		if ($scope.RightWrite) {
			ViewService.footbar(StateParameters.ViewId, {
				template: 'pages/lists/listsFooter.html',
				scope: $scope
			});
		}

	}

	ListsEditController.$inject = ['$scope', 'StateParameters', 'ViewService', '$rootScope'];

	function ListsEditController($scope, StateParameters, ViewService, $rootScope) {

		$scope.selectedView = 'list_items';
		$scope.header = {title: StateParameters.thisList.Process.LanguageTranslation[$rootScope.clientInformation.locale_id]};

		if (StateParameters.Recursive == 0 && StateParameters.adminPage) {
			$scope.listViews = [
				{name: 'LIST_ITEMS', value: 0, viewName: 'list_items'},
				{name: 'SUBLISTS', value: 1, viewName: 'list_sublist'},
				{name: 'SETTINGS_VIEW_CONDITIONS', value: 2, viewName: 'cc'}
			];

		} else if (StateParameters.Recursive == 1 && StateParameters.adminPage) {
			$scope.listViews = [
				{name: 'LIST_ITEMS', value: 0, viewName: 'list_items'},
				{name: 'SETTINGS_VIEW_CONDITIONS', value: 2, viewName: 'cc'}
			];

		} else {
			$scope.listViews = [{name: 'LIST_ITEMS', value: 0, viewName: 'list_items'}]
		}

		$scope.showConditions = function () {

			$scope.selectedView = 'cc';
			StateParameters.FromList = true;
			setTimeout(function () {
				ViewService.viewAdvanced('settings.conditions', {
					params: StateParameters
				}, {targetId: 'cc', ParentViewId: StateParameters.ViewId});
			}, 0);
		};

		$scope.showItems = function () {
			$scope.selectedView = 'list_items';
			setTimeout(function () {
				ViewService.viewAdvanced('listsItems', {
					params: StateParameters
				}, {targetId: 'list_items', ParentViewId: StateParameters.ViewId});
			}, 0);
		};

		$scope.editSubList = function () {
			$scope.selectedView = 'list_sublist';
			setTimeout(function () {
				ViewService.viewAdvanced('sublists', {
					params: StateParameters
				}, {targetId: 'list_sublist', ParentViewId: StateParameters.ViewId});
			}, 0);
		};

		$scope.pickView = function (x) {
			if (x == 0) {
				$scope.showItems();
			} else if (x == 1) {
				$scope.editSubList();
			} else {
				$scope.showConditions();
			}
		};
		$scope.pickView(1);
	}

	ListsItemsController.$inject = ['$scope', '$q', '$rootScope', 'SaveService', 'StateParameters', 'ViewService', 'List', 'Common', 'ListsService', 'MessageService', 'Symbols', 'Colors', 'SelectorService', 'Translator', 'SettingsService', 'UserService', 'States', 'Lists', 'Processes'];

	function ListsItemsController($scope, $q, $rootScope, SaveService, StateParameters, ViewService, List, Common, ListsService, MessageService, Symbols, Colors, SelectorService, Translator, SettingsService, UserService, States, Lists, Processes) {
		$scope.RightWrite = (States.indexOf("write") > -1);
		if (States.indexOf("admin") > -1) $scope.RightWrite = true;
		$scope.parentOptionName = 'list_item';
		$scope.showSymbol = 0;
		$scope.showUserGroups = 0;

		$scope.ugSelector = SelectorService.getProcessSelector('user_group') ? SelectorService.getProcessSelector('user_group').ProcessPortfolioId : 0;

		let defaultLanguage = "list_item_" + $rootScope.clientInformation.default_locale_id.toLowerCase().replace('-', '_');
		let myLanguageListItem = "list_item_" + $rootScope.clientInformation.locale_id.toLowerCase().replace('-', '_');

		let checkIfSymbols = function () {
			_.each(List.ListItems, function (item) {
				if (item.hasOwnProperty('list_symbol') && item['list_symbol'] != null) $scope.showSymbol = 1;
				if (item.hasOwnProperty('user_group') && item['user_group'] != null) $scope.showUserGroups = 1;
			});
		};

		$scope.adminMode = StateParameters.adminPage;
		$scope.listColumns = List.ListColumns;
		$scope.multi = 0;
		$scope.activeLang = $rootScope.clientInformation.locale_id;
		$scope.inUseOptions = [{'value': 1, 'name': 'IN_USE'}, {'value': 0, 'name': 'NOT_IN_USE'}, {
			'value': 2,
			'name': 'NOT_IN_USE_SHOW_FILTERS'
		}];

		$scope.windowHeight = 558;

		function resize() {
			let v = ViewService.getView(StateParameters.ViewId);
			$scope.windowHeight = v.parent.$content[0].offsetParent.clientHeight - ($scope.adminMode ? 200 : 150);
		}

		resize();

		Common.subscribeContentResize($scope, function () {
			resize();
		});


		checkIfSymbols();

		let langColumns = [];

		$scope.listColumnItems = {};

		_.each($scope.listColumns, function (lc_) {
			if (lc_.Name.includes("list_item_")) {
				langColumns.push(lc_)
			}

			if (lc_.DataType == 6 && lc_.DataAdditional != null) {
				$scope.listColumnItems[lc_.DataAdditional] = [];
				ListsService.getProcessItems(lc_.DataAdditional, 'order').then(function (items) {
					$scope.listColumnItems[lc_.DataAdditional] = items;
				})
			}
		});

		_.each($scope.listColumns, function (lc) {
			if (lc.Name == myLanguageListItem) {
				$scope.multi = 1;
			}
		});
		let executed = $scope.multi;

		$scope.getTrans = function (raw) {
			if (_.isEmpty(raw) || !raw.hasOwnProperty($scope.activeLang)) return "";
			if (raw[$scope.activeLang].toLowerCase().indexOf("translation") > -1)
				return typeof raw != 'undefined' ? Translator.translate(raw[$scope.activeLang].toUpperCase().replace(" ", "_")) : "";
			return typeof raw != 'undefined' ? raw[$scope.activeLang] : "";
		};

		$scope.isNumberCol = function (ic) {
			return !(ic.DataType == 1 || ic.DataType == 2 || ic.DataType == 3 || ic.DataType == 13);
		};

		function setupLangColumns() {
			let temp = [];
			_.each(langColumns.reverse(), function (lc) {
				let p = _.findIndex($scope.listColumns, function (o) {
					// @ts-ignore
					return o.ItemColumnId == lc.ItemColumnId;
				});
				$scope.listColumns.splice(p, 1);
				temp.push(lc)
			});

			setTimeout(function () {
				_.each(temp, function (v) {
					$scope.defaultColumns.unshift(v);
				})
			}, 0);

		}

		$scope.createLanguageColumns = function (value) {
			$scope.multi = value;
			if ($scope.multi == 1) {
				if (executed == 0) {
					ViewService.lock(StateParameters.ParentViewId);
					let mainLanguage = $rootScope.clientInformation.locale_id;
					let langCols = [];
					_.each($rootScope.languages, function (language) {
						let name = "list_item_" + language.value.toLowerCase().replace('-', '_');
						let langColumn = {LanguageTranslationRaw: {}, DataType: 0, Variable: name};
						langColumn.LanguageTranslationRaw[mainLanguage] = "Translation " + language.value;
						langCols.push(langColumn);
					});
					mapTranslations();
					ListsService.ItemColumnsSaveMany(StateParameters.process, langCols).then(function (nc) {

						let columnIds = [];
						_.each(nc, function (n) {
							langColumns.push(n);
							columnIds.push(n.ItemColumnId);
						});

						_.each($scope.listItems, function (item) {
							item[defaultLanguage] = item.list_item;
							$scope.modifyItems({ListItem: item});

						});
						for (let i = (langColumns.length - 1); i > -1; i--) {
							$scope.defaultColumns.unshift(langColumns[i]);
						}

						disableListItem();
						ViewService.unlock(StateParameters.ParentViewId);
						executed = 1;
						ListsService.addSelectorColumns(StateParameters.process, columnIds);
					});
				}
			} else {
				let puzzleAnswer = StateParameters.process;
				let puzzleTitle = Translator.translate('DELETE_CONFIRMATION_SERIOUS2')
					.replace('[X]', puzzleAnswer);

				let model = {
					puzzle: ''
				};

				let params = {
					message: puzzleTitle,
					inputs: [
						{
							type: 'string',
							label: 'DELETE_PUZZLE',
							model: 'puzzle'
						}],
					buttons: [
						{
							type: 'warn',
							text: 'DELETE_CONFIRMATION_OK',
							onClick: function () {
								if (model.puzzle == puzzleAnswer) {
									let deleteIds = Common.convertArraytoString(langColumns, 'ItemColumnId');
									SettingsService.ItemColumnsInContainersDeleteMany(StateParameters.process, deleteIds).then(function () {
										_.each(langColumns, function (col) {
											let delIndex = Common.getIndexOf($scope.defaultColumns, col.ItemColumnId, 'ItemColumnId');
											$scope.defaultColumns.splice(delIndex, 1);
											langColumns = [];
											executed = 0;
										});

										_.each($scope.listItems, function (b, index) {
											_.each(b, function (columnValue, columnName) {
												// @ts-ignore
												if (columnName.includes("list_item_")) {
													$scope.listItems[index][columnName] = null;
												}
											});
										});
										let k = _.find(initialDefaultColumns, function (o) {
											return o.Name == 'list_item';
										});
										$scope.defaultColumns.unshift(k)
									});
								} else {
									MessageService.toast('DELETE_PUZZLE_FAIL', 'warn');
									$scope.multi = 1;

								}
							}
						},
						{
							text: 'MSG_CANCEL',
							onClick: function () {
								$scope.multi = 1;
							}
						}]
				};

				MessageService.dialog(params, model);
			}
		};

		$scope.ul = $rootScope.me.locale_id;
		$scope.processesData = List.ProcessSelections;
		$scope.listItems = List.ListItems;
		$scope.symbols = Symbols;
		$scope.symbolColors = Colors.getColorsArray();
		for (let sc of $scope.symbolColors) {
			sc.symbol = "lens";
		}

		$scope.selectedParentList;
		$scope.selectedParentListItem = 0;
		$scope.adminPage = StateParameters.adminPage;
		$scope.selectedParents = [];

		$scope.orderable = StateParameters.orderable;
		$scope.portfolioId = 0;
		$scope.selectedRows = [];
		$scope.portfolioColumns = [];

		$scope.hasParentList = false;
		$scope.parentProcessesLoaded = false;

		$scope.addAllowed = false;

		let myHierarchy;
		$scope.recursiveParents = [];
		$scope.parentProcesses = [];
		$scope.parents = [];

		$scope.processChange = function (params) {
			if (params.itemColumn.DataType == 5) {
				let promises = [];
				let d = {};
				d[params.itemColumn.ItemColumnId] = [];
				angular.forEach($scope.processesData[params.item.item_id][params.itemColumn.ItemColumnId], function (userId) {
					let p = $q.defer();
					promises.push(p);
					UserService.getUser(userId).then(function (user) {
						p.resolve(user)
					});

					let user = {
						ItemId: params.item.item_id,
						ItemColumnId: params.itemColumn.ItemColumnId,
						SelectedItemId: userId
					};
					d[params.itemColumn.ItemColumnId].push(user);
				});
				$q.all(promises).then(function () {
					ListsService.saveItemsDataProcessSelections(StateParameters.process, params.item.item_id, d);
				});
			} else {
				let d = {};
				d[params.itemColumn.ItemColumnId] = [];
				angular.forEach($scope.processesData[params.item.item_id][params.itemColumn.ItemColumnId], function (listItemId) {
					let selectedListValue = {
						ItemId: params.item.item_id,
						ItemColumnId: params.itemColumn.ItemColumnId,
						SelectedItemId: listItemId
					};
					d[params.itemColumn.ItemColumnId].push(selectedListValue);
				});
				ListsService.saveItemsDataProcessSelections(StateParameters.process, params.item.item_id, d);
			}
		};

		let whatsMyHierarchy = function () {
			_.each(List.RecursiveHierarchy, function (v, k) {
				if (k == StateParameters.process) {
					myHierarchy = v;
				}
			});
		};

		let firstParent;
		let listParentChildRelations = {};

		let guessParentLists = function () {
			ListsService.getListProcessesData(2, StateParameters.process).then(function (parents) {
				$scope.parents = parents;
				$scope.parentCollection = {};
				for (let i = 0; i < parents.length; i++) {
					if (parents[i].ParentProcess == null) {
						firstParent = parents[i];
						ListsService.getProcessItems(firstParent.ProcessName, 'order').then(function (pItems) {
							if (pItems.length) {
								_.each(pItems[0], function (c, columna) {
									// @ts-ignore
									if (columna.includes("list_item_")) $scope.parentOptionName = myLanguageListItem;
								});
							}
							$scope.parentCollection[firstParent.ProcessName] = pItems;
						});
					}
					listParentChildRelations[parents[i].ProcessName] = parents[i].ParentProcess;
				}
			});
		};

		let selectedParentName = "";

		let getSingleLevelRecursion = function () {
			$scope.itemsRecursive = List.ListItems;
			$scope.itemsRecursive.unshift({item_id: 0, list_item: 'RECURSIVE_PARENT_ITEMS'});
		}

		$scope.changeParentProcess = function (parentName) {
			selectedParentName = parentName;
			$scope.selectedParentListItem = null;
			$scope.listItems = [];
			let processName = "";
			_.each(listParentChildRelations, function (v, k) {
				if (v == parentName) {
					processName = k;
				}
			});
			let selectedItem = $scope.selectedParents[parentName];
			if (_.includes(_.map(listParentChildRelations), parentName)) {
				ListsService.getChildProcessItems(processName, 1, selectedItem).then(function (childItems) {
					if (childItems.length > 0) {
						$scope.parentCollection[processName] = childItems;
					}
				});
			} else {
				$scope.selectedParentListItem = selectedItem;
				ListsService.getChildProcessItems(StateParameters.process, 1, selectedItem).then(function (data) {

					$scope.itemsRecursive = [];
					if ($scope.recursive == 1) {
						_.each(data, function (row) {
							findAllChildren(row.item_id, $scope.itemsRecursive);
						})
						_.each(data, function (row) {
							$scope.itemsRecursive.push(row)
						})
					}

					$scope.itemsRecursive.unshift({item_id: 0, list_item: 'RECURSIVE_PARENT_ITEMS'});
					$scope.listItems = data;

					angular.forEach($scope.listItems, function (processItem) {
						if (processItem.user_group) {
							let arr = processItem.user_group.split(",");
							processItem.user_group = [];
							for (let i of arr)
								processItem.user_group.push(parseInt(i));
						}
					});


					$scope.addAllowed = true;
				});
			}
		};

		let getSubItems = function () {
			ListsService.getListProcessesDataNew(2, StateParameters.process).then(function (parentLists_) {
				$scope.parentLists = parentLists_;
				_.each($scope.parentLists, function (parentList) {
					ListsService.getProcessItems(parentList.ProcessName, 'order').then(function (parentItems_) {
						$scope.parentItems[parentList.ProcessName] = parentItems_;
						$scope.parentProcessesLoaded = true;
					});
				});
			});
		};

		console.log("1")


		$scope.isSublist = StateParameters.thisList.Process.isSublist;

		$scope.getRecursiveSubItems = function (id?) {
			$scope.selectedParentListItem = id;
			whatsMyHierarchy();
			initializePairs();
			if (id == 0 && StateParameters.thisList.Process.isSublist === true) $scope.changeParentProcess(selectedParentName);
		};

		if (StateParameters.Recursive == 0 && !StateParameters.thisList.Process.isSublist) {
			$scope.addAllowed = true;
		}

		if (StateParameters.Recursive == 1) {
			$scope.recursive = true;
			$scope.addAllowed = true;

			if (StateParameters.thisList.Process.isSublist === true) {
				whatsMyHierarchy();
				initializePairs();
			} else {
				$scope.getRecursiveSubItems(0);
				getSingleLevelRecursion();
			}

		} else {
			$scope.recursive = false;
		}

		if (StateParameters.thisList.Process.isSublist) {
			$scope.hasParentList = true;
			$scope.listItems = []; //this might cause timing problems
			guessParentLists();
			getSubItems();
		}

		List['Defaults'] = [];

		let i = $scope.listColumns.length;
		while (i--) {
			if ($scope.listColumns[i].Name == 'list_item' || $scope.listColumns[i].Name == 'list_symbol' || $scope.listColumns[i].Name == 'list_symbol_fill' || $scope.listColumns[i].Name == 'in_use' || $scope.listColumns[i].Name == 'user_group') {
				List['Defaults'].push($scope.listColumns[i]);
				$scope.listColumns.splice(i, 1);
			}
		}

		$scope.syncOrder = function (current, prev, next) {
			let beforeOrderNo;
			let afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.order_no;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.order_no;
			}

			current.order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			let toSave = {};
			toSave['ListItem'] = current;
			$scope.modifyItems(toSave);
			SaveService.tick();
		};

		$scope.addListItem = function (pitem?, lvl?, _index?) {
			let parentItemId;
			let newListRow = {process: StateParameters.process, parent_item_id: null, in_use: 1};
			if (StateParameters.Recursive == 1) {

				if (typeof pitem != 'undefined') {
					parentItemId = pitem
				} else {
					parentItemId = $scope.selectedParentListItem;
				}
				newListRow = {process: StateParameters.process, parent_item_id: parentItemId};
			} else {
				parentItemId = $scope.selectedParentListItem[0];
			}

			if (parentItemId != null && parentItemId != 0) {
				ListsService.ListProcessesSubItemSave(StateParameters.process, parentItemId, $scope.recursive, newListRow).then(function (item) {
					item.in_use = 1;
					if (typeof _index == 'undefined') {
						_index = $scope.listItems.length - 1
					}

					let indexToInsert = 0;

					if (_.filter($scope.listItems, function (o) {
						return o.parent_item_id == pitem;
					}).length == 0) {
						indexToInsert = _index + 1;
					} else {
						indexToInsert = _index + _.filter($scope.listItems, function (o) {
							return o.parent_item_id == pitem;
						}).length;
					}

					$scope.listItems.splice(indexToInsert, 0, item);

					if ($scope.recursive) {
						$scope.itemsRecursive.push(item);
						if (List.RecursiveHierarchy[StateParameters.process].hasOwnProperty(parentItemId)) {
							List.RecursiveHierarchy[StateParameters.process][parentItemId].push(item.item_id);
						} else {
							List.RecursiveHierarchy[StateParameters.process][parentItemId] = [];
							List.RecursiveHierarchy[StateParameters.process][parentItemId].push(item.item_id);
						}
						if ($scope.isSublist) List.ListItems.push(item)

					}
				});
			} else {
				if (typeof newListRow.parent_item_id == 'undefined') {
					newListRow.parent_item_id = 0;
				}
				ListsService.ListProcessesSave(StateParameters.process, newListRow).then(function (item) {
					item.in_use = 1;
					if (item.parent_item_id == 0 || item.parent_item_id == null) {
						$scope.recursiveParents.push(item);
					}
					$scope.listItems.push(item);
				});
			}
		};

		$scope.defaultColumns = _.sortBy(List.Defaults, ['ItemColumnId']);
		$scope.lastColumns = [];

		let inuseindex = _.findLastIndex($scope.defaultColumns, function (o) {
			return o.Name == 'in_use'
		});

		if (inuseindex != -1) {
			$scope.lastColumns.push($scope.defaultColumns[inuseindex]);
			$scope.defaultColumns.splice(inuseindex, 1);
		}

		let initialDefaultColumns = angular.copy($scope.defaultColumns);

		let disableListItem = function () {
			if ($scope.multi == 1) {
				let list_item_id = _.find(initialDefaultColumns, function (o) {
					return o.Name == 'list_item';
				}).ItemColumnId;
				let ind = _.findIndex($scope.defaultColumns, function (o) {
					// @ts-ignore
					return o.ItemColumnId == list_item_id;
				});
				$scope.defaultColumns.splice(ind, 1);
			}
		};

		$scope.toggleUserGroups = function (value) {
			$scope.showUserGroups = value;
			$scope.setDefaultColumns();
		};
		$scope.toggleIconColumns = function (value) {
			$scope.showSymbol = value;
			$scope.setDefaultColumns();
		};
		$scope.setDefaultColumns = function () {
			let loopProtection = [];
			_.each(initialDefaultColumns, function (def) {

				let p = true;

				if (def.Name == 'list_symbol' || def.Name == 'list_symbol_fill') {
					if ($scope.showSymbol == 0) p = false;
				}

				if (def.Name == 'user_group') {
					if ($scope.showUserGroups == 0) p = false;
				}

				if (def.Name == 'list_item') {
					if (langColumns.length > 0) p = false;
				}

				if (p) loopProtection.push(def);
			});

			$scope.defaultColumns = loopProtection;
			if ($scope.multi == 1) setupLangColumns();
		}
		$scope.setDefaultColumns();

		let dataTypes = [{value: 0, name: 'DATA_NVARCHAR'},
			{value: 1, name: 'DATA_INT'},
			{value: 2, name: 'DATA_FLOAT'},
			{value: 3, name: 'DATA_DATE'},
			{value: 4, name: 'DATA_TEXT'},
			{value: 5, name: 'DATA_PROCESS'},
			{value: 6, name: 'DATA_LIST'}];

		let changedItems = [];
		let changedColumns = [];

		SaveService.subscribeSave($scope, function () {
			if (changedItems.length > 0) {
				saveItems();
			}
			if (changedColumns.length > 0) {
				saveColumns();
			}
		});

		$scope.addListColumn = function () {
			let params = {
				template: 'pages/lists/listsColumnsAdd.html',
				controller: 'ListColumnsAddController',
				locals: {
					Process: StateParameters.process,
					DataTypes: dataTypes,
					ListColumns: $scope.listColumns,
					StateParameters: StateParameters,
					Lists: Lists,
					Processes: Processes,
					ListColumnItems: $scope.listColumnItems
				}
			};
			MessageService.dialogAdvanced(params);
		};
		let saveItems = function () {
			ListsService.ListProcessesPut(StateParameters.process, changedItems).then(function () {
				changedItems = [];
			});
		};

		let saveColumns = function () {
			ListsService.ItemColumnsPut(StateParameters.process, changedColumns).then(function () {
				changedColumns = [];
			});
		};

		$scope.modifyItems = function (item) {
			if ($scope.multi == 1) item.ListItem.list_item = item.ListItem[defaultLanguage];
			if (typeof Common.getIndexOf(changedItems, item.ListItem.item_id, 'item_id') == 'undefined')
				changedItems.push(item.ListItem);
		};

		$scope.modifyColumns = function (column) {
			if (typeof Common.getIndexOf(changedColumns, column.ListColumn.ItemColumnId, 'ItemColumnId') == 'undefined') {
				changedColumns.push(column.ListColumn);
				SaveService.tick();
			}
		};

		$scope.parentLists = [];
		$scope.parentItems = [];


		$scope.getSublistItems = function () {
			ListsService.getChildProcessItems(StateParameters.process, 1, $scope.selectedParentListItem).then(function (childItems) {
				$scope.addAllowed = true;
				$scope.listItems = childItems;
			});
		};

		function initializePairs() {
			let recItems = [];
			_.each(myHierarchy[$scope.selectedParentListItem], function (id) {
				let child = List.ListItems[Common.getIndexOf(List.ListItems, id, 'item_id')];
				recItems.push(child);
			});
			$scope.listItems = recItems;
		}

		function findAllChildren(identifier, recItems) {
			if (myHierarchy.hasOwnProperty(identifier)) {
				_.each(myHierarchy[identifier], function (id) {
					let child = List.ListItems[Common.getIndexOf(List.ListItems, id, 'item_id')];
					recItems.push(child);
					findAllChildren(id, recItems);
				});
			}
		};

		$scope.getRecursiveSubItems = function (id?) {
			$scope.selectedParentListItem = id;
			whatsMyHierarchy();
			initializePairs();
			if (id == 0 && $scope.isSublist) $scope.changeParentProcess(selectedParentName);
		};

		$scope.deleteItems = function () {
			MessageService.confirm('CONFIRM_DELETE', function () {
				let deleteIds = Common.convertArraytoString($scope.selectedRows, 'id');
				if (deleteIds.length > 0) {
					ListsService.ProcessItemsDelete(StateParameters.process, deleteIds).then(function () {
						angular.forEach($scope.selectedRows, function (value) {
							let index = Common.getIndexOf($scope.listItems, value.id, 'item_id');
							$scope.listItems.splice(index, 1);
							let index2 = Common.getIndexOf($scope.itemsRecursive, value.id, 'item_id');
							$scope.itemsRecursive.splice(index2, 1);
						});
						$scope.selectedRows = [];
					});
				}
			});
		};

		let columnData = {LanguageTranslationRaw: ""};

		$scope.editListColumn = function (listColumn) {
			columnData.LanguageTranslationRaw = listColumn.LanguageTranslationRaw;
			let save = function () {
				$scope.modifyColumns({'ListColumn': listColumn});
			};

			let del = function () {
				deleteListColumn(listColumn);
			};

			let modifications = {
				create: true,
			};

			if (listColumn.Name == 'list_item' || listColumn.Name == 'list_symbol' || listColumn.Name == 'list_symbol_fill' || listColumn.Name == 'in_use' || listColumn.Name == 'user_group') {
				modifications.create = false;
			}

			let content = {
				buttons: [
					{text: "DELETE", type: 'warn', onClick: del, modify: 'create'},
					{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{type: 'translation', label: 'TITLE_TRANSLATION', model: 'LanguageTranslationRaw'}],
				title: 'EDIT_COLUMN'
			};

			MessageService.dialog(content, columnData, null, modifications);
		};


		let deleteListColumn = function (listColumn_) {
			MessageService.confirm('LISTCOLUMN_DELETE_CONFIRM', function () {
				SettingsService.ItemColumnsInContainersDelete(StateParameters.process, listColumn_.ItemColumnId).then(function () {
					let delIndex = Common.getIndexOf($scope.listColumns, listColumn_.ItemColumnId, 'ItemColumnId');
					$scope.listColumns.splice(delIndex, 1);
				});
			});
		};

		if ($scope.RightWrite) {
			ViewService.footbar(StateParameters.ViewId, {
				template: 'pages/lists/listsItemsFooter.html',
				scope: $scope
			});
		}

		let transMap = {};

		function mapTranslations() {
			_.each($scope.listItems, function (listItem) {
				transMap[listItem.item_id] = listItem.list_item;
			});

		}

		disableListItem();

	}

	ListsSublistsController.$inject = ['$q', '$scope', 'Translator', 'MessageService', 'SaveService', 'Common', 'ViewService', 'StateParameters', 'CurrentSubLists', 'ListsService', 'SettingsService', '$rootScope'];

	function ListsSublistsController($q, $scope, Translator, MessageService, SaveService, Common, ViewService, StateParameters, CurrentSubLists, ListsService, SettingsService, $rootScope) {

		$scope.currentSublists = CurrentSubLists;

		$scope.myLanguage = $rootScope.me.locale_id;

		$scope.editList = function (listParams) {
			StateParameters.process = listParams.ProcessName;
			StateParameters.thisList.Process = listParams;
			StateParameters.thisList.Process.isSublist = true;
			ViewService.view('listsEdit', {params: StateParameters}, {split: true});
		};

		$scope.addSubList = function () {
			ListsService.addNewList(StateParameters.process, $scope.myLanguage).then(function (newList) {
				if (newList) {
					$scope.currentSublists.push(newList);
				}
			});
		};
	}

})();