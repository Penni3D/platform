﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(NotificationTagsConfig)
		.service('NotificationTagsService', NotificationTagsService)
		.controller('NotificationTagsController', NotificationTagsController)
		.controller('NotificationTagsEditController', NotificationTagsEditController);

	NotificationTagsConfig.$inject = ['ViewsProvider'];

	function NotificationTagsConfig(ViewsProvider) {
		ViewsProvider.addView('notificationtags', {
			controller: 'NotificationTagsController',
			template: 'pages/notificationtags/notificationtags.html',
			resolve: {
				Lists: function (ProcessService) {
					return ProcessService.getListItems('notification_tags', true);
				}
			}
		});

		ViewsProvider.addView('notificationtags.edit', {
			controller: 'NotificationTagsEditController',
			template: 'pages/notificationtags/notificationtagsEdit.html',
			resolve: {
				Processes: function (ProcessesService) {
					return ProcessesService.get();


				},
				Data: function (NotificationTagsService, StateParameters, ProcessService) {
					let columns = [];
					return NotificationTagsService.getNotificationsData(StateParameters.itemId).then(function (data) {
						angular.forEach(data, function (key) {
							if (angular.isUndefined(columns[key.process_name]) && key.process_name != null) {
								columns[key.process_name] = {};

								ProcessService.getColumns(key.process_name).then(function (columns) {
									columns[key.process_name] = columns;

								});

							}
						});

						return {
							data: data,
							columns: columns
						};
					});
				},

				AllInstanceColumns: function (ProcessesService, Common, $rootScope) {
					let result = [];
					result = ProcessesService.getColumns();
					return result;
				}
			}
		});

		ViewsProvider.addPage('notificationtags', "PAGE_NOTIFICATION_TAGS", ['read', 'write']);
	}

	NotificationTagsService.$inject = ['ApiService'];

	function NotificationTagsService(ApiService) {
		let self = this;
		let NotificationsDataQuery = ApiService.v1api('settings/NotificationsData');

		self.getNotificationsData = function (itemId) {
			return NotificationsDataQuery.get(itemId);
		};

		self.addNotificationsData = function (itemId) {
			return NotificationsDataQuery.new(itemId, {});
		};

		self.saveNotificationsData = function (itemId, data) {
			return NotificationsDataQuery.save(itemId, data);
		};

		self.deleteNotificationsData = function (itemIds) {
			return NotificationsDataQuery.delete(itemIds);
		};
	}

	NotificationTagsEditController.$inject = ['$scope', 'SaveService', 'NotificationTagsService', 'StateParameters', 'Data', 'Processes', 'Common', 'ProcessService', 'AllInstanceColumns', '$rootScope'];

	function NotificationTagsEditController($scope, SaveService, NotificationTagsService, StateParameters, Data, Processes, Common, ProcessService, AllInstanceColumns, $rootScope) {

		let changedItems = {};
		let newValuesArray = [];
		let userLanguage = $rootScope.clientInformation.locale_id;
		let instanceColumns = [];

		// Adding the datatypes and maintenance labels to columns.
		instanceColumns = _.each(AllInstanceColumns, function (value) {
			if (value.Validation.Label != "" && value.Validation.Label != null && JSON.parse(value.Validation.Label)[userLanguage] != null) {
				value.LanguageTranslation[userLanguage] = value.LanguageTranslation[userLanguage] + ' - ' + Common.getDataTypeTranslation(value.DataType) + ' (' + JSON.parse(value.Validation.Label)[userLanguage] + ')';
			} else {
				value.LanguageTranslation[userLanguage] = value.LanguageTranslation[userLanguage] + ' - ' + Common.getDataTypeTranslation(value.DataType);
			}
		})
		$scope.columnsSortedByProcess = _.groupBy(instanceColumns, "Process");


		$scope.rows = Data.data;
		$scope.edit = true;
		$scope.selectedRows = [];
		$scope.processes = Processes;


		let deleteNotificationTags = function () {
			let deleteIds = Common.convertArraytoString($scope.selectedRows, 'item_id');
			NotificationTagsService.deleteNotificationsData(deleteIds);
			angular.forEach($scope.selectedRows, function (row) {
				let index = Common.getIndexOf($scope.rows, row.item_id, 'item_id');
				$scope.rows.splice(index, 1);
			});
			$scope.updateProcessesArray();
			$scope.selectedRows = [];
		};

		$scope.header = {
			title: 'NotificationTags Edit - ' + StateParameters.name,
			menu: [
				{
					name: 'DELETE',
					onClick: deleteNotificationTags
				}]
		};

		$scope.edit = StateParameters.mode == 'edit';

		SaveService.subscribeSave($scope, function () {
			if (changedItems.length) {
				saveNotificationTags();
			}
		});

		let saveNotificationTags = function () {
			let dataArray = [];
			angular.forEach(changedItems, function (key) {
				let index = Common.getIndexOf($scope.rows, key.item_id, 'item_id');
				dataArray.push($scope.rows[index]);
			});
			NotificationTagsService.saveNotificationsData(StateParameters.itemId, dataArray);
			changedItems = [];
			newValuesArray = [];
		};

		$scope.addNotificationTags = function () {
			NotificationTagsService.addNotificationsData(StateParameters.itemId).then(function (data) {
				$scope.rows.push(data);
			});
		};

		$scope.updateProcessesArray = function () {
			let processesArray = angular.copy(Processes);

			angular.forEach($scope.rows, function (key) {
				let index = Common.getIndexOf(processesArray, key.process_name, 'ProcessName');
				if (index == 0 || index != false) {
					processesArray.splice(index, 1);
				}
			});

			$scope.processes = processesArray;
		};

		$scope.changeValue = function (params) {
			if (params.tag_type == 5 || params.tag_type == 6) params.process_name = 'user';

			newValuesArray.push({item_id: params.item_id});
			let noDuplicateObjects = {};

			let l = newValuesArray.length;

			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.item_id] = item;
			}

			let nonDuplicateArray = [];

			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};

	}

	NotificationTagsController.$inject = ['$scope', 'ViewService', 'StateParameters', 'Lists', 'ListsService', 'MessageService', 'SaveService', 'Common', '$filter', 'NotificationTagsService'];

	function NotificationTagsController($scope, ViewService, StateParameters, Lists, ListsService, MessageService, SaveService, Common, $filter, NotificationTagsService) {
		$scope.edit = true;
		let data = {};
		let changedItems = {};
		let newValuesArray = [];

		$scope.header = {
			title: 'NOTIFICATION_TAGS'
		};
		$scope.rows = $filter('orderBy')(Lists, 'list_item');

		SaveService.subscribeSave($scope, function () {
			if (changedItems.length) {
				saveNotificationTags();
				changedItems = [];
			}
		});

		let saveNotificationTags = function () {
			let itemsArray = [];
			angular.forEach(changedItems, function (value) {
				let index = Common.getIndexOf($scope.rows, value.id, 'item_id');
				itemsArray.push($scope.rows[index]);
			});

			ListsService.ListProcessesPut('notification_tags', itemsArray);
		};

		$scope.addNewTag = function () {
			data = {Translation: null};
			let save = function () {
				addNewTagSave(data);
			};

			let content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{type: 'string', label: 'PROCESS_NAME', model: 'Translation'}],
				title: "Edit title"
			};

			MessageService.dialog(content, data);
		};

		$scope.deleteTag = function (id, index) {
			if (!id) return;
			MessageService.confirm("TAG_DELETE", function () {
				NotificationTagsService.getNotificationsData(id).then(function (rows) {
					let d = [];
					for (let r of rows) {
						d.push(r.item_id);
					}
					let complete = function () {
						ListsService.ProcessItemsDelete('notification_tags', id).then(function () {
							$scope.rows.splice(index, 1);
						});
					}
					if (d.length > 0) {
						NotificationTagsService.deleteNotificationsData(Common.convertArraytoString(d)).then(function () {
							complete();
						});
					} else {
						complete();
					}
				});
			});
		}

		let addNewTagSave = function () {
			let p = {
				process: 'notification_tags'
			};

			ListsService.ListProcessesSave('notification_tags', p).then(function (item) {
				let itemsArray = [{
					'item_id': item.item_id,
					'process': 'notification_tags',
					'list_item': data.Translation
				}];
				ListsService.ListProcessesPut('notification_tags', itemsArray).then(function (item) {
					item[0].list_item = data['Translation'];
					$scope.rows.push(item[0]);
				});
			});
		};

		$scope.editRow = function (id, name) {
			StateParameters.name = name;
			StateParameters.itemId = id;
			ViewService.view('notificationtags.edit', {params: {itemId: id, name: name}});
		};

		$scope.changeValue = function (params) {
			newValuesArray.push({id: params.id});
			let noDuplicateObjects = {};

			let l = newValuesArray.length;

			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.id] = item;
			}

			let nonDuplicateArray = [];

			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};
	}
})();