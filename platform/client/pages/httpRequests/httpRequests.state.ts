(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(HttpRequestsConfig)
		.service('HttpRequestService', HttpRequestService)
		.controller('HttpRequestsController', HttpRequestsController);

	HttpRequestsConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider', 'GlobalsProvider'];
	function HttpRequestsConfig(ViewsProvider, ViewConfiguratorProvider, GlobalsProvider) {
		GlobalsProvider.addGlobal('httpRequest', []);
		ViewConfiguratorProvider.addConfiguration('httpRequest', {
			onSetup: {
				Portfolios: function(PortfolioService) {
					return PortfolioService.getPortfolios('notification');
				}
			},
			tabs: {
				default: {
					httpRequestsPortfolioId: function (resolves) {
						return {
							type: 'select',
							options: resolves.Portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation',
							label: 'HTTP_REQUESTS_PORTFOLIO'
						};
					}
				}
			}
		});

		ViewsProvider.addView('httpRequests', {
			controller: 'HttpRequestsController',
			template: 'pages/httpRequests/httpRequests.html',
			resolve: {
				Portfolios: function(PortfolioService) {
					return PortfolioService.getPortfolios('notification');
				},
				HttpRequests: function (PortfolioService, ViewConfigurator) {
					let portfolioId = ViewConfigurator.getGlobal('httpRequest').Params.httpRequestsPortfolioId;
					return PortfolioService
						.getPortfolioData('notification', portfolioId, {local: true}, {limit: 100}).then(function(stuff){
							return stuff.rowdata;
						});
				}
			}
		});

		ViewsProvider.addPage('httpRequests', 'HTTP_REQUESTS_PAGE', ['read']);
	}

	HttpRequestsController.$inject = ['$scope', 'HttpRequests', 'ViewService', 'StateParameters', 'HttpRequestService', 'NotificationService', 'ProcessService',  'MessageService', 'Translator'];
	function HttpRequestsController($scope, HttpRequests, ViewService, StateParameters, HttpRequestService, NotificationService, ProcessService, MessageService, Translator) {
		$scope.requests = HttpRequests.sort(function(a,b) {
			return a.itemId == b.itemId;
		});
		$scope.requestTypes = [{name: "GET", value: 0},
			{name: "POST", value: 1},
			{name: "PUT", value: 2},
			{name: "DELETE", value: 3},
			{name: "PATCH", value: 4}
		];

		$scope.compareOptions = [
			{ value: 0, name: Translator.translate('LIST_COMPARE0') },
			{ value: 1, name: Translator.translate('LIST_COMPARE1') },
			{ value: 2, name: Translator.translate('LIST_COMPARE2') }];

		$scope.selectedRows = [];
		$scope.header = {
			title: Translator.translate('PAGE_HTTPREQUESTS')
		};

		ViewService.footbar(StateParameters.ViewId, {
			template: 'pages/httpRequests/httpRequests.footer.html',
			scope: $scope
		});

		$scope.deleteRequestWarn = function () {
			MessageService.confirm("DELETE_CONFIRMATION", deleteRequests, 'warn');
		};


		let deleteRequests = function(){
			ViewService.lock(StateParameters.ViewId);
			HttpRequestService.deleteRequests($scope.selectedRows).then(function(){
				ViewService.unlock(StateParameters.ViewId);
				_.each($scope.selectedRows, function(rowId){
					$scope.requests.splice(
						_.findIndex($scope.requests, function(o) {
							// @ts-ignore
							return o.item_id == rowId; }), 1
					)
				});
				$scope.selectedRows = [];
			});
		};

		$scope.createHttpRequest = function(){
			let data = {type: 3};
				ProcessService.newItem('notification', {Data: data}).then(function (id) {
					 $scope.requests.push(ProcessService.getCachedItemData('notification', id).Data);
				});

		};

		$scope.modifyRequest = function (params) {
			ProcessService.setDataChanged('notification', params.item_id);
		};
	}

	HttpRequestService.$inject = ['ApiService'];
	function HttpRequestService(ApiService) {
		let self = this;
		let notificationsData = ApiService.v1api('settings/NotificationsData');

		self.deleteRequests = function (itemIds) {
			return notificationsData.new(["httpRequests"], itemIds);
		};

	}

})();