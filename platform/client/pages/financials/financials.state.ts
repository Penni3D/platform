(function (){
	'use strict';
	angular
		.module('core.pages')
		.config(FinancialsPageConfig)
		.controller('FinancialsPageController', FinancialsPageController);

	FinancialsPageConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
	function FinancialsPageConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('FinancialsPage', {
			controller: 'FinancialsPageController',
			template: 'pages/financials/financials.html',
			resolve: {
				FilterColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.process && StateParameters.portfolioId)
						return PortfolioService.getPortfolioColumns(StateParameters.process, StateParameters.portfolioId);
					return null;
				},
				SavedSettings: function (AllocationService, StateParameters) {
					return AllocationService.getSavedSettings(StateParameters.portfolioId).then(function (o) {
						return o;
					});
				},
				ActiveSettings: function (AllocationService, StateParameters) {
					return AllocationService.getActiveSettings(StateParameters.portfolioId);
				}
			}
		});

		ViewConfiguratorProvider.addConfiguration('FinancialsPage', {
			global: true,
			onSetup: {
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				},
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				}
			},
			tabs: {
				default: {
					process: function (resolves) {
						return {
							type: 'select',
							label: 'FINANCIALS_PAGE_PROCESS',
							options: resolves.Processes,
							optionValue: 'ProcessName',
							optionName: 'LanguageTranslation'
						};
					},
					portfolioId: function () {
						return {
							label: 'FINANCIALS_PAGE_PORTFOLIO',
							type: 'select',
							options: [],
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					}
				}
			},
			onChange: function (p, params, resolves, options) {
				if (p == 'process' && params.process && params.process.length > 0) {
					let override = [];
					if (resolves.Portfolios) {
						_.each(resolves.Portfolios, function (d) {
							if (d.Process == params.process) {
								override.push(d);
							}
						});
					}
					options.default.portfolioId.options = override;
				}
			}
		});

		ViewsProvider.addPage('FinancialsPage', 'FINANCIALS_PAGE', ['read']);
	}

	FinancialsPageController.$inject = [
		'$rootScope',
		'$scope',
		'StateParameters',
		'ViewService',
		'MessageService',
		'FilterColumns',
		'PortfolioService',
		'SelectorService',
		'ProcessService',
		'SidenavService',
		'Translator',
		'SavedSettings',
		'AllocationService',
		'ActiveSettings',
		'FinancialsService'
	];
	function FinancialsPageController(
		$rootScope,
		$scope,
		StateParameters,
		ViewService,
		MessageService,
		FilterColumns,
		PortfolioService,
		SelectorService,
		ProcessService,
		SidenavService,
		Translator,
		SavedSettings,
		AllocationService,
		ActiveSettings,
		FinancialsService
	) {
		$scope.pros = [];

		$scope.filterColumns = FilterColumns;

		let selector = SelectorService.getProcessSelector(StateParameters.process);

		$scope.getPorcesses = function (settings) {
			$scope.pros = [];

			let options = {
				offset: 0,
				limit: 0
			};

			PortfolioService.getPortfolioData(StateParameters.process, StateParameters.portfolioId, options, settings).then(function (data) {
				angular.forEach(data.rowdata, function (row) {
					let title = '';
					angular.forEach(selector.Primary, function (primary) {
						if (title != '') title += ' - ';
						title += row[primary.Name];
					});

					ProcessService.getStates(StateParameters.process, row.item_id).then(function (states) {
						let processGroupId = GetGroupId(states);

						if (angular.isDefined(states.financials) && states.financials.groupStates[processGroupId] !== 'undefined'
							&& (states.financials.groupStates[processGroupId].s.indexOf("financials_read_planned") > -1
							|| states.financials.groupStates[processGroupId].s.indexOf("financials_read_estimated") > -1
							|| states.financials.groupStates[processGroupId].s.indexOf("financials_read_committed") > -1
							|| states.financials.groupStates[processGroupId].s.indexOf("financials_read_actual") > -1)) {

							$scope.pros.push({
								name: title,
								processItemId: row.item_id,
								processGroupId: processGroupId,
								states: states,
								selected_rows: [],
								loading: true
							});
						}
					});
				});
			});
		};

		function GetGroupId(states) {
			for (const [key, value] of Object.entries(states.meta.groupStates)) {
				if (value.s.indexOf("write") > -1) return key;
			}
			return 0;
		}

		$scope.archiveDate = null;
		$scope.process = StateParameters.process;
		$scope.scope = 0;
		$scope.move = 0;
		$scope.prev_tooltip = '';
		$scope.next_tooltip = '';

		$scope.showFilters = false;
		$scope.portfolioId = StateParameters.portfolioId;
		$scope.navigationMenuId = StateParameters.NavigationMenuId;

		$scope.header = {
			title: 'FINANCIALS_PAGE_TITLE',
			icons: []
		};

		$scope.header.icons.push({
			icon: 'filter_list', onClick: function () {
				$scope.showFilters = !$scope.showFilters;
			}
		});

		$scope.Init = function (scope, move, prev_tooltip, next_tooltip) {
			$scope.scope = scope;
			$scope.move = move;
			$scope.prev_tooltip = prev_tooltip;
			$scope.next_tooltip = next_tooltip;
		};

		$scope.ShowFooter = function () {
			ViewService.footbar(StateParameters.ViewId,
				{
					template: 'pages/financials/financialsFooter.html'
				});
		};

		$scope.Move = function (move) {
			$rootScope.$broadcast('financials-move', { move: move });
		};

		$scope.DeleteRows = function () {
			MessageService.confirm("FINANCIALS_DELETE_CONFIRM", function () {
				let deletes = {};
				angular.forEach($scope.pros, function (pro) {
					if (pro.selected_rows.length > 0) {
						deletes[pro.processItemId] = {
							"processGroupId": pro.processGroupId,
							"selectedRows": pro.selected_rows
						};
						pro.loading = true;
					}
				});

				FinancialsService.deleteMultipleRows(deletes).then(function (last_modifieds) {
					$rootScope.$broadcast('financials-rowsDeleted', { last_modifieds: last_modifieds });
				});
			});
		};

		$scope.SelectedRows = function () {
			let result = false;
			angular.forEach($scope.pros, function (pro) {
				if (pro.selected_rows.length > 0) result = true;
			});
			return result;
		};

		$scope.Loading = function () {
			let result = false;
			angular.forEach($scope.pros, function (pro) {
				if (pro.loading) {
					result = true;
					return;
				}
			});
			return result;
		};

		let currentFilters = PortfolioService.getActiveSettings($scope.portfolioId).filters

		$scope.filter = function () {
			ActiveSettings.filters = PortfolioService.getActiveSettings($scope.portfolioId);

			$scope.getPorcesses(PortfolioService.getActiveSettings($scope.portfolioId));
		};

		$scope.filter();

		$scope.saveFilters = function () {
			ViewService.view('configure.allocation', {
				params: {
					isNew: true,
					view: $scope.portfolioId,
					default_right: true
				},
				locals: {
					CurrentSettings: {
						Name: Translator.translate('PORTFOLIO_NEW'),
						Value: { 'filters': currentFilters }
					},
					ReloadNavigation: function () {
						reloadNavigation();
					}
				}
			});
		};

		function reloadNavigation() {
			AllocationService.getSavedSettings($scope.portfolioId).then(function (newSettings) {
				setNavigation(newSettings);
			});
		}

		let activeTab = {};

		function setNavigation(settings) {
			let sidenav = [];
			let savedTabs = [];

			angular.forEach(settings, function (s) {
				s.Name = angular.fromJson(s.Name);
				let t = {
					hideSplit: true,
					name: Translator.translation(s.Name),
					onClick: function () {
						activeTab.$$active = false;
						activeTab = t;
						activeTab.$$active = true;

						if (s.Value.filters) {
							angular.forEach(s.Value.filters, function (value, key) {
								if (!ActiveSettings.hasOwnProperty('filters')) {
									ActiveSettings.filters = { 'filters': {} }
								}
								ActiveSettings.filters.filters[key] = angular.copy(value);
							});
						}
						else {
							PortfolioService.clearActiveSettings($scope.portfolioId);
						}

						$scope.filter();
					},
					menu: [{
						icon: 'settings',
						name: 'CONFIGURE',
						onClick: function () {
							ViewService.view('configure.allocation', {
								params: {
									portfolioId: $scope.portfolioId,
									isNew: false,
									view: $scope.portfolioId
								},
								locals: {
									CurrentSettings: s,
									ReloadNavigation: reloadNavigation
								}
							});
						}
					}]
				};

				savedTabs.push(t);
			});

			if (savedTabs.length)
				sidenav.push({ name: 'PORTFOLIO_SAVED', tabs: savedTabs, showGroup: true });

			SidenavService.navigation(sidenav, {}, true);
		}

		setNavigation(SavedSettings);
	}
})();