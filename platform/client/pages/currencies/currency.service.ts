(function () {
	'use strict';

	angular
		.module('core.pages')
		.service('CurrencyService', CurrencyService);

	CurrencyService.$inject = ['$rootScope', 'ApiService', '$q'];
	function CurrencyService($rootScope, ApiService, $q) {
		let self = this;

		let CurrencyRest = ApiService.v1api('core/CurrenciesRest');
		let exchangeRates = [];

		self.getCurrencyValues = function () {
			if(exchangeRates.length == 0){
				return CurrencyRest.get().then(function(c){
					exchangeRates = c;
					return exchangeRates;
				});
			}
			return exchangeRates;
		}

		self.getCurrencies = function() {
			return CurrencyRest.get("currencies");
		}

		self.insertNewCurrency = function(currency){
			return CurrencyRest.new([""], currency);
		}

		self.saveCurrencies = function(listOfCurrencies){
			return CurrencyRest.save([""], listOfCurrencies);
		}

		self.getInstanceDefaults = function(){
			return CurrencyRest.get("instance_defaults");
		}

		self.saveInstanceDefaultCurrency = function(c){
			return CurrencyRest.save(["default"], c);
		}

		self.deleteInstanceCurrency = function(c){
			return CurrencyRest.delete([c.CurrencyId, c.Abbreviation]);
		}
	}
})();