(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(CurrenciesConfig)
		.controller('CurrenciesController', CurrenciesController);

	CurrenciesConfig.$inject = ['ViewsProvider'];

	function CurrenciesConfig(ViewsProvider) {
		ViewsProvider.addView('currencies', {
			controller: 'CurrenciesController',
			template: 'pages/currencies/currencies.html',
			resolve: {
				CurrencyValues: function(CurrencyService){
					return CurrencyService.getCurrencyValues();
				},
				Currencies: function(CurrencyService){
					return CurrencyService.getCurrencies();
				},
				ListItems: function(ProcessService){
					return ProcessService.getListItems('list_system_currencies');
				},
				InstanceDefaults: function(CurrencyService){
					return CurrencyService.getInstanceDefaults();
				}
			}
		});

		ViewsProvider.addPage('currencies', "PAGE_CURRENCIES", ['read','write']);
	}

	CurrenciesController.$inject = ['$scope','StateParameters', 'ApiService', '$rootScope', 'CurrencyValues', 'Currencies', 'MessageService', 'CurrencyService', 'SaveService', 'ViewService', 'ListItems', 'InstanceDefaults', 'Translator'];

	function CurrenciesController($scope, StateParameters, ApiService, $rootScope, CurrencyValues, Currencies, MessageService, CurrencyService, SaveService, ViewService, ListItems, InstanceDefaults, Translator) {

		$scope.header = {
			title: 'CURRENCY_MANAGEMENT'
		}
		ViewService.footbar(StateParameters.ViewId, {template: 'pages/currencies/currencies.footer.html'});

		$scope.currencies = Currencies;
		$scope.listItems = ListItems;
		$scope.instanceDefaults = InstanceDefaults;

		$scope.addCurrency = function(){
			let newCurrency = {Name: "", Abbreviation: "", Translations: {}};
			let saveCurrency = function(){
				CurrencyService.insertNewCurrency(newCurrency).then(function(result){
					$scope.currencies.push(result);
					newCurrency = {Name: "", Abbreviation: "", Translations: {}} ;
				});
			}
			let content = {
				buttons: [{ text: 'MSG_CANCEL' }, { type: 'primary', onClick: saveCurrency, text: 'CREATE' }],
				inputs: [{ type: 'translation', label: 'CURRENCY_NAME', model: 'Translations' }, { type: 'string', label: 'CURRENCY_ABBR', model: 'Abbreviation' }],
				title: 'ADD_NEW_CURRENCY'
			};
			MessageService.dialog(content, newCurrency);
		}

		$scope.saveCurrency = function(currencyId, index){
			if(_.findIndex(changedItems, function(o) { return o.CurrencyId == currencyId; }) == -1){
				changedItems.push($scope.currencies[index]);
				SaveService.tick();
			}
		}

		$scope.changeDefaultCurrency = function(){
			let c = angular.copy($scope.instanceDefaults);
			CurrencyService.saveInstanceDefaultCurrency(c);
		}

		let changedItems = [];

		SaveService.subscribeSave($scope, function(){
			if(changedItems.length){
				CurrencyService.saveCurrencies(changedItems).then(function(s){
					_.each(s, function(s2){
						let i = _.findIndex($scope.currencies, function(o) { // @ts-ignore
							return o.CurrencyId == s2.CurrencyId; });
						$scope.currencies[i].OldAbbreviation = s2.OldAbbreviation
					})
					changedItems = [];
				});
			}
		});

		$scope.deleteCurrency = function(currency){
			MessageService.confirm(Translator.translate('U_SURE_DELETE_CURRENCY'),
				function () {
				let index = _.findIndex($scope.currencies, function(o) { return o.CurrencyId == currency.CurrencyId; });
					CurrencyService.deleteInstanceCurrency(currency).then(function(){
						$scope.currencies.splice(index, 1);
					});
				});
		}
	}
})();