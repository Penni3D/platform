﻿(function () {
    'use strict';

    angular
        .module('core.pages')
        .config(O365SettingsServiceConfig)
        .controller('O365SettingsController', O365SettingsController)
        .service('O365SettingsService', O365SettingsService);


    O365SettingsServiceConfig.$inject = ['ViewsProvider'];

    function O365SettingsServiceConfig(ViewsProvider) {
        ViewsProvider.addView('o365Settings', {
            controller: 'O365SettingsController',
            template: 'pages/o365Settings/o365Settings.html',
            resolve: {
                Configurations: function (O365SettingsService) {
                    return O365SettingsService.getO365SettingsConfiguration();
                }
            }
        });

        ViewsProvider.addPage('o365Settings', 'o365Settings', ['read', 'write']);
    }

    O365SettingsController.$inject = ['$scope', 'Translator', 'ViewService', 'StateParameters', '$rootScope', 'Configurations', 'SaveService', 'O365SettingsService'];

    function O365SettingsController($scope, Translator, ViewService, StateParameters, $rootScope, Configurations, SaveService, O365SettingsService) {
        $scope.configurations = Configurations;
        $scope.enabledOptions = [{name: 'True', value: true}, {name: 'False', value: false}];

        $scope.toggleConfigurations = function (value) {
            $scope.configurations.Enabled = value;
            O365SettingsService.saveO365Settings($scope.configurations);
        }

        $scope.toggleConfigurations2 = function (value) {
            $scope.emailVisible = value;
        }

        $scope.header = {title: Translator.translate("O365_TITLE")};
        ViewService.footbar(StateParameters.ViewId, { template: 'pages/o365Settings/o365SettingsFootbar.html' });

        $scope.removeRows = function (key) {
                O365SettingsService.deleteO365Settings(key).then(function () {
                O365SettingsService.getO365SettingsConfiguration().then(function (configuration) {
                    $scope.configurations = configuration;
                });
            });
        }

         // $scope.saveValue = function() {
     
         //  	 SaveService.tick();
         //  }
         
        $scope.changeValue = function(){
            O365SettingsService.saveO365Settings($scope.configurations);
        }

        // SaveService.subscribeSave($scope, function () {
        //     O365SettingsService.saveO365Settings($scope.configurations);
        // });
        
        $scope.getEmailText = function () {
            return Translator.translate("O365_EMAIL_TEXT").replace("xxx", $scope.configurations.LoginUrl);
        }
    }

    O365SettingsService.$inject = ['ApiService'];

    function O365SettingsService(ApiService) {
        let self = this;
        let o365Rest = ApiService.v1api('core/O365SettingsRest');

        self.getO365SettingsConfiguration = function () {
            return o365Rest.get().then(function (configurations) {
                if (configurations.LoginUrl == '' || configurations.LoginUrl == null)
                    configurations.LoginUrl = window.location.href;
                return configurations;
            });
        };
        self.saveO365Settings = function (changedItem) {
            return o365Rest.save([""], changedItem);
        }

        self.deleteO365Settings = function (removeRows) {
            return o365Rest.delete(removeRows);
        }
    }

})();