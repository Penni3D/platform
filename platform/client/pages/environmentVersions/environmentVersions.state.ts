(function (){
	'use strict';
	angular
		.module('core.pages')
		.config(EnvironmentVersionsConfig)
		.controller('EnvironmentVersionsController', EnvironmentVersionsController)
		.service('EnvironmentVersionsService', EnvironmentVersionsService);

	EnvironmentVersionsConfig.$inject = ['ViewsProvider', 'GlobalsProvider', 'ViewConfiguratorProvider'];
	function EnvironmentVersionsConfig(ViewsProvider, GlobalsProvider, ViewConfiguratorProvider) {

		ViewsProvider.addView('environmentVersions', {
			controller: 'EnvironmentVersionsController',
			template: 'pages/environmentVersions/environmentVersions.html',
			resolve: {
				EnvironmentVersions: function (EnvironmentVersionsService, StateParameters) {
					return EnvironmentVersionsService.getLocalDirectories(StateParameters.EnvironmentList);
				}
			}
		});
		ViewsProvider.addPage('environmentVersions', 'LIST_OF_APPLICATIONS', ['read', 'write']);

		ViewConfiguratorProvider.addConfiguration('environmentVersions', {
			onSetup: {
				Lists: function (ListsService, $filter) {
					return ListsService.getListProcesses().then(function (processes) {
						var lists = [];
						angular.forEach(processes, function (key) {
							lists.push(key);
						});
						return $filter('orderBy')(lists);
					});
				}
			},
			tabs: {
				interface: {
					EnvironmentList: function (resolves) {
						return {
							type: 'select',
							options: resolves.Lists,
							label: "LIST_FOR_APPLICATIONS",
							optionValue: 'ProcessName',
							optionName: 'LanguageTranslation'
						};
					}
				}
			}
		});
	}

	EnvironmentVersionsController.$inject = ['$scope', 'EnvironmentVersionsService', 'EnvironmentVersions', 'CalendarService'];
	function EnvironmentVersionsController($scope, EnvironmentVersionsService, EnvironmentVersions, CalendarService) {
		$scope.allServers = EnvironmentVersions;
		angular.forEach($scope.allServers, function(server){
			_.each(server, function(application){
				application.ReleaseDate = CalendarService.formatDate(application.ReleaseDate);
			})
		});
		$scope.header = {
			title: 'LIST_OF_APPLICATIONS'
		};
	}

	EnvironmentVersionsService.$inject = ['ApiService'];
	function EnvironmentVersionsService(ApiService) {
		var self = this;
		var LocalVersions = ApiService.v1api('diagnostics/localVersions');

		self.getLocalDirectories = function (listname) {
			return LocalVersions.get([listname]).then(function(t) {
				return t
			});
		};
	}
})();