﻿(function () {
	'use strict';

	angular
		.module('core')
		.controller('LinkDialogController', LinkDialogController);

	LinkDialogController.$inject = ['$scope', 'LocalStorageService', '$http', 'MessageService', '$location', 'Translator'];
	function LinkDialogController($scope, LocalStorageService, $http, MessageService, $location, Translator) {
		var linkRetrieved = false;

		$scope.btnAction = Translator.translate('LAYOUT_LINK_CREATE');

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.expiration = 3;
		$scope.selectOptions = [
			{
				name: 'LAYOUT_LINK_IN_DAY',
				value: 1
			},
			{
				name: 'LAYOUT_LINK_IN_THREE_DAYS',
				value: 3
			},
			{
				name: 'LAYOUT_LINK_IN_WEEK',
				value: 7
			},
			{
				name: 'LAYOUT_LINK_IN_TWO_WEEKS',
				value: 14
			},
			{
				name: 'LAYOUT_LINK_IN_MONTH',
				value: 30
			},
			{
				name: 'LAYOUT_LINK_NEVER',
				value: 9125
			}
		];

		$scope.createLink = function () {
			if (typeof $scope.retrievingLink === 'undefined') {
				$scope.retrievingLink = true;
				var p = { Params: LocalStorageService.get('restoreView'), Expiration: $scope.expiration };

				$http.post('v1/core/Links', p).then(function (response) {
					var url = $location.absUrl();
					var i = url.length;

					while (i--) {
						if (url[i] == '#') {
							url = url.slice(0, i);
							break;
						}
					}

					linkRetrieved = true;
					$scope.retrievingLink = false;
					$scope.btnAction = Translator.translate('LAYOUT_LINK_CLOSE');
					$scope.link = url + '?view=' + response.data;
				});
			} else if (linkRetrieved) {
				MessageService.closeDialog();
			}
		};
	}
})();
