﻿(function () {
	'use strict';

	angular
		.module('core')
		.controller('LayoutController', LayoutController)
		.filter('UserFilter', UserFilter);

	LayoutController.$inject = [
		'$scope',
		'Translator',
		'NavNavigation',
		'ViewService',
		'$rootScope',
		'MessageService',
		'Configuration',
		'Common',
		'ToolbarService',
		'SidenavService',
		'SaveService',
		'$interval',
		'$window',
		'$timeout',
		'ProcessService',
		'SelectorService',
		'FavouritesService',
		'MyTasks',
		'StickyNavigation',
		'ViewConfigurator',
		'Profile',
		'Impersonate',
		'VersionRight',
		'UserService',
		'Delegates',
		'CoreService',
		'CalendarService',
		'UnreadNotifications',
		'NotificationService',
		'LocalStorageService',
		'PortfolioService',
		'ErrorService'
	];

	function LayoutController(
		$scope,
		Translator,
		NavNavigation,
		ViewService,
		$rootScope,
		MessageService,
		Configuration,
		Common,
		ToolbarService,
		SidenavService,
		SaveService,
		$interval,
		$window,
		$timeout,
		ProcessService,
		SelectorService,
		FavouritesService,
		MyTasks,
		StickyNavigation,
		ViewConfigurator,
		Profile,
		Impersonate,
		VersionRight,
		UserService,
		Delegates,
		CoreService,
		CalendarService,
		UnreadNotifications,
		NotificationService,
		LocalStorageService,
		PortfolioService,
		ErrorService
	) {

		$scope.openSearch = function () {
			$scope.openS = !$scope.openS;
			if ($scope.openS) {
				setTimeout(function () {
					$("#navBarSearchBox input").focus();
				}, 75);
			}
		};

		$scope.walkThePath = function(pathPart, index){
			if(index == 0){
				ViewService.view('frontpage', {});
				return
			}
			let storedPortfolio = ToolbarService.getPathPortfolio();
			if(storedPortfolio.ProcessPortfolioId != 0){
				ViewService.view('portfolio', {
						params: {
							portfolioId: storedPortfolio.ProcessPortfolioId,
							process: storedPortfolio.Process,
						}
					},
					{
						split: false
					}
				);
			} else {
				let thisProcess = LocalStorageService.get("currentActiveProcess");
				PortfolioService.getPortfolios().then(function(ps){
					_.each(ps, function(portfolio){
						_.each(portfolio.LanguageTranslation, function(translation){
							if(translation == pathPart && portfolio.Process == thisProcess){
								ViewService.view('portfolio', {
										params: {
											portfolioId: portfolio.ProcessPortfolioId,
											process: portfolio.Process,
										}
									},
									{
										split: false
									}
								);
							}
						});
					});
				})
			}
		}

		let userLanguage = $rootScope.clientInformation.locale_id;

		let executeFinalizeFunction = function(name){
			switch(name) {
				case "DoActionForSelectedItemIds":
					angular.forEach(ViewService.getViews().views, function (view, viewId) {
						if(view.name == "portfolio"){
							ViewService.refresh(viewId);
						}
					});
					break;
			}
		}

		NotificationService.subscribeNotificationNeeded($scope, function (e, p) {

			let toast = {
				message: p.success == "true" ? "BG_PROCESS_DONE" : "BG_PROCESS_FAILED",
				color: undefined,
				button: undefined
			};

			if (p.link != "") {
				toast.message = "BG_PROCESS_FILE_DONE";
				toast.button = {
					text: "OPEN",
					onClick: function () {
						document.location.href = p.link;
					}
				}
			}

			if (p.hasOwnProperty('showtoast')) {
				setTimeout(function () {
					if (toast.button) MessageService.toastVeryLong(toast); else MessageService.toastLong(toast);
					if(p.onfinish == "True"){
						executeFinalizeFunction(p.name);
					}
				}, 2000);
			} else {
				if(p.onfinish == "True"){
					executeFinalizeFunction(p.name);
				}
			}
			NotificationService.getUnreadUserNotifications().then(function (newNotifications) {
				$scope.unreadNotifications = newNotifications
			});
		});

		$scope.stickyName = Translator.translation(ViewConfigurator.getGlobal('sticky').LanguageTranslation);
		$scope.applicationName = Configuration.getClientConfigurations('Main application', 'Application name');
		$scope.environmentName = Configuration.getClientConfigurations('Main application', 'Environment banner');
		$scope.showRightSidenav = Common.isFalse(
			Configuration.getClientConfigurations('Main application', 'Disable right sidenav')
		);
		$scope.showNav = Configuration.getClientConfigurations('Main application', 'Disable all navs') != 'true';

		$scope.showConversation = Common.isFalse(
			Configuration.getClientConfigurations('Main application', 'Disable conversation')
		);

		$scope.me = $rootScope.me;
		$scope.delegator = $rootScope.clientInformation.delegator;
		$scope.isAnonymous = Common.isTrue($rootScope.clientInformation.anonymous);
		$scope.showVersion = VersionRight.indexOf('write') > -1;

		$rootScope.environment = Configuration.getServerConfigurations('hostingenvironment').environment.value;

		$scope.version = $rootScope.clientInformation.version;
		$scope.versiondate = CalendarService.formatDate($rootScope.clientInformation.versiondate);
		$scope.myTasks = MyTasks;
		$scope.showLoading = false;

		$rootScope.$on('notifyShowLoading', function (event, p) {
			$scope.showLoading = p.show;
		});

		let $leftSidenavPusher;
		let $rightSidenavPusher;
		let $leftSidenav;
		let $rightSidenav;
		let $searchNav;

		function showLeftSidenav() {
			$leftSidenavPusher.addClass('sidenav-left-pusher');
			$leftSidenav.addClass('sidenav-show');
		}

		function hideLeftSidenav() {
			$leftSidenavPusher.removeClass('sidenav-left-pusher');
			$leftSidenav.removeClass('sidenav-show');
		}

		function showRightSidenav() {
			if ($scope.showRightSidenav) {
				$rightSidenavPusher.addClass('sidenav-right-pusher');
				$rightSidenav.addClass('sidenav-show');
			}
		}


		function showSearchNav() {
			$rightSidenavPusher.addClass('card-opener');
		}

		function hideRightSidenav() {
			$rightSidenavPusher.removeClass('sidenav-right-pusher');
			$rightSidenav.removeClass('sidenav-show');
		}

		setTimeout(function () {
			$leftSidenavPusher = $('#main-container > pusher:first-of-type');
			$rightSidenavPusher = $('#main-container > pusher:last-of-type');
			$leftSidenav = $('#sidenav-left');
			$rightSidenav = $('#sidenav-right');

			if ($window.innerWidth > 1024) {
				showLeftSidenav();
			}
		});

		let saveTime = moment();
		$scope.navStaticNavigation = NavNavigation;

		let newSubHeader = function (previousTabName, subchilds, group, i) {
			if (subchilds.length > 1) {
				angular.forEach(subchilds, function (c) {
					c.visible = false;
					c.LanguageTranslation[userLanguage] = c.LanguageTranslation[userLanguage].replace("[" + previousTabName + "]", "");
					c.childClass = "menu-child";
				});
				let o = {
					LanguageTranslation: {},
					view: "",
					subchilds: subchilds,
					visible: true,
					position: i - subchilds.length - 1,

				}
				o.LanguageTranslation[userLanguage] = previousTabName;
				//Add new group
				newHeaders.push(o);
				return 1;
			}
			return 0;
		};

		let findSimilarity = function (str1: string): string {
			let foundAt = _.lastIndexOf(str1, "]");
			if (foundAt > 0) {
				str1 = str1.substr(1, foundAt - 1);
			}
			return str1;
		};

		let newHeaders = [];

		_.each($scope.navStaticNavigation, function(navLevel){

			let thisTabName;
			let previousTabName = "";
			let subchildren = [];
			let previousTab;
			let i = 1;

			_.each(navLevel.Links, function(link){
				//	if (previousTab) subchilds.push(previousTab);
				if(link.LanguageTranslation[userLanguage] && link.LanguageTranslation[userLanguage].includes("[")){

					subchildren.push(previousTab);
					thisTabName = findSimilarity(link.LanguageTranslation[userLanguage]);

					if (previousTabName != thisTabName) {
						i += newSubHeader(previousTabName, subchildren, navLevel, i);
						subchildren = [];

					}
				}
				previousTabName = thisTabName;
				previousTab = link;
				i += 1;
			})

			if (previousTab) subchildren.push(previousTab);
			newSubHeader(previousTabName, subchildren, navLevel, i);
			//Add new subheaders
			angular.forEach(newHeaders, function (header) {
				navLevel.Links.splice(header.position, 0, header);
			});
			newHeaders = [];

		});

		$scope.navStickyNavigation = StickyNavigation;
		$scope.showEnvironment = true;

		$scope.showFrontpage = function () {
			ViewService.frontpage();
		};

		/*
		 * NAVIGATION BAR
		 *
		 */

		$scope.executeConditionOutcomes = function(){
			ToolbarService.executeConditionOutcomes(ViewService.getActiveItemId(),
				LocalStorageService.get("currentActiveProcess"));
		}
		$scope.hasConfigureFunction = function () {
			return ToolbarService.hasConfigureFunction();
		}
		$scope.executeConfigureFunction = function () {
			ToolbarService.executeConfigureFunction();
		}
		$scope.hasQuickNavigation = function () {
			return ToolbarService.hasQuickNavigation();
		}
		$scope.getQuickNavPosition = function () {
			return ToolbarService.currentQuickNavPosition();
		}
		$scope.getQuickNavTotal = function () {
			return ToolbarService.currentQuickNavTotal();
		}
		$scope.hasDebugFunction = function () {
			return ToolbarService.hasDebugFunction();
		}
		$scope.executeDebugFunction = function () {
			ToolbarService.executeDebugFunction();
		}
		$scope.hasDeleteFunction = function () {
			return ToolbarService.hasDeleteFunction();
		}
		$scope.executeDeleteFunction = function () {
			ToolbarService.executeDeleteFunction();
		}
		$scope.openView = function (link) {
			if (link.subchilds && link.subchilds.length > 0) {
				angular.forEach(link.subchilds, function (childtab) {
					childtab.visible = !childtab.visible;
				});
			} else {

				link.LanguageTranslation ?
					ToolbarService.setFirstSlot(link.LanguageTranslation) : ToolbarService.setFirstSlot('none');

				if ($window.innerWidth <= 1024) {
					hideLeftSidenav();
					MessageService.removeBackdrop();
				}

				SidenavService.staticNavigation(link.InstanceMenuId);
			}
		};

		$scope.logoClick = function () {
			ViewService.view('frontpage', {});
		};

		//Add button press detection to all aria classes
		$(document).on("keyup", ".aria-detect", function (e) {
			if (e.keyCode == 13) {
				$(this).trigger("click");
			}
		});

		$scope.unreadNotifications = UnreadNotifications;
		$scope.showNotifications = function () {

			if (typeof $scope.unreadNotifications == 'undefined') {
				MessageService.toast('UNREAD_PORTFOLIO_MISSING');
				return;
			}

			if ($scope.unreadNotifications.length) {
				ViewService.view('notification.view', {
					locals: {
						UnreadNotifications: $scope.unreadNotifications
					}
				});
			} else {
				MessageService.toast('NO_UNREAD_NOTIFICATIONS');
			}
		};

		$interval(function () {
			if ($scope.saveFailure) {
				$scope.saveTime = Translator.translate('LAYOUT_SAVE_FAILURE');
			} else if (SaveService.saving) {
				$scope.saveTime = Translator.translate('LAYOUT_SAVE_UNSAVED');
			} else {
				let s = moment.duration(moment().diff(saveTime)).asSeconds();

				if (s < 30) {
					$scope.saveTime = Translator.translate('LAYOUT_SAVE_SUCCESS'); //'All changes saved';
				} else if (s < 90) {
					$scope.saveTime = Translator.translate('LAYOUT_SAVE_LAST') + ' ' + Translator.translate('LAYOUT_SAVE_MINUTE_AGO'); //'Last save a minute ago';
				} else if (s < 3600) {
					let m = Math.round(s / 60);
					$scope.saveTime = Translator.translate('LAYOUT_SAVE_LAST') + ' ' + m + ' ' + Translator.translate('LAYOUT_SAVE_MINUTES_AGO');
				} else if (s < 7200) {
					$scope.saveTime = Translator.translate('LAYOUT_SAVE_LAST') + ' ' + Translator.translate('LAYOUT_SAVE_HOUR_AGO'); //'Last save an hour ago';
				} else {
					let h = Math.round(s / 3600);
					$scope.saveTime = Translator.translate('LAYOUT_SAVE_LAST') + ' ' + h + ' ' + Translator.translate('LAYOUT_SAVE_HOURS_AGO'); //'Last save ' + h + ' hours ago';
				}
			}
		}, 1314);

		$scope.logout = function () {
			MessageService.toast('Redirecting...');
			CoreService.logout();
		};

		$scope.pathParts = [];
		ToolbarService.subscribePath($scope, function () {
			let path = ToolbarService.getPath();
			$scope.toolbarPathHL = path.highlight;
			$scope.toolbarPath = path.path;
			$scope.pathParts = ToolbarService.getPathSlots();
		});

		SaveService.subscribeToolbarSaved($scope, function () {
			saveTime = moment();
			$scope.saveFailure = false;
		});

		SaveService.subscribeToolbarSaveFailure($scope, function () {
			$scope.saveFailure = true;
		});

		$scope.disableAnimation = Configuration
			.getClientConfigurations('Main application', 'Disable animations');

		let previousWidth;
		Common.subscribeContentResize($scope, function () {
			let w = $window.innerWidth;
			if (previousWidth != w) {
				if (w <= 1024) {
					hideLeftSidenav();
				}

				if (w < 1440) {
					hideRightSidenav();
				}

				previousWidth = w;
			}
		});

		$scope.closeLeftSidenav = function () {
			hideLeftSidenav();
			MessageService.removeBackdrop();
		};

		let currentIndex = null
		SidenavService.setNavigatePortfolioObj('current_index', null)
		let orderPortfolioRows = function (rows, parentItemId, result) {
			let parents = _.filter(rows, function (row) {
				return row.parent_item_id == parentItemId;
			})
			_.each(parents, function (parent) {
				result.push(parent);
				_.each(rows, function (row2) {

					if (row2.parent_item_id == parent.itemId) {
						result.push(row2);
						orderPortfolioRows(rows, row2.itemId, result)
					}
				})
			});
		}

		let rows = [];
		$scope.switchPortfolioRow = function (way) {
			let currentView = ViewService.getCurrentLocals().StateParameters.$$helpName;
			let currentTabId = ViewService.getCurrentLocals().StateParameters.tabId;

			let portfolioSettings = PortfolioService.getActiveSettings(SidenavService.getNavigatePortfolio("portfolio_id"));
			let movePage = false;
			rows = [];
			orderPortfolioRows(SidenavService.getNavigatePortfolio("rows"), '0', rows);
			SidenavService.setNavigatePortfolioObj('rows', rows);
			let itemIdName = "";

			currentIndex = _.findIndex(rows, function (o) {
				itemIdName = o.hasOwnProperty('actual_item_id') ? 'actual_item_id' : 'item_id';
				return o[itemIdName] == ViewService.getActiveItemId();
			});

			let sumRowModifier = _.filter(rows, function(o) { return o.item_id == "sumRow" }).length == 1 ? 2 : 1;

			if ((currentIndex == rows.length - sumRowModifier && way == 'next') ||
				(currentIndex == 0 && way == 'prev')) movePage = true;

			if (way == 'prev' && currentIndex > 0) {
				currentIndex--;
				SidenavService.setNavigatePortfolioObj('current_index', currentIndex)
			}
			if (way == 'next' && currentIndex < rows.length - 1) {
				currentIndex++;
				SidenavService.setNavigatePortfolioObj('current_index', currentIndex)
			}

			let currentActive = SidenavService.getNavigatePortfolio('rows')[currentIndex];

			let limit = SidenavService.getNavigatePortfolio("limit")
			if (movePage) {
				if(typeof SidenavService.getNavigatePortfolio("p") == 'undefined'){
					let pageNumber = way == 'next' ? portfolioSettings.filters.page + 1 :
						portfolioSettings.filters.page - 1;
					if (pageNumber < 0) pageNumber = 0;
					SidenavService.setNavigatePortfolioObj('p', pageNumber)
				} else {
					if(way == 'next'){
						SidenavService.setNavigatePortfolioObj('p', SidenavService.getNavigatePortfolio("p") + 1)
					} else {
						SidenavService.setNavigatePortfolioObj('p', SidenavService.getNavigatePortfolio("p") - 1)
					}

				}
				PortfolioService
					.getPortfolioData(
						currentActive.process,
						SidenavService.getNavigatePortfolio("portfolio_id"),
						{
							local: true,
							restrictions: SidenavService.getNavigatePortfolio("restrictions") == null ? undefined : SidenavService.getNavigatePortfolio("restrictions"),
							recursive: portfolioSettings.recursive == -1 ? false : true
						},
						{
							limit: limit,
							page: SidenavService.getNavigatePortfolio("p"),
							sort: portfolioSettings.filters.sort.length ? portfolioSettings.filters.sort : [{'column': 'item_id', 'order': 'asc'}]
						},
						0)
					.then(function (portfolioData) {
						portfolioSettings.filters.page = SidenavService.getNavigatePortfolio("p")
						let i = portfolioData.rowdata.length;
						while (i--) {
							let row = portfolioData.rowdata[i];

							if (portfolioSettings.recursive == 1) {
								portfolioData.rowdata[i] = ProcessService.setData(
									{Data: row},
									currentActive.process,
									row.item_id
								).Data;
							} else {
								row.stored_parent_item_id = row.parent_item_id;
								row.parent_item_id = 0;
							}
						}
						rows = [];
						orderPortfolioRows(portfolioData.rowdata, '0', rows)

						let pIndex = _.findIndex(rows, function (o) {
							itemIdName = o.hasOwnProperty('actual_item_id') ? 'actual_item_id' : 'item_id';
							return o[itemIdName] == ViewService.getActiveItemId();
						});

						if (way == 'prev'){
							pIndex = rows.length - 2;
						}
						let itemId = rows[pIndex + 1].actual_item_id ? rows[pIndex + 1].actual_item_id : rows[pIndex + 1].item_id

						SidenavService.setNavigatePortfolioObj('rows', rows)

						currentIndex = null
						SidenavService.setNavigatePortfolioObj('current_index', currentIndex)

						if (itemId && itemId > 0 && itemId != null) {
							let toState = {
								view: currentView,
								ownerIdCol: null,
								params: {
									process: portfolioData.rowdata[0].process,
									itemId: itemId,
									tabId: currentView == "process.tab" ? currentTabId : null
								}
							};
							ViewService.view(toState.view, {params: toState.params});
						}

					});
			} else {
				let itemIdHere = currentActive.actual_item_id ? currentActive.actual_item_id : currentActive.item_id;
				if (itemIdHere && itemIdHere > 0 && itemIdHere != null) {
					let toState = {
						view: currentView,
						ownerIdCol: null,
						params: {
							process: currentActive.process,
							itemId: itemIdHere,
							tabId: currentView == "process.tab" ? currentTabId : null
						}
					};
					ViewService.view(toState.view, {params: toState.params});
				}
			}
		}

		$scope.toggleLeftNav = function () {
			if ($leftSidenavPusher.hasClass('sidenav-left-pusher')) {
				hideLeftSidenav();

				if ($window.innerWidth <= 1024) {
					MessageService.removeBackdrop();
				}
			} else {
				showLeftSidenav();
			}

			if ($window.innerWidth <= 1024) {
				let onClick = function () {
					hideLeftSidenav();
					MessageService.removeBackdrop();
				};

				let onSwipe = {
					left: function () {
						onClick();
					}
				};

				MessageService.backdrop(function () {
					hideLeftSidenav();
					MessageService.removeBackdrop();
				});
			}
		};
		
		$scope.forceCloseRightNav = function() {
			hideRightSidenav();
			MessageService.removeBackdrop();
		}
		
		$scope.toggleRightNav = function () {
			if ($rightSidenavPusher.hasClass('sidenav-right-pusher')) {
				hideRightSidenav();
			} else {
				showRightSidenav();
			}
			if ($window.innerWidth <= 1024) {
				let onClick = function () {
					hideRightSidenav();
					MessageService.removeBackdrop();
				};

				let onSwipe = {
					right: function () {
						onClick();
					}
				};

				MessageService.backdrop(function () {
					hideRightSidenav();
					MessageService.removeBackdrop();
				});
			}
		};

		// $scope.toggleEnvironment = function () {
		// 	$scope.showEnvironment = !$scope.showEnvironment;
		// };

		$scope.showUserMenu = function (event) {

			let o = [];

			if (Profile.indexOf('read') !== -1) {
				o.push({
					name: 'LAYOUT_VIEW_PROFILE',
					onClick: function () {
						ViewService.view('process.tab', {
							params: {
								process: 'user',
								itemId: $rootScope.clientInformation.item_id
							}
						});
					}
				});
			}

			o.push({
				name: 'LAYOUT_CREATE_LINK',
				onClick: function () {
					MessageService.dialogAdvanced({
						template: 'core/layout/link/linkDialog.html',
						controller: 'LinkDialogController'
					});
				}
			});

			if (Impersonate.indexOf('delegate') !== -1) {
				o.push({
					name: 'LAYOUT_DELEGATE',
					onClick: function () {
						let model = {
							users: []
						};

						let params = {
							title: 'LAYOUT_DELEGATE_CHANGE',
							inputs: [{
								type: 'process',
								process: 'user',
								model: 'users',
								label: 'LAYOUT_DELEGATE_USER'
							}],
							buttons: [
								{
									type: 'secondary',
									text: 'MSG_CANCEL'
								},
								{
									type: 'primary',
									text: 'LAYOUT_DELEGATE_DO',
									onClick: function () {
										return CoreService.delegate(model.users);
									}
								}]
						};

						UserService.getCurrentUser().then(function (user) {
							model.users = user.delegate_users;
							MessageService.dialog(params, model);
						});
					}
				});
			}

			if (Impersonate.indexOf('admin') !== -1) {
				o.push({
					name: 'LAYOUT_IMPERSONATE',
					onClick: function () {
						let model = {
							users: []
						};

						let params = {
							title: 'LAYOUT_IMPERSONATE_CHANGE',
							inputs: [
								{
									type: 'process',
									process: 'user',
									model: 'users',
									label: 'LAYOUT_IMPERSONATE_USER',
									max: 1
								}],
							buttons: [
								{
									type: 'secondary',
									text: 'MSG_CANCEL'
								},
								{
									type: 'primary',
									text: 'LAYOUT_IMPERSONATE_DO',
									onClick: function () {
										return CoreService.impersonate(model.users[0]);
									}
								}]
						};

						MessageService.dialog(params, model);
					}
				});
			}

			if (Impersonate.indexOf('impersonate') !== -1) {
				angular.forEach(Delegates, function (delegate) {
					o.push({
						name: '--> ' + delegate.primary,
						onClick: function () {
							return CoreService.impersonate(delegate.item_id);
						}
					});
				});
			}

			o.push({
				name: 'LAYOUT_LOGOUT',
				onClick: function () {
					SaveService.bypassTick(true).then(function () {
						MessageService.toast('LAYOUT_LOGGING_OUT');
						CoreService.logout();
					});
				}
			});

			MessageService.menu(o, event.currentTarget);
		};

		/*
		 * SIDENAV
		 *
		 */

		$scope.selectors = [];
		$scope.filterOptions = {};
		$scope.filters = {};
		$scope.selector = {
			name: "all",
			search: undefined,
			offsetNumber: 0,
			portfolio: {Process: "all"},
			value: "all",
			items: [],
			maxRowCount: 0,
			multipleSelectedRows: [],
			multipleAmount: 0
		};
		$scope.searchItem = '';
		$scope.searching = false;
		$scope.favourites = FavouritesService.getFavourites();

		function hasSelectedFilters(columns) {
			let hasSelected = false;
			angular.forEach(columns, function (column) {
				if (column.length) {
					hasSelected = true;
				}
			});

			return hasSelected;
		}

		$scope.isCustom = 0;

		$scope.loadMoreSearchResults = function (tab) {
			tab.offsetNumber = tab.offsetNumber + 25;
			itemsQuery(tab, true);
		}

		function itemsQuery(tab, loadMore?) {
			let pp = tab.portfolio.ProcessPortfolioId;
			let s = tab.search;
			if (typeof s != 'undefined' && s.length > 1 || hasSelectedFilters($scope.filters[pp])) {
				$scope.searching = true;
				let p;

				if (tab.portfolio.Process === 'all') {
					p = ProcessService.getItemsFilteredAll(s);
				} else {
					p = ProcessService.getItemsFiltered(
						tab.portfolio.Process,
						{text: s, select: $scope.filters[pp], offset: tab.offsetNumber});
				}
				p.then(function (items) {
					tab.maxRowCount = items.length > 0 ? items[0].$$maxCount : 0;
					if (!loadMore) {
						let displayItems = {};
						let i = items.length;
						while (i--) {
							let item = items[i];
							let process = Translator.translation(defaultSelectors[item.process].default.LanguageTranslation);
							if (typeof displayItems[process] === 'undefined') {
								displayItems[process] = [];
							}
							displayItems[process].push(item);
						}
						let n = items.length ? Translator.translation(defaultSelectors[items[0].process].default.LanguageTranslation) : ""
						if (displayItems[n]) {
							displayItems[n].unshift({
								item_id: [],
								primary: Translator.translate('DRAG_MULTIPLE_ROWS'),
								secondary: Translator.translate('DRAG_MULTIPLE_ROWS_0'),
								process: items.length ? items[0].process : "",
								customItem: true
							})
						}

						tab.items = displayItems;
						$scope.searching = false;
					} else {
						let i = items.length;
						while (i--) {
							let item = items[i];
							let process = Translator.translation(defaultSelectors[item.process].default.LanguageTranslation);
							tab.items[process].push(item);
							$scope.searching = false;
						}
						let n = items.length ? Translator.translation(defaultSelectors[items[0].process].default.LanguageTranslation) : ""
						if (tab[n]) {
							tab[n].items.unshift({
								item_id: [],
								primary: Translator.translate('DRAG_MULTIPLE_ROWS'),
								secondary: Translator.translate('DRAG_MULTIPLE_ROWS_0'),
								process: items.length ? items[0].process : "",
								customItem: true
							})
						}
					}
				});
			} else {
				tab.items = [];
			}
		}

		$scope.searchOverload = "";
		$scope.maxRowCount = 0;

		function itemsQueryNavBar() {
			if (typeof $scope.searchItem != 'undefined' && $scope.searchItem.length > 1) {
				$scope.searching = true;
				let p = ProcessService.getItemsFilteredAll($scope.searchItem);
				p.then(function (items) {
					if(items.length && items[0].$$maxRowCount > 25){
						$scope.maxRowCount = items[0].$$maxRowCount;
						$scope.searchOverload = Translator.translate('LAYOUT_SEARCH_MANY_RESULTS') + items[0].$$maxRowCount + Translator.translate('LAYOUT_SEARCH_MANY_RESULTS1');
					}
					let displayItems = {};
					let i = items.length;
					while (i--) {
						let item = items[i];

						let process = Translator.translation(defaultSelectors[item.process].default.LanguageTranslation);
						if (typeof displayItems[process] === 'undefined') {
							displayItems[process] = [];
						}

						displayItems[process].push(item);
					}

					$scope.items = displayItems;
					$scope.searching = false;

				})
			} else {
				$scope.items = [];
			}
		}

		$scope.goToProcess = function (process, itemId, item) {
			let complete = function () {
				let params = {
					process: process,
					itemId: itemId,
					tabId: null,
				};
				ViewService.view('1stTab', {params: params});
			};

			if (item.process == "task") {
				ViewService.openSpecialTask(item, false).then(function (result) {
					if (result == false) complete();
				});
			} else {
				complete();
			}
		};

		let i = 1;
		let defaultSelectors = SelectorService.getSelectors();
		let favouritesToGet = [];
		angular.forEach(defaultSelectors, function (selectorpol) {
			if (selectorpol.default) {
				if (i === 6) i = 1;

				let selector = selectorpol.default;
				selector.translation = Translator.translation(selector.LanguageTranslation);

				angular.forEach(selector.ProcessPortfolioColumns, function (column) {
					let itemColumn = column.ItemColumn;
					let dt = Common.getRelevantDataType(itemColumn);

					if (column.UseInFilter && (dt == 6 || dt == 5)) {
						let listProcess = itemColumn.DataAdditional;

						ProcessService.getItems(listProcess).then(function (items) {
							let pid = selector.ProcessPortfolioId;

							if (typeof $scope.filterOptions[pid] === 'undefined') {
								$scope.filterOptions[pid] = {};
								$scope.filters[pid] = {};
							}

							$scope.filterOptions[pid][itemColumn.ItemColumnId] = {
								options: items,
								label: itemColumn.Variable,
								data_type: itemColumn.DataType,
								data_additional: itemColumn.DataAdditional
							};
						});
					}
				});

				let s = {
					name: selector.translation,
					search: undefined,
					offsetNumber: 0,
					portfolio: selector,
					value: selector.Process,
					items: [],
					maxRowCount: 0,
					multipleSelectedRows: [],
					multipleAmount: 0
				};
				favouritesToGet.push(selector.Process);

				i++;
				$scope.selectors.push(s);
			}
		});
		FavouritesService.getAllFavourites(favouritesToGet);

		$scope.multipleItem = undefined;

		$scope.selectMultipleRows = function (tab, item, value) {
			let direction = angular.copy(item.$$isSelected)
			if (item.primary == Translator.translate('DRAG_MULTIPLE_ROWS')) {
				for (let i = 1; i < value.length; i++) {
					if (direction == true) {
						value[0].item_id.push(value[i].item_id);
						value[i].$$isSelected = true;
						tab.multipleSelectedRows.push(value[i]);
						tab.multipleAmount++;
					} else {
						value[i].$$isSelected = false;
					}
				}
				if (direction == false) {
					tab.multipleAmount = 0;
					value[0].item_id = [];
				}
			} else {
				value[0].process = item.process;
				$scope.multipleItem = {
					item_id: [],
					primary: Translator.translate('DRAG_MULTIPLE_ROWS'),
					secondary: tab.multipleAmount,
					process: item.process,
					customItem: true
				};

				if (_.findIndex(value, function (o) {
					return o.primary == Translator.translate('DRAG_MULTIPLE_ROWS');
				}) == -1) {
					value.unshift($scope.multipleItem);
					$scope.isCustom = 1;
				}
				if (direction == true) {
					value[0]['item_id'].push(item.item_id);
					value[0].$$hide = false;
					tab.multipleAmount++;
					tab.multipleSelectedRows.push(item);
				} else {
					_.remove(value[0]['item_id'], function (n) {
						return n == item.item_id;
					});
					let v = _.findIndex(tab.multipleSelectedRows, function (o) {
						return o.item_id == item.item_id;
					});

					tab.multipleSelectedRows.splice(v, 1);

					tab.multipleAmount = tab.multipleAmount - 1;

					if (value[0]['item_id'].length == 0) {
						$scope.isCustom = 0;
					}
				}
			}
			value[0].secondary = tab.multipleAmount + " " + Translator.translate('DRAG_MULTIPLE_ROWS_COUNT');
			toggleCustomFavorite(tab);
		}

		let tickTimeout;
		$scope.searchItems = function (selectedTab) {
			clearTimeout(tickTimeout);
			tickTimeout = setTimeout(function () {
				selectedTab.multipleSelectedRows = [];
				selectedTab.multipleAmount = 0;
				itemsQuery(selectedTab);
			}, 1500);
		};

		$scope.searchItemsNavBar = function () {
			$('#navBarSearchParent').on("click", "*", function (event) {
				event.stopPropagation();
			});
			$(window).on("click", function (e) {
				if ($scope.openS) {
					$scope.searchItem = "";
					$scope.items = [];
					$('#navBarSearchParent').off("click");
				}
			});

			clearTimeout(tickTimeout);
			tickTimeout = setTimeout(function () {
			//	if ($scope.searchItem.length > 200) {
					$scope.searching = true;
					itemsQueryNavBar();
				//}
			}, 250);
		};

		$scope.canBeStickied = function (tab) {
			return SidenavService.isStickable(tab);
		};

		$scope.showStickyEdit = function (tab) {
			return SidenavService.canEditSticky(tab);
		};

		$scope.editSticky = function (tab, event) {
			event.stopPropagation();
			SidenavService.editSticky(tab);
		};

		$scope.openSticky = function (tab) {
			ViewService.view(tab.view, {params: tab.params});
		};

		$scope.stick = function (tab, event) {
			event.stopPropagation();
			SidenavService.stickNavigation(tab);
		};

		$scope.openTask = function (task) {
			ProcessService.getProcess(task.owner_item_id).then(function (process) {
				let toState = {
					params: {
						process: process,
						itemId: task.owner_item_id,
						rowId: undefined,
						returnView: undefined
					},
					view: undefined
				};

				if (task.task_type == 21) {
					toState.params.rowId = task.item_id;
					toState.params.returnView = 'process.gantt';
					toState.view = 'process.kanban';
				} else {
					toState.view = task.parent_item_id ? 'process.gantt' : 'process.backlog';
				}

				ViewService.view(toState.view, {params: toState.params});
			});
		};

		$scope.routes = {}
		Common.subscribeViewLoaded($scope, function () {
			$scope.routes = ViewService.getLayoutRoutes();
		});
		$scope.routes = ViewService.getLayoutRoutes();
		$scope.getRouteName = function (route, primary = true) {
			if (primary) {
				if (route.name == "process.tab" || (route.process && route.itemId > 0)) {
					let row = ProcessService.getCachedItemData(route.process, route.itemId);
					return row.Data.primary;
				}

				if (route.name.indexOf("settings.phases.tabs") > -1)
					return Translator.translation(route.activeTitle) + " - " + route.phaseName;

				if (route.name.indexOf("settings.") > -1)
					return Translator.translation(route.activeTitle)

				return route.title != "" ? route.title : "?";
			} else {
				if (route.name.indexOf("settings.") > -1)
					return Translator.translation(route.process);

				if (route.name == "process.tab" || (route.process && route.itemId > 0))
					return route.title != "" ? route.title : "?";

				if (route.params && route.params.activeTitle) return route.params.activeTitle;
				if (route.activeTitle) return route.activeTitle;

				return "";
			}
		}

		$scope.openRoute = function (route) {
			ViewService.view(route.name, {params: route});
		}

		let toggleTimeout = undefined;
		$scope.toggleRouteFavourite = function (route) {
			route.fav = !route.fav;

			if (toggleTimeout) clearTimeout(toggleTimeout);
			toggleTimeout = setTimeout(function () {
				let favs = [];
				for (let f of $scope.routes) {
					if (f.fav) favs.push(f);
				}

				Configuration.getSavedPreferences('routeFavourites', 1).then(function (p) {
					if (p.length == 0) {
						Configuration.newPreferences('routeFavourites', 1, 'ok', 0, favs);
					} else {
						Configuration.savePreferences('routeFavourites', 1, p[0].ConfigurationId, "ok", 0, favs);
					}
				});
			}, 1000);
		}


		$scope.customFavorite = true;
		let toggleCustomFavorite = function (tab) {
			for (let i = 0; i < tab.multipleSelectedRows.length; i++) {
				let process = tab.multipleSelectedRows[i].process;
				if (!$scope.favourites[process]) $scope.favourites[process] = {};
				if (!$scope.favourites[process][tab.multipleSelectedRows[i].item_id]) {
					$scope.customFavorite = false;
					break;
				} else {
					$scope.customFavorite = true;
				}
			}
		}

		$scope.translated = Translator.translate('DRAG_MULTIPLE_ROWS');

		$scope.toggleFavourite = function (tab, item) {
			let process = item.process;
			let itemId = item.itemId;

			if (item.primary == Translator.translate('DRAG_MULTIPLE_ROWS')) {
				$scope.customFavorite = !$scope.customFavorite;
			}

			if (item.primary != Translator.translate('DRAG_MULTIPLE_ROWS')) {
				if ($scope.favourites[process][itemId]) {
					FavouritesService.removeFavourite(process, itemId);
				} else {
					FavouritesService.addFavourite(process, itemId);
				}
			} else {
				if (tab) {
					_.each(tab.multipleSelectedRows, function (item) {
						if ($scope.favourites[process][item.item_id] && !$scope.customFavorite)
							FavouritesService.removeFavourite(process, item.item_id);

						if (!$scope.favourites[process][item.item_id] && $scope.customFavorite)
							FavouritesService.addFavourite(process, item.item_id);
					});
				}
			}
		};

		$scope.dynamicTabIndex = -1;
		$scope.showDynamicNavigation = function () {
			if (!$scope.staticLock) {
				$leftSidenav.addClass('show-dynamic');
			}
			$scope.dynamicTabIndex = 0;
		};

		$scope.hideDynamicNavigation = function () {
			$leftSidenav.removeClass('show-dynamic');
			$scope.dynamicTabIndex = -1;
		};

		SidenavService.subscribeSidenav($scope, function (argument) {
			let o = argument.open;
			if (o === 'dynamic') {
				$scope.staticLock = false;
				$scope.showDynamicNavigation();
			} else if (o === 'static') {
				$scope.hideDynamicNavigation();
			} else if (o === 'right') {
				showRightSidenav();
			} else if (o === 'left') {
				showLeftSidenav();
			}

			let p = argument.process;
			if (p) {
				let i = $scope.selectors.length;
				while (i--) {
					let s = $scope.selectors[i];
					if (s.value == p) {
						s.onClick(s.onClickArgument);
						break;
					}
				}

				$scope['tabs-sidenav-tabs'].switch('favourites');
				$scope['accordion-sidenav-accordion-favourites'].open({id: p});
			}
		});
		
		
		$scope.errorCount = function() {
			return ErrorService.errorCount();
		}
		
		$scope.openErrors = function() {
			let params = {
				controller: 'ErrorDialogController',
				locals: {},
				template: 'core/common/errorDialog.html'
			};
			MessageService.dialogAdvanced(params);
		}
	}

	UserFilter.$inject = ['$filter'];

	function UserFilter($filter) {
		return function (array, array_) {
			return $filter('filter')(array, function (arrayItem) {
				return array_.indexOf(arrayItem) !== -1;
			});
		};
	}

})();

