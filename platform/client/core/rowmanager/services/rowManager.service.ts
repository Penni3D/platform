﻿var RMset = {};
var allDI = {};
var allCA = {};
var alias;
(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.service('RMService', RMService);

	RMService.$inject = [
		'$http',
		'$rootScope',
		'Translator',
		'SaveService',
		'ApiService',
		'Common',
		'CalendarService',
		'ProcessService',
		'PortfolioService',
		'UserService',
		'ViewService',
		'MessageService',
		'CellFactoryService',
		'ArrowFactoryService',
		'Colors',
		'$q',
		'Configuration',
		'HTMLService',
		'Sanitizer',
		'$sanitize'
	];

	function RMService(
		$http,
		$rootScope,
		Translator,
		SaveService,
		ApiService,
		Common,
		CalendarService,
		ProcessService,
		PortfolioService,
		UserService,
		ViewService,
		MessageService,
		CellFactoryService,
		ArrowFactoryService,
		Colors,
		$q,
		Configuration,
		HTMLService,
		Sanitizer,
		$sanitize
	) {
		var statusChanges = [];
		var userTasks = {};
		var statusChangeMapping = {};
		var Tasks = ApiService.v1api('features/Tasks');
		var self = this;
		self.rmDragActive = false;
		self.RMsets = {};
		self.displayFormat = function (portfolioColumn, field, showName, rowProcess, unionPortfolioId) {
			var d = $q.defer();
			var column = portfolioColumn.ItemColumn;
			var type = Common.getRelevantDataType(column);

			if (field === null || typeof field === 'undefined' || field.length === 0) {
				d.resolve({
					html: ''
				});
			} else {
				if (type == 1 || type == 2) {
					if (portfolioColumn.ItemColumn.DataAdditional != 'AttachmentCount') {
						var n = Common.formatNumber(field, portfolioColumn.Validation.Precision);
						d.resolve({
							html: n,
							tooltip: n
						});
					} else {
						var n = Common.formatNumber(field, portfolioColumn.Validation.Precision);
						var obj = JSON.parse(portfolioColumn.ItemColumn.DataAdditional2);
						let $result = '';

						if (n != 0) {
							if (obj['@ATTACHLINKCOLUMNTYPE'] == 1) {
								$result = '<i style="color: blue; background-color:transparent" class="material-icons process-progress circle circle-small ' + self.sanitize(field) + '" aria-hidden="true">attachment</i>' + ' ' + n;
							} else {
								$result = '<i style="color: blue; background-color:transparent" class="material-icons process-progress circle circle-small ' + self.sanitize(field) + '" aria-hidden="true">launch</i>' + ' ' + n;
							}
						} else {
							$result = n;
						}

						d.resolve({
							html: $result,
							tooltip: n
						});
					}
				} else if (type == 23 || type == 4) {
					if (field.includes('html') && field.includes('delta')) {
						let t = JSON.parse(field);
						if (t[$rootScope.clientInformation.locale_id]) {
							result = Common.stripHtmlTags(t[$rootScope.clientInformation.locale_id].html);
						} else {
							result = Common.stripHtmlTags(t.html);
						}
					} else {
						result = Sanitizer(field);
					}
					d.resolve({
						html: result,
						tooltip: field
					});
				} else if (type == 13) {
					var date = CalendarService.formatDateTime(field);
					d.resolve({
						html: date,
						tooltip: date
					});
				} else if (type == 3) {
					var date = CalendarService.formatDate(field);
					d.resolve({
						html: date,
						tooltip: date
					});
				} else if (type == 5 || type == 6 || type == 16 || type == 20) {
					let process = "";
					let ud = PortfolioService.getUnionXRef(column.Process);
					if (ud)
						process = Common.getRelevantDataAdditional(ud[rowProcess + unionPortfolioId][column.Name + "_" + unionPortfolioId]);
					else
						process = Common.getRelevantDataAdditional(column);

					if (process == 'user' && !showName) {
						UserService.getAvatars(field, {limit: 2, size: 'smaller'})
							.then(function ($avatars) {
								d.resolve({
									html: $avatars
								});
							});
					} else {
						ProcessService.getItems(process, field).then(function (items) {
							var tooltip = '';
							var $result = $('<div class="process-value">');
							var processValues = '';
							var showType = portfolioColumn.ShowTypeCombined;

							for (var i = 0, l = items.length; i < l; i++) {
								var data = items[i];
								if (!data.primary && data.primary !== 0) {
									data.primary = '';
								}

								if (showType != 1 && data.list_symbol) {
									var icon = HTMLService.initialize('i').class('material-icons');
									var colour = Colors.getColor(data.list_symbol_fill).rgb;

									var style = colour ? ['color: ' + colour] : [];
									style.push('margin: 0');

									if (showType != 3) {
										style.push('font-size: 1.625rem');
									}

									icon.style(style);

									if (i !== 0) {
										icon.content(', ' + self.sanitize(data.list_symbol));
									} else {
										icon.content(self.sanitize(data.list_symbol));
									}

									processValues += icon.getHTML();

									if (showType == 3 && data.primary) {
										processValues += HTMLService.initialize('span')
											.style('margin-left: 4px')
											.content(self.sanitize(data.primary))
											.getHTML();
									}
								} else if ((showType != 2 || type == 5) && data.primary) {
									var s = HTMLService.initialize('span');

									if (processValues.length) {
										s.content(', ' + self.sanitize(data.primary));
									} else {
										s.content(self.sanitize(data.primary));
									}

									processValues += s.getHTML();
								} else {
									continue;

								}
								let userListLanguage = 'list_item_' + $rootScope.clientInformation.locale_id.toLowerCase().replace('-', '_');

								if (data.hasOwnProperty(userListLanguage)) {
									data.primary = data[userListLanguage];
								}

								if (data.primary) {
									if (tooltip.length) {
										tooltip += ', ' + data.primary;
									} else {
										tooltip += data.primary;
									}
									data.list_item = data.primary;
								}
							}

							d.resolve({
								html: $result.html(processValues),
								tooltip: tooltip
							});
						});
					}


				} else if (type == 19) {
					var r = _.join(field, ', ');
					d.resolve({
						html: r,
						tooltip: r
					});
				} else if (type == 60) {
					var $result = $('<i class="material-icons process-progress circle circle-small ' + self.sanitize(field) + '" aria-hidden="true">arrow_forward</i>');
					d.resolve({
						html: $result
					});
				}
				else if (type == 65) {
					var symbol = field.list_symbol;
					var result = '';

					if (typeof symbol !== 'undefined') {
						var c = Colors.getColor(field.list_symbol_fill).rgb;
						result = $('<i class="material-icons process-milestone" aria-hidden="true" style="color: ' + c + '">forward</i>');
					}

					d.resolve({
						html: result,
						tooltip: field.description
					});
				} else {
					d.resolve({
						html: self.sanitize(field),
						tooltip: field
					});
				}
			}

			return d.promise;
		};

		self.sanitize = Sanitizer;

		self.addRM = function (id, RM) {
			self.RMsets[id] = RM;
			return id;
		};

		self.openMenu = MessageService.menu;

		self.openFeaturette = function (DI, readonly) {
			let data = DI.data;
			let forceReadOnly = !DI.editable() || readonly;
			let process = typeof data.process !== 'undefined' && data.process.length > 0 ? data.process : 'task';

			ViewService.view('featurette',
				{
					params: {
						process: process,
						itemId: typeof data.actual_item_id === 'undefined' ? data.item_id : data.actual_item_id,
						title: data.title,
						forceReadOnly: forceReadOnly,
						sizeForNext: DI.RM.options.featuretteWidth
					}
				},
				{
					split: true,
					size: DI.RM.options.featuretteWidth
				}
			);
		};

		self.getColor = function (id) {
			return Colors.getColor(id).rgb;
		};

		self.getCell = function (itemId, startDate, endDate) {
			return CellFactoryService.get(self.itemSet[itemId], startDate, endDate);
		};

		self.getUserTasks = function () {
			return Tasks.get($rootScope.clientInformation.item_id).then(function (response) {
				for (var i = 0, l = response.length; i < l; i++) {
					userTasks[response[i].id] = response[i];
				}

				return userTasks;
			});
		};

		/*  ___  __  ___  __                 __     __
		   / _ \/  |/  / / /____ __ _  ___  / /__ _/ /____
		  / , _/ /|_/ / / __/ -_)  ' \/ _ \/ / _ `/ __/ -_)
		 /_/|_/_/  /_/  \__/\__/_/_/_/ .__/_/\_,_/\__/\__/
									/_/
		 this is a basis of the all managers, these common functions are called when drag/drop events are lauched.
		 */
		self.alias = {};
		alias = self.alias;
		self.inheritParent = function (DIData, parentData) {
			DIData.parent_item_id = parentData.item_id;
			DIData.owner_item_id = parentData.owner_item_id;
			DIData.owner_item_type = parentData.owner_item_type;
		};

		self.getDefaultDIData = function (title, type, startDate) {
			var data = {};

			if (typeof title === 'undefined') {
				title = Translator.translate('RM_ITEM');
			}

			data.has_write_right = true;
			data.title = Translator.translate('RM_NEW') + ' ' + title;
			data.description = '';
			data.task_type = type;
			var d = new Date();

			if (typeof startDate !== 'undefined') {
				startDate = new Date(startDate);
				startDate.setDate(startDate.getDate() + 1);
				if (d < startDate) {
					d = startDate;
				}
			}

			data.start_date = new Date(d);
			data.end_date = new Date(d);
			data.linked_tasks = {};

			if (type == 21) {
				data.end_date.setDate(data.end_date.getDate() + 14);
			} else if (type == 1) {
				data.end_date.setDate(data.end_date.getDate() + 7);
			}

			return data;
		};
		var RMidentification = 0;
		self.managerTemplate = function () {
			var RM = {};
			var initFields = '';
			RM.fieldCache = {};
			RM._readOnly = false;
			RM.initLock = function ($ele) {
				RM.lock = function () {
					if (!RM._readOnly) {
						RM._readOnly = true;
						setTimeout(function () {
							RM.$addrowEle.hide();
							$ele.closest('div').find('.RMlock i').html('lock');
							RM.calendarReDraw();
						}, 0);
					}
				};
				RM.unlock = function () {
					if (RM._readOnly) {
						RM._readOnly = false;
						setTimeout(function () {
							RM.$addrowEle.show();
							$ele.closest('div').find('.RMlock i').html('lock_open');
							RM.calendarReDraw();
						}, 0);

					}
				};
				var toggleLock = function () {
					if (RM._readOnly) {
						RM.unlock();
					} else {
						RM.lock();
					}
				};
				setTimeout(function () {
					if (!RM.options.readOnly) {
						$ele.closest('div').find('.RMlock i').click(toggleLock);
					} else {
						$ele.closest('div').find('.RMlock i').hide();
					}

				}, 0);
			};

			var countAndUpdateChildren = function (parent) {
				var runner = parent.firstEle;
				var ignoreList = new Array();
				while (typeof runner !== "undefined") {

					if (typeof runner.firstEle === "undefined") {
						runner.setDataFromChildren();
					} else {
						countAndUpdateChildren(runner);
					}
					runner.updateFields();
					runner = runner.next;
				}
			};
			RM.countAndUpdateAllRows = function () {
				countAndUpdateChildren(RM.rootParent);
			};
			RM.setValues = function (rootParent, rows) {
				RM.rootParent = rootParent;
				RM.id = rootParent.data.owner_item_id + rootParent.data.owner_item_type + (RMidentification++);
				RM.rows = rows;
			};

			RM.deleteRow = function (itemId) {
				var i = RM.rows.length;
				while (i--) {
					if (RM.rows[i].item_id == itemId) {
						RM.rows.splice(i, 1);
						break;
					}
				}
			};

			RM.addRow = function (data) {
				RM.rows.push(data);
			};

			RM.createFieldsCache = function (portfolioColumns) {
				for (var i = 0, cc = portfolioColumns.length; i < cc; i++) {
					var col = portfolioColumns[i];
					if (col.ItemColumn.Name == 'responsibles') {
						RM.test = col.ItemColumn.ItemColumnId;
					} // This might need some work MPe
					col.visible = true;
					RM.fieldCache[col.ItemColumn.Name] = col;

					if (typeof col.ItemColumn.showType === 'undefined') {
						col.ItemColumn.showType = 'noType';
					}

					switch (col.UseInSelectResult) {
						case 3: // start date
						case 4: // end date
							self.alias[col.UseInSelectResult] = col.ItemColumn.Name;
							break;
						case 6: // sumUp
						case 9: // avg
						case 10: // min
						case 11: // max	
						case 12: // sumUp All						
							if (typeof self.alias[col.UseInSelectResult] === "undefined")
								self.alias[col.UseInSelectResult] = new Array();

							if (self.alias[col.UseInSelectResult].indexOf(col.ItemColumn.Name) == -1)
								self.alias[col.UseInSelectResult].push(col.ItemColumn.Name);

							if (col.ItemColumn.DataType !== 1 && col.ItemColumn.DataType !== 2)
								ProcessService.ignoreColumn(RM.options.process, col.ItemColumn.Name, true);

							break;
					}
				}
			};

			RM.getFieldTitle = function (name) {
				var field = RM.fieldCache[name];
				var shortTranslation = Translator.translation(field.ShortNameTranslation);
				return shortTranslation ? shortTranslation : Translator.translation(field.ItemColumn.LanguageTranslation);
			};
			RM.getFieldTip = function (name) {
				var field = RM.fieldCache[name];
				var shortTranslation = Translator.translation(field.ShortNameTranslation);
				return shortTranslation ? shortTranslation : Translator.translation(field.ItemColumn.LanguageTranslation);
			};
			var forceMinWidths = function ($ele) {
				var availableWidth = $ele.parent().width();
				var requiredWidth = 0;
				var totalWidth = 0;

				if (typeof calendarBody !== 'undefined') requiredWidth = 560;
				if (availableWidth == 0) availableWidth = $ele.parent().parent().width();

				var autoCols = [];
				var groupWidths = {};
				$.each(RM.fieldCache, function (i, col) {
					if (col.visible) {
						if (col.Width != null) {
							requiredWidth += col.Width;
							totalWidth += col.Width;

							var groupId = col.ProcessPortfolioColumnGroupId;
							if (typeof groupWidths[groupId] === 'undefined') {
								groupWidths[groupId] = 0;
							}

							groupWidths[groupId] += col.Width;
						} else {
							requiredWidth += Common.getDefaultWidth(Common.getRelevantDataType(col.ItemColumn));
							autoCols.push(col);
						}
					}
				});

				var numberOfAutoCols = autoCols.length;
				var divided = 0;

				if (requiredWidth < availableWidth) {
					var divider = numberOfAutoCols;

					if (typeof calendarBody !== 'undefined') {
						divider++;
					}

					divided = ((availableWidth - requiredWidth) / divider);
				}

				for (var i = 0; i < numberOfAutoCols; i++) {
					var w = Common.getDefaultWidth(Common.getRelevantDataType(autoCols[i].ItemColumn));
					var tw = Math.floor(w + divided);
					totalWidth += tw;
					var pc = autoCols[i];
					var groupId = pc.ProcessPortfolioColumnGroupId;

					if (typeof groupWidths[groupId] === 'undefined') {
						groupWidths[groupId] = 0
					}

					groupWidths[groupId] += tw;

					if (typeof pc.$ele !== 'undefined') {
						pc.$ele.css({
							'width': tw,
							'min-width': tw,
							'max-width': tw
						});
					}
					if (typeof pc.$hEle !== 'undefined') {
						pc.$hEle.css({
							'width': tw,
							'min-width': tw,
							'max-width': tw
						});
					}
				}

				angular.forEach(RM.$headerGroups, function (headers, key) {
					var w = groupWidths[key];
					if (w) {
						w -= 2;
						headers.$header.css('width', w);

						if (headers.$headerbar) {
							headers.$headerbar.css('width', w);
						}
					}
				});

				if (typeof calendarBody !== 'undefined') {
					var w = Math.floor(560 + divided - 5);
					totalWidth += w;

					calendarBody.css({
						'width': w,
						'min-width': w,
						'max-width': w
					});

					if (typeof calendarHead !== 'undefined') {
						calendarHead.css({
							'width': w,
							'min-width': w,
							'max-width': w
						});
					}
				}

				if (totalWidth > availableWidth) {
					totalWidth = availableWidth;
				}

				$ele.width(totalWidth);
				RM.calendarReDraw();
			};

			RM.setFieldWidth = function (name, $ele, $hEle) {
				var w = RM.fieldCache[name].Width;
				RM.fieldCache[name].$ele = $ele;

				$ele.css({
					'width': w,
					'min-width': w,
					'max-width': w
				});

				if (typeof $hEle !== 'undefined') {
					RM.fieldCache[name].$hEle = $hEle;
					if (w != null) {
						$hEle.css({
							'width': w,
							'min-width': w,
							'max-width': w
						});
					}
				}
			};

			// returns css class for last field initialized via addField
			RM.getFieldCls = function (name) {
				var cls = name;
				var itemColumn = RM.fieldCache[name].ItemColumn;
				var type = Common.getRelevantDataType(itemColumn);
				switch (type) {
					case 1:
					case 2:
					case 3:
					case 13:
					case 14:
						cls += ' align-right';
						break;
				}

				return cls;
			};

			RM.initializeScroll = function (scope) {
				RM.$ele.find('.row-manager').attr('diset', RM.id);

				var $content = RM.$ele.closest('content[view]');
				if ($content.length) {
					var cTop = $content.offset().top;

					$content.on('scroll.' + RM.id, function () {
						var tHeight = RM.$ele.height();
						var startPosition = RM.$ele.offset().top - cTop;

						var shift = -startPosition;

						if (shift < 0) {
							shift = 0;
						}

						var max = tHeight - 146;

						if (max < 0) {
							max = 0;
						}

						if (shift > max) {
							shift = max;
						}

						RM.$fHeaderEle.css({'transform': 'translateY(' + shift + 'px)'});

						if (shift) {
							RM.$fHeaderEle.addClass('z-depth-1');
						} else {
							RM.$fHeaderEle.removeClass('z-depth-1');
						}
					});

					scope.$on('$destroy', function () {
						$content.off('scroll.' + RM.id);
					});
				}
			};

			RM.setVisibility = function (arr, $ele1, $ele2) {
				var showFields = {};
				var groupedColumns = {};
				var hideFields = '';
				initFields = '';

				$.each(RM.fieldCache, function (i, col) {
					var name = col.ItemColumn.Name;
					var found = true;

					for (var j = 0, cj = arr.length; j < cj; j++) {
						if (arr[j] == name) {
							found = false;
						}
					}
					if (found) {
						var groupId = col.ProcessPortfolioColumnGroupId;
						if (typeof groupedColumns[groupId] === 'undefined') {
							groupedColumns[groupId] = [];
						}

						groupedColumns[groupId].push(col);

						if (!col.visible) {
							if (typeof showFields[col.ItemColumn.showType] === 'undefined') {
								showFields[col.ItemColumn.showType] = '.' + name;
							} else {
								showFields[col.ItemColumn.showType] += ', .' + name;
							}
							col.visible = true;
						}
					} else {
						initFields += ', .' + name;
						if (col.visible) {
							hideFields += ', .' + name;
							col.visible = false;
						}
					}
				});

				$.each(RM.headerGroups, function (key, group) {
					var gc = groupedColumns[key];
					if (gc && gc.length) {
						var ic = gc[0];

						if (typeof RM.$headerGroups[key] === 'undefined') {
							var $g = $(group);

							RM.$headerGroups[key] = {
								$header: $g
							};
						}

						var hg = RM.$headerGroups[key];
						hg.$header.appendTo(ic.$ele);

						if (typeof hg.$headerbar === 'undefined') {
							hg.$headerbar = $g.clone();
						}

						hg.$headerbar.appendTo(ic.$hEle);
					}
				});

				if (initFields.length > 2) {
					initFields = initFields.substr(2);
				}

				$.each(showFields, function (displayAs, elements) {
					if (displayAs === 'noType') {
						displayAs = '';
					}
					if (typeof $ele1 !== 'undefined') {
						$ele1.find(elements).parent().css('display', displayAs);
					}

					if (typeof $ele2 !== 'undefined') {
						$ele2.find(elements).parent().css('display', displayAs);
					}
				});

				if (hideFields.length > 2) {
					hideFields = hideFields.substr(2);
					if (typeof $ele1 !== 'undefined') {
						$ele1.find(hideFields).parent().css('display', 'none');
					}

					if (typeof $ele2 !== 'undefined') {
						$ele2.find(hideFields).parent().css('display', 'none');
					}
				}

				if (typeof $ele1 !== 'undefined') {
					forceMinWidths($ele1);
				}
			};

			RM.initVisibility = function ($ele) {
				$ele.find(initFields).parent().css('display', 'none');
			};

			RM.addEmptyArea = function ($ele, cls) {
				var $area = $('<tr><td colspan=\'4\' class=\'' + self.sanitize(cls) + '\'></td></tr>').appendTo($ele).find('td');
				emptyCalendarRow(cls);
				return $area;
			};
			/*____                ___   ____
			 / __ \___  ___ ___  / _ | / / /
			/ /_/ / _ \/ -_) _ \/ __ |/ / / 
			\____/ .__/\__/_//_/_/ |_/_/_/  
				/*/
			var openables = {};
			RM.addOpenable = function(DI) {
				openables[DI.data.item_id] = DI;
				RM.updateOpenAll();
			}
			RM.removeOpenable = function(DI) {
				delete openables[DI.data.item_id];
				RM.updateOpenAll();
			}
			RM.updateOpenAll = function() {
				if(RM.options.hideOpenAll) {
					return false;
				}
				var css = "openAll btn btn-icon"
				var hide = " hide";
				var flip = " ";
				$.each(openables,function(id,DI) {

					if(!DI.isOpen) {
						flip=" flip";
					}
					hide = "";

				});
				var $openAll = RM.$ele.find(".openAll")
				$openAll.attr("class", css+hide+flip);
			}

			RM.openAll = function() {
				var $openAll = RM.$ele.find(".openAll")
				if($openAll.hasClass("flip")) {
					$openAll.removeClass("flip");
					$.each(openables, function( id, DI ) {
						DI.open(false,false);
					});

				} else {
					$openAll.addClass("flip");
					$.each(openables, function( id, DI ) {
						DI.close(false,false);
					});
				}
				RM.updateOpenAll();
				RM.calendarReDraw();
			}


			/* _____     __            __         __  ___
			  / ___/__ _/ /__ ___  ___/ /__ _____/  |/  /__ ____  ___ ____ ____ ____
			 / /__/ _ `/ / -_) _ \/ _  / _ `/ __/ /|_/ / _ `/ _ \/ _ `/ _ `/ -_) __/
			 \___/\_,_/_/\__/_//_/\_,_/\_,_/_/ /_/  /_/\_,_/_//_/\_,_/\_, /\__/_/
																	 /__*/

			var emptyCalendarRow = function () {
			};
			RM.calendarReDraw = function () {
			};
			RM.initializeArrows = function () {
			};
			RM.setCalendarHeight = function () {
			};
			RM.initializeCalendar = function () {
			};
			RM.calendarAreaStart = function () {
			};
			RM.calendarNextDate = function () {
			};
			RM.moveCalendar = function (d) {
			};
			var calendarBody;
			var calendarHead;
			RM.initCalendar = function ($wrapper, $scope, options) {
				var savedSettings = self.getPortfolioSettings($scope.portfolioId).calendar;
				var calendarSteps = $scope.options.calendarSteps;

				if (typeof calendarSteps === 'undefined') {
					calendarSteps = [15, 90, 180, 360, 720];
				}
				var settings = {
					$mainHolder: $wrapper.find('.rowmanager-body tr:first').children().last(),
					$headerHolder: $wrapper.find('.rowmanager-floating-header tr:first').children().last(),
					monthsofyear: Common.getLocalShortMonthsOfYear(),
					daysofweek: Common.getLocalShortDaysOfWeek(),
					portfolioId: $scope.portfolioId,
					showAllocation: $scope.showAllocation,
					textModeDays: Translator.translate('CALENDAR_DAYS'), //"days",
					textModeMonths: Translator.translate('CALENDAR_MONTHS'), //"months",
					textModeQ: Translator.translate('CALENDAR_QUARTALS'), //"quartals",
					textModeYears: Translator.translate('CALENDAR_YEARS'), //"years",
					textDayView: Translator.translate('CALENDAR_DAY_VIEW'), //"Day View",
					textWeekView: Translator.translate('CALENDAR_WEEK_VIEW'), //"Week View",
					textMonthView: Translator.translate('CALENDAR_MONTH_VIEW'), //"Month View",
					textQuartalView: Translator.translate('CALENDAR_QUARTAL_VIEW'), //"Quartal View",
					textYearView: Translator.translate('CALENDAR_YEAR_VIEW'), //"Year View",
					calendarSteps: calendarSteps,
					projectStartDate: $scope.options.projectStartDate,
					projectEndDate: $scope.options.projectEndDate,
					showWeeknum: Common.isTrue(Configuration.getClientConfigurations('Calendar', 'Show weeknumber')),
					qStart: Configuration.getClientConfigurations('Calendar', 'First quartal')
				};

				var CA = $wrapper.AddCalendar(self, settings, savedSettings, RM);
				if (CA[0] && CA[0].id && CA[0].id != "") allCA[CA[0].id] = CA;

				CA.slacks = $scope.options.slacks;
				calendarBody = CA.getBody();
				calendarHead = CA.getHead();
				RM.setResolution = CA.setResolution;
				RM.getResolution = CA.getResolution;
				RM.calendarReDraw = CA.reDraw;
				RM.clickActions = {
					dblClick: function (id, sd, ed) {
					},
					cellClick: function (cell) {
					},
					emptyClick: function (id, sd, ed) {
					}
				};
				RM.moveCalendar = CA.moveCalendar;

				if (typeof options !== 'undefined') {
					if (typeof options.dblClick === 'function') RM.clickActions.dblClick = options.dblClick;
					if (typeof options.cellClick === 'function') RM.clickActions.cellClick = options.cellClick;
					if (typeof options.emptyClick === 'function') RM.clickActions.emptyClick = options.emptyClick;
				}
				CA.setArrowManager(ArrowFactoryService.arrowManager(CA))
				RM.setCalendarHeight = CA.setHeight;
				RM.initializeArrows = CA.initializeArrows;
				RM.initializeCalendar = CA.initialize;
				RM.initCalendarRow = function (DI) {
					CA.initCalendarRow(DI);
				};
				RM.initCalendarIcons = CA.initCalendarIcons;
				RM.initCalendarBar = CA.initCalendarBar;
				emptyCalendarRow = CA.emptyCalendarRow;
				RM.calendarAreaStart = CA.calendarAreaStart;
				RM.calendarNextDate = CA.calendarNextDate;
				return RM;
			};
			// Resized
			RM.initializeResize = function (scope, $ele) {
				var resizedFunction = function () {
					forceMinWidths($ele);
				};
				setTimeout(function () {
					//Common.subscribeContentResize(scope, resizedFunction);
					resizedFunction();
				}, 0);
			};

			//Empty placeholder classes, rewritten in calendar manager
			RM.addNewRow = function () {
			};
			RM.initCalendarRow = function () {
			};
			RM.initCalendarBar = function () {
			};
			RM.initCalendarIcons = function () {
			};
			RM.selectMode = false;
			var rules = {};
			var legalNodes = {};

			var reDraw = function (DI) {
				if (DI.$ele) {
					DI.$ele.remove();
				}

				if (RM.previousRow.isRoot) {
					RM.init(DI).prepend();
					DI.setPadding(1);
				} else {
					RM.init(DI).after(RM.previousRow);
					DI.setPadding(RM.targetParent.getCurrentIndex() + 1);
				}

				DI.initVisibility();
			};

			RM.drawBar = function () {
			};

			RM.validIndex = 0;
			var DIIndex = 0;

			var findPrev = function (target) {
				var p;
				if (typeof target.prev !== 'undefined' && !target.prev.isRoot) {
					p = target.prev.prevRow();
				} else {
					p = target.parent;
				}

				return p;
			};

			RM.canBeDragged = function (itemId) {
				if (RM.options.readOnly) {
					return false;
				}
				var DI = self.itemSet[itemId];
				return DI.isDraggable();
			};

			RM.initDrag = function (itemId) {
				RM.closeMenu();
				RM.clearClicks();

				var DI = self.itemSet[itemId];
				DI.calendarRow.hide();

				if (DI.isOpen) {
					DI.close(false, true);
				}

				DIIndex = DI.getCurrentIndex();
				RM.validIndex = DIIndex - 1;

				RM.drawBar = function () {
					DI.calendarRow.show();
				};

				if (typeof DI.prev !== 'undefined') {
					RM.previousRow = DI.prev.prevRow();
				} else {
					if (!DI.parent.isRoot) {
						RM.previousRow = DI.parent.prevRow();
					} else {
						RM.previousRow = DI.parent;
					}
				}

				RM.nextRow = DI.nextRow();

				findLegalNodes(DI, RM.previousRow, RM.nextRow);
				RM.setCalendarHeight();
				setPlaceholderPosition();
				RM.targetParent = DI.parent;
				return RM;
			};

			RM.initializeArrows = function () {
			};

			// triggered when dragged is on top of the placeholder, so basicly not moved on the row it was
			// DI - dragged element
			// indexes - 
			RM.onPlaceholder = function (itemId, indexes) {
				if (RM.selectMode) {
					return false;
				}

				var abs = DIIndex + indexes.index;
				var n = RM.getLegalNode(abs);

				if (n) {
					RM.targetParent = n.node;
					RM.validIndex = n.index;
					setPlaceholderPosition();
					return true;
				}

				return false;
			};

			// triggered when dragged is on top of a another row, placed on the way that it will be placed after given row
			// DI - dragged element
			// target - target element
			// index - 
			RM.after = function (itemId, targetId, indexes) {
				if (RM.selectMode) {
					return false;
				}

				var DI = self.itemSet[itemId];
				var target = self.itemSet[targetId];
				var currentPreviousRow = target;
				var currentNextRow = target.nextRow();

				if (!currentNextRow.isRoot && currentNextRow.data.item_id == DI.data.item_id) {
					currentNextRow = DI.nextRow();
				}

				var abs = DIIndex + indexes.index;
				findLegalNodes(DI, currentPreviousRow, currentNextRow);
				var n = RM.getLegalNode(abs);

				if (n) {
					RM.dropTarget = target;
					RM.previousRow = currentPreviousRow;
					RM.nextRow = currentNextRow;
					RM.targetParent = n.node;
					RM.validIndex = n.index;
					RM.drawBar = function () {
						DI.calendarRow.after(target, false);
					};
					setPlaceholderPosition();
					return true;
				}

				return false;
			};
			// triggered when dragged is on top of a another row, placed on the way that it will be placed before given row
			// DI - dragged element
			// target - target element
			// index - 
			RM.before = function (itemId, targetId, indexes) {
				if (RM.selectMode) {
					return false;
				}

				var DI = self.itemSet[itemId];
				var target = self.itemSet[targetId];
				var currentNextRow = target;
				var currentPreviousRow = findPrev(target);

				if (!currentPreviousRow.isRoot && currentPreviousRow.data.item_id == DI.data.item_id) {
					currentPreviousRow = findPrev(DI);
				}

				var abs = DIIndex + indexes.index;
				findLegalNodes(DI, currentPreviousRow, currentNextRow);
				var n = RM.getLegalNode(abs);

				if (n) {
					RM.dropTarget = target;
					RM.previousRow = currentPreviousRow;
					RM.nextRow = currentNextRow;
					RM.targetParent = n.node;
					RM.validIndex = n.index;
					RM.drawBar = function () {
						DI.calendarRow.before(target, false);
					};
					setPlaceholderPosition();
					return true;
				}
				return false;
			};

			var placeholderPadding = 0;
			// sets padding for placeholder
			var setPlaceholderPosition = function () {
				placeholderPadding = 0;

				if (RM.validIndex > 1) {
					placeholderPadding = RM.validIndex * 24;
				}
				if (typeof RM.$placeholder !== 'undefined') {
					RM.$placeholder.find('td:first-of-type').css('padding-left', placeholderPadding);
				}

				return RM;
			};

			// return placeholder element of this manager. 
			// type - integer type of dragged element
			RM.getPlaceholder = function (type) {
				RM.$placeholder = $(
					'<tr>' +
					'<td class="placeholder" colspan="100%" style="padding-left:' + placeholderPadding + 'px">' +
					'<div>&nbsp;</div>' +
					'</td>' +
					'</tr>');

				return RM.$placeholder;
			};

			// triggered when dragged is inside status, ie used in kanban
			RM.inStatus = function (DI, status, row) {
				return false;
			};

			// triggered when dragged is on top of the row with class "empty"
			RM.onEmpty = function (itemId) {
				if (RM.selectMode) {
					return false;
				}

				var DI = self.itemSet[itemId];
				var t = RM.hasRights(1, DI, RM.rootParent);

				if (t) {
					RM.previousRow = RM.rootParent.lastEle;
					RM.nextRow = RM.rootParent;
					RM.targetParent = RM.rootParent;
					RM.validIndex = 1;

					RM.drawBar = function () {
						DI.calendarRow.append(false);
					};
					setPlaceholderPosition();
					return true;
				}

				return false;
			};

			RM.init = function (DI) { // takes any DI and rebrands it for this RM
				return DI;
			};

			RM.closeMenu = function () {
			};
			RM.clearClicks = function () {
			};

			RM.getFirstLegalNode = function (self, previousRow, nextRow) {
				if (previousRow.isRoot) {
					if (RM.hasRights(1, self, previousRow)) {
						return {node: previousRow, index: 1};
					} else {
						return;
					}
				}

				var runner = previousRow;
				var i = previousRow.getCurrentIndex() + 1;

				if (RM.hasRights(i, self, previousRow)) {
					return {node: previousRow, index: i};
				} else {
					while (nextRow.isRoot || nextRow.parent.isRoot ||
					runner.data.item_id != nextRow.parent.data.item_id) {
						if (RM.hasRights(i, self, runner)) {
							return {node: runner, index: i};
						}
						if (runner.isRoot) {
							return;
						}
						i--;
						runner = runner.parent;
					}
				}
			};

			RM.applyChanges = function (itemId) {
				var DI = self.itemSet[itemId];
				if (DI.RM.id != RM.id) {
					self.RMsets[DI.RM.id].deleteRow(itemId);
					RM.addRow(DI.data);
					reDraw(DI);
				}
				var oldParent = DI.parent;
				RM.setRowParent(DI);
				if (!oldParent.isRoot) {
					oldParent.setDataFromChildren().updateFields().setDataChanged();
				}
				RM.drawBar();
				DI.setDataFromChildren().setDataChanged();


				if (!DI.parent.isRoot) {
					DI.parent.highlightLong();
				}
				if (DI.parent.isRoot || DI.parent.isOpen) {
					if (DI.wasOpen) {
						DI.open(false, true);
						RM.setCalendarHeight();
					}
				} else {
					DI.calendarRow.hide();
					DI.hide();
				}
				DI.calendarRow.updateArrows();
				DI.unSelectParents();
				RM.setCalendarHeight();
			};

			var getId = function (row) {
				if (typeof row === 'undefined' || row.isRoot) {
					return 'root';
				}

				return row.data.item_id;
			};

			var lastIdentifier = '';

			var findLegalNodes = function (self, previousRow, nextRow) {
				var currentIdentifier = getId(self) + '_' + getId(previousRow) + '_' + getId(nextRow);
				if (lastIdentifier == currentIdentifier) {
					return false;
				}

				lastIdentifier = currentIdentifier;
				legalNodes = {};

				var maxParent = getId(nextRow.parent);
				var runner = {parent: previousRow};
				var i = previousRow.getCurrentIndex() + 1;
				do {
					runner = runner.parent;
					if (RM.hasRights(i, self, runner)) {
						legalNodes[i] = runner;
					}
					i--;
				} while (getId(runner) != maxParent);
			};

			RM.getLegalNode = function (position) {
				var offset = 1;
				var max = 50;
				var node = legalNodes[position];

				if (!node) {
					var newIndex;
					var n;

					while (true) {
						newIndex = position - offset;
						n = legalNodes[newIndex];

						if (n) {
							return {node: n, index: newIndex};
						}

						newIndex = position + offset;
						n = legalNodes[newIndex];

						if (n) {
							return {node: n, index: newIndex};
						}

						if (offset > max) {
							return;
						}

						offset++;
					}
				}

				return {node: node, index: position};
			};

			RM.getRelativeNode = function (previousRow, nextRow, relativeIndex) {
				if (previousRow.isRoot) {
					return {index: 1, node: previousRow};
				}

				var runner = previousRow;
				var i = relativeIndex;

				if (relativeIndex < 0) {
					runner = runner.parent;

					while (typeof runner.parent !== 'undefined' && nextRow.parent != runner && relativeIndex++) {
						runner = runner.parent;
					}

					i += relativeIndex;
				} else if (relativeIndex > 0) {
					if (previousRow.isOpen) {
						while (typeof runner.lastEle !== 'undefined' && nextRow.parent != runner && --relativeIndex) {
							runner = runner.lastEle;
						}
						i -= relativeIndex;
					} else {
						runner = previousRow;
						i = 0;
					}
				} else {
					if (previousRow.isRoot) {
						runner = previousRow;
					} else {
						runner = previousRow.parent;
					}
					i = runner.getCurrentIndex() + 1;
				}
				return {index: i, node: runner};
			};

			RM.hasRights = function (index, self, parent) {
				var selfType = self.data.task_type;
				if ((typeof parent === 'undefined' || parent.isRoot) && index == 1) {
					if (typeof rules.parents !== 'undefined') {
						var p = rules.parents[selfType];
						if (typeof p !== 'undefined') {
							for (var i = 0, l = p.length; i < l; i++) {
								if (p[i] == 'root') {
									return true;
								}
							}
						}
					} else {
						return true;
					}

					return false;
				}

				var parentType = parent.data.task_type;

				if (typeof rules.levels !== 'undefined') {
					var rule = rules.levels[selfType];

					if (typeof rule !== 'undefined') {
						var f = false;

						for (var i = 0, l = rule.length; i < l; i++) {
							if (rule[i] == index) {
								f = true;
								break;
							}
						}

						if (!f) {
							return false;
						}
					}
				}

				if (typeof rules.parents !== 'undefined') {
					var p = rules.parents[selfType];

					if (typeof p !== 'undefined') {
						var f = false;

						for (var i = 0, l = p.length; i < l; i++) {
							if (p[i] == parentType) {
								f = true;
								break;
							}
						}

						if (!f) {
							return false;
						}
					} else {
						return false;
					}
				}

				return true;
			};
			RM.addButtonsOnEmpty = function ($empty, rules, options) {
				var btnHtml = '';
				var $btnArea = $empty.find('.buttonArea');

				angular.forEach(rules.parents, function (value, key) {

					for (var i = 0, l = value.length; i < l; i++) {
						if (value[i] === 'root') {
							var alias = options.alias[key];

							if (typeof alias === 'undefined') {
								alias = Translator.translate('RM_ITEM');
							}
							btnHtml += ', <span class="addNewItem" itemType="' + self.sanitize(key) + '">' + self.sanitize(alias) + '</span>';
						}
					}
				});
				$empty.on('mouseup', '.addNewItem', function (e) {
					RM.addNewItem($(e.target).attr('itemType'));
				});
				var btnLen = btnHtml.length;
				if (btnLen > 2) btnHtml = btnHtml.substr(2, btnLen);
				$btnArea.append(btnHtml);
			};

			RM.getLegalChildren = function (DI) {
				var returnVals = [];
				var targetIndex = DI.getCurrentIndex() + 1;

				$.each(rules.parents, function (taskType, parentArray) {
					var abort = false;

					for (var i = 0, cc = parentArray.length; i < cc && !abort; i++) {
						if (parentArray[i] == DI.data.task_type) {
							if (typeof rules.levels !== 'undefined') {

								var cj = rules.levels[taskType].length;
								for (var j = 0; j < cj && !abort; j++) {
									if (rules.levels[taskType][j] == targetIndex) {
										returnVals.push(taskType);
										abort = true;
									}
								}
							} else {
								returnVals.push(taskType);
								abort = true;
							}
						}
					}
				});

				return returnVals;
			};

			/*
			 rules = {
			 levels: {1:[2,3], 2:[1,2]},
			 parents: {1:[2], 2[2,"root"]}
			 }
			 // 2 can be under root(1) and on first level(2), task can be under first level and one below
			 // 1 can be under 2, 2 can be under root or 2

			 */
			RM.setRules = function (r) {
				rules = r;
			};

			return RM;
		};


		/*  ___       __       ______              ___  ____
		   / _ \___ _/ /____ _/  _/ /____ __ _    / _ \/  _/
		  / // / _ `/ __/ _ `// // __/ -_)  ' \  / // // /  
		 /____/\_,_/\__/\_,_/___/\__/\__/_/_/_/ /____/___/

		 DI is used as a common item with all stages of the task machine.
		 Common item makes it possible to drag different elements between machines.
		 */

		self.itemSet = {};
		allDI = self.itemSet;

		self.getPortfolioSettings = function (portfolioId) {
			return PortfolioService.getActiveSettings(portfolioId);
		};

		self.savePortfolioSettings = function (portfolioId) {
			return PortfolioService.saveActiveSettings(portfolioId);
		};

		self.rowMenu = function (options, RM, overwriteFunction) {
			var RMenu = this;
			var initMenu;
			if (typeof overwriteFunction !== 'function') {
				var isTaskProcess = RM.options.process == 'task';
				initMenu = function (DI) {
					var items = [];
					if (isTaskProcess) {
						items.push({
							name: Translator.translate('GANTT_EDIT'), onClick: function () {
								self.openFeaturette(DI, RM.options.readOnly);
							}
						});
					}

					if (typeof options.menuItems[DI.data.task_type] !== 'undefined') {
						var customItems = options.menuItems[DI.data.task_type];
						for (var i = 0, cc = customItems.length; i < cc; i++) {
							if (typeof customItems[i].isActive === 'function') {
								if (customItems[i].isActive(DI.data.item_id)) {
									items.push({
										name: customItems[i].name,
										onClickArgument: DI.data.item_id,
										onClick: customItems[i].onClick
									});
								}
							} else if (!RM.options.readOnly) {
								items.push({
									name: customItems[i].name,
									onClickArgument: DI.data.item_id,
									onClick: customItems[i].onClick
								});
							}
						}
					}

					if (typeof options.menuItems.default !== 'undefined') {
						var customItems = options.menuItems.default;
						for (var i = 0, cc = customItems.length; i < cc; i++) {
							if (typeof customItems[i].isActive === 'function' &&
								customItems[i].isActive(DI.data.item_id)
							) {
								items.push({
									name: customItems[i].name,
									onClickArgument: DI.data.item_id,
									onClick: customItems[i].onClick
								});
							}
						}
					}

					if (!DI.editable()) {
						return items;
					}

					items.push({
						name: Translator.translate('GANTT_DELETE'), onClick: function () {
							MessageService.confirm('GANTT_DELETE_CONFIRM', function () {
								ProcessService.deleteItem(options.process, DI.data.item_id).then(function () {
									RM.deleteRow(DI.data.item_id);
									if (DI.isOpen) {
										DI.close();
									}

									DI.delete();
									RM.setCalendarHeight();
								});
							});
						}
					});

					if (self.clipBoard.check()) {
						items.push({
							name: Translator.translate('GANTT_PASTE_AFTER'), onClick: function () {
								self.clipBoard.paste(options, DI, RM, false);
							}
						});

						if (DI.isOpenable()) {
							items.push({
								name: Translator.translate('GANTT_PASTE_INTO'), onClick: function () {
									self.clipBoard.paste(options, DI, RM, true);
								}
							});
						}
					}
					if (options.split && DI.data.responsibles.length > 0 && DI.data.task_type == 1) {
						items.push({
							name: Translator.translate('GANTT_SPLIT'),
							onClick: function () {
								MessageService.confirm(Translator.translate('GANTT_SPLIT_CONFIRM'), function(){
									RM.addNewItem(2, DI.parent).then(function (phase) {
										phase.after(DI)
											.linkAfter(DI)
											.setOrderNo(Common.getOrderNoBetween);
										phase.calendarRow.after(DI);
										DI.data.linked_tasks = {};
										DI.data.parent_item_id = phase.data.item_id;

										DI.after(phase)
											.linkFirst(phase)
											.setOrderNo(Common.getOrderNoBetween)
											.setPadding(DI.getCurrentIndex())
											.initVisibility();

										if (DI.data.responsibles.length > 1) {
											var first = DI.data.responsibles[0];
											var i;
											var cc = DI.data.responsibles.length;
											var arrayOfData = new Array();
											var dataStorage = {};

											angular.forEach(DI.data.responsibles, function (responsible, i) {
												if (i > 0) {
													var newData = angular.copy(DI.data);
													newData.responsibles = [responsible];
													ProcessService.getLink('task', DI.data.item_id, RM.test, responsible).then(function (d) {
														// DI process_name, DI.data.item_id, DI responsible column id, responsible id
														newData.effort = d.effort;
													});
													dataStorage[responsible] = newData;
													arrayOfData.push({Data: newData});
												}
											});

											var innerGetLink = function (ele) {
												ProcessService.getLink('task', ele.data.item_id, RM.test, ele.data.responsibles[0]).then(function (d) {
													d.effort = dataStorage[ele.data.responsibles[0]].effort;

													ProcessService.setDataChanged(d.process, d.item_id);
												});
											}

											ProcessService.newItems(options.process, arrayOfData).then(function (newRows) {
												cc = newRows.length;
												for (i = 0; i < cc; i++) {
													RM.initNew(newRows[i].Data);
												}

												for (i = cc - 1; i >= 0; i--) {
													var ele = self.itemSet[newRows[i].Data.item_id];
													innerGetLink(ele);
													ele.linkAfter(DI)
														.after(DI)
														.setOrderNo(Common.getOrderNoBetween)
														.setPadding(ele.getCurrentIndex())
														.initVisibility();
													ele.calendarRow.append();
													ele.calendarRow.after(DI);
													ele.setDataChanged();
												}
												phase.open();
												RM.setCalendarHeight();
											});
											DI.data.responsibles = [first];
										} else {
											phase.open();
											RM.setCalendarHeight();
										}

										DI.setDataChanged();
										phase.data.title = DI.data.title;
										phase.setPadding();
										phase.setDataChanged();
									});
								})

							}
						});
					}

					var canBeAdded = RM.getLegalChildren(DI);
					for (var i = 0, cc = canBeAdded.length; i < cc; i++) {
						if (typeof options.alias[canBeAdded[i]] !== 'undefined') {
							//var s = options.alias[canBeAdded[i]].toLowerCase();
							//var translated = $translate.instant('RM_' + (s.charAt(0).toUpperCase() + s.slice(1)).toUpperCase());
							items.push({
								name: '+' + options.alias[canBeAdded[i]],
								onClickArgument: canBeAdded[i],
								onClick: function (type) {
									RMenu.addNewItem(type, DI);
								}
							});
						}
					}

					return items;
				};
			} else {
				initMenu = overwriteFunction;
			}

			RMenu.addNewItem = function () {
			};
			RMenu.closeMenu = function (DI) {
				if (typeof DI !== 'undefined') {
					if (typeof DI.$menu !== 'undefined') {
						DI.$menu.css({'width': DI.lastInsertWidth, opacity: 0});

						setTimeout(function () {
							DI.setPadding(DI.getCurrentIndex());

							if (typeof DI.$menu !== 'undefined') {
								DI.$menu.remove();
								DI.$menu = undefined;
							}
						}, 225);
					}
				}
			};

			RM.closeMenu = RMenu.closeMenu;

			RMenu.hideMenu = function (DI) {
				if (typeof DI.$menu !== 'undefined') {
					DI.$menu.remove();
					DI.$menu = undefined;
				}
			};

			RMenu.openMenu = function (DI) {
				DI.$menu = $('<div class=\'inlineMenu\'></div>');

				var items = initMenu(DI);

				//No menu items
				if (items.length == 0) return false;

				//If only one open item -- open immediately
				if (items.length === 1 && Translator.translate('OPEN') == items[0].name && typeof items[0].onClick !== 'undefined') {
					if (typeof items[0].onClickArgument !== 'undefined') {
						items[0].onClick(items[0].onClickArgument);
					} else {
						items[0].onClick();
					}
					DI.clearHighlights();
					DI.highlightPermanent();
					DI.$menu = undefined;
					return false;
				}

				for (var i = 0, cc = items.length; i < cc; i++) {
					var $ele;

					if (typeof items[i].html !== 'undefined') {
						$ele = $($sanitize(items[i].html)).appendTo(DI.$menu);
					} else {
						$ele = $('<a class="caption pointer">' + self.sanitize(items[i].name) + '</a>').appendTo(DI.$menu);
					}

					if (typeof items[i].disabled !== 'undefined') {
						$ele.addClass('disabled');
					}
				}

				DI.$menu.on('click.menuElement', '*', function (e) {
					var index = $(this).index();
					if (typeof items[index].onClick === 'undefined' ||
						typeof items[$(this).index()].disabled !== 'undefined') {
						RMenu.closeMenu(DI);

						return false;
					}

					e.stopPropagation();

					var onClickArgument = items[index].onClickArgument;

					if (typeof onClickArgument !== 'undefined') {
						items[index].onClick(onClickArgument, e);
					} else {
						items[index].onClick(e);
					}

					if (Translator.translate('OPEN') == items[index].name) {
						DI.clearHighlights();
						DI.highlightPermanent();
					}

					RMenu.closeMenu(DI);
				});

				DI.$opener.before(DI.$menu);
				var initialW = DI.$menu.width();
				DI.lastInsertWidth = DI.getLeftInsertWidth();
				DI.$menu.css({'width': DI.lastInsertWidth});

				setTimeout(function () {
					if (typeof DI.$menu !== 'undefined') {
						DI.$menu.css({'width': initialW + 5, 'opacity': 1});
					}
				}, 0);
			};

			RMenu.toggle = function (DI) {
				if (typeof DI.$menu !== 'undefined') { // close menu
					RMenu.closeMenu(DI);
				} else { // open menu
					RMenu.openMenu(DI);
				}
			};

			return RMenu;
		};

		self.clickHandler = function (o) {
			var CH = {};
			var secondClick;
			var longClick;
			var actionTriggered = false;
			var rowId;
			var DI;
			var lastDI = undefined;

			if (typeof o.options.headerClick !== 'undefined') {
				o.$element.on('click', 'th div.data-column', function (event) {
					o.options.headerClick(event.currentTarget.classList[0]);
				});
			}

			o.$element.on('mousedown', '.avatar', function (event) {
				Common.stopEvents(event);
			});

			o.$element.on('mouseup', '.avatar', function (event) {
				Common.stopEvents(event);
				UserService.card(event);
			});
			o.RM.clearClicks = function () {
				clearTimeout(longClick);
				clearTimeout(secondClick);
				secondClick = undefined;
			};
			o.$element.on('mouseup', 'tr.empty .addNewRow', o.RM.addNewRow);
			o.$element.on('mousedown', 'tr.rmTarget', function (e) {
				if (e.which != 1) {
					return false;
				}
				actionTriggered = false;
				rowId = $(e.currentTarget).attr('rowid');
				DI = self.itemSet[rowId];

				if (o.RM.selectMode && !o.options.readOnly) {
					DI.select();
				} else {
					longClick = setTimeout(function () {
						if (!self.rmDragActive && o.options.selectable && !o.options.readOnly && DI && DI.editable()) {
							o.RM.setSelectMode(true);
							DI.select();
							actionTriggered = true;
						}
					}, 750);
				}
			});

			let upFn = function (target) {
				if($(target).closest(".openAll").length > 0) {
					o.RM.openAll();
					return false;
				}
				if (typeof lastDI !== 'undefined') {
					o.menu.closeMenu(lastDI);
					if (lastDI !== DI) {
						clearTimeout(secondClick);
						secondClick = undefined;
					}
					lastDI = undefined;
				}
				if (self.rmDragActive) {
					DI = undefined;
					return false;
				}
				if (typeof DI !== 'undefined') {
					clearTimeout(longClick);
					Common.removeSelection();

					if (!o.RM.selectMode) {
						$.each(o.options.buttons, function (key, onClick) { // looping through the options.buttons and checks is any of those was clicked
							if ($(target).closest('.' + key).length > 0) {
								onClick(rowId);
								actionTriggered = true;
							}
						});

						if (!actionTriggered && typeof o.options.rowClick === 'function') { // row click set, this overrides other options
							o.options.rowClick(rowId);
							actionTriggered = true;
						}

						if (!actionTriggered) {
							if (typeof secondClick !== 'undefined') { // dblClick, no other actions triggered
								DI.clearHighlights();
								DI.highlightPermanent();
								clearTimeout(secondClick);
								secondClick = undefined;
								if (typeof o.options.rowDblClick === 'function') {
									o.options.rowDblClick(rowId);
								} else {
									self.openFeaturette(DI, o.RM.options.readOnly);
								}

								o.menu.hideMenu(DI);
							} else {
								o.menu.toggle(DI);
								secondClick = setTimeout(function () {
									secondClick = undefined;
								}, 300);
							}
						}
					}
					lastDI = DI;
					DI = undefined;
				}
			};

			$(document).on('keyup', function (e) {
				if (e.keyCode == 13) {
					let toId = 0;
					if (e.target && e.target.attributes.rowid) {
						toId = e.target.attributes.rowid.value;
					} else if ($(e.target)[0] && $(e.target)[0].attributes["rowid"] && $(e.target)[0].attributes["rowid"].nodeValue) {
						toId = $(e.target)[0].attributes["rowid"].nodeValue;
					}

					if (toId > 0 && allDI[toId] && o.options.process && allDI[toId].data.process == o.options.process) {
						o.options.rowClick(toId, o.options.process);
						actionTriggered = true;
					}
				}
			});
			// $(document).off('mouseup.upFn');		
			o.RM.$ele.on('mouseup.upFn', function (e) {
				upFn(e.target);
			});

			return CH;
		};

		self.sendChanges = function () {
			if (statusChanges.length > 0) {
				var tasks = {};
				tasks.status = statusChanges;

				Tasks.new('', tasks);
				statusChanges = [];
				statusChangeMapping = {};
			}
		};

		SaveService.subscribeSave($rootScope, self.sendChanges);

		var isChildOf = function (ele, targetParentId) {
			if (typeof ele === 'undefined') {
				return false;
			}

			if (ele.parent_item_id == targetParentId) {
				return true;
			}

			return isChildOf(self.itemSet[ele.parent_item_id].data, targetParentId);
		};

		self.setStatusChanged = function (data) {
			if (typeof statusChangeMapping[data.status_id] === 'undefined') {
				statusChangeMapping[data.status_id] = statusChanges.length;
				statusChanges.push(data);
			} else {
				statusChanges[statusChangeMapping[data.status_id]] = data;
			}

			SaveService.tick();
		};

		self.deleteTask = function (id, success) {
			ProcessService.deleteItem('task', id).then(success);
		};

		self.removeKanbanPhase = function (id) {
			return Tasks.new(['delStatus', id]);
		};

		self.newKanbanPhase = function (row, order = 0) {
			row.creationOrder = order;
			return Tasks.new('newStatus', row);
		};
		/* ________      __                    __
		  / ___/ (_)__  / /  ___  ___ ________/ /
		 / /__/ / / _ \/ _ \/ _ \/ _ `/ __/ _  /
		 \___/_/_/ .__/_.__/\___/\_,_/_/  \_,_/
				/*/
		self.clipBoard = new function () {
			var CB = this;
			var board = [];
			var boardMap = {};

			var action;
			var removeCutted = function () {
				//FIX IT
				$('.rowmanager-body tr.cutted').removeClass('cutted');
			};

			CB.cut = function (selectedRows) {
				removeCutted();
				action = 'cut';
				board = new Array();
				boardMap = {};

				$.each(selectedRows, function (i, DI) {
					DI.$ele.addClass('cutted');
					if (typeof selectedRows[DI.data.parent_item_id] === 'undefined') {
						board.push(DI);
						boardMap[i] = DI;
					}
				});
			};
			CB.copy = function (selectedRows) {
				removeCutted();
				action = 'copy';
				board = new Array();
				boardMap = {};

				$.each(selectedRows, function (i, DI) {
					boardMap[i] = DI;
					board.push(DI);
				});

			};
			CB.del = function (RM) {
				removeCutted();

				var deleteItems = new Array();
				var parents = new Array();
				var outsideParentIds = new Array();
				$.each(RM.selectedRows, function (i, DI) {
					deleteItems.push(Number(i));
					if (typeof RM.selectedRows[DI.data.parent_item_id] === 'undefined') {
						parents.push(DI);
						outsideParentIds.push(DI.data.parent_item_id);
					}
				});

				ProcessService.deleteItems(RM.options.process, deleteItems).then(function () {
					var cc = parents.length;
					for (var i = 0; i < cc; i++) {
						var DI = parents[i];
						if (DI.isOpen) {
							DI.close();
						}
						DI.delete();
					}

					board = new Array();
					boardMap = {};
					$.each(outsideParentIds, function (i, id) {
						if (self.itemSet && self.itemSet[id]) self.itemSet[id].setPadding();
					});
				});
				RM.calendarReDraw();


			};

			CB.checkCutted = function (DI) {
				if (action != 'cut') {
					return false;
				}

				return typeof boardMap[DI.data.item_id] !== 'undefined';
			};

			CB.check = function () {
				return board.length > 0;
			};

			// Sorts the clipBoard, 
			// first generate OrderStack creates an paramenter DI.orderStack, using recursiveOrderStack function
			// then sortBoard can be used to sort the clipBoard with ordernumbers of whole tree

			var sortBoard = function (a, b) {
				var i = 0;
				while (a.orderStack[i] == b.orderStack[i]) {
					if (a.orderStack.length > i && b.orderStack.length > i) {
						i++;
					} else {
						return 0;
					}
				}
				if (a.orderStack[i] > b.orderStack[i]) {
					return 1
				}
				return -1;
			};

			var recursiveOrderStack = function (stack, DI) {
				if (typeof DI.parent !== 'undefined' && !DI.parent.isRoot) {
					recursiveOrderStack(stack, DI.parent);
				}
				stack.push(DI.data.order_no);
			};

			var generateOrderStack = function (DI) {
				DI.orderStack = new Array();
				recursiveOrderStack(DI.orderStack, DI);
			};

			var initPaste = function () {
				var i;
				var cc = board.length;

				for (i = 0; i < cc; i++) {
					generateOrderStack(board[i]);
				}

				board.sort(sortBoard);
			};

			CB.paste = function (options, DI, RM, pasteInto) {
				if (options.readOnly) {
					return false;
				}
				var i;
				var cc = board.length;

				$rootScope.$emit('RMPasteStart');

				initPaste();
				if (pasteInto) {
					if (!DI.isOpen) {
						DI.open();
					}
				} else {
					if (DI.isOpen) {
						DI.close();
					}
				}
				switch (action) {
					case 'cut':
						for (i = cc - 1; i >= 0; i--) {
							var ele = board[i];
							if (ele.isOpen) {
								ele.wasOpen = true;
								ele.close(false, true);
							}
							if (pasteInto) {
								ele.data.parent_item_id = DI.data.item_id;
								ele.linkFirst(DI);
							} else {
								ele.data.parent_item_id = DI.data.parent_item_id;
								ele.linkAfter(DI);
							}
							ele.data.owner_item_id = DI.data.owner_item_id;
							ele.data.owner_item_type = DI.data.owner_item_type;
							ele.calendarRow.append();
							ele.after(DI)
								.setOrderNo(Common.getOrderNoBetween)
								.setDataChanged()
								.setPadding(ele.getCurrentIndex())
								.initVisibility();
							ele.calendarRow.after(DI);
						}
						RM.calendarReDraw();
						$rootScope.$emit('RMPasteEnd');
						break;
					case 'copy':
						var arrayOfData = new Array();
						var parentsMap = {};
						for (i = 0; i < cc; i++) {
							if (typeof boardMap[board[i].data.parent_item_id] !== 'undefined') {
								parentsMap[i] = board[i].data.parent_item_id;
							}
							board[i].data.linked_tasks = {};
							board[i].data.rmCopied = true;
							var d = angular.copy(board[i].data);

							if (pasteInto) {
								d.parent_item_id = DI.data.item_id;
							} else {
								d.parent_item_id = DI.data.parent_item_id;
							}
							d.owner_item_id = DI.data.owner_item_id;
							d.owner_item_type = DI.data.owner_item_type;
							arrayOfData.push({Data: d});
						}

						$.each(parentsMap, function (j, id) {
							for (i = 0; i < cc; i++) {
								if (id == board[i].data.item_id) {
									parentsMap[j] = i;
								}
							}
						});
						ProcessService.newItems(options.process, arrayOfData).then(function (newRows) {
							for (i = 0; i < cc; i++) {
								RM.initNew(newRows[i].Data);
							}

							for (i = cc - 1; i >= 0; i--) {
								var ele = self.itemSet[newRows[i].Data.item_id];
								if (typeof parentsMap[i] !== 'undefined') {
									var parentPos = parentsMap[i];
									var parentId = newRows[parentPos].Data.item_id;
									var parentDI = self.itemSet[parentId];

									ele.data.parent_item_id = parentId;
									ele.linkFirst(parentDI);
								}
							}

							for (i = cc - 1; i >= 0; i--) {
								var ele = self.itemSet[newRows[i].Data.item_id];
								if (typeof parentsMap[i] === 'undefined') {
									if (pasteInto) {
										ele.linkFirst(DI);
									} else {
										ele.linkAfter(DI);
									}

									ele.data.title = ele.data.title + " " + Translator.translate('RM_COPY_INFO');

									ele.calendarRow.append();
									ele.after(DI)
										.setOrderNo(Common.getOrderNoBetween)
										.setPadding(ele.getCurrentIndex())
										.initVisibility();

									ele.calendarRow.after(DI);
								}
								ele.setDataChanged();
							}
							DI.setPadding();
							DI.open().setOpenIcon();
							RM.calendarReDraw();
							$rootScope.$emit('RMPasteEnd');
						});

						break;
					default:
						console.log('not implemented');
				}

				if (pasteInto) {
					DI.setDataFromChildren().updateFields().setDataChanged().setPadding();
				} else {
					if (typeof DI.parent !== 'undefined' && !DI.parent.isRoot) {
						DI.parent.setDataFromChildren().updateFields().setDataChanged().setPadding();
					}
				}

				action = '';
				removeCutted();
				board = new Array();
				boardMap = {};

			};
			return CB;
		};
	}
})();
