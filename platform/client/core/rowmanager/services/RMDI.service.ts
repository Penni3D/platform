(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.service('RMDIService', RMDIService);

	RMDIService.$inject = [
		'RMService',
		'ProcessService',
		'UserService'
	];
	function RMDIService(
		RMService,
		ProcessService,
		UserService
	) {
		var self = this;
		var CalendarRow = function() {
			return {
				draw: function () { },
				append: function () { },
				after: function () { },
				before: function () { },
				hide: function () { },
				show: function () { },
				updateArrows: function() {},
				delete:function() {}
			};
		};

		var subscribeChangeSet = {};

		var fn = function (itemId) {
			angular.forEach(RMService.itemSet, function(DI, rowId) {
				if (rowId.indexOf(itemId) > -1) {
					if (typeof DI.data === 'undefined') {
						DI.delete();
					} else {
						DI.setDataFromChildren().updateFields();
					}
				}
			});
		};

		self.bindSubscribeChanged = function (types, scope) {

			angular.forEach(types, function(type) {
				if (typeof subscribeChangeSet[type] === "undefined") {
					subscribeChangeSet[type] = [];
				}
				if (subscribeChangeSet[type].length == 0) {
					ProcessService.subscribeChanged(fn, type);
				}

				subscribeChangeSet[type].push(true);
			});

			scope.$on("$destroy", function () {
				angular.forEach(types, function(type) {
					subscribeChangeSet[type].pop();
					if (subscribeChangeSet[type].length == 0) {
						ProcessService.unsubscribeChanged(fn, type);
					}
				});
			});
		};

		self.baseDI = function () {
			var currentIndex;
			var DI = {};
			DI.initVisibility = function () {
				DI.RM.initVisibility(DI.$ele);
				return DI;
			};

			// initializes field for later use
			DI.addField = function (str) {
				var f = DI.RM.fieldCache[str].ItemColumn;
				DI.addFieldWithData(str, f);
			};

			// Returns value of field formatted as set globally
			DI.assignRowValues = function () {
				var dt = DI.data.task_type;
				var showName = !(dt == 21 || dt == 2 || dt == 1);

				$.each(DI.fields, function (i) {
					DI.fields[i].$ele = DI.$ele.find("div." + i);

					if (typeof DI.RM.fieldCache[i] !== 'undefined') {

						var val =  DI.data[i];
						RMService.displayFormat(DI.RM.fieldCache[i], val, showName, DI.data.process, DI.data.union_portfolio_id)
							.then(function (field) {

								DI.fields[i].$ele.html(field.html);
								if(typeof field.tooltip !=="undefined") {
									DI.fields[i].$ele.attr("tooltip", field.tooltip);
								}
							});
					}
				});
			};

			DI.editable = function() {
				if(DI.RM._readOnly) {return false;}
				if(DI.RM.options.readOnly) {return false;}

				switch(parseInt(DI.data.task_type)) {
					case 3:
						return false;
					/*
					case 1:
					case 2:
					case 21:
					case 50:
						if (typeof DI.data.has_write_right !== "undefined") {
							return DI.data.has_write_right;
						}
						*/
				}
				/*var allocation_status = parseInt(DI.data.allocation_status); 
				if(isNaN(allocation_status)) {allocation_status = 0 }
				
				if(allocation_status>10) {
					return false;
				}
				*/
				if (typeof DI.data.has_write_right !== "undefined") {
					return DI.data.has_write_right;
				}
				return false;
			};

			DI.isSelected = false;
			DI.inheritDates = false;
			DI.sumDatas = false;
			DI.selectionInherited = false;
			DI.isRoot = false;
			DI.myArrows = {};

			// DI.isDraggable = function () {
			// 	// just a placeholder, need to be overriden where we have scope
			// 	return true;
			// };

			DI.checkParent = function () {
				if (typeof DI.parent === "undefined") {
					return "Undefined, error, crash!!!";
				}
				if (DI.parent.isRoot) {
					return "parent is root";
				}
				DI.parent.$ele.css({ "background": "red" });
				setTimeout(function () {
					DI.parent.$ele.css({ "background": "", "transition": "background 1.5s ease-out" });
				}, 500);

				return "parent id is " + DI.parent.data.item_id;
			};

			DI.applyChanges = function () {
				var parent = RMService.itemSet[DI.data.parent_item_id];
				if (typeof parent === "undefined") {
					parent = rootParent;
				}

				var runner = parent.firstEle;
				var putAfter;

				while (typeof runner !== "undefined") {
					if (DI.data.order_no > runner.data.order_no) {
						putAfter = runner;
					} else {
						break;
					}
					runner = runner.next;
				}
				if (typeof putAfter === "undefined") {
					DI.linkFirst(parent);
				} else {
					DI.linkAfter(putAfter);
				}

				if (parent.isRoot || parent.isOpen) {
					DI.initVisibility();

					if (typeof DI.prev !== "undefined") {
						DI.prev.$ele.after(DI.$ele);
						DI.calendarRow.after(DI.prev);
					} else {
						parent.$ele.after(DI.$ele);
						DI.calendarRow.after(parent);
					}

					DI.setPadding(DI.getCurrentIndex());
				}
				return DI.setDataFromChildren().updateFields().setDataChanged();
			};

			DI.inParent = function (parent) {
				if (typeof parent === "undefined") {
					return DI;
				}
				DI.linkLast(parent);
				if (parent.isOpen) {
					if (typeof DI.prev !== "undefined") {
						var prev = DI.prev;
						while (prev.isOpen) {
							prev = prev.lastEle;
						}

						prev.$ele.after(DI.$ele);
						DI.calendarRow.after(prev);
					} else {
						parent.$ele.after(DI.$ele);
						DI.calendarRow.after(parent);
					}
					DI.setPadding();
				}

				if (!parent.isRoot) {
					parent.setPadding();
				}

				return DI;
			};

			DI.clearHighlights = function() {
				$(".highlight-permanent").removeClass('highlight-permanent');
			};

			DI.highlightPermanent = function() {
				DI.$ele.addClass('highlight-permanent');
			};

			DI.highlight = function () {
				DI.$ele.addClass('highlight');

				setTimeout(function () {
					DI.$ele.removeClass('highlight');
				}, 600);
			};

			DI.highlightLong = function () {
				DI.$ele.addClass('highlight-long');
				setTimeout(function () {
					DI.$ele.removeClass('highlight-long');
				}, 800);
			};

			DI.getLeftInsertWidth = function () {
				return (currentIndex - 1) * 24;
			};
			DI.showHighlight = function() {};
			DI.setPadding = function (index) {
				if (typeof index !== "undefined") {
					currentIndex = index;
				}

				DI.$opener.css({ 'margin-left': DI.getLeftInsertWidth() });

				if (DI.isOpenable() && typeof DI.firstEle !== "undefined") {
					DI.$opener.find("i").css("display", "block");
					DI.RM.addOpenable(DI);
				} else {
					DI.$opener.find("i").css("display", "none");
					DI.RM.removeOpenable(DI);
				}

				if (RMService.clipBoard.checkCutted(DI)) {
					DI.$ele.addClass("cutted");
				} else {
					DI.$ele.removeClass("cutted");
				}

				return DI.hideDates();
			};

			var wasSprintItem;
			DI.hideDates = function () {
				var sprintItem = false;
				if (typeof DI.parent !== 'undefined' && !DI.parent.isRoot && DI.parent.data.task_type == 21) {
					sprintItem = true;
				}

				if (wasSprintItem === sprintItem) {
					return DI;
				}
				wasSprintItem = sprintItem;
				$.each(DI.fields, function (i) {
					var displayAs = DI.fields[i].showType;
					if(displayAs ==="noType") {displayAs="";}

					if (sprintItem && (DI.fields[i].type == 3 || DI.fields[i].type == 5)) { // parent is sprint and type is date(3)
						DI.fields[i].$ele.css("display","none");
					} else {
						DI.fields[i].$ele.css("display",displayAs);
					}
				});

				return DI;
			};

			DI.isOpenable = function () {
				if (typeof DI.firstEle === 'undefined') {
					return false;
				}

				switch (Number(DI.data.task_type)) {
					case 1:
					case 2:
					case 21:
					case 51:
					case 52:
						return true;
				}

				if (typeof DI.data.task_type === 'undefined') {
					return true;
				}

				return false;
			};

			DI.loadWasOpen = function (portfolioId) {
				DI.wasOpen = false;
				var rowData = RMService.getPortfolioSettings(portfolioId).rows[DI.data.item_id];
				if (typeof rowData !== "undefined") {
					DI.wasOpen = rowData.wasOpen;
				}
			};

			DI.open = function (transite, ignoreChanges) {
				if (!DI.isOpenable()) {
					return DI;
				}

				var i = DI.getCurrentIndex() + 1;
				DI.isOpen = true;

				if (transite) {
					DI.highlight();
				}

				if (!ignoreChanges) {
					DI.wasOpen = true;
					DI.saveWasOpen();
				}

				var runner = DI.firstEle;
				var hook = DI;
				var delayOpen = [];

				while (typeof runner !== "undefined") {
					runner.setPadding(i);

					if (runner.RM.id !== DI.RM.id) {
						runner = RM.init(runner);
					}

					runner.initVisibility();
					hook.$ele.after(runner.$ele);
					runner.calendarRow.after(hook, true);

					if (runner.wasOpen) {
						delayOpen.push(runner);
					}

					hook = runner;
					runner = runner.next;
				}
				for (var i = 0; i < delayOpen.length; i++) {
					delayOpen[i].open(false, true);
				}
				DI.calendarRow.updateArrows();
				return DI.setOpenIcon();
			};

			DI.close = function (transite, ignoreChanges) {
				if(!DI.isOpen) {
					return DI;
				}
				DI.isOpen = false;

				if (!DI.isOpenable()) {
					return DI;
				}

				if (!ignoreChanges) {
					DI.wasOpen = false;
					DI.saveWasOpen();
				}

				if (transite) {
					DI.highlight();
				}

				var runner = DI.firstEle;

				while (typeof runner !== "undefined") {
					if (runner.isOpen) {
						runner.close(transite, true);
					}

					runner.hide();
					runner = runner.next;
				}
				DI.calendarRow.updateArrows();
				return DI.setOpenIcon();
			};


			DI.calendarRow = new CalendarRow();
			/* ___   ____    ___ _       __    __   
			  / _ \ /  _/   / _/(_)___  / /___/ /___
			 / // /_/ / _  / _// // -_)/ // _  /(_-<
			/____//___/(_)/_/ /_/ \__//_/ \_,_//___/
			Fields is used to handle datatypes and formating of datafield
			*/
			DI.fields = {};
			DI.addFieldWithData = function (str, f) {
				DI.fields[str] = {
					type: f.DataType,
					process: f.DataAdditional,
					showType:f.showType
				};
			};
			// Used for edit dialog, return standard input element array used
			// loops all datafields and if it is found from the fields, push input element in return array
			DI.getContentInputs = function () {
				var inputs = [];
				var contains = {};

				$.each(data, function (i) {
					if (typeof contains[i] === "undefined") {
						contains[i] = true;
						if (typeof DI.fields[i] !== "undefined") {
							switch (DI.fields[i].type) {
								case 3:
									inputs.push({
										type: "date",
										label: i,
										model: i,
										modify: true
									});
									break;
								default:
									inputs.push({
										type: "string",
										label: i,
										model: i
									});
							}
						}
					}
				});
				return inputs;
			};

			// updates data in DI by looping direct children and counting sums and adjusting dates to match min and max values
			DI.setDataFromChildren = function (ignoreList) {
				if(typeof RMService.alias[12] != "undefined") {
					var i =0;
					var cc=RMService.alias[12].length;
					for (i = 0; i < cc; i++) {
						var name = RMService.alias[12][i];
						var nameSum = RMService.alias[12][i]+"_sum";
						DI.data[nameSum] = parseFloat(DI.data[name]);
					}
				}
				if(typeof ignoreList !=="undefined") {
					if(ignoreList.indexOf(DI.data.item_id)>-1) return DI;
					ignoreList.push(DI.data.item_id);
				};
				if (!DI.inheritDates && !DI.sumDatas) return DI;

				if (typeof DI.firstEle !== "undefined") {
					if(typeof RMService.alias[6] != "undefined") {// sumUp
						var i =0;
						var cc=RMService.alias[6].length;
						for (i = 0; i < cc; i++) {
							var name = RMService.alias[6][i];
							var runner = DI.firstEle;
							if (DI.data.task_type != 21) { //Sprint is different to a phase --BSa 29.10.2019
								DI.data[name] = 0;
								while (typeof runner !== "undefined") {
									var num = parseFloat(runner.data[name]);
									if (isNaN(num)) num = 0;
									DI.data[name] += num;
									runner = runner.next;
								}
							}
						}
					}
					if(typeof RMService.alias[12] != "undefined") {// sumUp
						var i =0;
						var cc=RMService.alias[12].length;
						for (i = 0; i < cc; i++) {
							var name = RMService.alias[12][i];
							var nameSum = RMService.alias[12][i]+"_sum";
							var runner = DI.firstEle;
							if (DI.data.task_type != 21) { //Sprint is different to a phase --BSa 29.10.2019
								DI.data[nameSum] = parseFloat(DI.data[name]);
								while (typeof runner !== "undefined") {
									var num = parseFloat(runner.data[nameSum]);
									if (isNaN(num)) num = 0;
									DI.data[nameSum] += num;
									runner = runner.next;
								}
							}
						}
					}
					if( typeof RMService.alias[9] != "undefined") {// avg
						var i =0;
						var cc=RMService.alias[9].length;
						for (i = 0; i < cc; i++) {
							var name = RMService.alias[9][i];
							var runner = DI.firstEle;
							var c= 0;
							var val = 0;
							while (typeof runner !== "undefined") {
								var num = parseFloat(runner.data[name]);
								if (!isNaN(num)) {
									val += num;
									c++;
								}
								runner = runner.next;
							}
							DI.data[name] = Math.round(val/c);
						}
					}
					if( typeof RMService.alias[10] != "undefined") {//min
						var i =0;
						var cc=RMService.alias[10].length;
						for (i = 0; i < cc; i++) {
							var name = RMService.alias[10][i];
							var runner = DI.firstEle;
							var min;
							while (typeof runner !== "undefined") {
								var num = parseFloat(runner.data[name]);
								if(!isNaN(num)) {
									if(isNaN(min)) {
										min = num;
									} else {
										if(num<min) min = num;
									}
								}
								runner = runner.next;
							}
							DI.data[name] = min;
						}
					}
					if( typeof RMService.alias[11] != "undefined") {//max
						var i =0;
						var cc=RMService.alias[11].length;
						for (i = 0; i < cc; i++) {
							var name = RMService.alias[11][i];
							var runner = DI.firstEle;
							var max;
							while (typeof runner !== "undefined") {
								var num = parseFloat(runner.data[name]);
								if (isNaN(num)) num = 0;
								if(isNaN(max)) {
									max = num;
								} else {
									if(num>max) max = num;
								}
								runner = runner.next;
							}
							DI.data[name] = max;
						}
					}
					if (DI.data.task_type !== 21 && DI.inheritDates) {
						DI.data[RMService.alias[3]] = new Date(DI.firstEle.data[RMService.alias[3]]);
						DI.data[RMService.alias[4]] = new Date(DI.firstEle.data[RMService.alias[4]]);
						var runner = DI.firstEle.next;

						while (typeof runner !== "undefined") {
							var sd = new Date(runner.data[RMService.alias[3]]);
							var ed = new Date(runner.data[RMService.alias[4]]);

							if (sd < DI.data[RMService.alias[3]]) {
								DI.data[RMService.alias[3]] = sd;
							}

							if (ed > DI.data[RMService.alias[4]]) {
								DI.data[RMService.alias[4]] = ed;
							}

							runner = runner.next;
						}
					}
				}
				if (typeof DI.parent !== "undefined" && !DI.parent.isRoot) {
					DI.parent.setDataFromChildren(ignoreList).setDataChanged().updateFields();
				}
				DI.showHighlight();
				return DI;
			};

			// Cache for start and end dates
			var oldSd;
			var oldEd;

			// Updates row with data with values from the DI.data according to DI.fields
			DI.updateFields = function (ignoreCalendar) {
				var i =0;
				if (typeof RMService.alias[6] !=="undefined") {
					var cc=RMService.alias[6].length;
					for (i=0;i<cc;i++) {
						var name = RMService.alias[6][i];
						var num = parseFloat(DI.data[name]);
						if (isNaN(num)) {
							num = 0;
						}
						DI.data[name] = num;
					}
				}
				if (typeof RMService.alias[12] !=="undefined") {
					var cc=RMService.alias[12].length;
					for (i=0;i<cc;i++) {
						var name = RMService.alias[12][i];
						var num = parseFloat(DI.data[name]);
						if (isNaN(num)) {
							num = 0;
						}
						DI.data[name] = num;
					}
				}
				if (DI.data[RMService.alias[3]] !== null && DI.data[RMService.alias[4]] !== null) {
					if (new Date(DI.data[RMService.alias[3]]) > new Date(DI.data[RMService.alias[4]])) {
						DI.data[RMService.alias[3]] = oldSd;
						DI.data[RMService.alias[4]] = oldEd;
						// if(oldEd === DI.data[RMService.alias[4]]) { // end date is unchanged
						// 	var d = new Date(DI.data[RMService.alias[4]]);
						// 	d = d.setDate(d.getDate()-1);
						// 	DI.data[RMService.alias[3]] = d;
						// } else {
						// 	var d = new Date(DI.data[RMService.alias[3]]);
						// 	d = d.setDate(d.getDate()+1);
						// 	DI.data[RMService.alias[4]] = d;
						// }
						// var d = DI.data[RMService.alias[3]];
						// DI.data[RMService.alias[3]] = DI.data[RMService.alias[4]];
						// DI.data[RMService.alias[4]] = d;
						// if(!DI.RM.options.readOnly) { DI.setDataChanged(); }
						DI.setDataChanged();
					} else {
						oldSd = DI.data[RMService.alias[3]];
						oldEd = DI.data[RMService.alias[4]];
					}
				}


				var dt = DI.data.task_type;
				var showName = !(dt == 21 || dt == 2 || dt == 1);
				angular.forEach(DI.fields, function (v, i) {

					if (typeof v !== "undefined") {
						if((typeof RMService.alias[12] !=="undefined") && (RMService.alias[12].indexOf(i)>-1)) {
							RMService.displayFormat(DI.RM.fieldCache[i], DI.data[i+"_sum"], showName, DI.data.process, DI.data.union_portfolio_id)
								.then(function (field) {
									DI.fields[i].$ele.html(field.html);
									if(typeof field.tooltip !=="undefined") {
										DI.fields[i].$ele.attr("tooltip", field.tooltip);
									}
								});

							// var cc=RMService.alias[6].length;
							// for(var j=0;j<cc;j++) {
							// 	var name = RMService.alias[6][j];								
							// 	RMService.displayFormat(DI.RM.fieldCache[name], DI.data[name+"_sum"], showName)
							// 		.then(function (field) {
							// 			DI.fields[name].$ele.html(field.html);
							// 			if(typeof field.tooltip !=="undefined") {
							// 				DI.fields[name].$ele.attr("tooltip", field.tooltip);
							// 			}
							// 		});
							// }
						} else {
							RMService.displayFormat(DI.RM.fieldCache[i], DI.data[i], showName, DI.data.process, DI.data.union_portfolio_id)
								.then(function (field) {
									DI.fields[i].$ele.html(field.html);
									if(typeof field.tooltip !=="undefined") {
										DI.fields[i].$ele.attr("tooltip", field.tooltip);
									}
								});
						}
					}
				});

				if (!ignoreCalendar && typeof DI.calendarRow !== "undefined") DI.calendarRow.draw();

				DI.setUserIcons();
				return DI;
			};

			// TODO:To improve performance, 
			// add identifier into DI object that equals to allDI associative array id and never changes (outside .data property)
			// then always find items with this identifier instead of looping through all objects
			function getConstructedItemId(DI) {
				let result = "";
				_.each(allDI, function(item, id){
					if (item === DI) {
						result = id;
						return;
					}
				});
				return result;
			}

			// Sends tick that DI has been changed
			DI.setDataChanged = function () {
				let itemId = getConstructedItemId(DI);

				if (typeof DI.data.process === 'undefined' || DI.data.process.length == 0) {
					ProcessService.setDataChanged("task", itemId);
				} else {
					ProcessService.setDataChanged(DI.data.process, itemId);
				}

				return DI;
			};

			DI.hideInactiveOpener = function () {
				return DI;
			};

			DI.prevRow = function () {
				var runner = DI;
				while (runner.isOpen && typeof runner.lastEle !== "undefined") {
					runner = runner.lastEle;
				}

				return runner;
			};

			DI.nextRow = function () {
				var runner = DI;
				if (runner.isOpen && typeof runner.firstEle !== 'undefined') {
					return runner.firstEle;
				}

				while (typeof runner.next === 'undefined' && !runner.parent.isRoot) {
					runner = runner.parent;
				}

				return typeof runner.next !== 'undefined' ? runner.next : runner.parent;
			};

			DI.getCurrentIndex = function () {
				var runner = DI;
				var index = 0;

				while (typeof runner !== 'undefined' && !runner.isRoot) {
					index++;
					runner = runner.parent;

					if (index > 100) {
						break;
					}
				}

				return index;
			};

			// Visibility controls working instant, no animations
			DI.show = function () {
				DI.calendarRow.show();
				return DI;
			};

			DI.hide = function () {
				if (typeof DI.$ele !== 'undefined') {
					DI.$ele.remove();
				}

				DI.calendarRow.hide();
				return DI;
			};

			// removes links from this item 
			DI.unlink = function () {
				if (typeof DI.parent !== "undefined") {
					if (typeof DI.parent.lastEle !== "undefined") {
						if (DI.parent.lastEle.data.item_id == DI.data.item_id) {
							DI.parent.lastEle = DI.prev;
						}
					}

					if (typeof DI.parent.firstEle !== "undefined") {
						if (DI.parent.firstEle.data.item_id == DI.data.item_id) {
							DI.parent.firstEle = DI.next;
						}
					}

					DI.parent = undefined;
				}

				if (typeof DI.prev !== "undefined") {
					DI.prev.next = DI.next;
				}

				if (typeof DI.next !== "undefined") {
					DI.next.prev = DI.prev;
				}

				DI.next = undefined;
				DI.prev = undefined;

				return DI;
			};

			DI.after = function (target) {
				target.$ele.after(DI.$ele);
				return DI;
			};

			DI.before = function (target) {
				target.$ele.before(DI.$ele);

				return DI;
			};

			DI.setOrderNo = function (getOrderNoBetween) {
				var ord1;
				var ord2;

				if (typeof DI.prev !== "undefined") {
					ord1 = DI.prev.data.order_no;
				}

				if (typeof DI.next !== "undefined") {
					ord2 = DI.next.data.order_no;
				}

				DI.data.order_no = getOrderNoBetween(ord1, ord2);

				return DI;
			};

			DI.delete = function () {
				var parent = DI.parent;
				DI.isDeleted = true;
				DI.unlink();
				DI.calendarRow.delete();
				DI.hide();

				if (typeof parent !== 'undefined' && !parent.isRoot) {
					parent.hideInactiveOpener();
					parent.setDataFromChildren();
					parent.updateFields();
					parent.setDataChanged();
				}
				var RM = DI.RM;
				delete RMService.itemSet[DI.data.item_id];
				RM.emptyVisibility();
			};

			DI.linkAfter = function (ele) {
				DI.unlink();

				if (typeof ele.next !== "undefined") {
					ele.next.prev = DI;
					DI.next = ele.next;
				}

				DI.prev = ele;
				ele.next = DI;

				if (typeof ele.parent !== "undefined") {
					DI.parent = ele.parent;
					if (typeof ele.parent.lastEle !== "undefined") {
						if (ele.parent.lastEle == ele) {
							ele.parent.lastEle = DI;
						}
					}
				}

				return DI;
			};

			DI.linkBefore = function (ele) {
				DI.unlink();

				if (typeof ele.prev !== "undefined") {
					ele.prev.next = DI;
					DI.prev = ele.prev;
				}

				DI.next = ele;
				ele.prev = DI;

				if (typeof ele.parent !== "undefined") {
					DI.parent = ele.parent;
					if (typeof ele.parent.firstEle !== "undefined") {
						if (ele.parent.firstEle == ele) {
							ele.parent.firstEle = DI;
						}
					}
				}

				return DI;
			};

			DI.linkFirst = function (parent) {
				DI.unlink();
				DI.parent = parent;

				if (typeof parent.firstEle !== "undefined") {
					parent.firstEle.prev = DI;
					DI.next = parent.firstEle;
				} else {
					parent.lastEle = DI;
				}

				parent.firstEle = DI;

				return DI;
			};

			DI.linkLast = function (parent) {
				DI.unlink();
				DI.parent = parent;

				if (typeof parent.lastEle !== "undefined") {
					parent.lastEle.next = DI;
					DI.prev = parent.lastEle;
				} else {
					parent.firstEle = DI;
				}

				parent.lastEle = DI;

				return DI;
			};

			DI.setUserIcons = function () {
				if (typeof DI.$users !== "undefined" && DI.data.responsible_id) {
					UserService.getAvatars(DI.data.responsible_id, { limit: 2, size: 'smaller' })
						.then(function ($avatars) {
							DI.$users.html($avatars);
						});
				}
			};

			DI.setOpenIcon = function () {
				if (!DI.isOpen) {
					DI.$opener.addClass('flip');
				} else {
					DI.$opener.removeClass('flip');
				}
				DI.RM.updateOpenAll()
				return DI;
			};

			DI.append = function (ignoreTops) {
				DI.initVisibility();
				DI.RM.$emptyEle.before(DI.$ele);
				//DI.show();
				DI.calendarRow.append(ignoreTops);
				return DI;
			};

			DI.prepend = function () {
				DI.initVisibility();
				DI.RM.$headerEle.after(DI.$ele);

				return DI;
			};
			DI.saveWasOpen = function () {
				var portfolioSettings = RMService.getPortfolioSettings(DI.RM.portfolioId).rows;
				if (typeof portfolioSettings[DI.data.item_id] === "undefined") {
					portfolioSettings[DI.data.item_id] = {};
				}
				portfolioSettings[DI.data.item_id].wasOpen = DI.wasOpen;
				RMService.savePortfolioSettings(DI.RM.portfolioId);
			};

			DI.unSelectParents = function () {
				if (DI.isSelected) {
					return DI;
				}

				var runner = DI.parent;
				while (!runner.isRoot && runner.isSelected) {
					runner.isSelected = false;
					//if (typeof DI.RM.templates[DI.data.task_type] !== 'undefined') {
					runner.$ele.removeClass('selected');
					//}
					delete DI.RM.selectedRows[runner.data.item_id];
					runner = runner.parent;
				}

				return DI
			};

			DI.select = function (set) {
				if(!DI.editable()){ return false;}
				if (!DI.RM.selectMode) { return false;}

				// if (typeof DI.RM.templates[DI.data.task_type] === 'undefined') {
				// 	return DI;
				// }

				var recursive = true;
				if (set == DI.isSelected) {
					return DI;
				}

				if (typeof set === "undefined") {
					recursive = false;
					set = !DI.isSelected;
				}

				DI.isSelected = set;
				DI.unSelectParents();

				if (set) {
					//if (typeof DI.RM.templates[DI.data.task_type] !== 'undefined') {
					DI.$ele.addClass('selected');
					//}
					DI.RM.selectedRows[DI.data.item_id] = DI;
				} else {
					//if (typeof DI.RM.templates[DI.data.task_type] !== 'undefined') {
					DI.$ele.removeClass('selected');
					//}
					delete DI.RM.selectedRows[DI.data.item_id];
				}

				var runner = DI.firstEle;
				while (typeof runner != "undefined") {
					runner.select(set);
					runner = runner.next;
				}

				if (!recursive) {
					DI.RM.$parent.modifySelection = DI.RM.hasSelectedRows();
					DI.RM.$parent.$digest();
				}

				return DI;
			};

			var cacheStatusCls = "hidden";
			DI.initLock = function() {
				var allocation_status = parseInt(DI.data.allocation_status);
				if(isNaN(allocation_status)) {allocation_status = 0 }
				if(allocation_status>=20) {
					DI.RM.lock();
				}
			};
			DI.highLightGrey = function() {
				if (typeof DI.RM.options.signedColumn ==="undefined") return false;
				if (typeof DI.RM.options.signedValue ==="undefined") return false;

				var target = DI.data[DI.RM.options.signedColumn];
				if (typeof target === "undefined") return false;
				if (target === "undefined" || target === "") return false;

				if (Array.isArray(target)) {
					if (target.indexOf(DI.RM.options.signedValue)>-1) return true;
				} else if (target === DI.RM.options.signedValue ) return true;

				return false;
			};
			DI.showHighlight = function(overWriteColour) {
				var statusCls = "hidden";
				if(typeof overWriteColour!=="undefined") {
					statusCls = "highlight-bar "+overWriteColour;
				} else {
					if (DI.highLightGrey()) {
						statusCls = "highlight-bar grey";
					} else {
						if (DI.RM.options.currentUserId > 0 && typeof DI.data.responsible_id !== "undefined" && typeof DI.data.responsible_id !== "string") {
							if (DI.data.responsible_id.indexOf(DI.RM.options.currentUserId) !== -1) {
								statusCls = "highlight-bar purple";
							}
						}
						var allocation_status = parseInt(DI.data.allocation_status);
						if (isNaN(allocation_status)) {
							allocation_status = 0
						}

						switch (allocation_status) {
							case 20:
							case 25:
								statusCls = "highlight-bar teal";
								break;
							case 30:
								statusCls = "highlight-bar red";
								break;
							case 40:
							case 50:
							case 53:
							case 55:
							case 60:
								statusCls = "highlight-bar cyan";
								break;
						}
					}
				}

				if (cacheStatusCls !== statusCls) {
					DI.$statusBar.attr("class", statusCls);
					cacheStatusCls = statusCls;
				}
			};

			DI.isDraggable = function() {
				if (DI.RM.options.readOnly) { return false; }
				if (!DI.editable()) { return false; }
				if (typeof DI.RM.rules === "undefined" || typeof DI.RM.rules.draggables === "undefined") { return true; }
				var i, l = DI.RM.rules.draggables.length;
				var found =false;
				for (i = 0; i < l; i++) {
					if (DI.RM.rules.draggables[i] == DI.data.task_type) {
						found = true;
					}
				}
				return found;
			};

			return DI;
		};

		self.dataItem = function (RM, data) {
			var DI = self.baseDI();
			DI.RM = RM;
			DI.data = data;

			RMService.itemSet[data.item_id] = DI;
			return DI;
		};
	}


})();