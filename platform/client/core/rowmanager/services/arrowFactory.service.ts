﻿var AM;
(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.service('ArrowFactoryService', ArrowFactoryService);

	ArrowFactoryService.$inject = ["MessageService"];
	function ArrowFactoryService(MessageService) {
		var self = this;
		var freeId = 0;
		self.barheight = 16;
		self.arrowMargin = 7;
		self.arrowWidth = 2;
		
		var getNextFreeId = function() {
			return freeId++;
		};

		function Arrow(type, owner) { // 0=end to start, 1=start to start, 2=end to end, 3=start to end
			var AR = {
				isDotted:false,
				id:freeId++				
			};			
						
            var wasDotted = AR.isDotted;
			AR.id = getNextFreeId();
			AR.$e1 = $("<div class='arrow hidden'/>").appendTo(owner.wrapper);
			AR.$e2 = $("<div class='arrow hidden' />").appendTo(owner.wrapper);
			AR.$arrowHead = $("<i class='material-icons'>arrow_drop_down</i>");
			AR.$leadtime = $("<div class='leadtime hidden'></div>").appendTo(owner.wrapper);
			
			AR.hide = function () {
				AR.$e1.attr("class", "arrow hidden");
				AR.$e2.attr("class", "arrow hidden");
				AR.$leadtime.addClass("hidden");
			};
			AR.leadtime=0;
			
			AR.isCritical = false;
			AR.setCritical = function () {
				if (!AR.isCritical) {
					AR.isCritical = true;
					AR.$e1.addClass("critical");
					AR.$e2.addClass("critical");
				}
			};
			AR.notCritical = function () {
				if (AR.isCritical) {
					AR.isCritical = false;
					AR.$e1.removeClass("critical");
					AR.$e2.removeClass("critical");	
				} 
				
			};
			var currentState = {};
			
			var recalculate = function (source_l, source_w, source_t, target_l, target_w, target_t) {
				wasDotted = AR.isDotted;
				AR.isDotted = false;
				var edge = "edge";
				
				var e1_l, e1_w, e1_t, e1_h, e2_l, e2_w, e2_t, e2_h;
				var lt_l, lt_w, lt_t, lt_h;
				var lineNS = "N";
				var arrowNS = "S";
				
				var e1cls;
				var e2cls;
				var headcls;
				var headHtml;
				var headEle;

				if (source_t < target_t) {
					e1_t = source_t + Math.round(self.barheight / 2);
					e1_h = Math.round(self.barheight / 2) + self.arrowMargin;
					e2_t = e1_t + e1_h + self.arrowWidth;
					e2_h = target_t - e2_t - self.arrowWidth + 1;
					headHtml = "arrow_drop_down";
				} else {
					lineNS = "S";
					arrowNS = "N";
					e1_t = source_t - self.arrowMargin + self.arrowWidth - 1;
					e1_h = Math.round(self.barheight / 2) + self.arrowMargin - self.arrowWidth ;
					e2_t = target_t + self.barheight;
					e2_h = e1_t - e2_t;			
					headHtml = "arrow_drop_up";
				}

				var widthMargin = Math.round(target_w / 10);
				var leadPixels = owner.dayPixels * AR.leadtime;
				if (widthMargin > self.arrowMargin) {
					widthMargin = self.arrowMargin;
				}
				lt_t = source_t+5;			
				switch (AR.type) {
					case 5: // iconArrow
					case 0:	 //  0=end to start
						lt_l = source_l;
						lt_w = source_w + AR.leadtime*owner.dayPixels;
						
						if (source_l + source_w < target_l + self.arrowMargin) { // One element
							e1_h = e1_h + e2_h;
							e1_l = source_l + source_w;
							e1_w = (target_l + widthMargin) - (source_l + source_w);							
							e2_w = 0;
							if(leadPixels > e1_w) {
								edge = "dotted";
								AR.isDotted = true;
							}
							e1cls = "arrow " + edge + lineNS + "E";
							if (arrowNS === "N") {e1_t = e2_t;}

							headcls = "head" + arrowNS + "E";
							headEle = 1;
							
						} else {
							if(leadPixels + source_l+source_w>target_l) {
								edge = "dotted";
								AR.isDotted = true;
							}
							
							e1_l = source_l + source_w;
							e1_w = self.arrowMargin;

							e2_l = target_l + widthMargin;
							e2_w = (source_l + source_w) - (target_l + widthMargin);
							if (e2_w < 1) { e2_w = 1; }
							e1cls="arrow " + edge + "NES";
							e2cls =  "arrow " + edge + lineNS + "W";

							headcls = "head" + arrowNS + "W";
							headEle = 2;
							
						}
						break;
					case 1: // 1=start to start
						lt_l = source_l + AR.leadtime*owner.dayPixels;
						lt_w = source_w - AR.leadtime*owner.dayPixels;
						if(target_l-source_l < leadPixels) {
							edge = "dotted";
							AR.isDotted = true;
						}
						if (target_l + self.arrowMargin < source_l) { // One element
							e1_h = e1_h + e2_h;
							e1_l = target_l + widthMargin;
							e1_w = source_l - (target_l + widthMargin);
							e2_w = 0;

							e1cls = "arrow " + edge  + lineNS + "W";

							if (arrowNS === "N") { e1_t = e2_t; }

							headcls =  "head" + arrowNS + "W";
							headEle = 1;
						} else {
							e1_l = source_l - self.arrowMargin;
							e1_w = self.arrowMargin;

							e2_l = source_l;
							e2_w = (target_l + widthMargin) - source_l;

							if (e2_w < 1) {
								e2_w = 1;
							}

							e1cls = "arrow " + edge + "NWS";
							e2cls =  "arrow " + edge  + lineNS + "E";

							headcls =  "head" + arrowNS + "E";
							headEle = 2;
						}
						break;
					
					case 2: // 2=end to end
						lt_l = source_l;
						lt_w = source_w + AR.leadtime*owner.dayPixels;
						
						if(source_l+ source_w + leadPixels > target_l + target_w +1 ) {
							edge = "dotted";
							AR.isDotted = true;
						}
						if (source_l + source_w < target_l + target_w - self.arrowMargin) { // One element
							e1_h = e1_h + e2_h;
							e1_l = source_l + source_w;
							e1_w = (target_l + target_w - self.arrowMargin) - (source_l + source_w);
						
							e2_w = 0;
							e1cls = "arrow " + edge  + lineNS + "E";
							if (arrowNS === "N") { e1_t = e2_t; }

							headcls = "head" + arrowNS + "E";
							headEle = 1;
						} else {
							e1_l = source_l + source_w;
							e1_w = self.arrowMargin;

							e2_l = (target_l + target_w - self.arrowMargin);
							e2_w = (source_l + source_w) - (target_l + target_w - self.arrowMargin);

							if (e2_w < 1) {
								e2_w = 1;
							}

							e1cls = "arrow " + edge + "NES";
							e2cls = "arrow " + edge + lineNS + "W";

							headcls = "head" + arrowNS + "W";
							headEle = 2;
						}
						break;
					case 3: // 3=start to end
						lt_l = source_l + AR.leadtime*owner.dayPixels;
						lt_w = source_w - AR.leadtime*owner.dayPixels;
						if (target_l + target_w - self.arrowMargin < source_l) { // One element
							e1_h = e1_h + e2_h;
							e1_l = target_l + target_w - widthMargin;
							e1_w = source_l - (target_l + target_w - widthMargin);
							if(leadPixels + source_l > target_l+target_w+owner.dayPixels) {
								edge = "dotted";
								AR.isDotted = true;
							}
							e2_w = 0;

							e1cls = "arrow " + edge  + lineNS + "W";

							if (arrowNS === "N") {
								e1_t = e2_t;
							}

							headcls =  "head" + arrowNS + "W";
							headEle = 1;
							
						} else {
							if(leadPixels + source_l>target_l+target_w+owner.dayPixels) {
							 	edge = "dotted";
							 	AR.isDotted = true;
							}
							e1_l = source_l - self.arrowMargin;
							e1_w = self.arrowMargin;
							e2_l = source_l;
							e2_w = (target_l + target_w - self.arrowMargin) - source_l;

							if (e2_w < 1) {
								e2_w = 1;
							}
							e1cls =  "arrow " + edge + "NWS";
							e2cls =  "arrow " + edge + lineNS + "E";

							headcls =  "head" + arrowNS + "E";						
							headEle = 2;
							// AR.isDotted = true;
						}
						break;
				}
				

				if (e1_w < 1) {
					e1_w = 1;
				}
				if (AR.leadtime !== 0) {
						AR.$leadtime.css({
							left: lt_l,
							width: lt_w,
							top: lt_t,
							
						});
						AR.$leadtime.removeClass("hidden");
					} else {
						AR.$leadtime.addClass("hidden");
					}
				AR.$e1.css({ left: e1_l, width: e1_w, top: e1_t, height: e1_h });				
				if (e2_w > 0) {
					AR.$e2.css({ left: e2_l, width: e2_w, top: e2_t, height: e2_h });				
				} else {
					e2cls = "arrow hidden";
				}
				
				if (AR.isCritical) {
					e1cls += " critical";
					e2cls += " critical";
				}
				
				if(!AR.isHidden) {
					if(currentState.headHtml !== headHtml) {
						AR.$arrowHead.html(headHtml);
					}
					if(currentState.headcls !== headcls) {
						AR.$arrowHead.attr("class", "material-icons " + headcls);
					}
					if(currentState.e1cls !== e1cls) {
						AR.$e1.attr("class", e1cls);	
					}
					if(currentState.e2cls !== e2cls) {
						AR.$e2.attr("class", e2cls);
					}
					if(currentState.headEle !== headEle) {
						if(headEle === 1) {
							AR.$arrowHead.appendTo(AR.$e1);
						} else {
							AR.$arrowHead.appendTo(AR.$e2);
						}
					}
				} else {
					AR.hide();
				}
				if(wasDotted !== AR.isDotted) {
					owner.updateWarning();
				}
			};
		
			AR.update = function() {
				var fL = 0;
				var fW = 0;
				var fT = 0;
				var tL = 0;
				var tW = 0;
				var tT = 0;
				
				switch (AR.type) {
					case 5:
						var fRow = AR.fromDI.calendarRow;
						var tRow = AR.toDI.calendarRow;
						
						if (typeof(fRow)!=="undefined" && typeof(tRow)!=="undefined" ) {
							if (typeof(fRow.bars)!=="undefined" && typeof(tRow.bars)!=="undefined" ) {
								if (fRow.bars[AR.fEle] && tRow.bars[AR.tEle]) {
									var fEle = fRow.bars[AR.fEle];
									var tEle = tRow.bars[AR.tEle];
									fL = fEle.left-2;
									fW = 16;
									fT = fRow.top+16;
									tL = tEle.left+9;
									tW = 16;
									tT = tRow.top+16;
									AR.isHidden = (!fRow.visible && !tRow.visible);
								}
							}
						}
						break;
					case 0:
					case 1:
					case 2:
					case 3:						
						var fRow = AR.fromDI.calendarRow;
						var tRow = AR.toDI.calendarRow;
						var runner;						
						fL = fRow.bars["ganttBar"].left;
						fW = fRow.bars["ganttBar"].width;
						fT = fRow.top+16;
						tL = tRow.bars["ganttBar"].left;
						tW = tRow.bars["ganttBar"].width;
						tT = tRow.top+16;						
						if((!fRow.visible && !tRow.visible) || fRow.sprintRow ||tRow.sprintRow) {
							AR.isHidden = true;
						} else {
							AR.isHidden = false;
							if (!fRow.visible) {
								runner = fRow.owner;
								while (!runner.isRoot && !runner.calendarRow.visible) {
									runner = runner.parent;
								}
								if (runner.isRoot) {
									AR.isHidden = true;
								} else { 
									fT = runner.calendarRow.top+10;						
								}						 
							}
							if (!tRow.visible) {
								runner = tRow.owner;
								while (!runner.isRoot && !runner.calendarRow.visible) {
									runner = runner.parent;
								}
								if (runner.isRoot) {
									AR.isHidden = true;
								} else {
									tT = runner.calendarRow.top+10;						
								}
								
							}
						}
						break;
				}
				recalculate(fL, fW, fT, tL, tW, tT); //lwt				
				return AR;
			};

			AR.saveArrow = function() {
				if (typeof AR.fromDI.data.linked_tasks === "string") {
					AR.fromDI.data.linked_tasks= {}
				}

				if (typeof AR.fromDI.data.linked_tasks[AR.toDI.data.item_id] === "undefined") {
					AR.fromDI.data.linked_tasks[AR.toDI.data.item_id] = {};
				}

				AR.fromDI.data.linked_tasks[AR.toDI.data.item_id].link_type = AR.type;
				AR.fromDI.data.linked_tasks[AR.toDI.data.item_id].leadtime = AR.leadtime;
				AR.fromDI.setDataChanged();

				return AR;
			};

			var del = function(fromDI, toDI) {				
				if (typeof fromDI.myArrows[toDI.data.item_id] !== "undefined") {
					delete fromDI.myArrows[toDI.data.item_id];	
				}
			};

			AR.delArrow = function() {
				del(AR.fromDI, AR.toDI);
				del(AR.toDI, AR.fromDI);
				AR.$e1.remove();
				AR.$e2.remove();
				AR.$leadtime.remove();
				if (typeof AR.fromDI.data.linked_tasks === "string") {						
					AR.fromDI.data.linked_tasks = {}
				} else {
					if(!AR.fromDI.isDeleted) {
						delete AR.fromDI.data.linked_tasks[AR.toDI.data.item_id];
						AR.fromDI.setDataChanged();
					}					
				}				
			};

			AR.type = type; // 0=end to start, 1=start to start, 2=end to end, 3=start to end

			AR.openMenu = function(top, left) {
				var menuitems = [
					{
						name: "RM_END_TO_START",
						onClick: function () {
							AR.type = 0;
							AR.saveArrow().update();
						}
					}, {
						name: "RM_START_TO_START",
						onClick: function () {
							AR.type = 1;
							AR.saveArrow().update();
						}
					}, {
						name: "RM_END_TO_END",
						onClick: function () {
							AR.type = 2;
							AR.saveArrow().update();
						}
					}, {
						name: "RM_START_TO_END",
						onClick: function () {
							AR.type = 3;
							AR.saveArrow().update();
						}
					}, {
						name: "RM_LEADTIME",
						onClick:function() {
							var model = {leadtime: AR.leadtime};
							var dialogContent = {
								title: "RM_SET_LEADTIME",
								buttons:[{
									text: "Cancel",
									onClick: function() {
										MessageService.closeDialog();									
									}
								},{
									text: "Save",
									type: "primary",
									onClick: function() {										
										MessageService.closeDialog();
										AR.leadtime = model.leadtime;
									AR.saveArrow().update();
									}
								}],
								inputs:[{
									type: "integer",
									label: "RM_LEADTIME",
									model: "leadtime"
								}]								
							}
							MessageService.dialog(dialogContent, model)
						}
					}, {
						name: "RM_DELETE",
						onClick: function () {
							AR.delArrow();
						}
					}
				];
				
				MessageService.menu(
				menuitems, {
					top: top,
					left: left
				}, {
					override:true
				});
			
			};
			return AR;
		}

		self.arrowManager = function(CA) {
			AM = this;
			AM.wrapper = CA.$anchor;
			var showWarning = function () {};
			var hideWarning = function () {};
			AM.linkWarningIcon = function(RM) {
				showWarning = function () {
					RM.$parent.calendarWarning = true;
				};
				hideWarning = function () {
					RM.$parent.calendarWarning = false;
				};
			};
			var arrowColletor ={};
			var delayedUpdate;
			AM.updateWarning = function( ) {
				clearTimeout(delayedUpdate);
				delayedUpdate = setTimeout(function() {
					var dottedWarning = false;
					$.each(arrowColletor, function(i, AR){
						if(AR.isDotted && !AR.isHidden) { 
							dottedWarning = true;
						}					
					});
					if(dottedWarning) {
						showWarning();
					} else {
						hideWarning();
					}
				}, 0);
				return AM;		
			};
			AM.addArrow = function(type, leadtime, fromDI, toDI) {
				if(typeof fromDI !=="undefined" && typeof toDI !=="undefined") {
					var AR = new Arrow(type, AM);
					AR.fromDI = fromDI;
					AR.toDI = toDI;
					AR.leadtime = leadtime;
					arrowColletor[AR.id] = AR;
					fromDI.myArrows[toDI.data.item_id] = AR;
					toDI.myArrows[fromDI.data.item_id] = AR;
					AR.update();
					return AR;
				}	
			};
			// AM.addIconArrow = function(fromEle, toEle) {
			// 	if(typeof fromEle !=="undefined" && typeof toEle !=="undefined") {
			// 		var AR = new Arrow(5, AM);
			// 		AR.fEle = fromEle;
			// 		AR.tEle = toEle;
			// 		// AR.leadtime = leadtime;
			// 		arrowColletor[AR.id] = AR;
			// 		// fromDI.myArrows[toDI.data.item_id] = AR;
			// 		// toDI.myArrows[fromDI.data.item_id] = AR;
			// 		AR.update();
			// 		return AR;
			// 	}
			// }

			AM.addIconArrow = function(fromDI, fromEle, toDI, toEle) {
				if(typeof fromEle !=="undefined" && typeof toEle !=="undefined") {
					var AR = new Arrow(5, AM);
					AR.fromDI = fromDI;
					AR.toDI = toDI;
					AR.fEle = fromEle;
					AR.tEle = toEle;
					arrowColletor[AR.id] = AR;
					AR.update();
					return AR;
				}
			};
			
			AM.getArrow = function(DI1, DI2) {
				if (typeof DI1.myArrows[DI2.data.item_id] !== "undefined") {
					return DI1.myArrows[DI2.data.item_id];
				}

				if (typeof DI2.myArrows[DI1.data.item_id] !== "undefined") {
					return DI2.myArrows[DI1.data.item_id];
				}

				return undefined;
			};
			
			AM.updateAll = function() {				
				$.each(arrowColletor,function(i,AR){
					AR.update();
				});				
			};
			AM.removeCriticals = function() {
				$.each(arrowColletor,function(i,AR){
					AR.notCritical();
				});					
			};
			
			
			AM.del = function(AR) {
				delete arrowColletor[AR.id];
				AR.delArrow();
				return AM;
			};
			AM.delRow = function(DI) {
				if(typeof DI.myArrows !=="undefined") {					
					$.each(DI.myArrows,function(i, AR) {
						//delete arrowColletor[AR.id];
						AM.del(AR);
					});
				}
				AM.updateWarning();
				return AM;

			}
			// AM.dayPixels = 0;
			AM.changeArrowType = function(AR, type) {
				AR.type = type;
				AR.update();
			};

			return AM;
		}
	}
})();