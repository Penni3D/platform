(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.service('RMSortService', RMSortService);

	RMSortService.$inject = [
		'Common',
		'RMService',
		'$rootScope'
	];

	function RMSortService(
		Common,
		RMService,
		$rootScope
	) {
		var self = this;
		var masterClassSelector = '';
		var masterClassRegExp = '';

		self.addMasterClass = function (str) {
			if (masterClassSelector.length > 0) {
				masterClassSelector += ', ';
			}
			if (masterClassRegExp.length > 0) {
				masterClassRegExp += '|';
			}
			masterClassSelector += '.' + str;
			masterClassRegExp += '\\b' + str + '\\b';
		};

		var hasMasterClass = function (cls) {
			return cls.match(new RegExp(masterClassRegExp, 'g'));
		};

		var getHandle = function (bind) {
			var dragHandle = bind.selector;
			if (typeof bind.handle != 'undefined') {
				dragHandle += ' ' + bind.handle;
			}
			return dragHandle;
		};
		var _dragId = 0;
		self.bindStatusDrag = function (bind) {
			_dragId++;
			bind.$element.on('mousedown.drag' + _dragId, getHandle(bind), function (e1) {
				if (e1.which !== 1) return false;
				if (RMService.rmDragActive) {
					return false;
				}

				var $ele = $(this);

				if (typeof bind.handle !== 'undefined') {
					$ele = $(this).closest(bind.selector);
				}

				var status = $ele.attr('status');

				var statusCollection = $('kanban [status=\'' + status + '\']');
				var $dragged = $('<table class=\'kanbanColumnDrag z-depth-1\'></table>');

				var $placeholder;
				var pos = $ele.offset();
				var oX = e1.pageX;
				var oY = e1.pageY;
				var dX = oX - pos.left;
				var dY = oY - pos.top;

				var ownerId = $ele.parents(masterClassSelector).attr('diset');
				var draggedId = $ele.attr('statusid');

				var newPlace = [];
				var initDone = false;

				var initDrag = function () {
					if (!RMService.RMsets[ownerId].canBeDragged(draggedId)) {
						RMService.rmDragActive = false;
						return false;
					}

					RMService.rmDragActive = true;
					var w = $ele.width() + 32;
					var cc = statusCollection.length;
					var h;

					$(document.body).addClass('preventSelection');
					$placeholder = [];
					$dragged.appendTo($(document.body)).css({'width': w - 3});

					for (var i = 0; i < cc; i++) {
						var $td = $(statusCollection[i]);
						h = $td.height();

						if (i) {
							$('<tr></tr>').appendTo($dragged).append($td.clone().css('height', h));
						}

						$placeholder.push($('<td style=\'width:' + w + 'px;height:' + h + 'px\' class=\'placeholder\'>&nbsp;</td>').insertAfter($td));
						$td.css('display', 'none');
					}

					initDone = true;
				};

				var hasRole = function (e, $ele) {
					var cls = $ele.attr('class');

					if (typeof cls != 'undefined' && cls.length > 3) {
						if (cls == 'placeholder') {
							return true;
						}

						if (hasMasterClass(cls)) {
							ownerId = $ele.attr('diset');
							return true;
						}
					}

					var s = $ele.attr('status');

					if (typeof s === 'undefined') {
						return false;
					}

					var w = Math.round($ele.width() / 2);
					var pos = $ele.offset();

					if (e.pageX < pos.left + w) { // before
						newPlace.push({status: s, placement: 'before'});
					} else { //after
						newPlace.push({status: s, placement: 'after'});
					}

					return false;
				};

				var lastPlaceholder = {};

				$(document).on('mousemove.drag' + _dragId, function (e2) {
					if (!initDone) {
						if (e2.pageX > oX + 5 || e2.pageX < oX - 5 || e2.pageY > oY + 5 || e2.pageY < oY - 5) initDrag();
					} else {
						$dragged.css('display', 'none');
						var hover = document.elementFromPoint(e2.pageX - $(document).scrollLeft(), e2.pageY - $(document).scrollTop());
						var $hover = $(hover);
						var $parents = $hover.parents();
						var cc = $parents.length;

						$dragged.show();
						$dragged.css({'left': e2.pageX - dX, 'top': e2.pageY - dY});

						if (cc < 1) {
							return false;
						}

						newPlace = [];

						if (!hasRole(e2, $hover)) {
							for (var i = 0; i < cc; i++) {
								if (hasRole(e2, $($parents[i]))) break;
							}
						}

						cc = newPlace.length;
						var RM = RMService.RMsets[ownerId];

						if (cc > 0) {
							for (var i = 0; i < cc; i++) {
								if (newPlace[i].placement == 'before') {
									if (lastPlaceholder.place == newPlace[i].placement && lastPlaceholder.status == newPlace[i].status) {
										break;
									}

									lastPlaceholder = {place: newPlace[i].placement, status: newPlace[i].status};
									var targetStatus = $('kanban [status=\'' + newPlace[i].status + '\']');
									var cts = targetStatus.length;

									for (var j = 0; j < cts; j++) {
										$(targetStatus[j]).before($placeholder[j]);
									}

									RM.statusBefore(status, newPlace[i].status);
									break;
								}

								if (newPlace[i].placement == 'after') {
									if (lastPlaceholder.place == newPlace[i].placement && lastPlaceholder.status == newPlace[i].status) {
										break;
									}

									lastPlaceholder = {place: newPlace[i].placement, status: newPlace[i].status};
									var targetStatus = $('kanban [status=\'' + newPlace[i].status + '\']');
									var cts = targetStatus.length;

									for (var j = 0; j < cts; j++) {
										$(targetStatus[j]).after($placeholder[j]);
									}

									RM.statusAfter(status, newPlace[i].status);
									break;
								}
							}
						}
					}
				});

				$(document).on('mouseup.drag' + _dragId, function (e2) {
					if (initDone) {
						Common.stopEvents(e2);
						$(document.body).removeClass('preventSelection');

						for (var i = 0, cc = statusCollection.length; i < cc; i++) {
							$placeholder[i].after($(statusCollection[i]).show()).remove();
						}

						$dragged.remove();
					}

					$(document).off('mousemove.drag' + _dragId);
					$(document).off('mouseup.drag' + _dragId);
					RMService.rmDragActive = false
				});
			});
		};
		self.bindRowDrag = function (bind) {
			_dragId++;			
			bind.$element.on('mousedown.drag' + _dragId, getHandle(bind), function (e1) {
				if (e1.which != 1 || RMService.rmDragActive) {
					return false
				}

				var $ele = $(this);				
				
				if (typeof bind.handle !== 'undefined') {
					$ele = $(this).closest(bind.selector);
				}

				var ownerId = $ele.parents(masterClassSelector).attr('diset');
				var RM = RMService.RMsets[ownerId];

				if (RM.selectMode) {
					RMService.rmDragActive = false;
					return false;
				}

				var taskType = new RegExp(/type(\d+)/g).exec($ele.attr('class'));
				taskType = taskType[1];
				var draggedId = $ele.attr('rowid');
				var $dragged;

				var newOwnerId;
				var $placeholder = RM.getPlaceholder(taskType);
				$placeholder.ownerId = ownerId;

				var pos = $ele.offset();
				var oX = e1.pageX;
				var oY = e1.pageY;
				var dX = oX - pos.left;
				var dY = oY - pos.top;
				var initDone = false;
				var newPlace = [];

				var idx = 0;
				var deltaIdx = 0;
				var previousIndex = 0;

				var getPlacement = function (e, $ele) {
				    return false;
				};

				var initDrag = function () {
					if(typeof bind.dragStart !="undefined") {
						bind.dragStart(draggedId);
					}
					if (RM.selectMode) {
						RMService.rmDragActive = false;
						return false;
					}

					if (!RMService.RMsets[ownerId].canBeDragged(draggedId)) {
						RMService.rmDragActive = false;
						return false;
					}

					RMService.RMsets[ownerId].initDrag(draggedId, $ele);

					RMService.rmDragActive = true;
					$(document.body).addClass('preventSelection');
					var w = $ele.width();
					var h = $ele.height();

					$dragged = $ele.clone();
					$ele.children().each(function (i, c) {
						var w = $(c).width();
						$dragged.children().eq(i).css('width', w);
					});

					if (typeof bind.wrapAll != 'undefined') {
						$dragged = $dragged.wrapAll(bind.wrap).parents().last();
					}

					if (typeof bind.wrap != 'undefined') {
						$dragged = $dragged.wrap(bind.wrap).parents().last();
					} else {
						$dragged.addClass(bind.dragClass);
					}

					$dragged.appendTo($(document.body)).css({
						'width': w,
						'height': h,
						'left': e1.pageX - dX,
						'top': e1.pageY - dY
					});
					$ele.css('display', 'none');

					$placeholder.insertAfter($ele);

					switch (bind.hoverMode) {
						case 'h':
							getPlacement = function (e, $ele) {
								var pos = $ele.offset();
								var w = Math.round($ele.width() / 2);
								return (e.pageX < pos.left + w);
							};
							break;
						default:
							getPlacement = function (e, $ele) {
								var pos = $ele.offset();
								var h = Math.round($ele.height() / 2);
								return (e.pageY < pos.top + h);
							}
					}
					initDone = true;
				};

				var hasRole = function (e, $ele) {
					var cls = $ele.attr('class');

					if (typeof cls !== 'undefined' && cls.length > 3) {
						if (cls.match(/\bplaceholder\b/g)) {
							newPlace.push({name: cls, $ele: $ele, placement: 'placeholder'});
							return false;
						}

						if (hasMasterClass(cls)) {
							newOwnerId = $ele.attr('diset');
							return true;
						}

						if (cls.match(/\btype[0-9]+\b/g)) {
							if (getPlacement(e, $ele)) {
								newPlace.push({name: cls, $ele: $ele, placement: 'before'});
							} else {
								newPlace.push({name: cls, $ele: $ele, placement: 'after'});
							}

							return false;
						}

						if (cls.match(/\bempty\b/g)) {
							newPlace.push({name: 'empty', $ele: $ele, placement: 'empty'});
							return false;
						}
					}

					if (typeof $ele.attr('status') !== 'undefined') {
						newPlace.push({name: 'status', $ele: $ele, placement: 'status'});
					}

					return false;
				};

				$(document).on('mousemove.drag' + _dragId, function (e2) {
					if (!initDone) {
						if (e2.pageX > oX + 5 || e2.pageX < oX - 5 || e2.pageY > oY + 5 || e2.pageY < oY - 5) initDrag();
					} else {
						$dragged.css('display', 'none');
						var hover = document.elementFromPoint(e2.pageX - $(document).scrollLeft(), e2.pageY - $(document).scrollTop());
						$dragged.show();
						$dragged.css({'left': e2.pageX - dX, 'top': e2.pageY - dY});
						previousIndex = deltaIdx;
						idx = Math.round((e2.pageX - oX) / 64);

						if (idx) {
							deltaIdx += idx;
							oX = e2.pageX;
						}

						var $hover = $(hover);
						var $parents = $hover.parents();
						var cc = $parents.length;

						if (cc < 1) {
							return false;
						}

						newPlace = [];
						newOwnerId = undefined;

						if (!hasRole(e2, $hover)) {
							for (var i = 0; i < cc; i++) {
								if (hasRole(e2, $($parents[i]))) break;
							}
						}
						if (bind.lockMode) {
							if (newPlace.length>1) {
								if(newPlace[newPlace.length-1].$ele.attr("rowid") !== $ele.closest("tr").attr("rowid")) {
									newPlace = [];
								}
							}							
						} 
						var triggered = false;
						var newPlaceholder = function () {
							if (ownerId != $placeholder.ownerId) {
								$placeholder.remove();

								$placeholder = RMService.RMsets[ownerId].getPlaceholder(taskType);
								$placeholder.ownerId = ownerId;
							}
						};
						
						if (typeof newOwnerId !== 'undefined') {
							var status = undefined;
							cc = newPlace.length;							
							if (ownerId != newOwnerId) {
								deltaIdx = 0;
							}	
							
							for (var i = 0; i < cc; i++) {
								if (RMService.RMsets[newOwnerId].options.readOnly) {
									break;
								}
								if (newPlace[i].placement == 'empty') {
									if (RMService.RMsets[newOwnerId].onEmpty(draggedId)) {
										ownerId = newOwnerId;
										newPlaceholder();
										newPlace[i].$ele.before($placeholder);
									}

									triggered = true;
									break;
								}

								if (newPlace[i].placement == 'placeholder') {
									break;
								}

								if (typeof status !== 'undefined') {
									var targetId = newPlace[i].$ele.attr('rowid');
									if (newPlace[i].placement == 'before' || newPlace[i].placement == 'after') {										
										if (RMService.RMsets[newOwnerId].inStatus(draggedId, status.id, targetId)) {											
											ownerId = newOwnerId;
											newPlaceholder();
											status.$ele.append($placeholder);
											break;
										}
									}
									triggered = true;
								}

								if (newPlace[i].placement == 'before') {
									var targetId = newPlace[i].$ele.attr('rowid');
									var indexes = {index: deltaIdx};

									if (RMService.RMsets[newOwnerId].before(draggedId, targetId, indexes)) {
										ownerId = newOwnerId;
										newPlaceholder();
										newPlace[i].$ele.before($placeholder);
									}

									triggered = true;
									break;
								}

								if (newPlace[i].placement == 'after') {
									var targetId = newPlace[i].$ele.attr('rowid');
									var indexes = {index: deltaIdx};

									if (RMService.RMsets[newOwnerId].after(draggedId, targetId, indexes)) {
										ownerId = newOwnerId;
										newPlaceholder();
										newPlace[i].$ele.after($placeholder);
									}

									triggered = true;
									break;
								}

								if (newPlace[i].placement == 'status') {
									ownerId = newOwnerId;
									status = {};
									status.$ele = newPlace[i].$ele;
									status.id = status.$ele.attr('status');
									triggered = true;
								}
							}
						}

						if (!triggered && deltaIdx != previousIndex) {
							var indexes = {index: deltaIdx};
							if (RMService.RMsets[ownerId].onPlaceholder(draggedId, indexes)) {
								newPlaceholder();
							}
						}
					}
				});

				$(document).on('mouseup.drag' + _dragId, function (e2) {
					if (initDone) {
						Common.stopEvents(e2);
						$(document.body).removeClass('preventSelection');
						$placeholder.after($ele);
						$ele.show();
						$placeholder.remove();
						$dragged.remove();

						if (typeof ownerId !== 'undefined') {
							RMService.RMsets[ownerId].applyChanges(draggedId);
							$rootScope.$broadcast('RMDragProcessStop', draggedId);
						}
					}

					$(document).off('mousemove.drag' + _dragId);
					$(document).off('mouseup.drag' + _dragId);
					RMService.rmDragActive = false
				});
			});
		};

	}


})();