﻿var temp;
(function () {
	'use strict';
	angular
		.module('core.rowmanager')
		.directive('kanban', Kanban);

	Kanban.$inject = [
		'RMService',
		'RMDIService',
		'RMSortService',
		'MessageService',
		'UserService',
		'ViewService',
		'ProcessService',
		'PortfolioService',
		'Colors',
		'Translator',
		'$q'
	];

	function Kanban(
		RMService,
		RMDIService,
		RMSortService,
		MessageService,
		UserService,
		ViewService,
		ProcessService,
		PortfolioService,
		Colors,
		Translator,
		$q
	) {
		return {
			scope: {
				parentItemId: '@',
				addtype: '@',
				options: '<',
				rows: '=',
				newItem: '&?'
			},
			template: '<table class="kanbanBody"></table>',
			transclude: true,
			link: function (scope, ele, attr, controller, transclude) {
				transclude(scope, function (clone) {
					setTimeout(function () {
						controller.setTemplates(clone);
					}, 0);
				});
			},
			controller: function ($scope, Common, $element) {
				var masterClass = 'kanban';
				RMSortService.addMasterClass(masterClass);
				$element.addClass(masterClass);

				var RM = new RMService.managerTemplate();
				RM.emptyVisibility = function() {};  
				RM.options = $.extend({
					cardRights: false,
					sprintRights: false,
					ownCardRights: false,
					defaultStatus: [{
						title: Translator.translate('KANBAN_PHASE'),
						order_no: 'a',
						parent_item_id: $scope.parentItemId,
						is_done: true
					}]
				}, $scope.options);
				
				RM.createFieldsCache([{ItemColumn: {DataType: 0, Name: 'title'}}, {
					ItemColumn: {
						DataType: 0,
						Name: 'description'
					}
				}]);

				RM.$ele = $element;
				var $body = $element.find('.kanbanBody');

				RM.rules = $scope.rules;
				var statusParent = {};
				var rootParent = {
					getCurrentIndex: function () {
						return 0;
					},
					isRoot: true,
					data: {
						owner_item_id: RM.options.ownerId,
						owner_item_type: RM.options.ownerType,
						item_id: $scope.parentItemId
					}
				};

				RM.setValues(rootParent, $scope.rows);
				RM.$header = $();
				RM.emptyContent = '';

				this.setTemplates = function (clone) {
					var l = clone.length;
					for (var i = 0; i < l; i++) {
						switch ($(clone[i]).attr('type')) {
							case 'empty':
								RM.emptyContent = clone[0].innerHTML;//$(clone[i]).html();
								break;
						}
					}

					setTimeout(function () {
						initializeKanban();
					});
				};

				var draggedProcess;
				$scope.$on('DragProcessStart', function (event, process) {
					draggedProcess = process;
				});

				RMDIService.bindSubscribeChanged([RM.options.process], $scope);
				var initializeKanban = function () {
					PortfolioService.getTaskStatus($scope.parentItemId).then(function (status) {
						var tasks = $scope.rows;

						RM.setRowParent = function () {
						};
						var reDraw = function () {
						};

						RM.getPlaceholder = function (type) {
							switch (type) {
								case '0':
									RM.$placeholder = $('<div class="placeholder"></div>');
									break;
								case '1':
									var runner = statusParent.firstEle;
									var statusCount = 1;
									while (typeof runner !== 'undefined') {
										statusCount++;
										runner = runner.next;
									}

									RM.$placeholder = $('<tr><td colspan="' + statusCount + '"><div class="placeholder"></div></td></tr>');
									break;
							}

							return RM.$placeholder;
						};

						RM.onEmpty = function (itemId) {
							var DI = RMService.itemSet[itemId];

							if (DI.data.task_type == 1) {
								reDraw = function () {
									if (DI.$ele) {
										DI.$ele.remove();
									}

									DI = initRow(DI).append();
									var runner = DI.firstEle;
									while (typeof runner !== 'undefined') {
										if (runner.$ele) runner.$ele.remove();

										var childEle = initCard(runner);

										if (typeof childEle !== 'undefined') {
											childEle.append(true);
										}

										runner = runner.next;
									}
								};

								RM.setRowParent = function () {
									RMService.inheritParent(DI.data, rootParent.data);
									DI.linkFirst(rootParent).setOrderNo(Common.getOrderNoBetween);
								};

								return true;
							}

							return false;
						};

						RM.init = function (DI) {
							if (DI.data.task_type == 0) {
								return initCard(DI);
							}

							return initRow(DI);
						};

						RM.initNew = function (itemId) {
							return RM.init(new RMDIService.dataItem(RM, itemId));
						};

						RM.onPlaceholder = function (itemId, indexes) {
							indexes.index = 0;

							return true;
						};

						RM.after = function (itemId, targetId, indexes) {
							indexes.index = 0;
							var DI = RMService.itemSet[itemId];
							var target = RMService.itemSet[targetId];

							if (DI.data.task_type != target.data.task_type) {
								return false;
							}

							if (DI.data.task_type == 0) {
								reDraw = function () {
									if (DI.$ele) {
										DI.$ele.remove();
									}

									initCard(DI).after(target);
								};

								RM.setRowParent = function () {
									DI.data.status_id = target.data.status_id;
									RMService.inheritParent(DI.data, target.parent.data);
									DI.linkAfter(target).setOrderNo(Common.getOrderNoBetween);
								};
							} else {
								reDraw = function () {
									if (typeof DI.$ele !== 'undefined') {
										DI.$ele.remove();
									}

									DI = initRow(DI).after(target);

									var runner = DI.firstEle;
									while (typeof runner !== 'undefined') {
										if (typeof runner.$ele !== 'undefined') {
											runner.$ele.remove();
										}

										var childEle = initCard(runner);
										if (typeof childEle !== 'undefined') {
											childEle.append(true);
										}

										runner = runner.next;
									}
								};

								RM.setRowParent = function () {
									RMService.inheritParent(DI.data, rootParent.data);
									DI.linkAfter(target).setOrderNo(Common.getOrderNoBetween);
								};
							}

							return true;
						};

						RM.before = function (itemId, targetId, indexes) {
							indexes.index = 0;
							var DI = RMService.itemSet[itemId];
							var target = RMService.itemSet[targetId];

							if (DI.data.task_type != target.data.task_type) {
								return false;
							}

							if (DI.data.task_type == 0) {
								reDraw = function () {
									if (typeof DI.$ele !== 'undefined') {
										DI.$ele.remove();
									}

									initCard(DI).before(target);
								};

								RM.setRowParent = function () {
									DI.data.status_id = target.data.status_id;
									RMService.inheritParent(DI.data, target.parent.data);
									DI.linkBefore(target).setOrderNo(Common.getOrderNoBetween);
								};
							} else {
								reDraw = function () {
									if (typeof DI.$ele !== 'undefined') {
										DI.$ele.remove();
									}

									DI = initRow(DI).before(target);
									var runner = DI.firstEle;
									while (typeof runner !== 'undefined') {
										if (typeof runner.$ele !== 'undefined') {
											runner.$ele.remove();
										}

										var childEle = initCard(runner);
										if (typeof childEle !== 'undefined') {
											childEle.append(true);
										}

										runner = runner.next;
									}
								};

								RM.setRowParent = function () {
									RMService.inheritParent(DI.data, rootParent.data);
									DI.linkBefore(target).setOrderNo(Common.getOrderNoBetween);
								};
							}

							return true;
						};

						RM.inStatus = function (itemId, status, rowId) {
							var DI = RMService.itemSet[itemId];

							if (DI.data.task_type == 0) {
								reDraw = function () {
									if (typeof DI.$ele !== 'undefined') {
										DI.$ele.remove();
									}

									initCard(DI).append();
								};

								RM.setRowParent = function () {
									DI.data.status_id = status;
									var parent = RMService.itemSet[rowId];
									RMService.inheritParent(DI.data, parent.data);

									DI.linkLast(parent).setOrderNo(Common.getOrderNoBetween);
								};

								return true;
							}

							return false;
						};

						RM.applyChanges = function (itemId) {
							var DI = RMService.itemSet[itemId];
							RM.setRowParent();
							DI.setDataChanged();

							if (DI.RM.id != RM.id) {
								reDraw();
							}
							resetLimiter();
							//cardLimiter(DI.data.status_id, undefined);
						};

						RM.statusBefore = function (id, targetId) {
							var moved = statusParent[id];
							moved.linkBefore(statusParent[targetId]).setOrderNo(Common.getOrderNoBetween);
							RMService.setStatusChanged(moved.data);
							var $ele = RM.$toolbar.find('th[status=' + id + ']');
							var $targetEle = RM.$toolbar.find('th[status=' + targetId + ']');
							$targetEle.before($ele);
						};

						RM.statusAfter = function (id, targetId) {
							var moved = statusParent[id];
							moved.linkAfter(statusParent[targetId]).setOrderNo(Common.getOrderNoBetween);
							RMService.setStatusChanged(moved.data);
							var $ele = RM.$toolbar.find('th[status=' + id + ']');
							var $targetEle = RM.$toolbar.find('th[status=' + targetId + ']');
							$targetEle.after($ele);

						};

						RMService.addRM(RM.id, RM);
						initTable(status).then(function () {

							var redoOrders = false;
							var kanbanTasks = [];
							var kanbanRows = [];
							var i;
							var taskL = tasks.length;

							for (i = 0; i < taskL; i++) {
								var DI = new RMDIService.dataItem(RM, tasks[i]);
								if (DI.data.parent_item_id == rootParent.data.item_id) {
									kanbanRows.push({
										id: DI.data.item_id,
										taskCount: 0
									});
								}
							}
							var rowL = kanbanRows.length;
							for (i = 0; i < taskL; i++) {
								var DI = RMService.itemSet[tasks[i].item_id];
								var j;
								for (j = 0; j < rowL; j++) {
									if (DI.data.parent_item_id == kanbanRows[j].id && DI.data.task_type == 0) {
										kanbanTasks.push(DI.data.item_id);
										kanbanRows[j].taskCount++;
									}
								}
							}

							for (i = 0; i < rowL; i++) {
								var DI = RMService.itemSet[kanbanRows[i].id];
								RMService.itemSet[DI.data.item_id] = initRow(DI);
								DI.append();
								if (DI.data.order_no.length == 0) {
									redoOrders = true;
								}
							}

							var kanbanTaskL = kanbanTasks.length;
							for (i = 0; i < kanbanTaskL; i++) {
								var DI = RMService.itemSet[kanbanTasks[i]];
								RMService.itemSet[DI.data.item_id] = initCard(DI);

								DI.append();
								if (DI.data.order_no.length == 0) {
									redoOrders = true;
								}
							}

							if (redoOrders) {
								var newOrdersNumbers = Common.fixOrders(taskL);

								for (i = 0; i < taskL; i++) {
									var DI = RMService.itemSet[tasks[i]];
									DI.data.order_no = newOrdersNumbers[i];
									DI.setDataChanged();
								}
							}
							initializeLimiter();
						});
						var dragStarted = function(id) {
							var DI = RMService.itemSet[id];
							hideLimiter(DI.data.status_id, DI.data.parent_item_id)
						}
						RM.initializeScroll($scope);
					
						RMSortService.bindRowDrag({
							$element: RM.$ele,
							dragStart: dragStarted,
							selector: 'tr td div.type0',
							hoverMode: 'v',
							dragClass: 'kanbanCardDrag z-depth-1',
							lockMode: $scope.options.mode == 1
						});

						RMSortService.bindRowDrag({
							$element: RM.$ele,
							selector: 'tr',
							hoverMode: 'v',
							handle: '.backlog',
							wrap: '<table class="kanbanRowDrag z-depth-1"></table>'
						});

						RMSortService.bindStatusDrag({
							$element: RM.$ele,
							selector: 'tr th',
							hoverMode: 'h',
							wrap: '<table class="kanbanColumnDrag z-depth-1"></table>',
							wrapAll: '<tr></tr>'
						});

						$element.on('dblclick', 'tr.type1 div.type0', function (e) {
							Common.removeSelection();
							var DI = RMService.itemSet[$(e.currentTarget).attr('rowid')];
							RMService.openFeaturette(DI);
						});

						$element.on('mousedown', 'div .editTask', function (e) {
							Common.stopEvents(e);
							var rowId = $(e.currentTarget).parents('.type0').first().attr('rowid');
							var DI = RMService.itemSet[rowId];
							var menuItems = [
								{
									name: 'KANBAN_CARD_EDIT',
									onClick: function () {
										RMService.openFeaturette(DI);
									},
                                    html: undefined
								}];
							if(!DI.isFirst()) {
								menuItems.push({
									name:"KANBAN_MOVE_TOP",
									onClick:DI.moveFirst,
									html: undefined
								});
							}
							if(!DI.isLast()) {
								menuItems.push({
									name:"KANBAN_MOVE_BOTTOM",
									onClick:DI.moveLast,
									html: undefined
								});
							}
							if ($scope.options.mode != 1) {
								menuItems.push(
									{
										name: 'KANBAN_CARD_ADD',
										onClick: function () {
											createCardAfter(rowId);
										},
                                        html: undefined
									});

								menuItems.push({
									name: 'KANBAN_CARD_DELETE',
									onClick: function () {
										MessageService.confirm(Translator.translate('TASK') + ' ' + DI.data.title + ' ' + Translator.translate('KANBAN_DELETE_CONFIRMATION'), function () {
											ProcessService.deleteItem(RM.options.process, rowId).then(function () {
												RMService.itemSet[rowId].delete();
												cardLimiter(DI.data.status_id, undefined);
											});											
										});
									},
                                    html: undefined
								});
							}


							angular.forEach(Colors.getColors(), function (colour, code) {
								menuItems.push({
									html: '<div class="kCircle circle ' + colour.background + '">&nbsp;</div>',
									onClick: function () {
										DI.setColor(code, colour.background);
									},
                                    name: undefined
								});
							});
							MessageService.menu(menuItems, e.currentTarget);
						});

						$element.on('mousedown', 'td.backlog .rmEditItem', function (e) {
							Common.stopEvents(e);
							var rowId = $(e.currentTarget).parents('.type1').first().attr('rowid');
							var DI = RMService.itemSet[rowId];
							var menuItems = [];

							if (RM.options.sprintRights) {
								menuItems.push({
									name: 'KANBAN_ROW_EDIT', onClick: function () {
										RMService.openFeaturette(DI);
									},
                                    html: undefined
								});
							}

							if (RM.options.cardRights) {
								menuItems.push({
									name: 'KANBAN_CARD_ADD', onClick: function () {
										createCardInRow(rowId);
									},
                                    html: undefined
								});
							}

							if (RM.options.sprintRights) {
								menuItems.push({
									name: 'KANBAN_ROW_ABOVE', onClick: function () {
										createRowBefore(rowId);
									},
                                    html: undefined
								});

								menuItems.push({
									name: 'KANBAN_ROW_BELOW', onClick: function () {
										createRowAfter(rowId);
									},
                                    html: undefined
								});
							}

							if (RM.options.sprintRights) {
								if (rootParent.firstEle && rootParent.firstEle.next) {
									menuItems.push({
										name: 'KANBAN_ROW_DELETE', onClick: function () {
											MessageService.confirm(Translator.translate('ROW') + ' ' + DI.data.title + ' ' + Translator.translate('KANBAN_DELETE_CONFIRMATION'), function () {
												ProcessService.deleteItem(RM.options.process, rowId).then(function () {
													RMService.itemSet[rowId].delete();
												});
											});
										},
                                        html: undefined
									});
								} else {
									menuItems.push({
										disabled: true,
										name: 'DELETE',
                                        html: undefined
									});
								}
							}

							MessageService.menu(menuItems, e.currentTarget);
						});

						$element.on('dblclick', 'tr.type1 td.backlog', function (e) {
							Common.removeSelection();
							var DI = RMService.itemSet[$(e.currentTarget).parents('.type1').first().attr('rowid')];
							RMService.openFeaturette(DI);
						});

						function editPhase(statusId) {
							var content = {
								buttons: [{text: 'CANCEL'}, {type: 'primary', text: 'SAVE'}],
								inputs: [
									{type: 'string', label: 'KANBAN_PHASE', model: 'title'},
									{
										type: 'select',
										label: 'PHASE_TYPE',
										model: 'status_list_item_id',
										options: kanbanStatusOptions,
										optionName: 'list_item',
										optionValue: 'item_id',
										singleValue: true
									}
								],
								title: 'KANBAN_PHASE_EDIT'
							};

							MessageService.dialog(content, statusParent[statusId].data).then(function () {
								RM.$header.find('th[status=' + statusId + '] .title').text(statusParent[statusId].data.title);
								RM.$toolbar.find('th[status=' + statusId + '] .title').text(statusParent[statusId].data.title);
								RMService.setStatusChanged(statusParent[statusId].data);
							});
						}

						function phaseDblClick(e) {
							Common.removeSelection();
							var $statusCell = $(e.currentTarget);
							var statusId = $statusCell.attr('status');
							editPhase(statusId, $statusCell);
						}

						function editPhaseClick(e) {
							Common.stopEvents(e);
							var $statusCell = $(e.currentTarget).closest('th');
							var statusId = $statusCell.attr('status');
							var menuItems = [
								{
									name: 'KANBAN_PHASE_EDIT', onClick: function () {
										editPhase(statusId, $statusCell);
									}
								},
								{
									name: 'KANBAN_PHASE_RIGHT', onClick: function () {
										createPhaseAfter(statusId);
									}
								},
								{
									name: 'KANBAN_PHASE_LEFT', onClick: function () {
										createStatusBefore(statusId);
									}
								}];

							if (statusParent.firstEle && statusParent.firstEle.next) {
								menuItems.push({
									name: 'KANBAN_PHASE_DELETE', onClick: function () {
										MessageService.confirm(Translator.translate('KANBAN_PHASE') + ' ' + statusParent[statusId].data.title + ' ' + Translator.translate('KANBAN_DELETE_CONFIRMATION'), function () {
											RMService.removeKanbanPhase(statusId).then(function () {
												statusParent[statusId].delete();
											});
										});
									}
								});
							} else {
								menuItems.push({
									disabled: true,
									name: 'KANBAN_PHASE_DELETE'
								});
							}
							if(!isLimited[statusId]) {
								isLimited[statusId] = true;
								menuItems.push({
									name: 'limit cards',
									onClick: function () {
										cardLimiter(statusId,true);
									}
								});

							} else {
								isLimited[statusId] = false;
								menuItems.push({
									name: 'show cards',
									onClick: function () {
										cardLimiter(statusId,false);
									}
								});

							}
							
							
							MessageService.menu(menuItems, e.currentTarget, {size: 'large'});
						}

						$element.on('dblclick', 'th:not(.backlog)', phaseDblClick);
						RM.$toolbar.on('dblclick', 'th:not(.backlog)', phaseDblClick);
						$element.on('mousedown', '.editStatus', editPhaseClick);
						RM.$toolbar.on('mousedown', '.editStatus', editPhaseClick);

						$element.attr('diset', RM.id);
					});
				};

				var createCardAfter = function (prevId) {
					var parent = RMService.itemSet[prevId].parent;
					var card = defaultCard(parent.data.item_id);

					card.status_id = RMService.itemSet[prevId].data.status_id;
					card.order_no = getOrderAfter(prevId);

					newCard(card);
				};
				var createRowAfter = function (prevId) {
					var row = defaultRow(prevId);
					row.order_no = getOrderAfter(prevId);
					newRow(row);
				};
				var createRowBefore = function (nextId) {
					var row = defaultRow(nextId);
					row.order_no = getOrderBefore(nextId);
					newRow(row);
				};
				var createNewRow = function () {
					var data = RMService.getDefaultDIData(Translator.translate('RM_TASK'), 1);
					data.parent_item_id = rootParent.data.item_id;
					data.owner_item_id = rootParent.data.owner_item_id;
					data.owner_item_type = rootParent.data.owner_item_type;
					var lastEle = rootParent.lastEle;
					var lastOrder = undefined;
					if (typeof lastEle !== "undefined") {
						lastOrder = lastEle.data.order_no;
					}
					data.order_no = Common.getOrderNoBetween(lastOrder, undefined);

					newRow(data);
				};
				var createCardInRow = function (rowId, status?) {
					var card = defaultCard(rowId);

					if (typeof status === 'undefined') {
						status = statusParent.firstEle.data.status_id;
					}
					card.status_id = status;

					var parent = RMService.itemSet[rowId];
					var lastId = parent.lastEle;
					var firstId = parent.firstEle;

					// if (typeof parent.lastEle !== 'undefined') {
					// 	lastId = parent.lastEle.data.item_id;
					// }
					if (typeof parent.firstEle !== 'undefined') {
						firstId = parent.firstEle.data.item_id;
					}
					card.order_no = getOrderBefore(firstId)
					// card.order_no = getOrderAfter(lastId);

					newCard(card);
				};

				var createPhaseAfter = function (prevId) {
					var phase = {};
					var target = statusParent[prevId];
					var prev = target.data.order_no;
					var next = undefined;

					if (typeof target.next !== 'undefined') {
						next = target.next.data.order_no;
					}
					
					phase.order_no = Common.getOrderNoBetween(prev, next);
					newPhase(phase);
				};
				var isLimited = [];
				// var $limitterEle = $("<div class='limitter'></div>");
				// var checkLimitter = function($ele, rowValue) {
				// 	var $limitter = $ele.find(".limitter");
				// 	if($limitter.length) {
				// 		if($limitter.find(".cardCount").length) {
				// 			return true
				// 		} else {
				// 			return false;
				// 		}
				// 	}
				// 	return rowValue;
				// }
				// var setLimitter = function($ele, count, show) {
				// 	var $limitter = $ele.find(".limitter");
				// 	if(!$limitter.length) {
				// 		$limitter = $("<div class='limitter'></div>").appendTo($ele)
				// 	}
				//	
				// }
				const CARDLIMIT = 4;
				
				// var $hiddenCardsEle = $("<div class='hiddenCards'>hidden cards:<span class='cardCount'></span> <span class='showCards'>Show All</span></div>")
				// var $limitCardsEle = $("<div class='limitCards'>Limit Cards</div>")
				var limiterStatuses = [];
				var initializeLimiter = function() {
					setTimeout(function () {
						$.each(limiterStatuses, function (i,statusId) {
							cardLimiter(statusId, true);
						});
					},0);
					
				}
				var resetLimiter = function() {
					setTimeout(function () {
						$.each(limiterStatuses, function (i,statusId) {
							cardLimiter(statusId, undefined);
						});
					},0);
					
				}
				var addStatusInLimiter = function(statusId) {
					limiterStatuses.push(statusId)
				}
				var hideLimiter = function(statusId, rowId) {
					$("kanban table tr[rowid="+rowId+"] td[status="+statusId+"] div.limiter").hide();
					$("kanban table tr[rowid="+rowId+"] td[status="+statusId+"] div.type0").show();
				}
			
				var cardLimiter = function(statusId, setLimit) {
					if(typeof setLimit !=="undefined") {
						isLimited[statusId] =setLimit; 
					}
					var $statusBoxes = $("kanban table tr td[status="+statusId+"]");
					$statusBoxes.each(function(i,e){
						var $box = $(e);
						var $limiter = $box.find("div.limiter");
					
						var $hiddenCards = $box.find("div.type0:nth-child(n+" + CARDLIMIT + ")");
						var hiddenCount =$hiddenCards.length;
						var showCards = function() {
							$limiter.show();
							$box.find("div.type0:hidden").show();

							$limiter.html("<span class='toggleCards'>" + Translator.translate("KANBAN_LIMIT_CARDS") + "</span>");
							$limiter.appendTo($box);
						}
						var hideCards = function() {
							$limiter.show();
							$box.find("div.type0:hidden").show();
							$hiddenCards = $box.find("div.type0:nth-child(n+" + CARDLIMIT + ")");
							$hiddenCards.hide();
							hiddenCount=$hiddenCards.length;
							$limiter.html(Translator.translate("KANBAN_HIDDEN_CARDS") + " <span class='cardCount'>"+hiddenCount+"</span> " + Translator.translate("KANBAN_UNIT") + " --- <span class='toggleCards'>" + Translator.translate("KANBAN_SHOW_CARDS") + "</span>");

							$limiter.appendTo($box);
						}
						var noLimits = function() {
							$limiter.hide();
							$box.find("div.type0:hidden").show();
						}
						var toggleCards = function() {
							if($limiter.find(".cardCount").length) {
								showCards();
							} else {
								hideCards();
							}
						}
						var hasLimiter = true;
						if(!$limiter.length) {
							// no limiter found, creating
							hasLimiter = false;
							$limiter = $("<div class='limiter'></div>");
							$box.on("mouseup", "div.limiter span.toggleCards", toggleCards);
						//} else { // limiter was found
						}
						$limiter.appendTo($box);
						// if($limiter.is(":visible") == false) {hasLimiter = false;}
						
						if(hiddenCount) { // if not enough cards for limiting, no limits							
							if(typeof setLimit !=="undefined") { // Limit is changing. This will overrides everything!
								if(setLimit) {
									hideCards();
								} else {
									showCards();
								}
							} else {								
								if(hasLimiter) { // has limiter applied already, redo
									if($box.find("div.limiter span.cardCount").length>0) {
										hideCards();
									} else {
										showCards();
									}
									
								} else { //set limitter to be row default
									if(isLimited[statusId]) {
										showCards();	
									} else {
										hideCards();
									}
								}
							}
							
							if(isLimited[statusId]) {
							
							} else {
								showCards();							
							}
							
						} else {
							noLimits();
						} 
						
					})
				};
				var createStatusBefore = function (nextId) {
					var phase = {};
					var target = statusParent[nextId];
					var prev = undefined;
					var next = target.data.order_no;

					if (typeof target.prev !== 'undefined') {
						prev = target.prev.data.order_no;
					}

					phase.order_no = Common.getOrderNoBetween(prev, next);
					newPhase(phase);
				};

				var defaultCard = function (baseId) {
					var parent = RMService.itemSet[baseId].data;
					var card = RMService.getDefaultDIData(Translator.translate('RM_KANBAN_CARD'), 0);

					card.status_id = parent.status_id;
					card.color = 'yellow';
					RMService.inheritParent(card, parent);

					return card;
				};

				var defaultRow = function (baseId) {
					var parent = RMService.itemSet[baseId].parent.data;
					var row = RMService.getDefaultDIData(Translator.translate('RM_TASK'), 1);
					RMService.inheritParent(row, parent);
					return row;
				};

				var newPhase = function (phase) {
					phase.parent_item_id = rootParent.data.item_id;
					if (kanbanStatusOptions.length >= 1) phase.status_list_item_id = kanbanStatusOptions[1].item_id;
					return RMService.newKanbanPhase(phase).then(function (p) {
						var runner = statusParent.firstEle;
						var putAfter = undefined;
						var DI = initPhaseDI(p);

						while (typeof runner !== 'undefined' && runner.data.order_no < p.order_no) {
							putAfter = runner;
							runner = runner.next;
						}

						if (typeof putAfter !== 'undefined') {
							DI.after(putAfter.data.status_id);
						} else {
							DI.before(statusParent.firstEle.data.status_id);
						}

						return DI;
					});
				};

				var initCard = function (DI) {
					var clone = function(ele) {
						return JSON.parse(JSON.stringify(ele));
					}
					if (DI.data.task_type != 0) {
						return undefined;
					}
										
					DI.findFirstInStatus = function() {
						var runner = DI.parent.firstEle;
						while (typeof runner !== "undefined") {
							if(runner.data.status_id == DI.data.status_id) {
								return runner;
							}
							runner = runner.next;							
						}
						return null;						
					}
					DI.findLastInStatus = function() {
						var runner = DI.parent.lastEle;
						while (typeof runner !== "undefined") {
							if(runner.data.status_id == DI.data.status_id) {
								return runner;
							}
							runner = runner.prev;
						}
						return null;
					}
					DI.isFirst = function() {
						var first = DI.findFirstInStatus();
						if(DI.data.item_id === first.data.item_id) {
							return true;
						}
						return false;
						// return DI.data.item_id == DI.parent.firstEle.data.item_id;
					}
					DI.isLast = function() {
						var last = DI.findLastInStatus();
						if(DI.data.item_id === last.data.item_id) {
							return true;
						}
						return false;
						// return DI.data.item_id == DI.parent.lastEle.data.item_id;
					}
					DI.moveFirst = function() {						
						//var firstEle = DI.parent.firstEle;						
						var firstEle = DI.findFirstInStatus();
						DI.before(firstEle).linkBefore(firstEle);
						DI.setOrderNo(Common.getOrderNoBetween);
						DI.setDataChanged();
					}
					DI.moveLast = function() {
						// var lastEle = DI.parent.lastEle;
						var lastEle = DI.findLastInStatus();
						DI.after(lastEle).linkAfter(lastEle);
						DI.setOrderNo(Common.getOrderNoBetween);
						DI.setDataChanged();
					}
					DI.isRoot = false;

					if (!Array.isArray(DI.data.responsibles)) {
						DI.data.responsibles = [];
					}

					DI.editable = function () {	
						if(RM.options.ownCardRights) {						
							var cc = DI.data.responsibles.length;
							if(cc>0) {
								for(var i = 0;i<cc; i++) {
									if(DI.data.responsibles[i] == UserService.getCurrentUserId()) {
										return true;
									}
								}
							}							
						}
						return RM.options.cardRights;
					};

					DI.applyChanges = function () {
						var parent = RMService.itemSet[DI.data.parent_item_id];
						var status = parent.status[DI.data.status_id];

						if (typeof status === 'undefined') {
							status = parent.status[statusParent.firstEle.data.status_id];
						}

						var runner = parent.firstEle;
						var putAfter = undefined;

						if (typeof runner !== 'undefined') {
							while (typeof runner !== 'undefined') {
								if (DI.data.order_no > runner.data.order_no &&
									DI.data.status_id == runner.data.status_id
								) {
									putAfter = runner;
								}

								runner = runner.next;
							}
						}

						if (typeof putAfter === 'undefined') {
							DI.linkFirst(parent);
							DI.$ele.prependTo(status.$ele);
						} else {
							DI.linkAfter(putAfter);
							DI.after(putAfter);
						}

						
						return DI.updateFields();
					};

					DI.append = function (dontLink) {
						var parent = RMService.itemSet[DI.data.parent_item_id];
						if (!dontLink) {
							DI.linkLast(parent);
						}

						var status = parent.status[DI.data.status_id];
						if (typeof status === 'undefined') {
							status = parent.status[statusParent.firstEle.data.status_id];
						}

						DI.$ele.appendTo(status.$ele);
						DI.show();

						return DI;
					};

					DI.setColor = function (code, colourClass) {
						DI.$ele.removeClass(Colors.getColor(DI.data.color).background);
						DI.data.color = code;
						DI.$ele.addClass(colourClass);
						DI.setDataChanged();
					};

					DI.setUserIcons = function () {
						UserService.getAvatars(DI.data.responsibles, {
							limit: 2,
							size: 'smaller'
						}).then(function ($avatars) {
							if (typeof DI.$users !== 'undefined') {
								DI.$users.remove();
							}

							DI.$users = $avatars.appendTo(DI.$ele);
						});
					};

					DI.RM = RM;
					DI.$ele = $('<div class="type0 ' + Colors.getColor(DI.data.color).background + '" rowid="' + RMService.sanitize(DI.data.item_id) + '">');
					DI.fields = {};
					DI.fields.title = $('<p class="title ellipsis">' + RMService.sanitize(DI.data.title) + '</p>').appendTo(DI.$ele);
					DI.fields.description = $('<p class="description caption">' + RMService.sanitize(DI.data.description) + '</div>').appendTo(DI.$ele);

					if (DI.editable()) {
						$('<div class="editTask btn btn-icon material-icons">more_vert</div>').appendTo(DI.$ele);
					}

					DI.show();
					DI.setUserIcons();

					if (DI.editable()) {
						DI.$ele.droppable({
							accept: '[draggable-process]',
							drop: function (event, ui) {								
								if (draggedProcess === 'user') {
									var userId = ui.draggable.attr('itemId');
									var i = DI.data.responsibles.length;
									while (i--) {
										if (DI.data.responsibles[i] == userId) {
											break;
										}
									}

									if (i === -1) {
										DI.data.responsibles.push(userId);
										DI.setUserIcons();
										DI.setDataChanged();
									}
								}								
							}
						});
					}

					DI.addFieldWithData('title', {DataType: 0});
					DI.addFieldWithData('description', {DataType: 4});

					$.each(DI.fields, function (i) {
						DI.fields[i].$ele = DI.$ele.find('p.' + i);
					});
					cardLimiter(DI.data.status_id, undefined);
					return DI;
				};

				var newCard = function (data) {
					var d = $q.defer();
					$scope.newItem()('task', data).then(function (innerData) {
						var DI = initCard(new RMDIService.dataItem(RM, innerData.Data)).applyChanges();
						RMService.itemSet[DI.data.item_id] = DI;
						cardLimiter(DI.data.status_id, undefined);
						d.resolve(DI);						
					});
					return d.promise;
				};

				var newRow = function (data) {
					var d = $q.defer();
					$scope.newItem()('task', data).then(function (innerData) {
						var DI = initRow(new RMDIService.dataItem(RM, innerData.Data)).applyChanges();
						RMService.itemSet[DI.data.item_id] = DI;

						d.resolve(DI);
					});
					return d.promise;
				};

				var initRow = function (DI) {
					if (DI.data.task_type != 1) {
						return undefined;
					}

					DI.editable = function () {						
						return RM.options.sprintRights;
					};
					DI.isRoot = false;
					DI.applyChanges = function () {
						var runner = rootParent.firstEle;
						var putAfter = undefined;
						while (typeof runner !== 'undefined') {
							if (DI.data.order_no > runner.data.order_no) {
								putAfter = runner;
							} else {
								break;
							}

							runner = runner.next;
						}

						if (typeof putAfter === 'undefined') {
							DI.linkFirst(rootParent);
							RM.$header.after(DI.$ele);
						} else {
							DI.linkAfter(putAfter).after(putAfter);
						}

						return DI.updateFields();
					};

					DI.append = function () {
						DI.$ele.appendTo($body);
						DI.linkLast(rootParent);
						DI.show();

						return DI;
					};

					DI.RM = RM;
					DI.status = {};

					if (typeof DI.$ele !== 'undefined') {
						DI.$ele.remove();
					}

					DI.fields = {};
					DI.$ele = $('<tr class="type1" rowid="' + RMService.sanitize(DI.data.item_id) + '"></tr>');

					var $backlog = $('<td class="backlog mode' + $scope.options.mode + '"></td>').appendTo(DI.$ele);

					if ($scope.options.mode != 1) {
						DI.fields.title = $('<p class="title ellipsis">' + RMService.sanitize(DI.data.title) + '</p>').appendTo($backlog);
						if (DI.editable() || RM.options.cardRights) {
							$('<div class="rmEditItem btn btn-icon material-icons">more_vert</div>').appendTo($backlog);
						}
						DI.fields.description = $('<p class="description caption">' + RMService.sanitize(DI.data.description) + '</p>').appendTo($backlog);
					}

					DI.show();

					var runner = statusParent.firstEle;
					while (typeof runner !== 'undefined') {
						DI.status[runner.data.status_id] = {};
						DI.status[runner.data.status_id].$ele = statusParent[runner.data.status_id].getStatusCell().appendTo(DI.$ele);
						runner = runner.next;
					}

					DI.addFieldWithData('title', {DataType: 0});
					DI.addFieldWithData('description', {DataType: 4});

					$.each(DI.fields, function (i) {
						DI.fields[i].$ele = DI.$ele.find('p.' + i);
					});

					return DI;
				};

				var kanbanStatusOptions;
				ProcessService.getListItems('list_task_status', true).then(function (items) {
					kanbanStatusOptions = items;
				});

				var initDefaultStatuses = function () {
					var promises = [];
					var l = RM.options.defaultStatus.length;
					for (var i = 0; i < l; i++) {
						promises.push(RMService.newKanbanPhase(RM.options.defaultStatus[i], i + 1));
					}

					return $q.all(promises).then(function (statuses) {
						angular.forEach(statuses, function (status) {
							initPhaseDI(status).appendStatus();
						});
					});

				};

				var getOrderAfter = function (prevId) {
					var prevOrder = undefined;
					var nextOrder = undefined;

					if (typeof prevId !== 'undefined') {
						var prev = RMService.itemSet[prevId];
						var next = prev.next;
						prevOrder = RMService.itemSet[prevId].data.order_no;

						if (typeof next !== 'undefined' && prev.data.status_id == next.data.status_id) {
							nextOrder = RMService.itemSet[prevId].next.data.order_no;
						}
					}

					return Common.getOrderNoBetween(prevOrder, nextOrder);
				};

				var getOrderBefore = function (nextId) {
					var prevOrder = undefined;
					var nextOrder = undefined;

					if (typeof nextId !== 'undefined') {
						var next = RMService.itemSet[nextId];
						var prev = next.prev;
						nextOrder = next.data.order_no;

						if (typeof prev !== 'undefined' && prev.data.status_id == next.data.status_id) {
							prevOrder = prev.data.order_no;
						}
					}

					return Common.getOrderNoBetween(prevOrder, nextOrder);
				};
				var addButtonsOnEmpty = function ($empty) {
					if ($scope.options.mode != 1) {
						var $btnArea = $empty.find(".buttonArea");
						var btnHtml = "<span class='addNewRow'>" + RMService.sanitize(Translator.translate("KANBAN_ROW")) + "</span>";
						$btnArea.append(btnHtml);
						$empty.on("mouseup", ".addNewRow", function (e) {
							createNewRow();
						});
					}
				};
				var initTable = function (s) {
					var statusCount = s.length;
					var redoOrder = false;

					RM.$fHeaderEle = $('<table class="kanbanFloatingHeader"><tr></tr></table>').prependTo(RM.$ele);
					RM.$header = $('<tr>').appendTo($body);
					RM.$header.append('<th class="backlog mode' + $scope.options.mode + '">&nbsp;</th>');

					RM.$toolbar = RM.$fHeaderEle.find('tr').first();
					RM.$toolbar.append('<th class="backlog mode' + $scope.options.mode + '">&nbsp;</th>');
					if( RM.options.sprintRights) {
						RM.$emptyEle = $('<div class="empty">' + RM.emptyContent + '</div>').insertAfter($body);
						addButtonsOnEmpty(RM.$emptyEle);
					}
					for (var i = 0; i < statusCount; i++) {
						if (s[i].order_no.length < 1) {
							redoOrder = true;
						}

						initPhaseDI(s[i]).appendStatus();
					}

					if (redoOrder) {
						var newOrdersNumbers = Common.fixOrders(statusCount);
						var runner = statusParent.firstEle;

						var i = 0;
						while (typeof runner !== 'undefined') {
							runner.data.order_no = newOrdersNumbers[i++];
							RMService.setStatusChanged(runner.data);
							runner = runner.next;
						}
					}

					if (typeof statusParent.firstEle === 'undefined') {
						return initDefaultStatuses();
					} else {
						var d = $q.defer();
						d.resolve();
						return d.promise;
					}

				};

				var initPhaseDI = function (data) {
					var phaseDI = new RMDIService.baseDI();
					var phaseId = 'status' + data.status_id;
					RMService.itemSet[phaseId] = phaseDI;

					phaseDI.editable = function () {
						return RM.options.sprintRights;
					};

					phaseDI.RM = RM;
					phaseDI.data = data;
					phaseDI.data.item_id = phaseId;
					phaseDI.parent = statusParent;
					phaseDI.getStatusHeader = function () {
						var headStr = '<th status="' + RMService.sanitize(data.status_id) + '" statusid="' + RMService.sanitize(phaseId) + '">' +
							'<p class="title caption ellipsis">' + RMService.sanitize(data.title) + '</p>';

						if (phaseDI.editable()) {
							headStr += '<div class="editStatus btn btn-icon material-icons">more_vert</div>';
						}

						headStr += '</th>';

						return $(headStr);
					};

					phaseDI.getHoverHeader = function () {
						return phaseDI.getStatusHeader();
					};

					phaseDI.getStatusCell = function () {
						return $('<td status="' + RMService.sanitize(data.status_id) + '"></td>');
					};

					phaseDI.appendStatus = function () {
						phaseDI.getStatusHeader().appendTo(RM.$header);
						phaseDI.getHoverHeader().appendTo(RM.$toolbar);

						var runner = rootParent.firstEle;
						while (typeof runner !== 'undefined') {
							runner.status[phaseDI.data.status_id] = {};
							runner.status[phaseDI.data.status_id].$ele = phaseDI.getStatusCell().appendTo(runner.$ele);
							runner = runner.next;
						}

						if (typeof statusParent.lastEle === 'undefined') {
							statusParent.firstEle = phaseDI;
							statusParent.lastEle = phaseDI;
						} else {
							phaseDI.linkAfter(statusParent.lastEle);
						}

						return phaseDI
					};

					phaseDI.after = function (id) {
						RM.$header.find('th[status=' + id + ']').after(phaseDI.getStatusHeader());
						RM.$toolbar.find('th[status=' + id + ']').after(phaseDI.getHoverHeader());

						var runner = rootParent.firstEle;
						while (typeof runner !== 'undefined') {
							runner.status[phaseDI.data.status_id] = {};
							runner.status[phaseDI.data.status_id].$ele = phaseDI.getStatusCell().insertAfter(runner.status[id].$ele);
							runner = runner.next;
						}

						phaseDI.linkAfter(statusParent[id]);
					};

					phaseDI.before = function (id) {
						RM.$header.find('th[status=' + id + ']').before(phaseDI.getStatusHeader());
						RM.$toolbar.find('th[status=' + id + ']').before(phaseDI.getHoverHeader());

						var runner = rootParent.firstEle;
						while (typeof runner !== 'undefined') {
							runner.status[phaseDI.data.status_id] = {};
							runner.status[phaseDI.data.status_id].$ele = phaseDI.getStatusCell().insertBefore(runner.status[id].$ele);
							runner = runner.next;
						}

						phaseDI.linkBefore(statusParent[id]);
					};

					phaseDI.delete = function () {
						phaseDI.unlink();
						$element.find('[status=' + phaseDI.data.status_id + ']').remove();
						RM.$toolbar.find('[status=' + phaseDI.data.status_id + ']').remove();

						var rowRunner = rootParent.firstEle;
						while (typeof rowRunner !== 'undefined') {
							rowRunner.status[phaseDI.data.status_id] = undefined;
							var itemRunner = rowRunner.firstEle;

							while (typeof itemRunner !== 'undefined') {
								if (itemRunner.data.status_id == phaseDI.data.status_id) {
									itemRunner.append();
								}
								itemRunner = itemRunner.next;
							}

							rowRunner = rowRunner.next;
						}
					};

					statusParent[data.status_id] = phaseDI;
					addStatusInLimiter(data.status_id);					
					return phaseDI;
				}

			}
		}
	}
})();
