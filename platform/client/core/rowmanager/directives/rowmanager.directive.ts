﻿var getCount;
(function () {
	'use strict';
	angular
		.module('core.rowmanager')
		.directive('rowManager', RowManager);

	RowManager.$inject = [
		'RMService',
		'RMDIService',
		'RMSortService',
		'Common',
		'MessageService',
		'$timeout',
		'ProcessService',
		'PortfolioService',
		'Translator'
	];

	function RowManager(
		RMService,
		RMDIService,
		RMSortService,
		Common,
		MessageService,
		$timeout,
		ProcessService,
		PortfolioService,
		Translator
	) {
		return {
			scope: {
				portfolioId: '@',
				rules: '<',
				portfolioColumns: '<',
				options: '<',
				rows: '=',
				onChange: '&?',
				refreshCalculations: '@?',
				newItem: '&?'
			},
			template: '<table class="rowmanager-body row-manager"></table>',
			transclude: true,
			link: function (scope, element, attr, controller, transclude) {
				transclude(scope, function (clone) {
					$timeout(function () {
						angular.forEach(clone, function (template) {
							let type = template.getAttribute('type');

							if (template.nodeName === 'GROUP-TEMPLATE') {
								switch (type) {
									case 'header-group':
										angular.forEach(template.children, function (t) {
											t.classList.add('header-group');
											controller.RM.headerGroups[t.getAttribute('groupid')] = t;
										});

										break;
								}
							} else {
								let regex = /(<\/*table(.|\n)*?>|<\/*tbody(.|\n)*?>|\<!--(.|\n)*?-->)+/gi;
								let str = template.innerHTML.replace(regex, ' ');

								switch (type) {
									case 'empty':
										// if (!controller.RM.options.readOnly) {
										controller.RM.$emptyEle = $(str);
										// }
										break;
									case 'addrow':
										if (!controller.RM.options.readOnly) {
											controller.RM.$addrowEle = $(str);
										}
										break;
									case 'header':
										let template = str.split(/[\[\]]/g);
										let html = '';
										let cc = template.length - 1;

										for (let i = 0; i < cc; i += 2) {
											let name = template[i + 1];
											let pc = ' data-column';
											html += template[i];
											if (i == 0) {
												html += "<a class='openAll btn btn-icon flip hide'><i class='material-icons'>expand_more</i></a>";
											}
											html += '<div class="' + RMService.sanitize(controller.RM.getFieldCls(name)) + pc +
												'" tooltip="' + RMService.sanitize(controller.RM.getFieldTip(name)) + '">' + RMService.sanitize(controller.RM.getFieldTitle(name)) + '</div>';
										}

										html += template[cc];

										if (controller.RM.options.showCalendar) {
											html = html.replace(/<\/tr>/g, '<th style="position: relative; width: 50%"></th></tr>');
										}

										controller.RM.$headerEle = $(html);
										controller.RM.$fHeaderEle = $('<table class="rowmanager-floating-header">' + html + '</table>').prependTo(element);

										for (let i = 0; i < cc; i += 2) {
											let name = template[i + 1];
											let $ele = controller.RM.$headerEle.find('div.' + name).parent();
											let $hEle = controller.RM.$fHeaderEle.find('div.' + name).parent();
											controller.RM.setFieldWidth(name, $ele, $hEle);
										}

										controller.RM.$headerEle.appendTo(element.find('.rowmanager-body'));
										break;
									default:
										controller.RM.templates[type] = str.split(/[\[\]]/g);
								}
							}
						});
						controller.initializeRowManager();
					}, 0);
				});
			},
			controller: function ($scope, $element) {
				let RM = new RMService.managerTemplate();

				RM.options = $.extend({
					hideOpenAll: false,
					currentUserId: 0,
					skipNewItemInitialisation: false,
					readOnly: false,
					showCalendar: false,
					menuItems: {},
					alias: {},
					buttons: {},
					hiddenColumns: undefined,
					rowClick: undefined,
					inheritDates: false,
					sumDatas: false,
					openAll: false,
					showUserName: false,
					sortable: false,
					ownerId: 0,
					ownerType: 'rm',
					selectable: true,
					process: 'task'
				}, $scope.options);

				let uniqueProcesses = [RM.options.process];
				RM.rules = $scope.rules;

				if (typeof RM.rules === 'undefined') {
					RM.rules = {};
				}

				RM.options.currentUserId = parseInt(RM.options.currentUserId);

				let rootParent = {
					getCurrentIndex: function () {
						return 0;
					},
					isRoot: true,
					ownerId: RM.options.ownerId,
					ownerType: RM.options.ownerType,
					data: {
						owner_item_id: RM.options.ownerId,
						owner_item_type: RM.options.ownerType,
						item_id: RM.options.ownerId
					}
				};

				RM.setValues(rootParent, $scope.rows);
				if ($scope.$parent && $scope.$parent.$parent && $scope.$parent.$parent.$parent) RM.$parent = $scope.$parent.$parent.$parent;

				if (RM.$parent == null) RM.$parent = $scope.$parent;

				RM.$addrowEle = $('<tr class="rowmanager-header" style="height:0;"><td colspan="100%" style="height:0;"></td></tr>');
				RM.$emptyEle = $('<tr class="rowmanager-header" style="height:0;"><td colspan="100%" style="height:0;"></td></tr>');

				RM.$headerEle = $();
				RM.$columnEle = undefined;
				RM.templates = {};
				RM.headerGroups = {};
				RM.$headerGroups = {};
				RM.portfolioId = $scope.portfolioId;
				this.RM = RM;
				RM.$ele = $element;
				RM.createFieldsCache($scope.portfolioColumns);

				let draggedProcess;
				let savedSettings = PortfolioService.getActiveSettings($scope.portfolioId);
				let tdCount = 1;
				RM.parentCount = function() {
					var i = 0;
					var runner = rootParent.firstEle;
					while(runner != undefined) {
						runner = runner.next;
						i++;
					}
					return i;
				}
				RM.emptyVisibility = function() {
					if(RM.parentCount()>0) {
						RM.$emptyEle.hide();
					} else {
						RM.$emptyEle.show();
					}
				}
				getCount = RM.parentCount;

				$scope.$on('DragProcessStart', function (event, process) {
					if (process !== 'user') return false;
					draggedProcess = process;
					let css = '';
					$.each(RM.rules.accepts, function (key, acceptedProcesses) {
						for (let i = 0; i < acceptedProcesses.length; i++) {
							if (acceptedProcesses[i] === 'user') {
								css += ', tr.type' + key + ' div.users';
							}
						}
					});
					if (css.length > 2) {
						css = css.substr(2, css.length);
					}
					RM.$ele.find(css).addClass('placeholder-user');
				});

				$scope.$on('DragProcessStop', function () {
					$element.find('.rowmanager-body div.users').removeClass('placeholder-user');
				});

				RMSortService.addMasterClass('row-manager');

				$scope.isValid = function () {
					return true;
				};

				RM.selectedRows = {};
				let unSelectRows = function () {
					$.each(RM.selectedRows, function (i, DI) {
						if (typeof DI !== 'undefined') {
							DI.isSelected = false;
							DI.$ele.removeClass('selected');
						}
					});

					RM.selectedRows = {};
				};
				RM.hasSelectedRows = function () {
					return Object.keys(RM.selectedRows).length > 0;
				};

				let initRow = function (DI) {
					DI.RM = RM;
					if(DI.RM.rules.forceAllOpen){
						DI.wasOpen = true;
					} else {
						DI.loadWasOpen($scope.portfolioId);
					}
					DI.inheritDates = RM.options.inheritDates;
					DI.sumDatas = RM.options.sumDatas;
					let template = DI.RM.templates[DI.data.task_type];

					if (RM.options.openAll) {
						DI.isOpenable = function () {
							return true;
						};
					}

					if (typeof template === 'undefined') {
						template = DI.RM.templates['default'];
					}

					if (typeof template !== 'undefined') {
						let cc = template.length - 1;
						let html = '';
						for (let i = 0; i < cc; i += 2) {
							let name = template[i + 1];
							DI.addField(name);
							html += template[i];
							let pc = ' data-column';
							if (i == 0) {
								html += "<a class='openRow btn btn-icon flip'><i class='material-icons'>expand_more</i></a>";
								pc += ' first-column';
							}

							html += '<div class="' + RMService.sanitize(DI.RM.getFieldCls(name)) + pc + '"></div>';
						}

						html += template[cc];

						if (RM.options.showCalendar) {
							html = html.replace(/<\/tr>/g, '<td></td></tr>');
						}
						DI.$ele = $(html);
						tdCount = DI.$ele.children().length;

						if (RM.options.showCalendar) {
							tdCount--;
						}
						DI.$statusBar = $('<div class=\'hidden\'>&nbsp;</div>').prependTo(DI.$ele.find('td:first'));
						DI.$users = DI.$ele.find('.users');
						DI.assignRowValues(RM.options.showUserName);
						DI.$ele.attr('rowid', DI.data.item_id);
						DI.$ele.addClass('rmTarget');
						if (typeof DI.data.addclass !== "undefined") {
							DI.$ele.addClass(DI.data.addclass);
						}
						DI.$opener = DI.$ele.find('.openRow');
						if (RM.options.showCalendar) {
							DI.RM.initCalendarRow(DI);
							let startDate = RMService.alias[3];
							if (typeof DI.data[startDate] !== 'undefined' && DI.data[startDate] !== null) {
								let d1 = new Date(DI.data[startDate]);
								DI.data[startDate] = new Date(Date.UTC(d1.getFullYear(), d1.getMonth(), d1.getDate()));
							}
							let endDate = RMService.alias[4];
							if (typeof DI.data[endDate] !== 'undefined' && DI.data[endDate] !== null) {
								let d2 = new Date(DI.data[endDate]);
								DI.data[endDate] = new Date(Date.UTC(d2.getFullYear(), d2.getMonth(), d2.getDate()));
							}

							let bar = DI.RM.initCalendarBar(DI, 'ganttBar');

							bar.drawBarIcon = function () {
								if (typeof DI.data.bar_icon !== 'undefined' && DI.data.bar_icon !== null && DI.data.bar_icon.length > 0) {
									let icon = ProcessService.getCachedItemData(DI.data.bar_icon[0]);
									if (typeof icon != 'undefined' && icon.Data && icon.Data.list_symbol) {
										let color;
										if (icon.Data.list_symbol_fill !== null) {
											color = RMService.getColor(icon.Data.list_symbol_fill)
										}
										bar.addIcon(icon.Data.list_symbol, color);
									} else {
										bar.removeIcon();
									}
								} else {
									bar.removeIcon();
								}
							};
							if (typeof DI.data.bar_icons !== 'undefined' && DI.data.bar_icons !== null) {
								DI.RM.initCalendarIcons(DI, DI.data.bar_icons, "description");
							}
						}

						DI.setUserIcons();
						DI.showHighlight();
						DI.initLock();
						if (!RM.options.readOnly) {
							DI.$ele.droppable({
								accept: '[draggable-process]',
								drop: function (event, ui) {
									if (RM.rules.accepts[DI.data.task_type] == draggedProcess) {
										let userId = ui.draggable.attr('item-id');

										let cc = DI.data.responsibles.length;
										let hasAlready = false;

										for (let i = 0; i < cc; i++) {
											if (DI.data.responsibles[i] == userId) hasAlready = true;
										}

										if (!hasAlready) {
											if (!Array.isArray(DI.data.responsibles)) {
												DI.data.responsibles = [];
											}

											DI.data.responsibles.push(userId);
										}
										DI.setUserIcons();
										DI.setDataChanged();
									}
								}
							});
						}
					}

					return DI;
				};

				this.initializeRowManager = function () {
					/* ___  __  ___  ______               __     __
					  / _ \/  |/  / /_  __/__ __ _  ___  / /__ _/ /____
					 / , _/ /|_/ /   / / / -_)  ' \/ _ \/ / _ `/ __/ -_)
					/_/|_/_/  /_/   /_/  \__/_/_/_/ .__/_/\_,_/\__/\__/
				   /*/
					RM.setVisibility(typeof RM.options.hiddenColumns === 'undefined' ?
						savedSettings.filters.columns : RM.options.hiddenColumns
					);

					RM.init = function (DI) {
						return initRow(DI);
					};

					RM.initNew = function (data) {
						return RM.init(new RMDIService.dataItem(RM, data));
					};

					RM.setRules(RM.rules);

					RM.setRowParent = function (DI) {
						let ti = RM.previousRow.getCurrentIndex();
						let previousParent = DI.parent;

						if (ti == RM.validIndex) {
							DI.linkAfter(RM.previousRow).setOrderNo(Common.getOrderNoBetween);
						} else if (ti > RM.validIndex) {
							let dif = ti - RM.validIndex;
							let runner = RM.previousRow;
							while (dif--) {
								runner = runner.parent;
							}
							DI.linkAfter(runner).setOrderNo(Common.getOrderNoBetween);
						} else {
							DI.linkFirst(RM.targetParent).setOrderNo(Common.getOrderNoBetween);
						}

						if (!previousParent.isRoot) {
							previousParent.setPadding()
						}

						DI.setPadding(RM.targetParent.getCurrentIndex() + 1);

						if (!RM.targetParent.isRoot) {
							RM.targetParent.setPadding()
						}

						RMService.inheritParent(DI.data, RM.targetParent.data);
						DI.parent = RM.targetParent;
						DI.setDataChanged();
					};

					RM.setSelectMode = function (set) {
						rmMenu.closeMenu();
						if (set) {
							RM.$parent.showSelection = true;
							RM.$parent.$digest();
						} else {
							unSelectRows();
							RM.$parent.showSelection = false;
						}
						RM.selectMode = set;
					};

					RM.getPlaceholder = function () {
						RM.$placeholder = $('<tr class="transition-in"><td class="placeholder" colspan="' +
							tdCount + '"><div>&nbsp;</div></td></tr>');
						return RM.$placeholder;
					};

					RM.initializeResize($scope, RM.$ele);
					RMService.addRM(RM.id, RM);

					$scope.$watchCollection(function () {
						return savedSettings.filters.columns;
					}, function (n, o) {
						if (n !== o) {
							RM.setVisibility(typeof RM.options.hiddenColumns === 'undefined' ?
									savedSettings.filters.columns : RM.options.hiddenColumns,
								$element,
								RM.$fHeaderEle
							);
						}
					});

					RM.initVisibility(RM.$fHeaderEle);
					RM.initVisibility(RM.$headerEle);
					if (RM.options.showCalendar) {
						RM.initCalendar(RM.$ele, $scope);
					}

					let redoOrders = false;
					let checkWasOpen = [];
					let parentRows = [];
					let childRows = [];

					if (RM.options.readOnly) {
						RM.$addrowEle.hide();
					}
					// if (RM.options.readOnly) {
					// 	RM.$emptyEle.hide();
					// }

					RM.$emptyEle.appendTo(RM.$ele.find('.rowmanager-body'));
					RM.$addrowEle.appendTo(RM.$ele.find('.rowmanager-body'));
					RM.addButtonsOnEmpty(RM.$addrowEle, RM.rules, RM.options);
					RM.initLock(RM.$ele);
					/* __                  _             ___               
					  / /  ___  ___  ___  (_)__  ___ _  / _ \___ _    _____
					 / /__/ _ \/ _ \/ _ \/ / _ \/ _ `/ / , _/ _ \ |/|/ (_-<
					/____/\___/\___/ .__/_/_//_/\_, / /_/|_|\___/__,__/___/
					              /_/          /__*/
					let itemCount = $scope.rows.length;

					for (let i = 0; i < itemCount; i++) {
						var rowdata = new RMDIService.dataItem(RM, $scope.rows[i]);

						let DI = initRow(rowdata);
						let itemId = DI.data.item_id;
						if (DI.data.process && uniqueProcesses.indexOf(DI.data.process) === -1) {
							uniqueProcesses.push(DI.data.process);
						}
						if (typeof RMService.alias[12] != "undefined") {// sumUp
							for (var j = 0; j < RMService.alias[12].length; j++) {
								var name = RMService.alias[12][j]
								var nameSum = RMService.alias[12][j] + "_sum"
								DI.data[nameSum] = DI.data[name];
							}
						}
						if (DI.data.parent_item_id == rootParent.data.item_id) {
							DI.append(true).linkLast(rootParent);
							checkWasOpen.push(DI);
							parentRows.push(itemId);
						} else {
							childRows.push(itemId);
						}

						if (typeof DI.data.order_no === 'undefined' || DI.data.order_no === null || DI.data.order_no.length === 0) {
							redoOrders = true;
						}
					}

					if (redoOrders) {
						let newOrdersNumbers = Common.fixOrders(itemCount);
						for (let i = 0; i < itemCount; i++) {
							let DI = RMService.itemSet[$scope.rows[i].data.item_id];

							DI.data.order_no = newOrdersNumbers[i];
							DI.setDataChanged();
						}
					}
					for (let i = 0; i < parentRows.length; i++) {
						let DI = RMService.itemSet[parentRows[i]];
						DI.inParent(rootParent);

					}
					if(parentRows.length<1) {
						RM.$emptyEle.show();
					}
					for (let i = 0; i < childRows.length; i++) {
						let DI = RMService.itemSet[childRows[i]];
						let parent = RMService.itemSet[DI.data.parent_item_id];
						DI.inParent(parent);

					}

					for (let i = 0; i < checkWasOpen.length; i++) {
						checkWasOpen[i].setPadding();
						if (checkWasOpen[i].wasOpen) {
							checkWasOpen[i].open();
						}
					}

					RM.initializeCalendar();
					RM.initializeArrows();
					RM.countAndUpdateAllRows();
					RM.$ele.show();

					//This updates things like sumups on first load..
					if ($scope.refreshCalculations === "true") RM.countAndUpdateAllRows();

					/* ___     __   __  _  __             ______
					  / _ |___/ /__/ / / |/ /__ _    __  /  _/ /____ __ _
					 / __ / _  / _  / /    / -_) |/|/ / _/ // __/ -_)  ' \
					/_/ |_\_,_/\_,_/ /_/|_/\__/|__,__/ /___/\__/\__/_/_/*/
					RM.addNewItem = function (type, parent) {
						let data = RMService.getDefaultDIData(RM.options.alias[type], type, RM.options.projectStartDate);

						let lastOrder = undefined;
						let targetParent = parent;
						if (typeof parent !== 'undefined' && !parent.isRoot) {
							if (typeof parent.lastEle !== 'undefined') {
								lastOrder = parent.lastEle.data.order_no;
							}
						} else {
							if (typeof rootParent.lastEle !== 'undefined') {
								lastOrder = rootParent.lastEle.data.order_no;
							}
							targetParent = rootParent;
						}

						RMService.inheritParent(data, targetParent.data);
						data.order_no = Common.getOrderNoBetween(lastOrder, undefined);
						return $scope.newItem()(RM.options.process, data).then(function (innerData) {
							if (RM.options.skipNewItemInitialisation) {
								return;
							}
							//ProcessService.newItem(RM.options.process, {Data: data}).then(function (id) {
							let DI = initRow(new RMDIService.dataItem(RM, innerData.Data));

							if (typeof parent !== 'undefined') {
								DI.inParent(parent).setDataFromChildren();
							} else {
								DI.append().linkLast(targetParent);
							}

							DI.calendarRow.updateArrows();
							RM.setCalendarHeight();
							DI.setPadding(DI.getCurrentIndex());
							DI.initVisibility();

							RM.addRow(DI.data);
							if (typeof parent !== 'undefined' && !parent.isRoot) {
								if (!parent.isOpen) {
									parent.open();
									RM.setCalendarHeight();
								}
							}
							RM.emptyVisibility();
							return DI;
						});
					};

					RM.addNewRow = function (event) {
						let menuItems = [];
						angular.forEach(RM.rules.parents, function (value, key) {
							for (let i = 0, l = value.length; i < l; i++) {
								if (value[i] === 'root') {
									let alias = typeof RM.options.alias[key] === 'undefined' ?
										Translator.translate('RM_ITEM') : RM.options.alias[key];
									let item = {
										name: '+' + alias, onClick: function () {
											RM.addNewItem(key);
										}
									};

									menuItems.push(item);
									break;
								}
							}
						});

						MessageService.menu(menuItems, event.currentTarget);
						RM.emptyVisibility();
					};
					$scope.$parent.addNewItem = RM.addNewRow;
					RMDIService.bindSubscribeChanged(uniqueProcesses, $scope);

					/* __ __             ____
					  / // /__ ____  ___/ / /__ _______
					 / _  / _ `/ _ \/ _  / / -_) __(_-<
					/_//_/\_,_/_//_/\_,_/_/\__/_/ /__*/
					let rmMenu = new function () {
						let menu = new RMService.rowMenu(RM.options, RM);
						menu.addNewItem = function (type, DI) {
							RM.addNewItem(type, DI);
						};
						return menu;
					};

					RM.options.buttons.openRow = function (id) {
						let DI = RMService.itemSet[id];

						if (DI.isOpen) {
							DI.close();
						} else {
							DI.open();
						}

						RM.setCalendarHeight();
					};

					if (RM.options.sortable) {
						RMSortService.bindRowDrag({
							$element: RM.$ele,
							selector: 'tr.rmTarget',
							hoverMode: 'v',
							wrap: '<table class=\'rmDragged\'></table>'
						});
					}

					RM.initializeScroll($scope);

					if (Object.keys(RM.headerGroups).length) {
						RM.$ele.children().addClass('margin-group');
					}

					RMService.clickHandler({
						RM: RM,
						menu: rmMenu,
						options: RM.options,
						$element: RM.$ele
					});

					RM.$parent.clickSelStop = function () {
						if (RM.selectMode) {
							RM.setSelectMode(false);
						}
					};

					RM.$parent.getRMSelectedRows = function () {
						if (RM.selectMode) {
							return RM.selectedRows;
						}
					};

					$scope.$parent.clickSelDel = function () {
						MessageService.confirm('Are you sure you want to delete all selected rows?', function () {
							RMService.clipBoard.del(RM);
							RM.setCalendarHeight();
							RM.setSelectMode(false);
						});
					};

					$scope.$parent.clickSelCut = function () {
						RMService.clipBoard.cut(RM.selectedRows);
						RM.setSelectMode(false);
					};

					$scope.$parent.clickSelCopy = function () {
						RMService.clipBoard.copy(RM.selectedRows);
						RM.setSelectMode(false);
					};

					$scope.$parent.clickSelModify = function () {
						MessageService.dialogAdvanced({
							controller: 'RowManagerEditController',
							template: 'core/rowmanager/dialogs/edit/edit.html',
							locals: {
								SelectedRows: RM.selectedRows
							}
						});

						RM.setSelectMode(false);
					};
				}
			}
		}
	}
})();
