(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.directive('liteManager', LiteManager);

	LiteManager.$inject = [
		'PortfolioService',
		'$q',
		'Common',
		'Translator',
		'ProcessService',
		'RMService',
		'ViewService'
	];

	function LiteManager(
		PortfolioService,
		$q,
		Common,
		Translator,
		ProcessService,
		RMService,
		ViewService
	) {
		return {
			scope: {
				restrictions: '<?',
				label: '@',
				modify: '<'
			},
			templateUrl: 'core/rowmanager/directives/lite.html',
			link: function (scope, element, attrs) {
				let featuretteWidth = attrs.openable;
				let confs = scope.restrictions.options;
				let ownerColumnId = confs.ownerColumnId;
				let parent = attrs.parent;
				if (attrs.parentfieldname && attrs.parentfieldname.length > 0) {
					let parentData = ProcessService.getCachedItemData(parent).Data;
					if (parentData && parentData[attrs.parentfieldname]) {
						parent = parentData[attrs.parentfieldname];
					}
				}

				let viewId = ViewService.getViewIdFromElement(element);
				let process = attrs.process;
				let restrictions = {};
				let actions = {};
				restrictions[ownerColumnId] = [parent];

				let promises = [];

				scope.loading = true;
				scope.addingRow = false;
				scope.portfolioColumns = [];
				scope.options = {
					skipNewItemInitialisation: confs.useDialog,
					readOnly: !scope.modify,
					openAll: true,
					selectable: false,
					ownerId: parent,
					featuretteWidth: featuretteWidth,
					ownerItemType: 'lite',
					process: process,
					alias: {},
					menuItems: {},
					sumDatas: true
				};
				scope.getArchive = function () {
					return attrs.archive;
				};

				if (confs.useDialog) {
					ProcessService.subscribeNewItem(scope, function () {
						scope.loading = true;

						getRows().then(function () {
							scope.loading = false;
						});
					}, process);
				}

				scope.newItem = function (process, data) {
					if (confs.useDialog) {
						ViewService.view('new.process', {
							params: {
								toast: 'LITE_NEW_ROW_CREATED',
								process: process,
								overwriteData: data,
								actionId: actions[data.task_type],
								containerId: _.find(scope.restrictions.definitions, function (o) {
									return o.type == data.task_type
								}).containerId
							}
						});

						let d = $q.defer();
						d.resolve();

						return d.promise;
					} else {
						// let $eRow = $(".rowmanager-body .empty");
						// $eRow.hide();
						scope.addingRow = true;
						return ProcessService.newItem(process, {Data: data}, actions[data.task_type]).then(function (id) {
							// $eRow.show();
							scope.addingRow = false;
							let result = ProcessService.getCachedItemData(process, id);
							ViewService.view('featurette',
								{
									params: {
										process: process,
										itemId: id,
										title: data.title
									}
								},
								{
									split: true,
									size: featuretteWidth
								}
							);
							result.Data.has_write_right = true;
							return result;
						});
					}
				};

				scope.rules = {
					parents: {},
					draggables: []
				};

				if(attrs.forceallopen && attrs.forceallopen == "true") scope.rules['forceAllOpen'] = true;

				scope.portfolioId = confs.portfolioId;

				if (Common.isTrue(confs.sortable)) {
					scope.options.sortable = true;
				}

				angular.forEach(scope.restrictions.definitions, function (r) {
					scope.rules.parents[r.type] = r.rules;

					if (r.draggable) {
						scope.rules.draggables.push(r.type);
					}

					if (process !== 'task') {
						scope.options.menuItems[r.type] = [openRowMenuItem()];
					}

					if (r.actionId) {
						actions[r.type] = r.actionId;
					}

					scope.options.alias[r.type] = Translator.translation(r.name);
				});

				function openRowMenuItem() {
					return {
						isActive: function () {
							return true;
						},
						name: Translator.translate('OPEN'),
						onClick: function (itemId) {
							let DI = RMService.itemSet[itemId];
							RMService.openFeaturette(DI, DI.RM.options.readOnly);
						}
					};
				}

				scope.$on('RefreshAllTables', function (event, opt) {
					getRows();
				});

				function getRows() {
					return PortfolioService.getPortfolio(process, scope.portfolioId).then(function (p) {
						let order_name = 'order_no';
						let order_dir = 'asc';
						if (p.OrderItemColumnName && p.OrderItemColumnName.length > 0 && p.OrderDirectionName && p.OrderDirectionName.length > 0) {
							order_name = p.OrderItemColumnName;
							order_dir = p.OrderDirectionName;
						}
						let options = { local: true, restrictions: restrictions, archive: undefined };
						if (scope.getArchive()) options.archive = scope.getArchive();
						return PortfolioService
							.getPortfolioData(
								process,
								scope.portfolioId,
								options,
								{ sort: [{ column: order_name, order: order_dir }], limit: 0 })
							.then(function (rows) {
								for (let r in rows.rowdata) {
									rows.rowdata[r].has_write_right = true;
								}
								scope.portfolioRows = rows.rowdata;
							});
					});
				}

				promises.push(PortfolioService.getPortfolioColumns(process, scope.portfolioId).then(function (columns) {
					for (let i = 0, l = columns.length; i < l; i++) {
						let name = columns[i].ItemColumn.Name;
						if (name === 'parent_item_id' || name === 'owner_item_id' || name === 'task_type') continue;

						let c = columns[i];
						if (c.ItemColumn.DataType == 16 &&
							(c.UseInSelectResult == 6 || c.UseInSelectResult == 9 || c.UseInSelectResult == 10 || c.UseInSelectResult == 11 || c.UseInSelectResult == 12)
						) ProcessService.ignoreColumn(process, name, true);
						scope.portfolioColumns.push(columns[i]);
					}
				}));

				promises.push(getRows());

				$q.all(promises).then(function () {
					scope.loading = false;
				});

				if (attrs.ignoreexcelexport == '' || Common.isFalse(attrs.ignoreexcelexport)) {
					let excelExportName = GetNameForExcelExport();
					ViewService.addDownloadable(excelExportName + ' -> Excel', function () {
						PortfolioService.getPortfolioExcel(
							process,
							scope.portfolioId,
							{ recursive: false, name: excelExportName, restrictions: restrictions },
							scope.portfolioColumns);
					}, viewId);
				}

				function GetNameForExcelExport() {
					if (scope.label != '') return scope.label;
					let containerTitle = Translator.translation(scope.$parent.$parent.$parent.grid.LanguageTranslation);
					if (containerTitle != '') return containerTitle;
					let headerTitle = scope.$parent.$parent.$parent.$parent.header.title;
					if (headerTitle != '') return headerTitle;
					return Translator.translate('PORTFOLIO_EXCEL_EXPORT_TABLE');
				}
			},
			controller: function ($scope) {
				if (typeof $scope.restrictions === 'undefined') {
					$scope.restrictions = {
						definitions: {},
						options: {}
					};
				}
			}
		};
	}
})();