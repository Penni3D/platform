﻿(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.config(KanbanConfig)
		.controller('KanbanController', KanbanController);

	KanbanConfig.$inject = ['ViewsProvider'];

	function KanbanConfig(ViewsProvider) {
		ViewsProvider.addView('process.kanban', {
			controller: 'KanbanController',
			template: 'core/rowmanager/views/kanban/kanban.html',
			level: 2,
			resolve: {
				Rows: function (StateParameters, States, PortfolioService, ViewConfigurator, Actions) {
					return PortfolioService.getTaskData(
						StateParameters.process,
						StateParameters.itemId,
						States.meta,
						StateParameters.processGroupId,
						'gantt',
						ViewConfigurator.getConfiguration('process.gantt', StateParameters.process).kanbanPortfolioId,
						Actions.ArchiveDates, StateParameters.rowId);
				},
				Sprint: function (ProcessService, StateParameters) {
					return ProcessService.getItem('task', StateParameters.rowId);
				},
				kanbanMode: function (ViewConfigurator, StateParameters) {
					return ViewConfigurator.getConfiguration('process.gantt', StateParameters.process).kanbanMode;
				}
			},
			params: {
				bypassFallback: true,
				returnView: 'process.gantt'
			},
			options: {
				split: true,
				replace: 'process.gantt',
				preventOn: ['process.gantt', 'process.sprint'],
				size: 'wider'
			}
		});
	}

	KanbanController.$inject = [
		'$scope',
		'StateParameters',
		'ViewService',
		'Rows',
		'ViewConfigurator',
		'CalendarService',
		'States',
		'Sprint',
		'UserService',
		'Translator',
		'ProcessService',
		'kanbanMode'
	];

	function KanbanController(
		$scope,
		StateParameters,
		ViewService,
		Rows,
		ViewConfigurator,
		CalendarService,
		States,
		Sprint,
		UserService,
		Translator,
		ProcessService,
		kanbanMode
	) {
		let configurations = ViewConfigurator.getConfiguration('process.gantt', StateParameters.process);
		let order = ['a', 'b', 'c', 'd', 'e', 'f'];
		let defaultStatuses = [];
		let runner = 0;

		$scope.rowId = StateParameters.rowId;
		$scope.rows = Rows.tasks;
		$scope.options = {
			cardRights: false,
			sprintRights: false,
			process: "task",
			ownerId: StateParameters.itemId,
			ownerType: 'gantt',
			mode: kanbanMode
		};

		if (StateParameters.cardId && StateParameters.cardId > 0) {
			ViewService.view('featurette', {
				params: {
					process: 'task',
					itemId: StateParameters.cardId,
					tabId: null
				}
			}, {
				split: true,
				size: StateParameters.splitSize
			})
		}

		$scope.newItem = function (process, data) {
			if (data.task_type === 1 && data.title === "New Task") data.title = "New Row";
			return ProcessService.newItem(process, {Data: data}).then(function (id) {
				return ProcessService.getCachedItemData(process, id);
			});
		};
		if(hasState("kanbanOwnCards")) {
			$scope.options.ownCardRights = true;
		}
		if (hasState('kanbanWrite')) {
			$scope.options.cardRights = true;
			$scope.options.sprintRights = true;
		} else if (hasState('kanbanCard')) {
			$scope.options.cardRights = true;
		} else {
			if (Array.isArray(Sprint.responsibles)) {
				angular.forEach(Sprint.responsibles, function (responsible) {
					if (responsible == UserService.getCurrentUserId()) {
						$scope.options.cardRights = true;
					}
				});
			}
		}

		addDefaultStatus(configurations.firstKanbanStatus);
		addDefaultStatus(configurations.secondKanbanStatus);
		addDefaultStatus(configurations.thirdKanbanStatus);
		addDefaultStatus(configurations.fourthKanbanStatus);
		addDefaultStatus(configurations.fifthKanbanStatus);
		addDefaultStatus(configurations.sixthKanbanStatus);

		if (defaultStatuses.length) {
			defaultStatuses[defaultStatuses.length - 1].is_done = true;
			$scope.options.defaultStatus = defaultStatuses;
		}

		$scope.header = {
			title: StateParameters.title,
			extTitle: Rows.archiveDate ? '(' + CalendarService.formatDate(Rows.archiveDate) + ')' : '',
			icons: [
			// 	{
			// 	icon: 'visibility',
			// 	onClick: function() {
			// 		// HACK for changeing an icon. If not wanted, remove and change icon above
			// 		var c = $("content-header div.material-icons");
			// 		for(var i=0;i<c.length;i++) {
			//
			// 			if(c[i].textContent=="visibility") {
			// 				c[i].textContent = "visibility_off";
			// 			}else if(c[i].textContent=="visibility_off") {
			// 				c[i].textContent = "visibility";
			// 			}
			// 		}
			// 		// HACK end
			// 		$("body").toggleClass("limitKanban")
			// 	}
			// },
				{
				icon: 'check_box_outline_blank',
				onClick: function() {
					// HACK for changeing an icon. If not wanted, remove and change icon above
					var c = $("content-header div.material-icons"); 
					for(var i=0;i<c.length;i++) {

						if(c[i].textContent=="check_box_outline_blank") {							
							c[i].textContent = "check_box";
						}else if(c[i].textContent=="check_box") {
							c[i].textContent = "check_box_outline_blank";
						}
					}
					// HACK end
					$("body").toggleClass("compactKanban")
				}
			},{
				icon: 'flip_to_back',
				onClick: function () {
					StateParameters.jumpToKanban = false;
					ViewService.view(StateParameters.returnView,
						{params: StateParameters},
						{split: true, replace: 'process.kanban'});
				}
			}]
		};

		function addDefaultStatus(status) {
			let s = Translator.translation(status);
			if (typeof s === 'string' && s.length) {
				defaultStatuses.push({
					title: s,
					is_done: false,
					parent_item_id: $scope.rowId,
					order_no: order[runner++]
				});
			}
		}

		function hasState(state) {
			return States.gantt &&
				States.gantt.groupStates[StateParameters.processGroupId] &&
				States.gantt.groupStates[StateParameters.processGroupId].s.findIndex((x) => x.toLowerCase() == state.toLowerCase()) > -1
		}
	}
})();
