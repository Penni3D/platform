(function () {
	'use strict';

	angular
		.module('core')
		.config(GanttTemplateConfig)
		.controller('GanttTemplateController', GanttTemplateController);

	GanttTemplateConfig.$inject = ['ViewsProvider'];

	function GanttTemplateConfig(ViewsProvider) {
		ViewsProvider.addDialog('gantt.template', {
			template: 'core/rowmanager/views/gantt/ganttTemplate.html',
			controller: 'GanttTemplateController',
			resolve: {
				PortfolioColumns: function (PortfolioService, StateParameters) {
					return PortfolioService.getPortfolioColumns(StateParameters.modelProcess, StateParameters.modelPortfolioId).then(function (columns) {
						return columns;
					});
				}
			},
			afterResolve: {
				PortfolioRows: function (PortfolioService, StateParameters, PortfolioColumns) {
					return PortfolioService.getPortfolio(StateParameters.modelProcess, StateParameters.modelPortfolioId).then(function (p) {
						let order_name = 'item_id';
						let order_dir = 'asc';
						if (_.isNumber(p.OrderItemColumnId) && p.OrderItemColumnId > 0) {
							_.each(PortfolioColumns, function (c) {
								if (c.ItemColumn.ItemColumnId == p.OrderItemColumnId) {
									order_name = c.ItemColumn.Name;
									order_dir = p.OrderDirection == 0 ? 'asc' : 'desc';
									return;
								}
							});
						}

						return PortfolioService
							.getPortfolioData(
								StateParameters.modelProcess,
								StateParameters.modelPortfolioId,
								{local: true},
								{limit: 0, sort: [{column: order_name, order: order_dir}]})
							.then(function (portfolioData) {
								return portfolioData.rowdata;
							});
					});
				}
			}
		});
	}

	GanttTemplateController.$inject = [
		'$scope',
		'MessageService',
		'PortfolioRows',
		'PortfolioColumns',
		'StateParameters',
		'ProcessService',
		'ViewService',
		'ApiService'
	];

	function GanttTemplateController(
		$scope,
		MessageService,
		PortfolioRows,
		PortfolioColumns,
		StateParameters,
		ProcessService,
		ViewService,
		ApiService
	) {
		$scope.portfolioColumns = PortfolioColumns.slice(0, 3);
		$scope.portfolioRows = PortfolioRows;
		$scope.copying = false;
		$scope.canCopy = false;
		$scope.allowSelect = true;
		$scope.selectedRows = {};
		$scope.selectRow = function () {
			var r = true;

			angular.forEach($scope.selectedRows, function (v, k) {
				if (v) {
					$scope.data.copy_from_id = k;
					r = false;

				}
			});

			$scope.allowSelect = r;
			canCopy();
		};

		$scope.data = {
			copy_start_date: new Date()
		};

		$scope.dateChanged = function () {
			canCopy();
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.copy = function () {
			$scope.copying = true;
			ApiService.v1api('features/tasks/copyTasksFromModel').new(StateParameters.itemId, $scope.data).then(function () {
				MessageService.closeDialog();
				ViewService.refresh(StateParameters.viewId);
			});

		};

		function canCopy() {
			$scope.canCopy = $scope.data.copy_start_date && $scope.data.copy_from_id;
		}
	}
})();