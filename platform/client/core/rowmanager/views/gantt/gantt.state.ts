(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.config(GanttConfig)
		.controller('GanttController', GanttController);

	GanttConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function GanttConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('process.gantt', {
			onSetup: {
				TaskPortfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('task', 0);
				},
				ItemColumns: function (ProcessService, StateParameters, $filter) {
					return ProcessService.getColumns(StateParameters.process).then(function (columns) {
						let ret = [];

						angular.forEach(columns, function (key) {
							if (key.DataType === 3) {
								ret.push(key);
							}
						});

						return $filter('orderBy')(ret);
					});
				},
				ListItems: function (ProcessService) {
					return ProcessService.getListItems;
				},
				RelevantDataType: function (Common) {
					return Common.getRelevantDataType;
				},
				TaskColumns: function (ProcessService, Common, $filter) {
					return ProcessService.getColumns("task", true).then(function (columns) {
						let ret = [];

						angular.forEach(columns, function (key) {
							if (Common.getRelevantDataType(key.DataType) === 1 || Common.getRelevantDataType(key.DataType) === 6) {
								ret.push(key);
							}
						});

						return $filter('orderBy')(ret);
					});
				},
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				}
			},
			defaults: {
				linkAllocationsWithTasks: 'false',
				taskParents: [21, 2],
				phaseParents: ['root', 2],
				sprintParents: ['root', 2],
				sprintEnd: 0,
				kanbanMode: 0
			},
			onChange: function (p, params, resolves, options) {
				if (p === 'modelPortfolioId') {
					let i = resolves.Portfolios.length;
					while (i--) {
						let portfolio = resolves.Portfolios[i];
						if (portfolio.ProcessPortfolioId == params.modelPortfolioId) {
							params.modelProcess = portfolio.Process;
							break;
						}
					}
				} else if (p === 'signedTaskColumn') {
					options.GANTT.signedTaskListValueItemId.modify = false;
					options.GANTT.signedTaskIntValue.modify = false;
					let itemColumns = resolves.TaskColumns;
					let i = itemColumns.length;
					while (i--) {
						let ic = itemColumns[i];
						if (ic.Name === params.signedTaskColumn) {
							if (resolves.RelevantDataType(ic.DataType) === 6) {
								resolves.ListItems(ic.DataAdditional).then(function (items) {
									options.GANTT.signedTaskListValueItemId.modify = true;
									options.GANTT.signedTaskListValueItemId.options = items;
									params.signedTaskIntValue = undefined;
								});
							} else {
								options.GANTT.signedTaskIntValue.modify = true;
								params.signedTaskListValueItemId = undefined;
							}
							break;
						}
					}
				}
			},
			tabs: {
				GANTT: {
					featuretteWidth: function () {
						return {
							type: 'select',
							options: [{name: 'VIEW_SPLIT_EVEN', value: 'even'}, {
								name: 'VIEW_SPLIT_WIDER',
								value: 'wider'
							}, {name: 'VIEW_SPLIT_NARROWER', value: 'narrower'}],
							label: "GANTT_FEATURETTE_SIZE"
						}
					},
					ganttPortfolioId: function (resolves) {
						return {
							type: 'select',
							options: resolves.TaskPortfolios,
							label: 'GANTT_PORTFOLIO_ID',
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					modelPortfolioId: function (resolves) {
						return {
							type: 'select',
							options: resolves.Portfolios,
							label: 'GANTT_COPY_MODEL_PORTFOLIO',
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},

					notificationText: function () {
						return {
							type: 'string',
							label: 'GANTT_NOTIFICATION_TEXT'
						};
					},

					signedTaskColumn: function (resolves) {
						return {
							type: 'select',
							options: resolves.TaskColumns,
							label: 'GANTT_SIGNED_TASK_COLUMN',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						};
					},
					signedTaskListValueItemId: function () {
						return {
							type: 'select',
							options: [],
							label: 'GANTT_SIGNED_TASK_LIST_VALUE',
							optionValue: 'item_id',
							optionName: 'list_item'
						};
					},
					signedTaskIntValue: function () {
						return {
							type: 'integer',
							label: 'GANTT_SIGNED_TASK_INT_VALUE'
						};
					},

					taskParents: function () {
						return {
							type: 'multiselect',
							label: 'TASK_PARENT',
							options: [
								{name: 'root', value: 'root'},
								{name: 'task', value: 1},
								{name: 'sprint', value: 21},
								{name: 'phase', value: 2}
							]
						};
					},
					sprintParents: function () {
						return {
							label: 'SPRINT_PARENT',
							type: 'multiselect',
							value: ['root', 2],
							options: [
								{name: 'root', value: 'root'},
								{name: 'task', value: 1},
								{name: 'sprint', value: 21},
								{name: 'phase', value: 2}
							]
						};
					},
					phaseParents: function () {
						return {
							type: 'multiselect',
							label: 'PHASE_PARENT',
							value: ['root', 2],
							options: [
								{name: 'root', value: 'root'},
								{name: 'task', value: 1},
								{name: 'sprint', value: 21},
								{name: 'phase', value: 2}
							]
						};
					},
					projectStartDate: function (resolves) {
						return {
							type: 'select',
							options: resolves.ItemColumns,
							label: 'GANTT_PROJECT_START_DATE',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						};
					},
					projectEndDate: function (resolves) {
						return {
							type: 'select',
							options: resolves.ItemColumns,
							label: 'GANTT_PROJECT_END_DATE',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						};
					},
					enableProjectDateRange: function () {
						return {
							type: 'select',
							options: [{name: 'Disabled', value: false}, {name: 'Enabled', value: true}],
							label: 'GANTT_ENABLE_PROJECT_DATE_RANGE'
						};
					}
				},
				KANBAN: {
					kanbanPortfolioId: function (resolves) {
						return {
							type: 'select',
							options: resolves.TaskPortfolios,
							label: 'KANBAN_PORTFOLIO_ID',
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						}
					},
					sprintEnd: function () {
						return {
							type: 'select',
							options: [{name: 'END_DATE ', value: 0}, {name: 'END_STATUS', value: 1}],
							label: 'KANBAN_SPRINT_END'
						}
					},
					kanbanMode: function () {
						return {
							type: 'select',
							options: [{Translation: "KANBAN_ADVANCED", Value: 0}, {
								Translation: "KANBAN_SIMPLE",
								Value: 1
							}],
							label: 'KANBAN_MODE',
							optionValue: 'Value',
							optionName: 'Translation'
						}
					},
					firstKanbanStatus: function () {
						return {
							type: 'translation',
							label: 'KANBAN_FIRST_STATUS'
						};
					},
					secondKanbanStatus: function () {
						return {
							type: 'translation',
							label: 'KANBAN_SECOND_STATUS'
						};
					},
					thirdKanbanStatus: function () {
						return {
							type: 'translation',
							label: 'KANBAN_THIRD_STATUS'
						};
					},
					fourthKanbanStatus: function () {
						return {
							type: 'translation',
							label: 'KANBAN_FOURTH_STATUS'
						};
					},
					fifthKanbanStatus: function () {
						return {
							type: 'translation',
							label: 'KANBAN_FIFTH_STATUS'
						};
					},
					sixthKanbanStatus: function () {
						return {
							type: 'translation',
							label: 'KANBAN_SIXTH_STATUS'
						};
					}
				},
				ALLOCATION: {
					enableTaskAllocation: function () {
						return {
							type: 'select',
							options: [{name: 'Disabled', value: false}, {name: 'Enabled', value: true}],
							label: 'GANTT_ENABLE_TASK_ALLOCATION'
						};
					},
					showAllocationLegend: function () {
						return {
							type: 'select',
							options: [{name: 'Hide', value: false}, {name: 'Show', value: true}],
							label: 'GANTT_SHOW_ALLOCATION_LEGEND'
						};
					},
					enableAllocation: function () {
						return {
							type: 'select',
							options: [{name: 'Disabled', value: false}, {name: 'Enabled', value: true}],
							label: 'GANTT_ENABLE_ALLOCATION'
						};
					},
					resetAllocation: function () {
						return {
							type: 'select',
							options: [{name: 'Disabled', value: false}, {name: 'Enabled', value: true}],
							label: 'GANTT_ENABLE_RESET'
						};
					},
					linkAllocationsWithTasks: function () {
						return {
							type: 'select',
							options: [{name: 'Disabled', value: false}, {name: 'Enabled', value: true}],
							label: 'GANTT_LINK_ALLOCATIONS'
						};
					}
				}
			}
		});

		ViewsProvider.addView('process.gantt', {
			controller: 'GanttController',
			template: 'core/rowmanager/views/gantt/gantt.html',
			level: 2,
			resolve: {
				Tasks: function (StateParameters, PortfolioService, States, Actions) {
					let r = PortfolioService.getTaskData(
						StateParameters.process,
						StateParameters.itemId,
						States.meta,
						StateParameters.processGroupId,
						'gantt',
						StateParameters.ganttPortfolioId, Actions.ArchiveDates);
					return r;
				},
				PortfolioColumns: function (PortfolioService, StateParameters, $q) {
					let returnCols = [];
					let primary;
					return PortfolioService.getPortfolioColumns(
						'task',
						StateParameters.ganttPortfolioId)
						.then(function (columns) {
							let d = $q.defer();
							for (let i = 0, l = columns.length; i < l; i++) {
								if (columns[i].UseInSelectResult === 1) {
									primary = columns[i];
								} else {
									if (columns[i].ReportColumn != 1) returnCols.push(columns[i]);
								}
							}

							if (typeof primary !== "undefined") {
								returnCols.unshift(primary);
								d.resolve(returnCols);
							} else {
								d.reject();
							}
							return d.promise;
						});

				}
			},
			afterResolve: {
				Route: function (StateParameters, ViewService, Common) {
					if (Common.isTrue(StateParameters.jumpToKanban)) {

						ViewService.reroute(StateParameters.ViewId, 'process.kanban');
					}
				}
			},
			options: {
				preventOn: ['process.sprint', 'kanban']
			}
		});

		ViewsProvider.addFeature(
			'process.gantt',
			'gantt',
			'FEATURE_GANTT',
			['read', 'write', 'history', 'kanbanRead', 'kanbanWrite', 'kanbanCard', 'kanbanOwnCards', 'help']);
	}

	GanttController.$inject = [
		'$rootScope',
		'$scope',
		'PortfolioColumns',
		'StateParameters',
		'ViewService',
		'Translator',
		'Tasks',
		'RMService',
		'MessageService',
		'$q',
		'AllocationService',
		'States',
		'ProcessService',
		'Data',
		'CalendarService',
		'Common',
		'Configuration'
	];

	function GanttController(
		$rootScope,
		$scope,
		PortfolioColumns,
		StateParameters,
		ViewService,
		Translator,
		Tasks,
		RMService,
		MessageService,
		$q,
		AllocationService,
		States,
		ProcessService,
		Data,
		CalendarService,
		Common,
		Configuration
	) {
		let autoSlack = true;

		if (StateParameters.notificationText) {
			setTimeout(function () {
				ViewService.notification(StateParameters.ViewId, StateParameters.notificationText);
			}, 1000);
		}

		$scope.portfolioId = StateParameters.ganttPortfolioId;
		$scope.portfolioColumns = PortfolioColumns;
		$scope.rows = Tasks.tasks;

		$scope.loading = false;

		$scope.slackChange = function () {
			if (autoSlack && $scope.selectedSlacks.length === 1 && $scope.selectedSlacks[0] === 'slack') {
				$scope.selectedSlacks.unshift('to');
				$scope.selectedSlacks.unshift('from');
				autoSlack = true;
			} else if (autoSlack && $scope.selectedSlacks.length === 2 && $scope.selectedSlacks.indexOf('slack') === -1) {
				$scope.selectedSlacks.length = 0;
				autoSlack = true;
			} else {
				autoSlack = $scope.selectedSlacks.length === 0;
			}
		};

		let hasWriteRights = hasState('write') && !Tasks.archiveDate;

		$scope.slackOptions = [
			{
				value: 'from',
				name: 'GANTT_SLACK_FROM'
			},
			{
				value: 'to',
				name: 'GANTT_SLACK_TO'
			},
			{
				value: 'slack',
				name: 'GANTT_SLACK_SLACK'
			}
		];

		$scope.header = {
			title: StateParameters.title,
			extTitle: Tasks.archiveDate ? '(' + CalendarService.formatDate(Tasks.archiveDate) + ')' : '',
			icons: [{
				icon: 'filter_list',
				name: 'FILTER',
				onClick: function () {
					$scope.showFilters = !$scope.showFilters;

					if ($scope.showFilters) {
						ViewService.scrollTop(StateParameters.ViewId);
					}
				}
			}]
		};

		let RMPasteStartlistener = $rootScope.$on('RMPasteStart', function (event, val) {
			$scope.loading = true;
		});
		let RMPasteEndlistener = $rootScope.$on('RMPasteEnd', function (event, val) {
			$scope.loading = false;
		});

		if (hasWriteRights && StateParameters.modelPortfolioId) {
			$scope.header.icons.push({
				icon: 'content_copy',
				onClick: function () {
					ViewService.view('gantt.template', {
						params: {
							modelPortfolioId: StateParameters.modelPortfolioId,
							modelProcess: StateParameters.modelProcess,
							itemId: StateParameters.itemId,
							viewId: StateParameters.ViewId
						}
					});
				},
				tooltip:'GANTT_COPY_MODEL'
			})
		}
		$scope.rules = {
			parents: {
				1: StateParameters.taskParents,
				2: StateParameters.phaseParents,
				21: StateParameters.sprintParents
			},
			accepts: {
				1: ["user"],
				21: ["user"]
			},
			draggables: [
				1,
				2,
				21
			]
		};
		$scope.modifyZoomOut = true;
		$scope.selectedSlacks = [];

		let wantedVal = StateParameters.signedTaskListValueItemId;
		if (typeof wantedVal === "undefined") {
			wantedVal = StateParameters.signedTaskIntValue;
		}
		$scope.options = {
			calendarSteps: [15, 30, 90, 180, 360, 720],
			signedColumn: StateParameters.signedTaskColumn,
			signedValue: wantedVal,
			split: true,
			currentUserId: $rootScope.clientInformation.item_id,
			inheritDates: true,
			sumDatas: true,
			ownerId: StateParameters.itemId,
			ownerType: 'gantt',
			process: "task",
			showCalendar: true,
			sortable: true,
			selectable: true,
			featuretteWidth: StateParameters.featuretteWidth,
			readOnly: !hasWriteRights,
			slacks: $scope.selectedSlacks,
			alias: {
				1: Translator.translate("RM_TASK"),
				2: Translator.translate("RM_PHASE"),
				21: Translator.translate("RM_SPRINT")
			},
			menuItems: {
				1: [],
				21: []
			}
		};
		$scope.newItem = function (process, data) {
			return ProcessService.newItem(process, {Data: data}).then(function (id) {
				return ProcessService.getCachedItemData(process, id);
			});
		};

		ViewService.footbar(StateParameters.ViewId, {template: 'core/rowmanager/views/gantt/ganttFootbar.html'});
		if (hasState('kanbanRead') || hasState('kanbanWrite')) {
			$scope.options.menuItems[21].push(
				{
					name: "Kanban",
					isActive: function () {
						return true;
					},
					onClick: function (id) {
						let p = {
							params: {
								itemId: StateParameters.itemId,
								rowId: id,
								processGroupId: StateParameters.processGroupId,
								process: StateParameters.process,
								returnView: "process.gantt",
								title: RMService.itemSet[id].data.title
							}
						};

						ViewService.view('process.kanban', p);
					}
				});
		}

		if (StateParameters.enableProjectDateRange) {
			$scope.options["projectStartDate"] = Data.Data[StateParameters.projectStartDate];
			$scope.options["projectEndDate"] = Data.Data[StateParameters.projectEndDate];
		}

		if (StateParameters.enableTaskAllocation) {
			let menuItem = {
				name: Translator.translate('GANTT_ASSIGN'),
				onClick: function (itemId) {
					let p = {
						process: StateParameters.process,
						itemId: StateParameters.itemId,
						taskId: itemId,
						name: RMService.itemSet[itemId].data.title,
						portfolioId: $scope.portfolioId
					};

					ViewService.view('process.taskAllocation', {params: p});
				}
			};

			$scope.options.menuItems[1].push(menuItem);
			$scope.options.menuItems[21].push(menuItem);

		}

		if (StateParameters.showAllocationLegend) {
			$scope.showAllocationDescription = true;
		}

		let req = Configuration.getServerConfigurations('gantt');
		if (StateParameters.enableAllocation) { //|| req.autoallocatetaskinsert.value removed by BSA

			let menuItem = {
				isActive: function (id) {
					if ($scope.options.readOnly) {
						return false;
					}

					let DI = RMService.itemSet[id];
					return DI.data.allocation_status >= 20;
				},
				name: Translator.translate('GANTT_ALLOCATE'),
				onClick: function (itemId) {
					allocateFunction(itemId);
				}
			};

			$scope.options.menuItems[1].push(menuItem);
			$scope.options.menuItems[21].push(menuItem);
		}

		if (StateParameters.openRow) {
			setTimeout(function () {
				setTimeout(function () {
					let DI = RMService.itemSet[StateParameters.openRow];
					RMService.openFeaturette(DI, $scope.options.readOnly);
				});
			});
		}


		function hasState(state) {
			return States.gantt &&
				States.gantt.groupStates[StateParameters.processGroupId] &&
				States.gantt.groupStates[StateParameters.processGroupId].s.findIndex((x) => x.toLowerCase() == state.toLowerCase()) > -1
		}


		function allocateFunction(itemId) {
			let DI = RMService.itemSet[itemId];

			AllocationService.requestResources(DI.data.responsibles).then(function (model) {
				MessageService.toast('GANTT_REQ_ALLOCATIONS');
				let promises = [];
				let durations = [];
				let sd = new Date(DI.data.start_date);
				let ed = new Date(DI.data.end_date);
				let effort = 0;

				let days = Math.round((ed.getTime() - sd.getTime()) / (24 * 60 * 60 * 1000)) + 1;

				if (DI.data.effort !== '' && typeof DI.data.effort !== 'undefined' && typeof days === 'number') {
					effort = DI.data.effort / days;
				}

				let l = model.user.length;
				let l_ = model.competency.length;

				effort = effort / (l + l_);

				let runner = sd;
				while (runner <= ed) {
					let d = runner.getDate();

					durations.push({
						hours: effort,
						date: new Date(runner)
					});

					runner.setDate(d + 1);
				}

				let title = Translator.translate('ALLOCATION_REQUEST_FOR_TASK') + ' ' + DI.data.title;

				let requestedIds = [];
				let i;
				for (i = 0; i < l; i++) {
					requestedIds.push(model.user[i]);
					promises.push(AllocationService.createAllocation(model.user[i], StateParameters.itemId, {
						title: title,
						durations: durations,
						task_item_id: configurations.linkAllocationsWithTasks ? itemId : undefined
					}));
				}

				for (i = 0; i < l_; i++) {
					requestedIds.push(model.competency[i]);
					promises.push(AllocationService.createAllocation(model.competency[i], StateParameters.itemId, {
						title: title,
						durations: durations,
						task_item_id: configurations.linkAllocationsWithTasks ? itemId : undefined
					}));
				}

				let params = StateParameters;
				params.openRows = requestedIds;

				$q.all(promises).then(function () {
					ViewService.view('process.projectAllocation', {
						params: params
					});
				});
			});
		}

	}
})();
