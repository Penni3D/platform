﻿(function () {
	'use strict';

    angular
        .module('core.rowmanager')
        .config(BacklogConfig)
        .controller('BacklogController', BacklogController)
		.service('BacklogService', BacklogService);

	BacklogConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
	function BacklogConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('process.backlog', {
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('task', 0);
				}
			},
			tabs: {
				default: {
					featuretteWidth: function () {
						return {
							type: 'select',
							options: [{name: 'VIEW_SPLIT_EVEN', value: 'even'}, {name: 'VIEW_SPLIT_WIDER', value: 'wider'}, {name: 'VIEW_SPLIT_NARROWER', value: 'narrower'}],
							label: "BACKLOG_FEATURETTE_SIZE"
						}
					},
					portfolioId: function (resolves) {
						return {
							type: 'select',
							options: resolves.Portfolios,
							label: 'BACKLOG_PORTFOLIO_ID',
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						}
					}
				}
			}
		});

		ViewsProvider.addView('process.backlog', {
			controller: 'BacklogController',
			template: 'core/rowmanager/views/backlog/backlog.html',
			level: 2,
			resolve: {
				BacklogItems: function (StateParameters, PortfolioService, States, Actions) {
					return PortfolioService.getTaskData(
						StateParameters.process,
						StateParameters.itemId,
						States.meta,
						StateParameters.processGroupId,
						'backlog',
						StateParameters.portfolioId, Actions.ArchiveDates);
				},
				PortfolioColumns: function (PortfolioService, StateParameters) {
					return PortfolioService.getPortfolioColumns('task', StateParameters.portfolioId);
                },
				BacklogChartData: function (BacklogService, StateParameters) {
					return BacklogService.getBacklogChartData(StateParameters.itemId);
				}
            }
        });

		ViewsProvider.addFeature('process.backlog', 'backlog', 'FEATURE_BACKLOG', ['read', 'write', 'history']);
    }

	BacklogController.$inject = [
		'$scope',
		'ViewService',
		'PortfolioColumns',
		'StateParameters',
		'Translator',
		'BacklogItems',
		'BacklogChartData',
		'BacklogService',
		'SaveService',
		'States',
		'CalendarService',
		'ProcessService'
	];

	function BacklogController(
		$scope,
		ViewService,
		PortfolioColumns,
		StateParameters,
		Translator,
		BacklogItems,
		BacklogChartData,
		BacklogService,
		SaveService,
		States,
		CalendarService,
		ProcessService
	) {
		
    	$scope.portfolioId = StateParameters.portfolioId;

		$scope.portfolioColumns = PortfolioColumns;
		$scope.rows = BacklogItems.tasks;

    	$scope.rules = {
    	    parents: {
                1: ['root']
    	    },
			draggables: [
				1
			]
    	};
		$scope.newItem = function(process, data) {
			return ProcessService.newItem(process, {Data: data}).then(function (id) {
				return ProcessService.getCachedItemData(process, id);
			});
		}
    	$scope.options = {
			process: "task",
			sortable: true,
			selectable: false,
			ownerId: StateParameters.itemId,
			featuretteWidth: StateParameters.featuretteWidth,
			ownerType: 'backlog',
			alias: {
				1: Translator.translate("RM_TASK")
			}
		};
		$scope.options.readOnly = !(angular.isDefined(States.backlog) &&
			States.backlog.groupStates[StateParameters.processGroupId] !== 'undefined' > -1 &&
			States.backlog.groupStates[StateParameters.processGroupId].s.indexOf("write") > -1)
            && Tasks.archiveDate;
		
		
		$scope.header = {
			title: StateParameters.title,
			extTitle: BacklogItems.archiveDate ? '(' + CalendarService.formatDate(BacklogItems.archiveDate) + ')' : '',
			icons: []
		};

		/*
		if(!$scope.options.readOnly) {
			$scope.header.icons.push({
				icon: 'add',
				onClick: function (event) {
					$scope.addNewItem(event);
				}
			});
		}
		*/
		$scope.donePercent = Math.round(100 * BacklogChartData.donePercent);
		$scope.totalEffort = BacklogChartData.totalEffort;
		$scope.doneEffort = BacklogChartData.doneEffort;
		$scope.totalCount = BacklogChartData.totalCount;
		$scope.doneCount = BacklogChartData.doneCount;
		$scope.backlogHistory = BacklogChartData.backlogHistory;
		$scope.isLoaded = true;

		SaveService.subscribeSave($scope, function () {
			$scope.isLoaded = false;
			return BacklogService.getBacklogChartData(StateParameters.itemId).then(function (bData) {
				$scope.donePercent = Math.round(100 * bData.donePercent);
				$scope.totalEffort = bData.totalEffort;
				$scope.doneEffort = bData.doneEffort;
				$scope.totalCount = bData.totalCount;
				$scope.doneCount = bData.doneCount;
				$scope.backlogHistory = bData.backlogHistory;
				$scope.isLoaded = true;
			});
		});
	}

	BacklogService.$inject = ['ApiService'];
	function BacklogService(ApiService) {
		var self = this;

		var chartData = ApiService.v1api('features/Tasks/getBacklogChartData');

		self.getBacklogChartData = function (processId) {
			return chartData.get(processId);
		};
	}
})();