﻿﻿(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.controller('RowManagerSplitController', RowManagerSplitController);

	RowManagerSplitController.$inject = ['$scope'];
	function RowManagerSplitController($scope) {
		$scope.title = 'Split task';

	}
})();
