﻿(function () {
	'use strict';

	angular
		.module('core.rowmanager')
		.controller('RowManagerEditController', RowManagerEditController);

	RowManagerEditController.$inject = [
		'$scope', 
		'SelectedRows', 
		'MessageService', 
		'ProcessService'];
	function RowManagerEditController(
		$scope, 
		SelectedRows, 
		MessageService, 
		ProcessService) {
		var step = 86400000;// = (1000*60*60*24)

		var countDays = function(d1, d2) {
			var utc1 = Date.UTC(d1.getFullYear(), d1.getMonth(), d1.getDate());
			var utc2 = Date.UTC(d2.getFullYear(), d2.getMonth(), d2.getDate());

			return Math.round((utc2 - utc1) / step);
		};

		$scope.apply = function() {
			$.each(SelectedRows, function (i, DI) {
				var moveDays = 0;
				var sd = new Date(DI.data.start_date);
				var ed = new Date(DI.data.end_date);

				if($scope.responsible.length>0) {
					DI.data.responsibles = $scope.responsible;
				}
				if($scope.startDate != null && $scope.endDate != null) {				
					DI.data.start_date =  new Date($scope.startDate);
					DI.data.end_date = new Date($scope.endDate);
				} else if($scope.startDate != null) {
					var newSd = new Date($scope.startDate);
					moveDays = countDays(sd,newSd);
				} else if($scope.endDate != null) {
					var newEd = new Date($scope.endDate);
					moveDays = countDays(ed,newEd)
				}
				if(!isNaN($scope.moveDays)) {
					moveDays += $scope.moveDays;
				}
				if(moveDays !== 0) {
					sd.setDate(sd.getDate() + moveDays);
					ed.setDate(ed.getDate() + moveDays);
					DI.data.start_date = sd;
					DI.data.end_date = ed;
				}

				if(!isNaN($scope.setEffort)) {
					// DI.data.effort = $scope.setEffort;
					
					for (var i=0; i<DI.data.responsibles.length; i++) {
						ProcessService.getLink("task", DI.data.item_id, DI.RM.test, DI.data.responsibles[i]).then(function(d) {
							// newData.effort = d.effort;
							d.effort = $scope.setEffort;
						});	
						
					} 
					
				}
				DI.setDataChanged();
			});
			MessageService.closeDialog();
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};
	}
})();
