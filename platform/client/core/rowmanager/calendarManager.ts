﻿(function ($) {
	'use strict';

	$.fn.AddCalendar = function (RMService, settings, savedSettings, RM) {
		var CALENDAR_HOVER_WIDTH = 15360; //four times 4K resolution, required for Hover effects
		var CALENDAR_HOVER_WIDTH_HALVED = CALENDAR_HOVER_WIDTH / 2;
		var CA = this;
		var AM;
		CA.setArrowManager = function (newAM) {
			AM = newAM;
			AM.linkWarningIcon(RM);
		};
		
		/*______          ____    _ __ 
		 /_  __/__  ___  / / /__ (_) /_
		  / / / _ \/ _ \/ /  '_// / __/
		 /_/  \___/\___/_/_/\_\/_/\_*/

		// cuts a way decimal part of a number, faster alternative
		var int = function (v) {
			return v >> 0;
		};

		function newUTCDate(d) {
			var date;

			if (typeof d !== 'undefined') {
				date = new Date(d);
			} else {
				date = new Date();
			}

			return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
		}

		// converts given pixels to date
		var pixelsToDate = function (px) {
			var d = new Date(today);
			d.setUTCDate(d.getUTCDate() + Math.round(-(CALENDAR_HOVER_WIDTH_HALVED + px) / dayPixels));

			return d;
		};

		// converts given dates to viewpixels
		var dateToPixels = function (d) {
			d = new Date(d);
			return Math.round((d - today) / mspx) + CALENDAR_HOVER_WIDTH_HALVED;
		};

		var dateDifPixels = function (startDate, endDate) {
			startDate = new Date(startDate);
			endDate = new Date(endDate);

			return Math.round((endDate - startDate) / mspx);
		};

		// adds days to given date
		var addDays = function (date, days) {
			var rd = new Date(date);
			rd.setUTCDate(rd.getUTCDate() + days);

			return rd;
		};

		// counts and places gray zones on the calendar when monitored time is set to be shown.
		var showStartPixels;
		var showEndPixels;
		var showStart = settings.projectStartDate;
		var showEnd = settings.projectEndDate;
		// var showStart = new Date();
		// var showEnd = new Date();
		// showStart.setFullYear(showStart.getUTCFullYear()-10);	
		// showEnd.setFullYear(showEnd.getUTCFullYear()+10);

		var disableAreas = function () {
			if (typeof showStart !== 'undefined') {
				showStartPixels = dateToPixels(showStart);
				if(!_.isNaN(showStartPixels)){
					$disableStart.css({
						"display": "block",
						"left": +(showStartPixels - CALENDAR_HOVER_WIDTH) + "px",
						"width": CALENDAR_HOVER_WIDTH + "px"
					});
				}

			} else {
				showStartPixels = undefined;
			}

			if (typeof showEnd !== 'undefined') {
				showEndPixels = dateToPixels(showEnd);
				if(!_.isNaN(showEndPixels)) {
					$disableEnd.css({
						"display": "block",
						"left": showEndPixels + "px",
						"width": CALENDAR_HOVER_WIDTH + "px"
					});
				}
				showEndPixels -= dayPixels * visibleDays;
			} else {
				showEndPixels = undefined;
			}
		};

		/*_____     __            __        
		 / ___/__ _/ /__ ___  ___/ /__ _____
		/ /__/ _ `/ / -_) _ \/ _  / _ `/ __/
		\___/\_,_/_/\__/_//_/\_,_/\_,_/*/
		// calendarFirst and calendarLast are handles to access data structure of calendar Elements.
		var calendarFirst;
		var calendarLast;
		var calendarRows = {};

		// Calendar Elements are used to store handles to onscreen calendar items in both anchors on right side of screen.		
		var calendarElement = function () {
			this.init = function () {
				this.$box = $('<div></div>').appendTo($anchor);
				this.$hBox = $('<div></div>').appendTo($hAnchor);
				this.cells = [];

				/*
												/T /I
												/ |/ | .-~/
											T\ Y  I  |/  /  _
							/T               | \I  |  I  Y.-~/
						I l   /I       T\ |  |  l  |  T  /
						T\ |  \ Y l  /T   | \I  l   \ `  l Y
					__  | \l   \l  \I l __l  l   \   `  _. |
					\ ~-l  `\   `\  \  \ ~\  \   `. .-~   |
					\   ~-. "-.  `  \  ^._ ^. "-.  /  \   |
				.--~-._  ~-  `  _  ~-_.-"-." ._ /._ ." ./
					>--.  ~-.   ._  ~>-"    "\   7   7   ]
				^.___~"--._    ~-{  .-~ .  `\ Y . /    |
					<__ ~"-.  ~       /_/   \   \I  Y   : |
					^-.__           ~(_/   \   >._:   | l______
						^--.,___.-~"  /_/   !  `-.~"--l_ /     ~"-.
								(_/ .  ~(   /'     "~"--,Y   -=b-. _)
								(_/ .  \  :           / l      c"~o \
								\ /    `.    .     .^   \_.-~"~--.  )
									(_/ .   `  /     /       !       )/
									/ / _.   '.   .':      /        '
									~(_/ .   /    _  `  .-<_
									/_/ . ' .-~" `.  / \  \          ,z=.
									~( /   '  :   | K   "-.~-.______//
										"-,.    l   I/ \_    __{--->._(==.
										//(     \  <    ~"~"     //
										/' /\     \  \     ,v=.  ((
									.^. / /\     "  }__ //===-  `
									/ / ' '  "-.,__ {---(==-
									.^ '       :  T  ~"   ll
								/ .  .  . : | :!        \
								(_/  /   | | j-"          ~^
									~-<_(_.^-~"
				*/
				this.liberate = function () {
					while (this.cells.length) {
						var cell = this.cells.pop();
						cell.freeYourSelf();
					}
				};

				this.getAvailableCell = function (itemId, startDate, endDate) {
					var cell = RMService.getCell(itemId, startDate, endDate);

					if (cell) {
						cell.CE = this;
						this.cells.push(cell);

						return cell;
					}
				};

				return this;
			};
		};
		/*
		//Old weekNum
		var weekNum = function (d) {
			var thu = new Date(d);
			thu.setUTCDate(thu.getUTCDate() - (thu.getDay() + 6) % 7 + 7 + 3);

			return $.datepicker.iso8601Week(thu);
		};*/
		var getWeekNum = function (d) {
			var thu = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
			thu.setUTCDate(thu.getUTCDate() + 4 - thu.getUTCDay() || 7);
			var yearStart = new Date(Date.UTC(thu.getUTCFullYear(), 0, 1));
			return Math.ceil((((thu - yearStart) / 86400000) + 1) / 7)
		};
		var getQNum = function (d) {
			var r = (Math.floor((d.getUTCMonth() - settings.qStart) / 3));
			while (r > 3) {
				r = r - 4
			}
			while (r < 0) {
				r = r + 4
			}
			return r + 1;
		};

		var calendarMove = function () {
		};
		var populateContinuous = function () {
			disableAreas();
			setModename(visibleDays);
			var startDate = new Date(pixelsToDate(viewPoint));

			if (visibleDays < 60) { // Day Mode
				calendarNextDate = function (d) {
					d = new Date(d);
					d.setUTCDate(d.getUTCDate() + 1);

					return d;
				};

				calendarPrevDate = function (d) {
					d = new Date(d);
					d.setUTCDate(d.getUTCDate() - 1);

					return d;
				};

				calendarAreaStart = function (d) {
					return new Date(d);
				};

				calendarMove = function (CE, d) {// Day Mode, one calendarElement per day
					CE.date = new Date(d);
					var pos = dateToPixels(CE.date);
					var upper = '';
					var day = CE.date.getUTCDate();
					var month = CE.date.getUTCMonth();
					var year = CE.date.getUTCFullYear();
					var monthTranslation = RMService.sanitize(settings.monthsofyear[month]);
					var cls = 'day';
					var dowStamp = '';
					CE.header = monthTranslation + ' ' + year;

					if (day == 1) { // first day of month
						if (month == 0) { // first day of year
							upper = "<div class='upper'>" + monthTranslation + " " + year + "</div>";
							cls = 'year';
						} else if (month % 3 == 0) { // first day of quartal
							upper = "<div class='upper'>" + monthTranslation + "</div>";
							cls = 'quartal';
						} else { // Month
							upper = "<div class='upper'>" + monthTranslation + "</div>";
							cls = 'month';
						}
					}

					if (visibleDays < 15) {
						dowStamp = ' ' + RMService.sanitize(settings.daysofweek[CE.date.getDay()]);
					}

					var weekDay = CE.date.getUTCDay();
					if (weekDay === 0 || weekDay === 6) {
						cls += ' is-weekend';
					}

					CE.$box.html('<div class="calendar-element-date">' + day + '</div>' + upper);
					CE.$hBox.html('<div class="calendar-element-date">' + day + '</div>' + dowStamp + upper);

					CE.$box.attr('class', cls);
					CE.$hBox.attr('class', cls);

					CE.$box.css({left: pos, width: dayPixels});
					CE.$hBox.css({left: pos, width: dayPixels});

					if (settings.showAllocation) {
						CE.liberate();

						angular.forEach(calendarRows, function (row, ownerId) {
							var cell = CE.getAvailableCell(ownerId, CE.date, CE.date);
							if (cell && cell.calculate()) {
								cell.draw(pos, dayPixels);
							}
						});
					}
				};
			} else if (visibleDays < 200) { // Week Mode, one calendarElement per week, with quartals
				startDate.setUTCDate(startDate.getUTCDate() - startDate.getDay() + 1);

				calendarNextDate = function (d) {
					d = new Date(d);
					d.setUTCDate(d.getUTCDate() + 7);

					return d;
				};

				calendarPrevDate = function (d) {
					d = new Date(d);
					d.setUTCDate(d.getUTCDate() - 7);

					return d;
				};

				calendarAreaStart = function (d) {
					d = new Date(d);

					return new Date(d.getTime() - ((d.getDay() + 6) % 7) * step);
				};


				calendarMove = function (CE, d) {
					CE.date = new Date(d);
					var pos = dateToPixels(CE.date);
					var day = CE.date.getUTCDate();
					var month = CE.date.getUTCMonth();
					var year = CE.date.getUTCFullYear();
					var monthTranslation = RMService.sanitize(settings.monthsofyear[CE.date.getUTCMonth()]);
					var CEWidth = 7 * dayPixels;
					var cls = 'week';
					var upper = '';

					if (day < 8) { // first week of month
						if (month == 0) { // first week of year
							upper = "<div class='upper'>" + monthTranslation + ' ' + year + "</div>";
							cls = 'year';
						} else if (month % 3 == 0) { // first week of quartal
							upper = "<div class='upper'>" + monthTranslation + "</div>";
							cls = 'quartal';
						} else { // first week of any other month
							upper = "<div class='upper'>" + monthTranslation + "</div>";
							cls = 'month';
						}
					}

					CE.header = year;

					if (settings.showWeeknum) {
						var w = getWeekNum(CE.date);
						CE.$box.html('<div class="calendar-element-date">' + w + '</div>' + upper);
						CE.$hBox.html('<div class="calendar-element-date">' + w + '</div>' + upper);
					} else {
						CE.$box.html('<div class="calendar-element-date">' + day + '</div>' + upper);
						CE.$hBox.html('<div class="calendar-element-date">' + day + '</div>' + upper);
					}


					CE.$box.attr('class', cls);
					CE.$hBox.attr('class', cls);
					CE.$box.css({left: pos, width: CEWidth});
					CE.$hBox.css({left: pos, width: CEWidth});

					if (settings.showAllocation) {
						CE.liberate();

						var endDate = new Date(CE.date);
						endDate.setUTCDate(endDate.getUTCDate() + 6);

						angular.forEach(calendarRows, function (row, ownerId) {
							var cell = CE.getAvailableCell(ownerId, CE.date, endDate);
							if (cell && cell.calculate()) {
								cell.draw(pos, CEWidth);
							}
						});
					}
				};
			} else if (visibleDays < 400) { // Month Mode, one calendarElement per month
				startDate.setUTCDate(1);
				calendarNextDate = function (d) {
					d = new Date(d);
					d.setUTCMonth(d.getUTCMonth() + 1);
					return d;
				};
				calendarPrevDate = function (d) {
					d = new Date(d);
					d.setUTCMonth(d.getUTCMonth() - 1);
					return d;
				};

				calendarAreaStart = function (d) {
					d = new Date(d);
					d.setUTCDate(1);
					return d;
				};

				calendarMove = function (CE, d) {
					CE.date = new Date(d);

					var pos = dateToPixels(CE.date);
					var upper = "";


					var CEWidth = (calendarNextDate(CE.date).getTime() - CE.date.getTime()) / step * dayPixels;

					if (CE.date.getUTCMonth() == 0) { // first month of the year
						var y = CE.date.getUTCFullYear();
						var q = getQNum(CE.date);
						CE.$box.attr("class", "year");
						CE.$hBox.attr("class", "year");
						upper = "<div class='upper'>Q" + q + " " + y + "</div>";
					} else if (CE.date.getUTCMonth() % 3 == 0) { // first month of Quartal
						var q = getQNum(CE.date);
						CE.$box.attr("class", "quartal");
						CE.$hBox.attr("class", "quartal");
						upper = "<div class='upper'>Q" + q + "</div>";
					} else { // other months
						CE.$box.attr("class", "month");
						CE.$hBox.attr("class", "month");
					}

					CE.header = y;

					CE.$box.html('<div class="calendar-element-date">' + RMService.sanitize(settings.monthsofyear[CE.date.getUTCMonth()]) + '</div>' + upper);
					CE.$hBox.html('<div class="calendar-element-date">' + RMService.sanitize(settings.monthsofyear[CE.date.getUTCMonth()]) + '</div>' + upper);

					CE.$box.css({left: pos + "px", width: CEWidth + "px"});
					CE.$hBox.css({left: pos + "px", width: CEWidth + "px"});

					if (settings.showAllocation) {
						CE.liberate();

						var endDate = new Date(CE.date);
						endDate.setUTCMonth(endDate.getUTCMonth() + 1);
						endDate.setUTCDate(endDate.getUTCDate() - 1);

						angular.forEach(calendarRows, function (row, ownerId) {
							var cell = CE.getAvailableCell(ownerId, CE.date, endDate);
							if (cell && cell.calculate()) {
								cell.draw(pos, CEWidth);
							}
						});
					}
				};
			} else { // Quartal Mode, one calendarElement per quartal
				startDate.setUTCDate(1);
				startDate.setUTCMonth(Math.floor(startDate.getUTCMonth() / 3) * 3);
				calendarNextDate = function (d) {
					d = new Date(d);
					d.setUTCMonth(d.getUTCMonth() + 3);
					return d;
				};
				calendarPrevDate = function (d) {
					d = new Date(d);
					d.setUTCMonth(d.getUTCMonth() - 3);
					return d;
				};

				calendarAreaStart = function (d) {
					d = new Date(d);
					d.setUTCMonth(Math.floor(d.getUTCMonth() / 3 + 1));
					d.setUTCDate(1);

					return d;
				};
				calendarMove = function (CE, d) {
					CE.date = new Date(d);
					var pos = dateToPixels(CE.date);
					var upper = "";
					var y = CE.date.getUTCFullYear();

					if (CE.date.getUTCMonth() == 0) { // first quartal of the year
						CE.$box.attr("class", "year");
						CE.$hBox.attr("class", "year");
						upper = "<div class='upper'>" + y + "</div>";
					} else { // any other quartal
						CE.$box.attr("class", "quartal");
						CE.$hBox.attr("class", "quartal");
					}

					var q = getQNum(CE.date);
					CE.header = "";
					CE.$box.html('<div class="calendar-element-date">Q' + q + '</div>' + upper);
					CE.$hBox.html('<div class="calendar-element-date">Q' + q + '</div>' + upper);

					CE.$box.css({left: pos + "px", width: dayPixels + "px"});
					CE.$hBox.css({left: pos + "px", width: dayPixels + "px"});
				};
			}

			if (!calendarFirst) {
				calendarFirst = new calendarElement();
				calendarLast = calendarFirst;
				calendarFirst.init();
			}

			calendarLast.next = undefined;
			calendarFirst.prev = undefined;

			var runner = calendarFirst;
			var d = new Date(startDate);
			calendarMove(runner, startDate);

			while (runner.next) {
				runner = runner.next;
				d = calendarNextDate(d);
				calendarMove(runner, d);
			}

			var endDate = new Date(startDate);
			endDate.setUTCDate(endDate.getUTCDate() + visibleDays);
			calendarLast = runner;

			while (calendarPrevDate(calendarLast.date) < calendarNextDate(endDate)) {
				d = calendarNextDate(d);

				calendarLast.next = new calendarElement();
				calendarLast.next.prev = calendarLast;
				calendarLast = calendarLast.next;
				calendarLast.init();
				calendarMove(calendarLast, d);
			}

			// Creating loop of
			calendarFirst.prev = calendarLast;
			calendarLast.next = calendarFirst;
			updateCalendarFixed();
		};

		var headCache = "";
		var updateCalendarFixed = function () {
			if (headCache !== calendarFirst.next.next.header) {
				headCache = calendarFirst.next.next.header;
				$fixed.html(headCache);
			}
		};

		var setModename = function (d) {
			if (d < 60) {
				RM.$parent.modeName = d + " " + settings.textModeDays;
				// $resolutionModeName.text(d + " " + settings.textModeDays);
				// $resolutionModeOutName.text(settings.textWeekView);
			} else if (d < 180) { // Week Mode, one calendarElement per week
				RM.$parent.modeName = Math.round(d / 30) + " " + settings.textModeMonths;
				// $resolutionModeName.text(Math.round(d / 30) + " " + settings.textModeMonths);
				// $resolutionModeInName.text(settings.textDayView);
				// $resolutionModeOutName.text(settings.textMonthView);
			} else if (d < 360) {
				RM.$parent.modeName = Math.round(d / 30) + " " + settings.textModeMonths;
				// $resolutionModeName.text(Math.round(d / 30) + " " + settings.textModeMonths);
				// $resolutionModeInName.text(settings.textWeekView);
				// $resolutionModeOutName.text(settings.textQuartalView);
			} else if (d < 400) { // Month Mode, one calendarElement per month
				RM.$parent.modeName = Math.round(d / 90) + " " + settings.textModeQ;
				// $resolutionModeName.text(Math.round(d / 90) + " " + settings.textModeQ);
				// $resolutionModeInName.text(settings.textMonthView);
				// $resolutionModeOutName.text(settings.textYearView);
			} else {
				RM.$parent.modeName = Math.round(d / 360) + " " + settings.textModeYears;
				// $resolutionModeName.text(Math.round(d / 360) + " " + settings.textModeYears);
				// $resolutionModeInName.text(settings.textQuartalView);
			}
			// RM.$parent.modeName = "test";
			// RM.$parent.$apply();
		};

		var limitVP = function (pos) {
			var vpMargin = 50;
			if (typeof showEndPixels !== "undefined" && pos < -showEndPixels - vpMargin && (showEndPixels > showStartPixels)) {
				pos = -showEndPixels - vpMargin;
			}

			if (typeof showStartPixels !== "undefined" && pos > -showStartPixels + vpMargin) {
				pos = -showStartPixels + vpMargin;
			}
			return pos;
		};

		var rollCalendarToDate = function (showDate) {
			$anchor.hide();
			while (calendarPrevDate(showDate) < calendarNextDate(calendarFirst.date)) {
				calendarMove(calendarLast, calendarPrevDate(calendarFirst.date));
				calendarFirst = calendarFirst.prev;
				calendarLast = calendarLast.prev;
			}

			while (calendarPrevDate(showDate) > calendarNextDate(calendarFirst.date)) {
				calendarMove(calendarFirst, calendarNextDate(calendarLast.date));
				calendarFirst = calendarFirst.next;
				calendarLast = calendarLast.next;
			}
			$anchor.show();
		};
		var moveViewPoint = function (pos, animate) {
			var doMoving = function () {

				var showDate = pixelsToDate(pos);
				savedSettings.zeroDate = addDays(showDate, int(visibleDays / 2));
				RMService.savePortfolioSettings(settings.portfolioId);

				rollCalendarToDate(showDate);
				updateCalendarFixed();
			};

			if (animate) {
				$anchor.animate({"margin-left": pos + "px"}, 500);
				$hAnchor.animate({"margin-left": pos + "px"}, 500);
				setTimeout(doMoving, 250);
			} else {
				$anchor.css("margin-left", pos + "px");
				$hAnchor.css("margin-left", pos + "px");
				doMoving();
			}
		};

		CA.setResolution = function (d) {
			calendarIsVisible = true;

			if (typeof d === "undefined") {
				d = visibleDays;
			}
			visibleDays = d;
			savedSettings.visibleDays = d;
			dayPixels = calWidth / visibleDays;
			mspx = step / dayPixels;

			var showDate = addDays(savedSettings.zeroDate, -int(visibleDays / 2));
			viewPoint = -int(dateToPixels(showDate));
			populateContinuous();
			moveViewPoint(viewPoint);

			updateBars();
		};
		CA.getResolution = function() {
			return visibleDays;
		}
		CA.moveCalendar = function (d) {
			if (typeof d === 'undefined') {
				d = today;
			} else {
				if (visibleDays < 60) {
					d.setDate(d.getDate() - 1);
				} else if (visibleDays < 200) {
					d.setDate(d.getDate() - 4);
				} else if (visibleDays < 400) {
					d.setDate(d.getDate() - 15);
				} else {
					d.setDate(d.getDate() - 30);
				}
			}

			viewPoint = -dateToPixels(d);
			moveViewPoint(viewPoint);
		};

		var calendarInitialized = false;
		CA.reDraw = function () {
			setHeight();
			if (!calendarInitialized) {
				dayPixels = calWidth / visibleDays;
				mspx = step / dayPixels;
				calendarInitialized = true;
			}
			CA.setResolution(visibleDays);
			CA.setTops();
		};

		var setWidth = function () {
			calWidth = calendarBody.width();
			if (typeof settings.$headerHolder !== 'undefined') {
				settings.$headerHolder.outerWidth(calWidth);
			}
		};

		var setHeight = function () {
			setWidth();
			calHeight = $barTable.height();
			$wrapper.css({"height": calHeight + "px"});
			$hWrapper.css({"width": calWidth + "px"});
		};

		var delayHeight;
		CA.setHeight = function () {
			clearTimeout(delayHeight);
			delayHeight = setTimeout(setHeight, 0);

		};
		CA.initialize = function () {
			// CA.setTops();			
		};

		CA.initializeArrows = function () {
			var i;
			var cc = linkedTasks.length;

			for (i = 0; i < cc; i++) {
				AM.addArrow(linkedTasks[i].type, linkedTasks[i].leadtime, linkedTasks[i].DI, RMService.itemSet[linkedTasks[i].tId]);
			}
			cc = dependencyArrows.length;
			for (i = 0; i < cc; i++) {
				var o = dependencyArrows[i];
				if (typeof o.from != 'undefined' && typeof RMService.itemSet[o.to] != 'undefined') {
					var to = RMService.itemSet[o.to];
					var toIcon = undefined;
					var fromIcon = "icon" + o.fromIcon;
					$.each(to.data.bar_icons, function (c, item) {
						if (item.item_id == o.toIcon) {
							toIcon = "icon" + c;
							return;
						}
					});
					if (toIcon) AM.addIconArrow(o.from, fromIcon, to, toIcon);
				}
			}
			drawCriticalPath();
		};

		var linkedTasks = [];
		var dependencyArrows = [];
		var datediff = function (d1, d2, days) {
			var dd = Math.round((new Date(d2) - new Date(d1)) / step) - days;
			if (isNaN(dd)) {
				dd = 0;
			}
			return dd;
		};
		var affected = {};
		var drawAffected = function () {
			$.each(affected, function (id, bar) {
				bar.applyBarChanges();
				bar.draw(true);
			});
		};
		var onPath = {};
		var clearCriticalPath = function () {
			angular.forEach(onPath, function (bar) {
				bar.$ele.removeClass("critical");
			});
			AM.removeCriticals();
			onPath = {};
		}
		var drawCriticalPath = function () {
			// RM.$ele.find(".bar.critical").removeClass("critical");
			// AM.removeCriticals();			
			var lastBar = undefined;
			angular.forEach(calendarRows, function (row) {
				var bar = row.bars.ganttBar;
				if (row.owner.data.task_type == 1) {
					if (typeof lastBar !== 'undefined') {
						if (lastBar.getEndDate() < bar.getEndDate()) {
							lastBar = bar;
						}
					} else {
						lastBar = bar;
					}
				}
			});
			if (typeof lastBar !== "undefined") {
				lastBar.pathW(lastBar);
			}
		}


		CA.initCalendarBar = function (DI, barType) {
			var row = DI.calendarRow;
			var wApplied = 0;
			var eApplied = 0;
			var applied = 0;
			if (typeof row.bars[barType] !== 'undefined') {
				return row.bars[barType];
			}

			var bar = {
				visible: true,
				owner: DI,
				editable: false
			};


			bar.getStartDate = function () {
				return newUTCDate(DI.data[RMService.alias[3]]);
			};
			bar.getEndDate = function () {
				return newUTCDate(DI.data[RMService.alias[4]]);
			};
			bar.setStartDate = function (date) {
				DI.data[RMService.alias[3]] = date;

				return date;
			};
			bar.setEndDate = function (date) {
				DI.data[RMService.alias[4]] = date;
				return date;
			};
			bar.applyBarChanges = function () {
				DI.updateFields(true).setDataChanged();
				if (!DI.parent.isRoot) {
					DI.parent.setDataFromChildren()
				}
				return bar;
			};
			bar.hide = function () {
				bar.$ele.css('display', 'none');
				bar.visible = false;
			};
			bar.show = function () {
				bar.$ele.show();
				bar.visible = true;
			};
			bar.draw = function (ignoreArrows) {
				if (typeof DI.parent !== 'undefined' &&
					typeof DI.parent.data !== 'undefined' &&
					DI.parent.data.task_type == 21) {
					bar.left = 0;
					bar.width = 0;
					row.sprintRow = true;
				} else {
					var sd = bar.getStartDate();
					var ed = bar.getEndDate();
					bar.left = int(dateToPixels(sd));
					bar.width = int(dateDifPixels(sd, ed) + dayPixels);
				}
				if (row.visible && bar.visible) {
					bar.$ele.css({'left': bar.left, 'width': bar.width});
					if (DI.editable() && !(DI.inheritDates && DI.data.task_type ==2)) {
						if (!bar.editable) {
							bar.editable = true;
							bar.$ele.addClass("editable");
						}
					} else {
						if (bar.editable) {
							bar.editable = false;
							bar.$ele.removeClass("editable");
						}
					}
				}

				if (typeof ignoreArrows === "undefined") {
					AM.dayPixels = dayPixels;
					$.each(DI.myArrows, function (id, AR) {
						if (typeof AR !== "undefined") {
							AR.update();

						}
					});
				}
				bar.drawBarIcon();
			};
			bar.drawBarIcon = function () {
			};
			bar.initMove = function () {
				wApplied = 0;
				eApplied = 0;
			};
			bar.applyChanges = function (w, e) { 
				affected[bar.owner.data.item_id] = bar;
				var wDate = bar.getStartDate();
				var eDate = bar.getEndDate();
				wDate = addDays(wDate, w);
				eDate = addDays(eDate, e);
				if (wDate <= eDate) {
					bar.setStartDate(wDate);
					bar.setEndDate(eDate);
				}
			};
			bar.recursiveW = function (delta, callerId) {
				$.each(DI.myArrows, function (id, AR) {
					if (typeof AR !== "undefined") {
						if (AR.toDI.data.item_id == DI.data.item_id) {
							var aDI = AR.fromDI;
							if (aDI.data.item_id !== callerId) {
								if (!aDI.editable()) {
									return true;
								}
								if (typeof affected[aDI.data.item_id] === "undefined") {
									var joinedBar = aDI.calendarRow.bars.ganttBar;
									joinedBar.applyChanges(delta, delta);
									joinedBar.recursiveE(delta, callerId);
									joinedBar.recursiveW(delta, callerId);
								}
							}

						}
					}
				});
			};
			bar.recursiveE = function (delta, callerId) {
				$.each(DI.myArrows, function (id, AR) {
					if (typeof AR !== "undefined") {
						if (AR.fromDI.data.item_id == DI.data.item_id) {
							var aDI = AR.toDI;
							if (aDI.data.item_id !== callerId) {
								if (!aDI.editable()) {
									return true;
								}
								if (typeof affected[aDI.data.item_id] === "undefined") {
									var joinedBar = aDI.calendarRow.bars.ganttBar;
									joinedBar.applyChanges(delta, delta);
									joinedBar.recursiveE(delta, callerId);
									joinedBar.recursiveW(delta, callerId);
								}
							}

						}
					}
				});
			};
			bar.pathW = function (lastBar) {
				bar.$ele.addClass("critical");
				$.each(DI.myArrows, function (id, AR) {
					if (typeof AR !== "undefined") {
						if (AR.toDI.data.item_id == DI.data.item_id) {
							var aDI = AR.fromDI;

							if (aDI.data.item_id !== lastBar.owner.data.item_id && typeof onPath[aDI.data.item_id] === "undefined") {

								var joinedBar = aDI.calendarRow.bars.ganttBar;
								onPath[aDI.data.item_id] = joinedBar;
								var difference;
								switch (AR.type) {
									case 0:	// end to start
										difference = datediff(bar.getStartDate(), joinedBar.getEndDate(), -AR.leadtime);
										break;
									case 1: // start to start
										difference = datediff(bar.getStartDate(), joinedBar.getStartDate(), AR.leadtime + 1);
										break;
									case 2: // end to end
										difference = datediff(bar.getEndDate(), joinedBar.getEndDate(), -AR.leadtime + 1);
										break;
									case 3: // start to end	
										difference = datediff(bar.getEndDate(), joinedBar.getStartDate(), AR.leadtime);
										break;
								}
								if (difference >= -1) {
									AR.setCritical();
									joinedBar.pathW(lastBar);
								}
							}
						}
					}
				});
			};
			bar.slackW = function (delta, callerId) {
				$.each(DI.myArrows, function (id, AR) {
					if (typeof AR !== "undefined") {
						if (AR.toDI.data.item_id == DI.data.item_id) {
							var aDI = AR.fromDI;
							if (aDI.data.item_id !== callerId) {
								if (!aDI.editable()) {
									return true;
								}
								if (typeof affected[aDI.data.item_id] === "undefined") {
									var joinedBar = aDI.calendarRow.bars.ganttBar;
									var difference;
									switch (AR.type) {
										case 0:	// end to start
											difference = datediff(bar.getStartDate(), joinedBar.getEndDate(), -AR.leadtime);
											break;
										case 1: // start to start
											difference = datediff(bar.getStartDate(), joinedBar.getStartDate(), -AR.leadtime + 1);
											break;
										case 2: // end to end
											difference = datediff(bar.getEndDate(), joinedBar.getEndDate(), -AR.leadtime + 1);
											break;
										case 3: // start to end	
											difference = datediff(bar.getEndDate(), joinedBar.getStartDate(), -AR.leadtime);
											break;
									}
									if (difference >= -1 && delta < 0) {
										if (-delta > difference + 1) {
											delta = -(difference + 1);
										}
										joinedBar.applyChanges(delta, delta);
										joinedBar.slackW(delta, callerId);
									}
								}
							}
						}
					}
				});
			};
			bar.slackE = function (delta, callerId) {
				$.each(DI.myArrows, function (id, AR) {
					if (typeof AR !== "undefined") {
						if (AR.fromDI.data.item_id == DI.data.item_id) {
							var aDI = AR.toDI;
							if (aDI.data.item_id !== callerId) {
								if (!aDI.editable()) {
									return true;
								}
								if (typeof affected[aDI.data.item_id] === "undefined") {
									var joinedBar = aDI.calendarRow.bars.ganttBar;
									var difference;
									switch (AR.type) {
										case 0:	// end to start
											difference = datediff(bar.getEndDate(), joinedBar.getStartDate(), AR.leadtime);
											break;
										case 1: // start to start
											difference = datediff(bar.getStartDate(), joinedBar.getStartDate(), AR.leadtime - 1);
											break;
										case 2: // end to end
											difference = datediff(bar.getEndDate(), joinedBar.getEndDate(), AR.leadtime - 1);
											break;
										case 3: // start to end	
											difference = datediff(bar.getStartDate(), joinedBar.getEndDate(), AR.leadtime);
											break;
									}
									if (difference <= 1 && delta > 0) {
										if (-delta < difference - 1) {
											delta = -(difference - 1);
										}
										joinedBar.applyChanges(delta, delta);
										joinedBar.slackE(delta, callerId);
									}
								}
							}
						}
					}
				});
			};
			bar.cumulativeChanges = function (wDelta, eDelta) {
				var wApply = 0;
				var eApply = 0;

				if (wDelta !== wApplied) {
					wApply = wDelta - wApplied;
					wApplied = wDelta;
				}
				if (eDelta !== eApplied) {
					eApply = eDelta - eApplied;
					eApplied = eDelta;
				}

				if (wApply !== 0 || eApply !== 0) {
					var id = DI.data.item_id;
					affected = {};
					bar.applyChanges(wApply, eApply);
					if (typeof CA.slacks !== "undefined") {
						var doFrom = (CA.slacks.indexOf("from") !== -1);
						var doTo = (CA.slacks.indexOf("to") !== -1);
						var doSlack = (CA.slacks.indexOf("slack") !== -1);
						var doAll = (CA.slacks.indexOf("all") !== -1);
						if (doFrom && wApply !== 0) {
							if (doSlack) {
								bar.slackW(wApply, id);
							} else {
								bar.recursiveW(wApply, id);
							}
						}
						if (doTo && eApply !== 0) {
							if (doSlack) {
								bar.slackE(eApply, id);
							} else {
								bar.recursiveE(eApply, id);
							}
						}
					}
					drawAffected();
				}
			};
			bar.move = function (dx) {
				if (settings.showAllocation && barType === 'ganttBar') {
					return;
				}

				var delta = Math.round(dx / dayPixels);
				bar.cumulativeChanges(delta, delta);
			};
			bar.moveW = function (dx) {
				if (settings.showAllocation && barType === 'ganttBar') {
					return;
				}
				var delta = Math.round(dx / dayPixels);
				bar.cumulativeChanges(delta, 0);
			};
			bar.moveE = function (dx) {
				if (settings.showAllocation && barType === 'ganttBar') {
					return;
				}

				var delta = Math.round(dx / dayPixels);
				bar.cumulativeChanges(0, delta);
			};
			bar.addIcon = function (icon, color) {
				if (typeof bar.$icon === "undefined") {
					bar.$icon = $("<div class='icon material-icons'>" + RMService.sanitize(icon) + "</div>").appendTo(bar.$ele);
				}
				if (typeof color === "undefined") {
					color = "";
				}
				bar.$icon.css({"color": color})
			};
			bar.removeIcon = function () {
				if (typeof bar.$icon !== "undefined") {
					bar.$icon.remove();
					bar.$icon = undefined;
				}

			};

			switch (barType) {
				case 'allocationDateRange':
					bar.getStartDate = function () {
						return newUTCDate(row.currentDateSelection.startDate);
					};

					bar.getEndDate = function () {
						return newUTCDate(row.currentDateSelection.endDate);
					};

					bar.setStartDate = function (date) {
						row.currentDateSelection.startDate = date;

						return date;
					};

					bar.setEndDate = function (date) {
						row.currentDateSelection.endDate = date;

						return date;
					};

					bar.applyBarChanges = function () {
						return bar;
					};
					break;
			}
			switch (Number(DI.data.task_type)) {
				case 1:
					bar.$ele = $('<div class="bar task" barType="' + RMService.sanitize(barType) + '"></div>')
						.appendTo(row.$td)
						.append('<div class="w"></div><div class="done"></div><div class="e"></div>');
					break;
				case 2:
					bar.$ele = $('<div class="bar phase" barType="' + RMService.sanitize(barType) + '"></div>')
						.appendTo(row.$td)
						.append('<div class="w-caret"></div><div class="e-caret"></div>');
					break;
				case 21:
					bar.$ele = $('<div class="bar sprint" barType="' + RMService.sanitize(barType) + '"></div>')
						.appendTo(row.$td)
						.append('<div class="w"></div><div class="e"></div>');
					break;
				case 50:
					bar.$ele = $('<div class="bar allocation" barType="' + RMService.sanitize(barType) + '"></div>')
						.appendTo(row.$td)
						.append('<div class="w-caret"></div><div class="e-caret"></div>' +
							'<div class="w"></div><div class="e"></div>');
					break;
				default:
					bar.$ele = $('<div class="bar process" barType="' + RMService.sanitize(barType) + '"></div>')
						.appendTo(row.$td)
						.append('<div class="w"></div><div class="e"></div>');

			}

			row.bars[barType] = bar;
			return bar;
		};

		function iconBase(row, data, titleColumn) {
			this.end_date = data.end_date;
			this.list_symbol = data.list_symbol;
			this.row = row;

			this.color = "";
			if (data.list_symbol_fill !== null) this.color = RMService.getColor(data.list_symbol_fill);
			titleColumn = data[titleColumn] ? data[titleColumn] : "";
			this.$ele = $("<div title='" + RMService.sanitize(titleColumn) + "' class='icon material-icons'>" + RMService.sanitize(this.list_symbol) + "</div>").appendTo(row.$td);
		}

		iconBase.prototype.draw = function () {
			this.left = int(dateToPixels(this.end_date));
			this.$ele.css({'left': this.left, "color": this.color});
		};

		CA.initCalendarIcons = function (DI, iconCollection, titleColumn) {
			var row = DI.calendarRow;
			var i;
			var l = iconCollection.length;
			for (i = 0; i < l; i++) {
				row.bars["icon" + i] = new iconBase(row, iconCollection[i], titleColumn);
			}
		};

		CA.setTops = function () {
			if (!settings.showAllocation) {
				$.each(calendarRows, function (id, r) {
					r.top = r.$tr.position().top;
				});
			}
		};
		CA.initCalendarRow = function (DI) {
			var id = DI.data.item_id;
			var row = {};
			DI.calendarRow = row;
			row.owner = DI;
			row.visible = false;
			row.sprintRow = false;
			row.$tr = $('<tr rowid="' + RMService.sanitize(id) + '"></tr>');
			row.$td = $('<td></td>').appendTo(row.$tr);
			row.bars = {};

			row.currentDateSelection = {};

			calendarRows[id] = row;

			if (typeof DI.data.bar_icons !== 'undefined') {
				$.each(DI.data.bar_icons, function (i1, ele) {
					if (typeof ele["dependencies"] !== 'undefined' && ele["dependencies"] != null) {
						var list = ele["dependencies"].split(',');
						$.each(list, function (i2, item) {
							var components = item.split(':');

							dependencyArrows.push({
								from: DI,
								fromIcon: i1,
								to: parseInt(components[0]),
								toIcon: parseInt(components[1])
							});
						});
					}
				});
			}

			$.each(DI.data.linked_tasks, function (id, ele) {
				ele.leadtime = parseInt(ele.leadtime);
				if (isNaN(ele.leadtime)) {
					ele.leadtime = 0;
				}
				linkedTasks.push({
					DI: DI,
					tId: id,
					type: ele.link_type,
					leadtime: ele.leadtime
				});
			});

			row.updateArrows = function () {
				CA.setTops();
				AM.updateAll();
			};
			row.deleteArrows = function () {
				AM.delRow(DI);
			};
			row.append = function (ignoreTops) {

				row.show();
				row.$tr.appendTo($barTable);
				if (typeof ignoreTops == "undefined" || !ignoreTops) {
					CA.setTops();
				}
			};

			row.prepend = function (ignoreTops) {
				row.show();
				var $h = $barTable.find("tr:first");
				$h.after(row.$tr);
				if (typeof ignoreTops == "undefined" || !ignoreTops) {
					CA.setTops();
				}
			};

			row.after = function (target, ignoreTops) {
				if (typeof target.calendarRow.$tr === "undefined") {
					return false
				}
				row.show();
				target.calendarRow.$tr.after(row.$tr);
				if (typeof ignoreTops == "undefined" || !ignoreTops) {
					CA.setTops();
				}
			};

			row.before = function (target, ignoreTops) {
				if (typeof target.calendarRow.$tr === "undefined") {
					return false
				}
				row.show();
				target.calendarRow.$tr.before(row.$tr);
				if (typeof ignoreTops == "undefined" || !ignoreTops) {
					CA.setTops();
				}
			};

			row.draw = function () {
				if (!calendarIsVisible) return false;
				angular.forEach(row.bars, function (bar) {
					bar.draw();
				});
			};

			row.show = function () {
				row.$tr.show();
				row.visible = true;

				row.draw();
			};

			row.hide = function () {
				row.$tr.css('display', 'none');
				row.visible = false;
				row.updateArrows();
			};

			row.delete = function () {
				row.$tr.css('display', 'none');
				row.visible = false;
				row.deleteArrows();
			};

			return row;
		};

		var updateBars = function () {
			if (!calendarIsVisible) return false;
			angular.forEach(calendarRows, function (row) {
				row.draw();
			});
			CA.setTops();
			AM.updateAll();
		};

		CA.getBody = function () {
			return settings.$mainHolder;
		};

		CA.getHead = function () {
			return settings.$headerHolder;
		};

		CA.emptyCalendarRow = function (cls) {
			$("<tr><td class='" + RMService.sanitize(cls) + "'></td></tr>").appendTo($barTable);

		};
		/* ____     _ __ 
		  /  _/__  (_) /_
		 _/ // _ \/ / __/
		/___/_//_/_/\_*/

		// Dom elements

		var calendarBody = CA.getBody();
		var calendarHead = CA.getHead();
		calendarBody.closest('table').addClass('has-calendar');

		if (typeof calendarHead !== 'undefined') {
			calendarHead.closest('table').addClass('has-calendar');
		}

		var calWidth = 0;
		var calHeight = CA.height();//-$header.outerHeight();
		var $fixed = $("<div class='calendarFixedHead'></div>").appendTo(settings.$mainHolder);
		var $wrapper = $("<div class='calendarWrapper'></div>");
		var $anchor = $("<div class='calendarAnchor'></div>").appendTo($wrapper);

		calendarBody.append($wrapper);
		CA.$anchor = $anchor;
		var calendarIsVisible = false;

		var $today = $("<div class='today' >&nbsp;</div>").appendTo($anchor);
		var $disableStart = $("<div class='calendarLimitter'></div>").appendTo($anchor);
		var $disableEnd = $("<div class='calendarLimitter'></div>").appendTo($anchor);
		var $barTable = $("<table class='barTable'></table>").appendTo($anchor).append("<tr><th>&nbsp;</th></tr>");

		var $hWrapper = $("<div class='calendarWrapper' style='width:height:22px;'></div>");
		var $hAnchor = $("<div class='calendarAnchor'></div>").appendTo($hWrapper);

		if (typeof calendarHead !== 'undefined') {
			calendarHead.append($hWrapper);
		}

		var $hToday = $("<div class='today' >&nbsp;</div>").appendTo($hAnchor);
		var $hDisableStart = $("<div class='calendarLimitter'></div>").appendTo($hAnchor);
		var $hDisableEnd = $("<div class='calendarLimitter'></div>").appendTo($hAnchor);

		/*
		//FIX IT, TOO GENERAL
		var $resolutionModeName = $(".RMCalendarResolution > .modeName");
		//FIX IT, TOO GENERAL
		var $resolutionModeIn = $(".RMCalendarZoomIn");
		var $resolutionModeInName = $resolutionModeIn.find(".modeName");
		//FIX IT, TOO GENERAL
		var $resolutionModeOut = $(".RMCalendarZoomOut");
		var $resolutionModeOutName = $resolutionModeOut.find(".modeName");

		var $resolutionModeToggle = $(".RMCalendarZoomToggle");
		//FIX IT, TOO GENERAL
		var $calendarToday = $(".RMCalendarToday");
		//FIX IT, TOO GENERAL
		var $calendarWarning = $(".RMWarnings");
*/
		// Common variables
		var viewPoint = savedSettings.viewPoint;
		var step = 86400000;// = (1000*60*60*24)
		var visibleDays = savedSettings.visibleDays;
		savedSettings.zeroDate = typeof savedSettings.zeroDate !== 'undefined' ? savedSettings.zeroDate : new Date();
		var dayPixels;
		var mspx;

		var calendarNextDate = function () {
		};
		var calendarPrevDate;
		var calendarAreaStart = function () {
		};
		CA.calendarAreaStart = function (d) {
			return calendarAreaStart(d);
		};
		CA.calendarNextDate = function (d) {
			return calendarNextDate(d);
		};
		// Date variables
		var today = newUTCDate();

		var getSteps = function () {
			var steps = {};
			var l = settings.calendarSteps.length;
			var i;
			for (i = 0; i < l; i++) {
				if (settings.calendarSteps[i] < visibleDays) {
					if (typeof steps.prev !== "undefined") {
						steps.prevprev = steps.prev;
					}

					steps.prev = settings.calendarSteps[i];
				} else if (settings.calendarSteps[i] > visibleDays) {
					if (typeof steps.next !== "undefined") {
						steps.nextnext = settings.calendarSteps[i];
						break;
					}

					steps.next = settings.calendarSteps[i];
				}
			}
			return steps;
		};

		var initStepsDisable = function () {
			var steps = getSteps();

			if (typeof steps.next !== "undefined") {
				RM.$parent.modifyZoomOut = true;
			} else {
				RM.$parent.modifyZoomOut = false;
			}

			if (typeof steps.prev !== "undefined") {
				RM.$parent.modifyZoomIn = true;
			} else {
				RM.$parent.modifyZoomIn = false;
			}

			return steps;
		};

		initStepsDisable();
		/* __ __             ____
		  / // /__ ____  ___/ / /__ _______
		 / _  / _ `/ _ \/ _  / / -_) __(_-<
		/_//_/\_,_/_//_/\_,_/_/\__/_/ /__*/
		RM.$parent.calendarZoomIn = function () {
			if (RM.$parent.modifyZoomIn) {
				RM.$parent.modifyZoomOut = true;
				var steps = getSteps();
				if (typeof steps.prev !== "undefined") {
					CA.setResolution(steps.prev);
				}
				if (typeof steps.prevprev === "undefined") {
					RM.$parent.modifyZoomIn = false;
				}
			}
		}
		CA.calendarZoomOut = function() {
			var steps = getSteps();
			if (typeof steps.next !== "undefined") {
				CA.setResolution(steps.next);
			}
		}
		RM.$parent.calendarZoomOut = function () {
			if (RM.$parent.modifyZoomOut) {
				RM.$parent.modifyZoomIn = true;
				var steps = getSteps();

				if (typeof steps.next !== "undefined") {
					CA.setResolution(steps.next);
				}
				if (typeof steps.nextnext === "undefined") {
					RM.$parent.modifyZoomOut = false;
				}
			}
		}
		CA.calendarZoomIn = function() {
			var steps = getSteps();
			if (typeof steps.prev !== "undefined") {
				CA.setResolution(steps.prev);
			}
		}
		CA.calendarToday = function() {
			viewPoint = limitVP(-dateToPixels(today));
			moveViewPoint(viewPoint);
		}
		RM.$parent.calendarToday = function () {
			viewPoint = limitVP(-dateToPixels(today));
			moveViewPoint(viewPoint);
		}

		RM.$parent.resolutionModeToggle = function () {
			var steps = getSteps();

			if (steps.next) {
				CA.setResolution(steps.next);
			} else {
				CA.setResolution(steps.prev);
			}
		}

		/*
		$resolutionModeIn.on("click", '.btn.btn-icon', function () {
			if ($(this).hasClass("disabled")) {
				return false;
			}

			$resolutionModeOut.removeClass("disabled");
			var steps = getSteps();

			if (typeof steps.prev !== "undefined") {
				CA.setResolution(steps.prev);
			}
			if (typeof steps.prevprev === "undefined") {
				$resolutionModeIn.addClass("disabled");
			}
		});
		$resolutionModeOut.on("click", '.btn.btn-icon', function () {
			if ($(this).hasClass("disabled")) {
				return false;
			}

			$resolutionModeIn.removeClass("disabled");
			var steps = getSteps();

			if (typeof steps.next !== "undefined") {
				CA.setResolution(steps.next);
			}
			if (typeof steps.nextnext === "undefined") {
				$resolutionModeOut.addClass("disabled");
			}
		});

		$resolutionModeToggle.on("click", '.btn.btn-icon', function () {
			var steps = getSteps();

			if (steps.next) {
				CA.setResolution(steps.next);
			} else {
				CA.setResolution(steps.prev);
			}
		});

		$calendarToday.on("click", ".btn.btn-icon", function () {
			viewPoint = limitVP(-dateToPixels(today));
			moveViewPoint(viewPoint);
		});
*/
		var arrowStart;
		var dblClickTimeout;
		var calendarMouseBinds = function (e) {
			stopEvent(e);
			clearCriticalPath();
			var sX = int(e.pageX);
			var sY = int(e.pageY);
			var hasMoved = false;
			var bm = viewPoint;
			var $target = $(e.target);
			if ($target.closest(".bar.editable").length > 0) {
				$(document.body).addClass("ew-resize");
				var id = $target.closest("tr").attr("rowid");
				var DI = RMService.itemSet[id];
				var bar = DI.calendarRow.bars[$target.closest(".bar").attr("barType")];

				bar.initMove();
				var callerClass = $target.attr("class");
				switch (callerClass) {
					case "w":
						$(document).on("mousemove.barDrag", function (e) {
							bar.moveW(e.pageX - sX);
							hasMoved = true;
						});
						break;
					case "e":
						$(document).on("mousemove.barDrag", function (e) {
							bar.moveE(e.pageX - sX);
							hasMoved = true;
						});
						break;
					default:
						$(document).on("mousemove.barDrag", function (e) {
							bar.move(e.pageX - sX);
							hasMoved = true;
						});
				}


				$(document).on("mouseup.barDrag", function (e) {
					$(document).off("mousemove.barDrag");
					$(document).off("mouseup.barDrag");
					$(document.body).removeClass("ew-resize");

					if (!hasMoved && $target.closest(".task, .sprint").length > 0) {
						if (typeof arrowStart === "undefined") {
							arrowStart = DI;
							DI.calendarRow.bars.ganttBar.$ele.addClass("selected");
						} else {
							if (arrowStart.data.item_id !== DI.data.item_id) {
								var AR = AM.getArrow(arrowStart, DI);
								if (typeof AR !== "undefined") {
									AR.fromDI = arrowStart;
									AR.toDI = DI;
									AR.openMenu(e.pageY, e.pageX);
									AM.dayPixels = dayPixels;
									AR.update();
								} else {
									var AR = AM.addArrow(0, 0, arrowStart, DI);
									AR.saveArrow();
								}
							}
							arrowStart.calendarRow.bars.ganttBar.$ele.removeClass("selected");
							arrowStart = undefined;
						}
					}
					drawCriticalPath();
				});
			} else { // not clicked on top of the bar in task timeline			
				$(document).on("mousemove.calendarDrag", function (e) {
					bm = viewPoint + e.pageX - sX;
					moveViewPoint(bm);
					hasMoved = true;
				});
				$(document).on("mouseup.calendar", function (e) {
					stopEvent(e);
					if (hasMoved && viewPoint != bm) {
						moveViewPoint(bm, false);
						viewPoint = limitVP(bm);
						moveViewPoint(viewPoint, true);
					} else {
						if (typeof dblClickTimeout === "undefined") { //firstClick, binding timeout
							var $cell = $target.closest(".cell"); // finding .cell from parents of the target
							if ($cell.length > 0) { // clicked on a cell
								dblClickTimeout = setTimeout(function () {
									RM.clickActions.cellClick($cell);
									dblClickTimeout = undefined;
								}, 300);
							} else { // not on the cell
								dblClickTimeout = setTimeout(function () {
									if (typeof hoverId !== "undefined") {
										var date = pixelsToDate(viewPoint + $wrapper.offset().left + (dayPixels / 2) - e.pageX);
										var sd = calendarAreaStart(date);
										var ed = calendarNextDate(sd);
										var topPixels = $hoverRow.offset().top;
										var leftPixels = viewPoint + $wrapper.offset().left + dateToPixels(sd);
										ed.setUTCDate(ed.getUTCDate() - 1);
										RM.clickActions.emptyClick(hoverId, sd, ed, leftPixels, topPixels);
										dblClickTimeout = undefined;
									}
								}, 300);
							}
						} else { // doubleClick happened
							if (typeof hoverId !== "undefined") {
								var date = pixelsToDate(viewPoint + $wrapper.offset().left + (dayPixels / 2) - e.pageX);
								var sd = calendarAreaStart(date);
								var ed = calendarNextDate(sd);
								ed.setUTCDate(ed.getUTCDate() - 1);
								RM.clickActions.dblClick(hoverId, sd, ed);
								clearTimeout(dblClickTimeout);
								dblClickTimeout = undefined;
							}

						}
					}
					$(document).off("mousemove.calendarDrag");
					$(document).off("mouseup.calendar");
					drawCriticalPath();
				});
			}
		};

		$wrapper.on("mousedown", calendarMouseBinds);
		$hWrapper.on("mousedown", calendarMouseBinds);
		var hoverId;
		var $hoverRow;
		$wrapper.on('mousemove', function (e) {
			var oldHoverId = hoverId;
			$wrapper.css('display', 'none');
			var $hover = $(document.elementFromPoint(e.pageX - $(document).scrollLeft(), e.pageY - $(document).scrollTop()));
			$wrapper.css('display', '');
			$hoverRow = $hover.closest("tr");
			hoverId = $hoverRow.attr("rowid");
			if (typeof hoverId !== "undefined" && hoverId !== oldHoverId) {
				if (typeof oldHoverId !== "undefined" && RMService.itemSet[oldHoverId] && RMService.itemSet[oldHoverId].$ele) RMService.itemSet[oldHoverId].$ele.removeClass("hover");
				if (RMService.itemSet[hoverId])	RMService.itemSet[hoverId].$ele.addClass("hover");
			}
		});

		$wrapper.on('mouseleave', function () {
			if (typeof hoverId !== "undefined" && RMService.itemSet[hoverId] && RMService.itemSet[hoverId].$ele) RMService.itemSet[hoverId].$ele.removeClass("hover");
			hoverId = undefined;
		});
		return CA;
	};
})(jQuery);