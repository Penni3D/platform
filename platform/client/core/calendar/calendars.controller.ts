﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.controller('CalendarsController', CalendarsController);

	CalendarsController.$inject = [
	    '$scope', 
        'MessageService', 
        'ViewService', 
        'CalendarService', 
        'ParentCalendars', 
        'UserCalendars', 
        'StateParameters'
    ];
	function CalendarsController($scope, MessageService, ViewService, CalendarService, ParentCalendars, UserCalendars, StateParameters) {
		$scope.calendars = ParentCalendars;
		$scope.header = {
			title: StateParameters.title
		};
				
		$scope.addCalendar = function () {
			let calendar = {
			    IsDefault: false,
                BaseCalendar: false
            };

			let save = function () {
				calendar.IsDefault = false;
				calendar.BaseCalendar = true;

				return CalendarService.newCalendar(calendar).then(function (data) {
					$scope.calendars.push(data);
				});
			};

			let content = {
				buttons: [{ text: 'MSG_CANCEL' }, { type: 'primary', onClick: save, text: 'CREATE' }],
				inputs: [{ type: 'string', label: 'CALENDAR_NAME', model: 'CalendarName' }],
				title: 'ADD_NEW_CALENDAR'
			};

			MessageService.dialog(content, calendar);
		};

		$scope.deleteCalendar = function (calendarId) {
			MessageService.confirm('DELETE_CALENDAR', function () {
				return CalendarService.deleteCalendar(calendarId).then(function () {
					let i = $scope.calendars.length;
					while (i--) {
						if ($scope.calendars[i].CalendarId == calendarId) {
							$scope.calendars.splice(i, 1);
							break;
						}
					}
				});
			});
		};

		$scope.showCalendar = function (calendarId)  {
			ViewService.view('calendar.editor', { params: { calendarId: calendarId }});
		};

		$scope.showUserCalendars = function () {
			$scope.calendars = $scope.calendars.concat(UserCalendars);
			$scope.moreIsShown = true;
		};
	}
})();
