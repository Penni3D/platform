﻿(function () {
	'use strict';

	angular
		.module('core')
		.config(CalendarConfig)
		.filter('date', DateFilter)
		.service('CalendarService', CalendarService);

	CalendarConfig.$inject = ['ConfigurationProvider'];

	function CalendarConfig(ConfigurationProvider) {
		ConfigurationProvider.addConfiguration('Calendar',
			{
				'Show weeknumber': {
					format: 'boolean',
					default: 'false'
				},
				'First quartal': {
					format: 'select',
					options: [{name: "January", value: 0}, {name: "April", value: 3}, {
						name: "July",
						value: 6
					}, {name: "October", value: 9}],
					default: 0
				}
			});
	}

	CalendarService.$inject = ['$q', 'ApiService', '$rootScope', 'Common'];

	function CalendarService($q, ApiService, $rootScope, Common) {
		let self = this;

		let inputDates = {};
		let userCalendarSupport = {
			calendarId: undefined,
			baseCalendarId: undefined,
			workHours: undefined,
			timesheetHours: undefined,
			weeklyHours: undefined,
			startDate: undefined,
			endDate: undefined,
		};
		let userCalendarData = {};
		let baseCalendarSupport = {
			calendarId: undefined,
			baseCalendarId: undefined,
			workHours: undefined,
			timesheetHours: undefined,
			weeklyHours: undefined,
			startDate: undefined,
			endDate: undefined,
		};
		let baseCalendarData = {};
		let inputCallbacks = {};

		let Calendars = ApiService.v1api('calendars');
		let UserCalendars = ApiService.v1api('calendars/User');
		let CalendarEvents = ApiService.v1api('calendars/Events');
		let CalendarStatics = ApiService.v1api('calendars/StaticHolidays');

		let timezones = null;

		function getTimezones() {
			if (timezones == null) {
				Calendars.get(['Timezones']).then(function (tzs) {
					timezones = tzs;
				});
			}
		}

		getTimezones();

		self.setGroupEndDate = function (group, date) {
			if (typeof inputDates[group] === 'undefined') {
				inputDates[group] = {};
			}

			inputDates[group].end = date;
		};

		self.setGroupStartDate = function (group, date) {
			if (typeof inputDates[group] === 'undefined') {
				inputDates[group] = {};
			}

			inputDates[group].start = date;
		};

		self.getGroupDates = function (group) {
			return inputDates[group];
		};

		self.getUTCDate = function (d, restrict = undefined, allUTC = false): Date {
			let date;

			if (typeof d === 'undefined' || d === null) {
				if (restrict) return d;
				date = new Date();
			} else {
				date = new Date(d);
				if (isNaN(date.getTime())) date = new Date();
			}

			let year = date.getFullYear();
			let month = date.getMonth();
			let day = date.getDate();
			if (allUTC) {
				day = date.getUTCDate();
				month = date.getUTCMonth();
				year = date.getUTCFullYear();
			}

			return new Date(Date.UTC(year, month, day));
		};

		self.dateTimeToUTCSql = function(date: any) : string {
			let y = new Intl.DateTimeFormat('fi-FI', {year: 'numeric', timeZone: 'Utc'}).format(new Date(date));
			let m = new Intl.DateTimeFormat('fi-FI', {month: '2-digit', timeZone: 'Utc'}).format(new Date(date));
			let d = new Intl.DateTimeFormat('fi-FI', {day: '2-digit', timeZone: 'Utc'}).format(new Date(date));
			let h = new Intl.DateTimeFormat('fi-FI', {hour: '2-digit', timeZone: 'Utc'}).format(new Date(date));
			let mi = new Intl.DateTimeFormat('fi-FI', {minute: '2-digit', timeZone: 'Utc'}).format(new Date(date));
			return y + "-" + m + "-" + d + " " + h + ":" + mi
		}

		self.getUTCDateTime = function (d, restrict = undefined, allUTC = false): Date {
			let date;

			if (typeof d === 'undefined' || d === null) {
				if (restrict) return d;
				date = new Date();
			} else {
				date = new Date(d);
				if (isNaN(date.getTime())) date = new Date();
			}

			if (!allUTC && $rootScope.clientInformation.hasOwnProperty('timezone') && $rootScope.clientInformation.timezone != null) {
				date = fromTimezone(date, timezones[$rootScope.clientInformation.timezone.split("_")[0]]);
			}

			return date;
		};

		function fromTimezone(date, timezone) {
			let utc = new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: 'UTC'}));
			let tz = new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: timezone}));
			date.setTime(date.getTime() + utc.getTime() - tz.getTime());
			return date;
		}

		let toBeFetched = [];
		let fetchCalendarTimer;

		function fetchUsersCalendarData() {
			let fetchData = toBeFetched[0];

			let sd = new Date(fetchData.startDate);
			let ed = new Date(fetchData.endDate);

			let userIds = [];
			for (let u of fetchData.users) {
				if (!isNaN(u)) userIds.push(u);
			}

			let dates = formatUserCalendarDate(sd, ed);
			fetchData.fetching = true;

			Calendars.new([dates.startDate, dates.endDate], userIds).then(function (calendarData) {
				angular.forEach(calendarData, function (userData, userId) {
					if (typeof userCalendarSupport[userId] === 'undefined') {
						userCalendarSupport[userId] = {
							calendarId: userData.CalendarId,
							baseCalendarId: userData.BaseCalendarId,
							workHours: userData.WorkHours,
							timesheetHours: userData.TimesheetHours,
							weeklyHours: userData.WeeklyHours,
							startDate: sd,
							endDate: ed,
						};
					} else {
						userCalendarSupport[userId] = {
							calendarId: userData.CalendarId,
							baseCalendarId: userData.BaseCalendarId,
							workHours: userData.WorkHours,
							timesheetHours: userData.TimesheetHours,
							weeklyHours: userData.WeeklyHours,
							startDate: sd < userCalendarSupport[userId].startDate ?
								sd : userCalendarSupport[userId].startDate,
							endDate: ed > userCalendarSupport[userId].endDate ?
								ed : userCalendarSupport[userId].endDate
						};
					}

					angular.forEach(userData.Exceptions, function (exception) {
						let date = new Date(exception.EventDate);

						let y = date.getFullYear();
						let m = date.getMonth() + 1;
						let d = date.getDate();

						if (typeof userCalendarData[y] === 'undefined') {
							userCalendarData[y] = {};
						}

						if (typeof userCalendarData[y][m] === 'undefined') {
							userCalendarData[y][m] = {};
						}

						if (typeof userCalendarData[y][m][d] === 'undefined') {
							userCalendarData[y][m][d] = {};
						}

						userCalendarData[y][m][d][userId] = {
							dayOff: exception.DayOff,
							holiday: exception.Holiday,
							notWork: exception.Notwork,
							eventType: exception.EventType
						};
					});
				});

				fetchData.defer.resolve();
				toBeFetched.shift();
			});
		}

		function prepareCalendarFetch(userId, startDate, endDate) {
			let i = toBeFetched.length;
			while (i--) {
				let fetch = toBeFetched[i];
				let j = fetch.users.length;

				if (fetch.fetching) {
					if (startDate >= fetch.startDate && endDate <= fetch.endDate) {
						while (j--) {
							if (fetch.users[j] == userId) {
								return fetch.defer.promise;
							}
						}
					}
				} else {
					if (startDate < fetch.startDate) {
						fetch.startDate = startDate;
					}

					if (endDate > fetch.endDate) {
						fetch.endDate = endDate;
					}

					while (j--) {
						if (fetch.users[j] == userId) {
							break;
						}
					}

					if (j === -1) {
						fetch.users.push(userId);
					}

					return fetch.defer.promise;
				}
			}

			let f = {
				startDate: startDate,
				endDate: endDate,
				users: [userId],
				defer: $q.defer()
			};

			toBeFetched.push(f);

			clearTimeout(fetchCalendarTimer);
			fetchCalendarTimer = setTimeout(fetchUsersCalendarData, 25);

			return f.defer.promise;
		}

		function formatUserCalendarDate(startDate, endDate) {
			let sd = startDate.getUTCFullYear() + '-' + (startDate.getUTCMonth() + 1) + '-' + startDate.getUTCDate();
			let ed = endDate.getUTCFullYear() + '-' + (endDate.getUTCMonth() + 1) + '-' + endDate.getUTCDate();

			return {startDate: sd, endDate: ed};
		}

		function combineUserCalendarExceptions(userId, startDate, endDate) {
			let calendarDates = {};
			let dateRunner = moment.utc(startDate);

			while (dateRunner.isSameOrBefore(endDate)) {
				let y = dateRunner.year();
				let m = dateRunner.month() + 1;
				let d = dateRunner.date();

				if (typeof calendarDates[y] === 'undefined') calendarDates[y] = {};
				if (typeof calendarDates[y][m] === 'undefined') calendarDates[y][m] = {};
				if (typeof calendarDates[y][m][d] === 'undefined') calendarDates[y][m][d] = {};

				if (typeof userCalendarData[y] === 'undefined' ||
					typeof userCalendarData[y][m] === 'undefined' ||
					typeof userCalendarData[y][m][d] === 'undefined' ||
					typeof userCalendarData[y][m][d][userId] === 'undefined'
				) {
					calendarDates[y][m][d] = {
						dayOff: false,
						holiday: false,
						notWork: false
					};
				} else {
					calendarDates[y][m][d] = angular.copy(userCalendarData[y][m][d][userId]);
				}

				let userCalendarDates = calendarDates[y][m][d];
				let supportCalendar = userCalendarSupport[userId];

				userCalendarDates.weeklyHours = supportCalendar ? supportCalendar.weeklyHours : 0;
				userCalendarDates.timesheetHours = supportCalendar ? supportCalendar.timesheetHours : 0;
				userCalendarDates.workHours = supportCalendar ? supportCalendar.workHours : 0;

				dateRunner.add(1, 'days');
			}

			return calendarDates;
		}

		let mToD = 1000 * 60 * 60 * 24;

		self.getUserWorkHours = function (userId) {
			let d = {
				workHours: 0,
				timesheetHours: 0,
				weeklyHours: -2
			};

			let userData = userCalendarSupport[userId];
			if (userData) {
				d.workHours = userData.workHours;
				d.timesheetHours = userData.timesheetHours;
				d.weeklyHours = userData.weeklyHours;
			}

			return d;
		};

		self.getUserCalendar = function (userId, startDate, endDate) {
			let promises = [];

			let sd = self.getUTCDate(startDate, undefined, true);
			let ed = self.getUTCDate(endDate, undefined, true);
			let sd6m = new Date(sd);
			let ed6m = new Date(ed);

			sd6m.setUTCMonth(sd6m.getUTCMonth() - 6);
			ed6m.setUTCMonth(ed6m.getUTCMonth() + 6);

			let userSupport = userCalendarSupport[userId];

			if (typeof userSupport === 'undefined') {
				promises.push(prepareCalendarFetch(userId, sd6m, ed6m));
			} else {
				let startDateDif = Math.round((sd.getTime() - userSupport.startDate.getTime()) / mToD);
				let endDateDif = Math.round((userSupport.endDate.getTime() - ed.getTime()) / mToD);

				if (startDateDif < 0) {
					promises.push(prepareCalendarFetch(userId, sd6m, userSupport.startDate));
				} else if (startDateDif < 90) {
					prepareCalendarFetch(userId, sd6m, userSupport.startDate);
				}

				if (endDateDif < 0) {
					promises.push(prepareCalendarFetch(userId, userSupport.endDate, ed6m));
				} else if (endDateDif < 90) {
					prepareCalendarFetch(userId, userSupport.endDate, ed6m);
				}
			}

			return $q.all(promises).then(function () {
				return combineUserCalendarExceptions(userId, sd, ed);
			});
		};

		let toBeFetchedBase = [];
		let fetchBaseCalendarTimer;

		function fetchBasesCalendarData() {
			let fetchData = toBeFetchedBase[0];

			let sd = new Date(fetchData.startDate);
			let ed = new Date(fetchData.endDate);

			let i = fetchData.baseCalendars.length;
			let baseCalendarIds = fetchData.baseCalendars[i - 1];
			i--;
			while (i--) {
				baseCalendarIds = baseCalendarIds + ',' + fetchData.baseCalendars[i];
			}

			let dates = formatUserCalendarDate(sd, ed);
			fetchData.fetching = true;

			Calendars.get(['base', baseCalendarIds, dates.startDate, dates.endDate]).then(function (calendarData) {
				angular.forEach(calendarData, function (bcData, baseCalendarId) {
					if (typeof baseCalendarSupport[baseCalendarId] === 'undefined') {
						baseCalendarSupport[baseCalendarId] = {
							calendarId: bcData.CalendarId,
							baseCalendarId: bcData.BaseCalendarId,
							workHours: bcData.WorkHours,
							timesheetHours: bcData.TimesheetHours,
							weeklyHours: bcData.WeeklyHours,
							startDate: sd,
							endDate: ed,
						};
					} else {
						baseCalendarSupport[baseCalendarId] = {
							calendarId: bcData.CalendarId,
							baseCalendarId: bcData.BaseCalendarId,
							workHours: bcData.WorkHours,
							timesheetHours: bcData.TimesheetHours,
							weeklyHours: bcData.WeeklyHours,
							startDate: sd < baseCalendarSupport[baseCalendarId].startDate ?
								sd : baseCalendarSupport[baseCalendarId].startDate,
							endDate: ed > baseCalendarSupport[baseCalendarId].endDate ?
								ed : baseCalendarSupport[baseCalendarId].endDate
						};
					}

					angular.forEach(bcData.Exceptions, function (exception) {
						let date = new Date(exception.EventDate);

						let y = date.getFullYear();
						let m = date.getMonth() + 1;
						let d = date.getDate();

						if (typeof baseCalendarData[y] === 'undefined') {
							baseCalendarData[y] = {};
						}

						if (typeof baseCalendarData[y][m] === 'undefined') {
							baseCalendarData[y][m] = {};
						}

						if (typeof baseCalendarData[y][m][d] === 'undefined') {
							baseCalendarData[y][m][d] = {};
						}

						baseCalendarData[y][m][d][baseCalendarId] = {
							dayOff: exception.DayOff,
							holiday: exception.Holiday,
							notWork: exception.Notwork
						};
					});
				});

				fetchData.defer.resolve();
				toBeFetchedBase.shift();
			});
		}

		function prepareBaseCalendarFetch(baseCalendarId, startDate, endDate) {
			let i = toBeFetchedBase.length;
			while (i--) {
				let fetch = toBeFetchedBase[i];
				let j = fetch.baseCalendars.length;

				if (fetch.fetching) {
					if (startDate >= fetch.startDate && endDate <= fetch.endDate) {
						while (j--) {
							if (fetch.baseCalendars[j] == baseCalendarId) {
								return fetch.defer.promise;
							}
						}
					}
				} else {
					if (startDate < fetch.startDate) {
						fetch.startDate = startDate;
					}

					if (endDate > fetch.endDate) {
						fetch.endDate = endDate;
					}

					while (j--) {
						if (fetch.baseCalendars[j] == baseCalendarId) {
							break;
						}
					}

					if (j === -1) {
						fetch.baseCalendars.push(baseCalendarId);
					}

					return fetch.defer.promise;
				}
			}

			let f = {
				startDate: startDate,
				endDate: endDate,
				baseCalendars: [baseCalendarId],
				defer: $q.defer()
			};

			toBeFetchedBase.push(f);

			clearTimeout(fetchBaseCalendarTimer);
			fetchBaseCalendarTimer = setTimeout(fetchBasesCalendarData, 25);

			return f.defer.promise;
		}

		function combineBaseCalendarExceptions(baseCalendarId, startDate, endDate) {
			let calendarDates = {};
			let dateRunner = moment.utc(startDate);

			while (dateRunner.isSameOrBefore(endDate)) {
				let y = dateRunner.year();
				let m = dateRunner.month() + 1;
				let d = dateRunner.date();

				if (typeof calendarDates[y] === 'undefined') calendarDates[y] = {};
				if (typeof calendarDates[y][m] === 'undefined') calendarDates[y][m] = {};
				if (typeof calendarDates[y][m][d] === 'undefined') calendarDates[y][m][d] = {};

				if (typeof baseCalendarData[y] === 'undefined' ||
					typeof baseCalendarData[y][m] === 'undefined' ||
					typeof baseCalendarData[y][m][d] === 'undefined' ||
					typeof baseCalendarData[y][m][d][baseCalendarId] === 'undefined'
				) {
					calendarDates[y][m][d] = {
						dayOff: false,
						holiday: false,
						notWork: false
					};
				} else {
					calendarDates[y][m][d] = angular.copy(baseCalendarData[y][m][d][baseCalendarId]);
				}

				let baseCalendarDates = calendarDates[y][m][d];
				let supportCalendar = baseCalendarSupport[baseCalendarId];

				baseCalendarDates.weeklyHours = supportCalendar.weeklyHours;
				baseCalendarDates.timesheetHours = supportCalendar.timesheetHours;
				baseCalendarDates.workHours = supportCalendar.workHours;

				dateRunner.add(1, 'days');
			}

			return calendarDates;
		}

		self.getBaseWorkHours = function (baseCalendarId) {
			let d = {
				workHours: 0,
				timesheetHours: 0,
				weeklyHours: -2
			};

			let bcData = baseCalendarSupport[baseCalendarId];
			if (bcData) {
				d.workHours = bcData.workHours;
				d.timesheetHours = bcData.timesheetHours;
				d.weeklyHours = bcData.weeklyHours;
			}

			return d;
		};

		self.getBaseCalendar = function (baseCalendarId, startDate, endDate) {
			let promises = [];

			let sd = self.getUTCDate(startDate, undefined, true);
			let ed = self.getUTCDate(endDate, undefined, true);
			let sd6m = new Date(sd);
			let ed6m = new Date(ed);

			sd6m.setUTCMonth(sd6m.getUTCMonth() - 6);
			ed6m.setUTCMonth(ed6m.getUTCMonth() + 6);

			let bcSupport = baseCalendarSupport[baseCalendarId];

			if (typeof bcSupport === 'undefined') {
				promises.push(prepareBaseCalendarFetch(baseCalendarId, sd6m, ed6m));
			} else {
				let startDateDif = Math.round((sd.getTime() - bcSupport.startDate.getTime()) / mToD);
				let endDateDif = Math.round((bcSupport.endDate.getTime() - ed.getTime()) / mToD);

				if (startDateDif < 0) {
					promises.push(prepareBaseCalendarFetch(baseCalendarId, sd6m, bcSupport.startDate));
				} else if (startDateDif < 90) {
					prepareBaseCalendarFetch(baseCalendarId, sd6m, bcSupport.startDate);
				}

				if (endDateDif < 0) {
					promises.push(prepareBaseCalendarFetch(baseCalendarId, bcSupport.endDate, ed6m));
				} else if (endDateDif < 90) {
					prepareBaseCalendarFetch(baseCalendarId, bcSupport.endDate, ed6m);
				}
			}

			return $q.all(promises).then(function () {
				return combineBaseCalendarExceptions(baseCalendarId, sd, ed);
			});
		};

		function toDoubleDigit(value) {
			return value < 10 ? '0' + value : value;
		}

		function invalidateCalendarCache(calendarId) {
			let keys = [];
			angular.forEach(userCalendarSupport, function (d, k) {
				if (d && d.calendarId && d.baseCalendarId) {
					if (d.baseCalendarId == calendarId || d.calendarId == calendarId) {
						keys.push(k);
					}
				}
			});

			let i = keys.length;
			while (i--) {
				let u = keys[i];
				delete userCalendarSupport[u];

				angular.forEach(userCalendarData, function (year) {
					angular.forEach(year, function (month) {
						angular.forEach(month, function (day) {
							if (typeof day[u] === 'undefined') return;
							delete day[u];
						});
					});
				});
			}
		}

		self.formatDateTime = function (date, withoutYear = false, conversionFromUTC = true, alsoSeconds = false, timeOnly = false, leading_zeros = true) {
			if (!date) return "";
			let d = typeof date === "string" ? date : date.toString();
			if (conversionFromUTC && $rootScope.clientInformation.hasOwnProperty('timezone') && $rootScope.clientInformation.timezone != null) {
				d = moment.utc(d).toDate();
				d = toTimezone(d, timezones[$rootScope.clientInformation.timezone.split("_")[0]]);
			} else
				d = new Date(date);

			let h = d.getHours().toString();
			let m = d.getMinutes().toString();
			let s = d.getSeconds().toString();
			if (leading_zeros) {
				if (h.length == 1) h = "0" + h;
			}
			if (m.length == 1) m = "0" + m;
			let result = self.formatDate(new Date(d.getFullYear(), d.getMonth(), d.getDate()), withoutYear, false, true, leading_zeros);
			s = alsoSeconds ? '.' + s : '';
			if (timeOnly)
				return result == "" ? "" : h + '.' + m + s;
			else
				return result == "" ? "" : result + ' ' + h + '.' + m + s;
		};

		function toTimezone(date, timezone) {
			return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: timezone}));
		}

		self.formatDate = function (date, withoutYear, conversionToUTC = true, forDatetime = false, leading_zeros = true) {
			if (typeof date === 'undefined' || date === null || !date) return '';
			if (conversionToUTC) date = self.getUTCDate(date);

			let monthShortNames = Common.getLocalShortMonthsOfYear();
			let dateFormat = $rootScope.dateFormat ? $rootScope.dateFormat : 'dd/mm/yy';

			let result;
			let d;
			let m;
			let y;
			if (forDatetime) {
				if (leading_zeros) {
					d = toDoubleDigit(date.getDate());
					m = toDoubleDigit(date.getMonth() + 1);
				} else {
					d = date.getDate();
					m = date.getMonth() + 1;
				}
				y = date.getFullYear();
			} else {
				if (leading_zeros) {
					d = toDoubleDigit(date.getUTCDate());
					m = toDoubleDigit(date.getUTCMonth() + 1);
				} else {
					d = date.getUTCDate();
					m = date.getUTCMonth() + 1;
				}
				y = date.getUTCFullYear();
			}
			let year = y;

			if (isNaN(d) || isNaN(m) || isNaN(y)) return '';

			switch (dateFormat) {
				case 'dd.mm.yyyy':
				case 'dd.mm.yy':
					result = d + '.' + m + '.';
					break;
				case 'dd-mm-yyyy':
				case 'dd-mm-yy':
					result = d + '-' + m;
					year = '-' + y;

					break;
				case 'dd/mm/yyyy':
				case 'dd/mm/yy':
					result = d + '/' + m;
					year = '/' + y;

					break;
				case 'mm.dd.yyyy':
				case 'mm.dd.yy':
					result = m + '.' + d + '.';

					break;
				case 'mm-dd-yyyy':
				case 'mm-dd-yy':
					result = m + '-' + d + '-' + y;
					year = '-' + y;

					break;
				case 'mm/dd/yyyy':
				case 'mm/dd/yy':
					result = m + '/' + d;
					year = '/' + y;

					break;
				case 'd M yyyy':
				case 'd M yy':
					if (forDatetime)
						result = date.getDate() + ' ' + monthShortNames[date.getMonth()];
					else
						result = date.getUTCDate() + ' ' + monthShortNames[date.getUTCMonth()];
					year = ' ' + y;

					break;
				case 'd.M.yyyy':
				case 'd.M.yy':
					if (forDatetime)
						result = date.getDate() + '.' + monthShortNames[date.getMonth()] + '.';
					else
						result = date.getUTCDate() + '.' + monthShortNames[date.getUTCMonth()] + '.';

					break;
				default:
					result = d + '/' + m;
					year = '/' + year;

					break;
			}

			if (Common.isFalse(withoutYear)) result += year;

			return result;
		};

		self.getCalendars = function () {
			return Calendars.get();
		};
		self.getBaseCalendars = function () {
			return Calendars.get('Base');
		};
		self.getUsersCalendar = function (userId: number) {
			return UserCalendars.get(userId);
		};
		let userBaseCalendarCache = {};
		self.getUsersBaseCalendar = function (userId: number) {
			if(!userBaseCalendarCache[userId]){
				return UserCalendars.get('Base/' + userId).then(function(calendar){
					userBaseCalendarCache[userId] = calendar;
					return calendar;
				});
			}
			let d = $q.defer();
			d.resolve(userBaseCalendarCache[userId]);
			return d.promise;
		};

		self.getCalendar = function (calendarId) {
			return Calendars.get(calendarId);
		};

		self.newCalendar = function (calendar) {
			return Calendars.new('', calendar);
		};

		self.deleteCalendar = function (calendarId) {
			return Calendars.delete(calendarId);
		};

		self.saveCalendar = function (calendar) {
			let calendarId = calendar.CalendarId;
			return Calendars.save('', calendar).then(function (c) {
				invalidateCalendarCache(calendarId);
				return c;
			});
		};

		self.getFeatureStates = function (item_id) {
			return UserCalendars.get('FeatureStates/' + item_id);
		};

		self.getEvents = function (calendarId) {
			return CalendarEvents.get(calendarId);
		};

		self.newEvent = function (event) {
			return CalendarEvents.new('', event);
		};

		self.saveEvents = function (events) {
			return CalendarEvents.save('', events).then(function (events) {
				let i = events.length;
				while (i--) {
					invalidateCalendarCache(events[i].CalendarId);
				}

				return events;
			});
		};

		self.deleteEvents = function (events) {
			events = angular.copy(events);
			return CalendarEvents.delete(events.join(',')).then(function () {
				let i = events.length;
				while (i--) {
					invalidateCalendarCache(events[i]);
				}
			});
		};

		self.getStaticHolidays = function (calendarId) {
			return CalendarStatics.get(calendarId);
		};

		self.newStaticHoliday = function (holiday) {
			return CalendarStatics.new('', holiday);
		};

		self.saveStaticHolidays = function (holidays) {
			holidays = angular.copy(holidays);
			return CalendarStatics.save('', holidays).then(function (holidays) {
				let i = holidays.length;
				while (i--) {
					invalidateCalendarCache(holidays[i].CalendarId);
				}

				return holidays;
			});
		};

		self.getWeekDay = function (date, firstIsMonday = true) {
			if (firstIsMonday) return (date.getUTCDay() + 6) % 7;
			return date.getUTCDay() % 7;
		};

		self.getWeekNumber = function (date) {
			date = self.getUTCDate(date);
			date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay() || 7));
			let yearStart = new Date(Date.UTC(date.getUTCFullYear(), 0, 1));

			return Math.ceil((((date - yearStart) / 86400000) + 1) / 7);
		}

		self.deleteStaticHoliday = function (holidayId) {
			return CalendarStatics.delete(holidayId).then(function () {
				invalidateCalendarCache(holidayId);
			});
		};

		self.applyCalendar = function (calendarId, startDate) {
			return Calendars.save(['apply', calendarId, startDate]).then(function () {
				invalidateCalendarCache(calendarId);
			});
		};

		self.applyFutureCalendar = function (calendarId) {
			return Calendars.save(['apply', 'future', calendarId]).then(function () {
				invalidateCalendarCache(calendarId);
			});
		};

		self.newUserCalendar = function (userId) {
			return Calendars.new(['user', userId]);
		};

		self.subscribeCalendarInputChange = function (scope, callback, group) {
			if (typeof inputCallbacks[group] === 'undefined') {
				inputCallbacks[group] = [];
			}

			let groupCallback = inputCallbacks[group];
			groupCallback.push(callback);

			scope.$on('$destroy', function () {
				let i = groupCallback.length;
				while (i--) {
					if (groupCallback[i] === callback) {
						groupCallback.splice(i, 1);
						break;
					}
				}
			});
		};

		self.notifyCalendarInputChange = function (data, group) {
			if (typeof inputCallbacks[group] === 'undefined') {
				return;
			}

			let i = inputCallbacks[group].length;
			while (i--) {
				let fn = inputCallbacks[group][i];
				if (typeof fn === 'function') {
					fn(data);
				}
			}

			if ('end' in data) {
				self.setGroupEndDate(group, data.end);
			}

			if ('start' in data) {
				self.setGroupStartDate(group, data.start);
			}
		};
	}

	DateFilter.$inject = ['CalendarService'];

	function DateFilter(CalendarService) {
		return function (date) {
			return CalendarService.formatDate(date);
		};
	}
})();