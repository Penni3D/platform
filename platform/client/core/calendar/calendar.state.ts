﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.config(CalendarConfig);

	CalendarConfig.$inject = ['ViewsProvider'];
	function CalendarConfig(ViewsProvider) {
		ViewsProvider.addView('calendars', {
			controller: 'CalendarsController',
			template: 'core/calendar/calendars.html',
			resolve: {
				Calendars: function (CalendarService) {
					return CalendarService.getCalendars();
				}
			},
			afterResolve: {
				ParentCalendars: function (Calendars) {
				    let r = [];
					let i = Calendars.length;
					while (i--) {
						if (Calendars[i].BaseCalendar) {
							r.push(Calendars[i]);
						}
					}

					return r;
				},
				UserCalendars: function (Calendars) {
					let r = [];
					let i = Calendars.length;
					while (i--) {
						if (Calendars[i].ItemId) {
							r.push(Calendars[i]);
						}
					}

					return r;
				},
				CurrentCalendar: function (StateParameters, Calendars) {
					if (typeof StateParameters.itemId === 'undefined') {
						return;
					}

					let i = Calendars.length;
					while (i--) {
						if (Calendars[i].ItemId == StateParameters.itemId) {
							StateParameters.calendarId = Calendars[i].CalendarId;
							break;
						}
					}
				}
			}
		});
		ViewsProvider.addPage('calendars', 'PAGE_CALENDAR', ['read','write']);
	}
})();
