﻿(function () {
	'use strict';

	angular
		.module('core.pages')
		.controller('CalendarController', CalendarController);

	CalendarController.$inject = [
		'$scope',
		'MessageService',
		'SaveService',
		'Common',
		'CalendarService',
		'StateParameters',
		'Calendar',
		'ParentCalendars',
		'ViewService',
		'UniqueService',
		'CalendarEvents',
		'ParentEvents',
		'StaticHolidays',
		'$q',
		'Translator',
		'BaseCalendars',
		'Rights'
	];

	function CalendarController(
		$scope,
		MessageService,
		SaveService,
		Common,
		CalendarService,
		StateParameters,
		Calendar, //Resolve
		ParentCalendars, //OTHER
		ViewService,
		UniqueService,
		CalendarEvents, //Resolve
		ParentEvents, //After Resolve
		StaticHolidays, //Resolve
		$q,
		Translator,
		BaseCalendars,
		Rights
	) {
		let Events = CalendarEvents.all;
		let Vacations = CalendarEvents.v;
		let WorkDays = CalendarEvents.wd;
		let NationalHolidays = CalendarEvents.nh;

		$scope.calendars = BaseCalendars;

		let monthTranslations = Common.getLocalMonthsOfYear();
		let years = [];
		let yearLooper = new Date(2000, 0, 1);

		$scope.read = Rights.read;
		$scope.write = Rights.write;
		$scope.addVacationRight = (Rights.write || Rights.vacations);
		$scope.addWorkDayRight = (Rights.write || Rights.workdays);
		
		if ($scope.addVacationRight || $scope.addWorkDayRight)
			ViewService.footbar(StateParameters.ViewId, {template: 'core/calendar/footbar.html'});

		$scope.monthDateOptions = [];
		$scope.year = new Date().getFullYear();

		for (let i = $scope.year - 3, l = $scope.year + 4; i < l; i++) {
			let y = {
				name: i,
				onClick: function () {
					$scope.year = this.name;
				}
			};

			years.push(y);
		}

		for (let i = 0, l = monthTranslations.length; i < l; i++) {
			let month = monthTranslations[i];
			let dateRunner = moment(yearLooper);
			while (true) {
				let m = dateRunner.month();
				let date = dateRunner.date();

				if (m !== i) {
					break;
				}

				$scope.monthDateOptions.push({
					parent: month,
					name: monthTranslations[i] + ' ' + date,
					value: (i + 1) + '-' + date
				});

				dateRunner.add(1, 'days');
			}

			yearLooper = dateRunner.toDate();

		}

		$scope.weekendsLooper = [
			{model: 'Def1', label: 'CAL_DAYS_LONG_1'}, {model: 'Def2', label: 'CAL_DAYS_LONG_2'},
			{model: 'Def3', label: 'CAL_DAYS_LONG_3'}, {model: 'Def4', label: 'CAL_DAYS_LONG_4'},
			{model: 'Def5', label: 'CAL_DAYS_LONG_5'}, {model: 'Def6', label: 'CAL_DAYS_LONG_6'},
			{model: 'Def7', label: 'CAL_DAYS_LONG_0'}];
		$scope.weekends = {};
		$scope.parentWeekends = {};
		$scope.selectedEvents = [];
		$scope.calendar = Calendar;

		$scope.selectedYear = $scope.year;

		$scope.changeYear = function () {
			$scope.vacations = getEventByYear(Vacations);
			$scope.workDays = getEventByYear(WorkDays);
			$scope.nationalHolidays = getEventByYear(NationalHolidays);

			addParentRows($scope.vacations, getEventByYear(ParentEvents.v));
			addParentRows($scope.workDays, getEventByYear(ParentEvents.wd));
			addParentRows($scope.nationalHolidays, getEventByYear(ParentEvents.nh));
		};

		$scope.changeYear();

		function getEventByYear(events) {
			let year_events = [];

			angular.forEach(events, function (event) {
				if (event.EventStart.substring(0, 4) == $scope.selectedYear || event.EventEnd.substring(0, 4) == $scope.selectedYear) {
					year_events.push(event);
				}
			});

			return year_events;
		}

		$scope.prevYear = function () {
			$scope.selectedYear--;
			$scope.changeYear();
		};

		$scope.nextYear = function () {
			$scope.selectedYear++;
			$scope.changeYear();
		};

		$scope.staticHolidays = StaticHolidays;

		$scope.isBaseCalendar = Calendar.BaseCalendar;
		$scope.lockWeekends = Calendar.UseBaseCalendar;
		$scope.lockWorkHours = Calendar.WorkHours == -1;
		$scope.lockTimesheetHours = Calendar.TimesheetHours == -1;
		$scope.lockWeeklyHours = Calendar.WeeklyHours;

		let weekends = ['Def1', 'Def2', 'Def3', 'Def4', 'Def5', 'Def6', 'Def7'];

		if (!$scope.isBaseCalendar) {
			let i = ParentCalendars.length;
			while (i--) {
				let c = ParentCalendars[i];

				if (c.CalendarId == Calendar.ParentCalendarId) {
					$scope.parentCalendar = c;

					break;
				}
			}
		}

		if ($scope.parentCalendar) {
			let i = weekends.length;
			while (i--) {
				let w = weekends[i];
				$scope.parentWeekends[w] = $scope.parentCalendar[w];
			}
		}

		let allEvents = {};
		let changedEvents = [];
		let changedHolidays = [];
		let calendarChanged = false;

		let i = Events.length;
		while (i--) {
			let e = Events[i];
			allEvents[e.CalendarEventId] = e;
		}

		$scope.clearSelections = function () {
			$scope.selectedEvents.length = 0;
		};

		$scope.header = {
			title: 'CALENDAR_SETTINGS',
			
			//COMMENTED OUT FOR NOW -- YEAR SELECTOR
			// icons: [
			// 	{
			// 		icon: 'calendar_today',
			// 		onClick: function (event) {
			// 			MessageService.menu(years, event.currentTarget);
			// 		}
			// 	}
			// ]
		};

		$scope.removeStaticHoliday = function (holidayId) {
			$scope.removing = true;
			CalendarService.deleteStaticHoliday(holidayId).then(function () {
				$scope.removing = false;
				let i = $scope.staticHolidays.length;
				while (i--) {
					if ($scope.staticHolidays[i].CalendarStaticHolidayId == holidayId) {
						$scope.staticHolidays.splice(i, 1);
						break;
					}
				}
			});
		};

		$scope.deleteSelections = function () {
			$scope.deleting = true;

			$scope.selEvents = [];
			angular.forEach($scope.selectedEvents, function (id) {
				if (id.toString().split('_')[0] == 's') {
					$scope.removeStaticHoliday(id.split('_')[1]);
				}
				else {
					$scope.selEvents.push(id);
				}
			});

			if ($scope.selEvents.length > 0) {
				CalendarService.deleteEvents($scope.selEvents).then(function () {
					let i = $scope.selEvents.length;
					while (i--) {
						let e = $scope.selEvents[i];
						let loopidy = [];

						switch (allEvents[e].EventType) {
							case 0:
								loopidy = $scope.vacations;
								break;
							case 1:
								loopidy = $scope.workDays;
								break;
							case 2:
								loopidy = $scope.nationalHolidays;
								break;
						}

						let j = loopidy.length;
						while (j--) {
							if (loopidy[j].CalendarEventId == e) {
								loopidy.splice(j, 1);
								break;
							}
						}
					}
					$scope.deleting = false;
					$scope.selectedEvents = [];
				});
			}
			else {
				$scope.deleting = false;
				$scope.selectedEvents = [];
			}
		};

		$scope.calendarChanged = function (params) {
			calendarChanged = true;
			if (params.reload) {
				SaveService.tick();
				ViewService.refresh(StateParameters.ViewId);
			}
		};
		
		$scope.addVacation = function () {
			let v = eventTemplate();
			v.EventType = 0;

			allEvents[v.CalendarEventId] = v;
			$scope.vacations.unshift(allEvents[v.CalendarEventId]);

		};

		$scope.addStaticHoliday = function () {
			$scope.staticHolidays.unshift({
				CalendarId: StateParameters.calendarId,
				CalendarStaticHolidayId: UniqueService.get('csh', true),
				isNew: true
			});
		};

		$scope.staticHolidayChanged = function (holidayId) {
			let i = $scope.staticHolidays.length;
			while (i--) {
				let h = $scope.staticHolidays[i];

				if (h.CalendarStaticHolidayId == holidayId) {
					if (h.Name && h.date) {
						if (changedHolidays.indexOf(holidayId) === -1) {
							changedHolidays.push(holidayId);
						}

						SaveService.tick();
					}

					break;
				}
			}
		};

		$scope.addWorkDay = function () {
			let v = eventTemplate();
			v.EventType = 1;

			allEvents[v.CalendarEventId] = v;
			$scope.workDays.unshift(allEvents[v.CalendarEventId]);
		};

		$scope.addNationalHoliday = function () {
			let v = eventTemplate();
			v.EventType = 2;

			allEvents[v.CalendarEventId] = v;
			$scope.nationalHolidays.unshift(allEvents[v.CalendarEventId]);
		};

		$scope.eventChanged = function (eventId) {
			let event = allEvents[eventId];
			if (event.EventStart && event.Name) {
				if (changedEvents.indexOf(eventId) === -1) {
					changedEvents.push(eventId);
				}
				SaveService.tick();
			}
		};

		$scope.saveWeekends = function () {
			let model = {
				applyPast: false,
				startDate: undefined,
				save: false,
				cancel: false
			};

			let modify = {
				calendar: false,
				save: true,
				cancel: true
			};

			function onChange() {
				if (model.applyPast) {
					modify.calendar = true;
				} else {
					model.startDate = undefined;
					modify.calendar = false;
				}
			}

			let ApplyFunction = function () {
				$scope.calendar.UseBaseCalendar = $scope.lockWeekends;

				if ($scope.lockWeekends) {
					angular.forEach($scope.parentWeekends, function (w, k) {
						$scope.calendar[k] = w;
					});
				} else {
					angular.forEach($scope.weekends, function (w, k) {
						$scope.calendar[k] = w;
					});
				}

				$scope.calendar.WorkHours = $scope.lockWorkHours ? -1 : $scope.userWorkHours;
				$scope.calendar.TimesheetHours = $scope.lockTimesheetHours ? -1 : $scope.userTimesheetHours;
				$scope.calendar.WeeklyHours = $scope.lockWeeklyHours < 0 ?
					$scope.lockWeeklyHours : $scope.userWeeklyHours;

				calendarChanged = false;
				model.save = false;
				model.cancel = false;

				CalendarService.saveCalendar($scope.calendar).then(function () {
					if (model.applyPast) {
						let d = model.startDate;
						let date = typeof d === 'undefined' ?
							undefined : d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
						CalendarService.applyCalendar(getCalendarId(), date);
					} else {
						CalendarService.applyFutureCalendar(getCalendarId());
					}
				});
			};

			MessageService.dialog({
				title: 'CALENDAR_SAVE_SETTINGS',
				inputs: [{
					type: 'checkbox',
					label: 'CALENDAR_APPLY_TO_PAST',
					model: 'applyPast'
				}, {
					type: 'date',
					label: 'CALENDAR_APPLY_START_DATE',
					model: 'startDate',
					modify: 'calendar'
				}],
				buttons: [
					{
						text: 'MSG_CANCEL',
						modify: 'cancel'
					},
					{
						type: 'primary',
						text: 'SAVE',
						modify: 'save',
						onClick: ApplyFunction
					}]
			}, model, onChange, modify);
		};

		SaveService.subscribeSave($scope, function () {
			let promises = [];
			if (calendarChanged) {
				promises.push(CalendarService.saveCalendar($scope.calendar));
				calendarChanged = false;
			}

			let events = [];
			angular.forEach(changedEvents, function (eventId) {
				let e = allEvents[eventId];
				if (e.isNew) {
					e.isReadOnly = true;
					e.CalendarEventId = null;
					e.CalendarId = $scope.calendar.CalendarId;

					promises.push(CalendarService
						.newEvent(e)
						.then(function (event) {
							e.CalendarEventId = event.CalendarEventId;
							e.isNew = false;
							e.isReadOnly = false;
							allEvents[e.CalendarEventId] = e;
						})
					);
				} else {
					events.push(e);
				}
			});

			if (events.length) {
				promises.push(CalendarService.saveEvents(events));
			}

			let holidays = [];
			angular.forEach($scope.staticHolidays, function (holiday) {
				let d = holiday.date.split('-');
				holiday.Month = d[0];
				holiday.Day = d[1];

				if (holiday.isNew) {
					holiday.isReadOnly = true;
					delete holiday.CalendarStaticHolidayId;

					promises.push(CalendarService
						.newStaticHoliday(holiday)
						.then(function (h) {
							holiday.CalendarStaticHolidayId = h.CalendarStaticHolidayId;
							holiday.isNew = false;
							holiday.isReadOnly = false;
						})
					);
				} else {
					holidays.push(holiday);
				}
			});

			if (holidays.length) {
				promises.push(CalendarService.saveStaticHolidays(holidays));
			}

			changedEvents = [];
			changedHolidays = [];

			return $q.all(promises);
		});

		function getCalendarId() {
			if (StateParameters.calendarId) return StateParameters.calendarId;
			return $scope.calendar.CalendarId;
		}
		
		function eventTemplate() {
			return {
				CalendarId: StateParameters.calendarId,
				CalendarEventId: UniqueService.get('ce', true),
				isNew: true,
				EventType: undefined
			};
		}

		function addParentRows(array, rows) {
			let i = rows.length;
			while (i--) {
				let r = rows[i];

				let j = array.length;
				while (j--) {
					if (array[j].EventStart >= r.EventStart) {
						array.splice(j, 0, r);
						break;
					}
				}

				if (j === -1) {
					array.push(r);
				}
			}
		}

		function initializeMenu() {
			if ($scope.lockWeekends) {
				let i = weekends.length;
				while (i--) {
					let w = weekends[i];
					$scope.weekends[w] = $scope.parentCalendar[w];
				}

				$scope.weekendsOptions = [{
					name: Translator.translate('CALENDAR_USE_USER_WEEKENDS'),
					onClick: function () {
						$scope.lockWeekends = false;
						initializeMenu();
					}
				}];
			} else {
				let i = weekends.length;
				while (i--) {
					let w = weekends[i];
					$scope.weekends[w] = Calendar[w];
				}

				$scope.weekendsOptions = [{
					name: Translator.translate('CALENDAR_USE_PARENT_WEEKENDS'),
					onClick: function () {
						$scope.lockWeekends = true;
						initializeMenu();
					}
				}];
			}


			if ($scope.lockWorkHours) {
				$scope.workHoursOptions = [{
					name: Translator.translate('CALENDAR_USE_USER_WORKHOURS'),
					onClick: function () {
						$scope.lockWorkHours = false;
						initializeMenu();
					}
				}];

				$scope.userWorkHours = $scope.parentCalendar.WorkHours;
			} else {
				$scope.workHoursOptions = [{
					name: Translator.translate('CALENDAR_USE_PARENT_WORKHOURS'),
					onClick: function () {
						$scope.lockWorkHours = true;
						initializeMenu();
					}
				}];

				if (!$scope.userWorkHours) {
					$scope.userWorkHours = $scope.calendar.WorkHours == -1 ?
						$scope.parentCalendar.WorkHours : $scope.calendar.WorkHours;
				}
			}

			if ($scope.lockTimesheetHours) {
				$scope.timesheetHoursOptions = [{
					name: Translator.translate('CALENDAR_USE_USER_TIMESHEET'),
					onClick: function () {
						$scope.lockTimesheetHours = false;
						initializeMenu();
					}
				}];

				$scope.userTimesheetHours = $scope.parentCalendar.TimesheetHours;
			} else {
				$scope.timesheetHoursOptions = [{
					name: Translator.translate('CALENDAR_USE_PARENT_TIMESHEET'),
					onClick: function () {
						$scope.lockTimesheetHours = true;
						initializeMenu();
					}
				}];

				if (!$scope.userTimesheetHours) {
					$scope.userTimesheetHours = $scope.calendar.TimesheetHours == -1 ?
						$scope.parentCalendar.TimesheetHours : $scope.calendar.TimesheetHours;
				}
			}

			if ($scope.lockWeeklyHours == -2) {
				$scope.weeklyHoursOptions = [{
					name: 'CALENDAR_USE_USER_WEEKHOURS',
					onClick: function () {
						$scope.lockWeeklyHours = $scope.userWeeklyHours;
						initializeMenu();
					}
				}];

				if (!Calendar.BaseCalendar) {
					$scope.weeklyHoursOptions.push({
						name: 'CALENDAR_USE_PARENT_WEEKHOURS',
						onClick: function () {
							$scope.lockWeeklyHours = -1;
							initializeMenu();
						}
					});
				}
			} else if ($scope.lockWeeklyHours == -1) {
				$scope.weeklyHoursOptions = [
					{
						name: 'CALENDAR_USE_AUTO_WEEKHOURS',
						onClick: function () {
							$scope.lockWeeklyHours = -2;
							initializeMenu();
						}
					},
					{
						name: 'CALENDAR_USE_USER_WEEKHOURS',
						onClick: function () {
							$scope.lockWeeklyHours = $scope.userWeeklyHours;
							initializeMenu();
						}
					}];

			} else {
				$scope.userWeeklyHours = $scope.lockWeeklyHours;

				$scope.weeklyHoursOptions = [{
					name: 'CALENDAR_USE_AUTO_WEEKHOURS',
					onClick: function () {
						$scope.lockWeeklyHours = -2;
						initializeMenu();
					}
				}];

				if (!Calendar.BaseCalendar) {
					$scope.weeklyHoursOptions.push({
						name: 'CALENDAR_USE_PARENT_WEEKHOURS',
						onClick: function () {
							$scope.lockWeeklyHours = -1;
							initializeMenu();
						}
					});
				}
			}

			$scope.recalculateWeeklyHours();
		}

		$scope.recalculateWeeklyHours = function () {
			if ($scope.lockWeeklyHours == -2) {
				let looper = $scope.lockWeekends ? $scope.parentWeekends : $scope.weekends;

				let c = 7;
				angular.forEach(looper, function (w) {
					if (w) {
						c--;
					}
				});

				$scope.userWeeklyHours = c * $scope.userTimesheetHours;
			} else if ($scope.lockWeeklyHours == -1) {
				if ($scope.parentCalendar.WeeklyHours == -2) {
					let c = 7;
					angular.forEach($scope.parentWeekends, function (w) {
						if (w) {
							c--;
						}
					});

					$scope.userWeeklyHours = c * $scope.parentCalendar.TimesheetHours;
				} else {
					$scope.userWeeklyHours = $scope.parentCalendar.WeeklyHours;
				}
			}
		};

		initializeMenu();
	}
})();
