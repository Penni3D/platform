﻿(function () {
	'use strict';

	angular
		.module('core.features')
		.config(CalendarEditorConfig);

	CalendarEditorConfig.$inject = ['ViewsProvider'];

	function CalendarEditorConfig(ViewsProvider) {
		ViewsProvider.addView('calendar.editor', {
			controller: 'CalendarController',
			template: 'core/calendar/calendar.html',
			resolve: {
				Calendar: function (CalendarService, StateParameters) {
					if (typeof StateParameters.calendarId == 'undefined') {
						return CalendarService.getUsersCalendar(StateParameters.itemId);
					}
					return CalendarService.getCalendar(StateParameters.calendarId);
				},
				ParentCalendar: function (CalendarService, StateParameters) {
					if (typeof StateParameters.calendarId == 'undefined') {
						return CalendarService.getUsersBaseCalendar(StateParameters.itemId);
					}
					return CalendarService.getCalendar(StateParameters.calendarId);
				},
				BaseCalendars: function (CalendarService) {
					return CalendarService.getBaseCalendars();
				},
				Rights: function (ViewConfigurator, CalendarService, StateParameters) {
					//Page Rights
					let result = {
						write: false,
						read: false,
						vacations: false,
						workdays: false
					};
					return ViewConfigurator.getRights('calendars').then(function (states) {
						result.write = _.includes(states, 'write');
						result.read = _.includes(states, 'read');

						//Feature Rights
						if (StateParameters.itemId) {
							return CalendarService.getFeatureStates(StateParameters.itemId).then(function (states) {
								_.each(states, function (state) {
									if (state == "calendar_user.write" && result.write == false) result.write = true;
									if (state == "calendar_user.read" && result.read == false) result.read = true;
									if (state == "calendar_user.vacations" && result.vacations == false) result.vacations = true;
									if (state == "calendar_user.workdays" && result.workdays == false) result.workdays = true;
								});

								return result;
							});
						} else {
							return result;
						}
					});
				}
			},
			afterResolve: {
				StaticHolidays: function (CalendarService, Calendar) {
					return CalendarService.getStaticHolidays(Calendar.CalendarId).then(function (holidays) {

						let i = holidays.length;
						while (i--) {
							let h = holidays[i];
							h.date = h.Month + '-' + h.Day;
						}

						return holidays;
					});
				},
				CalendarEvents: function (Calendar, CalendarService) {
					return CalendarService.getEvents(Calendar.CalendarId).then(function (events) {
						let r = {
							wd: [],
							v: [],
							nh: [],
							all: events
						};

						for (let i = 0, l = events.length; i < l; i++) {
							let e = events[i];
							switch (e.EventType) {
								case 0:
									r.v.push(e);
									break;
								case 1:
									r.wd.push(e);
									break;
								case 2:
									r.nh.push(e);
									break;
							}
						}

						return r;
					});
				},
				ParentCalendars: function (ParentCalendar) {
					return [ParentCalendar];
				},
				ParentEvents: function (Calendar, CalendarService) {
					let r = {
						wd: [],
						v: [],
						nh: [],
						all: undefined
					};

					if (Calendar.ParentCalendarId) {
						return CalendarService.getEvents(Calendar.ParentCalendarId).then(function (events) {
							r.all = events;

							for (let i = 0, l = events.length; i < l; i++) {
								let e = events[i];
								e.isParent = true;
								switch (e.EventType) {
									case 0:
										r.v.push(e);
										break;
									case 1:
										r.wd.push(e);
										break;
									case 2:
										r.nh.push(e);
										break;
								}
							}

							return r;
						});
					} else {
						return r;
					}
				}
			}
		});

		ViewsProvider.addFeature('calendar.editor', 'calendar_user', 'FEATURE_CALENDAR_USER', ['read', 'write', 'vacations', 'workdays']);
	}
})();
