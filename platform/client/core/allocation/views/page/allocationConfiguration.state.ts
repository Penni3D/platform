(function () {
	'use strict';

	angular
		.module('core.allocation')
		.config(AllocationConfigurationConfig)
		.controller('AllocationConfigurationController', AllocationConfigurationController);

	AllocationConfigurationConfig.$inject = ['ViewsProvider'];
	function AllocationConfigurationConfig(ViewsProvider) {
		ViewsProvider.addDialog('configure.allocation', {
			template: 'core/allocation/views/page/allocationConfiguration.html',
			controller: 'AllocationConfigurationController'
		});
	}

	AllocationConfigurationController.$inject = [
		'$scope',
		'CurrentSettings',
		'MessageService',
		'ReloadNavigation',
		'StateParameters',
		'AllocationService',
		'Translator'
	];

	function AllocationConfigurationController(
		$scope,
		CurrentSettings,
		MessageService,
		ReloadNavigation,
		StateParameters,
		AllocationService,
		Translator
	) {
		$scope.title = Translator.translation(CurrentSettings.Name);
		$scope.name = CurrentSettings.Name;
		$scope.isNew = StateParameters.isNew;
		$scope.view = StateParameters.view;
		$scope.save = 'SAVE';
		$scope.delete = 'DELETE';
		$scope.listPublic = 0;
		$scope.defaultRight = StateParameters.default_right;
		$scope.visibilityList = [];
		$scope.visibilityList.push(
			{
				name: Translator.translate('PORTFOLIO_CONFIGURATION_TO_ME'),
				value: 0
			});

		if($scope.defaultRight == true){
			$scope.visibilityList.push(
			{
				name: Translator.translate('PORTFOLIO_CONFIGURATION_TO_DEFAULT'),
					value: 3
			});
		}

		if(StateParameters.public == true){
			$scope.visibilityList.push({
				name: Translator.translate('PORTFOLIO_CONFIGURATION_PUBLIC'), value: 1
			})
		}


		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.saveSettings = function () {
			$scope.saving = true;
			$scope.save = 'SAVING';
			let p;
			if ($scope.isNew) {
				p = AllocationService.setSavedSettings(
						$scope.view,
						$scope.name,
						CurrentSettings.Value,
						$scope.listPublic);
			} else {
				p = AllocationService.saveSavedSettings(
						$scope.view,
						CurrentSettings.ConfigurationId,
						$scope.name,
						CurrentSettings.Value,
						$scope.listPublic);
			}

			p.then(function () {
				MessageService.closeDialog();
				ReloadNavigation();
			});
		};

		$scope.deleteSettings = function () {
			MessageService.confirm('CONFIRM_DELETE', function () {
				$scope.deleting = true;
				$scope.delete = 'DELETING';

				AllocationService
					.removeSavedSettings($scope.view, CurrentSettings.ConfigurationId, CurrentSettings.UserId)
					.then(function () {
						MessageService.closeDialog();
						ReloadNavigation();
					});
			});
		};
	}
})();
