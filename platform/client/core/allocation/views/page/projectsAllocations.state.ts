(function () {
	'use strict';

	angular
		.module('core.allocation')
		.config(ProjectsAllocationsConfig)
		.controller('ProjectsAllocationsController', ProjectsAllocationsController);

	ProjectsAllocationsConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function ProjectsAllocationsConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('projectsAllocations', {
			controller: 'ProjectsAllocationsController',
			template: 'core/allocation/views/page/projectsAllocations.html',
			resolve: {
				CostCenters: function (ProcessService) {
					return ProcessService.getItems('list_cost_center', undefined, true)
				},
				ActiveSettings: function (AllocationService, StateParameters) {
					let defaultStatuses = StateParameters.DefaultStatuses ? StateParameters.DefaultStatuses : [];
					return AllocationService.getActiveSettings('projects', defaultStatuses);
				},
				Allocations: function (ViewService, AllocationService, StateParameters) {
					let confs = AllocationService.getActiveSettings('projects');
					if (!confs.projects.length) return {
						rowdata: [],
						dates: {}
					};
					return AllocationService.getProjectsFilteredAllocationsData(confs.projects, confs.filters && confs.filters.filters ? confs.filters.filters : undefined, StateParameters.FilteringPortfolioId);
				},
				Rights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('projectsAllocations');
				},
				SavedSettings: function (AllocationService) {
					return AllocationService.getSavedSettings('projects');
				},
				PortfolioColumns: function () {
					let cols = [];

					let p = {
						ItemColumn: {
							DataType: 0,
							Name: 'primary',
							LanguageTranslation: 'ALLOCATION_NAME'
						},
						Width: 384,
						UseInSelectResult: 1
					};

					let t = {
						UseInSelectResult: 0,
						ItemColumn: {
							Name: 'cost_center',
							LanguageTranslation: 'ALLOCATION_COST_CENTER',
							DataType: 6,
							DataAdditional: 'list_cost_center',
							showType: 'inline-block'
						},
						Width: 0
					};

					let s = {
						ItemColumn: {
							DataType: 6,
							Name: 'organisation',
							LanguageTranslation: 'ALLOCATION_ORGANISATION',
							DataAdditional: 'organisation',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					let q = {
						UseInSelectResult: 0,
						ItemColumn: {
							Name: 'status_translation',
							LanguageTranslation: 'ALLOCATION_STATUS',
							DataType: 0,
							showType: 'inline-block'
						},
						Width: 0
					};

					let pt = {
						ItemColumn: {
							DataType: 0,
							Name: 'process_translation',
							LanguageTranslation: 'ALLOCATION_PROCESS',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					let at = {
						ItemColumn: {
							DataType: 0,
							Name: 'allocation_total',
							LanguageTranslation: 'ALLOCATION_TOTAL',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					cols.push(p);
					cols.push(s);
					cols.push(t);
					cols.push(q);
					cols.push(pt);
					cols.push(at);

					return cols;
				},
				FilterItemColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioId)
						return PortfolioService.getPortfolioColumns('allocation', StateParameters.FilteringPortfolioId);

					return null;
				}
			}
		});

		ViewsProvider.addPage('projectsAllocations', 'PAGE_PROJECTS_ALLOCATIONS', ['read', 'write', 'default']);
		ViewConfiguratorProvider.addConfiguration('projectsAllocations', {
			onSetup: {
				Processes: function (ProcessesService) {
					return ProcessesService.get(0);
				},
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('allocation');
				},
				ProcessPortfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				}
			},
			tabs: {
				default: {
					FilteringPortfolioId: function (resolves) {
						return {
							label: 'WT_STATUS_FILTERING_PORTFOLIO',
							type: 'select',
							options: resolves.Portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					DefaultStatuses: function () {
						return {
							label: 'DEFAULT_STATUS_FILTERS',
							type: 'multiselect',
							options: [{ value: 'draft', name: 'ALLOCATION_DRAFT_STATUS' },
							{ value: 'previousDraft', name: 'ALLOCATION_PREVIOUS_DRAFT_STATUS' },
							{ value: 'request', name: 'ALLOCATION_REQUEST_STATUS' },
							{ value: 'reject', name: 'ALLOCATION_REJECT_STATUS' },
							{ value: 'approve', name: 'ALLOCATION_APPROVE_STATUS' },
							{ value: 'release', name: 'ALLOCATION_RELEASED_STATUS' }],
							optionValue: 'value',
							optionName: 'name'
						}
					},
					SecondaryColumnTitle: function () {
						return {
							type: 'translation',
							label: 'SECONDARY_COLUMN_TITLE'
						};
					},
					DisableFte: function(){
						return {
							type: 'select',
							options: [{name: 'TRUE', value: 1}, {name: 'FALSE', value: 0}],
							optionName: 'name',
							optionValue: 'value',
							label: 'PORTFOLIO_DISABLE_FTE',
							max: 1
						};
					}
				},
				Processes: {
					SelectedProcesses: function (resolves) {
						return {
							type: 'select',
							options: resolves.Processes,
							label: 'ALLOCATION_PROJECT_CONFIG',
							optionValue: 'ProcessName',
							optionName: 'Variable',
							max: 1
						};
					},
					ExcelPortfolioId1: function (resolves) {
						return {
							label: 'ALLOCATION_PROJECT_PORTFOLIO_CONFIG',
							type: 'select',
							options: [],
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					SelectedProcesses2: function (resolves) {
						return {
							type: 'select',
							options: resolves.Processes,
							label: 'ALLOCATION_PROJECT_CONFIG_ADDITIONAL',
							optionValue: 'ProcessName',
							optionName: 'Variable',
							max: 1
						};
					},
					ExcelPortfolioId2: function (resolves) {
						return {
							label: 'ALLOCATION_PROJECT_PORTFOLIO_CONFIG',
							type: 'select',
							options: [],
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					SelectedProcesses3: function (resolves) {
						return {
							type: 'select',
							options: resolves.Processes,
							label: 'ALLOCATION_PROJECT_CONFIG_ADDITIONAL',
							optionValue: 'ProcessName',
							optionName: 'Variable',
							max: 1
						};
					},
					ExcelPortfolioId3: function (resolves) {
						return {
							label: 'ALLOCATION_PROJECT_PORTFOLIO_CONFIG',
							type: 'select',
							options: [],
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					}
				}
			},
			onChange: function (p, params, resolves, options) {
				let override1 = [];
				let override2 = [];
				let override3 = [];
				if (resolves.ProcessPortfolios) {
					_.each(resolves.ProcessPortfolios, function (d) {
						if (d.Process == params.SelectedProcesses) {
							override1.push(d);
						}
						if (d.Process == params.SelectedProcesses2) {
							override2.push(d);
						}
						if (d.Process == params.SelectedProcesses3) {
							override3.push(d);
						}
					})
				}
				options.Processes.ExcelPortfolioId1.options = override1;
				options.Processes.ExcelPortfolioId2.options = override2;
				options.Processes.ExcelPortfolioId3.options = override3;
			}
		});
	}

	ProjectsAllocationsController.$inject = [
		'$scope',
		'$rootScope',
		'PortfolioColumns',
		'StateParameters',
		'ViewService',
		'Allocations',
		'Translator',
		'AllocationService',
		'SidenavService',
		'ActiveSettings',
		'SavedSettings',
		'MessageService',
		'FilterItemColumns',
		'PortfolioService',
		'$timeout',
		'Rights',
		'AttachmentService',
		'RMService',
		'CostCenters',
		'Common',
		'CalendarService',
		'CellFactoryService'
	];

	function ProjectsAllocationsController(
		$scope,
		$rootScope,
		PortfolioColumns,
		StateParameters,
		ViewService,
		Allocations,
		Translator,
		AllocationService,
		SidenavService,
		ActiveSettings,
		SavedSettings,
		MessageService,
		FilterItemColumns,
		PortfolioService,
		$timeout,
		Rights,
		AttachmentService,
		RMService,
		CostCenters,
		Common,
		CalendarService,
		CellFactoryService
	) {
		let allRows = Allocations.rowdata;
		let userLanguage = $rootScope.clientInformation.locale_id;

		CellFactoryService.reset();

		let titleAddition = Translator.translate('ALLOCATION_SHOW_NORMAL');

		if (AllocationService.getModeName() == "project_allocation") titleAddition = Translator.translate('ALLOCATION_SHOW_DAY_MODE')
		else if(AllocationService.getModeName() == "project_allocation_fte") titleAddition = Translator.translate('ALLOCATION_SHOW_FTE')

		ViewService.footbar(StateParameters.ViewId, { template: 'core/allocation/views/allocationFootbar.html' });
		$scope.header = {
			title: StateParameters.title + " (" + titleAddition + ")",
			icons: StateParameters.DisableFte && StateParameters.DisableFte == 1 ? [] : [{
				icon: 'settings', onClick: function () {
					let mm = [{
					name: Translator.translate('ALLOCATION_SHOW_NORMAL'),
					onClick: function(){
						$scope.toggleFte('hours')
						}
					},
						{
							name: Translator.translate('ALLOCATION_SHOW_DAY_MODE'),
							onClick: function(){
								$scope.toggleFte('project_allocation')
							}
						},
						{
							name: Translator.translate('ALLOCATION_SHOW_FTE'),
							onClick: function(){
								$scope.toggleFte('project_allocation_fte')
							}
						},
					]
					MessageService.menu(mm, event.currentTarget);
				},
			},]
		};

		let tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };

		let sortedValues = [];

		let headerCellColor = { 'Red': 255, 'Blue': 255, 'Green': 102 };

		let orderRows = function ($ele) {
			if ($ele.data.owner_item_id != 0) sortedValues.push($ele)
			if ($ele.firstEle) orderRows($ele.firstEle)
			if ($ele.next) orderRows($ele.next)
		}

		let activeWeeks = [];
		let activeMonths = [];
		let addHeaders = function (dateRanges) {
			activeWeeks = [];
			activeMonths = [];

			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('OWNER_NAME'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('USER_NAME'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('ALLOCATION_TITLE'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('COST_CENTER_EXCEL'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('STATUS_EXCEL'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('TOTAL'), BackgroundColor: headerCellColor, DataType: 4 })

			_.each(Allocations.dates, function (yearValues, year) {
				_.each(yearValues, function (monthValues, month) {
					_.each(monthValues, function (dailyValue, day) {
						let today = new Date(year, month - 1, day)
						let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2'])
						let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d'])
						let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

						if (today >= minDate && today <= maxDate) {
							if (precision == 1) {
								tableValues.Rows[0]['Cells'].push({ Value: day + "/" + Number(month) + "/" + year, BackgroundColor: headerCellColor, DataType: 4 });
							} else if (precision == 2) {
								if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
									return o.Value == Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year
								})) {
									tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year, BackgroundColor: headerCellColor, DataType: 4 });
									activeWeeks.push(thisWeek + "_" + year);
								}
							} else {
								let monthDif = month - 1;
								if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
									return o.Value == Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year
								})) {
									tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year, BackgroundColor: headerCellColor, DataType: 4 });
									activeMonths.push(month + "_" + year);
								}
							}
						}
					})
				})
			})
		}

		let dateStart = "";
		let dateEnd = "";
		let precision = 0;

		$scope.isFteOn = AllocationService.getUseFte('project_allocation');

		$scope.toggleFte = function (modeIdentifier) {
			let fte = modeIdentifier == 'hours' ? false : true;
			AllocationService.setFte('project_allocation', fte);
			AllocationService.setFte('project_allocation_fte', fte);
			AllocationService.setModeName(modeIdentifier);
			$scope.isFteOn = fte
			ViewService.refresh(StateParameters.ViewId);
		}

		//$scope.setDateRange = function () {

		//	let today = new Date();
		//	let monthAgo = new Date();
		//	monthAgo.setDate(today.getDate() - 30);

		//	let eRowData = { 'date_start': monthAgo, 'date_end': today, 'precision': 0 };
		//	let precisionOptions = [{ 'Name': Translator.translate('LAYOUT_LINK_IN_DAY'), 'Value': 1 }, { 'Name': Translator.translate('LAYOUT_LINK_IN_WEEK'), 'Value': 2 }, {
		//		'Name': Translator.translate('LAYOUT_LINK_IN_MONTH'),
		//		'Value': 3
		//	}]

		//	let content = {
		//		buttons: [
		//			{
		//				text: 'CANCEL',
		//				modify: 'cancel'

		//			},
		//			{
		//				type: 'primary',
		//				modify: 'create',
		//				text: 'WORKTIME_REPORT_18_CR',
		//				onClick: function () {
		//					dateStart = eRowData.date_start;
		//					dateEnd = eRowData.date_end;
		//					precision = eRowData.precision;
		//					formExcel();
		//				}
		//			}
		//		],
		//		inputs: [{
		//			type: 'date',
		//			model: 'date_start',
		//			label: 'SELECT_RANGE_START',
		//		},
		//		{
		//			type: 'date',
		//			model: 'date_end',
		//			label: 'SELECT_RANGE_END',
		//		},

		//		{
		//			type: 'select',
		//			options: precisionOptions,
		//			optionName: 'Name',
		//			optionValue: 'Value',
		//			model: 'precision',
		//			label: 'SELECT_PRECISION',
		//		},

		//		],
		//		title: 'CREATE_ALLOCATION_EXCEL'
		//	};
		//	MessageService.dialog(content, eRowData);
		//}

		//let excelDone1 = true;
		//let excelDone2 = true;
		//let excelDone3 = true;

		//let formExcel = function () {
		//	sortedValues = [];

		//	let parent = "";
		//	_.each(RMService.itemSet, function (item) {
		//		if (item.RM.rootParent.ownerType == 'project_allocation' && item.data.process != 'user' && item.parent.isRoot) {
		//			parent = item.parent;
		//			return;
		//		}
		//	})

		//	orderRows(parent);

		//	excelDone1 = false;
		//	excelDone2 = false;
		//	excelDone3 = false;

		//	if (StateParameters.ExcelPortfolioId1 > 0) {
		//		let ids = [];
		//		_.each(sortedValues, function (row) {
		//			if (row.data.task_type == 52 && row.data.process == StateParameters.SelectedProcesses) {
		//				ids.push(row.data.itemId);
		//			}
		//		});
		//		if (ids.length > 0) {
		//			ProcessService.GetPrimariesAndSecondaries(StateParameters.SelectedProcesses, StateParameters.ExcelPortfolioId1, ids).then(function (data) {
		//				_.each(sortedValues, function (row) {
		//					if (row.data.task_type == 52 && row.data.process == StateParameters.SelectedProcesses) {
		//						row.data.primary = data[row.data.itemId].primary;
		//						row.data.secondary = data[row.data.itemId].secondary;
		//					}
		//				});
		//				excelDone1 = true;
		//				doFormExcel();
		//			});
		//		}
		//		else {
		//			excelDone1 = true;
		//		}
		//	}
		//	else {
		//		excelDone1 = true;
		//	}

		//	if (StateParameters.ExcelPortfolioId2 > 0) {
		//		let ids = [];
		//		_.each(sortedValues, function (row) {
		//			if (row.data.task_type == 52 && row.data.process == StateParameters.SelectedProcesses2) {
		//				ids.push(row.data.itemId.split('_')[1]);
		//			}
		//		});
		//		if (ids.length > 0) {
		//			ProcessService.GetPrimariesAndSecondaries(StateParameters.SelectedProcesses2, StateParameters.ExcelPortfolioId2, ids).then(function (data) {
		//				_.each(sortedValues, function (row) {
		//					if (row.data.task_type == 52 && row.data.process == StateParameters.SelectedProcesses2) {
		//						row.data.primary = data[row.data.itemId].primary;
		//						row.data.secondary = data[row.data.itemId].secondary;
		//					}
		//				});
		//				excelDone2 = true;
		//				doFormExcel();
		//			});
		//		}
		//		else {
		//			excelDone2 = true;
		//		}
		//	}
		//	else {
		//		excelDone2 = true;
		//	}

		//	if (StateParameters.ExcelPortfolioId3 > 0) {
		//		let ids = [];
		//		_.each(sortedValues, function (row) {
		//			if (row.data.task_type == 52 && row.data.process == StateParameters.SelectedProcesses3) {
		//				ids.push(row.data.itemId.split('_')[1]);
		//			}
		//		});
		//		if (ids.length > 0) {
		//			ProcessService.GetPrimariesAndSecondaries(StateParameters.SelectedProcesses3, StateParameters.ExcelPortfolioId3, ids).then(function (data) {
		//				_.each(sortedValues, function (row) {
		//					if (row.data.task_type == 52 && row.data.process == StateParameters.SelectedProcesses3) {
		//						row.data.primary = data[row.data.itemId].primary;
		//						row.data.secondary = data[row.data.itemId].secondary;
		//					}
		//				});
		//				excelDone3 = true;
		//				doFormExcel();
		//			});
		//		}
		//		else {
		//			excelDone3 = true;
		//		}
		//	}
		//	else {
		//		excelDone3 = true;
		//	}

		//	doFormExcel();
		//}
		//let doFormExcel = function () {
		//	if (excelDone1 && excelDone2 && excelDone3) {
		//		let dateRanges = {
		//			y: dateStart.getFullYear(),
		//			y2: dateEnd.getFullYear(),
		//			m: dateStart.getMonth() + 1,
		//			m2: dateEnd.getMonth() + 1,
		//			d: dateStart.getDate(),
		//			d2: dateEnd.getDate()
		//		}

		//		let index = 1;
		//		tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };
		//		tableValues.Rows.push({ 'Cells': [] });

		//		addHeaders(dateRanges);
		//		let parentProject = "";
		//		let parentUser = "";
		//		let allocation = "";
		//		let secondary = "";

		//		_.each(sortedValues, function (row) {
		//			let valuesByWeek = {};
		//			let valuesByMonth = {};
		//			tableValues.Rows.push({ 'Cells': [] });
		//			if (row.data.task_type == 52) {
		//				parentProject = row.data.primary;
		//				secondary = row.data.secondary;
		//			} else if (row.data.task_type == 51) {
		//				parentUser = row.data.primary;
		//			} else {
		//				allocation = row.data.primary;
		//				tableValues.Rows[index]['Cells'].push({ Value: parentProject })
		//				tableValues.Rows[index]['Cells'].push({ Value: secondary })
		//				tableValues.Rows[index]['Cells'].push({ Value: parentUser })
		//				tableValues.Rows[index]['Cells'].push({ Value: allocation })
		//				tableValues.Rows[index]['Cells'].push({
		//					Value: row.data.cost_center ? _.find(CostCenters, function (o) {
		//						return o.item_id == row.data.cost_center
		//					}).list_item : ""
		//				});
		//				tableValues.Rows[index]['Cells'].push({ Value: row.data.status_translation });
		//				tableValues.Rows[index]['Cells'].push({ Value: row.data.allocation_total });
		//				let rowTotal = 0;
		//				_.each(Allocations.dates, function (yearValues, year) {
		//					_.each(yearValues, function (monthValues, month) {
		//						_.each(monthValues, function (dailyValue, day) {
		//							let today = new Date(year, month - 1, day);
		//							let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2']);
		//							let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d']);
		//							let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

		//							if (today >= minDate && today <= maxDate) {
		//								if (precision == 1) {
		//									if (monthValues[day][row.data.item_id]) {
		//										rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//										tableValues.Rows[index]['Cells'].push({
		//											Value: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2),
		//											DataType: 2,
		//											NValue: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2)
		//										});
		//									} else {
		//										tableValues.Rows[index]['Cells'].push({ Value: 0, DataType: 2 });
		//									}
		//								} else if (precision == 2) {
		//									if (!valuesByWeek[thisWeek + "_" + year]) {
		//										valuesByWeek[thisWeek + "_" + year] = 0;
		//									}
		//									if (monthValues[day][row.data.item_id]) {
		//										rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//										valuesByWeek[thisWeek + "_" + year] = valuesByWeek[thisWeek + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//									}
		//								} else {
		//									if (!valuesByMonth[month + "_" + year]) {
		//										valuesByMonth[month + "_" + year] = 0;
		//									}
		//									if (monthValues[day][row.data.item_id]) {
		//										rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//										valuesByMonth[month + "_" + year] = valuesByMonth[month + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//									}
		//								}
		//							}
		//						});
		//						tableValues.Rows[index]['Cells'][6] = { Value: rowTotal, DataType: 2, NValue: Common.rounder(rowTotal, 2) };
		//					});
		//				});

		//				if (precision == 2) {
		//					_.each(activeWeeks, function (week) {
		//						tableValues.Rows[index]['Cells'].push({
		//							Value: Common.rounder(valuesByWeek[week], 2),
		//							DataType: 2,
		//							NValue: Common.rounder(valuesByWeek[week], 2)
		//						});
		//					});
		//				}
		//				if (precision == 3) {
		//					_.each(activeMonths, function (month) {
		//						tableValues.Rows[index]['Cells'].push({
		//							Value: Common.rounder(valuesByMonth[month], 2),
		//							DataType: 2,
		//							NValue: Common.rounder(valuesByMonth[month], 2)
		//						});
		//					});
		//				}
		//				index++;
		//			}
		//		});
		//		tableValues.Rows[0]['Cells'].splice(1, 0, { Value: StateParameters.SecondaryColumnTitle && StateParameters.SecondaryColumnTitle.hasOwnProperty(userLanguage) ? StateParameters.SecondaryColumnTitle[userLanguage] : "Secondary", BackgroundColor: headerCellColor, DataType: 4 });

		//		AttachmentService.getExcel($scope.header.title + " - Excel", [], tableValues);
		//	}
		//}

		let downloadExcel = function () {
			sortedValues = [];

			let parent = "";
			_.each(RMService.itemSet, function (item) {
				if (item.RM.rootParent.ownerType == 'project_allocation' && item.data.process != 'user' && item.parent.isRoot) {
					parent = item.parent;
					return;
				}
			})

			orderRows(parent);

			let ids = [];

			_.each(sortedValues, function (row) {
				if (row.data.task_type == 52 && (row.data.process == StateParameters.SelectedProcesses || row.data.process == StateParameters.SelectedProcesses2 || row.data.process == StateParameters.SelectedProcesses3)) {
					ids.push(row.data.itemId);
				}
			});

			AllocationService.setDateRange($scope.header.title, 'process', 0, null, null, AllocationService.getModeName(), ids);
			//$scope.setDateRange();
		}

		ViewService.addDownloadable('GET_ALLOCATION_EXCEL', function () {
			$scope.header.extTitle = '';
			downloadExcel();
		}, StateParameters.ViewId);

		if (StateParameters.SelectedProcesses && StateParameters.SelectedProcesses != "") {
			$scope.header.icons.push({
				icon: 'add', onClick: function () {
					let model = {
						users: [],
						ids2: [],
						ids3: []
					};

					let params = {
						title: 'ALLOCATION_ADD_TO_VIEW',
						inputs: [
							{
								type: 'process',
								process: StateParameters.SelectedProcesses,
								model: 'users',
								label: Translator.translate('ALLOCATION_ADD_TO_VIEW_1') + " ' " + Translator.translate(StateParameters.SelectedProcesses) + " ' ",
								max: 0
							}],
						buttons: [
							{
								type: 'secondary',
								text: 'MSG_CANCEL'
							},
							{
								type: 'primary',
								text: 'ADD',
								onClick: function () {
									_.each(model.users, function (itemId) {
										$rootScope.$broadcast('DragProcessOverride', {
											process: StateParameters.SelectedProcesses,
											itemId: itemId
										});
									});

									_.each(model.ids2, function (itemId2) {
										$rootScope.$broadcast('DragProcessOverride', {
											process: StateParameters.SelectedProcesses2,
											itemId: itemId2
										});
									});

									_.each(model.ids3, function (itemId3) {
										$rootScope.$broadcast('DragProcessOverride', {
											process: StateParameters.SelectedProcesses3,
											itemId: itemId3
										});
									});


								}
							}]
					};
					if (StateParameters.SelectedProcesses2 && StateParameters.SelectedProcesses2 != "") {
						params.inputs.push({
							type: 'process',
							process: StateParameters.SelectedProcesses2,
							model: 'ids2',
							label: Translator.translate('ALLOCATION_ADD_TO_VIEW_1') + " ' " + Translator.translate(StateParameters.SelectedProcesses2) + " ' ",
							max: 0
						})
					}
					if (StateParameters.SelectedProcesses3 && StateParameters.SelectedProcesses3 != "") {
						params.inputs.push({
							type: 'process',
							process: StateParameters.SelectedProcesses3,
							model: 'ids3',
							label: Translator.translate('ALLOCATION_ADD_TO_VIEW_1') + " ' " + Translator.translate(StateParameters.SelectedProcesses3) + " ' ",
							max: 0
						})
					}
					MessageService.dialog(params, model);
				}
			});
		}


		$scope.clearAllAllocations = function(){
			MessageService.confirm("CLEAR_CONFIRM_ALLOCATIONS", function () {
				AllocationService.clearViewAllocations('projects')
				$scope.rows = [];
			})
		}

		setRows();

		$scope.portfolioColumns = PortfolioColumns;
		$scope.filterColumns = FilterItemColumns;
		$scope.portfolioId = StateParameters.FilteringPortfolioId;

		if (!$scope.portfolioId) $scope.portfolioId = 0;
		$scope.hideFilters = true;

		if ($scope.portfolioId > 0) {

			$scope.header.icons.push({
				icon: 'filter_list', onClick: function () {
					$scope.hideFilters = !$scope.hideFilters;
				}
			});
		}
		$scope.AMRules = {
			parents: {
				52: ['root'],
				51: [52],
				50: [51]
			}
		};

		$scope.AMOptions = {
			calendarSteps: [15, 60, 360],
			inheritDates: false,
			ownerId: 0,
			showCalendar: true,
			sortable: true,
			selectable: false,
			hasWriteRights: !$scope.isFteOn,
			hasManagerRights: true,
			multipleProjects: true,
			allowTaskJump: true,
			allowProjectJump: true,
			ownerType: 'project_allocation'
		};

		setNavigation(SavedSettings);

		let activeTab = {};

		$scope.saveFilters = function () {
			ViewService.view('configure.allocation', {
				params: {
					isNew: true,
					view: 'projects',
					default_right: Rights.indexOf('default') !== -1
				},
				locals: {
					CurrentSettings: {
						Name: Translator.translate('ALLOCATION_SAVED_NEW'),
						Value: ActiveSettings
					},
					ReloadNavigation: reloadNavigation
				}
			});
		};

		function setNavigation(settings) {
			let sidenav = [];
			let savedTabs = [];
			angular.forEach(settings, function (s) {
				s.Name = angular.fromJson(s.Name);

				let t = {
					hideSplit: true,
					name: Translator.translation(s.Name),
					onClick: function () {
						activeTab.$$active = false;
						activeTab = t;
						activeTab.$$active = true;

						ActiveSettings.projects.length = 0;
						angular.forEach(s.Value.projects, function (value, key) {
							ActiveSettings.projects[key] = angular.copy(value);
						});

						ActiveSettings.filters.filters.length = 0;
						if (s.Value.filters) {
							angular.forEach(s.Value.filters.filters, function (value, key) {
								ActiveSettings.filters.filters[key] = angular.copy(value);
							});
						} else {
							PortfolioService.clearActiveSettings($scope.portfolioId);
						}

						$scope.reloadView();
						$scope.header.extTitle = " - " + Translator.translation(s.Name);
					},
					menu: [{
						icon: 'settings',
						name: 'CONFIGURE',
						onClick: function () {
							ViewService.view('configure.allocation', {
								params: {
									portfolioId: StateParameters.portfolioId,
									isNew: false,
									view: 'projects'
								},
								locals: {
									CurrentSettings: s,
									ReloadNavigation: reloadNavigation
								}
							});
						}
					}]
				};

				savedTabs.push(t);
			});

			if (savedTabs.length) {
				sidenav.push({ name: 'ALLOCATION_SAVED', tabs: savedTabs, showGroup: true });
			}

			SidenavService.navigation(sidenav);
		}

		function reloadNavigation() {
			AllocationService
				.getSavedSettings('projects')
				.then(function (newSettings) {
					setNavigation(newSettings);
				});
		}

		$scope.reloadView = function () {
			$scope.header.extTitle = '';
			$scope.searching = true;
			ActiveSettings.filters = PortfolioService.getActiveSettings($scope.portfolioId);
			if (ActiveSettings.projects && ActiveSettings.projects.length > 0) {
				AllocationService.getProjectsFilteredAllocationsData(ActiveSettings.projects, ActiveSettings.filters.filters, $scope.portfolioId).then(function (rows) {
					AllocationService.saveActiveSettings('projects');
					PortfolioService.saveActiveSettings($scope.portfolioId);
					allRows = rows.rowdata;
					setRows();

					$timeout(function () {
						$scope.searching = false;
					}, 50);
				});
			} else {
				$scope.searching = false;
			}
		};

		$scope.inactiveStatus = AllocationService.getActiveSettings('projects').status;
		$scope.toggleStatus = function (status) {
			AllocationService.toggleFilterStatus('projects', status);
			setRows();
		};

		function setRows() {
			$scope.rows = AllocationService.filterAllocationsByStatus('projects', AllocationService.addTotals(allRows, 0, 10, false, false, true, "project_allocation"));
		}
	}
})();