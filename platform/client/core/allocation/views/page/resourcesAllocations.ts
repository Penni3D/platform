(function () {
	'use strict';

	angular
		.module('core.allocation')
		.config(ResourcesAllocationsConfig)
		.controller('ResourcesAllocationsController', ResourcesAllocationsController);

	ResourcesAllocationsConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function ResourcesAllocationsConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('resourcesAllocations', {
			controller: 'ResourcesAllocationsController',
			template: 'core/allocation/views/page/resourcesAllocations.html',
			resolve: {
				Rights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('resourcesAllocations');
				},
				SavedSettings: function (AllocationService) {
					return AllocationService.getSavedSettings('resources');
				},
				ActiveSettings: function (AllocationService, StateParameters) {
					let defaultStatuses = StateParameters.DefaultStatuses ? StateParameters.DefaultStatuses : [];
					return AllocationService.getActiveSettings('resources', defaultStatuses);
				},
				Allocations: function (ViewService, AllocationService, $q, $rootScope, StateParameters) {
					let confs = AllocationService.getActiveSettings('resources');
					if (!confs.resources.length) confs.resources.push($rootScope.me.item_id);
					return AllocationService.getResourcesFilteredAllocationData('-1/M', confs.resources, confs.filters, StateParameters.FilteringPortfolioId);
				},
				FilterItemColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioId)
						return PortfolioService.getPortfolioColumns('allocation', StateParameters.FilteringPortfolioId);

					return null;
				},
				CostCenters: function (ProcessService) {
					return ProcessService.getItems('list_cost_center', undefined, true)
				},
				PortfolioColumns: function () {
					let cols = [];

					let p = {
						ItemColumn: {
							DataType: 0,
							Name: 'primary',
							LanguageTranslation: 'ALLOCATION_NAME'
						},
						Width: 434,
						UseInSelectResult: 1
					};

					let t = {
						UseInSelectResult: 0,
						ItemColumn: {
							Name: 'cost_center',
							LanguageTranslation: 'ALLOCATION_COST_CENTER',
							DataType: 6,
							DataAdditional: 'list_cost_center',
							showType: 'inline-block'
						},
						Width: 0
					};

					let s = {
						ItemColumn: {
							DataType: 6,
							Name: 'organisation',
							LanguageTranslation: 'ALLOCATION_ORGANISATION',
							DataAdditional: 'organisation',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					let q = {
						UseInSelectResult: 0,
						ItemColumn: {
							Name: 'status_translation',
							LanguageTranslation: 'ALLOCATION_STATUS',
							DataType: 0,
							showType: 'inline-block'
						},
						Width: 0
					};

					let pt = {
						ItemColumn: {
							DataType: 0,
							Name: 'process_translation',
							LanguageTranslation: 'ALLOCATION_PROCESS',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					let at = {
						ItemColumn: {
							DataType: 0,
							Name: 'allocation_total',
							LanguageTranslation: 'ALLOCATION_TOTAL',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					cols.push(p);
					cols.push(s);
					cols.push(t);
					cols.push(q);
					cols.push(pt);
					cols.push(at);

					return cols;
				}
			}
		});

		ViewsProvider.addPage('resourcesAllocations', 'PAGE_RESOURCES_ALLOCATIONS', ['read', 'write', 'send', 'request', 'drag', 'default']);
		ViewConfiguratorProvider.addConfiguration('resourcesAllocations', {
			global: true,
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('allocation');
				},
				UserPortfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('user');
				},
				AllPortfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				}
			},
			tabs: {
				default: {
					FilteringPortfolioId: function (resolves) {
						return {
							label: 'WT_STATUS_FILTERING_PORTFOLIO',
							type: 'select',
							options: resolves.Portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					UseAdvancedSearch: function () {
						return {
							label: 'USE_ADVANCED_SEARCH',
							type: 'select',
							options: [{ name: 'MSG_NO', value: 1 }, { name: 'MSG_YES', value: 2 }],
							optionValue: 'value',
							optionName: 'name'
						};
					},
					AdvancedPortfolioId: function (resolves) {
						return {
							label: 'ADVANCED_PORTFOLIO_ID',
							type: 'select',
							options: resolves.UserPortfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					DefaultStatuses: function () {
						return {
							label: 'DEFAULT_STATUS_FILTERS',
							type: 'multiselect',
							options: [{ value: 'draft', name: 'ALLOCATION_DRAFT_STATUS' },
							{ value: 'previousDraft', name: 'ALLOCATION_PREVIOUS_DRAFT_STATUS' },
							{ value: 'request', name: 'ALLOCATION_REQUEST_STATUS' },
							{ value: 'reject', name: 'ALLOCATION_REJECT_STATUS' },
							{ value: 'approve', name: 'ALLOCATION_APPROVE_STATUS' },
							{ value: 'release', name: 'ALLOCATION_RELEASED_STATUS' }],
							optionValue: 'value',
							optionName: 'name'
						}
					},
					SecondaryColumnTitle: function () {
						return {
							type: 'translation',
							label: 'SECONDARY_COLUMN_TITLE'
						};
					},
					ExcelPortfolioId: function (resolves) {
						return {
							label: 'ALLOCATION_USER_PORTFOLIO_CONFIG',
							type: 'select',
							options: resolves.AllPortfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					DisableFte: function(){
						return {
							type: 'select',
							options: [{name: 'TRUE', value: 1}, {name: 'FALSE', value: 0}],
							optionName: 'name',
							optionValue: 'value',
							label: 'PORTFOLIO_DISABLE_FTE',
							max: 1
						};
					}
				}
			}
		});
	}

	ResourcesAllocationsController.$inject = [
		'$scope',
		'PortfolioColumns',
		'StateParameters',
		'ViewService',
		'Allocations',
		'Translator',
		'ActiveSettings',
		'AllocationService',
		'SavedSettings',
		'Common',
		'SidenavService',
		'$rootScope',
		'$timeout',
		'Rights',
		'MessageService',
		'PortfolioService',
		'FilterItemColumns',
		'CalendarService',
		'CostCenters',
		'AttachmentService',
		'RMService',
		'CellFactoryService'
	];

	function ResourcesAllocationsController(
		$scope,
		PortfolioColumns,
		StateParameters,
		ViewService,
		Allocations,
		Translator,
		ActiveSettings,
		AllocationService,
		SavedSettings,
		Common,
		SidenavService,
		$rootScope,
		$timeout,
		Rights,
		MessageService,
		PortfolioService,
		FilterItemColumns,
		CalendarService,
		CostCenters,
		AttachmentService,
		RMService,
		CellFactoryService
	) {
		ViewService.footbar(StateParameters.ViewId, { template: 'core/allocation/views/allocationFootbar.html' });
		CellFactoryService.reset();
		let allRows = _.flatten(_.map(Allocations, function (a) {
			return a.rowdata;
		}));

		let titleAddition = Translator.translate('ALLOCATION_SHOW_NORMAL');

		if (AllocationService.getModeName() == "resource_allocation") titleAddition = Translator.translate('ALLOCATION_SHOW_DAY_MODE')
		else if(AllocationService.getModeName() == "resource_allocation_fte") titleAddition = Translator.translate('ALLOCATION_SHOW_FTE')
		$scope.header = {
			title: StateParameters.title + " (" + titleAddition + ")",
			icons: StateParameters.DisableFte && StateParameters.DisableFte == 1 ? [] : [
				{
					icon: 'settings', onClick: function () {
						let mm = [{
							name: Translator.translate('ALLOCATION_SHOW_NORMAL'),
							onClick: function(){
								$scope.toggleFte('hours')
							}
						},
							{
								name: Translator.translate('ALLOCATION_SHOW_DAY_MODE'),
								onClick: function(){
									$scope.toggleFte('resource_allocation')
								}
							},
							{
								name: Translator.translate('ALLOCATION_SHOW_FTE'),
								onClick: function(){
									$scope.toggleFte('resource_allocation_fte')
								}
							},
						]

						if(StateParameters.DisableFte && StateParameters.DisableFte == 1){
							mm.splice(0,1);
							mm.splice(0,1);
							mm.splice(0,1);
						}

						MessageService.menu(mm, event.currentTarget);
					},
				},
				{
					icon: 'add', onClick: function () {
						let model = {
							users: []
						};

						if (StateParameters.UseAdvancedSearch !== 2) {
							let params = {
								title: 'ALLOCATION_ADD_USER_TITLE',
								inputs: [
									{
										type: 'process',
										process: 'user',
										model: 'users',
										label: 'ALLOCATION_ADD_USER',
										max: 0
									}],
								buttons: [
									{
										type: 'secondary',
										text: 'MSG_CANCEL'
									},
									{
										type: 'primary',
										text: 'ADD',
										onClick: function () {
											_.each(model.users, function (itemId) {
												$rootScope.$broadcast('DragProcessOverride', {
													process: 'user',
													itemId: itemId
												});
											});
										}
									}]
							};
							MessageService.dialog(params, model);
						} else {
							ViewService.view('processFilter', {
								params: {
									portfolioId: StateParameters.AdvancedPortfolioId,
									process: 'user',
									preload: true,
									swap: false,
									itemColumnId: 0,
									max: 0,
									label: Translator.translate("ALLOCATION_ADD_USER_TITLE")
								},
								locals: {
									LinkData: {},
									ParentColumns: {},
									Model: model.users,
									OnFinish: function (items) {
										_.each(items, function (item) {
											$rootScope.$broadcast('DragProcessOverride', {
												process: 'user',
												itemId: item.actual_item_id
											});
										});
									}
								}
							}, { wider: true });
						}
					},
				},
			]
		};

		$scope.clearAllAllocations = function(){
			MessageService.confirm("CLEAR_CONFIRM_ALLOCATIONS", function () {
				AllocationService.clearViewAllocations('resources')
				$scope.rows = [];
			})
		}

		$scope.isFteOn = AllocationService.getUseFte('resource_allocation');

		$scope.AMOptions = {
			calendarSteps: [15, 60, 360],
			inheritDates: false,
			showCalendar: true,
			sortable: true,
			selectable: false,
			hasDragRights: Rights.indexOf('drag') !== -1,
			hasRequestRights: Rights.indexOf('request') !== -1,
			hasSendRights: Rights.indexOf('send') !== -1,
			hasWriteRights: Rights.indexOf('write') !== -1 && !$scope.isFteOn,
			hasOwnerRights: Rights.indexOf('read') !== -1,
			multipleResources: true,
			allowTaskJump: true,
			allowProjectJump: true,
			userResourcesMode: true,
			openRows: [],
			disableAutoOpen: true,
			ownerType: 'resource_allocation',
			fteDivider: 1
		};

		$scope.toggleFte = function (modeIdentifier) {
			let fte = modeIdentifier == 'hours' ? false : true;
			AllocationService.setFte('resource_allocation', fte);
			AllocationService.setFte('resource_allocation_fte', fte);
			AllocationService.setModeName(modeIdentifier);
			$scope.isFteOn = fte
			ViewService.refresh(StateParameters.ViewId);
		}


		$scope.AMRules = {
			parents: {
				51: ['root'], //User under root
				52: [51], //Project under user
				50: [52] //allocation under project
			},
			draggables: [
				50
			]
		};

		ViewService.addDownloadable('GET_ALLOCATION_EXCEL', function () {
			$scope.header.extTitle = '';
			downloadExcel();
		}, StateParameters.ViewId);

		let orderRows = function ($ele) {
			if ($ele.data.owner_item_id != 0) sortedValues.push($ele)
			if ($ele.firstEle) orderRows($ele.firstEle)
			if ($ele.next) orderRows($ele.next)
		}

		let sortedValues = [];

		let downloadExcel = function () {
			sortedValues = [];

			let parent = "";
			_.each(RMService.itemSet, function (item) {
				if (item.RM.rootParent.ownerType == 'resource_allocation' && item.data.process == 'user' && item.parent.isRoot) {
					parent = item.parent;
					return;
				}
			})

			orderRows(parent);

			let ids = [];

			_.each(sortedValues, function (row) {
				if (row.data.task_type == 51) {
					ids.push(row.data.itemId);
				}
			});

			AllocationService.setDateRange($scope.header.title, 'user', 0, null, null, AllocationService.getModeName(), ids);
			//$scope.setDateRange();
		}

		//let dateStart = "";
		//let dateEnd = "";
		//let precision = 0;
		//$scope.setDateRange = function () {

		//	let today = new Date();
		//	let monthAgo = new Date();
		//	monthAgo.setDate(today.getDate() - 30);

		//	let eRowData = { 'date_start': monthAgo, 'date_end': today, 'precision': 0 };
		//	let precisionOptions = [{ 'Name': Translator.translate('LAYOUT_LINK_IN_DAY'), 'Value': 1 }, { 'Name': Translator.translate('LAYOUT_LINK_IN_WEEK'), 'Value': 2 }, {
		//		'Name': Translator.translate('LAYOUT_LINK_IN_MONTH'),
		//		'Value': 3
		//	}]

		//	let content = {
		//		buttons: [
		//			{
		//				text: 'CANCEL',
		//				modify: 'cancel'

		//			},
		//			{
		//				type: 'primary',
		//				modify: 'create',
		//				text: 'WORKTIME_REPORT_18_CR',
		//				onClick: function () {
		//					dateStart = eRowData.date_start;
		//					dateEnd = eRowData.date_end;
		//					precision = eRowData.precision;
		//					formExcel();
		//				}
		//			}
		//		],
		//		inputs: [{
		//			type: 'date',
		//			model: 'date_start',
		//			label: 'SELECT_RANGE_START',
		//		},
		//		{
		//			type: 'date',
		//			model: 'date_end',
		//			label: 'SELECT_RANGE_END',
		//		},

		//		{
		//			type: 'select',
		//			options: precisionOptions,
		//			optionName: 'Name',
		//			optionValue: 'Value',
		//			model: 'precision',
		//			label: 'SELECT_PRECISION',
		//		},

		//		],
		//		title: 'CREATE_ALLOCATION_EXCEL'
		//	};
		//	MessageService.dialog(content, eRowData);
		//}

		//let activeWeeks = [];
		//let activeMonths = [];
		//let headerCellColor = { 'Red': 255, 'Blue': 255, 'Green': 102 };

		//let addHeaders = function (dateRanges) {
		//	activeWeeks = [];
		//	activeMonths = [];

		//	tableValues.Rows[0]['Cells'].push({
		//		Value: Translator.translate('OWNER_NAME'),
		//		BackgroundColor: headerCellColor,
		//		DataType: 4
		//	})
		//	tableValues.Rows[0]['Cells'].push({
		//		Value: Translator.translate('USER_NAME'),
		//		BackgroundColor: headerCellColor,
		//		DataType: 4
		//	})
		//	tableValues.Rows[0]['Cells'].push({
		//		Value: Translator.translate('ALLOCATION_TITLE'),
		//		BackgroundColor: headerCellColor,
		//		DataType: 4
		//	})
		//	tableValues.Rows[0]['Cells'].push({
		//		Value: Translator.translate('COST_CENTER_EXCEL'),
		//		BackgroundColor: headerCellColor,
		//		DataType: 4
		//	})
		//	tableValues.Rows[0]['Cells'].push({
		//		Value: Translator.translate('STATUS_EXCEL'),
		//		BackgroundColor: headerCellColor,
		//		DataType: 4
		//	})
		//	tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('TOTAL'), BackgroundColor: headerCellColor })
		//	_.each(Allocations, function (user) {
		//		_.each(user.dates, function (yearValues, year) {
		//			_.each(yearValues, function (monthValues, month) {
		//				_.each(monthValues, function (dailyValue, day) {
		//					let today = new Date(year, month - 1, day)
		//					let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2'])
		//					let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d'])
		//					let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

		//					if (today >= minDate && today <= maxDate) {
		//						if (precision == 1) {
		//							tableValues.Rows[0]['Cells'].push({
		//								Value: day + "/" + Number(month) + "/" + year,
		//								BackgroundColor: headerCellColor,
		//								DataType: 4
		//							});
		//						} else if (precision == 2) {
		//							if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
		//								return o.Value == Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year
		//							})) {
		//								tableValues.Rows[0]['Cells'].push({
		//									Value: Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year,
		//									BackgroundColor: headerCellColor,
		//									DataType: 4
		//								});
		//								activeWeeks.push(thisWeek + "_" + year);
		//							}
		//						} else {
		//							let monthDif = month - 1;
		//							if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
		//								return o.Value == Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year
		//							})) {
		//								tableValues.Rows[0]['Cells'].push({
		//									Value: Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year,
		//									BackgroundColor: headerCellColor,
		//									DataType: 4
		//								});
		//								activeMonths.push(month + "_" + year);
		//							}
		//						}
		//					}
		//				})
		//			})
		//		})
		//	})
		//}

		//let tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };

		//let formExcel = function () {
		//	sortedValues = [];

		//	let parent = "";
		//	_.each(RMService.itemSet, function (item) {
		//		if (item.RM.rootParent.ownerType == 'resource_allocation' && item.data.process == 'user' && item.parent.isRoot) {
		//			parent = item.parent;
		//			return;
		//		}
		//	})

		//	orderRows(parent);

		//	if (StateParameters.ExcelPortfolioId > 0) {
		//		let process = '';
		//		let ids = [];
		//		_.each(sortedValues, function (row) {
		//			if (row.data.task_type == 52) {
		//				process = row.data.process;
		//				ids.push(row.data.itemId.split('_')[1]);
		//			}
		//		});
		//		if (process != '') {
		//			ProcessService.GetPrimariesAndSecondaries(process, StateParameters.ExcelPortfolioId, ids).then(function (data) {
		//				_.each(sortedValues, function (row) {
		//					if (row.data.task_type == 52) {
		//						row.data.primary = data[row.data.itemId.split('_')[1]].primary;
		//						row.data.secondary = data[row.data.itemId.split('_')[1]].secondary;
		//					}
		//				});
		//				doFormExcel();
		//			});
		//		}
		//		else {
		//			doFormExcel();
		//		}
		//	}
		//	else {
		//		doFormExcel();
		//	}
		//}
		//let doFormExcel = function () {
		//	let dateRanges = {
		//		y: dateStart.getFullYear(),
		//		y2: dateEnd.getFullYear(),
		//		m: dateStart.getMonth() + 1,
		//		m2: dateEnd.getMonth() + 1,
		//		d: dateStart.getDate(),
		//		d2: dateEnd.getDate()
		//	}

		//	let index = 1;
		//	tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };
		//	tableValues.Rows.push({ 'Cells': [] });

		//	addHeaders(dateRanges);
		//	let parentProject = "";
		//	let parentUser = "";
		//	let allocation = "";
		//	let secondary = "";

		//	_.each(sortedValues, function (row) {
		//		let valuesByWeek = {};
		//		let valuesByMonth = {};
		//		tableValues.Rows.push({ 'Cells': [] });

		//		if (row.data.task_type == 52) {
		//			parentProject = row.data.primary;
		//			secondary = row.data.secondary;
		//		} else if (row.data.task_type == 51) {
		//			parentUser = row.data.primary;
		//		} else {
		//			allocation = row.data.primary;
		//			tableValues.Rows[index]['Cells'].push({ Value: parentProject });
		//			tableValues.Rows[index]['Cells'].push({ Value: secondary });
		//			tableValues.Rows[index]['Cells'].push({ Value: parentUser });
		//			tableValues.Rows[index]['Cells'].push({ Value: allocation });
		//			tableValues.Rows[index]['Cells'].push({
		//				Value: row.data.cost_center ? _.find(CostCenters, function (o) {
		//					return o.item_id == row.data.cost_center
		//				}).list_item : ""
		//			});
		//			tableValues.Rows[index]['Cells'].push({ Value: row.data.status_translation });
		//			tableValues.Rows[index]['Cells'].push({ Value: row.data.allocation_total });

		//			_.each(Allocations, function (user) {
		//				valuesByWeek = {};
		//				valuesByMonth = {};
		//				let rowTotal = 0;
		//				_.each(user.dates, function (yearValues, year) {
		//					_.each(yearValues, function (monthValues, month) {
		//						_.each(monthValues, function (dailyValue, day) {
		//							let today = new Date(year, month - 1, day);
		//							let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2']);
		//							let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d']);
		//							let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

		//							if (today >= minDate && today <= maxDate) {
		//								if (precision == 1) {
		//									if (monthValues[day][row.data.item_id]) {
		//										rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//										tableValues.Rows[index]['Cells'].push({
		//											Value: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2),
		//											DataType: 2,
		//											NValue: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2)
		//										});
		//									} else {
		//										tableValues.Rows[index]['Cells'].push({ Value: 0, DataType: 2 });
		//									}
		//								} else if (precision == 2) {

		//									if (!valuesByWeek[thisWeek + "_" + year]) {
		//										valuesByWeek[thisWeek + "_" + year] = 0;
		//									}
		//									if (monthValues[day][row.data.item_id]) {
		//										rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//										valuesByWeek[thisWeek + "_" + year] = valuesByWeek[thisWeek + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//									}
		//								} else {
		//									if (!valuesByMonth[month + "_" + year]) {
		//										valuesByMonth[month + "_" + year] = 0;
		//									}
		//									if (monthValues[day][row.data.item_id]) {
		//										rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//										valuesByMonth[month + "_" + year] = valuesByMonth[month + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
		//									}
		//								}
		//							}
		//						});
		//						tableValues.Rows[index]['Cells'][6] = { Value: Common.rounder(rowTotal, 2), DataType: 2, NValue: Common.rounder(rowTotal, 2) };
		//					});
		//				});
		//			});

		//			if (precision == 2) {
		//				_.each(activeWeeks, function (week) {
		//					tableValues.Rows[index]['Cells'].push({
		//						Value: Common.rounder(valuesByWeek[week], 2),
		//						DataType: 2,
		//						NValue: Common.rounder(valuesByWeek[week], 2)
		//					});
		//				});
		//			}
		//			if (precision == 3) {
		//				_.each(activeMonths, function (month) {
		//					tableValues.Rows[index]['Cells'].push({
		//						Value: Common.rounder(valuesByMonth[month], 2),
		//						DataType: 2,
		//						NValue: Common.rounder(valuesByMonth[month], 2)
		//					});
		//				});
		//			}
		//			index++;
		//		}
		//	});

		//	let userLanguage = $rootScope.clientInformation.locale_id;

		//	tableValues.Rows[0]['Cells'].splice(1, 0, { Value: StateParameters.SecondaryColumnTitle && StateParameters.SecondaryColumnTitle.hasOwnProperty(userLanguage) ? StateParameters.SecondaryColumnTitle[userLanguage] : "Secondary", BackgroundColor: headerCellColor, DataType: 4 });

		//	AttachmentService.getExcel($scope.header.title + " - Excel", [], tableValues);
		//}

		setRows();
		$scope.portfolioColumns = PortfolioColumns;
		$scope.filterColumns = FilterItemColumns;


		$scope.navigationMenuId = StateParameters.NavigationMenuId;
		$scope.portfolioId = StateParameters.FilteringPortfolioId;

		if (!$scope.portfolioId) $scope.portfolioId = 0;
		$scope.hideFilters = true;

		if ($scope.portfolioId > 0) {
			$scope.header.icons.push({
				icon: 'filter_list', onClick: function () {
					$scope.hideFilters = !$scope.hideFilters;
				}
			});
		}

		$scope.saveFilters = function () {
			ViewService.view('configure.allocation', {
				params: {
					isNew: true,
					view: 'resources',
					default_right: Rights.indexOf('default') !== -1
				},
				locals: {
					CurrentSettings: {
						Name: Translator.translate('ALLOCATION_SAVED_NEW'),
						Value: ActiveSettings
					},
					ReloadNavigation: reloadNavigation
				}
			});
		};

		$scope.inactiveStatus = ActiveSettings.status;

		$scope.toggleStatus = function (status) {
			AllocationService.toggleFilterStatus('resources', status);
			setRows();
		};

		setNavigation(SavedSettings);

		let activeTab = {
			$$active: undefined,
			activeTab: undefined
		};

		function setNavigation(settings) {
			let sidenav = [];
			let savedTabs = [];
			angular.forEach(settings, function (s) {

				s.Name = angular.fromJson(s.Name);
				let t = {
					hideSplit: true,
					name: Translator.translation(s.Name),
					onClick: function () {
						activeTab.$$active = false;
						activeTab = t;
						activeTab.$$active = true;

						ActiveSettings.resources.length = 0;
						angular.forEach(s.Value.resources, function (value, key) {
							ActiveSettings.resources[key] = angular.copy(value);
						});

						ActiveSettings.filters.filters.length = 0;
						if (s.Value.filters) {
							angular.forEach(s.Value.filters.filters, function (value, key) {
								ActiveSettings.filters.filters[key] = angular.copy(value);
							});
						} else {
							PortfolioService.clearActiveSettings($scope.portfolioId);
						}

						$scope.reloadView();
						$scope.header.extTitle = " - " + Translator.translation(s.Name);
					},
					menu: [{
						icon: 'settings',
						name: 'CONFIGURE',
						onClick: function () {
							ViewService.view('configure.allocation', {
								params: {
									portfolioId: StateParameters.portfolioId,
									isNew: false,
									view: 'resources'
								},
								locals: {
									CurrentSettings: s,
									ReloadNavigation: reloadNavigation
								}
							});
						}
					}]
				};

				savedTabs.push(t);
			});

			if (savedTabs.length) {
				sidenav.push({ name: 'ALLOCATION_SAVED', tabs: savedTabs, showGroup: true });
			}

			SidenavService.navigation(sidenav);
		}

		function reloadNavigation() {
			AllocationService
				.getSavedSettings('resources')
				.then(function (newSettings) {
					setNavigation(newSettings);
				});
		}

		$scope.reloadView = function () {
			$scope.header.extTitle = '';
			$scope.searching = true;

			ActiveSettings.filters = PortfolioService.getActiveSettings($scope.portfolioId);
			if (ActiveSettings.resources && ActiveSettings.resources.length > 0) {
				AllocationService.getResourcesFilteredAllocationData('-1/M', ActiveSettings.resources, ActiveSettings.filters, $scope.portfolioId).then(function (rows) {
					AllocationService.saveActiveSettings('resources');
					PortfolioService.saveActiveSettings($scope.portfolioId);
					allRows = _.flatten(_.map(rows, function (r) {
						return r.rowdata;
					}));
					setRows();

					$timeout(function () {
						$scope.searching = false;
					}, 50);
				});
			} else {
				$scope.searching = false;
			}
		};

		function setRows() {
			let sorted = _.orderBy(allRows, ['primary'], ['desc']);
			$scope.AMOptions.openRows.length = 0;
			$scope.rows = AllocationService.filterAllocationsByStatus('resources', AllocationService.addTotals(sorted, 0, 10, false, false, true, "resource_allocation"));
		}
	}
})();