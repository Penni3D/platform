﻿(function () {
	'use strict';

	angular
		.module('core.allocation')
		.config(AllocationApprovalConfig)
		.controller('AllocationApprovalController', AllocationApprovalController);

	AllocationApprovalConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function AllocationApprovalConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('resourceAllocation', {
			controller: 'AllocationApprovalController',
			template: 'core/allocation/views/page/allocationApproval.html',
			resolve: {
				Rights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('resourceAllocation');
				},
				ActiveSettings: function (AllocationService, StateParameters) {
					let defaultStatuses = StateParameters.DefaultStatuses ? StateParameters.DefaultStatuses : [];
					return AllocationService.getActiveSettings('approvals', defaultStatuses);
				},
				Allocations: function (ViewService, AllocationService, StateParameters) {
					let dateFilter = typeof StateParameters.endsAfter === 'undefined' ?
						'-1/M' : StateParameters.endsAfter;
					return AllocationService.getResourceAllocationData(dateFilter);
				},
				SavedSettings: function (AllocationService) {
					return AllocationService.getSavedSettings('approvals');
				},
				CostCenters: function (UserService, AllocationService, ViewService) {
					return AllocationService.getUserCostCenters(UserService.getCurrentUserId()).then(function (costCenters) {
						return costCenters.length ? costCenters : ViewService.fail("ALLOCATION_CC_NOT_SET");
					});
				},
				PortfolioColumns: function () {
					let cols = [];

					let p = {
						ItemColumn: {
							DataType: 0,
							Name: 'primary',
							LanguageTranslation: 'ALLOCATION_NAME'
						},
						Width: 434,
						UseInSelectResult: 1
					};

					let t = {
						UseInSelectResult: 0,
						ItemColumn: {
							Name: 'competency',
							LanguageTranslation: 'ALLOCATION_COMPETENCY',
							DataType: 5,
							DataAdditional: 'competency',
							showType: 'inline-block'
						},
						Width: 0
					};

					let s = {
						ItemColumn: {
							DataType: 6,
							Name: 'organisation',
							LanguageTranslation: 'ALLOCATION_ORGANISATION',
							DataAdditional: 'organisation',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					let q = {
						UseInSelectResult: 0,
						ItemColumn: {
							Name: 'status_translation',
							LanguageTranslation: 'ALLOCATION_STATUS',
							DataType: 0,
							showType: 'inline-block'
						},
						Width: 0
					};

					let pt = {
						ItemColumn: {
							DataType: 0,
							Name: 'process_translation',
							LanguageTranslation: 'ALLOCATION_PROCESS',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					let at = {
						ItemColumn: {
							DataType: 0,
							Name: 'allocation_total',
							LanguageTranslation: 'ALLOCATION_TOTAL',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					cols.push(p);
					cols.push(s);
					cols.push(t);
					cols.push(q);
					cols.push(pt);
					cols.push(at);

					return cols;
				},
				UserFilterColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioId)
						return PortfolioService.getPortfolioColumns('allocation', StateParameters.FilteringPortfolioId);

					return null;
				},

				CompetencyFilterColumns: function (PortfolioService, StateParameters) {
					if (StateParameters.FilteringPortfolioCompetencyPortfolioId)
						return PortfolioService.getPortfolioColumns('competency', StateParameters.FilteringPortfolioCompetencyPortfolioId);

					return null;
				},
			}
		});

		ViewsProvider.addPage('resourceAllocation', 'PAGE_RESOURCE_ALLOCATION', ['read', 'write', 'send', 'request', 'drag', 'reset']);
		ViewConfiguratorProvider.addConfiguration('resourceAllocation', {
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('user');
				},

				Portfolios2: function (PortfolioService) {
					return PortfolioService.getPortfolios('competency');
				}
			},
			tabs: {
				default: {
					FilteringPortfolioId: function (resolves) {
						return {
							label: 'APPROVAL_FILTERING_PORTFOLIO',
							type: 'select',
							options: resolves.Portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					FilteringPortfolioCompetencyPortfolioId: function (resolves) {
						return {
							label: 'APPROVAL_FILTERING_PORTFOLIO_2',
							type: 'select',
							options: resolves.Portfolios2,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					DefaultStatuses: function () {
						return {
							label: 'DEFAULT_STATUS_FILTERS',
							type: 'multiselect',
							options: [{value: 'draft', name: 'ALLOCATION_DRAFT_STATUS'},
								{value: 'previousDraft', name: 'ALLOCATION_PREVIOUS_DRAFT_STATUS'},
								{value: 'request', name: 'ALLOCATION_REQUEST_STATUS'},
								{value: 'reject', name: 'ALLOCATION_REJECT_STATUS'},
								{value: 'approve', name: 'ALLOCATION_APPROVE_STATUS'},
								{value: 'release', name: 'ALLOCATION_RELEASED_STATUS'}],
							optionValue: 'value',
							optionName: 'name'
						}
					},
					FTEModes: function () {
						return {
							label: "FTE_MODES_ENABLED",
							type: "select",
							options: [{value: 0, name: 'YES'}, {value: 1, name: 'NO'}],
							optionValue: 'value',
							optionName: 'name'
						}
					}
				},
			}
		});
	}

	AllocationApprovalController.$inject = [
		'$scope',
		'PortfolioColumns',
		'StateParameters',
		'ViewService',
		'Allocations',
		'CostCenters',
		'SidenavService',
		'AllocationService',
		'$timeout',
		'Translator',
		'Rights',
		'MessageService',
		'CalendarService',
		'UserFilterColumns',
		'ActiveSettings',
		'SavedSettings',
		'PortfolioService',
		'CompetencyFilterColumns',
		'Configuration',
		'$rootScope'
	];

	function AllocationApprovalController(
		$scope,
		PortfolioColumns,
		StateParameters,
		ViewService,
		Allocations,
		CostCenters,
		SidenavService,
		AllocationService,
		$timeout,
		Translator,
		Rights,
		MessageService,
		CalendarService,
		UserFilterColumns,
		ActiveSettings,
		SavedSettings,
		PortfolioService,
		CompetencyFilterColumns,
		Configuration,
		$rootScope
	) {
		let allRows = [];
		let sidenav = [];
		let activeTab = {};
		let currentCostCenterId = 0;
		let group = {
			name: 'ALLOCATION_COST_CENTERS',
			tabs: []
		};

		$scope.userFilterColumns = UserFilterColumns;
		$scope.competencyFilterColumns = CompetencyFilterColumns;

		function setNavigation(settings) {
			let sidenav = [];
			let savedTabs = [];
			let costcenterTabs = [];

			angular.forEach(CostCenters, function (costCenter) {
				let tab = {
					name: costCenter.list_item,
					onClick: function () {
						activeTab.$$active = false;
						activeTab = tab;
						activeTab.$$active = true;
						currentCostCenterId = costCenter.item_id;
						updateCostCenter(costCenter.item_id);
						setHeader(costCenter.list_item);
					}
				};

				if (actionsRequired[costCenter.item_id]) {
					tab.icon = 'error'
				}

				costcenterTabs.push(tab)
			});

			angular.forEach(settings, function (s) {
				s.Name = angular.fromJson(s.Name);
				let t = {
					hideSplit: true,
					name: Translator.translation(s.Name),
					onClick: function () {
						activeTab.$$active = false;
						activeTab = t;
						activeTab.$$active = true;

						/*ActiveSettings.projects.length = 0;
						angular.forEach(s.Value.projects, function (value, key) {
							ActiveSettings.projects[key] = angular.copy(value);
						});*/

						//SETS SAVED FILTERS TO THE FILTER AREA
						if (s.Value[0].filters) {
							angular.forEach(s.Value[0].filters.filters, function (value, key) {
								if (!ActiveSettings.filters) ActiveSettings.filters = {'filters': {}}
								ActiveSettings.filters.filters[key] = angular.copy(value);
							});
						} else {
							PortfolioService.clearActiveSettings($scope.portfolioId);
						}


						$scope.reloadView(s.Value);
						$scope.header.extTitle = " - " + Translator.translation(s.Name);
					},
					menu: [{
						icon: 'settings',
						name: 'CONFIGURE',
						onClick: function () {
							ViewService.view('configure.allocation', {
								params: {
									portfolioId: StateParameters.portfolioId,
									isNew: false,
									view: 'approvals'
								},
								locals: {
									CurrentSettings: s,
									ReloadNavigation: reloadNavigation
								}
							});
						}
					}]
				};

				savedTabs.push(t);
			});

			if (savedTabs.length)
				sidenav.push({name: 'ALLOCATION_SAVED', tabs: savedTabs, showGroup: true});


			if (costcenterTabs.length)
				sidenav.push({name: 'COST_CENTERS', tabs: costcenterTabs, showGroup: true, expanded: true});


			SidenavService.navigation(sidenav);
		}


		function reloadNavigation() {
			AllocationService
				.getSavedSettings('approvals')
				.then(function (newSettings) {
					setNavigation(newSettings);
				});
		}

		reloadNavigation();

		$scope.hideFilters = true;
		$scope.hideFilters2 = true;

		$scope.loading = true;
		$rootScope.$on('RMDragProcessStop', function (event, id) {
			//Resets this view -- doesn't update row parent situation tho..
			// $scope.loading = true;
			// $scope.limitAllocations();
		});

		$scope.reloadView = function (filtersFromMenu) {

			$scope.header.extTitle = '';
			$scope.searching = true;

			let filterToUse = typeof filtersFromMenu != 'undefined' ? filtersFromMenu[0].filters : PortfolioService.getActiveSettings($scope.portfolioId);
			let competencyFilters = typeof filtersFromMenu != 'undefined' ? filtersFromMenu[1].filters : PortfolioService.getActiveSettings($scope.portfolioId2);

			let dateFilter = typeof StateParameters.endsAfter === 'undefined' ?
				'-1/M' : StateParameters.endsAfter;

			let activeUserFilters = [];
			let activeCompetencyFilters = [];

			let reference = competencyFilters.hasOwnProperty('filters') ? competencyFilters.filters : competencyFilters

			_.each(filterToUse.filters.select, function (fs) {
				_.each(fs, function (f) {
					activeUserFilters.push(f)
				})
			})

			_.each(reference.select, function (fs) {
				_.each(fs, function (f) {
					activeCompetencyFilters.push(f)
				})
			})

			ActiveSettings.filters = PortfolioService.getActiveSettings($scope.portfolioId);
			if (filterToUse.filters.text.length == 0 && !activeUserFilters.length && reference.text.length == 0 && !activeCompetencyFilters.length) {
				AllocationService.getResourceAllocationData(dateFilter).then(function (rows) {
					Allocations = rows
					updateCostCenter(currentCostCenterId)
				});
			} else {
				let allocationFilters = {
					excludeItemIds: [],
					filters: filterToUse.filters.select,
					limit: filterToUse.filters.limit,
					offset: filterToUse.filters.offset,
					recursive: filterToUse.recursive,
					sort: filterToUse.filters.sort,
					text: filterToUse.filters.text
				}

				let compFilters = {
					excludeItemIds: [],
					filters: reference.select,
					limit: reference.limit,
					offset: reference.offset,
					recursive: false,
					sort: reference.sort,
					text: reference.text
				}

				AllocationService.getResourceAllocationDataWithFilters(
					[
						(activeUserFilters.length == 0 && filterToUse.filters.text == "" ? null : allocationFilters),
						(activeCompetencyFilters.length == 0 && reference.text == "" ? null : compFilters)
					], $scope.portfolioId, $scope.portfolioId2, dateFilter).then(function (rows_) {
					Allocations = rows_;

					let allocationPortfolioSettings = Configuration.getActivePreferences('portfolios')[$scope.portfolioId];
					allocationPortfolioSettings.filters.text = allocationFilters.text;
					allocationPortfolioSettings.filters.select = allocationFilters.filters;

					let competencyPortfolioSettings = Configuration.getActivePreferences('portfolios')[$scope.portfolioId2];
					competencyPortfolioSettings.filters.text = compFilters.text;
					competencyPortfolioSettings.filters.select = compFilters.filters;

					updateCostCenter(currentCostCenterId)
					$timeout(function () {
						$scope.searching = false;
					}, 50);
				});
			}
		};

		$scope.saveFilters = function () {
			ViewService.view('configure.allocation', {
				params: {
					isNew: true,
					view: 'approvals',
					default_right: Rights.indexOf('default') !== -1
				},
				locals: {
					CurrentSettings: {
						Name: Translator.translate('ALLOCATION_SAVED_NEW'),
						Value: [ActiveSettings, PortfolioService.getActiveSettings($scope.portfolioId2)]
					},
					ReloadNavigation: reloadNavigation
				}
			});
		};

		$scope.portfolioId = StateParameters.FilteringPortfolioId;
		$scope.portfolioId2 = StateParameters.FilteringPortfolioCompetencyPortfolioId;

		PortfolioService.clearActiveSettings($scope.portfolioId)
		PortfolioService.clearActiveSettings($scope.portfolioId2)

		let actionsRequired = {};
		let i = Allocations.rowdata.length;
		while (i--) {
			let row = Allocations.rowdata[i];
			if (row.task_type == 50 && AllocationService.allocationRequiresAction(row.status, true, false)) {
				actionsRequired[row.cost_center] = true;
			}
		}

		// function setCostCenterNavi() {
		// 	//group.tabs = [];
		//	
		// }
		// setCostCenterNavi();


		sidenav.push(group);
		SidenavService.navigation(sidenav);
		$scope.ownerCostCenters = CostCenters;

		ViewService.footbar(StateParameters.ViewId, {template: 'core/allocation/views/allocationFootbar.html'});

		$scope.hideClearButton = true;

		$scope.inactiveStatus = AllocationService.getActiveSettings('resource').status;

		$scope.AllocationTotals = AllocationService.getCachedAllocationTotals();

		$scope.toggleStatus = function (status) {
			AllocationService.toggleFilterStatus('resource', status);
			setRows();
		};
		let titleAddition = Translator.translate('ALLOCATION_SHOW_NORMAL');

		if (AllocationService.getModeName() == "approval") titleAddition = Translator.translate('ALLOCATION_SHOW_DAY_MODE')
		else if (AllocationService.getModeName() == "approval_fte") titleAddition = Translator.translate('ALLOCATION_SHOW_FTE')
		$scope.header = {
			title: StateParameters.title + " (" + titleAddition + ")",
			icons: [
				{
					icon: 'settings', onClick: function () {
						let mm = [];
						if (StateParameters.FTEModes != 1) {
							mm.push({
								name: Translator.translate('ALLOCATION_SHOW_NORMAL'),
								onClick: function () {
									$scope.toggleFte('hours')
								}
							});

							mm.push({
								name: Translator.translate('ALLOCATION_SHOW_DAY_MODE'),
								onClick: function () {
									$scope.toggleFte('approval')
								}
							});

							mm.push({
								name: Translator.translate('ALLOCATION_SHOW_FTE'),
								onClick: function () {
									$scope.toggleFte('approval_fte')
								}
							});
						}

						mm.push({
							name: 'ALLOCATION_SET_DATE_RANGE', onClick: function () {
								let model = angular.copy($scope.calendarDates);
								let endsAfter = 'endsAfter';
								let params = {
									title: 'ALLOCATION_LIMIT_TITLE',
									inputs: [
										{
											type: 'daterange',
											label: 'ALLOCATION_LIMIT_DATE',
											modelStart: 's',
											modelEnd: 'e'
										},
										{
											type: 'select',
											label: 'ALLOCATION_LIMIT_ENDS',
											options: [{name: 'QUARTAL', value: '-1/Q'}, {
												name: 'MONTH',
												value: '-1/M'
											}, {
												name: 'WEEK',
												value: '-2/W'
											}],
											model: endsAfter,
											optionName: 'name',
											optionValue: 'value',
											singleValue: true
										}
									],
									buttons: [
										{
											type: 'secondary',
											text: 'MSG_CANCEL'
										},
										{
											type: 'primary',
											text: 'CLEAR',
											onClick: function () {
												$scope.calendarDates = {
													s: undefined,
													e: undefined
												};
												StateParameters.endsAfter = model.endsAfter;
												$scope.limitAllocations();
											}
										},
										{
											type: 'primary',
											text: 'FILTER',
											onClick: function () {
												StateParameters.endsAfter = model.endsAfter;
												$scope.calendarDates = model;
												$scope.limitAllocations();
											}
										}]
								};

								MessageService.dialog(params, model);
							}
						});

						MessageService.menu(mm, event.currentTarget);
					},
				}
			]
		};

		$scope.calendarDates = {
			s: undefined,
			e: undefined,
			endsAfter: typeof StateParameters.endsAfter === 'undefined' ? '-1/M' : StateParameters.endsAfter
		};

		$scope.portfolioColumns = PortfolioColumns;

		$scope.AMRules = {
			parents: {
				51: ['root'], //User under root
				52: [51], //Project under user
				50: [51] //allocation under project
			},
			draggables: [
				50
			],
			dragRestrictions: [
				{'resource_type': 'competency'}
			]
		};

		$scope.isFteOn = AllocationService.getUseFte('approval');

		$scope.toggleFte = function (modeIdentifier) {
			let fte = modeIdentifier == 'hours' ? false : true;
			AllocationService.setFte('approval', fte);
			AllocationService.setFte('approval_fte', fte);
			AllocationService.setModeName(modeIdentifier);
			$scope.isFteOn = fte
			ViewService.refresh(StateParameters.ViewId);
		}

		$scope.AMOptions = {
			calendarSteps: [15, 60, 360],
			inheritDates: false,
			showCalendar: true,
			sortable: true,
			selectable: Rights.indexOf('send') !== -1,
			allowTaskJump: true,
			allowProjectJump: true,
			openRows: [],
			highlightRows: [],
			hasDragRights: Rights.indexOf('drag') !== -1,
			hasRequestRights: Rights.indexOf('request') !== -1,
			hasSendRights: Rights.indexOf('send') !== -1,
			hasWriteRights: Rights.indexOf('write') !== -1 && !$scope.isFteOn,
			hasOwnerRights: Rights.indexOf('read') !== -1,
			hasResetRights: Rights.indexOf('reset') !== -1,
			ownerType: 'approval'
		};

		let highlightInfo = {};

		if (StateParameters.highlightRow) {
			let foundHighlightRow = false;
			let i = Allocations.rowdata.length;
			while (i--) {
				let a = Allocations.rowdata[i];
				if (a.item_id == StateParameters.highlightRow) {
					let j = Allocations.rowdata.length;
					while (j--) {
						let u = Allocations.rowdata[j];

						if (u.item_id == a.resource_item_id) {
							highlightInfo.self = a.item_id;
							highlightInfo.parent = u.item_id;
							$scope.AMOptions.highlightRows.push(a.item_id);
							let status = AllocationService.getStatusGroup(a.status);

							if (status) {
								$scope.inactiveStatus[status] = false;
							}

							let costCenter = _.find(CostCenters, ['item_id', u.cost_center[0]]);

							if (costCenter) {
								updateCostCenter(costCenter.item_id);
								setHeader(costCenter.list_item);

								foundHighlightRow = true;
							}

							break;
						}
					}

					break;
				}
			}

			if (foundHighlightRow === false) {
				updateCostCenter(CostCenters[0].item_id);
				setHeader(CostCenters[0].list_item);
			}
		} else {
			updateCostCenter(CostCenters[0].item_id);
			setHeader(CostCenters[0].list_item);
		}

		function setHeader(costCenter) {
			if ($scope.calendarDates.s) {
				$scope.header.title = costCenter + ': ' + Translator.translate('ALLOCATION_RESOURCE_TITLE') + ' - ' + Translator.translate('ALLOCATION_LIMIT_FILTER') + ': ' + CalendarService.formatDate($scope.calendarDates.s) + ' - ' + CalendarService.formatDate($scope.calendarDates.e);
			} else {
				$scope.header.title = costCenter + ': ' + Translator.translate('ALLOCATION_RESOURCE_TITLE');
			}
		}

		function updateCostCenter(costCenterId) {
			currentCostCenterId = costCenterId;
			let parent = [];
			let children = {};
			for (let i = 0, l = Allocations.rowdata.length; i < l; i++) {
				let allocation = Allocations.rowdata[i];
				if ((allocation.task_type == 51 || allocation.task_type == 52) && allocation.cost_center) {
					if (typeof allocation.cost_center === "number") {
						if (allocation.cost_center == costCenterId)
							parent.push(allocation);
					} else {
						for (let j = 0, l_ = allocation.cost_center.length; j < l_; j++) {
							if (allocation.cost_center[j] === costCenterId) {
								parent.push(allocation);
								break;
							}
						}
					}
				} else if (allocation.task_type == 50 && allocation.cost_center == costCenterId) {
					if (typeof children[allocation.parent_item_id] === 'undefined') {
						children[allocation.parent_item_id] = [allocation];
					} else {
						children[allocation.parent_item_id].push(allocation);
					}
				}
			}
			let i = parent.length;
			while (i--) {
				if (typeof children[parent[i].item_id] === 'undefined') {
					continue;
				}
				parent.splice.apply(parent, [i + 1, 0].concat(children[parent[i].item_id]));
			}
			allRows = parent;

			setRows();
		}

		let executeQueue = [];
		let execute = function (i) {
			let item = executeQueue[i];
			if (item) {
				$scope.curentActionExecution += 1;
				let newStatus = AllocationService.nextStatus(item.data.status, 'approve', 0);
				return AllocationService.setAllocationStatus(item, newStatus).then(function () {
					execute(i + 1);
				});
			} else {
				$scope.calendarReDraw();
				$scope.clickSelStop();
			}
		};

		$scope.hasAtLeastOneCC = true; //checked in footbar file
		$scope.approveMultiple = function (action) {
			MessageService.confirm('ALLOCATION_APPROVE_MULTIPLE', function () {
				$scope.actionsExecuting = true;
				executeQueue = [];
				$scope.curentActionExecution = 0;

				_.each($scope.getRMSelectedRows(), function (row) {
					if (row.data.status == 20) executeQueue.push(row);
				});
				$scope.totalActionExecution = executeQueue.length;
				execute(0);
			});
		};

		$scope.limitAllocations = function () {
			let dateFilter = typeof StateParameters.endsAfter === 'undefined' ?
				'-1/M' : StateParameters.endsAfter;

			if ($scope.calendarDates.s && $scope.calendarDates.e) {
				let s = moment($scope.calendarDates.s).format('YYYY-MM-DD');
				let e = moment($scope.calendarDates.e).format('YYYY-MM-DD');
				setHeader(_.find(CostCenters, function (o) {
					return o.item_id == currentCostCenterId;
				}).list_item);

				AllocationService.getResourceAllocationData(dateFilter, undefined, s, e).then(function (allocations) {
					Allocations = allocations;
					updateCostCenter(currentCostCenterId);
				});
			} else {
				$scope.header.title = StateParameters.title;
				setHeader(_.find(CostCenters, function (o) {
					return o.item_id == currentCostCenterId;
				}).list_item);
				AllocationService.getResourceAllocationData(dateFilter).then(function (allocations) {
					Allocations = allocations;
					updateCostCenter(currentCostCenterId);
				});
			}
		};

		function setRows() {
			$scope.rows = AllocationService.filterAllocationsByStatus('resource', AllocationService.addTotals(allRows, 0, 10, false, false, true, "approval"));
			$scope.AMOptions.openRows.length = 0;
			if (highlightInfo.self && _.find($scope.rows, function (r) {
				return r.item_id == highlightInfo.self
			})) {
				$scope.AMOptions.openRows.push(highlightInfo.parent);
			}
			$scope.loading = false;
		}

		if ($scope.portfolioId > 0) {
			$scope.header.icons.push({
				icon: 'filter_list', onClick: function () {
					$scope.hideFilters = !$scope.hideFilters;
					$scope.hideFilters2 = !$scope.hideFilters2;
				}
			});
		}
	}
})();