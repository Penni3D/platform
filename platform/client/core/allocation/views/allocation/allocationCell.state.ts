﻿(function () {
	'use strict';

	angular
		.module('core.allocation')
		.config(AllocationCellConfig)
		.controller('AllocationCellController', AllocationCellController);

	AllocationCellConfig.$inject = ['ViewsProvider'];

	function AllocationCellConfig(ViewsProvider) {
		ViewsProvider.addView('allocation.cell', {
			controller: 'AllocationCellController',
			template: 'core/allocation/views/allocation/allocationCell.html',
			resolve: {
				CostCenters: function (ViewService, RMService, ProcessService, StateParameters) {
					let parent = RMService.itemSet[StateParameters.parentItemId];

					let f = function (costcenter) {
						if (parent.data.process == 'user' || typeof costcenter == "number") {
							return ProcessService
								.getItem('list_cost_center', costcenter, true)
								.then(function (result) {
									return [result];
								});
						} else {
							return ProcessService.getItems('list_cost_center', undefined, true).then(function (result) {
								let r = [];
								for (let i = 0, l = costcenter.length; i < l; i++) {
									for (let j = 0, l_ = result.length; j < l_; j++) {
										if (costcenter[i] == result[j].item_id) r.push(result[j]);
									}
								}

								return r;
							});
						}
					};

					if (!parent.data.cost_center || parent.data.cost_center.length == 0) {
						return ProcessService.getItem(parent.data.process, parent.data.itemId, true).then(function (res) {
							if (res.cost_center && res.cost_center.length != 0) return f(res.cost_center);
							return ViewService.fail('ALLOCATION_COST_CENTER_MISSING');
						});
					}

					return f(parent.data.cost_center);
				},
				AllocationInfo: function (StateParameters, AllocationService) {
					return AllocationService.getAllocationDates(StateParameters.itemId);
				},
				ParentInfo: function (DI, ProcessService) {
					let parentItemId = DI.data.status == 55 ? ProcessService.getActualItemId(DI.data.parent_item_id) : DI.data.resource_item_id;
					return ProcessService.getProcess(parentItemId).then(function (process) {
						return {process: process, itemId: parentItemId};
					});
				},
				InstanceDefaultHours: function(CalendarService){
					let defaultHours = 1;
					return CalendarService.getBaseCalendars().then(function (b) {
						_.each(b, function (calendar) {
							if (calendar.IsDefault == true) {
								defaultHours = calendar.WorkHours;
								return
							}
						})
						return defaultHours;
					})
				}
			},
			options: {
				remember: false,
				size: 'narrower'
			}
		});

		ViewsProvider.addView('allocation', {
			resolve: {}
		});
	}

	AllocationCellController.$inject = [
		'$scope',
		'DI',
		'RM',
		'AllocationService',
		'MessageService',
		'ViewService',
		'StateParameters',
		'ProcessService',
		'Rights',
		'CostCenters',
		'RMService',
		'SaveService',
		'Common',
		'CalendarService',
		'ResetSelection',
		'AllocationInfo',
		'Translator',
		'ParentInfo',
		'InstanceDefaultHours'
	];

	function AllocationCellController(
		$scope,
		DI,
		RM,
		AllocationService,
		MessageService,
		ViewService,
		StateParameters,
		ProcessService,
		Rights,
		CostCenters,
		RMService,
		SaveService,
		Common,
		CalendarService,
		ResetSelection,
		AllocationInfo,
		Translator,
		ParentInfo,
		InstanceDefaultHours
	) {

		let costCenterChanged = false;
		let changedDates = [];
		let updatingStatus = false;

		DI.$ele.addClass('selected');

		// $scope.restrictStart = undefined;
		// $scope.restrictEnd = undefined;
		//
		// //Restrict to between s and e dates
		// if (StateParameters.projectStartDate && StateParameters.projectEndDate) {
		// 	ProcessService.getProcess(StateParameters.projectId).then(function (process) {
		// 		ProcessService.getItem(process, StateParameters.projectId, true).then(function (item) {
		// 			$scope.restrictStart = item[StateParameters.projectStartDate];
		// 			$scope.restrictEnd = item[StateParameters.projectEndDate];
		// 			$scope.isValid(); 
		// 		})
		// 	});
		// }

		$scope.allocation = {
			process: DI.data.process,
			itemId: DI.data.item_id,
			title: DI.data.primary,
			parent: [ParentInfo.itemId],
			parentProcess: ParentInfo.process,
			dates: DI.calendarRow.currentDateSelection,
			costCenter: DI.data.cost_center == 0 ? undefined : DI.data.cost_center,
			costCenterOptions: CostCenters
		};


		let giveFteMultiplier = function(){
			if(ParentInfo.process && ParentInfo.process == 'user'){
				return CalendarService.getUserWorkHours(DI.data.resource_item_id).workHours;
			} else {
				return InstanceDefaultHours;
			}
		}


		if (!$scope.allocation.dates.startDate && !$scope.allocation.dates.endDate) {
			$scope.allocation.dates.startDate = moment.utc(AllocationInfo.start_date);
			$scope.allocation.dates.endDate = moment.utc(AllocationInfo.end_date);
		}

		$scope.error = "";
		$scope.isValid = function () {
			if (StateParameters.projectStartDate && StateParameters.projectEndDate) {
				if ($scope.allocation.dates.startDate && $scope.allocation.dates.endDate) {
					let as = CalendarService.getUTCDate(new Date($scope.allocation.dates.startDate));
					let ps = CalendarService.getUTCDate(new Date(StateParameters.projectStartDate));
					let ae = CalendarService.getUTCDate(new Date($scope.allocation.dates.endDate));
					let pe = CalendarService.getUTCDate(new Date(StateParameters.projectEndDate));
					
					if (as >= ps && ae <= pe) {
						$scope.error = "";
						return true;
					} else {
						$scope.error = Translator.translate("ALLOCATION_CELL_PROJECT_ERROR");
					}
				} else {
					$scope.error = Translator.translate("ALLOCATION_CELL_DATE_ERROR");
				}
				return false;
			}
			$scope.error = "";
			return true;
		}
		$scope.isValid();
		$scope.selectedTotalHours = 0;
		$scope.selectedTotalDays = 0;
		$scope.perDay = undefined;
		$scope.totalHours = undefined;
		$scope.includeNotWorks = false;

		if (CostCenters.length === 1) {
			$scope.allocation.costCenter = CostCenters[0].item_id;
		}
		$scope.useFte = false;

		$scope.titleChanged = function () {
			DI.data.title = $scope.allocation.title;
			DI.data.primary = DI.data.title;
			$scope.header.title = DI.data.title;
			ProcessService.setDataChanged('allocation', DI.data.item_id);
		};

		SaveService.subscribeSave($scope, function () {
			if (costCenterChanged) {
				return AllocationService
					.setAllocationCostCenter(DI.data.item_id, $scope.allocation.costCenter, StateParameters.projectId)
					.then(function () {
						costCenterChanged = false;
						setCellRights(DI.data.status);
					});
			}
		});

		$scope.costCenterChanged = function () {
			if (CostCenters.length > 1) {
				costCenterChanged = true;
				SaveService.tick();
			}
		};

		$scope.clearAllocation = function() {
			MessageService.confirm(Translator.translate("ALLOCATION_CONFIRM_ALLOCATION_CLEAR") + ' ' + $scope.allocation.title + '?', function () {
				let scopeStart = moment.utc($scope.allocation.dates.startDate);
				let scopeEnd = moment.utc($scope.allocation.dates.endDate);
				let infoStart = moment.utc(AllocationInfo.start_date);
				let infoEnd = moment.utc(AllocationInfo.end_date);

				AllocationService.calendarDataChanged(
					scopeStart.isSameOrBefore(infoStart) ? scopeStart.toDate() : infoStart.toDate(),
					scopeEnd.isSameOrAfter(infoEnd) ? scopeEnd.toDate() : infoEnd.toDate(),
					0,
					DI
				);
				RM.setResolution();
			});
		}

		let status = DI.data.status;

		$scope.edit = false;
		$scope.useCalendar = true;
		setCellRights(status);

		ViewService.footbar(StateParameters.ViewId, {template: 'core/allocation/views/allocation/allocationFootbar.html'});

		$scope.workDays = function () {
			calculateCalendar();
		};

		$scope.$watch(function () {
			return $scope.allocation.dates.startDate + $scope.allocation.dates.endDate;
		}, function () {
			calculateCalendar();
			$scope.totalHours = null;
			$scope.perDay = null;
			DI.calendarRow.bars.allocationDateRange.draw();
		});

		$scope.$on('$destroy', function () {
			DI.calendarRow.bars.allocationDateRange.hide();
			DI.$ele.removeClass('selected');
			ResetSelection();
		});

		let removeAllocation = function () {
			AllocationService.removeAllocation(DI, StateParameters.ViewId);
		};

		if (DI.data.status == 55) {
			removeAllocation = function () {
				AllocationService.removeAssignedAllocation(DI.parent, RM, StateParameters.ViewId);
			}
		}

		$scope.header = {
			title: DI.data.primary,
			icons: [{
				icon: 'delete',
				onClick: removeAllocation
			},
				{
					icon: 'chat',
					onClick: function () {
						$scope.showChat = !$scope.showChat;
					}
				}]
		};

		$scope.reset = function () {
			MessageService.confirm($scope.resetText + ' ' + $scope.allocation.title + '?', function () {
				let newStatus = AllocationService.nextStatus(status, 'reset', StateParameters.autoAccept);

				return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
					status = newStatus;
					setCellRights(newStatus);
					$scope.edit = true;
					RM.setResolution();
				});
			});
		};

		$scope.approve = function () {
			MessageService.confirm($scope.approveText + ' ' + $scope.allocation.title + '?', function () {
				let newStatus = AllocationService.nextStatus(status == 13 ? 20 : status, 'approve', StateParameters.autoAccept);
				return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
					status = newStatus;
					setCellRights(newStatus);
					RM.setResolution();
				});
			});
		};

		$scope.send = function () {
			MessageService.confirm($scope.approveText + ' ' + $scope.allocation.title + '?', function () {
				let newStatus = AllocationService.nextStatus(status, 'approve', StateParameters.autoAccept);
				return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
					status = newStatus;
					setCellRights(newStatus);
					RM.setResolution();
				});
			});
		};

		$scope.reject = function () {
			MessageService.confirm($scope.rejectText + ' ' + $scope.allocation.title + '?', function () {
				let newStatus = AllocationService.nextStatus(status, 'reject', StateParameters.autoAccept);

				return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
					status = newStatus;
					setCellRights(newStatus);
					RM.setResolution();
				});
			});
		};

		$scope.weekDays = [{name: 'CAL_DAYS_LONG_1', value: 1}, {
			name: 'CAL_DAYS_LONG_2',
			value: 2
		}, {name: 'CAL_DAYS_LONG_3', value: 3}, {
			name: 'CAL_DAYS_LONG_4',
			value: 4
		}, {name: 'CAL_DAYS_LONG_5', value: 5}, {name: 'CAL_DAYS_LONG_6', value: 6}, {name: 'CAL_DAYS_LONG_0', value: 0}]

		$scope.additionalNotWorks = [];

		$scope.perDayFullDecimals = 0;


		$scope.changed = function (input) {
			switch (input) {
				case 'perDay':
					perDayToTotal();
					break;
				case 'total':
					totalToPerDay();
					break;
			}

			if (status == 20 && !updatingStatus) {
				updatingStatus = true;
				AllocationService.setAllocationStatus(DI, 25).then(function () {
					status = 25;
					updatingStatus = false;
					setCellRights(25);
				});
			}

			if (isNaN($scope.perDay)) {
				return;
			}

			let multiplier = $scope.useFte ? giveFteMultiplier() : 1;
			$scope.selectedTotalHours = Common.rounder($scope.totalHours* multiplier, 4);

			let i = changedDates.length;
			while (i--) {
				let dates = changedDates[i];

				AllocationService.calendarDataChanged(
					dates.startDate.toDate(),
					dates.endDate.toDate(),
					!$scope.useFte ? $scope.perDay : $scope.perDayFullDecimals*multiplier,
					DI
				);
			}

			RM.setResolution();
		};

		function calculateCalendar() {
			let dateRunner = moment.utc($scope.allocation.dates.startDate);
			let endDate = moment.utc($scope.allocation.dates.endDate);
			changedDates = [];
			if(!$scope.useFte){
				$scope.selectedTotalHours = 0;
			}
			$scope.selectedTotalDays = 0;

			let f = function (parentCalendar) {
				let sd = undefined;
				let start;

				while (dateRunner.isSameOrBefore(endDate)) {
					let d = dateRunner.date();
					let m = dateRunner.month() + 1;
					let y = dateRunner.year();

					let day = parentCalendar[y][m][d];
					let thisDay = new Date(y,m - 1,d);
					let exclude = $scope.additionalNotWorks.includes(thisDay.getDay())

					if (day && !(day.dayOff && !$scope.includeNotWorks) && !exclude) {

						let ca = AllocationService.getCalendarDataByDate(y, m, d)[DI.data.item_id];
						if (ca && typeof ca[DI.data.status] !== 'undefined' && !$scope.useFte) {
							$scope.selectedTotalHours += ca[DI.data.status];
						}

						if (!start || !sd) {
							sd = dateRunner.clone();
						}
						start = true;
						$scope.selectedTotalDays++;
					} else {
						if (start) {
							changedDates.push({
								startDate: sd,
								endDate: dateRunner.clone().subtract(1, 'days')
							});
						}

						start = false;
						sd = dateRunner.clone();
					}

					dateRunner.add(1, 'days');
				}

				if($scope.useFte)totalToPerDay();

				if (start) {
					changedDates.push({
						startDate: sd,
						endDate: dateRunner.clone().subtract(1, 'days')
					});
				}
			}

			if ($scope.useCalendar) {
				if ($scope.allocation.parentProcess == "competency") {
					CalendarService.getBaseCalendars().then(function (bases) {
						let b = bases.find(b => b.IsDefault === true);
						CalendarService.getBaseCalendar(
							b.CalendarId,
							$scope.allocation.dates.startDate,
							$scope.allocation.dates.endDate
						).then(function (parentCalendar) {
							f(parentCalendar);
						});
					});
				} else {
					CalendarService.getUserCalendar(
						ParentInfo.itemId,
						$scope.allocation.dates.startDate,
						$scope.allocation.dates.endDate
					).then(function (parentCalendar) {
						f(parentCalendar);
					});
				}
			} else {
				while (dateRunner.isSameOrBefore(endDate)) {
					let ca = AllocationService
						.getCalendarDataByDate(dateRunner.year(), dateRunner.month() + 1, dateRunner.date())[DI.data.item_id];

					if (ca && typeof ca[DI.data.status] !== 'undefined') {
						$scope.selectedTotalHours += ca[DI.data.status];
					}

					$scope.selectedTotalDays++;
					dateRunner.add(1, 'days');
				}

				changedDates.push({
					startDate: moment.utc($scope.allocation.dates.startDate),
					endDate: moment.utc($scope.allocation.dates.endDate)
				});
			}

			$scope.selectedTotalHours = Common.rounder($scope.selectedTotalHours, 2);
		}

		function totalToPerDay() {
			$scope.perDay = Common.rounder($scope.totalHours / $scope.selectedTotalDays, 2);
			$scope.perDayFullDecimals = $scope.totalHours / $scope.selectedTotalDays;

		}

		function perDayToTotal() {
			$scope.totalHours = Common.rounder($scope.selectedTotalDays * $scope.perDay, 4);
			$scope.perDayFullDecimals = $scope.totalHours / $scope.selectedTotalDays;
		}

		function setCellRights(status) {
			let info = AllocationService.getStatusInfo(
				status,
				{
					owner: Rights.owner,
					manager: Rights.manager,
					write: Rights.write,
					send: Rights.send,
					request: Rights.request,
					drag: Rights.drag,
					reset: Rights.reset
				});

			let r = info.rights;
			let t = info.text.long;

			if (typeof $scope.allocation.costCenter === 'undefined') {
				$scope.approveButton = false;
				$scope.rejectButton = false;
				$scope.resetButton = false;
				$scope.clearButton = false;
				$scope.sendButton = false;
			} else {
				$scope.approveButton = r.approve || r.reApprove;
				$scope.rejectButton = r.reject;
				$scope.resetButton = r.reset;
				$scope.clearButton = r.edit;
				$scope.sendButton = (status > 10 && r.request || r.reApprove);
			}
			$scope.edit = r.edit;

			$scope.resetText = t.reset;
			$scope.approveText = t.approve;
			if (status == 13) $scope.approveText = Translator.translate('ALLOCATION_APPROVE');
			$scope.rejectText = t.reject;
			$scope.sendText = Translator.translate('ALLOCATION_SEND_LONG');

			$scope.currentStatus = info.name;
		}
	}
})();