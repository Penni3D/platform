(function () {
	'use strict';

	angular
		.module('core.allocation')
		.config(TaskAllocationConfig)
		.controller('TaskAllocationController', TaskAllocationController);


	TaskAllocationConfig.$inject = ['ViewsProvider'];
	function TaskAllocationConfig(ViewsProvider) {
		ViewsProvider.addView('process.taskAllocation',
			{
				controller: 'TaskAllocationController',
				template: 'core/allocation/views/task/taskAllocation.html',
				level: 2,
				options: {
					extend: true
				},
				resolve: {
					ParentTask: function (StateParameters, ProcessService) {
						return ProcessService.getItem('task', StateParameters.taskId);
					},
					Allocations: function (StateParameters, AllocationService) {
						return AllocationService.getTaskAllocationData(StateParameters.taskId);
					},
					PortfolioColumns: function () {
						var cols = [];

						var p = {
							ItemColumn: {
								DataType: 0,
								Name: 'primary',
								LanguageTranslation: 'NAME'
							},
							Width: 256,
							UseInSelectResult: 1
						};

						var s = {
							ItemColumn: {
								DataType: 0,
								Name: 'secondary',
								LanguageTranslation: 'SECONDARY'
							},
							Width: undefined,
							UseInSelectResult: 2
						};

						var sd = {
							ItemColumn: {
								DataType: 3,
								Name: 'start_date',
								LanguageTranslation: 'START_DATE'
							},
							Width: 128,
							UseInSelectResult: 3
						};

						var ed = {
							ItemColumn: {
								DataType: 3,
								Name: 'end_date',
								LanguageTranslation: 'END_DATE'
							},
							Width: 128,
							UseInSelectResult: 4
						};

						cols.push(p);
						cols.push(s);
						cols.push(sd);
						cols.push(ed);

						return cols;
					}
				}
			});
	}


	TaskAllocationController.$inject = ['$scope', 'ViewService', 'StateParameters', 'Allocations', 'PortfolioColumns', 'ViewConfigurator', 'Translator'];
	function TaskAllocationController($scope, ViewService, StateParameters, Allocations, PortfolioColumns, ViewConfigurator, Translator) {
		$scope.header = {
			title: Translator.translate('ALLOCATION_TASK_TITLE') + ' ' + StateParameters.title,
			icons: [
				{
					icon: 'flip_to_back',
					onClick: function () {
						ViewService.view('process.gantt',
							{ params: StateParameters },
							{ split: true, replace: 'process.taskAllocation' });
					}
				}]
		};

		$scope.rows = Allocations.rowdata;
		$scope.portfolioColumns = PortfolioColumns;
		$scope.selectorId = ViewConfigurator.getConfiguration('process.projectAllocation', StateParameters.process).userSelectorPortfolioId;
		$scope.portfolioId = StateParameters.portfolioId;
		$scope.autoAccept = true;

		ViewService.footbar(StateParameters.ViewId, { template: 'core/allocation/views/allocationFootbar.html' });

		$scope.rules = {
			parents: {
				51: ['root'],
				50: [51]
			}
		};

		$scope.options = {
			calendarSteps: [15,30,60],
			inheritDates: false,
			showCalendar: true,
			selectable: false,
			sortable: true,
			ownerId: StateParameters.taskId,
			projectId: StateParameters.itemId,
			taskId: StateParameters.taskId,
			hasManagerRights: true
		};
	}
})();