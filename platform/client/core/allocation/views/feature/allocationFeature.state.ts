﻿(function () {
	'use strict';

	angular
		.module('core.allocation')
		.config(AllocationFeatureConfig)
		.controller('AllocationFeatureController', AllocationFeatureController);

	AllocationFeatureConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function AllocationFeatureConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('process.projectAllocation', {
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('user', 0);
				},
				ItemColumns: function (ProcessService, StateParameters, $filter) {
					return ProcessService.getColumns(StateParameters.process).then(function (columns) {
						let ret = [];

						angular.forEach(columns, function (key) {
							if (key.DataType === 3) {
								ret.push(key);
							}
						});

						return $filter('orderBy')(ret);
					});
				},
			},
			tabs: {
				default: {
					notificationText: function () {
						return {
							type: 'string',
							label: 'ALLOCATION_NOTIFICATION_TEXT'
						};
					},
					userSelectorPortfolioId: function (resolves) {
						return {
							type: 'select',
							options: resolves.Portfolios,
							label: 'ALLOCATION_USER_PORTFOLIO_ID',
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						}
					},
					autoAccept: function () {
						return {
							type: 'boolean',
							label: 'ALLOCATION_AUTO_ACCEPT'
						}
					},
					projectStartDate: function (resolves) {
						return {
							type: 'select',
							options: resolves.ItemColumns,
							label: 'GANTT_PROJECT_START_DATE',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						};
					},
					projectEndDate: function (resolves) {
						return {
							type: 'select',
							options: resolves.ItemColumns,
							label: 'GANTT_PROJECT_END_DATE',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						};
					},

					DefaultStatuses: function () {
						return {
							label: 'DEFAULT_STATUS_FILTERS',
							type: 'multiselect',
							options: [{value: 'draft', name: 'ALLOCATION_DRAFT_STATUS'},
								{value: 'previousDraft', name: 'ALLOCATION_PREVIOUS_DRAFT_STATUS'},
								{value: 'request', name: 'ALLOCATION_REQUEST_STATUS'},
								{value: 'reject', name: 'ALLOCATION_REJECT_STATUS'},
								{value: 'approve', name: 'ALLOCATION_APPROVE_STATUS'},
								{value: 'release', name: 'ALLOCATION_RELEASED_STATUS'}],
							optionValue: 'value',
							optionName: 'name'
						}
					}
				}
			}
		});

		ViewsProvider.addView('process.projectAllocation', {
			controller: 'AllocationFeatureController',
			template: 'core/allocation/views/feature/allocationFeature.html',
			level: 2,
			resolve: {
				Allocations: function (StateParameters, AllocationService, ArchiveDate, CalendarService, ViewHelperService) {
					if (ArchiveDate) {
						return AllocationService.getProjectArchiveAllocationData(StateParameters.itemId, ArchiveDate).then(function (allocations) {
							return {
								history: true,
								date: CalendarService.formatDate(ArchiveDate),
								data: allocations
							};
						});
					} else {
						return AllocationService.getProjectAllocationData(StateParameters.itemId).then(function (allocations) {
							return {
								history: false,
								data: allocations
							};
						});
					}
				},
				CostCenters: function (UserService, AllocationService) {
					return AllocationService.getUserCostCenters(UserService.getCurrentUserId()).then(function (costCenters) {
						return costCenters;
					});
				},
				PortfolioColumns: function () {
					let cols = [];

					let p = {
						ItemColumn: {
							DataType: 0,
							Name: 'primary',
							LanguageTranslation: 'ALLOCATION_NAME',
							showType: 'block'
						},
						Width: 384,
						UseInSelectResult: 1
					};

					let t = {
						UseInSelectResult: 0,
						ItemColumn: {
							Name: 'cost_center',
							LanguageTranslation: 'ALLOCATION_COST_CENTER',
							DataType: 6,
							DataAdditional: 'list_cost_center',
							showType: 'inline-block'
						},
						Width: 0
					};

					let s = {
						ItemColumn: {
							DataType: 6,
							Name: 'organisation',
							LanguageTranslation: 'ALLOCATION_ORGANISATION',
							DataAdditional: 'organisation',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					let q = {
						ItemColumn: {
							DataType: 0,
							Name: 'status_translation',
							LanguageTranslation: 'ALLOCATION_STATUS',
							showType: 'block'
						},
						Width: 0,
						UseInSelectResult: 0
					};

					let at = {
						ItemColumn: {
							DataType: 0,
							Name: 'allocation_total',
							LanguageTranslation: 'ALLOCATION_TOTAL',
							showType: 'inline-block'
						},
						Width: 0,
						UseInSelectResult: 2
					};

					cols.push(p);
					cols.push(s);
					cols.push(t);
					cols.push(q);
					cols.push(at);

					return cols;
				}
			}
		});

		ViewsProvider.addFeature('process.projectAllocation', 'project_allocation', 'FEATURE_PROJECT_ALLOCATION', ['read', 'write', 'send', 'request', 'drag', 'owner', 'reset']);
		ViewsProvider.addFeature('process.progress', 'progressProgress', "FEATURE_PROCESS_PROGRESS", ['read', 'write', 'grey', 'green', 'yellow', 'red', 'black', 'accent-1', 'accent-2']);
	}

	AllocationFeatureController.$inject = [
		'$scope',
		'PortfolioColumns',
		'StateParameters',
		'ViewService',
		'Allocations',
		'States',
		'Translator',
		'Common',
		'AllocationService',
		'CostCenters',
		'MessageService',
		'CalendarService',
		'Data',
		'LocalStorageService',
		'CellFactoryService'
	];

	function AllocationFeatureController(
		$scope,
		PortfolioColumns,
		StateParameters,
		ViewService,
		Allocations,
		States,
		Translator,
		Common,
		AllocationService,
		CostCenters,
		MessageService,
		CalendarService,
		Data,
		LocalStorageService,
		CellFactoryService
	) {
		if (StateParameters.notificationText) {
			setTimeout(function () {
				ViewService.notification(StateParameters.ViewId, StateParameters.notificationText);
			}, 1000);
		}
		CellFactoryService.reset();
		$scope.ownerCostCenters = CostCenters;
		$scope.header = {
			title: StateParameters.title,
			icons: []
		};
		$scope.isFteOn = AllocationService.getUseFte('allocation');

		$scope.toggleFte = function (modeIdentifier) {
			let fte = modeIdentifier == 'hours' ? false : true;
			let name1 = 'allocation'
			let name2 = 'allocation_fte'
			AllocationService.setFte(name1, fte);
			AllocationService.setFte(name2, fte);
			AllocationService.setModeName(modeIdentifier);
			$scope.isFteOn = fte
			ViewService.refresh(StateParameters.ViewId);
		}

		$scope.showDisregardCheck = true;
		$scope.disregardOtherAllocations = StateParameters.disregardOtherAllocations;
		$scope.reCalc = function () {
			$scope.disregardOtherAllocations = !$scope.disregardOtherAllocations;
			StateParameters.disregardOtherAllocations = $scope.disregardOtherAllocations;


			ViewService.refresh(StateParameters.ViewId, StateParameters);
		}

		let hasManagerRights = true;
		let hasOwnerRights = false;
		let hasWriteRights = false;
		let hasSendRights = false;
		let hasRequestRights = false;
		let hasDragRights = false;
		let hasResetRights = false;
		let states = States.project_allocation.groupStates[StateParameters.processGroupId].s;
		$scope.calendarDates = {
			s:LocalStorageService.get('date_range_feature_' + StateParameters.itemId) ? LocalStorageService.get('date_range_feature_' + StateParameters.itemId).s: undefined,
			e: LocalStorageService.get('date_range_feature_' + StateParameters.itemId) ? LocalStorageService.get('date_range_feature_' + StateParameters.itemId).e: undefined
		};

		$scope.header.icons.push({
			icon: 'settings', onClick: function () {
				let mm = [{
					name: Translator.translate('ALLOCATION_SHOW_NORMAL'),
					onClick: function () {
						$scope.toggleFte('hours')
					}
				},
					{
						name: Translator.translate('ALLOCATION_SHOW_DAY_MODE'),
						onClick: function () {
							$scope.toggleFte('allocation')
						}
					},
					{
						name: Translator.translate('ALLOCATION_SHOW_FTE'),
						onClick: function () {
							$scope.toggleFte('allocation_fte')
						}
					},
				]

				if (!Allocations.history) {
					mm.push({
						name: Translator.translate('ALLOCATION_SET_DATE_RANGE'),
						onClick: function () {
							let model = angular.copy($scope.calendarDates);
							let params = {
								title: 'ALLOCATION_LIMIT_TITLE',
								inputs: [
									{
										type: 'daterange',
										label: 'ALLOCATION_LIMIT_DATE',
										modelStart: 's',
										modelEnd: 'e'
									}],
								buttons: [
									{
										type: 'secondary',
										text: 'MSG_CANCEL'
									},
									{
										type: 'primary',
										text: 'CLEAR',
										onClick: function () {
											$scope.calendarDates = {
												s: undefined,
												e: undefined
											};
											StateParameters.calendarDates = undefined;
											$scope.limitAllocations();
										}
									},
									{
										type: 'primary',
										text: 'FILTER',
										onClick: function () {
											StateParameters.calendarDates = model;
											$scope.calendarDates = model;
											$scope.limitAllocations();
											LocalStorageService.store('date_range_feature_' + StateParameters.itemId, $scope.calendarDates);
										}
									}]
							};

							MessageService.dialog(params, model);
						}
					})
				}

				MessageService.menu(mm, event.currentTarget);
			},
		})
		
		if (!Allocations.history) {
			if (states.indexOf('send') !== -1) hasSendRights = true;
			if (states.indexOf('request') !== -1) hasRequestRights = true;
			if (states.indexOf('write') !== -1) hasWriteRights = true;
			if (states.indexOf('drag') !== -1) hasDragRights = true;
			if (states.indexOf('owner') !== -1) hasOwnerRights = true;
			if (states.indexOf('reset') !== -1) hasResetRights = true;
		} else {
			$scope.header.extTitle = '(' + Allocations.date + ')';
		}

		if (hasWriteRights || hasRequestRights) {
			$scope.header.icons.push({
				icon: 'add',
				onClick: function (event) {
					$scope.addNewItem(event);
				}
			});
		}
		$scope.hideClearButton = true;
		$scope.autoAccept = Common.isTrue(StateParameters.autoAccept) ? 1 : 0;
		$scope.portfolioColumns = PortfolioColumns;
		ViewService.footbar(StateParameters.ViewId, {template: 'core/allocation/views/allocationFootbar.html'});

		$scope.inactiveStatus = AllocationService.getActiveSettings(StateParameters.itemId, StateParameters.DefaultStatuses).status;
		$scope.toggleStatus = function (status) {
			AllocationService.toggleFilterStatus(StateParameters.itemId, status);
			setRows();
		};

		$scope.AMRules = {
			parents: {
				52: ['root'],
				51: [52],
				50: [51]
			}
		};
		if (hasDragRights) $scope.AMRules.draggables = [
			50
		];

		let executeQueue = [];
		let execute = function (i) {
			let item = executeQueue[i];
			if (item) {
				$scope.curentActionExecution += 1;
				let newStatus = AllocationService.nextStatus(item.data.status, 'approve', 0);
				return AllocationService.setAllocationStatus(item, newStatus).then(function () {
					execute(i + 1);
				});
			} else {
				$scope.calendarReDraw();
				$scope.clickSelStop();
			}
		};

		function hasCostCenterRights(DI) {
			if (Array.isArray($scope.ownerCostCenters)) {
				let i = $scope.ownerCostCenters.length;
				while (i--) {
					let costCenter = $scope.ownerCostCenters[i].item_id;
					if (Array.isArray(DI.data.cost_center)) {

						let j = DI.data.cost_center.length;
						while (j--) {
							if (DI.data.cost_center[j] == costCenter) {
								return true;
							}
						}
					} else if (costCenter == DI.data.cost_center) {
						return true;
					}
				}
			}

			return false;
		}

		$scope.approveMultiple = function (action) {
			MessageService.confirm('ALLOCATION_APPROVE_MULTIPLE', function () {
				$scope.actionsExecuting = true;
				executeQueue = [];
				$scope.curentActionExecution = 0;

				_.each($scope.getRMSelectedRows(), function (row) {
					if ((row.data.status == 20 && hasCostCenterRights(row)) || row.data.status == 13) {
						if (row.data.status == 13) row.data.status = 20;
						executeQueue.push(row);
					}
				});
				$scope.totalActionExecution = executeQueue.length;
				execute(0);
			});
		};
		$scope.sendMultiple = function (action) {
			MessageService.confirm('ALLOCATION_SEND_MULTIPLE', function () {
				$scope.actionsExecuting = true;
				executeQueue = [];
				$scope.curentActionExecution = 0;

				_.each($scope.getRMSelectedRows(), function (row) {
					if (row.data.status == 10 || row.data.status == 13) executeQueue.push(row);
				});
				$scope.totalActionExecution = executeQueue.length;
				execute(0);
			});
		};

		$scope.limitAllocations = function () {
			if ($scope.calendarDates.s && $scope.calendarDates.e) {
				let s = moment($scope.calendarDates.s).format('YYYY-MM-DD');
				let e = moment($scope.calendarDates.e).format('YYYY-MM-DD');

				$scope.header.title = StateParameters.title + ' - ' + Translator.translate('ALLOCATION_LIMIT_FILTER') + ': ' + CalendarService.formatDate($scope.calendarDates.s) + ' - ' + CalendarService.formatDate($scope.calendarDates.e);

				AllocationService.getProjectAllocationData(StateParameters.itemId, s, e).then(function (allocations) {
					Allocations.data = allocations;
					setRows();
				});
			} else {
				$scope.header.title = StateParameters.title;
				AllocationService.getProjectAllocationData(StateParameters.itemId).then(function (allocations) {
					Allocations.data = allocations;
					setRows();
				});
			}
		};

		$scope.AMoptions = {
			calendarSteps: [15, 60, 360],
			inheritDates: false,
			showCalendar: true,
			sortable: true,
			selectable: hasOwnerRights,
			ownerId: StateParameters.itemId + '_0',
			projectId: StateParameters.itemId,
			projectStartDate: Data.Data[StateParameters.projectStartDate],
			projectEndDate: Data.Data[StateParameters.projectEndDate],
			hasOwnerRights: hasOwnerRights,
			hasWriteRights: hasWriteRights,
			hasRequestRights: hasRequestRights,
			hasSendRights: hasSendRights,
			hasManagerRights: hasManagerRights,
			hasDragRights: hasDragRights,
			hasResetRights: hasResetRights,
			autoAccept: $scope.autoAccept,
			alias: {50: Translator.translate('RM_ALLOCATION')},
			openRows: StateParameters.openRows,
			competencyHours: StateParameters.CompetencyHours,
			disregardOtherAllocations: $scope.disregardOtherAllocations
		};


		if (StateParameters.calendarDates) {
			$scope.calendarDates = {
				s: StateParameters.calendarDates.s,
				e: StateParameters.calendarDates.e
			};
			setRows();
			$scope.limitAllocations();
		} else if($scope.calendarDates.s && $scope.calendarDates.e){
			setRows();
			$scope.limitAllocations();
		} else {
			setRows();
		}

		$scope.hasAtLeastOneCC = false; //checked in footbar file
		$scope.isManager = false;

		function setRows() {
			$scope.rows = AllocationService.filterAllocationsByStatus(
				StateParameters.itemId,
				AllocationService.addTotals(Allocations.data.rowdata, StateParameters.itemId, 0, false, false, !$scope.disregardOtherAllocations)
			);
			setTimeout(function () {
				$scope.isManager = hasManagerRights;
				_.each(allDI, function (d) {
					if (hasCostCenterRights(d)) {
						$scope.hasAtLeastOneCC = true;
						return;
					}
				});
			}, 500);
		}
	}
})();