﻿let cacheCalendar = {};
(function () {
	'use strict';

	angular
		.module('core.allocation', [])
		.service('AllocationService', AllocationService);

	AllocationService.$inject = [
		'ApiService',
		'SaveService',
		'$rootScope',
		'ProcessService',
		'Colors',
		'$q',
		'PortfolioService',
		'MessageService',
		'Translator',
		'Configuration',
		'Common',
		'ViewService',
		'$injector',
		'CalendarService',
		'AttachmentService',
		'BackgroundProcessesService'
	];

	function AllocationService(
		ApiService,
		SaveService,
		$rootScope,
		ProcessService,
		Colors,
		$q,
		PortfolioService,
		MessageService,
		Translator,
		Configuration,
		Common,
		ViewService,
		$injector,
		CalendarService,
		AttachmentService,
		BackgroundProcessesService
	) {

		/*
			--- ALLOCATION STATUS EXPLANATION ---
			10:	DRAFT
			13: DRAFT WITH MODIFICATIONS
			20:	REQUESTED
			25: REQUESTED WITH MODIFICATIONS
			30: REJECTED
			40: APPROVING WITH MODIFICATIONS
			50:	APPROVED
			53: OTHER APPROVED ALLOCATIONS
			55: ASSIGNED TASK / AUTO-APPROVE
			57: OTHER GROUPED ALLOCATIONS
			60:
			70: RELEASED
			80: ADVANCED FILTER DIALOG
		*/


		let self = this;
		let Allocation = ApiService.v1api('resources');
		let Totals = ApiService.v1api('resources/allocation/totals');
		let CostCenter = ApiService.v1api('settings/OrganisationManagement/list_cost_center/order/list_item');
		let QuickLook = ApiService.v1api('resources/allocation/quickLook');

		let groupedAllocations = {};
		let cachedAllocationData = {};
		let cachedAllocationTotals = {};
		let openCloseCache = {};

		let defaultHours = 1;

		CalendarService.getBaseCalendars().then(function (b) {
			_.each(b, function (calendar) {
				if (calendar.IsDefault == true) {
					defaultHours = calendar.WorkHours;
					return
				}
			})
		})

		self.returnDefaultHours = function(){
			return defaultHours;
		}

		let useFte = {};
		let modeName = "hours";

		self.setModeName = function (name) {
			modeName = name;
		}
		self.getModeName = function () {
			return modeName;
		}

		self.getUseFte = function (key) {
			if (useFte[key]) return useFte[key]
			return false;
		}

		self.setFte = function (key, isOn) {
			if (!useFte[key]) useFte[key] = false;
			useFte[key] = isOn;
		}

		self.getDefaultHours = function (resolution) {
			//New fte implementations:
			//resolution is passed from cellfactory.service using there existing row manager
			//mode names listed below
			//This bold logic trusts that it is always correct to multiply daily default hours by 5 to get the base value
			//Does the same assuming that there are 4 weeks in a month
			//User values are then later compared to this "base maximum value" of "work" and given how many percents it includes

			let dayMultiplier = modeName == 'resource_allocation' || modeName == 'project_allocation' || modeName == 'approval' ? 1 : 5;
			let weekMultiplier = modeName == 'resource_allocation' || modeName == 'project_allocation' || modeName == 'approval' ? 1 : 4;
			if (resolution == 60) {
				return defaultHours * dayMultiplier;
			} else if (resolution == 360) {
				return defaultHours * dayMultiplier * weekMultiplier;
			}
			return defaultHours;
		}

		self.setOpenClose = function (id, action) {
			openCloseCache[id] = action;
		}
		self.getOpenCloseCache = function () {
			return openCloseCache;
		}

		type AllocationDataType = {
			startDate: any,
			endDate: any,
			status: any,
			DI: any
		}
		let changedAllocationData: AllocationDataType = undefined;
		cacheCalendar = cachedAllocationData;

		function defaultAllocation() {
			return {
				title: Translator.translate('ALLOCATION_ALLOCATION_NEW'),
				durations: [],
				resource_item_id: undefined,
				process_item_id: undefined
			};
		}

		let cellColours = {
			allocation: {
				'-1': Colors.getColor('grey'),
				'-2': Colors.getColor('darkGrey'),
				10: Colors.getColor('grey'),
				13: Colors.getColor('cyan'),
				20: Colors.getColor('amber'),
				25: Colors.getColor('amber'),
				30: Colors.getColor('red'),
				40: Colors.getColor('green'),
				50: Colors.getColor('green'),
				55: Colors.getColor('green'),
				70: Colors.getColor('pink')
			}
		};

		let statusGroups = {
			10: 'draft',
			13: 'previousDraft',
			20: 'request',
			25: 'request',
			30: 'reject',
			40: 'approve',
			50: 'approve',
			70: 'release'
		};

		self.resetRequests = function (users) {
			let d = $q.defer();

			let model = {
				user: angular.copy(users)
			};

			let modifies = {
				user: false,
				cancel: true,
				reset: true
			};

			let params = {
				title: 'ALLOCATION_RESET_TITLE',
				inputs: [{
					type: 'user',
					model: 'user',
					label: 'ALLOCATION_REQUEST_USER',
					modify: 'user'
				}],
				buttons: [
					{
						modify: 'cancel',
						text: 'MSG_CANCEL'
					},
					{
						type: 'primary',
						modify: 'reset',
						text: 'MSG_RESET',
						onClick: function () {
							d.resolve();
						}
					}
				]
			};

			MessageService.dialog(params, model, undefined, modifies);

			return d.promise;
		};

		self.resetTaskAllocations = function (taskId) {
			return Allocation.new(['allocation', 'task', taskId, 'todraft']);
		};

		self.requestResources = function (users) {
			let d = $q.defer();
			let model = {user: [], competency: []};

			if (Array.isArray(users)) {
				model.user = angular.copy(users);
			}

			let params = {
				title: 'ALLOCATION_REQUEST_TITLE',
				inputs: [{
					type: 'user',
					model: 'user',
					label: 'ALLOCATION_REQUEST_USER'
				},
					{
						type: 'process',
						process: 'competency',
						model: 'competency',
						label: 'ALLOCATION_REQUEST_COMPETENCY'
					}],
				buttons: [{
					text: 'MSG_CANCEL'
				},
					{
						type: 'primary',
						text: 'MSG_REQUEST',
						onClick: function () {
							d.resolve(model);
						}
					}
				]
			};

			MessageService.dialog(params, model);

			return d.promise;
		};

		self.getSavedSettings = function (allocationId) {
			return Configuration.getSavedPreferences('allocation', allocationId);
		};

		self.setSavedSettings = function (allocationId, name, settings, listPublic?) {
			return Configuration.newPreferences('allocation', allocationId, name, listPublic, settings);
		};

		self.saveSavedSettings = function (allocationId, id, name, settings, listPublic?) {
			return Configuration.savePreferences('allocation', allocationId, id, name, listPublic, settings);
		};

		self.removeSavedSettings = function (allocationId, id, userId) {
			return Configuration.deleteSavedPreferences('allocation', allocationId, id, userId);
		};

		self.saveActiveSettings = function (allocationId) {
			if (typeof allocationId === 'undefined') {
				return;
			}

			return Configuration.saveActivePreferences('allocation', allocationId);
		};

		self.getActiveSettings = function (allocationId, defaultActiveSettings) {
			let allocationSettings = Configuration.getActivePreferences('allocation');
			allocationSettings[allocationId] = _.defaultsDeep(allocationSettings[allocationId], allocationDefaultConfigurations(defaultActiveSettings));

			return allocationSettings[allocationId];
		};

		self.allocationRequiresAction = function (status, isOwner, isManager) {
			if (isOwner && isManager) {
				return (status >= 20 && status < 30) || (status == 40 || status == 10);
			} else if (isOwner) {
				return status >= 20 && status < 30;
			} else if (isManager) {
				return status == 40 || status == 10;
			}

			return false;
		};

		self.getCellColour = function (process, status) {
			if (typeof cellColours[process] === 'undefined') {
				process = 'allocation';
			}

			if (typeof cellColours[process][status] === 'undefined') {
				return;
			}

			return cellColours[process][status].background;
		};

		self.getCellColourHex = function (process, status) {
			if (typeof cellColours[process] === 'undefined') {
				process = 'allocation';
			}

			let item = cellColours[process][status];
			return item ? item.hex : '#000000';
		};

		function isOtherAllocation(a, isOn) {
			if (isOn) return false;
			return a.item_id.toString().indexOf("_00") > -1 && a.item_id.replace("_00", "") == a.parent_item_id;
		}

		self.addTotals = function (allocations, parentId = 0, requireStatusOver = 10, flatView = false, removeZeros = false, sumOtherAllocations = true, viewType = "") {
			let topParents = [];
			let singleParent = undefined;

			_.each(allocations, function (r) {
				//This row is a parent
				if (r.parent_item_id == 0 && flatView == false) {
					topParents.push(r);
					return;
				}

				//Single Parent defined (in feature)
				if (r.item_id == parentId) {
					singleParent = r;
					return;
				}

				//Get from Cache
				let c = cachedAllocationTotals[r.item_id];
				let h = c ? c : 0;

				//Sum up children
				if (r.process != 'allocation') {
					_.each(allocations, function (a) {
						if (!isOtherAllocation(a, sumOtherAllocations) && (a.parent_item_id == r.item_id && requireStatusOver == 0 || (a.parent_item_id == r.item_id && a.status > requireStatusOver))) {
							let c = cachedAllocationTotals[a.item_id];
							h += c ? c : 0;
						}
					});
				}
				r.allocation_total = h;
			});

			//Calculate 3rd Level Parents
			for (let p of topParents) {
				let h = 0;
				_.each(allocations, function (a) {
					if (!isOtherAllocation(a, sumOtherAllocations) && a.parent_item_id == p.item_id) h += a.allocation_total ? a.allocation_total : 0;
				});
				p.allocation_total = h;
			}

			//Calculate Single Parent Total
			if (singleParent) {
				let h = 0;
				_.each(allocations, function (r) {
					if (!isOtherAllocation(r, sumOtherAllocations) && r.process == 'allocation' && r.status != 53 && r.status > requireStatusOver) {
						let c = cachedAllocationTotals[r.item_id];
						h += c ? c : 0;
					}
				});
				singleParent.allocation_total = h;
			}

			let result = [];
			let divider = self.getUseFte(viewType) ? defaultHours : 1;
			let addHChar = divider == 1 ? ' h' : ''
			_.each(allocations, function (r) {
				if (removeZeros && r.process == "task" && r.allocation_total == 0) return;
				r.allocation_total = Common.formatNumber(r.allocation_total / divider, 0) + addHChar;
				result.push(r);
			});

			return result;
		};

		let allocations_;

		self.filterAllocationsByStatus = function (allocationId, allocations) {
			let filterStatus = self.getActiveSettings(allocationId).status;
			let removedAllocations = [];
			let returnAllocations = [];

			angular.forEach(allocations, function (allocation) {
				if (allocation.process === 'allocation' && filterStatus[statusGroups[allocation.status]]) {
					removedAllocations.push(allocation);
				} else {
					returnAllocations.push(allocation);
				}
			});

			angular.forEach(groupedAllocations[allocationId], function (allocationId) {
				self.invalidateCalendarCache(allocationId);
			});

			groupedAllocations[allocationId] = [];

			let hoursForGroupedAllocations = {};
			angular.forEach(removedAllocations, function (allocation) {
				let parentId = allocation.parent_item_id;
				if (typeof hoursForGroupedAllocations[parentId] === 'undefined') {
					hoursForGroupedAllocations[parentId] = {};
				}

				angular.forEach(self.getAllocationData(allocation.item_id), function (year, y) {
					if (typeof hoursForGroupedAllocations[parentId][y] === 'undefined') {
						hoursForGroupedAllocations[parentId][y] = {};
					}

					angular.forEach(year, function (month, m) {
						if (typeof hoursForGroupedAllocations[parentId][y][m] === 'undefined') {
							hoursForGroupedAllocations[parentId][y][m] = {};
						}

						angular.forEach(month, function (day, d) {
							if (typeof hoursForGroupedAllocations[parentId][y][m][d] === 'undefined') {
								hoursForGroupedAllocations[parentId][y][m][d] = {};
							}

							angular.forEach(day, function (hours, status) {
								if (typeof hoursForGroupedAllocations[parentId][y][m][d][status] === 'undefined') {
									hoursForGroupedAllocations[parentId][y][m][d][status] = 0;
								}

								hoursForGroupedAllocations[parentId][y][m][d][status] += hours;
							});
						});
					});
				});
			});

			angular.forEach(hoursForGroupedAllocations, function (hours, resourceId) {
				let groupedAllocationsId = resourceId + '_01';

				groupedAllocations[allocationId].push(groupedAllocationsId);

				let o = {
					item_id: groupedAllocationsId,
					itemId: groupedAllocationsId,
					resource_item_id: resourceId,
					parent_item_id: resourceId,
					task_type: 50,
					status: 57,
					process: 'allocation',
					allocation_total: undefined,
					primary: Translator.translate('FILTERED_ALLOCATIONS')
				};

				returnAllocations.push(o);
				let h = 0;
				angular.forEach(hours, function (year, y) {
					angular.forEach(year, function (month, m) {
						angular.forEach(month, function (day, d) {
							angular.forEach(day, function (hours, status) {
								self.setCalendarData(y, m, d, groupedAllocationsId, status, hours);
								h += hours;
							});
						});
					});
				});
				o.allocation_total = Common.formatNumber(h, 0);
			});

			allocations_ = returnAllocations;
			return returnAllocations;
		};

		self.getStatusGroup = function (status) {
			return statusGroups[status];
		};

		self.toggleFilterStatus = function (allocationId, status) {
			let inactiveStatus = self.getActiveSettings(allocationId).status;
			inactiveStatus[status] = !inactiveStatus[status];
			self.saveActiveSettings(allocationId);
		};

		self.portfolioResourceCalendardates = {};

		function allocationDefaultConfigurations(defaultActiveSettings) {
			let isDefaultSettings = typeof defaultActiveSettings != 'undefined'
			return {
				projects: [],
				resources: [],
				status: {
					draft: isDefaultSettings && defaultActiveSettings.includes('draft') ? false : true,
					previousDraft: isDefaultSettings && defaultActiveSettings.includes('previousDraft') ? false : true,
					request: isDefaultSettings && defaultActiveSettings.includes('request') ? false : true,
					reject: isDefaultSettings && defaultActiveSettings.includes('reject') ? false : true,
					approve: isDefaultSettings && defaultActiveSettings.includes('approve') ? false : true,
					release: isDefaultSettings && defaultActiveSettings.includes('release') ? false : true
				}
			};
		}

		function getStatusTranslation(status) {
			let s = '';
			switch (status) {
				case 10:
					s = Translator.translate('ALLOCATION_DRAFT_STATUS');
					break;
				case 13:
					s = Translator.translate('ALLOCATION_PREVIOUS_DRAFT_STATUS');
					break;
				case 20:
					s = Translator.translate('ALLOCATION_REQUEST_STATUS');
					break;
				case 25:
					s = Translator.translate('ALLOCATION_REQUEST_MODIFIED_STATUS');
					break;
				case 30:
					s = Translator.translate('ALLOCATION_REJECT_STATUS');
					break;
				case 40:
					s = Translator.translate('ALLOCATION_APPROVE_MODIFIED_STATUS');
					break;
				case 50:
				case 55:
					s = Translator.translate('ALLOCATION_APPROVE_STATUS');
					break;
				case 70:
					s = Translator.translate('ALLOCATION_RELEASED_STATUS');
					break;
			}

			return s;
		}

		self.getStatusInfo = function (status, rights) {
			let allowed = {
				drag: false,
				request: false,
				approve: false,
				reject: false,
				reset: false,
				edit: false,
				reApprove: false
			};

			let text = {
				long: {
					approve: Translator.translate('ALLOCATION_APPROVE'),
					reject: Translator.translate('ALLOCATION_REJECT'),
					reset: Translator.translate('ALLOCATION_RESET'),
					reApprove: Translator.translate('ALLOCATION_APPROVE')
				},
				short: {
					approve: Translator.translate('ALLOCATION_APPROVE'),
					reject: Translator.translate('ALLOCATION_REJECT'),
					reset: Translator.translate('ALLOCATION_RESET'),
					reApprove: Translator.translate('ALLOCATION_APPROVE')
				}
			};

			switch (status) {
				case 10:
					if (rights.manager) {
						if (rights.write || rights.send) {
							allowed.approve = true;
						}

						if (rights.write || rights.request) {
							allowed.request = true;
							allowed.edit = true;
						}

						if (rights.write || rights.drag) {
							allowed.drag = true;
						}

						text.long.approve = Translator.translate('ALLOCATION_SEND_LONG');
						text.short.approve = Translator.translate('ALLOCATION_SEND_SHORT');
					}
					break;
				case 13:
					if (rights.manager) {
						if (rights.write || rights.send) allowed.approve = true;
						if (rights.write || rights.request) {
							allowed.request = true;
							allowed.edit = true;
						}
						if (rights.write || rights.drag) allowed.drag = true;

						text.long.approve = Translator.translate('ALLOCATION_SEND_LONG');
						text.short.approve = Translator.translate('ALLOCATION_SEND_SHORT');
					}

					if (rights.owner) {
						allowed.reApprove = true;
						if (rights.write) allowed.edit = true;
						text.long.approve = Translator.translate('ALLOCATION_SEND_LONG');
						text.short.approve = Translator.translate('ALLOCATION_SEND_SHORT');

						text.long.reApprove = Translator.translate('ALLOCATION_APPROVE');
						text.short.reApprove = Translator.translate('ALLOCATION_APPROVE');
					}
					allowed.reset = false;
					break;
				case 20:
					if (rights.owner) {
						if (rights.write || rights.send) {
							allowed.approve = true;
							allowed.reject = true;
						}

						if (rights.write) allowed.edit = true;
						if (rights.write || rights.drag) allowed.drag = true;

						text.long.approve = text.long.approve + ' ' + Translator.translate('ALLOCATION_REQUEST_LONG');
						text.long.reject = text.long.reject + ' ' + Translator.translate('ALLOCATION_REQUEST_LONG');
					}

					if (rights.manager && (rights.request || rights.write)) allowed.reset = true;

					break;
				case 25:
					if (rights.owner) {
						if (rights.write || rights.send) {
							allowed.approve = true;
							allowed.reject = true;
						}

						if (rights.write) allowed.edit = true;
						if (rights.write || rights.drag) allowed.drag = true;

						text.long.approve = text.long.approve + ' ' + Translator.translate('ALLOCATION_REQUEST_MODIFIED_LONG');
						text.long.reject = text.long.reject + ' ' + Translator.translate('ALLOCATION_REQUEST_LONG');
					}

					if (rights.manager && (rights.request || rights.write)) allowed.reset = true;

					break;
				case 30:
					if (rights.manager && (rights.request || rights.write)) allowed.reset = true;
					if (rights.reset) allowed.reset = true;
					break;
				case 40:
					if (rights.manager) {
						if (rights.write || rights.send) {
							allowed.approve = true;
							allowed.reject = true;
						}

						if (rights.write) allowed.reset = true;

						text.long.approve = text.long.approve + ' ' + Translator.translate('ALLOCATION_MODIFIED_LONG');
						text.long.reject += text.long.reject + ' ' + Translator.translate('ALLOCATION_MODIFIED_LONG');
					}
					if (rights.reset) allowed.reset = true;

					break;
				case 50:
					if (rights.reset) allowed.reset = true;
					break;
				case 55:
					if (rights.reset) allowed.reset = true;
					if (rights.manager && rights.write) allowed.edit = true;
					break;
				case 70:
					break;
			}

			return {
				rights: allowed,
				text: text,
				name: getStatusTranslation(status)
			};
		};

		let statusChanges = {
			reApprove: {
				0: {13: 50},
				1: {13: 55}
			},
			approve: {
				0: {
					10: 20,
					13: 20,
					20: 50,
					25: 40,
					40: 50
				},
				1: {
					10: 55
				}
			},
			reject: {
				0: {
					20: 30,
					25: 30,
					40: 30
				}
			},
			reset: {
				0: {
					13: 10,
					20: 10,
					25: 10,
					30: 10,
					40: 13,
					50: 13,
					55: 13
				}
			}
		};

		self.nextStatus = function (status, action, layer) {
			if (typeof layer === 'undefined') {
				layer = 0;
			}

			return statusChanges[action][layer][status];
		};

		let itemType = {};
		let converter = {};

		SaveService.subscribeSave($rootScope, function () {
			if (changedAllocationData) {
				let promises = [];
				let allocationPayload = [];
				let taskPayload = [];

				angular.forEach(changedAllocationData, function (conf, allocationId) {
					let dateRunner = moment.utc(conf.startDate);
					while (dateRunner.isSameOrBefore(conf.endDate)) {
						let y = dateRunner.year();
						let m = dateRunner.month() + 1;
						let d = dateRunner.date();

						if (typeof cachedAllocationData[y] === 'undefined' ||
							typeof cachedAllocationData[y][m] === 'undefined' ||
							typeof cachedAllocationData[y][m][d] === 'undefined' ||
							typeof cachedAllocationData[y][m][d][allocationId] === 'undefined') {

							dateRunner.add(1, 'days');
							continue;
						}

						let dateString = y + '-' + m + '-' + d;
						let allocationHours = cachedAllocationData[y][m][d][allocationId][conf.DI.data.status];
						switch (itemType[allocationId]) {
							case 'task':
								let ids = converter[allocationId];
								taskPayload.push({
									resource_item_id: ids.resource_item_id,
									task_item_id: ids.task_item_id,
									date: dateString,
									hours: allocationHours
								});
								break;
							case 'allocation':
								allocationPayload.push({
									allocation_item_id: allocationId,
									date: dateString,
									hours: allocationHours
								});
								break;
						}

						dateRunner.add(1, 'days');
					}
				});

				if (allocationPayload.length) {
					promises.push(Allocation.save(['allocation', 'dates'], allocationPayload));
				}

				if (taskPayload.length) {
					promises.push(Allocation.save(['task', 'dates'], taskPayload));
				}

				changedAllocationData = undefined;
				return $q.all(promises);
			}
		});

		function assignItemTaskType(rows) {
			let i = rows.length;
			while (i--) {
				let row = rows[i];

				if (row.task_type == 50) {
					itemType[row.item_id] = 'task';

					converter[row.item_id] = {
						item_id: row.actual_item_id,
						resource_item_id: row.parent_item_id,
						task_item_id: row.actual_item_id
					};
				}
			}
		}

		function updateCalendarData(itemId, oldStatus, newStatus) {
			angular.forEach(cachedAllocationData, function (year) {
				angular.forEach(year, function (month) {
					angular.forEach(month, function (day) {
						if (typeof day[itemId] === 'undefined' || typeof day[itemId][oldStatus] === 'undefined') {
							return;
						}

						day[itemId][newStatus] = day[itemId][oldStatus];
						delete day[itemId][oldStatus];
					});
				});
			});
		}

		function updateResponsibles(method, taskId, itemId) {
			ProcessService.getItem('task', taskId).then(function (result) {
				if (result.responsibles === '') {
					result.responsibles = [];
				}

				let responsibles = result.responsibles;
				let f = responsibles.indexOf(itemId);

				if (method === 'add') {
					if (f === -1) {
						responsibles.push(itemId);
						ProcessService.setDataChanged('task', taskId);
					}
				} else if (method === 'remove') {
					if (f !== -1) {
						responsibles.splice(f, 1);
						ProcessService.setDataChanged('task', taskId);
					}
				}
			});
		}

		self.clearViewAllocations = function(type){
			let savedSettings = type == 'projects' ? self.getActiveSettings(type).projects : self.getActiveSettings(type).resources;
			let i = savedSettings.length;
			while (i--) {
					savedSettings.splice(i, 1);
					self.saveActiveSettings(type);
			}
		}

		self.hasAllocationHours = function (itemId) {
			let hasHours = false;

			angular.forEach(cachedAllocationData, function (year) {
				if (hasHours) {
					return;
				}

				angular.forEach(year, function (month) {
					if (hasHours) {
						return;
					}

					angular.forEach(month, function (day) {
						let allocations = day[itemId];

						if (hasHours || typeof allocations === 'undefined') {
							return;
						}

						angular.forEach(allocations, function (a) {
							if (a > 0) {
								hasHours = true;
							}
						});
					});
				});
			});

			return hasHours;
		};

		self.getAllocationDates = function (itemId) {
			return Allocation.get('allocation/dates/' + itemId);
		};

		self.getAllocationData = function (itemId) {
			let ret = {};

			angular.forEach(cachedAllocationData, function (year, y) {
				angular.forEach(year, function (month, m) {
					angular.forEach(month, function (day, d) {
						if (typeof day[itemId] === 'undefined') {
							return;
						}

						extendCalendarObject(ret, y, m, d);
						ret[y][m][d] = day[itemId];
					});
				});
			});

			return ret;
		};

		function extendCalendarObject(object, year, month, day) {
			if (typeof object[year] === 'undefined') {
				object[year] = {};
			}

			if (typeof object[year][month] === 'undefined') {
				object[year][month] = {};
			}

			if (typeof object[year][month][day] === 'undefined') {
				object[year][month][day] = {};
			}
		}

		self.getAllocationTotalById = function (itemId, startDate, endDate, grouping) {
			return Totals.get([itemId, startDate, endDate, grouping]);
		};

		self.getAssignedTasks = function (taskId, process, itemId) {
			return Allocation.new(['task', taskId, 'resource', itemId]).then(function (data) {
				let promises = [];

				for (let i = 0, l = data.rowdata.length; i < l; i++) {
					let row = data.rowdata[i];

					if (row.task_type == 51) {
						row.status = 60;
						row.status_translation = getStatusTranslation(row.status);
						row.allocation_total = 666;
						updateResponsibles('add', taskId, itemId);
					} else if (row.task_type == 50) {
						row.status = 55;
						row.allocation_total = 0;
					}

					self.invalidateCalendarCache(row.item_id);
					data.rowdata[i] = ProcessService.setData({Data: row}, row.process, row.item_id).Data;

					promises.push(ProcessService.prepareItem(row.process, data.rowdata[i]));
				}

				return $q.all(promises).then(function () {
					assignItemTaskType(data.rowdata);
					return cacheCalendarData(data);
				});
			});
		};

		self.getOtherUserAllocations = function (processId, itemId) {
			return Allocation.get(['bydate', 'byprocess', processId, 'resources', itemId, 'other']).then(function (allocations) {
				let oa = allocations.rowdata[0];
				if (typeof oa === 'undefined') {
					return;
				}

				itemType[oa.item_id] = 'allocation';
				oa.task_type = 50;
				oa.status_translation = getStatusTranslation(oa.status);
				oa.allocation_total = 111;
				self.invalidateCalendarCache(oa.item_id);

				oa = ProcessService.setData({Data: oa}, oa.process, oa.item_id).Data;
				return ProcessService.prepareItem(oa.process, oa).then(function () {
					oa.primary = Translator.translate('ALLOCATION_OTHER_ALLOCATIONS');
					cacheCalendarData(allocations);
					return oa;
				});
			});
		};

		self.createAllocation = function (resource, itemId, params) {
			let a = defaultAllocation();

			a.resource_item_id = resource;
			a.process_item_id = itemId;

			angular.forEach(params, function (value, key) {
				a[key] = value;
			});

			return Allocation.new('allocation', a).then(function (data) {
				data.status_translation = getStatusTranslation(data.status);
				data.allocation_total = 0;
				data.process = 'allocation';
				ProcessService.setData({Data: data}, 'allocation', data.item_id);
				itemType[data.item_id] = 'allocation';

				return ProcessService.getItem('allocation', data.item_id);
			});
		};

		function cacheCalendarData(portfolioData) {
			angular.forEach(portfolioData.dates, function (year, y) {
				angular.forEach(year, function (month, m) {
					angular.forEach(month, function (day, d) {
						angular.forEach(day, function (row, r) {
							angular.forEach(row, function (hours, status) {
								self.setCalendarData(y, m, d, r, status, hours);
							});
						});
					});
				});
			});

			portfolioData.dates = cachedAllocationData;
			return portfolioData;
		}

		self.invalidateCalendarCache = function (itemId, type) {
			angular.forEach(cachedAllocationData, function (year) {
				angular.forEach(year, function (month) {
					angular.forEach(month, function (day) {
						if (typeof day[itemId] === 'undefined') {
							return;
						}

						typeof type === 'undefined' ? delete day[itemId] : delete day[itemId][type];
					});
				});
			});
		};

		self.setAllocationStatus = function (DI, status) {
			return Allocation.new(['allocation', DI.data.item_id, 'status', status]).then(function () {
				updateCalendarData(DI.data.item_id, DI.data.status, status);
				DI.data.status = status;
				DI.data.status_translation = getStatusTranslation(status);
				DI.updateFields();
				DI.showHighlight();

				if (DI.parent) {
					DI.parent.setDataFromChildren();
				}
			});
		};

		self.setAllocationCostCenter = function (allocationId, costCenter, projectId) {
			return Allocation.save(['allocation', allocationId], {
				item_id: allocationId,
				cost_center: costCenter,
				process_item_id: projectId
			});
		};

		self.getUserCostCenters = function (userId) {
			return CostCenter.get().then(function (costCenters) {
				let i = costCenters.length;
				while (i--) {
					let j = costCenters[i].owner.length;
					while (j--) {
						if (costCenters[i].owner[j] == userId) {
							break;
						}
					}

					if (j === -1) {
						costCenters.splice(i, 1);
					}
				}

				return costCenters;
			});
		};

		function getEndAfterDate(date) {
			let today = moment();
			let p = date.split('/');
			let offsetAmount = Number(p[0]);
			let offsetClass = p[1];

			switch (offsetClass) {
				case 'D':
					if (offsetAmount) {
						today.add(offsetAmount, 'days');
					}

					break;
				case 'W':
					if (offsetAmount) {
						today.add(offsetAmount, 'weeks');
					}

					today.startOf('isoWeek');
					break;
				case 'M':
					if (offsetAmount) {
						today.add(offsetAmount, 'months');
					}

					today.startOf('month');
					break;
				case 'Q':
					if (offsetAmount < 0) {
						today.subtract(-offsetAmount, 'quarters');
					} else if (offsetAmount) {
						today.add(offsetAmount, 'quarters');
					}

					today.startOf('month');
					break;
			}

			return today.format('YYYY-MM-DD');
		}

		self.getResourceAllocationData = function (endAfterDate, itemId, sd = undefined, ed = undefined) {
			self.clearCachedAllocationTotals();
			let url = ['bydate'];

			if (typeof endAfterDate === 'string') {
				url.push(getEndAfterDate(endAfterDate));
			}

			if (typeof itemId !== 'undefined') {
				url.push('user');
				url.push(itemId);
			}

			if (typeof sd !== 'undefined') url.push(sd);
			if (typeof ed !== 'undefined') url.push(ed);

			return Allocation.get(url).then(function (allocations) {
				return prepareResourceAllocation(allocations.rowdata).then(function () {
					return cacheCalendarData(allocations);
				});
			});
		};

		self.getResourceAllocationDataWithFilters = function (filters, portfolioId, portfolioId2, date) {
			self.clearCachedAllocationTotals();

			let parsedDate = "";
			if (typeof date === 'string') {
				parsedDate = getEndAfterDate(date)
			}

			return Allocation.new([portfolioId, portfolioId2, parsedDate, "filtered"], filters).then(function (allocations) {
				return prepareResourceAllocation(allocations.rowdata).then(function () {
					return cacheCalendarData(allocations);
				});
			});
		}

		self.getResourcesAllocationData = function (endAfterDate, itemIds) {
			self.clearCachedAllocationTotals();
			let url = ['bydate'];

			if (typeof endAfterDate === 'string') url.push(getEndAfterDate(endAfterDate));

			url.push('users');
			url.push(_.join(itemIds, ','));

			return Allocation.get(url).then(function (userAllocations) {
				let promises = [];
				angular.forEach(userAllocations, function (allocations) {
					promises.push(prepareResourceAllocation(allocations.rowdata).then(function () {
						return cacheCalendarData(allocations);
					}));
				});

				return $q.all(promises).then(function () {
					return userAllocations;
				});
			});
		};

		let dateStart = "";
		let dateEnd = "";
		let precision = 0;
		let Allocations = {};
		let CostCenters = {};

		self.setDateRange = function (title, process, portfolioId, portfolioFilters, additionalFields, resolution, ids) {
			var actual_filters = {};
			var portfolioFilters_sort = '';
			var portfolioFilters_text = '';
			if (portfolioFilters != null) {
				portfolioFilters_sort = portfolioFilters.sort;
				portfolioFilters_text = portfolioFilters.text;
				angular.forEach(portfolioFilters.select, function (valueList, columnId) {
					if (valueList.length > 0) {
							actual_filters[columnId] = valueList;
					}
				});
			}

			let filters = {
				excludeItemIds: [],
				offset: 0,
				limit: 0,
				recursive: false,
				filters: actual_filters,
				sort: portfolioFilters_sort,
				text: portfolioFilters_text
			};

			ProcessService.getItems('list_cost_center', undefined, true).then(function (ccs) {
				CostCenters = ccs;
				let today = new Date();
				let monthAgo = new Date();
				monthAgo.setDate(today.getDate() - 30);

				let eRowData = { 'date_start': monthAgo, 'date_end': today, 'precision': [3] };
				let precisionOptions = [{ 'Name': Translator.translate('LAYOUT_LINK_IN_DAY'), 'Value': 1 }, { 'Name': Translator.translate('LAYOUT_LINK_IN_WEEK'), 'Value': 2 }, {
					'Name': Translator.translate('LAYOUT_LINK_IN_MONTH'),
					'Value': 3
				}]

				let content = {
					buttons: [
						{
							text: 'CANCEL',
							modify: 'cancel'

						},
						{
							type: 'primary',
							modify: 'create',
							text: 'WORKTIME_REPORT_18_CR',
							onClick: function () {
								dateStart = eRowData.date_start;
								dateEnd = eRowData.date_end;
								precision = eRowData.precision;
								getResourcesExcel(title, process, portfolioId, dateStart, dateEnd, precision, filters, additionalFields, resolution, ids);
							}
						}
					],
					inputs: [{
						type: 'date',
						model: 'date_start',
						label: 'SELECT_RANGE_START',
					},
						{
							type: 'date',
							model: 'date_end',
							label: 'SELECT_RANGE_END',
						},

						{
							type: 'select',
							options: precisionOptions,
							optionName: 'Name',
							optionValue: 'Value',
							model: 'precision',
							label: 'SELECT_PRECISION',
						},

					],
					title: 'CREATE_ALLOCATION_EXCEL'
				};
				MessageService.dialog(content, eRowData);
			})
		}

		let getResourcesExcel = function (title, process, portfolioId, dateStart, dateEnd, precision, filters, additionalFields, resolution, ids) {
			let bgParams = {};
			bgParams["Mode"] = 'ResourcesExcel';
			bgParams["PortfolioId"] = portfolioId;
			bgParams["FileName"] = title + ' - Excel';
			bgParams["Process"] = process;
			bgParams["DateStart"] = moment(dateStart).format('YYYY-MM-DD');
			bgParams["DateEnd"] = moment(dateEnd).format('YYYY-MM-DD');
			bgParams["Precision"] = precision[0];
			bgParams["Filters"] = JSON.stringify(filters);
			bgParams["AdditionalFields"] = additionalFields;
			bgParams["Resolution"] = resolution;
			bgParams["Ids"] = ids;
			BackgroundProcessesService.executeOnDemand("GetDataAsExcel", bgParams, 'BG_PROCESS_STARTED');
		}

		// self.setDateRangeOld = function (itemSet, portfolioId, additionalSettings, rows) {
		// 	ProcessService.getItems('list_cost_center', undefined, true).then(function (ccs) {
		// 		CostCenters = ccs;
		// 		let today = new Date();
		// 		let monthAgo = new Date();
		// 		monthAgo.setDate(today.getDate() - 30);
		//
		// 		let eRowData = { 'date_start': monthAgo, 'date_end': today, 'precision': [3] };
		// 		let precisionOptions = [{ 'Name': Translator.translate('LAYOUT_LINK_IN_DAY'), 'Value': 1 }, { 'Name': Translator.translate('LAYOUT_LINK_IN_WEEK'), 'Value': 2 }, {
		// 			'Name': Translator.translate('LAYOUT_LINK_IN_MONTH'),
		// 			'Value': 3
		// 		}]
		//
		// 		let content = {
		// 			buttons: [
		// 				{
		// 					text: 'CANCEL',
		// 					modify: 'cancel'
		//
		// 				},
		// 				{
		// 					type: 'primary',
		// 					modify: 'create',
		// 					text: 'WORKTIME_REPORT_18_CR',
		// 					onClick: function () {
		// 						dateStart = eRowData.date_start;
		// 						dateEnd = eRowData.date_end;
		// 						precision = eRowData.precision;
		// 						additionalSettings.process == 'user' ? formExcelUsers(itemSet, portfolioId, additionalSettings, rows) : formExcelProjects(itemSet, portfolioId, additionalSettings, rows);
		// 					}
		// 				}
		// 			],
		// 			inputs: [{
		// 				type: 'date',
		// 				model: 'date_start',
		// 				label: 'SELECT_RANGE_START',
		// 			},
		// 			{
		// 				type: 'date',
		// 				model: 'date_end',
		// 				label: 'SELECT_RANGE_END',
		// 			},
		//
		// 			{
		// 				type: 'select',
		// 				options: precisionOptions,
		// 				optionName: 'Name',
		// 				optionValue: 'Value',
		// 				model: 'precision',
		// 				label: 'SELECT_PRECISION',
		// 			},
		//
		// 			],
		// 			title: 'CREATE_ALLOCATION_EXCEL'
		// 		};
		// 		MessageService.dialog(content, eRowData);
		// 	})
		// }

		let sortedValues = [];

		let orderRows = function ($ele, rows) {
			if($ele.data.parent_item_id == 0 && _.findIndex(rows, function(o) { return o.item_id == $ele.data.item_id; }) != -1 ){
				if ($ele.data.owner_item_id != 0) sortedValues.push($ele)
				if ($ele.firstEle) orderRows($ele.firstEle, rows)
				if ($ele.next) orderRows($ele.next, rows)
			} else if($ele.data.parent_item_id != 0) {
				if ($ele.data.owner_item_id != 0) sortedValues.push($ele)
				if ($ele.firstEle) orderRows($ele.firstEle, rows)
				if ($ele.next) orderRows($ele.next, rows)
			}
		}

		let excelDone1 = true;
		let excelDone2 = true;
		let excelDone3 = true;

		// let formExcelUsers = function(itemSet, portfolioId, additionalSettings, rows){
		// 	let confs = self.getActiveSettings('resources');
		// 	if (!confs.resources.length) confs.resources.push($rootScope.me.item_id);
		// 	self.getResourcesFilteredAllocationData('-1/M', confs.resources, confs.filters, additionalSettings.FilteringPortfolioId).then(function(allos) {
		// 		Allocations = allos;
		// 		sortedValues = [];
		// 		let parent = "";
		// 		_.each(itemSet, function (item) {
		// 			if (item.RM.rootParent.ownerType == 'resource_allocation' && item.data.process == 'user' && item.parent.isRoot) {
		// 				parent = item.parent;
		// 				return;
		// 			}
		// 		})
		//
		// 		orderRows(parent, rows);
		//
		// 		if (additionalSettings.ExcelPortfolioId1 > 0) {
		// 			let process = '';
		// 			let ids = [];
		// 			_.each(sortedValues, function (row) {
		// 				if (row.data.task_type == 52) {
		// 					process = row.data.process;
		// 					ids.push(row.data.itemId.split('_')[1]);
		// 				}
		// 			});
		// 			if (process != '') {
		// 				ProcessService.GetPrimariesAndSecondaries(process, additionalSettings.ExcelPortfolioId1, ids).then(function (data) {
		// 					_.each(sortedValues, function (row) {
		// 						if (row.data.task_type == 52) {
		// 							row.data.primary = data[row.data.itemId.split('_')[1]].primary;
		// 							row.data.secondary = data[row.data.itemId.split('_')[1]].secondary;
		// 						}
		// 					});
		// 					doFormExcelUsers(additionalSettings);
		// 				});
		// 			} else {
		// 				doFormExcelUsers(additionalSettings);
		// 			}
		// 		} else {
		// 			doFormExcelUsers(additionalSettings);
		// 		}
		// 	})
		// }

		let excelDones = [false,false,false];

		// let formExcelProjects = function (itemSet, portfolioId, additionalSettings, rows) {
		// 	let confs = self.getActiveSettings('projects');
		// 	if (!confs.projects.length) return {
		// 		rowdata: [],
		// 		dates: {}
		// 	};
		//
		// 	return self.getProjectsFilteredAllocationsData(confs.projects, confs.filters && confs.filters.filters ? confs.filters.filters : undefined, additionalSettings.FilteringPortfolioId).then(function(projectAllocations){
		// 		Allocations = projectAllocations;
		//
		// 	sortedValues = [];
		//
		// 	let parent = "";
		// 	_.each(itemSet, function (item) {
		// 		if (item.RM.rootParent.ownerType == 'project_allocation' && item.data.process != 'user' && item.parent.isRoot) {
		// 			parent = item.parent;
		// 			return;
		// 		}
		// 	})
		// 	orderRows(parent, rows);
		//
		// 	excelDone1 = false;
		// 	excelDone2 = false;
		// 	excelDone3 = false;
		//
		// 	let ids = [];
		// 		_.each(sortedValues, function (row) {
		// 			if (row.data.task_type == 52 && row.data.process == additionalSettings.SelectedProcesses) {
		// 				ids.push(row.data.itemId);
		// 			}
		// 		});
		//
		// 	for(let i = 1; i < 4; i++){
		// 		if (additionalSettings["ExcelPortfolioId" + i] > 0) {
		// 			if (ids.length > 0) {
		// 				ProcessService.GetPrimariesAndSecondaries(additionalSettings["SelectedProcesses" + i], additionalSettings["ExcelPortfolioId" + i], ids).then(function (data) {
		// 					_.each(sortedValues, function (row) {
		// 						if (row.data.task_type == 52 && row.data.process == additionalSettings.process) {
		// 							row.data.primary = data[row.data.itemId].primary;
		// 							row.data.secondary = data[row.data.itemId].secondary;
		// 						}
		// 					});
		// 					excelDones[i-1] = true;
		// 					doFormExcelProjects(additionalSettings);
		// 				});
		// 			}
		// 			else {
		// 				excelDones[i-1] = true;
		// 			}
		// 		}
		// 	}
		// 	doFormExcelProjects(additionalSettings);
		// 	});
		// }

		let tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };
		let headerCellColor = { 'Red': 255, 'Blue': 255, 'Green': 102 };
		let activeWeeks = [];
		let activeMonths = [];


		let addHeadersUsers = function (dateRanges) {
			activeWeeks = [];
			activeMonths = [];

			tableValues.Rows[0]['Cells'].push({
				Value: Translator.translate('OWNER_NAME'),
				BackgroundColor: headerCellColor,
				DataType: 4
			})
			tableValues.Rows[0]['Cells'].push({
				Value: Translator.translate('USER_NAME'),
				BackgroundColor: headerCellColor,
				DataType: 4
			})
			tableValues.Rows[0]['Cells'].push({
				Value: Translator.translate('ALLOCATION_TITLE'),
				BackgroundColor: headerCellColor,
				DataType: 4
			})
			tableValues.Rows[0]['Cells'].push({
				Value: Translator.translate('COST_CENTER_EXCEL'),
				BackgroundColor: headerCellColor,
				DataType: 4
			})
			tableValues.Rows[0]['Cells'].push({
				Value: Translator.translate('STATUS_EXCEL'),
				BackgroundColor: headerCellColor,
				DataType: 4
			})
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('TOTAL'), BackgroundColor: headerCellColor })
			_.each(Allocations, function (user) {
				_.each(user.dates, function (yearValues, year) {
					_.each(yearValues, function (monthValues, month) {
						_.each(monthValues, function (dailyValue, day) {
							let today = new Date(year, month - 1, day)
							let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2'])
							let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d'])
							let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

							if (today >= minDate && today <= maxDate) {
								if (precision == 1) {
									tableValues.Rows[0]['Cells'].push({
										Value: day + "/" + Number(month) + "/" + year,
										BackgroundColor: headerCellColor,
										DataType: 4
									});
								} else if (precision == 2) {
									if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
										return o.Value == Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year
									})) {
										tableValues.Rows[0]['Cells'].push({
											Value: Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year,
											BackgroundColor: headerCellColor,
											DataType: 4
										});
										activeWeeks.push(thisWeek + "_" + year);
									}
								} else {
									let monthDif = month - 1;
									if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
										return o.Value == Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year
									})) {
										tableValues.Rows[0]['Cells'].push({
											Value: Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year,
											BackgroundColor: headerCellColor,
											DataType: 4
										});
										activeMonths.push(month + "_" + year);
									}
								}
							}
						})
					})
				})
			})
		}

		let addHeadersProjects = function (dateRanges) {
			activeWeeks = [];
			activeMonths = [];

			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('OWNER_NAME'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('USER_NAME'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('ALLOCATION_TITLE'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('COST_CENTER_EXCEL'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('STATUS_EXCEL'), BackgroundColor: headerCellColor, DataType: 4 })
			tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('TOTAL'), BackgroundColor: headerCellColor, DataType: 4 })

			_.each(Allocations.dates, function (yearValues, year) {
				_.each(yearValues, function (monthValues, month) {
					_.each(monthValues, function (dailyValue, day) {
						let today = new Date(year, month - 1, day)
						let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2'])
						let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d'])
						let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

						if (today >= minDate && today <= maxDate) {
							if (precision == 1) {
								tableValues.Rows[0]['Cells'].push({ Value: day + "/" + Number(month) + "/" + year, BackgroundColor: headerCellColor, DataType: 4 });
							} else if (precision == 2) {
								if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
									return o.Value == Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year
								})) {
									tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('SEQ_DATEADD_WEEK') + thisWeek + "/" + year, BackgroundColor: headerCellColor, DataType: 4 });
									activeWeeks.push(thisWeek + "_" + year);
								}
							} else {
								let monthDif = month - 1;
								if (!_.find(tableValues.Rows[0]['Cells'], function (o) {
									return o.Value == Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year
								})) {
									tableValues.Rows[0]['Cells'].push({ Value: Translator.translate('CAL_MONTHS_LONG_' + monthDif) + "/" + year, BackgroundColor: headerCellColor, DataType: 4 });
									activeMonths.push(month + "_" + year);
								}
							}
						}
					})
				})
			})
		}


		let doFormExcelUsers = function (additionalSettings) {
			let dateRanges = {
				y: dateStart.getFullYear(),
				y2: dateEnd.getFullYear(),
				m: dateStart.getMonth() + 1,
				m2: dateEnd.getMonth() + 1,
				d: dateStart.getDate(),
				d2: dateEnd.getDate()
			}
			let index = 1;
			tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };
			tableValues.Rows.push({ 'Cells': [] });

			addHeadersUsers(dateRanges);
			let parentProject = "";
			let parentUser = "";
			let allocation = "";
			let secondary = "";

			_.each(sortedValues, function (row) {
				let valuesByWeek = {};
				let valuesByMonth = {};
				tableValues.Rows.push({ 'Cells': [] });

				if (row.data.task_type == 52) {
					parentProject = row.data.primary;
					secondary = row.data.secondary;
				} else if (row.data.task_type == 51) {
					parentUser = row.data.primary;
				} else {
					allocation = row.data.primary;
					tableValues.Rows[index]['Cells'].push({ Value: parentProject });
					tableValues.Rows[index]['Cells'].push({ Value: secondary });
					tableValues.Rows[index]['Cells'].push({ Value: parentUser });
					tableValues.Rows[index]['Cells'].push({ Value: allocation });
					tableValues.Rows[index]['Cells'].push({
						Value: row.data.cost_center ? _.find(CostCenters, function (o) {
							return o.item_id == row.data.cost_center
						}).list_item : ""
					});
					tableValues.Rows[index]['Cells'].push({ Value: row.data.status_translation });
					tableValues.Rows[index]['Cells'].push({ Value: row.data.allocation_total });

					_.each(Allocations, function (user) {
						valuesByWeek = {};
						valuesByMonth = {};
						let rowTotal = 0;
						_.each(user.dates, function (yearValues, year) {
							_.each(yearValues, function (monthValues, month) {
								_.each(monthValues, function (dailyValue, day) {
									let today = new Date(year, month - 1, day);
									let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2']);
									let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d']);
									let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

									if (today >= minDate && today <= maxDate) {
										if (precision == 1) {
											if (monthValues[day][row.data.item_id]) {
												rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
												tableValues.Rows[index]['Cells'].push({
													Value: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2),
													DataType: 2,
													NValue: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2)
												});
											} else {
												tableValues.Rows[index]['Cells'].push({ Value: 0, DataType: 2 });
											}
										} else if (precision == 2) {

											if (!valuesByWeek[thisWeek + "_" + year]) {
												valuesByWeek[thisWeek + "_" + year] = 0;
											}
											if (monthValues[day][row.data.item_id]) {
												rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
												valuesByWeek[thisWeek + "_" + year] = valuesByWeek[thisWeek + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
											}
										} else {
											if (!valuesByMonth[month + "_" + year]) {
												valuesByMonth[month + "_" + year] = 0;
											}
											if (monthValues[day][row.data.item_id]) {
												rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
												valuesByMonth[month + "_" + year] = valuesByMonth[month + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
											}
										}
									}
								});
								tableValues.Rows[index]['Cells'][6] = { Value: Common.rounder(rowTotal, 2), DataType: 2, NValue: Common.rounder(rowTotal, 2) };
							});
						});
					});

					if (precision == 2) {
						_.each(activeWeeks, function (week) {
							tableValues.Rows[index]['Cells'].push({
								Value: Common.rounder(valuesByWeek[week], 2),
								DataType: 2,
								NValue: Common.rounder(valuesByWeek[week], 2)
							});
						});
					}
					if (precision == 3) {
						_.each(activeMonths, function (month) {
							tableValues.Rows[index]['Cells'].push({
								Value: Common.rounder(valuesByMonth[month], 2),
								DataType: 2,
								NValue: Common.rounder(valuesByMonth[month], 2)
							});
						});
					}
					index++;
				}
			});

			tableValues.Rows[0]['Cells'].splice(1, 0, { Value: additionalSettings.SecondaryColumnTitle && additionalSettings.SecondaryColumnTitle.hasOwnProperty($rootScope.clientInformation.locale_id) ? additionalSettings.SecondaryColumnTitle[$rootScope.clientInformation.locale_id] : "Secondary", BackgroundColor: headerCellColor, DataType: 4 });
			AttachmentService.getExcel(additionalSettings.$$excelTitle + " - Excel", [], tableValues);

		}

		let doFormExcelProjects = function (additionalSettings) {
			if (excelDones[0] == true || excelDones[1] == true || excelDones[2] == true) {
				let dateRanges = {
					y: dateStart.getFullYear(),
					y2: dateEnd.getFullYear(),
					m: dateStart.getMonth() + 1,
					m2: dateEnd.getMonth() + 1,
					d: dateStart.getDate(),
					d2: dateEnd.getDate()
				}

				let index = 1;
				tableValues = { 'Rows': [], 'Name': Translator.translate("ROWS") };
				tableValues.Rows.push({ 'Cells': [] });

				addHeadersProjects(dateRanges);
				let parentProject = "";
				let parentUser = "";
				let allocation = "";
				let secondary = "";

				_.each(sortedValues, function (row) {

					let valuesByWeek = {};
					let valuesByMonth = {};
					tableValues.Rows.push({ 'Cells': [] });
					if (row.data.task_type == 52) {
						parentProject = row.data.primary;
						secondary = row.data.secondary;
					} else if (row.data.task_type == 51) {
						parentUser = row.data.primary;
					} else {
						allocation = row.data.primary;
						tableValues.Rows[index]['Cells'].push({ Value: parentProject })
						tableValues.Rows[index]['Cells'].push({ Value: secondary })
						tableValues.Rows[index]['Cells'].push({ Value: parentUser })
						tableValues.Rows[index]['Cells'].push({ Value: allocation })
						tableValues.Rows[index]['Cells'].push({
							Value: row.data.cost_center ? _.find(CostCenters, function (o) {
								return o.item_id == row.data.cost_center
							}).list_item : ""
						});
						tableValues.Rows[index]['Cells'].push({ Value: row.data.status_translation });
						tableValues.Rows[index]['Cells'].push({ Value: row.data.allocation_total });
						let rowTotal = 0;
						_.each(Allocations.dates, function (yearValues, year) {
							_.each(yearValues, function (monthValues, month) {
								_.each(monthValues, function (dailyValue, day) {
									let today = new Date(year, month - 1, day);
									let maxDate = new Date(dateRanges['y2'], dateRanges['m2'] - 1, dateRanges['d2']);
									let minDate = new Date(dateRanges['y'], dateRanges['m'] - 1, dateRanges['d']);
									let thisWeek = CalendarService.getWeekNumber(new Date(year, (month - 1), day));

									if (today >= minDate && today <= maxDate) {
										if (precision == 1) {
											if (monthValues[day][row.data.item_id]) {
												rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
												tableValues.Rows[index]['Cells'].push({
													Value: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2),
													DataType: 2,
													NValue: Common.rounder(monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]], 2)
												});
											} else {
												tableValues.Rows[index]['Cells'].push({ Value: 0, DataType: 2 });
											}
										} else if (precision == 2) {
											if (!valuesByWeek[thisWeek + "_" + year]) {
												valuesByWeek[thisWeek + "_" + year] = 0;
											}
											if (monthValues[day][row.data.item_id]) {
												rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
												valuesByWeek[thisWeek + "_" + year] = valuesByWeek[thisWeek + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
											}
										} else {
											if (!valuesByMonth[month + "_" + year]) {
												valuesByMonth[month + "_" + year] = 0;
											}
											if (monthValues[day][row.data.item_id]) {
												rowTotal = rowTotal + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
												valuesByMonth[month + "_" + year] = valuesByMonth[month + "_" + year] + monthValues[day][row.data.item_id][Object.keys(monthValues[day][row.data.item_id])[0]];
											}
										}
									}
								});
								tableValues.Rows[index]['Cells'][6] = { Value: rowTotal, DataType: 2, NValue: Common.rounder(rowTotal, 2) };
							});
						});

						if (precision == 2) {
							_.each(activeWeeks, function (week) {
								tableValues.Rows[index]['Cells'].push({
									Value: Common.rounder(valuesByWeek[week], 2),
									DataType: 2,
									NValue: Common.rounder(valuesByWeek[week], 2)
								});
							});
						}
						if (precision == 3) {
							_.each(activeMonths, function (month) {
								tableValues.Rows[index]['Cells'].push({
									Value: Common.rounder(valuesByMonth[month], 2),
									DataType: 2,
									NValue: Common.rounder(valuesByMonth[month], 2)
								});
							});
						}
						index++;
					}
				});
				tableValues.Rows[0]['Cells'].splice(1, 0, { Value: additionalSettings.SecondaryColumnTitle && additionalSettings.SecondaryColumnTitle.hasOwnProperty($rootScope.clientInformation.locale_id) ? additionalSettings.SecondaryColumnTitle[$rootScope.clientInformation.locale_id] : "Secondary", BackgroundColor: headerCellColor, DataType: 4 });

				AttachmentService.getExcel(additionalSettings.$$excelTitle + " - Excel", [], tableValues);
			}
		}


		self.getResourcesAllocationDataLimited = function (itemIds, sd: string, ed: string) {
			self.clearCachedAllocationTotals();
			let url = ['bydates'];
			url.push(sd);
			url.push(ed);
			url.push('users');
			return Allocation.new(url, itemIds).then(function (userAllocations) {
				let promises = [];
				angular.forEach(userAllocations, function (allocations) {
					promises.push(prepareResourceAllocation(allocations.rowdata).then(function () {
						return cacheCalendarData(allocations);
					}));
				});

				return $q.all(promises).then(function () {
					return userAllocations;
				});
			});
		};

		function prepareResourceAllocation(rows) {
			let promises = [];

			for (let i = 0, l = rows.length; i < l; i++) {
				let row = rows[i];

				if (row.parent_item_id == 0) {
					row.task_type = 51;
					row.allocation_total = 0;
				} else if (row.process != "allocation") {
					row.task_type = 52;
				} else {
					row.task_type = 50;
					itemType[row.item_id] = 'allocation';
					row.status_translation = getStatusTranslation(row.status);
					row.allocation_total = 0;
				}

				self.invalidateCalendarCache(row.item_id);
				rows[i] = ProcessService.setData({Data: row}, row.process, row.item_id).Data;

				promises.push(ProcessService.prepareItem(row.process, rows[i]));
			}

			return $q.all(promises);
		}

		self.getProjectsAllocationsData = function (ids: Array<number>, sd: string = undefined, ed: string = undefined) {
			self.clearCachedAllocationTotals();
			let params = ['bydate', 'byprocesses'];
			if (sd && ed) {
				params.push('date');
				params.push(sd);
				params.push(ed);
			}

			return Allocation.new(params, ids).then(function (allocations) {
				return prepareProjectAllocation(allocations.rowdata).then(function () {
					return cacheCalendarData(allocations);
				});
			});
		};

		self.getResourcesFilteredAllocationData = function (endAfterDate, itemIds, filters, portfolioId) {
			self.clearCachedAllocationTotals();
			if (!filters || !portfolioId) return self.getResourcesAllocationData(endAfterDate, itemIds);
			return PortfolioService.getPortfolioQueryObject("allocation", portfolioId, filters).then(function (filters) {

				let url = ['bydate'];

				if (typeof endAfterDate === 'string') url.push(getEndAfterDate(endAfterDate));

				url.push('users');
				url.push(portfolioId);
				url.push(_.join(itemIds, ','));

				return Allocation.new(url, filters).then(function (userAllocations) {
					let promises = [];
					angular.forEach(userAllocations, function (allocations) {
						promises.push(prepareResourceAllocation(allocations.rowdata).then(function () {
							return cacheCalendarData(allocations);
						}));
					});

					return $q.all(promises).then(function () {
						return userAllocations;
					});
				});
			});
		};
		self.getProjectsFilteredAllocationsData = function (itemIds, filters, portfolioId) {
			self.clearCachedAllocationTotals();
			if (!filters || !portfolioId) return self.getProjectsAllocationsData(itemIds);
			return PortfolioService.getPortfolioQueryObject("allocation", portfolioId, filters).then(function (filters) {
				let url = ['bydate'];
				url.push('byprocesses');
				url.push(portfolioId);
				url.push(_.join(itemIds, ','));
				return Allocation.new(url, filters).then(function (projectAllocations) {
					return prepareProjectAllocation(projectAllocations.rowdata).then(function () {
						return cacheCalendarData(projectAllocations);
					});
				});
			});
		};

		self.getProjectAllocationData = function (itemId, sd = undefined, ed = undefined) {
			self.clearCachedAllocationTotals();
			return Allocation.get(['bydate', 'byprocess', itemId, sd, ed]).then(function (allocations) {
				return prepareProjectAllocation(allocations.rowdata, itemId).then(function () {
					return cacheCalendarData(allocations);
				});
			});
		};

		self.getProjectArchiveAllocationData = function (itemId, date) {
			self.clearCachedAllocationTotals();
			return Allocation.get(['archive', date, 'bydate', 'byprocess', itemId]).then(function (allocations) {
				return prepareProjectAllocation(allocations.rowdata, itemId, true).then(function () {
					return cacheCalendarData(allocations);
				});
			});
		};

		function prepareProjectAllocation(rows, processId?, local?) {
			let promises = [];

			angular.forEach(rows, function (row, key) {
				if (row.parent_item_id == 0) {
					row.task_type = 52;

					if (typeof processId !== 'undefined') {
						row.parent_item_id = processId + '_0';
					}
				} else if (row.process == 'allocation') {
					itemType[row.item_id] = 'allocation';
					row.task_type = 50;
					row.status_translation = getStatusTranslation(row.status);
					//row.allocation_total = 888;
				} else {
					row.task_type = 51;
					//row.allocation_total = 888;
				}

				self.invalidateCalendarCache(row.item_id);

				if (Common.isFalse(local)) {
					rows[key] = ProcessService.setData({Data: row}, row.process, row.item_id).Data;
				}

				promises.push(ProcessService.prepareItem(row.process, rows[key]).then(function () {
					if (row.status == 53) {
						rows[key].primary = Translator.translate('ALLOCATION_OTHER_ALLOCATIONS');
					}
				}));
			});

			return $q.all(promises);
		}

		self.getTaskAllocationData = function (taskId) {
			self.clearCachedAllocationTotals();
			return Allocation.get(['bydate', 'bytask', taskId]).then(function (data) {
				let promises = [];

				for (let i = 0, l = data.rowdata.length; i < l; i++) {
					let row = data.rowdata[i];

					if (row.task_type == 51) {
						row.status = 60;
						row.allocation_total = 999;
					} else if (row.task_type == 50) {
						row.status = 55;
						row.status_translation = getStatusTranslation(row.status);
						row.allocation_total = 999;
					}

					self.invalidateCalendarCache(row.item_id);
					data.rowdata[i] = ProcessService.setData({Data: row}, row.process, row.item_id).Data;

					promises.push(ProcessService.prepareItem(row.process, data.rowdata[i]));
				}

				return $q.all(promises).then(function () {
					assignItemTaskType(data.rowdata);
					return cacheCalendarData(data);
				});
			});
		};

		self.setCalendarData = function (year, month, day, itemId, status, hours) {
			if (typeof cachedAllocationData[year] === 'undefined')
				cachedAllocationData[year] = {};

			if (typeof cachedAllocationData[year][month] === 'undefined')
				cachedAllocationData[year][month] = {};

			if (typeof cachedAllocationData[year][month][day] === 'undefined')
				cachedAllocationData[year][month][day] = {};

			if (typeof cachedAllocationData[year][month][day][itemId] === 'undefined')
				cachedAllocationData[year][month][day][itemId] = {};

			cachedAllocationTotals[itemId] = typeof cachedAllocationTotals[itemId] === 'undefined' ?
				hours : cachedAllocationTotals[itemId] + hours;

			cachedAllocationData[year][month][day][itemId][status] = hours;
		};

		self.calendarDataChanged = function (startDate, endDate, hours, DI) {
			let allocationId = DI.data.item_id;
			let status = DI.data.status;
			let dateRunner = moment.utc(startDate);

			while (dateRunner.isSameOrBefore(endDate)) {
				self.setCalendarData(dateRunner.year(), dateRunner.month() + 1, dateRunner.date(), allocationId, status, hours);
				dateRunner.add(1, 'days');
			}

			let changedAllocation = undefined;
			if (changedAllocationData) {
				changedAllocation = changedAllocationData[allocationId];
			} else {
				changedAllocationData = {};
			}

			if (typeof changedAllocation === 'undefined') {
				changedAllocationData[allocationId] = {
					startDate: startDate,
					endDate: endDate,
					status: status,
					DI: DI
				};
			} else {
				if (changedAllocation.startDate > startDate) {
					changedAllocation.startDate = startDate;
				}

				if (changedAllocation.endDate < endDate) {
					changedAllocation.endDate = endDate;
				}
			}

			SaveService.tick();
		};

		self.removeAssignedAllocation = function (DI, RM, viewId) {
			MessageService.confirm(Translator.translate('ALLOCATION_TASK_RESOURCE') + ' ' + DI.data.primary + ' ' + Translator.translate('ALLOCATION_TASK_RESOURCE_NO_LONGER'), function () {
				self.removeResourceFromTask(RM.options.taskId, DI.data.item_id).then(function () {
					self.invalidateCalendarCache(DI.data.item_id);
					DI.close();

					let RMService = $injector.get('RMService');
					let toBeDeleted = [];
					let runner = DI.firstEle;
					while (runner) {
						toBeDeleted.push(runner.data.item_id);
						runner = runner.next;
					}

					let i = toBeDeleted.length;
					while (i--) {
						RMService.itemSet[toBeDeleted[i]].delete();
					}

					DI.delete();
					RM.setCalendarHeight();

					if (typeof viewId !== 'undefined') {
						ViewService.close(viewId);
					}
				});
			});
		};

		self.removeAllocation = function (DI, viewId) {
			MessageService.confirm(
				Translator.translate('ALLOCATION_CONFIRM_ALLOCATION_DELETE') + ' ' + DI.data.primary + '?', function () {
					ProcessService.deleteItem('allocation', DI.data.item_id).then(function () {
						self.invalidateCalendarCache(DI.data.item_id);
						let RM = DI.RM;
						let parent = DI.parent;
						DI.delete();

						let hasAllocations = false;
						let runner = parent.firstEle;

						while (runner) {
							if (runner.data.status !== 53) {
								hasAllocations = true;
								break;
							}

							runner = runner.next;
						}

						if (hasAllocations) {
							parent.setDataFromChildren();
						} else {
							parent.parent.setDataFromChildren();
							parent.close();
							parent.delete();
						}

						if (typeof viewId !== 'undefined') {
							ViewService.close(viewId);
						}

						if (RM) {
							RM.calendarReDraw();
						}
					});
				});
		};

		self.removeResourceFromTask = function (taskId, itemId) {
			return Allocation.delete(['task', taskId, 'resource', itemId]).then(function () {
				self.invalidateCalendarCache(itemId);
				updateResponsibles('delete', taskId, itemId);
			});
		};

		self.getCalendarDataByDate = function (year, month, day) {
			if (typeof cachedAllocationData[year] === 'undefined' ||
				typeof cachedAllocationData[year][month] === 'undefined' ||
				typeof cachedAllocationData[year][month][day] === 'undefined'
			) {
				return {};
			}

			return cachedAllocationData[year][month][day];
		};

		self.clearCachedAllocationTotals = function () {
			cachedAllocationTotals = {};
		};

		self.getCachedAllocationTotals = function () {
			return cachedAllocationTotals;
		};

		self.getAllocatedUsersForTask = function (taskId, processId) {
			self.clearCachedAllocationTotals();
			return Allocation.get(['users', 'allocated', taskId, processId]).then(function (data) {
				let promises = [];

				for (let i = 0, l = data.rowdata.length; i < l; i++) {
					let row = data.rowdata[i];

					row.parent_item_id = taskId;
					self.invalidateCalendarCache(row.item_id);
					data.rowdata[i] = ProcessService.setData({Data: row}, 'user', row.item_id).Data;

					promises.push(ProcessService.prepareItem('user', data.rowdata[i]));
				}

				return $q.all(promises).then(function () {
					return cacheCalendarData(data);
				});
			});
		};

		self.getUsersForTask = function (taskId, processId, portfolioId, noFilters) {
			let filters = PortfolioService.getActiveSettings(portfolioId).filters;
			let textFilter = filters.text === '' ? ' ' : filters.text;

			let selectFilters = _.chain(filters.select)
				.map(function (filter, key) {
					if (!_.isEmpty(filter)) {
						return key + ';' + _.join(filter, '-');
					}
				})
				.filter(function (val) {
					return !_.isEmpty(val);
				})
				.join(',')
				.value();

			if (Common.isTrue(noFilters)) {
				textFilter = undefined;
				selectFilters = undefined;
			}

			return Allocation.get(['users', taskId, processId, textFilter, selectFilters]).then(function (data) {
				let promises = [];

				for (let i = 0, l = data.rowdata.length; i < l; i++) {
					let row = data.rowdata[i];

					row.parent_item_id = taskId;
					self.invalidateCalendarCache(row.item_id);
					data.rowdata[i] = ProcessService.setData({Data: row}, row.process, row.item_id).Data;

					promises.push(ProcessService.prepareItem(row.process, data.rowdata[i]));
				}

				return $q.all(promises).then(function () {
					return cacheCalendarData(data);
				});
			});
		};

		self.getUsersQuickLook = function (ids: Array<number>) {
			return QuickLook.new([], ids);
		}
	}
})();