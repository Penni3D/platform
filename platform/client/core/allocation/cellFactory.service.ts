﻿let allCells;

(function () {
	'use strict';

	angular
		.module('core.allocation')
		.service('CellFactoryService', CellFactoryService);

	CellFactoryService.$inject = ['UniqueService', 'AllocationService', 'Colors', 'CalendarService', 'Common', '$q', 'Translator'];

	function CellFactoryService(UniqueService, AllocationService, Colors, CalendarService, Common, $q, Translator) {
		let self = this;
		let rowHeight = 46;
		let orderTop = [-1, -2, 0];
		let orderBottom = [
			10, 13, 20, 25, 30, 40, 50, 53,
			55, 60, 70, 80
		];

		function calculateCellPixelHeight(value, maxValue) {
			let proportion = value / maxValue;
			return Common.rounder(proportion * rowHeight, 1);
		}

		self.cells = {};

		allCells = self.cells;

		let cellPool = {
			inheritChildren: [],
			allocationUser: [],
			allocation: [],
			task: [],
			taskUser: [],
			maxChildren: [],
			maxChildrenUser: [],
			itemFilter: [],
			projectFilter: [],
			default: []
		};
		let divider = 1;
		let useFte = false;
		let defHours = 7.5;

		self.reset = function () {
			cellPool = {
				inheritChildren: [],
				allocationUser: [],
				allocation: [],
				task: [],
				taskUser: [],
				maxChildren: [],
				maxChildrenUser: [],
				itemFilter: [],
				projectFilter: [],
				default: []
			};
		}

		self.get = function (DI, startDate, endDate) {
			if (typeof DI === 'undefined') {
				return;
			}

			useFte = AllocationService.getUseFte(DI.RM.options.ownerType);
			//Logic to find divider by which "mode" is selected (day, month,quartal)
			divider = useFte ? AllocationService.getDefaultHours(DI.RM.getResolution()) : 1;
			defHours = AllocationService.returnDefaultHours();
			let cell;
			let type = DI.cell.type ? DI.cell.type : 'default';

			if (cellPool[type].length) {
				cell = cellPool[type].pop();
			} else {
				switch (type) {
					case 'task':
						cell = taskCell();
						break;
					case 'taskUser':
						cell = taskUserCell();
						break;
					case 'inheritChildren':
						cell = inheritChildrenCell();
						break;
					case 'allocation':
						cell = allocationCell();
						break;
					case "allocationUser":
						cell = allocationUserCell();
						break;
					case 'maxChildren':
						cell = maxChildrenCell();
						break;
					case 'maxChildrenUser':
						cell = maxChildrenUserCell();
						break;
					case 'itemFilter':
						cell = itemFilterCell();
						break;
					case 'projectFilter':
						cell = projectFilterCell();
						break;
					default:
						cell = defaultCell();
						break;
				}
			}

			cell.clear(DI, startDate, endDate);
			return cell;
		};

		function defaultCell() {
			let cell = cellTemplate();
			cell.calculate = function () {
				cell.max = 0;
				let DI = cell.owner;

				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d)[DI.data.item_id];

					angular.forEach(calendarData, function (hours, type) {
						addHours(cell, type, hours);
					});
				});

				if (cell.total == 0) {
					return false;
				}

				if (DI.data.process == 'user') {
					cell.isLoading = true;
					CalendarService
						.getUserCalendar(DI.data.resource_item_id, cell.startDate, cell.endDate)
						.then(function (userCalendar) {
							cell.isLoading = false;

							if (cell.isCleared) {
								cellPool[cell.type].push(cell);
								return;
							}

							loopCellDates(cell, function (y, m, d) {
								let userDay = userCalendar[y][m][d];
								cell.max += userDay.notWork ? 0 : userDay.workHours;
							});

							if (Common.rounder(cell.total, 2) > Common.rounder(cell.max, 2)) {
								cell.overflow = true;
							}

							cell.draw(cell.position.left, cell.position.width);
						});
				} else {
					cell.max = cell.total;
				}

				return cell;
			};

			return cell;
		}

		function taskCell() {
			let cell = cellTemplate();
			cell.type = 'task';
			cell.calculate = function () {
				let DI = cell.owner;
				cell.max = 0;

				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d);
					angular.forEach(calendarData[DI.data.item_id], function (hours, type) {
						addHours(cell, type, hours);
					});

					if (DI.parent && calendarData[DI.parent.data.item_id]) {
						cell.max += calendarData[DI.parent.data.item_id].ta;
					}
				});

				if (cell.total == 0) {
					return false;
				}

				if (Common.rounder(cell.total, 2) > Common.rounder(cell.max, 2)) {
					cell.overflow = true;
				}

				return cell;
			};

			cell.getBackground = function () {
				return Colors.getColor('yellow').background;
			};

			return cell;
		}

		function taskUserCell() {
			let cell = cellTemplate();
			cell.type = 'taskUser';
			cell.calculate = function () {
				cell.max = 0;
				let DI = cell.owner;

				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d);
					let data = calendarData[DI.data.item_id];

					if (data) {
						if (data.ta) {
							cell.max += calendarData[DI.data.item_id].ta;
						}

						if (data.oa && DI.cell.mode === 'calculateOa') {
							cell.total += calendarData[DI.data.item_id].oa;
						}
					}

					inheritChildrenHours(cell, DI, calendarData);
				});

				if (cell.max == 0 && cell.total == 0) {
					return false;
				}

				if (Common.rounder(cell.total, 2) > Common.rounder(cell.max, 2)) {
					cell.overflow = true;
				}

				return cell;
			};

			cell.getBackground = function () {
				return Colors.getColor('teal').background;
			};

			cell.drawTotal = function () {
				let addHChar = divider == 1 ? ' h' : ''
				showTotal(cell, Common.rounder(cell.total, 1) + addHChar);
			};

			return cell;
		}

		function itemFilterCell() {
			let cell = cellTemplate();
			cell.type = 'itemFilter';
			cell.getBackground = function (type) {
				if (type == 80) {
					return Colors.getColor('red').background;
				}

				return Colors.getColor('lightBlue').background;
			};

			cell.calculate = function () {
				cell.max = 0;
				cell.isLoading = true;
				let DI = cell.owner;
				let itemId = DI.data.item_id;

				CalendarService
					.getUserCalendar(itemId, cell.startDate, cell.endDate)
					.then(function (itemCalendar) {
						cell.isLoading = false;

						if (cell.isCleared) {
							cellPool[cell.type].push(cell);
							return;
						}

						loopCellDates(cell, function (y, m, d) {
							let dayAllocations = AllocationService.getCalendarDataByDate(y, m, d);
							let todayEffort = 0;

							angular.forEach(DI.data.allocations, function (allocationId) {
								let calendarData = dayAllocations[allocationId];

								angular.forEach(calendarData, function (hours, type) {
									if (type == 50 || type == 53 || type == 55) {
										todayEffort += hours;
									}
								});
							});

							addHours(cell, 50, todayEffort);

							let userDay = itemCalendar[y][m][d];
							let workHours = userDay.notWork ? 0 : userDay.workHours;

							if (dayAllocations[itemId] && dayAllocations[itemId][80]) {
								let projectHours = dayAllocations[itemId][80];
								addHours(cell, 80, projectHours);
								todayEffort += projectHours;
							}

							if (Common.rounder(todayEffort, 2) > workHours) {
								cell.warn = true;
							}

							cell.max += workHours
						});

						if (Common.rounder(cell.total, 2) <= 0 && !cell.calculations[80]) {
							return;
						}
						cell.draw(cell.position.left, cell.position.width);
					});

				return cell;
			};

			return cell;
		}

		function projectFilterCell() {
			let cell = cellTemplate();
			cell.type = 'projectFilter';
			cell.getBackground = function () {
				return Colors.getColor('red').background;
			};

			cell.calculate = function () {
				cell.max = 0;
				cell.isLoading = true;

				let DI = cell.owner;
				let promises = [];

				let usedEffort = 0;
				loopCellDates(cell, function (y, m, d) {
					let dayAllocations = AllocationService.getCalendarDataByDate(y, m, d);
					let itemId = DI.data.item_id;

					if (dayAllocations[itemId] && dayAllocations[itemId][80]) {
						let projectHours = dayAllocations[itemId][80];
						cell.max += projectHours;
						cell.total += projectHours;

						angular.forEach(DI.data.selectedEffortItems, function (effortItem, effortItemId) {
							promises.push(CalendarService
								.getUserCalendar(effortItemId, cell.startDate, cell.endDate)
								.then(function (userCalendar) {
									let totalAllocations = 0;

									angular.forEach(effortItem.allocations, function (allocationId) {
										let calendarData = dayAllocations[allocationId];

										angular.forEach(calendarData, function (hours, type) {
											if (type == 50 || type == 53 || type == 55) {
												totalAllocations += hours;
											}
										});
									});

									let userDay = userCalendar[y][m][d];
									let workHoursLeft = userDay.workHours - totalAllocations;

									if (!userDay.notWork && workHoursLeft > 0) {
										if (workHoursLeft > projectHours) {
											usedEffort += projectHours;
										} else {
											usedEffort += workHoursLeft;
										}
									}
								}));
						});
					}
				});

				$q.all(promises).then(function () {
					cell.isLoading = false;

					let effortLeft = cell.max - usedEffort;
					cell.calculations[80] = effortLeft < 0 ? 0 : effortLeft;

					if (cell.isCleared) {
						cellPool[cell.type].push(cell);
						return;
					}

					if (cell.total) {
						cell.draw(cell.position.left, cell.position.width);
					}
				});

				return cell;
			};

			return cell;
		}

		function maxChildrenCell() {
			let cell = cellTemplate();
			cell.type = 'maxChildren';
			cell.calculate = function () {
				cell.max = 0;
				let DI = cell.owner;

				let runner = 0;
				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d);

					let thisDay = new Date(y, m - 1, d);
					if (thisDay.getDay() != 0 && thisDay.getDay() != 6) {
						runner++;
					}

					inheritChildrenHours(cell, DI, calendarData);
				});

				if (cell.total == 0) {
					return false;
				}

				angular.forEach(cell.calculations, function (value) {
					cell.max += value;
				});
				cell.$$capacity = angular.copy(cell.max);
				if (runner == 0) runner = 1;
				//PROJECT VIEW MAIN ROW (projects) or competency, adding up all children rows
				if (Common.rounder(cell.total, 2) > Common.rounder(cell.max, 2)) {
					cell.overflow = true;
				}

				let hasFte = AllocationService.getModeName().includes("_fte");

				//FTE MODE
				if (useFte && hasFte) {
					cell.total = cell.total / (runner * defHours);
					cell.max = 1;
				}

				//DAY MODE
				else if (useFte && !hasFte) {
					cell.max = cell.max / defHours;
					cell.total = cell.total / defHours
				}

				cell.hideTooltip = true;
				return cell;
			};

			return cell;
		}

		function maxChildrenUserCell() {
			let cell = cellTemplate();
			cell.type = 'maxChildren';
			cell.calculate = function () {
				cell.max = 0;
				let DI = cell.owner;

				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d);
					inheritChildrenHours(cell, DI, calendarData);
				});

				if (cell.total == 0) return false;
				if (DI.parent.data.item_id) {
					cell.isLoading = true;
					let runner = 0;
					CalendarService
						.getUserCalendar(DI.parent.data.item_id, cell.startDate, cell.endDate)
						.then(function (userCalendar) {
							CalendarService.getUsersBaseCalendar(DI.parent.data.item_id).then(function (m) {
								defHours = m.WorkHours;

							if (cell.isCleared) {
								cellPool[cell.type].push(cell);
								return;
							}

							cell.isLoading = false;
							loopCellDates(cell, function (y, m, d) {
								let userDay = userCalendar[y][m][d];
								let workHours = userDay.notWork ? 0 : userDay.workHours;
								cell.max += workHours;

								let thisDay = new Date(y, m - 1, d);
								if (thisDay.getDay() != 0 && thisDay.getDay() != 6) {
									runner++;
								}
							});

							//RESOURCE VIEW(?) PROJECTS UNDER USERS
							let hasFte = AllocationService.getModeName().includes("_fte");
							cell.$$capacity = angular.copy(cell.max);
							//FTE MODE
							if (useFte && hasFte) {
								//Check inheritChildrenCell for logic explanation
								cell.max = 1;
								cell.total = cell.total / (defHours * runner)
							}

							//DAY MODE
							else if (useFte && !hasFte) {
								cell.max = cell.max / CalendarService.getUserWorkHours(DI.parent.data.item_id).workHours;
								cell.total = cell.total / CalendarService.getUserWorkHours(DI.parent.data.item_id).workHours;
							}

							cell.hideTooltip = true;
							cell.draw(cell.position.left, cell.position.width);
						});
						})
					return cell;
				} else {
					cell.total = cell.total / divider;
					return cell;
				}
			};
			return cell;
		}

		function allocationUserCell() {
			let cell = cellTemplate();
			cell.type = 'allocation';
			cell.calculate = function () {
				cell.max = 0;
				let DI = cell.owner;

				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d)[DI.data.item_id];

					angular.forEach(calendarData, function (hours, type) {
						addHours(cell, type, hours);
					});
				});

				if (cell.total == 0) {
					return false;
				}

				cell.isLoading = true;
				CalendarService
					.getUserCalendar(DI.data.resource_item_id, cell.startDate, cell.endDate)
					.then(function (userCalendar) {
						cell.isLoading = false;
						CalendarService.getUsersBaseCalendar(DI.data.resource_item_id).then(function (m) {
							defHours = m.WorkHours;

						if (cell.isCleared) {
							cellPool[cell.type].push(cell);
							return;
						}

						let runner = 0;
						loopCellDates(cell, function (y, m, d) {
							let calendarData = AllocationService.getCalendarDataByDate(y, m, d)[DI.data.item_id];
							let allocatedHours = 0;
							angular.forEach(calendarData, function (hours) {
								allocatedHours += hours;
							});

							let userDay = userCalendar[y][m][d];
							let workHours = userDay.notWork ? 0 : userDay.workHours;

							if (allocatedHours > workHours) {
								cell.warn = true;
							}

							let thisDay = new Date(y, m - 1, d);
							if (thisDay.getDay() != 0 && thisDay.getDay() != 6) {
								runner++;
							}
							cell.max += workHours;
						});

						cell.$$capacity = angular.copy(cell.max);
						//Allocation cell under project (user view?)
						let hasFte = AllocationService.getModeName().includes("_fte");
						if (runner == 0) runner = 1;
						//FTE MODE
						if (useFte && hasFte) {
							//Check inheritChildrenCell for logic explanation
							cell.total = cell.total / (runner * defHours);
							cell.max = runner * defHours != 0 ? cell.max / (runner * defHours) : 1;
						} else if (useFte && !hasFte) {
							cell.max = cell.max / CalendarService.getUserWorkHours(DI.data.resource_item_id).workHours; //DAY MODE
							cell.total = cell.total / CalendarService.getUserWorkHours(DI.data.resource_item_id).workHours;
						}

						if (Common.rounder(cell.total, 2) > Common.rounder(cell.max, 2)) {
							cell.overflow = true;
						}

						cell.hideTooltip = true;
						cell.draw(cell.position.left, cell.position.width);
					});
					});

				return cell;
			};

			return cell;
		}

		function allocationCell() {
			let cell = cellTemplate();
			cell.type = 'allocation';
			cell.calculate = function () {
				cell.max = 0;
				let DI = cell.owner;

				cell.hideTooltip = true;
				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d)[DI.data.item_id];

					angular.forEach(calendarData, function (hours, type) {
						addHours(cell, type, hours);
					});
				});

				if (cell.total == 0) {
					return false;
				}

				if (DI.parent.data.process === 'user') {
					cell.isLoading = true;
					CalendarService
						.getUserCalendar(DI.data.resource_item_id, cell.startDate, cell.endDate)
						.then(function (userCalendar) {

							CalendarService.getUsersBaseCalendar(DI.data.resource_item_id).then(function (m) {
								defHours = m.WorkHours;

							cell.isLoading = false;

							if (cell.isCleared) {
								cellPool[cell.type].push(cell);
								return;
							}

							let runner = 0;
							loopCellDates(cell, function (y, m, d) {
								let calendarData = AllocationService.getCalendarDataByDate(y, m, d)[DI.data.item_id];
								let allocatedHours = 0;
								angular.forEach(calendarData, function (hours) {
									allocatedHours += hours;
								});

								let userDay = userCalendar[y][m][d];
								let workHours = userDay.notWork ? 0 : userDay.workHours;

								if (allocatedHours > workHours) {
									cell.warn = true;
								}

								let thisDay = new Date(y, m - 1, d);

								if (thisDay.getDay() != 0 && thisDay.getDay() != 6) {
									runner++;
								}


								cell.max += workHours;
							});
							cell.$$capacity = angular.copy(cell.max);
							//allocation cell under user in project view
							let hasFte = AllocationService.getModeName().includes("_fte");
							let resourceItemId = DI.data.resource_item_id;
							if (runner == 0) runner = 1;

							//sometimes resource item id is in id1_id2 form
							if (typeof DI.data.resource_item_id === 'string' && DI.data.resource_item_id.includes("_")) {
								resourceItemId = DI.data.resource_item_id.split("_")[1];
							}

							if (useFte && hasFte) {
								cell.max = runner * defHours != 0 ? cell.max / (runner * defHours) : 1; //FTE MODE
								cell.total = cell.total / (runner * defHours);
							} else if (useFte && !hasFte) {
								cell.max = cell.max / CalendarService.getUserWorkHours(resourceItemId).workHours; //DAY MODE
								cell.total = cell.total / CalendarService.getUserWorkHours(resourceItemId).workHours;
							}

							if (Common.rounder(cell.total, 2) > Common.rounder(cell.max, 2)) {
								cell.overflow = true;
							}

							cell.draw(cell.position.left, cell.position.width);
						});
						});
				} else {
					let runner = 0;
					loopCellDates(cell, function (y, m, d) {
						let thisDay = new Date(y, m - 1, d);
						if (thisDay.getDay() != 0 && thisDay.getDay() != 6) {
							runner++;
						}
					});
					let hasFte = AllocationService.getModeName().includes("_fte");
					if (useFte && hasFte) {
						cell.total = cell.total / (runner * defHours);
					} else if (useFte && !hasFte) {
						cell.total = cell.total / defHours;
					}
					cell.max = cell.total / runner;
					cell.$$capacity = angular.copy(cell.max);
				}
				return cell;
			};

			return cell;
		}

		function inheritChildrenCell() {
			let cell = cellTemplate();
			cell.type = 'inheritChildren';
			cell.calculate = function () {
				cell.max = 0;
				let DI = cell.owner;

				loopCellDates(cell, function (y, m, d) {
					let calendarData = AllocationService.getCalendarDataByDate(y, m, d);
					inheritChildrenHours(cell, DI, calendarData);
				});

				if (cell.total == 0 && DI.data.process != 'user') {
					return false;
				}

				if (DI.data.process === 'user') {
					cell.isLoading = true;
					let itemId = cell.owner.cell.useActualId ? DI.data.actual_item_id : DI.data.item_id;
					CalendarService.getUsersBaseCalendar(itemId).then(function (m) {
						CalendarService.getUserCalendar(itemId, cell.startDate, cell.endDate).then(function (userCalendar) {
							CalendarService.getBaseCalendar(m.CalendarId, cell.startDate, cell.endDate).then(function (baseCalendar) {
								defHours = m.WorkHours;
								cell.isLoading = false;

								if (cell.isCleared) {
									cellPool[cell.type].push(cell);
									return;
								}

								let hasHoliday = false;
								let runner = 0;
								loopCellDates(cell, function (y, m, d) {
									let calendarData = AllocationService.getCalendarDataByDate(y, m, d);
									let userDay = userCalendar[y][m][d];
									let calendarDay = baseCalendar[y][m][d];
									let workHours = userDay.notWork ? 0 : userDay.workHours;

									let dailyTotal = getChildrenHours(cell, DI, calendarData, workHours);
									if (dailyTotal > workHours) {
										cell.warn = true;
									}

									//Obsolete or maybe not
									/*	if(userDay.holiday == true && (thisDay.getDay() != 0 && thisDay.getDay() != 6)){
											hasHoliday = true
										}*/

									if (userDay.holiday == true && !!userDay.notWork) {
										hasHoliday = true
									}

									if (calendarDay.dayOff != true) runner++;
									cell.max += workHours;
								});
								if (cell.total > 0) hasHoliday = false;
								if (cell.startDate.getTime() == cell.endDate.getTime()) {
									if (cell.startDate.getDay() == 0 || cell.startDate.getDay() == 6) {
										hasHoliday = false;
									}
								}
								if (cell.total == 0 && !hasHoliday) return false;
								if (_.isNaN(cell.total)) return;

								let hasFte = AllocationService.getModeName().includes("_fte");

								//USER MAIN ROW

								if(runner == 0) runner = 1;
								cell.$$capacity = angular.copy(cell.max);
								//FTE MODE
								if (useFte && hasFte) {
									//In fte mode, runner is how many working days user's base calendar has between two dates
									//defHours is working hours per day for user's base calendar
									//max == tooltip
									cell.max = runner * defHours != 0 ? cell.max / (runner * defHours) : 1;
									cell.total = cell.total / (runner * defHours);
								} else if (useFte && !hasFte) {
									cell.total = cell.total / CalendarService.getUserWorkHours(itemId).workHours
									cell.max = cell.max / CalendarService.getUserWorkHours(itemId).workHours; //DAY MODE
								}

								if (hasHoliday && cell.total == 0 && cell.startDate.getTime() == cell.endDate.getTime()) {
									//Show holiday on day mode
									cell.holidayOverflow = true;
								} else if (hasHoliday && cell.total == 0) {
									//Do not show holidays on week / month / etc mode
									return false;
								} else if (Common.rounder(cell.total, 2) > Common.rounder(cell.max, 2)) {
									cell.overflow = true;
									cell.holidayOverflow = false;
									cell.$$capacity = cell.total;
								} else {
									cell.holidayOverflow = false;
								}

								cell.draw(cell.position.left, cell.position.width);
							});
						});
					});
				} else {
					cell.max = cell.total / divider;
				}
				return cell;
			};

			return cell;
		}

		function getChildrenHours(cell, DI, calendarData, dailyAllowance) {
			let totalHours = 0;
			let runner = DI.firstEle;
			while (runner) {
				if (runner.firstEle && typeof inheritChildrenWarnings === "function") {
					totalHours += inheritChildrenWarnings(cell, runner, calendarData, dailyAllowance);
				}

				angular.forEach(calendarData[runner.data.item_id], function (hours, type) {
					if (!cell.owner.cell.ignore(runner.data.item_id, type)) {
						totalHours += hours;
					}
				});

				runner = runner.next;
			}

			return totalHours;
		}

		function loopCellDates(cell, callback) {
			let dateRunner = moment.utc(cell.startDate);
			while (dateRunner.isSameOrBefore(cell.endDate)) {
				callback(dateRunner.year(), dateRunner.month() + 1, dateRunner.date());
				dateRunner.add(1, 'days');
			}
		}

		function addHours(cell, type, hours) {
			if (typeof cell.calculations[type] === 'undefined') {
				cell.calculations[type] = hours;
			} else {
				cell.calculations[type] += hours;
			}

			cell.total += hours;
		}

		function inheritChildrenHours(cell, DI, calendarData) {
			let runner = DI.firstEle;
			while (runner) {
				if (runner.firstEle) inheritChildrenHours(cell, runner, calendarData);

				angular.forEach(calendarData[runner.data.item_id], function (hours, type) {
					if (!cell.owner.cell.ignore(runner.data.item_id, type)) {
						if (DI.RM.options.disregardOtherAllocations) {
							if (!(runner.data.item_id.toString().indexOf('_00') > -1))
								addHours(cell, type, hours);
						} else {
							addHours(cell, type, hours);
						}
					}
				});

				runner = runner.next;
			}
		}

		function showTotal(cell, value) {
			cell.$total.html(value).appendTo(cell.$cell);
		}

		function showHolidayTotal(cell, value) {
			cell.$holidayTotal.html(value).appendTo(cell.$cell);
		}


		function showOverflow(cell, force) {
			if (!cell.overflowed || force) {
				cell.$cell.attr('class', 'cell overflow');
				cell.$overflow.appendTo(cell.$cell);
				cell.overflowed = true;

				resetWarning(cell);
			}
		}

		function showHolidayOverflow(cell, force) {
			if (!cell.overflowed || force) {
				cell.$cell.attr('class', 'cell overflow');
				cell.$holidayOverflow.appendTo(cell.$cell);
				cell.overflowed = true;

				resetWarning(cell);
			}
		}

		function showWarning(cell, force) {
			if (!cell.warned || force) {
				cell.$cell.attr('class', 'cell warning');
				cell.$warning.appendTo(cell.$cell);
				cell.warned = true;

				resetOverflow(cell);
			}
		}

		function hideOverflowWarning(cell, force) {
			if (cell.overflowed || cell.warned || force) {
				cell.$cell.attr('class', 'cell');

				resetOverflow(cell);
				resetWarning(cell);
			}
		}

		function resetWarning(cell) {
			if (cell.warned) {
				cell.$warning.remove();
			}

			cell.warned = false;
			cell.warn = false;
		}

		function resetOverflow(cell) {
			if (cell.overflowed) {
				cell.$overflow.remove();
			}

			cell.overflowed = false;
			cell.overflow = false;
		}

		function showCell(cell, left, width) {
			cell.$cell.css({left: left, width: width});
			cell.$cell.appendTo(cell.owner.calendarRow.$td).css('display', 'block');
		}

		function cellTemplate() {
			let id = UniqueService.get('allocation-cell');
			let cell = {
				id: id,
				position: {
					left: 0,
					width: 0
				},
				$cell: $('<div class="cell" cellid="' + id + '">'),
				calculations: {},
				overflow: false,
				overflowed: false,
				warn: false,
				warned: false,
				holidayOverflow: false,
				max: 7.5,
				total: 0,
				type: 'default',
				isLoading: false,
				isCleared: false,
				freeYourSelf: function () {
					cell.$cell.remove();
					cell.isCleared = true;

					if (!cell.isLoading) {
						cellPool[cell.type].push(cell);
					}
				},
				clear: function (DI, startDate, endDate) {
					cell.$cell.empty();
					cell.$cell.attr('class', 'cell');
					cell.calculations = {};
					cell.overflow = false;
					cell.overflowed = false;
					cell.warn = false;
					cell.warned = false;
					cell.max = 7.5;
					cell.total = 0;
					cell.owner = DI;
					cell.startDate = new Date(startDate);
					cell.endDate = new Date(endDate);
					cell.isLoading = false;
					cell.isCleared = false;
				},
				drawTotal: function () {
					let ninetiesResolution = $(window).width() < 1920;
					let addHChar = divider == 1 ? ' h' : '';
					let decimals = useFte ? 2 : 1;
					let value = Common.rounder(cell.total, 2) + (!ninetiesResolution ? addHChar : '');
					if (ninetiesResolution && cell.total < 1) value = value.replace("0.", ".");
					showTotal(cell, value);
				},
				drawHolidayTotal: function () {
					showHolidayTotal(cell, '');
				},
				getBackground: function (type) {
					return AllocationService.getCellColour('allocation', type);
				},
				getHtml: function () {
					let html = '';
					let bottomPx = 0;

					for (let i = 0, l = orderTop.length; i < l; i++) {
						let type = orderTop[i];

						if (typeof cell.calculations[type] === 'undefined') {
							continue;
						}
						let cellHeight = calculateCellPixelHeight(cell.calculations[type], cell.$$capacity);
						if (cellHeight >= 1) {
							html += '<div class="status-' + type + ' ' + cell.getBackground(type) + '" style="height: ' + cellHeight + 'px"></div>';
						}
					}

					for (let i = 0, l = orderBottom.length; i < l; i++) {
						let type = orderBottom[i];

						if (typeof cell.calculations[type] === 'undefined') {
							continue;
						}

						let cellHeight = calculateCellPixelHeight(cell.calculations[type], cell.$$capacity);
						if (cellHeight >= 1) {
							html += '<div class="bottomBar status-' + type + ' ' + cell.getBackground(type) + '" style="height: ' + cellHeight + 'px; bottom:' + bottomPx + 'px;"></div>';
						}

						bottomPx += cellHeight;
					}

					return html;
				},
				draw: function (left, width, reDraw) {
					cell.position.left = left;
					cell.position.width = width;

					if (cell.isLoading) return;

					let decimals = useFte ? 2 : 1;

					if (cell.hideTooltip != true) cell.$cell.attr("title", Translator.translate("ALLOCATION_CAPACITY") + " " + Common.formatNumber(cell.max, decimals));

					cell.$cell.html(cell.getHtml());
					if (!cell.holidayOverflow) cell.drawTotal();
					else cell.drawHolidayTotal();

					if (cell.overflow) {
						showOverflow(cell, reDraw);
					} else if (cell.warn) {
						showWarning(cell, reDraw);
					} else if (cell.holidayOverflow) {
						showHolidayOverflow(cell, reDraw);
					} else {
						hideOverflowWarning(cell, reDraw);
					}

					if (reDraw !== true) {
						showCell(cell, left, width);
					}

					return cell;
				},
				calculate: function () {
					return cell;
				}
			};
			cell.$total = $('<div class="allocation-total">');
			cell.$holidayTotal = $('<div class="allocation-holiday-total">');
			cell.$overflow = $('<div class="allocation-overflow"><i class="material-icons" aria-hidden="true">error</i></div>');
			cell.$holidayOverflow = $('<div class="allocation-holiday-overflow"></div>');
			cell.$warning = $('<div class="allocation-warning"><i class="material-icons" aria-hidden="true">error</i></div>');
			self.cells[cell.id] = cell;

			return cell;
		}
	}
})();