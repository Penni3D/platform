﻿(function () {
	'use strict';

	angular
		.module('core.allocation')
		.directive('allocationManager', allocationManager);

	allocationManager.$inject = [
		'$q',
		'RMService',
		"RMDIService",
		'RMSortService',
		'$timeout',
		'AllocationService',
		'MessageService',
		'ProcessService',
		'$compile',
		'PortfolioService',
		'Translator',
		'Sanitizer',
		'CellFactoryService',
		'InputService',
		'ViewService',
		'Common',
		'CalendarService',
		'HTMLService'
	];

	function allocationManager(
		$q,
		RMService,
		RMDIService,
		RMSortService,
		$timeout,
		AllocationService,
		MessageService,
		ProcessService,
		$compile,
		PortfolioService,
		Translator,
		Sanitizer,
		CellFactoryService,
		InputService,
		ViewService,
		Common,
		CalendarService,
		HTMLService
	) {
		return {
			scope: {
				rules: '<',
				portfolioColumns: '<',
				options: '<',
				rows: '=',
				rights: '<',
				portfolioId: '@',
				ownercc: '<',
				orderCol: '@',
				orderDir: '@',
			},
			template: '<table class="rowmanager-body allocation-manager"></table>',
			transclude: true,
			link: function (scope, element, attr, controller, transclude) {
				transclude(scope, function (clone) {
					$timeout(function () {
						let regex = /(<\/*table(.|\n)*?>|<\/*tbody(.|\n)*?>|<!--(.|\n)*?-->)+/gi;

						angular.forEach(clone, function (template) {
							let type = template.getAttribute('type');
							let str = template.innerHTML.replace(regex, ' ');

							switch (type) {
								case 'header':
									let template = str.split(/[\[\]]/g);
									let html = '';
									let cc = template.length - 1;

									for (let i = 0; i < cc; i += 2) {
										let name = template[i + 1];
										let pc = ' data-column';

										html += template[i] + '<div class="' + RMService.sanitize(controller.RM.getFieldCls(name)) + pc +
											'" tooltip="' + RMService.sanitize(controller.RM.getFieldTip(name)) + '">' + RMService.sanitize(controller.RM.getFieldTitle(name)) + '</div>';
									}

									html += template[cc];

									if (controller.RM.options.showCalendar) {
										html = html.replace(/<\/tr>/g, '<th style="position: relative; width: 50%"></th></tr>');
									}

									controller.RM.$headerEle = $(html);
									controller.RM.$fHeaderEle = $('<table class="rowmanager-floating-header">' + html + '</table>').prependTo(element);

									for (let i = 0; i < cc; i += 2) {
										let name = template[i + 1];
										let $ele = controller.RM.$headerEle.find('div.' + name).parent();
										let $hEle = controller.RM.$fHeaderEle.find('div.' + name).parent();
										controller.RM.setFieldWidth(name, $ele, $hEle);
									}

									controller.RM.$headerEle.appendTo(element.find('.rowmanager-body'));
									break;
								default:
									controller.RM.templates[type] = str.split(/[\[\]]/g);
							}
						});

						controller.initializeRowManager();
					});
				});
			},
			controller: function ($scope, $element) {
				$scope.showAllocation = true;
				$scope.managerOfCostCenters = new Array() as Array<boolean>;
				angular.forEach($scope.ownercc, function (costCenter) {
					$scope.managerOfCostCenters.push(costCenter.item_id);
				});

				let RM = new RMService.managerTemplate();

				RM.options = $.extend({
					showCalendar: true,
					menuItems: {},
					alias: {},
					buttons: {
						openRow: function (id) {
							let DI = RMService.itemSet[id];
							AllocationService.setOpenClose(id, !DI.isOpen)
							if (DI.isOpen) {
								DI.close();
							} else {
								DI.open();
							}
							RM.setCalendarHeight();
						}
					},
					rowDblClick: onEmptyDoubleClick,
					inheritDates: false,
					sumDatas: false,
					showUserName: false,
					ownerId: 0,
					ownerType: 'allocation',
					projectId: 0,
					taskId: 0,
					allowProjectJump: false,
					allowTaskJump: false,
					keepOrder: false,
					hasDragRights: false,
					hasRequestRights: false,
					hasSendRights: false,
					hasWriteRights: false,
					hasManagerRights: false,
					hasOwnerRights: false,
					hasResetRights: false,
					multipleProjects: false,
					multipleResources: false,
					selectable: false,
					openRows: [],
					highlightRows: [],
					autoAccept: 0,
					disableAutoOpen: false
				}, $scope.options);

				let $body = $element.find('.rowmanager-body');
				let useActualItemId = false;

				if (RM.options.multipleProjects) {
					useActualItemId = true;
				}

				RM.rules = $scope.rules;
				RM.$ele = $element;

				let rootParent = {
					getCurrentIndex: function () {
						return 0;
					},
					isRoot: true,
					firstEle: undefined,
					ownerId: RM.options.ownerId,
					ownerType: RM.options.ownerType,
					data: {
						owner_item_id: RM.options.ownerId,
						owner_item_type: RM.options.ownerType,
						item_id: RM.options.ownerId
					}
				};

				let controller = this;
				RM.setValues(rootParent, $scope.rows);

				let visibleColumns = ['primary', 'competency', 'organisation', 'cost_center', 'status_translation', 'process_translation', 'allocation_total'];
				if ($scope.options.visibleColumns) {
					visibleColumns = $scope.options.visibleColumns;
				}

				let tdCount = 1;
				controller.RM = RM;
				RM.$emptyEle = $();
				RM.$headerEle = $();
				RM.$columnEle = undefined;
				RM.templates = {};
				RM.$parent = $scope.$parent;

				RM.createFieldsCache($scope.portfolioColumns);

				RMSortService.addMasterClass('allocation-manager');

				function addRows(rows, initialize?) {

					let openRows = [];
					let parentRows = [];
					let childRows = [];

					if (!RM.options.keepOrder) {
						if (!$scope.orderCol) $scope.orderCol = 'primary';
						if (!$scope.orderDir) $scope.orderDir = 'asc';

						rows.sort(function (a, b) {
							if ($scope.orderCol === 'primary') {
								if (a.status == 53) {
									return 1;
								} else if (b.status == 53) {
									return 0;
								}

								if (a.status == 57) {
									return 1;
								} else if (b.status == 57) {
									return 0;
								}
							}

							if ($scope.orderDir == "asc") {
								return a[$scope.orderCol] < b[$scope.orderCol] ? -1 : 1;
							} else {
								return a[$scope.orderCol] > b[$scope.orderCol] ? -1 : 1;
							}
						});

						// let highlightRows = _.remove(rows, function (r:any) {
						// 	return RM.options.highlightRows.indexOf(r.item_id) !== -1;
						// });
						//
						// for (let i = 0, l = highlightRows.length; i < l; i++) {
						// 	rows.unshift(highlightRows[i]);
						// }
					}


					for (let i = 0, ll = rows.length; i < ll; i++) {
						let DI = RMService.itemSet[rows[i].item_id];


						if (isVisibleDI(rootParent, rows[i].item_id)) {
							DI.highlightLong();
						} else {
							if (DI) {
								DI.delete();
							}

							DI = new RMDIService.baseDI();
							DI.data = rows[i];
							let itemId = DI.data.item_id;
							RMService.itemSet[itemId] = DI;
							initRow(DI);
							if (rootParent.data.item_id == DI.data.parent_item_id) {
								parentRows.push(itemId);
							} else {
								childRows.push(itemId);
							}

							if (DI.data.task_type !== 1) {
								DI.data.start_date = undefined;
								DI.data.end_date = undefined;
							}

							if (false && DI.data.task_type == 52 && !RM.options.disableAutoOpen) {
								openRows.push(DI);
								DI.wasOpen = true;
							} else {
								let j = RM.options.openRows.length;
								while (j--) {
									if (RM.options.openRows[j] == itemId) {
										openRows.push(DI);
										DI.wasOpen = true;
										break;
									}
								}
							}
						}
					}

					let lastId = $('.allocationFilters').parent().prev('.rmTarget').attr('rowid');
					let lastRow = RMService.itemSet[lastId];

					for (let i = 0, l = parentRows.length; i < l; i++) {
						let DI = RMService.itemSet[parentRows[i]];
						if (typeof lastRow === 'undefined') {
							DI.append();
							DI.calendarRow.append();
						} else {
							DI.after(lastRow);
							DI.calendarRow.after(lastRow);
						}

						DI.setPadding();
						DI.inParent(rootParent).show();
						DI.initVisibility();
						lastRow = DI;
					}

					for (let i = 0, l = childRows.length; i < l; i++) {
						let DI = RMService.itemSet[childRows[i]];

						if (typeof RM.templates[DI.data.task_type] === 'undefined' &&
							typeof RM.templates['default'] === 'undefined'
						) {
							continue;
						}

						let parent = RMService.itemSet[DI.data.parent_item_id];
						DI.inParent(parent);

						if (DI.data.task_type == 50 && !parent.isRoot) parent.setDataFromChildren();
					}
					for (let i = 0, l = parentRows.length; i < l; i++) {
						let DI = RMService.itemSet[parentRows[i]];
						DI.setDataFromChildren();
					}

					for (let i = 0, l = openRows.length; i < l; i++) {
						openRows[i].setPadding();
						openRows[i].open();
					}

					function openParents(item) {
						if (item.open) {
							item.open();
						}

						if (item.parent) {
							openParents(item.parent);
						}
					}

					for (var i = 0; i < RM.options.highlightRows.length; i++) {
						var item = RMService.itemSet[RM.options.highlightRows[i]];
						openParents(item);
					}

					if (RM.options.taskId) {
						addFilterArea();
					}

					if (!initialize) {
						setTimeout(function () {
							RM.calendarReDraw();
						}, 75);
					}
					setOpenCloseRows();
				}

				let lowerRows = [];
				let filterAreaExist = false;

				function addFilterArea() {
					if (!filterAreaExist) {
						filterAreaExist = true;

						let $filterArea = RM.addEmptyArea($body, 'allocationFilters');

						$scope.getRows = function (empty) {
							if ($scope.allowFilter) {
								AllocationService.getUsersForTask(
									RM.options.taskId,
									RM.options.projectId,
									$scope.portfolioId,
									empty
								).then(function (users) {
									addLowerRows(users.rowdata);
								});
							} else {
								AllocationService.getAllocatedUsersForTask(
									RM.options.taskId,
									RM.options.projectId
								).then(function (users) {
									addLowerRows(users.rowdata);
								})
							}
						};

						let promises = [];

						promises.push(PortfolioService.getPortfolioColumns('user', $scope.portfolioId));
						promises.push(ProcessService.getSublists('user'));

						$q.all(promises).then(function (resolves) {
							$scope.portfolioColumns = resolves[0];
							$scope.showFilters = false;
							$scope.allowFilter = false;

							let $allocationFilters = RM.$ele.find('.allocationFilters');
							$scope.toggleFilters = function () {
								$scope.showFilters = !$scope.showFilters;

								if ($scope.allowFilter && $scope.showFilters) {
									$allocationFilters.css('height', 256);
								} else {
									$allocationFilters.css('height', 48);
								}


								setTimeout(function () {
									RM.setCalendarHeight();
								}, 75);
							};

							$scope.toggleFilters();
							$scope.getRows(true);

							let filterHtml =
								'<div>{{::"ALLOCATION_ASSIGN_USERS" | translate}}' +
								'<span class="flex"></span>' +
								'<input-button type="icon" ng-if="::!allowFilter" ng-click="getRows(true)"><i class="material-icons white" aria-hidden="true">refresh</i></input-button>' +
								'<input-button type="icon" ng-if="::allowFilter" ng-click="toggleFilters()" ng-class="{ flip: showFilters }"><i class="material-icons white" aria-hidden="true">keyboard_arrow_up</i></input-button>' +
								'</div>' +
								'<portfolio-filters ng-if="::allowFilter" portfolio-columns="portfolioColumns" ' +
								'is-visible="showFilters" portfolio-id="{{::portfolioId}}" ' +
								'on-change="getRows">' +
								'</portfolio-filters>';

							$filterArea.html(filterHtml);
							$compile($filterArea)($scope);
						});
					}
				}

				function addLowerRows(rows) {
					let i = lowerRows.length;
					while (i--) {
						lowerRows[i].delete();
					}

					lowerRows.length = 0;

					for (let i = 0, l = rows.length; i < l; i++) {
						if (!isVisibleDI(rootParent, rows[i].item_id)) {
							let DI = RMService.itemSet[rows[i].item_id];

							if (DI) {
								DI.delete();
							}

							DI = new RMDIService.baseDI();
							DI.data = rows[i];
							RMService.itemSet[DI.data.item_id] = DI;
							initRow(DI);

							DI.cell.type = 'taskUser';
							DI.cell.mode = 'calculateOa';

							DI.append().inParent(rootParent).setPadding().show();
							DI.calendarRow.append();
							DI.initVisibility();
							lowerRows.push(DI);
						}
					}

					RM.calendarReDraw();
				}

				function isVisibleDI(DI, itemId) {
					let runner = DI;
					while (runner) {
						if (runner.data.item_id == itemId) {
							return true;
						} else if (runner.firstEle && isVisibleDI(runner.firstEle, itemId)) {
							return true
						}

						runner = runner.next;
					}
				}

				let requestNewAllocation = function (itemId, process?) {
					let f = isVisibleDI(rootParent, itemId);
					if (f) {
						addNewItem(50, RMService.itemSet[itemId]);
					} else {
						ProcessService.getItem(process, itemId).then(function (user) {
							let DI = new RMDIService.baseDI();
							DI.data = user;
							DI.data.task_type = 51;

							let itemId = DI.data.item_id;
							RMService.itemSet[itemId] = DI;

							initRow(DI);
							DI.inParent(RMService.itemSet[RM.options.projectId]);
							DI.parent.open();
							DI.setPadding(DI.getCurrentIndex());

							addNewItem(50, DI).then(function () {
								if (DI.lastEle && DI.lastEle.data.status != 53) {
									let parent = DI;
									AllocationService.getOtherUserAllocations(RM.options.ownerId, parent.data.item_id)
										.then(function (otherAllocation) {
											let DI = new RMDIService.baseDI();
											DI.data = otherAllocation;
											DI.data.parent_item_id = parent.data.item_id;
											let itemId = DI.data.item_id;
											RMService.itemSet[itemId] = DI;
											initRow(DI);

											DI.inParent(parent);
											RM.addRow(DI.data);
											RM.calendarReDraw();
											DI.setPadding(DI.getCurrentIndex());
											DI.initVisibility();
										});
								}
							});
						});
					}
				};


				function addNewItem(type, parent) {
					let id = useActualItemId ? parent.data.actual_item_id : parent.data.item_id;
					let projectId = RM.options.multipleProjects ? parent.parent.data.item_id : RM.options.projectId;
					let params = {
						status: undefined
					};

					if (RM.options.autoAccept) {
						params.status = 55;
					}

					return AllocationService.createAllocation(id, projectId, params).then(function (allocation) {
						let DI = new RMDIService.baseDI();
						DI.data = allocation;
						DI.data.parent_item_id = parent.data.item_id;
						DI.data.task_type = type;

						let itemId = DI.data.item_id;
						let sd = RM.calendarAreaStart(CalendarService.getUTCDate());
						let ed = RM.calendarNextDate(sd);
						ed.setUTCDate(ed.getUTCDate() - 1);

						RMService.itemSet[itemId] = DI;
						initRow(DI);

						if (parent.lastEle && parent.lastEle.data.status == 53) {
							DI.linkBefore(parent.lastEle);
						} else {
							DI.inParent(parent);
						}

						RM.addRow(DI.data);
						RM.calendarReDraw();
						DI.setPadding(DI.getCurrentIndex());
						DI.initVisibility();
						parent.open();
						onEmptyDoubleClick(itemId, sd, ed);
					});
				}

				if (RM.options.taskId) {
					requestNewAllocation = function (itemId, process) {
						let f = isVisibleDI(rootParent, itemId);
						if (f) {
							let DI = RMService.itemSet[itemId];
							DI.open();
							DI.highlightLong();
						} else {
							AllocationService.getAssignedTasks(RM.options.taskId, process, itemId)
								.then(function (allocations) {
									addRows(allocations.rowdata);
								});
						}
					};
				}

				let checkIfEmpty = function () {
				};

				RM.selectedRows = {};
				let unSelectRows = function () {
					$.each(RM.selectedRows, function (i, DI) {
						if (DI) {
							DI.isSelected = false;
							DI.$ele.removeClass('selected');
						}
					});

					RM.selectedRows = {};
				};

				RM.hasSelectedRows = function () {
					$.each(RM.selectedRows, function (i, DI) {
						if (DI) {
							return true;
						}
					});

					return false;
				};

				function initRow(DI) {
					DI.RM = RM;
					DI.cell = {
						ignore: function (itemId, type) {
							return type == 30 || type == 70;
						}
					};

					/*  
						MAYBE TRUE (TZh on 26.3.2019):
						50 = Allocation
						51 = User or Competency
						52 = Project
						53 = Advanced Filter: User
						54 = Advanced Filter: Task
					*/


					if (!RM.options.taskId) {
						if (DI.data.task_type == 50) {
							DI.cell.type = 'allocation';
						} else if (DI.data.task_type == 51) {
							if (DI.data.process == 'competency') {
								DI.cell.type = 'maxChildren';
							} else {
								DI.cell.type = 'inheritChildren';
							}
						} else if (DI.data.task_type == 52) {
							DI.cell.type = 'maxChildren';

							DI.cell.ignore = function (itemId, type) {
								if (type == 30) return true;
								if (RMService.itemSet[itemId].data.status == 53) return true;
							};
						} else if (DI.data.task_type == 53) {
							DI.cell.type = 'itemFilter';
						} else if (DI.data.task_type == 54) {
							DI.cell.type = 'projectFilter';
						}
					} else {
						if (DI.data.task_type == 50) {
							DI.cell.type = 'task';
						} else {
							DI.cell.type = 'taskUser';
						}
					}

					if (RM.options.userResourcesMode) {
						if (DI.data.task_type == 50) {
							DI.cell.type = 'allocationUser';
						} else if (DI.data.task_type == 51) {
							DI.cell.type = 'inheritChildren';
						} else {
							DI.cell.type = 'maxChildrenUser';
						}
					}

					DI.cell.useActualId = useActualItemId;

					DI.wasOpen = false;
					DI.inheritDates = RM.options.inheritDates;
					DI.sumDatas = RM.options.sumDatas;

					DI.editable = function () {
						return true;
					};

					DI.isDraggable = function () {
						if (typeof DI.RM.rules.draggables === 'undefined') return false;

						if (DI.RM.rules.dragRestrictions) {
							for (let restriction of DI.RM.rules.dragRestrictions) {
								for (const item in restriction) {
									if (restriction[item] != DI.data[item]) return false;
								}
							}
						}

						let i = DI.RM.rules.draggables.length;
						while (i--) {
							if (DI.RM.rules.draggables[i] == DI.data.task_type) {
								return true;
							}
						}

						return getCellRights(DI).drag;
					};

					DI.append = function () {
						DI.$ele.appendTo($body);
						return DI;
					};

					DI.delete = function () {
						let parent = DI.parent;
						DI.unlink();
						DI.hide();
						DI.calendarRow.delete();

						if (parent && !parent.isRoot) {
							parent.hideInactiveOpener();
						}

						delete RMService.itemSet[DI.data.item_id];
					};

					DI.showHighlight = function (overWriteColour) {
						let colour;
						if (overWriteColour) {
							colour = RMService.sanitize(overWriteColour);
						} else {
							colour = AllocationService.getCellColour('', DI.data.status);
						}

						colour = typeof colour === 'undefined' ? '' : colour;

						if (DI.$highlight) {
							DI.$highlight.remove();
						}

						DI.$highlight = $('<div class="highlight-bar ' + colour + '"></div>')
							.prependTo(DI.$ele.find('td')
								.first());

						if (RM.options.highlightRows.indexOf(DI.data.item_id) !== -1) {
							DI.$ele.addClass('highlight');
						}
					};

					DI.setDataFromChildren = function () {
						let broke;

						if (DI.firstEle) {
							let runner = DI.firstEle;

							while (runner) {
								if (AllocationService.allocationRequiresAction(
									runner.data.status,
									RM.options.hasOwnerRights,
									RM.options.hasManagerRights) || runner.data.openAlsoParent
								) {
									broke = 'background-purple';
									DI.data.openAlsoParent = true;
									if (RM.options.openRows.length === 0) DI.open();
									break;

								}
								runner = runner.next;
							}
						}

						DI.showHighlight(broke);
						return DI;
					};

					let html = '';
					let template = RM.templates[DI.data.task_type];

					if (typeof template === 'undefined') {
						template = RM.templates['default'];
					}

					if (template) {
						let cc = template.length - 1;

						for (let i = 0; i < cc; i += 2) {
							let name = template[i + 1];
							let pc = ' data-column';

							DI.addField(name);
							html += template[i];
							if (i === 0) {
								html += '<a class="openRow btn btn-icon flip"><i class="material-icons" aria-hidden="true">expand_more</i></a>';
								pc += ' first-column';
							}

							html += '<div class="' + RMService.sanitize(RM.getFieldCls(name)) + pc + '"></div>';
						}

						html += template[cc];
						html = html.replace(/<\/tr>/g, '<td></td></tr>');

						DI.$ele = $(html);
						tdCount = DI.$ele.children().length - 1;

						DI.$users = DI.$ele.find('.users');
						DI.assignRowValues(RM.options.showUserName);
						DI.$ele.attr('rowid', DI.data.item_id).addClass('rmTarget');
						DI.$opener = DI.$ele.find('.openRow');
						RM.initCalendarRow(DI);

						if (DI.data.task_type == 1) {
							DI.data[RMService.alias[3]] = new Date(DI.data[RMService.alias[3]]);
							DI.data[RMService.alias[4]] = new Date(DI.data[RMService.alias[4]]);
							RM.initCalendarBar(DI, 'ganttBar');
						}

						DI.setUserIcons();
						DI.showHighlight();
					}
					return DI;
				}

				let mToD = 1000 * 60 * 60 * 24;

				function onCellSingleClick(cell) {
					let c = CellFactoryService.cells[cell.attr('cellid')];

					if (getCellRights(c.owner).edit !== true) {
						return;
					}

					let offset = cell.offset();
					let hours = {
						inputModel: Common.rounder(c.total, 2)
					};

					let startDate = CalendarService.getUTCDate(c.startDate);
					let endDate = CalendarService.getUTCDate(c.endDate);

					function onChange() {
						onCellChange(c.owner, startDate, endDate, hours.inputModel)
					}

					InputService.inputDialog(hours,
						{
							type: 'decimal',
							precision: 2,
							min: 0
						},
						{
							top: offset.top,
							left: offset.left,
							width: cell.width()
						},
						$scope,
						{
							onChange: onChange,
							override: true
						});
				}

				function onCellChange(DI, startDate, endDate, hours) {
					if (DI.parent.data.process === 'user') {
						let itemId = RM.options.taskId ? DI.data.parent_item_id : DI.data.resource_item_id;
						CalendarService.getUserCalendar(itemId, startDate, endDate).then(function (calendarData) {
							let dates = [];
							let dateRunner = moment.utc(startDate);
							let isFirst = true;

							while (dateRunner.isSameOrBefore(endDate)) {
								let dayData = calendarData[dateRunner.year()][dateRunner.month() + 1][dateRunner.date()];
								if (!dayData.notWork || isFirst) {
									dates.push(dateRunner.clone().toDate());
									isFirst = false;
								}

								dateRunner.add(1, 'days');
							}

							let i = dates.length;
							let perDay = Common.rounder(hours / i, 4);

							if (isNaN(perDay)) {
								return;
							}

							while (i--) {
								let date = dates[i];
								AllocationService.calendarDataChanged(
									date,
									date,
									perDay,
									DI);
							}

							RM.setResolution();
						});
					} else {
						let perDay = Common
							.rounder(hours / Math.abs((endDate.getTime() - startDate.getTime()) / mToD + 1), 4);
						AllocationService.calendarDataChanged(
							startDate,
							endDate,
							perDay,
							DI);

						RM.setResolution();
					}

					if (DI.data.status == 20) {
						AllocationService.setAllocationStatus(DI, 25);
					}
				}

				function hasCostCenterRights(DI) {
					if (Array.isArray($scope.managerOfCostCenters)) {
						let i = $scope.managerOfCostCenters.length;
						while (i--) {
							let costCenter = $scope.managerOfCostCenters[i];

							if (Array.isArray(DI.data.cost_center)) {
								let j = DI.data.cost_center.length;
								while (j--) {
									if (DI.data.cost_center[j] == costCenter) {
										return true;
									}
								}
							} else if (costCenter == DI.data.cost_center) {
								return true;
							}
						}
					}

					return false;
				}

				function getCellRights(DI) {
					if (typeof DI === 'undefined') {
						return {};
					}

					if (DI.data.task_type !== 50 || DI.data.status == 53 || (!RM.options.taskId && DI.data.status == 55)) {
						return {};
					}

					return AllocationService
						.getStatusInfo(
							DI.data.status,
							{
								owner: RM.options.hasOwnerRights,
								manager: RM.options.hasManagerRights,
								write: RM.options.hasWriteRights,
								request: RM.options.hasRequestRights,
								send: RM.options.hasSendRights,
								drag: RM.options.hasDragRights,
								reset: RM.options.hasResetRights
							}).rights;
				}

				function onEmptyClick(itemId, startDate, endDate, left, top) {
					let DI = RMService.itemSet[itemId];

					if (getCellRights(DI).edit !== true) {
						return;
					}

					let hours = {
						inputModel: 0
					};

					let sd = CalendarService.getUTCDate(startDate);
					let ed = CalendarService.getUTCDate(endDate);

					function onChange() {
						onCellChange(DI, sd, ed, hours.inputModel);
					}

					InputService.inputDialog(hours,
						{
							type: 'decimal',
							precision: 2,
							min: 0
						},
						{
							top: top,
							left: left
						},
						$scope,
						{
							onChange: onChange,
							override: true
						});
				}

				let currentlySelectedDI;
				let openClose = false;

				function onEmptyDoubleClick(itemId, startDate, endDate) {
					let DI = RMService.itemSet[itemId];

					if (typeof DI === 'undefined') {
						return;
					}

					if (DI.data.task_type != 50 || DI.data.status == 53) {
						return;
					}

					if (!RM.options.taskId && DI.data.status == 55) {
						ViewService.view('featurette',
							{
								params: {
									process: 'task',
									itemId: DI.data.task_item_id,
									title: DI.data.primary
								}
							},
							{
								split: true
							}
						);

						return;
					}

					let rights = AllocationService.getStatusInfo(
						DI.data.status,
						{
							owner: RM.options.hasOwnerRights,
							manager: RM.options.hasManagerRights,
							write: RM.options.hasWriteRights,
							send: RM.options.hasSendRights,
							request: RM.options.hasRequestRights,
							drag: RM.options.hasDragRights,
							reset: RM.options.hasResetRights
						}).rights;

					if (!(rights.request || rights.approve || rights.reject || rights.reset || rights.edit || rights.drag || rights.reset || rights.reApprove)) {
						return;
					}

					if (currentlySelectedDI == DI.data.item_id) {
						DI.calendarRow.currentDateSelection.startDate = new Date(startDate);
						DI.calendarRow.currentDateSelection.endDate = new Date(endDate);
					} else {
						currentlySelectedDI = DI.data.item_id;
						openClose = true;

						RM.initCalendarBar(DI, 'allocationDateRange');
						DI.calendarRow.currentDateSelection = {
							startDate: startDate,
							endDate: endDate
						};

						DI.calendarRow.bars.allocationDateRange.show();
						DI.calendarRow.bars.allocationDateRange.draw();

						ViewService.view('allocation.cell',
							{
								params: {
									parentItemId: DI.parent.data.item_id,
									itemId: DI.data.item_id,
									autoAccept: RM.options.autoAccept,
									projectId: RM.options.projectId,
									projectStartDate: RM.options.projectStartDate,
									projectEndDate: RM.options.projectEndDate
								},
								locals: {
									DI: DI,
									RM: RM,
									Rights: {
										write: RM.options.hasWriteRights,
										request: RM.options.hasRequestRights,
										send: RM.options.hasSendRights,
										manager: RM.options.hasManagerRights,
										owner: RM.options.hasOwnerRights && hasCostCenterRights(DI),
										drag: RM.options.hasDragRights,
										reset: RM.options.hasResetRights
									},
									ResetSelection: function () {
										if (!openClose) {
											currentlySelectedDI = undefined;
										}
									}
								}
							},
							{
								split: true,
								replace: 'allocation.cell',
								id: RM.options.viewId,
								extend: true,
								threshold: 'normal'
							}
						);

						setTimeout(function () {
							openClose = false;
						});
					}
				}


				this.initializeRowManager = function () {
					if (RM.options.taskId) {
						visibleColumns.push('start_date');
						visibleColumns.push('end_date');
					}

					let hideColumns = [];
					angular.forEach($scope.portfolioColumns, function (filter) {
						let name = filter.ItemColumn.Name;
						if (visibleColumns.indexOf(name) === -1) {
							hideColumns.push(name)
						}
					});

					RM.setVisibility(hideColumns);

					RM.init = function (DI) {
						return initRow(DI);
					};

					RM.initNew = function (data) {
						return RM.init(new RMDIService.dataItem(RM, data));
					};

					RM.setRules(RM.rules);
					RM.showEmpty = checkIfEmpty;
					RM.hideEmpty = function () {
					};

					function assignUser(DI) {
						let i = lowerRows.length;
						while (i--) {
							if (lowerRows[i].data.item_id == DI.data.item_id) {
								lowerRows.splice(i, 1);
								break;
							}
						}

						DI.cell.mode = '';

						return AllocationService.getAssignedTasks(RM.options.taskId, DI.data.process, DI.data.item_id)
							.then(function (allocations) {
								let rows = allocations.rowdata;
								addRows(rows);

								let i = rows.length;
								while (i--) {
									if (rows[i].actual_item_id == RM.options.taskId) {
										let parentTask = ProcessService.getCachedItemData('task', RM.options.taskId).Data;
										onEmptyDoubleClick(
											rows[i].item_id,
											CalendarService.getUTCDate(parentTask.start_date),
											CalendarService.getUTCDate(parentTask.end_date));

										break;
									}
								}
							});
					}

					RM.setRowParent = function (DI) {
						let ti = RM.previousRow.getCurrentIndex();
						let previousParent = DI.parent;

						if (ti == RM.validIndex) {
							DI.linkAfter(RM.previousRow).setOrderNo(Common.getOrderNoBetween);
						} else if (ti > RM.validIndex) {
							let dif = ti - RM.validIndex;
							let runner = RM.previousRow;

							while (dif--) {
								runner = runner.parent;
							}

							DI.linkAfter(runner);
						} else {
							DI.linkFirst(RM.targetParent);
						}

						if (!previousParent.isRoot) {
							previousParent.setPadding();
						}

						DI.setPadding(RM.targetParent.getCurrentIndex() + 1);

						if (!RM.targetParent.isRoot) {
							RM.targetParent.setPadding();
						}

						RMService.inheritParent(DI.data, RM.targetParent.data);
						DI.parent = RM.targetParent;
						ProcessService.setDataChanged(DI.data.process, DI.data.item_id);

						if (DI.data.status != 25 && RM.targetParent.data.item_id != previousParent.data.item_id) {
							let ns = DI.data.status == 50 ? 40 : 25;
							AllocationService.setAllocationStatus(DI, ns);
						}
					};

					RM.applyChanges = function (itemId) {
						let DI = RMService.itemSet[itemId];
						let oldParent = DI.parent;
						RM.setRowParent(DI);
						DI.data.resource_item_id = DI.parent.data.item_id;

						RM.drawBar();
						DI.setDataChanged();

						if (!DI.parent.isRoot) {
							DI.parent.highlightLong();
						}

						if (DI.parent.isRoot || DI.parent.isOpen) {
							if (DI.wasOpen) {
								DI.open(false, true);
							}
						} else {
							DI.calendarRow.hide();
							DI.hide();
						}

						if (!oldParent.isRoot) {
							oldParent.setDataFromChildren();
						}

						if (!DI.parent.isRoot) {
							DI.parent.setDataFromChildren()
						}

						DI.unSelectParents();
						RM.calendarReDraw();
					};

					RM.setSelectMode = function (set) {
						rmMenu.closeMenu();

						if (set) {
							RM.$parent.showSelection = true;
							RM.$parent.$digest();
						} else {
							unSelectRows();
							RM.$parent.showSelection = false;
						}

						RM.selectMode = set;
					};

					RM.getPlaceholder = function () {
						RM.$placeholder = $('<tr><td class="placeholder" colspan="' + tdCount + '"><div>&nbsp;</div></td></tr>');
						return RM.$placeholder;
					};

					RM.initializeResize($scope, RM.$ele);
					RMService.addRM(RM.id, RM);

					RM.initVisibility(RM.$fHeaderEle);
					RM.initVisibility(RM.$headerEle);
					RM.initializeScroll($scope);

					if (RM.options.showCalendar) {
						let clickEvents = {
							dblClick: onEmptyDoubleClick,
							cellClick: onCellSingleClick,
							emptyClick: onEmptyClick
						};

						RM.initCalendar(RM.$ele, $scope, clickEvents);
					}

					addRows($scope.rows, true);

					$scope.$watch('rows', function (newRows, oldRows) {
						if (newRows !== oldRows) {
							angular.forEach(oldRows, function (r) {
								let DI = RMService.itemSet[r.itemId];
								if (DI) {
									DI.delete();
								}
							});

							addRows(newRows);
						}
					});

					RM.initializeCalendar();
					RM.$ele.show();
					checkIfEmpty();

					let viewId = RM.$ele.closest('content').parent().attr('id');
					if (RM.options.multipleProjects || RM.options.multipleResources) {
						let droppableZone = HTMLService.initialize('div')
							.attr('id', 'allocation-backdrop')
							.attr('droppable-process', 'dropProcess')
							.inner(HTMLService.initialize('div')
								.attr('ng-show', 'checkProcessState')
								.class('align-column')
								.inner(HTMLService.initialize('loader').class('contrast large')
									.element('h1').class('h5 white').content(Sanitizer(Translator.translate('ALLOCATION_PROJECT_PLEASE_WAIT')))
								)
								.element('div').attr('ng-show', '!checkProcessState').class('align-column').inner(
									HTMLService.initialize().icon('keyboard_arrow_down', 'white')
										.element('h1')
										.class('h5 white')
										.content(Sanitizer(Translator.translate(
											RM.options.multipleProjects ?
												'ALLOCATION_PROJECT_DROP_REQUEST' : 'ALLOCATION_RESOURCE_DROP_REQUEST'))
										)
								)
							).getHTML();

						$scope.$on('DragProcessStart', function (event, process) {
							if (RM.options.multipleProjects || process === 'user' || process === 'competency') {
								ViewService.backdrop(viewId, {
									template: droppableZone,
									scope: $scope
								});
							}
						});

						$scope.$on('DragProcessStop', function () {
							if (!$scope.checkProcessState) {
								ViewService.removeBackdrop(viewId);
							}
						});

						$scope.$on('DragProcessOverride', function (event, opt) {
							$scope.dropProcess(opt.process, opt.itemId);
						});

						$scope.dropProcess = function (process, itemId) {
							if (isVisibleDI(rootParent, itemId)) {
								ViewService.removeBackdrop(viewId);

								RMService.itemSet[itemId].highlightLong();

								return;
							}

							if (RM.options.multipleProjects) {
								$scope.checkProcessState = true;

								ProcessService.getStates(process, itemId).then(function (states) {
									let hasRights = false;
									if (typeof states.project_allocation !== 'undefined') {
										let s = states.project_allocation.States;
										if (s.indexOf('read') !== -1 || s.indexOf('owner') !== -1) hasRights = true;
									}

									if (hasRights) {
										AllocationService.getProjectsAllocationsData([itemId]).then(function (rows) {

											addRows(AllocationService.addTotals(rows.rowdata));

											AllocationService.getActiveSettings('projects').projects.push(itemId);
											AllocationService.saveActiveSettings('projects');

											ViewService.removeBackdrop(viewId);
											$scope.checkProcessState = false;
										});
									} else {
										ViewService.removeBackdrop(viewId);
										$scope.checkProcessState = false;
										MessageService.toast('NO_ALLOC_RIGHTS', 'warn');
									}
								});
							} else {
								AllocationService.getResourceAllocationData(undefined, itemId).then(function (rows) {
									addRows(AllocationService.addTotals(rows.rowdata));
									AllocationService.getActiveSettings('resources').resources.push(itemId);
									AllocationService.saveActiveSettings('resources');

									ViewService.removeBackdrop(viewId);
								});
							}
						};
					} else {
						let droppableZone = HTMLService.initialize('div')
							.attr('droppable-process', 'dropProcess')
							.attr('id', 'allocation-backdrop')
							.inner(HTMLService.initialize('div').class('align-column')
								.inner(HTMLService.initialize().icon('keyboard_arrow_down', 'white')
									.element('h1').class('h5 white').content(Sanitizer(Translator.translate('ALLOCATION_ALLOCATION_DROP_REQUEST')))
								)
							).getHTML();

						$scope.$on('DragProcessStart', function (event, process) {
							if ((process === 'user' || process === 'competency') && RM.options.hasManagerRights && RM.options.hasRequestRights) {
								ViewService.backdrop(viewId, {
									template: droppableZone,
									scope: $scope
								});
							}
						});

						$scope.$on('DragProcessStop', function (event, process) {
							if (process === 'user' || process === 'competency') {
								ViewService.removeBackdrop(viewId);
							}
						});

						$scope.dropProcess = function (process, itemId) {
							requestNewAllocation(itemId, process);
						};
					}
					RM.$parent.getRMSelectedRows = function () {
						if (RM.selectMode) {
							return RM.selectedRows;
						}
					};
					RM.$parent.calendarReDraw = function () {
						RM.calendarReDraw();
					};
					RM.$parent.addNewItem = function (event) {
						let menuItems = [{
							name: 'ALLOCATION_ALLOCATION_REQUEST',
							onClick: function () {
								AllocationService.requestResources().then(function (model) {
									for (let i = 0, l = model.user.length; i < l; i++) {
										requestNewAllocation(model.user[i], 'user');
									}

									for (let i = 0, l = model.competency.length; i < l; i++) {
										requestNewAllocation(model.competency[i], 'competency');
									}
								});
							}
						}];

						MessageService.menu(menuItems, event.currentTarget);
					};

					RMDIService.bindSubscribeChanged(['allocation'], $scope);

					function getMinDay(date) {
						let mY = undefined;
						let mM = undefined;
						let mD = undefined;

						angular.forEach(date, function (year, y) {
							if (y <= mY || typeof mY === 'undefined') {
								angular.forEach(year, function (month, m) {
									if (m <= mM || typeof mM === 'undefined') {
										angular.forEach(month, function (day, d) {
											if (+d <= mD || typeof mD === 'undefined') {
												mY = y;
												mM = m;
												mD = d;
											}
										});
									}
								});
							}
						});

						return CalendarService.getUTCDate(new Date(mY, --mM, mD));
					}

					/* __ __             ____
					  / // /__ ____  ___/ / /__ _______
					 / _  / _ `/ _ \/ _  / / -_) __(_-<
					/_//_/\_,_/_//_/\_,_/_/\__/_/ /__*/
					// @ts-ignore
					let rmMenu = new function () {
						let fn = function (DI) {
							let items = [];
							let info = AllocationService.getStatusInfo(
								DI.data.status,
								{
									owner: RM.options.hasOwnerRights,
									manager: RM.options.hasManagerRights,
									write: RM.options.hasWriteRights,
									send: RM.options.hasSendRights,
									request: RM.options.hasRequestRights,
									drag: RM.options.hasDragRights,
									reset: RM.options.hasResetRights
								});
							let rights = info.rights;
							let shortText = info.text.short;
							let longText = info.text.long;

							let taskType = DI.data.task_type;
							let itemId = DI.data.item_id;
							let process = DI.data.process;
							let primary = DI.data.primary;
							let status = DI.data.status;
							let parentProcess = DI.data.process_name;
							let taskId = DI.data.task_item_id;
							let parentId = DI.data.process_item_id;


							if (taskType == 50) {
								items.push({
									name: Translator.translate('SCROLL'),
									onClick: function () {
										RM.moveCalendar(getMinDay(AllocationService.getAllocationData(itemId)));
									}
								});
							}

							if (RM.options.multipleProjects && taskType == 52) {
								items.push(
									{
										name: Translator.translate('VIEW'),
										onClick: function () {
											let p = {
												params: {
													itemId: itemId,
													process: process
												}
											};

											ViewService.view('process.gantt', p);
										}
									});

								items.push(
									{
										name: Translator.translate("REMOVE"),
										onClick: function () {
											let savedSettings = AllocationService.getActiveSettings('projects').projects;
											let i = savedSettings.length;
											while (i--) {
												if (savedSettings[i] == itemId) {
													savedSettings.splice(i, 1);
													AllocationService.saveActiveSettings('projects');
													break;
												}
											}

											DI.close();
											DI.delete();
											RM.setCalendarHeight();
										}
									});
							}

							if (RM.options.multipleResources && taskType == 51) {
								items.push(
									{
										name: Translator.translate("REMOVE"),
										onClick: function () {
											let savedSettings = AllocationService.getActiveSettings('resources').resources;
											let i = savedSettings.length;
											while (i--) {
												if (savedSettings[i] == itemId) {
													savedSettings.splice(i, 1);
													AllocationService.saveActiveSettings('resources');
													break;
												}
											}

											DI.close();
											DI.delete();
											RM.setCalendarHeight();
										}
									});
							}

							if (RM.options.hasManagerRights && rights.request && taskType == 50 && status != 55) {
								items.push(
									{
										name: Translator.translate('REMOVE'),
										onClick: function () {
											AllocationService.removeAllocation(DI);
										}
									});
							}

							if (rights.reject && hasCostCenterRights(DI)) {
								items.push(
									{
										name: shortText.reject,
										onClick: function () {
											MessageService.confirm(longText.reject + ' ' + primary, function () {
												let newStatus = AllocationService.nextStatus(status, 'reject', RM.options.autoAccept);
												return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
													DI.highlight();
													RM.calendarReDraw();
												});
											});
										}
									});
							}

							if (rights.reApprove && status == 13 && hasCostCenterRights(DI)) {
								items.push(
									{
										name: shortText.reApprove,
										onClick: function () {
											MessageService.confirm(longText.approve + ' ' + primary, function () {
												let newStatus = AllocationService.nextStatus(status, 'reApprove', RM.options.autoAccept);
												return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
													DI.highlight();
													RM.calendarReDraw();
												});
											});
										}
									});

							}

							if ((rights.approve && hasCostCenterRights(DI) && status > 10) || (rights.approve && status == 10) || (rights.approve && status == 13)) {
								items.push(
									{
										name: shortText.approve,
										onClick: function () {
											if (AllocationService.hasAllocationHours(itemId)) {
												MessageService.confirm(longText.approve + ' ' + primary, function () {
													let newStatus = AllocationService.nextStatus(status, 'approve', RM.options.autoAccept);
													return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
														DI.highlight();
														RM.calendarReDraw();
													});
												});
											} else {
												MessageService.msgBox('ALLOCATION_EMPTY_HOURS');
											}
										}
									});
							}

							if (rights.reset) {
								items.push(
									{
										name: shortText.reset,
										onClick: function () {
											MessageService.confirm(longText.reset + ' ' + primary, function () {
												let newStatus = AllocationService.nextStatus(status, 'reset', RM.options.autoAccept);

												return AllocationService.setAllocationStatus(DI, newStatus).then(function () {
													RM.setResolution();
												});
											});
										}
									});
							}

							if (taskType == 50 && status != 53) {
								if (RM.options.allowTaskJump) {

									if (taskId) {
										items.push({
											name: Translator.translate('PEEK_TASK'),
											onClick: function () {
												let p = {
													params: {
														itemId: taskId,
														process: 'task',
														forceReadOnly: true
													}
												};

												ViewService.view('process.tab', p, {split: true, size: 'narrower'});
											}
										});
									}

									if (parentId && parentProcess) {
										items.push({
											name: Translator.translate('OPEN_TIMELINE'),
											onClick: function () {
												let p = {
													params: {
														itemId: parentId,
														process: parentProcess,
														openRow: taskId
													}
												};

												ViewService.view('process.gantt', p);
											}
										});
									}
								}

								if (RM.options.allowProjectJump && parentId && parentProcess) {
									let getParentState = function () {
										return ProcessService
											.getItemData(parentProcess, parentId)
											.then(function (parentData) {
													let state = parentData.Data.current_state;
													if (typeof state === 'string' &&
														state.indexOf('{') === -1 &&
														_.includes(state, 'tab')
													) {
														return state.split('_')[1];
													}
												}
											);
									};

									items.push({
										name: Translator.translate('PEEK_PROJECT'),
										onClick: function () {
											getParentState().then(function (tabId) {
												let p = {
													params: {
														itemId: parentId,
														process: parentProcess,
														tabId: tabId
													}
												};
												ViewService.view('1stTab', p, {split: true, size: 'narrower'});
											});
										}
									});

									items.push({
										name: Translator.translate('OPEN_PROJECT'),
										onClick: function () {
											getParentState().then(function (tabId) {
												let p = {
													params: {
														itemId: parentId,
														process: parentProcess,
														tabId: tabId
													}
												};

												ViewService.view('1stTab', p);
											});

										}
									});
								}
							}

							if (RM.options.taskId && taskType == 51) {
								let i = lowerRows.length;
								while (i--) {
									if (lowerRows[i].data.item_id == itemId) {
										break;
									}
								}

								if (i === -1) {
									items.push({
										name: Translator.translate('ALLOCATION_UNSIGN'),
										onClick: function () {
											AllocationService.removeAssignedAllocation(DI, RM);
										}
									});
								} else {
									items.push({
										name: Translator.translate('ALLOCATION_ASSIGN'),
										onClick: function () {
											assignUser(DI).then(function () {
												let firstEle = rootParent.firstEle;
												DI.linkAfter(firstEle).after(firstEle);
												DI.calendarRow.after(firstEle, false);
												DI.open();
											});
										}
									});
								}
							}

							if (taskType == 51 && !RM.options.taskId && RM.options.hasManagerRights) {
								if (!DI.firstEle ||
									(typeof DI.firstEle.next === 'undefined' &&
										DI.firstEle.data.status == 53)) {
									items.push(
										{
											name: Translator.translate('ALLOCATION_ALLOCATION_REMOVE_USER'),
											onClick: function () {
												DI.close();
												DI.delete();

												RM.calendarReDraw();
											}
										});
								}

								if (rights.request) {
									items.push(
										{
											name: Translator.translate('ALLOCATION_ALLOCATION_REQUEST'),
											onClick: function () {
												requestNewAllocation(itemId);
											}
										});
								}

								let draftAllocations = [];

								let runner = DI.firstEle;
								let emptyRequest = false;
								while (runner) {
									if (runner.data.status == 10) {
										let r = AllocationService.getStatusInfo(
											runner.data.status,
											{
												owner: RM.options.hasOwnerRights,
												manager: RM.options.hasManagerRights,
												write: RM.options.hasWriteRights,
												send: RM.options.hasSendRights,
												request: RM.options.hasRequestRights,
												drag: RM.options.hasDragRights,
												reset: RM.options.hasResetRights
											});

										if (r.rights.approve) {
											if (emptyRequest || !AllocationService.hasAllocationHours(runner.data.item_id)) {
												emptyRequest = true;
											}

											draftAllocations.push(runner);
										}
									}

									runner = runner.next;
								}

								if (draftAllocations.length) {
									items.push(
										{
											name: Translator.translate('ALLOCATION_SEND_DRAFTS'),
											onClick: function () {
												if (emptyRequest) {
													MessageService.msgBox('ALLOCATION_EMPTY_HOURS');
												} else {
													MessageService.confirm('ALLOCATION_ALLOCATION_REQUEST_ALL', function () {
														let i = draftAllocations.length;
														let promises = [];
														while (i--) {
															let a = draftAllocations[i];
															let newStatus = AllocationService.nextStatus(a.data.status, 'approve', RM.options.autoAccept);
															let p = AllocationService.setAllocationStatus(a, newStatus);
															promises.push(p);
														}

														return $q.all(promises).then(function () {
															RM.setResolution();
														});
													});
												}
											}
										});
								}
							}

							return items;
						};

						return new RMService.rowMenu(RM.options, RM, fn);
					};

					if (RM.options.sortable) {
						RMSortService.bindRowDrag({
							$element: RM.$ele,
							selector: 'tr.rmTarget',
							hoverMode: 'v',
							wrap: '<table class="rmDragged"></table>'
						});
					}

					$body.attr('diset', RM.id);

					RMService.clickHandler({
						RM: RM,
						menu: rmMenu,
						options: RM.options,
						$element: RM.$ele
					});

					RM.$parent.clickSelStop = function () {
						if (RM.selectMode) {
							RM.setSelectMode(false);
						}
					};
				};

				function setOpenCloseRows() {
					let cache = AllocationService.getOpenCloseCache();
					_.each(RMService.itemSet, function (value, key) {
						if (cache.hasOwnProperty(key)) {
							if (cache[key] == false) {
								value.close();
							} else {
								value.open();
							}
						}
					})
				}
			}
		};

	}
})();
