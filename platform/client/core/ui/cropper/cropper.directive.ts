﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('cropper', Cropper);

	Cropper.$inject = [];
	function Cropper() {
		return {
			restrict: 'E',
			scope: { file: '=', crop: '=', initialCrop: '=?', croppedImage: '=', restrictions: '<?' },
			templateUrl: 'core/ui/cropper/cropper.html',
			controller: function ($scope, $element, $timeout) {
				var reader = new FileReader();
				var img = $element.find('img');
				var timer, ratio;
				if (typeof $scope.restrictions === 'undefined') {
					$scope.restrictions = {};
				}

				switch ($scope.restrictions.ratio) {
					case '1:1':
						ratio = 1;
						break;
					case '4:3':
						ratio = 1.33;
						break;
					case '3:2':
						ratio = 1.5;
						break;
					case '16:10':
						ratio = 1.6;
						break;
					case '10:6':
						ratio = 1.6667;
						break;
					case '16:9':
						ratio = 1.78;
						break;
					case '18:9':
						ratio = 2;
						break;
					case '21:9':
						ratio = 2.37;
						break;
					default:
						ratio = NaN;
						break;
				}

				reader.onload = function () {
					$timeout(function () {
						$scope.image = reader.result;
						crop();
					});
				};

				$scope.$watch('file', function () {
					img.cropper('destroy');
					if ($scope.file && _.startsWith($scope.file.type, 'image')) {
						reader.readAsDataURL($scope.file);
					} else {
						$scope.image = '';
					}
				});

				function crop() {
					$timeout(function () {
						var params = {
							//viewMode: 3,
							background: false,
							crop: function () {
								clearTimeout(timer);

								timer = setTimeout(function () {
									var options = {
										height: parseInt($scope.restrictions.height),
										maxHeight: parseInt($scope.restrictions.maxHeight),
										width: parseInt($scope.restrictions.width),
										maxWidth: parseInt($scope.restrictions.maxWidth)
									};

									if (ratio) {
										options.width = options.height * ratio;
										options.maxWidth = options.maxHeight * ratio;
									}

									$scope.crop = img.cropper('getData', true);
									$scope.croppedImage = img.cropper('getCroppedCanvas').toDataURL();
								}, 300);
							},
							guides: false,
							center: false,
							aspectRatio: ratio
						};

						if ($scope.initialCrop) {
							params.built = function () {
								img.cropper('setData', $scope.initialCrop);
							};
						}

						img.cropper(params);
					});
				}
			}
		};
	}
})();
