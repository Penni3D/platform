(function () {
	'use strict';

	angular
		.module('core')
		.directive('transition', Transition);

	Transition.$inject = ['Common', '$parse'];
	function Transition(Common, $parse) {
		return {
			restrict: 'E',
			transclude: true,
			scope: true,
			link: function (scope, element, attr, ctrl, transclude) {
				var transition;
				var transitionOutTimer;

				transclude(scope.$parent, function (clone) {
					clone.appendTo(element);
				});

				scope.$watch(function () {
					return transition = $parse(attr.transite)(scope.$parent);
				}, function () {
					clearTimeout(transitionOutTimer);

					if (Common.isTrue(transition)) {
						element.css('display', 'block');
						setTimeout(function () {
							element.addClass('transition-in');
						}, 75);
					} else {
						element.removeClass('transition-in');

						transitionOutTimer = setTimeout(function () {
							element.css('display', 'none');
						}, 150);
					}
				});
			}
		}
	}
})();