(function () {
	'use strict';

	angular
		.module('core')
		.directive('board', Board);


	Board.$inject = ['InputService', 'UniqueService', 'Colors', 'Common', 'CalendarService', 'HTMLService', '$compile', 'Sanitizer'];
	function Board(InputService, UniqueService, Colors, Common, CalendarService, HTMLService, $compile, Sanitizer) {
		return {
			scope: {
				cards: '<',
				onClick: '&?',
				options: '=?'
			},
			templateUrl: 'core/ui/board/board.html',
			link: function (scope, element, attrs) {
				scope.chips = typeof attrs.cardChips === 'undefined' ? 'chips' : attrs.cardChips;
				scope.title = typeof attrs.cardTitle === 'undefined' ? 'title' : attrs.cardTitle;
				scope.description = typeof attrs.cardDescription === 'undefined' ? 'description' : attrs.cardDescription;

				var blocksOrder = [];
				var unassignedCards = [];
				var blocks = {};
				var cards = {};

				var $boardHeader = element.find('.board-header');
				var $boardContent = element.find('.board-content');
				var boardWidth = element.width();

				var headerAlignClass = '';
				var maxBlockWidth;
				var cardWidth = 324;

				var toggleIndex = 1;
				var conf = {
					self: typeof attrs.self === 'undefined' ? 'item_id' : attrs.self,
					date: typeof attrs.cardDate === 'undefined' ? 'start_date' : attrs.cardDate,
					color: typeof attrs.cardColor === 'undefined' ? '' : attrs.cardColor
				};

				var boardFunctions = {
					onWidthChange: function () {
						return Math.ceil(boardWidth / (maxBlockWidth));
					},
					widthChangeNextBlock: function () {},
					determineColumn: function (card, block) {
						var cardDate = new Date(InputService.findProperty(card.data, conf.date));
						var cardTime = cardDate.getTime();

						if (block.startDate.getTime() <= cardTime && cardTime <= block.endDate.getTime()) {
							return cardDate;
						}
					},
					initializeBlock: function () {},
					getPreviousStartDate: function () {},
					getNextStartDate: function () {},
					determinePosition: function () {
						return {};
					}
				};

				scope.cardClick = function (card) {
					if (typeof scope.onClick === 'function' && typeof scope.onClick() === 'function') {
						scope.onClick()(card.data);
					}
				};

				scope.getColor = function (card) {
					var color = Colors.getColor(card.data[conf.color]);
					if (!color.background) {
						color = Colors.getColor('grey');
					}

					return color.background;
				};

				scope.$watch('options.columns', function () {
					scope.options.scroll = 0;
					initialize();
				});

				scope.$watch('cards', function () {
					var removed = [];
					var added = [];
					var moved = [];

					angular.forEach(scope.cards, function (card) {
						var found = false;

						angular.forEach(cards, function (card_) {
							if (card == card_.data) {
								if (card_.date &&
									card_.date.getTime() !== new Date(InputService.findProperty(card, conf.date)).getTime()
								) {
									moved.push(card_);
								}

								found = true;
							}
						});

						if (!found) {
							added.push(card);
						}
					});

					angular.forEach(cards, function (card) {
						var found = false;

						angular.forEach(scope.cards, function (card_) {
							if (card.data == card_) {
								found = true;
							}
						});

						if (!found) {
							removed.push(card);
						}
					});

					var i = removed.length;
					while (i--) {
						removed[i].delete();
					}

					for (var i = 0, l = added.length; i < l; i++) {
						boardCard(added[i]);
					}

					var i = moved.length;
					while (i--) {
						var c = moved[i];
						c.remove();
						determineCardColumn(c);
					}
				}, true);

				var xScrollLock = true;
				$boardContent.on('wheel', '.column-block', function (e) {
					var block = blocks[e.currentTarget.getAttribute('block-id')];
					var initScroll = block.$cards.scrollTop();
					var newScroll = initScroll + e.originalEvent.deltaY;
					var maxScroll = block.$cards[0].scrollHeight - block.$cards.height();

					if (newScroll < maxScroll) {
						if (newScroll > 0) {
							block.float();
							e.preventDefault();
						} else {
							block.unfloat();
						}
					}

					block.$cards.scrollTop(newScroll);
				});

				$boardContent.on('mousedown', '.column-block', function (e) {
					if (e.which !== 1) {
						return;
					}

					var block = blocks[e.currentTarget.getAttribute('block-id')];
					var initScroll = block.$cards.scrollTop();
					var initY = e.pageY;
					var maxScroll = block.$cards[0].scrollHeight - block.$cards.height();

					element.on('mouseup mouseleave', function () {
						element.off('mouseup mousemove mouseleave');
					});

					element.on('mousemove', function (e) {
						var delta = initY - e.pageY;
						if (xScrollLock) {
							if (Math.abs(delta) > 64) {
								xScrollLock = false;
								initY = e.pageY;
								delta = 0;
							} else {
								return;
							}
						}

						Common.removeSelection(e);
						var newScroll = initScroll + delta;
						if (newScroll < 0) {
							newScroll = 0;
						}

						if (newScroll > maxScroll) {
							newScroll = maxScroll;
						}

						newScroll ? block.float() : block.unfloat();
						block.$cards.scrollTop(newScroll);
					});
				});

				element.on('mousedown', function (e) {
					if (e.which !== 1) {
						return;
					}

					var initScroll = element.scrollLeft();
					var initX = e.pageX;
					var lastScroll = initScroll;

					element.on('mouseup mouseleave', function () {
						element.off('mouseup mousemove mouseleave');
					});

					element.on('mousemove', function (e) {
						var delta = initX - e.pageX;
						if (!xScrollLock) {
							if (Math.abs(delta) > 64) {
								xScrollLock = true;
								initX = e.pageX;
								delta = 0;
							} else {
								return;
							}
						}

						Common.removeSelection(e);
						var newScroll = initScroll + delta;
						if (newScroll < 0) {
							newScroll = 0;
						}

						var overWriteScroll = lastScroll < newScroll ?
							scrollingRight(newScroll) : scrollingLeft(newScroll);

						if (overWriteScroll) {
							initScroll = overWriteScroll;
							newScroll = overWriteScroll;
							initX = e.pageX;
						}

						lastScroll = newScroll;
						element.scrollLeft(newScroll);
						scope.options.scroll = newScroll;
					});
				});

				function initialize() {
					if (scope.options.columns === 'quartal') {
						maxBlockWidth = 504;
						headerAlignClass = 'align-center';
						var columnWidth = 168;

						boardFunctions.widthChangeNextBlock = function (date) {
							date.setMonth(date.getMonth() + 3);
						};

						boardFunctions.initializeBlock = function (block, date) {
							var columnPerBlock = 3;

							var translatedMonths = Common.getLocalMonthsOfYear();
							var startDate = new Date(date);
							var m = startDate.getMonth();

							startDate.setMonth(m - (m % 3));
							startDate.setDate(1);
							setMidnight(startDate);

							var startMonth = startDate.getMonth();
							var endDate = new Date(startDate);

							endDate.setMonth(startMonth + 3);
							endDate.setDate(0);
							setMidnight(endDate, true);

							var quartal = Math.floor(startMonth / 3);

							block.startDate = startDate;
							block.endDate = endDate;
							block.scope.header.title = 'Q' + (quartal + 1) + ' ' + startDate.getFullYear();

							for (var i = quartal * columnPerBlock, l = (quartal * columnPerBlock) + columnPerBlock; i < l; i++) {
								var hStartDate = new Date(startDate);
								hStartDate.setMonth(i);

								var hEndDate = new Date(hStartDate);
								hEndDate.setMonth(i + 1);
								hEndDate.setDate(0);
								setMidnight(hEndDate, true);

								var h = {
									title: translatedMonths[i],
									startDate: hStartDate,
									endDate: hEndDate,
									width: columnWidth
								};

								block.scope.header.subheaders.push(h);
							}
						};

						boardFunctions.getPreviousStartDate = function (nextStartDate) {
							var startDate = new Date(nextStartDate);
							startDate.setMonth(startDate.getMonth() - 3);

							return startDate;
						};

						boardFunctions.getNextStartDate = function (prevStartDate) {
							var startDate = new Date(prevStartDate);
							startDate.setMonth(startDate.getMonth() + 3);

							return startDate;
						};

						boardFunctions.determinePosition = function (card, block) {
							var cardDate = new Date(InputService.findProperty(card.data, conf.date));
							var cardMonth = cardDate.getMonth();
							var p = {
								card: undefined,
								pointer: undefined
							};

							if (block.endDate.getMonth() === cardMonth) {
								p.card = columnWidth;
								p.pointer = cardWidth - columnWidth + Math.round((cardDate.getDate() - 1) / 30  * columnWidth);
							} else {
								p.pointer = Math.round((cardDate.getDate() - 1) / 30 * columnWidth);

								if (block.startDate.getMonth() !== cardMonth) {
									p.card = columnWidth / 2;
									p.pointer += columnWidth / 2;
								}
							}

							return p;
						};
					} else if (scope.options.columns == 'month') {
						headerAlignClass = 'align-left';
						maxBlockWidth = 744;
						var dayWidth = 24;

						boardFunctions.widthChangeNextBlock = function (date) {
							date.setMonth(date.getMonth() + 1);
						};

						boardFunctions.initializeBlock = function (block, date) {
							var translatedMonths = Common.getLocalMonthsOfYear();

							var startDate = new Date(date);
							var m = startDate.getMonth();
							startDate.setDate(1);

							var endDate = new Date(startDate);
							endDate.setMonth(m + 1);
							endDate.setDate(0);

							var startWeekDay = CalendarService.getWeekDay(startDate);
							var endWeekDay = CalendarService.getWeekDay(endDate);

							var startWeekEnd = new Date(startDate);
							startWeekEnd.setDate(startWeekEnd.getDate() + (6 - startWeekDay));

							var endWeekStart =  new Date(endDate);
							endWeekStart.setDate(endWeekStart.getDate() - endWeekDay);

							setMidnight(startDate);
							setMidnight(endWeekStart);
							setMidnight(endDate, true);
							setMidnight(startWeekEnd, true);

							var startWeekEndDate = startWeekEnd.getDate();

							block.startDate = startDate;
							block.endDate = endDate;
							block.scope.header.title = translatedMonths[m] + ' ' + startDate.getFullYear();
							block.scope.header.subheaders.push({
								title: startWeekEndDate > 2 ? CalendarService.formatDate(startDate, true) : '',//firstWeekTitle,
								startDate: startDate,
								endDate: startWeekEnd,
								width: startWeekEndDate * dayWidth
							});

							var weeksBetween = (endWeekStart.getDate() - startWeekEnd.getDate() - 1) / 7;
							for (var i = 1; i <= weeksBetween; i++) {

								var hEndDate = new Date(startWeekEnd);
								hEndDate.setDate(hEndDate.getDate() + i * 7);

								var hStartDate = new Date(hEndDate);
								hStartDate.setDate(hStartDate.getDate() - 6);
								setMidnight(hStartDate);

								var h = {
									title: CalendarService.formatDate(hStartDate, true),
									startDate: hStartDate,
									endDate: hEndDate,
									width: 7 * dayWidth
								};

								block.scope.header.subheaders.push(h);
							}

							var endWeekDays = endDate.getDate() - endWeekStart.getDate() + 1;
							block.scope.header.subheaders.push({
								title: endWeekDays > 2 ? CalendarService.formatDate(endWeekStart, true) : '' ,
								startDate: endWeekStart,
								endDate: endDate,
								width: endWeekDays * dayWidth
							});

						};

						boardFunctions.getPreviousStartDate = function (nextStartDate) {
							var startDate = new Date(nextStartDate);
							startDate.setMonth(startDate.getMonth() - 1);

							return startDate;
						};

						boardFunctions.getNextStartDate = function (prevStartDate) {
							var startDate = new Date(prevStartDate);
							startDate.setMonth(startDate.getMonth() + 1);

							return startDate;
						};

						boardFunctions.determinePosition = function (card, block) {
							var cardDate = new Date(InputService.findProperty(card.data, conf.date));

							var subheader;
							var reverted = false;
							var shiftCard = 0;
							var shiftPoint = 0;

							var p = {
								card: undefined,
								pointer: undefined
							};

							var subheaders = block.scope.header.subheaders;
							for (var i = 0, l = subheaders.length; i < l; i++) {
								subheader = subheaders[i];

								if (cardDate.getTime() < subheaders[i].endDate.getTime()) {
									if (l === 5) {
										if (i > 2) {
											reverted = true;
											shiftCard += subheader.width - cardWidth;
										} else if (i === 2) {
											shiftCard -= 42;
											shiftPoint = 42;
										}
									} else if (l === 6) {
										if (i > 2) {
											reverted = true;
											shiftCard += subheader.width - cardWidth;
										}
									} else {
										if (i > 1) {
											reverted = true;
											shiftCard += subheader.width - cardWidth;
										}
									}

									break;
								} else {
									shiftCard += subheader.width;
								}
							}

							if (reverted) {
								shiftPoint += cardWidth - (subheader.endDate.getDate() - cardDate.getDate() + 1) * dayWidth;
							} else {
								shiftPoint += (cardDate.getDate() - subheader.startDate.getDate()) * dayWidth;
							}

							p.card = shiftCard;
							p.pointer = Math.round(shiftPoint);

							return p;
						};
					}

					angular.forEach(blocks, function (block) {
						block.scope.$destroy();
						block.$header.remove();
						block.$cards.remove();
					});

					blocksOrder = [];
					unassignedCards = [];
					blocks = {};
					cards = {};

					onWidthChange();
					angular.forEach(scope.cards, function (cardData) {
						boardCard(cardData);
					});

					element.scrollLeft(scope.options.scroll);
				}

				function constructColumnBlock() {
					var blockScope = scope.$new();
					blockScope.cards = [];
					var block = {
						id: UniqueService.get('board-block'),
						scope: blockScope,
						columns: 0,
						float: function () {
							block.$header.addClass('z-depth-1');
						},
						unfloat: function () {
							block.$header.removeClass('z-depth-1');
						},
						last: function (startDate) {
							block.initialize(startDate);

							block.$header.appendTo($boardHeader);
							block.$cards.appendTo($boardContent);

							blocksOrder.push(block.id);

							if (blocksOrder.length === 1) {
								scope.options.startDate = startDate;
							} else {
								scope.options.startDate = blocks[blocksOrder[0]].startDate;
							}

						},
						first: function (startDate) {
							block.initialize(startDate);

							block.$header.prependTo($boardHeader);
							block.$cards.prependTo($boardContent);

							blocksOrder.unshift(block.id);

							scope.options.startDate = startDate;
						},
						initialize: function (startDate) {
							var i = blocksOrder.length;
							while (i--) {
								 if (blocksOrder[i] === block.id) {
									 blocksOrder.splice(i, 1);
									 break;
								 }
							}

							block.unfloat();
							block.clearCards();
							blockScope.header = {
								title: '',
								subheaders: []
							};

							boardFunctions.initializeBlock(block, startDate);
							var totalWidth = 0;
							var i = blockScope.header.subheaders.length;
							while (i--) {
								totalWidth += blockScope.header.subheaders[i].width;
							}

							block.width = totalWidth + 6;
							block.$header.css({ 'width': totalWidth, 'min-width': totalWidth});
							block.$cards.css({ 'width': totalWidth, 'min-width': totalWidth});

						},
						clearCards: function () {
							var i = blockScope.cards.length;
							while (i--) {
								blockScope.cards[i].remove();
							}
						}
					};

					var headerBlock = HTMLService.initialize('div')
						.class('header align-center subtitle-2')
						.attr('tooltip', '{{header.title}}')
						.content('{{header.title}}')
						.element('div')
						.class('sub-headers')
						.inner(
							HTMLService.initialize('div')
								.class(headerAlignClass)
								.repeat('header in header.subheaders')
								.ngStyle('::{ width: header.width }')
								.attr('tooltip', '{{header.title}}')
								.content('{{header.title}}')
						).getHTML();

					var cardBlock = HTMLService.initialize('div')
						.class('board-card')
						.repeat('card in cards')
						.style('width: ' + cardWidth + 'px')
						.ngStyle('::{ left: card.position.card }')
						.inner(
							HTMLService.initialize('div')
								.class('card-pointer')
								.ngStyle('::{ left: card.position.pointer }')
								.inner(
									HTMLService.initialize('div').class('pointer').ngClass('getColor(card)')
										.element('div').class('line')
								)
								.element('div')
								.class('card-content')
								.attr('ng-dblclick', 'cardClick(card)')
								.ngClass('getColor(card)')
								.inner(
									HTMLService.initialize('div')
										.class('card-title ellipsis subtitle-1')
										.attr('tooltip', '{{card.data[title]}}')
										.content('{{card.data[title]}}')
										.element('div')
										.class('card-description caption')
										.attr('tooltip', '{{card.data[description]}}')
										.content('{{card.data[description]}}')
										.element('div')
										.class('card-chips')
										.inner(
											HTMLService.initialize('chip')
												.repeat('chip in card.data[chips]')
												.inner(
													HTMLService.initialize('chip-name')
														.attr('tooltip', '{{::chip.name}}')
														.bind('::chip.name')
												)
										)
								)
						).getHTML();

					toggleIndex = !toggleIndex;
					var zebraClass = toggleIndex ? ' striped' : '';

					block.$header = $compile(HTMLService.initialize('div')
						.class('column-block' + zebraClass)
						.attr('block-id', block.id)
						.content(headerBlock)
						.getHTML())(blockScope);

					block.$cards = $compile(HTMLService.initialize('div')
						.class('column-block align-column start' + zebraClass)
						.attr('block-id', block.id)
						.content(cardBlock)
						.getHTML())(blockScope);

					blocks[block.id] = block;
					return block;
				}

				setTimeout(function () {
					var $content = element.closest('content[view]');

					if (!$content.length) {
						return;
					}

					var boardId = UniqueService.get('board', true);
					var cTop = $content.offset().top;

					$content.on('scroll.' + boardId, function () {
						var tHeight = element.height();
						var startPosition = element.offset().top - cTop;

						var shift = calculateTop(startPosition, tHeight);
						$boardHeader.css({'transform': 'translateY(' + shift + 'px)'});

						shift ? $boardHeader.addClass('z-depth-1') : $boardHeader.removeClass('z-depth-1');
					});

					scope.$on('$destroy', function () {
						$content.off('scroll.' + boardId);
					});

					function calculateTop(start, height) {
						var s = -start;

						if (s < 0) {
							return 0;
						}

						var max = height - 256;
						max = max < 0 ? 0 : max;

						return s > max ? max : s;
					}
				});

				function setMidnight(date, before) {
					before ? date.setHours(23, 59, 59, 999) : date.setHours(0, 0, 0, 0);
				}

				function boardCard(data) {
					var i = InputService.findProperty(data, conf.self);
					var removeFromBlock = function (card) {
						var cards = card.block.scope.cards;
						var i = cards.length;
						while (i--) {
							if (cards[i].id === card.id) {
								cards.splice(i, 1);
								break;
							}
						}

						card.date = undefined;
						card.block = undefined;
					};

					var card = {
						position: {},
						remove: function () {
							removeFromBlock(card);
							unassignedCards.push(card.id);
						},
						delete: function () {
							if (card.block) {
								removeFromBlock(card);
							} else {
								var i = unassignedCards.indexOf(card.id);
								if (i !== -1) {
									unassignedCards.splice(i, 1);
								}
							}

							delete cards[card.id];
						},
						add: function (block, date) {
							if (card.block) {
								card.remove();
							}

							card.date = date;
							card.block = block;

							var i = unassignedCards.indexOf(card.id);
							if (i !== -1) {
								unassignedCards.splice(i, 1);
							}

							var wantedPosition = getCardIndex(card);

							var l_ = block.scope.cards.length;
							if (l_ === 0) {
								block.scope.cards.push(card);
							} else {
								var lastIndex = getCardIndex(block.scope.cards[l_ - 1]);

								if (lastIndex < wantedPosition) {
									block.scope.cards.push(card);
								} else {
									var firstIndex = getCardIndex(block.scope.cards[0]);

									if (firstIndex > wantedPosition) {
										block.scope.cards.unshift(card);
									} else {
										for (var i = 1; i < l_; i++) {
											var f = block.scope.cards[i - 1];
											var s = block.scope.cards[i];

											var j = getCardIndex(f);
											var k = getCardIndex(s);

											if (j < wantedPosition && k > wantedPosition) {
												block.scope.cards.splice(j, 0, card);
												break;
											}
										}
									}
								}
							}

							var p = boardFunctions.determinePosition(card, block);

							card.position.pointer = p.pointer;
							card.position.card = p.card;
						},
						id: typeof i === 'undefined' ? UniqueService.get('block-card') : Sanitizer(i),
						data: data
					};

					unassignedCards.push(card.id);
					cards[card.id] = card;

					determineCardColumn(card);

					return card;
				}

				function getCardIndex(card) {
					var found = false;
					var index = 0;

					angular.forEach(scope.cards, function (c) {
						if (!found) {
							if (c === card.data) {
								found = true;
							} else {
								index++;
							}
						}
					});

					return index;
				}

				function determineCardColumn(card) {
					angular.forEach(blocks, function (block) {
						var d = boardFunctions.determineColumn(card, block);
						if (d) {
							card.add(block, d);
						}
					});
				}

				function scrollingLeft(scroll) {
					var l = blocksOrder.length;
					var afterBlock = blocks[blocksOrder[0]];
					var threshold = Math.ceil(afterBlock.width / 2);
					if (scroll < threshold) {
						var shiftBlock = blocks[blocksOrder[l - 1]];

						var d = boardFunctions.getPreviousStartDate(afterBlock.startDate);
						shiftBlock.first(d);
						shiftBlock.scope.$digest();
						assignCards();

						return element.scrollLeft() + shiftBlock.width;
					}
				}

				function scrollingRight(scroll) {
					var l = blocksOrder.length;
					var beforeBlock = blocks[blocksOrder[l - 1]];
					var threshold = Math.floor(beforeBlock.width * 1.5);

					if (scroll > threshold) {
						var shiftBlock = blocks[blocksOrder[0]];

						var d = boardFunctions.getNextStartDate(beforeBlock.startDate);
						var shiftAmount = shiftBlock.width;

						shiftBlock.last(d);
						shiftBlock.scope.$digest();

						assignCards();

						return element.scrollLeft() - shiftAmount;
					}
				}

				function onWidthChange() {
					var firstDate = new Date(scope.options.startDate);
					boardWidth = element.width();
					var requiredBlocks = boardFunctions.onWidthChange();

					if (requiredBlocks) {
						requiredBlocks += 2;
						requiredBlocks -= blocksOrder.length;

						if (requiredBlocks & 1) {
							requiredBlocks++;
						}

						if (requiredBlocks > 0) {
							while (requiredBlocks--) {
								var b = constructColumnBlock();
								b.last(firstDate);

								boardFunctions.widthChangeNextBlock(firstDate);
							}
						}
					}
				}

				function assignCards() {
					var i = unassignedCards.length;
					while (i--) {
						determineCardColumn(cards[unassignedCards[i]]);
					}
				}

				Common.subscribeContentResize(scope, function () {
					onWidthChange();
					assignCards();
				});
			},
			controller: function ($scope) {
				if (typeof $scope.options !== 'object' || $scope.options === null) {
					$scope.options = {};
				}

				if (typeof $scope.options.columns === 'undefined') {
					$scope.options.columns = 'quartal';
				}

				$scope.options.startDate = new Date($scope.options.startDate);
			}
		}
	}
})();