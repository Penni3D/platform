﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputSmartTable', InputSmartTable);

	InputSmartTable.$inject = ['$compile', 'Translator', 'Common', 'HTMLService', 'Sanitizer'];
	function InputSmartTable($compile, Translator, Common, HTMLService, Sanitizer) {
		return {
			restrict: 'E',
			scope: {
				modify: '<?',
				selectOptions: '<?options',
				settings: '<?settings',
				restrictions: '<?',
				inputModel: '=model',
				modelStart: '=?',
				modelEnd: '=?',
				change: '&?onChange',
				changeParameters: '<?onChangeParameters',
				da: '=?'
			},

			link: function (scope, element, attrs) {
				let placeholder = typeof attrs.placeholder === 'undefined' ?
					Sanitizer(Translator.translate('NONE')) : Sanitizer(attrs.placeholder);
				let showUnderLine = false;

				if (typeof attrs.underline !== 'undefined' &&
					(attrs.type == '1' || attrs.type == 'integer' || attrs.type == '2' || attrs.type == 'decimal')
				) {
					showUnderLine = true;
				}

				let attributes = {
					label: attrs.label,
					suffix: attrs.suffix,
					max: attrs.max,
					min: attrs.min,
					precision: attrs.precision,
					step: attrs.step,
					self: attrs.self,
					process: attrs.process,
					parent: attrs.parent,
					group: attrs.group,
					underline: showUnderLine,
					'portfolio-id': attrs.portfolioId,
					'item-column-id': attrs.itemColumnId,
					'parent-process': attrs.parentProcess,
					'link-process': attrs.linkProcess,
					'option-value': attrs.optionValue,
					'option-name': attrs.optionName,
					'option-active': attrs.optionActive,
					'option-icon': attrs.optionIcon,
					'option-color': attrs.optionColor,
					'option-flag': attrs.optionFlag,
					'option-true': attrs.optionTrue,
					'option-false': attrs.optionFalse
				};

				let cattributes = {
					preload: attrs.preload,
					spellcheck: attrs.spellcheck,
					filterable: attrs.filterable,
					'required-field': attrs.requiredField,
					'watch-options': attrs.watchOptions,
					'manual-save': attrs.manualSave,
					'icon-only': attrs.iconOnly,
					'single-value': attrs.singleValue
				};

				let da = scope.da ? JSON.parse(scope.da) : {};
				if (da.showAll) cattributes.preload = true;
				let template = HTMLService.initialize('input-smart')
					.class('smart-table')
					.attr('minimal')
					.attr('chip','minimal')
					.attr('placeholder', placeholder)
					.attr('type', Sanitizer(attrs.type))
					.attr('model', 'inputModel')
					.attrConditional(typeof attrs.openable !== 'undefined', 'openable',Sanitizer(attrs.openable))
					.attrConditional(typeof attrs.unselectable !== 'undefined', 'unselectable',Sanitizer(attrs.unselectable))
					.attrConditional(typeof attrs.modelStart !== 'undefined', 'model-start', 'modelStart')
					.attrConditional(typeof attrs.modelEnd !== 'undefined', 'model-end', 'modelEnd')
					.attrConditional(typeof scope.selectOptions !== 'undefined', 'options', 'selectOptions')
					.attrConditional(typeof scope.settings !== 'undefined', 'settings', '::settings')
					.attrConditional(typeof scope.restrictions !== 'undefined', 'restrictions', 'restrictions')
					.attrConditional(typeof scope.change !== 'undefined', 'on-change', 'inputChanged')
					.attrConditional(typeof scope.modify !== 'undefined', 'modify', 'modify')

					.attrConditional(da.fromProcessColumnName, 'fromProcessColumnName', da.fromProcessColumnName)
					.attrConditional(da.fromProcessColumnId, 'fromProcessColumnId', da.fromProcessColumnId)
					.attrConditional(da.targetProcessColumnName, 'targetProcessColumnName', da.targetProcessColumnName)
					.attrConditional(da.targetProcessColumnId, 'targetProcessColumnId', da.targetProcessColumnId)
					.attrConditional(da.targetProcessName, 'targetProcessName', da.targetProcessName)
					.attrConditional(da.targetListColumn, 'targetListColumn', da.targetListColumn)
					.attrConditional(da.portfolioId, 'portfolioId', da.portfolioId);

				angular.forEach(attributes, function (v, k) {
					template.attrConditional(v, k, Sanitizer(v));
				});

				angular.forEach(cattributes, function (v, k) {
					template.attrConditional(typeof v !== 'undefined', k, v);
				});



				element.replaceWith($compile(template.getHTML())(scope));

				scope.inputChanged = function () {
					if (typeof scope.change === 'function' && typeof scope.change() === 'function') {
						scope.change()(scope.changeParameters);
					}
				};
			}
		};
	}
})();
