﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputCheck', inputCheck);

	inputCheck.$inject = ['InputService'];
	function inputCheck(InputService) {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				label: '@?',
				model: '=',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/bare/checkbox.html',
			link: function (scope, element, attrs) {
				var optionTrue = attrs.optionTrue ? attrs.optionTrue : true;
				var optionFalse = attrs.optionFalse ? attrs.optionFalse : false;

				scope.optionIndeterminate = attrs.optionIndeterminate ? attrs.optionIndeterminate : null;

				scope.inputChanged = function () {
					scope.model = scope.checked ?  optionTrue : optionFalse;
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				};

				scope.$watch('model', function () {
					scope.checked = scope.model == optionTrue;
				});
			},
			controller: function ($scope, UniqueService) {
				$scope.id = UniqueService.get('input-checkbox', true);
			}
		};
	}
})();
