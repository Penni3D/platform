(function () {
	'use strict';

	angular
		.module('core')
		.directive('chipMenu', ChipMenu);

	ChipMenu.$inject = [];
	function ChipMenu() {
		return {
			templateUrl: 'core/ui/input/bare/chip/chipMenu.html',
			link: function (scope, element) {
				scope.search = '';

				setTimeout(function () {
					var $input = element.find('input');
					$input.focus();

					$input.on('click', function () {
						$input.select();
					});
				}, 75);

				scope.onChange = function () {
					scope.chips = [];

					for (var i = 0, l = scope.options.length; i <l; i++) {
						var o = scope.options[i];
						if (o.toLowerCase().indexOf(scope.search.toLowerCase()) === -1) {
							continue;
						}

						var j = scope.model.length;
						while (j--) {
							if (scope.model[j].toLowerCase() === o.toLowerCase()) {
								break;
							}
						}

						if (j === -1) {
							scope.chips.push(scope.options[i]);
						}
					}
				};

				scope.onChange();
			}
		}
	}
})();