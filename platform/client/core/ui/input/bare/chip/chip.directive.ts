(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputChip', InputChip);

	InputChip.$inject = ['UniqueService', 'InputService'];
	function InputChip(UniqueService, InputService) {
		return {
			restrict: 'E',
			scope: {
				model: '=',
				modify: '<',
				label: '@?',
				options: '=',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/bare/chip/chip.html',
			controller: function ($scope) {
				$scope.id = UniqueService.get('input-chip', true);
			},
			link: function (scope, element, attrs) {
				let settings = {
					minimal: typeof attrs.minimal !== 'undefined'
				};
								
				element.on('click', 'input-container > i, label', function (event) {
					event.stopPropagation();
					event.preventDefault();
					InputService.chipMenu(scope.model, scope.options, settings, element, scope, addChip)
				});

				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				
				function addChip(chip) {
					scope.options = InputService.ensureArray(scope.options);

					if (!Array.isArray(scope.model)) scope.model = [];

					let lowered = chip.toLowerCase();
					let i = scope.model.length;
					while (i--) {
						if (scope.model[i].toLowerCase() === lowered) {
							break;
						}
					}

					if (i === -1) scope.model.push(chip);
					let j = scope.options.length;
					while (j--) {
						if (scope.options[j].toLowerCase() === lowered) {
							break;
						}
					}

					if (j === -1) scope.options.push(chip);
					if (i === -1 || j === -1) onChange();
				}

				scope.removeChip = function (index) {
					scope.model.splice(index, 1);
					onChange();
				};

				function onChange() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}
			}
		};
	}
})();