(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputChart', InputChart);


	InputChart.$inject = ['$rootScope', 'Common', 'InputService', 'SettingsService'];

	function InputChart($rootScope, Common, InputService, SettingsService) {
		return {
			restrict: 'E',
			scope: {
				chartId: '=',
				itemIds: '=', //Must be integer/string or integer/string array
				archive: '@?',
				label: '@?',
				process: '=?',
				options: '=?'
			},
			templateUrl: 'core/ui/input/bare/chart.html',
			link: function (scope, element, attrs) {
				if (!scope.process && scope.$parent.data && scope.$parent.data.process)
					scope.process = scope.$parent.data.process;
				if (!scope.process && scope.$parent.$parent && scope.$parent.$parent.process)
					scope.process = scope.$parent.$parent.process;
				scope.inMeta = scope.itemIds && scope.itemIds > 0;
				SettingsService.ProcessDashboardGetChart(scope.process, scope.chartId).then(function (chart) {
					chart.filterItemIds = scope.itemIds;
					chart.archive = scope.archive;
					scope.chart = chart;
				});

				//Tutkii, onko chartin parametrit muuttuneet
				scope.$watch('chartId', function () {
					scope.inputChanged();
				});
				scope.$watch('itemIds', function () {
					scope.inputChanged();
				});
				$rootScope.$on('inputChart-changed', function (event, val) {
					if (scope.$$destroyed == false) {
						scope.chart = null;
						SettingsService.ProcessDashboardGetChart(scope.process, scope.chartId).then(function (chart) {
							chart.filterItemIds = scope.itemIds;
							scope.chart = chart;
						});
					}
				});
			},
			controller: function ($scope, InputService, UniqueService) {
				$scope.id = UniqueService.get('input-chart', true);
				$scope.inputChanged = function () {
				};
			}
		};
	}
})();