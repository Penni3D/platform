﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputFile', InputFile)
		.controller('FileLightboxVideo', FileLightboxVideo)
		.controller('FileLightbox', FileLightbox);

	InputFile.$inject = ['AttachmentService', 'MessageService', 'Translator', 'InputService', 'Upload', '$q', '$rootScope'];
	function InputFile(AttachmentService, MessageService, Translator, InputService, Upload, $q, $rootScope) {
		return {
			restrict: 'E',
			scope: {
				reloadBool: '=?',
				modify: '<',
				label: '@?',
				restrictions: '<?',
				onChange: '&?',
				onChangeParameters: '<?',
				settings: '<?',
				multiSelect: '<?',
				skipDialog: '<?',
				fileTypes: '@?'
			},
			templateUrl: 'core/ui/input/bare/file/file.html',
			link: function (scope, element, attrs) {
				scope.bigThumbNails = attrs.bigthumbnails;
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				scope.attachments = [];
				scope.loading = true;
				scope.max = typeof attrs.max === 'undefined' ? 0 : Number(attrs.max);
				if (typeof scope.restrictions === 'undefined') scope.restrictions = {};
				if (typeof scope.multiSelect === 'undefined') scope.multiSelect = true;
				if (typeof scope.skipDialog === 'undefined') scope.skipDialog = false;
				if (typeof scope.fileTypes === 'undefined') scope.fileTypes = '';

				scope.maxSelections = 10;
				if (!scope.multiSelect) scope.maxSelections = 1;

				let showCropper = !(attrs.hasOwnProperty('dialog') && attrs.dialog == "true");

				scope.archiveDate = '';
				if (!!scope.settings.archive) scope.archiveDate = '/' + scope.settings.archive;

				let parent = attrs.parent;
				let group = attrs.group;
				let self = attrs.self;
				let itemColumnId = attrs.itemColumnId;

				function loadAttachmentData() {
					scope.loading = true;
					scope.attachments = [];
					scope.thumbnailAttachments = [];

					let p = typeof group === 'undefined' ?
						AttachmentService.getItemAttachments(self, itemColumnId, scope.settings.archive) :
						AttachmentService.getControlAttachments(parent, group, scope.settings.archive);

					p.then(function (attachments) {
						for (let i = 0, l = attachments.length; i < l; i++) {
							let attachment = attachments[i];
							attachment.hasData = !!attachment.AttachmentData;

							if(attrs.bigthumbnails){
								if(attachment.hasData && attachment.AttachmentData.HasThumbnail){
									scope.thumbnailAttachments.push(attachment)
								} else {
									scope.attachments.push(attachment);
								}
							} else {
								scope.attachments.push(attachment);
							}
						}

						scope.$watchCollection('attachments', function () {
							scope.contains = scope.attachments.length ? 'set' : '';
						});

						scope.loading = false;
					});
				}

				loadAttachmentData();

				if (typeof scope.reloadBool !== 'undefined') {
					scope.$watch("reloadBool", function (newValue, oldValue) {
						if (newValue == true) {
							loadAttachmentData();
							scope.reloadBool = false;
						}
					});
				}

				
				let ffs = [];
				
				scope.uploadFiles = function (files) {
					if (files && files.length && showCropper) {
						MessageService.dialogAdvanced({
							template: 'core/ui/input/bare/file/fileCrop.html',
							controller: 'FileCropController',
							locals: {
								Files: files,
								Attachments: { '1': scope.attachments, '2': scope.thumbnailAttachments },
								OnChange: onChange,
								Params: {
									restrictions: scope.restrictions,
									name: group,
									itemId: self,
									itemColumnId: itemColumnId,
									parentId: parent,
									cropEnabled: attrs.cropenabled,
									bigThumbNails: attrs.bigthumbnails,
									skipDialog: scope.skipDialog
								}
							}
						});
					} else {
							let promises = [];
							_.each(files, function (file) {
								let d = {
									file: file,
									type: 0,
									url: null,
									name: file.name,
									itemId: undefined,
									itemColumnId: undefined,
									parentId: undefined,
									controlName: undefined,
									thumbnail: undefined,
									crop: 0
								};

								d["itemId"] = attrs.self;
								d["itemColumnId"] = itemColumnId;

								if (attrs.uploadondemand != "true") {
									promises.push(Upload.upload({
										data: d,
										url: "v1/meta/attachments"
									}));
								} else {
									ffs.push(d);
									scope.attachments.push({
											'FileName':file.name,
											'Name': file.name,
											'AttachmentData': {
												'HasThumbnail': false,
												'ContentType': file.type
											},
										'hasData':true,
										'draft': attrs.uploadondemand,
										'lastModified':file.lastModified
									});
								}
							})
							if(attrs.uploadondemand){
								AttachmentService.setOnDemandFiles(ffs, attrs.self);
							} else {
								$q.all(promises).then(function () {
									$rootScope.$broadcast('RefreshAllAttachments', {});
								})
							}
					}
				};

				scope.$on('RefreshAllAttachments', function (event, opt) {
					loadAttachmentData()
				});

				scope.removeAttachment = function (attachment, index) {
					if(attachment.hasOwnProperty('draft') && attachment.draft == "true"){
						scope.attachments.splice(index, 1);
						AttachmentService.clearOnDemandFiles(attrs.self, attachment.lastModified)
					} else {
						MessageService.confirm(Translator.translate('FILE_INPUT_ATTACHMENT_DELETE') + ' ' + attachment.Name + '?',
							function () {
								let i = scope.bigThumbNails && attachment.AttachmentData.HasThumbnail ? scope.thumbnailAttachments.length : scope.attachments.length;
								let whichArray = scope.bigThumbNails && attachment.AttachmentData.HasThumbnail ? scope.thumbnailAttachments : scope.attachments
								while (i--) {
									if (whichArray[i].AttachmentId == attachment.AttachmentId) {
										whichArray.splice(i, 1);
										break;
									}
								}
								return AttachmentService.deleteAttachment(attachment.AttachmentId).then(onChange);
							});
					}
				};

				function onChange() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}

				scope.checkMax = function () {
					if (!scope.multiSelect && (scope.thumbnailAttachments.length > 0 || scope.attachments.length > 0)) {
						return false;
					}
					else {
						return true;
					}
				};
			},
			controller: function ($scope, $window) {
				if (typeof $scope.settings === 'undefined') $scope.settings = {};
				$scope.archiveDate = '';
				if (!!$scope.settings.archive) {
					$scope.modify = false;
					$scope.archiveDate = '/' + $scope.settings.archive;
				}
				$scope.downloadAttachment = function (attachment) {
					openFile(attachment);
				};

				$scope.showVideo = function(attachment, archiveDate){
					AttachmentService.showVideo(attachment, archiveDate);
				}

				$scope.activeMouse = {};
				$scope.enter = function(attachment){
					$scope.activeMouse[attachment.AttachmentId] = true;
				}
				$scope.leave = function(attachment){
					$scope.activeMouse[attachment.AttachmentId] = false;
				}

				$scope.showAttachment = function (attachment, archiveDate) {
					AttachmentService.showAttachment(attachment.AttachmentId, archiveDate);
				};

				function openFile(attachment) {
					$window.open('v1/meta/AttachmentsDownload/' + attachment.AttachmentId + '/0' + $scope.archiveDate, '_blank');
				}
			}
		};
	}

	FileLightbox.$inject = ['$scope', 'Id', 'ArchiveDate', 'MessageService', '$window'];
	function FileLightbox($scope, Id, ArchiveDate, MessageService, $window) {
		$scope.attachmentId = Id;
		$scope.archiveDate = ArchiveDate;

		$scope.close = function () {
			MessageService.closeDialog();
		};

		$scope.showImage = function () {
			$window.open('v1/meta/AttachmentsDownload/' + Id + '/0' + $scope.archiveDate, '_blank');
		};
	}

	FileLightboxVideo.$inject = ['$scope', 'Attachment', 'ArchiveDate', 'MessageService', '$window'];
	function FileLightboxVideo($scope, Attachment, ArchiveDate, MessageService, $window) {

		$scope.close = function () {
			MessageService.closeDialog();
		};

		$scope.download = function () {
			$window.open('v1/meta/AttachmentsDownload/' + Attachment.AttachmentId + '/0' + ArchiveDate, '_blank');
		};

		let videoLink = 'v1/meta/AttachmentsDownload/' + Attachment.AttachmentId + '/0' + ArchiveDate

		$scope.videoContent = '<video type="video/mp4" src="' + videoLink + '" controls preload="metadata" loop class="stretch">' +
			'</video>'
	}

})();
