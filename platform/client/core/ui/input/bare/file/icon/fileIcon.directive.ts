(function () {
	'use strict';

	angular
		.module('core')
		.directive('fileIcon', FileIcon);

	FileIcon.$inject = ['$templateCache', '$compile'];

	function FileIcon($templateCache, $compile) {
		let knownMimes = {
			'application/msword': 'doc',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
			'application/vnd.ms-excel': 'xls',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx',
			'application/vnd.ms-powerpoint': 'ppt',
			'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'pptx',
			'application/vnd.openxmlformats-officedocument.presentationml.slideshow': 'ppsx',

			'application/vnd.oasis.opendocument.text': 'odt',
			'application/vnd.oasis.opendocument.spreadsheet	': 'ods',
			'application/vnd.oasis.opendocument.presentation': 'odp',

			'text/plain': 'txt',
			'application/pdf': 'pdf',
			'application/x-msdownload': 'exe'
		};


		return {
			link: function (scope, element) {
				let attachment = scope.hasOwnProperty('attachment') ? scope.attachment : scope.thumbAttch;
				let iconFile = 'core/ui/input/bare/file/icon/generalIcon.html';
				scope.isDraft = attachment.hasOwnProperty('draft') && attachment.draft == "true";
				if (attachment.hasData) {
					let contentType = attachment.AttachmentData.ContentType;
					if (_.startsWith(contentType, 'image')) {
						if (attachment.AttachmentData.HasThumbnail) {
							iconFile = 'core/ui/input/bare/file/icon/imageIcon.html';
						} else {
							iconFile = 'core/ui/input/bare/file/icon/imageIconNoThumb.html';
						}
					}
					else if(contentType == 'video/mp4'){
						iconFile = 'core/ui/input/bare/file/icon/showVideo.html';
					}
					else if (knownMimes[contentType]) {
						attachment.mime = knownMimes[contentType];
						iconFile = 'core/ui/input/bare/file/icon/mimeIcon.html';
					}
				} else {
					iconFile = 'core/ui/input/bare/file/icon/emptyIcon.html';
				}

				element.append($compile($templateCache.get(iconFile))(scope))
			}
		};
	}
})();