﻿(function () {
	'use strict';

	angular
		.module('core.dialogs')
		.controller('FileCropController', FileCropController);

	FileCropController.$inject = ['$scope', 'MessageService', 'Upload', 'Files', 'Attachments', 'Params', '$q', 'OnChange'];

	function FileCropController($scope, MessageService, Upload, Files, Attachments, Params, $q, OnChange) {

		let thumbnails = [];
		let data = {};
		let cropEnabled = 0;
		let bigThumbNails = false;
		if (Params.cropEnabled) cropEnabled = Params.cropEnabled;
		if (Params.bigThumbNails) bigThumbNails = true;

		$scope.uploading = false;
		$scope.total = Files.length;
		$scope.index = 0;
		$scope.cropRestrictions = Params.restrictions;

		function saveData() {
			Files[$scope.index].nameInput = $scope.name;
			if (_.startsWith(Files[$scope.index].type, 'image')) {
				data[$scope.index] = $scope.cropData;
				thumbnails[$scope.index] = $scope.cropImage;
				$scope.cropImage = null;
			}
		}

		function setCrop() {
			if (cropEnabled == 1) {
				$scope.cropInitial = data[$scope.index];
				$scope.file = Files[$scope.index];

				let n = Files[$scope.index].nameInput;
				$scope.name = n ? n : $scope.file.name;
			} else {
				let n = Files[$scope.index].nameInput;
				$scope.name = n ? n : Files[$scope.index].name;
			}
		}

		setCrop();

		$scope.next = function () {
			saveData();
			if ($scope.index === $scope.total - 1) {
				upload().then(function (attachments) {
					for (let i = 0, l = attachments.length; i < l; i++) {
						angular.forEach(attachments[i].data, function (value) {
							value.hasData = !!value.AttachmentData;
							if (value.hasData && value.AttachmentData.ContentType.includes('image')) {
								value.AttachmentData.HasThumbnail = true;
							}
							if (bigThumbNails && value.hasData && value.AttachmentData.ContentType.includes('image')) {
								Attachments["2"].push(value);
							}
							else {
								Attachments["1"].push(value);
							}
						});
					}
					MessageService.closeDialog();
					OnChange();
				});
			} else {
				$scope.index++;
				setCrop();
			}
		};

		$scope.prev = function () {
			saveData();

			if ($scope.index !== 0) {
				$scope.index--;
			}

			setCrop();
		};

		$scope.cancel = function () {
			MessageService.closeDialog($scope);
		};

		$scope.isAllProcessed = function () {
			return $scope.index === $scope.total - 1;
		};

		function upload() {
			let promises = [];
			$scope.uploading = true;

			for (let i = 0; i < $scope.total; i++) {
				let d = {
					file: Files[i],
					type: 0,
					url: null,
					name: Files[i].nameInput,
					itemId: undefined,
					itemColumnId: undefined,
					parentId: undefined,
					controlName: undefined,
					thumbnail: undefined,
					crop: 0
				};

				if (Params.name) {
					d.itemId = null;
					d.itemColumnId = null;
					d.parentId = Params.parentId;
					d.controlName = Params.name;
				} else {
					d.itemId = Params.itemId;
					d.itemColumnId = Params.itemColumnId;
					d.parentId = null;
					d.controlName = null;
				}

				if (Params.cropEnabled) d.crop = Params.cropEnabled;

				if (thumbnails[i]) {
					d.thumbnail = Upload.dataUrltoBlob(thumbnails[i], Files[i].nameInput);
				}

				promises.push(Upload.upload({
					data: d,
					url: "v1/meta/attachments"
				}));
			}

			return $q.all(promises);
		}

		if (Params.skipDialog) $scope.next();
	}
})();