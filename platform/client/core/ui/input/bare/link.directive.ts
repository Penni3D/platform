(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputLink', InputLink);

	InputLink.$inject = ['AttachmentService', 'MessageService', 'Translator', 'InputService'];
	function InputLink(AttachmentService, MessageService, Translator, InputService) {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				label: '@?',
				onChange: '&?',
				onChangeParameters: '<?',
				settings: '<?'
			},
			templateUrl: 'core/ui/input/bare/link.html',
			link: function (scope, element, attrs) {
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				scope.links = [];
				scope.loading = true;
				scope.max = typeof attrs.max === 'undefined' ? 0 : Number(attrs.max);

				let parent = attrs.parent;
				let group = attrs.group;
				let self = attrs.self;
				let itemColumnId = attrs.itemColumnId;

				let p = typeof group === 'undefined' ?
					AttachmentService.getItemLinks(self, itemColumnId, scope.settings.archive) :
					AttachmentService.getControlLinks(parent, group, scope.settings.archive);

				p.then(function (links) {
					for (let i = 0, l = links.length; i < l; i++) {
						scope.links.push(links[i]);
					}

					scope.$watchCollection('links', function () {
						scope.contains = scope.links.length ? 'set' : '';
					});

					scope.loading = false;
				});

				scope.addLink = function () {
					let model = {
						name: '',
						link: ''
					};

					let modifies = {
						add: false,
						cancel: true
					};

					let onChange = function () {
						modifies.add = !!(model.name.length && model.link.length);
					};

					let params = {
						title: 'LINK_INPUT_ADD_LINK',
						inputs: [{
							type: 'string',
							model: 'name',
							label: 'LINK_INPUT_NAME'
						}, {
							type: 'string',
							model: 'link',
							label: 'LINK_INPUT_LINK'
						}],
						buttons: [{
							text: 'MSG_CANCEL',
							modify: 'cancel'
						}, {
							type: 'primary',
							text: 'LINK_INPUT_ADD',
							modify: 'add',
							onClick: function () {
								let p = typeof group === 'undefined' ?
									AttachmentService.addItemLink(self, itemColumnId, model.name, model.link) :
									AttachmentService.addControlLinks(parent, group, model.name, model.link);

								return p.then(function (linkId) {
									let l = {
										AttachmentId: linkId,
										ControlName: group,
										ItemColumnId: itemColumnId,
										ItemId: self,
										Name: model.name,
										ParentId: parent,
										Url: model.link
									};

									scope.links.push(l);
									onChange();
								});
							}
						}]
					};

					MessageService.dialog(params, model, onChange, modifies);
				};

				scope.removeLink = function (link) {
					MessageService.confirm(Translator.translate('LINK_INPUT_DELETE_LINK') + ' ' + link.Name + '?',
						function () {
							let i = scope.links.length;
							while (i--) {
								if (scope.links[i].AttachmentId == link.AttachmentId) {
									scope.links.splice(i, 1);
									break;
								}
							}

							return AttachmentService.deleteLink(link.AttachmentId).then(onChange);
						});
				};

				function onChange() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}
			},
			controller: function ($scope) {
				if (typeof $scope.settings === 'undefined') {
					$scope.settings = {};
				}

				if (!!$scope.settings.archive) {
					$scope.modify = false;
				}

				$scope.goToUrl = function (link) {
					AttachmentService.goToUrl(link.Url);
				};
			}
		}
	}
})();