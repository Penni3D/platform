﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputText', InputText);

	InputText.$inject = ['Common', '$timeout'];

	function InputText(Common, $timeout) {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				model: '=',
				label: '@?',
				max: '@?',
				placeholder: '@?',
				onChange: '&?',
				onChangeParameters: '<?'
			},

			templateUrl: function (element, attrs) {
				if (typeof attrs.table !== 'undefined') {
					return 'core/ui/input/bare/textarea-table.html'
				}
				return 'core/ui/input/bare/textarea.html'
			},
			link: function (scope, element, attrs) {
				scope.ta = element.find('textarea')[0]
				scope.container = element.find('input-container')[0];

				let min_rows = attrs.rows === undefined || attrs.rows < 2 ? 2 : attrs.rows;
				element.find("p.body-1").css("min-height", min_rows * 19 + 1 + "px");

				if (navigator.userAgent.indexOf("Firefox") > -1) {
					scope.ta.style.minHeight = min_rows * 19 + 1 + "px";
					scope.container.style.minHeight = min_rows * 19 + 47 + "px";
				}
				else {
					scope.ta.style.minHeight = min_rows * 19 + 7 + "px";
					scope.container.style.minHeight = min_rows * 19 + 53 + "px";
				}

				function reSizeArea() {
					scope.ta.style.height = 'auto';
					if (navigator.userAgent.indexOf("Firefox") > -1) {
						scope.ta.style.height = scope.ta.scrollHeight + 7 + "px";
						scope.container.style.height = scope.ta.scrollHeight + 47 + "px";
					}
					else {
						scope.ta.style.height = scope.ta.scrollHeight + 1 + "px";
						scope.container.style.height = scope.ta.scrollHeight + 47 + "px";
					}
				}

				let isMinimal = typeof attrs.minimal !== 'undefined';
				scope.isTable = typeof attrs.table !== 'undefined';

				if(!scope.isTable && scope.modify){
					Common.subscribeContentResize(scope, function(){
						reSizeArea();
					});
					scope.ta.addEventListener('input', function(){
						reSizeArea();
					}, false);
				}

				scope.tableEdit = false;
				let rows = attrs.rows ? attrs.rows : isMinimal ? 3 : 5;

				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				scope.spellcheck = typeof attrs.spellcheck !== 'undefined';

				let c = element.children('input-container');
				
				//Table Mode Height Functions
				scope.h = 0;
				scope.determineHeight = function () {
					let ph = element.find('p')[0].scrollHeight;
					let rh = element.parent().parent().parent()[0].offsetHeight;
					scope.h = (ph > rh ? ph : rh) + 10;
				};
				if (scope.isTable) $timeout(scope.determineHeight, 0);
				scope.getRowHeight = function () {
					if (scope.h > 140) scope.h = 140;
					if (!scope.modify) scope.h = 44; 
					return scope.h + "px";
				};
				scope.editTable = function () {
					if (scope.modify) {
						scope.tableEdit = true;
						element.find('textarea').css('height', scope.h - 15);
						$timeout(function () {
							element.find('textarea').focus();
						}, 10);
					}
				};

				if (!scope.label || scope.isTable) scope.placeholderValue = scope.placeholder;
				scope.onBlur = function () {
					if (scope.label) scope.placeholderValue = '';
					scope.tableEdit = false;
					scope.determineHeight();
					reSizeArea();
				};
				scope.onFocus = function () {
					if (scope.label) scope.placeholderValue = scope.placeholder;
					reSizeArea();
				};

				if (!scope.isTable) {
					scope.$watch('modify', function () {
						if (Common.isTrue(scope.modify)) {
							let rowHeight = rows * 18;
							c.css('height', rowHeight + (isMinimal ? 26 : 54));
							element.find('textarea').css('height', rowHeight + 8);
						} else {
							c.css('height', 'auto');
						}
					});
				} else {
					scope.$watch('model', function () {
						if (!scope.editTable) reSizeArea();
						if (!scope.editTable) scope.determineHeight();
					});
				}
				if(!scope.isTable && scope.modify){
					setTimeout(reSizeArea, 0)
				}
			},
			controller: function ($scope, InputService, UniqueService) {
				$scope.id = UniqueService.get('input-text', true);

				$scope.inputChanged = function () {
					InputService.inputChanged($scope.onChange, $scope.onChangeParameters);
				};
			}
		};
	}
})();
