(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputMatrix', InputMatrix);

	InputMatrix.$inject = ['MatrixProcessService', 'ProcessService', '$q', 'InputService', 'Common', '$rootScope', 'UserService', 'CalendarService'];

	function InputMatrix(MatrixProcessService, ProcessService, $q, InputService, Common, $rootScope, UserService, CalendarService) {
		var matrixDataProcess = 'matrix_process_row_data';

		return {
			restrict: 'E',
			scope: {
				modify: '<',
				label: '@?',
				onChange: '&?',
				onChangeParameters: '<?',
				archiveDate: '@?',
				oneMandatory: '@?'
			},
			templateUrl: 'core/ui/input/bare/matrix.html',
			link: function (scope, element, attrs) {

				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				scope.loading = true;
				let promises = [];
				let selectedQuestions = {};
				//let unselectable = scope.requiredField ? false : typeof attrs.unselectable !== 'undefined';
				let unselectable = true;
				let maxScore = 0;
				scope.notChangingData = true;
				promises.push(MatrixProcessService.getMatrixRows(attrs.process, true).then(function (structure) {
					if(structure.customWidthRows == true){
						calcCustomWidths(structure.columns, structure['columns'][0].custom_width, 'row')
					} else if (structure.customWidthColumns == true){
						calcCustomWidths(structure.columns, structure['columns'][1].custom_width, 'columns')
					}

					maxScore = structure.rows.length * _.maxBy(structure.columns, function(o) { return o.score; }).score;
					scope.matrix = structure;
				}));

				promises.push(MatrixProcessService.getMatrixRowData(attrs.parent, attrs.itemColumnId, attrs.archiveDate));

				promises.push(ProcessService.getItemData(attrs.parentProcess, attrs.parent).then(function(data){
					scope.parentProcessData = data;
				}));

				let calcCustomWidths = function(columns, masterWidth, type){
					//TWO VARIABLES FOR EASIER UNDERSTANDING AND DEBUGGING. I AM AWARE OF TERNARY OPERATOR.
					//for columns if width set for questions
					let widthForTheRest = (100 - masterWidth) / (columns.length - 1);

					//for questions if custom width set for columns
					let widthForTheRest2 = 100 - (masterWidth * (columns.length - 1));

					_.each(columns, function(col, index){
						if(index != "0" && type == 'row'){
							col.temp_custom_width = widthForTheRest;
						} else if (index == "0" && type == 'columns'){
							col.temp_custom_width = widthForTheRest2;
						} else {
							col.temp_custom_width = col.custom_width;
						}
					})

					if(attrs.showLastModified){
						_.each(columns, function(col){
							col.temp_custom_width = col.temp_custom_width - (20 / columns.length)
						})
					}
				}

				$q.all(promises).then(function (resolves) {
					let uniqueUsers = [];
					angular.forEach(resolves[1], function (selected) {
						selectedQuestions[selected.question_item_id] = selected;
						angular.forEach(scope.matrix['rows'], function (row) {
							if(row.item_id == selected.question_item_id){
								selectedQuestions[selected.question_item_id].weighting = row.weighting;
							}
							angular.forEach(row.$descriptions, function (desc) {
								if (desc.item_id == selected.cell_item_id) {
									desc.selected = true;
								}
								if(row.item_id == selected.question_item_id){
									row.comments = selected.comments
									if(!uniqueUsers.includes(selected.archive_userid))uniqueUsers.push(selected.archive_userid);
									row.$$archive_start = selected.archive_start;
									row.$$archive_userid = selected.archive_userid;
								}

							});
						})
					});

					angular.forEach(scope.matrix['rows'], function (row) {
						angular.forEach(row.$descriptions, function (desc) {
							if(desc.tooltip == null) desc.tooltip = desc.name;
						});
					})

					isAllSelected();
					setupScoreCalculation();
					getUsers(uniqueUsers);
					scope.loading = false;
				});

				let itemsToDelete = [];
				let going = false;

				let deleteFunction = function(){
					if(!going && itemsToDelete.length){
						going = true;
						ProcessService.deleteItem(matrixDataProcess, itemsToDelete[0]).then(function () {
							going = false;
							itemsToDelete.splice(0, 1);
							deleteFunction();
							calculateScore();
						});
					} else {
						scope.notChangingData = true;
						return;
					}
				};

				scope.cellClick = function (cell, row) {

					if (!scope.modify) {
						return;
					}

					if(!scope.notChangingData) return;

					scope.notChangingData = false;

					var qid = cell.question_item_id;

					if (unselectable && cell.selected) {
						cell.selected = false;
						var question = selectedQuestions[qid];

						itemsToDelete.push(question.item_id);

						deleteFunction();

						delete selectedQuestions[qid];
					} else if (!cell.selected) {

						_.each(row.$descriptions, function (c) {
							c.selected = false;
						});

						cell.selected = true;
						cell.weighting = row.weighting;

						if (typeof selectedQuestions[qid] === 'undefined') {
							var data = {
								Data: {
									owner_item_id: attrs.parent,
									question_item_id: qid,
									score: cell.$parentColumn.score,
									item_column_id: attrs.itemColumnId,
									cell_item_id: cell.item_id
								}
							};

							ProcessService.newItem(matrixDataProcess, data).then(function (createdId) {
								scope.notChangingData = true;
								ProcessService.getItem(matrixDataProcess, createdId).then(function (item) {
									selectedQuestions[item.question_item_id] = item;
									item.weighting = row.weighting;
									calculateScore();
									isAllSelected();
								});
							}, function (error) {
								if (error.data.Type == 'UNIQUE') {
									//ProcessService.setDataChanged('matrix_process_row_data', cell.item_id);
								}
							});
						} else {
							updateCurrent(cell);
							calculateScore();
						}
					}

					inputChanged();
					isAllSelected();
				};

				scope.scoreType;
				var scoreColumn;
				scope.scoreColumnTranslation;
				scope.totalScore = 0;
				scope.showScore = false;
				scope.showComment = false;
				scope.showLastModified = false;
				scope.hideScore = false;
				scope.hideHeader = false;
				scope.userNames = {};

				function getUsers (idArray){
					UserService.getUsers(idArray).then(function(users){
						_.each(users, function(user){
							if(!scope.userNames[user.item_id] && user.primary){
								scope.userNames[user.item_id] = user.primary;
							}
						})
					})
				}
				function setupScoreCalculation() {
					return ProcessService.getColumn(attrs.parentProcess, attrs.itemColumnId).then(function (column) {
						if (column.DataAdditional2 != null) {
							let da2 = JSON.parse(column.DataAdditional2);

							if(da2.hasOwnProperty('ShowComment') && da2.ShowComment == 1){
								scope.showComment = true;
							}

							if(da2.hasOwnProperty('ShowLastModified') && da2.ShowLastModified == 1){
								scope.showLastModified = true;
							}

							if(da2.hasOwnProperty('HideScore') && da2.HideScore == 1){
								scope.hideScore = true;
							}

							if(da2.hasOwnProperty('HideHeader') && da2.HideHeader == 1){
								scope.hideHeader = true;
							}

							if(da2.hasOwnProperty('Type') && da2.hasOwnProperty('ItemColumnId')){
								scope.showScore = true;
								scope.scoreType = da2.Type;
								ProcessService.getColumn(attrs.parentProcess, da2.ItemColumnId).then(function (totalScoreColumn) {
									scoreColumn = totalScoreColumn.Name;
									scope.scoreColumnTranslation = totalScoreColumn.LanguageTranslation[$rootScope.me.locale_id];
									calculateScore();
								});
							}
						}
					});
				};

				function calculateScore() {
					scope.totalScore = 0;
					if(!_.isNil(scoreColumn) && !_.isNil(scope.scoreType)) {
						_.each(selectedQuestions, function (selected) {
							if(selected.weighting == null) selected.weighting =1;
							scope.totalScore = scope.totalScore + (selected.score * selected.weighting);
						});
						if (scope.scoreType == 2) {
							scope.totalScore = scope.totalScore / _.size(selectedQuestions);
						} else if (scope.scoreType == 3){
							scope.totalScore = scope.totalScore / maxScore * 100;
						}
						scope.totalScore = Common.rounder(scope.totalScore, 2);
						scope.parentProcessData.Data[scoreColumn] = scope.totalScore;
						ProcessService.setDataChanged(attrs.parentProcess, scope.parentProcessData.Data.item_id);
					}
				};

				function isAllSelected() {
					if(attrs.oneMandatory != "true"){
						var allSelected = true;
						for (var i = 0, l = scope.matrix.rows.length; i < l; i++) {
							if (typeof selectedQuestions[scope.matrix.rows[i].item_id] === 'undefined') {
								allSelected = false;
							}
						}
						scope.contains = allSelected ? 'set' : '';
					} else {
						let oneSelected = false;
						for (var i = 0, l = scope.matrix.rows.length; i < l; i++) {
							if (typeof selectedQuestions[scope.matrix.rows[i].item_id] !== 'undefined') {
								oneSelected = true;
							}
						}
						scope.contains = oneSelected ? 'set' : '';
					}
				}

				function updateCurrent(cell) {
					var currentSelection = selectedQuestions[cell.question_item_id];
					scope.notChangingData = true;
					currentSelection.cell_item_id = cell.item_id;
					currentSelection.score = cell.$parentColumn.score;
					ProcessService.setDataChanged(matrixDataProcess, currentSelection.item_id);
				}

				scope.updateComment = function(row){
					if (typeof selectedQuestions[row.item_id] === 'undefined') {
						var data = {
							Data: {
								owner_item_id: attrs.parent,
								question_item_id: row.itemId,
								score: null,
								item_column_id: attrs.itemColumnId,
								cell_item_id: null,
								comments: row.comments
							}
						};
						selectedQuestions[row.item_id] = data.Data;

						ProcessService.newItem(matrixDataProcess, data).then(function (createdId) {
							ProcessService.getItem(matrixDataProcess, createdId).then(function (item) {
								selectedQuestions[item.question_item_id] = item;
								item.weighting = row.weighting;
							});
						}, function (error) {
							if (error.data.Type == 'UNIQUE') {
							}
						});
					} else {
						let cs = selectedQuestions[row.item_id];
						cs.comments = row.comments
						ProcessService.setDataChanged(matrixDataProcess, cs.item_id);
					}
				}

				scope.formatDateTime = function(dt){
					return CalendarService.formatDateTime(dt);
				}

				function inputChanged() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}
			},
			controller: function ($scope, UniqueService) {
				$scope.id = UniqueService.get('input-matrix', true);
			}
		};
	}
})();
