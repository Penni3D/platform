﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputHidden', InputHidden);

	InputHidden.$inject = [];
	function InputHidden() {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				model: '=',
				label: '@?',
				placeholder: '@?',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/bare/hidden/hidden.html',
			link: function (scope, element, attrs) {
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
			},
			controller: function ($scope, MessageService, InputService, UniqueService, Common) {
				$scope.id = UniqueService.get('input-hidden', true);
				let modelCopy = { value: $scope.model };

				$scope.openDialog = function () {
					if (Common.isTrue($scope.modify)) {
						MessageService.dialogAdvanced({
							controller: 'HiddenQueryController',
							template: 'core/ui/input/bare/hidden/hiddenQuery.html',
							locals: {
								Callback: function () {
									$scope.model = modelCopy.value;
									InputService.inputChanged($scope.onChange, $scope.onChangeParameters);
								},
								Model: modelCopy
							}
						});
					}
				};
			}
		};
	}
})();
