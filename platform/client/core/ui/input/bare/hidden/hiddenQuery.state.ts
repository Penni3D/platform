﻿(function () {
	'use strict';

	angular
		.module('core.dialogs')
		.controller('HiddenQueryController', HiddenQueryController);


	HiddenQueryController.$inject = ['$scope', 'MessageService', 'Model', 'Callback', 'UniqueService','Common','Translator'];
	function HiddenQueryController($scope, MessageService, Model, Callback, UniqueService,Common,Translator) {
		$scope.pid = UniqueService.get('input-hidden', true);
		$scope.rid = UniqueService.get('input-hidden', true);
		$scope.dialogLabel = Translator.translate("DATA_PASSWORD").toLowerCase();
		$scope.dialogModel = { passwd: '', rPasswd: '' };
		
		$scope.validated = false;
		
		$scope.disabled = function () {
			return !$scope.validated;
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.getValidation = function() {
			let r = "";
			if (r == "" && !$scope.dialogModel.passwd.length) r = "PASSWORD_INVALID_EMPTY";
			if (r == "" && $scope.dialogModel.passwd !== $scope.dialogModel.rPasswd) r =  "PASSWORD_INVALID_SAME";
			if (r == "") r = Common.isValidPassword($scope.dialogModel.passwd, false, true);
			$scope.validated = r == "";
			return Translator.translation(r);
		}
		$scope.remove = function() {
			Model.value = null;
			Callback();
			MessageService.closeDialog();
		}
		$scope.save = function () {
			Model.value = $scope.dialogModel.passwd;
			Callback();
			MessageService.closeDialog();
		};
	}
})();