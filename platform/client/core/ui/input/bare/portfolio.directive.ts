(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputPortfolio', InputPortfolio);

	InputPortfolio.$inject = [
		'PortfolioService',
		'$q',
		'CalendarService',
		'ProcessService',
		'InputService',
		'ViewService',
		'Common',
		'SaveService',
		'MessageService',
		'$rootScope',
		'Translator'
	];

	function InputPortfolio(
		PortfolioService,
		$q,
		CalendarService,
		ProcessService,
		InputService,
		ViewService,
		Common,
		SaveService,
		MessageService,
		$rootScope,
		Translator
	) {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				label: '@?',
				settings: '<?',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/bare/portfolio.html',
			link: function (scope, element, attrs) {
				scope.userLanguage = $rootScope.me.locale_id;
				scope.loading = true;
				scope.listAllOptions = {};
				scope.listOptions = {};
				scope.lists = [];
				scope.portfolioColumns = [];
				scope.portfolioColumnsIncludingReportColumns = [];
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				scope.openable = typeof attrs.openable !== 'undefined';
				scope.showSumRow = Common.isTrue(scope.settings.showSum);
				scope.showSumRowCumulative = Common.isTrue(scope.settings.showSumCumulative);
				scope.showAddDelete = !(Common.isTrue(scope.settings.showAddDelete));
				scope.archiveFieldName = attrs.archivefieldname;
				scope.sumFooter = {};

				let splitWidth = attrs.openable;
				let process = attrs.process;
				let portfolioId = attrs.portfolioId;
				let parent = attrs.parent;

				if (attrs.parentfieldname && attrs.parentfieldname.length > 0) {
					let parentData = ProcessService.getCachedItemData(parent).Data;
					if (parentData && parentData[attrs.parentfieldname]) {
						parent = parentData[attrs.parentfieldname];
					}
				}

				let itemColumnId = attrs.itemColumnId;
				let hasFiltering = !!(itemColumnId && parent);
				let filterColumn;
				let isListFilter = false;
				let useNewDialog = Common.isTrue(scope.settings.useDialog);
				let restrictions = {};
				let actionId = scope.settings.action;

				let orderBy = 'asc';

				scope.formatDateTime = function (d) {
					return CalendarService.formatDateTime(d);
				};

				scope.getArchive = function () {
					if (scope.archiveFieldName) {
						let parentData = ProcessService.getCachedItemData(attrs.parent).Data;
						if (parentData && parentData[scope.archiveFieldName]) {
							scope.modify = false;
							return parentData[scope.archiveFieldName];
						}
					}
					return scope.settings.archive;
				};

				scope.sortByColumn = function (col) {
					if (orderBy == 'asc') {
						orderBy = 'desc';
					} else orderBy = 'asc';

					PortfolioService.getPortfolioData(
						process,
						portfolioId,
						{local: true, restrictions: restrictions, archive: scope.getArchive()},
						{limit: 0, sort: [{column: col.Name, order: orderBy}]})
						.then(function (rows) {
							scope.portfolioRows = rows.rowdata;
						});

				};

				if (useNewDialog) {
					ProcessService.subscribeNewItem(scope, function () {
						getRows().then(function (newRows) {
							let i = newRows.length;
							while (i--) {
								let row = newRows[i];

								let j = scope.portfolioRows.length;
								while (j--) {
									if (scope.portfolioRows[j].itemId == row.itemId) {
										break;
									}
								}

								if (j === -1) {
									addRow(row);
								}
							}
						});
					}, process);
				}

				scope.$on('RefreshAllTables', function (event, opt) {
					getRows().then(function (rows) {
						scope.portfolioRows = rows;
						calculateSum();
						updateModel();
						scope.loading = false;
					});
				});

				PortfolioService.getPortfolioColumns(process, portfolioId).then(function (columns) {

					let promises = [];

					promises.push(ProcessService.getSublists(process, portfolioId, false, true).then(function (r) {
						scope.sublists = r;
					}));

					angular.forEach(columns, function (column) {
						let ic = column.ItemColumn;
						let dt = Common.getRelevantDataType(ic);

						if (dt == 0 || dt == 1 ||
							dt == 2 || dt == 3 ||
							dt == 4 || dt == 5 ||
							dt == 6 || dt == 7 ||
							dt == 13
						) {
							if (ic.ItemColumnId == itemColumnId) {
								filterColumn = ic;
								return;
							}

							ic.readOnly = (ic.DataType == 16 || ic.DataType == 20 || ic.Validation.ReadOnly == true);
							ic.width = column.Width ? column.Width : undefined;
							ic.dataType = dt;
							ic.alignRight = dt == 1 || dt == 2;
							ic.translation = column.ShortNameTranslation ? column.ShortNameTranslation : ic.LanguageTranslation;
							ic.sumColumn = column.SumColumn;
							ic.ShowType = column.ShowType;
							ic.ShowTypeCombined = column.ShowTypeCombined;
							ic.DataAdditional2 = column.ItemColumn.DataAdditional2;
							ic.$$reportColumn = column.ReportColumn;
							 scope.portfolioColumns.push(ic);

							if (ic.SubDataType == 5) {
								column.DataAdditional = ic.SubDataAdditional;
								ic.DataAdditional = ic.SubDataAdditional;
							}
							scope.portfolioColumnsIncludingReportColumns.push(ic);


							if (dt == 6) {
								if (ic.DataType == 20) ic.DataAdditional = ic.InputName;
								if (ic.DataType == 16) ic.DataAdditional = ic.SubDataAdditional;

								scope.lists.push(ic);
								promises.push(ProcessService
									.getListItems(ic.DataAdditional)
									.then(function (items) {
										scope.listAllOptions[ic.DataAdditional] = items;
									})
								);
							}
						}
					});

					if (hasFiltering && filterColumn) {
						let dt = Common.getRelevantDataType(filterColumn);

						if (dt == 1 || dt == 2 || dt == 5) {
							restrictions[itemColumnId] = [parent];

							if (dt == 5) {
								isListFilter = true;
							}
						}
					}
					return $q.all(promises);
				}).then(function () {
					return getRows().then(function (rows) {
						scope.portfolioRows = rows;
						calculateSum();
						updateModel();
						scope.loading = false;
					});
				});

				scope.sublists = {};
				scope.sublistsData = {};

				scope.updateSubList = function (col, row) {
					if (scope.sublists && _.size(scope.sublists[col.DataAdditional]) > 0) {
						angular.forEach(scope.sublists[col.DataAdditional], function (subList, sublistProcessName) {
							let subListData = [];
							let subListIds = [];

							angular.forEach(row[col.Name], function (parentId) {
								angular.forEach(subList[parentId], function (subListItem) {
									if (scope.listAllOptions[sublistProcessName]) {
										for (let i of scope.listAllOptions[sublistProcessName]) {
											if (i.item_id == subListItem) {
												subListData.push(i);
												subListIds.push(subListItem);
											}
										}
									}
								});
							});

							scope.listOptions[row.item_id][sublistProcessName] = subListData;
						});
					}
				};

				let hasDataChanged = false;
				scope.inputChanged = function (params) {
					if (params.col.DataType == 6) scope.updateSubList(params.col, params.row);
					ProcessService.setDataChanged(process, params.row.item_id);
					hasDataChanged = true;
					calculateSum();
				};

				SaveService.subscribeSave(scope, function () {
					if (hasDataChanged) {
						hasDataChanged = false;
						onChange();
					}
				});

				scope.openRow = function (row) {
					let iid = row.itemId || row.item_id;
					if (splitWidth === 'full') {
						ViewService.view('process.tab',
							{
								params: {
									process: process,
									itemId: iid
								}
							}
						);
					} else {
						ViewService.view('featurette',
							{
								params: {
									process: process,
									itemId: iid,
									forceReadOnly: false,
									disableNextPrev: true,
									title:row.hasOwnProperty('title') ? row['title'] : ""
								}
							},
							{
								split: true,
								size: splitWidth
							}
						);
					}
				};

				scope.addRow = function () {
					let data = {};
					if (hasFiltering && filterColumn) data[filterColumn.Name] = isListFilter ? [parent] : parent;
					if (useNewDialog) {
						ViewService.view('new.process', {
							params: {
								openerItemId: parent,
								toast: 'TABLE_NEW_ROW_CREATED',
								process: process,
								overwriteData: data,
								actionId: actionId
							}
						});
					} else {
						scope.adding = true;
						ProcessService.newItem(process, {Data: data}, actionId).then(function (itemId) {
							scope.adding = false;
							ProcessService.getItemData(process, itemId, true).then(function(newData) {
								addRow(newData.Data);
							});
						});
					}
				};

				scope.removeRow = function (itemId) {
					MessageService.confirm("CONFIRM_DELETE", function () {
						scope.loading = true;
						ProcessService.deleteItem(process, itemId).then(function () {
							scope.loading = false;
							removeRow(itemId);
						});
					}, 'warn');
				};

				function getSublistParent(listProcessName) {
					let result = undefined;
					angular.forEach(scope.sublists, function (item, parentName) {
						angular.forEach(item, function (subitem, subName) {
							if (subName == listProcessName) result = parentName;
							return;
						});
						if (result) return;
					});
					return result;
				}

				function addRow(row) {
					for (let l of scope.lists) {
						if (typeof (scope.listOptions[row.item_id]) == "undefined") scope.listOptions[row.item_id] = {};
						if (getSublistParent(l.DataAdditional)) {
							scope.listOptions[row.item_id][l.DataAdditional] = [];
						} else {
							scope.listOptions[row.item_id][l.DataAdditional] = scope.listAllOptions[l.DataAdditional];
						}
					}
					for (let col of scope.portfolioColumns) {
						if (col.DataType == 16 && col.SubDataType == 6) {
							scope.updateSubList(col, row);
							break;
						}
					}

					scope.portfolioRows.push(row);
					updateModel();
					onChange();
				}

				function removeRow(itemId) {
					let i = scope.portfolioRows.length;
					while (i--) {
						if (scope.portfolioRows[i].item_id == itemId) {
							scope.portfolioRows.splice(i, 1);
							break;
						}
					}

					onChange();
					updateModel();
				}

				function getRows() {
					return PortfolioService.getPortfolio(process, portfolioId).then(function (p) {
						let order_name = 'item_id';
						let order_dir = 'asc';

						if (_.isNumber(p.OrderItemColumnId) && p.OrderItemColumnId > 0) {
							_.each(scope.portfolioColumnsIncludingReportColumns, function (c) {
								if (c.ItemColumnId == p.OrderItemColumnId) {
									order_name = c.Name;
									order_dir = p.OrderDirection == 0 ? 'asc' : 'desc';
									orderBy = order_dir;
									return;
								}
							});
						}

						return PortfolioService.getPortfolioData(
							process,
							portfolioId,
							{local: true, restrictions: restrictions, archive: scope.getArchive()},
							{limit: 0, sort: [{column: order_name, order: order_dir}]})
							.then(function (rows) {
								if (process == 'action_history') {
									for (let r of rows.rowdata) {
										if (r.action_json && r.action_json.length > 0)
											r.action_json = JSON.parse(r.action_json)[scope.userLanguage];
									}
								}

								for (let l of scope.lists) {
									for (let row of rows.rowdata) {
										if (typeof (scope.listOptions[row.item_id]) == "undefined") scope.listOptions[row.item_id] = {};
										let parentList = getSublistParent(l.DataAdditional);
										if (parentList) {
											for (let col of scope.portfolioColumns) {
												if (col.DataAdditional == parentList) {
													scope.updateSubList(col, row);
													break;
												}
											}
										} else {
											scope.listOptions[row.item_id][l.DataAdditional] = scope.listAllOptions[l.DataAdditional];
										}
									}
								}
								return rows.rowdata;
							});
					});
				}

				function calculateSum() {
					let sumResults = {};
					let i = scope.portfolioColumns.length;
					let previous = 0;

					_.each(scope.portfolioColumns, function (ic) {
						if (ic.sumColumn && (ic.dataType == 1 || ic.dataType == 2)) {
							let name = ic.Name;
							let sum = _.sumBy(scope.portfolioRows, name);
							sumResults[name] = Common.formatNumber(sum + previous);
							if (scope.showSumRowCumulative) previous += sum;
						}
					});
					scope.sumFooter = sumResults;
				}

				scope.returnSum = function(col){
					calculateSum();
					return scope.sumFooter[col]
				}

				function updateModel() {
					scope.contains = scope.portfolioRows.length ? 'set' : '';
				}

				function onChange() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}

				if (attrs.ignoreexcelexport == '' || Common.isFalse(attrs.ignoreexcelexport)) {
					let excelExportName = GetNameForExcelExport();
					ViewService.addDownloadable(excelExportName + ' -> Excel', function () {
						PortfolioService.getPortfolioExcel(
							process,
							portfolioId,
							{ recursive: false, name: excelExportName, restrictions: restrictions },
							scope.portfolioColumns);
					}, ViewService.getViewIdFromElement(element));
				}

				function GetNameForExcelExport() {
					if (scope.label != '') return scope.label;
					let containerTitle = Translator.translation(scope.$parent.$parent.$parent.grid.LanguageTranslation);
					if (containerTitle != '') return containerTitle;
					let headerTitle = scope.$parent.$parent.$parent.$parent.header.title;
					if (headerTitle != '') return headerTitle;
					return Translator.translate('PORTFOLIO_EXCEL_EXPORT_TABLE');
				}
			},
			controller: function ($scope, UniqueService) {
				$scope.id = UniqueService.get('portfolio');
				if (typeof $scope.settings === 'undefined') $scope.settings = {};
				if (!!$scope.settings.archive) $scope.modify = false;
			}
		};
	}
})();