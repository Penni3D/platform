﻿(function () {
	'use strict';


	angular
		.module('core')
		.directive('inputButton', InputButton);

	InputButton.$inject = ['UniqueService', '$compile', 'Common'];

	function InputButton(UniqueService, $compile, Common) {
		return {
			transclude: true,
			replace: true,
			restrict: 'E',
			templateUrl: 'core/ui/input/bare/button.html',
			scope: {oModify: '<?modify', loading: '<?'},
			link: function (scope, element, attrs, ctrl, transclude) {
				let $catcher = $compile('<click-catcher ng-click="onClick($event)">')(scope);
				scope.modify = true;
				scope.id = UniqueService.get('input-button', true);

				transclude(scope.$parent, function (clone) {
					clone.appendTo(element);

					if (typeof attrs.loading !== 'undefined') {
						let $loader = $compile('<loader class="tiny simple">')(scope);
						let loaderShown = false;

						scope.$watch('loading', function (isLoading) {
							if (Common.isTrue(isLoading)) {
								scope.modify = false;

								if (!loaderShown) {
									loaderShown = true;
									$loader.appendTo(element);
								}

								element.css('color', 'rgba(0, 0, 0, .12)');
							} else {
								scope.modify = true;

								if (loaderShown) {
									loaderShown = false;
									$loader.detach();
								}

								element.css('color', '');
							}
						});
					}
				});

				if (typeof scope.oModify === 'undefined') scope.oModify = true;
				if (scope.oModify) {
					if (element.attr("tabindex") === "undefined") {

					} else if (element.attr("tabindex") == "-1") {

					} else {
						element.addClass("aria-detect");
						element.attr("tabindex", "0");
					}
				}
				element.attr("role", "button");
				scope.onClick = function (event) {
					if (!(scope.oModify && Common.isTrue(scope.modify))) {
						event.stopPropagation();
					}
				};

				scope.$watch('oModify + modify', function () {
					if (scope.oModify && Common.isTrue(scope.modify)) {
						element.attr('disabled', false);
						element.attr('aria-disabled', false);
						$catcher.detach();
					} else {
						element.attr('disabled', '');
						element.attr('aria-disabled', true);
						$catcher.appendTo(element);
					}
				});

				let wave;
				let addWaves = typeof attrs.noWaves === 'undefined';
				let type = attrs.type;

				if (typeof type === 'undefined') {
					element.addClass('btn-secondary');
					wave = ['waves-button'];
				} else {
					type = type.split(' ');
					if (type[1] === 'raised') {
						element.addClass("btn-raised");
					}

					type = type[0];

					if (type === 'icon') {
						element.addClass('btn-icon');
						wave = ['waves-circle'];
					} else if (type === 'primary') {
						element.addClass('btn-primary');
						wave = ['waves-button'];
					} else if (type === 'secondary-warn') {
						element.addClass('btn-secondary-warn');
						wave = ['waves-button'];
					} else if (type === 'warn') {
						element.addClass('btn-warn');
						wave = ['waves-button', 'waves-light'];
					} else if (type === 'link') {
						element.addClass('btn-link');
					} else if (type === 'fab') {
						element.addClass('btn-fab');
						wave = ['waves-circle'];
					} else {
						element.addClass('btn-secondary');
						wave = ['waves-button'];
					}
				}

				if (wave && addWaves) {
					Waves.attach(element, wave);
				}
			}
		};
	}
})();
