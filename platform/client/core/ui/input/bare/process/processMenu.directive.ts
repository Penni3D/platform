(function () {
	'use strict';

	angular
		.module('core')
		.directive('processMenu', ProcessMenu);

	ProcessMenu.$inject = ['PortfolioService', 'ProcessService', '$q', 'FavouritesService'];
	function ProcessMenu(PortfolioService, ProcessService, $q, FavouritesService) {
		return {
			templateUrl: 'core/ui/input/bare/process/processMenu.html',
			link: function (scope, element) {
				scope.favourites = FavouritesService.getFavourites(scope.settings.process);
				scope.search = '';
				initializeEmpty();
				scope.modifier = scope.model.length == 0 ? 0 : scope.model.length;
				let process = scope.settings.process;
				let isUsingPortfolio = scope.settings.portfolio;
				let isParentDirection = scope.settings.direction === 'parent';
				let hasPreload = scope.settings.preload;
				scope.limit = 50;
				
				setTimeout(function () {
					let $input = element.find('input');
					$input.focus();

					$input.on('click', function () {
						$input.select();
					});
				}, 75);

				scope.toggleFavorite = function (itemId, event) {
					event.stopPropagation();

					scope.favourites[itemId] ?
						FavouritesService.removeFavourite(process, itemId)
						: FavouritesService.addFavourite(process, itemId);
				};

				let bouncer;
				scope.onSearch = function (loadMore?) {
					clearTimeout(bouncer);
					if (!loadMore) offsetNumberPortfolio = 0;
					if (!hasPreload && scope.search.length < 2) {
						initializeEmpty();
						return;
					}

					bouncer = setTimeout(function () {
						scope.noResults = false;
						scope.searching = true;

						if(loadMore !== true){
							scope.items = [];
						}

						getItems(loadMore).then(function (items) {
							if(typeof scope.modifyResult == 'function'){
								items = scope.modifyResult()(angular.copy(items));
							}
							scope.searching = false;
							let concatted = scope.items.concat(items);
							scope.items = concatted;
							if(!hasPreload){
								scope.maxRowCount = concatted.length > 0 ? concatted[0].$$maxCount : 0;
							}
							moveFavouritesFirst();
							removeSelectedOnes();

							if (scope.items.length === 0) {
								scope.noResults = true;
							}
						});
					}, 1000);
				};

				let getItems = normalSearch;

				let offsetNumberPortfolio = 0;

				if (isUsingPortfolio) {
					getItems = function (loadMore: boolean = false) {
						let promises = [];
						if (loadMore) offsetNumberPortfolio = offsetNumberPortfolio + scope.limit;
						
						return PortfolioService
							.getSimplePortfolioData(
								process,
								scope.settings.portfolioId,
								{ limit: scope.limit, text: scope.search, offset:offsetNumberPortfolio })
							.then(function (rows) {
								scope.maxRowCount = rows.rowcount;
								for (let i = 0, l = rows.rowdata.length; i < l; i++) {
									promises.push(ProcessService.prepareItem(process, rows.rowdata[i]));
								}
								return $q.all(promises).then(function () {
									return _.orderBy(rows.rowdata, 'primary');
								});
							}, function () {
								getItems = normalSearch;
								return getItems(loadMore);
							});
					};
				}

				let offsetNumber = 0;
				scope.useExtendedSearch = false;

				scope.loadMoreSearchResults = function(){
					scope.useExtendedSearch = true;
					offsetNumber = offsetNumber + scope.limit;
					scope.onSearch(true);
				}

			scope.maxRowCount = hasPreload ? scope.rowcount : 0;

				function normalSearch(loadMore: boolean = false) {
					return ProcessService.getItemsFiltered(process, { text: scope.search, limit: scope.limit, offset: offsetNumber }).then(function(dataRows){
						scope.maxRowCount = dataRows.length > 0 ? dataRows[0].$$maxCount : 0;
						return dataRows;
					});
				}

				function moveFavouritesFirst() {
					if (scope.favourites) {
						let favs = _.remove(scope.items, function (item) {
							return scope.favourites[item.itemId]
						});

						scope.items = _.concat(favs, scope.items);
					}
				}

				function initializeEmpty() {
					if (scope.preloadItems.length || isUsingPortfolio) {
						scope.items = _.clone(scope.preloadItems);

						moveFavouritesFirst();
					} else {
						scope.items = _.sortBy(_.values(scope.favourites), 'primary');
					}
					removeSelectedOnes();
				}

				function removeSelectedOnes() {
					let i = scope.items.length;
					while (i--) {
						let j = scope.model.length;
						while (j--) {
							let c = isParentDirection ? scope.model[j].itemId : scope.model[j];
							if (scope.items[i].itemId == c) {
								scope.items.splice(i, 1);
								break;
							}
						}
					}
				}
			}
		}
	}
})();