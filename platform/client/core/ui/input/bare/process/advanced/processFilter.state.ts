(function () {
	'use strict';

	angular
		.module('core')
		.config(ProcessFilterConfig)
		.controller('ProcessFilterController', ProcessFilterController);

	ProcessFilterConfig.$inject = ['ViewsProvider'];

	function ProcessFilterConfig(ViewsProvider) {
		ViewsProvider.addDialog('processFilter', {
			template: 'core/ui/input/bare/process/advanced/processFilter.html',
			controller: 'ProcessFilterController',
			resolve: {
				PortfolioColumns: function (PortfolioService, StateParameters) {
					return PortfolioService.getPortfolioColumns(StateParameters.process, StateParameters.portfolioId);
				},
				Portfolio: function (PortfolioService, StateParameters) {
					return PortfolioService.getPortfolio(StateParameters.process, StateParameters.portfolioId);
				},
				Settings: function (StateParameters, PortfolioService) {
					PortfolioService.clearActiveSettings(StateParameters.portfolioId);
					return PortfolioService.getActiveSettings(StateParameters.portfolioId);
				},
				Relevant: function (StateParameters) {
					return !!(StateParameters.parentProcess && StateParameters.parentId && StateParameters.itemColumnId);
				},
				Parent: function (StateParameters, ProcessService) {
					if (StateParameters.parentId && StateParameters.parentProcess) {
						return ProcessService
							.getItem(StateParameters.parentProcess, StateParameters.parentId)
							.then(function (parent) {
								let p = _.clone(parent);
								p.parent_item_id = 0;
								p.task_type = 54;

								return p;
							});
					}
				}
			},
			afterResolve: {
				ParentInfo: function (ParentColumns, Parent, Settings) {
					if (Parent && ParentColumns) {
						let startDate = Parent[ParentColumns.startDate];

						Settings.calendar.zeroDate = startDate;

						return {
							startDate: startDate,
							endDate: Parent[ParentColumns.endDate],
							effort: Parent[ParentColumns.parentEffort] ? Parent[ParentColumns.parentEffort] : 0
						};
					}
				},
				FilterColumns: function (PortfolioColumns, Common) {
					let columns = [];

					for (let i = 0, l = PortfolioColumns.length; i < l; i++) {
						let pc = PortfolioColumns[i];
						let ic = pc.ItemColumn;
						let dt = Common.getRelevantDataType(ic);

						if (pc.UseInFilter && (dt === 6 || dt === 5)) {
							ic.dataAdditional = Common.getRelevantDataAdditional(ic);
							columns.push(pc);
						}
					}

					return columns;
				},
				VisibleColumns: function (PortfolioColumns) {
					return _.take(_.filter(PortfolioColumns, function (c) {
						return c.IsDefault;
					}), 4);
				},
				HiddenColumns: function (PortfolioColumns) {
					let columns = [];
					angular.forEach(PortfolioColumns, function (c) {
						if (!c.IsDefault) {
							columns.push(c);
						}
					});

					return columns;
				}
			}
		});
	}

	ProcessFilterController.$inject = [
		'$scope',
		'PortfolioService',
		'FilterColumns',
		'PortfolioColumns',
		'StateParameters',
		'Model',
		'OnFinish',
		'MessageService',
		'Settings',
		'AllocationService',
		'VisibleColumns',
		'HiddenColumns',
		'RMService',
		'Colors',
		'FavouritesService',
		'Portfolio',
		'ProcessService',
		'Relevant',
		'Parent',
		'ParentInfo',
		'LinkData',
		'ParentColumns',
		'$q',
		'CalendarService',
		'Common',
		'$timeout'
	];

	function ProcessFilterController(
		$scope,
		PortfolioService,
		FilterColumns,
		PortfolioColumns,
		StateParameters,
		Model,
		OnFinish,
		MessageService,
		Settings,
		AllocationService,
		VisibleColumns,
		HiddenColumns,
		RMService,
		Colors,
		FavouritesService,
		Portfolio,
		ProcessService,
		Relevant,
		Parent,
		ParentInfo,
		LinkData,
		ParentColumns,
		$q,
		CalendarService,
		Common,
		$timeout
	) {
		let selectedItems = _.clone(Model);
		let filters = Settings.filters;
		let allocatedEffort = 0;
		let itemsWithEffort = [];

		$scope.finishing = false;
		$scope.showEffortCalculations = false;
		$scope.selectedItemsCount = 0;
		$scope.unallocatedEffort = 0;
		$scope.calendarIsShown = !!Portfolio.CalendarType;
		$scope.filterColumns = FilterColumns;
		$scope.portfolioColumns = PortfolioColumns;
		$scope.visibleColumns = VisibleColumns;

		$scope.label = Parent ? Parent.primary + ': ' + StateParameters.label : StateParameters.label;
		$scope.favourites = FavouritesService.getFavourites(StateParameters.process);
		$scope.process = StateParameters.process;
		$scope.portfolioId = StateParameters.portfolioId;
		$scope.items = [];
		$scope.RMRules = {};
		$scope.showRelevantButton = Relevant;

		$scope.showAllocationManager = $scope.process === 'user';

		if (ParentInfo && ParentInfo.startDate && ParentInfo.endDate) {
			let firstTextColumn = _.find(VisibleColumns, function (pc) {
				let dt = Common.getRelevantDataType(pc.ItemColumn);

				return dt === 0 || dt === 4;
			});

			Parent[firstTextColumn.ItemColumn.Name] = Parent.primary;

			if (ParentInfo.startDate <= ParentInfo.endDate) {
				$scope.showEffortCalculations = true;
				ParentInfo.startDate = moment(ParentInfo.startDate).startOf('day');
				ParentInfo.endDate = moment(ParentInfo.endDate).endOf('day');

				if (ParentColumns.itemEffort) {
					angular.forEach(selectedItems, function (item) {
						let itemId = item.itemId;

						let effort = LinkData[itemId][ParentColumns.itemEffort];
						if (_.isFinite(Number(effort))) {
							allocatedEffort += effort;
						}
					});
				}

				$scope.unallocatedEffort = allocatedEffort <= ParentInfo.effort ? ParentInfo.effort - allocatedEffort : 0;

				recalculateParentEffort();
			}
		}


		$scope.AMOptions = {
			calendarSteps: [15, 60],
			inheritDates: false,
			keepOrder: true,
			readOnly: true,
			showCalendar: $scope.calendarIsShown,
			hiddenColumns: _.map(HiddenColumns, function (c) {
				return c.ItemColumn.Name;
			}),
			visibleColumns: _.map(VisibleColumns, function (c) {
				return c.ItemColumn.Name;
			}),
			rowClick: function (itemId) {
				if (itemId == Parent.itemId && Parent.process != StateParameters.process) {
					return;
				}

				let DI = RMService.itemSet[itemId];
				let i = selectedItems.length;
				while (i--) {
					if (selectedItems[i].itemId == itemId) {
						DI.showHighlight('');
						selectedItems.splice(i, 1);

						$scope.selectedItemsCount--;

						if ($scope.showEffortCalculations) {
							AllocationService.invalidateCalendarCache(itemId);

							let j = itemsWithEffort.length;
							while (j--) {
								if (itemsWithEffort[j] == itemId) {
									itemsWithEffort.splice(j, 1);
									break;
								}
							}
						}

						break;
					}
				}

				if (i === -1) {
					if (StateParameters.swap && selectedItems.length) {
						let lastSelectionId = selectedItems.pop().itemId;
						let lastDI = RMService.itemSet[lastSelectionId];
						if (lastDI) {
							lastDI.showHighlight('');
						}

						$scope.selectedItemsCount = 0;

						if ($scope.showEffortCalculations) {
							AllocationService.invalidateCalendarCache(lastSelectionId);
							itemsWithEffort = [];
						}
					}

					if (canAdd()) {
						DI.showHighlight(Colors.getColor('accent').background);
						selectedItems.push(DI.data);
						$scope.selectedItemsCount++;

						if ($scope.showEffortCalculations) {
							itemsWithEffort.push(itemId);
						}
					}
				}

				if ($scope.showEffortCalculations) {
					recalculateItemsEffort().then(function () {
						DI.RM.calendarReDraw();
					});
				}
			}
		};

		$scope.showRelevant = function () {
			PortfolioService.clearActiveSettings(StateParameters.portfolioId);
			$scope.activeFilter = 'relevant';

			$scope.filter();
		};

		$scope.showFavorites = function () {
			PortfolioService.clearActiveSettings(StateParameters.portfolioId);
			$scope.activeFilter = 'favorites';

			$scope.filter();
		};

		$scope.showFilters = function () {
			$scope.activeFilter = 'filter';

			if ($scope.showFilterArea) {
				$scope.filter();
			} else {
				$scope.selectedItemsCount = 0;
				$scope.showFilterArea = true;
			}
		};

		function copy(r) {
			//Store original item ids
			r.actual_item_id = r.item_id ? angular.copy(r.item_id) : angular.copy(r.itemId);
			r.actual_parent_item_id = angular.copy(r.parent_item_id);

			r.item_id = "copy" + r.item_id;
			r.itemId = "copy" + r.itemId;
			r.parent_item_id = "copy" + r.parent_item_id;
			return r;
		}

		function restore(item) {
			//Restore stored item ids
			if (item.actual_item_id) {
				item.item_id = angular.copy(item.actual_item_id);
				item.itemId = angular.copy(item.actual_item_id);
			}
			item.parent_item_id = item.actual_parent_item_id ? angular.copy(item.actual_parent_item_id) : item.parent_item_id;
			return item;
		}

		$scope.filter = function () {
			$scope.showFilterArea = false;
			$scope.showLoading = true;
			$scope.selectedItemsCount = 0;

			filters.limit = $scope.activeFilter === 'favorites' ? 0 : 50;
			filters.sort = [{column: VisibleColumns[0].ItemColumn.Name}];

			let context = $scope.activeFilter === 'relevant' ?
				{
					process: StateParameters.parentProcess,
					itemId: StateParameters.parentId,
					itemColumnId: StateParameters.itemColumnId
				} : undefined;

			PortfolioService
				.getPortfolioData(
					StateParameters.process,
					StateParameters.portfolioId,
					{context: context, local: true},
					filters,
					undefined,
					true
				).then(function (result) {
				selectedItems = _.clone(Model);
				let rowCandidates = [];
				if (!Parent) {
					Parent = {};
					Parent.itemId = 10000000000;
				}
				angular.forEach(result.rowdata, function (r) {
					if (!_.find(selectedItems, function (s) {
						return s.itemId == r.itemId
					}) && Parent.itemId != r.itemId) {
						if ($scope.activeFilter === 'favorites') {
							if (_.find($scope.favourites, function (f) {
								return f.itemId == r.itemId
							})) {
								rowCandidates.push(copy(r));
							}
						} else {
							rowCandidates.push(copy(r));
						}
					}
				});

				if (_.isEmpty(rowCandidates)) {
					finishLoading([]);
					return;
				}

				if (!$scope.calendarIsShown || !$scope.showAllocationManager) {
					finishLoading(rowCandidates);
					return;
				}

				AllocationService.getResourcesAllocationData('-1/M', _.map(rowCandidates, function (r) {
					return r.itemId;
				})).then(function (allocationData) {
					itemsWithEffort = [];

					angular.forEach(rowCandidates, function (r) {
						let a = allocationData[r.itemId];
						r.task_type = 53;
						r.allocations = [];

						angular.forEach(a.rowdata, function (row) {
							if (row.itemId != r.itemId) r.allocations.push(row.item_id);
						});
					});

					finishLoading(rowCandidates);
				});

			});
		};

		$scope.finish = function () {
			$scope.finishing = true;

			//Restore original item ids
			for (let item of selectedItems) {
				item = restore(item);
			}

			if ($scope.showEffortCalculations) {
				let effortPerItem = Common.rounder($scope.unallocatedEffort / $scope.selectedItemsCount);

				$q.all(_.map(itemsWithEffort, function (itemId) {
					if (effortPerItem) {
						return ProcessService.getLink(
							StateParameters.parentProcess,
							StateParameters.parentId,
							StateParameters.itemColumnId,
							itemId
						).then(function (link) {
							link[ParentColumns.itemEffort] = effortPerItem;
						});
					}
				})).then(function () {
					OnFinish(selectedItems);
					$scope.close();
				});
			} else {
				OnFinish(selectedItems);
				$scope.close();
			}
		};

		$scope.close = function () {
			if ($scope.showEffortCalculations) {
				AllocationService.invalidateCalendarCache(Parent.itemId);

				angular.forEach(itemsWithEffort, function (itemId) {
					AllocationService.invalidateCalendarCache(itemId);
				});
			}

			MessageService.closeDialog();
		};

		$scope.changeParentEffort = function (event) {
			let p = {
				template: 'core/ui/input/bare/process/advanced/card.html',
				controller: 'ProcessFilterCardController',
				locals: {
					CurrentEffort: $scope.unallocatedEffort,
					ChangeEffort: function (newEffort) {
						$scope.unallocatedEffort = newEffort;
						recalculateParentEffort();
						recalculateItemsEffort().then(function () {
							RMService.itemSet["Process_Filter_" + StateParameters.parentId].RM.calendarReDraw();
						});
					}
				}
			};

			MessageService.card(p, event.currentTarget);
		};

		function finishLoading(items) {
			if ($scope.showEffortCalculations) {
				angular.forEach($scope.items, function (item) {
					let itemId = item.itemId;

					if (itemId == Parent.itemId) {
						return;
					}

					AllocationService.invalidateCalendarCache(itemId);
				});
			}

			angular.forEach(items, function (item) {
				item.parent_item_id = 0;
			});

			$scope.items = items;
			if (Parent && Parent.process == 'task' && StateParameters.process == 'user') {
				if ($scope.items.length && Parent.process !== StateParameters.process) {
					let p = angular.copy(Parent);
					p.item_id = "Process_Filter_" + Parent.item_id;
					$scope.items.unshift(p);
				}
			}

			$timeout(function () {
				$scope.showLoading = false;
				$(".background-accent").first().attr("colspan", VisibleColumns.length);
				window.dispatchEvent(new Event('resize'));
			}, 50);
		}

		function canAdd() {
			return StateParameters.max == 0 || selectedItems.length < StateParameters.max;
		}

		function recalculateParentEffort() {
			let effortPerDay = $scope.unallocatedEffort / (ParentInfo.endDate.diff(ParentInfo.startDate, 'days') + 1);
			let dateRunner = ParentInfo.startDate.clone();

			while (dateRunner.isSameOrBefore(ParentInfo.endDate)) {
				AllocationService.setCalendarData(
					dateRunner.year(),
					dateRunner.month() + 1,
					dateRunner.date(),
					"Process_Filter_" + StateParameters.parentId,
					80,
					effortPerDay
				);

				dateRunner.add(1, 'days');
			}
		}

		$scope.resolutionModeToggle = function () {
			var RM = RMService.itemSet["Process_Filter_" + StateParameters.parentId].RM;
			var res = parseInt(RM.getResolution());
			switch (res) {
				case 360:
					RM.setResolution(720);
					break;
				case 720:
					RM.setResolution(60);
					break;
				case 60:
					RM.setResolution(120);
					break;
				case 120:
					RM.setResolution(240);
					break;
				default:
					RM.setResolution(360);
			}
		}

		function recalculateItemsEffort() {
			let splitBetweenItems = $scope.selectedItemsCount;
			Parent.selectedEffortItems = {};

			return $q.all(_.map(itemsWithEffort, function (itemId) {
				Parent.selectedEffortItems[itemId] = _.find($scope.items, function (item) {
					return item.itemId == itemId;
				});

				return CalendarService
					.getUserCalendar(itemId, ParentInfo.startDate.toDate(), ParentInfo.endDate.toDate())
					.then(function (calendarData) {
						let workDates = [];
						let runner = ParentInfo.startDate.clone();
						while (runner.isSameOrBefore(ParentInfo.endDate)) {
							let y = runner.year();
							let m = runner.month() + 1;
							let d = runner.date();

							let c = calendarData[y][m][d];

							if (!c.notWork) {
								workDates.push(y + '-' + m + '-' + d);
							}

							runner.add(1, 'days');
						}

						if (workDates.length) {
							let effortPerDay = $scope.unallocatedEffort / workDates.length;
							let effortPerDayAndUser = effortPerDay / splitBetweenItems;

							let i = workDates.length;
							while (i--) {
								let date = workDates[i].split('-');

								AllocationService.setCalendarData(
									date[0], date[1], date[2], itemId, 80, effortPerDayAndUser
								);
							}
						}
					});
			}));
		}

		if (StateParameters.preload) {
			if (Relevant) {
				$scope.showRelevant();
			} else {
				$scope.activeFilter = 'filter';
				$scope.filter();
			}
		} else {
			$scope.showFilterArea = true;
			$scope.activeFilter = 'filter';
		}
	}
})();