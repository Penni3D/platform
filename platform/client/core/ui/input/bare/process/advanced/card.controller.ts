(function () {
	'use strict';

	angular
		.module('core')
		.controller('ProcessFilterCardController', ProcessFilterCardController);

	ProcessFilterCardController.$inject = ['$scope', 'ChangeEffort', 'CurrentEffort', 'MessageService'];
	function ProcessFilterCardController($scope, ChangeEffort, CurrentEffort, MessageService) {
		$scope.effort = CurrentEffort;
		$scope.apply = function () {
			ChangeEffort($scope.effort);
			$scope.close();
		};

		$scope.close = function () {
			MessageService.closeCard();
		};
	}
})();