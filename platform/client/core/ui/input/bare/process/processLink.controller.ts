(function () {
	'use strict';

	angular
		.module('core')
		.controller('ProcessLinkController', ProcessLinkController);

	ProcessLinkController.$inject = [
		'$scope',
		'ProcessService',
		'LinkProcess',
		'ParentItemId',
		'Item',
		'ParentProcess',
		'ItemColumnId',
		'$q',
		'Modify',
		'OnChange'
	];
	function ProcessLinkController(
		$scope,
		ProcessService,
		LinkProcess,
		ParentItemId,
		Item,
		ParentProcess,
		ItemColumnId,
		$q,
		Modify,
		OnChange
	) {
		let promises = [];
		promises.push(ProcessService
			.getLink(ParentProcess, ParentItemId, ItemColumnId, Item.item_id)
			.then(function (link) {
				$scope.item = link;
			})
		);

		promises.push(ProcessService
			.getColumns(LinkProcess)
			.then(function (cols) {
				angular.forEach(cols, function (column) {
					if (column.DataType == 0 || column.DataType == 1 || column.DataType == 2) {
						$scope.columns.push(column);
					}
				});
			})
		);

		promises.push(ProcessService
			.getItem(Item.process, Item.item_id)
			.then(function (item) {
				$scope.title = item.primary;
			})
		);

		$q.all(promises).then(function () {
			$scope.loading = false;
		}, function () {
			$scope.loadingFailed = true;
		});

		$scope.loading = true;
		$scope.columns = [];
		$scope.canEdit = Modify;

		$scope.itemChange = function () {
			ProcessService.setDataChanged(LinkProcess, $scope.item.item_id);
			ProcessService.setDataChanged(ParentProcess, ParentItemId);

			OnChange();
		};
	}
})();