﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputProcess', InputProcess);

	InputProcess.$inject = [
		'InputService',
		'$q',
		'$timeout',
		'ProcessService',
		'ViewService',
		'PortfolioService',
		'Common',
		'MessageService',
		'CalendarService',
		'SaveService'
	];

	function InputProcess(
		InputService,
		$q,
		$timeout,
		ProcessService,
		ViewService,
		PortfolioService,
		Common,
		MessageService,
		CalendarService,
		SaveService
	) {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				model: '=?',
				label: '@?',
				filterSettings: '<?settings',
				placeholder: '@?',
				process: '@?',
				onChange: '&?',
				onChangeParameters: '<?',
				modifyResult: '&?'
			},
			templateUrl: function (element, attrs) {
				if (typeof attrs.chip === 'undefined') {
					if (typeof attrs.table === 'undefined') {
						return 'core/ui/input/bare/process/process.html';
					}
					return 'core/ui/input/bare/process/process.table.html';
				}
				return 'core/ui/input/bare/process/process.chips.html';
			},
			link: postLink,
			controller: function ($scope, UniqueService) {
				$scope.id = UniqueService.get('input-process', true);
				if (!Array.isArray($scope.model)) $scope.model = [];
				$scope.$on('DragProcessStop', function () {
					$scope.processOnDrag = false;
				});
			}
		};

		function postLink(scope, element, attrs) {

			let linkProcess = attrs.linkProcess;
			let parentId = attrs.parent;
			let openerItemId = attrs.openeritemid;
			let itemColumnId = attrs.itemColumnId;
			let parentProcess = attrs.parentProcess;
			let hasLink = linkProcess && parentId && itemColumnId && parentProcess;
			let openable = typeof attrs.openable !== 'undefined';
			let splitWidth = attrs.openable;

			let fromProcessColumnName: string = attrs.fromprocesscolumnname;
			let fromProcessColumnId: number = attrs.fromprocesscolumnid;

			let targetProcessColumnName: string = attrs.targetprocesscolumnname;
			let targetProcessColumnId: number = attrs.targetprocesscolumnid;

			let targetProcessName: string = attrs.targetprocessname;

			let filterProcesses: boolean = (
				fromProcessColumnName && targetProcessName
				&& fromProcessColumnName != "" && targetProcessName != "");



			let advancedFilter = typeof attrs.filterable !== 'undefined';

			let selfTriggered = false;
			let preloadItems = [];
			let initialLoad = [];

			scope.max = typeof attrs.max === 'undefined' ? 0 : Number(attrs.max);
			scope.items = [];
			scope.loading = true;
			scope.requiredField = typeof attrs.requiredField !== 'undefined';
			scope.tabindex = scope.modify ? '0' : '-1';
			scope.rowcount = 0;

			let settings = {
				process: scope.process,
				label: scope.label,
				minimal: typeof attrs.minimal !== 'undefined',
				direction: typeof attrs.direction !== 'undefined' && attrs.direction === 'parent' ? 'parent' : 'child',
				portfolio: typeof attrs.portfolioId !== 'undefined' && attrs.portfolioId,
				portfolioId: attrs.portfolioId,
				preload: typeof attrs.preload !== 'undefined',
				chip: typeof attrs.chip !== 'undefined',
				minimalChip: attrs.chip === 'minimal',
				table: typeof attrs.table !== 'undefined',
				swap: undefined,
				max: attrs.max
			};
			scope.minimalChip = settings.minimalChip;
			let clickTargets = '.process-add-minimal, .process-add, label';
			if (!settings.table && !settings.chip) {
				clickTargets += ', input';
			}

			element.on('click', clickTargets, function (event) {
				event.stopPropagation();
				event.preventDefault();
				settings.swap = false;
				openMenu();
			});

			let isParentDirection = settings.direction === 'parent';
			if (scope.max === 1 &&
				(Array.isArray(scope.model) && scope.model.length <= 1) &&
				!(settings.chip || settings.table)
			) {
				element.addClass('one-process');
			}

			if (isParentDirection) {
				initialLoad.push(ProcessService
					.getRelativeParents(settings.process, parentId, itemColumnId)
					.then(function (items) {
						scope.items = items;
					})
				);
			} else {
				initialLoad.push(ProcessService
					.getItems(settings.process, scope.model)
					.then(function (items) {
						scope.items = items;
						updateItems();
					})
				);
			}

			let filters = {limit: 50, filters: undefined};
			let initF = function () {
				initialLoad.push(PortfolioService
					.getSimplePortfolioData(settings.process, settings.portfolioId, filters)
					.then(function (rows) {
						scope.rowcount = rows.rowcount;
						let promises = [];
						for (let i = 0, l = rows.rowdata.length; i < l; i++) {
							promises.push(ProcessService.prepareItem(settings.process, rows.rowdata[i]));
						}

						return $q.all(promises).then(function () {
							preloadItems = rows.rowdata.sort(function (a, b) {
								return a.primary < b.primary ? -1 : 1;
							});

						});
					})
				);
			};

			let complete = function () {
				$q.all(initialLoad).then(function () {
					scope.loading = false;
				});
			};

			if (settings.preload && settings.portfolio) {
				if (filterProcesses) {
					//In this case process field does not have a direct parent but exists in a creation dialog
					if (openerItemId && targetProcessColumnName && targetProcessColumnName != "") {
						ProcessService.getItem(targetProcessName, openerItemId).then(function (result) {
							filters.filters = {};
							filters.filters[fromProcessColumnId] = result[targetProcessColumnName];
							initF();
							complete();
						});
					}

					//In this case the process field sits directly on its parent process
					if (parentId) {
						ProcessService.getItem(parentProcess, parentId).then(function (result) {
							filters.filters = {};
							let r = result[fromProcessColumnName];
							filters.filters[targetProcessColumnId] = [r];
							if (Array.isArray(r)) filters.filters[targetProcessColumnId] = r;
							initF();
							complete();
						});
					}
				} else {
					initF();
					complete();
				}
			} else {
				complete();
			}

			scope.linkProcessChange = function (col, item) {
				let itemId = item.itemId;
				if (col.Process == "task_user_effort" && col.Name == "fte" && scope.linkData[itemId].hasOwnProperty("effort")) {
					CalendarService.getUserCalendar(itemId, new Date(), new Date()).then(function () {
						let hours = CalendarService.getUserWorkHours(itemId);
						scope.linkData[itemId]["effort"] = scope.linkData[itemId]["fte"] * hours.workHours;
						ProcessService.setDataChanged(parentProcess, parentId);
					});
				} else if (col.Process == "task_user_effort" && col.Name == "effort" && scope.linkData[itemId].hasOwnProperty("fte")) {
					CalendarService.getUserCalendar(itemId, new Date(), new Date()).then(function () {
						let hours = CalendarService.getUserWorkHours(itemId);
						scope.linkData[itemId]["fte"] = scope.linkData[itemId]["effort"] / hours.workHours;
						ProcessService.setDataChanged(parentProcess, parentId);
					});
				} else {
					ProcessService.setDataChanged(parentProcess, parentId);
				}
			};

			let getParentState = function () {
				return ProcessService
					.getItemData(parentProcess, parentId)
					.then(function (parentData) {
							let state = parentData.Data.current_state;
							if (typeof state === 'string' &&
								state.indexOf('{') === -1 &&
								_.includes(state, 'tab')
							) {
								return state.split('_')[1];
							}
						}
					);
			};

			scope.openProcess = function (item) {
				if (openable) {
					if (splitWidth === 'full') {
						getParentState().then(function (tabId) {
							ViewService.view('process.tab',
								{
									params: {
										itemId: item.itemId,
										process: settings.process,
										tabId: tabId
									}
								});
						});
					} else {
						ViewService.view('featurette',
							{
								params: {
									itemId: item.actual_item_id ? item.actual_item_id : item.itemId,
									process: settings.process,
									title: item.primary,
									disableNextPrev: true
								}
							},
							{
								split: true,
								size: splitWidth
							});
					}
				}
			};

			scope.openLink = function (item, event) {
				if (hasLink) {
					let p = {
						template: 'core/ui/input/bare/process/processLink.html',
						controller: 'ProcessLinkController',
						locals: {
							LinkProcess: linkProcess,
							ParentItemId: parentId,
							ParentProcess: parentProcess,
							Item: item,
							Modify: scope.modify,
							ItemColumnId: itemColumnId,
							OnChange: inputChanged
						}
					};

					MessageService.card(p, event.currentTarget);
				}
			};

			scope.$on('DragProcessStart', function (e, process_) {
				if (scope.canAdd() && settings.process === process_) {
					scope.processOnDrag = true;
				}
			});

			scope.onDrop = function (process, itemId) {
				scope.loading = true;
				ProcessService.getItem(process, itemId).then(function (item) {
					addItem(item);
					scope.loading = false;
				});
			};

			scope.removeItem = function (itemId, event) {
				if (event) {
					event.stopPropagation();
				}

				if (isParentDirection) {
					scope.loading = true;
					ProcessService
						.removeRelativeParent(
							settings.process,
							itemId,
							itemColumnId,
							parentId
						).then(function () {
						let i = scope.items.length;
						while (i--) {
							if (scope.items[i].itemId == itemId) {
								if (scope.linkData && scope.linkData[itemId]) delete scope.linkData[itemId];
								scope.items.splice(i, 1);
								scope.loading = false;
								inputChanged();
								break;
							}
						}
					}, function () {
						MessageService.toast('NO_RIGHTS', 'warn');
						scope.loading = false;
					});
				} else {
					let index = scope.model.indexOf(itemId);
					if (index === -1) {
						return;
					}
					if (scope.linkData && scope.linkData[itemId]) delete scope.linkData[itemId];
					scope.model.splice(index, 1);
					inputChanged();
				}
			};

			scope.determineAction = function (itemId, event, actualItemId) {
				if (itemId.length === 0 && scope.canAdd()) {
					event.stopPropagation();
					event.preventDefault();
					openMenu();
				} else if (scope.max === 1) {
					event.stopPropagation();
					event.preventDefault();
					settings.swap = true;
					openMenu();
				} else {
					scope.removeItem(typeof actualItemId != 'undefined' ? actualItemId : itemId, event);
				}
			};

			let linkOptionsCache = {};
			scope.getLinkListOptions = function (options, item) {
				if (options && item) {
					if (!linkOptionsCache[item.item_id]) {
						if (linkProcess == 'task_competency_effort' && options) {
							let result = [];
							for (let o of options) if (item.cost_center && item.cost_center.includes(o.item_id)) result.push(o);
							linkOptionsCache[item.item_id] = result;
						} else
							linkOptionsCache[item.item_id] = options;
					}
					return linkOptionsCache[item.item_id];
				}
				return options;
			};

			scope.getLinkSum = function (col) {
				if (!(col.DataType == 1 || col.DataType == 2)) return "";

				let r = 0;
				_.each(scope.linkData, function (i) {
					r += i[col.Name];
				});
				return Common.formatNumber(r, 2);
			};

			if (!settings.chip && hasLink) {
				scope.listOptions = {};
				scope.linkData = {};
				scope.linkColumns = [];
				scope.hasLinkNumbers = false;
				ProcessService.getColumns(linkProcess).then(function (cols) {
					angular.forEach(_.orderBy(cols, ['ItemColumnId'], ['asc']), function (column) {
						if (column.DataType == 0 ||
							column.DataType == 1 ||
							column.DataType == 2
						) {
							scope.hasLinkNumbers = true;
							scope.linkColumns.push(column);
						} else if (column.DataType == 6) {
							ProcessService.getItems(column.DataAdditional).then(function (items) {
								scope.listOptions[column.ItemColumnId] = items;
								scope.linkColumns.push(column);
							});
						}
					});
				});
			}

			scope.canAdd = function () {
				if (Common.isTrue(scope.modify)) {
					return scope.max === 0 || (isParentDirection ? scope.items.length < scope.max : scope.model.length < scope.max);
				}

				return false;
			};

			function openMenu() {
				if (scope.canAdd() || settings.swap) {
					let w = ViewService.widthMode();

					if (!isParentDirection &&
						advancedFilter &&
						settings.portfolio &&
						(w === 'NORMAL' || w === 'LARGE')
					) {
						ViewService.view('processFilter', {
							params: {
								swap: settings.swap,
								process: settings.process,
								parentProcess: parentProcess,
								parentId: parentId,
								itemColumnId: itemColumnId,
								preload: settings.preload,
								portfolioId: settings.portfolioId,
								label: settings.label,
								max: scope.max
							},

							locals: {
								LinkData: scope.linkData,
								ParentColumns: scope.filterSettings,
								Model: scope.items,
								OnFinish: function (items) {
									let i = scope.model.length;
									while (i--) {
										let m = scope.model[i];
										let j = items.length;
										while (j--) {
											if (items[j].itemId == m) {
												break;
											}
										}

										if (j === -1) {
											scope.removeItem(m)
										}
									}

									for (let i = 0, l = items.length; i < l; i++) {
										addItem(items[i]);
									}
								},
							}

						}, {wider: true});

					} else {
						InputService.processMenu(
							isParentDirection ? scope.items : scope.model,
							preloadItems,
							settings,
							element,
							scope,
							function (item) {
								if (settings.swap) {
									if (isParentDirection) {
										let i = scope.items.length;
										while (i--) {
											scope.removeItem(scope.items[i].itemId);
										}
									} else {
										let i = scope.model.length;
										while (i--) {
											scope.removeItem(scope.model[i]);
										}
									}
								}

								if (item) {
									addItem(item);
								}
							},
							scope.modifyResult
						);
					}
				}
			}

			function linkData(item) {
				return ProcessService.getLink(parentProcess, parentId, itemColumnId, item.itemId).then(function (link) {
					if(link.cost_center && link.cost_center.length == 0){
						let id = scope.linkColumns.find(f => f.Name == 'cost_center').ItemColumnId;
						if(id){
							let o = scope.getLinkListOptions(scope.listOptions[id], item)
							if(o && o.length > 0) link.cost_center = [o[0].item_id]
						}
					}
					scope.linkData[item.itemId] = link;
				});
			}

			function alreadySelected(itemId) {
				if (isParentDirection) {
					let i = scope.items.length;
					while (i--) {
						if (scope.items[i].itemId == itemId) {
							return true;
						}
					}

					return false;
				} else {
					return scope.model.indexOf(itemId) !== -1;
				}
			}

			function inputChanged() {
				InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				ProcessService.getColumn(scope.process, itemColumnId).then(function (itemColumn) {
					if (itemColumn.CheckConditions) setTimeout(function () { SaveService.tick(true); }, 100);
				});
			}

			function updateItems() {
				scope.loading = true;
				let promises = [];
				for (let i = 0, l = scope.model.length; i < l; i++) {
					let j = scope.items.length;
					while (j--) {
						if (scope.model[i] == scope.items[j].itemId) {
							if (!settings.chip && hasLink) promises.push(linkData(scope.items[j]));
							break;
						}
					}

					if (j === -1) {
						promises.push(ProcessService
							.getItem(settings.process, scope.model[i])
							.then(function (item) {

								scope.items.push(item);
								if (!settings.chip && hasLink) {
									return linkData(item);
								}
							})
						);
					}
				}

				let i = scope.items.length;
				while (i--) {
					let j = scope.model.length;
					while (j--) {
						if (scope.model[j] == scope.items[i].itemId) {
							break;
						}
					}

					if (j === -1) {
						if (!settings.chip && hasLink) {
							ProcessService.invalidateLink(
								parentProcess,
								parentId,
								scope.items[i].itemId,
								itemColumnId
							);
						}

						scope.items.splice(i, 1);
					}
				}

				scope.contains = scope.model.length ? 'set' : '';
				$q.all(promises).then(function () {
					scope.loading = false;
				});
			}

			function addItem(item) {
				if (!alreadySelected(item.itemId)) {
					if (isParentDirection) {
						scope.loading = true;
						ProcessService
							.addRelativeParent(
								settings.process,
								item.itemId,
								itemColumnId,
								parentId
							).then(function () {
							scope.items.push(item);
							scope.loading = false;
							inputChanged();
						}, function () {
							MessageService.toast('NO_RIGHTS', 'warn');
							scope.loading = false;
						});
					} else {
						scope.model.push(item.itemId);
						inputChanged();
					}
				}
			}

			if (!isParentDirection) {
				scope.$watchCollection('model', function (newModel, oldModel) {
					if (newModel === oldModel || selfTriggered) {
						selfTriggered = false;
						return;
					}

					if (Array.isArray(scope.model)) {
						selfTriggered = false;
					} else {
						scope.model = [];
						selfTriggered = true;
					}

					updateItems();
				});
			}
		}
	}
})();
