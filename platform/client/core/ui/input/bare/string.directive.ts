﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputString', InputString);

	InputString.$inject = [];
	function InputString() {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				model: '=',
				label: '@?',
				max: '@?',
				placeholder: '@?',
				onChange: '&?',
				onChangeParameters: '<?',
				onBlur: '&?',
				onFocus: '&?'
			},
			templateUrl: 'core/ui/input/bare/string.html',
			link: function (scope, element, attrs) {
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				scope.spellcheck = typeof attrs.spellcheck !== 'undefined';
				scope.tabindex = scope.modify ? '0' : '-1';
				if (attrs.unique && attrs.unique != '') {
					let components = attrs.unique.split(",");
					scope.unique = {
						process: components[0],
						itemColumnId: components[1],
						itemId: components[2]
					};
				}
				scope.invalid = false;
			},
			controller: function ($scope, InputService, UniqueService, ProcessService) {
				$scope.id = UniqueService.get('input-string', true);
				$scope.utimer = undefined;
				if (!$scope.label) $scope.placeholderValue = $scope.placeholder;
				$scope.onBlurFunction = function () {
					if ($scope.label) $scope.placeholderValue = '';
					if (typeof $scope.onBlur === 'function') $scope.onBlur();
				};
				$scope.onFocusFunction = function() {
					if ($scope.label) $scope.placeholderValue = $scope.placeholder;
					if (typeof $scope.onFocus === 'function') $scope.onFocus();
				};
				$scope.inputChanged = function () {
					if ($scope.unique) {
						let updateFunction = function() {
							ProcessService.isValueUnique($scope.unique.process, $scope.unique.itemColumnId, $scope.unique.itemId, $scope.model).then(function(result) {
								if (result) {
									InputService.inputChanged($scope.onChange, $scope.onChangeParameters);
									$scope.invalid = false;
								} else {
									$scope.invalid = true;
								}
							});
						};
						
						if ($scope.utimer) clearTimeout($scope.utimer);
						$scope.utimer = setTimeout(updateFunction,500);
					} else {
						InputService.inputChanged($scope.onChange, $scope.onChangeParameters);
					}
				};
			}
		};
	}
})();
