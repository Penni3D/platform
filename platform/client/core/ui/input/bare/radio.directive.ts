﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputRadio', InputRadio);

	InputRadio.$inject = ['Common'];
	function InputRadio(Common) {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				options: '<',
				model: '=',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/bare/radio.html',
			link: function (scope, element, attrs) {
				scope.name = attrs.optionName ? attrs.optionName : 'name';
				scope.value = attrs.optionValue ? attrs.optionValue : 'value';
				scope.group = attrs.group ? 'input-radio-' + attrs.group : scope.id;
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
			},
			controller: function ($scope, InputService, UniqueService) {
				$scope.id = UniqueService.get('input-radio', true);

				$scope.labelClick = function (event) {
					if (Common.isFalse($scope.modify)) {
						event.preventDefault();
					}
				};

				$scope.inputChanged = function () {
					InputService.inputChanged($scope.onChange, $scope.onChangeParameters);
				};
			}
		};
	}
})();
