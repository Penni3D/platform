﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputDate', InputDate)
		.directive('dateElement', DateElement);

	InputDate.$inject = ['InputService', '$rootScope', 'CalendarService', '$timeout', 'UniqueService', 'Common'];
	function InputDate(InputService, $rootScope, CalendarService, $timeout, UniqueService, Common) {
		let date_matcher = '';
		let datetime_matcher = '';
		let separator = '';

		switch ($rootScope.dateFormat) {
			case 'dd.mm.yyyy':
			case 'dd.mm.yy':
			case 'd.M.yyyy':
			case 'd.M.yy':
				date_matcher = 'D.M.YYYY';
				separator = '.';
				break;
			case 'dd-mm-yyyy':
			case 'dd-mm-yy':
				date_matcher = 'D-M-YYYY';
				separator = '-';
				break;
			case 'dd/mm/yyyy':
			case 'dd/mm/yy':
				date_matcher = 'D/M/YYYY';
				separator = '/';
				break;
			case 'mm.dd.yyyy':
			case 'mm.dd.yy':
				date_matcher = 'M.D.YYYY';
				separator = '.';
				break;
			case 'mm-dd-yyyy':
			case 'mm-dd-yy':
				date_matcher = 'M-D-YYYY';
				separator = '-';
				break;
			case 'mm/dd/yyyy':
			case 'mm/dd/yy':
				date_matcher = 'M/D/YYYY';
				separator = '/';
				break;
			case 'd M yyyy':
			case 'd M yy':
				date_matcher = 'D M YYYY';
				separator = ' ';
				break;
		}

		datetime_matcher = date_matcher + ' H.m';

		return {
			restrict: 'E',
			scope: {
				modify: '<',
				model: '=?',
				label: '@?',
				placeholder: '@?',
				settings: '<?',
				modelStart: '=?',
				modelEnd: '=?',
				onChange: '&?',
				onChangeParameters: '<?',
				time: '<?'
			},
			templateUrl: 'core/ui/input/bare/date/date.html',
			controller: function ($scope, $rootScope) {
				if ($rootScope.dateFormat == "dd/mm/yy") $scope.dateStyle = "dd/mm/yyyy";
				if ($rootScope.dateFormat == "mm/dd/yy") $scope.dateStyle = "mm/dd/yyyy";
				if ($rootScope.dateFormat == "dd.mm.yy") $scope.dateStyle = "dd.mm.yyyy";
				if (typeof $scope.settings === 'undefined') {
					$scope.settings = {};
				}
			},
			link: function (scope, element, attrs) {
				scope.id = UniqueService.get('input-date', true);
				scope.pattern = /^[0-9/. -]*$/;
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				scope.errorText = "";

				if (scope.time)
					scope.matcher = datetime_matcher;
				else
					scope.matcher = date_matcher;

				let $input = element.find('input');
				let separatedRange = typeof attrs.group !== 'undefined' &&
					(attrs.modelStart || attrs.modelEnd);

				let settings = {
					dateRange: typeof attrs.timeline !== 'undefined' || separatedRange,
					max: typeof attrs.max === 'undefined' ? 0 : attrs.max,
					min: typeof attrs.min === 'undefined' ? 0 : attrs.min,
					group: attrs.group,
					nextVisibleMonths: typeof scope.settings.next === 'undefined' ? 0 : scope.settings.next,
					previousVisibleMonths: typeof scope.settings.prev === 'undefined' ? 0 : scope.settings.prev,
					CalendarConnection: attrs.calendarConnection
				};
				scope.tabindex = scope.modify ? '0' : '-1';
				let isInvalid = false;
				let isStartDate = false;
				let userChanges = false;
				if (settings.dateRange && separatedRange) {
					if (attrs.modelStart) {
						isStartDate = true;
					}

					CalendarService.subscribeCalendarInputChange(scope, function (data) {
						if (isStartDate) {
							if (data.type === 'end') {
								if ('start' in data) {
									scope.modelStart = data.start;
									inputChanged()
								} else if (isInvalid) {
									scope.validateDate();
								}
							}
						} else {
							if (data.type === 'start') {
								if ('end' in data) {
									scope.modelEnd = data.end;
									inputChanged()
								} else if (isInvalid) {
									scope.validateDate();
								}
							}
						}
					}, settings.group);
				}

				scope.openDatepicker = function (event) {
					event.stopPropagation();
					let model = {
						start: undefined,
						end: undefined,
						connection: undefined,
						isStartDate: true,
						time: false
					};
					if (settings.dateRange) {
						if (separatedRange) {
							let groupDates = CalendarService.getGroupDates(settings.group);
							if (isStartDate) {
								model.start = scope.modelStart;
								model.end = groupDates.end;
								model.connection = settings.CalendarConnection;
							} else {
								model.start = groupDates.start;
								model.end = scope.modelEnd;
								model.connection = settings.CalendarConnection;
								model.isStartDate = false;
							}
						} else {
							model.start = scope.modelStart;
							model.end = scope.modelEnd;
							model.connection = settings.CalendarConnection;
						}
					} else {
						if (scope.time) {
							if (scope.model === null)
								model.start = null;
							else
								model.start = moment(scope.userModel, scope.matcher, true).format();
						}
						else
							model.start = scope.model;
					}
					model.time = scope.time;

					InputService.datepicker(model, settings, element, scope, onSelect);
				};

				scope.validateDate = function () {
					let s = scope.userModel;
					if (typeof s !== 'string' || !s.length) {
						if (Common.isFalse(scope.requiredField)) {
							removeInvalidClass();
						}

						isInvalid = false;

						if (settings.dateRange) {
							if (separatedRange) {
								if (isStartDate) {
									scope.modelStart = null;
								} else {
									scope.modelEnd = null;
								}
							} else {
								scope.modelStart = null;
								scope.modelEnd = null;
							}
						} else {
							scope.model = null;
						}

						inputChanged();

						return;
					}

					if (settings.dateRange && !separatedRange) {
						if (s.length === 8 && /^[0-9]+$/.test(s)) {
							scope.userModel =
								s.substr(0, 2) + separator + s.substr(2, 2) + separator + s.substr(4) + ' - ';
						} else if (moment(scope.userModel, scope.matcher, true).isValid()) {
							scope.userModel += ' - ';
						}

						let dates = scope.userModel.split(' - ');

						if (typeof dates[1] !== 'undefined' && dates[1].length === 8 && /^[0-9]+$/.test(dates[1])) {
							scope.userModel = scope.userModel.slice(0, -8) +
								dates[1].substr(0, 2) + separator + dates[1].substr(2, 2) + separator + dates[1].substr(4);
						}

						let m = moment(scope.userModel, scope.matcher + ' - ' + scope.matcher, true);
						if (m.isValid()) {
							let startDate = CalendarService.getUTCDate(moment(dates[0], scope.matcher).toDate());
							let endDate = CalendarService.getUTCDate(moment(dates[1], scope.matcher).toDate());

							if (startDate.getTime() > endDate.getTime()) {
								addInvalidClass();
								isInvalid = true;
							} else {
								removeInvalidClass();
								isInvalid = false;

								scope.modelStart = startDate;
								scope.modelEnd = endDate;
								userChanges = true;

								inputChanged();
							}
						} else {
							isInvalid = true;
							addInvalidClass();
						}
					} else {
						if (!scope.time && s.length === 8 && /^[0-9]+$/.test(s)) {
							scope.userModel = s.substr(0, 2) + separator + s.substr(2, 2) + separator + s.substr(4);
						}
						else if (scope.time && s.length === 12 && /^[0-9]+$/.test(s)) {
							scope.userModel = s.substr(0, 2) + separator + s.substr(2, 2) + separator + s.substr(4, 4) + ' ' + s.substr(8, 2) + '.' + s.substr(10);
						}

						let m = moment(scope.userModel, scope.matcher, true);
						if (m.isValid()) {
							isInvalid = false;
							removeInvalidClass();

							let date;
							if (scope.time) {
								let d = CalendarService.getUTCDateTime(m.toDate());
								date = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes()));
							}
							else
								date = CalendarService.getUTCDate(m.toDate());

							if (separatedRange) {
								let groupDates = CalendarService.getGroupDates(settings.group);
								if (isStartDate) {
									if (isBefore(date, groupDates.end)) {
										scope.modelStart = date;
										removeInvalidClass();
										$timeout(function () {
											CalendarService.notifyCalendarInputChange({
												type: 'start',
												start: scope.modelStart
											}, settings.group);
										});

										userChanges = true;
										inputChanged();
									} else {
										isInvalid = true;
										addInvalidClass();
									}
								} else {
									if (isBefore(groupDates.start, date)) {
										scope.modelEnd = date;
										removeInvalidClass();

										$timeout(function () {
											CalendarService.notifyCalendarInputChange({
												type: 'end',
												end: scope.modelEnd
											}, settings.group);
										});

										userChanges = true;
										inputChanged();
									} else {
										isInvalid = true;
										addInvalidClass();
									}
								}
							} else {
								scope.model = date;
								userChanges = true;
								inputChanged();
							}
						} else {
							isInvalid = true;
							addInvalidClass();
						}
					}
				};

				if (settings.dateRange) {
					scope.$watch('modelStart + modelEnd', function () {
						if (separatedRange) {
							isStartDate ?
								CalendarService.setGroupStartDate(settings.group, scope.modelStart) :
								CalendarService.setGroupEndDate(settings.group, scope.modelEnd);
						}

						scope.updateDateValue();
					});
				} else {
					scope.$watch('model', function () {
						scope.updateDateValue();
					});
				}

				scope.blurred = function () {
					if (typeof scope.userModel !== 'string' || !scope.userModel.length) {
						scope.userModel = '';

						if (Common.isFalse(scope.requiredField)) {
							removeInvalidClass();
						}
					} else if (isInvalid) {
						if (settings.dateRange) {
							let start = fd(scope.modelStart);
							let end = fd(scope.modelEnd);

							if (separatedRange) {
								if (isStartDate && start) {
									scope.userModel = start;
									removeInvalidClass();
								} else if (end) {
									scope.userModel = end;
									removeInvalidClass();
								} else {
									scope.userModel = '';
								}
							} else {
								if (start && end) {
									scope.userModel = start + ' - ' + end;
									removeInvalidClass();
								} else {
									scope.userModel = '';
								}
							}
						} else {
							if (scope.model) {
								if (scope.time) {
									let m = moment(scope.userModel, scope.matcher, true);
									scope.userModel = fd(m.toDate(), false, false);
								}
								else
									scope.userModel = fd(scope.model, false, false);

								removeInvalidClass();
							} else {
								scope.userModel = '';
							}
						}

						if (Common.isFalse(scope.requiredField)) {
							removeInvalidClass();
						}
					} else {
						scope.updateDateValue();
					}
				};

				function addInvalidClass() {
					$input.addClass('ng-invalid');
				}

				function removeInvalidClass() {
					$input.removeClass('ng-invalid');
				}

				function fd(d, w?, c?, a?, t?) {
					if (scope.time) {
						return CalendarService.formatDateTime(d, w, c, a, t);
					} else {
						return CalendarService.formatDate(d, w, c, a, t);
					}
				}

				scope.updateDateValue = function () {
					if (userChanges) {
						return userChanges = !userChanges;
					}

					if (isInvalid) {
						return;
					}

					if (settings.dateRange) {
						let start = fd(scope.modelStart);
						let end = fd(scope.modelEnd);

						if (separatedRange) {
							scope.userModel = isStartDate ? start : end;
						} else {
							scope.userModel = start;

							if (start && end) {
								scope.userModel += ' - ' + end;
							}
						}
					} else {
						if (typeof scope.model === 'string') {
							scope.userModel = fd(scope.model);
						} else {
							if (scope.time) {
								if(scope.model != null){
									let m = moment(scope.userModel, scope.matcher, true);
									scope.userModel = fd(m.toDate(), false, false);
								} else {
									scope.userModel = null;
								}

							}
							else {
								scope.userModel = fd(scope.model, false, false);
							}

						}
					}
				};

				function onSelect(startDate, endDate) {
					isInvalid = false;
					userChanges = false;
					removeInvalidClass();
					if (settings.dateRange) {
						if (separatedRange) {
							if (isStartDate) {
								scope.modelStart = startDate;
							} else {
								scope.modelEnd = endDate;
							}

							CalendarService.notifyCalendarInputChange({
								type: isStartDate ? 'start' : 'end',
								start: startDate,
								end: endDate
							}, settings.group);
						} else {
							scope.modelStart = startDate;
							scope.modelEnd = endDate;
						}
					} else {
						if (scope.time) {
							let d = CalendarService.getUTCDateTime(startDate);
							scope.userModel = fd(startDate, false, false);
							scope.model = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes()));
						}
						else
							scope.model = startDate;
					}

					inputChanged();
				}

				function inputChanged() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}

				function isBefore(startDate, endDate) {
					if (typeof startDate === 'undefined' || typeof endDate === 'undefined' ||
						startDate === null || endDate === null
					) {
						return true;
					}

					let s = new Date(startDate);
					let e = new Date(endDate);

					if (!s || !e) {
						return false;
					} else {
						return s.getTime() <= e.getTime()
					}
				}
			}
		}
	}

	DateElement.$inject = [];
	function DateElement() {
		return {
			restrict: 'A',
			scope: {
				dateElement: '<'
			},
			link: function (scope, element) {
				scope.$watch('dateElement', function (status) {
					element.removeClass('gap-date');
					element.removeClass('circle-date');
					element.removeClass('start-date');
					element.removeClass('end-date');

					switch (status) {
						case 'c':
							element.addClass('circle-date');
							break;
						case 'g':
							element.addClass('gap-date');
							break;
						case 's':
							element.addClass('start-date');
							break;
						case 'e':
							element.addClass('end-date');
							break;
						case 'se':
							element.addClass('start-date');
							element.addClass('end-date');
							break;
					}
				});
			}
		};
	}
})();