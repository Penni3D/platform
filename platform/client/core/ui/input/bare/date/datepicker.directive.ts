(function () {
	'use strict';

	angular
		.module('core')
		.directive('datepicker', DatePicker);

	DatePicker.$inject = ['Common', 'CalendarService', '$q', '$compile', 'InputService', 'UserService', '$rootScope', 'HTMLService'];

	function DatePicker(Common, CalendarService, $q, $compile, InputService, UserService, $rootScope, HTMLService) {
		return {
			templateUrl: 'core/ui/input/bare/date/datepicker.html',
			link: function (scope, element) {
				let weekDayNames = Common.getLocalShortDaysOfWeek();
				let shortWeekDayNanes = [undefined];
				for (let i = 0; i < 7; i++) {
					shortWeekDayNanes.push(weekDayNames[i][0]);
				}

				let monthNamesShort = Common.getLocalShortMonthsOfYear();
				let monthNames = Common.getLocalMonthsOfYear();
				let currentDate = CalendarService.getUTCDate();
				currentDate.setUTCDate(1);
				let today = CalendarService.getUTCDate();
				let weekStartsMonday = $rootScope.clientInformation.first_day_of_week == 1;
				let connected = !(scope.model.connection == "false");
				let startDate;
				let endDate;
				scope.errorText = "";
				scope.lastSet = 'e';
				if (scope.model.isStartDate == false) scope.lastSet = 's';

				scope.selectedDays = {};
				scope.months = Common.getLocalMonthsOfYear();

				if (scope.model.start) {
					if (scope.model.time) {
						startDate = CalendarService.getUTCDateTime(scope.model.start, undefined, true);
						currentDate = new Date(Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()));
						currentDate.setDate(1);
					}
					else {
						startDate = CalendarService.getUTCDate(scope.model.start);
						currentDate = new Date(startDate);
						currentDate.setUTCDate(1);
					}

					if (scope.settings.dateRange) {
						endDate = scope.model.end ? CalendarService.getUTCDate(scope.model.end) : startDate;
						if (connected) setClass(startDate, endDate);
					} else {
						if (scope.model.time)
							scope.selectedDays[new Date(Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()))] = 'c';
						else
							scope.selectedDays[startDate] = 'c';
					}
				} else if (scope.model.end) {
					endDate = CalendarService.getUTCDate(scope.model.end);
					currentDate = new Date(endDate);
					currentDate.setUTCDate(1);

					if (scope.settings.dateRange) {
						startDate = endDate;
						if (connected) setClass(startDate, endDate);
					} else {
						scope.selectedDays[endDate] = 'c';
					}
				}

				let previousMonths = scope.settings.previousVisibleMonths;
				let nextMonths = scope.settings.nextVisibleMonths;

				let visibleMonths = previousMonths + nextMonths + 1;
				let overflownPixels = monthWidth(visibleMonths) - window.innerWidth;

				let $monthView = element.find('.month-view');
				let $yearPicker = element.find('.year-picker');
				let $monthPicker = element.find('.month-picker');

				if (overflownPixels > 0) {
					let leftOverflownPixels = overflownPixels;

					if (previousMonths) {
						previousMonths = previousMonths - Math.ceil(overflownPixels / 336);
						leftOverflownPixels = overflownPixels - monthWidth(previousMonths);
					}

					if (leftOverflownPixels > 0) {
						nextMonths = nextMonths - Math.ceil(leftOverflownPixels / 336);
						nextMonths = nextMonths < 0 ? 0 : nextMonths;

						previousMonths = 0;
					}

					visibleMonths = nextMonths + previousMonths + 1;
				}

				let isFirst = true;
				let firstBlock = constructBlock();
				let secondBlock;

				let stacks = [];
				let inStack = false;

				scope.currentMonth = currentDate.getUTCMonth();

				scope.cancel = function () {
					InputService.removeDatepicker();
				};

				scope.setMonth = function (month) {
					isFirst ? firstBlock.remove() : secondBlock.remove();

					isFirst = true;
					currentDate.setUTCMonth(month);
					scope.currentMonth = month;
					firstBlock = constructBlock();
					firstBlock.$block.appendTo($monthView);
					setHeader();

					hidePicker($monthPicker);
					showMonthView();
				};

				scope.setYear = function (year) {
					isFirst ? firstBlock.remove() : secondBlock.remove();

					isFirst = true;
					currentDate.setUTCFullYear(year);
					firstBlock = constructBlock();
					firstBlock.$block.appendTo($monthView);
					setHeader();

					hidePicker($yearPicker);
					showMonthView();
				};

				scope.showMonthPicker = function () {
					hidePicker($yearPicker);
					hideMonthView();

					showPicker($monthPicker);
				};

				scope.showYearPicker = function () {
					hidePicker($monthPicker);
					hideMonthView();

					showPicker($yearPicker);
				};

				scope.closePickers = function () {
					hidePicker($yearPicker);
					hidePicker($monthPicker);

					showMonthView();
				};

				function showMonthView() {
					$monthView.css('display', '');

					setTimeout(function () {
						$monthView.addClass('transition-in');
					}, 75);
				}

				function hideMonthView() {
					$monthView.css('display', 'none');
					$monthView.removeClass('transition-in');
				}

				function showPicker($picker) {
					$picker.css('display', 'flex');

					setTimeout(function () {
						$picker.addClass('transition-in');
					}, 75);
				}

				function hidePicker($picker) {
					$picker.css('display', '');
					$picker.removeClass('transition-in');
				}

				scope.setDate = function () {
					if (typeof startDate === 'undefined') {
						InputService.removeDatepicker();
					} else if (typeof endDate === 'undefined') {
						scope.applyDate(startDate, startDate);
						InputService.removeDatepicker();
					} else {
						scope.applyDate(startDate, endDate);
						InputService.removeDatepicker();
					}
				};

				scope.previousMonth = function () {
					stackIt('right');
				};

				scope.nextMonth = function () {
					stackIt('left');
				};

				scope.today = function () {
					stackIt('today');

					let h = startDate.getHours();
					let m = startDate.getMinutes();

					//	if (!startDate) {
					scope.selectDay({ date: new Date(today) });
					//	}
					//	scope.selectDay(startDate)
					startDate = new Date(today);
					scope.blurredTime(h + '.' + m);
				};

				function addDays(date, days) {
					let d = new Date(date);
					d.setDate(date.getDate() + days);
					return d;
				}

				function substractDays(date1, date2) {
					return Math.round((date2 - date1) / 86400000); // = 24 * 60 * 60 * 1000 = hours * minutes * seconds * milliseconds
				}

				let errorTimeOut;
				function showError(text) {
					scope.errorText = text;
					if (errorTimeOut) clearTimeout(errorTimeOut);
					setTimeout(function () {
						scope.errorText = "";
					}, 2000);
				}

				scope.selectDay = function (day) {
					if (typeof day === 'undefined') {
						return;
					}

					scope.selectedDays = {};

					if (scope.settings.dateRange && !connected) {
						if (scope.lastSet == 'e') {
							if (day.date <= endDate) {
								startDate = day.date;
								scope.selectedDays[day.date] = 'c';
							} else {
								endDate = addDays(day.date, substractDays(startDate, endDate));
								startDate = day.date;
							}
						} else if (scope.lastSet == 's') {
							if (day.date >= startDate) {
								endDate = day.date;
								scope.selectedDays[day.date] = 'c';
							} else {
								startDate = addDays(day.date, substractDays(endDate, startDate));
								endDate = day.date;
							}
						}
					} else if (scope.settings.dateRange && connected) {
						if (typeof startDate === 'undefined') {
							scope.lastSet = 's';
							startDate = day.date;
							scope.selectedDays[startDate] = 'se';
						} else {
							let time = day.date.getTime();

							let tempStart = startDate;
							let tempEnd = endDate;

							//Set date
							if (scope.lastSet == 'e') {
								startDate = day.date;
								scope.lastSet = 's';
							} else if (scope.lastSet == 's') {
								endDate = day.date;
								scope.lastSet = 'e';
							}

							//Ensure correct order
							if (startDate > endDate) {
								if (tempEnd > startDate) {
									tempEnd = endDate;
									endDate = startDate;
									startDate = tempEnd;
								} else {
									endDate = startDate;
									startDate = tempEnd;
								}
							}

							setClass(startDate, endDate);
						}
					} else {
						scope.selectedDays[day.date] = 'c';
						startDate = day.date;
						if (scope.model.time) scope.validateTime(scope.timeValue);
					}

					setHeader();
				};

				function monthWidth(months) {
					return months * 336 + 24;
				}

				function shiftLeft() {
					if (isFirst) {
						secondBlock = constructBlock();
						secondBlock.$block.addClass('shift-right');
						firstBlock.$block.after(secondBlock.$block);
					} else {
						firstBlock = constructBlock();
						firstBlock.$block.addClass('shift-right');
						secondBlock.$block.after(firstBlock.$block);
					}

					setTimeout(function () {
						if (isFirst) {
							secondBlock.$block.removeClass('shift-right');
							firstBlock.$block.addClass('shift-left');
						} else {
							firstBlock.$block.removeClass('shift-right');
							secondBlock.$block.addClass('shift-left');
						}

						setTimeout(function () {
							isFirst ? firstBlock.remove() : secondBlock.remove();
							isFirst = !isFirst;

							shiftBlock();
						}, 300);
					}, 75);
				}

				function shiftRight() {
					if (isFirst) {
						secondBlock = constructBlock();
						secondBlock.$block.addClass('shift-left');
						firstBlock.$block.before(secondBlock.$block);
					} else {
						firstBlock = constructBlock();
						firstBlock.$block.addClass('shift-left');
						secondBlock.$block.before(firstBlock.$block);
					}

					setTimeout(function () {
						if (isFirst) {
							secondBlock.$block.removeClass('shift-left');
							firstBlock.$block.addClass('shift-right');
						} else {
							firstBlock.$block.removeClass('shift-left');
							secondBlock.$block.addClass('shift-right');
						}

						setTimeout(function () {
							isFirst ? firstBlock.remove() : secondBlock.remove();
							isFirst = !isFirst;

							shiftBlock();
						}, 300);
					}, 75);
				}

				function stackIt(stack) {
					if (stacks.length === 0) stacks.push(stack);
					if (!inStack && stacks.length === 1) shiftBlock();
				}

				function shiftBlock() {
					let s = stacks.shift();

					if (typeof s === 'undefined') {
						inStack = false;
					} else {
						inStack = true;
						if (s === 'right') {
							currentDate.setUTCMonth(currentDate.getUTCMonth() - visibleMonths);

							shiftRight();
						} else if (s === 'left') {
							currentDate.setUTCMonth(currentDate.getUTCMonth() + visibleMonths);

							shiftLeft();
						} else {
							currentDate = new Date(today);
							currentDate.setUTCDate(1);

							let t = today.getTime();
							let c = currentDate.getTime();

							if (c !== t) {
								c < t ? shiftLeft() : shiftRight();
							}
						}

						scope.currentMonth = currentDate.getUTCMonth();
					}
				}

				function setClass(startDate, endDate) {
					let dateRunner = moment.utc(startDate);
					dateRunner.add(1, 'days');
					while (dateRunner.isBefore(endDate)) {
						scope.selectedDays[dateRunner.toDate()] = 'g';
						dateRunner.add(1, 'days');
					}

					if (endDate.getTime() === startDate.getTime()) {
						scope.selectedDays[startDate] = 'se';
					} else {
						scope.selectedDays[startDate] = 's';
						scope.selectedDays[endDate] = 'e';
					}
				}

				function getCalendarData() {
					let d = $q.defer();
					let firstDate = new Date(currentDate);
					firstDate.setUTCMonth(firstDate.getUTCMonth() - previousMonths);
					firstDate.setUTCDate(1);

					let lastDate = new Date(currentDate);
					lastDate.setUTCMonth(lastDate.getUTCMonth() + nextMonths + 1);
					lastDate.setUTCDate(0);

					let useCalendar = false;
					let userCalendar;
					CalendarService.getUserCalendar(UserService.getCurrentUserId(), firstDate, lastDate).then(function (data) {
						useCalendar = true;
						userCalendar = data;
					}).finally(function () {
						let months = [{
							name: monthNames[firstDate.getUTCMonth()] + ' ' + firstDate.getUTCFullYear(),
							weeks: [
								{
									name: weekStartsMonday ? getWeekNumber(firstDate) : '',
									days: []
								}
							]
						}];

						let w = 0;
						let m = 0;

						while (firstDate <= lastDate) {
							let year = firstDate.getUTCFullYear();
							let month = firstDate.getUTCMonth() + 1;
							let day = firstDate.getUTCDate();

							let t = CalendarService.getWeekDay(firstDate, weekStartsMonday);

							let cWeek = months[m].weeks[w];

							cWeek.days[t] = {
								date: new Date(firstDate),
								day: day,
								today: today.getTime() === firstDate.getTime()
							};

							let dayOfWeek = cWeek.days[t];

							if (useCalendar) {
								let dateCalendar = userCalendar[year][month][day];
								dayOfWeek.dateType = dayOfWeek.today ? 'today ' : '';

								if (dateCalendar.dayOff) dayOfWeek.dateType += 'dayOff ';
								if (dateCalendar.holiday) dayOfWeek.dateType += 'holiday ';
								if (dateCalendar.notWork) dayOfWeek.dateType += 'notWork';
							} else {
								if (t !== 5 && t !== 6) dayOfWeek.dateType = 'notWork';
							}

							firstDate.setUTCDate(firstDate.getUTCDate() + 1);

							if (firstDate.getTime() <= lastDate.getTime()) {
								let wentIn = false;
								let nMonth = firstDate.getUTCMonth();

								if (month !== (nMonth + 1)) {
									wentIn = true;
									m++;
									w = 0;
									months.push({
										weeks: [],
										name: monthNames[nMonth] + ' ' + firstDate.getUTCFullYear()
									});
								} else if (t === 6) {
									wentIn = true;
									w++;
								}

								if (wentIn) {
									months[m].weeks.push({
										name: weekStartsMonday ? getWeekNumber(firstDate) : '',
										days: []
									});
								}
							}
						}
						d.resolve(months)
					});

					return d.promise;
				}

				function constructBlock() {
					let scope_ = scope.$new(false);
					scope_.weekNames = shortWeekDayNanes; //[undefined, 'M', 'T', 'W', 'T', 'F', 'S', 'S'];

					let $monthBlock = $('<div class="month-block">');
					let $loader = $compile('<loader>')(scope_).appendTo($monthBlock);

					getCalendarData().then(function (data) {
						$loader.remove();
						scope_.months = data;
						$compile(HTMLService.initialize('div')
							.class('no-select')
							.repeat('month in ::months')
							.inner(
								HTMLService.initialize()
									.element('span').bind('::month.name')
									.element('div').class('week-days')
									.inner(
										HTMLService.initialize('div')
											.repeat('week in ::weekNames track by $index')
											.class('week-day')
											.bind('::week')
									)
									.element('div').repeat('week in ::month.weeks').class('week')
									.inner(
										HTMLService.initialize()
											.element('div').class('week-number').bind('::week.name')
											.element('div')
											.repeat('day in ::week.days track by $index')
											.class('day-container pointer aria-detect')
											.click('$parent.selectDay(day)')
											.attr('date-element', '$parent.selectedDays[day.date]')
											.attr("tab-index", "0")
											.ngClass('::day.dateType')
											.inner(
												HTMLService
													.initialize('div').tabindex(0)
													.class('day aria-detect')
													.bind('::day.day')
											)
									)
							).getHTML())(scope_).appendTo($monthBlock);
					});

					setHeader();

					return block($monthBlock, scope_);
				}

				function block($handler, scope) {
					return {
						$block: $handler,
						remove: function () {
							this.$block.remove();
							this.scope.$destroy();
						},
						replace: function (newMenu) {
							this.$block.replaceWith(newMenu.$block);
							this.$block = newMenu.$block;
							this.scope.$destroy();
							this.scope = newMenu.scope;
						},
						scope: scope
					};
				}

				function getWeekNumber(date) {
					date = CalendarService.getUTCDate(date);
					date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay() || 7));
					let yearStart = new Date(Date.UTC(date.getUTCFullYear(), 0, 1));

					return Math.ceil((((date - yearStart) / 86400000) + 1) / 7);
				}

				function setHeader() {
					let sDate;
					if (scope.model.start)
						sDate = new Date(Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()));
					else
						sDate = startDate;

					let sd = typeof sDate === 'undefined' ? today : sDate;

					scope.startYear = sd.getUTCFullYear();
					scope.startDates = weekDayNames[weekStartsMonday ? CalendarService.getWeekDay(sd) : sd.getUTCDay()] +
						', ' + monthNamesShort[sd.getUTCMonth()] + ' ' + sd.getUTCDate();

					let sy = typeof sDate === 'undefined' ? currentDate.getUTCFullYear() : sDate.getUTCFullYear();

					scope.yearList = [];
					for (let i = -7; i < 8; i++) {
						scope.yearList.push(sy + i)
					}

					if (scope.settings.dateRange && endDate) {
						let ey = endDate.getUTCFullYear();
						scope.endYear = ey === scope.startYear ? '' : '- ' + ey;
						scope.endDates = '- ' +
							weekDayNames[weekStartsMonday ? CalendarService.getWeekDay(endDate) : endDate.getUTCDay()] +
							', ' + monthNamesShort[endDate.getUTCMonth()] + ' ' + endDate.getUTCDate();
					}
				}

				firstBlock.$block.appendTo($monthView);
				element.css('width', monthWidth(visibleMonths));

				if (scope.model.time) {
					scope.timePattern = /^[0-9.]*$/;
					if (typeof startDate === 'undefined')
						scope.timeValue = '00.00';
					else
						scope.timeValue = CalendarService.formatDateTime(scope.model.start, false, false, false, true);
				}

				scope.validateTime = function (value) {
					if (value === undefined) value = '00:00';

					scope.timeValue = value;

					let hours = 0;
					let minutes = 0;
					let time_parts = scope.timeValue.split('.');
					if (time_parts.length == 2) {
						let h = parseInt(time_parts[0]);
						let m = parseInt(time_parts[1]);
						if (!isNaN(h) && !isNaN(m) && h >= 0 && h <= 23 && m >= 0 && m <= 59) {
							hours = h;
							minutes = m;
						}
					}

					if (typeof startDate !== 'undefined') startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), hours, minutes);
				}

				scope.blurredTime = function (value) {
					if (value === undefined) value = '00:00';

					scope.validateTime(value);
					scope.timeValue = CalendarService.formatDateTime(startDate, false, false, false, true);

					$("input#timePicker").val(scope.timeValue);
				}
			}
		}
	}
})();