﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputRich', InputRich);

	InputRich.$inject = ['InputService', 'Colors', '$timeout', 'PortfolioService', '$rootScope', 'MessageService', 'ApiService'];

	function InputRich(InputService, Colors, $timeout, PortfolioService, $rootScope, MessageService, ApiService) {
		let allowedColors = [
			Colors.getColor('text').rgb,
			Colors.getColor('red').rgb,
			Colors.getColor('purple').rgb,
			Colors.getColor('blue').rgb,
			Colors.getColor('cyan').rgb,
			Colors.getColor('green').rgb,
			Colors.getColor('orange').rgb
		];

		return {
			restrict: 'E',
			scope: {
				label: '@?',
				model: '=',
				onChange: '&?',
				onChangeParameters: '<?',
				modify: '<',
				hasImage: '@?',
				hasColor: '@?',
				portfolioId: '@?',
				itemId: '@?',
				parentProcess: '@?'
			},
			templateUrl: 'core/ui/input/bare/rich.html',
			link: function (scope, element) {
				let selfTrigger = false;
				let modelChanged = false;
				let $quillEditor = $(element).find('.quill-editor');
				scope.originalText = angular.copy(scope.model.html);

				let Items = ApiService.v1api('meta/Items');

				let replaceTags = function () {
					scope.originalText = angular.copy(scope.model.html);
					return Items.new([scope.parentProcess, "replace"], {
						Body: scope.model.html,
						ItemId: scope.itemId
					}).then(function (replacedText) {
						return replacedText;
					});
				}

				scope.templates = [];
				scope.showTemplates = function () {

					let eRowData = {'selected': scope.selectedTemplateText};
					let content = {
						buttons: [
							{
								text: 'CANCEL',
								modify: 'cancel'

							},
							{
								type: 'primary',
								modify: 'create',
								text: 'SAVE',
								onClick: function () {
									scope.selectedTemplateText = eRowData.selected[0];
									scope.applyTemplate();
								}
							}
						],
						inputs: [{
							type: 'select',
							model: 'selected',
							options: scope.templates,
							optionValue: 'body_variable',
							optionName: 'name_variable',
							label: 'SELECT_TEMPLATE',
						}],
						title: 'SELECT_TEMPLATE'
					};
					MessageService.dialog(content, eRowData);
				}
				scope.hasTemplate = scope.hasOwnProperty('portfolioId') && scope.portfolioId.length > 0;
				if (scope.hasTemplate) {
					scope.getRichTextTemplates = function () {
						PortfolioService.getPortfolioData('notification', JSON.parse(scope.portfolioId).PortfolioId).then(function (portfolioData) {
							scope.templates = portfolioData.rowdata;
						});

					}
					scope.getRichTextTemplates();
				}

				scope.applyTemplate = function () {
					MessageService.confirm('APPLY_TEMPLATE_RICH', function () {
						scope.model.html = angular.copy(JSON.parse(scope.selectedTemplateText[$rootScope.me.locale_id])).html;
						replaceTags().then(function (d) {
							quill.root.innerHTML = d;
						})
					});
				}

				let tools = [];
				tools.push(['bold', 'italic', 'underline']);
				if (scope.hasColor == "true") tools.push([{color: allowedColors}]);
				tools.push([{list: 'ordered'}, {list: 'bullet'}]);
				tools.push([{indent: '-1'}, {indent: '+1'}]);
				if (scope.hasImage == "true") tools.push(['link', 'image']); else tools.push(['link']); 
				
				let quillContent = {
					theme: 'snow',
					modules: {
						toolbar: tools
					}
				};
				let quill = new Quill($quillEditor[0], quillContent);

				scope.$watch('model', function () {
					if (selfTrigger) {
						selfTrigger = false;
						return;
					}

					if (scope.model) {
						modelChanged = true;
						quill.setContents(scope.model.delta);
					}
				});

				quill.on('text-change', function () {

					//Idk what is this and why is this, but this commented block prevented copy paste text to work in
					//rich text (helptexts). Removed for now 24.9.21 -Aleksi
					/*if (modelChanged) {
						modelChanged = false;
						return;
					}*/
					selfTrigger = true;
					scope.model = {html: quill.root.innerHTML, delta: quill.getContents()};
					$timeout(function () {
						InputService.inputChanged(scope.onChange, scope.onChangeParameters);
					});
				});

				quill.clipboard.addMatcher(1, function (node, delta) {
					let ops = [];
					angular.forEach(delta.ops, function (op) {
						if (op.insert && typeof op.insert === 'string') {
							ops.push({
								insert: op.insert
							})
						}
					});
					delta.ops = ops;
					return delta;
				});

				$quillEditor.children('.ql-editor')
					.on('focusin', function () {
						$quillEditor.addClass('focused');
					})
					.on('focusout', function () {
						$quillEditor.removeClass('focused');
					});
			},
			controller: function ($scope) {
				if (typeof $scope.model === 'undefined' || $scope.model === null) {
					$scope.model = {};
				}
			}
		};
	}
})();
