(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputRating', InputRating);

	InputRating.$inject = ['InputService', 'Translator', 'Common', 'Colors'];
	function InputRating(InputService, Translator, Common, Colors) {
		return {
			scope: {
				modify: '<',
				label: '@',
				model: '=',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/bare/rating.html',
			link: function (scope, element, attrs) {
				var count = attrs.max ? Number(attrs.max) : 5;
				var group = attrs.group ? Number(attrs.group) : 0;
				var placeholder = attrs.placeholder ? Common.rounder(attrs.placeholder, 4) : 0;

				var trueIcon = attrs.optionTrue ? attrs.optionTrue : 'star';
				var falseIcon = attrs.optionFalse ? attrs.optionFalse : 'star_outline';
				var tc = attrs.optionColor ? Colors.getColor(attrs.optionColor) : Colors.getColor('accent');
				var pc = Colors.getColor('icon');
				
				var trueColor = tc.rgb;
				var inactiveTrueColor = tc.orgb;
				var placeholderColor = pc.rgb;
				var inactivePlaceholderColor = pc.orgb;

				scope.ratings = [];
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				setTranslation();

				scope.$watch('modify', function () {
					var modify = Common.isTrue(scope.modify);
					var i = scope.ratings.length;
					while (i--) {
						var r = scope.ratings[i];
						if (r.selected) {
							r.color = modify ? trueColor : inactiveTrueColor;
						} else {
							r.color = modify ? placeholderColor : inactivePlaceholderColor;
						}
					}
				});

				scope.$watch('model', function () {
					var i = scope.ratings.length;
					var showPlaceholder = true;
					var modify = Common.isTrue(scope.modify);
					while (i--) {
						var r = scope.ratings[i];
						r.placeholder = r.value <= placeholder;
						r.selected = r.value  <= scope.model;
						r.icon = falseIcon;
						r.color = modify ? placeholderColor : inactivePlaceholderColor;

						if (r.selected) {
							r.icon = trueIcon;
							r.color = modify ? trueColor : inactiveTrueColor;
							showPlaceholder = false;
						}
					}

					if (showPlaceholder) {
						var i = scope.ratings.length;
						while (i--) {
							var r = scope.ratings[i];
							if (r.placeholder) {
								r.icon = trueIcon;
							}
						}
					}
				});

				scope.onEnter = function (rating) {
					if (Common.isFalse(scope.modify)) {
						return;
					}

					var i = scope.ratings.length;
					while (i--) {
						var r = scope.ratings[i];

						if (r.value <= rating.value) {
							r.icon = trueIcon;
							r.color = trueColor;
						} else {
							r.icon = falseIcon;
							r.color = placeholderColor;
						}
					}
				};

				scope.onLeave = function () {
					if (Common.isFalse(scope.modify)) {
						return;
					}

					var showPlaceholder = true;
					var i = scope.ratings.length;
					while (i--) {
						var r = scope.ratings[i];
						r.icon = r.selected ? trueIcon : falseIcon;
						r.color = r.selected ? trueColor : placeholderColor;

						if (r.selected) {
							showPlaceholder = false;
						}
					}

					if (showPlaceholder) {
						var i = scope.ratings.length;
						while (i--) {
							var r = scope.ratings[i];
							if (r.placeholder) {
								r.icon = trueIcon;
							}
						}
					}
				};

				for (var i = 1, l = count; i <= l; i++) {
					var r = {
						value: i,
						icon: falseIcon,
						placeholder: false,
						selected: false,
						select: function () {
							onSelect(this);
						}
					};
					scope.ratings.push(r);
				}

				function setTranslation() {
					scope.text = placeholder ?
						Common.rounder(placeholder, 1) + ' ' + Translator.translate('RATING_AVERAGE') : '';

					var totalText = group ? group + ' ' + Translator.translate('RATING_TOTAL_REVIEWS') : '';

					if (scope.text && totalText) {
						scope.text += Translator.translate('RATING_FROM') + ' ';
					}

					scope.text += totalText;
				}

				function onSelect(rating) {
					if (Common.isFalse(scope.modify)) {
						return;
					}

					scope.model = rating.value;
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}
			}
		};
	}
})();