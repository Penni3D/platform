﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('selectMenu', SelectMenu);

	SelectMenu.$inject = ['$compile', 'Common', 'HTMLService', 'Translator', '$timeout', 'Sanitizer'];
	function SelectMenu($compile, Common, HTMLService, Translator, $timeout, Sanitizer) {
		return {
			link: function (scope, element) {
				let inTable = scope.settings.inTable;
				let minimal = scope.settings.minimal;
				let isMultiselect = scope.settings.multiselect;
				let unselectable = scope.settings.unselectable;
				let allselectable = scope.settings.allselectable;
				let showAllselectable = isAllselectableVisible();
				let isFirst = true;
				let $inputSelectMenu = element.closest('input-select-menu');
				let firstMenu = constructMenu(scope.root.children);
				let secondMenu;
				let parents = [];
				let depth = 0;
				let oH = 40;

				scope.$on('$destroy', function () {
					scope.closing();
				});

				let showSearch = false;
				if (scope.root.children.length > 14) {
					showSearch = true;
				} else {
					let i = scope.root.children.length;
					while (i--) {
						if (scope.root.children[i].children.length > 0) {
							showSearch = true;
							break;
						}
					}
				}

				if (showSearch) {
					$compile(
						HTMLService.initialize('input-string')
							.attr('model', 'searchString')
							.attr('minimal')
							.attr('modify', 'true')
							.attr('on-change', 'search')
							.attr('placeholder', Sanitizer(Translator.translate('SEARCH') + " " + scope.root.searchLabel))
							.getHTML())(scope).appendTo(element);

					setTimeout(function () {
						let $input = element.find('input');
						$input.focus();
						$input.on('click', function () {
							$input.select();
						});
					}, 100);

					element.addClass('has-search');

					let searchTimer;
					scope.search = function () {
						clearTimeout(searchTimer);
						searchTimer = setTimeout(function () {
							if (scope.searchString.length) {
								showSearchResult();
							} else {
								depth = 0;
								parents = [];
								let menu = constructMenu(scope.root.children);
								element.css('height', getHeight(scope.root.children));

								isFirst ? firstMenu.replace(menu) : secondMenu.replace(menu);
							}
						}, 500);
					};
				}

				function showSearchResult() {
					let searchResults = [];
					let paths = {};

					function recursiveSearch (parent, path) {
						path = path === '' ? path + parent.name : path + ' > ' + parent.name;

						let copyOfSearch = angular.copy(scope.searchString);
						if(copyOfSearch == '&') copyOfSearch = "&amp";
						if(copyOfSearch == '"') copyOfSearch = "&quot";

						for (let i = 0, l = parent.children.length; i < l; i++) {
							let hasChildren = parent.children[i].children.length > 0;

							if (!(scope.restrictions.leafOnly && hasChildren) &&
								parent.children[i].name.toLowerCase().indexOf(copyOfSearch.toLowerCase()) !== -1
							) {
								searchResults.push(parent.children[i]);
								paths[parent.children[i].id] = path;
							}

							if (hasChildren) {
								recursiveSearch(parent.children[i], path);
							}
						}
					}

					recursiveSearch(scope.root, '');
					let menu = constructSearchMenu(searchResults, paths);
					element.css('height', getHeight(searchResults, true));

					isFirst ? firstMenu.replace(menu) : secondMenu.replace(menu);
				}

				function getHeight(options, search?) {
					let l = _.filter(options, function(o){
						return o.hide != true;
					}).length;

					let lH = search ? 56 : oH;

					let h = l * lH + 1;

					if (showSearch) {
						h += 36;
					}

					if (!search) {
						if (depth) {
							h += oH;
						}

						if (unselectable || allselectable) {
							h += oH;
						}
					}

					return h > 305 ? 305 : h;
				}

				function menu($handler, scope) {
					return {
						$menu: $handler,
						remove: function () {
							this.$menu.remove();
							this.scope.$destroy();
						},
						replace: function (newMenu) {
							this.$menu.replaceWith(newMenu.$menu);
							this.$menu = newMenu.$menu;
							this.scope.$destroy();
							this.scope = newMenu.scope;
						},
						scope: scope
					};
				}

				function constructSearchMenu(options, paths) {
					let scope_ = scope.$new(true);
					scope_.options = options;
					scope_.goIn = function (option, event) {
						scope.goIn(option, event);
					};

					scope_.optionClick = function (option) {
						option.onSelect();
					};

					let html = HTMLService.initialize();

					for (let i = 0, l = options.length; i < l; i++) {
						if(options[i].hide) continue;
						html
							.element('div').tabindex(0)
							.class('list-item no-select aria-detect menu-item-' + i)
							.ariaRole('option')
							.ngClass('{ selected: options[' + i + '].selected, disabled: options[' + i + '].disabled }')
							.click('optionClick(options[' + i + '])');

						let inner = HTMLService.initialize();
						if (l < 100 && isMultiselect) {
							inner
								.element('input-check').attr('model', 'options[' + i +'].selected').attr('modify', 'false');
						}

						if (options[i].icon) {
							inner
								.element('i')
								.class('material-icons')
								.attrConditional(options[i].color, 'style', 'color: ' + options[i].color)
								.content(options[i].icon)

						} else if (options[i].flag) {
							inner
								.element('i')
								.class('flag-icon flag-icon-' + options[i].flag);
						}

						inner
							.element('div').attr('tooltip', options[i].name).class('list-item-text')
							.inner(HTMLService.initialize()
								.element('span').class('primary').content(options[i].name)
								.element('span').class('secondary').content(paths[options[i].id])
							);

						html.inner(inner);
					}

					return menu($compile($('<div class="two-line dense list" tabIndex="0">').html(html.getHTML()))(scope_), scope_);
				}


				//obsolete?
				function loopChildrenLevels(options){
					if(options.length == 0){
						return
					}

					if(options.children && options.children.length > 0){
						_.each(options.children, function(ch){
							ch.onSelect();
							loopChildrenLevels(ch.children);
						})
					} else if (_.isArray(options)){
						_.each(options, function(o){
							o.onSelect();
						})
					}
				}

				function constructMenu(options, path?) {
					let pap = 0;
					if(options.length) pap = options[0].parentId;

					let scope_ = scope.$new(true);
					scope_.options = options;
					scope_.goIn = function (option) {
						scope.goIn(option);
					};

					scope_.selectChildren = function(options){
						if(options.children.length){
							_.each(options.children, function(child){
								if(child.hide != true){
									child.onSelect();
								}
								if(child.children.length){
									scope_.selectChildren(child)
								}
							})

							if(scope.restrictions.fromFilters == "true") options.onSelect();
						}
					}

					scope_.goOut = scope.goOut;
					scope_.optionClick = function (option) {
						if (scope.restrictions.leafOnly) {
							if (option.children.length) {
								scope.goIn(option);
								return;
								option.onSelect();
							} else {
								option.onSelect();
							}
						} else {
							option.onSelect();
						}

						initializeGlobalSelect();
					};

					let html = HTMLService.initialize();
					if (unselectable || allselectable) {
						initializeGlobalSelect();

						html
							.element('div').click('globalSelect()').class('list-item no-select')
							.inner(HTMLService.initialize('div')
								.class('primary global-select list-item-text')
								.attr('tooltip', '{{globalSelectText}}').content('{{globalSelectText}}')
							);
					}

					if (depth > 0) {
						html
							.element('div').click('goOut()').class('list-item previous no-select')
							.inner(HTMLService.initialize()
								.icon('chevron_left')
								.element('div').class('primary list-item-text').attr('tooltip', path).content(path)
							);
					}

					for (let i = 0, l = options.length; i < l; i++) {
						if(options[i].hide) continue;
						html
							.element('div').tabindex(0)
							.class((options[i].children.length ? 'has-children ' : '') + 'list-item aria-detect no-select menu-item-' + i)
							.ariaRole('option')
							.ngClass('{ selected: options[' + i + '].selected, disabled: options[' + i + '].disabled }')
							.click('optionClick(options[' + i + '])');

						let inner = HTMLService.initialize();
						if (l < 100 && isMultiselect &&  scope.restrictions.leafOnly) {
							inner
								.element('input-check')
								.attr('model', 'options[' + i + '].selected')
								.click('selectChildren(options[' + i + '])')
								.attr('modify', 'false');
						}

						if (options[i].icon) {
							inner
								.element('i').class('material-icons')
								.attrConditional(options[i].color, 'style', 'color:' + options[i].color)
								.content(options[i].icon);
						} else if (options[i].flag) {
							inner.element('i').class('flag-icon flag-icon-' + options[i].flag);
						}

						inner
							.element('div').attr('tooltip', options[i].name).class('list-item-text')
							.inner(HTMLService.initialize('span').class('primary').content(options[i].name));


						if (options[i].children.length) {
							inner
								.element('span').class('flex')
								.element('i')
								.class('material-icons')
								.click('$event.stopPropagation(); goIn(options[' + i + '])')
								.content('chevron_right')
						}

						html.inner(inner);
					}

					let m = menu($compile($('<div tabIndex="0" class="one-line dense list">').html(html.getHTML()))(scope_), scope_);

					if (depth > 0) {
						m.$menu.addClass('depth');
					}

					function initializeGlobalSelect() {
						$timeout(function () {
							showAllselectable = isAllselectableVisible();

							if (showAllselectable) {
								scope_.globalSelect = function () {
									scope.allselect(pap);
									initializeGlobalSelect();
								};

								scope_.globalSelectText = Translator.translate('SELECT_ALL');
							} else {
								scope_.globalSelect = function () {
									scope.unselect();
									initializeGlobalSelect();
								};

								scope_.globalSelectText = isMultiselect ?
									Translator.translate('UNSELECT_ALL') : Translator.translate('UNSELECT');
							}
						});
					}
					return m;
				}

				function isAllselectableVisible() {
					function hasSelection(option) {
						let i = option.children.length;
						if (i) {
							if (!scope.restrictions.leafOnly && option.selected) {
								return true;
							}

							while (i--) {
								if (hasSelection(option.children[i])) {
									return true;
								}
							}
						} else if (option.selected) {
							return true;
						}
					}

					return isMultiselect && allselectable ? !hasSelection(scope.root) : false;
				}

				function getParentPath() {
					let path = '';

					for (let i = 0, l = parents.length; i < l; i++) {
						let p = parents[i];

						if (i === l - 1) {
							path += p.name;
						} else {
							path = path + p.name + ' > ';
						}
					}

					return path;
				}

				scope.goIn = function (option) {
					parents.push(option);
					depth++;

					let p = getParentPath();

					if (isFirst) {
						secondMenu = constructMenu(option.children, p);
						secondMenu.$menu.addClass('shift-right');
						firstMenu.$menu.after(secondMenu.$menu);
					} else {
						firstMenu = constructMenu(option.children, p);
						firstMenu.$menu.addClass('shift-right');
						secondMenu.$menu.after(firstMenu.$menu);
					}

					setTimeout(function () {
						if (isFirst) {
							secondMenu.$menu.removeClass('shift-right');
							firstMenu.$menu.addClass('shift-left');
						} else {
							firstMenu.$menu.removeClass('shift-right');
							secondMenu.$menu.addClass('shift-left');
						}

						element.css('height', getHeight(option.children));

						setTimeout(function () {
							isFirst ? firstMenu.remove() : secondMenu.remove();
							isFirst = !isFirst;
						}, 225);
					}, 75);
				};

				scope.goOut = function () {
					let parent = parents.pop().parent;
					let children = parent.children;
					let path = getParentPath();
					depth--;

					if (isFirst) {
						secondMenu = constructMenu(children, path);
						secondMenu.$menu.addClass('shift-left');
						firstMenu.$menu.before(secondMenu.$menu);
					} else {
						firstMenu = constructMenu(children, path);
						firstMenu.$menu.addClass('shift-left');
						secondMenu.$menu.before(firstMenu.$menu);
					}

					setTimeout(function () {
						if (isFirst) {
							secondMenu.$menu.removeClass('shift-left');
							firstMenu.$menu.addClass('shift-right');
						} else {
							firstMenu.$menu.removeClass('shift-left');
							secondMenu.$menu.addClass('shift-right');
						}

						element.css('height', getHeight(parent.children));

						setTimeout(function () {
							isFirst ? firstMenu.remove() : secondMenu.remove();
							isFirst = !isFirst;
						}, 225);
					}, 75);
				};

				let shift = 0;
				let scroll = 0;

				if (!isMultiselect) {
					let i = scope.root.children.length;
					while (i--) {
						if (scope.root.children[i].selected) {
							break;
						}
					}

					if ((unselectable || allselectable) && i >= 0) {
						i++;
					}

					if (i !== -1) {
						if (i < 4) {
							shift = -i * oH;
						} else {
							scroll = (i - 4) * oH;
							shift = -4 * oH;
						}

						if (showSearch) {
							shift -= 36;
						}
					}
				}

				if (inTable) {
					shift += 2;
				} else if (!minimal) {
					shift += 28;
				}

				let offset = scope.targetElement.offset();
				let menuHeight = getHeight(scope.root.children);
				let hCompensate = window.innerHeight - menuHeight - offset.top - 24 - shift;
				if (hCompensate < 0) {
					let micro = hCompensate % oH + oH;
					$inputSelectMenu.css({
						'top': offset.top + hCompensate + shift - micro,
						'max-height': menuHeight - shift + micro + 32
					});
				} else {
					let top = offset.top + shift;
					top = top > 0 ? top : 0;
					$inputSelectMenu.css({
						'top': top,
						'max-height': menuHeight + hCompensate - shift + 32
					});
				}

				firstMenu.$menu.appendTo(element);

				setTimeout(function () {
					firstMenu.$menu.scrollTop(scroll);
				});

				element.css('height', menuHeight);
			}
		}
	}
})();