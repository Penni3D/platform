﻿﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputSelect', InputSelect);

	InputSelect.$inject = ['InputService', 'Translator', 'Colors', 'UniqueService', 'Sanitizer', 'Common', '$rootScope', 'ProcessService'];

	function InputSelect(InputService, Translator, Colors, UniqueService, Sanitizer, Common, $rootScope, ProcessService) {
		return {
			restrict: 'E',
			scope: {
				oModify: '<modify',
				model: '=',
				label: '@?',
				placeholder: '@?',
				options: '<',
				restrictions: '<?',
				onChange: '&?',
				onChangeParameters: '<?',
				ignoreUserGroups: '<?'
			},
			templateUrl: 'core/ui/input/bare/select/select.html',
			link: postLink,
			controller: function ($scope) {
				$scope.id = UniqueService.get('input-select', true);
			}
		};

		function postLink(scope, element, attrs) {
			let $input = element.find('input');
			let $valueElement = element.find('.input-value');
			let linkedOptions = [];
			let association;
			let hasChanges = false;

			scope.options = InputService.ensureArray(scope.options);

			//Client filtering properties
			let fromProcessColumnName: string = attrs.fromprocesscolumnname;
			let fromProcessColumnId: number = attrs.fromprocesscolumnid;
			let targetProcessColumnName: string = attrs.targetprocesscolumnname;
			let targetProcessColumnId: number = attrs.targetprocesscolumnid;
			let targetProcessName: string = attrs.targetprocessname;
			let targetListColumn: string = attrs.targetlistcolumn;

			let filterProcesses: boolean = (
				fromProcessColumnName && targetProcessColumnName && targetProcessName && targetListColumn
				&& fromProcessColumnName != "" && targetProcessColumnName != ""
				&& targetProcessName != "" && targetListColumn != "");


			let conf = {
				name: typeof attrs.optionName === 'undefined' ? 'name' : attrs.optionName,
				value: typeof attrs.optionValue === 'undefined' ? 'value' : attrs.optionValue,
				active: typeof attrs.optionActive === 'undefined' ? '' : attrs.optionActive,
				icon: typeof attrs.optionIcon === 'undefined' ? 'icon' : attrs.optionIcon,
				iconColor: typeof attrs.optionColor === 'undefined' ? 'color' : attrs.optionColor,
				flag: typeof attrs.optionFlag === 'undefined' ? 'flag' : attrs.optionFlag,
				unselect: attrs.unselectable,
				placeholder: typeof attrs.placeholder === 'undefined' ? '' : Sanitizer(attrs.placeholder),
				parent: typeof attrs.parent === 'undefined' ? 'parent_item_id' : attrs.parent,
				self: typeof attrs.self === 'undefined' ? 'item_id' : attrs.self,
				watch: typeof attrs.watchOptions !== 'undefined',
				disabled: typeof attrs.disabled === 'undefined' ? 'in_use' : attrs.disabled
			};

			scope.filterValue = undefined;

			let filterOptions = function () {
				if (scope.ignoreUserGroups && filterProcesses == false) return;
				if (!attrs.optionInuseAll || attrs.optionInuseAll == 'false') {
					_.each(scope.options, function (listItem) {
						if (!listItem.process) return;
						if (listItem.user_group != null && listItem.user_group.length && !_.isArray(listItem.user_group) && listItem.user_group.indexOf("[") == -1) {
							listItem.user_group = "[" + listItem.user_group + "]";
							listItem.user_group = JSON.parse(listItem.user_group);
						}
					});
				}
				if (filterProcesses && scope.filterValue) {
					_.each(scope.options, function (listItem) {
						let inuse = listItem.in_use == "1" || _.isNull(listItem.in_use);
						if (!inuse) return;
						if (typeof scope.filterValue == 'object' || typeof listItem[targetListColumn] == 'object') {
							let a = [];
							let b = [];
							if (typeof scope.filterValue != 'object') a.push(scope.FilterValue); else a = scope.filterValue;
							if (typeof listItem[targetListColumn] != 'object') b.push(listItem[targetListColumn]); else b = listItem[targetListColumn];

							let result = _.intersection(a, b);
							if (!result.length) {
								listItem.in_use = 0;
							}
						} else {
							if (listItem[targetListColumn].toString().toLowerCase() != scope.filterValue.toString().toLowerCase()) {
								listItem.in_use = 0;
							}
						}
					});
				}
			}

			if (filterProcesses) {
				ProcessService.getItem(attrs.parentProcess, attrs.owner).then(function (result) {
					let i = result[fromProcessColumnName];

					ProcessService.getItem(targetProcessName, i).then(function (f) {
						scope.filterValue = f[targetProcessColumnName];

						filterOptions();
					})
				});
			} else {
				filterOptions();
			}
			scope.requiredField = typeof attrs.requiredField !== 'undefined';

			element.on('click', 'input, label, .input-value, i', function (event) {
				event.stopPropagation();
				event.preventDefault();
				openMenu();
			});

			let rootLevel = {
				isRoot: true,
				children: [],
				name: '',
				searchLabel: attrs.label ? attrs.label : ""
			};

			let restrictions = angular.extend({
				leafOnly: true,
				flatten: false,
				fromFilters: attrs.fromFilters
			}, scope.restrictions);

			//Language Support

			let setLanguage = function () {
				if (scope.options.length > 0) {
					let locale_id = $rootScope.clientInformation.locale_id.toLowerCase().replace('-', '_');

					//Checking from lists first item is sometimes unreliable. For now, checking all items --BSa 5.11.2019
					let hasLang = true;

					if (attrs.optionName === "list_item") {
						_.each(scope.options, function (listItem) {
							if (!listItem.hasOwnProperty("list_item_" + locale_id)) {
								hasLang = false;
								return;
							}
						})
					}

					if (attrs.optionName === "list_item" && hasLang) {
						attrs.optionName = "list_item_" + locale_id;
						conf.name = attrs.optionName
					}
				}
			}


			setLanguage();
			let settings = {
				singleValue: typeof attrs.singleValue !== 'undefined',
				multiselect: !(typeof attrs.max === 'undefined' || attrs.max == 1),
				unselectable: typeof attrs.unselectable !== 'undefined',
				allselectable: false,
				inTable: !!element.closest('table').length,
				minimal: typeof attrs.minimal !== 'undefined',
				restrictions: restrictions,
				onlyIcon: typeof attrs.iconOnly !== 'undefined',
				max: Number(attrs.max)
			};

			if (settings.multiselect) {
				settings.singleValue = false;
			}

			if (!settings.singleValue && !Array.isArray(scope.model)) {
				scope.model = [];
			}

			function findOptionByValue(value) {
				let i = linkedOptions.length;
				while (i--) {
					let o = linkedOptions[i];
					if (o.value == value) {
						return o;
					}
				}

				return false;
			}

			function updateDisplayValue() {
				$valueElement.empty();
				let showPlaceholder = true;
				let tooltip = '';

				if (!settings.singleValue) {
					let m = scope.model.length;

					if (m > 0) {
						for (let i = 0; i < m; i++) {
							let option = findOptionByValue(scope.model[i]);

							if (!option) {
								continue;
							}

							let iconShown = false;

							if (option.icon) {
								let $iconElement = $('<i class="material-icons" aria-hidden="true">' + option.icon + '</i>')
									.appendTo($valueElement);

								if (option.color) {
									$iconElement.css('color', option.color);
								}
								iconShown = true;
							} else if (option.flag) {
								$valueElement.append('<i class="flag-icon flag-icon-' + option.flag + '"></i>');
								iconShown = true;
							}

							if (i === m - 1) {
								tooltip += option.name;

								if (!(iconShown && settings.onlyIcon)) {
									$valueElement.append(option.name);
								}
							} else {
								tooltip = tooltip + option.name + ', ';

								if (iconShown && settings.onlyIcon) {
									$valueElement.append(',&nbsp;');
								} else {
									$valueElement.append(option.name + ',&nbsp;');
								}
							}
						}

						showPlaceholder = false;
					}
				} else if (typeof scope.model !== 'undefined' && scope.model !== null && scope.model !== '') {
					let option = findOptionByValue(scope.model);

					if (option) {
						let iconShown = false;

						if (option.icon) {
							let $iconElement = $('<i class="material-icons" aria-hidden="true">' + option.icon + '</i>')
								.appendTo($valueElement);

							if (option.color) {
								$iconElement.css('color', option.color);
							}

							iconShown = true;
						} else if (option.flag) {
							$valueElement.append('<i class="flag-icon flag-icon-' + option.flag + '"></i>');
							iconShown = true;
						}

						tooltip += option.name;

						if (!(iconShown && settings.onlyIcon)) {
							$valueElement.append(option.name);
						}

						showPlaceholder = false;
					}
				}

				if (showPlaceholder) {
					$valueElement.removeAttr('tooltip');
					$valueElement.append(conf.placeholder);
					$valueElement.addClass('placeholder');
				} else {
					$valueElement.attr('tooltip', tooltip);
					$valueElement.removeClass('placeholder');
				}
			}

			setTimeout(function () {

				let selfTriggeredModel = false;
				let selfTriggeredOptions = false;
				if (conf.watch) {
					scope.$watch('options', function (newOptions) {
						if (selfTriggeredOptions) {
							selfTriggeredOptions = false;
							return;
						}
						scope.options = InputService.ensureArray(newOptions);
						setLanguage();
						filterOptions();
						selfTriggeredOptions = newOptions.length > 0 && newOptions !== scope.options;

						initializeInput();
						canModify();

					}, true);
				}

				scope.$watch('oModify', canModify);

				scope.$watchCollection('model', function () {
					if (selfTriggeredModel) {
						selfTriggeredModel = false;
						return;
					}

					if (!settings.singleValue && !Array.isArray(scope.model)) {
						scope.model = [];
						selfTriggeredModel = true;
					} else {
						selfTriggeredModel = false;
					}

					let i = linkedOptions.length;
					while (i--) {
						let option = linkedOptions[i];
						let f = false;

						if (!settings.singleValue) {
							let notFound = scope.model.indexOf(option.value) === -1;

							if (notFound) {
								let overlimit =
									settings.multiselect &&
									settings.max !== 0 &&
									scope.model.length >= settings.max;

								option.disabled = !!(restrictions.leafOnly ? !option.children.length && overlimit : overlimit);
								//Note to everyone: this might sometimes disable items unwanted. Might have something to do with the overlimit
							} else {
								f = true;
								option.disabled = false;
							}

						} else if ((scope.model || scope.model === 0) &&
							typeof option.value !== 'undefined' &&
							scope.model == option.value
						) {
							f = true;
						}

						option.selected = f;
					}

					updateSelectValue();
				});

				initializeInput();
			});

			let allHidden = false;

			function initializeInput() {
				linkedOptions = [];
				association = {};
				rootLevel.children = [];

				let parentize = [];
				for (let i = 0, l = scope.options.length; i < l; i++) {
					linkedOption(scope.options[i]);
				}

				for (let i = 0, l = linkedOptions.length; i < l; i++) {
					let option = linkedOptions[i];
					if (!restrictions.flatten && option.parentId) {
						parentize.push(option);
					} else {
						option.parent = rootLevel;
						rootLevel.children.push(option)
					}

					if (!settings.singleValue) {
						if (scope.model.indexOf(option.value) !== -1) {
							option.selected = true;
						}

						if (settings.multiselect &&
							!option.selected &&
							settings.max !== 0 &&
							scope.model.length >= settings.max
						) {
							option.disabled = true;
						}
					} else if ((scope.model || scope.model === 0) &&
						typeof option.value !== 'undefined' &&
						scope.model == option.value
					) {
						option.selected = true;
					}
				}

				for (let i = 0, l = parentize.length; i < l; i++) {
					let o = parentize[i];
					o.inParent(o.parentId);
				}

				let i = 0;
				getAssociation(rootLevel.children);

				function getAssociation(options) {
					for (let j = 0, l = options.length; j < l; j++) {
						let o = options[j];
						association[o.value] = i++;

						if (o.children.length) {
							getAssociation(o.children);
						}
					}
				}

				if (settings.multiselect) {
					let s = false;
					let count = 0;

					if (settings.max === 0) {
						s = true;
					} else {
						selectableCount(rootLevel.children, count);
						if (count <= settings.max) s = true;
					}

					settings.allselectable = s;
				}

				function selectableCount(option, count) {
					let i = option.length;
					while (i--) {
						if (option[i].children.length) {
							if (!restrictions.leafOnly) {
								count++;
							}

							selectableCount(option[i].children, count);
						} else {
							count++;
						}
					}
				}
				allHidden = _.filter(linkedOptions, function(o) { return o.hide == false; }).length == 0;
				updateSelectValue();
			}

			function openMenu() {
				if (scope.modify) {
					$input.removeClass('ng-untouched').addClass('ng-touched');
					InputService.selectMenu(rootLevel, settings, element, scope, {
						unselect: unselect,
						allselect: allselect,
						onClose: inputChanged
					});
				}
			}

			function onSelect(option) {
				if (option.disabled) return;

				if (settings.multiselect) {
					if (option.selected) {
						scope.model.splice(scope.model.indexOf(option.value), 1);
					} else {
						if (settings.singleValue) {
							scope.model = option.value;
						} else {
							let l = scope.model.length;
							let wantedPosition = association[option.value];

							if (l === 0 || association[scope.model[l - 1]] < wantedPosition) {
								scope.model.push(option.value);
							} else if (association[scope.model[0]] > wantedPosition) {
								scope.model.unshift(option.value);
							} else {
								while (l--) {
									if (association[scope.model[l - 1]] < wantedPosition &&
										association[scope.model[l]] > wantedPosition) {
										break;
									}
								}
								scope.model.splice(l, 0, option.value);
							}
						}
					}
				} else {
					if (settings.singleValue) {
						scope.model = option.value;
					} else {
						scope.model = [option.value];
					}

					InputService.removeSelectMenu();
				}

				hasChanges = true;
			}

			function canModify() {
				scope.modify = (Object.keys(scope.options).length && allHidden != true) ? scope.oModify : false;
			}

			function linkedOption(option) {
				let l = {
					id: undefined,
					name: undefined,
					value: undefined,
					children: [],
					parentId: InputService.findProperty(option, conf.parent),
					parent: null,
					inParent: function (parentId) {
						let i = linkedOptions.length;
						while (i--) {
							let o = linkedOptions[i];
							if (o.id == parentId) {
								o.children.push(l);
								l.parent = o;

								break;
							}
						}

						if (i === -1) {
							let j = linkedOptions.length;
							while (j--) {
								if (linkedOptions[j].id == l.id) {
									linkedOptions.splice(j, 1);
									break;
								}
							}
						}
					},
					onSelect: function () {
						onSelect(l);
					},
					disabled: false,
					selected: false,
					icon: undefined,
					flag: undefined,
					color: undefined,
					hide: false
				};

				if (typeof option === 'object') {
					let i = InputService.findProperty(option, conf.self);

					l.id = typeof i === 'undefined' ? UniqueService.get('select-option') : Sanitizer(i);
					l.name = Sanitizer(Translator.translation(InputService.findProperty(option, conf.name)));
					l.value = InputService.findProperty(option, conf.value);
					l.icon = Sanitizer(InputService.findProperty(option, conf.icon));
					l.flag = Sanitizer(InputService.findProperty(option, conf.flag));
					l.color = Colors.getColor(InputService.findProperty(option, conf.iconColor)).rgb;
					l.disabled = false;

					if (option.in_use == "0" || (option.in_use == "2" && attrs.fromFilters != "true")) l.hide = true;

					if (option.user_group && option.user_group.length > 0) {
						//In some cases option.user_group might be a string --BSa
						if (typeof option.user_group == undefined) option.user_group = [];
						if (typeof option.user_group == 'string') option.user_group = option.user_group.split(",").map(Number);

						let o = _.intersection(option.user_group, $rootScope.clientInformation.user_groups);
						if (!o.length) l.hide = true
					}

					if (conf.hasOwnProperty('disabled') && !attrs.optionInuseAll)
						l.disabled = option[conf.disabled] == 0;
				} else {
					l.id = UniqueService.get('select-option');
					l.name = Sanitizer(option);
					l.value = Sanitizer(option);
				}

				if (l.name == null) l.name = '';
				linkedOptions.push(l);
				return l;
			}

			function updateSelectValue() {
				if (!settings.singleValue) {
					scope.contains = scope.model.length ? 'set' : '';
				} else if (scope.model || scope.model === 0 || scope.model === false) {
					scope.contains = 'set';
				} else {
					scope.contains = '';
				}

				updateDisplayValue();
			}

			function allselect(parent_item_id?) {
				let i = linkedOptions.length;
				while (i--) {
					let option = linkedOptions[i];

					/*if (restrictions.leafOnly && option.children.length) {
						continue;
					}*/

					if (restrictions.leafOnly && option.children.length > 0 && attrs.fromFilters != "true") {
						continue;
					}
					if (parent_item_id == 0 || (typeof parent_item_id == 'undefined') || option.parentId == parent_item_id) {
						if (option.hide != true) option.onSelect();
					}


					/*//Im not 100% sure if only recursive sub items have parentId so might cause some problems...
					let isRecursiveChild = typeof option.parentId != 'undefined' && option.parentId != 0 && option.parentId != null

					if (!isRecursiveChild || (isRecursiveChild && parent_item_id == option.parentId)) {
						option.onSelect();
					}*/
				}
			}

			function unselect() {
				if (conf.unselect) {
					if (conf.unselect === 'space') {
						scope.model = !settings.singleValue ? [] : '';
					} else if (conf.unselect === 'null') {
						scope.model = null;
					} else {
						scope.model = conf.unselect;
					}
				} else {
					scope.model = !settings.singleValue ? [] : undefined;
				}

				hasChanges = true;

				if (!settings.multiselect) {
					InputService.removeSelectMenu();
				}
			}

			function inputChanged() {
				if (hasChanges) {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
					hasChanges = false;
				}
			}
		}
	}
})();
