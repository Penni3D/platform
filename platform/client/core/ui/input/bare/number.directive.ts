﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputNumber', InputNumber);

	InputNumber.$inject = ['Common', 'InputService'];

	function InputNumber(Common, InputService) {
		return {
			restrict: 'E',
			scope: {
				modify: '<',
				model: '=',
				label: '@?',
				max: '@?',
				suffix: '@?',
				min: '@?',
				placeholder: '@?',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/bare/number.html',
			link: function (scope, element, attrs) {
				scope.requiredField = typeof attrs.requiredField !== 'undefined';
				let $input = element.find('input');
				let $inputValue = element.find('.input-value');
				let precision = 0;
				let ngModel = $input.controller('ngModel');
				let skipRounding = false;
				let hasFocus = false;
				if (typeof (attrs.max) == 'undefined' || attrs.max == 0 || attrs.max > 2147483647) attrs.max = 2147483647;

				scope.initialModel = angular.copy(ensureDelimiters(scope.model));

				if (typeof attrs.step === 'undefined') {
					if (attrs.precision) precision = Number(attrs.precision);
					scope.step = precision > 0 ? Number('0.' + Array(precision).join('0') + '1') : Math.pow(10, -precision);
				} else {
					scope.step = _.startsWith(attrs.step, '.') ? '0' + attrs.step : attrs.step;
					if (scope.step.indexOf('.') !== -1) precision = scope.step.split('.')[1].length;
				}

				element.on('mouseenter', 'label, input, .input-value', function () {
					if (Common.isTrue(scope.modify) && !hasFocus) hideFormat();
				}).on('mouseleave', function () {
					if (Common.isTrue(scope.modify) && hasFocus == false) leave();
				});

				if (!scope.label) scope.placeholderValue = scope.placeholder;
				scope.onFocus = function () {
					if (scope.label) scope.placeholderValue = scope.placeholder;
					hasFocus = true;
					if (Common.isTrue(scope.modify)) {
						hideFormat();
						$input.select();
					}
				};

				//Prevent non-numeric input
				scope.initialModelChanged = function () {
					if (!scope.initialModel) scope.initialModel = '';
					let r = /[^\d.-]/g;
					if (dl() == ",") r = /[^\d,-]/g;
					if (precision == 0) r = /[^\d-]/g;
					scope.initialModel = scope.initialModel.replace(r, '');

					let ind = scope.initialModel.indexOf(dl());
					if (precision > 0 && scope.initialModel.length > 0 && ind > 0) {
						scope.initialModel = scope.initialModel.substring(0, ind + 1 + precision);
					}
				};

				//Test that given variable is a JS number
				function checkNumber(t) {
					if (typeof t === "undefined") return false;
					if (typeof t === "string") t = t.replace(",", ".");
					return (_.isNumber(Number(t)));
				}

				function ensureDelimiters(t) {
					if (t) t = t.toString().replace(ol(), dl());
					return t;
				}

				function patternMatching(v, full: boolean = true) {
					//Check that a valid number and suitable precision
					if (typeof v === "undefined") return null;

					if (typeof v === "string") {
						if (v == "") return null;
						v = v.replace(",", ".");
						if (v.endsWith(".")) {
							v = v.substring(0, v.length - 1);
						}
						v = Number(v);
					}

					//Check decimals not over step bounds
					let s = Math.pow(10, precision);
					let reminder = Math.round(v * s) % Number(scope.step * s);
					if (reminder) {
						skipRounding = true;
						v -= Common.rounder(reminder / s, precision);
					}

					if (checkNumber(attrs.max) && v > attrs.max) v = Number(attrs.max);
					if (checkNumber(attrs.min) && v < attrs.min) v = Number(attrs.min);

					return v;
				}

				scope.$watch('model', function () {
					if (!skipRounding) preciseRounding();

					if (checkNumber(scope.model)) {
						$inputValue.removeClass('placeholder');
						scope.formatModel = Common.formatNumber(scope.model, precision);
						//problems with step property -- so do this only if no stepping
						if ((scope.step == 0.01 || scope.step == 1) && scope.initialModel != '-') scope.initialModel = angular.copy(ensureDelimiters(scope.model));
					} else {
						$inputValue.addClass('placeholder');
						scope.formatModel = scope.placeholder;
						//problems with step property -- so do this only if no stepping
						if (scope.step == 0.01 || scope.step == 1) scope.initialModel = angular.copy(ensureDelimiters(scope.model));
					}
					skipRounding = false;
				});

				function updateModel() {
					scope.model = patternMatching(scope.initialModel);
					scope.inputChanged();
					dirty = false;
				}

				let updateModelTimer;
				let dirty = false;
				$input.on("keyup", function () {
					scope.formatModel = Common.formatNumber(patternMatching(scope.initialModel), precision);
					dirty = true;
					if (updateModelTimer) clearTimeout(updateModelTimer);
					updateModelTimer = setTimeout(
						function () {
							updateModel();
						}, 10
					)
				});

				$input.on('blur', function () {
					if (scope.initialModel == '-') scope.initialModel = ''
					if (scope.label) scope.placeholderValue = '';
					hasFocus = false;
					leave();
				});

				function leave() {
					if (updateModelTimer) clearTimeout(updateModelTimer);
					if (Common.isTrue(scope.modify) && dirty) updateModel();
					showFormat();

					if (ngModel.$invalid) {
						ngModel.$viewValue = '';
						$input.val('');
						ngModel.$commitViewValue();
					}
				}

				preciseRounding();
				showFormat();

				function dl() {
					return Common.getDelimiter();
				}

				function ol() {
					if (dl() == ".") return ",";
					return ".";
				}

				function hideFormat() {
					$inputValue.css('display', 'none');
					$input.css('font-size', '');
					scope.initialModel = angular.copy(ensureDelimiters(scope.model));
				}

				function showFormat() {
					if (!hasFocus) {
						$inputValue.css('display', 'block');
						$input.css('font-size', 0);
					}
				}

				function preciseRounding() {
					let reminder = scope.model - Common.rounder(scope.model, precision);
					if (reminder) {
						scope.model -= reminder;
						skipRounding = true;
					}
				}
			},
			controller: function ($scope, InputService, UniqueService) {
				$scope.id = UniqueService.get('input-number', true);

				if (typeof $scope.model !== 'undefined' && $scope.model !== null)
					$scope.model = Number($scope.model);

				$scope.inputChanged = function () {
					$scope.$apply();
					InputService.inputChanged($scope.onChange, $scope.onChangeParameters);
				};
			}
		};
	}
})();