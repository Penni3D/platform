﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputFormula', inputFormula);

	inputFormula.$inject = ['Common', 'InputService', 'ProcessService'];
	function inputFormula(Common, InputService, ProcessService) {
		return {
			scope: {
				modify: '<?',
				model: '=',
				label: '@?',
				placeholder: '@?',
				onChange: '&?',
				onChangeParameters: '<?',
				data: '<',
				lists: '<',
				grids: '<'
			},
			restrict: 'E',
			template: '<input-smart type="decimal" model="model" label="{{::label}}" placeholder="{{::placeholder}}" precision="1" modify="false"></input-smart>',
			link: function (scope, elem, attrs) {
				if (typeof attrs.minimal !== 'undefined') {
					$(elem).find('input-container').addClass('minimal');
				}

				function onSave() {
					var newModel = Common.rounder(scope.$eval(attrs.formula), 1);
					if (!isNaN(newModel) && scope.model != newModel) {
						scope.model = newModel;
						InputService.inputChanged(scope.onChange, scope.onChangeParameters);
					}
				}

				scope.getvalue = function (v, lv) {
					if (Array.isArray(scope.data[v])) {
						if (scope.data[v].length) {
							var val = scope.data[v][0];
							var retval = 0;

							angular.forEach(scope.grids, function (g) {
								for (var i = 0, l = g.length; i < l; i++) {
									var grid = g[i];
									for (var j = 0, l_ = grid.fields.length; j < l_; j++) {
										var field = grid.fields[j];
										if (field.ItemColumn.Name == v) {
											angular.forEach(scope.lists[field.ItemColumn.DataAdditional], function (listVal) {
												if (listVal.item_id == val && !isNaN(Number(listVal[lv]))) {
													retval = Number(listVal[lv]);
												}
											});
										}
									}
								}
							});

							return retval;
						} else {
							return 0;
						}
					} else {
						return scope.data[v];
					}
				};

				onSave();
				ProcessService.subscribeChanged(onSave, attrs.process);
			}
		};
	}
})();