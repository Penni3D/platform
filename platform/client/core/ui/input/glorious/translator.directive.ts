(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputTranslation', InputTranslation);

	InputTranslation.$inject = [];

	function InputTranslation() {
		return {
			scope: {
				model: '=',
				label: '@?',
				onChange: '&?',
				onChangeParameters: '<?',
				modify: '<?'
			},
			restrict: 'E',
			templateUrl: function () {
				return 'core/ui/input/glorious/translator.html';
			},
			link: {
				pre: function (scope, element, attrs) {
					let $inputString = element.children('input-string');

					if (typeof attrs.minimal !== 'undefined')
						$inputString.attr('minimal', '');

					element.on('mouseenter', 'input-select, input-string', function () {
						scope.showFlag = true;
					}).on('mouseleave', function () {
						scope.showFlag = false;
					});

					if (typeof attrs.spellcheck !== 'undefined')
						$inputString.attr('spellcheck', '');
				}
			},
			controller: function ($scope, $rootScope, Translator) {
				let localId = $rootScope.clientInformation.locale_id;

				if (typeof $scope.model === 'undefined' || $scope.model === null) {
					$scope.model = {};
				} else if (typeof $scope.model === 'string' || typeof $scope.model === 'number') {
					let variable = $scope.model;
					$scope.model = {};
					$scope.model[localId] = variable;
				}

				$scope.showFlag = false;
				$scope.languages = {};
				angular.forEach($rootScope.languages, function (language) {
					language.flag = language.value.split('-')[1].toLowerCase();
					$scope.languages[language.value] = {
						Translation: language.name,
						Name: language.value,
						Flag: language.flag
					};
				});

				$scope.placeholder = Translator.translation($scope.model);
				$scope.activeLanguage = localId;
				
				
				$scope.onBlurText = function () {
					//$scope.showFlag = false;
				};
				$scope.onFocusText = function () {
					$scope.showFlag = true;
				};


				$scope.inputChanged = function () {
					if (typeof $scope.onChange === 'function' && typeof $scope.onChange() === 'function') {
						$scope.onChange()($scope.onChangeParameters);
					}
				};
			}
		}
	}
})();