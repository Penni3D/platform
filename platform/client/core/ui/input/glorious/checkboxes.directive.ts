(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputChecks', InputChecks);

	InputChecks.$inject = ['InputService', 'Translator'];
	function InputChecks(InputService, Translator) {
		return {
			scope: {
				modify: '<',
				label: '@?',
				model: '=',
				options: '<',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: 'core/ui/input/glorious/checkboxes.html',
			link: function (scope, element, attrs) {
				var optionName = typeof attrs.optionName === 'undefined' ? 'name' : attrs.optionName;
				var optionValue = typeof attrs.optionValue === 'undefined' ? 'value' : attrs.optionValue;
				var selfTriggered = false;
				var association = {};

				scope.checkboxes = [];
				scope.toggleChecks = function () {
					scope.showChecks = !scope.showChecks;
				};

				scope.toggleAll = function () {
					scope.model = [];
					for (var i = 0, l = scope.checkboxes.length; i < l; i++ ) {
						var c = scope.checkboxes[i];
						if (scope.allSelected) {
							c.selected = true;
							scope.model.push(c.value);
						} else {
							c.selected = false;
						}
					}

					selfTriggered = true;
					inputChanged();
				};

				scope.$watchCollection('model', function () {
					if (selfTriggered) {
						selfTriggered = false;
						return;
					}

					if (Array.isArray(scope.model)) {
						selfTriggered = false;
					} else {
						scope.model = [];
						selfTriggered = true;
					}

					var i = scope.checkboxes.length;
					while (i--) {
						var c = scope.checkboxes[i];
						c.selected = scope.model.indexOf(c.value) !== -1;
					}

					updateMaincheck();
				});

				initializeInput();

				function checkChanged(option) {
					if (option.selected) {
						var l = scope.model.length;
						var wantedPosition = association[option.value];

						if (l === 0 || association[scope.model[l - 1]] < wantedPosition) {
							scope.model.push(option.value);
						} else if (association[scope.model[0]] > wantedPosition) {
							scope.model.unshift(option.value);
						} else {
							while (l--) {
								if (association[scope.model[l - 1]] < wantedPosition &&
									association[scope.model[l]] > wantedPosition) {
									break;
								}
							}

							scope.model.splice(l, 0, option.value);
						}
					} else {
						scope.model.splice(scope.model.indexOf(option.value), 1);
					}

					selfTriggered = true;
					updateMaincheck();
					inputChanged();
				}

				function initializeInput() {
					for (var i = 0, l = scope.options.length; i < l; i++) {
						var s = checkbox(scope.options[i]);
						association[s.value] = i;
					}
				}

				function checkbox(option) {
					var s = {
						name: Translator.translation(InputService.findProperty(option, optionName)),
						value: InputService.findProperty(option, optionValue),
						selected: false,
						onSelect: function () {
							checkChanged(s);
						}
					};

					scope.checkboxes.push(s);

					return s;
				}

				function updateMaincheck() {
					var i = scope.checkboxes.length;
					var selected = i;

					while (i--) {
						if (scope.checkboxes[i].selected) {
							selected--;
						}
					}

					if (selected === scope.checkboxes.length) {
						scope.allSelected = false;
					} else if (selected === 0) {
						scope.allSelected = true;
					} else {
						scope.allSelected = null;
					}
				}

				function inputChanged() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}
			},
			controller: function ($scope) {
				$scope.options = InputService.ensureArray($scope.options);
			}
		}
	}
})();