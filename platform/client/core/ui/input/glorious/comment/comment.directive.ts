﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputComment', InputComment);

	InputComment.$inject = [
		'UserService',
		'CommentService',
		'CalendarService',
		'HTMLService',
		'Translator',
		'InputService',
		'Sanitizer',
		'MessageService',
		'$compile',
		'$window',
		'AttachmentService',
		'Common'
	];
	function InputComment(
		UserService,
		CommentService,
		CalendarService,
		HTMLService,
		Translator,
		InputService,
		Sanitizer,
		MessageService,
		$compile,
		$window,
		AttachmentService,
		Common
	) {
		function getChip(comment) {
			let content = '<span class="comment-sender subtitle-2 ellipsis">' + Sanitizer(comment.senderName) + '</span>';
			content += parseContent(comment) + getDateHtml(comment.Date);

			return HTMLService.initialize('chip')
				.attr('comment-id', Sanitizer(comment.CommentId))
				.attrConditional(comment.IsDeleted, 'class', 'is-deleted no-select')
				.content(content).getHTML();
		}

		function getNoteChip(comment) {
			let isSender = comment.Sender == UserService.getCurrentUserId();

			let content =
				'<span class="comment-sender subtitle-2 ellipsis">' +
				(isSender ? Sanitizer(Translator.translate('YOU')) : Sanitizer(comment.senderName)) +
				'</span>' +
				'<span class="comment-date caption">' + Sanitizer(CalendarService.formatDate(comment.Date)) + '</span>';

			content += parseContent(comment);

			let classes = '';

			if (isSender) {
				classes += 'is-sender ';
			}

			if (comment.IsDeleted) {
				classes += 'is-deleted no-select';
			}

			return HTMLService.initialize('chip')
				.attr('comment-id', Sanitizer(comment.CommentId))
				.attr('class', classes)
				.content(content).getHTML();
		}

		function getSenderChip(comment) {
			let content = parseContent(comment) + getDateHtml(comment.Date);
			return HTMLService.initialize('chip')
				.attr('comment-id', Sanitizer(comment.CommentId))
				.class('is-sender' + (comment.IsDeleted ? ' is-deleted' : ''))
				.content(content).getHTML();
		}

		function getCurrentFormattedDate(date) {
			return HTMLService.initialize('div').class('date align-item-center subtitle-2')
				.content(Sanitizer(CalendarService.formatDate(date))).getHTML();
		}

		function getDateHtml(date) {
			return '<span class="comment-date caption">' + Sanitizer(date.toTimeString().substr(0, 5)) + '</span>';
		}

		function getDeletedContent() {
			return '<chip-name class="body-1 italic no-select">' + Sanitizer(Translator.translate('COMMENT_IS_DELETED')) + '</chip-name>';
		}

		let parseRegex = /^\${[a-z]*\/[0-9]*}\$/;

		function parseContent(comment) {
			if (comment.IsDeleted) {
				return getDeletedContent();
			}

			let commentContent = comment.Data;

			if (typeof commentContent === 'string') {
				let file  = commentContent.match(parseRegex);
				if (file) {
					let f = file[0];
					let info = f.substring(2, f.length - 2).split('/');
					let name = Sanitizer(commentContent.substring(f.length));

					let attachmentId = Sanitizer(info[1]);
					let type = info[0];

					if (type === 'image') {
						return '<img class="pointer no-select" src="v1/meta/AttachmentsDownload/' + attachmentId + '/3" attachment="'+ attachmentId + '">' +
							'<chip-name class="subtitle-1 ellipsis">' + name + '</chip-name>';
					} else if (type === 'file') {
						return '<div class="comment-file align-column pointer no-select" attachment="' + attachmentId + '">' +
							'<i class="material-icons" aria-hidden="true">description</i>' +
							'</div>' +
							'<chip-name class="subtitle-1 ellipsis">' + name + '</chip-name>'
					}
				}
			}

			return '<chip-name class="body-1">' + anchorme(Sanitizer(commentContent), { truncate: 25 }) + '</chip-name>';
		}

		return {
			restrict: 'E',
			scope: {
				modify: '<',
				label: '@?',
				onChange: '&?',
				onChangeParameters: '<?'
			},
			templateUrl: function (element, attrs) {
				return typeof attrs.note === 'undefined' ?
					'core/ui/input/glorious/comment/comment.html' : 'core/ui/input/glorious/comment/note.html'
			},
			link: function (scope, element, attrs) {
				let activeCommentId;
				let process = attrs.process;
				let self = attrs.self;
				let group = attrs.group;
				let allComments = [];
				let $commentSection = element.find('.comment-section');
				let $inputChip = element.find('.chip-input');
				let $textarea = element.find('textarea');

				scope.spellcheck = typeof attrs.spellcheck !== 'undefined';

				let $empty = $('<div class="align-item-center italic caption empty no-select">' +
					Sanitizer(Translator.translate('COMMENTS_NO_COMMENTS')) + '</div>');

				let $delete = $compile(
					HTMLService.initialize('i')
						.class('remove-comment z-depth-1 material-icons pointer')
						.click('removeComment()')
						.content('clear')
						.getHTML()
				)(scope);

				element.on('click', 'img', function (event) {
					AttachmentService.showAttachment(event.currentTarget.getAttribute('attachment'), '');
				});

				element.on('click', '.comment-file', function (event) {
					$window.open('v1/meta/AttachmentsDownload/' + event.currentTarget.getAttribute('attachment') + '/0', '_blank');
				});

				element.on('click', 'chip-name > a', function (event) {
					event.preventDefault();
					AttachmentService.goToUrl(event.currentTarget.getAttribute('href'));
				});

				let removeTimer;
				element.on('mouseenter', '.is-sender:not(.is-deleted)', function (event) {
					if (Common.isTrue(scope.modify)) {
						activeCommentId = event.currentTarget.getAttribute('comment-id');
						$delete.appendTo(event.currentTarget);

						clearTimeout(removeTimer);
						removeTimer = setTimeout(function () {
							$delete.addClass('transition-in');
						}, 75);
					}
				});

				element.on('mouseleave', '.is-sender', function () {
					if (Common.isTrue(scope.modify)) {
						$delete.removeClass('transition-in');

						clearTimeout(removeTimer);
						removeTimer = setTimeout(function () {
							$delete.detach();
						}, 150);
					}
				});

				let currentDate;
				let isNote = typeof attrs.note !== 'undefined';
				scope.preventAttachments = typeof attrs.preventatt !== 'undefined';

				CommentService.get(process, group, self).then(function (comments) {
					let chipsHtml = '';
					let l = comments.length;
					allComments = comments;

					if (isNote) {
						if (l === 0 && Common.isFalse(scope.modify)) {
							$empty.appendTo($commentSection);
						}

						while (l--) {
							let comment = comments[l];
							chipsHtml += getNoteChip(comment);
						}

						$commentSection.find('loader').remove();
						$commentSection.append(chipsHtml);

					} else {
						for (let i = 0; i < l; i++) {
							let comment = comments[i];
							let y = comment.Date.getFullYear();
							let m = comment.Date.getMonth();
							let d = comment.Date.getDate();
							let date = new Date(y, m, d);

							if (typeof currentDate === 'undefined') {
								currentDate = date;
								chipsHtml += getCurrentFormattedDate(date);
							} else {
								if (date.getTime() > currentDate.getTime()) {
									currentDate = date;
									chipsHtml += getCurrentFormattedDate(date);
								}
							}

							if (comment.Sender == UserService.getCurrentUserId()) {
								chipsHtml += getSenderChip(comment);
							} else {
								chipsHtml += getChip(comment);
							}
						}

						if (l === 0) {
							currentDate = new Date();
							currentDate.setHours(0, 0, 0, 0);

							chipsHtml += getCurrentFormattedDate(new Date());
						}

						$commentSection.html(chipsHtml);

						if (l === 0 && Common.isFalse(scope.modify)) {
							$empty.appendTo($commentSection);
						}

						setTimeout(function () {
							$commentSection.scrollTop($commentSection.prop('scrollHeight'));
						}, 75)
					}
				});

				scope.removeComment = function () {
					if (activeCommentId) {
						let $targetComment = $('[comment-id=' + activeCommentId + ']');
						MessageService.confirm('COMMENT_CONFIRM_DELETE', function () {
							return CommentService.delete(process, group, self, activeCommentId).then(function (commentId) {
								let lastComment = _.last(allComments);

								if (lastComment && lastComment.CommentId == commentId) {
									$targetComment.remove();
								} else {
									$targetComment.addClass('is-deleted')
										.find('chip-name').addClass('italic no-select').text(Translator.translate('COMMENT_IS_DELETED'));
								}
							});
						});
					}
				};

				scope.submitComment = function (event) {
					if (event && event.which !== 13) {
						return;
					}

					if (scope.comment) {
						newComment(scope.comment).then(function () {
							scope.comment = '';
						});
					}
				};

				scope.onBlur = function () {
					if (!scope.comment) {
						scope.rows = 1;
						setTextareaHeight();
					}
				};

				scope.onFocus = function () {
					scope.rows = 4;
					setTextareaHeight();
				};

				scope.uploadImage = function (files) {
					if (Common.isTrue(scope.modify && !scope.preventAttachments) && files && files.length) {
						let newFiles = [];

						MessageService.dialogAdvanced({
							template: 'core/ui/input/bare/file/fileCrop.html',
							controller: 'FileCropController',
							locals: {
								Files: files,
								Attachments: {'1':newFiles},
								OnChange: function () {
									queuer(newFiles.pop());

									function queuer(file) {
										if (file) {
											let type = _.startsWith(file.AttachmentData.ContentType, 'image') ?
												'image' : 'file';

											newComment('${' + type + '/' + file.AttachmentId + '}$' + file.Name)
												.then(function () {
													queuer(newFiles.pop());
											});
										}
									}
								},
								Params: {
									name: 'instance_user',
									parentId: undefined,
									cropEnabled: 2
								}
							}
						});
					}
				};

				setTextareaHeight();

				function newComment(comment) {
					scope.loading = true;
					return CommentService.post(process, group, self, comment).then(function (comment) {
						onChange();
						allComments.push(comment);

						$empty.detach();
						let $newComment;
						if (isNote) {
							scope.onBlur();
							$newComment = $(getNoteChip(comment));
							$inputChip.after($newComment);
							$commentSection.animate({ scrollTop: 0 }, 600, 'easeOutCubic');
						} else {
							let today = new Date();
							today.setHours(0, 0, 0, 0);

							if (typeof currentDate === 'undefined' || currentDate.getTime() < today.getTime()) {
								currentDate = today;
								$commentSection.append(getCurrentFormattedDate(today));
							}

							$newComment = $(getSenderChip(comment));
							$commentSection.append($newComment);
							$commentSection.animate(
								{ scrollTop: $commentSection.prop('scrollHeight') }, 600, 'easeOutCubic'
							);
						}

						$newComment.addClass('transition-in');
						setTimeout(function () {
							$newComment.removeClass('transition-in');
						}, 600);
					}).finally(function () {
						scope.loading = false;
					});
				}

				function onChange() {
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}

				function setTextareaHeight() {
					$textarea.css('height', scope.rows * 19 + 9);
				}
			},
			controller: function ($scope) {
				$scope.rows = 1;
			}
		}
	}
})();