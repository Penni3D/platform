﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('inputSmart', InputSmart);

	InputSmart.$inject = ['SaveService', '$compile', 'Common', 'Translator', 'HTMLService', 'Sanitizer', 'CalendarService'];

	function InputSmart(SaveService, $compile, Common, Translator, HTMLService, Sanitizer, CalendarService) {
		function translate(value) {
			return value ? Translator.translate(value) : '';
		}

		return {
			restrict: 'E',
			replace: true,
			scope: {
				modify: '<?',
				options: '<?',
				settings: '<?',
				restrictions: '<?',
				inputModel: '=?model',
				modelStart: '=?',
				modelEnd: '=?',
				change: '&?onChange',
				changeParameters: '<?onChangeParameters',
				modifyResult: '&?'
			},
			link: postLink,
			controller: function ($scope) {
				$scope.modify = typeof $scope.modify === 'undefined' ? true : $scope.modify;
			}
		};

		function postLink(scope, element, attrs) {

			let autoSave = typeof attrs.manualSave === 'undefined';
			let type = attrs.type;
			let placeholder = Sanitizer(translate(attrs.placeholder));
			let max = Sanitizer(attrs.max);
			let precision = Sanitizer(attrs.precision);
			let process = Sanitizer(attrs.process);
			let timeline = typeof attrs.timeline !== 'undefined';

			let optionValue = Sanitizer(attrs.optionValue);
			let optionName = Sanitizer(attrs.optionName);
			let parent = Sanitizer(attrs.parent);
			let self = Sanitizer(attrs.self);
			let group = Sanitizer(attrs.group);
			let itemColumnId = Sanitizer(attrs.itemColumnId);
			let unique = attrs.unique;
			let s;
			switch (type) {
				case '0':
				case 'string':
					s = 2;
					break;
				case '2':
				case 'decimal':
					if (typeof attrs.precision === 'undefined') {
						precision = 2;
					}
				case '1':
				case 'integer':
					s = 0;
					break;
				case 'daterange':
					timeline = true;
				case '3':
				case 'date':
					s = 4;
					break;
				case '6':
				case 'select':
					s = 1;
					break;
				case 'multiselect':
					s = 1;
					if (typeof attrs.max === 'undefined' || attrs.max === '') max = 0;
					break;
				case 'boolean':
					s = 1;
					max = 1;
					scope.options = [
						{name: 'TRUE', value: 'true'},
						{name: 'FALSE', value: 'false'}];
					break;
				case 'user':
					process = 'user';
					s = 6;
				case '5':
				case 'process':
					s = 6;
					break;
				case '4':
				case 'text':
					s = 5;
					break;
				case '7':
				case 'hidden':
					s = 3;
					break;
				case 'checkbox':
					s = 7;
					break;
				case 'radio':
					s = 8;
					break;
				case '10':
				case 'file':
					s = 9;
					break;
				case 'translation':
					s = 10;
					break;
				case '15':
				case 'link':
					s = 11;
					break;
				case '19':
				case 'chip':
					s = 12;
					break;
				case '9':
				case 'rating':
					s = 13;
					break;
				case '25':
				case 'chart':
					s = 14;
					break;
				case 'datetime':
				case '13':
					s = 22;
					break;
				default:
					s = 2;
					break;
			}

			let $input = HTMLService.initialize();
			switch (s) {
				case 0:
					$input
						.element('input-number')
						.attr('model', 'inputModel')
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(attrs.step, 'step', Sanitizer(attrs.step))
						.attrConditional(attrs.suffix, 'suffix', Sanitizer(attrs.suffix))
						.attrConditional(attrs.min, 'min', Sanitizer(attrs.min))
						.attrConditional(max, 'max', max)
						.attrConditional(!isNaN(precision), 'precision', precision);
					break;
				case 1:
					$input
						.element('input-select')
						.attr('model', 'inputModel')
						.attr('options', 'options')
						.attrConditional(max, 'max', max)
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(optionValue, 'option-value', optionValue)
						.attrConditional(optionName, 'option-name', optionName)
						.attrConditional(parent, 'owner', parent)
						.attrConditional(attrs.parentProcess, 'parent-process', Sanitizer(attrs.parentProcess))
						.attrConditional(self, 'self', self)
						.attrConditional(typeof scope.restrictions !== 'undefined', 'restrictions', 'restrictions')
						.attrConditional(typeof attrs.singleValue !== 'undefined', 'single-value')
						.attrConditional(typeof attrs.watchOptions !== 'undefined', 'watch-options')
						.attrConditional(typeof attrs.iconOnly !== 'undefined', 'icon-only')
						.attrConditional(typeof attrs.unselectable !== 'undefined', 'unselectable', Sanitizer(attrs.unselectable))
						.attrConditional(attrs.optionActive, 'option-active', Sanitizer(attrs.optionActive))
						.attrConditional(attrs.optionIcon, 'option-icon', Sanitizer(attrs.optionIcon))
						.attrConditional(attrs.optionColor, 'option-color', Sanitizer(attrs.optionColor))
						.attrConditional(attrs.optionFlag, 'option-flag', Sanitizer(attrs.optionFlag))
						.attrConditional(attrs.optionInuseAll, 'option-inuse-all', Sanitizer(attrs.optionInuseAll))
						.attrConditional(attrs.fromFilters, 'from-filters', Sanitizer(attrs.fromFilters))
						.attrConditional(attrs.ignoreUserGroups, 'ignore-user-groups', Sanitizer(attrs.ignoreUserGroups))

						.attrConditional(attrs.fromprocesscolumnname, 'fromProcessColumnName', attrs.fromprocesscolumnname)
						.attrConditional(attrs.fromprocesscolumnid, 'fromProcessColumnId', attrs.fromprocesscolumnid)
						.attrConditional(attrs.targetprocesscolumnname, 'targetProcessColumnName', attrs.targetprocesscolumnname)
						.attrConditional(attrs.targetprocesscolumnid, 'targetProcessColumnId', attrs.targetprocesscolumnid)
						.attrConditional(attrs.targetprocessname, 'targetProcessName', attrs.targetprocessname)
						.attrConditional(attrs.targetlistcolumn, 'targetListColumn', attrs.targetlistcolumn);
					break;
				case 2:
					$input
						.element('input-string')
						.attr('model', 'inputModel')
						.attrConditional(max, 'max', max)
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(typeof attrs.spellcheck !== 'undefined', 'spellcheck');
					break;
				case 3:
					$input
						.element('input-hidden')
						.attr('model', 'inputModel')
						.attrConditional(placeholder, 'placeholder', placeholder);
					break;
				case 4:
					$input
						.element('input-date')
						.attr('model', 'inputModel')
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(timeline, 'timeline')
						.attrConditional(group, 'group', group)
						.attrConditional(attrs.settings, 'settings', '::settings')
						.attrConditional(attrs.modelStart, 'model-start', 'modelStart')
						.attrConditional(attrs.modelEnd, 'model-end', 'modelEnd');

					break;
				case 5:
					$input
						.element('input-text')
						.attrConditional(attrs.class == 'smart-table', 'table')
						.attr('model', 'inputModel')
						.attrConditional(max, 'max', max)
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(typeof attrs.spellcheck !== 'undefined', 'spellcheck')
						.attrConditional(attrs.rows, 'rows', Sanitizer(attrs.rows));
					break;
				case 6:
					$input
						.element('input-process')
						.attr('process', process)
						.attr('model', 'inputModel')
						.attr('options', 'options')
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(max, 'max', max)
						.attrConditional(itemColumnId, 'item-column-id', itemColumnId)
						.attrConditional(parent, 'parent', parent)
						.attrConditional(typeof attrs.filterable !== 'undefined', 'filterable')
						.attrConditional(typeof attrs.chip !== 'undefined', 'chip', attrs.chip)
						.attrConditional(typeof attrs.preload !== 'undefined', 'preload')
						.attrConditional(typeof attrs.table !== 'undefined', 'table')
						.attrConditional(typeof attrs.openable !== 'undefined', 'openable', Sanitizer(attrs.openWidth))
						.attrConditional(attrs.portfolioId, 'portfolio-id', Sanitizer(attrs.portfolioId))
						.attrConditional(attrs.portfolioid, 'portfolio-id', Sanitizer(attrs.portfolioid))
						.attrConditional(attrs.parentProcess, 'parent-process', Sanitizer(attrs.parentProcess))
						.attrConditional(attrs.direction, 'direction', Sanitizer(attrs.direction))
						.attrConditional(attrs.linkProcess, 'link-process', Sanitizer(attrs.linkProcess))
						.attrConditional(attrs.fromprocesscolumnname, 'fromProcessColumnName', attrs.fromprocesscolumnname)
						.attrConditional(attrs.fromprocesscolumnid, 'fromProcessColumnId', attrs.fromprocesscolumnid)
						.attrConditional(attrs.targetprocesscolumnname, 'targetProcessColumnName', attrs.targetprocesscolumnname)
						.attrConditional(attrs.targetprocesscolumnid, 'targetProcessColumnId', attrs.targetprocesscolumnid)
						.attrConditional(attrs.targetprocessname, 'targetProcessName', attrs.targetprocessname)
						.attrConditional(attrs.modifyResult, 'modify-result', 'returnModifiedResult');
					break;
				case 7:
					$input
						.element('input-check')
						.attr('model', 'inputModel')
						.attr('option-true', Sanitizer(attrs.optionTrue))
						.attr('option-false', Sanitizer(attrs.optionFalse));
					break;
				case 8:
					$input
						.element('input-radio')
						.attr('options', 'options')
						.attr('model', 'inputModel')
						.attrConditional(group, 'group', group)
						.attrConditional(optionValue, 'option-value', optionValue)
						.attrConditional(optionName, 'option-name', optionName);
					break;
				case 9:
					$input
						.element('input-file')
						.attr('self', self)
						.attr('item-column-id', itemColumnId)
						.attrConditional(parent, 'parent', parent)
						.attrConditional(group, 'group', group)
						.attrConditional(typeof scope.restrictions !== 'undefined', 'restrictions', 'restrictions')
						.attrConditional(attrs.settings, 'settings', '::settings');
					break;
				case 10:
					$input
						.element('input-translation')
						.attr('model', 'inputModel')
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(typeof attrs.spellcheck !== 'undefined', 'spellcheck');
					break;
				case 11:
					$input
						.element('input-link')
						.attr('self', self)
						.attr('item-column-id', itemColumnId)
						.attrConditional(parent, 'parent', parent)
						.attrConditional(group, 'group', group)
						.attrConditional(attrs.settings, 'settings', '::settings');
					break;
				case 12:
					$input
						.element('input-chip')
						.attr('model', 'inputModel')
						.attr('options', 'options');
					break;
				case 13:
					$input
						.element('input-rating')
						.attr('model', 'inputModel');
					break;
				case 14:
					$input
						.element('input-chart')
						.attr('chart-id', attrs.chartId)
						.attr('item-ids', attrs.itemId);
					break;
				case 22:
					$input
						.element('input-date')
						.attr('model', 'inputModel')
						.attr('time', true)
						.attrConditional(placeholder, 'placeholder', placeholder)
						.attrConditional(timeline, 'timeline')
						.attrConditional(group, 'group', group)
						.attrConditional(attrs.settings, 'settings', '::settings')
						.attrConditional(attrs.modelStart, 'model-start', 'modelStart')
						.attrConditional(attrs.modelEnd, 'model-end', 'modelEnd');
					break;
			}
			addMandatoryAttributes();

			$compile($input.getHTML())(scope).appendTo(element);

			scope.inputChanged = function () {
				if (autoSave) SaveService.tick();
				if (typeof scope.change === 'function' && typeof scope.change() === 'function') {
					scope.change()(scope.changeParameters);
				}
			};

			scope.returnModifiedResult = function (items) {
				if (typeof scope.modifyResult === 'function' && typeof scope.modifyResult() === 'function') {
					return scope.modifyResult()(items);
				}
			};

			function addMandatoryAttributes() {
				let label = Sanitizer(translate(attrs.label));
				$input
					.attr('modify', 'modify')
					.attr('on-change', 'inputChanged')
					.attrConditional(unique, 'unique', unique)
					.attrConditional(label, 'label', label)
					.attrConditional(typeof attrs.minimal !== 'undefined', 'minimal')
					.attrConditional(Common.isTrue(attrs.requiredField), 'required-field');
			}
		}
	}
})();
