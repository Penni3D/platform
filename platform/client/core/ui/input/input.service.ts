﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('onEnter', OnEnter)
		.service('InputService', InputService);

	OnEnter.$inject = [];

	function OnEnter() {
		return function (scope, element, attrs) {
			element.bind('keydown keypress', function (event) {
				if (event.which === 13) {
					scope.$apply(function () {
						scope.$eval(attrs.onEnter);
					});

					event.preventDefault();
				}
			});
		};
	}

	InputService.$inject = ['$compile', '$rootScope', 'MessageService', '$timeout', 'HTMLService', 'Sanitizer', 'ConnectorService', 'ProcessService'];
	function InputService($compile, $rootScope, MessageService, $timeout, HTMLService, Sanitizer, ConnectorService,ProcessService) {
		let self = this;

		function appendClickTrigger(identifier, specific) {
			if (typeof specific === 'string' && specific.length) {
				return identifier + '.' + specific;
			}

			return identifier;
		}

		self.findProperty = function (object, path) {
			if (typeof path === 'undefined') {
				path = '';
			}

			let p = path.split('.');
			let o = object;

			for (let i = 0, l = p.length; i < l; i++) {
				if (typeof o === 'undefined') {
					return '';
				}

				o = o[p[i]];
			}

			return o;
		};

		self.ensureArray = function (object) {
			if (typeof object !== 'object' || object === null) {
				return [];
			}

			if (Array.isArray(object)) {
				return object;
			}

			let o = [];
			angular.forEach(object, function (option) {
				o.push(option);
			});

			return o;
		};

		self.inputChanged = function (onChange, onChangeParameters) {
			if (typeof onChange === 'function' && typeof onChange() === 'function') {
				$timeout(function () {
					onChange()(onChangeParameters);
				});
			}
		};

		self.removeProcessMenu = function (identifier) {
			if (typeof identifier === 'undefined') {
				identifier = 'process';
			}

			MessageService.triggerCatcher(identifier);
		};

		self.removeChipMenu = function (identifier) {
			if (typeof identifier === 'undefined') {
				identifier = 'chip';
			}

			MessageService.triggerCatcher(identifier);
		};

		self.removeSelectMenu = function (identifier) {
			if (typeof identifier === 'undefined') {
				identifier = 'select';
			}

			MessageService.triggerCatcher(identifier);
		};

		self.removeDatepicker = function (identifier) {
			if (typeof identifier === 'undefined') {
				identifier = 'datepicker';
			}

			MessageService.triggerCatcher(identifier);
		};

		self.datepicker = function (model, settings, element, scope, onSelect) {
			let menuId = appendClickTrigger('datepicker', scope.id);

			scope.$on('$destroy', function () {
				self.removeDatepicker(menuId);
			});

			let $datepicker = $('<datepicker tabindex="0">');
			let datepickerScope = $rootScope.$new(true);
			datepickerScope.applyDate = onSelect;
			datepickerScope.model = model;
			datepickerScope.settings = settings;

			let p = {
				id: menuId,
				inBody: false
			};

			$compile($datepicker)(datepickerScope);
			MessageService.element($datepicker, p, element, datepickerScope);
		};

		self.selectMenu = function (rootLevel, settings, element, scope, globalSelect) {
			let menuId = appendClickTrigger('select', scope.id);

			scope.$on('$destroy', function () {
				self.removeSelectMenu(menuId);
			});

			let width = element.width();
			let $selectMenu = $('<input-select-menu tabindex="0"><select-menu></select-menu></input-select-menu>');
			let selectScope = $rootScope.$new(true);

			selectScope.root = rootLevel;
			selectScope.settings = settings;
			selectScope.restrictions = settings.restrictions;
			selectScope.targetElement = element;
			selectScope.unselect = globalSelect.unselect;
			selectScope.allselect = globalSelect.allselect;
			selectScope.closing = globalSelect.onClose;

			let p = {
				id: menuId,
				inBody: false,
				preventTopAlign: true,
				shiftLeft: -16,
				keyboard: true,
				focus: {
					prevent: true
				}
			};

			$selectMenu.css('width', width + 24);
			$compile($selectMenu)(selectScope);

			MessageService.element($selectMenu, p, element, selectScope);
		};

		self.chipMenu = function (model, options, settings, element, scope, onSelect) {
			let menuId = appendClickTrigger('chip', scope.id);

			scope.$on('$destroy', function () {
				self.removeChipMenu(menuId);
			});

			let chipScope = $rootScope.$new(true);
			chipScope.options = options;
			chipScope.model = model;
			chipScope.settings = settings;
			chipScope.onSelect = function (chip) {
				self.removeChipMenu(menuId);

				let i = options.length;
				while (i--) {
					if (options[i].toLowerCase() === chip.toLowerCase()) {
						onSelect(options[i]);

						return;
					}
				}

				onSelect(chip);
			};

			let $chipMenu = $('<input-chip-menu tabindex="0"><chip-menu></chip-menu></input-chip-menu>');
			$chipMenu.css('width', element.width());
			$compile($chipMenu)(chipScope);

			let shift = 28;

			if (settings.minimal) {
				shift = 0;
			}

			let p = {
				shiftTop: shift,
				id: menuId,
				keyboard: true
			};

			MessageService.element($chipMenu, p, element, chipScope);
		};

		self.processMenu = function (model, items, settings, element, scope, onSelect, modifyResult) {
			let menuId = appendClickTrigger('process', scope.id);

			scope.$on('$destroy', function () {
				self.removeProcessMenu(menuId);
			});

			let processScope = $rootScope.$new(true);
			processScope.settings = settings;
			processScope.preloadItems = items;
			processScope.model = model;
			processScope.preselectedItems = [];
			processScope.rowcount = scope.rowcount;
			processScope.onSelect = function (item) {
				//if(settings.max == 1){
					self.removeProcessMenu(menuId);
					onSelect(item);
			/*	} else {
					item.$$isSelected = true;
					processScope.preselectedItems.push(item);
				}*/
			};

			processScope.onSelectFinally = function(item){
				onSelect(item);
				//item.$$isSelected = false;
			}

			processScope.modifyResult = modifyResult;

			let $processMenu = $('<input-process-menu tabindex="0"><process-menu></process-menu></input-process-menu>');
			$processMenu.css('width', element.width());



			$compile($processMenu)(processScope);

			let shift = 28;

			if (settings.minimal) {
				shift = 0;
			} else if (settings.table) {
				shift = 22;
			}

			let p = {
				shiftTop: shift,
				id: menuId,
				keyboard: true
			};

			MessageService.element($processMenu, p, element, processScope);
		};

		self.inputDialog = function (model, values, element, scope, options) {
			let menuId = appendClickTrigger('input-dialog', scope.id);

			if (typeof values.placeholder === 'undefined') {
				values.placeholder = '';
			}

			let $inputDialog = HTMLService.initialize('input-dialog')
				.inner(HTMLService.initialize('input-smart')
					.attr('minimal')
					.attr('placeholder', Sanitizer(values.placeholder))
					.attr('model', 'modelObject.inputModel')
					.attr('on-change', 'inputChanged')
					.attrConditional(values.step, 'step', Sanitizer(values.step))
					.attrConditional(values.type, 'type', Sanitizer(values.type))
					.attrConditional(values.max, 'max', Sanitizer(values.max))
					.attrConditional(values.min, 'min', Sanitizer(values.min))
					.attrConditional(values.precision, 'precision', Sanitizer(values.precision)))
				.getObject();

			let p = {
				focus: {
					target: function () {
						return $inputDialog.find('input')
					}
				},
				shiftTop: -6,
				shiftLeft: -16,
				id: menuId,
				override: true,
				top: undefined,
				left: undefined
			};

			if (options.override) {
				p.top = element.top;
				p.left = element.left;
				$inputDialog.css('width', element.width + 72);
			} else {
				let offset = element.offset();
				p.top = offset.top;
				p.left = offset.left;
				$inputDialog.css('width', element.width());
			}

			let scope_ = $rootScope.$new();
			scope_.modelObject = model;
			scope_.inputChanged = function () {
				$timeout(function () {
					options.onChange(options.onChangeParameters);
				});
			};

			$compile($inputDialog)(scope_);

			$inputDialog.on('keydown', function (event) {
				if (event.which === 13) {
					setTimeout(function () {
						MessageService.removeElement($inputDialog, menuId, scope_);
					});
				}
			});

			scope.$on('$destroy', function () {
				MessageService.removeElement($inputDialog, menuId, scope_);
			});

			MessageService.element($inputDialog, p, undefined, scope_);
		};

		self.startHelpUpdateListener = function () {
			ConnectorService.listen("HelpTextUpdated", function (process) {
				//When a help text is updated on server, update related help texts on the client
				ProcessService.invalidateActions(process);
			});
		}
	}
})();
