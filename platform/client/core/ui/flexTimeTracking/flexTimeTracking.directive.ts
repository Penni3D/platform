﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('flexTimeTracking', FlexTimeTracking);

	FlexTimeTracking.$inject = [];
	function FlexTimeTracking() {
		return {
			restrict: 'E',
			scope: {
				userItemId: '=?',
				selectedDate: '=?'
			},
			replace: true,
			templateUrl: 'core/ui/flexTimeTracking/flexTimeTracking.html',
			resolve: {
				flexDay: function (flexTimeTrackingService) {
					return flexTimeTrackingService.getFlexDay(null, null);
				}
			},
			controller: function ($scope, $rootScope, ApiService, SaveService, MessageService, Translator) {
				$rootScope.me.locale_id = '';

				if ($scope.selectedDate == null) {
					$scope.selectedDate = ParseDate(new Date());
				}

				function GetData(user_item_id, selected_date) {
					ApiService.v1api('time/flextimetracking').get([user_item_id + '/' + selected_date]).then(function (flexDay) {
						$scope.flexDay = flexDay;
						$scope.selected_date = $scope.flexDay.selected_date;
						$scope.getting = false;
						for (var r = 0; r < $scope.flexDay.flex_rows.length; r++) {
							if ($scope.flexDay.flex_rows[r].type == 0) {
								CalcWorkHours($scope.flexDay.flex_rows[r]);
							}
						}
						CheckRows();
						CalcTotals();
					});
				}

				$scope.getting = false;
				var getting_interval = null;
				$scope.GetDay = function (user_item_id, selected_date) {
					if (!$scope.getting) {
						$scope.getting = true;
						getting_interval = setInterval(function () {
							if (!SaveService.saving) {
								clearInterval(getting_interval);
								GetData(user_item_id, selected_date);
							}
						}, 100);
					}
				};

				$scope.DayChanged = function () {
					SaveService.tick(true);
					$scope.GetDay($scope.flexDay.user_item_id, ParseDate($scope.selected_date));
				};

				$scope.ChangeDay = function (day) {
					day.date = ParseDate(new Date(day.date));
					if (day.date != $scope.flexDay.selected_date) {
						$scope.GetDay($scope.flexDay.user_item_id, day.date);
					}
				};

				$scope.PrevMonth = function () {
					var d = new Date($scope.flexDay.selected_date);
					if (d.getMonth() == 0) {
						d.setFullYear(d.getFullYear() - 1);
						d.setMonth(11);
					}
					else {
						d.setMonth(d.getMonth() - 1);
					}
					$scope.GetDay($scope.flexDay.user_item_id, ParseDate(d));
				};

				$scope.PrevWeek = function () {
					var d = new Date($scope.flexDay.selected_date);
					d.setDate(d.getDate() - 7);
					$scope.GetDay($scope.flexDay.user_item_id, ParseDate(d));
				};

				$scope.PrevDay = function () {
					var d = new Date($scope.flexDay.selected_date);
					d.setDate(d.getDate() - 1);
					$scope.GetDay($scope.flexDay.user_item_id, ParseDate(d));

				};

				$scope.NextDay = function () {
					var d = new Date($scope.flexDay.selected_date);
					d.setDate(d.getDate() + 1);
					$scope.GetDay($scope.flexDay.user_item_id, ParseDate(d));
				};

				$scope.NextWeek = function () {
					var d = new Date($scope.flexDay.selected_date);
					d.setDate(d.getDate() + 7);
					$scope.GetDay($scope.flexDay.user_item_id, ParseDate(d));
				};

				$scope.NextMonth = function () {
					var d = new Date($scope.flexDay.selected_date);
					if (d.getMonth() == 11) {
						d.setFullYear(d.getFullYear() + 1);
						d.setMonth(0);
					}
					else {
						d.setMonth(d.getMonth() + 1);
					}
					$scope.GetDay($scope.flexDay.user_item_id, ParseDate(d));
				};

				function ParseDate(d) {
					return d.getFullYear() + '-' + LeadingZero(d.getMonth() + 1) + '-' + LeadingZero(d.getDate()) + 'T00:00:00Z';
				}

				function LeadingZero(num) {
					if (num < 10) {
						return '0' + num;
					}
					return num
				}

				$scope.ParseHours = function (time, row, leading_zeros, end_time) {
					if (isNumeric(time.hours)) {
						if (time.hours < 0) {
							time.hours = '0';
						}
						else if (!end_time && time.hours > 23) {
							time.hours = '23';
						}
						else if (end_time && time.hours > 24) {
							time.hours = '24';
						}
						else {
							time.hours = parseInt(time.hours);
						}
						if (!isNumeric(time.minutes) || time.hours == 24) {
							if (leading_zeros) {
								time.minutes = '00';
							}
							else {
								time.minutes = '0';
							}
						}
					}
					else {
						time.hours = null;
					}
					CalcWorkHours(row);
					CheckRows();
					CalcTotals();
				};

				$scope.ParseMinutes = function (time, row, leading_zeros) {
					if (isNumeric(time.minutes)) {
						if (time.hours == 24) {
							if (leading_zeros) {
								time.minutes = '00';
							}
							else {
								time.minutes = '0';
							}
						}
						else if (time.minutes < 0) {
							time.minutes = '0';
						}
						else if (time.minutes < 10) {
							if (leading_zeros) {
								time.minutes = '0' + parseInt(time.minutes);
							}
							else {
								time.minutes = parseInt(time.minutes);
							}
						}
						else if (time.minutes > 59) {
							time.minutes = '59';
						}
						else {
							time.minutes = parseInt(time.minutes);
						}
						if (!isNumeric(time.hours)) {
							time.hours = '0';
						}
					}
					else {
						time.minutes = null;
					}
					CalcWorkHours(row);
					CheckRows();
					CalcTotals();
				};

				function isNumeric(num) {
					return !isNaN(parseFloat(num)) && isFinite(num);
				}

				function CalcWorkHours(row) {
					row.order_conflict = false;
					row.lunch_conflict = false;

					if (isNumeric(row.start.hours) && isNumeric(row.start.minutes) && isNumeric(row.end.hours) && isNumeric(row.end.minutes) && isNumeric(row.lunch.hours) && isNumeric(row.lunch.minutes)) {
						row.row_ok = true;

						var start_hours = parseInt(row.start.hours);
						var start_minutes = parseInt(row.start.minutes);
						var end_hours = parseInt(row.end.hours);
						var end_minutes = parseInt(row.end.minutes);
						var lunch_hours = parseInt(row.lunch.hours);
						var lunch_minutes = parseInt(row.lunch.minutes);
						var hours = 0;
						var minutes = 0;

						if (start_hours > end_hours || (start_hours == end_hours && start_minutes >= end_minutes)) {
							row.work.hours = null;
							row.work.minutes = null;

							row.order_conflict = true;

							return;
						}

						hours = end_hours - start_hours - lunch_hours;
						minutes = end_minutes - start_minutes - lunch_minutes;
						while (minutes < 0) {
							hours -= 1;
							minutes += 60;
						}

						if (hours >= 0 && minutes >= 0 && hours + minutes > 0) {
							row.work.hours = hours.toString();
							row.work.minutes = minutes.toString();
						}
						else {
							row.work.hours = null;
							row.work.minutes = null;

							row.lunch_conflict = true;
						}
					}
					else {
						row.work.hours = null;
						row.work.minutes = null;
						
						row.row_ok = false;
					}
				}

				function CheckRows() {
					for (var r = 0; r < $scope.flexDay.flex_rows.length; r++) {
						$scope.flexDay.flex_rows[r].start_conflict = false;
						$scope.flexDay.flex_rows[r].end_conflict = false;
					}

					for (var r1 = 0; r1 < $scope.flexDay.flex_rows.length; r1++) {
						if ($scope.flexDay.flex_rows[r1].type == 0 && isNumeric($scope.flexDay.flex_rows[r1].start.hours) && isNumeric($scope.flexDay.flex_rows[r1].end.hours) && isNumeric($scope.flexDay.flex_rows[r1].start.minutes) && isNumeric($scope.flexDay.flex_rows[r1].end.minutes)) {
							var r1_start_hours = parseInt($scope.flexDay.flex_rows[r1].start.hours);
							var r1_end_hours = parseInt($scope.flexDay.flex_rows[r1].end.hours);
							var r1_start_minutes = parseInt($scope.flexDay.flex_rows[r1].start.minutes);
							var r1_end_minutes = parseInt($scope.flexDay.flex_rows[r1].end.minutes);

							for (var r2 = 0; r2 < $scope.flexDay.flex_rows.length; r2++) {
								if ($scope.flexDay.flex_rows[r2].type == 0 && r1 != r2 && isNumeric($scope.flexDay.flex_rows[r2].start.hours) && isNumeric($scope.flexDay.flex_rows[r2].end.hours) && isNumeric($scope.flexDay.flex_rows[r2].start.minutes) && isNumeric($scope.flexDay.flex_rows[r2].end.minutes)) {
									var r2_start_hours = parseInt($scope.flexDay.flex_rows[r2].start.hours);
									var r2_end_hours = parseInt($scope.flexDay.flex_rows[r2].end.hours);
									var r2_start_minutes = parseInt($scope.flexDay.flex_rows[r2].start.minutes);
									var r2_end_minutes = parseInt($scope.flexDay.flex_rows[r2].end.minutes);

									if ((r1_end_hours > r2_start_hours || (r1_end_hours == r2_start_hours && r1_end_minutes > r2_start_minutes))
										&& (r1_end_hours < r2_end_hours || (r1_end_hours == r2_end_hours && r1_end_minutes <= r2_end_minutes))) {
										$scope.flexDay.flex_rows[r1].end_conflict = true;
										$scope.flexDay.flex_rows[r2].start_conflict = true;
									}

									if ((r1_start_hours < r2_end_hours || (r1_start_hours == r2_end_hours && r1_start_minutes < r2_end_minutes))
										&& (r1_start_hours > r2_start_hours || (r1_start_hours == r2_start_hours && r1_start_minutes >= r2_start_minutes))) {
										$scope.flexDay.flex_rows[r1].start_conflict = true;
										$scope.flexDay.flex_rows[r2].end_conflict = true;
									}
								}
							}
						}
					}
				}

				function CalcTotals() {
					var hours = 0;
					var minutes = 0;
					var sick_leave_hours = 0;
					var sick_leave_minutes = 0;
					var balance_free_hours = 0;
					var balance_free_minutes = 0;
					var has_sick_leave = false;
					var has_balance_free = false;
					for (var r = 0; r < $scope.flexDay.flex_rows.length; r++) {
						if (($scope.flexDay.flex_rows[r].type == 0 || $scope.flexDay.flex_rows[r].type == 1) && isNumeric($scope.flexDay.flex_rows[r].work.hours) && isNumeric($scope.flexDay.flex_rows[r].work.minutes)) {
							if ($scope.flexDay.flex_rows[r].special == 0 || $scope.flexDay.flex_rows[r].special == 1) {
								hours += parseInt($scope.flexDay.flex_rows[r].work.hours);
								minutes += parseInt($scope.flexDay.flex_rows[r].work.minutes);
							}
							else if ($scope.flexDay.flex_rows[r].special == 2) {
								sick_leave_hours += parseInt($scope.flexDay.flex_rows[r].work.hours);
								sick_leave_minutes += parseInt($scope.flexDay.flex_rows[r].work.minutes);
								has_sick_leave = true
							}
							else if ($scope.flexDay.flex_rows[r].special == 3) {
								balance_free_hours += parseInt($scope.flexDay.flex_rows[r].work.hours);
								balance_free_minutes += parseInt($scope.flexDay.flex_rows[r].work.minutes);
								has_balance_free = true;
							}
						}
					}

					$scope.flexDay.day_hours.hours = hours + Math.floor(minutes / 60);
					$scope.flexDay.day_hours.minutes = Math.floor(minutes % 60);
					$scope.flexDay.week[$scope.flexDay.selected_index].day_hours = $scope.flexDay.day_hours;

					$scope.flexDay.day_sick_leave.hours = sick_leave_hours + Math.floor(sick_leave_minutes / 60);
					$scope.flexDay.day_sick_leave.minutes = Math.floor(sick_leave_minutes % 60);
					$scope.flexDay.week[$scope.flexDay.selected_index].sick_leave = has_sick_leave;

					$scope.flexDay.day_balance_free.hours = balance_free_hours + Math.floor(balance_free_minutes / 60);
					$scope.flexDay.day_balance_free.minutes = Math.floor(balance_free_minutes % 60);
					$scope.flexDay.week[$scope.flexDay.selected_index].balance_free = has_balance_free;

					UpdateTotals();
				}

				function UpdateTotals() {
					var html = '';
					if (parseInt($scope.flexDay.day_hours.hours) > 0) {
						html += $scope.flexDay.day_hours.hours + ' <span>h</span> '
					}
					if (parseInt($scope.flexDay.day_hours.hours) == 0 || parseInt($scope.flexDay.day_hours.minutes) > 0) {
						html += $scope.flexDay.day_hours.minutes + ' <span>m</span> '
					}
					html += '/ '
					var daily_hours = parseInt($scope.flexDay.daily_hours.hours) - parseInt($scope.flexDay.day_sick_leave.hours) - parseInt($scope.flexDay.day_balance_free.hours);
					var daily_minutes = parseInt($scope.flexDay.daily_hours.minutes) - parseInt($scope.flexDay.day_sick_leave.minutes) - parseInt($scope.flexDay.day_balance_free.minutes);
					daily_hours += Math.floor(daily_minutes / 60);
					daily_minutes = Math.floor(daily_minutes % 60);
					if (daily_hours > 0) {
						html += daily_hours + ' <span>h</span> '
					}
					if (daily_hours == 0 || daily_minutes > 0) {
						html += daily_minutes + ' <span>m</span>'
					}
					$("div.flex-time-directive td.day_hours").html(html);

					html = '';
					var diff_hours = 0;
					var diff_minutes = 0;
					var diff_negative = false;
					daily_hours += parseInt($scope.flexDay.day_balance_free.hours);
					daily_minutes += parseInt($scope.flexDay.day_balance_free.minutes);
					daily_hours += Math.floor(daily_minutes / 60);
					daily_minutes = Math.floor(daily_minutes % 60);
					if (parseInt($scope.flexDay.day_hours.hours) > daily_hours || (parseInt($scope.flexDay.day_hours.hours) == daily_hours && parseInt($scope.flexDay.day_hours.minutes) > daily_minutes)) {
						diff_hours = parseInt($scope.flexDay.day_hours.hours) - daily_hours;
						diff_minutes = parseInt($scope.flexDay.day_hours.minutes) - daily_minutes;
						if (diff_minutes < 0) {
							diff_hours -= 1;
							diff_minutes += 60;
						}
						html += '+';
					}
					else if (parseInt($scope.flexDay.day_hours.hours) < daily_hours || (parseInt($scope.flexDay.day_hours.hours) == daily_hours && parseInt($scope.flexDay.day_hours.minutes) < daily_minutes)) {
						diff_hours = daily_hours - parseInt($scope.flexDay.day_hours.hours);
						diff_minutes = daily_minutes - parseInt($scope.flexDay.day_hours.minutes);
						if (diff_minutes < 0) {
							diff_hours -= 1;
							diff_minutes += 60;
						}
						html += '-';
						diff_negative = true;
					}
					else {
						html += '±';
					}
					if (diff_hours > 0) {
						html += diff_hours + ' <span>h</span> ';
					}
					if (diff_hours == 0 || diff_minutes > 0) {
						html += diff_minutes + ' <span>m</span>';
					}
					$("div.flex-time-directive td.day_hours_diff").html(html);

					var balance_hours = parseInt($scope.flexDay.balance.hours);
					var balance_minutes = parseInt($scope.flexDay.balance.minutes);
					if (diff_negative) {
						diff_hours = -diff_hours;
						diff_minutes = -diff_minutes;
					}
					if (balance_hours < 0 || balance_minutes < 0) {
						balance_minutes = -Math.abs(balance_minutes);
					}
					balance_hours += diff_hours;
					balance_minutes += diff_minutes;
					if (balance_hours > 0 && balance_minutes < 0) {
						balance_hours -= 1;
						balance_minutes += 60;
					}
					else if (balance_hours < 0 && balance_minutes > 0) {
						balance_hours += 1;
						balance_minutes -= 60;
					}
					if (balance_minutes >= 60) {
						balance_hours += 1;
						balance_minutes -= 60;
					}
					else if (balance_minutes <= -60) {
						balance_hours -= 1;
						balance_minutes += 60;
					}
					var balance = '+';
					if (balance_hours < 0 || balance_minutes < 0) {
						balance = '-';
					}
					else if (balance_hours == 0 && balance_minutes == 0) {
						balance = '±';
					}
					if (balance_hours != 0) {
						balance += Math.abs(balance_hours) + ' <span>h</span> '
					}
					if (balance_hours == 0 || balance_minutes != 0) {
						balance += Math.abs(balance_minutes) + ' <span>m</span>'
					}
					$("div.flex-time-directive td.balance-total").html(Translator.translate('FLEX_TIME_TRACKING_BALANCE') + ': ' + balance);
				}

				$scope.adding = 0;
				var adding_interval = null;
				$scope.Add = function () {
					if ($scope.adding == 0) {
						$scope.adding = 1;
						adding_interval = setInterval(function () {
							if (!SaveService.saving && $scope.adding) {
								clearInterval(adding_interval);
								$scope.adding = 2;

								ApiService.v1api('time/flextimetracking').new([0], $scope.flexDay).then(function (flexDay) {
									$scope.flexDay = flexDay;
									$scope.adding = 0;
									for (var r = 0; r < $scope.flexDay.flex_rows.length; r++) {
										if ($scope.flexDay.flex_rows[r].type == 0) {
											CalcWorkHours($scope.flexDay.flex_rows[r]);
										}
									}
									CheckRows();
									CalcTotals();
								});
							}
						}, 100);
					}
				};

				SaveService.subscribeSave($scope, function () {
					ApiService.v1api('time/flextimetracking').save([], $scope.flexDay);
				});

				$scope.Delete = function (flex_row_item_id) {
					MessageService.confirm("FLEX_TIME_TRACKING_DELETE_CONFIRM", function () {
						ApiService.v1api('time/flextimetracking').delete([flex_row_item_id]);

						for (var r = 0; r < $scope.flexDay.flex_rows.length; r++) {
							if ($scope.flexDay.flex_rows[r].flex_row_item_id == flex_row_item_id) {
								$scope.flexDay.flex_rows.splice(r, 1);
								CheckRows();
								CalcTotals();
								SaveService.tick(true);
								break;
							}
						}
					}, 'warn');
				};

				$scope.ParseWeekWorkHours = function (day_hours) {
					var result = '';
					if (day_hours.hours > 0) {
						result += day_hours.hours + ' h';
						if (day_hours.minutes > 0) {
							result += ' ';
						}
					}
					if (day_hours.minutes > 0) {
						result += day_hours.minutes + ' m';
					}
					return result;
				};

				$scope.GetWeekWorkHoursSpace = function (day_hours) {
					var result = '';
					if ($scope.flexDay.has_double_info && day_hours.hours == 0 && day_hours.minutes == 0) result = String.fromCharCode(8287);
					return result;
				};

				$scope.GetIconsSpace = function (day_hours, has_icons) {
					var result = '';
					if ($scope.flexDay.has_double_info) {
						if (!has_icons) result = String.fromCharCode(8287);
					}
					else {
						if (day_hours.hours == 0 && day_hours.minutes == 0 && !has_icons) result = String.fromCharCode(8287);
					}
					return result;
				};

				$scope.CommentChanged = function (flex_row) {
					if (flex_row.comment.length == 0) {
						flex_row.comment_icon = 'chat_bubble';
					}
					else {
						flex_row.comment_icon = 'chat';
					}
				};

				$scope.SpecialChanged = function () {
					CalcTotals();
				}

				GetData($scope.userItemId, $scope.selectedDate);

				setTimeout('$("div.flex-time-directive div.aria-detect").removeClass("aria-detect");', 1000);
			}
		};
	}
})();