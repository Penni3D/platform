(function () {
	'use strict';

	angular
		.module('core')
		.directive('bindHtmlCompile', BindHtmlCompile);

	BindHtmlCompile.$inject = ['$compile'];
	function BindHtmlCompile($compile) {
		return {
			scope: {
				bindHtmlCompile: '<'
			},
			link: function (scope, element) {
				element.append($compile(scope.bindHtmlCompile)(scope));
			}
		}
	}
})();