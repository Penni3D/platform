﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('FinancialsService', FinancialsService);

	FinancialsService.$inject = ['ApiService'];
	function FinancialsService(ApiService) {
		let self = this;
		let financials = ApiService.v1api('features/FinancialsRest');

		self.getData = function (project_item_id, process_group_id, archive_date, current_date) {
			return financials.get([project_item_id + '/' + process_group_id + '/' + archive_date + '/' + current_date]);
		};

		self.save = function (project_item_id, process_group_id, archive_date, save_rows) {
			return financials.save([project_item_id + '/' + process_group_id + '/' + archive_date], save_rows);
		};

		self.addRow = function (project_item_id, process_group_id, archive_date, category_item_id, current_date) {
			return financials.get([project_item_id + '/' + process_group_id + '/' + archive_date + '/' + category_item_id + '/' + current_date]);
		};

		self.deleteRows = function (project_item_id, process_group_id, archive_date, selected_rows) {
			return financials.new([project_item_id + '/' + process_group_id + '/' + archive_date], selected_rows);
		};

		self.deleteMultipleRows = function (deletes) {
			return financials.new(["deletes"], deletes);
		};

		self.doNothing = function () {
			return;
		};
	}
})();
