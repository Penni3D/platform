﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('financials', Financials);

	Financials.$inject = [];
	function Financials() {
		return {
			restrict: 'E',
			scope: {
				processItemId: '=',
				processGroupId: '=',
				archiveDate: '=',
				states: '=',
				process: '=',
				selectedRows: '=',
				showFooter: '=',
				init: '=',
				loading: '=',
				hideCharts: '=?'
			},
			replace: true,
			templateUrl: 'core/ui/financials/financials.html',
			controller: function ($rootScope, $scope, Common, FinancialsService, SaveService, CalendarService) {
				$scope.loading = true;

				if (typeof $scope.hideCharts !== 'boolean') $scope.hideCharts = false;

				$scope.read = false;
				$scope.write = false;
				$scope.delete_row = false;
				$scope.read_planned = false;
				$scope.write_planned = false;
				$scope.read_estimated = false;
				$scope.write_estimated = false;
				$scope.read_committed = false;
				$scope.write_committed = false;
				$scope.read_actual = false;
				$scope.write_actual = false;
				$scope.period_colspan = false;

				if (angular.isDefined($scope.states.financials) && $scope.states.financials.groupStates[$scope.processGroupId] !== 'undefined') {
					if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_read_planned") > -1) {
						$scope.read = true;
						$scope.read_planned = true;
						$scope.period_colspan++;
					}
					if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_read_estimated") > -1) {
						$scope.read = true;
						$scope.read_estimated = true;
						$scope.period_colspan++;
					}
					if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_read_committed") > -1) {
						$scope.read = true;
						$scope.read_committed = true;
						$scope.period_colspan++;
					}
					if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_read_actual") > -1) {
						$scope.read = true;
						$scope.read_actual = true;
						$scope.period_colspan++;
					}

					if (typeof $scope.archiveDate === 'undefined' || $scope.archiveDate === null) {
						if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_delete") > -1) {
							$scope.delete_row = true;
						}
						if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_write_planned") > -1) {
							$scope.write = true;
							$scope.write_planned = true;
						}
						if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_write_estimated") > -1) {
							$scope.write = true;
							$scope.write_estimated = true;
						}
						if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_write_committed") > -1) {
							$scope.write = true;
							$scope.write_committed = true;
						}
						if ($scope.states.financials.groupStates[$scope.processGroupId].s.indexOf("financials_write_actual") > -1) {
							$scope.write = true;
							$scope.write_actual = true;
						}
					}
				}

				if ($scope.delete_row) $scope.showFooter();

				$scope.multi_level_mode_colspan = 1;

				$scope.first_load = true;

				$scope.data = {};
				
				$scope.selectedRows = [];

				$scope.current_date = ParseDate(new Date());

				$scope.move = 0;
				$scope.prev_tooltip = '';
				$scope.next_tooltip = '';

				$scope.charts = null;

				$scope.GetData = function (current_date) {
					$scope.loading = true;
					if ($scope.saving) {
						setTimeout(function () {
							$scope.GetData(current_date);
						}, 100);
					}
					else {
						FinancialsService.getData($scope.processItemId, $scope.processGroupId, $scope.archiveDate, current_date).then(function (data) {
							if ($scope.charts == null) {
								$scope.charts = data.charts;
							}
							$scope.data = data;
							$scope.current_date = data.current_date;
							if (data.scope == 0) {
								$scope.move = 12;
							} else if (data.scope == 1) {
								$scope.move = 3;
								$scope.prev_tooltip = 'FINANCIALS_PREV_QUARTAL';
								$scope.next_tooltip = 'FINANCIALS_NEXT_QUARTAL';
							} else if (data.scope == 2) {
								$scope.move = 1;
								$scope.prev_tooltip = 'FINANCIALS_PREV_MONTH';
								$scope.next_tooltip = 'FINANCIALS_NEXT_MONTH';
							}
							if ($scope.data.show_last_modified > 0) $scope.data.last_modified.modified_at_text = CalendarService.formatDateTime($scope.data.last_modified.modified_at, false, true, false, false, false);
							CalculateTotals(0);
							$scope.loading = false;

							if ($scope.data.one_level_mode && $scope.first_load) {
								$scope.first_load = false;

								if ($scope.data.one_level_mode) $scope.multi_level_mode_colspan = 0;

								$(document).ready(function () {
									$("div.financials-" + $scope.processItemId + " tr.one-level-mode>td:first-child").remove();
									$("div.financials-" + $scope.processItemId + " tr.one-level-mode>th:first-child").remove();
								});
							}

							$scope.init($scope.data.scope, $scope.move, $scope.prev_tooltip, $scope.next_tooltip);
						});
					}
				};
				$scope.GetData($scope.current_date);

				let moving_interval = null;
				$rootScope.$on('financials-move', function (obj, params) {
					var move = params.move;
					$scope.loading = true;
					moving_interval = setInterval(function () {
						if (!SaveService.saving && !$scope.saving) {
							clearInterval(moving_interval);
							let current_date = new Date($scope.current_date);
							let year = current_date.getFullYear();
							let month = current_date.getMonth() + move;
							while (month < 1) {
								year--;
								month += 12;
							}
							while (month > 12) {
								year++;
								month -= 12;
							}
							setTimeout(function () {
								$scope.GetData(ParseDate(new Date(year, month, 1)));
							}, 100);
						}
					}, 100);
					SaveService.tick(true);
				});

				$scope.saving = false;

				$scope.Changed = function (type) {
					$scope.saving = true;
					if (type > -1) CalculateTotals(type);
				};

				SaveService.subscribeSave($scope, function () {
					if ($scope.saving) {
						let save_rows = [];
						angular.forEach($scope.data.categories, function (cat) {
							angular.forEach(cat.rows, function (row) {
								let fields = {};
								let index = 0;
								angular.forEach(row.fields, function (field) {
									fields[$scope.data.fields[index].name] = field.value;
									index++;
								});
								let save_row = {
									category_item_id: cat.category_item_id,
									row_item_id: row.row_item_id,
									periods: row.periods,
									fields: fields
								};
								save_rows.push(save_row);
							});
						});
						FinancialsService.save($scope.processItemId, $scope.processGroupId, $scope.archiveDate, save_rows).then(function (last_modified) {
							if ($scope.data.show_last_modified > 0) {
								$scope.data.last_modified = last_modified;
								$scope.data.last_modified.modified_at_text = CalendarService.formatDateTime($scope.data.last_modified.modified_at, false, true, false, false, false);
							}
							$rootScope.$broadcast('inputChart-changed');
						});
						$scope.saving = false;
					}
				});

				$scope.AddRow = function (category_item_id) {
					$scope.loading = true;
					FinancialsService.addRow($scope.processItemId, $scope.processGroupId, $scope.archiveDate, category_item_id, $scope.current_date).then(function (row) {
						$scope.data.categories['C' + category_item_id].rows[row.row_item_id] = row;
						if ($scope.data.show_last_modified > 0) {
							$scope.data.last_modified = row.last_modified;
							$scope.data.last_modified.modified_at_text = CalendarService.formatDateTime($scope.data.last_modified.modified_at, false, true, false, false, false);
						}
						$scope.loading = false;
					});
				};

				$rootScope.$on('financials-deleteRows', function () {
					if ($scope.selectedRows.length > 0) {
						FinancialsService.deleteRows($scope.processItemId, $scope.processGroupId, $scope.archiveDate, $scope.selectedRows).then(function (last_modified) {
							if ($scope.data.show_last_modified > 0) {
								$scope.data.last_modified = last_modified;
								$scope.data.last_modified.modified_at_text = CalendarService.formatDateTime($scope.data.last_modified.modified_at, false, true, false, false, false);
							}
						});
						angular.forEach($scope.selectedRows, function (row) {
							delete $scope.data.categories['C' + row.category_item_id].rows[row.row_item_id];
						});
						$scope.selectedRows = [];
						CalculateTotals(0);
					}
				});

				$rootScope.$on('financials-rowsDeleted', function (obj, params) {
					if ($scope.selectedRows.length > 0) {
						if ($scope.data.show_last_modified > 0) {
							$scope.data.last_modified = params.last_modifieds[$scope.processItemId];
							$scope.data.last_modified.modified_at_text = CalendarService.formatDateTime($scope.data.last_modified.modified_at, false, true, false, false, false);
						}
						angular.forEach($scope.selectedRows, function (row) {
							delete $scope.data.categories['C' + row.category_item_id].rows[row.row_item_id];
						});
						$scope.selectedRows = [];
						CalculateTotals(0);
						$scope.loading = false;
					}
				});

				$scope.FormatNumber = function (num) {
					return Common.formatNumber(num);
				};
								
				let CalculateTotals = function (type) {
					if ($scope.read_planned && (type == 0 || type == 1)) CalculatePlaTotals();
					if ($scope.read_estimated && (type == 0 || type == 2)) CalculateEstTotals();
					if ($scope.read_committed && (type == 0 || type == 3)) CalculateComTotals();
					if ($scope.read_actual && (type == 0 || type == 4)) CalculateActTotals();
				};

				let CalculatePlaTotals = function () {
					let cumulative_pla = [];
					angular.forEach($scope.data.totals, function (tot) {
						tot.planned_value = 0;
					});
					$scope.data.year_totals.planned_value = 0;
					$scope.data.total_totals.planned_value = 0;
					angular.forEach($scope.data.cumulatives, function (cum) {
						cum.planned_value = 0;
					});
					$scope.data.year_cumulatives.planned_value = 0;
					angular.forEach($scope.data.categories, function (cat) {
						angular.forEach(cat.periods, function (per) {
							per.planned_value = 0;
						});
						cat.year_periods.planned_value = 0;
						cat.total_periods.planned_value = 0;
						angular.forEach(cat.rows, function (row) {
							let row_year_pla = 0;
							let row_total_pla = 0;
							if (cat.income == 1) {
								cumulative_pla[row.row_item_id] = -row.cumulatives_others.planned_value;
							} else {
								cumulative_pla[row.row_item_id] = row.cumulatives_others.planned_value;
							}
							angular.forEach(row.periods, function (per, p) {
								per.planned_value = parseFloat(per.planned_value);
								if (isNaN(per.planned_value) || !isFinite(per.planned_value)) {
									per.planned_value = 0;
								}
								row_total_pla += per.planned_value;
								if (cat.income == 1) {
									cat.periods[p].planned_value -= per.planned_value;
									$scope.data.totals[p].planned_value -= per.planned_value;
									cumulative_pla[row.row_item_id] -= per.planned_value;
								} else {
									cat.periods[p].planned_value += per.planned_value;
									$scope.data.totals[p].planned_value += per.planned_value;
									cumulative_pla[row.row_item_id] += per.planned_value;
								}
								$scope.data.cumulatives[p].planned_value += cumulative_pla[row.row_item_id];
								if (new Date(per.date).getFullYear() == $scope.data.current_year) {
									row_year_pla += per.planned_value;
									$scope.data.year_cumulatives.planned_value = $scope.data.cumulatives[p].planned_value;
								}
							});
							row.year_periods.planned_value = row_year_pla + row.year_periods_others.planned_value;
							row.total_periods.planned_value = row_total_pla + row.total_periods_others.planned_value;
							if (cat.income == 1) {
								cat.year_periods.planned_value -= row.year_periods.planned_value;
								cat.total_periods.planned_value -= row.total_periods.planned_value;
							} else {
								cat.year_periods.planned_value += row.year_periods.planned_value;
								cat.total_periods.planned_value += row.total_periods.planned_value;
							}
						});
						$scope.data.year_totals.planned_value += cat.year_periods.planned_value;
						$scope.data.total_totals.planned_value += cat.total_periods.planned_value;
					});
				};

				let CalculateEstTotals = function () {
					let cumulative_est = [];
					angular.forEach($scope.data.totals, function (tot) {
						tot.estimated_value = 0;
					});
					$scope.data.year_totals.estimated_value = 0;
					$scope.data.total_totals.estimated_value = 0;
					angular.forEach($scope.data.cumulatives, function (cum) {
						cum.estimated_value = 0;
					});
					$scope.data.year_cumulatives.estimated_value = 0;
					angular.forEach($scope.data.categories, function (cat) {
						angular.forEach(cat.periods, function (per) {
							per.estimated_value = 0;
						});
						cat.year_periods.estimated_value = 0;
						cat.total_periods.estimated_value = 0;
						angular.forEach(cat.rows, function (row) {
							let row_year_est = 0;
							let row_total_est = 0;
							if (cat.income == 1) {
								cumulative_est[row.row_item_id] = -row.cumulatives_others.estimated_value;
							} else {
								cumulative_est[row.row_item_id] = row.cumulatives_others.estimated_value;
							}
							angular.forEach(row.periods, function (per, p) {
								per.estimated_value = parseFloat(per.estimated_value);
								if (isNaN(per.estimated_value) || !isFinite(per.estimated_value)) {
									per.estimated_value = 0;
								}
								row_total_est += per.estimated_value;
								if (cat.income == 1) {
									cat.periods[p].estimated_value -= per.estimated_value;
									$scope.data.totals[p].estimated_value -= per.estimated_value;
									cumulative_est[row.row_item_id] -= per.estimated_value;
								} else {
									cat.periods[p].estimated_value += per.estimated_value;
									$scope.data.totals[p].estimated_value += per.estimated_value;
									cumulative_est[row.row_item_id] += per.estimated_value;
								}
								$scope.data.cumulatives[p].estimated_value += cumulative_est[row.row_item_id];
								if (new Date(per.date).getFullYear() == $scope.data.current_year) {
									row_year_est += per.estimated_value;
									$scope.data.year_cumulatives.estimated_value = $scope.data.cumulatives[p].estimated_value;
								}
							});
							row.year_periods.estimated_value = row_year_est + row.year_periods_others.estimated_value;
							row.total_periods.estimated_value = row_total_est + row.total_periods_others.estimated_value;
							if (cat.income == 1) {
								cat.year_periods.estimated_value -= row.year_periods.estimated_value;
								cat.total_periods.estimated_value -= row.total_periods.estimated_value;
							} else {
								cat.year_periods.estimated_value += row.year_periods.estimated_value;
								cat.total_periods.estimated_value += row.total_periods.estimated_value;
							}
						});
						$scope.data.year_totals.estimated_value += cat.year_periods.estimated_value;
						$scope.data.total_totals.estimated_value += cat.total_periods.estimated_value;
					});
				};

				let CalculateComTotals = function () {
					let cumulative_com = [];
					angular.forEach($scope.data.totals, function (tot) {
						tot.committed_value = 0;
					});
					$scope.data.year_totals.committed_value = 0;
					$scope.data.total_totals.committed_value = 0;
					angular.forEach($scope.data.cumulatives, function (cum) {
						cum.committed_value = 0;
					});
					$scope.data.year_cumulatives.committed_value = 0;
					angular.forEach($scope.data.categories, function (cat) {
						angular.forEach(cat.periods, function (per) {
							per.committed_value = 0;
						});
						cat.year_periods.committed_value = 0;
						cat.total_periods.committed_value = 0;
						angular.forEach(cat.rows, function (row) {
							let row_year_com = 0;
							let row_total_com = 0;
							if (cat.income == 1) {
								cumulative_com[row.row_item_id] = -row.cumulatives_others.committed_value;
							} else {
								cumulative_com[row.row_item_id] = row.cumulatives_others.committed_value;
							}
							angular.forEach(row.periods, function (per, p) {
								per.committed_value = parseFloat(per.committed_value);
								if (isNaN(per.committed_value) || !isFinite(per.committed_value)) {
									per.committed_value = 0;
								}
								row_total_com += per.committed_value;
								if (cat.income == 1) {
									cat.periods[p].committed_value -= per.committed_value;
									$scope.data.totals[p].committed_value -= per.committed_value;
									cumulative_com[row.row_item_id] -= per.committed_value;
								} else {
									cat.periods[p].committed_value += per.committed_value;
									$scope.data.totals[p].committed_value += per.committed_value;
									cumulative_com[row.row_item_id] += per.committed_value;
								}
								$scope.data.cumulatives[p].committed_value += cumulative_com[row.row_item_id];
								if (new Date(per.date).getFullYear() == $scope.data.current_year) {
									row_year_com += per.committed_value;
									$scope.data.year_cumulatives.committed_value = $scope.data.cumulatives[p].committed_value;
								}
							});
							row.year_periods.committed_value = row_year_com + row.year_periods_others.committed_value;
							row.total_periods.committed_value = row_total_com + row.total_periods_others.committed_value;
							if (cat.income == 1) {
								cat.year_periods.committed_value -= row.year_periods.committed_value;
								cat.total_periods.committed_value -= row.total_periods.committed_value;
							} else {
								cat.year_periods.committed_value += row.year_periods.committed_value;
								cat.total_periods.committed_value += row.total_periods.committed_value;
							}
						});
						$scope.data.year_totals.committed_value += cat.year_periods.committed_value;
						$scope.data.total_totals.committed_value += cat.total_periods.committed_value;
					});
				};

				let CalculateActTotals = function () {
					let cumulative_act = [];
					angular.forEach($scope.data.totals, function (tot) {
						tot.actual_value = 0;
					});
					$scope.data.year_totals.actual_value = 0;
					$scope.data.total_totals.actual_value = 0;
					angular.forEach($scope.data.cumulatives, function (cum) {
						cum.actual_value = 0;
					});
					$scope.data.year_cumulatives.actual_value = 0;
					angular.forEach($scope.data.categories, function (cat) {
						angular.forEach(cat.periods, function (per) {
							per.actual_value = 0;
						});
						cat.year_periods.actual_value = 0;
						cat.total_periods.actual_value = 0;
						angular.forEach(cat.rows, function (row) {
							let row_year_act = 0
							let row_total_act = 0;
							if (cat.income == 1) {
								cumulative_act[row.row_item_id] = -row.cumulatives_others.actual_value;
							} else {
								cumulative_act[row.row_item_id] = row.cumulatives_others.actual_value;
							}
							angular.forEach(row.periods, function (per, p) {
								per.actual_value = parseFloat(per.actual_value);
								if (isNaN(per.actual_value) || !isFinite(per.actual_value)) {
									per.actual_value = 0;
								}
								row_total_act += per.actual_value;
								if (cat.income == 1) {
									cat.periods[p].actual_value -= per.actual_value;
									$scope.data.totals[p].actual_value -= per.actual_value;
									cumulative_act[row.row_item_id] -= per.actual_value;
								} else {
									cat.periods[p].actual_value += per.actual_value;
									$scope.data.totals[p].actual_value += per.actual_value;
									cumulative_act[row.row_item_id] += per.actual_value;
								}
								$scope.data.cumulatives[p].actual_value += cumulative_act[row.row_item_id];
								if (new Date(per.date).getFullYear() == $scope.data.current_year) {
									row_year_act += per.actual_value;
									$scope.data.year_cumulatives.actual_value = $scope.data.cumulatives[p].actual_value;
								}
							});
							row.year_periods.actual_value = row_year_act + row.year_periods_others.actual_value;
							row.total_periods.actual_value = row_total_act + row.total_periods_others.actual_value;
							if (cat.income == 1) {
								cat.year_periods.actual_value -= row.year_periods.actual_value;
								cat.total_periods.actual_value -= row.total_periods.actual_value;
							} else {
								cat.year_periods.actual_value += row.year_periods.actual_value;
								cat.total_periods.actual_value += row.total_periods.actual_value;
							}
						});
						$scope.data.year_totals.actual_value += cat.year_periods.actual_value;
						$scope.data.total_totals.actual_value += cat.total_periods.actual_value;
					});
				};

				function ParseDate(d) {
					return d.getFullYear() + '-' + LeadingZero(d.getMonth() + 1) + '-' + LeadingZero(d.getDate()) + 'T00:00:00Z';
				}

				function LeadingZero(num) {
					if (num < 10) {
						return '0' + num;
					}
					return num
				}
			}
		};
	}
})();