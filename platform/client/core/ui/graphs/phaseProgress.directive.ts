(function () {
	'use strict';
    angular
        .module('core.graphs.phaseProgress', [])
        .directive('phaseProgress', phaseProgressDirective);

    phaseProgressDirective.$inject = ['ProcessService', 'ApiService', '$rootScope','ChartColourService', 'Translator'];
    function phaseProgressDirective(ProcessService, ApiService, $rootScope, ChartColourService, Translator) {
		return {
			restrict: 'E',
            templateUrl: 'core/ui/graphs/phaseProgress.html',
			replace: false,
			scope: {
				chartData: '=',
				chartColors: '=',
				chartConfig: '=',
				chartOptions: '='
			},
            controller: function ($scope) {
                var minBubbleSize = 3;
                var maxBubbleSize = 25 - minBubbleSize;
                var phasesExcluded;
                $scope.bubbleXLabels = [];
                $scope.bubbleColors = [];
                var xLabel = "";
                var yLabel = "";
                var toolTip = [];
                var chartGenerated = 0;
                var animationInitial;

                var originalConfigFields = $scope.chartConfig.OriginalData;
               
                if (originalConfigFields.fullWidth == false) {
                    minBubbleSize = minBubbleSize / 2;
                    maxBubbleSize = maxBubbleSize / 2;
                    $scope.maxWidth = '712px';
                }
                else {
                    $scope.maxWidth = '1200px';
                }

                var refreshData = function () {                   
                    if (originalConfigFields.showXAxisLabel == true) {
                        xLabel = originalConfigFields.xAxisLabel[$rootScope.clientInformation["locale_id"]];
                    }
                    if (originalConfigFields.showYAxisLabel == true) {
                        yLabel = originalConfigFields.yAxisLabel[$rootScope.clientInformation["locale_id"]];
                    }

                    ProcessService.getProcessGroups($scope.chartConfig.Process).then(function (GroupData) {
                        var data = $scope.chartData.data;
                        $scope.GroupData = GroupData;
                        phasesExcluded = JSON.parse($scope.chartData.phasesExcluded);

                        if (Array.isArray(phasesExcluded) == false) {
                            phasesExcluded = [];
                        }

                        var groupsInOrder = [];

                        _.each(GroupData, function (group) {
                            if (phasesExcluded.indexOf(group.ProcessGroupId) == -1) {
                                $scope.bubbleXLabels.push(Translator.translation(group.Translation));
                                groupsInOrder.push(group.ProcessGroupId);
                            }
                        });
                       
                        var fieldData = $scope.chartData.fieldTypes;
                        var yAxisFieldName = "";
                        var radiusFieldName = "";
                        var titleFieldName = "";
                        var yAxisFieldTranslation = "";
                        var radiusFieldTranslation = "";
                        yAxisFieldName = fieldData.valueFieldColumn.Name;
                        radiusFieldName = fieldData.value2FieldColumn.Name;
                        titleFieldName = fieldData.nameFieldColumn.Name;
                        yAxisFieldTranslation = fieldData.valueFieldColumn.Translation;
                        radiusFieldTranslation = fieldData.value2FieldColumn.Translation;

                        $scope.datasource = [[], []];
                        $scope.bubbleTitles = [];
                        $scope.bubbleData = [];

                        var maxR = _.maxBy(data, function (o) { return o[radiusFieldName]; })[radiusFieldName];
                        var itemCount = 0;
                        var biggestY = 0;

                        var sortedArr = _.sortBy(data, [radiusFieldName], ['asc']);
                        _.each(sortedArr, function (item) {
                            var yellowGroupCount = 0;
                            var yellowGroupId = 0;

                            angular.forEach(groupsInOrder, function (groupID) {
                                if (groupID in item.phaseData.groupStates && item.phaseData.groupStates[groupID].s.indexOf("yellow")>-1) {
                                    yellowGroupCount = groupsInOrder.indexOf(groupID);
                                    yellowGroupId = groupID;                              
                                }
                            });

                            if (phasesExcluded.indexOf(yellowGroupId) == -1 && yellowGroupId != 0) {
                                var r = item[radiusFieldName] / maxR * maxBubbleSize + minBubbleSize;
                                var y = item[yAxisFieldName]; //Math.floor((Math.random() * 100) + 1);
                                var x = Math.floor((Math.random() * (400 / $scope.bubbleXLabels.length)) + 1); //(item.processstatus / ($scope.bubbleXLabels.length) * 100) - 20 - (Math.floor((Math.random() * 20) + 1));

                                if (y > biggestY) { biggestY = y;}
                                if (x - r <= 0) { x = r+4; }
                                if (x + r > (400 / $scope.bubbleXLabels.length)) { x = (400 / $scope.bubbleXLabels.length) - r-4;}

                                $scope.bubbleTitles.push(itemCount);
                                toolTip.push([item[titleFieldName], yAxisFieldTranslation + ": " + item[yAxisFieldName], radiusFieldTranslation + ": " + item[radiusFieldName]]);

                                x += yellowGroupCount * (400 / $scope.bubbleXLabels.length);

                                if (yellowGroupCount > -1) {                                   
                                    $scope.bubbleData.push({ x: x, y: y, r: r });
                                }

                                itemCount += 1;
                            }                                     
                        });

                        biggestY = biggestY / 100 * 103;

                        angular.forEach(ChartColourService.getColours(), function (color) {
                            $scope.bubbleColors.push({
                                backgroundColor: color,
                                pointBackgroundColor: color,
                                hoverBackgroundColor: color,
                                borderColor: color,
                                fill: false
                            });
                        });

                        $scope.bubbleOptions = {
                            responsive: true,
                            maintainAspectRation: false,
                            tooltips: {
                                custom: function (tooltip) {
                                    if (!tooltip) return;
                                    tooltip.displayColors = false;
                                },
                                callbacks: {
                                    label: function (tooltipItem, data) {
                                        return toolTip[tooltipItem.index];
                                    }
                                }
                            },
                            scales: {
                                xAxes: [
                                    {
                                        id: 'x-axis-1',
                                        display: true,
                                        gridLines: { display: true, drawBorder: true },
                                        ticks: {
                                            fontFamily: 'Roboto', fontSize: 14, display: true, autoSkip: false,
                                            min: 0, max: 400, stepSize: 400 / $scope.bubbleXLabels.length,
                                            callback: function (value, index, values) {
                                                return $scope.bubbleXLabels[index];
                                            }
                                        },
                                        scaleLabel: {
                                            display: true,
                                            labelString: xLabel
                                        }
                                    }
                                ],
                                yAxes: [
                                    {
                                        id: 'y-axis-1',
                                        display: true,
                                        gridLines: { display: true, drawBorder: true },
                                        ticks: {
                                            fontFamily: 'Roboto', fontSize: 14, beginAtZero: true, suggestedMax: biggestY // biggestY - Trying to prevent cutting top bubble ball
                                        },
                                        scaleLabel: {
                                            display: true,
                                            labelString: yLabel
                                        }
                                    }
                                ]
                            }
                        };
                    });
                };
                //refreshData;
                $scope.$watch('chartData', refreshData);              
            }

		};
    }
})();
