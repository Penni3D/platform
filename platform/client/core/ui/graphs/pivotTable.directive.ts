(function () {
	'use strict';
    angular
        .module('core.graphs.pivotTable', [])
        .directive('pivotTable', pivotTableDirective);

    pivotTableDirective.$inject = ['Common', 'ProcessService', 'ApiService', '$rootScope', 'ChartColourService', 'Translator', 'ViewService', 'Colors', 'SelectorService', 'CalendarService','ChartsService'];
    function pivotTableDirective(Common, ProcessService, ApiService, $rootScope, ChartColourService, Translator, ViewService, Colors, SelectorService, CalendarService, ChartsService) {
        return {
            restrict: 'E',
            templateUrl: 'core/ui/graphs/pivotTable.html',
            replace: false,
            scope: {
                chartData: '=',
                chartColors: '=',
                chartConfig: '=',
                chartOptions: '='
            },
            controller: function ($scope, $element) {
                $scope.filtersVisible = false;
                $scope.quickFilterInUse = false;

                $scope.langKey = $rootScope.clientInformation["locale_id"];

                $scope.filterPortfolio = $scope.chartConfig.ProcessPortfolioId;
                if ($scope.chartConfig.OriginalData.main_process_portfolio_id && $scope.chartConfig.OriginalData.main_process_portfolio_id > 0) $scope.filterPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

                $scope.filterData = {};
                $scope.rawDataFiltered = [];

                $scope.filterKeyParams = {};
                $scope.filterKeyParams.chartId = $scope.chartConfig.OriginalData.chartId;
                $scope.filterKeyParams.linkId = $scope.chartConfig.OriginalData.linkId || 0;
                $scope.filterKeyParams.chartProcess = $scope.chartConfig.OriginalData.chartProcess;

                if ($scope.chartData.inMeta) {
                    $scope.filterKeyParams.type = "meta"
                }
                else if ($rootScope.frontpage) {
                    $scope.filterKeyParams.type = "frontPage"
                }
                else {
                    $scope.filterKeyParams.type = "portfolio"
                }

                let refreshData = function () {
                    if (Object.keys($scope.chartData.filterData).length > 0) {
                        $scope.quickFilterInUse = true;
                    }
                    else {
                        $scope.quickFilterInUse = false;
                    }

                    let cf = ChartsService.getChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId);
                    if (cf && $scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) {
                        $scope.filterData = cf;
                    }

                    let quickFiltersInEffect = false;
                    angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
                        if ($scope.filterData[key] != null && $scope.filterData[key].length > 0) quickFiltersInEffect = true;
                    });

                    if (quickFiltersInEffect) {
                        $scope.filterActive = true;
                    }
                    else {
                        $scope.filterActive = false;
                    }

                    let rawData = $scope.chartData.data;


                    if (rawData.length > 0 && $scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) { // Lets apply quick filters to rawData
                        let filtersToBeApplied = [];
                        $scope.rawDataFiltered = rawData;

                        angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
                            if (key in $scope.filterData && $scope.filterData[key].length > 0) filtersToBeApplied.push({ name: key, order: $scope.filterData[key].length, ids: $scope.filterData[key] });
                        });


                        if (filtersToBeApplied.length > 0) {
                            filtersToBeApplied = filtersToBeApplied.sort(dynamicSort("order"));

                            angular.forEach(filtersToBeApplied, function (obj) {
                                $scope.rawDataFiltered = $scope.rawDataFiltered.filter(function (x) {
                                    if (Array.isArray(x[obj["name"]])) {
                                        return x[obj["name"]].some(element => { return obj.ids.includes(element); });
                                    }
                                    else {
                                        return [parseInt(String(x[obj["name"]]).trim())].some(element => {
                                            return obj.ids.includes(element);
                                        });
                                    }
                                });
                            });
                        }

                    }
                    else {
                        $scope.rawDataFiltered = rawData;
                    }

                    let groupXSumValues = {};
                    $scope.groupSum = [];
                    $scope.groupXSum = [];
                    $scope.groupSumNames = [];

                    let labelFieldWithLanguage = String('list_item_' + $rootScope.clientInformation["locale_id"]).toLowerCase().replace("-", "_");
                    let isWide = $scope.chartConfig.OriginalData.fullWidth || false;
                    if (isWide) {
                        $scope.height = 120;
                    }

                    $scope.selectors = SelectorService.getSelectors();
                    let pivotCommon = $scope.chartConfig.OriginalData.pivotCommon; // All configuration values are here
                    let chartData = $scope.chartData; // List items and that kind of values
                    let xleve3IdentifierIndex = 0;

                    let fieldTypes = chartData.fieldTypes;

                    let cumulativeField = null;
                    if (pivotCommon.cumulativeEnd != null) cumulativeField = pivotCommon.cumulativeEnd[0] || null;
                    $scope.maxCumulativeTime = null;
                    
                    angular.forEach($scope.rawDataFiltered, function (obj) {
                        angular.forEach(chartData.fieldTypes, function (type) {
                            if (type.subdataType && type.subdataType > 0 && (type.data_type == 6 || type.data_type == 5)) {
                                if (Array.isArray(obj[type.name]) == false && obj[type.name] != null) {
                                    obj[type.name] = [parseInt(obj[type.name])];
                                }
                            }
                        });
                        if (cumulativeField != null) {
                            let cumulativeDate = obj[cumulativeField] || null;
                            if (cumulativeDate != null && $scope.maxCumulativeTime != null && cumulativeDate > $scope.maxCumulativeTime) {
                                $scope.maxCumulativeTime = cumulativeDate;
                            }
                            else if ($scope.maxCumulativeTime == null && cumulativeDate!=null) {
                                $scope.maxCumulativeTime = cumulativeDate;
                            }
                        }
                    });
                   
                    let xSum1 = pivotCommon.showSumX1[0] || 0;
                    let xSum2 = pivotCommon.showSumX2[0] || 0;
                    let xSum3 = pivotCommon.showSumX3[0] || 0;

                    let ShowAllItemsX1 = pivotCommon.ShowAllItemsX1[0] || 0;
                    let ShowAllItemsX2 = pivotCommon.ShowAllItemsX2[0] || 0;
                    let ShowAllItemsX3 = pivotCommon.ShowAllItemsX3[0] || 0;

                    let ShowAllItemsY1 = pivotCommon.ShowAllItemsY1[0] || 0;
                    let ShowAllItemsY2 = pivotCommon.ShowAllItemsY2[0] || 0;
                    let ShowAllItemsY3 = pivotCommon.ShowAllItemsY3[0] || 0;

                    $scope.columnWidth = pivotCommon.columnWidth || 100;
                    $scope.maxYLabelWidth = 0;


                    $scope.sumName = "";
                    $scope.totalSumName = ""

                    $scope.pivotColors = [{ body: '#FFEBEE', head: '#ef9a9a', alt: "#e57373" },


                    { body: '#F3E5F5', head: '#ce93d8', alt: "#ba68c8" },
                    { body: '#E8EAF6', head: '#9fa8da', alt: "#7986cb" },
                    { body: '#E1F5FE', head: '#81d4fa', alt: "#4fc3f7" },
                    { body: '#E0F2F1', head: '#80cbc4', alt: "#4db6ac" },
                    { body: '#F1F8E9', head: '#c5e1a5', alt: "#aed581" },
                    { body: '#FFFDE7', head: '#fff59d', alt: "#fff176" },
                    { body: '#FFF3E0', head: '#ffcc80', alt: "#ffb74d" },
                    { body: '#FCE4EC', head: '#f48fb1', alt: "#f06292" },//
                    { body: '#EDE7F6', head: '#b39ddb', alt: "#9575cd" },//
                    { body: '#E3F2FD', head: '#90caf9', alt: "#64b5f6" },//
                    { body: '#E0F7FA', head: '#80deea', alt: "#4dd0e1" },//
                    { body: '#E8F5E9', head: '#a5d6a7', alt: "#81c784" },//
                    { body: '#F9FBE7', head: '#e6ee9c', alt: "#dce775" },//
                    { body: '#FFF8E1', head: '#ffe082', alt: "#ffd54f" },//
                    { body: '#FBE9E7', head: '#ffab91', alt: "#ff8a65" }];//

                    $scope.tableId = "pivotTable_" + $scope.chartOptions.title.text;
                    //Excel to download items
                    if (pivotCommon.excelInUse != null && pivotCommon.excelInUse == 1) {
                        let downloadName = Translator.translate('DASHBOARD_PIVOT_EXCEL1') + " " + $scope.chartOptions.title.text + " " + Translator.translate('DASHBOARD_PIVOT_EXCEL2');
                        ViewService.addDownloadable(downloadName, function () {
                            $scope.tableToExcel($scope.tableId);
                        }, ViewService.getViewIdFromElement($element));
                    }

                    $scope.yLevelSumInUse = pivotCommon.showSumY1[0] || 0;

                    $scope.xFields = [];
                    $scope.yFields = [];

                    $scope.xFieldExtras = [];
                    $scope.yFieldExtras = [];

                    $scope.xFieldTypes = [];
                    $scope.yFieldTypes = [];

                    // checkBiggestLevelInuse
                    $scope.xLevelSumInUse = 0;
                    $scope.lastColor = "";
                    $scope.lastLevel2TopIndex = "";

                    let aggregateType = pivotCommon.aggregateType[0] || 0;

                    $scope.aggregateLabel = "";

                    switch (aggregateType) {
                        case 0:
                            $scope.aggregateLabel = "Sum";
                            $scope.sumName = Translator.translate('CHART_COMMON_SUM');
                            $scope.totalSumName = Translator.translate('DASHBOARD_PIVOT_TOTAL_SUM');
                            break;
                        case 1:
                            $scope.aggregateLabel = "Max";
                            $scope.sumName = Translator.translate('CHART_COMMON_MIN');
                            $scope.totalSumName = Translator.translate('DASHBOARD_PIVOT_TOTAL_MAX');
                            break;
                        case 2:
                            $scope.aggregateLabel = "Min";
                            $scope.sumName = Translator.translate('CHART_COMMON_MAN');
                            $scope.totalSumName = Translator.translate('DASHBOARD_PIVOT_TOTAL_MIN');
                            break;
                        case 3:
                            $scope.sumName = "";
                            $scope.sumName = Translator.translate('CHART_COMMON_AVERAGE');
                            $scope.totalSumName = Translator.translate('DASHBOARD_PIVOT_TOTAL_AVERAGE');
                            break;
                        case 4:
                            $scope.aggregateLabel = Translator.translate('CHART_COMMON_CUMULATIVE');
                            $scope.sumName = Translator.translate('CHART_COMMON_SUM');
                            $scope.totalSumName = Translator.translate('DASHBOARD_PIVOT_TOTAL_SUM');
                            break;
                    }

                    let valueField = pivotCommon.value[0];

                    let xLevel = 0;
                    let yLevel = 0;
                    if (pivotCommon.xAxisLevel3 != null && pivotCommon.xAxisLevel3.length > 0) {
                        xLevel = 3;
                        if (xSum3 == 1) $scope.xLevelSumInUse = 1;
                    }
                    else if (pivotCommon.xAxisLevel2 != null && pivotCommon.xAxisLevel2.length > 0) {
                        xLevel = 2;
                        if (xSum2 == 1) $scope.xLevelSumInUse = 1;
                    }
                    else {
                        xLevel = 1;
                        if (xSum1 == 1) $scope.xLevelSumInUse = 1;
                    }
                    if (pivotCommon.yAxisLevel3 != null && pivotCommon.yAxisLevel3.length > 0) {
                        yLevel = 3;
                    }
                    else if (pivotCommon.yAxisLevel2 != null && pivotCommon.yAxisLevel2.length > 0) {
                        yLevel = 2;
                    }
                    else {
                        yLevel = 1;
                    }

                    if (xLevel == 1) {
                        $scope.pivotColors = [{ body: '#FFFFFF', head: '#FFFFFF', alt: "#FFFFFF" }];
                    }

                    $scope.xLevel = xLevel;
                    $scope.yLevel = yLevel;

                    $scope.x1LabelFields = [];
                    $scope.x2LabelFields = [];
                    $scope.x3LabelFields = [];

                    $scope.y1LabelFields = [];
                    $scope.y2LabelFields = [];
                    $scope.y3LabelFields = [];

                    for (let i = 1; i <= 3; i++) {
                        let fieldName = "";
                        let extraFieldName = "";

                        if (pivotCommon["xAxisLevel" + i] != null && pivotCommon["xAxisLevel" + i].length > 0) {
                            let fieldName = pivotCommon["xAxisLevel" + i][0];
                            $scope.xFields.push(fieldName);



                            if (pivotCommon["xAxisLevel" + i + "Extra"] != null && pivotCommon["xAxisLevel" + i + "Extra"].length > 0) {
                                $scope.xFieldExtras.push(pivotCommon["xAxisLevel" + i + "Extra"][0]);
                            }
                            let fieldType = parseInt(fieldTypes[pivotCommon["xAxisLevel" + i] + "_type"].data_type);
                            $scope.xFieldTypes.push(fieldType);

                            let extra = pivotCommon["xAxisLevel" + i + "Extra"];
                            switch (fieldType) {
                                case 5:
                                    if (extra.length > 0) {
                                        $scope["x" + i + "LabelFields"] = [extra];
                                    }
                                    else {
                                        let items = chartData[fieldName + "_listData"];

                                        if ($scope.selectors[items.process].default != null) {
                                            angular.forEach($scope.selectors[items.process].default.Primary, function (obj) {
                                                $scope["x" + i + "LabelFields"].push(obj.Name);
                                            });
                                        }
                                        else if ($scope.selectors[items.process].override != null) {
                                            angular.forEach($scope.selectors[items.process].override.Primary, function (obj) {
                                                $scope["x" + i + "LabelFields"].push(obj.Name);
                                            });
                                        }

                                    }
                                    break;
                                case 6:
                                    let items = $scope.chartData[fieldName + "_items"];
                                    if (extra.length > 0) {
                                        if (extra == "list_item" && items.length > 0 && items[0].hasOwnProperty(labelFieldWithLanguage)) {
                                            $scope["x" + i + "LabelFields"] = [labelFieldWithLanguage];
                                        }
                                        else {
                                            $scope["x" + i + "LabelFields"] = [extra];
                                        }
                                    }
                                    else {
                                        if (items.length > 0 && items[0].hasOwnProperty(labelFieldWithLanguage)) {
                                            $scope["x" + i + "LabelFields"] = [labelFieldWithLanguage];
                                        }
                                        else {
                                            $scope["x" + i + "LabelFields"] = ["list_item"];
                                        }
                                    }
                                    break;
                                case 3:
                                    $scope["x" + i + "LabelFields"] = [];
                                    break;
                            }
                        }
                        if (pivotCommon["yAxisLevel" + i] != null && pivotCommon["yAxisLevel" + i].length > 0) {
                            let fieldName = pivotCommon["yAxisLevel" + i][0];
                            $scope.yFields.push(fieldName);

                            if (pivotCommon["yAxisLevel" + i + "Extra"] != null && pivotCommon["yAxisLevel" + i + "Extra"].length > 0) {
                                $scope.yFieldExtras.push(pivotCommon["yAxisLevel" + i + "Extra"][0]);
                            }
                            let fieldType = fieldTypes[pivotCommon["yAxisLevel" + i] + "_type"].data_type;
                            $scope.yFieldTypes.push(fieldType);

                            let extra = pivotCommon["yAxisLevel" + i + "Extra"];
                            switch (parseInt(fieldType)) {
                                case 5:
                                    if (extra.length > 0) {
                                        $scope["y" + i + "LabelFields"] = [extra];
                                    }
                                    else {
                                        let items = chartData[fieldName + "_listData"];

                                        let selectorType = "default";
                                        if ($scope.selectors[items.process]['default'] == null) selectorType = "overwrite";

                                        angular.forEach($scope.selectors[items.process][selectorType].Primary, function (obj) {
                                            $scope["y" + i + "LabelFields"].push(obj.Name);
                                        });
                                    }
                                    break;
                                case 6:
                                    let items = $scope.chartData[fieldName + "_items"];
                                    if (extra.length > 0) {
                                        if (extra == "list_item" && items.length > 0 && items[0].hasOwnProperty(labelFieldWithLanguage)) {
                                            $scope["y" + i + "LabelFields"] = [labelFieldWithLanguage];
                                        }
                                        else {
                                            $scope["y" + i + "LabelFields"] = [extra];
                                        }
                                    }
                                    else {
                                        if (items.length > 0 && items[0].hasOwnProperty(labelFieldWithLanguage)) {
                                            $scope["y" + i + "LabelFields"] = [labelFieldWithLanguage];
                                        }
                                        else {
                                            $scope["y" + i + "LabelFields"] = ["list_item"];
                                        }
                                    }
                                    break;
                                case 3:
                                    $scope["y" + i + "LabelFields"] = [];
                                    break;
                            }
                        }

                        if ($scope.xFields.length < i) {
                            $scope.xFields.push("");
                        }
                        if ($scope.xFieldExtras.length < i) {
                            $scope.xFieldExtras.push("");
                        }
                        if ($scope.xFieldTypes.length < i) {
                            $scope.xFieldTypes.push(-1);
                        }

                        if ($scope.yFields.length < i) {
                            $scope.yFields.push("");
                        }
                        if ($scope.yFieldTypes.length < i) {
                            $scope.yFieldTypes.push(-1);
                        }
                        if ($scope.yFieldExtras.length < i) {
                            if ($scope.yFieldTypes[i] == 3) {
                                $scope.yFieldExtras.push(0);
                            }
                            else {
                                $scope.yFieldExtras.push("");
                            }
                        }

                    }

                    $scope.axisData = {};

                    $scope.axisData.xItems1 = {};
                    $scope.axisData.xItems2 = [];
                    $scope.axisData.xItems3 = [];

                    $scope.axisData.yItems1 = [];
                    $scope.axisData.yItems2 = [];
                    $scope.axisData.yItems3 = [];

                    $scope.axisData.xLabelsL2Filtered = [];
                    $scope.axisData.yLabelsL2Filtered = [];

                    $scope.axisData.level1XColumns = 0;
                    $scope.axisData.level2XColumns = 0;
                    $scope.axisData.level3XColumns = 0;

                    $scope.axisData.level2XLabels = [];
                    $scope.axisData.level3XLabels = [];

                    $scope.axisData.yLabels = [];
                    $scope.columnSpanBylevel = function (level, index) {
                        switch (level) {
                            case 1:
                                if ($scope.axisData.level3XColumns > 0) {

                                    var count = 0;
                                    angular.forEach($scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == index }), function (row) {
                                        count += row.labels.length;
                                    });
                                    return count;
                                }
                                else if ($scope.axisData.level2XColumns > 0) {
                                    var count = 0;
                                    angular.forEach($scope.axisData.xItems2.filter(function (x) { return x.topLevelIndex == index }), function (row) {
                                        count += row.labels.length;
                                    });
                                    return count;
                                }
                                else {
                                    return 1;
                                }
                                break;
                            case 2:
                                if ($scope.axisData.level3XColumns > 0) {
                                    return $scope.axisData.xItems3[index].labels.length;
                                }
                                else {
                                    return 1;
                                }

                                break;
                        }
                    }

                    $scope.upperColumnWidth = function (level, index) {
                        switch (level) {
                            case 1:
                                if ($scope.axisData.level3XColumns > 0) {

                                    var count = 0;
                                    angular.forEach($scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == index }), function (row) {
                                        count += row.labels.length;
                                    });
                                    return count * $scope.columnWidth;
                                }
                                else if ($scope.axisData.level2XColumns > 0) {
                                    var count = 0;
                                    angular.forEach($scope.axisData.xItems2.filter(function (x) { return x.topLevelIndex == index }), function (row) {
                                        count += row.labels.length;
                                    });
                                    return count * $scope.columnWidth;
                                }
                                else {
                                    return 1 * $scope.columnWidth;
                                }
                                break;
                            case 2:
                                if ($scope.axisData.level3XColumns > 0) {
                                    return $scope.axisData.xItems3[index].labels.length * $scope.columnWidth;
                                }
                                else {
                                    return 1 * $scope.columnWidth;
                                }

                                break;
                        }
                    }

                    $scope.openPortfolio = function (ids) {
                        var clickable = $scope.chartConfig.OriginalData.pivotCommon.drillDown || 0;
                        if (clickable == 1) {
                            if (ids.length > 0) {
                                let clickProcess = $scope.chartConfig.OriginalData.chartProcess;
                                let clickPortfolio = $scope.chartConfig.ProcessPortfolioId;

                                if ($scope.chartConfig.OriginalData.mainProcessLinkMode) {
                                    let linkField = $scope.chartConfig.OriginalData.link_process_field;
                                    clickProcess = $scope.chartConfig.OriginalData.process;
                                    clickPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

                                    let dataT = $scope.rawDataFiltered.filter(function (x) {
                                        return ids.includes(x.item_id);
                                    });
                                    let parentIds = [];

                                    angular.forEach(dataT, function (item) {
                                        let tempArr = item[linkField].toString().split(",");

                                        for (let i = 0; i < tempArr.length; i++) {
                                            if (parentIds.includes(tempArr[i]) == false) parentIds.push(tempArr[i]);
                                        }

                                    });
                                    ids = parentIds;
                                }

                                ViewService.view('portfolio', {
                                    params: {
                                        portfolioId: clickPortfolio,
                                        restrictions: { 0: ids },
                                        process: clickProcess,
                                        chartForceFlatMode: true,
                                        titleExtension: $scope.chartOptions.title.text
                                    }
                                },
                                    {
                                        split: true, size: 'normal'
                                    }
                                );
                            }
                        }
                    };

                    $scope.setFormat = function (value) {
                        let val = value || 0;
                        let numberOfDecimals = $scope.chartConfig.OriginalData.pivotCommon.decimalNumber || 0;
                        return Common.formatNumber(val, numberOfDecimals);
                    };
                    $scope.formatCell = function (valueItem) {
                        if (valueItem.isSum == true) {
                            return "background-color:" + $scope.pivotColors[valueItem.topLevelIndex].body + ";font-weight: 500;";
                        }
                        else {
                            return "background-color:" + $scope.pivotColors[valueItem.topLevelIndex].body;
                        }
                    }
                    $scope.formatSumRowCell = function (index) {
                        if ($scope.sumRowSumIndexes.indexOf(index) > -1) {
                            return "background-color: " + $scope.pivotColors[$scope.sumRowTopLevelIndex[index]].body;
                        }
                        else {
                            return "background-color: " + $scope.pivotColors[$scope.sumRowTopLevelIndex[index]].body;
                        }
                    }
                    $scope.getLabel = function (level, labelItem) {
                        let label = "";
                        if (typeof labelItem == "string" || typeof labelItem == "number") {
                            label = labelItem;
                        }
                        else {
                            if ($scope.xFieldTypes[level - 1] == 5 || $scope.xFieldTypes[level - 1] == 6) {
                                angular.forEach($scope["x" + level + "LabelFields"], function (field) {
                                    label += labelItem[field];
                                    if ($scope["x" + level + "LabelFields"].indexOf(field) != $scope["x" + level + "LabelFields"].length - 1) {
                                        label += " ";
                                    }
                                });
                            }
                        }
                        return label;
                    }

                    $scope.getMaxWidth = function () {
                        let columns = 0;
                        if ($scope.xLevel == 1) {
                            columns = $scope.axisData["xItems" + $scope.xLevel].labels.length;
                        }
                        else {
                            columns = $scope.axisData["level" + $scope.xLevel + "XLabels"].length;
                        }

                        let ysumInUse = pivotCommon.showSumY1[0] || 0;
                        let width = columns * $scope.columnWidth + $scope.maxYLabelWidth;
                        if (ysumInUse == 1) width += $scope.columnWidth;
                        if (ysumInUse == 2) {
                            for (let i = 0; i < $scope.groupSum.length; i++) {
                                width += $scope.columnWidth;
                            }
                        }
                        return "max-width:" + parseInt(width) + "px;";
                    }

                    $scope.formatColumnCell = function (lev, index, item, topLevelIndex) {
                        let result = [];
                        if ($scope.xLevel == lev) {
                            result.push("text-align:right");
                        }

                        if (lev == 1 && $scope.xLevel != 1) {
                            result.push("font-size: 24px")
                        }

                        if (lev == 2) {
                            if ($scope.xLevel == 3) {
                                result.push("color: white")
                            }
                            if (typeof item == "string") {
                                if ($scope.xLevel == 3) {

                                    if ($scope.lastColor != $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].alt) {
                                        result.push("background-color:" + $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].alt);
                                        $scope.lastColor = $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].alt;
                                    }
                                    else {
                                        result.push("background-color:" + $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].head);
                                        $scope.lastColor = $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].head;
                                    }
                                }
                                else {
                                    result.push("background-color:" + $scope.pivotColors[$scope.sumRowTopLevelIndex[index]].body);
                                }

                                if (item.toLowerCase() == "sum") result.push("font-weight: 500")
                            }
                            else {
                                if ($scope.xLevel == 3) {
                                    if ($scope.lastColor != $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].alt) {
                                        result.push("background-color:" + $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].alt);
                                        $scope.lastColor = $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].alt;
                                    }
                                    else {
                                        result.push("background-color:" + $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].head);
                                        $scope.lastColor = $scope.pivotColors[$scope.axisData.level2XLabels[index].topLevelIndex].head;
                                    }
                                }
                                else {
                                    result.push("background-color:" + $scope.pivotColors[$scope.sumRowTopLevelIndex[index]].body);
                                }

                            }
                        }
                        if (lev == 3) {
                            if (typeof item == "string") {
                                result.push("background-color:" + $scope.pivotColors[$scope.sumRowTopLevelIndex[index]].body);
                                if (item.toLowerCase() == "sum") result.push("font-weight: 500")
                                if (item.toLowerCase() == "sum") result.push("color: black")
                            }
                            else {
                                result.push("background-color:" + $scope.pivotColors[$scope.sumRowTopLevelIndex[index]].body);
                            }
                        }
                        return result.join(";");
                    }

                    $scope.tableToExcel = function (table) {
                        let sheetName = $scope.chartOptions.title.text;
                        let fileName = $scope.chartOptions.title.text + " - " + CalendarService.formatDate(new Date()) + ".xls";
                        let originalTable = document.getElementById(table);
                        let tableCopy = originalTable.cloneNode(true);

                        let xIndex = 0;
                        let yIndex = 1;
                        let index = 0;

                        angular.forEach(tableCopy.rows, function (row) {
                            index += 1;
                            if (yIndex > $scope.xLevel) {
                                angular.forEach(row.cells, function (cell) {
                                    if (xIndex > 0) {
                                        let text = cell.textContent;
                                        cell.textContent = text.replace(/\s/g, '');
                                        if (cell.attributes.style == null) cell.attributes.style = {};
                                        cell.attributes.style.value += ";border:thin solid #C0C0C0;font-weight: normal !important;";

                                    }
                                    else {
                                        let text = cell.textContent;

                                        if (text.indexOf("/") > -1) {
                                            let arr = text.split("/");
                                            cell.textContent = arr[0] + "|" + arr[1];
                                        }
                                        if (cell.attributes.style == null) cell.attributes.style = {};
                                        cell.attributes.style.value += ";border:thin solid #C0C0C0;font-weight: normal !important;";
                                    }
                                    xIndex += 1;
                                });
                            }
                            else {
                                angular.forEach(row.cells, function (cell) {
                                    let text = cell.textContent;
                                    if (text.indexOf("/") > -1) {
                                        let arr = text.split("/");
                                        cell.textContent = arr[0] + "|" + arr[1];
                                    }
                                    if (cell.attributes.style == null) cell.attributes.style = {};
                                    cell.attributes.style.value += ";border:thin solid #C0C0C0;font-weight: normal !important;";
                                    xIndex += 1;
                                });
                            }

                            yIndex += 1;
                            xIndex = 0;
                        });

                        let uri = 'data:application/vnd.ms-excel;base64,'
                            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="https://www.w3.org/TR/html401"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

                        if (!table.nodeType) table = tableCopy;
                        let ctx = { worksheet: sheetName || 'Worksheet', table: table.innerHTML };

                        if (window.navigator.userAgent.indexOf("MSIE") > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) || !!navigator.userAgent.match(/Trident\/7\./) || window.navigator.userAgent.indexOf("Edge") > -1) {
                            if (window.navigator.msSaveBlob) {
                                let tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
                                tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
                                tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';
                                tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
                                tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
                                tab_text = tab_text + "<table>";
                                tab_text = tab_text + (<HTMLScriptElement>tableCopy).innerHTML;
                                tab_text = tab_text + '</table></body></html>';

                                var blob = new Blob([tab_text], { type: "application/csv;charset=utf-8;" });
                                navigator.msSaveBlob(blob, 'Test file.xls');
                            }
                        } else {
                            let downloadLink;
                            downloadLink = document.createElement("a");
                            document.body.appendChild(downloadLink);
                            downloadLink.href = uri + base64(format(template, ctx));
                            downloadLink.download = fileName;
                            downloadLink.click();
                        }
                    };

                    $scope.dataByFilter = {};

                    // x-axis column construction
                    for (let i = 0; i < 3; i++) {
                        switch (i) {
                            case 0:
                                let labelsObj = getLabelItemsWithIds($scope.rawDataFiltered, 1, 0);

                                let undefined = [];
                                if (labelsObj.xItemsFilters.indexOf("Undefined") > -1) {
                                    angular.forEach(labelsObj.xItemsFilters, function (filter) {
                                        if (labelsObj.xItemsFilters[labelsObj.xItemsFilters.indexOf("Undefined")] == "Undefined") {
                                            undefined.push(1);
                                        }
                                        else {
                                            undefined.push(0);
                                        }
                                    });
                                }

                                if (labelsObj.xItems != null) {
                                    $scope.axisData.xItems1 = { labels: labelsObj.xItems, filters: labelsObj.xItemsFilters, dataType: $scope.xFieldTypes[i], undefined: undefined };
                                    $scope.axisData.yItems1 = { labels: labelsObj.yItems, filters: labelsObj.yItemsFilters, dataType: $scope.yFieldTypes[i] };
                                    $scope.axisData.level1XColumns = labelsObj.xItemsFilters.length;

                                    // Lets duplicate color array if number of level1 x-items is greater than length of color array 
                                    if (labelsObj.xItemsFilters.length > $scope.pivotColors.length) {
                                        while (labelsObj.xItemsFilters.length > $scope.pivotColors.length) {
                                            angular.forEach(angular.copy($scope.pivotColors), function (colorObj) {
                                                $scope.pivotColors.push(colorObj);
                                            });
                                        }
                                    }
                                }
                                break;
                            case 1:
                                if (xLevel > 1) {
                                    var undefinedToBeRemoved = [];
                                    angular.forEach($scope.axisData.xItems1.filters, function (filter) {
                                        var topLevelIndex = $scope.axisData.xItems1.filters.indexOf(filter);
                                        let filtered = $scope.rawDataFiltered.filter(function (x) {
                                            if ((filter == "Undefined" && x[$scope.xFields[0]].length == 0) || (x[$scope.xFields[0]].indexOf(filter) > -1) || $scope.xFieldTypes[0] == 3 && formatDateLabel(0, "x", x[$scope.xFields[0]]) == filter) {
                                                return true;
                                            }
                                            else {
                                                if (filter == "Undefined" && undefinedToBeRemoved.indexOf($scope.axisData.xItems1.filters.indexOf(filter)) == -1) {
                                                    undefinedToBeRemoved.push($scope.axisData.xItems1.filters.indexOf(filter));
                                                }
                                                return false;
                                            }
                                        });
                                        $scope.dataByFilter[filter] = filtered;
                                        if (filtered.length > 0 || ShowAllItemsX1 == 1) {
                                            $scope.axisData.xLabelsL2Filtered.push(filtered);
                                            let labelsObjX = getLabelItemsWithIds(filtered, 2, 0);
                                            $scope.axisData.xItems2.push({ topLevelIndex: topLevelIndex, level1Filter: filter, labels: labelsObjX.xItems, filters: labelsObjX.xItemsFilters, dataType: $scope.xFieldTypes[i] });

                                            angular.forEach(labelsObjX.xItems, function (item) {
                                                let undefinedV = 0;
                                                if (labelsObjX.xItemsFilters[labelsObjX.xItems.indexOf(item)] == "Undefined") {
                                                    undefinedV = 1;
                                                }
                                                $scope.axisData.level2XLabels.push({ item: item, topLevelIndex: topLevelIndex, undefined: undefinedV });
                                            });
                                            $scope.axisData.level2XColumns += labelsObjX.xItemsFilters.length;
                                        }
                                    });

                                    angular.forEach(undefinedToBeRemoved.reverse(), function (index) {
                                        $scope.axisData.xItems1.filters.splice(index, 1);
                                        $scope.axisData.xItems1.labels.splice(index, 1);
                                    });
                                }
                                if (yLevel > 1) {
                                    angular.forEach($scope.axisData.yItems1.filters, function (filter) {
                                        var topLevelIndex = $scope.axisData.yItems1.filters.indexOf(filter);
                                        let filtered = $scope.rawDataFiltered.filter(function (x) {
                                            if ((filter == "Undefined" && x[$scope.yFields[0]].length == 0) || (x[$scope.yFields[0]].indexOf(filter) > -1) || $scope.yFieldTypes[0] == 3 && formatDateLabel(0, "y", x[$scope.yFields[0]]) == filter) {
                                                return true;
                                            }
                                            else {
                                                return false;
                                            }
                                        });

                                        if (filtered.length > 0 || ShowAllItemsY1 == 1) {
                                            $scope.axisData.yLabelsL2Filtered.push(filtered);
                                            let labelsObjY = getLabelItemsWithIds(filtered, 2, 1);;
                                            $scope.axisData.yItems2.push({ topLevelIndex: topLevelIndex, level1Filter: filter, labels: labelsObjY.yItems, filters: labelsObjY.yItemsFilters, dataType: $scope.yFieldTypes[i] });
                                            $scope.axisData.level2YColumns += labelsObjY.yItemsFilters.length;
                                        }
                                    });
                                }
                                break;
                            case 2:
                                if (xLevel > 2) {
                                    angular.forEach($scope.axisData.xLabelsL2Filtered, function (filterSet) {
                                        let undefinedToBeRemoved2 = [];
                                        angular.forEach($scope.axisData.xItems2[$scope.axisData.xLabelsL2Filtered.indexOf(filterSet)].filters, function (filter) {
                                            let filtered = filterSet.filter(function (x) {
                                                if ((filter == "Undefined" && x[$scope.xFields[1]].length == 0) || (x[$scope.xFields[1]].indexOf(filter) > -1) || $scope.xFieldTypes[1] == 3 && formatDateLabel(1, "x", x[$scope.xFields[1]]) == filter) {
                                                    return true;
                                                }
                                                else {
                                                    if (filter == "Undefined" && undefinedToBeRemoved2.indexOf($scope.axisData.xItems2[$scope.axisData.xLabelsL2Filtered.indexOf(filterSet).topLevelIndex]) == -1) {
                                                        let x2Item = $scope.axisData.xItems2[$scope.axisData.xLabelsL2Filtered.indexOf(filterSet)];
                                                        let x3Items = $scope.axisData.xItems3.filter(function (x) {
                                                            return x.topLevelIndex == x2Item.toplevelIndex && x.level2Filter == filter;
                                                        });

                                                        if (x3Items.length == 0 && undefinedToBeRemoved2.indexOf(x2Item.topLevelIndex) == -1) {
                                                            undefinedToBeRemoved2.push(x2Item.topLevelIndex + 1);
                                                        }
                                                    }
                                                    return false;
                                                }
                                            });
                                            $scope.dataByFilter[filter] = filtered;
                                            let topLevelIndex = $scope.axisData.xItems2[$scope.axisData.xLabelsL2Filtered.indexOf(filterSet)].topLevelIndex;
                                            let level1Filter = $scope.axisData.xItems2[$scope.axisData.xLabelsL2Filtered.indexOf(filterSet)].level1Filter;

                                            if (filtered.length > 0 || ShowAllItemsX2 == 1) {
                                                let labelsObjX = getLabelItemsWithIds(filtered, 3, 0);

                                                $scope.axisData.xItems3.push({ topLevelIndex: topLevelIndex, level1Filter: level1Filter, level2Filter: filter, labels: labelsObjX.xItems, filters: labelsObjX.xItemsFilters, dataType: $scope.xFieldTypes[i], xleve3IdentifierIndex: xleve3IdentifierIndex });
                                                angular.forEach(labelsObjX.xItems, function (item) {
                                                    let undefinedV = 0;
                                                    if (labelsObjX.xItemsFilters[labelsObjX.xItems.indexOf(item)] == "Undefined") {
                                                        undefinedV = 1;
                                                    }
                                                    $scope.axisData.level3XLabels.push({ item: item, topLevelIndex: topLevelIndex, undefined: undefinedV, categoryIndex: -1, level: -1, xleve3IdentifierIndex: xleve3IdentifierIndex });
                                                });
                                                $scope.axisData.level3XColumns += labelsObjX.xItemsFilters.length;
                                                xleve3IdentifierIndex += 1;
                                            }
                                        });

                                        angular.forEach(undefinedToBeRemoved2.reverse(), function (index) {
                                            let undefined = $scope.axisData.level2XLabels.filter(function (x) { return x.topLevelIndex == index && x.item == Translator.translate('CHART_COMMON_UNDEFINED'); });
                                            let indexOfUndefined = $scope.axisData.level2XLabels.indexOf(undefined[0])
                                            if (indexOfUndefined > -1) {
                                                $scope.axisData.xItems2[$scope.axisData.xLabelsL2Filtered.indexOf(filterSet)].filters.splice(index, 1);
                                                $scope.axisData.xItems2[$scope.axisData.xLabelsL2Filtered.indexOf(filterSet)].labels.splice(index, 1);
                                                $scope.axisData.level2XLabels.splice(indexOfUndefined, 1);
                                            }
                                        });
                                    });
                                }

                                if (yLevel > 2) {
                                    angular.forEach($scope.axisData.yLabelsL2Filtered, function (filterSet) {
                                        angular.forEach($scope.axisData.yItems2[$scope.axisData.yLabelsL2Filtered.indexOf(filterSet)].filters, function (filter) {
                                            let filtered = filterSet.filter(function (x) {
                                                if ((filter == "Undefined" && x[$scope.yFields[1]].length == 0) || (x[$scope.yFields[1]].indexOf(filter) > -1) || $scope.yFieldTypes[1] == 3 && formatDateLabel(1, "y", x[$scope.yFields[1]]) == filter) {
                                                    return true;
                                                }
                                                else {
                                                    return false;
                                                }
                                            });

                                            var topLevelIndex = $scope.axisData.yItems2[$scope.axisData.yLabelsL2Filtered.indexOf(filterSet)].topLevelIndex;
                                            var level1Filter = $scope.axisData.yItems2[$scope.axisData.yLabelsL2Filtered.indexOf(filterSet)].level1Filter;

                                            if (filtered.length > 0 || ShowAllItemsY2 == 1) {
                                                let labelsObjY = getLabelItemsWithIds(filtered, 3, 1);

                                                $scope.axisData.yItems3.push({ topLevelIndex: topLevelIndex, level1Filter: level1Filter, level2Filter: filter, labels: labelsObjY.yItems, filters: labelsObjY.yItemsFilters, dataType: $scope.yFieldTypes[i] });

                                                $scope.axisData.level3YColumns += labelsObjY.yItemsFilters.length;
                                            }
                                        });
                                    });
                                }
                                break;
                        }
                    }

                    //Lets check unneeded levels x/y
                    let xLevel1ToBeRemoved = [];
                    let yLevel1ToBeRemoved = [];

                    for (let i = 0; i < $scope.axisData.xItems1.filters.length; i++) {

                        let filtered2 = $scope.axisData.xItems2.filter(function (x) { return x.topLevelIndex == i; });
                        let filtered3 = $scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == i; });

                        if (xLevel == 2 && (filtered2.length > 0 && filtered2[0].labels.length == 0 || filtered2.length == 0) && ShowAllItemsX1 == 0 && ShowAllItemsX2 == 0) {
                            xLevel1ToBeRemoved.push(i);
                        }

                        if (xLevel == 3 && (filtered3.length > 0 && filtered3[0].labels.length == 0 || filtered3.length == 0) && ShowAllItemsX2 == 0 && ShowAllItemsX3 == 0) {
                            xLevel1ToBeRemoved.push(i);
                        }
                    }

                    for (let i = 0; i < $scope.axisData.yItems1.filters.length; i++) {
                        let filtered2 = $scope.axisData.yItems2.filter(function (x) { return x.topLevelIndex == i; });
                        let filtered3 = $scope.axisData.yItems3.filter(function (x) { return x.topLevelIndex == i; });


                        if (yLevel == 2 && (filtered2.length > 0 && filtered2[0].labels.length == 0 || filtered2.length == 0) && ShowAllItemsY1 == 0 && ShowAllItemsY2 == 0) {
                            yLevel1ToBeRemoved.push(i);
                        }

                        if (yLevel == 3 && (filtered3.length > 0 && filtered3[0].labels.length == 0 || filtered3.length == 0) && ShowAllItemsY2 == 0 && ShowAllItemsY3 == 0) {
                            yLevel1ToBeRemoved.push(i);
                        }
                    }

                    //Remove unneeded xleve1
                    if (xLevel > 1 && xLevel1ToBeRemoved.length > 0) {
                        xLevel1ToBeRemoved.reverse();
                        angular.forEach(xLevel1ToBeRemoved, function (filter) {
                            $scope.axisData.xItems1.filters.splice(filter, 1);
                            $scope.axisData.xItems1.labels.splice(filter, 1);

                            let removedLevel2Items = $scope.axisData.xItems2.filter(function (x) { return x.topLevelIndex == filter; });
                            angular.forEach(removedLevel2Items.reverse(), function (labelObj) {
                                $scope.axisData.xItems2.splice($scope.axisData.xItems2.indexOf(labelObj), 1);
                            });

                            let removedLevel2Labels = $scope.axisData.level2XLabels.filter(function (x) { return x.topLevelIndex == filter; });
                            angular.forEach(removedLevel2Labels.reverse(), function (labelObj) {
                                $scope.axisData.level2XLabels.splice($scope.axisData.level2XLabels.indexOf(labelObj), 1);
                            });

                            let removedLevel3Labels = $scope.axisData.level3XLabels.filter(function (x) { return x.topLevelIndex == filter; });
                            angular.forEach(removedLevel3Labels.reverse(), function (labelObj) {
                                $scope.axisData.level3XLabels.splice($scope.axisData.level3XLabels.indexOf(labelObj), 1);
                            });

                            let removedLevel3Items = $scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == filter; });
                            angular.forEach(removedLevel3Items.reverse(), function (labelObj) {
                                $scope.axisData.xItems3.splice($scope.axisData.xItems3.indexOf(labelObj), 1);
                            });

                            let filtered2 = $scope.axisData.xItems2.filter(function (x) { return x.topLevelIndex == filter; });
                            let filtered3 = $scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == filter; });

                            if (xLevel == 2 && (filtered2.length > 0 && filtered2[0].labels.length == 0 || filtered2.length == 0)) {
                                angular.forEach($scope.axisData.xItems2, function (xitem2) {
                                    if (xitem2.topLevelIndex > filter) {
                                        xitem2.topLevelIndex = xitem2.topLevelIndex - 1;
                                    }
                                });
                                angular.forEach($scope.axisData.level2XLabels, function (item2) {
                                    if (item2.topLevelIndex > filter) {
                                        item2.topLevelIndex = item2.topLevelIndex - 1;
                                    }
                                });
                            }

                            if (xLevel == 3 && (filtered3.length > 0 && filtered3[0].labels.length == 0 || filtered3.length == 0)) {
                                angular.forEach($scope.axisData.xItems2, function (xitem2) {
                                    if (xitem2.topLevelIndex > filter) {
                                        xitem2.topLevelIndex = xitem2.topLevelIndex - 1;
                                    }
                                });
                                angular.forEach($scope.axisData.level2XLabels, function (item2) {
                                    if (item2.topLevelIndex > filter) {
                                        item2.topLevelIndex = item2.topLevelIndex - 1;
                                    }
                                });
                                angular.forEach($scope.axisData.xItems3, function (xitem3) {
                                    if (xitem3.topLevelIndex > filter) {
                                        xitem3.topLevelIndex = xitem3.topLevelIndex - 1;
                                    }
                                    angular.forEach($scope.axisData.level3XLabels, function (item3) {
                                        if (item3.topLevelIndex > filter) {
                                            item3.topLevelIndex = item3.topLevelIndex - 1;
                                        }
                                    });
                                });
                            }
                        });
                    }
                    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    //let whiteSpaceStr = "      ";
                    let whiteSpaceStr = '\xa0' + '\xa0' + '\xa0' + '\xa0' + '\xa0' + '\xa0' + '\xa0' + '\xa0';
                    let l2SubSumIndexes = [];
                    let l3SubSumIndexes = [];
                    angular.forEach($scope.axisData.yItems1.filters, function (filter) {
                        var level1Index = $scope.axisData.yItems1.filters.indexOf(filter);

                        //calc
                        let valuesXY = [];
                        let sum = 0;
                        let totalCount = 0;
                        let totalIds = [];

                        switch (xLevel) {
                            case 1:
                                angular.forEach($scope.axisData.xItems1.filters, function (filterx) {
                                    let valData = calcValue(1, 1, filterx, filter, "", "", "", "");
                                    let topLevelIndex = $scope.axisData.xItems1.filters.indexOf(filterx);

                                    if (yLevel > 1) {
                                        valuesXY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: true, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                    }
                                    else {
                                        valuesXY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                    }

                                    switch (aggregateType) {
                                        case 0://Sum
                                            sum += valData.originalValue;
                                            break;
                                        case 1://Min
                                            if (sum > valData.value) {
                                                sum = valData.value;
                                            }
                                            break;
                                        case 2://Max
                                            if (sum < valData.value) {
                                                sum = valData.value;
                                            }
                                            break;
                                        case 3://Average
                                            totalCount += valData.count;
                                            sum += valData.originalValue;
                                            break;
                                        case 4://Cumulative
                                            sum += valData.originalValue;
                                            break;
                                    }
                                    angular.forEach(valData.ids, function (id) {
                                        if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                    });
                                });
                                break;
                            case 2:
                                angular.forEach($scope.axisData.xItems2, function (xItem) {
                                    let topLevelIndex = xItem.topLevelIndex;
                                    let subSum = 0;
                                    let subCount = 0;
                                    let subIds = [];

                                    angular.forEach(xItem.filters, function (filterx) {
                                        let valData = calcValue(2, 1, filterx, filter, xItem.level1Filter, "", "", "");
                                        if (filterx in groupXSumValues == false && yLevel == 1) groupXSumValues[filterx] = {sum:0,count:0,ids:[]};
                                        switch (aggregateType) {
                                            case 0://Sum
                                                sum += valData.value;
                                                subSum += valData.value;   
                                                if (yLevel == 1)groupXSumValues[filterx].sum += valData.value; 
                                                break;
                                            case 1://Min
                                                if (sum > valData.value) {
                                                    sum = valData.value;
                                                }
                                                if (subSum > valData.value) {
                                                    subSum = valData.value;
                                                }
                                                if (yLevel == 1 && groupXSumValues[filterx].sum > valData.value) {
                                                    groupXSumValues[filterx].sum = valData.value;
                                                }
                                                break;
                                            case 2://Max
                                                if (sum < valData.value) {
                                                    sum = valData.value;
                                                }
                                                if (subSum < valData.value) {
                                                    subSum = valData.value;
                                                }
                                                if (yLevel == 1 && groupXSumValues[filterx].sum < valData.value) {
                                                    groupXSumValues[filterx].sum = valData.value;
                                                }
                                                break;
                                            case 3://Average
                                                sum += valData.originalValue;
                                                subSum += valData.originalValue;
                                                totalCount += valData.count;
                                                subCount += valData.count;

                                                if (yLevel == 1) groupXSumValues[filterx].sum += valData.value;
                                                if (yLevel == 1)groupXSumValues[filterx].count += 1;
                                                break;
                                        }

                                        angular.forEach(valData.ids, function (id) {
                                            if (yLevel == 1 && groupXSumValues[filterx].ids.indexOf(id) == -1) groupXSumValues[filterx].ids.push(id);
                                        });

                                        angular.forEach(valData.ids, function (id) {
                                            if (subIds.indexOf(id) == -1) subIds.push(id);
                                            if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                        });

                                        if (yLevel > 1) {
                                            valuesXY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: true, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                        }
                                        else {
                                            valuesXY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                        }

                                    });

                                    if (xItem.filters.length > 1) {
                                        if (l2SubSumIndexes.filter(function (x) { return x.index == valuesXY.length; }).length == 0) l2SubSumIndexes.push({ index: valuesXY.length, categoryIndex: $scope.axisData.xItems2.indexOf(xItem), topCategoryIndex: topLevelIndex });
                                        if (subCount == 0 && aggregateType != 3) subCount = 1;
                                        //if (xSum1 == 1) valuesXY.push(subSum / subCount);
                                        if (xSum1 == 1) valuesXY.push({ value: (subSum / subCount), originalValue: subSum, count: subCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: subIds, filterx: 0 });
                                    }

                                });
                                break;
                            case 3:
                                let categorySubSum = 0;
                                let categorySubCount = 0;
                                let lastTopLevelIndex = 0;
                                let lastLevel2CategoryIndex = 0;
                                let categorySubIds = [];
                                angular.forEach($scope.axisData.xItems3, function (xItem) {
                                    let level2CategoryIndex = $scope.axisData.xItems3.indexOf(xItem);
                                    let topLevelIndex = xItem.topLevelIndex;
                                    if (xItem.topLevelIndex != lastTopLevelIndex && xSum1 == 1) {
                                        if ($scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == lastTopLevelIndex; }).length > 1) {
                                            //valuesXY.push(categorySubSum);categorySubCount = 1;
                                            if (aggregateType != 3 && categorySubCount == 0) categorySubCount = 1;
                                            valuesXY.push({ value: categorySubSum, originalValue: categorySubSum, count: categorySubCount, isSum: true, isYSum: false, topLevelIndex: lastTopLevelIndex, ids: categorySubIds, filterx: 0 });
                                            if (l3SubSumIndexes.filter(function (x) { return x.index == valuesXY.length - 1 && x.level == 2; }).length == 0) l3SubSumIndexes.push({ index: valuesXY.length - 1, topCategoryIndex: lastTopLevelIndex, categoryIndex: lastLevel2CategoryIndex, level: 2 });
                                        }
                                        lastTopLevelIndex = xItem.topLevelIndex;
                                        categorySubSum = 0;
                                        categorySubIds = [];
                                    }

                                    let subSum = 0;
                                    let subCount = 0;
                                    let subIds = [];

                                    angular.forEach(xItem.filters, function (filterx) {
                                        let valData = calcValue(3, 1, filterx, filter, xItem.level1Filter, xItem.level2Filter, "", "");
                                        if (filterx in groupXSumValues == false && yLevel == 1) groupXSumValues[filterx] = { sum: 0, count: 0, ids: [] };

                                        switch (aggregateType) {
                                            case 0://Sum
                                                sum += valData.value;
                                                subSum += valData.value;
                                                categorySubSum += valData.value;
                                                if (yLevel == 1) groupXSumValues[filterx].sum += valData.value; 
                                                break;
                                            case 1://Min
                                                if (sum > valData.value) {
                                                    sum = valData.value;
                                                }
                                                if (subSum > valData.value) {
                                                    subSum = valData.value;
                                                }
                                                if (categorySubSum > valData.value) {
                                                    categorySubSum = valData.value;
                                                }
                                                if (yLevel == 1 && groupXSumValues[filterx].sum > valData.value) {
                                                    groupXSumValues[filterx].sum = valData.value;
                                                }
                                                break;
                                            case 2://Max
                                                if (sum < valData.value) {
                                                    sum = valData.value;
                                                }
                                                if (subSum < valData.value) {
                                                    subSum = valData.value;
                                                }
                                                if (categorySubSum < valData.value) {
                                                    categorySubSum = valData.value;
                                                }
                                                if (yLevel == 1 && groupXSumValues[filterx].sum < valData.value) {
                                                    groupXSumValues[filterx].sum = valData.value;
                                                }
                                                break;
                                            case 3://Average
                                                sum += valData.originalValue;
                                                subSum += valData.originalValue;
                                                totalCount += valData.count;
                                                subCount += valData.count;
                                                categorySubSum += valData.originalValue;
                                                categorySubCount += valData.count;

                                                if (yLevel == 1) groupXSumValues[filterx].sum += valData.value;
                                                if (yLevel == 1) groupXSumValues[filterx].count += 1;
                                                break;
                                        }

                                        angular.forEach(valData.ids, function (id) {
                                            if (yLevel == 1 && groupXSumValues[filterx].ids.indexOf(id) == -1) groupXSumValues[filterx].ids.push(id);
                                        });

                                        angular.forEach(valData.ids, function (id) {
                                            if (subIds.indexOf(id) == -1) subIds.push(id);
                                            if (categorySubIds.indexOf(id) == -1) categorySubIds.push(id);
                                            if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                        });
                                        if (yLevel > 1) {
                                            valuesXY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: true, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                        }
                                        else {
                                            valuesXY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                        }
                                    });

                                    if (xItem.labels.length > 1) {
                                        if (l3SubSumIndexes.filter(function (x) { return x.index == valuesXY.length && x.level == 3; }).length == 0) l3SubSumIndexes.push({ index: valuesXY.length, categoryIndex: level2CategoryIndex, level: 3, topCategoryIndex: topLevelIndex });
                                        if (aggregateType != 3 && subCount == 0) subCount = 1;
                                        if (xSum2 == 1) valuesXY.push({ value: (subSum / subCount), originalValue: subSum, count: subCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: subIds, filterx: 0 });
                                    }

                                    if (xSum1 == 1 && $scope.axisData.xItems3.indexOf(xItem) == $scope.axisData.xItems3.length - 1 && $scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == xItem.topLevelIndex; }).length > 1) {
                                        if (aggregateType != 3 && categorySubCount == 0) categorySubCount = 1;
                                        valuesXY.push({ value: (categorySubSum / categorySubCount), originalValue: categorySubSum, count: categorySubCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: categorySubIds, filterx: 0 });
                                        if (l3SubSumIndexes.filter(function (x) { return x.index == valuesXY.length - 1 && x.level == 2; }).length == 0) l3SubSumIndexes.push({ index: valuesXY.length - 1, topCategoryIndex: lastTopLevelIndex, categoryIndex: level2CategoryIndex, level: 2 });
                                    }
                                    lastLevel2CategoryIndex = level2CategoryIndex;
                                });
                                break;
                        }

                        //labels
                        let label1 = "";
                        let label = "";
                        if ($scope.axisData.yItems1.dataType == 5 || $scope.axisData.yItems1.dataType == 6) {
                            if (typeof $scope.axisData.yItems1.labels[level1Index] == "string") {
                                label = $scope.axisData.yItems1.labels[level1Index];
                            }
                            else {
                                angular.forEach($scope.y1LabelFields, function (field) {
                                    label += $scope.axisData.yItems1.labels[level1Index][field];

                                    if ($scope.y1LabelFields.indexOf(field) != $scope.y1LabelFields.length - 1) {
                                        label += " ";
                                    }
                                });
                            }
                        }
                        if ($scope.axisData.yItems1.dataType == 3) {
                            label += $scope.axisData.yItems1.labels[level1Index];
                        }

                        label1 = label;

                        if (totalCount == 0) totalCount = 1;
                        $scope.axisData.yLabels.push({ label: label1, level: 1, values: valuesXY, sum: sum / totalCount, totalIds: totalIds });

                        var level2Selection = $scope.axisData.yItems2.filter(function (x) { return x.level1Filter == filter; });
                        if (level2Selection.length > 0) {
                            angular.forEach(level2Selection[0].filters, function (filter2) {
                                var filterIndex = level2Selection[0].filters.indexOf(filter2);
                                var label2 = "";
                                if (level2Selection[0].dataType == 5 || level2Selection[0].dataType == 6) {
                                    let label = "";
                                    if (typeof level2Selection[0].labels[filterIndex] == "string") {
                                        label = level2Selection[0].labels[filterIndex];
                                    }
                                    else {
                                        angular.forEach($scope.y2LabelFields, function (field) {
                                            label += level2Selection[0].labels[filterIndex][field];
                                            if ($scope.y2LabelFields.indexOf(field) != $scope.y2LabelFields.length - 1) {
                                                label += " ";
                                            }
                                        });
                                    }
                                    label2 = whiteSpaceStr + label;
                                }
                                else if (level2Selection[0].dataType == 3) {
                                    label2 += whiteSpaceStr + level2Selection[0].labels[filterIndex];
                                }
                                let values2XY = [];
                                let sum = 0;
                                let totalCount = 0;
                                let totalIds = [];
                                switch (xLevel) {
                                    case 1:
                                        angular.forEach($scope.axisData.xItems1.filters, function (filterx) {
                                            let topLevelIndex = $scope.axisData.xItems1.filters.indexOf(filterx);
                                            let valData = calcValue(1, 2, filterx, filter2, "", "", level2Selection[0].level1Filter, "");

                                            if (yLevel == 3) {
                                                values2XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: true, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                            }
                                            else {
                                                values2XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                            }
                                            
                                            switch (aggregateType) {
                                                case 0://Sum
                                                    sum += valData.originalValue;
                                                    break;
                                                case 1://Min
                                                    if (sum > valData.value) {
                                                        sum = valData.value;
                                                    }
                                                    break;
                                                case 2://Max
                                                    if (sum < valData.value) {
                                                        sum = valData.value;
                                                    }
                                                    break;
                                                case 3://Average
                                                    totalCount += valData.count;
                                                    sum += valData.originalValue;
                                                    break;
                                            }

                                            angular.forEach(valData.ids, function (id) {
                                                if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                            });
                                        });
                                        break;
                                    case 2:
                                        angular.forEach($scope.axisData.xItems2, function (xItem) {
                                            let subSum = 0;
                                            let subCount = 0;
                                            let subIds = [];
                                            let topLevelIndex = xItem.topLevelIndex;
                                            angular.forEach(xItem.filters, function (filterx) {
                                                let valData = calcValue(2, 2, filterx, filter2, xItem.level1Filter, "", level2Selection[0].level1Filter, "");
                                                if (filterx in groupXSumValues == false && yLevel == 2) groupXSumValues[filterx] = { sum: 0, count: 0, ids: [] };

                                                switch (aggregateType) {
                                                    case 0://Sum
                                                        sum += valData.value;
                                                        subSum += valData.value;
                                                        if (yLevel == 2) groupXSumValues[filterx].sum += valData.value; 
                                                        break;
                                                    case 1://Min
                                                        if (sum > valData.value) {
                                                            sum = valData.value;
                                                        }
                                                        if (subSum > valData.value) {
                                                            subSum = valData.value;
                                                        }
                                                        if (yLevel == 2 && groupXSumValues[filterx].sum > valData.value) {
                                                            groupXSumValues[filterx].sum = valData.value;
                                                        }
                                                        break;
                                                    case 2://Max
                                                        if (sum < valData.value) {
                                                            sum = valData.value;
                                                        }
                                                        if (subSum < valData.value) {
                                                            subSum = valData.value;
                                                        }
                                                        if (yLevel == 2 && groupXSumValues[filterx].sum < valData.value) {
                                                            groupXSumValues[filterx].sum = valData.value;
                                                        }
                                                        break;
                                                    case 3://Average
                                                        sum += valData.originalValue;
                                                        subSum += valData.originalValue;
                                                        totalCount += valData.count;
                                                        subCount += valData.count;

                                                        if (yLevel == 2) groupXSumValues[filterx].sum += valData.value;
                                                        if (yLevel == 2) groupXSumValues[filterx].count += 1;
                                                        break;
                                                }

                                                angular.forEach(valData.ids, function (id) {
                                                    if (yLevel == 2 && groupXSumValues[filterx].ids.indexOf(id) == -1) groupXSumValues[filterx].ids.push(id);
                                                });

                                                angular.forEach(valData.ids, function (id) {
                                                    if (subIds.indexOf(id) == -1) subIds.push(id);
                                                    if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                                });

                                                if (yLevel == 3) {
                                                    values2XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: true, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                                }
                                                else {
                                                    values2XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                                }
                                            });

                                            if (xItem.filters.length > 1) {
                                                if (l2SubSumIndexes.filter(function (x) { return x.index == valuesXY.length; }).length == 0) l2SubSumIndexes.push({ index: valuesXY.length, categoryIndex: $scope.axisData.xItems2.indexOf(xItem), topCategoryIndex: topLevelIndex });
                                                if (aggregateType != 3 && subCount == 0) subCount = 1;
                                                if (xSum1 == 1) values2XY.push({ value: (subSum / subCount), originalValue: subSum, count: subCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: subIds, filterx: 0 });
                                            }
                                        });
                                        break;
                                    case 3:
                                        let categorySubSum = 0;
                                        let categorySubCount = 0;
                                        let categoryIds = [];
                                        let lastTopLevelIndex = 0;
                                        let lastLevel2CategoryIndex = 0;
                                        angular.forEach($scope.axisData.xItems3, function (xItem) {
                                            let level2CategoryIndex = $scope.axisData.xItems3.indexOf(xItem);
                                            let topLevelIndex = xItem.topLevelIndex;

                                            if (xItem.topLevelIndex != lastTopLevelIndex && xSum1 == 1) {
                                                if ($scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == lastTopLevelIndex; }).length > 1) {
                                                    values2XY.push({ value: categorySubSum, originalValue: categorySubSum, count: categorySubCount, isSum: true, isYSum: false, topLevelIndex: lastTopLevelIndex, ids: categoryIds, filterx: 0 });
                                                    if (aggregateType != 3 && categorySubCount == 0) categorySubCount = 1;
                                                    if (l3SubSumIndexes.filter(function (x) { return x.index == values2XY.length - 1 && x.level == 2; }).length == 0) l3SubSumIndexes.push({ index: values2XY.length - 1, topCategoryIndex: lastTopLevelIndex, categoryIndex: lastLevel2CategoryIndex, level: 2 });
                                                }
                                                lastTopLevelIndex = xItem.topLevelIndex;
                                                categorySubSum = 0;
                                                categoryIds = [];
                                            }

                                            let subSum = 0;
                                            let subCount = 0;
                                            let subIds = [];
                                            angular.forEach(xItem.filters, function (filterx) {
                                                if (filterx in groupXSumValues == false && yLevel == 2) groupXSumValues[filterx] = { sum: 0, count: 0, ids: [] };

                                                let valData = calcValue(3, 2, filterx, filter2, xItem.level1Filter, xItem.level2Filter, level2Selection[0].level1Filter, "");
                                                switch (aggregateType) {
                                                    case 0://Sum
                                                        sum += valData.value;
                                                        subSum += valData.value;
                                                        categorySubSum += valData.value;
                                                        if (yLevel == 2) groupXSumValues[filterx].sum += valData.value; 
                                                        break;
                                                    case 1://Min
                                                        if (sum > valData.value) {
                                                            sum = valData.value;
                                                        }
                                                        if (subSum > valData.value) {
                                                            subSum = valData.value;
                                                        }
                                                        if (categorySubSum > valData.value) {
                                                            categorySubSum = valData.value;
                                                        }
                                                        if (yLevel == 2 && groupXSumValues[filterx].sum > valData.value) {
                                                            groupXSumValues[filterx].sum = valData.value;
                                                        }
                                                        break;
                                                    case 2://Max
                                                        if (sum < valData.value) {
                                                            sum = valData.value;
                                                        }
                                                        if (subSum < valData.value) {
                                                            subSum = valData.value;
                                                        }
                                                        if (categorySubSum < valData.value) {
                                                            categorySubSum = valData.value;
                                                        }
                                                        if (yLevel == 2 && groupXSumValues[filterx].sum < valData.value) {
                                                            groupXSumValues[filterx].sum = valData.value;
                                                        }
                                                        break;
                                                    case 3://Average
                                                        sum += valData.originalValue;
                                                        subSum += valData.originalValue;
                                                        totalCount += valData.count;
                                                        subCount += valData.count;
                                                        categorySubSum += valData.originalValue;
                                                        categorySubCount += valData.count;

                                                        if (yLevel == 2) groupXSumValues[filterx].sum += valData.value;
                                                        if (yLevel == 2) groupXSumValues[filterx].count += 1;
                                                        break;
                                                }

                                                angular.forEach(valData.ids, function (id) {
                                                    if (yLevel == 2 && groupXSumValues[filterx].ids.indexOf(id) == -1) groupXSumValues[filterx].ids.push(id);
                                                });

                                                angular.forEach(valData.ids, function (id) {
                                                    if (subIds.indexOf(id) == -1) subIds.push(id);
                                                    if (categoryIds.indexOf(id) == -1) categoryIds.push(id);
                                                    if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                                });

                                                if (yLevel == 3) {
                                                    values2XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: true, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                                }
                                                else {
                                                    values2XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx  });
                                                }

                                            });
                                            if (xItem.labels.length > 1) {
                                                if (l3SubSumIndexes.filter(function (x) { return x.index == valuesXY.length && x.level == 3; }).length == 0) l3SubSumIndexes.push({ index: valuesXY.length, categoryIndex: level2CategoryIndex, level: 3, topCategoryIndex: topLevelIndex });
                                                if (aggregateType != 3 && subCount == 0) subCount = 1;
                                                if (xSum2 == 1) values2XY.push({ value: (subSum / subCount), originalValue: subSum, count: subCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: subIds, filterx: 0 });
                                            }

                                            if (xSum1 == 1 && $scope.axisData.xItems3.indexOf(xItem) == $scope.axisData.xItems3.length - 1 && $scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == xItem.topLevelIndex; }).length > 1) {
                                                if (aggregateType != 3 && categorySubCount == 0) categorySubCount = 1;
                                                values2XY.push({ value: (categorySubSum / categorySubCount), originalValue: categorySubSum, count: categorySubCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: categoryIds, filterx: 0  });
                                                if (l3SubSumIndexes.filter(function (x) { return x.index == values2XY.length - 1 && x.level == 2; }).length == 0) l3SubSumIndexes.push({ index: values2XY.length - 1, topCategoryIndex: lastTopLevelIndex, categoryIndex: level2CategoryIndex, level: 2 });
                                            }
                                            lastLevel2CategoryIndex = level2CategoryIndex;
                                        });
                                        break;
                                }

                                if (totalCount == 0) totalCount = 1;

                                $scope.axisData.yLabels.push({ label: label2, level: 2, values: values2XY, sum: sum / totalCount,sums:[], totalIds: totalIds });

                                var level3Selection = $scope.axisData.yItems3.filter(function (x) { return x.level1Filter == filter && x.level2Filter == filter2; });
                                if (level3Selection.length > 0) {
                                    angular.forEach(level3Selection[0].filters, function (filter3) {

                                        var level3Index = level3Selection[0].filters.indexOf(filter3);
                                        var label3 = "";
                                        if (level3Selection[0].dataType == 5 || level3Selection[0].dataType == 6) {
                                            let label = "";
                                            if (typeof level3Selection[0].labels[level3Index] == "string") {
                                                label = level3Selection[0].labels[level3Index];
                                            }
                                            else {
                                                angular.forEach($scope.y3LabelFields, function (field) {
                                                    label += level3Selection[0].labels[level3Index][field];
                                                    if ($scope.y3LabelFields.indexOf(field) != $scope.y3LabelFields.length - 1) {
                                                        label += " ";
                                                    }
                                                });
                                            }
                                            label3 = whiteSpaceStr + whiteSpaceStr + label;
                                        }
                                        if (level3Selection[0].dataType == 3) {
                                            label3 = whiteSpaceStr + whiteSpaceStr + level3Selection[0].labels[level3Index];
                                        }

                                        let values3XY = [];
                                        let sum = 0;
                                        let totalCount = 0;
                                        let totalIds = [];

                                        switch (xLevel) {
                                            case 1:
                                                angular.forEach($scope.axisData.xItems1.filters, function (filterx) {
                                                    let topLevelIndex = $scope.axisData.xItems1.filters.indexOf(filterx);
                                                    let valData = calcValue(1, 3, filterx, filter3, "", "", level3Selection[0].level1Filter, level3Selection[0].level2Filter);
                                                    values3XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx  });

                                                    switch (aggregateType) {
                                                        case 0://Sum
                                                            sum += valData.originalValue;
                                                            break;
                                                        case 1://Min
                                                            if (sum > valData.value) {
                                                                sum = valData.value;
                                                            }
                                                            break;
                                                        case 2://Max
                                                            if (sum < valData.value) {
                                                                sum = valData.value;
                                                            }
                                                            break;
                                                        case 3://Average
                                                            totalCount += valData.count;
                                                            sum += valData.originalValue;
                                                            break;
                                                    }

                                                    angular.forEach(valData.ids, function (id) {
                                                        if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                                    });
                                                });
                                                break;
                                            case 2:
                                                angular.forEach($scope.axisData.xItems2, function (xItem) {
                                                    let topLevelIndex = xItem.topLevelIndex;
                                                    let subSum = 0;
                                                    let subCount = 0;
                                                    let subIds = [];

                                                    angular.forEach(xItem.filters, function (filterx) {
                                                        let valData = calcValue(2, 3, filterx, filter3, xItem.level1Filter, "", level3Selection[0].level1Filter, level3Selection[0].level2Filter);
                                                        if (filterx in groupXSumValues == false && yLevel == 3) groupXSumValues[filterx] = { sum: 0, count: 0, ids: [] };

                                                        switch (aggregateType) {
                                                            case 0://Sum
                                                                sum += valData.value;
                                                                subSum += valData.value;

                                                                if (yLevel == 3) groupXSumValues[filterx].sum += valData.value; 
                                                                break;
                                                            case 1://Min
                                                                if (sum > valData.value) {
                                                                    sum = valData.value;
                                                                }
                                                                if (subSum > valData.value) {
                                                                    subSum = valData.value;
                                                                }
                                                                if (yLevel == 3 && groupXSumValues[filterx].sum > valData.value) {
                                                                    groupXSumValues[filterx].sum = valData.value;
                                                                }
                                                                break;
                                                            case 2://Max
                                                                if (sum < valData.value) {
                                                                    sum = valData.value;
                                                                }
                                                                if (subSum < valData.value) {
                                                                    subSum = valData.value;
                                                                }
                                                                if (yLevel == 3 && groupXSumValues[filterx].sum < valData.value) {
                                                                    groupXSumValues[filterx].sum = valData.value;
                                                                }
                                                                break;
                                                            case 3://Average
                                                                sum += valData.originalValue;
                                                                subSum += valData.originalValue;
                                                                totalCount += valData.count;
                                                                subCount += valData.count;

                                                                if (yLevel == 3) groupXSumValues[filterx].sum += valData.value;
                                                                if (yLevel == 3) groupXSumValues[filterx].count += 1;
                                                                break;
                                                        }

                                                        angular.forEach(valData.ids, function (id) {
                                                            if (yLevel == 3 && groupXSumValues[filterx].ids.indexOf(id) == -1) groupXSumValues[filterx].ids.push(id);
                                                        });

                                                        angular.forEach(valData.ids, function (id) {
                                                            if (subIds.indexOf(id) == -1) subIds.push(id);
                                                            if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                                        });

                                                        values3XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx });
                                                    });

                                                    if (xItem.filters.length > 1) {
                                                        if (l2SubSumIndexes.filter(function (x) { return x.index == values3XY.length; }).length == 0) l2SubSumIndexes.push({ index: values3XY.length, categoryIndex: $scope.axisData.xItems2.indexOf(xItem), topCategoryIndex: topLevelIndex });
                                                        if (aggregateType != 3 && subCount == 0) subCount = 1;
                                                        if (xSum1 == 1) values3XY.push({ value: (subSum / subCount), originalValue: subSum, count: subCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: subIds, filterx: 0 });
                                                    }
                                                });
                                                break;
                                            case 3:
                                                let categorySubSum = 0;
                                                let categorySubCount = 0;
                                                let categoryIds = [];
                                                let lastTopLevelIndex = 0;
                                                let lastLevel2CategoryIndex = 0;

                                                angular.forEach($scope.axisData.xItems3, function (xItem) {
                                                    let level2CategoryIndex = $scope.axisData.xItems3.indexOf(xItem);
                                                    let topLevelIndex = xItem.topLevelIndex;

                                                    if (xItem.topLevelIndex != lastTopLevelIndex && xSum1 == 1) {
                                                        if ($scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == lastTopLevelIndex; }).length > 1) {
                                                            if (aggregateType != 3 && categorySubCount == 0) categorySubCount = 1;
                                                            values3XY.push({ value: categorySubSum, originalValue: categorySubSum, count: categorySubCount, isSum: true, isYSum: false, topLevelIndex: lastTopLevelIndex, ids: categoryIds, filterx: 0  });
                                                            if (l3SubSumIndexes.filter(function (x) { return x.index == values3XY.length - 1 && x.level == 2; }).length == 0) l3SubSumIndexes.push({ index: values3XY.length - 1, topCategoryIndex: lastTopLevelIndex, categoryIndex: lastLevel2CategoryIndex, level: 2 });
                                                        }
                                                        lastTopLevelIndex = xItem.topLevelIndex;
                                                        categorySubSum = 0;
                                                        categoryIds = [];
                                                    }

                                                    let subSum = 0;
                                                    let subCount = 0;
                                                    let subIds = [];

                                                    angular.forEach(xItem.filters, function (filterx) {
                                                        let valData = calcValue(3, 3, filterx, filter3, xItem.level1Filter, xItem.level2Filter, level3Selection[0].level1Filter, level3Selection[0].level2Filter);
                                                        if (filterx in groupXSumValues == false && yLevel == 3) groupXSumValues[filterx] = { sum: 0, count: 0, ids: [] };

                                                        switch (aggregateType) {
                                                            case 0://Sum
                                                                sum += valData.value;
                                                                subSum += valData.value;
                                                                categorySubSum += valData.value;

                                                                groupXSumValues[filterx].sum += valData.value;
                                                                break;
                                                            case 1://Min
                                                                if (sum > valData.value) {
                                                                    sum = valData.value;
                                                                }
                                                                if (subSum > valData.value) {
                                                                    subSum = valData.value;
                                                                }
                                                                if (categorySubSum > valData.value) {
                                                                    categorySubSum = valData.value;
                                                                }
                                                                if (yLevel == 3 && groupXSumValues[filterx].sum > valData.value) {
                                                                    groupXSumValues[filterx].sum = valData.value;
                                                                }
                                                                break;
                                                            case 2://Max
                                                                if (sum < valData.value) {
                                                                    sum = valData.value;
                                                                }
                                                                if (subSum < valData.value) {
                                                                    subSum = valData.value;
                                                                }
                                                                if (categorySubSum < valData.value) {
                                                                    categorySubSum = valData.value;
                                                                }
                                                                if (yLevel == 3 && groupXSumValues[filterx].sum < valData.value) {
                                                                    groupXSumValues[filterx].sum = valData.value;
                                                                }
                                                                break;
                                                            case 3://Average
                                                                sum += valData.originalValue;
                                                                subSum += valData.originalValue;
                                                                totalCount += valData.count;
                                                                subCount += valData.count;
                                                                categorySubSum += valData.originalValue;
                                                                categorySubCount += valData.count;

                                                                if (yLevel == 3) groupXSumValues[filterx].sum += valData.value;
                                                                if (yLevel == 3) groupXSumValues[filterx].count += 1;
                                                                break;
                                                        }

                                                        angular.forEach(valData.ids, function (id) {
                                                            if (yLevel == 3 && groupXSumValues[filterx].ids.indexOf(id) == -1) groupXSumValues[filterx].ids.push(id);
                                                        });

                                                        angular.forEach(valData.ids, function (id) {
                                                            if (subIds.indexOf(id) == -1) subIds.push(id);
                                                            if (categoryIds.indexOf(id) == -1) categoryIds.push(id);
                                                            if (totalIds.indexOf(id) == -1) totalIds.push(id);
                                                        });
                                                        values3XY.push({ value: valData.value, originalValue: valData.originalValue, count: valData.count, isSum: false, isYSum: false, topLevelIndex: topLevelIndex, ids: valData.ids, filterx: filterx  });
                                                    });

                                                    if (xItem.labels.length > 1) {
                                                        if (l3SubSumIndexes.filter(function (x) { return x.index == values3XY.length && x.level == 3; }).length == 0) l3SubSumIndexes.push({ index: values3XY.length, categoryIndex: level2CategoryIndex, level: 3, topCategoryIndex: topLevelIndex });
                                                        if (aggregateType != 3 && subCount == 0) subCount = 1;
                                                        if (xSum2 == 1) values3XY.push({ value: (subSum / subCount), originalValue: subSum, count: subCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: subIds });
                                                    }

                                                    if (xSum1 == 1 && $scope.axisData.xItems3.indexOf(xItem) == $scope.axisData.xItems3.length - 1 && $scope.axisData.xItems3.filter(function (x) { return x.topLevelIndex == xItem.topLevelIndex; }).length > 1) {
                                                        if (aggregateType != 3 && categorySubCount == 0) categorySubCount = 1;
                                                        values3XY.push({ value: (categorySubSum / categorySubCount), originalValue: categorySubSum, count: categorySubCount, isSum: true, isYSum: false, topLevelIndex: topLevelIndex, ids: categoryIds, filterx: 0  });
                                                        if (l3SubSumIndexes.filter(function (x) { return x.index == values3XY.length - 1 && x.level == 2; }).length == 0) l3SubSumIndexes.push({ index: values3XY.length - 1, topCategoryIndex: lastTopLevelIndex, categoryIndex: level2CategoryIndex, level: 2 });
                                                    }
                                                    lastLevel2CategoryIndex = level2CategoryIndex;
                                                });
                                                break;
                                        }

                                        if (totalCount == 0) totalCount = 1;
                                        $scope.axisData.yLabels.push({ label: label3, level: 3, values: values3XY, sum: sum / totalCount, sums: [], totalIds: totalIds });
                                    });
                                }
                            });

                        }                   
                    });

                    //Lets add sub sum title columns
                    if (xSum1 == 1 && xLevel == 2) {
                        angular.forEach(l2SubSumIndexes, function (indexes) {
                            if ($scope.axisData.xItems2[indexes.topCategoryIndex].labels.filter(function (x) { return x == $scope.sumName }).length == 0) {
                                $scope.axisData.level2XLabels.splice(indexes.index, 0, { item: $scope.sumName, topLevelIndex: indexes.topCategoryIndex });
                                $scope.axisData.level2XColumns += 1;
                                $scope.axisData.xItems2[indexes.categoryIndex].labels.push("Sum");
                            }
                        });
                    }

                    //level2Sum
                    if (xSum2 == 1 && xLevel == 3) {
                        let loopIndex = 0;
                        angular.forEach(l3SubSumIndexes, function (indexes) {
                            let sumLabel = $scope.sumName;
                            if (indexes.level == 2) {
                                sumLabel = "";
                                let topLevel = indexes.topCategoryIndex;
                                let currentItem = $scope.axisData.xItems3[indexes.categoryIndex];
                                let newL3XItem = { topLevelIndex: topLevel, labels: ["Sum"], xleve3IdentifierIndex: xleve3IdentifierIndex };
                                indexes.categoryIndex += 1;
                                $scope.axisData.xItems3.splice(indexes.categoryIndex + loopIndex, 0, newL3XItem);
                                $scope.axisData.xItems2[indexes.topCategoryIndex].labels.push("Sum");
                                $scope.axisData.level2XLabels.splice(indexes.categoryIndex + loopIndex, 0, { item: $scope.sumName, topLevelIndex: indexes.topCategoryIndex });
                                loopIndex += 1;

                                $scope.axisData.level3XLabels.splice(indexes.index, 0, { item: sumLabel, topLevelIndex: indexes.topCategoryIndex, categoryIndex: indexes.categoryIndex, level: 3, xleve3IdentifierIndex: xleve3IdentifierIndex });
                                $scope.axisData.level3XColumns += 1;
                                xleve3IdentifierIndex += 1;
                            }
                            else {


                                if ($scope.axisData.xItems3[indexes.categoryIndex + loopIndex].labels.indexOf("Sum") == -1 && $scope.axisData.xItems3[indexes.categoryIndex + loopIndex].labels.length > 1) {
                                    $scope.axisData.xItems3[indexes.categoryIndex + loopIndex].labels.push("Sum");

                                    $scope.axisData.level3XLabels.splice(indexes.index, 0, { item: sumLabel, topLevelIndex: indexes.topCategoryIndex, categoryIndex: indexes.categoryIndex, level: 3, xleve3IdentifierIndex: $scope.axisData.xItems3[indexes.categoryIndex + loopIndex].xleve3IdentifierIndex });
                                    $scope.axisData.level3XColumns += 1;
                                }
                            }

                        });


                    }

                    /////////////////////

                    //Remove empty y-axis Undefined rows
                    let yToBeRemoved = [];

                    angular.forEach($scope.axisData.yLabels, function (yObj) {
                        if (typeof yObj.label == "string" && yObj.label.replace(/ /g, '') == Translator.translate('CHART_COMMON_UNDEFINED') && yObj.totalIds.length == 0) {
                            yToBeRemoved.push($scope.axisData.yLabels.indexOf(yObj));
                        }
                    });
                    yToBeRemoved.reverse();
                    angular.forEach(yToBeRemoved, function (index) {
                        $scope.axisData.yLabels.splice(index, 1);
                    });
                    /////////////////////

                    $scope.axisData.xColumns = [];
                    $scope.axisData.numberOfColumns = 0;
                    if ($scope.axisData.level3XColumns > 0) {
                        $scope.axisData.numberOfColumns = $scope.axisData.level3XColumns;
                    }
                    else if ($scope.axisData.level2XColumns > 0) {
                        $scope.axisData.numberOfColumns = $scope.axisData.level2XColumns;
                    }
                    else {
                        $scope.axisData.numberOfColumns = $scope.axisData.level1XColumns;
                    }

                    for (var i = 0; i < $scope.axisData.numberOfColumns; i++) {
                        $scope.axisData.xColumns.push("");
                    }

                    var columnCount = 0;
                    if (xLevel == 1) columnCount = $scope.axisData.xItems1.labels.length;
                    if (xLevel == 2) columnCount = $scope.axisData.level2XLabels.length;
                    if (xLevel == 3) columnCount = $scope.axisData.level3XLabels.length;
                    $scope.yTotalSum = 0;
                    $scope.yTotalIds = [];
                    $scope.yTotalCount = 0;
                    $scope.sumRowValues = [];
                    $scope.sumRowCount = [];
                    $scope.sumRowIds = [];
                    $scope.sumRowTopLevelIndex = [];
                    for (let i = 0; i < columnCount; i++) {
                        $scope.sumRowValues.push(0);
                        $scope.sumRowCount.push(0);
                        $scope.sumRowTopLevelIndex.push();
                        $scope.sumRowIds.push([]);
                    }
                    $scope.sumRowSumIndexes = [];

                    //Bottom sum logic
                    angular.forEach($scope.axisData.yLabels, function (row) {
                        for (let j = 0; j < row.values.length; j++) {
                            if (row.values[j].isSum == true) $scope.sumRowSumIndexes.push(j);
                            let tempIds = [];
                            angular.forEach(row.values[j].ids, function (id) {
                                $scope.sumRowIds[j].push(id);
                                $scope.yTotalIds.push(id);
                            });
                            if (row.level == yLevel) {
                                switch (aggregateType) {
                                    case 0://Sum 
                                        $scope.sumRowValues[j] += row.values[j].originalValue;
                                        $scope.sumRowTopLevelIndex[j] = row.values[j].topLevelIndex;

                                        if (row.values[j].isSum == false) {
                                            $scope.yTotalSum += row.values[j].originalValue;
                                        }
                                        break;
                                    case 1://Min
                                        if ($scope.sumRowValues[j] > row.values[j].originalValue) $scope.sumRowValues[j] = row.values[j].originalValue;
                                        if ($scope.yTotalSum > row.values[j].originalValue) $scope.yTotalSum = row.values[j].originalValue;
                                        $scope.sumRowTopLevelIndex[j] = row.values[j].topLevelIndex;
                                        break;
                                    case 2://Max
                                        if ($scope.sumRowValues[j] < row.values[j].originalValue) $scope.sumRowValues[j] = row.values[j].originalValue;
                                        if ($scope.yTotalSum < row.values[j].originalValue) $scope.yTotalSum = row.values[j].originalValue;
                                        $scope.sumRowTopLevelIndex[j] = row.values[j].topLevelIndex;
                                        break;
                                    case 3://Average        
                                        $scope.sumRowValues[j] += row.values[j].originalValue;
                                        $scope.sumRowCount[j] += row.values[j].count;
                                        $scope.sumRowTopLevelIndex[j] = row.values[j].topLevelIndex;
                                        if (row.values[j].isSum == false) {
                                            $scope.yTotalSum += row.values[j].originalValue;
                                            $scope.yTotalCount += row.values[j].count;
                                        }

                                        break;
                                    case 4://Cumulative
                                        $scope.sumRowValues[j] += row.values[j].originalValue;
                                        $scope.sumRowTopLevelIndex[j] = row.values[j].topLevelIndex;

                                        if (row.values[j].isSum == false) {
                                            $scope.yTotalSum += row.values[j].originalValue;
                                        }
                                        break;
                                }
                            }
                        }
                    });

                    if (aggregateType == 3) {
                        for (let i = 0; i < $scope.sumRowValues.length; i++) {
                            $scope.sumRowValues[i] = $scope.sumRowValues[i] / $scope.sumRowCount[i];
                        }

                        if ($scope.yTotalCount == 0) $scope.yTotalCount = 1;
                        $scope.yTotalSum = ($scope.yTotalSum / $scope.yTotalCount);

                    }

                    let columnAreasToBeRemoved = []; //X-axis
                    angular.forEach($scope.sumRowIds, function (ids) {
                        let undefined = 0;
                        if (xLevel == 1) {
                            undefined = $scope.axisData.xItems1.undefined[$scope.sumRowIds.indexOf(ids)] || 0;
                        }
                        else if (xLevel == 2) {
                            undefined = $scope.axisData.level2XLabels[$scope.sumRowIds.indexOf(ids)].undefined || 0;
                        }
                        else if (xLevel == 3) {
                            undefined = $scope.axisData.level3XLabels[$scope.sumRowIds.indexOf(ids)].undefined || 0;
                        }

                        if (ids.length == 0 && undefined == 1) {
                            columnAreasToBeRemoved.push($scope.sumRowIds.indexOf(ids));
                        }
                    });
                    if (columnAreasToBeRemoved.length > 0) {
                        let endRemover = [];
                        angular.forEach(columnAreasToBeRemoved.reverse(), function (index) {
                            $scope.sumRowIds.splice(index, 1);
                            $scope.sumRowValues.splice(index, 1);
                            $scope.sumRowTopLevelIndex.splice(index, 1);

                            switch (xLevel) {
                                case 1:
                                    $scope.axisData.xItems1.labels.splice(index, 1);
                                    $scope.axisData.xItems1.filters.splice(index, 1);
                                    break;
                                case 2:
                                    $scope.axisData.level2XLabels.splice(index, 1);
                                    $scope.axisData.level2XColumns -= 1;

                                    let count2 = 0;
                                    let level2Index = 0;
                                    angular.forEach($scope.axisData.xItems2, function (item) {
                                        for (let i = 0; i < item.labels.length; i++) {
                                            ;
                                            if (count2 + i == index) {
                                                endRemover.push({ position: level2Index, index: i });
                                                break;
                                            }
                                        }
                                        count2 += item.labels.length;
                                        level2Index += 1;
                                    });
                                    break;
                                case 3:
                                    $scope.axisData.level3XLabels.splice(index, 1);
                                    $scope.axisData.level3XColumns -= 1;

                                    let count3 = 0;
                                    let level3Index = 0;
                                    angular.forEach($scope.axisData.xItems3, function (item) {
                                        for (let i = 0; i < item.labels.length; i++) {
                                            //alert("test");
                                            if (count3 + i == index) {
                                                endRemover.push({ position: level3Index, index: i });
                                                break;
                                            }
                                        }
                                        count3 += item.labels.length;
                                        level3Index += 1;
                                    });
                                    break;
                            }

                            angular.forEach($scope.axisData.yLabels, function (ylabel) {
                                ylabel.values.splice(index, 1);
                            });

                        });

                        let indexCounter = 0;
                        angular.forEach($scope.axisData.xItems3, function (obj) {
                            indexCounter += obj.labels.length;
                        });

                        angular.forEach(endRemover, function (obj) {
                            if (xLevel == 3) {
                                $scope.axisData.xItems3[obj.position].filters.splice(obj.index, 1);
                                $scope.axisData.xItems3[obj.position].labels.splice(obj.index, 1);
                                if ($scope.axisData.xItems3[obj.position].labels.indexOf("Sum") > -1 && $scope.axisData.xItems3[obj.position].labels.length == 2) {
                                    $scope.axisData.xItems3[obj.position].labels.splice($scope.axisData.xItems3[obj.position].labels.indexOf("Sum"), 1);

                                    let level3SumItem = $scope.axisData.level3XLabels.filter(function (x) {
                                        return x.xleve3IdentifierIndex == $scope.axisData.xItems3[obj.position].xleve3IdentifierIndex && x.item == $scope.sumName;
                                    });
                                    if (level3SumItem.length > 0) {
                                        let sumIndex = $scope.axisData.level3XLabels.indexOf(level3SumItem[0]);
                                        $scope.axisData.level3XLabels.splice(sumIndex, 1);
                                        $scope.sumRowIds.splice(sumIndex, 1);
                                        $scope.sumRowValues.splice(sumIndex, 1);
                                        $scope.sumRowTopLevelIndex.splice(sumIndex, 1);
                                        for (let i = 0; i < $scope.axisData.yLabels.length; i++) {
                                            $scope.axisData.yLabels[i].values.splice(sumIndex, 1);
                                        }
                                    }
                                }
                            }
                            else if (xLevel == 2) {
                                $scope.axisData.xItems2[obj.position].filters.splice(obj.index, 1);
                                $scope.axisData.xItems2[obj.position].labels.splice(obj.index, 1);
                            }
                            indexCounter += 1;
                        });
                    }

                    if ($scope.axisData.yLabels.length > 0) {
                        angular.forEach($scope.axisData.yLabels, function (obj) {
                            let yWidth = displayTextWidth(obj.label, "12px \"Roboto\", \"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif");
                            if (parseInt(yWidth) > $scope.maxYLabelWidth) $scope.maxYLabelWidth = parseInt(yWidth);
                        });
                        $scope.maxYLabelWidth = parseInt($scope.maxYLabelWidth + (0.25 * $scope.maxYLabelWidth));
                    }

                    if ($scope.yLevelSumInUse == 2 && ($scope.xFieldTypes[xLevel-1] == 5 || $scope.xFieldTypes[xLevel-1] == 6)) {
                        if (Object.keys(groupXSumValues).length > 0) {
                            let keys = Object.keys(groupXSumValues);

                            let listItemScopeName = "";
                            let itemFields;
                            if (xLevel == 1) {
                                listItemScopeName = $scope.xFields[0] + "_items";
                                itemFields = $scope.x1LabelFields;
                            }
                            if (xLevel == 2) {
                                listItemScopeName = $scope.xFields[1] + "_items";
                                itemFields = $scope.x2LabelFields;
                            }
                            if (xLevel == 3) {
                                listItemScopeName = $scope.xFields[2] + "_items";
                                itemFields = $scope.x3LabelFields;
                            }

                            for (let i = 0; i < $scope.axisData.yLabels.length; i++) {

                                let arr = [];
                                // if ($scope.axisData.yLabels[i].values)

                                for (let j = 0; j < keys.length; j++) {
                                    let sum = 0;
                                    let count = 0;
                                    let ids = [];

                                    let items = $scope.axisData.yLabels[i].values.filter(function (x) {
                                        return x.filterx == keys[j];
                                    });
                                    let finalVal = 0;
                                    if (items.length > 0) {
                                        angular.forEach(items, function (item) {
                                            switch (aggregateType) {
                                                case 0://Sum
                                                    sum += item.originalValue;
                                                    break;
                                                case 1://Min
                                                    if (item.originalValue < sum) {
                                                        sum = item.originalValue;
                                                    }
                                                    break;
                                                case 2://Max
                                                    if (item.originalValue > sum) {
                                                        sum = item.originalValue;
                                                    }
                                                    break;
                                                case 3://Average
                                                    if (item.originalValue != null && item.originalValue > 0) count += item.count;
                                                    break;
                                            }
                                            angular.forEach(item.ids, function (id) {
                                                if (ids.indexOf(id) == -1) ids.push(id);
                                            });
                                        });
                                    }
                                    if (count == 0) count = 1;
                                    arr.push({ value: sum / count, ids: ids });
                                }

                                $scope.groupSum.push(arr);
                            }

                            for (let j = 0; j < keys.length; j++) {
                                let sum = groupXSumValues[keys[j]].sum;
                                let count = groupXSumValues[keys[j]].count;
                                let ids = groupXSumValues[keys[j]].ids;

                                if (count == 0) count = 1;
                                

                                let listItem = $scope.chartData[listItemScopeName].filter(function (x) {
                                    return x.item_id == keys[j];
                                });

                                if (listItem.length > 0) {
                                    let str = "";
                                    angular.forEach(itemFields, function (field) {
                                        str += " " + listItem[0][field];
                                    });
                                    $scope.groupSumNames.push(str.trim());
                                }
                                else {
                                    $scope.groupSumNames.push("");
                                }

                                $scope.groupXSum.push({ value: sum / count, ids: ids });
                            }
                        }
                    }

                    if ($scope.yLevel > 1) {
                        let undefinedTempsToBeRemoved = [];
                        let undefinedTempActive = false;

                        for (let i = 0; i < $scope.axisData.yLabels.length; i++) {
                            let obj = $scope.axisData.yLabels[i];
                            if ($scope.yLevel == 2) {
                                if (obj.label.includes("undefinedTemp")) {
                                    undefinedTempsToBeRemoved.push(i);
                                }
                            }
                            else {
                                if (obj.label.includes("undefinedTemp") && obj.level == 2) {
                                    undefinedTempsToBeRemoved.push(i);
                                    undefinedTempActive = true;
                                }
                                else if (obj.label.includes("undefinedTemp") == false && obj.level == 2) {
                                    undefinedTempActive = false;
                                }

                                if (undefinedTempActive && obj.level == 3) {
                                    undefinedTempsToBeRemoved.push(i);
                                }
                                else if (obj.level == 3 && obj.label.includes("undefinedTemp")) {
                                    undefinedTempsToBeRemoved.push(i);
                                }
                            }
                        }

                        angular.forEach(undefinedTempsToBeRemoved.reverse(), function (index) {
                            $scope.axisData.yLabels.splice(index, 1);
                            $scope.groupSum.splice(index, 1);
                        });
                    }

                    function getLabelItemsWithIds(rawDataFiltered, level, axisNumber) {
                        let tempData = {};

                        tempData["xItems"] = [];
                        tempData["yItems"] = [];
                        tempData["xItemsFilters"] = [];
                        tempData["yItemsFilters"] = [];

                        let index = level - 1;
                        angular.forEach(rawDataFiltered, function (row) {
                            if (level == 1 || axisNumber == 0) {
                                if ($scope.xFieldTypes[index] == 5 || $scope.xFieldTypes[index] == 6) {
                                    if ($scope.xFields[index] != "") {
                                        if (row[$scope.xFields[index]].length == 1 && tempData["xItemsFilters"].indexOf(parseInt(row[$scope.xFields[index]].join())) == -1) {
                                            tempData["xItemsFilters"].push(parseInt(row[$scope.xFields[index]].join()));
                                        }
                                        else {
                                            angular.forEach(row[$scope.xFields[index]], function (id) {
                                                if (tempData["xItemsFilters"].indexOf(id) == -1) {
                                                    tempData["xItemsFilters"].push(id);
                                                }
                                            });
                                        }
                                    }
                                }
                                else if ($scope.xFieldTypes[index] == 3) {//formatDateLabel
                                    if ($scope.xFields[index] != "" && row[$scope.xFields[index]]!=null) {
                                        if (tempData["xItemsFilters"].indexOf(formatDateLabel(index, "x", row[$scope.xFields[index]])) == -1) {
                                            tempData["xItemsFilters"].push(formatDateLabel(index, "x", row[$scope.xFields[index]]));
                                            tempData["xItems"].push(row[$scope.xFields[index]]);
                                        }
                                    }
                                }
                            }

                            if (level == 1 || axisNumber == 1) {
                                if ($scope.yFieldTypes[index] == 5 || $scope.yFieldTypes[index] == 6) {
                                    if ($scope.yFields[index] != "") {
                                        if (row[$scope.yFields[index]].length == 1 && tempData["yItemsFilters"].indexOf(parseInt(row[$scope.yFields[index]].join())) == -1) {
                                            tempData["yItemsFilters"].push(parseInt(row[$scope.yFields[index]].join()));
                                        }
                                        else {
                                            angular.forEach(row[$scope.yFields[index]], function (id) {
                                                if (tempData["yItemsFilters"].indexOf(id) == -1) {
                                                    tempData["yItemsFilters"].push(id);
                                                }
                                            });
                                        }
                                    }
                                }
                                else if ($scope.yFieldTypes[index] == 3) {//formatDateLabel
                                    if ($scope.yFields[index] != ""  && row[$scope.yFields[index]]!=null) {
                                        if (tempData["yItemsFilters"].indexOf(formatDateLabel(index, "y", row[$scope.yFields[index]])) == -1) {
                                            tempData["yItemsFilters"].push(formatDateLabel(index, "y", row[$scope.yFields[index]]));
                                            tempData["yItems"].push(row[$scope.yFields[index]]);
                                        }
                                    }
                                }
                            }
                        });

                        if ($scope.xFieldTypes[index] == 3) {
                            tempData["xItems"].sort(function (a, b) { return new Date(a) - new Date(b); });

                            angular.forEach(tempData["xItems"], function (date) {
                                tempData["xItems"][tempData["xItems"].indexOf(date)] = formatDateLabel(index, "x", date);
                            });
                            tempData["xItemsFilters"] = angular.copy(tempData["xItems"]);
                        }

                        if ($scope.yFieldTypes[index] == 3) {
                            tempData["yItems"].sort(function (a, b) { return new Date(a) - new Date(b); });

                            angular.forEach(tempData["yItems"], function (date) {
                                tempData["yItems"][tempData["yItems"].indexOf(date)] = formatDateLabel(index, "y", date);
                            });
                            tempData["yItemsFilters"] = angular.copy(tempData["yItems"]);
                        }

                        // categories for x/y axis
                        if (level == 1 || axisNumber == 0) {
                            let showAll = pivotCommon["ShowAllItemsX" + level][0] || 0;
                            if ($scope.xFieldTypes[index] == 5 || $scope.xFieldTypes[index] == 6) {
                                let allItems = chartData[$scope.xFields[index] + "_items"];
                                if (allItems != null) {
                                    let ids = tempData["xItemsFilters"].join().split(",").map(Number);
                                    let listItems = allItems.filter(function (x) { return ids.indexOf(x.item_id) > -1; });
                                    if (showAll) listItems = allItems;
                                    let sortName = chartData[$scope.xFields[index] + "_listData"].list_order;
                                    if (sortName == "" || sortName == null) sortName = "order_no";

                                    if ($scope.xFieldTypes[index] == 5) {
                                        angular.forEach(listItems, function (obj) {
                                            let tempSortName = "";

                                            angular.forEach($scope["x" + level + "LabelFields"], function (str) {
                                                tempSortName = tempSortName + obj[str];
                                            });
                                            listItems[listItems.indexOf(obj)].pivotSortName = tempSortName;
                                        });

                                        sortName = "pivotSortName";
                                    }
                                    if (listItems.includes("Undefined")) listItems.splice(listItems.indexOf("Undefined"), 1);
                                    if (sortName == "list_item" && listItems.length > 0 && listItems[0].hasOwnProperty(labelFieldWithLanguage)) sortName = labelFieldWithLanguage;
                                    tempData["xItems"] = listItems.sort(dynamicSort(sortName));
                                }

                                let orderedIds = [];

                                angular.forEach(tempData["xItems"], function (obj) {
                                    orderedIds.push(obj.item_id);
                                });

                                tempData["xItemsFilters"] = orderedIds;
                            }
                            else if ($scope.xFieldTypes[index] == 3) {
                                if (showAll) {
                                    if (tempData["xItems"].length > 0) {
                                        let mode = $scope["xFieldExtras"][index] || 0
                                        let newData = getTimeArrayFromStartToEnd(tempData["xItems"], tempData["xItemsFilters"], mode, "x");
                                        tempData["xItems"] = newData.items;
                                        tempData["xItemsFilters"] = newData.filters;
                                    }
                                }
                            }

                            var showUndefined = $scope.chartConfig.OriginalData.pivotCommon["ShowUndefinedX" + level][0] || 0;
                            if (showUndefined == 1) {
                                tempData["xItemsFilters"].push("Undefined");
                                tempData["xItems"].push(Translator.translate('CHART_COMMON_UNDEFINED'));
                            }
                        }

                        if (level == 1 || axisNumber == 1) {
                            let showAll = pivotCommon["ShowAllItemsY" + level][0] || 0;
                            if ($scope.yFieldTypes[index] == 5 || $scope.yFieldTypes[index] == 6) {
                                let allItems = chartData[$scope.yFields[index] + "_items"];
                                if (allItems != null) {
                                    let ids = tempData["yItemsFilters"].join().split(",").map(Number);
                                    let listItems = allItems.filter(function (x) { return ids.indexOf(x.item_id) > -1; });
                                    if (showAll) listItems = allItems;
                                    let sortName = chartData[$scope.yFields[index] + "_listData"].list_order;
                                    if (sortName == "" || sortName == null) sortName = "order_no";

                                    if ($scope.yFieldTypes[index] == 5) {
                                        angular.forEach(listItems, function (obj) {
                                            let tempSortName = "";
                                            angular.forEach($scope["y" + level + "LabelFields"], function (str) {
                                                tempSortName = tempSortName + obj[str];
                                            });
                                            listItems[listItems.indexOf(obj)].pivotSortName = tempSortName;
                                        });

                                        sortName = "pivotSortName";
                                    }
                                    if (listItems.includes("Undefined")) listItems.splice(listItems.indexOf("Undefined"), 1);
                                    if (sortName == "list_item" && listItems.length > 0 && listItems[0].hasOwnProperty(labelFieldWithLanguage)) sortName = labelFieldWithLanguage;
                                    tempData["yItems"] = listItems.sort(dynamicSort(sortName));
                                }

                                let orderedIds = [];
                                angular.forEach(tempData["yItems"], function (obj) {
                                    orderedIds.push(obj.item_id);
                                });

                                tempData["yItemsFilters"] = orderedIds;
                            }
                            else if ($scope.yFieldTypes[index] == 3) {
                                if (showAll) {
                                    if (tempData["yItems"].length > 0) {
                                        let mode = $scope["yFieldExtras"][index] || 0
                                        let newData = getTimeArrayFromStartToEnd(tempData["yItems"], tempData["yItemsFilters"], mode, "y");
                                        tempData["yItems"] = newData.items;
                                        tempData["yItemsFilters"] = newData.filters;
                                    }
                                }
                            }
                            var showUndefined = $scope.chartConfig.OriginalData.pivotCommon["ShowUndefinedY" + level][0] || 0;
                            if (showUndefined == 1) {
                                tempData["yItemsFilters"].push("Undefined");
                                tempData["yItems"].push(Translator.translate('CHART_COMMON_UNDEFINED'));
                            }
                            else {
                                if (level > 1) {
                                    tempData["yItemsFilters"].push("Undefined");
                                    tempData["yItems"].push("undefinedTemp");
                                }
                            }
                        }
                        return tempData;
                    }

                    function getFilterData(dataArr, type, xFilter, level1XFilter, level2XFilter) {
                        let returnArr = [];

                        switch (type) {
                            case 1:
                                for (let i = 0; i < dataArr.length; i++) {
                                    let x = dataArr[i];
                                    if (xFilter == "Undefined" && x[$scope.xFields[0]].length == 0 || calcCompareByFieldType("x", xLevel, xFilter, x)) returnArr.push(x);
                                }
                                break;
                            case 2:
                                for (let i = 0; i < dataArr.length; i++) {
                                    let x = dataArr[i];
                                    if ((xFilter == "Undefined" && x[$scope.xFields[1]].length == 0 || calcCompareByFieldType("x", xLevel, xFilter, x)) && (level1XFilter == "Undefined" && x[$scope.xFields[0]].length == 0 || calcCompareByFieldType("x", xLevel - 1, level1XFilter, x))) returnArr.push(x);
                                }
                                break;

                            case 3:
                                for (let i = 0; i < dataArr.length; i++) {
                                    let x = dataArr[i];
                                    if ((xFilter == "Undefined" && x[$scope.xFields[2]].length == 0 || calcCompareByFieldType("x", xLevel, xFilter, x)) && (level1XFilter == "Undefined" && x[$scope.xFields[0]].length == 0 || calcCompareByFieldType("x", xLevel - 2, level1XFilter, x)) && (level2XFilter == "Undefined" && x[$scope.xFields[1]].length == 0 || calcCompareByFieldType("x", xLevel - 1, level2XFilter, x))) returnArr.push(x);
                                }
                                break;
                        }

                        return returnArr;
                    }

                    function calcValue(xLevel, yLevel, xFilter, yFilter, level1XFilter, level2XFilter, level1YFilter, level2YFilter) {
                        let idArr = [];
                        let result = [];
                        let calcData = [];

                        switch (yLevel) {
                            case 1:
                                switch (xLevel) {
                                    case 1:
                                        if (xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[xFilter];
                                        }
                                        else {
                                            let tempArr = getFilterData($scope.rawDataFiltered, 1, xFilter, level1XFilter, level2XFilter);
                                            $scope.dataByFilter[xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x))) result.push(x);
                                        }

                                        break;
                                    case 2:
                                        if (level1XFilter + "|" + xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[level1XFilter + "|" + xFilter];
                                        }
                                        else {
                                            let tempArr = [];
                                            if (level1XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter], 2, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else {
                                                tempArr = getFilterData($scope.rawDataFiltered, 2, xFilter, level1XFilter, level2XFilter);
                                            }
                                            $scope.dataByFilter[level1XFilter + "|" + xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x))) result.push(x);
                                        }

                                        break;
                                    case 3:
                                        if (level1XFilter + "|" + level2XFilter + "|" + xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[level1XFilter + "|" + level2XFilter + "|" + xFilter];
                                        }
                                        else {
                                            let tempArr = [];
                                            if (level1XFilter + "|" + level2XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter + "|" + level2XFilter], 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else if (level1XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter], 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else {
                                                tempArr = getFilterData($scope.rawDataFiltered, 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            $scope.dataByFilter[level1XFilter + "|" + level2XFilter + "|" + xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x))) result.push(x);
                                        }
                                        break;
                                }
                                break;
                            case 2:
                                switch (xLevel) {
                                    case 1:
                                        if (xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[xFilter];
                                        }
                                        else {
                                            let tempArr = getFilterData($scope.rawDataFiltered, 1, xFilter, level1XFilter, level2XFilter);
                                            $scope.dataByFilter[xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[1]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x)) && (level1YFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel - 1, level1YFilter, x))) result.push(x);
                                        }
                                        break;
                                    case 2:
                                        if (level1XFilter + "|" + xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[level1XFilter + "|" + xFilter];
                                        }
                                        else {
                                            let tempArr = [];
                                            if (level1XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter], 2, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else {
                                                tempArr = getFilterData($scope.rawDataFiltered, 2, xFilter, level1XFilter, level2XFilter);
                                            }
                                            $scope.dataByFilter[level1XFilter + "|" + xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[1]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x)) && (level1YFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel - 1, level1YFilter, x))) result.push(x);
                                        }
                                        break;
                                    case 3:
                                        if (level1XFilter + "|" + level2XFilter + "|" + xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[level1XFilter + "|" + level2XFilter + "|" + xFilter];
                                        }
                                        else {
                                            let tempArr = [];
                                            if (level1XFilter + "|" + level2XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter + "|" + level2XFilter], 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else if (level1XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter], 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else {
                                                tempArr = getFilterData($scope.rawDataFiltered, 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            $scope.dataByFilter[level1XFilter + "|" + level2XFilter + "|" + xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[1]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x)) && (level1YFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel - 1, level1YFilter, x))) result.push(x);
                                        }
                                        break;
                                }
                                break;
                            case 3:
                                switch (xLevel) {
                                    case 1:
                                        if (xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[xFilter];
                                        }
                                        else {
                                            let tempArr = getFilterData($scope.rawDataFiltered, 1, xFilter, level1XFilter, level2XFilter);
                                            $scope.dataByFilter[xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[2]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x)) && (level1YFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel - 2, level1YFilter, x)) && (level2YFilter == "Undefined" && x[$scope.yFields[1]].length == 0 || calcCompareByFieldType("y", yLevel - 1, level2YFilter, x))) result.push(x);
                                        }
                                        break;
                                    case 2:
                                        if (level1XFilter + "|" + xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[level1XFilter + "|" + xFilter];
                                        }
                                        else {
                                            let tempArr = [];
                                            if (level1XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter], 2, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else {
                                                tempArr = getFilterData($scope.rawDataFiltered, 2, xFilter, level1XFilter, level2XFilter);
                                            }
                                            $scope.dataByFilter[level1XFilter + "|" + xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[2]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x)) && (level1YFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel - 2, level1YFilter, x)) && (level2YFilter == "Undefined" && x[$scope.yFields[1]].length == 0 || calcCompareByFieldType("y", yLevel - 1, level2YFilter, x))) result.push(x);
                                        }
                                        break;
                                    case 3:
                                        if (level1XFilter + "|" + level2XFilter + "|" + xFilter in $scope.dataByFilter) {
                                            calcData = $scope.dataByFilter[level1XFilter + "|" + level2XFilter + "|" + xFilter];
                                        }
                                        else {
                                            let tempArr = [];
                                            if (level1XFilter + "|" + level2XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter + "|" + level2XFilter], 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else if (level1XFilter in $scope.dataByFilter) {
                                                tempArr = getFilterData($scope.dataByFilter[level1XFilter], 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            else {
                                                tempArr = getFilterData($scope.rawDataFiltered, 3, xFilter, level1XFilter, level2XFilter);
                                            }
                                            $scope.dataByFilter[level1XFilter + "|" + level2XFilter + "|" + xFilter] = tempArr;
                                            calcData = tempArr;
                                        }
                                        for (let i = 0; i < calcData.length; i++) {
                                            let x = calcData[i];
                                            if ((yFilter == "Undefined" && x[$scope.yFields[2]].length == 0 || calcCompareByFieldType("y", yLevel, yFilter, x)) && (level1YFilter == "Undefined" && x[$scope.yFields[0]].length == 0 || calcCompareByFieldType("y", yLevel - 2, level1YFilter, x)) && (level2YFilter == "Undefined" && x[$scope.yFields[1]].length == 0 || calcCompareByFieldType("y", yLevel - 1, level2YFilter, x))) result.push(x);
                                        }
                                        break;
                                }
                                break;
                        }


                        let value = 0;
                        let aggregateType = pivotCommon.aggregateType[0] || 0;
                        for (let i = 0; i < result.length; i++) {
                            idArr.push(result[i]["item_id"]);
                            switch (aggregateType) {
                                case 0://Sum
                                    value += result[i][valueField] || 0;
                                    break;
                                case 1://Min
                                    let tempV = result[i][valueField] || 0;
                                    if (value > tempV) value = tempV;
                                    break;
                                case 2://Max
                                    let tempV = result[i][valueField] || 0;
                                    if (value < tempV) value = tempV;
                                    break;
                                case 3://Average
                                    value += result[i][valueField] || 0;
                                    break;
                                case 4://Cumulative
                                    value += result[i][valueField] || 0;
                                    break;
                            }
                        }
                        if (aggregateType == 3) {
                            return { value: value / result.length, originalValue: value, count: result.length, ids: idArr };
                        }
                        else {
                            return { value: value, originalValue: value, count: 1, ids: idArr };
                        }
                    }

                    function calcCompareByFieldType(type, level, filter, dataObj) {
                        //if (filter == "22/2017") alert("test");
                        if ($scope[type + "FieldTypes"][level - 1] == 5 || $scope[type + "FieldTypes"][level - 1] == 6) {
                            if (dataObj[$scope[type + "Fields"][level - 1]].indexOf(filter) > -1) return true;
                        }
                        else if ($scope[type + "FieldTypes"][level - 1] == 3) {
                            if (aggregateType == 4) {
                                let mode = 0;
                                if (pivotCommon.xAxisLevel1Extra != null && pivotCommon.xAxisLevel1Extra.length > 0) {
                                    mode = pivotCommon.xAxisLevel1Extra[0];
                                }

                                let dateVal = new Date(dataObj[$scope[type + "Fields"][level - 1]]);
                                let dateValFormatted = null;

                                let cumulativeEnd = new Date(dataObj[cumulativeField]);
                                let cumulativeFormatted = null;
                                let filterFormatted = null;

                                switch (mode) {
                                    case 0: // Year
                                        dateValFormatted = dateVal.getFullYear();
                                        cumulativeFormatted = cumulativeEnd.getFullYear();
                                        filterFormatted = filter;
                                        break;
                                    case 1: // Quarter
                                        let filterArr = filter.split("/");
                                        dateValFormatted = new Date(dateVal.getFullYear(), getQuaterNumber(dateVal) * 3, 1);
                                        cumulativeFormatted = new Date(cumulativeEnd.getFullYear(), getQuaterNumber(cumulativeEnd) * 3, 1);
                                        filterFormatted = new Date(filterArr[1], filterArr[0] * 3, 1);
                                        break;
                                    case 2: // Month
                                        let filterArr = filter.split("/");
                                        dateValFormatted = new Date(dateVal.getFullYear(), dateVal.getMonth() + 1, 1);
                                        cumulativeFormatted = new Date(cumulativeEnd.getFullYear(), cumulativeEnd.getMonth() + 1, 1);
                                        filterFormatted = new Date(filterArr[1], filterArr[0], 1);
                                        break;
                                    case 3: // Week
                                        let filterArr = filter.split("/");
                                        dateValFormatted = getDateOfISOWeek(getWeekNumber(dateVal), dateVal.getFullYear());
                                        cumulativeFormatted = getDateOfISOWeek(getWeekNumber(cumulativeEnd), cumulativeEnd.getFullYear());
                                        filterFormatted = getDateOfISOWeek(filterArr[0], filterArr[1]);
                                        break;
                                }

                                if (cumulativeField && cumulativeField.length > 0) { //
                                    if (dateValFormatted <= filterFormatted && filterFormatted <= cumulativeFormatted) return true;
                                }
                                else {
                                    if (dateValFormatted <= filterFormatted) {
                                        return true;
                                    }
                                }
                            }
                            else {
                                if (formatDateLabel(level - 1, type, dataObj[$scope[type + "Fields"][level - 1]]) == filter) return true;
                            }
                        }
                        return false;
                    }

                    function dynamicSort(property) {
                        let sortOrder = 1;

                        if (property[0] === "-") {
                            sortOrder = -1;
                            property = property.substr(1);
                        }

                        return function (a, b) {
                            if (sortOrder == -1) {
                                if (typeof b[property] == 'number') {
                                    return b[property] - a[property];
                                }
                                else {
                                    if (property == "order_no") {
                                        if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                                            return -1;
                                        } else if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                                            return 1;
                                        }
                                        return b[property].localeCompare(a[property]);
                                    }
                                    else {
                                        return b[property].localeCompare(a[property]);
                                    }
                                }

                            } else {
                                if (typeof b[property] == 'number') {
                                    return a[property] - b[property];
                                }
                                else {
                                    if (property == "order_no") {
                                        if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                                            return -1;
                                        } else if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                                            return 1;
                                        }
                                        return a[property].localeCompare(b[property]);
                                    }
                                    else {
                                        return a[property].localeCompare(b[property]);
                                    }
                                }
                            }
                        }
                    }
                    function startsWithUppercase(str) {
                        return str.substr(0, 1).match(/[A-Z\u00C0-\u00DC]/);
                    }

                    function formatDateLabel(index, typeStr, dateVal) {
                        let mode = $scope[typeStr + "FieldExtras"][index] || 0;
                        dateVal = new Date(dateVal);
                        let label = "";
                        switch (mode) {
                            case 0: // Year
                                label = dateVal.getFullYear();
                                break;
                            case 1: // Quarter
                                label = getQuaterNumber(dateVal) + "/" + dateVal.getFullYear();
                                break;
                            case 2: // Month
                                if (dateVal.getMonth() + 1 < 10 && typeStr == "y") {
                                    label = "0" + (dateVal.getMonth() + 1).toString() + "/" + dateVal.getFullYear();
                                }
                                else {
                                    label = dateVal.getMonth() + 1 + "/" + dateVal.getFullYear();
                                }
                                break;
                            case 3: // Week
                                if (getWeekNumber(dateVal) < 10 && typeStr == "y") {
                                    label = "0" + getWeekNumber(dateVal).toString() + "/" + dateVal.getFullYear();
                                }
                                else {
                                    label = getWeekNumber(dateVal) + "/" + dateVal.getFullYear();
                                }
                                break;
                        }

                        return label;
                    }

                    function getQuaterNumber(dt) {
                        let quarter = 0;
                        let month = dt.getMonth() + 1;
                        switch (true) {
                            case (month < 4):
                                quarter = 1;
                                break;
                            case (month < 7):
                                quarter = 2;
                                break;
                            case (month < 10):
                                quarter = 3;
                                break;
                            default:
                                quarter = 4;
                                break;
                        }
                        return quarter;
                    }

                    function getDateByQuarterAndYear(q, y) {
                        switch (parseInt(q)) {
                            case 1:
                                return new Date(y, 0, 1);
                                break;
                            case 2:
                                return new Date(y, 3, 1);
                                break;
                            case 3:
                                return new Date(y, 6, 1);
                                break;
                            default:
                                return new Date(y, 9, 1);
                                break;
                        }
                    }

                    function getWeekNumber(dt) {
                        let tdt = new Date(dt.valueOf());
                        let dayn = (dt.getDay() + 6) % 7;
                        tdt.setDate(tdt.getDate() - dayn + 3);
                        let firstThursday = tdt.valueOf();
                        tdt.setMonth(0, 1);
                        if (tdt.getDay() !== 4) {
                            tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);
                        }
                        return 1 + Math.ceil((firstThursday - tdt) / 604800000);
                    }

                    function getDateOfISOWeek(w, y) {
                        var simple = new Date(y, 0, 1 + (w - 1) * 7);
                        var dow = simple.getDay();
                        var ISOweekStart = simple;
                        if (dow <= 4)
                            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
                        else
                            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
                        return ISOweekStart;
                    }

                    function getTimeArrayFromStartToEnd(items, filters, mode, axisType) {
                        let newItemArr = [];
                        let newFilterArr = [];

                        var dateVal = new Date(dateVal);
                        let label = "";

                        let firstVal = items[0];
                        let lastVal = items[items.length - 1];

                        switch (mode) {
                            case 0: // Year
                                if ($scope.maxCumulativeTime != null) {
                                    let tempD = new Date($scope.maxCumulativeTime);
                                    if (tempD.getFullYear() > lastVal) lastVal = tempD.getFullYear();
                                }
                                if (firstVal != lastVal) {
                                    for (let i = 0; i < parseInt(lastVal) - parseInt(firstVal); i++) {
                                        if (newItemArr.indexOf((parseInt(firstVal) + i).toString()) == -1) newItemArr.push((parseInt(firstVal) + i).toString());
                                    }
                                    if (newItemArr.indexOf(lastVal) == -1) newItemArr.push(lastVal);
                                }
                                else {
                                    newItemArr.push(firstVal);
                                }
                                break;
                            case 1: // Quarter                        
                                let first = getDateByQuarterAndYear(firstVal.split("/")[0], firstVal.split("/")[1]);
                                let last = getDateByQuarterAndYear(lastVal.split("/")[0], lastVal.split("/")[1]);

                                if ($scope.maxCumulativeTime != null) {
                                    let tempD = new Date($scope.maxCumulativeTime);
                                    let quarter = getQuaterNumber(tempD);
                                    if (getDateByQuarterAndYear(quarter, tempD.getFullYear()) > last) last = getDateByQuarterAndYear(quarter, tempD.getFullYear());
                                }

                                $scope.temp = first;
                                while ($scope.temp < last) {
                                    if (newItemArr.indexOf(getQuaterNumber($scope.temp) + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(getQuaterNumber($scope.temp) + "/" + $scope.temp.getFullYear());
                                    $scope.temp = new Date($scope.temp.setMonth($scope.temp.getMonth() + 3));
                                }
                                if (newItemArr.indexOf(getQuaterNumber(last) + "/" + last.getFullYear()) == -1) newItemArr.push(getQuaterNumber(last) + "/" + last.getFullYear());
                                $scope.temp = null;
                                break;
                            case 2: // Month
                                let first = new Date(firstVal.split("/")[1], firstVal.split("/")[0], 1);
                                let last = new Date(lastVal.split("/")[1], lastVal.split("/")[0], 1);

                                if ($scope.maxCumulativeTime != null) {
                                    let tempD = new Date(new Date($scope.maxCumulativeTime).getFullYear(), new Date($scope.maxCumulativeTime).getMonth() + 1, 1);
                                    if (tempD>last) last = tempD;
                                }

                                $scope.temp = first; //let/var do not work for this becouse typescript ?????
                                newItemArr.push(($scope.temp.getMonth()) + "/" + $scope.temp.getFullYear());

                                while ($scope.temp < last) {
                                    if (newItemArr.indexOf(($scope.temp.getMonth() + 1) + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(($scope.temp.getMonth() + 1) + "/" + $scope.temp.getFullYear());
                                    $scope.temp = new Date($scope.temp.setMonth($scope.temp.getMonth() + 1));
                                }

                                if (axisType == "y") {
                                    let tempStr = $scope.temp.getMonth().toString();
                                    if (tempStr.length == 1) tempStr = "0" + tempStr;
                                    if (newItemArr.indexOf(tempStr + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(tempStr + "/" + $scope.temp.getFullYear());
                                    while ($scope.temp < last) {
                                        let tempStr2 = ($scope.temp.getMonth() + 1).toString();
                                        if (tempStr2.length == 1) tempStr2 = "0" + tempStr2;
                                        if (newItemArr.indexOf(tempStr2 + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(tempStr2 + "/" + $scope.temp.getFullYear());
                                        $scope.temp = new Date($scope.temp.setMonth($scope.temp.getMonth() + 1));
                                    }
                                }
                                else {
                                    if (newItemArr.indexOf(($scope.temp.getMonth()) + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(($scope.temp.getMonth()) + "/" + $scope.temp.getFullYear());
                                    while ($scope.temp < last) {
                                        if(newItemArr.indexOf(($scope.temp.getMonth() + 1) + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(($scope.temp.getMonth() + 1) + "/" + $scope.temp.getFullYear());
                                        $scope.temp = new Date($scope.temp.setMonth($scope.temp.getMonth() + 1));
                                    }
                                }

                                $scope.temp = null;
                                break;
                            case 3: // Week
                                let first = getDateOfISOWeek(firstVal.split("/")[0], firstVal.split("/")[1]);
                                let last = getDateOfISOWeek(lastVal.split("/")[0], lastVal.split("/")[1]);
                                $scope.temp = first; //let/var do not work for this becouse typescript ?????

                                if ($scope.maxCumulativeTime != null) {
                                    let tempD = new Date($scope.maxCumulativeTime);
                                    let tempDHandled = getDateOfISOWeek(getWeekNumber(tempD), tempD.getFullYear());

                                    if (tempDHandled > last) {
                                        last = tempDHandled;
                                    }
                                }

                                if (axisType == "y") {
                                    while ($scope.temp < last) {
                                        let weekNumber = getWeekNumber($scope.temp).toString();
                                        if (weekNumber.toString().length == 1) weekNumber = "0" + weekNumber;
                                        if (newItemArr.indexOf(weekNumber + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(weekNumber + "/" + $scope.temp.getFullYear());
                                        $scope.temp = new Date($scope.temp.setDate($scope.temp.getDate() + 7));
                                    }
                                    let weekNumber2 = getWeekNumber(last).toString();
                                    if (weekNumber2.toString().length == 1) weekNumber2 = "0" + weekNumber2;
                                    if (newItemArr.indexOf(weekNumber2 + "/" + last.getFullYear()) == -1) newItemArr.push(weekNumber2 + "/" + last.getFullYear());
                                }
                                else {
                                    while ($scope.temp < last) {
                                        if (newItemArr.indexOf(getWeekNumber($scope.temp) + "/" + $scope.temp.getFullYear()) == -1) newItemArr.push(getWeekNumber($scope.temp) + "/" + $scope.temp.getFullYear());
                                        $scope.temp = new Date($scope.temp.setDate($scope.temp.getDate() + 7));
                                    }
                                    if (newItemArr.indexOf(getWeekNumber(last) + "/" + last.getFullYear()) == -1)  newItemArr.push(getWeekNumber(last) + "/" + last.getFullYear());
                                }

                                $scope.temp = null;
                                break;
                        }
                        for (let i = 0; i < newItemArr.length; i++) {
                            var index = items.indexOf(newItemArr[i]);
                            if (index > -1) {
                                newFilterArr.push(filters[index]);
                            }
                            else {
                                newFilterArr.push(newItemArr[i]);
                            }
                        }

                        return { items: newItemArr, filters: newFilterArr };
                    }
                    function displayTextWidth(text, font) {
                        var myCanvas = displayTextWidth.canvas || (displayTextWidth.canvas = document.createElement("canvas"));
                        var context = myCanvas.getContext("2d");
                        context.font = font;

                        var metrics = context.measureText(text);
                        return metrics.width;
                    };
                }
                let refreshLayer = function () {
                    if ($scope.chartConfig && $scope.chartData) {
                        refreshData();
                    }
                }
                $scope.$watch('chartData', refreshLayer);

                $scope.filterChanged = function () {
                    ChartsService.setChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);
                    refreshLayer();
                }
            }
        }
    }
})();
