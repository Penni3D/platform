﻿(function () {
	'use strict';

	angular
		.module('core.graphs',
			[
				'core.graphs.circularProgress',
				'core.graphs.horizontalProgress',
				'core.graphs.horizontalCells',
                'core.graphs.phaseProgress',
                'core.graphs.mixedChart',
                'core.graphs.multiListChart',
                'core.graphs.pivotTable',
				'core.graphs.treeHierarchy',
				'core.graphs.resourceLoadChart',
				'core.graphs.circularVisualization'
			]);

})();