(function () {
    'use strict';
    angular
        .module('core.graphs.circularVisualization', [])
        .directive('circularVisualization', circularVisualizationDirective);

    circularVisualizationDirective.$inject = ['Common', 'ApiService', '$rootScope', 'Translator','ChartsService'];
    function circularVisualizationDirective(Common, ApiService, $rootScope, Translator, ChartsService) {
        return {
            restrict: 'E',
            templateUrl: 'core/ui/graphs/circularVisualization.html',
            replace: false,
            scope: {
                chartData: '=',
                chartColors: '=',
                chartConfig: '=',
                chartOptions: '='
            },
            controller: function ($scope) {
                $scope.filtersVisible = false;
                $scope.quickFilterInUse = false;

                $scope.langKey = $rootScope.clientInformation["locale_id"];
                $scope.filterData = {};

                $scope.rawDataFiltered = [];

                $scope.filterPortfolio = $scope.chartConfig.ProcessPortfolioId;
                if ($scope.chartConfig.OriginalData.main_process_portfolio_id && $scope.chartConfig.OriginalData.main_process_portfolio_id > 0) $scope.filterPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

                $scope.filterKeyParams = {};
                $scope.filterKeyParams.chartId = $scope.chartConfig.OriginalData.chartId;
                $scope.filterKeyParams.linkId = $scope.chartConfig.OriginalData.linkId || 0;
                $scope.filterKeyParams.chartProcess = $scope.chartConfig.OriginalData.chartProcess;

                if ($scope.chartData.inMeta) {
                    $scope.filterKeyParams.type = "meta"
                }
                else if ($rootScope.frontpage) {
                    $scope.filterKeyParams.type = "frontPage"
                }
                else {
                    $scope.filterKeyParams.type = "portfolio"
                }

                $scope.showVisuals = false;
                let refreshData = function () {
                    if (Object.keys($scope.chartData.filterData).length > 0) {
                        $scope.quickFilterInUse = true;
                    }
                    else {
                        $scope.quickFilterInUse = false;
                    }

                    let cf = ChartsService.getChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId);
                    if (cf && $scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) {
                        $scope.filterData = cf;
                    }

                    let quickFiltersInEffect = false;
                    angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
                        if ($scope.filterData[key] != null && $scope.filterData[key].length > 0) quickFiltersInEffect = true;
                    });

                    if (quickFiltersInEffect) {
                        $scope.filterActive = true;
                    }
                    else {
                        $scope.filterActive = false;
                    }

                    $scope.showVisuals = false;
                    let circularConfig = $scope.chartConfig.OriginalData.circularData;
                    let data = $scope.chartData.data;

                    if (data.length > 0 && $scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) { // Lets apply quick filters to rawData
                        let filtersToBeApplied = [];
                        $scope.rawDataFiltered = data;

                        angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
                            if (key in $scope.filterData && $scope.filterData[key].length > 0) filtersToBeApplied.push({ name: key, order: $scope.filterData[key].length, ids: $scope.filterData[key] });
                        });

                        if (filtersToBeApplied.length > 0) {
                            filtersToBeApplied = filtersToBeApplied.sort(dynamicSort("order"));

                            angular.forEach(filtersToBeApplied, function (obj) {
                                $scope.rawDataFiltered = $scope.rawDataFiltered.filter(function (x) {
                                    if (Array.isArray(x[obj["name"]])) {
                                        return x[obj["name"]].some(element => { return obj.ids.includes(element); });
                                    }
                                    else {
                                        return [parseInt(String(x[obj["name"]]).trim())].some(element => {
                                            return obj.ids.includes(element);
                                        });
                                    }

                                });
                            });
                        }

                    }
                    else {
                        $scope.rawDataFiltered = data;
                    }

                    $scope.circularHtml = "";
                    $scope.circularHtml = "";
                    $scope.ulWidth = 125;

                    $scope.isWide = $scope.chartConfig.OriginalData.fullWidth || false;
                    if ($scope.isWide == false) $scope.ulWidth = 110;

                    let elementCount = circularConfig.length;
                    $scope.elementCount = elementCount;
                    $scope.circularData = [];
                    $scope.circularUlData = [];

                    //Alignment per ul
                    setAlignment(elementCount, $scope.isWide);

                    $scope.hasData = function (obj) {
                        if ("precision" in obj) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                    let tempdata = [];
                    angular.forEach(circularConfig, function (obj) {
                        let aggregateType = 0;
                        let value1Name = "";
                        let value2Name = "";

                        let title = obj.title[($rootScope.clientInformation["locale_id"])] || "";
                        let suffix = obj.suffix[($rootScope.clientInformation["locale_id"])] || "";
                        let decimals = obj.decimals || 0;

                        let ignoreNulls = obj.ignoreNulls || 0;
                        let ignoreZeros = obj.ignoreZeros || 0;

                        if (obj.aggregateType.length > 0) obj.aggregateType[0] || 0;

                        if (obj.value1.length > 0) value1Name = obj.value1[0];
                        if (obj.value2.length > 0) value2Name = obj.value2[0];

                        let value1 = 0;
                        let value2 = 0;
                        let count = 0;
                        let count2 = 0;

                        angular.forEach($scope.rawDataFiltered, function (row) {
                            if (value1Name != "" && value2Name != "") {

                                switch (aggregateType) {
                                    case 0:
                                        value1 += row[value1Name];
                                        value2 += row[value2Name];
                                        break;
                                    case 1:
                                        if (row[value1Name] < value1) value1 = row[value1Name];
                                        if (row[value2Name] < value2) value2 = row[value2Name];
                                        break;
                                    case 2:
                                        if (row[value1Name] > value1) value1 = row[value1Name];
                                        if (row[value2Name] > value2) value2 = row[value2Name];
                                        break;
                                    case 3:
                                        if ((ignoreNulls == 0 && ignoreZeros == 0) || (ignoreZeros == 1 && (row[value1Name] != null || row[value1Name] != 0) || ignoreZeros == 0 && ignoreNulls == 1 && row[value1Name] != null)) {
                                            value1 += row[value1Name];
                                            count += 1;
                                        }
                                        if ((ignoreNulls == 0 && ignoreZeros == 0) || (ignoreZeros == 1 && (row[value2Name] != null || row[value2Name] != 0) || ignoreZeros == 0 && ignoreNulls == 1 && row[value2Name] != null)) {
                                            value2 += row[value2Name];
                                            count2 += 1;
                                        }

                                        break;
                                }
                            }
                            else if (value1Name != "") {
                                switch (aggregateType) {
                                    case 0:
                                        value1 += row[value1Name];
                                        value2 = value1;
                                        break;
                                    case 1:
                                        if (row[value1Name] < value1) value1 = row[value1Name];
                                        value2 = value1;
                                        break;
                                    case 2:
                                        if (row[value1Name] > value1) value1 = row[value1Name];
                                        value2 = value1;
                                        break;
                                    case 3:
                                        if ((ignoreNulls == 0 && ignoreZeros == 0) || (ignoreZeros == 1 && (row[value1Name] != null || row[value1Name] != 0) || ignoreZeros == 0 && ignoreNulls == 1 && row[value1Name] != null)) {
                                            value1 += row[value1Name];
                                            value2 = value1;
                                            count += 1;
                                        }
                                        break;
                                }
                            }
                        });

                        if (aggregateType == 3) {
                            if (value1Name != "" && value2Name != "") {
                                value1 = value1 / count;
                                value2 = value2 / count2;
                            }
                            else if (value1Name != "") {
                                value1 = value1 / count;
                            }
                        }
                        if (suffix != null && suffix != "") suffix = " " + suffix;

                        //value1 = parseFloat(Common.formatNumber(value1, decimals).replace(/\s/g, "").replace(",","."));

                        tempdata.push({ value1: value1, value2: value2, title: title, suffix: suffix, precision: decimals});               
                    });

                    let rowN = 0;
                    let columnN = 0;

                    for (let i = 0; i < tempdata.length; i++) {
                        let columnsLeft = $scope.circularData[rowN].length - columnN;
                        if (columnsLeft > 0) {
                            $scope.circularData[rowN][columnN] = tempdata[i];
                            columnN += 1;
                        }
                        else {
                            columnN = 0;
                            rowN += 1;
                            let columnsLeft = $scope.circularData[rowN].length - columnN;
                            if (columnsLeft > 0) {
                                $scope.circularData[rowN][columnN] = tempdata[i];
                                columnN += 1;
                            }
                        }
                    }
                    //createHtml();
                    let previousCounter = 0;
                    angular.forEach($scope.circularData, function (row) {
                        $scope.circularUlData.push([]);
                        switch (row.length) {
                            case 1:
                                if (elementCount > 11) {
                                    $scope.circularUlData[$scope.circularUlData.length - 1] = [row[0], {}, {}, {}, {}, {}, {}];
                                }
                                else {
                                    $scope.circularUlData[$scope.circularUlData.length - 1] = [{}, {}, row[0], {}, {}, {}, {}];
                                }
                                break;
                            case 2:
                                if (previousCounter == 4) {
                                    $scope.circularUlData[$scope.circularUlData.length - 1] = [{}, {}, row[0], {}, row[1], {}, {}];
                                }
                                else {
                                    $scope.circularUlData[$scope.circularUlData.length - 1] = [{}, row[0], {}, row[1], {}, {}, {}];
                                }
                                break;
                            case 3:
                                if (previousCounter == 4) {
                                    $scope.circularUlData[$scope.circularUlData.length - 1] = [{}, row[0], {}, row[1], {}, row[2], {}];
                                }
                                else {
                                    $scope.circularUlData[$scope.circularUlData.length - 1] = [row[0], {}, row[1], {}, row[2], {}, {}];
                                }
                                break;
                            case 4:
                                $scope.circularUlData[$scope.circularUlData.length - 1] = [row[0], {}, row[1], {}, row[2], {}, row[3]];
                                break;
                        }
                        previousCounter = row.length;
                    });
                    $scope.showVisuals = true;
                }
                $scope.$watch('chartData', refreshData);

                $scope.filterChanged = function () {
                    ChartsService.setChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);
                    refreshData();
                }

                $scope.formatVal2 = function (val2) {
                    if (val2 == 0) {
                        return 1;
                    }
                    else {
                        return val2;
                    }
                }

                function setAlignment(elementCount,isWide) {
                    if (isWide) {
                        if (elementCount <= 4) {
                            $scope.circularData.push([]);
                            for (let i = 0; i < elementCount; i++) {
                                $scope.circularData[0].push({});
                            }
                        }
                        else if (elementCount < 7) {
                            $scope.circularData.push([{}, {}, {}]);
                            $scope.circularData.push([]);
                            for (let i = 0; i < elementCount - 3; i++) {
                                $scope.circularData[1].push({});
                            }
                        }
                        else if (elementCount < 9) {
                            $scope.circularData.push([{}, {}, {}, {}]);
                            $scope.circularData.push([]);
                            for (let i = 0; i < elementCount - 4; i++) {
                                $scope.circularData[1].push({});
                            }
                        }
                        else if (elementCount == 9) {
                            $scope.circularData.push([{}, {}, {}]);
                            $scope.circularData.push([{}, {}, {}]);
                            $scope.circularData.push([{}, {}, {}]);
                        }
                        else {

                            for (let i = 0; i < Math.floor(elementCount / 4); i++) {
                                $scope.circularData.push([{}, {}, {}, {}]);
                            }
                            if (elementCount - Math.floor(elementCount / 4) * 4 > 0) {
                                $scope.circularData.push([]);
                                for (let i = 0; i < (elementCount - Math.floor(elementCount / 4) * 4); i++) {
                                    $scope.circularData[Math.floor(elementCount / 4)].push({});
                                }
                            }
                        }
                    }
                    else {
                        if (elementCount <= 3) {
                            $scope.circularData.push([]);
                            for (let i = 0; i < elementCount; i++) {
                                $scope.circularData[0].push({});
                            }
                        }
                        else if (elementCount == 4) {
                            $scope.circularData.push([{}, {}]);
                            $scope.circularData.push([{}, {}]);
                        }
                        else {

                            for (let i = 0; i < Math.floor(elementCount / 3); i++) {
                                $scope.circularData.push([{}, {}, {}]);
                            }
                            if (elementCount - Math.floor(elementCount / 3) * 3 > 0) {
                                $scope.circularData.push([]);
                                for (let i = 0; i < (elementCount - Math.floor(elementCount / 3) * 3); i++) {
                                    $scope.circularData[Math.floor(elementCount / 3)].push({});
                                }
                            }
                        }

                    }
                    
                }
                function dynamicSort(property) {
                    let sortOrder = 1;

                    if (property[0] === "-") {
                        sortOrder = -1;
                        property = property.substr(1);
                    }

                    return function (a, b) {
                        if (sortOrder == -1) {
                            if (typeof b[property] == 'number') {
                                return b[property] - a[property];
                            }
                            else {
                                if (property == "order_no") {
                                    if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                                        return -1;
                                    } else if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                                        return 1;
                                    }
                                    return b[property].localeCompare(a[property]);
                                }
                                else {
                                    return b[property].localeCompare(a[property]);
                                }
                            }

                        } else {
                            if (typeof b[property] == 'number') {
                                return a[property] - b[property];
                            }
                            else {
                                if (property == "order_no") {
                                    if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                                        return -1;
                                    } else if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                                        return 1;
                                    }
                                    return a[property].localeCompare(b[property]);
                                }
                                else {
                                    return a[property].localeCompare(b[property]);
                                }
                            }
                        }
                    }
                }
                function startsWithUppercase(str) {
                    return str.substr(0, 1).match(/[A-Z\u00C0-\u00DC]/);
                }
            }
        }
    }
})();
