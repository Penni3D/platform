(function () {
	'use strict';
	angular
		.module('core.graphs.resourceLoadChart', [])
		.directive('resourceLoadChart', resourceLoadChartDirective);

	resourceLoadChartDirective.$inject = ['Common', 'ProcessService', 'ApiService', '$rootScope', 'ChartColourService', 'Translator', 'ViewService', 'Colors', '$sce', 'UniqueService', 'SettingsService','ChartsService'];

	function resourceLoadChartDirective(Common, ProcessService, ApiService, $rootScope, ChartColourService, Translator, ViewService, Colors, $sce, UniqueService, SettingsService, ChartsService) {
		return {
			restrict: 'E',
			templateUrl: 'core/ui/graphs/resourceLoadChart.html',
			replace: false,
			scope: {
				chartData: '=',
				chartColors: '=',
				chartConfig: '=',
				chartOptions: '='
			},
			controller: function ($scope, $element) {
				$scope.filtersVisible = false;
				$scope.quickFilterInUse = false;

				$scope.langKey = $rootScope.clientInformation["locale_id"];

				$scope.filterPortfolio = $scope.chartConfig.ProcessPortfolioId;
				if ($scope.chartConfig.OriginalData.main_process_portfolio_id && $scope.chartConfig.OriginalData.main_process_portfolio_id > 0) $scope.filterPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

				$scope.filterData = {};
				$scope.filterData.yearSelection = [];
				$scope.filterData.secondaryTimeSelection = [];

				//$scope.firstRun = true;
				$scope.reload = false;

				$scope.yearArr = [];
				$scope.timeLevel2 = [];

				$scope.showCostCenterFilter = false;
				$scope.showCompetencyFilter = false;
				$scope.showQuickFilters = false;

				$scope.competencyOptions = angular.copy($scope.chartData.competencies);

				let fieldWithLanguage = String('list_item_' + $rootScope.clientInformation["locale_id"]).toLowerCase().replace("-", "_");
				$scope.costCenterNameField = "list_item";
				if ($scope.chartData.costCenters.length > 0) {
					if (fieldWithLanguage in $scope.chartData.costCenters[0]) $scope.costCenterNameField = fieldWithLanguage;
				}
				$scope.costCenterOptions = angular.copy($scope.chartData.costCenters);

				$scope.secondaryTimeSelectionModify = false;
				if ($scope.chartConfig) {
					$scope.id = UniqueService.get('resource-chart', true);
					let visualProviderColors = Colors.getColors();
					let isWide = $scope.chartConfig.OriginalData.fullWidth || false;
					$scope.height = Common.getCanvasResponsiveSize($scope.height, $scope, $element, isWide);
					$scope.legend = "";
					$scope.generateLegend = function () {
						setTimeout(function () {
							let chart = Common.getChartHandle($scope.id);
							if (chart) $scope.legend = $sce.trustAsHtml(chart.generateLegend());
						}, 0);
					}
					$scope.legendOnClick = function(index) {
						let ci = Common.getChartHandle($scope.id);
						let meta = ci.getDatasetMeta(index);
						meta.hidden = (meta.hidden === null || meta.hidden == false);
						ci.update();
					}

					$scope.filterChanged = function () {
						let filters = $scope.recallInfo.dashboardFilters;

						ChartsService.setChartFilters($scope.chartConfig.ProcessPortfolioId, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);

						let arr = [];
						let id = $scope.filterKeyParams.type + '_' + $scope.filterKeyParams.chartId + '_' + $scope.filterKeyParams.chartProcess + '_' + $scope.filterKeyParams.linkId;
						arr.push({ key: id, data: $scope.filterData });

						if (arr.length > 0) {
							filters["quickFilterPossibilities"] = arr;
						}

						SettingsService.ProcessDashboardsData(
							$scope.recallInfo.process,
							$scope.recallInfo.chartId,
							$scope.recallInfo.searchText,
							'',
							filters)
							.then(function (resp) {
								$scope.chartData = resp.data;
							});
					}

					$scope.filterKeyParams = {};
					$scope.filterKeyParams.chartId = $scope.chartConfig.OriginalData.chartId;
					$scope.filterKeyParams.linkId = $scope.chartConfig.OriginalData.linkId || 0;
					$scope.filterKeyParams.chartProcess = $scope.chartConfig.OriginalData.chartProcess;

					if ($scope.chartData.inMeta) {
						$scope.filterKeyParams.type = "meta"
					}
					else if ($rootScope.frontpage) {
						$scope.filterKeyParams.type = "frontPage"
					}
					else {
						$scope.filterKeyParams.type = "portfolio"
					}

					$scope.runStartFilters = false;

					if ($scope.chartConfig.OriginalData.resourceDataCommon.dateType && $scope.chartConfig.OriginalData.resourceDataCommon.dateType.length>0) {
						switch (parseInt($scope.chartConfig.OriginalData.resourceDataCommon.dateType[0])) {
							case 0:
								$scope.secondaryTimeSelectionTitle = "CHART_MIXED_WEEK";
								break;
							case 1:
								$scope.secondaryTimeSelectionTitle = "CHART_MIXED_MONTH";
								break;
							case 2:
								$scope.secondaryTimeSelectionTitle = "CHART_MIXED_QUARTER";
								break;
							case 3:
								$scope.secondaryTimeSelectionTitle = "CHART_MIXED_YEAR";
								break;
						}
					}

					let refreshData = function () {
						$scope.showCostCenterFilter = false;
						$scope.showCompetencyFilter = false;
						$scope.showQuickFilters = false;

						let originalData = $scope.chartConfig.OriginalData;
						let resourceDataCommon = originalData.resourceDataCommon;
						$scope.zoomInUse = resourceDataCommon.zoom || 0;

						if (resourceDataCommon.showCostCenters && resourceDataCommon.showCostCenters[0] == 1) $scope.showCostCenterFilter = true;
						if (resourceDataCommon.showCompetencies && resourceDataCommon.showCompetencies[0] == 1) $scope.showCompetencyFilter = true;
						if ($scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) $scope.showQuickFilters = true;


						let chartData = $scope.chartData.Data;
						/*if ($scope.firstRun == false && $scope.reload == false) {*/
						if ($scope.reload == false && ($scope.showCostCenterFilter || $scope.showCompetencyFilter)) {
							let cf = ChartsService.getChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId);
							if (cf) {							
								$scope.filterData = cf;						
							}
							else {
								$scope.filterData.secondaryTimeSelection = [];
								$scope.filterData.yearSelection = [];
								$scope.filterData.competencies = [];
								$scope.filterData.costCenters = [];					
							}
						}
						else if ($scope.reload) {
							$scope.savedFilteringsInUse = true;
							if ($scope.filterData.costCenters.length > 0 || $scope.filterData.competencies.length > 0) {
								$scope.runStartFilters = true;
							}
							$scope.reload = false;
						}

						let quickFiltersInEffect = false;
						angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
							if ($scope.filterData[key] != null && $scope.filterData[key].length > 0) quickFiltersInEffect = true;
						});

						if (quickFiltersInEffect || $scope.filterData.yearSelection && $scope.filterData.yearSelection.length > 0 || $scope.filterData.competencies && $scope.filterData.competencies.length > 0 || $scope.filterData.costCenters && $scope.filterData.costCenters.length>0) {
							$scope.filterActive = true;
						}
						else {
							$scope.filterActive = false;
						}

						let actualType = resourceDataCommon.actualType[0] || 0;
						let plannedType = resourceDataCommon.plannedType[0] || 0;
						let loadType = resourceDataCommon.loadType[0] || 0;

						let capacityType = 0; 
						if (resourceDataCommon.capacityType) capacityType = resourceDataCommon.capacityType[0] || 0;

						if (actualType == 1 || plannedType == 1 || loadType == 1 || capacityType == 1) {
							$scope.chartType = "bar";
						} else {
							$scope.chartType = "line";
						}
						$scope.dateType = resourceDataCommon.dateType[0] || 0;

						$scope.resourceLoadColors = [];

						$scope.showPlanned = resourceDataCommon.showPlanned || 0;
						$scope.showActual = resourceDataCommon.showActual || 0;
						$scope.showLoad = resourceDataCommon.showLoad || 0;
						$scope.showCapacity = resourceDataCommon.showCapacity || 0;

						$scope.resourceLoadData = [];
						$scope.resourceLoadDatasetOverride = [];

						let actuals = [];
						let planned = [];
						let labels = [];
						$scope.actualIds = [];
						$scope.plannedIds = [];
						$scope.loadIds = [];

						let chartProcess = $scope.chartConfig.OriginalData.chartProcess;

						let series = [];
						let load = [];
						$scope.onclickIds = [];

						$scope.subsTimeSelectionCollection = [];
						let yeartemp = [];
						angular.forEach(chartData, function (obj) {
							if (obj.plannedHours != null) {
								planned.push(obj.plannedHours);

								if (obj.plannedHours != 0) {
									if (chartProcess == "user") {
										$scope.plannedIds.push(obj.plannedResources);
									}
									else {
										$scope.plannedIds.push(obj.plannedIds);
									}
								}
								else {
									$scope.plannedIds.push([]);
                                }
							}
							else {
								$scope.plannedIds.push([]);
								planned.push(0);
							}
							if (obj.actualHours != null) {
								actuals.push(obj.actualHours);					

								if (obj.actualHours != 0) {
									if (chartProcess == "user") {
										$scope.actualIds.push(obj.actualResources);
									}
									else {
										$scope.actualIds.push(obj.actualIds);
									}
								}
								else {
									$scope.actualIds.push([]);
								}
							}
							else {
								$scope.actualIds.push([]);
								actuals.push(0);
							}

							if (obj.loadHours != null) {
								load.push(obj.loadHours);

								if (obj.loadHours != 0) {
									if (chartProcess == "user") {
										$scope.loadIds.push(obj.loadResources);
									}
									else {
										$scope.loadIds.push(obj.loadIds);
									}
								}
								else {
									$scope.loadIds.push([]);
								}
							}
							else {
								$scope.loadIds.push([]);
								load.push(0);
							}

							labels.push(obj.timeLabel);
							if (obj.timeLabel.toString().indexOf("/") == -1) {  // Zoom filterings
								if (yeartemp.filter(function (x) { return x.value == obj.timeLabel; }).length == 0) yeartemp.push({ value: obj.timeLabel, name: obj.timeLabel });
							} else {
								if (yeartemp.filter(function (x) { return x.value == obj.timeLabel.split("/")[1]; }).length == 0) yeartemp.push({ value: obj.timeLabel.split("/")[1], name: obj.timeLabel.split("/")[1] });
								//let items = listItemsPerField.filter(function (x) {
								//    return x.field == legendInUse;
								//});
								let olds = $scope.subsTimeSelectionCollection.filter(function (x) {
									return x.parent == obj.timeLabel.split("/")[1];
								});
								let subTemp = [];
								if (olds.length > 0) {
									subTemp = olds[0].subs;
									subTemp.push(obj.timeLabel);
									let oldIndex = $scope.subsTimeSelectionCollection.indexOf(olds[0]);
									$scope.subsTimeSelectionCollection[oldIndex].subs = subTemp;
								} else {
									subTemp.push(obj.timeLabel);
									$scope.subsTimeSelectionCollection.push({
										parent: obj.timeLabel.split("/")[1],
										subs: subTemp
									});
								}

							}
						});

						$scope.yearArr = yeartemp;

						//Zoom filtering------------------------------------------------
						if ($scope.filterData.yearSelection.length > 0 && $scope.zoomInUse && $scope.zoomInUse==1) {
							//let allSubs = [];
							let timeLevel2Temp = [];
							angular.forEach($scope.filterData.yearSelection, function (year) {
								let yearSubs = $scope.subsTimeSelectionCollection.filter(function (x) {
									return x.parent == year;
								});

								if (yearSubs.length > 0) {
									angular.forEach(yearSubs[0].subs, function (subItem) {
										timeLevel2Temp.push(subItem);
									});
								}
							});
							$scope.timeLevel2 = timeLevel2Temp;
							setTimeout(function () { $scope.timeLevel2 = timeLevel2Temp; }, 5);

							if ($scope.dateType == 3) {
								let selected = $scope.filterData.yearSelection.map((x) => x);
								selected.reverse();
								let toBeRemoved = [];

								let labelTemp = labels.map((x) => x);
								labelTemp.reverse();
								angular.forEach(labelTemp, function (label) {
									let index = labels.indexOf(label);
									if ($scope.filterData.yearSelection.indexOf(label) == -1) toBeRemoved.push(index);
								});

								angular.forEach(toBeRemoved, function (index) {
									actuals.splice(index, 1);
									planned.splice(index, 1);
									labels.splice(index, 1);
									$scope.actualIds.splice(index, 1);
									$scope.plannedIds.splice(index, 1);
									$scope.loadIds.splice(index, 1);
									load.splice(index, 1);
								});
							} else if ($scope.filterData.secondaryTimeSelection.length == 0) {
								let notToBeRemovedIndexes = [];
								let toBeRemoved = [];
								let tempArr = $scope.timeLevel2.map((x) => x);
								tempArr.reverse();
								angular.forEach(tempArr, function (label) {
									notToBeRemovedIndexes.push(labels.indexOf(label));
								});

								let labelTemp = labels.map((x) => x);
								labelTemp.reverse();

								angular.forEach(labelTemp, function (label) {
									let index = labels.indexOf(label);
									if (notToBeRemovedIndexes.indexOf(index) == -1) {
										toBeRemoved.push(index);
									}
								});

								labelTemp.reverse();
								angular.forEach(notToBeRemovedIndexes, function (index) {
									$scope.filterData.secondaryTimeSelection.push(labelTemp[index]);
								});
								$scope.filterData.secondaryTimeSelection.reverse();

								angular.forEach(toBeRemoved, function (index) {
									actuals.splice(index, 1);
									planned.splice(index, 1);
									labels.splice(index, 1);
									$scope.actualIds.splice(index, 1);
									$scope.plannedIds.splice(index, 1);
									$scope.loadIds.splice(index, 1);
									load.splice(index, 1);
								});
							} else {
								let notToBeRemovedIndexes = [];
								let toBeRemoved = [];

								let labelTemp = labels.map((x) => x);
								labelTemp.reverse();
								angular.forEach(labelTemp, function (label) {
									let index = labels.indexOf(label);
									if ($scope.filterData.secondaryTimeSelection.indexOf(label) == -1) toBeRemoved.push(index);
								});

								angular.forEach(toBeRemoved, function (index) {
									actuals.splice(index, 1);
									planned.splice(index, 1);
									labels.splice(index, 1);
									$scope.actualIds.splice(index, 1);
									$scope.plannedIds.splice(index, 1);
									$scope.loadIds.splice(index, 1);
									load.splice(index, 1);
								});
							}
						}

						//--------------------------------------------------------------
						let barIndexes = [];

						if ($scope.showActual == 1 && actuals.length > 0) {
							$scope.onclickIds.push($scope.actualIds);
							$scope.resourceLoadData.push(actuals);
							series.push(Translator.translate('CHART_RESOURCE_ACTUAL'));
							if (actualType == 1) {
								barIndexes.push($scope.resourceLoadData.length-1);
								$scope.resourceLoadDatasetOverride.push({ type: "bar", itemIndex: 0 });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.actualColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.actualColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.actualColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["purple"].hex, 100));
								}
							} else {
								$scope.resourceLoadDatasetOverride.push({ type: "line", radius: 0, itemIndex: 0 });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.actualColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.actualColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.actualColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["purple"].hex, 100));
								}
							}
						}
						if ($scope.showPlanned == 1 && planned.length > 0) {
							$scope.onclickIds.push($scope.plannedIds);
							$scope.resourceLoadData.push(planned);
							series.push(Translator.translate('CHART_RESOURCE_PLANNED'));
							if (plannedType == 1) {
								barIndexes.push($scope.resourceLoadData.length-1);
								$scope.resourceLoadDatasetOverride.push({ type: "bar", itemIndex: 1 });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.plannedColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.plannedColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.plannedColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["grey"].hex, 100));
								}
							} else {
								$scope.resourceLoadDatasetOverride.push({ type: "line", radius: 0, itemIndex: 1 });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.plannedColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.plannedColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.plannedColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["grey"].hex, 100));
								}
							}
							//visualProviderColors
						}
						if ($scope.showLoad == 1 && load.length > 0) {
							$scope.onclickIds.push($scope.loadIds);
							$scope.resourceLoadData.push(load);
							series.push(Translator.translate('CHART_RESOURCE_LOAD'));
							if (loadType == 1) {
								barIndexes.push($scope.resourceLoadData.length-1);
								$scope.resourceLoadDatasetOverride.push({ type: "bar", itemIndex: 2 });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.loadColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.loadColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.loadColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["orange"].hex, 100));
								}
							} else {
								$scope.resourceLoadDatasetOverride.push({ type: "line", radius: 0, itemIndex: 2 });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.loadColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.loadColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.loadColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["orange"].hex, 100));
								}
							}
						}

						if ($scope.showCapacity && $scope.chartData.Capacity && Object.keys($scope.chartData.Capacity).length > 0) {
							let capacities = [];
							$scope.onclickIds.push([]);

							angular.forEach(labels, function (label) {
								capacities.push($scope.chartData.Capacity[label]);
							});

							$scope.resourceLoadData.push(capacities);
							series.push(Translator.translate('CHART_RESOURCE_CAPACITY'));

							if (capacityType == 1) {
								barIndexes.push($scope.resourceLoadData.length-1);
								$scope.resourceLoadDatasetOverride.push({ type: "bar", itemIndex: 3  });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.capacityColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.capacityColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.capacityColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["orange"].hex, 100));
								}
							} else {
								$scope.resourceLoadDatasetOverride.push({ type: "line", radius: 0 ,itemIndex:3 });
								if ($scope.chartConfig.OriginalData.resourceDataCommon.capacityColor != null && $scope.chartConfig.OriginalData.resourceDataCommon.capacityColor.length > 0) {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors[$scope.chartConfig.OriginalData.resourceDataCommon.capacityColor[0]].hex, 100));
								} else {
									$scope.resourceLoadColors.push(convertHex(visualProviderColors["orange"].hex, 100));
								}
							}
						}

						$scope.resourceLoadLabels = labels;
						$scope.resourceLoadSeries = series;

						if (barIndexes.length > 0 && $scope.resourceLoadData.length > 1) {
							let resourceLoadDataTemp = [];
							let resourceLoadSeriesTemp = [];
							let resourceLoadDatasetOverrideTemp = [];
							let resourceLoadColorsTemp = [];
							angular.forEach(barIndexes.reverse(), function (ix) {
								resourceLoadDataTemp.unshift($scope.resourceLoadData[ix]);
								resourceLoadSeriesTemp.unshift($scope.resourceLoadSeries[ix]);
								resourceLoadDatasetOverrideTemp.unshift($scope.resourceLoadDatasetOverride[ix]);
								resourceLoadColorsTemp.unshift($scope.resourceLoadColors[ix]);

								$scope.resourceLoadData.splice(ix, 1);
								$scope.resourceLoadSeries.splice(ix, 1);
								$scope.resourceLoadDatasetOverride.splice(ix, 1);
								$scope.resourceLoadColors.splice(ix, 1);
							});

							for (let i = 0; i < resourceLoadDataTemp.length; i++) {
								$scope.resourceLoadData.unshift(resourceLoadDataTemp[i]);
								$scope.resourceLoadSeries.unshift(resourceLoadSeriesTemp[i]);
								$scope.resourceLoadDatasetOverride.unshift(resourceLoadDatasetOverrideTemp[i]);
								$scope.resourceLoadColors.unshift(resourceLoadColorsTemp[i]);
                            }
                        }

						let showLegend = false;
						let showLegendValue = resourceDataCommon.showLegend[0] || 0;
						if (showLegendValue == 1) {
							showLegend = true
						}

						let xLabel = "";
						let yLabel = "";
						let xLabelInUse = false;
						let yLabelInUse = false;

						if (originalData.showXAxisLabel == true) {
							xLabel = originalData.xAxisLabel[$rootScope.clientInformation["locale_id"]];
							xLabelInUse = true;
						}
						if (originalData.showYAxisLabel == true) {
							yLabel = originalData.yAxisLabel[$rootScope.clientInformation["locale_id"]];
							yLabelInUse = true;
						}

						$scope.resourceLoadOptions = {
							responsive: true,
							maintainAspectRation: false,
							legendCallback: function (chart) {
								let temp = {datasets: angular.copy(chart.data.datasets) };
								temp.datasets.sort(dynamicSort("itemIndex"));
								return Common.getChartLegend({ data: temp });
							},
							scales: {
								xAxes: [
									{
										id: 'x-axis-1',
										display: true,
										scaleLabel: {
											display: xLabelInUse,
											labelString: xLabel
										},
										ticks: {
											fontFamily: 'Roboto',
											fontSize: 12,
											fontColor: '#676a6e'
										},
										gridLines: {
											display: false,
											drawBorder: false,
											lineWidth: 2,
											color: '#f0f1f4',
											zeroLineWidth: 2,
											zeroLineColor: '#f0f1f4'
										},
									}
								],
								yAxes: [
									{
										id: 'y-axis-1',
										display: true,
										scaleLabel: {
											display: yLabelInUse,
											labelString: yLabel
										},
										ticks: {
											fontFamily: 'Roboto',
											fontSize: 12,
											fontColor: '#676a6e',
											autoSkip: false,
											callback: function (value, index, values) {
												let precision = Number(resourceDataCommon.precision || 0);
												return Common.formatNumber(value, precision);
											}
										},
										gridLines: {
											display: true,
											drawBorder: false,
											lineWidth: 2,
											color: '#f0f1f4',
											zeroLineWidth: 2,
											zeroLineColor: '#f0f1f4'
										},
									}
								]
							},
							tooltips: {
								mode: 'nearest',
								callbacks: {
									label: function (value) {
										let precision = Number(resourceDataCommon.precision || 0);
										let series = $scope.resourceLoadSeries[value.datasetIndex];

										return series + ": " + Common.formatNumber(value.value, precision);
									}
								}
							},
							onClick: function (e) {
								let clickable = $scope.chartConfig.OriginalData.resourceDataCommon.clickable || 0;
								if (clickable == 1) {
									let element = this.getElementAtEvent(e);
									if (element != null && element.length > 0) {
										let ids = [];
										ids = $scope.onclickIds[element[0]._datasetIndex][element[0]._index];
										if (ids != null && ids.length > 0) {

											let drillDownPortfolio = $scope.chartConfig.OriginalData.processPortfolioId;
											let drillDownProcess = $scope.chartConfig.OriginalData.chartProcess;

											if ($scope.chartData.linkIdMap) {
												drillDownProcess = $scope.chartConfig.OriginalData.process;
												drillDownPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;
;
												let linkIdMap = $scope.chartData.linkIdMap;

												let newIds = [];
												angular.forEach(ids, function (id) {
													if (linkIdMap[id] != null) {
														angular.forEach(linkIdMap[id], function (newId) {
															newIds.push(newId);
														});
                                                    }
												});
												ids = newIds;
											}

											ViewService.view('portfolio', {
												params: {
													portfolioId: drillDownPortfolio,
													restrictions: { 0: ids },
													process: drillDownProcess,
													chartForceFlatMode: true,
													titleExtension: $scope.chartOptions.title.text + ": " + $scope.resourceLoadLabels[element[0]._index] + " -> " + $scope.resourceLoadSeries[element[0]._datasetIndex]
												}
											},
												{
													split: true, size: 'normal'
												}
											);
										}
									}
								}
							}
						};
						if (showLegend) $scope.generateLegend();
					}
					let refreshLayer = function () {
						if ($scope.chartConfig && $scope.chartData) {
							refreshData();
						}
					}

					if ($scope.runStartFilters == false) {
						$scope.recallInfo = $scope.chartData.recallInfo;
						$scope.$watch('chartData', refreshLayer);
					}

					$scope.zoomChanged = function (option) {
						if (option == 1) {
							$scope.filterData.secondaryTimeSelection = [];
							$scope.timeLevel2 = [];
						}
						if (option == 2) {
							$scope.timeLevel2 = [];
						}
						ChartsService.setChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);

						refreshLayer();
					}
				}

				function convertHex(hex, opacity) {
					hex = hex.replace('#', '');
					let r = parseInt(hex.substring(0, 2), 16);
					let g = parseInt(hex.substring(2, 4), 16);
					let b = parseInt(hex.substring(4, 6), 16);
					let c = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
					return {
						backgroundColor: c,
						hoverBackgroundColor: c,
						borderColor: c
					};
				}

				function dynamicSort(property) {
					let sortOrder = 1;

					if (property[0] === "-") {
						sortOrder = -1;
						property = property.substr(1);
					}

					return function (a, b) {
						if (sortOrder == -1) {
							if (typeof b[property] == 'number') {
								return b[property] - a[property];
							}
							else {
								if (property == "order_no") {
									if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
										return -1;
									} else if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
										return 1;
									}
									return b[property].localeCompare(a[property]);
								}
								else {
									return b[property].localeCompare(a[property]);
								}
							}

						} else {
							if (typeof b[property] == 'number') {
								return a[property] - b[property];
							}
							else {
								if (property == "order_no") {
									if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
										return -1;
									} else if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
										return 1;
									}
									return a[property].localeCompare(b[property]);
								}
								else {
									return a[property].localeCompare(b[property]);
								}
							}
						}
					}
				}
				function startsWithUppercase(str) {
					return str.substr(0, 1).match(/[A-Z\u00C0-\u00DC]/);
				}
			}
		}
	}
})();