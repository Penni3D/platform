(function () {
	'use strict';
	angular
		.module('core.graphs.circularProgress',[])
		.directive('circularProgress', circularProgressDirective);

	circularProgressDirective.$inject = ['PortfolioService', 'ViewService', '$timeout', 'Common'];
	function circularProgressDirective(PortfolioService, ViewService, $timeout, Common) {
		return {
			restrict: 'E',
			templateUrl: 'core/ui/graphs/circularProgress.html',
			replace: true,
			scope: {
				height: '@',
				title: '@',
				value: '=',
				value2: '=',
				multipleValues: '=',
				colors: '=',
				suffix: '@',
				precision: '@',
                fontSize:'='
			},
			controller: function ($scope, $element) {
				var draw = function () {
					var percent = 100;
					var centerValueText = "";
					var dataset = [
						{sala: "value", value: percent, color: '#FFE97C'},
						{sala: "left", value: 100 - percent, color: '#E0E0E0'}
					];

					let precision = $scope.precision || 0;
					if ($scope.multipleValues == null) {
						centerValueText = Common.formatNumber($scope.value, precision);
					}
					
					if ($scope.multipleValues){
						var colors = $scope.colors;
						var sumOfAll = _.reduce($scope.multipleValues, function (memo, item) {
							return memo + item.value;
						}, 0);
						var values = [];
						dataset = _.map($scope.multipleValues, function (item, ind) {
							values.push(Common.formatNumber(item.value, precision));
							return {
								sala: 'item' + ind,
								value: item.value / sumOfAll,
								color: colors[ind],
								count: item.value,
								onclick: item.onclick
							}
						});
						centerValueText = values.join(', ');
					} else if($scope.value !== $scope.oldValue ||$scope.value2 !== $scope.oldValue2 ) {
						if (!suffix || suffix === "%") {
							suffix = "%";
							if ($scope.value === Infinity || isNaN($scope.value)) $scope.value = 0;
							percent = $scope.value;
						}
						if (angular.isDefined($scope.value2)) {
							percent = 100 * ($scope.value / $scope.value2);
						}
						
						if (suffix === "%" && percent > 100) {
							dataset = [
								{sala: "value", value: (percent - 100), color: 'rgb(244, 67, 53)'},
								{sala: "left", value: 100 - (percent-100), color: '#FFE97C'}
							];							
						} else {
							dataset = [
								{sala: "value", value: percent, color: '#FFE97C'},
								{sala: "left", value: 100 - percent, color: '#E0E0E0'}
							];	
						}

					}
						
					var width = $scope.height;
					var height = $scope.height;
					var radius = Math.min(width, height) / 2;
					var suffix = $scope.suffix;
					

					var svg = d3.select($element[0]).html('');

					svg = d3.select($element[0])
						.append('svg')
						.attr('class', 'circular-chart')
						.attr('width', width)
						.attr('height', height)
						.append('g')
						.attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

					var donutWidth = 8;

					var arc = d3.arc()
						.innerRadius(radius - donutWidth)
						.outerRadius(radius);

					var pie = d3.pie()
						.value(function (d) {
							return d.value;
						})
						.sort(null);

					var path = svg.selectAll('path')
						.data(pie(dataset))
						.enter()
						.append('path')
						.attr('d', arc)
						.attr('fill', function (d, i) {
							return d.data.color;
						});

					if ($scope.multipleValues){
						var itemCount = _.size($scope.multipleValues) - 1;
						var middleIndex = itemCount / 2;
						
						_.each(dataset, function (item, ind) {
							var dx = (ind - middleIndex) * 60;
							svg.append('text').text(item.count)
								.attr('fill', item.color)
								.attr("x", function () {
									return (dx) + 'px';
								})
								.attr("dy", ".05em")
								.attr("text-anchor", "middle")
								.attr('class', 'clickable')
                                .attr('font-size', function () {
                                    if ($scope.fontSize) return $scope.fontSize + "px";
									return (.15 * height) * (1 - this.getComputedTextLength() / width) + 'px';
								}).on("click", function() {
									item.onclick(item);
								});
						});
					} else {
						svg.append("text").text(centerValueText + suffix)
							//.attr('fill', 'black')
							.attr("x", ".15em")
							.attr("dy", ".05em")
							.attr("text-anchor", "middle")
                            .attr('font-size', function () {
                                if ($scope.fontSize) return $scope.fontSize + "px";
								var s = (.28 * height) * (1 - this.getComputedTextLength() / width) + 'px';
								return s;
							});
					}
					svg.append("text").text($scope.title)
						.attr("x", 0)
						.attr("dy", "1.4em")
						.attr("text-anchor", "middle")
						.attr('font-size', (.1 * height) + 'px');
					$scope.oldValue = $scope.value;
					$scope.oldValue2 = $scope.value2;
					
						
					//setTimeout(function(){ draw(); }, 10000);
				};
				//draw();
				$scope.$watch('value + value2', draw);
			}
		};
	}
})();
