(function () {
	'use strict';
	angular
		.module('core.graphs.multiListChart', [])
		.directive('multiListChart', multiListChartDirective);

	multiListChartDirective.$inject = ['Common', 'ProcessService', 'ApiService', '$rootScope', 'ChartColourService', 'Translator', 'ViewService', 'Colors', 'UniqueService', '$sce', 'HTMLService','ChartsService'];

	function multiListChartDirective(Common, ProcessService, ApiService, $rootScope, ChartColourService, Translator, ViewService, Colors, UniqueService, $sce, HTMLService, ChartsService) {
		return {
			restrict: 'E',
			templateUrl: 'core/ui/graphs/multiListChart.html',
			replace: false,
			scope: {
				chartData: '=',
				chartColors: '=',
				chartConfig: '=',
				chartOptions: '='
			},
			controller: function ($scope, $element) {
				$scope.filtersVisible = false;
				$scope.quickFilterInUse = false;
				$scope.langKey = $rootScope.clientInformation["locale_id"];
				$scope.filterData = {};

				$scope.rawDataFiltered = [];

				$scope.filterPortfolio = $scope.chartConfig.ProcessPortfolioId;
				if ($scope.chartConfig.OriginalData.main_process_portfolio_id && $scope.chartConfig.OriginalData.main_process_portfolio_id > 0) $scope.filterPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

				$scope.filterKeyParams = {};
				$scope.filterKeyParams.chartId = $scope.chartConfig.OriginalData.chartId;
				$scope.filterKeyParams.linkId = $scope.chartConfig.OriginalData.linkId || 0;
				$scope.filterKeyParams.chartProcess = $scope.chartConfig.OriginalData.chartProcess;

				if ($scope.chartData.inMeta) {
					$scope.filterKeyParams.type = "meta"
				}
				else if ($rootScope.frontpage) {
					$scope.filterKeyParams.type = "frontPage"
				}
				else {
					$scope.filterKeyParams.type = "portfolio"
				}

				$scope.id = UniqueService.get('multilist-chart', true);
				let isWide = $scope.chartConfig.OriginalData.fullWidth || false;
				$scope.height = Common.getCanvasResponsiveSize($scope.height, $scope, $element, isWide);
				$scope.legend = "";
				$scope.generateLegend = function () {
					setTimeout(function () {
						let chart = Common.getChartHandle($scope.id);
						if (chart) $scope.legend = $sce.trustAsHtml(chart.generateLegend());
					}, 0);
				}
				$scope.legendOnClick = function (index) {
					let ci = Common.getChartHandle($scope.id);
					let meta = ci.getDatasetMeta(index);
					meta.hidden = (meta.hidden === null || meta.hidden == false);
					ci.update();
				}

				if ($scope.chartConfig) {
					let refreshData = function () {
						if (Object.keys($scope.chartData.filterData).length > 0) {
							$scope.quickFilterInUse = true;
						}
						else {
							$scope.quickFilterInUse = false;
						}

						let cf = ChartsService.getChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId);
						if (cf && $scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) {
							$scope.filterData = cf;
						}

						let quickFiltersInEffect = false;
						angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
							if ($scope.filterData[key] != null && $scope.filterData[key].length > 0) quickFiltersInEffect = true;
						});

						if (quickFiltersInEffect) {
							$scope.filterActive = true;
						}
						else {
							$scope.filterActive = false;
						}

						let chartData = $scope.chartData.data;

						if (chartData.length > 0 && $scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) { // Lets apply quick filters to rawData
							let filtersToBeApplied = [];
							$scope.rawDataFiltered = chartData;
							angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
								if (key in $scope.filterData && $scope.filterData[key].length > 0) filtersToBeApplied.push({ name: key, order: $scope.filterData[key].length, ids: $scope.filterData[key] });
							});

							if (filtersToBeApplied.length > 0) {
								filtersToBeApplied = filtersToBeApplied.sort(dynamicSort("order"));

								angular.forEach(filtersToBeApplied, function (obj) {
									$scope.rawDataFiltered = $scope.rawDataFiltered.filter(function (x) {
										if (Array.isArray(x[obj["name"]])) {
											return x[obj["name"]].some(element => { return obj.ids.includes(element); });
										}
										else {
											return [parseInt(String(x[obj["name"]]).trim())].some(element => {
												return obj.ids.includes(element);
											});
                                        }
									});
								});
							}
						}
						else {
							$scope.rawDataFiltered = chartData;
						}

						let originalData = $scope.chartConfig.OriginalData;
						let listBarItems = originalData.multiListData;
						$scope.commonSettings = originalData.multiListDataCommon;

						//Gridline handling
						let hideHorizontalGridLinesVal = $scope.commonSettings.hideHorizontalGridLines && $scope.commonSettings.hideHorizontalGridLines[0] || 0;
						let hideVerticalGridLInesVal = $scope.commonSettings.hideVerticalGridLines && $scope.commonSettings.hideVerticalGridLines[0] || 0;

						let showHorizontalGridLines = true;
						let showVerticalGridLines = true;

						if (hideHorizontalGridLinesVal > 0) showHorizontalGridLines = false;
						if (hideVerticalGridLInesVal > 0) showVerticalGridLines = false;

						let listItemsPerField = $scope.chartData.listItemsPerField;

						let calcItemsCount = Object.keys(listBarItems).length;
						let barItemFieldNames = [];
						let foundIdsPerGroup = new Array(3);
						let sortedArr = new Array(3);

						let fieldWithLanguage = String('list_item_' + $rootScope.clientInformation["locale_id"]).toLowerCase().replace("-", "_");

						$scope.listColorArr = Colors.getColors();
						$scope.colorIndex = 0;
						$scope.colerBarIndexes = new Array(calcItemsCount);
						$scope.defaultColors = ChartColourService.getColours($scope.chartConfig.OriginalData.baseColor);

						$scope.colorArr = Colors.getColors();

						$scope.horizontalBarMode = $scope.commonSettings.horizontalBars[0] || 0;

						if ($scope.horizontalBarMode == 0) {
							$scope.chartType = "bar";
						} else {
							$scope.chartType = "horizontalBar";
						}

						let maxItemCount = 0;


						let dataHandled = [];
						$scope.multiListLabels = [];

						let xLabel = "";
						let yLabel = "";
						let xLabelInUse = false;
						let yLabelInUse = false;

						if (originalData.showXAxisLabel == true) {
							xLabel = originalData.xAxisLabel[$rootScope.clientInformation["locale_id"]];
							xLabelInUse = true;
						}
						if (originalData.showYAxisLabel == true) {
							yLabel = originalData.yAxisLabel[$rootScope.clientInformation["locale_id"]];
							yLabelInUse = true;
						}


						for (let i = 0; i < calcItemsCount; i++) {
							dataHandled[i] = {};
							foundIdsPerGroup[i] = [];
							sortedArr[i] = [];
							$scope.colerBarIndexes[i] = 0;
						}

						angular.forEach(listBarItems, function (item) {
							if (barItemFieldNames.indexOf(item.sourceList) == -1) {
								barItemFieldNames.push(item.sourceList);
								$scope.multiListLabels.push(item.nameField[$rootScope.clientInformation["locale_id"]]);
							}
						});

						angular.forEach($scope.rawDataFiltered , function (item) {
							angular.forEach(barItemFieldNames, function (columnName) {
								let selectedListItem = item[columnName] || "";
								if (Array.isArray(selectedListItem) == false && selectedListItem.length>0) {
									selectedListItem = [selectedListItem];
                                }
								let showUndefined = $scope.commonSettings.showUndefined || 0;
								if (selectedListItem == "" && showUndefined == 1) {

									if (dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"] == null) {
										let newOne = {
											itemNumber: -1,
											clickIds: [item["item_id"]],
											itemCount: 1,
											name: Translator.translate('CHART_COMMON_UNDEFINED'),
											color: {
												backgroundColor: "lightgrey",
												pointBackgroundColor: "lightgrey",
												hoverBackgroundColor: "lightgrey",
												fill: 'origin'
											}
										};

										dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"] = newOne;
										let oldTotal = dataHandled[barItemFieldNames.indexOf(columnName)]["total"] || 0;
										dataHandled[barItemFieldNames.indexOf(columnName)]["total"] = oldTotal + 1;
									} else {
										let categoryObj = dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"];
										categoryObj.clickIds.push(item["item_id"]);
										categoryObj.itemCount += 1;
										dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"] = categoryObj;
										dataHandled[barItemFieldNames.indexOf(columnName)]["total"] += 1;
									}
								} else if (selectedListItem != "") {
									if (selectedListItem.length > 1) {
										angular.forEach(selectedListItem, function (sItem) {
											if (dataHandled[barItemFieldNames.indexOf(columnName)][sItem] == null) {
												let listItem = listItemsPerField.filter(function (x) {
													return Number(x.item_id) == sItem;
												});

												let colorName = ""

												let barItem = listBarItems[barItemFieldNames.indexOf(columnName)];
												let useListColors = barItem.listColors || 0;

												if (useListColors == 1) {
													colorName = listItem[0].list_symbol_fill || "";
												}
												
												let nameStr = listItem[0]["list_item"];
												if (listItem[0].hasOwnProperty(fieldWithLanguage) && listItem[0][fieldWithLanguage] != "" && listItem[0][fieldWithLanguage] != null) {
													nameStr = listItem[0][fieldWithLanguage];
												}

												let newOne = {
													itemNumber: sItem,
													clickIds: [item["item_id"]],
													itemCount: 1 / selectedListItem.length,
													name: nameStr,
													color: getColorByColorName(colorName, barItemFieldNames.indexOf(columnName))
												};

												let categoryId = sItem;

												dataHandled[barItemFieldNames.indexOf(columnName)][categoryId] = newOne;
												if (foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].indexOf(sItem) == -1) {
													foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].push(sItem);
												}

											} else {
												let categoryObj = dataHandled[barItemFieldNames.indexOf(columnName)][sItem];
												categoryObj.clickIds.push(item["item_id"]);
												categoryObj.itemCount += (1 / selectedListItem.length);
												dataHandled[barItemFieldNames.indexOf(columnName)][sItem] = categoryObj;
												if (foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].indexOf(sItem) == -1) {
													foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].push(sItem);
												}
											}
										});
										let oldTotal = dataHandled[barItemFieldNames.indexOf(columnName)]["total"] || 0;
										dataHandled[barItemFieldNames.indexOf(columnName)]["total"] = oldTotal + 1;
									} else {
										if (dataHandled[barItemFieldNames.indexOf(columnName)][selectedListItem[0]] == null) {
											let listItem = listItemsPerField.filter(function (x) {
												if (selectedListItem.length > 0) {
													return Number(x.item_id) == selectedListItem[0];
												} else {
													return Number(x.item_id) == selectedListItem;
												}
											});

											let colorName = ""
											let barItem = listBarItems[barItemFieldNames.indexOf(columnName)];
											let useListColors = barItem.listColors || 0;

											if (useListColors == 1) {
												colorName = listItem[0].list_symbol_fill || "";
											}

											let nameStr = listItem[0]["list_item"];
											if (listItem[0].hasOwnProperty(fieldWithLanguage) && listItem[0][fieldWithLanguage] != "" && listItem[0][fieldWithLanguage] != null) {
												nameStr = listItem[0][fieldWithLanguage];
											}

											let newOne = {
												itemNumber: selectedListItem[0],
												clickIds: [item["item_id"]],
												itemCount: 1,
												name: nameStr,
												color: getColorByColorName(colorName, barItemFieldNames.indexOf(columnName))
											};

											let categoryId = selectedListItem[0];

											dataHandled[barItemFieldNames.indexOf(columnName)][categoryId] = newOne;
											let oldTotal = dataHandled[barItemFieldNames.indexOf(columnName)]["total"] || 0;
											dataHandled[barItemFieldNames.indexOf(columnName)]["total"] = oldTotal + 1;
											if (foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].indexOf(selectedListItem[0]) == -1) {
												foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].push(selectedListItem[0]);
											}
										} else {
											let categoryObj = dataHandled[barItemFieldNames.indexOf(columnName)][selectedListItem[0]];
											categoryObj.clickIds.push(item["item_id"]);
											categoryObj.itemCount += 1;
											dataHandled[barItemFieldNames.indexOf(columnName)][selectedListItem[0]] = categoryObj;
											dataHandled[barItemFieldNames.indexOf(columnName)]["total"] += 1;
											if (foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].indexOf(selectedListItem[0]) == -1) {
												foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].push(selectedListItem[0]);
											}
										}
									}
								}
							});
						});

						for (let i = 0; i < calcItemsCount; i++) {
							let keyLenght = Object.keys(dataHandled[i]).length - 1;
							if (keyLenght > maxItemCount) {
								maxItemCount = keyLenght;
							}
						}

						angular.forEach(barItemFieldNames, function (columnName) {
							let sortColumn = "";
							let items = listItemsPerField.filter(function (x) {
								return x.field == columnName && (foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].indexOf(Number(x.item_id)) > -1 || foundIdsPerGroup[barItemFieldNames.indexOf(columnName)].indexOf(x.item_id) > -1);
							});
							if (items.length) {
								sortColumn = items[0].list_order || "order_no";

								if (sortColumn == "list_item" && items.length>0 && items[0].hasOwnProperty(fieldWithLanguage)) sortColumn = fieldWithLanguage;

								let reversed = $scope.commonSettings.reverseOrder || 0;
								if (($scope.horizontalBarMode == 1 && reversed != 1) || $scope.horizontalBarMode != 1 && reversed == 1) {
									items = items.sort(dynamicSort(sortColumn.toLowerCase())).reverse();
								} else {
									items = items.sort(dynamicSort(sortColumn.toLowerCase()));
								}

								angular.forEach(items, function (item) {
									sortedArr[barItemFieldNames.indexOf(columnName)].push(dataHandled[barItemFieldNames.indexOf(columnName)][item["item_id"]]);
								});

								if (dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"] != null) {
									sortedArr[barItemFieldNames.indexOf(columnName)].push(dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"]);
								}
							} else {
								if (dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"] != null) {
									sortedArr[barItemFieldNames.indexOf(columnName)].push(dataHandled[barItemFieldNames.indexOf(columnName)]["undefined"]);
								}
							}
							sortedArr[barItemFieldNames.indexOf(columnName)].reverse();
						});

						$scope.dataHandled = sortedArr;

						let dataArr = new Array(maxItemCount);
						let loopItemsCount = $scope.chartData.listColumns.length;

						for (let i = 0; i < maxItemCount; i++) {
							//for (let i = 0; i < maxItemCount; i++) {
							dataArr[i] = new Array(maxItemCount);
						}

						for (let i = 0; i < loopItemsCount; i++) {
							//for (let i = 0; i < calcItemsCount; i++) {
							let index = 0;
							angular.forEach(sortedArr[i], function (item) {
								if (Object.keys(item).length > 0) {
									if ($scope.commonSettings.listType == 0) {
										dataArr[index][i] = item.itemCount;
									} else {
										dataArr[index][i] = item.itemCount / dataHandled[i].total * 100;
									}

									index += 1;
								}
							});
						}

						let borderWidth = $scope.commonSettings.borderWidth || 0;
						$scope.multiListDatasetOverride = [];
						for (let j = 0; j < maxItemCount; j++) {
							let tempArr = [];
							angular.forEach(sortedArr, function (item) {
								let item = item[j];
								if (item != null) {
									tempArr.push(item.color.backgroundColor);
								} else {
									tempArr.push("blue");
								}
							});
							$scope.multiListDatasetOverride.push({
								backgroundColor: tempArr,
								borderWidth: borderWidth,
								borderColor: "darkgrey"
							});
						}
						let barPercentage = 0.9; // Default
						if ($scope.multiListLabels.length < 10)
							barPercentage = 0.2 + ((($scope.multiListLabels.length) * 10.0) / 100.0);

						
						$scope.multiListData = dataArr;
						$scope.multiListOptions = {
							responsive: true,
							maintainAspectRation: false,
							legendCallback: function (chart) {
								let ul = HTMLService.initialize('ul');
								let i = 0;


								let legendInUse = $scope.commonSettings.legendList || "";
								if (legendInUse != "") {
									let items = listItemsPerField.filter(function (x) {
										return x.field == legendInUse;
									});
									let sortColumn = items[0].list_order || "order_no";
									let legends = [];
									items = items.sort(dynamicSort(sortColumn.toLowerCase()));

									angular.forEach(items, function (item) {
										let li = HTMLService.initialize('li')
										//li.click("legendOnClick(" + i + ")");
										li.inner(HTMLService.initialize('div').style("background-color:" + getColorByColorName(item.list_symbol_fill, 0).backgroundColor));
										li.inner(HTMLService.initialize('span').content(item.list_item));
										ul.inner(li);
									});
									let showUndefined = $scope.commonSettings.showUndefined || 0;

									if (showUndefined == 1) {
										let li = HTMLService.initialize('li')
										//li.click("legendOnClick(" + i + ")");
										li.inner(HTMLService.initialize('div').style("background-color:" + "lightgrey"));
										li.inner(HTMLService.initialize('span').content(Translator.translate('CHART_COMMON_UNDEFINED')));
										ul.inner(li);
									}
								}
								return ul.getHTML();
							},
							scales: {
								xAxes: [
									{
										id: 'x-axis-1',
										display: true,
										ticks: {
											fontFamily: 'Roboto',
											fontSize: 12,
											fontColor: '#676a6e'
										},
										gridLines: {
											display: showVerticalGridLines,
											drawBorder: false,
											lineWidth: 2,
											color: '#f0f1f4',
											zeroLineWidth: 2,
											zeroLineColor: '#f0f1f4'
										},
										stacked: true,
										scaleLabel: {
											display: xLabelInUse,
											labelString: xLabel
										},
										barPercentage: barPercentage
									}
								],
								yAxes: [
									{
										id: 'y-axis-1',
										display: true,
										ticks: {
											fontFamily: 'Roboto',
											fontSize: 12,
											fontColor: '#676a6e'
										},
										gridLines: {
											display: showHorizontalGridLines,
											drawBorder: false,
											lineWidth: 2,
											color: '#f0f1f4',
											zeroLineWidth: 2,
											zeroLineColor: '#f0f1f4'
										},
										stacked: true,
										scaleLabel: {
											display: yLabelInUse,
											labelString: yLabel
										},
										barPercentage: barPercentage
									}
								]
							},
							onClick: function (e) {
								let clickable = $scope.commonSettings.clickable || 0;
								if (clickable == 1) {
									let element = this.getElementAtEvent(e);
									if (element.length > 0) {
										let itemObj = $scope.dataHandled[element[0]._index][Object.keys($scope.dataHandled[element[0]._index])[element[0]._datasetIndex]];

										let clickProcess = $scope.chartConfig.OriginalData.chartProcess;
										let clickPortfolio = $scope.chartConfig.ProcessPortfolioId;

										if ($scope.chartConfig.OriginalData.mainProcessLinkMode) {
											let linkField = $scope.chartConfig.OriginalData.link_process_field;
											clickProcess = $scope.chartConfig.OriginalData.process;
											clickPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

											let dataT = $scope.rawDataFiltered.filter(function (x) {
												return itemObj.clickIds.includes(x.item_id);
											});
											let parentIds = [];

											angular.forEach(dataT, function (item) {
												let tempArr = item[linkField].toString().split(",");

												for (let i = 0; i < tempArr.length; i++) {
													if (parentIds.includes(tempArr[i]) == false) parentIds.push(tempArr[i]);
												}

											});
											itemObj.clickIds = parentIds;
										}

										ViewService.view('portfolio', {
											params: {
												portfolioId: clickPortfolio,
												restrictions: { 0: itemObj.clickIds },
												process: clickProcess,
												chartForceFlatMode: true,
												titleExtension: $scope.chartOptions.title.text + ": " + $scope.multiListLabels[element[0]._index] + " -> " + $scope.dataHandled[element[0]._index][element[0]._datasetIndex].name
											}
										},
											{
												split: true, size: 'normal'
											}
										);
									}
								}
							},
							tooltips: {
								mode: 'nearest',
								callbacks: {
									label: function (value) {
										let itemObj = $scope.dataHandled[value.index][Object.keys($scope.dataHandled[value.index])[value.datasetIndex]];

										let precision = Number($scope.commonSettings.precision) || 0;
										let listType = $scope.commonSettings.listType || 0;
										if (listType == 0) {
											let total = 0;
											angular.forEach($scope.dataHandled[value.index], function (item) {
												total += item.itemCount;
											});

											return itemObj.name + ": " + Common.formatNumber(value.value, precision) + " (" + Common.formatNumber(itemObj.itemCount / total * 100, precision) + " %)";
										} else {
											return itemObj.name + ": " + Common.formatNumber(itemObj.itemCount, precision) + " (" + Common.formatNumber(value.value, precision) + " %)";
										}
									}
								}
							}

						};

						if ($scope.commonSettings.listType == 1) {
							if ($scope.horizontalBarMode == 0) {
								$scope.multiListOptions["scales"]["yAxes"][0]["ticks"] = {
									min: 0, max: 100, callback: function (value, index, values) {
										return value + '%';
									}
								};
							} else {
								$scope.multiListOptions["scales"]["yAxes"][0]["ticks"] = {
									min: 0, max: 100
								};
								$scope.multiListOptions["scales"]["xAxes"][0]["ticks"] = {
									min: 0, max: 100,
									callback: function (value, index, values) {
										return value + '%';
									}
								};
							}
						}
						$scope.generateLegend();
					};

					let getColorByColorName = function (colorName, barIndex) {
						if (colorName != "") {
							let colorSelection = "";

							if (colorName.indexOf(' ') > -1) {
								let splitted = colorName.split(" ");
								colorSelection = $scope.colorArr[splitted[0] + splitted[1].charAt(0).toUpperCase() + splitted[1].slice(1)];
							} else {
								colorSelection = $scope.colorArr[colorName];
							}

							let color = {
								backgroundColor: colorSelection.hex,
								pointBackgroundColor: colorSelection.hex,
								hoverBackgroundColor: colorSelection.hex,
								fill: 'origin'
							};
							return color;
						} else {
							let defaultColors = $scope.defaultColors;
							let color = {
								backgroundColor: defaultColors[$scope.colerBarIndexes[barIndex]],
								pointBackgroundColor: defaultColors[$scope.colerBarIndexes[barIndex]],
								hoverBackgroundColor: defaultColors[$scope.colerBarIndexes[barIndex]],
								borderColor: defaultColors[$scope.colerBarIndexes[barIndex]],
								fill: 'origin'
							};
							$scope.colerBarIndexes[barIndex] += 1;
							return color;
						}
					};

					let refreshLayer = function () {
						if ($scope.chartConfig && $scope.chartData) {
							refreshData();
						}
					}
					$scope.$watch('chartData', refreshLayer);

					$scope.filterChanged = function () {
						console.log("saveFilters");
						ChartsService.setChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);
						refreshLayer();
					}
				}				
			}
		};

		function dynamicSort(property) {
			let sortOrder = 1;

			if (property[0] === "-") {
				sortOrder = -1;
				property = property.substr(1);
			}

			return function (a, b) {
				if (sortOrder == -1) {
					if (typeof b[property] == 'number') {
						return b[property] - a[property];
					}
					else {
						if (property == "order_no") {
							if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
								return -1;
							} else if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
								return 1;
							}
							return b[property].localeCompare(a[property]);
						}
						else {
							return b[property].localeCompare(a[property]);
						}
					}

				} else {
					if (typeof b[property] == 'number') {
						return a[property] - b[property];
					}
					else {
						if (property == "order_no") {
							if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
								return -1;
							} else if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
								return 1;
							}
							return a[property].localeCompare(b[property]);
						}
						else {
							return a[property].localeCompare(b[property]);
						}
					}
				}
			}
		}
		function startsWithUppercase(str) {
			return str.substr(0, 1).match(/[A-Z\u00C0-\u00DC]/);
		}
	}
})();
//tooltips: {
//    callbacks: {
//        title: function (value) {
//            return $scope.labelsL[value[0].index];
//        },
//        label: function (value) {
//            let labelPrefix = "";
//            if ($scope.prefix.length > 0) {
//                labelPrefix = " " + $scope.prefix;
//            }

//            let num = Common.formatNumber(finalData[value.datasetIndex][value.index] ,$scope.decimalNumbersPerItem[value.datasetIndex]);
//            return num + labelPrefix;
//        }
//    }
//}