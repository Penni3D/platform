(function () {
	'use strict';
	angular
		.module('core.graphs.horizontalProgress', [])
		.directive('horizontalProgress', horizontalProgressDirective);

	horizontalProgressDirective.$inject = ['PortfolioService', 'ViewService', '$timeout', 'Common'];

	function horizontalProgressDirective(PortfolioService, ViewService, $timeout, Common) {
		return {
			restrict: 'E',
			templateUrl: 'core/ui/graphs/horizontalProgress.html',
			replace: true,
			scope: {
				width: '@',
				height: '@',
				title: '@',
				value: '=',
				value2: '=',
				colors: '=?',
				suffix: '@'
			},
			controller: function ($scope, $element) {
				var draw = function () {
					var percent = 100;
					$element.width($scope.width);
					
					if (!$scope.colors) $scope.colors = ['#FFE97C', '#E0E0E0', 'rgb(244, 67, 53)'];

					var dataset = [
						{sala: "value", value: percent, color: $scope.colors[0]},
						{sala: "left", value: 100 - percent, color: $scope.colors[1]}
					];
					var suffix = $scope.suffix;

					if ($scope.value !== $scope.oldValue || $scope.value2 !== $scope.oldValue2) {
						if (!suffix || suffix === "%") {
							suffix = "%";
							if ($scope.value === Infinity || isNaN($scope.value)) $scope.value = 0;
							percent = $scope.value;
						}
						if (angular.isDefined($scope.value2)) {
							percent = 100 * ($scope.value / $scope.value2);
						}

						if (suffix === "%" && percent > 100) {
							dataset = [
								{sala: "value", value: (percent - 100), color: $scope.colors[2]},
								{sala: "left", value: 100 - (percent - 100), color: $scope.colors[0]}
							];
						} else {
							dataset = [
								{sala: "value", value: percent, color: $scope.colors[0]},
								{sala: "left", value: 100 - percent, color: $scope.colors[1]}
							];
						}
					}

					d3.select($element[0]).html('');
					var svg = d3.select($element[0])
						.append('svg')
						.attr('class', 'horizontal-chart')
						.attr('width', $scope.width)
						.attr('height', $scope.height)
						.append('g');
					svg.append("rect")
						.attr("x", 0)
						.attr("y", 0)
						.attr("width", dataset[0].value / 100 * $element.width())
						.attr("height", $scope.height)
						.attr("fill", dataset[0].color);
					svg.append("rect")
						.attr("x", dataset[0].value / 100 * $element.width())
						.attr("y", 0)
						.attr("width", dataset[1].value / 100 * $element.width())
						.attr("height", $scope.height)
						.attr("fill", dataset[1].color);
					svg.append("text").text($scope.title)
						.attr("dy", "1.25em")
						.attr("x", $scope.width / 2) //Center point
						.attr("text-anchor", "middle") //Centers around center point
						.attr('font-size', '10px');
					$scope.oldValue = $scope.value;
					$scope.oldValue2 = $scope.value2;
				};
				$scope.$watch('value + value2', draw);
			}
		};
	}
})();
