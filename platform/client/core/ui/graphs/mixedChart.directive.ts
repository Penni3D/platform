﻿(function () {
	'use strict';
	angular
		.module('core.graphs.mixedChart', [])
		.directive('mixedChart', mixedChartDirective);

	mixedChartDirective.$inject = ['Common', 'ProcessService', 'ApiService', '$rootScope', 'ChartColourService', 'Translator', 'ViewService', 'Colors', 'UniqueService', '$sce', 'ChartsService'];

	function mixedChartDirective(Common, ProcessService, ApiService, $rootScope, ChartColourService, Translator, ViewService, Colors, UniqueService, $sce, ChartsService) {
		return {
			restrict: 'E',
			templateUrl: 'core/ui/graphs/mixedChart.html',
			replace: false,
			scope: {
				chartData: '=',
				chartColors: '=',
				chartConfig: '=',
				chartOptions: '=',
				size: '@?'
			},
			controller: function ($scope, $element) {
				$scope.langKey = $rootScope.clientInformation["locale_id"];
				$scope.filterPortfolio = $scope.chartConfig.ProcessPortfolioId;
				if ($scope.chartConfig.OriginalData.main_process_portfolio_id && $scope.chartConfig.OriginalData.main_process_portfolio_id > 0) $scope.filterPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

				$scope.quickFilterInUse = false;
				$scope.filtersVisible = false;

				$scope.filterActive = false;
				
				$scope.filterData = {};
				$scope.savedFilteringsInUse = false;
				$scope.rawDataFiltered = [];

				$scope.zoomYears = [];
				$scope.zoomLevel2 = [];
				$scope.secondaryTimeSelectionModify = false;
				$scope.filterData.secondaryTimeSelection = [];
				
				//Etsi oikea arvo jo tässä!
				//$scope.secondaryTimeSelectionTitle = "CHART_MIXED_YEAR";
				if ($scope.chartConfig.OriginalData.mixedDataCommon.dateType && $scope.chartConfig.OriginalData.mixedDataCommon.dateType.length > 0) {
					switch (parseInt($scope.chartConfig.OriginalData.mixedDataCommon.dateType[0])) {
						case 1:
							$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_DAY');
							break;
						case 2:
							$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_WEEK');
							break;
						case 3:
							$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_MONTH');
							break;
						case 4:
							$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_QUARTER');
							break;
						case 5:
							$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_YEAR');
							break;
					}
                }
				
				let dateMode = 0;
				$scope.zoomInUse = false;
				$scope.zoomYearsChanged = false;
				$scope.filterData.yearSelection = [];
				var zoomInProgress = false;
				$scope.dataType = 0;
				$scope.normalTableRows = 0;
				$scope.normalTableColumns = 0;

				$scope.id = UniqueService.get('mixed-chart', true);
				let fieldWithLanguage = String('list_item_' + $rootScope.clientInformation["locale_id"]).toLowerCase().replace("-", "_");

				$scope.filterKeyParams = {};
				$scope.filterKeyParams.chartId = $scope.chartConfig.OriginalData.chartId;
				$scope.filterKeyParams.linkId = $scope.chartConfig.OriginalData.linkId || 0;
				$scope.filterKeyParams.chartProcess = $scope.chartConfig.OriginalData.chartProcess;

				if ($scope.chartData.inMeta) {
					$scope.filterKeyParams.type = "meta"
				} else if ($rootScope.frontpage) {
					$scope.filterKeyParams.type = "frontPage"
				} else {
					$scope.filterKeyParams.type = "portfolio"
				}

				$scope.legend = "";
				$scope.generateLegend = function () {
					setTimeout(function () {
						let chart = Common.getChartHandle($scope.id);
						if (chart) $scope.legend = $sce.trustAsHtml(chart.generateLegend());
					}, 0);
				}

				if ($scope.chartConfig.OriginalData.mixedDataCommon.zoom && parseInt($scope.chartConfig.OriginalData.mixedDataCommon.zoom[0]) == 1) {
					$scope.zoomInUse = true;
				}

				let refreshData = function () {
					if (Object.keys($scope.chartData.filterData).length > 0) {
						$scope.quickFilterInUse = true;
					}
					else {
						$scope.quickFilterInUse = false;
                    }

					let cf = ChartsService.getChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId);

					if (cf && ($scope.zoomInUse || Object.keys($scope.chartData.filterData).length > 0)) {
						$scope.filterData = cf;

						if (zoomInProgress == false) {
							if ($scope.filterData.yearSelection.length > 0) {
								zoomInProgress = true;
								$scope.zoomYearsChanged = true;
								$scope.savedFilteringsInUse = true;
							}
						}
					} else {
						$scope.filterData.yearSelection = [];
						$scope.filterData.secondaryTimeSelection = [];
						$scope.filterData.dayModeStart = null;
						$scope.filterData.dayModeEnd = null;
					}

					let quickFiltersInEffect = false;
					angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
						if ($scope.filterData[key] != null && $scope.filterData[key].length > 0) quickFiltersInEffect = true;
					});

					if (quickFiltersInEffect || $scope.filterData.yearSelection && $scope.filterData.yearSelection.length > 0 || $scope.filterData.dayModeStart && $scope.filterData.dayModeStart.length > 0 || $scope.filterData.dayModeEnd && $scope.filterData.dayModeEnd.length > 0) {
						$scope.filterActive = true;
					}
					else {
						$scope.filterActive = false;
                    }

					if ($scope.chartConfig) {
						let chartConfig = $scope.chartConfig.OriginalData;
						let xAxisInfo = $scope.chartData.fieldTypes;
						let parentColumn = xAxisInfo.xAxisColumnInfo;

						let rawData = $scope.chartData.data;

						if (rawData.length > 0 && $scope.chartData.filterData && Object.keys($scope.chartData.filterData).length > 0) { // Lets apply quick filters to rawData
							let filtersToBeApplied = [];
							$scope.rawDataFiltered = rawData;

							angular.forEach(Object.keys($scope.chartData.filterData), function (key) {
								if (key in $scope.filterData && $scope.filterData[key].length > 0) filtersToBeApplied.push({
									name: key,
									order: $scope.filterData[key].length,
									ids: $scope.filterData[key]
								});
							});

							if (filtersToBeApplied.length > 0) {
								filtersToBeApplied = filtersToBeApplied.sort(dynamicSort("order"));

								angular.forEach(filtersToBeApplied, function (obj) {
									$scope.rawDataFiltered = $scope.rawDataFiltered.filter(function (x) {
										if (Array.isArray(x[obj["name"]])) {
											return x[obj["name"]].some(element => {
												return obj.ids.includes(element);
											});
										} else {
											return [parseInt(String(x[obj["name"]]).trim())].some(element => {
												return obj.ids.includes(element);
											});
										}
									});
								});
							}

						} else {
							$scope.rawDataFiltered = rawData;
						}

						$scope.noXAxisLabels = [];

						$scope.gradientDataCopy = [];
						$scope.gradientBarInfo = {};
						$scope.gradientAnimationStatus = 0;
						$scope.gradientAnimationExtras = {};

						if (chartConfig.mixedDataCommon.showLegend[0] == 0) {
							$scope.generateLegend();
						} else {
							$scope.legend = "";
						}
						let isWide = $scope.chartConfig.OriginalData.fullWidth || false;
						let w = $('#main-container-transition > div').first().width();
						$scope.front = $rootScope.frontpage;
						$scope.height = Common.getCanvasResponsiveSize($scope.height, $scope, $element, isWide, false);
						/*	$scope.height = Common.getCanvasResponsiveSize($scope.height, $scope, $element, isWide, $rootScope.frontpage);*/

						//$scope.mixedData = null;
						//$scope.mixedLabels

						let stackNamesByNumber = {};
						let tempArrForSorting = [];
						$scope.extraNoXAxisLabelLenght = 0;
						$scope.extraNoXAxisLabelHeight = 0;
						let originalConfigFields = $scope.chartConfig.OriginalData;
						$scope.angledLabels = false;


						$scope.horizontalBarMode = chartConfig.mixedDataCommon.horizontalBars[0] || 0;

						//Gridline handling
						let hideHorizontalGridLinesVal = chartConfig.mixedDataCommon.hideHorizontalGridLines && chartConfig.mixedDataCommon.hideHorizontalGridLines[0] || 0;
						let hideVerticalGridLInesVal = chartConfig.mixedDataCommon.hideVerticalGridLines && chartConfig.mixedDataCommon.hideVerticalGridLines[0] || 0;

						let showHorizontalGridLines = true;
						let showVerticalGridLines = true;

						if ($scope.horizontalBarMode != 1) {
							if (hideHorizontalGridLinesVal > 0) showHorizontalGridLines = false;
							if (hideVerticalGridLInesVal > 0) showVerticalGridLines = false;
						} else {
							if (hideHorizontalGridLinesVal > 0) showVerticalGridLines = false;
							if (hideVerticalGridLInesVal > 0) showHorizontalGridLines = false;
						}
						//--

						$scope.maxIgnore = 0;
						$scope.maxDecimals = 0;

						let useListMode = chartConfig.mixedDataCommon.useListMode || 0;
						if (useListMode == 1 && $scope.chartData.fieldTypes.itemListItems.length > 0) {
							$scope.chartConfig.OriginalData.mixedData = [];
							let listConf = $scope.chartConfig.OriginalData.mixedListData;

							// Creating new values to data based on list category and source
							angular.forEach($scope.rawDataFiltered, function (rowObject) {
								angular.forEach($scope.chartData.fieldTypes.itemListItems, function (listItem) {
									let listCategory = listConf["selectedList"][0];
									let sourceFieldName = listConf["dataSource"][0];
									if (rowObject[listCategory].length > 0 && rowObject[listCategory].indexOf(listItem.item_id) > -1) {
										rowObject[listItem.item_id + "_mixedTemp"] = rowObject[sourceFieldName];
									} else {
										rowObject[listItem.item_id + "_mixedTemp"] = null;
									}
								});
							});

							let arrSorted = [];
							let listOrder = $scope.chartData.itemListProcessFields.list_order || "";
							if (listOrder == "") listOrder = "order_no";
							if (listOrder == "list_item" && $scope.chartData.fieldTypes.itemListItems.length > 0 && $scope.chartData.fieldTypes.itemListItems[0].hasOwnProperty(fieldWithLanguage)) listOrder = fieldWithLanguage;
							angular.forEach($scope.chartData.fieldTypes.itemListItems.sort(dynamicSort(listOrder)), function (item) {
								arrSorted.push(item);
							});
							if (listConf.reverseOrder != 1) arrSorted = arrSorted.reverse();

							angular.forEach(arrSorted, function (listItem) {
								let newItem = {};
								newItem.customColor = [];
								newItem.dataSource = listItem.item_id + "_mixedTemp";
								if (listConf.useCount) newItem.useCount = [listConf.useCount[0]];
								newItem.decimalNumber = listConf.decimalNumber;
								newItem.ignoreNulls = [listConf.ignoreNulls[0]];
								newItem.ignoreZeros = [listConf.ignoreZeros[0]];
								newItem.lineFillType = listConf.fillType;
								newItem.lineGraphicStyle = listConf.lineGraphicStyle || null;
								newItem.linePointStyle = listConf.linePointStyle || null;
								newItem.markerSize = listConf.markerSize || null;
								newItem.mode = [listConf.mode[0]];
								newItem.compareItemId = listItem.item_id;
								newItem.compareField = listConf["selectedList"][0];
								let itemLocale = $rootScope.clientInformation["locale_id"];
								//itemLocaleObj.
								newItem.nameField = {};

								if ("list_item_" + itemLocale.toLowerCase().replace("-", "_") in listItem) {
									newItem.nameField[itemLocale] = listItem["list_item_" + itemLocale.toLowerCase().replace("-", "_")];
								} else {
									newItem.nameField[itemLocale] = listItem.list_item;
								}

								newItem.stackName = {};
								if (listConf.autoStack == 1) newItem.stack = 1;
								if (listConf.listColors == 1) newItem.customColor = [listItem.list_symbol_fill];
								newItem.type = [listConf.type[0] || 0];

								$scope.chartConfig.OriginalData.mixedData.push(newItem);
							});
						}

						$scope.hasXAxis = true;
						$scope.toolTipSum = 0;
						$scope.tooltipMode = "index";
						if (chartConfig.mixedDataCommon.toolTipSum) {
							$scope.toolTipSum = chartConfig.mixedDataCommon.toolTipSum[0] || 0;
						}

						if (chartConfig.mixedDataCommon.showNearest && chartConfig.mixedDataCommon.showNearest == 1) {
							$scope.tooltipMode = "nearest";
						}

						if (chartConfig.mixedDataCommon.xaxis == null || chartConfig.mixedDataCommon.xaxis.length == 0) {
							chartConfig.mixedDataCommon.xaxis = [];
							chartConfig.mixedDataCommon.xaxis.push("noSelection");
						}

						if (chartConfig.mixedDataCommon.xaxis[0] == "noSelection") {
							$scope.hasXAxis = false;
							xAxisInfo["xAxisFieldType"] = {data_type: 0};
						}

						$scope.useListColors = false;
						$scope.xAxisListItemsWithColor = [];

						angular.forEach($scope.chartConfig.OriginalData.mixedData, function (element) {
							if ($scope.hasXAxis == false) {
								element.type = [2];
							}
							tempArrForSorting.push(element);
							if (element.stack != null && element.stackName != null && element.stackName[$rootScope.clientInformation["locale_id"]] != "") {
								if (stackNamesByNumber.hasOwnProperty(element.stack) == false) {
									stackNamesByNumber[element.stack] = element.stackName[$rootScope.clientInformation["locale_id"]] || "";
								}
							}
						});

						$scope.spaceWidth = displayTextWidth(" ", "12px \"Roboto\" ,\"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif");
						$scope.maxYWidth = 0; //Used in no x-axis chart's horizontal mode
						$scope.spaceString = "";

						$scope.chartConfig.OriginalData.mixedData = tempArrForSorting.sort(drawOrder).reverse();

						let dataArr;
						let xLabel = "";
						let yLabel = "";
						let XAxisIsDate = false;

						$scope.hasData = true;

						if (originalConfigFields.mixedData.length == 0) {
							$scope.hasData = false;
						}

						if (originalConfigFields.showXAxisLabel == true) {
							xLabel = originalConfigFields.xAxisLabel[$rootScope.clientInformation["locale_id"]];
						}
						if (originalConfigFields.showYAxisLabel == true) {
							yLabel = originalConfigFields.yAxisLabel[$rootScope.clientInformation["locale_id"]];
						}

						if (chartConfig.mixedDataCommon.gradientMode != null) {
							$scope.gradientOrder = chartConfig.mixedDataCommon.gradientOrder[0] || 0;
							$scope.gradientMode = chartConfig.mixedDataCommon.gradientMode[0] || 0;
							$scope.gradientPointer = chartConfig.mixedDataCommon.gradientPointers[0] || 0;
						}

						if ($scope.gradientMode == null) $scope.gradientMode = 0;

						$scope.xtableSumInUse = false;
						$scope.ytableSumInUse = false;

						if (chartConfig.mixedDataCommon.showTableXSum && chartConfig.mixedDataCommon.showTableXSum[0] == 1) {
							$scope.xtableSumInUse = true;
						}
						if (chartConfig.mixedDataCommon.showTableYSum && chartConfig.mixedDataCommon.showTableYSum[0] == 1) {
							$scope.ytableSumInUse = true;
						}

						$scope.showTable = false;
						if (chartConfig.mixedDataCommon.showTable[0] == 1) {
							$scope.showTable = true;
							if (chartConfig.mixedDataCommon.tableName[$rootScope.clientInformation["locale_id"]] != "") {
								$scope.tableName = chartConfig.mixedDataCommon.tableName[$rootScope.clientInformation["locale_id"]];
							}
						}
						$scope.decimalNumbersPerItem = [];
						$scope.prefix = chartConfig.mixedDataCommon.amountPrefix[$rootScope.clientInformation["locale_id"]] || "";

						let onlyLines = true;
						angular.forEach(originalConfigFields.mixedData, function (element) {
							if (element.type[0] == 2) {
								onlyLines = false;
							}
							let tempNum = element.decimalNumber || 0;
							$scope.decimalNumbersPerItem.push(tempNum);
						});


						if (onlyLines == true) {
							$scope.chartType = "line";
						} else {
							if ($scope.horizontalBarMode == 0) {
								$scope.chartType = "bar";
							} else {
								$scope.chartType = "horizontalBar";
							}
						}

						if ($scope.categoryNames != null && $scope.categoryNames.length > 0) {
							angular.forEach($scope.categoryNames, function (name) {
								let textWidth = displayTextWidth(name, "12px \"Roboto\" ,\"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif");
								if (textWidth > $scope.maxYWidth) $scope.maxYWidth = textWidth;
							});
							let numberOfSpacesNeeted = $scope.maxYWidth / $scope.spaceWidth;
							for (let i = 0; i < numberOfSpacesNeeted; i++) {
								//$scope.spaceString += " ";
								$scope.spaceString += "_";
							}
						}

						if ($scope.hasXAxis == false) { //resizes y-axis label area size so that all labels can be shown
							angular.forEach(originalConfigFields.mixedData, function (element) {
								if (element.stack != null && element.stackName[$rootScope.clientInformation["locale_id"]] != null && element.stackName[$rootScope.clientInformation["locale_id"]] != "") {
									let textWidth = displayTextWidth(checkLabelLength(element.stackName[$rootScope.clientInformation["locale_id"]]), "12px \"Roboto\" ,\"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif");
									if ($scope.extraNoXAxisLabelLenght < textWidth) $scope.extraNoXAxisLabelLenght = textWidth;
								} else {
									let textWidth = displayTextWidth(checkLabelLength(element.nameField[$rootScope.clientInformation["locale_id"]]), "12px \"Roboto\" ,\"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif");
									if ($scope.extraNoXAxisLabelLenght < textWidth) $scope.extraNoXAxisLabelLenght = textWidth;
								}
							});

							if ($scope.horizontalBarMode != 1) {
								$scope.maxLabelLength = $scope.extraNoXAxisLabelLenght;
								$scope.maxLabelHeight = Math.sin(55 * Math.PI / 180) * ($scope.maxLabelLength + 5);
								$scope.extraNoXAxisLabelLenght = 0;
							}
						}

						$scope.gradientObjectByIndex = {};
						let doNotRunForFirstTime = 0;

						if ($scope.hasXAxis == false && $scope.gradientMode == 1) {
							Common.subscribeContentResize($scope, function () {
								if (doNotRunForFirstTime > 0) {
									$scope.angledCalcDone = null;
									createGradient($scope, $scope.chartInstance);
									drawGradientMarkers($scope, $scope.chartInstance);
								}
								doNotRunForFirstTime += 1;
							});
						}
						$scope.datasetScaleObject = {};
						rawData = $scope.rawDataFiltered;

						let zoomExcludes = [];
						$scope.zoomYears = [];
						let labels = [];
						$scope.labelsL = [];
						$scope.tableLabels = [];
						let scaleMin = 0;
						let scaleMax = 0;

						$scope.listItemTranslations = {};

						if ($scope.zoomYearsChanged && $scope.savedFilteringsInUse == false) {
							$scope.filterData.secondaryTimeSelection = [];
							$scope.zoomLevel2 = [];
						}

						let sourceDataType = xAxisInfo.xAxisFieldType.data_type || 0;
						if (xAxisInfo.xAxisFieldType.data_type == 16) {

							if ([3, 6, 13].indexOf(xAxisInfo.xAxisFieldType.SubDataType) > -1) {
								sourceDataType = xAxisInfo.xAxisFieldType.SubDataType;
							} else if ([0, 1, 2].indexOf(xAxisInfo.xAxisFieldType.SubDataType) > -1) {
								sourceDataType = 20;
							}

						}
						if (xAxisInfo.xAxisFieldType.data_type == 20) {
							if (xAxisInfo.xAxisFieldType.SubDataType == 6) {
								sourceDataType = 6;
							} else if (xAxisInfo.xAxisFieldType.SubDataType == 5) {
								sourceDataType = 5;
							} else if (xAxisInfo.xAxisFieldType.SubDataType == 13) {
								sourceDataType = 3;
							} else if ([0, 1, 2].indexOf(xAxisInfo.xAxisFieldType.SubDataType) > -1) {
								sourceDataType = 20;
							}
						}
						if (xAxisInfo.xAxisFieldType.data_type == 0 && $scope.hasXAxis) {
							sourceDataType = 20;
						}

						if (sourceDataType == 13) sourceDataType = 3;
						$scope.dataType = sourceDataType;
						switch (parseInt(sourceDataType)) {
							case 0: // No xAxis selected
								$scope.noXAxisLabels = [];
								if ($scope.hasData) {
									$scope.zoomInUse = false;
									dataArr = [];
									dataArr[0] = new Array(originalConfigFields.mixedData.length);

									for (let i = 0; i < originalConfigFields.mixedData.length; i++) {
										dataArr[0][i] = [];
									}
								}

								angular.forEach(rawData, function (rowObject) {
									let chartElementIndex = 0;
									angular.forEach(originalConfigFields.mixedData, function (element) {
										let val = null;
										let useCount = element.useCount || 0;
										if ($scope.gradientMode == 1) useCount = 0;
										if (useCount == 1) {
											if (useListMode != 1) {
												val = 1;
											} else {
												if (rowObject[element.compareField] == element.compareItemId) {
													val = 1;
												} else {
													val = 0;
												}
											}
										} else {
											val = rowObject[element.dataSource];
										}
										dataArr[0][chartElementIndex].push({
											value: val,
											name: element.nameField,
											ItemID: rowObject["item_id"]
										});
										chartElementIndex += 1;
									});
								});
								angular.forEach(originalConfigFields.mixedData, function (element) {
									//if (labels.indexOf(element.nameField) == -1) $scope.noXAxisLabels.push(element.nameField);
									$scope.noXAxisLabels.push(element.nameField);
								});
								break;
							case 3: // date
								XAxisIsDate = true;
								let minDate;
								let maxDate;
								let timeType = chartConfig.mixedDataCommon.dateType[0];
								let handledData = rawData.filter(function (x) {
									return x[xAxisInfo.xAxisFieldType.name] != null;
								});

								let maxCumulativeDate = null;

								angular.forEach(handledData, function (rowObject) {
									if (minDate == null || new Date(rowObject[xAxisInfo.xAxisFieldType.name]) < minDate) {
										minDate = new Date(rowObject[xAxisInfo.xAxisFieldType.name]);
									}
									if (maxDate == null || new Date(rowObject[xAxisInfo.xAxisFieldType.name]) > maxDate) {
										maxDate = new Date(rowObject[xAxisInfo.xAxisFieldType.name]);
									}
									if ($scope.zoomInUse) {
										let date = new Date(rowObject[originalConfigFields.mixedDataCommon.xaxis[0]]);
									}

									if (chartConfig.mixedDataCommon.cumulativeEnd) {
										let cumulativeEnd = chartConfig.mixedDataCommon.cumulativeEnd[0] || null;
										if (cumulativeEnd != null) {
											let cumulativeDate = rowObject[cumulativeEnd];
											if (cumulativeDate != null && maxCumulativeDate != null && cumulativeDate > maxCumulativeDate) {
												maxCumulativeDate = cumulativeDate;
											} else if (maxCumulativeDate == null && cumulativeDate != null) {
												maxCumulativeDate = cumulativeDate;
											}
										}
									}
								});

								if (maxCumulativeDate != null && new Date(maxCumulativeDate) > maxDate) {
									maxDate = new Date(maxCumulativeDate);
								}

								if ($scope.chartData.xTimeFilters) {
									if ($scope.chartData.xTimeFilters.start != null) minDate = new Date($scope.chartData.xTimeFilters.start);
									if ($scope.chartData.xTimeFilters.end != null) maxDate = new Date($scope.chartData.xTimeFilters.end);
								}

								if (chartConfig.mixedDataCommon.absoluteStart) minDate = new Date(chartConfig.mixedDataCommon.absoluteStart);
								if (chartConfig.mixedDataCommon.absoluteEnd) maxDate = new Date(chartConfig.mixedDataCommon.absoluteEnd);

								if (maxDate == undefined || minDate == undefined) {
									minDate = new Date();
									maxDate = new Date();
                                }

								for (let i = 0; i < (maxDate.getFullYear() - minDate.getFullYear() + 1); i++) {
									$scope.zoomYears.push({
										name: minDate.getFullYear() + i,
										value: minDate.getFullYear() + i
									});
								}

								var yearArr = [];
								var secondArr = [];

								switch (timeType) {
									case 1://day
										var diff;
										let endDate;

										if (zoomInProgress) {
											endDate = new Date(moment($scope.dayModeEnd).format("YYYY-MM-DD"));
										}

										if (isNaN(endDate)) {
											diff = dayDiff(maxDate, minDate) + 1;
										} else {
											diff = dayDiff(endDate, minDate) + 1;
										}
										dataArr = new Array(diff);

										for (let i = 0; i < diff; i++) {
											let tempDate = new Date(new Date(moment(minDate).format("YYYY-MM-DD")));
											tempDate.setDate(minDate.getDate() + i);
											let day = tempDate.getDate().toString();
											let month = tempDate.getMonth() + 1;
											labels.push(day + " / " + month + " / " + tempDate.getFullYear());
											dataArr[i] = new Array(_.size(originalConfigFields.mixedData));

											for (let j = 0; j < _.size(originalConfigFields.mixedData); j++) {
												dataArr[i][j] = new Array();
											}
											let chartElementIndex = 0;
											angular.forEach(originalConfigFields.mixedData, function (element) {
												angular.forEach(handledData, function (row) {
													let rowDate = new Date(moment(row[xAxisInfo.xAxisFieldType.name]).format("YYYY-MM-DD"));
													if (tempDate.getFullYear() == rowDate.getFullYear() && tempDate.getMonth() + 1 == rowDate.getMonth() + 1 && tempDate.getDate() == rowDate.getDate()) {
														let val = null;
														let useCount = element.useCount || 0;
														if (useCount == 1) {
															if (useListMode != 1) {
																val = 1;
															} else {
																if (row[element.compareField] == element.compareItemId) {
																	val = 1;
																}
																//else {
																//	val = 0;
																//}
															}
														} else {
															val = row[element.dataSource];
														}
														dataArr[i][chartElementIndex].push({
															value: val,
															date: rowDate,
															endDate: row[chartConfig.mixedDataCommon.cumulativeEnd[0]],
															ItemID: row.item_id
														});
													}
												});

												chartElementIndex += 1;
											});

											//zoom
											if (zoomInProgress) {
												if (!isNaN($scope.filterData.dayModeStart) && !isNaN($scope.filterData.dayModeEnd) && tempDate >= $scope.filterData.dayModeStart && tempDate < endDate) {
													//
												} else if (!isNaN($scope.filterData.dayModeStart) && !isNaN($scope.filterData.dayModeEnd)) {
													zoomExcludes.push(i);
												}
												// zoomExcludes
											}
										}
										//$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_DAY');
										dateMode = 1;
										break;
									case 2://week
										// zoom


										angular.forEach($scope.filterData.yearSelection, function (years) {
											yearArr.push(years);
										});
										angular.forEach($scope.filterData.secondaryTimeSelection, function (timeS) {
											secondArr.push(timeS);
										});

										var diff = weekDiff(maxDate, minDate) + 1;
										dataArr = new Array(diff);
										for (let i = 0; i < diff; i++) {
											let tempDate = new Date(minDate);
											tempDate.setDate(minDate.getDate() + (i * 7));
											labels.push(getWeekNumber(tempDate) + " / " + tempDate.getFullYear());
											dataArr[i] = new Array(_.size(originalConfigFields.mixedData));

											for (let j = 0; j < _.size(originalConfigFields.mixedData); j++) {
												dataArr[i][j] = new Array();
											}

											if (yearArr.length > 0) {//secondaryTimeSelectionModify
												$scope.secondaryTimeSelectionModify = true;
											} else {
												$scope.secondaryTimeSelectionModify = false;
											}

											let chartElementIndex = 0;
											angular.forEach(originalConfigFields.mixedData, function (element) {
												angular.forEach(handledData, function (row) {
													let rowDate = new Date(row[xAxisInfo.xAxisFieldType.name]);

													if (tempDate.getFullYear() == rowDate.getFullYear() && getWeekNumber(tempDate) == getWeekNumber(rowDate)) {
														let val = null;
														let useCount = element.useCount || 0;
														if (useCount == 1) {
															if (useListMode != 1) {
																val = 1;
															} else {
																if (row[element.compareField] == element.compareItemId) {
																	val = 1;
																}
																//else {
																//	val = 0;
																//}
															}
														} else {
															val = row[element.dataSource];
														}
														dataArr[i][chartElementIndex].push({
															value: val,
															date: rowDate,
															endDate: row[chartConfig.mixedDataCommon.cumulativeEnd[0]],
															ItemID: row.item_id
														});
													}
												});
												chartElementIndex += 1;
											});

											//zoom
											if ($scope.filterData.yearSelection.indexOf(tempDate.getFullYear()) > -1) {
												if ($scope.zoomYearsChanged) {
													if ($scope.savedFilteringsInUse == false) {
														$scope.filterData.secondaryTimeSelection.push(getWeekNumber(tempDate) + " / " + tempDate.getFullYear());
														secondArr.push(getWeekNumber(tempDate) + " / " + tempDate.getFullYear());
													}
													$scope.zoomLevel2.push({
														name: getWeekNumber(tempDate) + " / " + tempDate.getFullYear(),
														value: getWeekNumber(tempDate) + " / " + tempDate.getFullYear()
													});
												}
											}

											if (zoomInProgress) {
												if (secondArr.indexOf(getWeekNumber(tempDate) + " / " + tempDate.getFullYear()) == -1) {
													zoomExcludes.push(i);
												}
												// zoomExcludes
											}
										}
										//$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_WEEK');
										dateMode = 2;
										break;
									case 3://month 
										// zoom
										angular.forEach($scope.filterData.yearSelection, function (years) {
											yearArr.push(years);
										});

										angular.forEach($scope.filterData.secondaryTimeSelection, function (timeS) {
											secondArr.push(timeS);
										});

										var diff = monthDiff(maxDate, minDate) + 1;
										dataArr = new Array(diff);
										let tempArr = [];

										for (let i = 0; i < diff; i++) {
											let tempDate = new Date(minDate.getFullYear(), minDate.getMonth(), 15);
											tempDate.setMonth(tempDate.getMonth() + i);
											labels.push(tempDate.getMonth() + 1 + " / " + tempDate.getFullYear());
											dataArr[i] = new Array(_.size(originalConfigFields.mixedData));
											for (let j = 0; j < _.size(originalConfigFields.mixedData); j++) {
												dataArr[i][j] = new Array();
											}

											if (yearArr.length > 0) {//secondaryTimeSelectionModify
												$scope.secondaryTimeSelectionModify = true;
											} else {
												$scope.secondaryTimeSelectionModify = false;
											}

											let chartElementIndex = 0;
											angular.forEach(originalConfigFields.mixedData, function (element) {
												angular.forEach(handledData, function (row) {
													let rowDate = new Date(row[xAxisInfo.xAxisFieldType.name]);

													if (tempDate.getFullYear() == rowDate.getFullYear() && tempDate.getMonth() == rowDate.getMonth()) {
														let val = null;
														let useCount = element.useCount || 0;
														if (useCount == 1) {
															if (useListMode != 1) {
																val = 1;
															} else {
																if (row[element.compareField] == element.compareItemId) {
																	val = 1;
																}
																//else {
																//	val = 0;
																//                                                }
															}
														} else {
															val = row[element.dataSource];
														}
														dataArr[i][chartElementIndex].push({
															value: val,
															date: rowDate,
															endDate: row[chartConfig.mixedDataCommon.cumulativeEnd[0]],
															ItemID: row.item_id
														});
													}
												});
												chartElementIndex += 1;
											});

											//zoom
											if ($scope.filterData.yearSelection.indexOf(tempDate.getFullYear()) > -1) {
												if ($scope.zoomYearsChanged) {
													if ($scope.savedFilteringsInUse == false) {
														$scope.filterData.secondaryTimeSelection.push((tempDate.getMonth() + 1) + " / " + tempDate.getFullYear());
														secondArr.push((tempDate.getMonth() + 1) + " / " + tempDate.getFullYear());
													}


													tempArr.push({
														name: (tempDate.getMonth() + 1) + " / " + tempDate.getFullYear(),
														value: (tempDate.getMonth() + 1) + " / " + tempDate.getFullYear()
													});
												}
											}

											if (zoomInProgress) {
												if (secondArr.indexOf((tempDate.getMonth() + 1) + " / " + tempDate.getFullYear()) == -1) {
													zoomExcludes.push(i);
												}
											}
											// zoomExcludes
										}

										if ($scope.zoomYearsChanged) $scope.zoomLevel2 = tempArr;

										//$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_MONTH');
										dateMode = 3;
										break;
									case 4://quarter
										// zoom
										angular.forEach($scope.filterData.yearSelection, function (years) {
											yearArr.push(years);
										});
										angular.forEach($scope.filterData.secondaryTimeSelection, function (timeS) {
											secondArr.push(timeS);
										});

										var diff = getQuarterCount(maxDate, minDate);
										dataArr = new Array(diff);
										for (let i = 0; i < diff; i++) {
											let tempDate = new Date(minDate.getFullYear(), minDate.getMonth(), 15);
											tempDate.setMonth(minDate.getMonth() + i * 3);
											labels.push(getQuaterNumber(tempDate) + " / " + tempDate.getFullYear());
											dataArr[i] = new Array(_.size(originalConfigFields.mixedData));

											for (let j = 0; j < _.size(originalConfigFields.mixedData); j++) {
												dataArr[i][j] = new Array();
											}

											if (yearArr.length > 0) {//secondaryTimeSelectionModify
												$scope.secondaryTimeSelectionModify = true;

											} else {
												$scope.secondaryTimeSelectionModify = false;
											}

											let chartElementIndex = 0;
											angular.forEach(originalConfigFields.mixedData, function (element) {
												angular.forEach(handledData, function (row) {
													let rowDate = new Date(row[xAxisInfo.xAxisFieldType.name]);

													if (tempDate.getFullYear() == rowDate.getFullYear() && getQuaterNumber(tempDate) == getQuaterNumber(rowDate)) {
														let val = null;
														let useCount = element.useCount || 0;
														if (useCount == 1) {
															if (useListMode != 1) {
																val = 1;
															} else {
																if (row[element.compareField] == element.compareItemId) {
																	val = 1;
																}
																//else {
																//	val = 0;
																//}
															}
														} else {
															val = row[element.dataSource];
														}
														dataArr[i][chartElementIndex].push({
															value: val,
															date: rowDate,
															endDate: row[chartConfig.mixedDataCommon.cumulativeEnd[0]],
															ItemID: row.item_id
														});
													}
												});

												chartElementIndex += 1;
											});

											//zoom
											if ($scope.filterData.yearSelection.indexOf(tempDate.getFullYear()) > -1) {
												if ($scope.zoomYearsChanged) {
													if ($scope.savedFilteringsInUse == false) {
														$scope.filterData.secondaryTimeSelection.push(getQuaterNumber(tempDate) + " / " + tempDate.getFullYear());
														secondArr.push(getQuaterNumber(tempDate) + " / " + tempDate.getFullYear());
													}
													$scope.zoomLevel2.push({
														name: getQuaterNumber(tempDate) + " / " + tempDate.getFullYear(),
														value: getQuaterNumber(tempDate) + " / " + tempDate.getFullYear()
													});
												}
											}

											if (zoomInProgress) {
												if (secondArr.indexOf(getQuaterNumber(tempDate) + " / " + tempDate.getFullYear()) == -1) {
													zoomExcludes.push(i);
												}
												// zoomExcludes
											}

										}

										//$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_QUARTER');
										dateMode = 4;
										break;
									case 5://year
										// zoom
										$scope.zoomLevel2 = [];
										angular.forEach($scope.filterData.yearSelection, function (years) {
											yearArr.push(years);
										});

										var diff = maxDate.getFullYear() - minDate.getFullYear() + 1;
										dataArr = new Array(diff);
										for (let i = 0; i < diff; i++) {
											let tempDate = new Date(minDate);
											tempDate.setYear(minDate.getFullYear() + i);
											labels.push(tempDate.getFullYear());
											dataArr[i] = new Array(_.size(originalConfigFields.mixedData));

											for (let j = 0; j < _.size(originalConfigFields.mixedData); j++) {
												dataArr[i][j] = new Array();
											}
											let chartElementIndex = 0;
											angular.forEach(originalConfigFields.mixedData, function (element) {
												angular.forEach(handledData, function (row) {
													let rowDate = new Date(row[xAxisInfo.xAxisFieldType.name]);

													if (tempDate.getFullYear() == rowDate.getFullYear()) {
														let val = null;
														let useCount = element.useCount || 0;
														if (useCount == 1) {
															if (useListMode != 1) {
																val = 1;
															} else {
																if (row[element.compareField] == element.compareItemId) {
																	val = 1;
																}
																//else {
																//	val = 0;
																//}
															}
														} else {
															val = row[element.dataSource];
														}
														dataArr[i][chartElementIndex].push({
															value: val,
															date: rowDate,
															endDate: row[chartConfig.mixedDataCommon.cumulativeEnd[0]],
															ItemID: row.item_id
														});
													}
												});

												chartElementIndex += 1;
											});
											if (zoomInProgress && yearArr.length > 0) {
												if (yearArr.indexOf(tempDate.getFullYear()) == -1) {
													zoomExcludes.push(i);
												}
											}
										}
										//$scope.secondaryTimeSelectionTitle = Translator.translate('CHART_MIXED_YEAR');
										dateMode = 5;
										break;
									default:
										break;
								}
								$scope.dateMode = dateMode;
								break;
							case 5: // process
								dataArr = [];

								angular.forEach(rawData, function (rowObject) {
									if (rowObject[xAxisInfo.xAxisFieldType.name].length > 0) {
										let categoryName = rowObject[xAxisInfo.xAxisFieldType.name];
										if (labels.indexOf(categoryName) == -1) {
											labels.push(categoryName);
											dataArr.push([]);
											dataArr[labels.indexOf(categoryName)] = new Array(_.size(originalConfigFields.mixedData));
											for (let i = 0; i < _.size(originalConfigFields.mixedData); i++) {
												dataArr[labels.indexOf(categoryName)][i] = new Array();
											}
										}
										let chartElementIndex = 0;
										angular.forEach(originalConfigFields.mixedData, function (element) {
											let val = null;
											let useCount = element.useCount || 0;
											if (useCount == 1) {
												if (useListMode != 1) {
													val = 1;
												} else {
													if (rowObject[element.compareField] == element.compareItemId) {
														val = 1;
													}
													//else {
													//	val = 0;
													//}
												}
											} else {
												val = rowObject[element.dataSource];
											}
											dataArr[labels.indexOf(categoryName)][chartElementIndex].push({
												value: val,
												name: categoryName,
												ItemID: rowObject["item_id"]
											});
											chartElementIndex += 1;
										});
									}
								});

								break;
							case 6: // list
								if (chartConfig.mixedDataCommon.listColors) {
									let listColors = chartConfig.mixedDataCommon.listColors[0] || 0;
									if (listColors == 1 && chartConfig.mixedData.length == 1) {
										$scope.useListColors = true;
									}
								}

								let categoryItems = xAxisInfo.xAxisItems;
								dataArr = [];
								let tempArr = [];
								let showAll = chartConfig.mixedDataCommon.showAllListItems[0] || 0;
								let showUndefined = chartConfig.mixedDataCommon.showUndefined[0] || 0;
								let sortField = "order_no";
								let listProcessFields = $scope.chartData.listProcessFields;

								if (listProcessFields.list_order != null && listProcessFields.list_order.length > 0) {
									sortField = listProcessFields.list_order;
								}
								sortField = sortField.toLowerCase();

								if (showAll == 1) {
									angular.forEach(categoryItems, function (items) {
										let name = "";
										name = items.list_item;
										if (items.hasOwnProperty(fieldWithLanguage)) {
											$scope.listItemTranslations[name] = items[fieldWithLanguage];
										}

										let lItem = items;
										lItem["name"] = name;
										lItem["color"] = lItem.list_symbol_fill;
										if (tempArr.indexOf(lItem) == -1) {
											tempArr.push(lItem);
											dataArr.push([]);
										}

										//if (tempArr.indexOf({
										//	name: name,
										//	order_value: items[sortField]
										//}) == -1) {
										//	tempArr.push({
										//		name: name,
										//		order_value: items[sortField],
										//		color: items.list_symbol_fill,
										//		item_id: items.item_id
										//	});
										//	dataArr.push([]);
										//}
									});
								} else {
									let temCArr = [];
									angular.forEach(rawData, function (rowObject) {
										let name = "";
										if (rowObject[xAxisInfo.xAxisFieldType.name]) {
											let listItem = categoryItems.filter(function (x) {
												return x.item_id == rowObject[xAxisInfo.xAxisFieldType.name];
											});

											if (listItem.length > 0) name = listItem[0].list_item;

											if (listItem.length > 0 && temCArr.indexOf(listItem[0].list_item) == -1) {
												if (listItem.length > 0 && listItem[0].hasOwnProperty(fieldWithLanguage)) {
													$scope.listItemTranslations[name] = listItem[0][fieldWithLanguage];
												}

												temCArr.push(listItem[0].list_item);

												listItem[0]["name"] = name;
												listItem[0]["color"] = listItem[0].list_symbol_fill;
												tempArr.push(listItem[0]);
												//tempArr.push({
												//	name: name,
												//	order_value: listItem[0][sortField],
												//	color: listItem[0].list_symbol_fill,
												//	item_id: listItem[0].item_id
												//});
												dataArr.push([]);
											}
										}
									});
								}

								if (sortField == "list_item" && tempArr.length > 0 && tempArr[0].name in $scope.listItemTranslations) sortField = fieldWithLanguage;
								angular.forEach(tempArr.sort(dynamicSort(sortField)), function (item) {
									labels.push(item.name);
									$scope.xAxisListItemsWithColor.push(item);
								});

								if (showUndefined == 1) {
									labels.push(Translator.translate('MIXED_CHART_UNDEFINED'));
									tempArr.push({
										name: Translator.translate('MIXED_CHART_UNDEFINED'),
										color: "light grey"
									});
									$scope.xAxisListItemsWithColor.push({
										name: Translator.translate('MIXED_CHART_UNDEFINED'),
										color: "light grey"
									});
									dataArr.push([]);
								}

								//$scope.xAxisListItemsWithColor = tempArr;
								angular.forEach(rawData, function (rowObject) {
									if (rowObject[xAxisInfo.xAxisFieldType.name]) {
										let listItem = categoryItems.filter(function (x) {
											return x.item_id == rowObject[xAxisInfo.xAxisFieldType.name];
										});

										if (listItem[0] || showUndefined == 1) {
											let categoryName = "";
											if (listItem.length > 0) {
												categoryName = listItem[0].list_item;
											} else if (showUndefined == 1) {
												categoryName = Translator.translate('MIXED_CHART_UNDEFINED');
											}

											if (dataArr[labels.indexOf(categoryName)].length == 0) {
												dataArr[labels.indexOf(categoryName)] = new Array(_.size(originalConfigFields.mixedData));
												for (let i = 0; i < _.size(originalConfigFields.mixedData); i++) {
													dataArr[labels.indexOf(categoryName)][i] = new Array();
												}
											}

											let chartElementIndex = 0;
											angular.forEach(originalConfigFields.mixedData, function (element) {
												let val = null;
												let useCount = element.useCount || 0;
												if (useCount == 1) {
													if (useListMode != 1) {
														val = 1;
													} else {
														if (rowObject[element.compareField] == element.compareItemId) {
															val = 1;
														}
														//else {
														//	val = 0;
														//}
													}
												} else {
													val = rowObject[element.dataSource];
												}
												dataArr[labels.indexOf(categoryName)][chartElementIndex].push({
													value: val,
													name: categoryName,
													ItemID: rowObject["item_id"]
												});
												chartElementIndex += 1;
											});
										}
									}
								});

								break;
							case 20: //  -- int,double,string -- and actually all int,double,string from subquary/equation and normal string
								dataArr = [];
								if (xAxisInfo.xAxisFieldType.name != null || xAxisInfo.xAxisFieldType.name != "") {
									//rawData.sort(dynamicSort(xAxisInfo.xAxisFieldType.name));
									let undefinedN = Translator.translate('MIXED_CHART_UNDEFINED');
									let xFieldName = xAxisInfo.xAxisFieldType.name;
									rawData.sort(function (a, b) {
										let aName = a[xFieldName];
										let bName = b[xFieldName];

										if (aName == null || aName == "") aName = undefinedN;
										if (bName == null || bName == "") bName = undefinedN;

										if (aName < bName)
											return -1;
										if (aName > bName)
											return 1;
										return 0;
									})
								}

								angular.forEach(rawData, function (rowObject) {
									//if (rowObject[xAxisInfo.xAxisFieldType.name].toString().length > 0) {
									if (true) {
										let categoryName = "";
										if (rowObject[xAxisInfo.xAxisFieldType.name] != null && rowObject[xAxisInfo.xAxisFieldType.name] != "") {
											categoryName = rowObject[xAxisInfo.xAxisFieldType.name].toString();
										} else {
											categoryName = Translator.translate('MIXED_CHART_UNDEFINED');
										}
										if (labels.indexOf(categoryName) == -1) {
											labels.push(categoryName);
											dataArr.push([]);
											dataArr[labels.indexOf(categoryName)] = new Array(_.size(originalConfigFields.mixedData));
											for (let i = 0; i < _.size(originalConfigFields.mixedData); i++) {
												dataArr[labels.indexOf(categoryName)][i] = new Array();
											}
										}
										let chartElementIndex = 0;
										angular.forEach(originalConfigFields.mixedData, function (element) {
											let val = null;
											let useCount = element.useCount || 0;
											if (useCount == 1) {
												if (useListMode != 1) {
													val = 1;
												} else {
													if (rowObject[element.compareField] == element.compareItemId) {
														val = 1;
													}
													//else {
													//	val = 0;
													//}
												}
											} else {
												val = rowObject[element.dataSource];
											}
											dataArr[labels.indexOf(categoryName)][chartElementIndex].push({
												value: val,
												name: categoryName,
												ItemID: rowObject["item_id"]
											});
											chartElementIndex += 1;
										});
									}
								});
								break;
							default:
							//It should never come here.
						}

						let finalData = new Array(_.size(originalConfigFields.mixedData));
						let finalIds = new Array(_.size(originalConfigFields.mixedData));
						//let finalZeros = new Array(_.size(originalConfigFields.mixedData));
						//let finalNulls = new Array(_.size(originalConfigFields.mixedData));

						for (let i = 0; i < _.size(originalConfigFields.mixedData); i++) {
							finalData[i] = new Array();
							finalIds[i] = new Array();
						}

						let cumulativeTemp = [];
						angular.forEach(dataArr, function (labelRow) {
							let elementCount = 0;

							for (let i = 0; i < labelRow.length; i++) {
								let elementProperties = originalConfigFields.mixedData[i];
								let ignoreNulls = originalConfigFields.mixedData[i].ignoreNulls || 0;
								let ignoreZeros = originalConfigFields.mixedData[i].ignoreZeros || 0;

								let finalValue = null;
								let tempIds = [];
								if ($scope.gradientMode == 1 && $scope.hasXAxis == false) {
									elementProperties.mode[0] = 4; // Forcing avarage for gradient mode
								}
								let calcMode = elementProperties.mode[0] || 1;

								switch (calcMode) {// Mode Sum,Max,Min,Avatage,Cumulative,Last value
									case 1: //Sum
										angular.forEach(labelRow[i], function (valueObj) {
											if (valueObj.value != null) {
												finalValue += valueObj.value;
											}

											if (ignoreNulls == 1 || ignoreZeros == 1) {
												if (ignoreNulls == 1 && ignoreZeros == 1) {
													if (valueObj.value != null && valueObj.value != 0) {
														tempIds.push(valueObj.ItemID);
													}
												} else if (ignoreNulls == 1 && valueObj.value != null) {
													tempIds.push(valueObj.ItemID);
												} else if (ignoreZeros == 1 && (valueObj.value != 0 && valueObj.value != null)) {
													tempIds.push(valueObj.ItemID);
												}
											} else {
												tempIds.push(valueObj.ItemID);
											}
										});
										finalData[elementCount].push(setDecimals(finalValue, $scope.decimalNumbersPerItem[elementCount]));

										finalIds[elementCount].push(tempIds);
										break;
									case 2: //MAX
										angular.forEach(labelRow[i], function (valueObj) {
											if (finalValue == 0 || finalValue == null || finalValue < valueObj.value) {
												if (ignoreNulls == 1 || ignoreZeros == 1) {
													if (ignoreNulls == 1 && ignoreZeros == 1) {
														if (valueObj.value != null && valueObj.value != 0) {
															finalValue = valueObj.value;
															tempIds.push(valueObj.ItemID);
														}
													} else if (ignoreNulls == 1 && valueObj.value != null) {
														finalValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													} else if (ignoreZeros == 1 && (valueObj.value != 0 || valueObj.value != null)) {
														finalValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													}
												} else {
													finalValue = valueObj.value || null;
													tempIds.push(valueObj.ItemID);
												}

											} else {
												if (ignoreNulls == 1 || ignoreZeros == 1) {
													if (ignoreNulls == 1 && ignoreZeros == 1) {
														if (valueObj.value != null && valueObj.value != 0) {
															tempIds.push(valueObj.ItemID);
														}
													} else if (ignoreNulls == 1 && valueObj.value != null) {
														tempIds.push(valueObj.ItemID);
													} else if (ignoreZeros == 1 && (valueObj.value != 0)) {
														tempIds.push(valueObj.ItemID);
													}
												} else {
													tempIds.push(valueObj.ItemID);
												}
											}
										});
										finalData[elementCount].push(setDecimals(finalValue, $scope.decimalNumbersPerItem[elementCount]));
										finalIds[elementCount].push(tempIds);
										break;
									case 3: //MIN
										angular.forEach(labelRow[i], function (valueObj) {
											if (finalValue == 0 || finalValue > valueObj.value) {
												if (ignoreNulls == 1 || ignoreZeros == 1) {
													if (ignoreNulls == 1 && ignoreZeros == 1) {
														if (valueObj.value != null && valueObj.value != 0) {
															finalValue = valueObj.value;
															tempIds.push(valueObj.ItemID);
														}
													} else if (ignoreNulls == 1 && valueObj.value != null) {
														finalValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													} else if (ignoreZeros == 1 && (valueObj.value != 0 || valueObj.value != null)) {
														finalValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													}
												} else {
													finalValue = valueObj.value || null;
													tempIds.push(valueObj.ItemID);
												}

											} else {
												if (ignoreNulls == 1 || ignoreZeros == 1) {
													if (ignoreNulls == 1 && ignoreZeros == 1) {
														if (valueObj.value != null && valueObj.value != 0) {
															tempIds.push(valueObj.ItemID);
														}
													} else if (ignoreNulls == 1 && valueObj.value != null) {
														tempIds.push(valueObj.ItemID);
													} else if (ignoreZeros == 1 && (valueObj.value != 0)) {
														tempIds.push(valueObj.ItemID);
													}
												} else {
													tempIds.push(valueObj.ItemID);
												}
											}
										});
										finalData[elementCount].push(setDecimals(finalValue, $scope.decimalNumbersPerItem[elementCount]));
										finalIds[elementCount].push(tempIds);
										break;
									case 4: //Avarage
										let tempSum = 0;
										let tempCount = 0;
										angular.forEach(labelRow[i], function (valueObj) {
											if (ignoreNulls == 1 || ignoreZeros == 1) {
												if (ignoreNulls == 1 && ignoreZeros == 1) {
													if (valueObj.value != null && valueObj.value != 0) {
														tempSum += valueObj.value;
														tempCount += 1;
														tempIds.push(valueObj.ItemID);
													}
												} else if (ignoreNulls == 1 && valueObj.value != null) {
													tempSum += valueObj.value;
													tempCount += 1;
													tempIds.push(valueObj.ItemID);
												} else if (ignoreZeros == 1 && (valueObj.value != 0 && valueObj.value != null)) {
													tempSum += valueObj.value;
													tempCount += 1;
													tempIds.push(valueObj.ItemID);
												}
											} else {
												tempSum += valueObj.value || null;
												tempCount += 1;
												tempIds.push(valueObj.ItemID);
											}
										});

										finalValue = tempSum / tempCount || null;
										finalData[elementCount].push(setDecimals(finalValue, $scope.decimalNumbersPerItem[elementCount]));
										finalIds[elementCount].push(tempIds);
										break;
									case 5: //Cumulative
										let labelIndex = dataArr.indexOf(labelRow);
										let currentLabel = labels[labelIndex];
										let comparisonDate;

										switch (dateMode) {
											case 1:
												comparisonDate = new Date(currentLabel.split("/")[2], parseInt(currentLabel.split("/")[1], 10), parseInt(currentLabel.split("/")[0], 10));
												//comparisonDate.setDate(comparisonDate.getDate() + 1);
												break;
											case 2:
												comparisonDate = moment().year(currentLabel.split("/")[1]).week(currentLabel.split("/")[0]).toDate();
												//comparisonDate.setDate(comparisonDate.getDay() + 7);
												break;
											case 3:
												comparisonDate = new Date(currentLabel.split("/")[1], currentLabel.split("/")[0] - 1, 1);
												break;
											case 4:
												comparisonDate = new Date(currentLabel.split("/")[1], (parseInt(currentLabel.split("/")[0], 10) * 3) - 1, 1);
												break;
											case 5:
												comparisonDate = new Date(currentLabel, 0, 1);
												break;
											default:
												break;
										}

										angular.forEach(labelRow[i], function (valueObj) {
											if (ignoreNulls == 1 || ignoreZeros == 1) {
												if (ignoreNulls == 1 && ignoreZeros == 1) {
													if (valueObj.value != null && valueObj.value != 0) {
														let itemlValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
														cumulativeTemp.push({
															item_id: valueObj.ItemID,
															value: itemlValue,
															endDate: valueObj.endDate,
															elem: elementCount
														});
													}
												} else if (ignoreNulls == 1 && valueObj.value != null) {
													let itemlValue = valueObj.value;
													tempIds.push(valueObj.ItemID);
													cumulativeTemp.push({
														item_id: valueObj.ItemID,
														value: itemlValue,
														endDate: valueObj.endDate,
														elem: elementCount
													});
												} else if (ignoreZeros == 1 && (valueObj.value != 0 && valueObj.value != null)) {
													let itemlValue = valueObj.value;
													tempIds.push(valueObj.ItemID);
													cumulativeTemp.push({
														item_id: valueObj.ItemID,
														value: itemlValue,
														endDate: valueObj.endDate,
														elem: elementCount
													});
												}
											} else {
												let itemlValue = valueObj.value;
												tempIds.push(valueObj.ItemID);
												cumulativeTemp.push({
													item_id: valueObj.ItemID,
													value: itemlValue,
													endDate: valueObj.endDate,
													elem: elementCount
												});
											}
										});

										let idsToBeRemoved = [];
										angular.forEach(cumulativeTemp, function (cObj) {
											if (cObj.endDate != null && chartConfig.mixedDataCommon.cumulativeEnd[0] != null && chartConfig.mixedDataCommon.cumulativeEnd[0] != '') {
												let endDate = new Date(cObj.endDate);
												switch (dateMode) {
													case 1:
														//No need for this handlijng??
														break;
													case 2:
														endDate = moment().year(endDate.getFullYear()).week(getWeekNumber(endDate)).toDate();
														break;
													case 3:
														endDate = new Date(endDate.getFullYear(), endDate.getMonth(), 1);
														break;
													case 4:
														endDate = new Date(endDate.getFullYear(), getQuaterNumber(endDate) * 3, 1);
														break;
													case 5:
														endDate = new Date(endDate.getFullYear(), 0, 1);
														break;
												}

												if (endDate >= new Date(comparisonDate)) {
													if (cObj.elem == elementCount) {
														finalValue += cObj.value || null;
													}

												} else {
													idsToBeRemoved.push(cObj.item_id);
												}
											} else {
												if (cObj.elem == elementCount) {
													finalValue += cObj.value;
												}
											}
										});

										finalData[elementCount].push(setDecimals(finalValue, $scope.decimalNumbersPerItem[elementCount]));
										let currentIds = tempIds;
										if (finalIds[elementCount].length > 0) {
											angular.forEach(finalIds[elementCount][finalIds[elementCount].length - 1], function (id) {
												if (currentIds.indexOf(id) == -1 && idsToBeRemoved.indexOf(id) == -1) currentIds.push(id);
											});
										}
										finalIds[elementCount].push(currentIds);
										break;
									case 6: // Create exclution if labels aren't in datetime mode - last value
										let lastValueDate = false;
										let valueCount = 1;

										let tempValue = 0;
										angular.forEach(labelRow[i], function (valueObj) {
											let tempD = new Date(valueObj.date.getFullYear(), valueObj.date.getMonth() + 1, valueObj.date.getDay());

											if (ignoreNulls == 1 || ignoreZeros == 1) {
												if (ignoreNulls == 1 && ignoreZeros == 1) {
													if (valueObj.value != null && valueObj.value != 0) {

														if (lastValueDate == false) {
															lastValueDate = tempD;
															tempValue = valueObj.value;
															tempIds.push(valueObj.ItemID);
														} else if (tempD == lastValueDate) {
															valueCount += 1;
															tempValue += valueObj.value;
															tempIds.push(valueObj.ItemID);
														} else if (tempD > lastValueDate) {
															valueCount = 1;
															lastValueDate = tempD;
															tempValue = valueObj.value;
															tempIds.push(valueObj.ItemID);
														}
													}
												} else if (ignoreNulls == 1 && valueObj.value != null) {

													if (lastValueDate == false) {
														lastValueDate = tempD;
														tempValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													} else if (tempD == lastValueDate) {
														valueCount += 1;
														tempValue += valueObj.value;
														tempIds.push(valueObj.ItemID);
													} else if (tempD > lastValueDate) {
														valueCount = 1;
														lastValueDate = tempD;
														tempValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													}
												} else if (ignoreZeros == 1 && (valueObj.value != 0 && valueObj.value != null)) {

													if (lastValueDate == false) {
														lastValueDate = tempD;
														tempValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													} else if (tempD == lastValueDate) {
														valueCount += 1;
														tempValue += valueObj.value;
														tempIds.push(valueObj.ItemID);
													} else if (tempD > lastValueDate) {
														valueCount = 1;
														lastValueDate = tempD;
														tempValue = valueObj.value;
														tempIds.push(valueObj.ItemID);
													}
												}
											} else {

												if (lastValueDate == false) {
													lastValueDate = tempD;
													tempValue = valueObj.value;
													tempIds.push(valueObj.ItemID);
												} else if (tempD == lastValueDate) {
													valueCount += 1;
													tempValue += valueObj.value;
													tempIds.push(valueObj.ItemID);
												} else if (tempD > lastValueDate) {
													valueCount = 1;
													lastValueDate = tempD;
													tempValue = valueObj.value;
													tempIds.push(valueObj.ItemID);
												}
											}

											finalValue = tempValue / valueCount || null;
										});
										finalData[elementCount].push(setDecimals(finalValue, $scope.decimalNumbersPerItem[elementCount]));
										finalIds[elementCount].push(tempIds);
										break;
									default:
									// Just placeholder for now                                     
								}
								elementCount += 1;
							}
							if (labelRow.length == 0 && sourceDataType == 6) {
								for (let i = 0; i < _.size(originalConfigFields.mixedData); i++) {
									finalData[i].push(setDecimals(0, $scope.decimalNumbersPerItem[i]));
									finalIds[i].push(0);
								}
							}
						});

						//zoom only date
						if (zoomExcludes.length > 0) {
							for (let i = zoomExcludes.length - 1; i > -1; i--) {
								labels.splice(zoomExcludes[i], 1);
								angular.forEach(finalData, function (elements) {
									elements.splice(zoomExcludes[i], 1);
								});
								angular.forEach(originalConfigFields.mixedData, function (element) {
									finalIds[originalConfigFields.mixedData.indexOf(element)].splice(zoomExcludes[i], 1);
								});
							}
						}

						if (Object.keys($scope.listItemTranslations).length > 0) {
							for (let i = 0; i < labels.length; i++) {
								if (labels[i] in $scope.listItemTranslations) {
									labels[i] = $scope.listItemTranslations[labels[i]];
								}
							}
						}

						angular.forEach(labels, function (label) {
							let timeType = chartConfig.mixedDataCommon.dateType[0] || 0;
							if (sourceDataType == 3 && timeType != 5) {
								$scope.tableLabels.push(label.replace(/ /g, ''));
							} else {
								$scope.tableLabels.push(label);
							}

							$scope.labelsL.push(label);
							labels[labels.indexOf(label)] = checkLabelLength(label);

						});

						$scope.mixedLabels = labels;

						if ($scope.gradientMode == 1 && $scope.hasXAxis == false) {
							$scope.tableData = angular.copy(finalData);
							$scope.mixedData = [finalData.length - 1];
							for (let i = 0; i < finalData.length; i++) {
								$scope.mixedData[i] = ["100"];
							}
							$scope.gradientDataCopy = finalData;
						} else {
							$scope.mixedData = finalData;
							$scope.tableData = angular.copy(finalData);
						}

						$scope.finalIds = finalIds;
						let index = 0;
						// Calc min max scale without stacks
						angular.forEach(originalConfigFields.mixedData, function (element) {
							angular.forEach(finalData[index], function (value) {
								if (typeof $scope.datasetScaleObject[index] === 'undefined') {
									let itemType = "";
									if (element.type[0] == 1) {
										itemType = "line";
									} else {
										itemType = "bar";
									}
									$scope.datasetScaleObject[index] = {
										min: value || 0,
										max: value || 0,
										isShown: true,
										stack: element.stack,
										itemType: itemType,
										index: originalConfigFields.mixedData.indexOf(element)
									};
								} else {
									if (parseInt(value) < parseInt($scope.datasetScaleObject[index].min)) {
										$scope.datasetScaleObject[index].min = parseInt(value);
									}
									if (parseInt(value) > parseInt($scope.datasetScaleObject[index].max)) {
										$scope.datasetScaleObject[index].max = parseInt(value);
									}
								}

							});
							index += 1;
						});

						$scope.mixedColors = [];
						$scope.categoryNames = [];

						let colorArr = Colors.getColors();

						angular.forEach(ChartColourService.getColours(originalConfigFields.baseColor), function (color) {

							$scope.mixedColors.push({
								backgroundColor: color,
								pointBackgroundColor: color,
								hoverBackgroundColor: color,
								borderColor: color,
								fill: false
							});
						});

						$scope.mixedDatasetOverride = [];
						let lineStacks = [];
						let barsNotInStack = 0;
						let overrideCount = 0;
						let lineNotInStackIndex = -5000;

						//Ignore empty labels
						if (chartConfig.mixedDataCommon.ignoreEmpties && $scope.hasXAxis) {
							let ignoreEmpties = chartConfig.mixedDataCommon.ignoreEmpties[0] || 0;
							if (ignoreEmpties == 1 && finalIds.length > 0) {
								let notNeeded = [];
								for (let i = 0; i < finalIds.length; i++) {
									angular.forEach(finalIds[i], function (idsArr) {
										if (idsArr.length == 0) {
											notNeeded.push(finalIds[i].indexOf(idsArr));
										}
									});
								}
								let toBeRemoved = [];
								for (let i = 0; i < notNeeded.length; i++) {
									let hits = notNeeded.filter(function (x) {
										return x == notNeeded[i];
									});
									if (hits.length == finalIds.length) {
										if (toBeRemoved.indexOf(notNeeded[i]) == -1) toBeRemoved.push(notNeeded[i]);
									}
								}
								toBeRemoved.sort(function (a, b) {
									return a - b
								});
								toBeRemoved.reverse();
								for (let i = 0; i < toBeRemoved.length; i++) {
									for (let j = 0; j < finalIds.length; j++) {
										$scope.finalIds[j].splice(toBeRemoved[i], 1);
										$scope.mixedData[j].splice(toBeRemoved[i], 1);
										$scope.tableData[j].splice(toBeRemoved[i], 1);
									}
									$scope.mixedLabels.splice(toBeRemoved[i], 1);
									$scope.tableLabels.splice(toBeRemoved[i], 1);
									$scope.labelsL.splice(toBeRemoved[i], 1);
								}
							}
						}

						angular.forEach(originalConfigFields.mixedData, function (element) {
							if (element.customColor != null && element.customColor[0] != null && $scope.gradientMode == 0) {
								let colorSelection = "";
								let colorName = element.customColor[0];

								if (colorName.indexOf(' ') > -1) {
									let splitted = colorName.split(" ");
									colorSelection = colorArr[splitted[0] + splitted[1].charAt(0).toUpperCase() + splitted[1].slice(1)];
								} else {
									colorSelection = colorArr[colorName];
								}

								if (element.customColor != null) {
									$scope.mixedColors[originalConfigFields.mixedData.indexOf(element)] = {
										backgroundColor: colorSelection.hex,
										pointBackgroundColor: colorSelection.hex,
										hoverBackgroundColor: colorSelection.hex,
										borderColor: colorSelection.hex,
										fill: false
									};
								}
							}

							let type = "";
							if (element.type[0] == 1) {
								type = "line";
							} else {
								if ($scope.horizontalBarMode == 0) {
									type = "bar";
								} else {
									type = "horizontalBar";
								}
							}
							$scope.categoryNames.push(element.nameField[$rootScope.clientInformation["locale_id"]]);

							let elementObj = {
								type: type,
								label: element.nameField[$rootScope.clientInformation["locale_id"]]
							};

							if (element.stack != null) {
								let stackedElementIndexes = [];
								if (type == "line") {
									if (lineStacks.indexOf('y-axis-' + (element.stack + 2)) == -1) {
										lineStacks.push('y-axis-' + (element.stack + 2));
										elementObj["yAxisID"] = lineStacks[lineStacks.indexOf('y-axis-' + (element.stack + 2))];
									} else {
										elementObj["yAxisID"] = lineStacks[lineStacks.indexOf('y-axis-' + (element.stack + 2))];
									}

									if (element.lineFillType[0] == 1 || element.lineFillType[0] == 2) $scope.mixedColors[overrideCount].borderColor = "white";

									let count = 0;
									angular.forEach(originalConfigFields.mixedData, function (x) {
										if (x.stack == element.stack && x.type[0] == 1) {
											stackedElementIndexes.push(count);
										}
										count++;
									});
								} else {
									elementObj["stack"] = element.stack;
									let count = 0;
									angular.forEach(originalConfigFields.mixedData, function (x) {
										if (x.stack == element.stack && x.type[0] == 2) {
											stackedElementIndexes.push(count);
										}
										count++;
									});
								}
							} else if (type == "line") {
								elementObj["yAxisID"] = 'y-axis-' + lineNotInStackIndex;
								lineStacks.push('y-axis-' + lineNotInStackIndex);
								lineNotInStackIndex += 1;
							} else {
								elementObj["stack"] = "bar" + barsNotInStack;
								barsNotInStack += 1;
							}

							if (element.lineFillType[0] == 0) {
								if (type == "bar") {
									elementObj["fill"] = true;
								}
							}
							if (element.lineFillType[0] == 1) {
								elementObj["fill"] = true;
								if ($scope.gradientMode == 0) {
									elementObj["backgroundColor"] = convertHex($scope.mixedColors[overrideCount].backgroundColor, 50);
								}
							} else if (element.lineFillType[0] == 2) {
								elementObj["fill"] = true;
							}

							if ($scope.useListColors) {
								elementObj.backgroundColor = [];
								elementObj.pointBackgroundColor = [];
								elementObj.hoverBackgroundColor = [];
								elementObj.borderColor = [];

								let tempColors = [];

								angular.forEach(colorArr, function (color) {
									tempColors.push(color);
								});

								for (let i = 0; i < $scope.xAxisListItemsWithColor.length; i++) {
									let color = tempColors.filter(function (x) {
										return x.name == $scope.xAxisListItemsWithColor[i].color;
									});
									if (color.length > 0) {
										elementObj.backgroundColor.push(color[0].hex);
										elementObj.pointBackgroundColor.push(color[0].hex);
										elementObj.hoverBackgroundColor.push(color[0].hex);
										elementObj.borderColor.push(color[0].hex);
									}
								}
							} else if ((type == "bar" || type == "horizontalBar") && $scope.mixedData.length == 1 && (originalConfigFields.mixedData[0].customColor == null || originalConfigFields.mixedData[0].customColor.length == 0)) {
								while ($scope.mixedColors.length < $scope.mixedLabels.length) {
									angular.forEach(angular.copy($scope.mixedColors), function (color) {
										$scope.mixedColors.push(color);
									});
								}

								//customColor 
								elementObj.backgroundColor = [];
								elementObj.pointBackgroundColor = [];
								elementObj.hoverBackgroundColor = [];
								elementObj.borderColor = [];

								for (let i = 0; i < $scope.mixedLabels.length; i++) {
									elementObj.backgroundColor.push($scope.mixedColors[i].backgroundColor);
									elementObj.pointBackgroundColor.push($scope.mixedColors[i].pointBackgroundColor);
									elementObj.hoverBackgroundColor.push($scope.mixedColors[i].hoverBackgroundColor);
									elementObj.borderColor.push($scope.mixedColors[i].borderColor);
								}
							}

							if (type == "line") {
								let lineGraphicStyle = element.lineGraphicStyle || "curvedLine";
								let linePointStyle = element.linePointStyle || "circle";

								switch (lineGraphicStyle[0]) {
									case ("invisible"):
										elementObj["showLine"] = false;
										break;
									case ("curvedLine"):
										// Nothing to do here for now
										break;
									case ("curvedDashedLine"):
										elementObj["borderDash"] = [6, 6];
										break;
									case ("straightLine"):
										elementObj["tension"] = 0;
										break;
									case ("straightDashedLine"):
										elementObj["tension"] = 0;
										elementObj["borderDash"] = [6, 6];
										break;
									case ("steppedLine"):
										elementObj["steppedLine"] = true;
										break;
									case ("steppedDashedLine"):
										elementObj["steppedLine"] = true;
										elementObj["borderDash"] = [10, 5];
										break;
								}

								elementObj["pointStyle"] = linePointStyle[0];

								if (element.markerSize != null) {
									let markerSize = element.markerSize;
									elementObj["radius"] = markerSize;
									elementObj["hoverRadius"] = markerSize * 1.35;
								}
							}
							$scope.mixedDatasetOverride.push(elementObj);
							overrideCount += 1;
						});

						//Logic for sum rows
						if ($scope.tableData.length > 0) {
							$scope.normalTableRows = angular.copy($scope.tableData.length);
							$scope.normalTableColumns = angular.copy($scope.tableData[0].length);
						}

						let sumMode = 0;
						if (chartConfig.mixedDataCommon.sumMode) sumMode = chartConfig.mixedDataCommon.sumMode[0] || 1;

						if ($scope.xtableSumInUse) {
							angular.forEach(finalData, function (arr) {
								let sum = null;
								for (let i = 0; i < arr.length; i++) {
									if (arr[i] != null) {
										switch (sumMode) {
											case (1):
												if (sum != null && arr[i] != null) {
													sum += parseFloat(arr[i]);
												} else {
													sum = parseFloat(arr[i]);
												}
												break;
											case (2):
												if (sum != null && arr[i] != null) {
													if (parseFloat(arr[i]) > sum) sum = parseFloat(arr[i]);
												} else if (parseFloat(arr[i]) != null) {
													sum = parseFloat(arr[i]);
												}
												break;
											case (3):
												if (sum != null && arr[i] != null) {
													if (parseFloat(arr[i]) < parseFloat(sum)) sum = parseFloat(arr[i]);
												} else if (parseFloat(arr[i]) != null) {
													sum = parseFloat(arr[i]);
												}
												break;
										}
									}
								}
								$scope.tableData[finalData.indexOf(arr)].push(sum);
							});

							switch (sumMode) {
								case (1):
									$scope.tableLabels.push(Translator.translate('CHART_COMMON_SUM'));
									break;
								case (2):
									$scope.tableLabels.push(Translator.translate('CHART_COMMON_MAX'));
									break;
								case (3):
									$scope.tableLabels.push(Translator.translate('CHART_COMMON_MIN'));
									break;
							}
						}
						if ($scope.ytableSumInUse) {
							let totalSum = null;
							if (finalData.length > 0) {
								let newArr = new Array(finalData[0].length);

								angular.forEach(finalData, function (arr) {
									for (let i = 0; i < arr.length; i++) {
										if (arr[i] != null) {
											switch (sumMode) {
												case (1):
													if (newArr[i] != null) {
														newArr[i] += parseFloat(arr[i]);
													} else {
														newArr[i] = parseFloat(arr[i]);
													}

													if (totalSum != null) {
														totalSum += parseFloat(arr[i]);
													} else {
														totalSum = parseFloat(arr[i]);
													}
													break;
												case (2):
													if (newArr[i] != null) {
														if (parseFloat(arr[i]) > newArr[i]) newArr[i] = parseFloat(arr[i]);
													} else if (parseFloat(arr[i]) != null) {
														newArr[i] = parseFloat(arr[i]);
													}

													if (totalSum != null) {
														if (parseFloat(arr[i]) > totalSum) totalSum = parseFloat(arr[i]);
													} else if (parseFloat(arr[i]) != null) {
														totalSum = parseFloat(arr[i]);
													}
													break;
												case (3):
													if (newArr[i] != null) {
														if (parseFloat(arr[i]) < newArr[i]) newArr[i] = parseFloat(arr[i]);
													} else if (parseFloat(arr[i]) != null) {
														newArr[i] = parseFloat(arr[i]);
													}

													if (totalSum != null) {
														if (parseFloat(arr[i]) < totalSum) totalSum = parseFloat(arr[i]);
													} else if (parseFloat(arr[i]) != null) {
														totalSum = parseFloat(arr[i]);
													}
													break;
											}
										}

									}
								});

								if ($scope.xtableSumInUse) {
									newArr.push(totalSum);
								}

								$scope.tableData.push(newArr);

								switch (sumMode) {
									case (1):
										$scope.categoryNames.push(Translator.translate('CHART_COMMON_SUM'));
										break;
									case (2):
										$scope.categoryNames.push(Translator.translate('CHART_COMMON_MAX'));
										break;
									case (3):
										$scope.categoryNames.push(Translator.translate('CHART_COMMON_MIN'));
										break;
								}
							}
						}

						for (let i = 0; i < $scope.chartConfig.OriginalData.mixedData.length; i++) {
							if ($scope.chartConfig.OriginalData.mixedData[i].ignoreNulls == 1) {
								if ($scope.maxIgnore < 1) $scope.maxIgnore = 1;
							}
							if ($scope.chartConfig.OriginalData.mixedData[i].ignoreZeros == 1) {
								if ($scope.maxIgnore < 2) $scope.maxIgnore = 2;
							}
							if ($scope.decimalNumbersPerItem[i] > 0 && $scope.decimalNumbersPerItem[i] > $scope.maxDecimals) {
								$scope.maxDecimals = $scope.decimalNumbersPerItem[i];
							}
						}

						//Sum rows End

						if ($scope.gradientMode == 1 && $scope.hasXAxis == false) {
							scaleMin = 0;
							scaleMax = 100;
						} else {
							let scaleValues = calculateCustomScale($scope.datasetScaleObject, $scope.mixedData);
							scaleMin = scaleValues.min;
							scaleMax = scaleValues.max;
						}

						let showDisplayColors = true;
						if ($scope.gradientMode == 1 && $scope.hasXAxis == false) {
							showDisplayColors = false
						}

						let barPercentage = 0.9; // Default                     
						if ($scope.gradientMode == 1 && $scope.hasXAxis == false) {
							barPercentage = chartConfig.mixedDataCommon.gradientBarPercentage || 0.9;
						} else {
							if (labels.length < 10) {
								if ($scope.hasXAxis) {
									if (((labels.length) * 10.0) / 100.0 - (($rootScope.frontpage && w / $element[0].offsetWidth <= 2.0) ? 0.3 : 0.0) > 0) {
										barPercentage = ((labels.length) * 10.0) / 100.0 - (($rootScope.frontpage && w / $element[0].offsetWidth <= 2.0) ? 0.3 : 0.0);
									} else {
										barPercentage = ((labels.length) * 10.0) / 100.0;
									}
								} else {
									if ((($scope.mixedData.length) * 10.0) / 100.0 - (($rootScope.frontpage && w / $element[0].offsetWidth <= 2.0) ? 0.3 : 0.0) > 0) {
										barPercentage = (($scope.mixedData.length) * 10.0) / 100.0 - (($rootScope.frontpage && w / $element[0].offsetWidth <= 2.0) ? 0.3 : 0.0);
									} else {
										barPercentage = (($scope.mixedData.length) * 10.0) / 100.0;
									}
								}
							}
						}
						if (barPercentage == 0 || barPercentage < 0) barPercentage = 0.2; //Final fallback

						$scope.mixedOptions = {
							responsive: true,
							maintainAspectRation: false,
							legend: {
								display: false,
								position: 'bottom',
								align: 'start',
								labels: {
									fontFamily: 'roboto',
									fontSize: 12,
									padding: 10,
									boxWidth: 8,
									usePointStyle: true,
								},
								onClick: function (e, legendItem) {

								},
							},
							legendCallback: function (chart) {
								return Common.getChartLegend(chart);
							},
							scales: {
								xAxes: [
									{
										id: 'x-axis-1',
										display: true,
										gridLines: {display: showVerticalGridLines, drawBorder: true},
										stacked: true,
										scaleLabel: {
											display: true,
											labelString: xLabel
										},
										ticks: {
											autoSkip: XAxisIsDate,
											fontFamily: 'Roboto',
											fontSize: 12,
											fontColor: '#676a6e',
											callback: function (value, index, values) {
												if ($scope.hasXAxis == false && $scope.gradientMode == 1) {
													if (typeof value == "string") {
														return value;
													} else {
														let tickDecimals = numberOfDecimals(value);
														return Common.formatNumber(value, tickDecimals) + '%';
													}
												} else {
													if (typeof value == "string" || $scope.dateMode > 0 && $scope.horizontalBarMode == false) {
														return value;
													} else {
														let tickDecimals = numberOfDecimals(value);
														return Common.formatNumber(value, tickDecimals);
													}
												}
											}
										},
										barPercentage: barPercentage
									}
								],
								yAxes: [
									{
										id: 'y-axis-1',
										display: true,
										gridLines: {
											display: showHorizontalGridLines,
											drawBorder: false,
											lineWidth: 2,
											color: '#f0f1f4',
											zeroLineWidth: 2,
											zeroLineColor: '#f0f1f4'
										},
										stacked: true,
										scaleLabel: {
											display: true,
											labelString: yLabel
										},
										ticks: {
											fontFamily: 'Roboto',
											fontSize: 12,
											fontColor: '#676a6e',
											suggestedMin: scaleMin,
											suggestedMax: scaleMax,
											autoSkip: false,
											callback: function (value, index, values) {
												if ($scope.hasXAxis == false && $scope.gradientMode == 1) {
													if (typeof value == "string") {
														return value;
													} else {
														let tickDecimals = numberOfDecimals(value);
														return Common.formatNumber(value, tickDecimals) + '%';
													}
												} else {
													if (typeof value == "string" || $scope.dateMode > 0 && $scope.horizontalBarMode == true) {
														return value;
													} else {
														let tickDecimals = numberOfDecimals(value);
														return Common.formatNumber(value, tickDecimals);
													}
												}
											}
										},
										barPercentage: barPercentage
									}
								]
							},
							layout: {
								padding: {
									left: $scope.extraNoXAxisLabelLenght,
									bottom: $scope.extraNoXAxisLabelHeight
								}
							},
							onClick: function (e) {//chartConfig.mixedDataCommon
								let clickable = $scope.chartConfig.OriginalData.mixedDataCommon.clickable || 0;
								if (clickable == 1) {
									let element = this.getElementAtEvent(e);
									if (element.length > 0) {
										let itemObj = $scope.finalIds[element[0]._datasetIndex][element[0]._index];
										if (itemObj.length > 0) {
											let titleExtension = $scope.chartOptions.title.text;
											if ($scope.mixedLabels[element[0]._index] && $scope.mixedLabels[element[0]._index] != "" && $scope.categoryNames[element[0]._datasetIndex] && $scope.categoryNames[element[0]._datasetIndex] != "") {
												titleExtension += ": " + $scope.mixedLabels[element[0]._index] + " -> " + $scope.categoryNames[element[0]._datasetIndex];
											} else if ($scope.categoryNames[element[0]._datasetIndex] && $scope.categoryNames[element[0]._datasetIndex] != "") {
												titleExtension += ": " + $scope.categoryNames[element[0]._datasetIndex];
											} else {
												titleExtension += ": " + $scope.mixedLabels[element[0]._index];
											}

											let clickProcess = $scope.chartConfig.OriginalData.chartProcess;
											let clickPortfolio = $scope.chartConfig.ProcessPortfolioId;

											if ($scope.chartConfig.OriginalData.mainProcessLinkMode) {
												let linkField = $scope.chartConfig.OriginalData.link_process_field;
												clickProcess = $scope.chartConfig.OriginalData.process;
												clickPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

												if (itemObj.length > 0) {
													let dataT = $scope.rawDataFiltered.filter(function (x) {
														return itemObj.includes(x.item_id);
													});
													let parentIds = [];

													angular.forEach(dataT, function (item) {
														let tempArr = item[linkField].toString().split(",");

														for (let i = 0; i < tempArr.length; i++) {
															if (parentIds.includes(tempArr[i]) == false) parentIds.push(tempArr[i]);
														}

													});
													itemObj = parentIds;
												}
											}


											ViewService.view('portfolio', {
													params: {
														portfolioId: clickPortfolio,
														restrictions: {0: itemObj},
														process: clickProcess,
														chartForceFlatMode: true,
														titleExtension: titleExtension
													}
												},
												{
													split: true, size: 'normal'
												}
											);
										}
									}
								}
							},
							tooltips: {
								mode: $scope.tooltipMode,
								displayColors: showDisplayColors,
								callbacks: {
									title: function (value) {
										if ($scope.hasXAxis) {
											return $scope.labelsL[value[0].index];
										} else {
											return $scope.noXAxisLabels[value[0].datasetIndex][$rootScope.clientInformation["locale_id"]];
										}
									},
									label: function (value) {
										let labelPrefix = "";
										if ($scope.prefix.length > 0) {
											labelPrefix = " " + $scope.prefix;
										}
										/*let name = $scope.chartConfig.OriginalData.mixedData[value.datasetIndex].nameField[$rootScope.clientInformation["locale_id"]]; tableLabels*/
										let name = $scope.categoryNames[value.datasetIndex];
										if ($scope.hasData) {
											let num = Common.formatNumber(finalData[value.datasetIndex][value.index], $scope.decimalNumbersPerItem[value.datasetIndex]) || 0;
											return name + ": " + num + labelPrefix;
										} else {
											return name + ": " + 0 + labelPrefix;
										}
									},
									footer: function (data) {
										if ($scope.toolTipSum > 0) {
											let sum = 0;
											angular.forEach(data, function (row) {
												sum += parseInt(row.value) || 0;
											});

											return Translator.translate('CHART_MIXED_TOOLTIP_SUM2') + ": " + Common.formatNumber(sum);
										}
									}
								}
							}
						};

						if ($scope.hasXAxis == false) {
							$scope.mixedOptions["animation"] =
								{
									"onProgress": function () {
										$scope.chartInstance = this.chart;
										let chartInstance = this.chart;
										if ($scope.gradientMode && $scope.gradientAnimationStatus == 0) {
											createGradient($scope, chartInstance);
										}
										if ($scope.hasData) {
											drawGradientMarkers($scope, chartInstance);
										}
										drawNoXAxisLabels(chartInstance, stackNamesByNumber, this.data, $scope);
									}
								}

							$scope.mixedOptions["tooltips"].mode = "nearest";

						}

						angular.forEach(lineStacks, function (stack) {
							let newStack = {
								id: stack,
								display: false,
								gridLines: {display: false, drawBorder: false},
								stacked: true,
								ticks: {
									suggestedMin: scaleMin,
									suggestedMax: scaleMax
								}
							};
							$scope.mixedOptions.scales.yAxes.push(newStack);
						});

						zoomInProgress = false;
					}
				};

				let refreshLayer = function () {
					if ($scope.chartConfig) {
						let xAxisInfo = $scope.chartData.fieldTypes;
						let parentColumn = xAxisInfo.xAxisColumnInfo;
						let rawData = $scope.chartData.data;

						if (xAxisInfo.xAxisFieldType == null || xAxisInfo.xAxisFieldType && xAxisInfo.xAxisFieldType.data_type != 5) {
							refreshData();
						} else {
							let runIndex = 0;
							let processName = "";
							if (parentColumn.DataType == 5) {
								processName = parentColumn.DataAdditional;
							} else {
								processName = parentColumn.SubDataAdditional;
							}
							angular.forEach(rawData, function (rowObject) {
								ProcessService.getItems(processName, rowObject[parentColumn.Name]).then(function (parents) {
									if (parents.length > 0) {
										rowObject[parentColumn.Name] = parents[0].primary;
									}
									runIndex += 1;

									if (rawData.length == runIndex) {
										$scope.chartData.data = rawData;
										refreshData();
									}
								});
							});
						}
					}
				}

				$scope.$watch('chartData', refreshLayer);

				$scope.legendOnClick = function (index) {
					let ci = Common.getChartHandle($scope.id);
					$scope.gradientAnimationStatus = 0;
					let meta = ci.getDatasetMeta(index);
					if (meta.hidden === null || meta.hidden == false) {
						meta.hidden = true;
					} else {
						meta.hidden = false;
					}

					let yAxis = ci.options.scales.yAxes;

					if ($scope.datasetScaleObject[index].isShown) {
						$scope.datasetScaleObject[index].isShown = false;
					} else {
						$scope.datasetScaleObject[index].isShown = true;
					}
					let scaleValues = calculateCustomScale($scope.datasetScaleObject, $scope.mixedData);
					angular.forEach(yAxis, function (item) {
						if ($scope.hasXAxis == false && $scope.gradientMode) {
							item.ticks.suggestedMin = 0;
							item.ticks.suggestedMax = 100;
						} else {
							item.ticks.suggestedMin = scaleValues.min;
							item.ticks.suggestedMax = scaleValues.max;
						}
					});
					ci.update();
				}

				$scope.zoomChanged = function (option) {
					if ($scope.filterData.yearSelection.length > 0) {
						zoomInProgress = true;
						if (option == 1) {
							$scope.zoomYearsChanged = true;
						} else {
							$scope.zoomYearsChanged = false;
						}
					} else {
						$scope.zoomYearsChanged = false;
						zoomInProgress = false;
						$scope.filterData.secondaryTimeSelection = [];
						$scope.zoomLevel2 = [];
					}

					ChartsService.setChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);
					$scope.savedFilteringsInUse = false;

					refreshData();
				};

				$scope.dayZoomChanged = function () {
					if ($scope.filterData.dayModeStart != null && $scope.filterData.dayModeEnd != null) {
						zoomInProgress = true;
					} else {
						zoomInProgress = false;
					}

					ChartsService.setChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);
					$scope.savedFilteringsInUse = false;

					refreshData();
				};

				$scope.filterChanged = function () {
					ChartsService.setChartFilters($scope.filterPortfolio, $scope.filterKeyParams.type, $scope.filterKeyParams.chartId, $scope.filterKeyParams.chartProcess, $scope.filterKeyParams.linkId, $scope.filterData);
					refreshData();
				}

				$scope.tableFormatter = function (num, digit, categoryIndex) {
					if (($scope.ytableSumInUse && categoryIndex != $scope.tableData.length - 1) || $scope.ytableSumInUse == false) {
						if (num != null && num != 0) {
							return Common.formatNumber(num, digit);
						} else if (num == 0) {
							let ignoreZeros = ($scope.chartConfig && $scope.chartConfig.OriginalData.mixedData[categoryIndex].ignoreZeros) || 0;
							if (ignoreZeros == 0) {
								return 0;
							} else {
								return "–";
							}
						} else {
							let ignoreNulls = ($scope.chartConfig && $scope.chartConfig.OriginalData.mixedData[categoryIndex].ignoreNulls) || 0;
							if (ignoreNulls == 0) {
								return 0;
							} else {
								return "–";
							}
						}
					} else {
						if (num != null && num != 0) {
							return Common.formatNumber(num, $scope.maxDecimals);
						} else if (num == 0) {
							if ($scope.maxIgnore < 2) {
								return 0;
							} else {
								return "–";
							}
						} else {
							if ($scope.maxIgnore < 1) {
								return 0;
							} else {
								return "–";
							}
						}
					}
				};

				$scope.tableCellStyleColor = function (categoryIndex) {
					if ($scope.tableData.length - 1 == categoryIndex && $scope.ytableSumInUse) {
						return "#eeeeee";
					} else {
						return "white";
					}
				}

				$scope.openFilteredPortfolio = function (categoryIndex, labelIndex) {
					let clickable = $scope.chartConfig.OriginalData.mixedDataCommon.clickable || 0;
					if (clickable == 1) {
						let itemObj = [];

						if ($scope.normalTableRows - 1 >= categoryIndex && $scope.normalTableColumns - 1 < labelIndex) { // X-axis sums		
							angular.forEach($scope.finalIds[categoryIndex], function (ids) {
								angular.forEach(ids, function (id) {
									if (itemObj.includes(id) == false) itemObj.push(id);
								});
							});
						} else if ($scope.normalTableRows - 1 < categoryIndex && $scope.normalTableColumns - 1 >= labelIndex) { // Sum row sums
							angular.forEach($scope.finalIds, function (row) {
								angular.forEach(row[labelIndex], function (id) {
									if (itemObj.includes(id) == false) itemObj.push(id);
								});
							});
						} else if ($scope.normalTableRows - 1 < categoryIndex && $scope.normalTableColumns - 1 < labelIndex) { // SUM row total sum
							for (let i = 0; i < $scope.normalTableRows; i++) {
								angular.forEach($scope.finalIds[i], function (ids) {
									angular.forEach(ids, function (id) {
										if (itemObj.includes(id) == false) itemObj.push(id);
									});
								});
							}
						} else {
							itemObj = $scope.finalIds[categoryIndex][labelIndex];
						}

						if (itemObj.length > 0) {
							let titleExtension = $scope.chartOptions.title.text;
							if ($scope.mixedLabels[labelIndex] && $scope.mixedLabels[labelIndex] != "" && $scope.categoryNames[categoryIndex] && $scope.categoryNames[categoryIndex] != "") {
								titleExtension += ": " + $scope.mixedLabels[labelIndex] + " -> " + $scope.categoryNames[categoryIndex];
							} else if ($scope.categoryNames[categoryIndex] && $scope.categoryNames[categoryIndex] != "") {
								titleExtension += ": " + $scope.categoryNames[categoryIndex];
							} else {
								titleExtension += ": " + $scope.mixedLabels[labelIndex];
							}

							let clickProcess = $scope.chartConfig.OriginalData.chartProcess;
							let clickPortfolio = $scope.chartConfig.ProcessPortfolioId;

							if ($scope.chartConfig.OriginalData.mainProcessLinkMode) {
								let linkField = $scope.chartConfig.OriginalData.link_process_field;
								clickProcess = $scope.chartConfig.OriginalData.process;
								clickPortfolio = $scope.chartConfig.OriginalData.main_process_portfolio_id;

								if (itemObj.length > 0) {
									let dataT = $scope.chartData.data.filter(function (x) {
										return itemObj.includes(x.item_id);
									});
									let parentIds = [];

									angular.forEach(dataT, function (item) {
										let tempArr = item[linkField].toString().split(",");

										for (let i = 0; i < tempArr.length; i++) {
											if (parentIds.includes(tempArr[i]) == false) parentIds.push(tempArr[i]);
										}

									});
									itemObj = parentIds;
								}
							}

							ViewService.view('portfolio', {
									params: {
										portfolioId: clickPortfolio,
										restrictions: {0: itemObj},
										process: clickProcess,
										chartForceFlatMode: true,
										titleExtension: titleExtension
									}
								},
								{
									split: true, size: 'normal'
								}
							);
						}
					}
				}
			}
		};

		function monthDiff(day2, day1) {
			let d1 = day1, d2 = day2;
			if (day1 < day2) {
				d1 = day2;
				d2 = day1;
			}
			d1.setDate(1);
			d2.setDate(1);

			let m = (d1.getFullYear() - d2.getFullYear()) * 12 + (d1.getMonth() - d2.getMonth());
			if (d1.getDate() < d2.getDate()) --m;
			return m;
		}

		function weekDiff(dt2, dt1) {

			let diff = (dt2.getTime() - dt1.getTime()) / 1000;
			diff /= (60 * 60 * 24 * 7);
			return Math.abs(Math.round(diff));
		}

		function dayDiff(dt2, dt1) {
			let diff = (dt2.getTime() - dt1.getTime()) / 1000;
			diff /= (60 * 60 * 24);
			return Math.abs(Math.round(diff));
		}

		function getWeekNumber(dt) {
			let tdt = new Date(dt.valueOf());
			let dayn = (dt.getDay() + 6) % 7;
			tdt.setDate(tdt.getDate() - dayn + 3);
			let firstThursday = tdt.valueOf();
			tdt.setMonth(0, 1);
			if (tdt.getDay() !== 4) {
				tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);
			}
			return 1 + Math.ceil((firstThursday - tdt) / 604800000);
		}

		function getQuaterNumber(dt) {
			let quarter = 0;
			let month = dt.getMonth() + 1;
			switch (true) {
				case (month < 4):
					quarter = 1;
					break;
				case (month < 7):
					quarter = 2;
					break;
				case (month < 10):
					quarter = 3;
					break;
				default:
					quarter = 4;
					break;
			}
			return quarter;
		}

		function getQuarterCount(dt2, dt1) {
			let yearDiffAndQuarters = (dt2.getFullYear() - dt1.getFullYear() + 1) * 4;
			let quartersInStartYear = 4 - getQuaterNumber(dt1) + 1;
			let quartersInEndYear = getQuaterNumber(dt2);
			return yearDiffAndQuarters - (4 - quartersInStartYear) - (4 - quartersInEndYear);
		}

		function convertHex(hex, opacity) {
			hex = hex.replace('#', '');
			let r = parseInt(hex.substring(0, 2), 16);
			let g = parseInt(hex.substring(2, 4), 16);
			let b = parseInt(hex.substring(4, 6), 16);

			let result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
			return result;
		}

		function checkLabelLength(originalLabel) {
			if (originalLabel != null && originalLabel.length > 35) {
				return originalLabel.substring(0, 32) + "...";
			} else {
				return originalLabel;
			}
		}

		function drawOrder(a, b) {
			let x = a.drawNumber;
			let y = b.drawNumber;
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		}

		function setDecimals(value, number) {
			if (value != null) {
				return Number(value).toFixed(number);
			} else {
				return value;
			}
		}

		function findLocale() {
			try {
				return (navigator.browserLanguage || navigator.language || navigator.userLanguage);
			} catch (e) {
				return "en-US";
			}
		}

		function dynamicSort(property) {
			let sortOrder = 1;

			if (property[0] === "-") {
				sortOrder = -1;
				property = property.substr(1);
			}

			return function (a, b) {
				if (sortOrder == -1) {
					if (typeof b[property] == 'number') {
						return b[property] - a[property];
					} else {
						if (property == "order_no") {
							if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
								return -1;
							} else if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
								return 1;
							}
							return b[property].localeCompare(a[property]);
						} else {
							return b[property].localeCompare(a[property]);
						}
					}

				} else {
					if (typeof b[property] == 'number') {
						return a[property] - b[property];
					} else {
						if (property == "order_no") {
							if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
								return -1;
							} else if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
								return 1;
							}
							return a[property].localeCompare(b[property]);
						} else {
							return a[property].localeCompare(b[property]);
						}
					}
				}
			}
		}

		function startsWithUppercase(str) {
			return str.substr(0, 1).match(/[A-Z\u00C0-\u00DC]/);
		}

		function drawNoXAxisLabels(chartInstance, stackNamesByNumber, dataL, $scope) {
			let horizontalBarMode = $scope.horizontalBarMode;
			let extraNoXAxisLabelLenght = $scope.extraNoXAxisLabelLenght;
			let datasetScaleObject = $scope.datasetScaleObject;
			let maxLabelLength = $scope.maxLabelLength;
			let maxLabelHeight = $scope.maxLabelHeight;
			//                    $scope.angledLabels = false;
			let ctx = chartInstance.ctx;
			if (horizontalBarMode == false && chartInstance.data.datasets.length > 1 && $scope.angledCalcDone == null) {
				let results = {};
				for (let i = 0; i < 2; i++) {
					let meta = chartInstance.getDatasetMeta(i).data[0]._model;
					results[i + "x"] = meta.x;
					results[i + "width"] = meta.width;
					results[i + "base"] = meta.base;
				}

				let rightSidePointOf1 = results["0x"] + results["0width"] / 2;
				let leftSidePointOf2 = results["1x"] - results["1width"] / 2;
				let spaceBetween = leftSidePointOf2 - rightSidePointOf1; //Both sides
				if (maxLabelLength >= results["0width"] + spaceBetween) {
					$scope.angledLabels = true;
					chartInstance.chart.options.layout.padding.bottom = maxLabelHeight - (chartInstance.canvas.clientHeight - results["0base"]);
					chartInstance.update();
				}
				$scope.angledCalcDone = true;
			}
			// meta.x, meta.base + 25

			ctx.textAlign = 'center';
			ctx.textBaseline = 'bottom';

			for (let i = 0; i < chartInstance.data.datasets.length; i++) {
				let data = "";
				let meta = chartInstance.getDatasetMeta(i).data[0]._model;
				if (datasetScaleObject[i].isShown) {
					let isDrawn = true;
					if (dataL.datasets[i].stack != null) {
						let stackedElements = dataL.datasets.filter(function (x) {
							return x.stack == dataL.datasets[i].stack;
						});
						if (stackedElements.length > 1) {
							let index = stackedElements.reverse().indexOf(dataL.datasets[i]);
							if (index < stackedElements.length - 1) {
								isDrawn = false;
							}
						}
					}
					if (isDrawn) {
						if (typeof dataL.datasets[i].stack == "string") {
							data = dataL.datasets[i].label;
						} else {
							data = stackNamesByNumber[dataL.datasets[i].stack] || "";
						}
						if (horizontalBarMode == 0) {
							data = checkLabelLength(data);
							let textLengthInPixels = displayTextWidth(data, "12px \"Roboto\" ,\"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif");

							ctx.fillStyle = "#666";
							ctx.font = "12px Roboto";

							if ($scope.angledLabels) {
								ctx.save()
								ctx.translate(meta.x + 5, meta.base + 20);
								ctx.rotate(-35 * Math.PI / 180);
								ctx.fillText(data, (-textLengthInPixels / 2), 0);
								ctx.restore();
							} else {
								ctx.fillText(data, meta.x, meta.base + 25);
							}
						} else {
							data = checkLabelLength(data);
							let minusWidth = displayTextWidth(data, "12px \"Roboto\" ,\"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif");

							ctx.fillStyle = "#666";
							ctx.font = "12px Roboto";
							ctx.fillText(data, chartInstance.chartArea.left - (minusWidth) + (minusWidth * 0.5) - 20, meta.y + 5);
						}
					}
				}
			}
		}

		function displayTextWidth(text, font) {
			let myCanvas = displayTextWidth.canvas || (displayTextWidth.canvas = document.createElement("canvas"));
			let context = myCanvas.getContext("2d");
			context.font = font;

			let metrics = context.measureText(text);
			return metrics.width;
		};

		function numberOfDecimals(value) {
			if (Math.floor(value) === value) return 0;
			return value.toString().split(".")[1].length || 0;
		}

		function calculateCustomScale(datasetScaleObject, mixedData) {
			let min;
			let max;
			let tempArr = [];
			angular.forEach(datasetScaleObject, function (obj) {
				tempArr.push(obj);
			});

			angular.forEach(datasetScaleObject, function (obj) {
				if (obj.isShown) {
					if (min == null || obj.min < min) min = obj.min;

					if (obj.stack != null) {
						let stackedElements = tempArr.filter(function (x) {
							return x.stack == obj.stack && x.itemType == obj.itemType;
						});

						let arr = [];
						angular.forEach(stackedElements, function (obj) {
							if (obj.isShown == true) {
								arr.push(obj.index);
							}
						});
						let index = 0;
						let tempD = [];
						angular.forEach(mixedData, function (data) {
							if (arr.indexOf(index) > -1) {
								tempD.push(data);
							}
							index += 1;
						});

						let calcTotals = {};
						angular.forEach(tempD, function (arrR) {
							for (let i = 0; i < arrR.length; i++) {
								let oldVal = calcTotals[i] || 0;
								let val = arrR[i] || 0;
								calcTotals[i] = parseInt(oldVal) + parseInt(val);
							}
						});

						angular.forEach(calcTotals, function (val) {
							if (max == null || val > max) max = val;
						});
					} else {
						if (max == null || obj.max > max) max = obj.max;
					}
				}
			});
			return {min: min, max: max};
		}

		function createGradient($scope, chartInstance) {
			let chartValuedColors = new Array(1);

			let isFirstTime = 0;

			let leftSide = chartInstance.chart.chartArea.left;
			let rightSide = chartInstance.chart.width - chartInstance.chart.chartArea.right;
			let bottomSide = chartInstance.chart.chartArea.top;
			let topSide = chartInstance.chart.height - chartInstance.chart.chartArea.bottom;

			let ctx = chartInstance.ctx;
			ctx.width = chartInstance.chart.width;
			ctx.height = chartInstance.chart.height;

			let leftInitializer = 0;
			let rightInitializer = 0;
			if ($scope.horizontalBarMode == 0) {
				leftInitializer = Math.abs(bottomSide / ctx.height);
				rightInitializer = Math.abs(topSide / ctx.height);
			} else {
				leftInitializer = Math.abs(leftSide / ctx.width);
				rightInitializer = Math.abs(rightSide / ctx.width);
			}

			let remainingPercentage = 1 - leftInitializer - rightInitializer;
			let startValue = leftInitializer;

			//let gradientColors = ["#d50000", "#dc2b00", "#e35500", "#ed9500", "#f8d400", "#ffff00", "#d9e405", "#a0bc0d", "#7aa113", "#548618", "#1b5e20"];
			//let gradientColors = ["#d50000", "#DD3300", "#E56600", "#EE9900", "#F6CC00", "#ffff00", "#D1DE06", "#A3BE0C", "#769E13", "#487E19", "#1b5e20"];
			let gradientColors = ["#F44336", "#F6682B", "#F88E20", "#FAB315", "#FCD90A", "#ffff00", "#E0F804", "#C1F109", "#A2EA0D", "#83E312", "#64DD17"];
			if ($scope.horizontalBarMode == 0 && $scope.gradientOrder == 0) gradientColors.reverse();
			if ($scope.horizontalBarMode == 1 && $scope.gradientOrder == 1) gradientColors.reverse();

			let grd;
			let barLength = 0;
			if ($scope.horizontalBarMode == 0) {
				grd = ctx.createLinearGradient(0, 0, 0, ctx.height);
				barLength = chartInstance.chart.chartArea.bottom - chartInstance.chart.chartArea.top;
			} else {
				grd = ctx.createLinearGradient(0, 0, ctx.width, 0);
				barLength = ctx.width - Math.abs(leftSide) - Math.abs(rightSide);
			}

			//Creating color blocks

			grd.addColorStop(0, gradientColors[0]); // left/bottom side side (non visible)
			grd.addColorStop(leftInitializer, gradientColors[0]);
			for (let i = 0; i < gradientColors.length; i++) {
				grd.addColorStop(startValue, gradientColors[i]);
				grd.addColorStop(startValue + remainingPercentage / gradientColors.length, gradientColors[i]);

				startValue = startValue + remainingPercentage / gradientColors.length;
			}
			grd.addColorStop(startValue, gradientColors[gradientColors.length - 1]); // Right/Top side (non visible)
			grd.addColorStop(1, gradientColors[gradientColors.length - 1]);

			for (let i = 0; i < chartInstance.data.datasets.length; i++) {
				chartInstance.data.datasets[i].backgroundColor = grd;
				chartInstance.data.datasets[i].gradientStroke = grd;
				chartInstance.data.datasets[i].hoverBackgroundColor = grd;
				chartInstance.data.datasets[i].borderColor = grd;
				chartInstance.data.datasets[i].pointBackgroundColor = grd;

				let meta = chartInstance.getDatasetMeta(i).data[0]._model;

				//Data for gradient markers
				if ($scope.horizontalBarMode == 0) {
					$scope.gradientBarInfo[i] = {
						length: barLength,
						value: $scope.gradientDataCopy[i][0],
						x: meta.x,
						y: meta.y,
						height: meta.height,
						width: meta.width,
						horizontalStartPoint: leftSide,
						startMargin: chartInstance.chart.chartArea.top
					};
				} else {
					$scope.gradientBarInfo[i] = {
						length: barLength,
						value: $scope.gradientDataCopy[i][0],
						x: meta.x,
						y: meta.y,
						height: meta.height,
						width: meta.width,
						horizontalStartPoint: leftSide,
						startMargin: leftSide
					};
				}
			}

			for (let j = 0; j < $scope.gradientDataCopy.length; j++) {
				let value = 0;
				for (let k = 0; k < gradientColors.length; k++) {
					if (parseFloat($scope.gradientDataCopy[j][0]) >= value && parseFloat($scope.gradientDataCopy[j][0]) < (value + 100 / gradientColors.length) || $scope.gradientDataCopy[j][0] > 100) {
						if ($scope.horizontalBarMode == 0) {
							let tempColors = angular.copy(gradientColors);
							if (parseFloat($scope.gradientDataCopy[j][0]) > 100) {
								chartValuedColors[j] = tempColors.reverse()[tempColors.length - 1];
							} else {
								chartValuedColors[j] = tempColors.reverse()[k];
							}
						} else {
							if (parseFloat($scope.gradientDataCopy[j][0]) > 100) {
								chartValuedColors[j] = gradientColors[gradientColors.length - 1];
							} else {
								chartValuedColors[j] = gradientColors[k];
							}
						}
					}
					value += 100 / gradientColors.length;
				}
			}
			chartInstance.update();

			// This is after update becouse otherwise it would be overridden by bar background color. This replaces legend colours
			for (let j = 0; j < $scope.gradientDataCopy.length; j++) {
				chartInstance.legend.legendItems[j].fillStyle = chartValuedColors[j];
				chartInstance.legend.legendItems[j].lineWidth = 0;
			}

			if ($scope.gradientAnimationStatus > 0) {
				isFirstTime = 1;
			}

			$scope.gradientAnimationStatus += 1;
			$scope.chartInstance = chartInstance;
			if (isFirstTime == 0) {
				//$(window).trigger('resize');
			}
		}

		function drawGradientMarkers($scope, chartInstance) {
			for (let i = 0; i < $scope.gradientDataCopy.length; i++) {
				if ($scope.datasetScaleObject[i].isShown == true) {
					let originalValue = $scope.gradientBarInfo[i].value;
					let numberOfDecimals = $scope.decimalNumbersPerItem[i];
					let valueAlarm = false;

					if ($scope.gradientBarInfo[i].value > 100) {
						$scope.gradientBarInfo[i].value = 100;
						valueAlarm = true;
					}

					let ctx = chartInstance.ctx;

					if ($scope.horizontalBarMode == 0) {
						ctx.height = chartInstance.chart.height;
						let drawPositionX = $scope.gradientBarInfo[i].x
						let drawPositionY = $scope.gradientBarInfo[i].startMargin + ($scope.gradientBarInfo[i].length / 100 * (100 - $scope.gradientBarInfo[i].value));

						// Line In newest specs this is always drawn
						ctx.beginPath();
						ctx.moveTo(drawPositionX - $scope.gradientBarInfo[i].width / 2, drawPositionY);
						ctx.strokeStyle = 'black';
						ctx.lineTo(drawPositionX + $scope.gradientBarInfo[i].width / 2, drawPositionY);
						ctx.stroke();

						if ($scope.gradientPointer == 1 || $scope.gradientPointer == 2) {
							ctx.beginPath();
							ctx.arc(drawPositionX + $scope.gradientBarInfo[i].width / 2 + 15, drawPositionY, 6, 0, 2 * Math.PI);
							ctx.strokeStyle = 'black';
							if ($scope.gradientPointer == 2) ctx.fill();
							ctx.stroke();
						}

						if (valueAlarm || ($scope.gradientAnimationExtras[i] != null && $scope.gradientAnimationExtras[i]["alarm"] != null && $scope.gradientAnimationExtras[i]["alarm"] == 1)) {
							ctx.strokeStyle = 'red';
							if ($scope.gradientAnimationExtras[i] == null) {
								$scope.gradientAnimationExtras[i] = {alarm: 1};
							}
						}

						if ($scope.gradientPointer == 3) {
							ctx.beginPath();
							ctx.lineWidthOriginal = ctx.lineWidth;
							ctx.lineWidth = 2;

							ctx.moveTo(drawPositionX + $scope.gradientBarInfo[i].width / 2 + 5, drawPositionY);
							ctx.lineTo(drawPositionX + $scope.gradientBarInfo[i].width / 2 + 5 + 11, drawPositionY);

							ctx.moveTo(drawPositionX + $scope.gradientBarInfo[i].width / 2 + 5, drawPositionY);
							ctx.lineTo(drawPositionX + $scope.gradientBarInfo[i].width / 2 + 10, drawPositionY - 5);

							ctx.moveTo(drawPositionX + $scope.gradientBarInfo[i].width / 2 + 5, drawPositionY);
							ctx.lineTo(drawPositionX + $scope.gradientBarInfo[i].width / 2 + 10, drawPositionY + 5);

							ctx.stroke();
							ctx.lineWidth = ctx.lineWidthOriginal;
							ctx.strokeStyle = 'black';
						}

						if ($scope.gradientPointer == 4) {
							ctx.fillText(setDecimals(originalValue, numberOfDecimals) + "%", drawPositionX + $scope.gradientBarInfo[i].width / 2 + 10, drawPositionY);
						}
					} else {
						ctx.width = chartInstance.chart.width;

						let drawPositionX = $scope.gradientBarInfo[i].startMargin + $scope.gradientBarInfo[i].length / 100 * $scope.gradientBarInfo[i].value;
						let drawPositionY = $scope.gradientBarInfo[i].y;

						ctx.beginPath();
						ctx.moveTo(drawPositionX, drawPositionY - $scope.gradientBarInfo[i].height / 2);
						ctx.strokeStyle = 'black';
						ctx.lineTo(drawPositionX, drawPositionY + $scope.gradientBarInfo[i].height / 2);
						ctx.stroke();

						if (valueAlarm || ($scope.gradientAnimationExtras[i] != null && $scope.gradientAnimationExtras[i]["alarm"] != null && $scope.gradientAnimationExtras[i]["alarm"] == 1)) {
							ctx.strokeStyle = 'red';
							if ($scope.gradientAnimationExtras[i] == null) {
								$scope.gradientAnimationExtras[i] = {alarm: 1};
							}
						}

						if ($scope.gradientPointer == 1 || $scope.gradientPointer == 2) {
							ctx.beginPath();
							ctx.arc(drawPositionX, (drawPositionY + $scope.gradientBarInfo[i].height / 2 + 15), 6, 0, 2 * Math.PI);
							ctx.strokeStyle = 'black';
							if ($scope.gradientPointer == 2) ctx.fill();
							ctx.stroke();
						}

						if ($scope.gradientPointer == 3) {
							ctx.beginPath();
							ctx.lineWidthOriginal = ctx.lineWidth;
							ctx.lineWidth = 2;

							ctx.moveTo(drawPositionX, (drawPositionY + $scope.gradientBarInfo[i].height / 2 + 5));
							ctx.lineTo(drawPositionX, (drawPositionY + $scope.gradientBarInfo[i].height / 2 + 5) + 11);

							ctx.moveTo(drawPositionX, (drawPositionY + $scope.gradientBarInfo[i].height / 2 + 5));
							ctx.lineTo(drawPositionX - 5, (drawPositionY + $scope.gradientBarInfo[i].height / 2 + 10));

							ctx.moveTo(drawPositionX, (drawPositionY + $scope.gradientBarInfo[i].height / 2 + 5));
							ctx.lineTo(drawPositionX + 5, (drawPositionY + $scope.gradientBarInfo[i].height / 2 + 10));

							ctx.stroke();
							ctx.lineWidth = ctx.lineWidthOriginal;
							ctx.strokeStyle = 'black';
						}

						if ($scope.gradientPointer == 4) {
							ctx.fillText(setDecimals(originalValue, numberOfDecimals) + "%", drawPositionX - displayTextWidth(setDecimals(originalValue, numberOfDecimals), "12px \"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif") / 2, drawPositionY + $scope.gradientBarInfo[i].height / 2 + 20);
						}
					}
				}
			}
		}
	}
})();
