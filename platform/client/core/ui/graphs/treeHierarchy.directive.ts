(function () {
	'use strict';
	angular
		.module('core.graphs.treeHierarchy', [])
		.directive('treeHierarchy', treeHierarchyDirective);

	treeHierarchyDirective.$inject = ['ViewService', 'Common', 'Colors'];

	function treeHierarchyDirective(ViewService, Common, Colors) {
		return {
			restrict: 'E',
			templateUrl: 'core/ui/graphs/treeHierarchy.html',
			replace: true,
			scope: {
				rows: '=',
				height: '@',
				width: '@',
				rootname: '@'
			},
			controller: function ($scope, $element) {
				var tree = {};
				var $card = $element.closest('content');

				var setSizes = function () {
					if (!$scope.width) $scope.width = $card.width();
					if (!$scope.height) $scope.height = $card.height();
				};
				setSizes();
				$scope.vertical = true;

				//Calculates Size
				var onResize = function () {
					setSizes();
					$element.width($scope.width);
					$element.height($scope.height + "px");
					d3.select(".tree-hierarchy")
						.style("width", $scope.width + "px")
						.style("height", $scope.height + "px");
				};
				Common.subscribeContentResize($scope, onResize);

				$scope.changeOrientation = function ()  {
					$scope.vertical = !$scope.vertical;
					update(tree, $scope.vertical);
					
				};
				
				//Setup data object
				var treeData = {
					"item_id": 0,
					"name": $scope.rootname,
					"children": $scope.rows
				};
				tree.treeIteration = 0;
				var levelWidth = [1];

				function childCount(level, n) {
					if (n.children && n.children.length > 0) {
						if (levelWidth.length <= level + 1) levelWidth.push(0);

						levelWidth[level + 1] += n.children.length;
						n.children.forEach(function (d) {
							childCount(level + 1, d);
						});
					}
				}

				childCount(0, treeData);

				var svgHeight = d3.max(levelWidth) * 20;
				tree.svgScale = 0.33;
				if (svgHeight > $scope.height) {
					tree.svgScale = 1 - ($scope.height / svgHeight);
				}

				tree.svg = d3
					.select($element[0])
					.append("svg")
					.attr('class', 'tree-hierarchy')
					.attr("viewBox", "-100 " + (-1 * ($scope.height) / 2) + " " + $scope.width + " " + $scope.height)
					.call(d3.zoom().scaleExtent([0.2, 2.0]).on("zoom", function () {
						tree.svg.attr("transform", d3.event.transform, d3.zoomIdentity.translate(100, 50).scale(tree.svgScale, tree.svgScale));
					}))
					.append("g");

				var nodeWidth = 20;
				var nodeHeight = 20;
				var horizontalSeparationBetweenNodes = 200;
				var verticalSeparationBetweenNodes = 20;

				if ($scope.vertical) {
					horizontalSeparationBetweenNodes = 20;
					verticalSeparationBetweenNodes = 200;
				}

				tree.treemap = d3.tree()
					.size([$scope.width, $scope.height])
					.nodeSize([nodeWidth + horizontalSeparationBetweenNodes, nodeHeight + verticalSeparationBetweenNodes])
					.separation(function (a, b) {
						return a.parent === b.parent ? 1 : 1.25;
					});

				// Assigns parent, children, height, depth
				tree.root = d3.hierarchy(treeData, function (d) {
					return d.children;
				});

				tree.root.x0 = 0;
				tree.root.y0 = 0;

				//Collapses all
				if (tree.root.children) { 
					tree.root.children.forEach(function (d) {
						d.children.forEach(function (d) {
							collapse(d);
						});
					});
				}
				update(tree, $scope.vertical);
				onResize();
			}
		};

		// Collapse the node and all it's children
		function collapse(d) {
			if (d.children) {
				d._children = d.children;
				d._children.forEach(collapse);
				d.children = null;
			}
		}

		function update(tree, vertical) {
			// Assigns the x and y position for the nodes
			var root = tree.root;
			var svg = tree.svg;
			var treemap = tree.treemap;
			var treeData = treemap(root);
			var duration = 750;

			// Compute the new tree layout. 
			var nodes = treeData.descendants(),
				links = treeData.descendants().slice(1);

			// Normalize for fixed-depth.
			nodes.forEach(function (d) {
				d.y = d.depth * 180;
			});
			var node = svg.selectAll('g.node')
				.data(nodes, function (d) {
					return d.item_id || (d.item_id = ++tree.treeIteration);
				});

			// Enter any new modes at the parent's previous position.
			var nodeEnter = node.enter().append('g')
				.attr('class', 'node')
				.attr("transform", function (d) {
					if (vertical) return "translate(" + root.y0 + "," + root.x0 + ")";
					return "translate(" + root.x0 + "," + root.y0 + ")";
				})
				.on('click', click);

			// Add Circle for the nodes
			nodeEnter.append('circle')
				.attr('class', 'node')
				.attr('r', 1e-6)
				.style("fill", function (d) {
					return d._children ? "#fff" : Colors.getColor('grey').rgb;
				});

			// Add labels for the nodes
			var component1 = "x";
			var component2 = "dy";
			if (!vertical) {
				component1 = "y";
				component2 = "dx";
			}
			nodeEnter.append('text')
				.attr(component2, ".35em")
				.attr(component1, function (d) {
					return d.children || d._children ? -13 : 13;
				})
				.attr("text-anchor", function (d) {
					return d.children || d._children ? "end" : "start";
				})
				.text(function (d) {
					var n = d.data.name;
					if (n && n.length > 14) {
						n = n.substring(0, 14) + "...";
					}
					return n;
				});

			// UPDATE
			var nodeUpdate = nodeEnter.merge(node);

			// Transition to the proper position for the node
			nodeUpdate.transition()
				.duration(duration)
				.attr("transform", function (d) {
					if (vertical) return "translate(" + d.y + "," + d.x + ")";
					return "translate(" + d.x + "," + d.y + ")";
				});

			// Update the node attributes and style
			nodeUpdate.select('circle.node')
				.attr('r', 10)
				.style("fill", function (d) {
					return d._children ? "#fff" : Colors.getColor('grey').rgb;
				})
				.attr('cursor', 'pointer');


			// Remove any exiting nodes
			var nodeExit = node.exit().transition()
				.duration(duration)
				.attr("transform", function (d) {
					if (vertical) return "translate(" + d.y + "," + d.x + ")";
					return "translate(" + d.x + "," + d.y + ")";
				})
				.remove();

			// On exit reduce the node circles size to 0
			nodeExit.select('circle')
				.attr('r', 1e-6);

			// On exit reduce the opacity of text labels
			nodeExit.select('text')
				.style('fill-opacity', 1e-6);

			// ****************** links section ***************************

			// Update the links...
			var link = svg.selectAll('path.link')
				.data(links, function (d) {
					return d.item_id;
				});

			// Enter any new links at the parent's previous position.
			var linkEnter = link.enter().insert('path', "g")
				.attr("class", "link")
				.attr('d', function (d) {
					var o = {x: d.x0, y: d.y0};
					return diagonal(o, o)
				});

			// UPDATE
			var linkUpdate = linkEnter.merge(link);

			// Transition back to the parent element position
			linkUpdate.transition()
				.duration(duration)
				.attr('d', function (d) {
					return diagonal(d, d.parent)
				});

			// Remove any exiting links
			var linkExit = link.exit().transition()
				.duration(duration)
				.attr('d', function (d) {
					var o = {x: d.x, y: d.y};
					return diagonal(o, o)
				})
				.remove();

			// Store the old positions for transition.
			nodes.forEach(function (d) {
				d.x0 = d.x;
				d.y0 = d.y;
			});

			// Toggle children on click.
			function click(d) {
				if (d.children) {
					d._children = d.children;
					d.children = null;
				} else {
					d.children = d._children;
					d._children = null;
				}

				update(tree, vertical);
			}

			// Creates a curved (diagonal) path from parent to the child nodes
			function diagonal(s, d) {
				if (vertical) return "M " + s.y + " " + s.x + " C " + ((s.y + d.y) / 2) + " " + s.x + ", " + ((s.y + d.y) / 2) + " " + d.x + ", " + d.y + " " + d.x;
				return "M " + s.x + " " + s.y + " C " + ((s.x + d.x) / 2) + " " + s.y + ", " + ((s.x + d.x) / 2) + " " + d.y + ", " + d.x + " " + d.y;
			}
		}
	}
})();
