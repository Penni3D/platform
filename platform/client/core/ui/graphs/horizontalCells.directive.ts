(function () {
	'use strict';
	angular
		.module('core.graphs.horizontalCells', [])
		.directive('horizontalCells', horizontalCellsDirective);

	horizontalCellsDirective.$inject = ['PortfolioService', 'ViewService', '$timeout', 'Common', 'CalendarService'];

	function horizontalCellsDirective(PortfolioService, ViewService, $timeout, Common, CalendarService) {
		return {
			restrict: 'E',
			templateUrl: 'core/ui/graphs/horizontalCells.html',
			replace: false,
			scope: {
				value: '=',
				header: '@?',
				footer: '<?'
			},
			controller: function ($scope, $element) {
				$scope.cellWidth = "5%";
				$scope.cellHeight = "14px";
				
				if ($scope.value == undefined) $scope.value = [];
				
				//Watch and draw
				let draw = function () {
					$scope.cellWidth = 100 / $scope.value.length + "%";
				};
				$scope.$watch('value', draw);

				//Helper functions
				$scope.getStatus = function (cell, force = undefined) {
					let d = cell ? new Date(cell.Date).getDay() : undefined;

					if (force == 1 || d == 6 || d == 0) return '#bdbdbd'; //grey
					if (force == 2 || (cell && cell.IsHoliday)) return '#ac70ac'; //purple
					

					let p = cell ? cell.Hours / cell.WorkingHours * 100 : undefined;
					if (force == 3 || p <= 75) return '#ffe97c'; //yellow
					if (force == 4 || p > 100) return '#f44335'; //red
					return '#8bc34a'; //green
				}

				$scope.getTooltip = function (cell) {
					if (cell.Hours == 0 && cell.WorkingHours == 0 || cell.IsHoliday) return $scope.fd(cell.Date); 
					return $scope.fd(cell.Date) + ": " +
						$scope.fn(cell.Hours, 2) + "h / " +
						$scope.fn(cell.WorkingHours) + "h (" +
						$scope.fn(cell.Hours / cell.WorkingHours * 100) + "%)";
				}
				$scope.fn = function(n,p = 0) {
					return Common.formatNumber(n, p);
				} 
				$scope.fd = function (d, withoutYear = false) {
					return CalendarService.formatDate(d, withoutYear);
				}
				$scope.isMonday = function(d) {
					let m = new Date(d).getDay();
					return m == 1;
				}
				$scope.isToday = function(d) {
					let m = new Date(d).getDay();
					return m == 1;
				}
			}
		};
	}
})();
