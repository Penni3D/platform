(function () {
	'use strict';

	angular
		.module('core')
		.directive('tableSelectedRows', TableSelectedRows)
		.directive('tableSelectRow', TableSelectRow)
		.directive('tableDynamic', TableDynamic)
		.directive('tableContainer', TableContainer);

	TableDynamic.$inject = [];
	function TableDynamic() {
		return {
			restrict: 'A',
			link: function (scope) {
				scope.$emit('tableColumnChanges');

				scope.$on('$destroy', function () {
					scope.$emit('tableColumnChanges');
				});
			}
		}
	}

	TableContainer.$inject = ['Common', '$compile', 'UniqueService'];
	function TableContainer(Common, $compile, UniqueService) {
		return {
			restrict: 'E',
			compile: function (tElement) {
				let cHeader = tElement.find('thead').clone();
				let cFooter = tElement.find('tfoot').clone();

				return function (scope, element) {
					let $cHeader = cHeader;
					let $cFooter = cFooter;

					let $header = element.find('thead');
					let $footer = element.find('tfoot');

					let hasHeader = $header.length;
					let hasFooter = $footer.length;

					element.on('mousedown', function (e) {
						if (e.which !== 1) {
							return;
						}

						let initScroll = element.scrollLeft();
						let initX = e.pageX;

						element.on('mouseup mouseleave', function () {
							element.off('mouseup mousemove mouseleave');
						});

						element.on('mousemove', function (e) {
							Common.removeSelection(e);
							element.scrollLeft(initScroll + initX - e.pageX);
						});
					});
					if (hasHeader || hasFooter) {
						setTimeout(function () {
							let $content = element.closest('content[view]');

							if (!$content.length) {
								return;
							}

							if (hasHeader) {
								$header.css('visibility', 'hidden');
							}

							if (hasFooter) {
								$header.css('visibility', 'hidden');
							}

							let tableId = UniqueService.get('table', true);
							let $table = element.find('table');
							let cTop = $content.offset().top;
							let cHeight = $content.height();

							let scrollFunction = function () {
							};
							let setWidthFunction = function () {
							};

							if (hasHeader && hasFooter) {
								initializeHeader();
								initializeFooter();

								scrollFunction = function (startPosition, height) {
									setHeaderScroll(startPosition, height);
									setFooterScroll(startPosition, height);
								};

								setWidthFunction = function () {
									setHeaderWidth();
									setFooterWidth();
								};

							} else if (hasHeader) {
								initializeHeader();
								scrollFunction = setHeaderScroll;
								setWidthFunction = setHeaderWidth;
							} else if (hasFooter) {
								initializeFooter();
								scrollFunction = setFooterScroll;
								setWidthFunction = setFooterWidth;
							}

							$content.on('scroll.' + tableId, function () {
								scrollFunction(element.offset().top - cTop, element.height());
							});

							scope.$on('$destroy', function () {
								$content.off('scroll.' + tableId);
							});

							Common.subscribeContentResize(scope, function () {
								cHeight = $content.height();

								setWidthFunction();
								$content.scroll();
							});

							let widthTimer;
							scope.$on('tableColumnChanges', function () {
								clearTimeout(widthTimer);

								widthTimer = setTimeout(function () {
									setWidthFunction();
									$content.scroll();
								}, 0);
							});


							function initializeHeader() {
								let $cTable = $table.clone().empty().addClass('floating-header');
								$cHeader.clone().appendTo($cTable);
								$table.before($compile($cTable)(scope));
								$cHeader = $cTable.find('thead');

								setHeaderWidth();
								$content.scroll();
							}

							function initializeFooter() {
								let $cTable = $table.clone().empty().addClass('floating-footer');
								$cFooter.clone().appendTo($cTable);
								$table.after($compile($cTable)(scope));
								$cFooter = $cTable.find('tfoot');

								setFooterWidth();
								$content.scroll();
							}

							function setHeaderWidth() {
								let $floatingColumns = $cHeader.find('th');
								$header.find('th').each(function (th) {
									setColumnWidth($floatingColumns[th], $(this).width());
								});
							}

							function setFooterWidth() {
								let $floatingColumns = $cFooter.find('td');
								$footer.find('td').each(function (td) {
									setColumnWidth($floatingColumns[td], $(this).width());
								})
							}

							function setColumnWidth(target, width) {
								$(target).css({
									'width': width,
									'min-width': width,
									'max-width': width
								});
							}

							function setHeaderScroll(startPosition, height) {
								floatHeader($cHeader, calculateTop(startPosition, height));
							}

							function setFooterScroll(startPosition, height) {
								floatHeader($cFooter, calculateBottom(startPosition, height));
							}

							function floatHeader($target, shift) {
								$target.css({'transform': 'translateY(' + shift + 'px)'});
								shift ? $target.addClass('z-depth-1') : $target.removeClass('z-depth-1');
							}

							function calculateTop(start, height) {
								let s = -start;

								if (s < 0) {
									return 0;
								}

								let max = height - 160;

								if (hasFooter) {
									max -= 56;
								}

								if (max < 0) {
									max = 0;
								}

								return s > max ? max : s;
							}

							function calculateBottom(start, height) {
								let compensate = height + start - cHeight - 110;
								height -= 168;

								if (hasHeader) {
									height -= 48;
								}

								compensate = compensate < 0 ? 0 : compensate;
								return compensate > height ? -height : -compensate;
							}
						});
					}
				}
			}
		};
	}

	TableSelectedRows.$inject = [];
	function TableSelectedRows() {
		return {
			restrict: 'A',
			link: function (scope, element) {
				$('<th class="icon">').prependTo(element.find('thead tr'));
			},
			controller: function ($scope, $attrs, $parse) {
				let selectedRows = $parse($attrs.tableSelectedRows)($scope);
				let selfTriggered = false;
				let selectTableRows = [];

				$scope.$watchCollection(function () {
					return $parse($attrs.tableSelectedRows)($scope);
				}, function (newArray) {
					if (selfTriggered) {
						selfTriggered = false;
						return;
					}

					selectedRows = newArray;
					let i = selectTableRows.length;
					while (i--) {
						let s = selectTableRows[i];
						if (selectedRows.indexOf(s.value) === -1) {
							s.functions.uncheck();
						} else {
							s.functions.check();
						}
					}
				});

				this.addRow = function (value, functions) {
					selectTableRows.push({ value: value, functions: functions });
				};

				this.removeRow = function (value) {
					let i = selectTableRows.length;
					while (i--) {
						if (selectTableRows[i].value == value) {
							selectTableRows.splice(i, 1);
							break;
						}
					}

					this.unselectRow(value);
				};

				this.selectRow = function (value) {
					selfTriggered = true;
					selectedRows.push(value);
				};

				this.unselectRow = function (value) {
					selfTriggered = true;
					let i = selectedRows.indexOf(value);
					if (i === -1) {
						return;
					}

					selectedRows.splice(i, 1);
				};
			}
		}
	}

	TableSelectRow.$inject = ['$parse'];
	function TableSelectRow($parse) {
		return {
			scope: true,
			require: '^tableSelectedRows',
			restrict: 'A',
			template: '<td><input-check minimal model="selected" on-change="onSelect" modify="modify"></input-check></td>',
			transclude: true,
			link: function (scope, element, attrs, tableController, transclude) {
				let selectProperty;

				if (typeof attrs.modify === 'undefined') {
					scope.modify = true;
				} else {
					scope.$watch(function () {
						return $parse(attrs.modify)(scope.$parent);
					}, function (value) {
						scope.modify = value;
					});
				}

				scope.$watch(function () {
					return $parse(attrs.tableSelectRow)(scope.$parent)
				}, function (newRow, oldRow) {
					tableController.removeRow(oldRow);

					selectProperty = newRow;
					addTableRow();
				}, true);

				scope.onSelect = function () {
					scope.selected ?
						tableController.selectRow(selectProperty) : tableController.unselectRow(selectProperty);
				};

				transclude(scope.$parent, function (clone) {
					clone.appendTo(element);
				});

				function addTableRow() {
					tableController.addRow(selectProperty,
						{
							uncheck: function () {
								scope.selected = false;
							},
							check: function () {
								scope.selected = true;
							}
						}
					);
				}
			}
		};
	}
})();