(function () {
	'use strict';

	angular
		.module('core')
		.directive('tablePager', TablePager);

	TablePager.$inject = ['$timeout'];
	function TablePager($timeout) {
		return {
			scope: {
				pageSize: '=?',
				page: '=?',
				max: '=',
				onChange: '&?',
				onPageSizeChange: '&?',
				onPageChange: '&?'
			},
			restrict: 'E',
			templateUrl: 'core/ui/table/pager.html',
			link: function (scope) {
				scope.pageSize = typeof scope.pageSize === 'number' ? scope.pageSize : 25;
				scope.page = typeof scope.page === 'number' ? scope.page : 0;

				scope.$watch('pageSize + page + max', recalculateRanges);

				scope.pageSizeChange = function () {
					scope.page = 0;
					onPageSizeChange();
				};

				scope.nextPage = function () {
					scope.page++;
					onPageChange();
				};

				scope.previousPage = function () {
					scope.page--;
					onPageChange();
				};

				function recalculateRanges() {
					var desired = scope.page * scope.pageSize + scope.pageSize;

					if (desired < scope.max) {
						scope.endRange = desired < scope.max ? desired : scope.max;
						scope.hasNextPage = true;
					} else {
						scope.endRange = scope.max;
						scope.hasNextPage = false;
					}

					if (scope.page * scope.pageSize + 1 > scope.endRange) {
						scope.startRange = scope.endRange;
					} else {
						scope.startRange = scope.page * scope.pageSize + 1;
					}

					scope.hasPreviousPage = scope.startRange > 1;
				}

				function onPageSizeChange() {
					if (isFunction(scope.onPageSizeChange)) {
						$timeout(function () {
							scope.onPageSizeChange()();
						});
					} else {
						onChange();
					}
				}

				function onPageChange() {
					if (isFunction(scope.onPageChange)) {
						$timeout(function () {
							scope.onPageChange()();
						})
					} else {
						onChange();
					}
				}

				function onChange() {
					$timeout(function () {
						if (isFunction(scope.onChange)) {
							scope.onChange()();
						}
					});
				}

				function isFunction(f) {
					return typeof f === 'function' && typeof f() === 'function';
				}
			}
		}
	}
})();