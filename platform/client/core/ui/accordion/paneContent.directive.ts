﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('paneContent', PaneContent);

	PaneContent.$inject = [];
	function PaneContent() {
		return {
			transclude: true,
			replace: true,
			restrict: 'E',
			required: '^pane',
			template: '<content><div ng-transclude></div></content>'
		};
	}
})();
