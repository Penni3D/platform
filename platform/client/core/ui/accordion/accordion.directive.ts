﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('accordion', Accordion);

	Accordion.$inject = ['$timeout'];

	function Accordion($timeout) {
		return {
			scope: true,
			transclude: true,
			restrict: 'E',
			link: function (scope, element, attr, ctrl, transclude) {
				transclude(scope.$parent, function (clone, transclusionScope) {
					let id = attr.control;
					if (typeof id !== 'undefined') {
						transclusionScope['accordion-' + id] = {
							open: function (args) {
								if (typeof args.index !== 'undefined') {
									let id = ctrl.indexes[args.index];
									if (typeof id === 'undefined') {
										return;
									}

									ctrl.open(id);
								} else if (typeof args.id !== 'undefined') {
									angular.forEach(ctrl.accordionPanes, function (pane, id) {
										if (id === args.id) {
											ctrl.open(id);
										}
									});
								}
							},
							close: function (args) {
								if (typeof args.index !== 'undefined') {
									let id = ctrl.indexes[args.index];
									if (typeof id === 'undefined') {
										return;
									}

									ctrl.close(id);
								} else if (typeof args.id !== 'undefined') {
									angular.forEach(ctrl.accordionPanes, function (pane, id) {
										if (id === args.id) {
											ctrl.close(id);
										}
									});
								}
							}
						};

						$timeout(function () {
							scope.$emit('accordion-' + id + ':ready');
						});
					}

					clone.appendTo(element);
				});
			},
			controller: function ($attrs) {
				let single = typeof $attrs.multiple === 'undefined';
				let paneStatuses = {};
				let self = this;
				
				self.indexes = [];

				self.addPane = function (id, pane) {
					self.accordionPanes[id] = pane;
					self.indexes.push(id);
				};

				self.accordionPanes = {};

				function toggleAria($header) {
					if ($header.attr("aria-expanded") == "true") {
						$header.attr("aria-expanded","false");
					} else {
						$header.attr("aria-expanded","true");
					}
				}
				
				self.open = function (id) {
					if (paneStatuses[id] === 'opened') {
						return;
					}

					if (single) {
						let i = self.indexes.length;
						while (i--) {
							let id_ = self.indexes[i];

							if (paneStatuses[id_] == 'opened') {
								self.close(id_);
							}
						}
					}

					let p = self.accordionPanes[id];
					let $content = p.$content;
					let $container = p.$container;
					let $p = p.$pane;
					toggleAria($p.children("pane-header").children("a"));
					
					let h = $content.outerHeight(true);
					$p.addClass('expanded');
					$container.css('height', 0);
					
					tabIndexChildren($container, 0);

					setTimeout(function () {
						$container.css('height', h);

						setTimeout(function () {
							$container.css('height', 'auto');
						}, 300);
					}, 75);

					paneStatuses[id] = 'opened';
				};

				let tabIndexChildren = function ($container, value) {
					if ($container[0] && $container[0].children[0]) {
						_.each($container[0].children[0].childNodes, function (n) {
							if (n.nodeName == 'DIV') n.tabIndex = value;
						});
					}
				};

				self.close = function (id) {
					if (paneStatuses[id] === 'closed') {
						return;
					}

					let p = self.accordionPanes[id];
					let $container = p.$container;
					let $p = p.$pane;
					toggleAria($p.children("pane-header").children("a"));
					
					tabIndexChildren($container, -1);
					let h = $container.height();
					$container.css('height', h);

					setTimeout(function () {
						$container.css('height', 0);

						setTimeout(function () {
							$p.removeClass('expanded');
						}, 300);
					}, 75);

					paneStatuses[id] = 'closed';
				};

				self.toggle = function (id) {
					paneStatuses[id] === 'opened' ? self.close(id) : self.open(id);
				};

				angular.element(document).ready(function () {
					if (typeof $attrs.initOpen !== 'undefined' && $attrs.initOpen == 'true') {
						angular.forEach($attrs.$$element[0].children, function (pane) {
							self.open(pane.attributes.entity.value);
						});
					}
				});
			}
		};
	}
})();
