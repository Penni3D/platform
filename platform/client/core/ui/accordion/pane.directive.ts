﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('pane', Pane);

	Pane.$inject = ['UniqueService'];
	function Pane(UniqueService) {
		return {
			scope: true,
			transclude: true,
			restrict: 'E',
			require: '^accordion',
			link: function (scope, element, attr, accordionCtrl, transclude) {
				transclude(scope.$parent, function (clone) {
					clone.appendTo(element);
				});

				let id = attr.entity;
				id = typeof id === 'undefined' ? UniqueService.get('pane') : id;

				accordionCtrl.addPane(id,
					{
						$container: element.find('content').first(),
						$content: element.find('div[ng-transclude]').first(),
						$pane: element
					});

				element.on('click', 'pane-header', function (event) {
					accordionCtrl.toggle(id);
					event.stopPropagation();
				});
				
				if (typeof attr.expand !== 'undefined') {
					accordionCtrl.open(id);
				}
			}
		};
	}
})();
