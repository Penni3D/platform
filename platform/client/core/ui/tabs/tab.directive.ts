﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('tab', Tab);

	Tab.$inject = ['UniqueService', '$parse', 'Translator'];
	function Tab(UniqueService, $parse, Translator) {
		return {
			require: '^tabs',
			restrict: 'E',
			transclude: true,
			scope: true,
			link: function (scope, element, attr, tabsController, transclude) {
				let id = attr.entity;
				id = typeof id === 'undefined' ? UniqueService.get('tab') : id;

				let o = {
					onClick: $parse(attr.onClick)(scope.$parent),
					id: id,
					$element: element,
					compiled: false,
					compile: compile,
					label: undefined,
					icon: undefined,
					hideTab: undefined,
					color: undefined,
					disableicon: undefined,
					orderNo: undefined,
					tabSave: $parse(attr.tabSave)(scope.$parent),
					deleteTab: $parse(attr.deleteTab)(scope.$parent)
				};

				if (typeof attr.orderNo !== 'undefined') {
					attr.$observe('orderNo', function (orderNo) {
						o.orderNo = orderNo;
					});
				}

				if (typeof attr.label !== 'undefined') {
					attr.$observe('label', function (label) {
						o.label = Translator.translate(label);
					});
				}

				if (typeof attr.icon !== 'undefined') {
					attr.$observe('icon', function (icon) {
						o.icon = icon;
					});
				}
				
				if (typeof attr.hideTab !== 'undefined') {
					attr.$observe('hideTab', function (hideTab) {
						o.hideTab = hideTab == 'true';
					});
				}

				if (typeof attr.color !== 'undefined') {
					attr.$observe('color', function (color) {
						o.color = color;
					});
				}

				if (typeof attr.disableicon !== 'undefined') {
					attr.$observe('disableicon', function (disableicon) {
						o.disableicon = disableicon;
					});
				}

				function compile() {
					transclude(scope.$parent, function (clone) {
						clone.appendTo(element);
					});
					o.compiled = true;
				}

				tabsController.addTab(o);
				element.attr('entity', 'tab-' + id);

				scope.$on('$destroy', function () {
					tabsController.removeTab(id);
				});
			}
		};
	}
})();