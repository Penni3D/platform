﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('tabs', Tabs);

	Tabs.$inject = ['Common','$rootScope','MessageService'];
	function Tabs(Common, $rootScope,MessageService) {
		return {
			scope: {
				hideIfSingleTab: '<?',
				initiallySelectedEntity: '@',
				currentlySelectedEntity: '=?',
				createFunction: '&?',
				modify: '<',
				move: '<',
				vertical: '<?',
				verticalHeight: '@?',
				firstDisableIndex: '@?',
				lastDisableIndex: '@?'
			},
			transclude: true,
			restrict: 'E',
			templateUrl: 'core/ui/tabs/tabs.html',
			link: function (scope, element, attr, ctrl, transclude) {
				element.addClass("tabs-disabled");
				if (scope.vertical) element.addClass("vertical");
				
				let $navigation = element.children('.tab-elements');
				scope.showArrows = false;
				scope.showScaffolding = true;

				if(typeof scope.createFunction == 'undefined'){
					scope.createFunction = function(){
						return;
					}
				}

				scope.create = function() {
					scope.createFunction();
				}

				scope.delete = function(removeTab) {
					MessageService.confirm("DELETE_TAB_CONFIRM", function() {
						let i = scope.tabs.length;
						while (i--) {
							let tab = scope.tabs[i];
							if (tab.id == removeTab.id) {
								scope.tabs.splice(i,1);
								break;
							}
						}
						removeTab.deleteTab(removeTab.id)
					});
				}

				scope.moveOrder = function(currentIndex, way){
					let newIndex = angular.copy(currentIndex);
					let changedTabs = [];

					if(way == 'next' && currentIndex < scope.tabs.length - 1){
						let tmp = scope.tabs[currentIndex];
						scope.tabs[currentIndex] = scope.tabs[currentIndex + 1];
						scope.tabs[currentIndex + 1] = tmp;
						changedTabs.push(scope.tabs[currentIndex])
						changedTabs.push(scope.tabs[currentIndex + 1])
						newIndex = currentIndex + 1;
					} else if (way == 'prev' && currentIndex > 0){
						let tmp = scope.tabs[currentIndex];
						scope.tabs[currentIndex] = scope.tabs[currentIndex - 1];
						scope.tabs[currentIndex - 1] = tmp;
						changedTabs.push(scope.tabs[currentIndex])
						changedTabs.push(scope.tabs[currentIndex - 1])
						newIndex = currentIndex - 1;
					}

					let order;
					if(newIndex == 0){
						order = Common.getOrderNoBetween(undefined, scope.tabs[newIndex + 1].orderNo);
					} else if (newIndex == scope.tabs.length - 1){
						order = Common.getOrderNoBetween(scope.tabs[newIndex - 1].orderNo, undefined);
					} else {
						order = Common.getOrderNoBetween(scope.tabs[newIndex - 1].orderNo, scope.tabs[newIndex + 1].orderNo);
					}
					scope.tabs[newIndex].orderNo = order;
					scope.tabs[newIndex].tabSave(changedTabs);
				}

				transclude(scope.$parent, function (clone, transclusionScope) {
					clone.appendTo(element);
					let id = attr.control;

					if (typeof id !== 'undefined') {
						transclusionScope['tabs-' + id] = {
							switch: function (entity) {
								if (typeof entity === 'undefined') {
									return;
								}

								setTimeout(function () {
									element.find('#tabs-navigation-' + entity).triggerHandler('click');
								});
							}
						};
					}

					setTimeout(function () {
						let $tab = element.find('[active]');
						if ($tab.length) {
							let id = $tab.attr('entity').split('-').pop();
							let i = scope.tabs.length;
							while (i--) {
								let tab = scope.tabs[i];
								if (tab.id == id) {
									scope.activeTab = tab;
									scope.currentlySelectedEntity = scope.tabs.indexOf(scope.activeTab);
									break;
								}
							}
						} else if(scope.initiallySelectedEntity && _.isNumber(Number(scope.initiallySelectedEntity))){
							scope.activeTab = scope.tabs[scope.initiallySelectedEntity];
							scope.currentlySelectedEntity = scope.tabs.indexOf(scope.activeTab);
						}
						else {
							scope.activeTab = scope.tabs[0];
							scope.currentlySelectedEntity = scope.tabs.indexOf(scope.activeTab);
						}

						if (scope.tabs.length == 1 && scope.hideIfSingleTab) scope.showScaffolding = false;
						showTab();
					});

					if (typeof scope.currentlySelectedEntity !== 'undefined') {
						scope.$watch("currentlySelectedEntity", function (newValue, oldValue) {
							if (newValue !== oldValue) {
								scope.onClick(event, scope.tabs[newValue]);
							}
						});
					}
				});

				scope.onClick = function (event, tab) {
					if (scope.activeTab == tab) {
						return;
					}

					let $target = $(event.target);
					let position = $target.position().left;
					let nW = $navigation.width();
					let w = $target.width();

					let scrollTo = $navigation.scrollLeft() - (((nW - w) / 2) - position) - 24;
					if (scrollTo < 0) {
						scrollTo = 0;
					} else if (scrollTo > nW) {
						scrollTo = nW;
					}

					$navigation.animate({ scrollLeft: scrollTo }, 225, 'easeOutCubic');

					hideTab();
					scope.activeTab = tab;
					scope.currentlySelectedEntity = scope.tabs.indexOf(scope.activeTab);

					showTab();
				};

				scope.shiftRight = function () {
					let scrollable = $navigation[0].scrollWidth - $navigation.width();

					if (scrollable) {
						let maxScroll = $navigation.width() / 2;

						if (scrollable > maxScroll) {
							scrollable = maxScroll;
						}

						$navigation.animate({ scrollLeft: $navigation.scrollLeft() - scrollable }, 300, 'easeOutCubic');
					}
				};

				scope.shiftLeft = function () {
					let scrollable = $navigation[0].scrollWidth - $navigation.width();

					if (scrollable) {
						let maxScroll = $navigation.width() / 2;

						if (scrollable > maxScroll) {
							scrollable = maxScroll;
						}

						$navigation.animate({ scrollLeft: $navigation.scrollLeft() + scrollable }, 300, 'easeOutCubic');
					}
				};

				function checkWidth() {
					if ($navigation[0]) {
						let scrollWidth = $navigation[0].scrollWidth;
						let outerWidth = $navigation.outerWidth();
	
						if (outerWidth < scrollWidth) {
							scope.showArrows = true;
							if (!scope.vertical) $navigation.css({ 'margin-left': 36, 'margin-right': 36 });
						} else if (outerWidth == scrollWidth) {
							scope.showArrows = false;
							if (!scope.vertical) $navigation.css({ 'margin-left': 0, 'margin-right': 0 });
						}
					}
				}

				function showTab() {
					if (typeof scope.activeTab === 'undefined') {
						return;
					}

					if (!scope.activeTab.compiled) {
						scope.activeTab.compile();
					}

					scope.activeTab.$element.css('display', 'block');

					setTimeout(function () {
						scope.activeTab.$element.addClass('transition-in');
						Common.notifyContentResize();
					}, 75);

					if (typeof scope.activeTab.onClick === 'function') {
						scope.activeTab.onClick(scope.activeTab.id);
					}
				}

				function hideTab() {
					if (typeof scope.activeTab === 'undefined') {
						return;
					}

					scope.activeTab.$element.css('display', 'none');
					scope.activeTab.$element.removeClass('transition-in');
				}

				Common.subscribeContentResize(scope, checkWidth);
				
				setTimeout(function () {
					checkWidth();
				}, 75);

			},
			controller: function ($scope) {
				$scope.tabs = [];
				this.addTab = function (tab) {
					$scope.tabs.push(tab);
				};

				this.removeTab = function (id) {
					let i = $scope.tabs.length;
					while (i--) {
						if ($scope.tabs[i].id == id) {
							$scope.tabs.splice(i, 1);
							break;
						}
					}
				};
			}
		};
	}
})();