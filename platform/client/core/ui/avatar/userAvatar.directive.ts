﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('avatarUser', AvatarUser);

	AvatarUser.$inject = ['UserService'];
	function AvatarUser(UserService) {
		return {
			scope: { user: '=' },
			restrict: 'E',
			link: function (scope, element) {
				(typeof scope.user === 'number' || typeof scope.user === 'string' ?
					UserService.getAvatarById : UserService.getAvatar)(scope.user).then(function (avatar) {
						element.replaceWith(avatar);

						avatar.on('click', function (event) {
							event.stopPropagation();
							UserService.card(event);
						})
				});
			}
		};
	}
})();
