﻿(function () {
	'use strict';

	angular
		.module('core')
		.directive('loader', Loader);

	Loader.$inject = [];
	function Loader() {
		return {
			restrict: 'E',
			template:
			'<div class="outer outer-top"></div>' +
			'<div class="outer outer-right"></div>' +
			'<div class="outer outer-bottom"></div>' +
			'<div class="outer outer-left"></div>',
			link: {
				pre: function (scope, iElement) {
					var cls = 'circle';
					if (iElement.hasClass('tiny')) {
						cls += ' circle-small';
					} else if (!(iElement.hasClass('small') || iElement.hasClass('large'))) {
						cls += ' circle-large'
					}

					iElement.addClass(cls);
				}
			}
		};
	}
})();