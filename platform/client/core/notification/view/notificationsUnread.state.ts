(function () {
	'use strict';

	angular
		.module('core')
		.config(NotificationConfig)
		.controller('NotificationsUnreadController', NotificationsUnreadController);

	NotificationConfig.$inject = ['ViewsProvider'];

	function NotificationConfig(ViewsProvider) {
		ViewsProvider.addDialog('notification.view', {
			controller: 'NotificationsUnreadController',
			template: 'core/notification/view/notificationsUnread.html'
		});
	}

	NotificationsUnreadController.$inject = ['$scope', 'NotificationService', 'UnreadNotifications', 'MessageService', 'Backgrounds'];

	function NotificationsUnreadController($scope, NotificationService, UnreadNotifications, MessageService, Backgrounds) {
		$scope.columns = NotificationService.getColumns();

		$scope.currentNotification = 1;
		$scope.notification = UnreadNotifications[0];
		$scope.undreadNotifications = UnreadNotifications;
		$scope.background = Backgrounds.getRandomBackground().background;


		$scope.close = function () {
			MessageService.closeDialog();
		};

		$scope.next = function () {
			if ($scope.currentNotification < $scope.undreadNotifications.length) {
				$scope.currentNotification += 1;
				$scope.notification = $scope.undreadNotifications[$scope.currentNotification - 1];
			}
		}

		$scope.prev = function () {
			if ($scope.currentNotification > 1) {
				$scope.currentNotification -= 1;
				$scope.notification = $scope.undreadNotifications[$scope.currentNotification - 1];
			}
		}

		$scope.read = function () {
			NotificationService.readNotification($scope.notification.item_id);
			$scope.notification = UnreadNotifications[0];
		};
	}
})();
