(function () {
	'use strict';

	angular
		.module('core')
		.config(NotificationConfig)
		.controller('NotificationEditController', NotificationEditController);

	NotificationConfig.$inject = ['ViewsProvider'];
	function NotificationConfig(ViewsProvider) {
		ViewsProvider.addView('notification.edit', {
			controller: 'NotificationEditController',
			template: 'core/notification/edit/notification.html',
			resolve: {
				Notification: function (NotificationService, StateParameters) {
					return NotificationService.getNotification(StateParameters.notificationId);
				},
				Tags: function (NotificationService) {
					return NotificationService.getTags();
				},
				Languages: function (NotificationService) {
					return NotificationService.getLanguages();
				},
				Rights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('notifications');
				}
			}
		});
	}

	NotificationEditController.$inject = [
		'$scope',
		'NotificationService',
		'StateParameters',
		'Notification',
		'Languages',
		'Tags',
		'$rootScope',
		'ProcessService',
		'Rights',
		'ViewService',
		'UserService',
		'MessageService',
		'Translator'
	];

	function NotificationEditController(
		$scope,
		NotificationService,
		StateParameters,
		Notification,
		Languages,
		Tags,
		$rootScope,
		ProcessService,
		Rights,
		ViewService,
		UserService,
		MessageService,
		Translator

	) {

		let tagsToId = {};
		angular.forEach(Tags, function (tag) {
			tagsToId['[' + tag.list_item + ']'] = '[' + tag.item_id + ']';
		});

		$scope.keys = {
			0: {
				HEADER: 'EMAILS',
				DELETE: 'NOTIFICATIONS_EMAILS_CONFIRM_DELETE',
				TITLE: 'EMAIL_TITLE',
				NAME: 'EMAIL_NAME'
			},
			1: {
				HEADER: 'NOTIFICATIONS',
				DELETE: 'NOTIFICATIONS_NOTIFICATIONS_CONFIRM_DELETE',
				TITLE: 'NOTIFICATION_TITLE',
				NAME: 'NOTIFICATION_NAME'
			},
			2: {
				HEADER: 'BULLETINS',
				DELETE: 'NOTIFICATIONS_BULLETINS_CONFIRM_DELETE',
				TITLE: 'BULLETIN_TITLE',
				NAME: 'BULLETIN_NAME'
			},
			5: {
				HEADER: 'BULLETINS',
				DELETE: 'NOTIFICATIONS_BULLETINS_CONFIRM_DELETE',
				TITLE: 'BULLETIN_TITLE',
				NAME: 'BULLETIN_NAME'
			}
		};

		$scope.key = $scope.keys[StateParameters.type];
		$scope.columns = NotificationService.getColumns();

		if (Notification.type == 2) {
			ViewService.footbar(StateParameters.ViewId, { template: 'core/notification/edit/notificationFooter.html' })
		}

		$scope.hasWriteRights = Rights.indexOf('write') !== -1;
		$scope.selectedLanguage = $rootScope.clientInformation.locale_id;
		$scope.notification = Notification
		$scope.digestText = !$scope.notification.hasOwnProperty('digest') ? Translator.translate("DIGEST_MISSING_IN_PORTFOLIO") : "";

		if($scope.notification.notify == null)$scope.notification.notify = false;
		$scope.languages = Languages;
		$scope.tags = Tags;

		if (StateParameters.type == 2) {
			$scope.type = 'notification';	
		} else if(StateParameters.type == 0){
			$scope.type = 'email';
		}
		$scope.header = {
			title: $scope.key.HEADER
		};

		$scope.body = angular.copy(Notification[$scope.columns.body]);

		if ($scope.body === null) {
			$scope.body = {};
		}

		let i = Languages.length;
		while (i--) {
			let l = Languages[i].value;

			$scope.body[l] = typeof $scope.body[l] === 'undefined' ?
				{ html: '', delta: undefined } : angular.fromJson($scope.body[l]);
		}

		$scope.onChange = function () {
			Notification[$scope.columns.modified] = new Date();
			Notification[$scope.columns.sender] = [UserService.getCurrentUserId()];
			ProcessService.setDataChanged('notification', Notification.itemId);
		};

		$scope.bodyChange = function () {
			setTimeout(function () {
			if (Notification[$scope.columns.body] === null) {
				Notification[$scope.columns.body] = {};
			}

			angular.forEach($scope.body, function (value, language) {
				let copy = angular.copy(value);
				Notification[$scope.columns.body][language] = angular.toJson(copy);
			});

			$scope.onChange();
			}, 250);
		};

		$scope.published = Notification[$scope.columns.status] == 0 ? false: true;

		$scope.publish = function () {
			MessageService.confirm(Translator.translate('U_SURE_PUBLISH'),
				function () {
					$scope.published = true;
					Notification[$scope.columns.status] = 1;
					StateParameters.Sync(Notification.item_id, 1);
					$scope.onChange();
				});
		};

		$scope.unpublish = function () {
			MessageService.confirm(Translator.translate('U_SURE_HIDE_BULLETIN'),
				function () {
					$scope.published = false;
					Notification[$scope.columns.status] = 0;
					StateParameters.Sync(Notification.item_id, 2);
					$scope.onChange();
				});
		};
	}
})();
