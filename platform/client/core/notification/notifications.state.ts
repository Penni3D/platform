﻿(function () {
	'use strict';

	angular
		.module('core')
		.config(NotificationsConfig)
		.controller('NotificationsController', NotificationsController);

	NotificationsConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function NotificationsConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('notifications', {
			tabs: {
				default: {
					type: function () {
						return {
							type: 'select',
							options: [
								{name: 'EMAILS', value: 0},
								{name: 'NOTIFICATIONS', value: 1},
								{name: 'BULLETINS', value: 2},
								{name: 'RICH_TEXT_TEMPLATES', value: 5}],
							label: 'NOTIFICATION_TYPE'
						};
					}
				}
			},
			defaults: {
				type: 0
			}
		});

		ViewsProvider.addView('notifications', {
			controller: 'NotificationsController',
			template: 'core/notification/notifications.html',
			resolve: {
				Notifications: function (NotificationService, StateParameters) {
					return NotificationService.getNotifications(StateParameters.type);
				},
				Rights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('notifications');
				},
				ActionConnections: function (NotificationService, $rootScope) {
					return NotificationService.getActionConnections($rootScope.me.instance);
				},

				NotificationConditionConnections: function (NotificationService, $rootScope) {
					return NotificationService.getNotificationConditionConnections($rootScope.me.instance);
				},
			}, afterResolve: {
				NotificationsByUsage: function (NotificationService, $rootScope, Notifications) {

					let u = {'Used': [], 'NotUsed': []};

					return NotificationService.getInUseIds($rootScope.me.instance).then(function (ids) {
						_.each(Notifications, function (notification) {
							if (_.includes(ids, notification.item_id)) {
								u['Used'].push(notification);
							} else {
								u['NotUsed'].push(notification);
							}
						});

						return u;

					});
				}
			}
		});
		ViewsProvider.addPage('notifications', 'PAGE_NOTIFICATIONS', ['read', 'write', 'admin']);
	}

	NotificationsController.$inject = [
		'$scope',
		'Notifications',
		'NotificationService',
		'ViewService',
		'SidenavService',
		'StateParameters',
		'MessageService',
		'$q',
		'Rights',
		'NotificationsByUsage',
		'ActionConnections',
		'NotificationConditionConnections'
	];

	function NotificationsController(
		$scope,
		Notifications,
		NotificationService,
		ViewService,
		SidenavService,
		StateParameters,
		MessageService,
		$q,
		Rights,
		NotificationsByUsage,
		ActionConnections,
		NotificationConditionConnections
	) {

		$scope.notiCondConnections = NotificationConditionConnections;

		$scope.keys = {
			0: {
				HEADER: 'EMAILS',
				DELETE: 'NOTIFICATIONS_EMAILS_CONFIRM_DELETE',
				TITLE: 'EMAIL_TITLE',
				NAME: 'EMAIL_NAME'
			},
			1: {
				HEADER: 'NOTIFICATIONS',
				DELETE: 'NOTIFICATIONS_NOTIFICATIONS_CONFIRM_DELETE',
				TITLE: 'NOTIFICATION_TITLE',
				NAME: 'NOTIFICATION_NAME'
			},
			2: {
				HEADER: 'BULLETINS',
				DELETE: 'NOTIFICATIONS_BULLETINS_CONFIRM_DELETE',
				TITLE: 'BULLETIN_TITLE',
				NAME: 'BULLETIN_NAME'
			},
			5: {
				HEADER: 'RICHTEXTS',
				DELETE: 'RICHTEXTS',
				TITLE: 'RICHTEXTS',
				NAME: 'RICHTEXTS'
			}
		};

		$scope.goToView = function (id, process, type, translation?) {

			if (type == 1) {
				ViewService.view('settings.notificationconditions', {
						params: {
							process: process,
						}
					},
					{
						split: true
					}
				);
			} else {

				ViewService.view('settings.processActionRows', {

						params: {
							process: process,
							ProcessActionId: id,
							UserLanguageTranslation: translation
						},

					},
					{
						split: true,
					}
				);
			}
		};

		$scope.key = $scope.keys[StateParameters.type];

		$scope.titleColumn = NotificationService.getColumns().title;
		$scope.header = {
			title: $scope.key.HEADER,
			icons: []
		};

		$scope.notifications = NotificationsByUsage['Used'];
		$scope.unUsedNotifications = NotificationsByUsage['NotUsed'];
		$scope.actionConnections = ActionConnections;
		$scope.hasWriteRights = Rights.indexOf('write') !== -1;

		if (Rights.indexOf('admin') !== -1) {
			$scope.header.icons.push({
				icon: 'open_in_new',
				onClick: function () {
					ViewService.view('settings.summary',
						{
							params: {
								process: 'notification'
							}
						});
				}
			});
		}

		let syncNotifications = function (itemId, type){
			if(type == 1){
				let index = _.findIndex($scope.unUsedNotifications, function(o) { return o.item_id == itemId; });
				$scope.notifications.push($scope.unUsedNotifications[index]);
				$scope.unUsedNotifications.splice(index, 1);
			} else {
				let index = _.findIndex($scope.notifications, function(o) { return o.item_id == itemId; });
				$scope.unUsedNotifications.push($scope.notifications[index]);
				$scope.notifications.splice(index, 1);
			}
		}

		$scope.editRow = function (notification) {
			ViewService.view('notification.edit', {
				params: {
					notificationId: notification.item_id,
					type: StateParameters.type,
					Sync: syncNotifications
				}
			}, {split: true, size: 'wider'});
		};

		$scope.removeRow = function (notificationId, type) {
			MessageService.confirm($scope.key.DELETE, function () {
				return NotificationService.deleteNotification([notificationId]).then(function () {
					_.remove(type == 2 ? $scope.unUsedNotifications : $scope.notifications, function (n) {
						return n.item_id == notificationId;
					});
				});
			});
		};

		$scope.addRow = function () {
			$scope.adding = true;
			NotificationService.newNotification(StateParameters.type).then(function (notification) {
				$scope.adding = false;
				$scope.unUsedNotifications.push(notification);
				$scope.editRow(notification);
			});
		};
	}
})();