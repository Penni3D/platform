﻿(function () {
	'use strict';

	angular
		.module('core')
		.config(NotificationConfig)
		.service('NotificationService', NotificationService);

	NotificationConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider', 'GlobalsProvider'];

	function NotificationConfig(ViewsProvider, ViewConfiguratorProvider, GlobalsProvider) {
		GlobalsProvider.addGlobal('notification', []);
		ViewConfiguratorProvider.addConfiguration('notification', {
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('notification');
				},
				ItemColumns: function (ProcessService) {
					return ProcessService.getColumns('notification', true);
				}
			},
			tabs: {
				default: {
					emailsPortfolioId: function (resolves) {
						return defaultPortfolio(resolves.Portfolios, 'EMAILS_PORTFOLIO');
					},
					notificationsPortfolioId: function (resolves) {
						return defaultPortfolio(resolves.Portfolios, 'NOTIFICATIONS_PORTFOLIO');
					},
					unreadNotificationsPortfolioId: function (resolves) {
						return defaultPortfolio(resolves.Portfolios, 'UNREAD_NOTIFICATIONS_PORTFOLIO');
					},
					bulletinsPortfolioId: function (resolves) {
						return defaultPortfolio(resolves.Portfolios, 'BULLETINS_PORTFOLIO')
					},
					widgetBulletinsPortfolioId: function (resolves) {
						return defaultPortfolio(resolves.Portfolios, 'WIDGET_BULLETINS_PORTFOLIO')
					},
					richTextPortfolioId: function (resolves) {
						return defaultPortfolio(resolves.Portfolios, 'RICH_TEXT_PORTFOLIO')
					}
				},
				columns: {
					nameColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_NAME_COLUMN');
					},
					titleColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_TITLE_COLUMN');
					},
					bodyColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_BODY_COLUMN');
					},
					senderColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_SENDER_COLUMN');
					},
					modifiedColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_MODIFIED_COLUMN');
					},
					statusColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_STATUS_COLUMN');
					},
					expirationColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_EXPIRATION_COLUMN');
					},
					digestColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_DIGEST_COLUMN');
					},
					notifyColumn: function (resolves) {
						return defaultColumn(resolves.ItemColumns, 'NOTIFICATION_NOTIFY_COLUMN');
					}
				}
			},
			defaults: {
				nameColumn: 'name_variable',
				titleColumn: 'title_variable',
				bodyColumn: 'body_variable',
				senderColumn: 'sender',
				modifiedColumn: 'modified',
				statusColumn: 'status',
				expirationColumn: 'expiration',
				digestColumn: 'digest',
				notifyColumn: 'notify'
			}
		});

		function defaultColumn(itemColumns, label) {
			return {
				type: 'select',
				options: itemColumns,
				optionValue: 'Name',
				optionName: 'LanguageTranslation',
				label: label
			};
		}

		function defaultPortfolio(portfolios, label) {
			return {
				type: 'select',
				options: portfolios,
				optionValue: 'ProcessPortfolioId',
				optionName: 'LanguageTranslation',
				label: label
			};
		}
	}

	NotificationService.$inject = [
		'ApiService',
		'ViewConfigurator',
		'PortfolioService',
		'ProcessService',
		'UserService',
		'$q',
		'Configuration',
		'Translator',
		'$rootScope'
	];

	function NotificationService(
		ApiService,
		ViewConfigurator,
		PortfolioService,
		ProcessService,
		UserService,
		$q,
		Configuration,
		Translator,
		$rootScope
	) {
		let self = this;

		let importantNotificationsShown = false;
		let importantNotifications = [];
		let unreadNotifications = [];
		let Languages = ApiService.v1api('meta/SystemList/LANGUAGES');
		let typeToPortfolioKey = {
			0: 'emailsPortfolioId',
			1: 'notificationsPortfolioId',
			2: 'bulletinsPortfolioId',
			5: 'richTextPortfolioId'
		};

		let notificationsData = ApiService.v1api('settings/NotificationsData');

		self.getInUseIds = function (instance) {
			return notificationsData.get(["inuse", instance]);
		};

		self.getActionConnections = function (instance) {
			return notificationsData.get(["actionconnections", instance]);
		};

		self.getNotificationConditionConnections = function (instance) {
			return notificationsData.get(["nocondconnections", instance]);
		};

		self.getColumns = function () {
			let c = ViewConfigurator.getGlobal('notification').Params;

			if (!c) return {
				title: undefined,
				body: undefined,
				name: undefined,
				sender: undefined,
				modified: undefined,
				status: undefined,
				expiration: undefined,
				digest: undefined,
				notify: undefined
			};

			return {
				title: c.titleColumn,
				body: c.bodyColumn,
				name: c.nameColumn,
				sender: c.senderColumn,
				modified: c.modifiedColumn,
				status: c.statusColumn,
				expiration: c.expirationColumn,
				digest: c.digestColumn,
				notify: c.notifyColumn
			};
		};

		self.getNotifications = function (type, offset, limit) {
			let portfolioId = getParams(typeToPortfolioKey[type]);
			let filters = {
				offset: offset ? offset : 0,
				limit: limit ? limit : 0,
			};

			return PortfolioService.getSimplePortfolioData('notification', portfolioId, filters).then(function (r) {
				_.each(r.rowdata, function(r2){
					if(r2.hasOwnProperty('name_variable') && r2.name_variable != null && r2.name_variable.hasOwnProperty($rootScope.clientInformation.locale_id)){
						r2.$$tempName = r2.name_variable[$rootScope.clientInformation.locale_id]
					} else if(r2.hasOwnProperty('name_variable') &&  r2.name_variable != null && !r2.name_variable.hasOwnProperty($rootScope.clientInformation.locale_id)) {
						_.each(r2.name_variable, function(translation, code){
							r2.$$tempName = r2.name_variable[code]
						})
					} else {
						r2.$$tempName = r2.item_id
					}
				})
				return _.sortBy(r.rowdata, '$$tempName', ['asc']);
			});
		};

		self.newNotification = function (type) {
			return ProcessService.newItem('notification', {
				Data: {
					type: type,
					created: new Date(),
					status: 0,
					sender: [UserService.getCurrentUserId()]
				}
			}).then(function (itemId) {
				return ProcessService.getItem('notification', itemId);
			});
		};

		self.deleteNotification = function (notificationId) {
			return ProcessService.deleteItem('notification', notificationId).then(function () {
				_.remove(unreadNotifications, function (n) {
					return n.item_id == notificationId;
				});

				_.remove(importantNotifications, function (n) {
					return n.item_id == notificationId;
				});
			});
		};

		self.getNotification = function (notificaitonId) {
			return ProcessService.getItem('notification', notificaitonId);
		};

		self.getTags = function () {
			return ProcessService.getListItems('notification_tags');
		};

		self.getLanguages = function () {
			return Languages.get();
		};

		self.getBulletins = function (offset, limit) {
			let portfolioId = getParams('widgetBulletinsPortfolioId');
			let filters = {
				offset: offset ? offset : 0,
				limit: limit ? limit : 0
			};
			return PortfolioService.getSimplePortfolioData('notification', portfolioId, filters).then(function (bulletins) {
				return prepareItems(bulletins.rowdata).then(function () {
					return bulletins;
				});
			});
		};

		self.getStrictBulletins = function () {

			return notificationsData.get(["bulletins", getParams('widgetBulletinsPortfolioId')]).then(function (bulletins) {
				return prepareItems(bulletins.rowdata).then(function () {
					return bulletins;
				});
			});
		}

		self.getImportantBulletins = function (offset, limit) {
			let portfolioId = getParams('bulletinsPortfolioId');
			let filters = {
				offset: offset ? offset : 0,
				limit: limit ? limit : 0
			};
			return PortfolioService.getSimplePortfolioData('notification', portfolioId, filters).then(function (bulletins) {
				return prepareItems(bulletins.rowdata).then(function () {
					let r = [];

					let unreadNotifications = _.reverse(bulletins.rowdata);
					_.remove(unreadNotifications, function (n) {
						return _.find(self.getActiveSettings(getParams('unreadNotificationsPortfolioId')).read, function (r) {
							return n.item_id == r
						})
					});

					for (let b of unreadNotifications) {
						if (b.notify == 1) r.push(b);
					}
					return r;
				});
			});
		};

		self.readBulletins = function (notificationId) {
			let portfolioId = getParams('unreadNotificationsPortfolioId');
			let readNotifications = self.getActiveSettings(portfolioId).read;

			if (readNotifications.indexOf(notificationId) === -1) {
				readNotifications.push(notificationId);

				_.remove(unreadNotifications, function (n) {
					return n.item_id == notificationId;
				});

				_.remove(importantNotifications, function (n) {
					return n.item_id == notificationId;
				});

				self.saveActiveSettings(portfolioId);
			}
		};

		self.notifyNotificationNeeded = function (parameters) {
			$rootScope.$emit('notifyNotificationNeeded', parameters);
		}

		self.subscribeNotificationNeeded = function (scope, callback) {
			let handler = $rootScope.$on('notifyNotificationNeeded', callback);
			scope.$on('$destroy', handler);
		};


		self.getUnreadUserNotifications = function () {
			return notificationsData.get(["userNotifications", getParams('unreadNotificationsPortfolioId')]).then(function (j) {
				let notify = self.getColumns().notify;
				unreadNotifications = _.reverse(j.rowdata);

				_.remove(unreadNotifications, function (n) {
					return _.find(self.getActiveSettings(getParams('unreadNotificationsPortfolioId')).read, function (r) {
						return n.item_id == r
					})
				});

				importantNotifications = _.filter(unreadNotifications, function (n) {
					return n[notify] === 1;
				});
				return prepareItems(unreadNotifications);
			})
		}

		/*		self.fetchUnreadNotifications = function () {
					let portfolioId = getParams('unreadNotificationsPortfolioId');
		
					if (typeof portfolioId === 'undefined') {
						return false;
					}
		
					let filters = {
						limit: 0
					};
		
					return PortfolioService
						.getSimplePortfolioData('notification', portfolioId, filters)
						.then(function (notifications) {
							let notify = self.getColumns().notify;
		
							unreadNotifications = _.reverse(notifications.rowdata);
		
							_.remove(unreadNotifications, function (n) {
								return _.find(self.getActiveSettings(portfolioId).read, function (r) {
									return n.item_id == r
								})
							});
		
							importantNotifications = _.filter(unreadNotifications, function (n) {
								return n[notify] === 1;
							});
							return prepareItems(unreadNotifications);
						});
				};*/

		self.getUnreadImportantNotifications = function () {
			return importantNotifications;
		};

		self.getUnreadNotifications = function () {
			return unreadNotifications;
		};

		self.readNotification = function (notificationId) {
			let portfolioId = getParams('unreadNotificationsPortfolioId');
			let readNotifications = self.getActiveSettings(portfolioId).read;

			if (readNotifications.indexOf(notificationId) === -1) {
				readNotifications.push(notificationId);

				_.remove(unreadNotifications, function (n) {
					return n.item_id == notificationId;
				});

				_.remove(importantNotifications, function (n) {
					return n.item_id == notificationId;
				});

				self.saveActiveSettings(portfolioId);
			}
		};

		self.saveActiveSettings = function (portfolioId) {
			if (typeof portfolioId === 'undefined') {
				return;
			}

			return Configuration.saveActivePreferences('notification', portfolioId);
		};

		self.setImportantShown = function () {
			importantNotificationsShown = true;
		};

		self.getActiveSettings = function (portfolioId) {
			let notificationSettings = Configuration.getActivePreferences('notification');
			notificationSettings[portfolioId] = _.defaultsDeep(notificationSettings[portfolioId], notificationDefaultConfigurations());

			return notificationSettings[portfolioId];
		};

		function notificationDefaultConfigurations() {
			return {
				read: []
			};
		}

		function getParams(param) {
			let g = ViewConfigurator.getGlobal('notification');
			if (g.Params) {
				return g.Params[param];
			}
		}

		function prepareItems(rows) {
			let columns = self.getColumns();

			let promises = _.map(rows, function (row) {
				let b = row[columns.body];
				let s = row[columns.sender];

				if (b && !_.isEmpty(b)) {
					let str = Translator.translation(b).replace(/(?:\r\n|\r|\n)/g, '<br>');
					row.$body = angular.fromJson(str).html;
				}

				if (s && s.length) {
					return ProcessService.getItem('user', s[0]).then(function (u) {
						row.$sender = u.primary;
					});
				}
			});

			return $q.all(promises).then(function () {
				return rows;
			});
		}
	}
})();
