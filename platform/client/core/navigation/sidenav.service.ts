﻿(function () {
	'use strict';

	angular
		.module('core')
		.config(SidenavConfig)
		.directive('dynamicNavigation', DynamicNavigation)
		.service('SidenavService', SidenavService);

	SidenavConfig.$inject = ['GlobalsProvider'];

	function SidenavConfig(GlobalsProvider) {
		GlobalsProvider.addGlobal('sticky', ['read', 'write', 'save', 'public']);
	}

	SidenavService.$inject = [
		'$rootScope',
		'$compile',
		'$timeout',
		'MessageService',
		'Configuration',
		'Translator',
		'Common',
		'ApiService',
		'ViewService'
	];

	function SidenavService(
		$rootScope,
		$compile,
		$timeout,
		MessageService,
		Configuration,
		Translator,
		Common,
		ApiService,
		ViewService
	) {
		let Navigation = ApiService.v1api('navigation');
		let self = this;
		let staticLinks = [];
		let staticNavigation = {};
		let navigation = {};
		let scope;
		let lockNavigation = false;
		let $navigationDynamic;
		let $navigation;
		let previousNavigation = '';
		let stickyNavigation = {};
		let hasRightsToStick = false;
		let hasRightsToEdit = false;
		let hasRightsToPublic = false;

		let $shroud = $('<div class="shroud"></div>').on('click', function (event) {
			event.stopPropagation();
		});

		self.isStickable = function () {
			return hasRightsToStick;
		};

		self.setStickyRights = function (rights) {
			if (rights.indexOf('save') !== -1) {
				hasRightsToStick = true;
			}

			if (rights.indexOf('write') !== -1) {
				hasRightsToEdit = true;
			}

			if (rights.indexOf('public') !== -1) {
				hasRightsToPublic = true;
			}
		};

		self.getStickyNavigation = function () {
			return stickyNavigation;
		};

		self.canEditSticky = function (sticky) {
			return hasRightsToEdit || $rootScope.me.itemId == sticky.UserId;
		};

		self.editSticky = function (sticky) {
			let model = {
				public: sticky.Public,
				translation: angular.extend({}, sticky.Name),
				process: [sticky.UserId]
			};

			let canEdit = self.canEditSticky();
			let canSetPublic = hasRightsToPublic && canEdit;

			let modifies = {
				translation: canEdit,
				delete: canEdit,
				save: canEdit,
				public: canSetPublic,
				process: false,
				cancel: true
			};

			let buttons = [{
				text: 'MSG_CANCEL',
				modify: 'cancel'
			}];

			let inputs = [
				{
					type: 'translation',
					model: 'translation',
					modify: 'translation',
					label: 'STICKY_TRANSLATION'
				},
				{
					type: 'user',
					max: '1',
					model: 'process',
					modify: 'process',
					label: 'STICKY_OWNER'
				}
			];

			if (canEdit) {
				buttons.push({
					text: 'DELETE',
					modify: 'delete',
					onClick: function () {
						return removeSavedSettings(sticky.ConfigurationId);
					}
				});
				buttons.push({
					type: 'primary',
					text: 'SAVE',
					modify: 'save',
					onClick: function () {
						sticky.Name = model.translation;
						sticky.Public = model.public;
						sticky.translation = Translator.translation(model.translation);
						return saveSavedSettings(sticky);
					}
				});
			}

			if (canSetPublic) {
				inputs.push({
					type: 'checkbox',
					model: 'public',
					modify: 'public',
					label: 'STICKY_PUBLIC'
				});
			}

			MessageService.dialog(
				{
					title: 'STICKY_EDIT',
					inputs: inputs,
					buttons: buttons
				}, model, undefined, modifies);
		};

		self.fetchSavedSettings = function () {
			return Configuration.getSavedPreferences('navigation', 'sticky').then(function (navigation) {
				stickyNavigation = navigation;

				let i = stickyNavigation.length;
				while (i--) {
					let n = stickyNavigation[i];
					n.translation = Translator.translation(n.Name);
				}

				return stickyNavigation;
			});
		};

		function saveSavedSettings(sticky) {
			return Configuration.savePreferences(
				'navigation',
				'sticky',
				sticky.ConfigurationId,
				sticky.Name,
				sticky.Public ? 1 : 0,
				sticky.Value);
		}

		function removeSavedSettings(configurationId) {
			return Configuration.deleteSavedPreferences('navigation', 'sticky', configurationId).then(function () {
				let i = stickyNavigation.length;
				while (i--) {
					if (stickyNavigation[i].ConfigurationId == configurationId) {
						stickyNavigation.splice(i, 1);
						break;
					}
				}
			});
		}

		self.stickNavigation = function (tab) {
			if (self.isStickable(tab)) {
				let translation = {};
				tab = angular.copy(tab);
				translation[$rootScope.clientInformation.locale_id] = Translator.translate(tab.name);

				let model = {
					public: false,
					translation: translation
				};

				let inputs = [{
					type: 'translation',
					model: 'translation',
					label: 'STICKY_TRANSLATION'
				}];

				if (hasRightsToPublic) {
					inputs.push({
						type: 'checkbox',
						model: 'public',
						label: 'STICKY_PUBLIC'
					});
				}

				MessageService.dialog({
					title: 'STICKY_CREATE',
					inputs: inputs,
					buttons: [
						{
							text: 'MSG_CANCEL'
						},
						{
							type: 'primary',
							text: 'CREATE',
							onClick: function () {
								return Configuration.newPreferences('navigation', 'sticky', translation, model.public ? 1 : 0, tab)
									.then(function (res) {
										res.translation = Translator.translation(res.Name);
										stickyNavigation.push(res);
									});
							}
						}
					]
				}, model);
			} else {
				MessageService.toast('SIDENAV_STICKY_EXISTS')
			}
		};

		self.subscribeSidenav = function (scope, callback) {
			let handler = $rootScope.$on('notifySidenav', function (event, args) {
				callback(args);
			});

			scope.$on('$destroy', handler);
		};

		self.notifySidenav = function (params) {
			$rootScope.$emit('notifySidenav', params);
		};

		function isSameNavigation(newNav) {
			let nJson = angular.toJson(newNav);

			if (nJson == previousNavigation) {
				return true;
			}

			previousNavigation = nJson;
			return false;
		}

		$rootScope.$on('viewChange', function (event, parameters) {
			if (parameters.action === 'PRE-NEW') {
				if (parameters.view === 'frontpage') {
					closeDynamic();
					lockNavigation = true;
					activeLink.$$activeLink = false;
				} else {
					lockNavigation = false;
				}

				addShroud();
			} else if (parameters.action === 'PRE-SPLIT') {
				lockNavigation = true;
			} else {
				removeShroud();

				if (parameters.action === "NEW") {
					if (Object.keys(navigation).length === 0) {
						closeDynamic();
					}
				} else if (parameters.action == 'FAIL') {
					if (!parameters.split) {
						closeDynamic();
					}
				} else if (parameters.action === 'CLOSE') {
					if (!parameters.views) {
						closeDynamic();
					}
				}
			}
		});

		function addShroud() {
			if (typeof $navigation === 'undefined') {
				return;
			}

			let $accordion = $navigation.find('accordion');
			$accordion.addClass('blur-background');
			$shroud.prependTo($accordion);
		}

		function removeShroud() {
			$shroud.detach();

			if (typeof $navigation === 'undefined') {
				return;
			}

			$navigation.find('accordion').removeClass('blur-background');
		}

		function closeDynamic(keep) {
			if ($navigation) {
				if (!keep) {
					self.notifySidenav({open: 'static'});
				}

				scope.$destroy();
				$navigation.remove();
			}

			lockNavigation = false;
			navigation = {};
			previousNavigation = '';
		}

		self.navigation = function (nav, options, ignoreSameNavigation = false) {
			if (lockNavigation) {
				return;
			}

			if (typeof nav === 'undefined' || nav.length === 0 || Object.keys(nav).length === 0) {
				closeDynamic(undefined);
				return;
			}

			if (isSameNavigation(nav) && ignoreSameNavigation == false) {
				self.notifySidenav({open: 'dynamic'});
				return;
			}

			if (typeof $navigationDynamic === 'undefined') {
				$navigationDynamic = $('#navigation-menu-dynamic');
			}

			if ($navigation) {
				scope.$destroy();
				$navigation.remove();
			}

			navigation = nav;
			options = typeof options === 'undefined' ? {} : angular.copy(options);

			$timeout(function () {
				scope = $rootScope.$new();
				scope.options = options;
				scope.navigation = nav;

				$navigation = $('<dynamic-navigation>');

				$compile($navigation)(scope).appendTo($navigationDynamic);
				self.notifySidenav({open: 'dynamic'});
			});
		};

		self.setActivePortfolioTab = function(visualMode, flatLock){

			//Tries to set $$active tab (tab that is active and has yellow background) based on view type

			_.each(navigation, function(menuObject){
				if(menuObject.hasOwnProperty('tabs') && menuObject['tabs'].length && menuObject['tabs'][0].hasOwnProperty('type')){
					if(menuObject['tabs'][0].type == visualMode){
						if(menuObject['tabs'][0].hasOwnProperty('subtype')){
							if(menuObject['tabs'][0].subtype == 0 && flatLock){
								menuObject['tabs'][0].$$activeTab = true;
							} else if(menuObject['tabs'][0].subtype == 1 && !flatLock){
								menuObject['tabs'][0].$$activeTab = true;
							} else {
								menuObject['tabs'][0].$$activeTab = false;
							}
						} else {
							menuObject['tabs'][0].$$activeTab = true;
						}
					} else {
						menuObject['tabs'][0].$$activeTab = false;
					}
				}
			})
		}

		self.fetchStaticNavigation = function () {
			return Navigation.get().then(function (navigation) {
				for (let i = 0, l = navigation.length; i < l; i++) {
					let n = navigation[i];

					staticLinks.push(n);

					for (let j = 0, l_ = n.Links.length; j < l_; j++) {
						let tab = n.Links[j];
						tab.ParentInstanceMenuId = n.InstanceMenuId;
						staticLinks.push(tab);
					}
				}

				staticNavigation = navigation;
				return staticNavigation;
			});
		};

		self.getStaticLinks = function () {
			return staticLinks;
		};

		self.getStaticNavigation = function (navigationId) {
			if (typeof navigationId === 'undefined') {
				return staticNavigation;
			}

			let i = staticNavigation.length;
			while (i--) {
				let links = staticNavigation[i].Links;
				let j = links.length;
				while (j--) {
					if (links[j].InstanceMenuId == navigationId) {
						return links[j];
					}
				}
			}
		};

		self.updateStaticNavigation = function (navigationId, object) {
			let i = staticLinks.length;
			while (i--) {
				let l = staticLinks[i];
				if (l.InstanceMenuId == navigationId) {
					if (object.Params) {
						l.params = object.Params;
					}

					l.LanguageTranslation = object.LanguageTranslation;

					break;
				}
			}
		};

		let activeLink = {$$activeLink: undefined};
		self.staticNavigation = function (navigationId, params, options) {
			activeLink.$$activeLink = false;
			let i = staticLinks.length;
			while (i--) {
				let l = staticLinks[i];
				if (l.InstanceMenuId == navigationId) {
					l.$$activeLink = true;
					activeLink = l;

					let p = {
						params: typeof l.params === 'undefined' ? {} : l.params
					};

					if (params) {
						angular.extend(p.params, params);
					}

					p.params.title = Translator.translation(l.LanguageTranslation);
					p.params.NavigationMenuId = l.InstanceMenuId;

					ViewService.view(l.view, p, options);
					break;
				}
			}
		};

		let navigatePortfolioObj = {
			'rows': [],
			'portfolio_id': null,
			'current_index': -1,
			'limit': 25,
			'restrictions': null,
			'active_item_id': 0
		};

		self.setNavigatePortfolioObj = function(key: string, stuff: object){
			navigatePortfolioObj[key] = stuff;
		}

		self.getNavigatePortfolio = function(key: string){
			if (!key) return navigatePortfolioObj;
			return navigatePortfolioObj[key];
		}
	}

	DynamicNavigation.$inject = [
		'$compile',
		'ViewService',
		'Common',
		'MessageService',
		'SidenavService',
		'HTMLService',
		'ViewConfigurator',
		'Translator',
		'Sanitizer'
	];

	function DynamicNavigation(
		$compile,
		ViewService,
		Common,
		MessageService,
		SidenavService,
		HTMLService,
		ViewConfigurator,
		Translator,
		Sanitizer
	) {
		return {
			link: function (scope, element) {

				scope.stickySidenav = SidenavService.getStickyNavigation();

				let index = typeof scope.options.index === 'undefined' || scope.options.index == 0
					? 0 : scope.options.index;
				let realIndex = 0;

				let l = scope.$on('accordion-navigation-dynamic:ready', function () {
					if (realIndex !== -1) {
						if (index > counter) {
							realIndex--;
						}

						scope['accordion-navigation-dynamic'].open({index: realIndex});
					}
					l();
				});

				let html = HTMLService.initialize('accordion')
					.attr('id', 'navigation-dynamic')
					.attr('control', 'navigation-dynamic')
					.attr('multiple');

				let parent = ViewService.getParentView();
				if (parent) {
					scope.upTab = {
						name: 'UP',
						view: parent.name,
						params: parent.params,
						hideSplit: true,
						hideStick: true
					};

					let up = Sanitizer(Translator.translate('UP'));

					html.inner(
						HTMLService.initialize('pane-mockup').class('pointer no-select').click('openView(upTab)')
							.inner(HTMLService.initialize('pane-header').tabindex("0").class("aria-detect").ariaLabel(up)
								.inner(HTMLService.initialize()
									.icon('keyboard_arrow_up', 'white navigation-back')
									.element('span').attr('tooltip', up).content(up)
								)
							)
					);
				}

				let counter = -1;
				let newHeaders = [];
				let newSubHeader = function (previousTabName, subchilds, group, active, i) {
					if (subchilds.length > 1) {
						angular.forEach(subchilds, function (c) {
							c.visible = false;
							c.name = c.name.replace("[" + previousTabName + "]", "");
							c.childClass = "menu-child";
						});

						//Add new group
						newHeaders.push({
							name: previousTabName,
							view: "",
							subchilds: subchilds,
							visible: true,
							active: active,
							position: i - subchilds.length - 1
						});
						return 1;
					}
					return 0;
				};

				let findSimilarity = function (str1: string): string {
					let foundAt = _.lastIndexOf(str1, "]");
					if (foundAt > 0) {
						str1 = str1.substr(1, foundAt - 1);
					}

					return str1;
				};

				let findProperIcon = function (type){
					if(type == 10) return "pie_chart";
					if(type == 6) return "format_list_numbered";
					if(type == 5) return "view_compact";
					if(type == 1) return "format_align_center";
					if(type == 8) return "equalizer";
					return "format_align_justify";
				}

				//Loop through all groups
				angular.forEach(scope.navigation, function (group) {
					let previousTabName = "";
					let previousTab;
					let thisTabName;
					let subchilds = [];
					let active = false;
					let i = 1;

					//Loop through all tabs
					angular.forEach(group.tabs, function (tab) {
						if(!tab.icon && tab.portfolioDefaultView && tab.portfolioDefaultView != -1) tab.icon = findProperIcon(tab.portfolioDefaultView);

						if (previousTab) subchilds.push(previousTab);
						tab.visible = true;
						tab.name = tab.hasOwnProperty('customName') && Translator.translate(tab.customName).length > 0 ? Translator.translate(tab.customName) : Translator.translate(tab.name);
						thisTabName = findSimilarity(tab.name);
						if (tab.$$activeTab == true) active = true;
						if (previousTabName != thisTabName) {
							i += newSubHeader(previousTabName, subchilds, group, active, i);
							subchilds = [];
							active = false;
						}

						previousTabName = thisTabName;
						previousTab = tab;
						i += 1;
					});

					if (previousTab) subchilds.push(previousTab);
					newSubHeader(previousTabName, subchilds, group, active, i);

					//Add new subheaders
					angular.forEach(newHeaders, function (header) {
						group.tabs.splice(header.position, 0, header);
					});
					newHeaders = [];
				});


				angular.forEach(scope.navigation, function (group, key) {
					
					counter++;

					let groupString = 'navigation[' + Sanitizer(key) + ']';
					let tabsLength = group.tabs.length;

					if (tabsLength > 1 || Common.isTrue(group.showGroup)) {
						if (counter < index) {
							realIndex++;
						}

						if (group.Subtitle) {
							html.inner(
								HTMLService.initialize('pane')
									.attr("style", "text-align: center;padding-right: 58px;")
									.ariaLabel('{{::' + groupString + '.name | translate}}')
									.attrConditional(group.expand, 'expand')
									.class("subtitle")
									.inner(HTMLService.initialize('pane-sub-header').bind('::' + groupString + '.name | translate')
									)
							);
						} else {
							html.inner(
								HTMLService.initialize('pane')
									.ariaLabel('{{::' + groupString + '.name | translate}}')
									.attrConditional(group.expand, 'expand')
									.inner(HTMLService.initialize('pane-header')
										.tabindex("0").class("aria-detect")

										.inner(HTMLService.initialize('i')
											.attr("aria-hidden", "true")
											.class('material-icons white aria-detect')
											.ngClass('::' + groupString + '.iconClass')
											.bind('::' + groupString + '.icon')
											.element('a').class('ellipsis').bind('::' + groupString + '.name | translate').attr("aria-expanded", group.expand ? "true" : "false")
										)
										.element('pane-content')
										.inner(HTMLService.initialize('div')
											.tabindex("{{(" + (group.expand ? '1==1' : '1==0') + " || tab.$$activeTab || isChildActive(tab)) ? '0' : '-1'}}")
											.class("aria-detect")
											.repeat('tab in ::' + groupString + '.tabs')
											.click('openView(tab)')
											.class('no-select pointer')
											.ngClass('{ active: tab.$$activeTab || isChildActive(tab) }')
											.ariaLabel('{{::tab.name | translate}}')
											.if('tab.visible')
											.inner(HTMLService.initialize('i')
												.class('material-icons white')
												.ngClass('::tab.iconClass')
												.bind('::tab.icon')
												.element('span').class('ellipsis stretch').ngClass('::tab.childClass')
												.attr('tooltip', '{{::tab.name | translate}}')
												.bind('::tab.name | translate')
												.button()
												.click('openMenu(tab, $event)')
												.attr('type', 'icon')
												.attr('minimal')
												.attr('no-waves')
												.if('::hasMenu(tab)')
												.inner(HTMLService.initialize().icon('{{::hasMenu(tab)}}')))
										)
									)
							);
						}
					} else if (tabsLength) {
						if (counter == index) realIndex = -1;

						let tab = groupString + '.tabs[0]';
						let pane =
							HTMLService
								.initialize('pane-mockup')
								.class('pointer no-select')
								.click('openView(' + tab + ')');

						let content = HTMLService.initialize('pane-header')
							.tabindex("0")
							.class("aria-detect")
							.ariaLabel('{{::' + groupString + '.name | translate}}')
							.ngClass('{ active: ' + tab + '.$$activeTab }');

						if (group.tabs[0].icon) {
							content
								.inner(HTMLService.initialize('i')
									.class('material-icons white')
									.ngClass('::' + tab + '.iconClass')
									.bind('::' + tab + '.icon')
									.element('span')
									.class('ellipsis')
									.attr('tooltip', '{{::' + tab + '.name}}')
									.bind('::' + tab + '.name | translate')
								);
						} else {
							content
								.inner(HTMLService.initialize('i')
									.class('material-icons white')
									.ngClass('::' + groupString + '.iconClass')
									.bind('::' + groupString + '.icon')
									.element('span')
									.class('ellipsis')
									.attr('tooltip', '{{::' + groupString + '.name}}')
									.bind('::' + groupString + '.name | translate')
								);
						}

						content
							.button()
							.click('openMenu(' + tab + ', $event)')
							.attr('minimal')
							.attr('type', 'icon')
							.attr('no-waves')
							.if('::hasMenu(' + tab + ')')
							.inner(HTMLService.initialize().icon('{{::hasMenu(' + tab + ')}}', 'white'));

						html.inner(pane.inner(content));
					}
				});

				html
					.inner(HTMLService.initialize('pane').if('stickySidenav.length')
						.inner(HTMLService.initialize('pane-header').tabindex("0").class("aria-detect")
							.inner(HTMLService.initialize('span').class('ellipsis')
								.content(Sanitizer(
									Translator.translation(
										ViewConfigurator.getGlobal('sticky').LanguageTranslation
									))
								))
							.element('pane-content')
							.inner(HTMLService.initialize('div')
								.repeat('s in stickySidenav')
								.class('no-select pointer')
								.click('openSticky(s.Value)')
								.ngClass('{ active: s.$$activeTab }')
								.inner(HTMLService.initialize()
									.element('i')
									.class('material-icons white')
									.bind('::s.Value.icon')
									.element('span').class('ellipsis stretch')
									.attr('tooltip', '{{s.translation}}')
									.content('{{s.translation}}')
									.button()
									.if('::showStickyEdit(s)')
									.click('editSticky(s, $event)')
									.attr('type', 'icon')
									.attr('minimal')
									.attr('no-waves')
									.inner(HTMLService.initialize().icon('edit'))
								)
							)
						)
					);
				$compile(html.getHTML())(scope).appendTo(element);
			},
			controller: function ($scope, $rootScope) {
				let activeTabs = {};

				let handler = $rootScope.$on('viewChange', function (event, parameters) {
					switch (parameters.action) {
						case 'NEW':
							angular.forEach(activeTabs, function (tab) {
								tab.$$activeTab = false;
							});

							activeTabs = {};
						case 'SPLIT':
							let i = $scope.navigation.length;
							let found = false;

							while (i-- && !found) {
								let group = $scope.navigation[i];
								let j = group.tabs.length;

								while (j--) {
									let tab = group.tabs[j];
									let f = true;

									if (tab.view != parameters.view) {
										continue;
									}

									if (tab.params) {
										angular.forEach(tab.params, function (param, key) {
											if (parameters.parameters[key] != param) {
												f = typeof param === 'object' ?
													angular.equals(parameters.parameters[key], param) : false
											}
										});
									} else if (tab.name !== parameters.name) {
										f = false;
									}

									if (f) {
										found = true;
										tab.$$activeTab = true;
										activeTabs[parameters.viewId] = tab;
										break;
									}
								}
							}

							break;
						case 'CLOSE':
							if (activeTabs[parameters.viewId]) {
								activeTabs[parameters.viewId].$$activeTab = false;
								delete activeTabs[parameters.viewId];
							}

							break;
					}
				});

				$scope.$on('$destroy', function () {
					angular.forEach(activeTabs, function (tab) {
						tab.$$activeTab = false;
					});

					handler();
				});

				$scope.openView = function (tab) {
					if (typeof tab.onClick === 'function') {
						tab.onClick();
						return;
					}
					if (typeof tab.params === 'undefined') tab.params = {};

					if (tab.subchilds && tab.subchilds.length > 0) {
						angular.forEach(tab.subchilds, function (childtab) {
							childtab.visible = !childtab.visible;
						});
					} else {
						ViewService.view(tab.view, {params: tab.params});
					}
				};

				$scope.isChildActive = function (tab) {
					let r = false;
					angular.forEach(tab.subchilds, function (childtab) {
						if (childtab.$$activeTab) {
							r = true;
							return;
						}
					});
					return r;
				};

				$scope.hasMenu = function (tab) {
					let icon = '';
					let count = 0;

					if (tab.subchilds && tab.subchilds.length > 0) return;

					angular.forEach(tab.menu, function (m) {
						icon = m.icon ? m.icon : 'error_outline';
						count++;
					});

					if (typeof tab.onClick === 'undefined') {
						if (Common.isFalse(tab.hideSplit)) {
							icon = 'open_in_new';
							count++;
						}

						if (Common.isFalse(tab.hideStick) && SidenavService.isStickable(tab)) {
							icon = 'star_border';
							count++;
						}
					}

					if (count > 1) {
						return 'more_vert';
					} else {
						return icon;
					}
				};

				$scope.openSticky = function (sticky) {
					ViewService.view(sticky.view, {params: sticky.params});
				};

				$scope.showStickyEdit = function (sticky) {
					return SidenavService.canEditSticky(sticky);
				};

				$scope.editSticky = function (sticky, event) {
					event.stopPropagation();

					SidenavService.editSticky(sticky);
				};

				$scope.openMenu = function (tab, event) {
					event.stopPropagation();
					let menu = [];

					angular.forEach(tab.menu, function (m) {
						menu.push(m);
					});

					if (typeof tab.onClick === 'undefined') {
						if (Common.isFalse(tab.hideSplit)) {
							menu.push({
								name: 'SIDENAV_SPLIT',
								onClick: function () {
									if (typeof tab.params === 'undefined') {
										tab.params = {};
									}

									ViewService.view(tab.view, {params: tab.params}, {split: true});
								}
							});
						}

						if (Common.isFalse(tab.hideStick) && SidenavService.isStickable(tab)) {
							menu.push({
								name: 'SIDENAV_STICK',
								onClick: function () {
									SidenavService.stickNavigation(tab);
								}
							});
						}
					}

					if (menu.length) {
						if (menu.length === 1) {
							menu[0].onClick();
						} else {
							MessageService.menu(menu, event.currentTarget);
						}
					}
				};
			}
		}
	}

})();
