(function () {
	'use strict';

	angular
		.module('core')
		.config(MenuNavigationConfig)
		.service('MenuNavigationService', MenuNavigationService);

	MenuNavigationConfig.$inject = ['ViewsProvider'];
	function MenuNavigationConfig(ViewsProvider) {
		ViewsProvider.addView('instanceMenu', {
			controller: 'MenuNavigationController',
			template: 'core/navigation/navigation.html',
			resolve: {
				Icons: function (ListsService) {
					return ListsService.getSystemLists('SYMBOLS');
				},
				Menu: function (MenuNavigationService) {
					return MenuNavigationService.menu();
				},
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				},
				UserGroups: function (MenuNavigationService) {
					return MenuNavigationService.getUserGroups();
				}
			},
			afterResolve: {
				Navigation: function (ViewService, SidenavService) {
					SidenavService.navigation([{
						name: 'INSTANCE_MENU',
						tabs: [
							{
								name: 'INSTANCE_MENU_FRONTPAGE',
								onClick: function () {
									ViewService.view('menuNavigation.frontpage');
								}
							},
							{
								name: 'INSTANCE_MENU_GLOBAL',
								onClick: function () {
									ViewService.view('menuNavigation.global');
								}
							}]
					}]);
				}
			}
		});

		ViewsProvider.addView('menuNavigation.global', {
			template: 'core/navigation/global/global.html',
			controller: 'GlobalNavigationController',
			resolve: {
				Links: function (ViewConfigurator) {
					return ViewConfigurator.getGlobals();
				},
				UserGroups: function (MenuNavigationService) {
					return MenuNavigationService.getUserGroups();
				}
			},
			afterResolve: {
				Navigation: function (ViewService, SidenavService) {
					SidenavService.navigation([{
						name: 'INSTANCE_MENU',
						tabs: [
							{
								name: 'INSTANCE_MENU',
								onClick: function () {
									ViewService.view('instanceMenu');
								}
							},
							{
								name: 'INSTANCE_MENU_FRONTPAGE',
								onClick: function () {
									ViewService.view('menuNavigation.frontpage');
								}
							}]
					}]);
				}
			}
		});

		ViewsProvider.addView('menuNavigation.frontpage', {
			template: 'core/navigation/frontpage/frontpage.html',
			controller: 'FrontpageNavigationController',
			resolve: {
				Widgets: function (MenuNavigationService) {
					return MenuNavigationService.frontpage();
				},
				UserGroups: function (MenuNavigationService) {
					return MenuNavigationService.getUserGroups();
				}
			},
			afterResolve: {
				Navigation: function (ViewService, SidenavService) {
					SidenavService.navigation([{
						name: 'INSTANCE_MENU',
						tabs: [
							{
								name: 'INSTANCE_MENU',
								onClick: function () {
									ViewService.view('instanceMenu');
								}
							},
							{
								name: 'INSTANCE_MENU_GLOBAL',
								onClick: function () {
									ViewService.view('menuNavigation.global');
								}
							}]
					}]);
				}
			}
		});

		ViewsProvider.addDialog('menuNavigation.states', {
			template: 'core/navigation/support/states.html',
			controller: 'MenuNavigationStatesController'
		});

		ViewsProvider.addDialog('menuNavigation.links', {
			template: 'core/navigation/support/links.html',
			controller: 'MenuNavigationLinksController'
		});

		ViewsProvider.addPage('instanceMenu', 'PAGE_NAVIGATION', ['read', 'write', 'delete']);
	}

	MenuNavigationService.$inject = ['ApiService', 'ProcessService', 'ViewConfigurator', 'Translator'];
	function MenuNavigationService(ApiService, ProcessService, ViewConfigurator, Translator) {
		var self = this;
		var InstanceMenu = ApiService.v1api('navigation/InstanceMenus');

		self.getUserGroups = function () {
			return ProcessService.getItems('user_group', undefined, true).then(function (groups) {
				var g = [];

				var i = groups.length;
				while (i--) {
					g.push({
						value: groups[i].item_id,
						name: groups[i].usergroup_name
					});
				}

				return g;
			});
		};

		self.menu = function () {
			return InstanceMenu.get();
		};

		self.frontpage = function () {
			return InstanceMenu.get('frontpage');
		};

		self.saveMenu = function (menuId, menu) {
			return InstanceMenu.save(menuId, menu);
		};

		self.saveMenus = function (menus) {
			return InstanceMenu.save('', menus);
		};

		self.newMenus = function (menu) {
			return InstanceMenu.new('', menu);
		};

		self.deleteMenu = function (menuIds) {
			return InstanceMenu.delete(menuIds);
		};

		self.deleteWidget = function (widgetIds) {
			return InstanceMenu.delete(['frontpage', widgetIds]).then(function () {
				return ViewConfigurator.removeWidgets(widgetIds);
			});
		};

		self.saveWidget = function (widgetId, widget) {
			return InstanceMenu.save(['frontpage', widgetId], widget);
		};

		self.saveWidgets = function (widgets) {
			return InstanceMenu.save('frontpage', widgets);
		};

		self.newWidgets = function (widgets) {
			widgets = angular.copy(widgets);
			return InstanceMenu.new('frontpage', widgets).then(function (ids) {
				for (var i = 0, l = widgets.length; i < l; i++) {
					var w = widgets[i];

					for (var j = 0, l_ = ids.length; i < l_; i++) {
						w.InstanceMenuId = ids[j];
						ViewConfigurator.addWidget(w);
					}
				}

				return widgets;
			});
		};

		self.getPageTemplate = function (p) {
			if (typeof p === 'undefined') {
				p = {};
			}

			return {
				DefaultState: p.view,
				LinkType: 0,
				Params: p.params,
				name: p.translation
			};
		};

		self.getPortfolioTemplate = function (p) {
			if (typeof p === 'undefined') {
				p = {};
			}

			return {
				DefaultState: 'portfolio',
				Icon: 'folder',
				LinkType: 1,
				Params: {
					process: p.Process,
					portfolioId: p.ProcessPortfolioId
				},
				name: Translator.translation(p.LanguageTranslation)
			};
		};

		self.getProcessTemplate = function (p) {
			if (typeof p === 'undefined') {
				p = {};
			}

			return {
				DefaultState: 'new.process',
				Icon: 'add',
				LinkType: 2,
				Params: {
					process: p.ProcessName
				},
				name: Translator.translation(p.LanguageTranslation)
			};
		};

		self.getWidgetTemplate = function (w) {
			if (typeof w === 'undefined') {
				w = {};
			}

			return {
				DefaultState: w.view,
				LinkType: 4,
				Params: w.params
			};
		};
	}

})();