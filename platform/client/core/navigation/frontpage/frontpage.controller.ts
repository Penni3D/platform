(function () {
	'use strict';

	angular
		.module('core')
		.controller('FrontpageNavigationController', FrontpageNavigationController);

	FrontpageNavigationController.$inject = [
		'$scope',
		'Widgets',
		'UserGroups',
		'ViewService',
		'SaveService',
		'MenuNavigationService',
		'MessageService',
		'Translator',
		'ViewConfigurator',
		'$q',
		'Common',
		'StateParameters'
	];
	function FrontpageNavigationController(
		$scope,
		Widgets,
		UserGroups,
		ViewService,
		SaveService,
		MenuNavigationService,
		MessageService,
		Translator,
		ViewConfigurator,
		$q,
		Common,
		StateParameters
	) {
		var updatedWidgets = [];
		$scope.widgets = Widgets;
		$scope.userGroups = UserGroups;
		$scope.selectedLinks = [];

		$scope.header = {
			title: 'INSTANCE_MENU_FRONTPAGE'
		};

		$scope.onSort = function (current, prev, next) {
			if (typeof prev === 'undefined') {
				current.OrderNo = Common.getOrderNoBetween(undefined, next.OrderNo);
			} else if (typeof next === 'undefined') {
				current.OrderNo = Common.getOrderNoBetween(prev.OrderNo);
			} else {
				current.OrderNo = Common.getOrderNoBetween(prev.OrderNo, next.OrderNo);
			}

			$scope.onChange(current);
		};

		SaveService.subscribeSave($scope, function () {
			var promises = [];
			var i = updatedWidgets.length;
			while (i--) {
				var l = updatedWidgets[i];

				var j = $scope.widgets.length;
				while (j--) {
					if ($scope.widgets[j].InstanceMenuId == l) {
						promises.push(MenuNavigationService.saveMenu(l, $scope.widgets[j]));
						break;
					}
				}
			}


			return $q.all(promises).then(function () {
				return ViewConfigurator.fetchWidgets();
			});
		});

		ViewService.footbar(StateParameters.ViewId, { template: 'core/navigation/footbar.html' });

		$scope.onChange = function (widget) {
			if (updatedWidgets.indexOf(widget.InstanceMenuId) === -1) {
				updatedWidgets.push(widget.InstanceMenuId);
			}

			SaveService.tick();
		};

		$scope.clearSelection = function () {
			$scope.selectedLinks = [];
		};

		$scope.removeLinks = function () {
			var confirmText = Translator.translate('MENU_NAVIGATION_DELETE') + ' ' +
				$scope.selectedLinks.length + ' ' +
				Translator.translate('MENU_NAVIGATION_ITEMS').toLowerCase();

			MessageService.confirm(confirmText, function () {
				var p = '';
				var i = $scope.selectedLinks.length;
				while (i--) {
					var l = $scope.selectedLinks[i];
					p += l;

					if (i !== 0) {
						p += ',';
					}

					var j = $scope.widgets.length;
					while (j--) {
						if ($scope.widgets[j].InstanceMenuId == l) {
							$scope.widgets.splice(j, 1);
							break;
						}
					}
				}

				MenuNavigationService.deleteMenu(p).then(function () {
					$scope.selectedLinks = [];
					ViewConfigurator.fetchWidgets();
				});
			});
		};

		$scope.promote = function (widget) {
			widget.LinkType = 5;
			$scope.onChange(widget);
		};

		$scope.demote = function (widget) {
			widget.LinkType = 4;
			$scope.onChange(widget);
		};

		$scope.editWidget = function (widget) {
			MessageService.dialogAdvanced({
				controller: 'FrontpageWidgetController',
				template: 'frontpage/new/edit.html',
				locals: {
					Widget: widget,
					Refresh: function () {
						widget.translation = Translator.translation(widget.LanguageTranslation);
						MenuNavigationService.saveWidget(widget.InstanceMenuId, widget);
					}
				}
			});
		};
	}
})();