(function () {
	'use strict';

	angular
		.module('core')
		.controller('MenuNavigationController', MenuNavigationController);

	MenuNavigationController.$inject = [
		'$scope',
		'Icons',
		'Menu',
		'Portfolios',
		'Processes',
		'UserGroups',
		'SaveService',
		'Common',
		'$q',
		'MenuNavigationService',
		'MessageService',
		'StateParameters',
		'ViewService',
		'Views',
		'ViewConfigurator',
		'Translator',
		'SidenavService'
	];
	function MenuNavigationController(
		$scope,
		Icons,
		Menu,
		Portfolios,
		Processes,
		UserGroups,
		SaveService,
		Common,
		$q,
		MenuNavigationService,
		MessageService,
		StateParameters,
		ViewService,
		Views,
		ViewConfigurator,
		Translator,
		SidenavService
	) {
		$scope.navigation = Menu;
		$scope.userGroups = UserGroups;
		$scope.icons = Icons;
		$scope.selectedLinks = [];
		$scope.showGroup = true;
		$scope.header = {
			title: StateParameters.title
		};

		var updatedLinks = [];
		ViewService.footbar(StateParameters.ViewId, { template: 'core/navigation/footbar.html' });

		SaveService.subscribeSave($scope, function () {
			var promises = [];
			var i = updatedLinks.length;
			while (i--) {
				var l = updatedLinks[i];

				var j = $scope.navigation.length;
				while (j--) {
					var g = $scope.navigation[j];
					if (g.InstanceMenuId == l) {
						promises.push(MenuNavigationService.saveMenu(l, g));
						break;
					} else if (Array.isArray(g.Links)) {
						var k = g.Links.length;
						while (k--) {
							var link = g.Links[k];

							if (link.InstanceMenuId == l) {
								SidenavService.updateStaticNavigation(l, link);
								promises.push(MenuNavigationService.saveMenu(l, link));

								break;
							}
						}

						if (k !== -1) {
							break;
						}
					}
				}
			}

			return $q.all(promises)
		});

		$scope.onChange = function (link) {
			if (updatedLinks.indexOf(link.InstanceMenuId) === -1) {
				updatedLinks.push(link.InstanceMenuId);
			}

			SaveService.tick();
		};

		$scope.removeGroup = function (group) {
			var ids = [group.InstanceMenuId];

			if (Array.isArray(group.Links)) {
				var i = group.Links.length;
				while (i--) {
					ids.push(group.Links[i].InstanceMenuId);
				}
			}

			removeLinks(ids, group);
		};

		$scope.editStates = function (link) {
			ViewService.view('menuNavigation.states', {
				locals: {
					Link: link,
					UserGroups: UserGroups,
					States: Views.getPage(link.DefaultState).states
				}
			});
		};

		$scope.addLinks = function (group) {
			ViewService.view('menuNavigation.links', {
				locals: {
					Links: group.Links,
					Group: group,
					UserGroups: UserGroups,
					Processes: Processes,
					Portfolios: Portfolios,
					Icons: Icons
				}
			});
		};

		$scope.removeLinks = function () {
			if ($scope.selectedLinks.length) {
				removeLinks($scope.selectedLinks)
			}
		};

		$scope.clearSelection = function () {
			$scope.selectedLinks = [];
		};

		$scope.isConfigurable = function (link) {
			return ViewConfigurator.isConfigureable(link.DefaultState);
		};

		$scope.configure = function (link) {
			function onSave(params) {
				link.Params = params;
				$scope.onChange(link);
			}

			ViewConfigurator.configurePage(link.DefaultState, link.Params, onSave);
		};

		$scope.newPage = function (parent, event) {
			var locals = {
				Name: 'translation',
				Value: 'view',
				Label: 'NAVIGATION_NEW_PAGES',
				Options: Views.getPages(),
				Group: parent,
				Template: MenuNavigationService.getPageTemplate
			};

			showCard(locals, event);
		};

		$scope.newPortfolio = function (parent, event) {
			var locals = {
				Name: 'LanguageTranslation',
				Value: 'ProcessPortfolioId',
				Label: 'NAVIGATION_NEW_PORTFOLIOS',
				Options: Portfolios,
				Group: parent,
				Template: MenuNavigationService.getPortfolioTemplate
			};

			showCard(locals, event);
		};

		$scope.newProcess = function (parent, event) {
			var locals = {
				Name: 'LanguageTranslation',
				Value: 'ProcessName',
				Label: 'NAVIGATION_NEW_PROCESSES',
				Options: Processes,
				Group: parent,
				Template: MenuNavigationService.getProcessTemplate
			};

			showCard(locals, event);
		};

		$scope.newGroup = function () {
			var lastGroup = $scope.navigation[$scope.navigation.length - 1];
			var group = {
				OrderNo: Common.getOrderNoBetween(lastGroup.OrderNo, undefined)
			};

			MenuNavigationService.newMenus([group]).then(function (groupId) {
				group.InstanceMenuId = groupId[0].InstanceMenuId;
				group.Variable = groupId[0].Variable;

				$scope.navigation.push(group);
				$scope.addLinks(group);
			});
		};

		$scope.onSort = function (self, prev, next, group) {
			if (typeof prev === 'undefined') {
				self.OrderNo = Common.getOrderNoBetween(undefined, next.OrderNo);
			} else if (typeof next === 'undefined') {
				self.OrderNo = Common.getOrderNoBetween(prev.OrderNo);
			} else {
				self.OrderNo = Common.getOrderNoBetween(prev.OrderNo, next.OrderNo);
			}

			if (group !== 'root') {
				self.ParentId = group.InstanceMenuId;
			}

			$scope.onChange(self);
		};

		function showCard(locals, event) {
			var p = {
				template: 'core/navigation/support/card.html',
				controller: 'MenuNavigationCardController',
				locals: locals
			};

			MessageService.card(p, event.currentTarget);
		}

		function removeLinks(links, group) {
			var confirmText;

			if (typeof group === 'undefined') {
				confirmText = Translator.translate('MENU_NAVIGATION_DELETE') + ' ' + links.length + ' ' + Translator.translate('MENU_NAVIGATION_ITEMS').toLowerCase();
			} else {
				confirmText = Translator.translate('MENU_NAVIGATION_GROUP') + ' ' + Translator.translation(group.LanguageTranslation) + ' ' +
					Translator.translate('MENU_NAVIGATION_GROUP_AND_TABS_REMOVED').toLowerCase() + ' ' + (links.length - 1) + ' ' + Translator.translate('MENU_NAVIGATION_ITEMS').toLowerCase();
			}

			MessageService.confirm(confirmText, function () {
				var p = '';
				var i = links.length;
				while (i--) {
					var l = links[i];
					p += l;

					if (i !== 0) {
						 p += ',';
					}

					var j = $scope.navigation.length;
					while (j--) {
						var g = $scope.navigation[j];
						if (g.InstanceMenuId == l) {
							$scope.navigation.splice(j, 1);
							break;
						} else if (Array.isArray(g.Links)) {
							var k = g.Links.length;
							while (k--) {
								if (g.Links[k].InstanceMenuId == l) {
									g.Links.splice(k, 1);
									break;
								}
							}

							if (k !== -1) {
								break;
							}
						}
					}
				}

				MenuNavigationService.deleteMenu(p).then(function () {
					$scope.selectedLinks = [];
				});
			});
		}
	}

})();