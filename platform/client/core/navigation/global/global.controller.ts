(function () {
	'use strict';

	angular
		.module('core')
		.controller('GlobalNavigationController', GlobalNavigationController);

	GlobalNavigationController.$inject = [
		'$scope',
		'MessageService',
		'Links',
		'UserGroups',
		'ViewService',
		'SaveService',
		'MenuNavigationService',
		'$q',
		'ViewConfigurator',
		'Translator',
		'StateParameters',
		'Globals'
	];
	function GlobalNavigationController(
		$scope,
		MessageService,
		Links,
		UserGroups,
		ViewService,
		SaveService,
		MenuNavigationService,
		$q,
		ViewConfigurator,
		Translator,
		StateParameters,
		Globals
	) {
		var updatedLinks = [];
		$scope.selectedLinks = [];
		$scope.group = {
			Links: Links
		};

		$scope.header = {
			title: 'INSTANCE_MENU_GLOBAL'
		};

		ViewService.footbar(StateParameters.ViewId, { template: 'core/navigation/footbar.html' });

		$scope.userGroups = UserGroups;
		$scope.editStates = function (link) {
			ViewService.view('menuNavigation.states', {
				locals: {
					Link: link,
					UserGroups: UserGroups,
					States: Globals.getGlobal(link.DefaultState).states
				}
			});
		};

		$scope.newGlobal = function (event) {
			var options = [];
			var globals = Globals.getGlobals();
			angular.forEach(globals, function (g) {
				var f = false;

				angular.forEach(Links, function (l) {
					if (g.view == l.DefaultState) {
						f = true;
					}
				});

				if (!f) {
					options.push(g);
				}
			});


			var p = {
				template: 'core/navigation/support/card.html',
				controller: 'MenuNavigationCardController',
				locals: {
					Name: 'view',
					Value: 'view',
					Label: 'NAVIGATION_NEW_GLOBALS',
					Options: options,
					Group: $scope.group,
					Template: function (g) {
						return {
							LinkType: 3,
							Params: {},
							DefaultState: g.view,
							name: g.view
						};
					}
				}
			};

			MessageService.card(p, event.currentTarget);
		};

		$scope.clearSelection = function () {
			$scope.selectedLinks = [];
		};

		SaveService.subscribeSave($scope, function () {
			var promises = [];
			var i = updatedLinks.length;
			while (i--) {
				var l = updatedLinks[i];
				var j = $scope.group.Links.length;
				while (j--) {
					var g = $scope.group.Links[j];
					if (g.InstanceMenuId == l) {
						promises.push(MenuNavigationService.saveMenu(l, g));
						break;
					}
				}
			}

			return $q.all(promises)
		});

		$scope.onChange = function (link) {
			if (updatedLinks.indexOf(link.InstanceMenuId) === -1) {
				updatedLinks.push(link.InstanceMenuId);
			}

			SaveService.tick();
		};

		$scope.isConfigurable = function (link) {
			return ViewConfigurator.isConfigureable(link.DefaultState);
		};

		$scope.configure = function (link) {
			function onSave(params) {
				link.Params = params;
				$scope.onChange(link);
			}

			ViewConfigurator.configurePage(link.DefaultState, link.Params, onSave);
		};

		$scope.removeLinks = function () {
			var confirmText = Translator.translate('MENU_NAVIGATION_DELETE') + ' ' +
				$scope.selectedLinks.length + ' ' +
				Translator.translate('MENU_NAVIGATION_ITEMS').toLowerCase();

			MessageService.confirm(confirmText, function () {
				var p = '';
				var i = $scope.selectedLinks.length;
				while (i--) {
					var l = $scope.selectedLinks[i];
					p += l;

					if (i !== 0) {
						p += ',';
					}

					var j = $scope.group.Links.length;
					while (j--) {
						if ($scope.group.Links[j].InstanceMenuId == l) {
							$scope.group.Links.splice(j, 1);
							break;
						}
					}
				}

				MenuNavigationService.deleteMenu(p).then(function () {
					$scope.selectedLinks = [];
				});
			});
		}
	}
})();