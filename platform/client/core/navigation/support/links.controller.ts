(function () {
	'use strict';

	angular
		.module('core')
		.controller('MenuNavigationStatesController', MenuNavigationStatesController)
		.controller('MenuNavigationLinksController', MenuNavigationLinksController);

	MenuNavigationStatesController.$inject = [
		'$scope',
		'Link',
		'UserGroups',
		'Views',
		'MenuNavigationService',
		'MessageService',
		'States',
		'ViewConfigurator'
	];
	function MenuNavigationStatesController(
		$scope,
		Link,
		UserGroups,
		Views,
		MenuNavigationService,
		MessageService,
		States,
		ViewConfigurator
	) {
		$scope.isActive = true;
		$scope.states = [];
		
		let z = "";
		for (let s of States) {
			$scope.states.push({
				name: ('MENU_STATE_' + s).toUpperCase(),
				value: s
			});
			z += ('"MENU_STATE_' + s).toUpperCase() + '": "",';
		}
		console.log(z);
		
		$scope.link = angular.copy(Link);
		$scope.userGroups = {};

		let i = UserGroups.length;
		while (i--) {
			let u = UserGroups[i];
			$scope.userGroups[u.value] = u.name;
		}

		$scope.statesChanged = function (states) {
			if (!$scope.lock) return;

			let i = $scope.link.UserGroups.length;
			while (i--) {
				$scope.link.UserGroupStates[$scope.link.UserGroups[i]] = states;
			}
		};

		$scope.save = function () {
			$scope.isActive = false;
			MenuNavigationService.saveMenu(Link.InstanceMenuId, $scope.link).then(function () {
				Link.UserGroupStates = $scope.link.UserGroupStates;
				ViewConfigurator.clearRights(Link.DefaultState);
				MessageService.closeDialog();
			});
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		}
	}

	MenuNavigationLinksController.$inject = [
		'$scope',
		'Links',
		'Group',
		'Processes',
		'Portfolios',
		'Icons',
		'MessageService',
		'MenuNavigationService',
		'Common',
		'UserGroups',
		'Translator',
		'Views'
	];
	function MenuNavigationLinksController(
		$scope,
		Links,
		Group,
		Processes,
		Portfolios,
		Icons,
		MessageService,
		MenuNavigationService,
		Common,
		UserGroups,
		Translator,
		Views
	) {
		$scope.index = 0;
		$scope.onEdit = [];
		$scope.icons = Icons;
		$scope.userGroups = UserGroups;
		$scope.continue = false;
		$scope.return = false;
		$scope.nextButton = Translator.translate('NAVIGATION_NEXT');

		var templates = {};
		var initialized = false;
		var pages = Views.getPages();

		$scope.phases = [{
			name: 'NAVIGATION_SELECTIONS',
			onClick: function () {
				$scope.onEdit = [];
				initialized = false;
				$scope.phases.length = 1;
			},
			canContinue: function () {
				var i = $scope.inputs.length;
				while (i--) {
					if ($scope.inputs[i].model.length) {
						return true;
					}
				}

				return false;
			}
		}];

		$scope.inputs = [
			{
				options: pages,
				name: 'translation',
				value: 'view',
				model: [],
				label: 'NAVIGATION_NEW_PAGES'
			},
			{
				options: Portfolios,
				name: 'LanguageTranslation',
				value: 'ProcessPortfolioId',
				model: [],
				label: 'NAVIGATION_NEW_PORTFOLIOS'
			},
			{
				options: Processes,
				name: 'LanguageTranslation',
				value: 'ProcessName',
				model: [],
				label: 'NAVIGATION_NEW_PROCESSES'
			}];

		$scope.back = function () {
			if ($scope.index) {
				$scope.phases[--$scope.index].onClick();
				$scope.onChange();
			} else {
				$scope.return = false;
			}

			$scope.nextButton = Translator.translate('NAVIGATION_NEXT');
		};

		$scope.next = function () {
			if ($scope.index && ($scope.index + 1 == $scope.phases.length)) {
				$scope.continue = false;
				$scope.return = false;

				var payload = [];
				angular.forEach(templates, function (v) {
					var i = v.length;
					while (i--) {
						payload.push(v[i]);
					}
				});

				MenuNavigationService.newMenus(payload).then(function (ids) {
					if (!Array.isArray(Group.Links)) {
						Group.Links = [];
					}

					var i = payload.length;
					while (i--) {
						var p = payload[i];
						p.InstanceMenuId = ids[i];
						Group.Links.unshift(p);
					}

					MessageService.closeDialog();
				});
			} else {
				$scope.return = true;

				if (!initialized) {
					generateTemplateMenu();
				}

				$scope.phases[++$scope.index].onClick();
				$scope.onChange();

				if ($scope.index + 1 == $scope.phases.length) {
					$scope.nextButton = Translator.translate('NAVIGATION_CREATE');
				}
			}
		};

		$scope.onChange = function () {
			$scope.continue = $scope.phases[$scope.index].canContinue();
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		function generateTemplateMenu() {
			initialized = true;
			templates = {
				pages: [],
				portfolios: [],
				processes: []
			};

			var i = $scope.inputs[0].model.length;
			while (i--) {
				var j = pages.length;
				while (j--) {
					if (pages[j].view === $scope.inputs[0].model[i]) {
						var t = MenuNavigationService.getPageTemplate(pages[j]);

						t.ParentId = Group.InstanceMenuId;
						t.LanguageTranslation = t.name;
						t.OrderNo = templates.pages.length ?
							Common.getOrderNoBetween(undefined, templates.pages[0].OrderNo) : Common.getOrderNoBetween();

						templates.pages.unshift(t);
						break;
					}
				}
			}

			if (templates.pages.length) {
				$scope.phases.push({
					name: 'NAVIGATION_PAGES',
					onClick: function () {
						if (templates.pages.length) {
							$scope.onEdit = templates.pages;
						} else {
							$scope.next();
						}
					},
					canContinue: hasTranslations
				});
			}

			var i = $scope.inputs[1].model.length;
			while (i--) {
				var j = Portfolios.length;
				while (j--) {
					if (Portfolios[j].ProcessPortfolioId == $scope.inputs[1].model[i]) {
						var t = MenuNavigationService.getPortfolioTemplate(Portfolios[j]);
						t.ParentId = Group.InstanceMenuId;
						t.LanguageTranslation = t.name;
						t.OrderNo = templates.portfolios.length ?
							Common.getOrderNoBetween(undefined, templates.portfolios[0].OrderNo) : Common.getOrderNoBetween();

						templates.portfolios.unshift(t);
						break;
					}
				}
			}

			if (templates.portfolios.length) {
				$scope.phases.push({
					name: 'NAVIGATION_PORTFOLIOS',
					onClick: function () {
						if (templates.portfolios.length) {
							$scope.onEdit = templates.portfolios;
						} else {
							$scope.next();
						}
					},
					canContinue: hasTranslations
				});
			}

			var i = $scope.inputs[2].model.length;
			while (i--) {
				var j = Processes.length;
				while (j--) {
					if (Processes[j].ProcessName == $scope.inputs[2].model[i]) {
						var t = MenuNavigationService.getProcessTemplate(Processes[j]);
						t.ParentId = Group.InstanceMenuId;
						t.LanguageTranslation = t.name;
						t.OrderNo = templates.processes.length ?
							Common.getOrderNoBetween(undefined, templates.processes[0].OrderNo) : Common.getOrderNoBetween();

						templates.processes.unshift(t);
						break;
					}
				}
			}

			if (templates.processes.length) {
				$scope.phases.push({
					name: 'NAVIGATION_PROCESSES',
					onClick: function () {
						if (templates.processes.length) {
							$scope.onEdit = templates.processes;
						} else {
							$scope.next();
						}
					},
					canContinue: hasTranslations
				});
			}
		}

		function hasTranslations() {
			var r = false;
			var i = $scope.onEdit.length;
			while (i--) {
				var e = $scope.onEdit[i];

				angular.forEach(e.LanguageTranslation, function (v) {
					if (v.length) {
						r = true;
					}
				});

				if (!r) {
					return false;
				}
			}

			return r;
		}
	}
})();