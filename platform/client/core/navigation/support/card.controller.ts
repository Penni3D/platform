(function () {
	'use strict';

	angular
		.module('core')
		.controller('MenuNavigationCardController', MenuNavigationCardController);

	MenuNavigationCardController.$inject = [
		'$scope',
		'Group',
		'Template',
		'Options',
		'Name',
		'Value',
		'Label',
		'MenuNavigationService',
		'Common',
		'MessageService',
		'Translator'
	];
	function MenuNavigationCardController(
		$scope,
		Group,
		Template,
		Options,
		Name,
		Value,
		Label,
		MenuNavigationService,
		Common,
		MessageService,
		Translator
	) {
		$scope.title = Translator.translation(Group.LanguageTranslation);
		$scope.options = Options;
		$scope.name = Name;
		$scope.value = Value;
		$scope.label = Label;
		$scope.selected = [];
		$scope.continue = false;
		$scope.links = [];
		$scope.nextButton = Translator.translate('NAVIGATION_NEXT');

		var lastOrderNo = Array.isArray(Group.Links) && Group.Links.length ?
			lastOrderNo = Group.Links[Group.Links.length - 1].OrderNo : undefined;

		$scope.cancel = function () {
			MessageService.closeCard();
		};

		$scope.onSelectChange = function () {
			$scope.continue = !!$scope.selected.length;
		};

		$scope.onTranslationChange = function () {
			var i = $scope.links.length;
			while (i--) {
				var f = false;
				angular.forEach($scope.links[i].LanguageTranslation, function (v) {
					if (v.length) {
						f = true;
					}
				});

				if (!f) {
					break;
				}
			}

			if (i === -1) {
				$scope.continue = true;
			}
		};

		$scope.next = function () {
			if ($scope.translationStep) {
				$scope.continue = false;
				MenuNavigationService.newMenus($scope.links).then(function (ids) {
					if (!Array.isArray(Group.Links)) {
						Group.Links = [];
					}

					for (var i = 0, l = $scope.links.length; i < l; i++) {
						var p = $scope.links[i];
						p.InstanceMenuId = ids[i].InstanceMenuId;
						p.Variable = ids[i].Variable;
						Group.Links.push(p);
					}

					MessageService.closeCard();
				});
			} else if ($scope.selected.length) {
				$scope.continue = false;
				$scope.nextButton = Translator.translate('NAVIGATION_CREATE');
				var i = $scope.selected.length;
				while (i--) {
					var j = Options.length;
					while (j--) {
						var o = Options[j];

						if ($scope.selected[i] == o[Value]) {
							var t = Template(o);
							t.ParentId = Group.InstanceMenuId;
							t.LanguageTranslation = t.name;
							t.OrderNo = $scope.links.length ?
								Common.getOrderNoBetween($scope.links[0].OrderNo, lastOrderNo) : Common.getOrderNoBetween(lastOrderNo);

							$scope.links.unshift(t);
							break;
						}
					}
				}

				$scope.translationStep = true;
				$scope.onTranslationChange();
			}
		};
	}
})();