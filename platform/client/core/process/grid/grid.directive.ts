(function () {
	'use strict';

	angular
		.module('core.process')
		.directive('processGrid', ProcessGrid);

	ProcessGrid.$inject = ['SettingsService', 'MessageService', 'Common', 'ViewService', 'ProcessesService', 'Translator', '$filter','ApiService'];

	function ProcessGrid(SettingsService, MessageService, Common, ViewService, ProcessesService, Translator, $filter, ApiService) {
		return {
			controller: function ($scope) {
				let process = $scope.$parent.process;
				$scope.deleted = false;

				//This sets $scope.inEdit to be the same as parent.inEdit (tab.controller.ts)
				$scope.$parent.$watch('inEdit', function () {
				});
				$scope.TranslateDataType = function (ic) {
					let e = "";
					if (ic.DataType == 6) e = " (" + Translator.translate(ic.DataAdditional) + ")";
					return Common.getDataTypeTranslation(ic.DataType) + e;
				}


				$scope.editValidation = function (grid, field) {
					let dialParameters = {
						'ProcessContainerColumn': field,
						'ProcessContainerId': grid.Id
					};
					let StateParameters = {
						process: process,
						$$originalProcess: process,
						ViewId: "?"
					}
					let params = {
						template: 'settings/containers/containerColumnsValidation.html',
						controller: 'ContainerColumnsValidationController',
						locals: {
							DialogParameters: dialParameters,
							ProcessContainerColumns: [],
							StateParameters: StateParameters,
							ItemColumn: field.ItemColumn
						}
					};

					MessageService.dialogAdvanced(params);
				};


				$scope.editProperties = function (grid, field) { //
					SettingsService.ListProcesses().then(function (lists) {
						ProcessesService.get().then(function (processes) {
							let parameters = {
								'ProcessContainerId': grid.Id,
								'ProcessContainerColumnId': field.ProcessContainerColumnId,
								'itemColumn': field.ItemColumn,
								'column': grid.Side,
								'process': process
							};
							//
							let params = {
								params: parameters,
								locals: {
									DialogParameters: parameters,
									Lists: lists,
									Processes: processes,
									UpdateConnection: function () {
										field.ItemColumn.LanguageTranslation = field.ItemColumn.LanguageTranslationRaw;
									}
								}
							};
							ViewService.view('settings.fields.properties', params, {split: true});
						});
					});
				};
				$scope.editContainer = function (grid) {
					SettingsService.ProcessContainers(process).then(function (containers) {
						let container = _.find(containers, function (o) {
							return o.ProcessContainerId == (grid.Id ? grid.Id : grid.ProcessContainerId)
						});

						let nameData = {
							LanguageTranslation: container.LanguageTranslation,
							MaintenanceName: container.MaintenanceName,
							RightContainerOld: container.RightContainer,
							HideTitle: [container.HideTitle]
						};

						let save = function () {
							container.MaintenanceName = nameData.MaintenanceName;
							container.LanguageTranslation = nameData.LanguageTranslation;
							container.RightContainer = Number(nameData.RightContainerOld);
							container.HideTitle = Number(nameData.HideTitle);
							grid.LanguageTranslation = nameData.LanguageTranslation;
							return SettingsService.ProcessContainersSave(process, [container]);
						};

						let del = function () {
							MessageService.confirm('CONTAINER_DISCONNECT', function () {
								SettingsService.ProcessTabContainersDelete(process, grid.tcId).then(function () {
									$scope.deleted = true;
								});
							});
						};

						let HideTitles = [{value: 0, name: 'PROCESS_CONTAINER_HIDE_TITLE_SHOW'},
							{value: 1, name: 'PROCESS_CONTAINER_HIDE_TITLE_HIDE'},
							{value: 2, name: 'PROCESS_CONTAINER_HIDE_TITLE_HIDE_BUT_KEEP_SPACE'}];

						let content = {
							buttons: [
								{text: "DELETE", type: 'warn', onClick: del},
								{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
							inputs: [{
								type: 'translation',
								label: 'PROCESS_CONTAINER_NAME',
								model: 'LanguageTranslation'
							},
								{type: 'string', label: 'MAINTENANCE_NAME', model: 'MaintenanceName'},
								{
									type: 'select',
									label: 'PROCESS_CONTAINER_HIDE_TITLE',
									model: 'HideTitle',
									options: HideTitles,
									optionName: 'name',
									optionValue: 'value'
								}
							],
							title: 'CHANGE_CONTAINER_NAME'
						};

						if (grid.tabId == 0) {
							content.inputs.push({
								type: 'checkbox',
								label: 'RIGHT_CONTAINER',
								model: 'RightContainerOld'
							});
						}

						MessageService.dialog(content, nameData);
					});
				}
				$scope.addField = function (grid) {
					let ProcessContainerId = (grid.Id ? grid.Id : grid.ProcessContainerId);
					let StateParameters = {
						process: process,
						$$originalProcess: process,
						ViewId: "?"
					}
					if (!grid.fields) grid.fields = [];
					let max_order_no = SettingsService.giveOrderNumber(angular.copy(grid.fields), ProcessContainerId);
					let parameters = {
						'StateParameters': StateParameters,
						'ProcessContainerId': ProcessContainerId,
						'Process': StateParameters.process,
						'OrderNo': max_order_no,
						'ViewId': StateParameters.ViewId
					};
					SettingsService.ItemColumns(StateParameters.process).then(function (columns) {
						columns = _.filter(columns, function (f) {
							return f.DataType != 8;
						});

						ProcessesService.get().then(function (processes) {
							SettingsService.ListProcesses().then(function (process) {
								ApiService.v1api('settings/RegisteredSubQueries/queryTypes').get().then(function (subqueries) {
									let params = {
										template: 'settings/containers/itemColumnAddDialog.html',
										controller: 'ContainersAddNewColumnController',
										locals: {
											DialogParameters: parameters,
											ItemColumnsData: columns,
											Lists: $filter('orderBy')(process, function (p) {
												return Translator.translation(p.LanguageTranslation)
											}),
											Processes: $filter('orderBy')(processes, function (process) {
												return Translator.translation(process.LanguageTranslation)
											}),
											ProcessContainerColumns: [],
											UnlinkedColumns: [],    
											SubQueries: subqueries,
											UpdateConnection: function () {
												SettingsService.ProcessContainerColumns(StateParameters.process).then(function (containerColumns) {
													grid.fields = containerColumns[ProcessContainerId];
												});
											}
										},
										wider: true
									};
									MessageService.dialogAdvanced(params);

								});
							});
						});
					});
				}
				$scope.removeField = function (grid, field) {
					MessageService.confirm('FIELD_DISCONNECT', function () {
						SettingsService.ProcessContainerColumnsDelete(process, field.ProcessContainerColumnId).then(function () {
							SettingsService.ProcessContainerColumns(process).then(function (containerColumns) {
								grid.fields = containerColumns[grid.Id];
							});
						});
					}, 'warn');
				}
				$scope.orderField = function (current, prev, next, parent) {
					let beforeOrderNo;
					let afterOrderNo;

					if (angular.isDefined(prev)) beforeOrderNo = prev.OrderNo;
					if (angular.isDefined(next)) afterOrderNo = next.OrderNo;

					current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
					current.ProcessContainerId = parseInt(parent);

					SettingsService.ProcessContainerColumnsSave(process, [current]);
				};

			},
			templateUrl: 'core/process/grid/grid.html'
		};
	}
})();