(function () {
	'use strict';

	angular
		.module('core.process')
		.directive('contentDescription', ContentDescription);

	ContentDescription.$inject = ['$sanitize'];

	function ContentDescription($sanitize) {
		let parseRegex = /\${.*}\$/;

		return {
			scope: {
				bind: '<',
				contentjson: '<',
				alert: '@?'
			},
			templateUrl: 'core/ui/input/bare/description.html',
			link: function (scope, element) {
				if (scope.alert == "true") {
					element.addClass("alert");
					element.attr("role", "alert");
				}
			},
			controller: function ($scope, $rootScope, UniqueService, Common) {
				if (typeof $scope.bind === 'string' && $scope.contentjson && $scope.contentjson != "") {
					let rawBind = $scope.contentjson.replace(parseRegex, function (binder) {
						let info = binder.split(':|:');
						let f = info[0];
						let s = info[1];

						let url = s ? s.substring(0, s.length - 2) : f.substring(2, f.length - 2);
						let name = s ? f.substring(2) : url.length > 25 ? url.substring(0, 25) + '...' : url;


						if (anchorme.validate.url(url)) {
							let u = anchorme(url, {list: true})[0];
							if (!_.startsWith(u.raw, u.protocol)) {
								u.raw = u.protocol + u.raw;
							}

							return '<a target="_blank" href="' + u.raw + '">' + name + '</a>';
						}

						return '';
					});
					$scope.descContent = Common.jsonParse(rawBind);
				}
				$scope.userLanguage = $rootScope.clientInformation.locale_id;
				$scope.id = UniqueService.get('input-description', true);
			}
		};
	}
})();