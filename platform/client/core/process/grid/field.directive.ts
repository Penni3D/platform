﻿(function () {
	'use strict';

	angular
		.module('core.process')
		.directive('processField', ProcessField);

	ProcessField.$inject = ['$compile', 'ViewService', 'Common', 'HTMLService', 'Sanitizer', 'TagsService', '$rootScope'];

	function ProcessField($compile, ViewService, Common, HTMLService, Sanitizer, TagsService, $rootScope) {
		function getDataAdditionalObject(itemColumn) {
			if (typeof itemColumn.DataAdditional2 === 'string' && itemColumn.DataAdditional2.length) {
				let r = angular.fromJson(itemColumn.DataAdditional2);
				if (typeof r === 'object' && r) return r;
			}
			return {};
		}

		return {
			scope: {
				field: '<',
				data: '=',
				readonlyExt: '<?',
				readonlyHelp: '<?',
				onChange: '&?',
				onListChange: '&?',
				onHelpChange: '&?',
				onEmptyChange: '&?',
				lists: '<?',
				grids: '<?',
				archiveDate: '<?',
				openerItemId: '<',
				dialog: '<?'
			},
			restrict: 'E',
			link: function (scope, element, attrs) {
				let inputElement;
				let dataType = scope.field.ItemColumn.DataType;

				let conta_validation = scope.field.Validation;
				let field_validation = scope.field.ItemColumn.Validation;
				let validation = angular.copy(field_validation);
				let currentDataType = scope.field.ItemColumn.DataType;
				
				scope.field.$$Readonly = false;
				_.each(conta_validation, function (value, key) {
					if (value !== null) {
						validation[key] = value;
						if (key == 'ReadOnly') {
							scope.field.$$Readonly = value;
						}
					} else if (key == 'Max' && value == null && currentDataType == 0 && scope.field.ItemColumn.InputName == null) {
						validation['Max'] = 255
					}
				});


				if (scope.field.ItemColumn.InputMethod == 1) {
					inputElement = HTMLService.initialize('input-select')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('options', 'lists[field.ItemColumn.InputName]')
						.attr('on-change', 'listChanged')
						.attr('option-icon', 'list_symbol')
						.attr('option-color', 'list_symbol_fill')
						.attr('single-value')
						.attr('watch-options')
						.attr('model', 'data[field.ItemColumn.Name]')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attrConditional(validation.Min !== null, 'min', Sanitizer(validation.Min))
						.attrConditional(validation.Max !== null, 'max', Sanitizer(validation.Max))
						.attrConditional(Common.isTrue(validation.Required), 'required-field')
						.attrConditional('disabled', 'in-use');


					addHelp();
				} else if (
					dataType == 0 || dataType == 1 ||
					dataType == 2 || dataType == 4 ||
					dataType == 7
				) {

					let u = scope.field.ItemColumn.Process + "," + scope.field.ItemColumn.ItemColumnId + "," + Sanitizer(attrs.itemId);
					inputElement = HTMLService.initialize('input-smart')
						.attr('type', '{{::field.ItemColumn.DataType}}')
						.attr('model', 'data[field.ItemColumn.Name]')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('on-change', 'fieldChanged')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('manual-save')
						.attrConditional(scope.field.ItemColumn.UniqueCol == 1, 'unique', u)
						.attrConditional(Common.isTrue(validation.Required), 'required-field', true)
						.attrConditional(validation.Min, 'min', Sanitizer(validation.Min))
						.attrConditional(validation.Max, 'max', Sanitizer(validation.Max))
						.attrConditional(validation.Rows, 'rows', Sanitizer(validation.Rows))
						.attrConditional(validation.Spellcheck, 'spellcheck')
						.attrConditional(validation.Precision !== null, 'precision', Sanitizer(validation.Precision))
						.attrConditional(validation.Suffix, 'suffix', Sanitizer(validation.Suffix));
					addHelp();
				} else if (dataType == 3 || dataType == 13) {
					inputElement = HTMLService.initialize('input-date')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('on-change', 'fieldChanged')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('calendar-connection', validation.CalendarConnection)
						.attrConditional(Common.isTrue(validation.Required), 'required-field');

					if (dataType == 13) inputElement.attr('time', true);

					let dateRange = validation.DateRange;

					if (typeof validation.ParentId === 'undefined' || _.isEmpty(dateRange)) {
						inputElement
							.attr('model', 'data[field.ItemColumn.Name]')
					} else {
						inputElement
							.attr('group', Sanitizer(validation.ParentId))
							.attr('timeline')
							.attr(dateRange === 'start' ? 'model-start' : 'model-end', 'data[field.ItemColumn.Name]');
					}

					if (validation.AfterMonths || validation.BeforeMonths) {
						inputElement
							.attr('settings', '::dateSettings');

						scope.dateSettings = {
							next: validation.AfterMonths,
							prev: validation.BeforeMonths
						};
					}

					addHelp();
				} else if (dataType == 5) {
					let da = getDataAdditionalObject(scope.field.ItemColumn);
					inputElement = HTMLService.initialize('input-process')
						.attr('parent', Sanitizer(attrs.itemId))
						.attr('link-process', '{{::field.ItemColumn.LinkProcess}}')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('parent-process', Sanitizer(attrs.process))
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('process', '{{::field.ItemColumn.DataAdditional}}')
						.attrConditional(da.fromProcessColumnName, 'fromProcessColumnName', da.fromProcessColumnName)
						.attrConditional(da.fromProcessColumnId, 'fromProcessColumnId', da.fromProcessColumnId)
						.attrConditional(da.targetProcessColumnName, 'targetProcessColumnName', da.targetProcessColumnName)
						.attrConditional(da.targetProcessColumnId, 'targetProcessColumnId', da.targetProcessColumnId)
						.attrConditional(da.targetProcessName, 'targetProcessName', da.targetProcessName)


						.attrConditional(da.portfolioId, 'portfolio-id', Sanitizer(da.portfolioId))
						.attrConditional(da.showAll, 'preload')
						.attrConditional(da.advancedFilter, 'filterable')
						.attrConditional(da.canOpen, 'openable', Sanitizer(da.openWidth))
						.attrConditional(scope.openerItemId, 'openeritemid', scope.openerItemId);

					if (attrs.process === 'task') {
						scope.taskOptions = {
							startDate: 'start_date',
							endDate: 'end_date',

							parentEffort: 'required_effort',
							itemEffort: 'effort'
						};

						inputElement.attr('settings', '::taskOptions');
					}

					if (scope.field.ItemColumn.InputMethod == 3) {
						inputElement
							.attr('item-column-id', '{{::field.ItemColumn.RelativeColumnId}}')
							.attr('direction', 'parent')
					} else {
						inputElement
							.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}')
							.attr('model', 'data[field.ItemColumn.Name]')
							.attr('on-change', 'fieldChanged')
							.attrConditional(da.table, 'table')
							.attrConditional(validation.Max !== null, 'max', Sanitizer(validation.Max))
							.attrConditional(Common.isTrue(validation.Required), 'required-field');
					}

					addHelp();
				} else if (dataType == 6) {
					let da = getDataAdditionalObject(scope.field.ItemColumn);
					inputElement = HTMLService.initialize('input-select')
						.attr('owner', Sanitizer(attrs.itemId))
						.attr('parent-process', Sanitizer(attrs.process))
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('options', 'lists[field.ItemColumn.DataAdditional]')
						.attr('option-value', 'item_id')
						.attr('option-name', 'list_item')
						.attr('option-icon', 'list_symbol')
						.attr('option-color', 'list_symbol_fill')
						.attr('on-change', 'listChanged')
						.attr('model', 'data[field.ItemColumn.Name]')
						.attr('restrictions', 'restrictions')
						.attr('watch-options')
						.attr('disabled', 'in_use')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attrConditional(validation.Max !== null, 'max', Sanitizer(validation.Max))
						.attrConditional(validation.Unselectable, 'unselectable', validation.Unselectable)
						.attrConditional(Common.isTrue(validation.Required), 'required-field')
						.attrConditional(da.fromProcessColumnName, 'fromProcessColumnName', da.fromProcessColumnName)
						.attrConditional(da.fromProcessColumnId, 'fromProcessColumnId', da.fromProcessColumnId)
						.attrConditional(da.targetProcessColumnName, 'targetProcessColumnName', da.targetProcessColumnName)
						.attrConditional(da.targetProcessColumnId, 'targetProcessColumnId', da.targetProcessColumnId)
						.attrConditional(da.targetProcessName, 'targetProcessName', da.targetProcessName)
						.attrConditional(da.targetListColumn, 'targetListColumn', da.targetListColumn)
						.attrConditional(scope.openerItemId, 'openeritemid', scope.openerItemId);

					if (scope.field.ItemColumn.ParentSelectType === 1) {
						scope.restrictions = {
							leafOnly: false
						};
					}

					addHelp();
				} else if (dataType == 9) {
					let da = getDataAdditionalObject(scope.field.ItemColumn);
					inputElement = HTMLService.initialize('input-rating')
						.attr('model', 'data[field.ItemColumn.Name]')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('on-change', 'fieldChanged')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attrConditional(Common.isTrue(validation.Required), 'required-field')
						.attrConditional(da.amount, 'max', Sanitizer(da.amount))
						.attrConditional(da.color, 'option-color', Sanitizer(da.color))
						.attrConditional(da.trueIcon, 'option-true', Sanitizer(da.trueIcon))
						.attrConditional(da.falseIcon, 'option-false', Sanitizer(da.falseIcon))
						.attrConditional(da.placeholder, 'placeholder', '{{::data[\'' + Sanitizer(da.placeholder) + '\']}}')
						.attrConditional(da.total, 'group', '{{::data[\'' + Sanitizer(da.total) + '\']}}');

					addHelp();
				} else if (dataType == 10) {
					let da = getDataAdditionalObject(scope.field.ItemColumn);
					scope.fileRestrictions = {
						ratio: '1:1'
					};
					inputElement = HTMLService.initialize('input-file')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('self', Sanitizer(attrs.itemId))
						.attr('restrictions', 'fileRestrictions')
						.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}')
						.attr('on-change', 'emptyChanged')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attrConditional(Common.isTrue(validation.Required), 'required-field')
						.attrConditional(da.cropEnabled, 'cropEnabled', Sanitizer(da.cropEnabled))
						.attrConditional(da.bigThumbNails, 'bigThumbNails', Sanitizer(da.bigThumbNails))
						.attrConditional(scope.dialog, 'dialog', Sanitizer(scope.dialog))
						.attrConditional(true, 'uploadOnDemand', true)
						.attrConditional(validation.Max !== null, 'max', Sanitizer(validation.Max));

					if (scope.archiveDate) {
						inputElement.attrConditional(scope.archiveDate, 'settings', '::fileOptions');

						scope.fileOptions = {
							archive: scope.archiveDate
						};
					}

					addHelp();
				} else if (dataType == 12) {
					let da = getDataAdditionalObject(scope.field.ItemColumn);

					inputElement = HTMLService.initialize('input-comment')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('process', '{{::field.ItemColumn.Process}}')
						.attr('on-change', 'emptyChanged')
						.attr('group', Sanitizer(attrs.itemId))
						.attr('self', '{{::field.ItemColumn.ItemColumnId}}')
						.attrConditional(validation.Spellcheck, 'spellcheck')
						.attrConditional(Common.isTrue(validation.Required), 'required-field')
						.attrConditional(da.note, 'note')
						.attrConditional(da.infinite, 'infinite')
						.attrConditional(da.preventatt, 'preventatt');

						if (scope.archiveDate) inputElement.attr('archive', scope.archiveDate);

				} else if (dataType == 14) {
					inputElement = HTMLService.initialize('input-formula')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('on-change', 'fieldChanged')
						.attr('model', 'data[field.ItemColumn.Name]')
						.attr('type', '{{::field.ItemColumn.DataType}}')
						.attr('item-id', Sanitizer(attrs.itemId))
						.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('formula', '{{::field.ItemColumn.DataAdditional}}')
						.attr('data', 'data')
						.attr('lists', 'lists')
						.attr('grids', 'grids')
						.attr('process', Sanitizer(attrs.process))
						.attrConditional(Common.isTrue(validation.Required), 'required-field');

					addHelp();
				} else if (dataType == 15) {
					inputElement = HTMLService.initialize('input-link')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('self', Sanitizer(attrs.itemId))
						.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}')
						.attr('on-change', 'emptyChanged')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attrConditional(scope.archiveDate, 'archive-date', '::archiveDate')
						.attrConditional(Common.isTrue(validation.Required), 'required-field')
						.attrConditional(validation.Max !== null, 'max', Sanitizer(validation.Max));

					if (scope.archiveDate) {
						scope.linkSettings = {
							archive: scope.archiveDate
						};

						inputElement.attr('settings', '::linkSettings');
					}

					addHelp();
				} else if (dataType == 25) {
					inputElement = HTMLService.initialize('input-chart')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('chart-id', scope.field.ItemColumn.DataAdditional)
						.attr('item-ids', Sanitizer(attrs.itemId));

					if (scope.archiveDate) inputElement.attr('archive', scope.archiveDate);

					addHelp();
				} else if (dataType == 16 || dataType == 20) {
					if (scope.field.ItemColumn.SubDataType == 6) {
						inputElement = HTMLService.initialize('input-select')
							.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
							.attr('options', 'lists[field.ItemColumn.SubDataAdditional]')
							.attr('option-value', 'item_id')
							.attr('option-name', 'list_item')
							.attr('option-icon', 'list_symbol')
							.attr('option-color', 'list_symbol_fill')
							.attr('model', 'data[field.ItemColumn.Name]')
							.attr('watch-options')
							.attr('modify', 'false')
							.attrConditional('disabled', 'in-use');

					} else if (scope.field.ItemColumn.SubDataType == 5) {
						let da = getDataAdditionalObject(scope.field.ItemColumn);

						let r = scope.field.ItemColumn.SubDataAdditional2.length != 0 ? angular.fromJson(scope.field.ItemColumn.SubDataAdditional2) : "";
						let customSelector = null;

						if (r != "") {
							da.canOpen = r.canOpen;
							da.openWidth = r.openWidth;

							if (r.portfolioId) {
								customSelector = r.portfolioId;
							}
						}

						inputElement = HTMLService.initialize('input-process')
							.attr('parent', Sanitizer(attrs.itemId))
							.attr('link-process', '{{::field.ItemColumn.LinkProcess}}')
							.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
							.attr('parent-process', Sanitizer(attrs.process))
							.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}')
							.attr('modify', 'false')
							.attr('process', '{{::field.ItemColumn.SubDataAdditional}}')
							.attr('model', 'data[field.ItemColumn.Name]')
							.attr('on-change', 'fieldChanged')
							.attrConditional(da.canOpen, 'openable', Sanitizer(da.openWidth))
							.attrConditional(customSelector != null, 'customselector', customSelector)
							.attrConditional(validation.Max, 'max', Sanitizer(validation.Max));
					} else if (scope.field.ItemColumn.SubDataType == 4 && scope.data[scope.field.ItemColumn.Name] && scope.data[scope.field.ItemColumn.Name].includes('html') && scope.data[scope.field.ItemColumn.Name].includes('delta')) {
						if (typeof scope.data[scope.field.ItemColumn.Name]) {
							scope.data[scope.field.ItemColumn.Name] = JSON.parse(scope.data[scope.field.ItemColumn.Name]);
						}
						inputElement = HTMLService.initialize('input-rich')
							.attr('model', 'data[field.ItemColumn.Name]')
							.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
							.attr('modify', 'false')
							.attr('item-id', Sanitizer(attrs.itemId))
							.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}');
					} else {
						inputElement = HTMLService.initialize('input-smart')
							.attr('type', '{{::field.ItemColumn.SubDataType}}')
							.attr('model', 'data[field.ItemColumn.Name]')
							.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
							.attr('item-id', Sanitizer(attrs.itemId))
							.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}')
							.attr('modify', 'false')
							.attrConditional(validation.Precision !== null, 'precision', Sanitizer(validation.Precision))
							.attrConditional(validation.Suffix, 'suffix', Sanitizer(validation.Suffix));
					}
					addHelp();
				} else if (dataType == 17) {
					let da2 = scope.field.ItemColumn.DataAdditional2 ? JSON.parse(scope.field.ItemColumn.DataAdditional2) : {};
					inputElement = HTMLService.initialize('content-description')
						.attr('contentjson', '::(data[field.ItemColumn.Name] ? data[field.ItemColumn.Name] : field.ItemColumn.DataAdditional)')
						.attr('bind', '::field.ItemColumn.LanguageTranslation | translateObject')
						.attrConditional(da2.isAlert, 'alert', true);
				} else if (dataType == 18) {
					let da = getDataAdditionalObject(scope.field.ItemColumn);
					inputElement = HTMLService.initialize('input-portfolio')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('process', '{{::field.ItemColumn.DataAdditional}}')
						.attr('parent', Sanitizer(attrs.itemId))
						.attr('on-change', 'emptyChanged')
						.attr('item-column-id', Sanitizer(da.itemColumnId))
						.attr('portfolio-id', Sanitizer(da.portfolioId))
						.attr('settings', '::portfolioSettings')
						.attr('archiveFieldName', da.archiveFieldName)
						.attr('parentFieldName', da.parentFieldName)
						.attr('ignoreExcelExport', da.ignoreExcelExport)
						.attrConditional(da.canOpen, 'openable', Sanitizer(da.openWidth))
						.attrConditional(Common.isTrue(validation.Required), 'required-field');

					scope.portfolioSettings = {
						showSum: da.showSum,
						showSumCumulative: da.showSumCumulative,
						useDialog: da.useDialog,
						action: da.actionId,
						showAddDelete: da.showAddDelete,
						archive: scope.archiveDate
					};

					addHelp();
				} else if (dataType == 19) {
					scope.tags = TagsService.get(attrs.process);

					inputElement = HTMLService.initialize('input-chip')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('options', 'tags')
						.attr('on-change', 'fieldChanged')
						.attr('model', 'data[field.ItemColumn.Name]')
						.attr('modify', '!(readonlyExt || validationReadOnly)')
						.attrConditional(Common.isTrue(validation.Required), 'required-field');

					addHelp();
				} else if (dataType == 21) {
					let da = getDataAdditionalObject(scope.field.ItemColumn);
					inputElement = HTMLService.initialize('lite-manager')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('process', '{{::field.ItemColumn.DataAdditional}}')
						.attr('parent', Sanitizer(attrs.itemId))
						.attr('restrictions', 'restrictions')
						.attr('archiveFieldName', da.options.archiveFieldName)
						.attr('parentFieldName', da.options.parentFieldName)
						.attr('ignoreExcelExport', da.options.ignoreExcelExport)
						.attr('forceAllOpen', da.options.forceAllOpen)
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr("archive", scope.archiveDate);

					if (da.options && da.options.openWidth) {
						inputElement.attr('openable', Sanitizer(da.options.openWidth));
					}

					scope.restrictions = da;

				} else if (dataType == 22) {
					let onlyOneRequired = false;
					let showLastModified = false;
					let z = JSON.parse(scope.field.ItemColumn.DataAdditional2)

					if (scope.field.ItemColumn.DataAdditional2 != null && z.hasOwnProperty('OnlyOneMandatory') && z.OnlyOneMandatory == true) onlyOneRequired = true;

					if (scope.field.ItemColumn.DataAdditional2 != null && z.hasOwnProperty('ShowLastModified') && z.ShowLastModified == true) showLastModified = true;

					inputElement = HTMLService.initialize('input-matrix')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('process', '{{::field.ItemColumn.DataAdditional}}')
						.attr('parent-process', '{{::field.ItemColumn.Process}}')
						.attr('item-column-id', '{{::field.ItemColumn.ItemColumnId}}')
						.attr('parent', Sanitizer(attrs.itemId))
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('on-change', 'emptyChanged')
						.attr('one-mandatory', onlyOneRequired)
						.attr('show-last-modified', showLastModified)
						.attr('archive-date', scope.archiveDate)
						.attrConditional(Common.isTrue(validation.Required), 'required-field');
				} else if (dataType == 23) {
					inputElement = HTMLService.initialize('input-rich')
						.attr('model', 'data[field.ItemColumn.Name]')
						.attr('label', '{{::field.ItemColumn.LanguageTranslation | translateObject}}')
						.attr('on-change', 'fieldChanged')
						.attr('modify', '!(readonlyExt || field.$$Readonly)')
						.attr('portfolio-id', '{{field.ItemColumn.DataAdditional}}')
						.attr('item-id', Sanitizer(attrs.itemId))
						.attr('parent-process', Sanitizer(attrs.process))
						.attr('has-image', '{{field.ItemColumn.DataAdditional2}}');

					addHelp();
				}

				if (inputElement) element.html($compile(inputElement.getHTML())(scope));
				scope.hasText = function (value) {
					if (typeof value === 'undefined' || value == null || value == '') return false;
					let parsedText = JSON.parse(value);
					if (!parsedText.hasOwnProperty($rootScope.clientInformation.locale_id) || typeof parsedText[$rootScope.clientInformation.locale_id].html === 'undefined') return false;
					return parsedText[$rootScope.clientInformation.locale_id].html != "";
				};

				function addHelp() {
					if (typeof scope.readonlyHelp === 'undefined') return;
					inputElement
						.element('meta-helper')
						.if('::hasText(field.HelpTextTranslation) || !readonlyHelp')
						.attr('modify', '!readonlyHelp')
						.attr('on-change', 'helpTextChanged')
						.ngClass('{ \'is-empty\': !field.HelpTextTranslation }')
						.attr('help', 'field.HelpTextTranslation');

					element.addClass('has-help');
				}
			},
			controller: function ($scope, Translator) {
				let label;

				if (typeof $scope.field.Validation.Label === 'string' && $scope.field.Validation.Label.length) {
					label = angular.fromJson($scope.field.Validation.Label);

				} else if (typeof $scope.field.ItemColumn.Validation.Label === 'string' && $scope.field.ItemColumn.Validation.Label.length) {
					label = angular.fromJson($scope.field.ItemColumn.Validation.Label);
				}
				let hasTranslation = Translator.translation(label);
				if (hasTranslation) {
					$scope.field.ItemColumn.LanguageTranslation = label;
				}

				$scope.helpTextChanged = function () {
					if (isFunction($scope.onHelpChange)) {
						$scope.onHelpChange()($scope.field);
					}
				};

				$scope.emptyChanged = function () {
					if (isFunction($scope.onEmptyChange)) {
						$scope.onEmptyChange()($scope.field);
					}
				};

				$scope.fieldChanged = function () {
					if (isFunction($scope.onChange)) {
						$scope.onChange()();
					}
				};

				$scope.listChanged = function () {
					if (isFunction($scope.onListChange)) {
						$scope.onListChange()($scope.field.ItemColumn);
					}
				};

				function isFunction(f) {
					return typeof f === 'function' && typeof f() === 'function';
				}
			}
		};
	}
})();