let cachedData;

(function () {
	'use strict';

	angular
		.module('core.process')
		.service('ProcessService', ProcessService);

	ProcessService.$inject = [
		'ApiService',
		'$q',
		'$rootScope',
		'SaveService',
		'SelectorService',
		'CalendarService',
		'Common',
		'ConnectorService'
	];

	function ProcessService(
		ApiService,
		$q,
		$rootScope,
		SaveService,
		SelectorService,
		CalendarService,
		Common,
		ConnectorService
	) {
		let self = this;

		let Actions = ApiService.v1api('meta/layout');
		let Items = ApiService.v1api('meta/Items');
		let ItemsData = ApiService.v1api('meta/ItemsData');
		let States = ApiService.v1api('navigation/States');
		let ProcessTabHierarchy = ApiService.v1api('settings/ProcessTabHierarchy');
		let DialogActions = ApiService.v1api('settings/ProcessActions');

		let DialogContainers = ApiService.v1api('settings/ProcessPortfolioDialogs');
		let Process = ApiService.v1api('meta/ItemsProcess');
		let ProcessContainer = ApiService.v1api('settings/ProcessProcessContainer');
		let ProcessSelections = ApiService.v1api('meta/ItemDataProcessSelections');

		let ItemColumns = ApiService.v1api('meta/ItemColumns');
		let ListProcesses = ApiService.v1api('meta/listProcesses');
		let Sublists = ApiService.v1api('settings/processes/listHierarchy');
		let ProcessGroups = ApiService.v1api('settings/ProcessGroups');
		let ArchiveData = ApiService.v1api('meta/ArchiveData/bydate');

		let Conditions = ApiService.v1api('settings/Conditions');

		let linkData = {};
		let sublistsData = {};

		let baseProcessData = {};
		let processData = {};
		let supportData = {};
		let changedData = {};
		let dataCallbacks = {};
		let statusCallbacks = {};
		let newItemCallbacks = {};
		let actionCallbacks = {};
		let ignoreColumns = {};
		let unignoreColumns = [];

		let processActions = {};
		let processColumns = {};
		let processGroups = {};

		let forceHistoryDates = {};
		let unforceHistoryDates = [];

		let itemPromises = {
			item: {},
			items: {},
			list: {}
		};

		let columnPromises = {
			column: {},
			columns: {}
		};

		cachedData = processData;

		self.invalidateData = function (process, itemId) {
			if (processData[process]) {
				delete processData[process][itemId];
			}

			if (supportData[process]) {
				delete supportData[process][itemId];
			}

			if (linkData[process]) {
				delete linkData[process][itemId];
			}
		};

		self.invalidateLink = function (process, parentId, itemId, itemColumnId) {
			if (typeof linkData[process] === 'undefined' ||
				typeof linkData[process][parentId] === 'undefined' ||
				typeof linkData[process][parentId][itemColumnId] === 'undefined'
			) {
				return;
			}

			let l = linkData[process][parentId][itemColumnId];
			angular.forEach(l, function (link, key) {
				if (key == itemId) {
					self.invalidateData(link.process, link.item_id);
				}
			});

			delete linkData[process][parentId][itemColumnId][itemId];
		};

		self.invalidateActions = function (process, itemId = undefined) {
			if(process == "refresh_all"){
				processActions = {};
				return;
			}

			if(process == 'refresh_tables') {
				$rootScope.$broadcast('RefreshAllTables', {});
			}

			if(process == 'soft_refresh'){
				$rootScope.$broadcast('RefreshViews', {});
			}

			/*if(process == 'hard_refresh'){
				processActions = {};
				$rootScope.$broadcast('RefreshViews', {});
			}*/

			if (typeof processActions[process] === 'undefined') {
				return;
			}

			if (typeof process === 'undefined') {
				processActions = {};
			} else if (typeof itemId === 'undefined') {
				delete processActions[process];
			} else {
				delete processActions[process][itemId];
			}
		};

		$rootScope.$on('viewChange', function (event, parameters) {
			if (parameters.action === 'PRE-NEW') {
				let i = unignoreColumns.length;
				while (i--) {
					let n = unignoreColumns[i];
					let c = ignoreColumns[n.process];

					if (c) {
						let j = c.length;
						while (j--) {
							if (c[j] === n.name) {
								c.splice(j, 1);
								break;
							}
						}
					}
				}

				i = unforceHistoryDates.length;
				while (i--) {
					let item = unforceHistoryDates[i];
					self.unforceHistoryDate(item.process, item.itemId);
				}

				unforceHistoryDates = [];
				unignoreColumns = [];

				if (parameters.view.split('.')[0] !== 'process') {
					forceHistoryDates = {};
				}
			}
		});

		self.ignoreColumn = function (process, name, resetOnViewChange) {
			if (typeof ignoreColumns[process] === 'undefined') {
				ignoreColumns[process] = [];
			}

			if (Array.isArray(name)) {
				angular.forEach(name, function (n) {
					ignoreColumn(process, n, resetOnViewChange);
				});
			} else {
				ignoreColumn(process, name, resetOnViewChange);
			}
		};

		function ignoreColumn(process, name, reset) {
			let ipc = ignoreColumns[process];
			if (ipc.indexOf(name) === -1) {
				ipc.push(name);

				if (Common.isTrue(reset)) {
					unignoreColumns.push({
						process: process,
						name: name
					});
				}
			}
		}

		self.unignoreColumn = function (process, name) {
			if (ignoreColumns[process]) {
				if (Array.isArray(name)) {
					angular.forEach(name, function (n) {
						unignoreColumn(process, n);
					});
				} else {
					unignoreColumn(process, name);
				}
			}
		};

		function unignoreColumn(process, name) {
			let ipc = ignoreColumns[process];

			let i = ipc.indexOf(name);
			if (i !== -1) {
				ipc.splice(i, 1);
			}
		}

		self.getActualItemId = function (id) {
			if (id && id.toString().indexOf('_') > 0) {
				let i = id.split('_');
				return i[i.length - 1];
			}
			return id;
		};

		self.setData = function (data, process, itemId) {
			if (typeof process === 'undefined' ||
				process === null ||
				process === '' ||
				typeof itemId === 'undefined' ||
				itemId === null ||
				itemId === ''
			) {
				return data;
			}

			if (typeof processData[process] === 'undefined') {
				processData[process] = {};
				baseProcessData[process] = {};
			}

			if (typeof processData[process][itemId] === 'undefined') {
				processData[process][itemId] = data.Data;
				baseProcessData[process][itemId] = angular.copy(data.Data);
			} else {
				let item = processData[process][itemId];

				angular.forEach(data.Data, function (value, key) {
					if (typeof value === 'object') {
						if (angular.toJson(value) !== angular.toJson(item[key])) {
							self.notifyChanged(process, itemId);
						}
					} else if (item[key] !== value) {
						self.notifyChanged(process, itemId);
					}

					item[key] = value;
					baseProcessData[process][itemId][key] = angular.copy(value);
				});

				data.Data = item;
			}

			if (typeof linkData[process] === 'undefined') {
				linkData[process] = {};
			}

			if (typeof linkData[process][itemId] === 'undefined') {
				linkData[process][itemId] = {};
			}

			let itemLink = linkData[process][itemId];
			angular.forEach(data.LinkData, function (item, itemColumnId) {
				if (typeof itemLink[itemColumnId] === 'undefined') {
					itemLink[itemColumnId] = {};
				}

				angular.forEach(item, function (link, key) {
					itemLink[itemColumnId][key] = self.setData({ Data: link }, link.process, link.item_id).Data;
				});
			});

			data.LinkData = itemLink;

			if (typeof supportData[process] === 'undefined') {
				supportData[process] = {};
			}

			if (typeof supportData[process][itemId] === 'undefined') {
				supportData[process][itemId] = {};
			}

			angular.forEach(data, function (value, key: string) {
				if (key === 'Data' || key === 'LinkData') {
					return;
				}

				supportData[process][itemId][key] = value;
			});

			let ret = {
				Data: processData[process][itemId],
				LinkData: itemLink
			};

			return angular.extend({}, supportData, ret);
		};

		function differize(process, itemId) {
			let changedColumns = [];
			let d = {};
			let o = baseProcessData[process][itemId];

			angular.forEach(processData[process][itemId], function (value, key: string) {
				if (key === 'primary' || key === 'secondary' || key === 'tertiary') return;

				if (typeof value === 'object') {
					if (angular.toJson(value) === angular.toJson(o[key])) return;

					//Check if dates
					if (value !== null && typeof value.getMonth !== 'undefined' && typeof o[key] !== 'undefined') {
						let s = angular.toJson(value).substring(1, 17);
						let e = angular.toJson(o[key]).substring(1, 17);
						if (s === e) return;
					}

					changedColumns.push(key);
				} else if (o[key] != value) {
					changedColumns.push(key);
				}
			});

			let ipc = ignoreColumns[process];
			if (ipc) {
				let i = changedColumns.length;
				while (i--) {
					let j = ipc.length;
					while (j--) {
						if (changedColumns[i] === ipc[j]) {
							changedColumns.splice(i, 1);
							break;
						}
					}
				}
			}

			let i = changedColumns.length;
			while (i--) {
				let k = changedColumns[i];
				d[k] = angular.copy(processData[process][itemId][k]);
			}

			return d;
		}

		let refreshAllCollection = {};

		SaveService.subscribeSave($rootScope, function () {
			let promises = [];
			angular.forEach(changedData, function (items, process) {
				let t = [];

				angular.forEach(items, function (fields, itemId) {
					let ldata = linkData[process][itemId];

					let data = {
						Data: {
							item_id: itemId,
							process: process
						},
						LinkData: ldata
					};


					let i = fields.length;
					while (i--) {
						let field = fields[i];
						data.Data[field] = processData[process][itemId][field];
					}


					let o = angular.extend(
						{},
						supportData[process][itemId],
						data
					);
					if (fields.length > 0 || (ldata && !_.isEmpty(ldata))) t.push(o);
				});

				if (t.length > 0) promises.push(ItemsData
					.save(process, t)
					.then(function (results) {
						SaveService.notifyActionNeeded();
						let checkConditions = [];
						let i = results.length;
						while (i--) {
							let item = results[i];
							let itemId = item.Data.item_id;
							if (item.statesChanged) {
								self.invalidateActions(process, itemId);
								notifyStatusChanged(process, itemId);
							} else {
								checkConditions.push(itemId);
							}

							self.setData(item, process, itemId);
						}

						i = checkConditions.length;
						if (i) {
							return self.getColumns(process).then(function (columns) {
								while (i--) {
									let itemId = checkConditions[i];
									let fields = items[itemId];
									let j = fields.length;
									let hasRefreshAllColumn = _.find(columns, function(o) { return o.CheckConditions == 2; });
									refreshAllCollection[itemId] = typeof hasRefreshAllColumn != 'undefined';
									while (j--) {
										for (let columnId in columns) {
											if (columns.hasOwnProperty(columnId) &&
												fields[j] == columns[columnId].Name &&
												columns[columnId].CheckConditions
											) {
												if(columns[columnId].CheckConditions == 3) process = 'refresh_tables';
												if(columns[columnId].CheckConditions == 4) process = 'soft_refresh';
												self.invalidateActions(process, itemId);
												//REFRESH ALL
												notifyStatusChanged(process, itemId);
												break;
											}
										}
									}
								}
							});
						}
					})
				);
			});

			changedData = {};
			return $q.all(promises);
		});

		self.setDataChanged = function (process, itemId, saveAll, changeSet) {
			if (self.getHistoryDate(process, itemId)) return;
			if (/^\d+$/.test(String(itemId)) && Number(itemId) > 0) {
				if (typeof changedData[process] === 'undefined') changedData[process] = {};
				if (typeof changedData[process][itemId] === 'undefined') changedData[process][itemId] = [];

				let changedItem = changedData[process][itemId];
				let dif = typeof changeSet === 'undefined' ?
					Common.isTrue(saveAll) ? processData[process][itemId] : differize(process, itemId)
					: changeSet;

				let changed = false;
				angular.forEach(dif, function (v, k) {
					if (Array.isArray(changeSet)) {
						k = v;
						v = processData[process][itemId][k];
					}

					changed = true;
					baseProcessData[process][itemId][k] = v;

					if (changedItem.indexOf(k) === -1) changedItem.push(k);
				});

				if (changed) {
					SaveService.tick();
					self.notifyChanged(process, itemId);
				}
				return changed;
			}
			return false;
		};

		self.getCachedItemData = function (process, itemId) {
			if (typeof itemId === 'undefined') {
				let id = process;
				for (let i in processData) {
					if (processData.hasOwnProperty(i) && processData[i][id]) {
						let data = {
							Data: processData[i][id],
							LinkData: linkData[i][id]
						};

						return angular.extend(
							{},
							supportData[i][id],
							data
						);
					}
				}
			} else {
				let data = {
					Data: processData[process][itemId],
					LinkData: linkData[process][itemId]
				};

				return angular.extend(
					{},
					supportData[process][itemId],
					data
				);
			}

			return {};
		};

		self.getArchiveData = function (process, itemId, date) {
			return ArchiveData.get([process, itemId, date]).then(function (data) {
				return {
					Data: data,
					HistoryDate: date
				};
			});
		};

		self.getItemData = function (process, itemId, reload) {
			let forcedHistory = self.getHistoryDate(process, itemId);
			if (forcedHistory) return self.getArchiveData(process, itemId, forcedHistory);
			if (Common.isFalse(reload) && processData[process] && processData[process][itemId]) {
				let d = $q.defer();
				let data = {
					Data: processData[process][itemId],
					LinkData: linkData[process][itemId]
				};

				let o = angular.extend(
					{},
					supportData[process][itemId],
					data
				);

				d.resolve(o);

				return d.promise;
			}

			let p = itemPromises.item;
			if (typeof p[process] === 'undefined' || typeof p[process][itemId] === 'undefined') {
				if (typeof p[process] === 'undefined') {
					p[process] = {};
				}

				p[process][itemId] = ItemsData.get([process, itemId]).then(function (data) {
					self.setData(data, process, itemId);
					let data_ = {
						Data: processData[process][itemId],
						LinkData: linkData[process][itemId]
					};

					delete p[process][itemId];

					return angular.extend(
						{},
						supportData[process][itemId],
						data_
					);
				});
			}

			return p[process][itemId];
		};

		self.newItem = function (process, payload, actionId) {
			return ItemsData.new(process, [payload]).then(function (response) {
				let itemId = response[0].Data.item_id;
				self.setData(response[0], process, itemId);

				if (actionId) {
					return self.doAction(process, itemId, actionId).then(function () {
						self.notifyNewItem(process, itemId);

						return itemId;
					});
				}

				self.notifyNewItem(process, itemId);

				return itemId;
			});
		};

		self.newItems = function (process, payloads) {
			return ItemsData.new(process, payloads).then(function (responses) {
				let i = responses.length;
				while (i--) {
					let response = responses[i];
					let itemId = response.Data.item_id;

					self.notifyNewItem(process, itemId);
					self.setData(response, process, itemId);
				}

				return responses;
			});
		};

		self.subscribeActionTriggered = function (scope, callback, process) {
			prepareSubscribe(actionCallbacks, scope, callback, process);
		};

		function notifyCallback(callbacks, process, itemId) {
			if (typeof callbacks[process] === 'undefined') return;
			let processCallback = callbacks[process];
			let i = processCallback.length;
			while (i--) {
				let fn = processCallback[i];
				if (typeof fn === 'function') {
					fn(itemId, refreshAllCollection[itemId]);
				}
			}
		}

		function prepareSubscribe(callbacks, scope, callback, process) {
			if (typeof callbacks[process] === 'undefined') {
				callbacks[process] = [];
			}

			let processCallback = callbacks[process];
			processCallback.push(callback);

			scope.$on('$destroy', function () {
				let i = processCallback.length;
				while (i--) {
					if (processCallback[i] === callback) {
						processCallback.splice(i, 1);
						break;
					}
				}
			});
		}

		function notifyActionTriggered(process, itemId) {
			notifyCallback(actionCallbacks, process, itemId);
		}

		self.subscribeStatusChanged = function (scope, callback, process) {
			prepareSubscribe(statusCallbacks, scope, callback, process);
		};

		function notifyStatusChanged(process, itemId) {
			notifyCallback(statusCallbacks, process, itemId);
		}

		self.subscribeChanged = function (callback, process) {
			if (typeof dataCallbacks[process] === 'undefined') {
				dataCallbacks[process] = [];
			}

			dataCallbacks[process].push(callback);
		};

		self.unsubscribeChanged = function (callback, process) {
			if (typeof dataCallbacks[process] === 'undefined') {
				return;
			}

			let processCallback = dataCallbacks[process];
			let i = processCallback.length;
			while (i--) {
				if (processCallback[i] === callback) {
					processCallback.splice(i, 1);
					break;
				}
			}
		};

		let callBackTimer = {};
		self.notifyChanged = function (process, itemId) {
			if (typeof dataCallbacks[process] === 'undefined') {
				return;
			}

			clearTimeout(callBackTimer[itemId]);
			callBackTimer[itemId] = setTimeout(function () {
				let processCallback = dataCallbacks[process];
				let i = processCallback.length;
				while (i--) {
					processCallback[i](itemId);
				}
			}, 25);
		};

		function getProcessValue(item, itemColumns) {
			let promises = [];
			let processValue = '';

			function constructReturn(value) {
				if (typeof value === 'undefined' || value == null) {
					return;
				}

				if (processValue) {
					processValue += ' ' + value;
				} else {
					processValue = value;
				}
			}

			angular.forEach(itemColumns, function (column) {
				let ic = column.Name;
				let columnValue = item[ic];
				let dt = Common.getRelevantDataType(column);
				let process = Common.getRelevantDataAdditional(column);

				if (Array.isArray(columnValue)) {
					angular.forEach(columnValue, function (value) {
						promises.push(self.getItem(process, value).then(function (data) {
							if (dt === 6) {
								constructReturn(data.list_item);
							} else if (dt === 5) {
								constructReturn(data.primary);
							} else {
								constructReturn(data[ic]);
							}
						}));
					});
				} else {
					if (dt === 3) {
						constructReturn(CalendarService.formatDate(columnValue));
					} else if (dt === 13) {
						constructReturn(CalendarService.formatDateTime(columnValue));
					} else if (dt === 1 || dt === 2) {
						constructReturn(Common.formatNumber(columnValue));
					} else {
						constructReturn(columnValue);
					}
				}
			});

			return $q.all(promises).then(function () {
				return processValue;
			});
		}

		self.prepareItem = function (process, item) {
			if(!item) return
			let promises = [];
			let selector = SelectorService.getProcessSelector(process);
			let primary;
			let secondary;
			let tertiary;

			if (typeof selector === 'undefined') {
				primary = [];
				secondary = [];
				tertiary = [];

				if (typeof item.list_item !== 'undefined') {
					primary.push({ Name: 'list_item' });
				}
			} else {
				primary = selector.Primary;
				secondary = selector.Secondary;
				tertiary = selector.Tertiary;
			}

			promises.push(getProcessValue(item, primary).then(function (value) {
				if(!value.length && item.name) item.primary = item.name;
				else item.primary = value;
			}));

			promises.push(getProcessValue(item, secondary).then(function (value) {
				item.secondary = value;
			}));

			promises.push(getProcessValue(item, tertiary).then(function (value) {
				item.tertiary = value;
			}));

			item.itemId = item.item_id;

			return $q.all(promises).then(function () {
				return item;
			});
		};

		self.getItemsFiltered = function (process, filters) {
			if (typeof filters === 'undefined') filters = {};
			let selector = SelectorService.getProcessSelector(process);
			let portfolioId = selector ? selector.ProcessPortfolioId : 0;
			let f = {
				filters: {},
				offset: typeof filters.offset != 'undefined' ? filters.offset : 0,
				limit: 25,
				text: undefined
			};

			if (filters.text) f.text = filters.text;

			angular.forEach(filters.select, function (columns, columnId) {
				f.filters[columnId] = columns;
			});

			return Items.new([process, 'portfolio', portfolioId], f).then(function (items) {
				let promises = [];

				angular.forEach(items.processes, function (data) {
					let t = {
						Data: data
					};

					self.setData(t, data.process, data.item_id);
				});

				let rows = items.rowdata;
				let i = rows.length;
				while (i--) {
					let row = rows[i];
					row.$$maxCount = items.rowcount;
					rows[i] = self.setData({ Data: row }, process, row.item_id).Data;

					promises.push(self.prepareItem(process, rows[i]));
				}

				return $q.all(promises).then(function () {
					return rows.sort(function (a, b) {
						return a.primary < b.primary ? -1 : 1;
					});
				});
			});
		};

		self.getItemsFilteredAll = function (text) {
			let f = {
				limit: 25,
				offset: 0,
				text: text
			};

			return Items.new(['searchAll'], f).then(function (items) {
				let promises = [];

				angular.forEach(items.processes, function (data) {
					let t = {
						Data: data
					};

					self.setData(t, data.process, data.item_id);
				});

				let rows = items.rowdata;
				let i = rows.length;
				while (i--) {
					let row = rows[i];
					row.$$maxRowCount = items.rowcount;
					rows[i] = self.setData({ Data: row }, row.process, row.item_id).Data;
					promises.push(self.prepareItem(row.process, rows[i]));
				}

				return $q.all(promises).then(function () {
					return rows.sort(function (a, b) {
						return a.primary < b.primary ? -1 : 1;
					});
				});
			});
		};

		self.getItem = function (process, itemId, reload) {
			return self.getItemData(process, itemId, reload).then(function (item) {
				return self.prepareItem(process, item.Data);
			});
		};

		self.addRelativeParent = function (process, parentItemId, columnId, selectedItemId) {
			return ProcessSelections.save([process, parentItemId, columnId, selectedItemId]);
		};

		self.removeRelativeParent = function (process, parentItemId, columnId, selectedItemId) {
			return ProcessSelections.delete([process, parentItemId, columnId, selectedItemId]);
		};

		self.getRelativeParents = function (process, itemId, columnId) {
			return Items.get([process, itemId, columnId, 'parents']).then(function (parents) {
				let promises = [];

				let i = parents.length;
				while (i--) {
					promises.push(self.prepareItem(process, parents[i]));
				}

				return $q.all(promises).then(function () {
					return parents.sort(function (a, b) {
						return a.primary < b.primary ? -1 : 1;
					});
				});
			});
		};

		self.getSublists = function (process: string, portfolioId: number = 0, reload: boolean = false, editable: boolean = false) {
			if (Common.isFalse(reload) && sublistsData[process]) {
				let d = $q.defer();
				d.resolve(sublistsData[process]);

				return d.promise;
			}

			let params: Array<any> = [process];
			if (portfolioId > 0) {
				params.push(portfolioId);
				if (editable) params.push(editable);
			}
			return Sublists.get(params).then(function (response) {
				
				
				self.invalidateSublist(process);

				angular.forEach(response.lists, function (list, process) {
					let i = list.length;
					while (i--) {
						let l = list[i];
						list[i] = self.setData({ Data: l }, process, l.item_id);
					}
				});

				angular.forEach(response.sublists, function (sublists, list) {
					let i = sublists.length;
					while (i--) {
						let sublist = sublists[i];

						angular.forEach(response.sublistsData[sublist], function (subItems, itemId) {
							let itemIds = [];
							for (let j = 0, l = subItems.length; j < l; j++) {
								itemIds.push(subItems[j].item_id);
							}

							setSublist(process, list, itemId, sublist, itemIds);
						});
					}
				});

				return sublistsData[process];
			});
		};

		self.hasSublistData = function (process, list, sublist) {
			if (typeof sublistsData[process] === 'undefined') {
				return false;
			}

			if (typeof sublistsData[process][list] === 'undefined') {
				return false;
			} else if (typeof sublist === 'undefined') {
				return true;
			}

			return typeof sublistsData[process][list][sublist] !== 'undefined';
		};

		self.getSublistData = function (process, list, itemId, sublist) {
			let items = [];

			if (sublistsData[process] && sublistsData[process][list] && sublistsData[process][list][sublist]) {
				let sd = sublistsData[process][list][sublist][itemId];
				for (let i = 0, l = sd.length; i < l; i++) {
					items.push(self.getCachedItemData(sublist, sd[i]).Data);
				}
			}

			return items;
		};

		self.invalidateSublist = function (process, list, sublist) {
			if (typeof sublist === 'undefined') {
				if (typeof sublistsData[process] === 'undefined') {
					return;
				}

				delete sublistsData[process][list];
			} else if (typeof list === 'undefined') {
				delete sublistsData[process];
			} else {
				if (typeof sublistsData[process] === 'undefined' ||
					typeof sublistsData[process][list] === 'undefined'
				) {
					return;
				}

				delete sublistsData[process][list][sublist];
			}
		};

		function setSublist(process, list, itemId, sublist, relations) {
			if (typeof sublistsData[process] === 'undefined') {
				sublistsData[process] = {};
			}

			if (typeof sublistsData[process][list] === 'undefined') {
				sublistsData[process][list] = {};
			}

			if (typeof sublistsData[process][list][sublist] === 'undefined') {
				sublistsData[process][list][sublist] = {};
			}

			sublistsData[process][list][sublist][itemId] = relations;
		}

		let orderCache = {};

		function orderList(process, items) {
			if (!orderCache.hasOwnProperty(process)) {
				return ListProcesses.get(process).then(function (pData) {
					orderCache[process] = pData.ListOrder
					return _.sortBy(items, [function (o) {
						if (orderCache[process] && orderCache[process].indexOf("list_item") > -1 && o[orderCache[process]]) return o[orderCache[process]].toLowerCase();
						return o[orderCache[process]];
					}]);
				})
			} else {
				let d = $q.defer();
				d.resolve(_.sortBy(items, [function (o) {
					if (orderCache[process] && orderCache[process].indexOf("list_item") > -1 && o[orderCache[process]]) return o[orderCache[process]].toLowerCase();
					return o[orderCache[process]];
				}]));
				return d.promise;
			}
		}

		self.getListItems = function (process, reload) {
			if (!reload && processData[process]) {
				let promises = [];
				_.each(processData[process], function (i) {
					promises.push(self.prepareItem(process, i));
				});
				return $q.all(promises).then(function (items) {
					return orderList(process, items);
				});
			}

			let p = itemPromises.list;
			if (typeof p[process] === 'undefined') {
				p[process] = ListProcesses.get(process).then(function (pData) {
					if (!orderCache[process]) {
						orderCache[process] = pData.ListOrder;
					}
					return self.getItems(process, undefined, reload, pData.ListOrder).then(function (items) {
						delete p[process];
						return items;
					});
				});
			}

			return p[process];
		};

		self.getItems = function (process, itemIds, reload, sortBy) {
			if (typeof sortBy === 'undefined') sortBy = 'primary';
			let promises = [];
			let items = [];

			if (Array.isArray(itemIds)) {
				let i = itemIds.length;
				while (i--) {
					if (itemIds[i] !== 0) {
						promises.push(self.getItem(process, itemIds[i]).then(function (item) {
							items.push(item);
						}));
					}
				}
			} else {
				if (Common.isFalse(reload) && processData[process]) {
					angular.forEach(processData[process], function (data) {
						promises.push(self.prepareItem(process, data).then(function (item) {
							items.push(item);
						}));
					});
				} else {
					let p = itemPromises.items;
					if (typeof p[process] === 'undefined') {
						p[process] = Items.get([process]).then(function (items_) {
							let promises_ = [];
							let i = items_.length;
							while (i--) {
								self.setData({ Data: items_[i] }, process, items_[i].item_id);

								promises_.push(self.prepareItem(process, items_[i]));
							}

							return $q.all(promises_).then(function (items_) {
								delete p[process];
								return items_;
							});
						});
					}

					promises.push(p[process].then(function (items_) {
						items = items_;
					}));
				}
			}

			return $q.all(promises).then(function () {
				return items.sort(function (a, b) {
					return a[sortBy] < b[sortBy] ? -1 : 1;
				});
			});
		};

		self.hasHistoryCondition = function (process, states, itemId, processGroupId, archiveDates) {
			let forcedHistory = self.getHistoryDate(process, itemId);

			if (forcedHistory) {
				return forcedHistory;
			} else if (states && states.groupStates[processGroupId]) {
				let s = states.groupStates[processGroupId].s;
				let i = s.length;
				let historyCondition = false;

				while (i--) {
					if (s[i] === 'history') {
						historyCondition = true;
					} else if (s[i] === 'write') {
						break;
					}
				}

				if (i === -1 && historyCondition) {
					let groupArchive = archiveDates[processGroupId];
					if (Array.isArray(groupArchive) && groupArchive.length) {
						return moment(groupArchive[0]).format('YYYY-MM-DDTHH:mm:ss');
					}
				}
			}
		};

		self.forceHistoryDate = function (process, itemId, date, resetOnViewChange) {
			if (typeof forceHistoryDates[process] === 'undefined') {
				forceHistoryDates[process] = {};
			}

			forceHistoryDates[process][itemId] = date;

			if (Common.isTrue(resetOnViewChange)) {
				let i = unforceHistoryDates.length;
				while (i--) {
					let item = unforceHistoryDates[i];

					if (item.process === process && item.itemId === itemId) {
						break;
					}
				}

				if (i === -1) {
					unforceHistoryDates.push({
						process: process,
						itemId: itemId
					});
				}
			}
		};

		self.getHistoryDate = function (process, itemId) {
			let h = forceHistoryDates[process];
			if (h && h[itemId]) {
				return moment(h[itemId]).format('YYYY-MM-DDTHH:mm:ss');
			}
		};

		self.unforceHistoryDate = function (process, itemId) {
			if (typeof itemId === 'undefined') {
				if (forceHistoryDates[process]) {
					delete forceHistoryDates[process][itemId];
				}
			} else if (typeof process === 'undefined') {
				//this weird?! process == undefined.. how can u delete?
				// @ts-ignore
				delete forceHistoryDates[process];
			} else {
				forceHistoryDates = {};
			}
		};

		self.deleteItems = function (process, itemIds) {
			return Items.new([process, 'delete'], itemIds).then(function () {
				let i = itemIds.length;
				while (i--) {
					self.invalidateData(process, itemIds[i]);
				}
			});
		};

		self.deleteItem = function (process, itemId) {
			return Items.delete([process, itemId]).then(function () {
				self.invalidateData(process, itemId);
			});
		};

		self.getStates = function (process, itemId) {
			return States.get([process, itemId]);
		};

		self.getStatesForGroup = function (process, itemId, groupId) {
			return States.get(['forGroup', process, itemId, groupId]);
		};

		self.setColumn = function (itemColumn, process, itemColumnId) {
			if (typeof processColumns[process] === 'undefined') {
				processColumns[process] = {};
			}

			let pc = processColumns[process];
			if (typeof pc[itemColumnId] === 'undefined') {
				pc[itemColumnId] = angular.copy(itemColumn);
			} else {
				angular.forEach(itemColumn, function (value, key) {
					pc[itemColumnId][key] = value;
				});
			}

			let dt = itemColumn.DataType;
			if (dt == 11 || dt == 16 || dt == 20) {
				self.ignoreColumn(process, itemColumn.Name);
			}

			return angular.copy(pc[itemColumnId]);
		};

		self.getLink = function (parentProcess, parentItemId, itemColumnId, itemId) {
			let forcedHistory = self.getHistoryDate(parentProcess, parentItemId);

			let d = $q.defer();

			if (typeof linkData[parentProcess] === 'undefined' ||
				typeof linkData[parentProcess][parentItemId] === 'undefined' ||
				typeof linkData[parentProcess][parentItemId][itemColumnId] === 'undefined' ||
				typeof linkData[parentProcess][parentItemId][itemColumnId][itemId] === 'undefined' || forcedHistory
			) {
				if (forcedHistory) {
					ItemsData.get([parentProcess, parentItemId, itemColumnId, itemId, forcedHistory]).then(function (link) {
						if (link) {
							d.resolve(link);
						} else {
							d.reject();
						}
					}, function () {
						d.reject();
					});
				} else {
					ItemsData.get([parentProcess, parentItemId, itemColumnId, itemId]).then(function (link) {
						if (link) {
							let data = {
								LinkData: {}
							};

							data.LinkData[itemColumnId] = {};
							data.LinkData[itemColumnId][itemId] = link;

							let l = self.setData(data, parentProcess, parentItemId);
							d.resolve(l.LinkData[itemColumnId][itemId]);
						} else {
							d.reject();
						}
					}, function () {
						d.reject();
					});
				}
			} else {
				d.resolve(linkData[parentProcess][parentItemId][itemColumnId][itemId]);
			}

			return d.promise;
		};

		self.getColumns = function (process, reload) {
			if (Common.isFalse(reload) && processColumns[process]) {
				let d = $q.defer();
				d.resolve(angular.copy(processColumns[process]));

				return d.promise;
			}

			let promises = columnPromises.columns;
			if (typeof promises[process] === 'undefined') {
				promises[process] = ItemColumns.get(process).then(function (itemColumns) {
					let i = itemColumns.length;
					while (i--) {
						let ic = itemColumns[i];
						self.setColumn(ic, process, ic.ItemColumnId);
					}

					delete promises[process];
					return angular.copy(processColumns[process]);
				});
			}

			return promises[process];
		};

		self.getColumnByName = function (process, columnName, reload) {
			if (Common.isFalse(reload)) {
				let itemColumnId;

				angular.forEach(processColumns[process], function (itemColumn, columnId) {
					if (itemColumn.Name === columnName) {
						itemColumnId = columnId;
					}
				});

				if (itemColumnId) {
					let d = $q.defer();
					d.resolve(angular.copy(processColumns[process][itemColumnId]));

					return d.promise;
				}
			}

			let promises = columnPromises.column;

			if (typeof promises[process] === 'undefined' || typeof promises[process][columnName] === 'undefined') {
				if (typeof promises[process] === 'undefined') {
					promises[process] = {};
				}

				promises[process][columnName] = ItemColumns.get([process, 'name', columnName]).then(function (itemColumn) {
					delete promises[process][columnName];
					return self.setColumn(itemColumn, process, itemColumn.ItemColumnId);
				});
			}

			return promises[process][columnName];
		};

		self.getColumn = function (process, itemColumnId, reload) {
			if (Common.isFalse(reload) && processColumns[process] && processColumns[process][itemColumnId]) {
				let d = $q.defer();
				d.resolve(angular.copy(processColumns[process][itemColumnId]));

				return d.promise;
			}

			let promises = columnPromises.column;

			if (typeof promises[process] === 'undefined' || typeof promises[process][itemColumnId] === 'undefined') {
				if (typeof promises[process] === 'undefined') {
					promises[process] = {};
				}

				promises[process][itemColumnId] = ItemColumns.get([process, itemColumnId]).then(function (itemColumn) {
					delete promises[process][itemColumnId];
					return self.setColumn(itemColumn, process, itemColumnId);
				});
			}

			return promises[process][itemColumnId];
		};

		self.getProcessGroups = function (process, reload) {
			if (Common.isFalse(reload) && processGroups[process]) {
				let d = $q.defer();
				d.resolve(angular.copy(processGroups[process]));
				return d.promise;
			}

			return ProcessGroups.get(process).then(function (groups) {
				processGroups[process] = groups;
				return angular.copy(processGroups[process]);
			});
		};

		self.getTabHierarchy = function (process, itemId) {
			return ProcessTabHierarchy.get([process, itemId]).then(function (hierarchy) {
				angular.forEach(hierarchy.lists, function (items, process) {
					let i = items.length;
					while (i--) {
						let item = items[i];
						if (typeof item.item_id === 'undefined') {
							continue;
						}

						items[i] = self.setData({ Data: item }, process, item.item_id).Data;
					}
				});

				angular.forEach(hierarchy.linkCols, function (columns, process) {
					let i = columns.length;
					while (i--) {
						let column = columns[i];
						if (typeof column.ItemColumnId === 'undefined') {
							continue;
						}

						columns[i] = self.setColumn(column, process, column.ItemColumnId);
					}
				});

				return hierarchy;
			});
		};

		self.getProcessContainer = function (process, containerId) {
			return ProcessContainer.get([process, containerId]);
		};

		self.getDialogAction = function (process, dialogId, itemId) {
			return DialogActions.get(['Dialogs', process, dialogId, itemId, 'trans']);
		};

		self.getDialogContainers = function (process) {
			return DialogContainers.get([process, 'dialog']);
		};
		self.getDialogContainersWithSpecificContainer = function (process, containerId) {
			return DialogContainers.get([process, 'dialog', containerId]);
		};

		self.getDialogContainersForMeta = function (process, containerId) {
			return DialogContainers.get([process, containerId, 'dialog']);
		};

		self.doAction = function (process, itemId, actionId, buttonId, optionalData?) {
			return DialogActions.new([process, actionId, itemId, buttonId]).then(function (response) {
				let data = optionalData ? optionalData : response.data;

				if (typeof data === 'undefined') {
					return response;
				}

				if (data != null && (data.statesChanged || optionalData)) {
					self.invalidateActions(process, itemId);
				}

				/*	if(data != null){
						self.setData(data, process, itemId);
					}
	*/
				notifyActionTriggered(process, itemId);

				return response;
			});
		};

		self.getActions = function (process, itemId, reload) {
			//Checks if actions are already in cache -- invalidation done by SignalR 'ActionExecuted'
			if (Common.isFalse(reload) && processActions[process] && processActions[process][itemId]) {
				let d = $q.defer();
				d.resolve(angular.copy(processActions[process][itemId]));
				return d.promise;
			}

			//Check which lists we already have
			let existingLists = [];
			_.each(processData, function (value, key) {
				if (key.toString().startsWith("list_")) existingLists.push({
					Name: key,
					Count: Object.keys(value).length
				});
			});

			//Get Layout
			return Actions.new([process, itemId], existingLists).then(function (response) {
				if (typeof processActions[process] === 'undefined') {
					processActions[process] = {};
				}

				processActions[process][itemId] = response;
				let promises = [];
				angular.forEach(response.Grids.lists, function (items, listProcess) {

					if (items == null) {
						promises.push(orderList(listProcess, processData[listProcess]).then(function (resultItems) {
							processActions[process][itemId].Grids.lists[listProcess] = resultItems;
						}));
					} else {
						let i = items.length;
						while (i--) {
							let item = items[i];
							if (typeof item.item_id === 'undefined') continue;
							items[i] = self.setData({ Data: item }, listProcess, item.item_id).Data;
						}
					}
				});

				angular.forEach(response.Grids.linkCols, function (columns, process) {
					let i = columns.length;
					while (i--) {
						let column = columns[i];
						if (typeof column.ItemColumnId === 'undefined') {
							continue;
						}

						columns[i] = self.setColumn(column, process, column.ItemColumnId);
					}
				});

				angular.forEach(response.Grids.hierarchy, function (group) {
					angular.forEach(group, function (tab) {
						let i = tab.grids.length;
						while (i--) {
							let grid = tab.grids[i];
							let f = grid.fields;
							let j = f.length;
							while (j--) {
								let ic = f[j].ItemColumn;
								f[j].ItemColumn = self.setColumn(ic, process, ic.ItemColumnId);
							}
						}
					});
				});


				return $q.all(promises).then(function () {
					return angular.copy(response);
				});
			});
		};

		self.getProcess = function (itemId) {
			return Process.get(itemId);
		};

		self.getConditions = function (process) {
			return Conditions.get(process);
		};

		self.isUnique = function (process, columnId, variable) {
			let o = {
				columnId: 0,
				itemId: 0,
				value: variable
			};
			return Items.new([process, columnId, 'unique'], o).then(function (response) {
				return !!response;
			});
		};
		self.isValueUnique = function (process, columnId, itemId, value) {
			let o = {
				columnId: columnId,
				itemId: itemId,
				value: value
			};
			return Items.new([process, 'unique'], o).then(function (response) {
				return !!response;
			});
		};

		self.notifyNewItem = function (process, itemId) {
			notifyCallback(newItemCallbacks, process, itemId);
		};

		self.subscribeNewItem = function (scope, callback, process) {
			prepareSubscribe(newItemCallbacks, scope, callback, process);
		};

		self.startActionListener = function () {
			ConnectorService.listen("ActionExecuted", function (response) {
				//When an action is triggered on the server all clients will remove related item ids fom cache
				_.forIn(response, function (process, itemId) {
					if (processActions[process] && processActions[process][itemId]) {
						delete processActions[process][itemId];
					}
				});
			});
			ConnectorService.listen("ListChanged", function (process) {
				//When a list is changed on the server all clients will remove related process fom cache
				delete processActions[process];
			});
		}

		self.GetPrimariesAndSecondaries = function (process, portfolioId, itemIds) {
			return Process.new([process, portfolioId], itemIds).then(function (data) {
				return data;
			});
		};

		self.deduceParentProcess = function(process: string, ownerColumnName: string){
			return Items.get([process,ownerColumnName,'deduceparent']);
		}
	}
})();