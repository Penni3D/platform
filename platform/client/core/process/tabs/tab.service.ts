﻿(function () {
	'use strict';

	angular
		.module('core.process')
		.service('ProcessTabService', ProcessTabService);

	ProcessTabService.$inject = ['$q', 'ApiService', '$filter', 'ProcessService', 'CalendarService', 'ViewService', '$rootScope', 'MessageService', 'Translator'];

	function ProcessTabService($q, ApiService, $filter, ProcessService, CalendarService, ViewService, $rootScope, MessageService, Translator) {
		var self = this;

		var ProcessTabs = ApiService.v1api('settings/ProcessTabs');
		var ArchiveReport = ApiService.v1api('features/ArchiveReport');
		var ItemColumn = ApiService.v1api('meta/ItemColumns');
		var Help = ApiService.v1api('settings/ProcessContainerColumns');

		self.getButtons = function (process, itemId, tabId) {
			return ProcessTabs.get([process, tabId]).then(function (tab) {
				return ProcessService.getStates(process, itemId).then(function () {
					var promises = [];

					for (var i = 0, l = tab.Columns.length; i < l; i++) {
						promises.push(ItemColumn.get([process, tab.Columns[i]]));
					}

					return $q.all(promises).then(function (data) {
						return data;
					});
				});
			});
		};

		self.getArchiveReport = function (process, itemId, itemColumnId) {
			return ArchiveReport.get([process, itemId, itemColumnId]).then(function (reports) {
				var promises = [];
				angular.forEach(reports, function (report) {
					var utcArchiveDate = moment.utc(report.archive_start).toDate();
					var localArchiveDate = moment(utcArchiveDate).local().format('HH:mm');
					report.archive_start_formatted = CalendarService.formatDate(report.archive_start) + ' ' + localArchiveDate;

					if (typeof report.archive_userid !== 'undefined' && report.archive_userid !== null) {
						promises.push(ProcessService
							.getItem('user', report.archive_userid)
							.then(function (p) {
								report.archive_start_formatted += ' (' + p.primary + ')';
							})
						);
					}
				});

				return $q.all(promises).then(function () {
					return reports;
				});
			});
		};

		self.saveHelp = function (process, itemId, processContainerColumnId, containerId, itemColumnId, helpText) {
			return Help.save([process, itemId, processContainerColumnId, containerId, itemColumnId], JSON.stringify(helpText));
		};

		self.doAction = function (params) {
			ViewService.lock(params.ViewId);
			self.doActionFinally(undefined, params)
		}

		self.doActionFinally = function (data, pp) {
			let fromSubProcess = typeof pp.ParentViewId != 'undefined';
			ProcessService
				.doAction(
					pp.process,
					pp.itemId,
					pp.button.ProcessActionId,
					pp.button.ItemColumnId,
					data)
				.then(function (action) {
					ViewService.unlock(!fromSubProcess ? pp.ViewId : pp.ParentViewId);
					fromSubProcess = false;
					if (typeof pp.button.DataAdditional2 !== 'string') {
						MessageService.toast('NO_ACTIONS_SET');
						return;
					}

					//FIX THIS

					let daOptions = JSON.parse(pp.button.DataAdditional2);
					let nextState = daOptions.initialState;

					let view;
					let viewId = typeof pp.ParentViewId === 'undefined' ?
						pp.ViewId : pp.ParentViewId;
					let params = {
						process: undefined,
						itemId: undefined,
						tabId: undefined
					};
					let options = {
						split: false,
						replace: undefined
					};

					//Check where to go next
					if (nextState === 'frontpage') {
						view = 'frontpage';
					} else if (nextState && nextState !== 'nothing' && nextState !== "") {
						params.itemId = pp.itemId;
						params.process = pp.process;

						if (nextState.indexOf('tab') === -1) {
							view = 'process.' + nextState;
						} else {
							view = 'process.tab';
							params.tabId = nextState.split('_')[1];
						}
					} else if (nextState == 'nothing') {
						view = undefined;
					}

					//Check view actions to take
					_.each(daOptions.viewActions, function (viewAction) {
						if (viewAction === 'refresh') {
							ViewService.refresh(viewId);
						} else if (viewAction === 'refresh_tables') {
							$rootScope.$broadcast('RefreshAllTables', {});
						} else if (viewAction === 'refresh_all' || viewAction === 'refresh_all_cache') {
							setTimeout(function () {
								if (viewAction === 'refresh_all_cache') ProcessService.invalidateActions();
								angular.forEach(ViewService.getViews().views, function (view, viewId) {
									ViewService.refresh(viewId);
								});
							}, 1500);
						} else if (viewAction === 'close') {
							ViewService.close(viewId);
						} else if (viewAction === 'new_item' || viewAction === 'new_item_replace') {
							let newItemProcess;
							let newItemId;

							angular.forEach(action.newIds, function (itemIds, process) {
								if (itemIds.length) {
									newItemProcess = process;
									newItemId = itemIds[0];
								}
							});

							if (newItemProcess && newItemId) {
								ProcessService.notifyNewItem(newItemProcess, newItemId);
								params.process = newItemProcess;
								params.itemId = newItemId;
								view = 'process.tab';

								if (viewAction === 'new_item_replace') {
									options.split = true;
									options.replace = pp.ParentViewId;
									view = 'featurette';
									options.size = 'even';
									params.disableNextPrev = true;
								}
							}
						}
					});

					//Move to another view

					if (view) ViewService.view(view, {params: params}, options);
					//SaveService.ignoreThing();
					//Check if dialog needs to be shown
					if (daOptions.optionalDoneDialog) {
						ProcessService.getDialogAction(pp.process, daOptions.optionalDoneDialog, pp.itemId).then(function (dialogContent) {
							if (daOptions.hasOwnProperty('optionalDoneDialogType') && daOptions['optionalDoneDialogType'] == 3) {
								MessageService.msgBox(Translator.translation(dialogContent).TextTranslation);
							} else {
								MessageService.toast(Translator.translation(dialogContent).TextTranslation, 'confirm');
							}
						});
					}
				});
		}
	}
})();