﻿(function () {
	'use strict';

	angular
		.module('core.process')
		.config(ProcessTabConfig);

	ProcessTabConfig.$inject = ['ViewsProvider'];
	function ProcessTabConfig(ViewsProvider) {
		ViewsProvider.addView('process.tab', {
			controller: 'ProcessTabController',
			template: 'core/process/tabs/tab.html',
			level: 2,
			resolve: {
				Archives: function (ProcessTabService, Grids, StateParameters, Translator) {
					if (Grids.hierarchy[StateParameters.processGroupId][StateParameters.tabId] &&
						Number(Grids.hierarchy[StateParameters.processGroupId][StateParameters.tabId].ArchiveItemColumnId) > 0
					) {
						return ProcessTabService
							.getArchiveReport(
								StateParameters.process,
								StateParameters.itemId,
								Grids.hierarchy[StateParameters.processGroupId][StateParameters.tabId].ArchiveItemColumnId)
							.then(function (archiveReports) {
								if (archiveReports.length) {
									archiveReports.unshift({
										archive_id: undefined,
										archive_start_formatted: Translator.translate('CURRENT_DATE')
									});
								}

								return archiveReports;
						});
					} else {
						return [];
					}
				},
				ArchiveData: function (StateParameters, ProcessService, ArchiveDate) {
					if (ArchiveDate) {
						return ProcessService.getArchiveData(StateParameters.process, StateParameters.itemId, ArchiveDate);
					}
				},

				Navigation: function(ProcessService, StateParameters){
					return ProcessService.getActions(StateParameters.process, StateParameters.itemId, false).then(function(stuff){
						return stuff.Navigation;
					});
				}
			}
		});

		ViewsProvider.addDialog('debug.tab', {
			controller: 'TabConditionDebugController',
			template: 'core/process/debug/conditionDebug.html',
			resolve: {
				ProcessGroups: function (ProcessService, StateParameters) {
					return ProcessService.getProcessGroups(StateParameters.process).then(function (processGroups) {
						var groups = {};
						var i = processGroups.length;
						while (i--) {
							var p = processGroups[i];
							groups[p.ProcessGroupId] = p;
						}

						return groups;
					});
				},
				ProcessConditions: function (ProcessService, StateParameters) {
					return ProcessService.getConditions(StateParameters.process).then(function (conditions) {
						var ret = {};
						var i = conditions.length;
						while (i--) {
							var c = conditions[i];
							ret[c.ConditionId] = c;
						}

						return ret;
					});
				},
				Conditions: function (StateParameters, ApiService) {
					return ApiService.v1api('navigation/States/GetStatesForStates_').get([StateParameters.process]);
				}
			},
			afterResolve: {
				UniqueConditions: function (Conditions, ApiService, StateParameters, $q) {
					var u = [];
					var promises = [];

					function getUniques(states) {
						angular.forEach(states.s, function (id) {
							if (u.indexOf(id) === -1) {
								u.push(id);
							}
						});
					}

					if (angular.isDefined(StateParameters.itemId)) {
						angular.forEach(Conditions, function (state) {
							getUniques(state);

							angular.forEach(state.groupStates, function (groupStates) {
								getUniques(groupStates);

								angular.forEach(groupStates.tabStates, function (tabStates) {
									getUniques(tabStates);

									angular.forEach(tabStates.containerStates, function (containerStates) {
										getUniques(containerStates);
									});
								});
							});
						});

						var result = {};
						angular.forEach(u, function (id) {
							promises.push(ApiService.v1api('navigation/States/getConditionsResult')
								.get([StateParameters.process, id, StateParameters.itemId])
								.then(function (r) {
									result[id] = r;
								}));
						});

						return $q.all(promises).then(function () {
							return result;
						});
					} else {
						return {};
					}
				}
			}
		});
	}
})();
