﻿(function () {
	'use strict';

	angular
		.module('core.process')
		.controller('ProcessTabController', ProcessTabController);

	ProcessTabController.$inject = [
		'$scope',
		'ProcessTabService',
		'ProcessService',
		'Data',
		'Grids',
		'StateParameters',
		'ViewService',
		'States',
		'$filter',
		'Archives',
		'ArchiveData',
		'Common',
		'CalendarService',
		'SaveService',
		'Backgrounds',
		'ArchiveDate',
		'Translator',
		'SettingsService',
		'$rootScope',
		'MessageService',
		'AttachmentService',
		'Navigation',
		'LocalStorageService'
	];

	function ProcessTabController(
		$scope,
		ProcessTabService,
		ProcessService,
		Data,
		Grids,
		StateParameters,
		ViewService,
		States,
		$filter,
		Archives,
		ArchiveData,
		Common,
		CalendarService,
		SaveService,
		Backgrounds,
		ArchiveDate,
		Translator,
		SettingsService,
		$rootScope,
		MessageService,
		AttachmentService,
		Navigation,
		LocalStorageService
	) {
		let listCols = [];
		let gridCount = 0;
		let originalData = Data;
		let archiveCache = {};
		let tab = Grids.hierarchy[StateParameters.processGroupId][StateParameters.tabId];

		let templateId = "";
		LocalStorageService.store("currentActiveProcess", StateParameters.process);

		if (StateParameters.processGroupId) {
			let g = _.find(Navigation, function (o) {
				return o.processGroupId == StateParameters.processGroupId;
			});
			_.each(g.tabs, function (t) {
				if (t.params && t.params.tabId == StateParameters.tabId) templateId = t.templatePortfolioId
			})
		}

		$scope.background = Backgrounds.getBackground(tab.Background).background;
		$scope.archives = Archives;
		$scope.readonly = StateParameters.forceReadOnly || ArchiveDate;
		$scope.lists = Grids.lists;
		$scope.data = ArchiveDate ? ArchiveData.Data : Data;
		$scope.archiveDate = ArchiveDate;
		$scope.process = StateParameters.process;
		$scope.itemId = StateParameters.itemId;
		$scope.helpReadonly = States
			.meta
			.groupStates[StateParameters.processGroupId]
			.tabStates[StateParameters.tabId].s.indexOf('help') === -1;
		$scope.grids = {};
		$scope.panels = {
			1: 'col-xs-12',
			2: 'col-xs-12 col-md-6'
		};
		$scope.inEdit = false;
		Common.subscribeEditMode($scope, function () {
			$scope.inEdit = !$scope.inEdit;
			if ($scope.inEdit == false) {
				ProcessService.invalidateActions($scope.process);
				angular.forEach(ViewService.getViews().views, function (view, viewId) {
					ViewService.refresh(viewId);
				});
			}
		});

		let grids = tab.grids;
		for (let i = 0, l = grids.length; i < l; i++) {
			let grid = grids[i];
			let side = grid.Side;
			if (typeof $scope.grids[side] === 'undefined') {
				$scope.grids[side] = [];

				if (side == 1 || side == 2) {
					gridCount++;
				}
			}

			grid.tabId = StateParameters.tabId;
			grid.groupId = StateParameters.processGroupId;

			$scope.grids[side].push(grid);
			for (let j = 0, l_ = grid.fields.length; j < l_; j++) {
				let f = grid.fields[j];
				if (f.ItemColumn.DataType == 6) {
					listCols.push(f.ItemColumn);
				}
			}

			let j = listCols.length;
			while (j--) {
				updateSublist(listCols[j]);
			}
		}

		if (StateParameters.ParentViewId) {
			let parent = ViewService.getView(StateParameters.ParentViewId);

			if (typeof parent.scope.addFooter === 'function') {
				parent.scope.addFooter(StateParameters.ViewId, {
					template: 'core/process/tabs/footbar.html',
					scope: $scope
				});
			}

			if (typeof parent.scope.showFooter === 'function') {
				parent.scope.showFooter(StateParameters.ViewId);
			}
		} else {
			ProcessService.subscribeStatusChanged($scope, function (itemId, refreshAll?) {
				if (StateParameters.itemId == itemId) {
					if(refreshAll){
						ProcessService.invalidateActions("refresh_all");
						ViewService.refreshOthers(StateParameters.ViewId);
						ViewService.refresh(StateParameters.ViewId);
					} else {
						ViewService.refresh(StateParameters.ViewId);
					}
				}
			}, StateParameters.process);

			if ($scope.grids[3] || $scope.archives.length) {
				ViewService.footbar(StateParameters.ViewId, {template: 'core/process/tabs/footbar.html'});
			}
		}

		angular.forEach($scope.grids[3], function (container) {
			let fields = container.fields;
			for (let i = 0, l = fields.length; i < l; i++) {
				let button = fields[i];
				let label = undefined;

				if (typeof button.Validation.Label === 'string' && button.Validation.Label.length) {
					label = angular.fromJson(button.Validation.Label);
				} else if (typeof button.ItemColumn.Validation.Label === 'string' && button.ItemColumn.Validation.Label.length) {
					label = angular.fromJson(button.ItemColumn.Validation.Label);
				}

				if (label) {
					let hasTranslation = Translator.translation(label);
					if (hasTranslation) button.ItemColumn.LanguageTranslation = label;
				}
				button.Clicked = false;
			}
		});

		$scope.sides = gridCount;

		$scope.fieldChange = function () {
			ProcessService.setDataChanged(StateParameters.process, StateParameters.itemId);
		};
		let checkTranslationLength = function (transObject) {
			if (transObject[$rootScope.clientInformation.locale_id]) {
				let translation = transObject[$rootScope.clientInformation.locale_id];
				if (translation.length > 0) return true;
			}
		};
		let getColumnSide = function (side) {
			let column;
			if (side == 1) {
				column = 'left';
			} else if (side == 2) {
				column = 'right';
			} else if (side == 3) {
				column = 'buttons';
			} else if (side == 4) {
				column = 'top';
			} else if (side == 5) {
				column = 'bottom';
			}
			return column;
		};
		let newContainer = {
			LanguageTranslation: {},
			ProcessContainerSide: 1,
			identifier: 'new',
			RightContainer: false
		};
		$scope.addContainer = function (side) {
			newContainer.ProcessContainerSide = side;
			let save = function () {
				let columnSide = getColumnSide(newContainer.ProcessContainerSide);
				let max_order_no = SettingsService.giveOrderNumber(grids, columnSide);

				let container: Client.ProcessContainer = {
					LanguageTranslation: newContainer.LanguageTranslation,
					MaintenanceName: '',
					ProcessContainerId: 0
				};

				let tab: Client.ProcessTab = {
					ProcessTabId: StateParameters.tabId
				};

				let tabContainer: Client.TabContainer = {
					OrderNo: max_order_no,
					ProcessTabContainerId: 0,
					ProcessTab: tab,
					ProcessContainerSide: newContainer.ProcessContainerSide,
					ProcessContainer: container
				};

				if (newContainer.RightContainer == true) {
					container.RightContainer = 1;
				}

				SettingsService.ProcessContainersAdd($scope.process, container).then(function (cont: Client.ProcessContainer) {
					tabContainer.ProcessContainer = cont;
					tabContainer.OrderNo = max_order_no;

					SettingsService.ProcessTabContainersAdd($scope.process, tabContainer).then(function (cont_: Client.TabContainer) {
						cont_.ProcessContainer.Id = cont_.ProcessContainer.ProcessContainerId;
						cont_.ProcessContainer.tcId = cont_.ProcessTabContainerId;
						if (angular.isUndefined($scope.grids[side.toString()]))
							$scope.grids[side.toString()] = [];
						$scope.grids[side.toString()].push(cont_.ProcessContainer);
					});

					newContainer = {
						LanguageTranslation: {},
						ProcessContainerSide: null,
						identifier: 'new',
						RightContainer: false
					};
				});
			};

			let modifications = {
				create: false,
				cancel: true
			};
			let t = false;

			let onChange = function () {
				if (newContainer.LanguageTranslation)
					t = checkTranslationLength(newContainer.LanguageTranslation);
				if ((newContainer.ProcessContainerSide != null && t) || StateParameters.ProcessTabId == 0)
					modifications.create = true;
			};

			let inputs = [{
				type: 'translation',
				label: 'PROCESS_CONTAINER_NAME',
				model: 'LanguageTranslation'
			}];

			let content = {
				buttons: [{modify: 'cancel', text: 'CANCEL'}, {
					type: 'primary',
					onClick: save,
					text: 'SAVE',
					modify: 'create'

				}],
				inputs: inputs,
				title: 'ADD_NEW_CONTAINER'
			};

			MessageService.dialog(content, newContainer, onChange, modifications)
		};

		let copiedContainer = {
			processContainerId: null,
			ProcessContainerSide: 1,
			identifier: 'copy',
			LanguageTranslation: null
		};
		$scope.copyContainer = function (side) {
			let containersToCopy = [];
			let tt = "";

			tt = 'ADD_EXISTING_CONTAINER';
			SettingsService.ProcessContainers(StateParameters.process).then(function (containers) {
				_.each(containers, function (container) {
					let exists = false;
					_.each($scope.grids, function (cols) {
						for (let item of cols) {
							if (item.Id == container.ProcessContainerId) exists = true;
						}
					});
					if (!exists) {
						container.MaintenanceName = container.MaintenanceName.length == 0 ? Translator.translation(container.LanguageTranslation) : Translator.translation(container.LanguageTranslation) + ' ' + '\'' + container.MaintenanceName + '\'';
						containersToCopy.push(container);
					}
				});

				let modifications = {
					create: false,
					cancel: true
				};

				let onChange = function () {
					if (copiedContainer.processContainerId != null) modifications.create = true;
					_.each(containers, function (container) {
						if (container.ProcessContainerId == copiedContainer.processContainerId) {
							copiedContainer.LanguageTranslation = container.LanguageTranslation;
							return;
						}
					});
				};

				let save = function () {
					let processContainers = $scope.grids[side.toString()] ? $scope.grids[side.toString()] : [];
					let columnSide = getColumnSide(copiedContainer.ProcessContainerSide);
					let max_order_no = SettingsService.giveOrderNumber(processContainers, side);

					let container: Client.ProcessContainer = {
						ProcessContainerId: parseFloat(copiedContainer.processContainerId)
					};

					let tab: Client.ProcessTab = {
						ProcessTabId: StateParameters.tabId
					};

					let tabContainer: Client.TabContainer = {
						OrderNo: max_order_no,
						ProcessTabContainerId: 0,
						ProcessTab: tab,
						ProcessContainerSide: copiedContainer.ProcessContainerSide,
						ProcessContainer: container
					};

					SettingsService.ProcessTabContainersAdd($scope.process, tabContainer).then(function (cont_: Client.TabContainer) {
						cont_.ProcessContainer.Id = cont_.ProcessContainer.ProcessContainerId;
						cont_.ProcessContainer.tcId = cont_.ProcessTabContainerId;
						cont_.ProcessContainer.LanguageTranslation = copiedContainer.LanguageTranslation;

						SettingsService.ProcessContainerColumns($scope.process, copiedContainer.processContainerId).then(function (columns) {
							cont_.ProcessContainer.fields = columns;
							if (angular.isUndefined($scope.grids[side.toString()]))
								$scope.grids[side.toString()] = [];
							$scope.grids[side.toString()].push(cont_.ProcessContainer);
						});
					});
				};

				copiedContainer.ProcessContainerSide = side;
				let content = {
					buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE', modify: 'create'}],
					inputs: [{
						type: 'select',
						label: 'CONTAINER',
						model: 'processContainerId',
						options: containersToCopy,
						optionName: 'MaintenanceName',
						optionValue: 'ProcessContainerId',
						singleValue: true
					}],
					title: tt
				};

				MessageService.dialog(content, copiedContainer, onChange, modifications)
			});
		};

		$scope.hasContainerWriteRight = function (containerId) {
			return typeof ArchiveDate === 'undefined' && States
				.meta
				.groupStates[StateParameters.processGroupId]
				.tabStates[StateParameters.tabId]
				.containerStates[containerId]
				.s
				.indexOf('write') !== -1;
		};

		$scope.archiveChange = function () {
			if ($scope.archiveDate) {
				$scope.readonly = true;

				if (typeof archiveCache[$scope.archiveDate] === 'undefined') {
					ProcessService
						.getArchiveData(StateParameters.process, StateParameters.itemId, $scope.archiveDate)
						.then(function (data) {
							archiveCache[$scope.archiveDate] = data.Data;
							$scope.data = archiveCache[$scope.archiveDate];

							setHeader('(' + CalendarService.formatDate($scope.data.modifyDate) + ')');
						});
				} else {
					$scope.data = archiveCache[$scope.archiveDate];
					setHeader('(' + CalendarService.formatDate($scope.data.modifyDate) + ')');
				}
			} else {
				$scope.readonly = false;
				$scope.data = originalData;
				setHeader();
			}
		};

		$scope.buttonClick = function (button) {
			button.Clicked = true;
			let viewName = 'dialog';
			let meta = null;
			let type = 0;
			if (button.ItemColumn.SubDataAdditional2 == "true" && templateId.length) {
				AttachmentService.getControlAttachments(templateId, 'portfolioTemplates').then(function (atts) {
					if(button.ItemColumn.DataAdditional2.length){
						let parsed = JSON.parse(button.ItemColumn.DataAdditional2);
						if(parsed.selectedTemplates && parsed.selectedTemplates.length){
							let i = atts.length;
							while (i--) {
								if (!parsed.selectedTemplates.includes(atts[i].Name)) {
									atts.splice(i,1);
								}
							}
						}
					}
					if(atts.length == 1){
						let chosenAtts = atts[0];
						ViewService.generateWord(StateParameters.ViewId, StateParameters.process,
							templateId,
							chosenAtts.AttachmentId,
							chosenAtts.Name,
							StateParameters.itemId
						);
						button.Clicked = false;
					} else {
						let data = {AttachmentId: null};
						let save = function () {
							let chosenAtts = _.find(atts, function (o) {
								return o.AttachmentId == data.AttachmentId;
							});
							ViewService.generateWord(StateParameters.ViewId, StateParameters.process,
								templateId,
								chosenAtts.AttachmentId,
								chosenAtts.Name,
								StateParameters.itemId
							);
							button.Clicked = false;
						};

						let discard = function(){
							button.Clicked = false;
							MessageService.closeDialog();
						}

						let content = {
							buttons: [{text: 'DISCARD', onClick:discard}, {type: 'primary', onClick: save, text: 'FILE_DOWNLOAD'}],
							inputs: [{
								type: 'select',
								label: 'FILE_NAME',
								model: 'AttachmentId',
								options: atts,
								optionName: 'Name',
								optionValue: 'AttachmentId'
							}],
							title: 'CHOOSE_A_FILE'
						};
						MessageService.dialog(content, data);
					}
				});
			} else {
				if (button.ItemColumn.ProcessDialogId != null) {
					SettingsService.getProcessActionDialogsNew(StateParameters.process, button.ItemColumn.ProcessDialogId).then(function (dialog) {
						if (dialog.Type == 4) {
							type = 1;
							meta = {'ContainerId': dialog.ProcessContainerId}
						}
						ViewService.view(viewName, {
							params: {
								process: StateParameters.process,
								button: button.ItemColumn,
								itemId: StateParameters.itemId,
								ViewId: StateParameters.ViewId,
								ParentViewId: StateParameters.ParentViewId,
								Meta: meta,
								Type: type
							}
						});
						button.Clicked = false;
					});
				} else {
					SaveService.tick(true);
					let parameters = {
						process: StateParameters.process,
						button: button.ItemColumn,
						itemId: StateParameters.itemId,
						ViewId: StateParameters.ViewId,
						ParentViewId: StateParameters.ParentViewId
					};
					delayedButtonClick(parameters, button);
				}
			}
		};

		function delayedButtonClick(parameters, button) {
			if (SaveService.saving) {
				setTimeout(function () {
					delayedButtonClick(parameters, button);
				}, 100);
			}
			else {
				ProcessTabService.doAction(parameters, button);
				button.Clicked = false;
			}
		}

		$scope.formIsValid = function (button) {
			if (button.DataAdditional &&
				(button.DataAdditional.substring(0, 2) === '1:' || button.DataAdditional === 'ignoreFormValidation')
			) {
				return true;
			} else if ($scope.tabForm && $scope.tabForm.$valid) {
				return true;
			}

			return false;
		};

		$scope.listFieldChange = function (itemColumn) {
			updateSublist(itemColumn);
			$scope.fieldChange();
			if (itemColumn.CheckConditions) SaveService.tick(true);
		};

		$scope.helpTextChange = function (field) {
			ProcessTabService.saveHelp(
				StateParameters.process,
				StateParameters.itemId,
				field.ProcessContainerColumnId,
				field.ProcessContainerId,
				field.ItemColumn.ItemColumnId,
				{'HelpText': field.HelpTextTranslation});
		};

		$scope.emptyFieldChange = function (field) {
			let c = {};
			c[field.ItemColumn.Name] = undefined;

			ProcessService.setDataChanged(StateParameters.process, StateParameters.itemId, false, c);
		};

		Common.subscribeContentResize($scope, determineColumn);

		function determineColumn() {
			let w = $('#main-container-transition > div').first().width();
			if (gridCount > 1 && w > 1280) {
				$scope.sides = 2;
			} else {
				$scope.sides = 1;
			}
		}

		setHeader(StateParameters.title); //, ArchiveDate ? '(' + CalendarService.formatDate(ArchiveDate) + ')' : '');
		function setHeader(e?) {
			if (StateParameters.title == e) e = "";
			$scope.header = {
				title: StateParameters.title,
				extTitle: e
			};
		}

		function updateSublist(field) {
			if (field.DataAdditional != null) {
				//TODO: Server problem
				let sl = Grids.sublists[field.DataAdditional];
				if (sl) {
					let i = sl.length;
					while (i--) {
						let sublist = sl[i];
						let sld = Grids.sublistsData[sublist];
						if (typeof sld === 'undefined') {
							continue;
						}

						let sublistData = [];
						let sublistIds = [];

						angular.forEach($scope.data.Data[field.Name], function (parentId) {
							for (let j = 0, l = sld[parentId].length; j < l; j++) {
								let sublistItem = sld[parentId][j];

								sublistData.push(sublistItem);
								sublistIds.push(sublistItem.item_id);
							}
						});

						$scope.lists[sublist] = sublistData;

						let j = listCols.length;
						while (j--) {
							let list = listCols[j];
							if (list.DataAdditional == sublist) {
								let name = list.Name;
								angular.forEach($scope.data.Data[name], function (id) {
									if (sublistIds.indexOf(id) === -1) {
										$scope.data.Data[name].splice($scope.data.Data[name].indexOf(id), 1);
										ProcessService.setDataChanged(StateParameters.process, StateParameters.itemId);
									}
								});

								updateSublist(list);
								break;
							}
						}
					}
				}
			}
		}
	}
})();