﻿(function () {
	'use strict';

	angular
		.module('core.process')
		.config(FeaturetteConfig)
		.controller('FeaturetteController', FeaturetteController);

	FeaturetteConfig.$inject = ['ViewsProvider'];

	function FeaturetteConfig(ViewsProvider) {
		ViewsProvider.addView('featurette', {
			controller: 'FeaturetteController',
			template: 'core/process/featurette/featurette.html',
			resolve: {
				Navigation: function (ProcessService, StateParameters, ViewService) {
					return ProcessService.getActions(StateParameters.process, StateParameters.itemId).then(function (nav) {
						if (nav.Navigation.length) return nav.Navigation;
						return ViewService.fail('NO_RIGHTS');
					});
				}
			},
			options: {
				replace: 'featurette',
				size: 'narrower'
			}
		});
	}

	FeaturetteController.$inject = [
		'$scope',
		'Navigation',
		'StateParameters',
		'ViewService',
		'UniqueService',
		'ProcessService',
		'SaveService',
		'Translator',
		'RMService'
	];

	function FeaturetteController(
		$scope,
		Navigation,
		StateParameters,
		ViewService,
		UniqueService,
		ProcessService,
		SaveService,
		Translator,
		RMService
	) {
		var compiled = {};
		$scope.navigation = [];
		$scope.footbars = {};
		$scope.id = UniqueService.get('featurette', true);
		$scope.disableNextPrev = true;

		//if you refresh the page RMService.itemSet is always empty so this timeout is to fix the issue
		setTimeout(function () {
			$scope.disableNextPrev = StateParameters.disableNextPrev || _.isEmpty(RMService.itemSet);
			setIcons();
			if(StateParameters.disableNextPrev) return;
			orderRows(RMService.itemSet[StateParameters.itemId], "prev");
			orderRows(RMService.itemSet[StateParameters.itemId], "next");
		}, 150)

		let count = 1;
		let prevCount = 0;
		let nextCount = 0;
		let orderRows = function ($ele, way) {
			if(!$ele) return false;
			if ($ele.prev && way == "prev"){
				orderRows($ele.prev, "prev")
				count++;
				prevCount++;
			}
			if ($ele.next && way == "next"){
				orderRows($ele.next, "next")
				count++;
				nextCount++;
			}
		}

		let syncTooltip = function(){

			//This operation is possibly pretty expensive so it meets the criteria of the "dirty rat's solution"

			//Update 13.9.21. Has the same problem with the row amount if user for example visits multiple different
			//row manager views -> itemSet should be replaced with "visible rows" object to fix this problem

			$scope.header.beforeDownloadIcons[0].hideThis = prevCount == 0;
			$scope.header.beforeDownloadIcons[1].hideThis = nextCount == 0;

			return (prevCount + 1) + " / " +
				count;
		}

		let setIcons = function () {
			if (_.isEmpty(RMService.itemSet)) return;
			if (!$scope.disableNextPrev) {
				$scope.header.beforeDownloadIcons.push({
					icon: 'keyboard_arrow_left',
					onClick: function () {
						$scope.prevQuestion();
					},
					tooltip: function(){
						return syncTooltip();
					},
					hideThis: false
				})
			}

			if (!$scope.disableNextPrev) {
				$scope.header.beforeDownloadIcons.push({
					icon: 'keyboard_arrow_right',
					onClick: function () {
						$scope.nextQuestion();
					},
					tooltip: function(){
						return syncTooltip();
					},
					hideThis: false
				})
			}
		}

		$scope.header = {
			title: StateParameters.title,
			icons: [],
			beforeDownloadIcons: []
		};

		$scope.addFooter = function (viewId, params) {
			$scope.footbars[viewId] = params;
		};

		$scope.showFooter = function (viewId) {
			showFooter(viewId);
		};

		$scope.nextQuestion = function () {
			let next = undefined;
			let parent = undefined;
			if (RMService.itemSet[StateParameters.itemId].next) {
				next = RMService.itemSet[StateParameters.itemId].next;
			} else if (RMService.itemSet[StateParameters.itemId].parent) {
				parent = RMService.itemSet[StateParameters.itemId].parent;
				if (parent && parent.next) next = parent.next.firstEle;
			}
			openQuestion(next);
		}

		$scope.prevQuestion = function () {
			let prev = undefined;
			let parent = undefined;
			if (RMService.itemSet[StateParameters.itemId].prev) {
				prev = RMService.itemSet[StateParameters.itemId].prev;
			} else if (RMService.itemSet[StateParameters.itemId].parent) {
				parent = RMService.itemSet[StateParameters.itemId].parent;
				if (parent && parent.prev) prev = parent.prev.lastEle;
			}

			openQuestion(prev);
		};

		function openQuestion(item) {
			if (item) {
				//Remove existing highlight and then highligh row
				$(".highlight-permanent").removeClass('highlight-permanent');
				item.$ele.addClass('highlight-permanent');
				//Reopen featurette 'wide'
				ViewService.view('featurette',
					{
						params: {
							process: StateParameters.process,
							itemId: item.data.item_id,
							title: item.data.title,
							sizeForNext: StateParameters.sizeForNext
						}
					},
					{
						split: true,
						size: typeof StateParameters.sizeForNext != 'undefined'
							? StateParameters.sizeForNext
							: 'narrower'
					}
				);
			}
		}


		for (var i = 0, l = Navigation.length; i < l; i++) {
			var g = Navigation[i];
			for (var j = 0, l_ = g.tabs.length; j < l_; j++) {
				var t = g.tabs[j];
				let nameProperty = t.hasOwnProperty('customName') && t.customName.length ? 'customName' : 'variable';
				t.translation = Translator.translation(t.view === 'process.tab' ? t.LanguageTranslation : t[nameProperty]);
				$scope.navigation.push(t);
			}
		}

		ProcessService.subscribeStatusChanged($scope, function (itemId, refreshAll?) {
			if (StateParameters.itemId == itemId) {
				if(refreshAll){
					ProcessService.invalidateActions("refresh_all");
					ViewService.refreshOthers(StateParameters.ViewId);
					ViewService.refresh(StateParameters.ViewId);
				} else {
					ViewService.refresh(StateParameters.ViewId);
				}
			}
		}, StateParameters.process);

		var isFirstLoad = true;
		$scope.loadTab = function (tabId) {
			var target = $scope.id + '-' + tabId;

			if (isFirstLoad) {
				loadTab(tabId, target);
				isFirstLoad = false;
			} else {
				ViewService.lock(StateParameters.ViewId);
				SaveService.bypassTick().then(function () {
					ViewService.unlock(StateParameters.ViewId);

					if (compiled[tabId]) {
						showFooter(target);
						return;
					}

					loadTab(tabId, target);
					showFooter(target);
				});
			}
		};

		function loadTab(tabId, targetId) {
			compiled[tabId] = true;

			ViewService.viewAdvanced(
				$scope.navigation[tabId].view,
				{
					params: angular.extend(
						$scope.navigation[tabId].params, {forceReadOnly: false})
				},
				{targetId: targetId, ParentViewId: StateParameters.ViewId});
		}

		function showFooter(viewId) {
			typeof $scope.footbars[viewId] === 'undefined' ?
				ViewService.removeFootbar(StateParameters.ViewId) :
				ViewService.footbar(StateParameters.ViewId, $scope.footbars[viewId]);
		}
	}
})();
