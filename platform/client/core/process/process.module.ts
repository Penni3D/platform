﻿(function () {
	'use strict';

	angular
		.module('core.process', [])
		.config(ProcessConfig);

	ProcessConfig.$inject = ['ViewsProvider'];

	function ProcessConfig(ViewsProvider) {
		ViewsProvider.addView('process', {
			beforeResolve: {
				Data: function (ProcessService, StateParameters) {
					return ProcessService.getItemData(StateParameters.process, StateParameters.itemId, true)
						.then(function (r) {
							return ProcessService.prepareItem(StateParameters.process, r.Data).then(function () {
								return r;
							});
						});
				}
			},
			resolve: {
				Actions: function (StateParameters, ProcessService) {
					//Force reload if not featurette
					let layoutOptimisationDisabled = !(StateParameters.ViewId && StateParameters.ViewId.indexOf('featurette') > -1);

					//Experimental feature: add comments to row below to disable faster layout processing
					layoutOptimisationDisabled = false;

					return ProcessService.getActions(StateParameters.process, StateParameters.itemId, layoutOptimisationDisabled);
				}
			},
			afterResolve: {
				ArchiveDate: function (StateParameters, ProcessService, Actions) {
					return ProcessService.hasHistoryCondition(
						StateParameters.process,
						Actions.States.meta,
						StateParameters.itemId,
						StateParameters.processGroupId,
						Actions.ArchiveDates
					);
				},
				Grids: function (Actions) {
					return Actions.Grids;
				},
				States: function (Actions) {
					return Actions.States;
				},
				Navigation: function (
					ViewService,
					Actions,
					SidenavService,
					MessageService,
					ProcessService,
					Common,
					StateParameters,
					Translator,
					ToolbarService,
					Data,
					$rootScope
				) {
					let states = Actions.States;
					let navigation = Actions.Navigation;

					let i = navigation.length;
					while (i--) {
						let g = navigation[i];
						g.name = Translator.translation(g.translation);
						let j = g.tabs.length;
						while (j--) {
							let t = g.tabs[j];
							if (t.view === 'process.tab') {
								t.name = Translator.translation(t.LanguageTranslation);
								t.params.title = t.name + ' - ' + g.name;
							} else {
								t.name = Translator.translation(t.variable);
								t.params.title = t.name + ' - ' + g.name;
							}
						}
					}

					if (Common.isFalse(StateParameters.bypassFallback)) {
						var v = ViewService.getView(StateParameters.ViewId);
						var isTab = v.name.split('.')[1] === 'tab';
						var tabParam = "";

						if (StateParameters.tabId == '1stTab' || StateParameters.tabId == 'lastTab') {
							tabParam = StateParameters.tabId;
							StateParameters.tabId = undefined;
						}

						if (typeof StateParameters.processGroupId === 'undefined') {
							if (isTab) {
								if (typeof StateParameters.tabId === 'undefined') {
									findSuitableTab(function (tab) {
										return typeof tab.params.tabId === 'undefined';
									});
								} else {
									findSuitableTab(function (tab) {
										return StateParameters.tabId != tab.params.tabId;
									});

									if (typeof StateParameters.processGroupId === 'undefined') {
										findSuitableTab(function (tab) {
											return typeof tab.params.tabId === 'undefined';
										});
									}
								}

								if (typeof StateParameters.processGroupId === 'undefined') {
									absoluteFallBack();
								}
							} else {
								findSuitableFeature();
							}
						} else {
							if (isTab) {
								let evaluate = typeof StateParameters.tabId === 'undefined' ?
									function (tab) {
										return typeof tab.params.tabId === 'undefined';
									} :
									function (tab) {
										return tab.params.tabId != StateParameters.tabId;
									};

								let failed = findSuitableTab(evaluate, StateParameters.processGroupId);
								if (failed) {
									absoluteFallBack();
								}
							} else {
								findSuitableFeature(StateParameters.processGroupId);
							}
						}

						if (typeof StateParameters.processGroupId === 'undefined' && Common.isFalse(v.options.extend)) {
							return ViewService.fail('META_NO_WHERE_TO_GO');
						}

						if (tabParam == "1stTab") findFirstLastTabId(StateParameters.processGroupId, true);
						if (tabParam == "lastTab") findFirstLastTabId(StateParameters.processGroupId, false);
					}

					let buttons = [];
					if (typeof states.meta !== 'undefined') {
						let s = states.meta.States;

						if (s.indexOf('configure') !== -1) ToolbarService.setConfigureFunction(function () {
							$rootScope.$emit('editModeChanged');
						});

						if (s.indexOf('debug') !== -1) ToolbarService.setDebugFunction(function () {
							ViewService.view('debug.tab', {
								params: StateParameters
							});
						});

						if (s.indexOf('delete') !== -1) ToolbarService.setDeleteFunction(function () {
							let puzzleAnswer = StateParameters.itemId;
							let puzzleTitle = Translator.translate('DELETE_CONFIRMATION_SERIOUS')
								.replace('[X]', puzzleAnswer).replace('[Y]', Data.Data.primary);

							let model = {
								puzzle: ''
							};

							let params = {
								message: puzzleTitle,
								inputs: [
									{
										type: 'string',
										label: 'DELETE_PUZZLE',
										model: 'puzzle'
									}],
								buttons: [
									{
										type: 'warn',
										text: 'DELETE_CONFIRMATION_OK',
										onClick: function () {
											if (model.puzzle == puzzleAnswer) {
												return ProcessService.deleteItem(
													StateParameters.process,
													StateParameters.itemId
												).then(function () {

													StateParameters.deleted = true;
													ViewService.deleteLayoutRoute(StateParameters.itemId)
													ViewService.view('frontpage', {});
												});
											} else {
												MessageService.toast('DELETE_PUZZLE_FAIL', 'warn');
											}
										}
									},
									{
										text: 'MSG_CANCEL'
									}]
							}
							MessageService.dialog(params, model);
						});
					}

					let accordionIndex = navigation.length;
					while (accordionIndex--) {
						if (navigation[accordionIndex].processGroupId == StateParameters.processGroupId) {
							break;
						}
					}

					ToolbarService.setSecondSlot(Data.Data.primary);

					//ToolbarService.setThirdSlot(StateParameters.name);
					SidenavService.navigation(navigation, {index: accordionIndex});

					if (buttons.length) {
						buttons.push({
							name: 'CLOSE',
							icon: 'close',
							onClick: function () {
								MessageService.removeBackdrop();
							}
						});

						//console.log(buttons);
						//MessageService.footer(buttons);
					}

					return navigation;


					function findFirstLastTabId(processGroupId, f) {
						let i = navigation.length;
						let writeOption;
						let readOption;

						while (i--) {
							let group = navigation[i];

							if (typeof group === 'undefined' || typeof group.tabs === 'undefined') continue;
							if (typeof processGroupId !== 'undefined' && processGroupId != group.processGroupId) continue;

							let tab = group.tabs[0];
							if (!f) tab = group.tabs[group.tabs.length - 1];

							StateParameters.name = tab.name;
							StateParameters.title = tab.params.title;
							StateParameters.tabId = tab.params.tabId;
							StateParameters.processGroupId = group.processGroupId;

							break;
						}
					}

					function findSuitableTab(evaluate, processGroupId?) {

						let writeOption;
						let readOption;
						let i = navigation.length;

						while (i--) {
							let group = navigation[i];

							if (typeof group === 'undefined' || typeof group.tabs === 'undefined') {
								continue;
							}

							if (typeof processGroupId !== 'undefined' && processGroupId != group.processGroupId) {
								continue;
							}

							let j = group.tabs.length;


							for(let k = 0; k < j; k++){
								let tab = group.tabs[k];

								let groupStates = states.meta.groupStates[group.processGroupId];

								if (evaluate(tab) ||
									typeof groupStates === 'undefined' ||
									typeof groupStates.tabStates[tab.params.tabId] === 'undefined'
								) {
									continue;
								}

								let tabState = states.meta.groupStates[group.processGroupId].tabStates[tab.params.tabId].s;
								if (typeof readOption === 'undefined' && tabState.indexOf('read') !== -1) {
									readOption = {
										name: tab.name,
										title: tab.params.title,
										tabId: tab.params.tabId,
										processGroupId: group.processGroupId
									};
								}

								if (typeof writeOption === 'undefined' && tabState.indexOf('write') !== -1) {
									writeOption = {
										name: tab.name,
										title: tab.params.title,
										tabId: tab.params.tabId,
										processGroupId: group.processGroupId
									};
								}

								if (tabState.indexOf('active') !== -1) {
									StateParameters.name = tab.name;
									StateParameters.title = tab.params.title;
									StateParameters.tabId = tab.params.tabId;
									StateParameters.processGroupId = group.processGroupId;

									return false;
								}
							}
						}

						if (writeOption) {
							StateParameters.name = writeOption.name;
							StateParameters.title = writeOption.title;
							StateParameters.tabId = writeOption.tabId;
							StateParameters.processGroupId = writeOption.processGroupId;

							return false;
						} else if (readOption) {
							StateParameters.name = readOption.name;
							StateParameters.title = readOption.title;
							StateParameters.tabId = readOption.tabId;
							StateParameters.processGroupId = readOption.processGroupId;

							return false;
						}

						return true;
					}

					function findSuitableFeature(processGroupId?) {
						let writeOption;
						let readOption;
						let absOption;
						let i = navigation.length;
						while (i--) {
							let group = navigation[i];

							if (typeof group === 'undefined' || typeof group.tabs === 'undefined') {
								continue;
							}

							if (typeof processGroupId !== 'undefined' && processGroupId != group.processGroupId) {
								continue;
							}

							let j = group.tabs.length;
							while (j--) {
								let tab = group.tabs[j];

								if (tab.view == v.name) {
									if (typeof absOption === 'undefined') {
										absOption = {
											name: tab.name,
											title: tab.params.title,
											processGroupId: group.processGroupId
										};
									}

									let groupStates = states.meta.groupStates[group.processGroupId];
									if (typeof groupStates === 'undefined') {
										continue;
									}

									if (typeof readOption === 'undefined' && groupStates.s.indexOf('read') !== -1) {
										readOption = {
											name: tab.name,
											title: tab.params.title,
											processGroupId: group.processGroupId
										};
									}

									if (typeof writeOption === 'undefined' && groupStates.s.indexOf('write') !== -1) {
										writeOption = {
											name: tab.name,
											title: tab.params.title,
											processGroupId: group.processGroupId
										};
									}

									if (groupStates.s.indexOf('active') !== -1) {
										StateParameters.name = tab.name;
										StateParameters.title = tab.params.title;
										StateParameters.processGroupId = group.processGroupId;

										return;
									}
								}
							}
						}

						if (writeOption) {
							StateParameters.name = writeOption.name;
							StateParameters.title = writeOption.title;
							StateParameters.processGroupId = writeOption.processGroupId;
						} else {
							if (readOption) {
								StateParameters.name = readOption.name;
								StateParameters.title = readOption.title;
								StateParameters.processGroupId = readOption.processGroupId;
							} else {
								if (absOption) {
									StateParameters.name = absOption.name;
									StateParameters.title = absOption.title;
									StateParameters.processGroupId = absOption.processGroupId;
								} else {
									absoluteFallBack();
								}
							}
						}
					}

					function absoluteFallBack() {
						let lastResort;
						let i = navigation.length;
						while (i--) {
							let group = navigation[i];

							if (typeof group === 'undefined' || typeof group.tabs === 'undefined') {
								continue;
							}

							let j = group.tabs.length;

							for(let k = 0; k < j; k++){
								let tab = group.tabs[k];
								if (typeof tab === 'undefined') {
									continue;
								} else if (typeof lastResort === 'undefined' && typeof tab.params.tabId === 'undefined') {
									lastResort = {
										name: tab.name,
										title: tab.params.title,
										processGroupId: group.processGroupId,
										view: tab.view
									};

									continue;
								}

								StateParameters.name = tab.name;
								StateParameters.title = tab.params.title;
								StateParameters.processGroupId = group.processGroupId;
								StateParameters.tabId = tab.params.tabId;

								if (!isTab) {
									ViewService.reroute(StateParameters.ViewId, 'process.tab');
								}

								return;
							}
						}

						if (lastResort) {
							StateParameters.name = lastResort.name;
							StateParameters.title = lastResort.title;
							StateParameters.processGroupId = lastResort.processGroupId;
							ViewService.reroute(StateParameters.ViewId, lastResort.view);
						}
					}
				}
			}
		});
	}
})();
