(function () {
	'use strict';

	angular
		.module('core.process')
		.controller('TabConditionDebugController', TabConditionDebugController);

	TabConditionDebugController.$inject = ['$scope', 'ProcessGroups', 'ProcessConditions', 'Conditions', 'UniqueConditions', 'ViewService', 'MessageService', 'StateParameters', 'Translator', '$rootScope'];
	function TabConditionDebugController($scope, ProcessGroups, ProcessConditions, Conditions, UniqueConditions, ViewService, MessageService, StateParameters, Translator, $rootScope) {

		$scope.uniqueConditions = UniqueConditions;
		$scope.evaluatedConditions = {};
		$scope.activeGroup = StateParameters.processGroupId;
		$scope.userLanguage = $rootScope.clientInformation.locale_id;

	/*	angular.forEach(UniqueConditions, function (u, key) {
			$scope.evaluatedConditions[key] = u.substr(u.length - 5, 5);
		});
*/
		$scope.conditions = Conditions;
		$scope.processGroups = ProcessGroups;
		$scope.processConditions = ProcessConditions;

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.showCondition = function (conditionId) {
			var condition = $scope.processConditions[conditionId];

			let params = {process: StateParameters.process, conditionId: conditionId,
				ConditionTranslation: Translator.translation(condition.LanguageTranslation)};

			params.Condition = condition;

			ViewService.view('settings.conditions.fields',
				{
					params: params,
					locals: {
						NewCondition: condition,
						ConditionTabTranslations: {},
						ConditionContainerTranslations: {}
					}
				},
				{
					split: true,
					remember: false
				});

			MessageService.closeDialog();
		};
	}
})();