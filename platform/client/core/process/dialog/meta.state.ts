﻿(function () {
	'use strict';

	angular
		.module('core.dialogs')
		.config(MetaConfig)
		.controller('MetaDialogController', MetaDialogController);

	MetaConfig.$inject = ['ViewsProvider'];

	function MetaConfig(ViewsProvider) {
		ViewsProvider.addDialog('dialog', {
			template: 'core/process/dialog/meta.html',
			controller: 'MetaDialogController',
			resolve: {
				Content: function (StateParameters, ProcessService) {
					if (StateParameters.button.ProcessDialogId) {
						return ProcessService.getDialogAction(StateParameters.process, StateParameters.button.ProcessDialogId, StateParameters.itemId);
					}
				},
				DialogContainer: function (StateParameters, ProcessService) {
					if (StateParameters.Meta != null) {
						return ProcessService.getDialogContainersForMeta(StateParameters.process, StateParameters.Meta.ContainerId)
					}
				},
				Data: function (ApiService, StateParameters, ProcessService) {
					if (StateParameters.Type == 0) return {};
					return ProcessService.getItemData(StateParameters.process, StateParameters.itemId);
				}
			}
		});
	}

	MetaDialogController.$inject = [
		'$scope',
		'Content',
		'StateParameters',
		'MessageService',
		'ProcessService',
		'ViewService',
		'Translator',
		'Backgrounds',
		'$rootScope',
		'DialogContainer',
		'Data',
		'ProcessTabService',
		'AttachmentService',
		'Upload',
		'$q'
	];

	function MetaDialogController(
		$scope,
		Content,
		StateParameters,
		MessageService,
		ProcessService,
		ViewService,
		Translator,
		Backgrounds,
		$rootScope,
		DialogContainer,
		Data,
		ProcessTabService,
		AttachmentService,
		Upload,
		$q
	) {
		$scope.dialogType = StateParameters.Type;
		$scope.data = angular.copy(Data);
		$scope.itemId = StateParameters.itemId;
		let listCols = [];

		$scope.background = Backgrounds.getRandomBackground().background;
		$scope.formValid = false;

		$scope.f = {};
		$scope.f.dialogForm = {};
		$scope.canModifyForm = function () {
			if ($scope.f.dialogForm.$valid == 'undefined') {
				return false;
			}
			return $scope.f.dialogForm.$valid;
		}

		function updateSublist(field) {
			let sl = [DialogContainer.sublists[field.DataAdditional]];
			let i = sl.length;
			while (i--) {
				let sublist = sl[i];
				let sld = DialogContainer.sublistsData[sublist];
				if (typeof sld === 'undefined') {
					continue;
				}

				let sublistData = [];
				let sublistIds = [];

				angular.forEach($scope.data.Data[field.Name], function (parentId) {
					for (let j = 0, l = sld[parentId].length; j < l; j++) {
						let sublistItem = sld[parentId][j];

						sublistData.push(sublistItem);
						sublistIds.push(sublistItem.item_id);
					}
				});

				$scope.lists[sublist] = sublistData;
				let j = listCols.length;
				while (j--) {
					let list = listCols[j];
					if (list.DataAdditional == sublist) {
						let n = list.Name;
						angular.forEach($scope.data.Data[n], function (id) {
							if (sublistIds.indexOf(id) === -1) {
								$scope.data.Data[n].splice($scope.data.Data[n].indexOf(id), 1);
							}
						});

						updateSublist(list);
						break;
					}
				}
			}
		}

		if (DialogContainer) {
			$scope.inputs = [];
			$scope.lists = DialogContainer.lists;
			$scope.texts = DialogContainer.texts;

			for (let i = 0, l = DialogContainer.inputs.length; i < l; i++) {
				let inputs = DialogContainer.inputs[i];

				let groupInputs = [];
				for (let j = 0, l_ = inputs.length; j < l_; j++) {
					let input = inputs[j];

					let dt = input.ItemColumn.DataType;
					if (dt == 0 || dt == 1 || dt == 2 ||
						dt == 3 || dt == 4 || dt == 5 ||
						dt == 6 || dt == 7 || dt == 9 ||
						dt == 13 || dt == 17 || dt == 19 || dt == 10
					) {
						groupInputs.push(input);

						if (dt == 6) {
							listCols.push(input.ItemColumn);
						}
					}
				}

				if (groupInputs.length) {
					$scope.inputs.push(groupInputs);
				}
			}
			let i = listCols.length;
			while (i--) {
				updateSublist(listCols[i]);
			}
		}

		if (Content) {
			let translated = Translator.translation(Content);

			$scope.title = translated.TitleTranslation;
			$scope.message = translated.TextTranslation;
			$scope.primary = translated.PrimaryButtonTranslation;
			$scope.secondary = translated.SecondaryButtonTranslation;


			$scope.primaryButton = function () {
				ViewService.lock(StateParameters.ParentViewId);
				if ($scope.dialogType == 1) {

					let promises = [];

					if(AttachmentService.getOnDemandFiles($scope.itemId).length){

						_.each(AttachmentService.getOnDemandFiles($scope.itemId), function(f){
							promises.push(Upload.upload({
								data: f,
								url: "v1/meta/attachments"
							}));
						})

						$q.all(promises).then(function () {
							AttachmentService.clearOnDemandFiles($scope.itemId);
							$rootScope.$broadcast('RefreshAllAttachments', {});
						})

					}
					
					_.each($scope.inputs[0], function (v) {
						Data.Data[v.ItemColumn.Name] = $scope.data.Data[v.ItemColumn.Name];
					});
					let changed = ProcessService.setDataChanged(StateParameters.process, StateParameters.itemId);
					if (changed) doAction($scope.data); else doAction();
				} else {
					doAction();
				}
			};

			$scope.cancel = function () {
				MessageService.closeDialog();
			};
		} else {
			$scope.title = Translator.translate('VIEW_PLEASE_WAIT');
			doAction();
		}

		function doAction(data?) {
			$scope.actionOn = true;
			MessageService.closeDialog();
			if (data) {
				ViewService.lock(StateParameters.ViewId);
				let h = $rootScope.$on('notifyActionNeeded', function (event, parameters) {
					setTimeout(function () {
						$rootScope.$$listeners.notifyActionNeeded = [];
						ProcessTabService.doActionFinally(data, StateParameters);
					}, 500);
					$scope.$on('$destroy', h);

					//Ensure main process has correct data
					ProcessService.setData(data, StateParameters.process, StateParameters.itemId);
				});
			} else {
				ViewService.lock(StateParameters.ViewId);
				ProcessTabService.doActionFinally(undefined, StateParameters)
			}
		}

		$scope.listChange = function (field) {
			updateSublist(field);
		};

	}
})();