(function () {
	'use strict';

	angular
		.module('core.process')
		.directive('metaHelper', MetaHelper)
		.controller('MetaHelperCardController', MetaHelperCardController);

	MetaHelper.$inject = ['MessageService', 'InputService', 'Translator', 'Common', '$rootScope'];
	function MetaHelper(MessageService, InputService, Translator, Common, $rootScope) {
		return {
			scope: {
				model: '=help',
				modify: '<',
				onChange: '&?',
				onChangeParameters: '&?'
			},
			templateUrl: 'core/process/helper/helper.html',
			link: function (scope, element) {
				scope.newModel = scope.model != '' ? JSON.parse(scope.model) : undefined;
				scope.tooltip = scope.newModel != null && scope.newModel.hasOwnProperty($rootScope.clientInformation.locale_id) ? scope.newModel : null

				scope.showCard = function () {
					if (Common.isFalse(scope.modify)) {
						return;
					}

					MessageService.card({
						template: 'core/process/helper/helperCard.html',
						controller: 'MetaHelperCardController',
						locals: {
							OnChange: onChange,
							Model: scope.model,
							Modify: scope.modify
						}
					}, element, true);
				};

				function onChange(translations) {
					scope.model = translations;
					scope.tooltip = Translator.translation(scope.model);
					InputService.inputChanged(scope.onChange, scope.onChangeParameters);
				}
			}
		}
	}

	MetaHelperCardController.$inject = ['$scope', 'OnChange', 'Model', 'Modify', 'MessageService','$rootScope', 'NotificationService'];
	function MetaHelperCardController($scope, OnChange, Model, Modify, MessageService, $rootScope, NotificationService) {

		if(Model === '') Model = null;

		$scope.translations = JSON.parse(angular.copy(Model));
		$scope.modify = Modify;
		$scope.userLanguage = $rootScope.clientInformation.locale_id;
		$scope.applyHelp = function () {
			if(_.isEmpty($scope.translations[$scope.userLanguage])){
				$scope.translations = null;
				MessageService.closeCard();
			} else {
				OnChange(JSON.stringify($scope.translations));
				MessageService.closeCard();
			}
		};

		$scope.cancelEdit = function(){
			MessageService.closeCard();
		}

		if ($scope.translations === null) {
			$scope.translations = {};
		}


		$scope.languages = [];
		 NotificationService.getLanguages().then(function(b){
			 $scope.languages = b;
			 let i = $scope.languages.length;
			 while (i--) {
				 let l = $scope.languages[i].value;
				 $scope.translations[l] = typeof $scope.translations[l] === 'undefined' ?
					 { html: '', delta: undefined } : angular.fromJson($scope.translations[l]);
			 }
		});

	}
})();