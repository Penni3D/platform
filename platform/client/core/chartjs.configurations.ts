(function () {
	'use strict';
	angular
		.module('core.chartconfig', [])
		.run(ChartJsConfigurations);

	function ChartJsConfigurations() {
		// 'TODAY' line for chartjs
		// Pass parameters via options object:
		// Option 1: Just set chartOptions.currentDayLine = 2 (<-index no)
		// Option 2: Pass config object:
		//          chartOptions.currentDayLine = {
		//              index: 2,
		//              strokeStyle: '#ff0000',
		//              lineWidth: '15',
		//              lineDash: [10, 10]
		//          };

		//Disable outlabel's visuals by default
		Chart.defaults.global.plugins.outlabels.display = false;
		Chart.defaults.global.plugins.outlabels.zoomOutPercentage = 0;

		var originalLineDraw = Chart.controllers.line.prototype.draw;
		Chart.helpers.extend(Chart.controllers.line.prototype, {
		  draw: function() {
			originalLineDraw.apply(this, arguments);
			var chart = this.chart;
			if(_.has(chart.options, 'currentDayLine')) {
				var ctx = chart.chart.ctx;
				var configForcurrentDayLine = chart.options.currentDayLine;
				var index;
				var strokeStyle = '#ff0000';
				var lineWidth, lineDash;
				if(_.isObject(configForcurrentDayLine)) {
					index = configForcurrentDayLine.index;
					if(configForcurrentDayLine.strokeStyle){
						strokeStyle = configForcurrentDayLine.strokeStyle;
					}
					if(configForcurrentDayLine.lineWidth){
						lineWidth = configForcurrentDayLine.lineWidth;
					}
					if(_.isArray(configForcurrentDayLine.lineDash)) {
						lineDash = configForcurrentDayLine.lineDash;
					}
				} else if(_.isFinite(configForcurrentDayLine)) {
					index = configForcurrentDayLine;
				}
				if (index) {
				  var xaxis = chart.scales['x-axis-0'];
				  var yaxis = chart.scales['y-axis-0'];

				  ctx.save();
				  ctx.beginPath();
				  ctx.moveTo(xaxis.getPixelForValue(undefined, index), yaxis.top);
				  ctx.strokeStyle = strokeStyle;
				  if(lineWidth) {
					  ctx.lineWidth = 5;
				  }
				  if(lineDash) {
					  ctx.setLineDash(lineDash);
				  }
				  ctx.lineTo(xaxis.getPixelForValue(undefined, index), yaxis.bottom);
				  ctx.stroke();
				  ctx.restore();
				}
			}
		  }
		});
	}

})();
