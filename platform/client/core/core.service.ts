﻿/// <reference path="../types/client.d.ts" />

(function () {
	'use strict';

	angular
		.module('core')
		.service('CoreService', CoreService)
		.service('ErrorService', ErrorService)
		.factory('httpInterceptor', httpInterceptor);

	CoreService.$inject = ['ApiService', 'UserService', '$window', 'LocalStorageService', 'MessageService', 'ConnectorService'];

	function CoreService(ApiService, UserService, $window, LocalStorageService, MessageService, ConnectorService) {
		let self = this;

		let ClientInformation = ApiService.v1api('core/ClientInformation');
		let Delegates = ApiService.v1api('core/ProtectedLogin/IDelegateFor');
		let Logout = ApiService.v1api('core/logout');
		let Impersonate = ApiService.v1api('core/ProtectedLogin');
		let ItemsData = ApiService.v1api('meta/ItemsData');


		self.getClientInformation = function (): Client.ClientInformationModel {
			return ClientInformation.get();
		};

		self.getDelegates = function () {
			return Delegates.get().then(function (users) {
				return users.length ? UserService.getUsers(users) : [];
			});
		};

		self.logout = function () {
			return ConnectorService.disconnect().then(function () {
				return Logout.new().then(function () {
					LocalStorageService.clear();
					$window.location.href = $window.location.href.split('#')[0] + '?al=1';
				});
			});

		};

		self.delegate = function (users) {
			let save = {
				Data: {
					delegate_users: users
				}
			};

			return ItemsData.save(['user', UserService.getCurrentUserId()], save);
		};

		self.impersonate = function (user) {
			return Impersonate.get(['UserSessionExists', user]).then(function (userIsLoggedIn) {
				let login = function () {
					return Impersonate.get(user).then(function (permission) {
						if (permission) {
							document.location.reload();
						}
					});
				};

				userIsLoggedIn ? MessageService.confirm('LAYOUT_IMPERSONATE_CHECK', login) : login();
			});
		};
	}

	ErrorService.$inject = ['ConnectorService', 'MessageService', 'Translator'];

	function ErrorService(ConnectorService, MessageService, Translator) {
		let self = this;

		self.clientError = function (errorObject, sender) {
			let errorTitle = errorObject.message;
			let errorStack = errorObject.stack;

			self.sendError(errorTitle, errorStack, sender);
			self.logError('Client (' + sender + ')', errorTitle, errorStack);
		};

		self.customServerError = function (errorObject) {
			let errorTitle = Translator.translate(errorObject.Variable);
			let errorDescription = Translator.translate(errorObject.Info.description);
			let id = errorObject.Info && errorObject.Info.logId ? ' -- Log Id: ' + errorObject.Info.logId : '';
			self.logError('Server (Other)', errorTitle, errorDescription + ' ' + id);
		};

		self.argumentServerError = function (errorObject) {
			let errorTitle = Translator.translate(errorObject.Variable);
			let errorDescription = Translator.translate(errorObject.Info.description);
			let id = errorObject.Info && errorObject.Info.logId ? ' -- Log Id: ' + errorObject.Info.logId : '';
			self.logError('Server (Args)', errorTitle, errorDescription + ' ' + id);
		};

		self.uniqueServerError = function (errorObject) {
			let errorTitle = Translator.translate("UNIQUE_COLUMN_ERROR_TITLE");
			let errorDescription = Translator.translate("UNIQUE_COLUMN_ERROR");
			MessageService.msgBox(errorDescription + "'" + errorObject.Data.col + "'", errorTitle);
		};

		self.permissionsServerError = function (errorObject) {
			if (errorObject.SubType == 'DELETE') {
				MessageService.msgBox(Translator.translate("ERROR_UNABLE_TO_REMOVE"));
			} else {
				let errorTitle = Translator.translate(errorObject.Variable);
				let errorDescription = Translator.translate(errorObject.Info.description);
				let processName = errorObject.Data && errorObject.Data.process ? Translator.translate(errorObject.Data.process) : '';
				let id = errorObject.Info && errorObject.Info.logId ? ' -- Log Id: ' + errorObject.Info.logId : '';
				self.logError('Server (Permissions)', errorTitle, errorDescription + ' ' + processName + ' ' + id);
			}
		};

		self.serverError = function (errorObject) {
			let errorTitle = Translator.translate(errorObject.Variable);
			let errorDescription = Translator.translate(errorObject.Info.description);
			let id = errorObject.Info && errorObject.Info.logId ? ' -- Log Id: ' + errorObject.Info.logId : '';
			self.logError('Server (Unhandled)', errorTitle, errorDescription + ' ' + id);
		};

		
		let errorQueue = [];
		
		self.errorCount = function() {
			return errorQueue.length;
		}
		
		self.getErrors = function() {
			return errorQueue;
		}
		
		self.clearError = function(i) {
			errorQueue.splice(i,1);
		}
		
		self.logError = function (source, title, stack) {
			errorQueue.push({
				source: source,
				title: title,
				stack: stack
			});
		};

		self.sendError = function (title, stack, sender) {
			if (!title) title = '?';
			if (!stack) stack = '';
			ConnectorService.invoke('DiagnosticsPutEvent', arguments);
		};
	}

	httpInterceptor.$inject = ['$q', '$window', '$location', '$injector'];

	function httpInterceptor($q, $window, $location, $injector) {
		let requestCount = 0;
		return {
			//REQUEST
			request: function (config) {
				//Success
				requestCount += 1;
				return config || $q.when(config);
			},
			requestError: function (rejection) {
				//Failure
				return $q.reject(rejection);
			},
			// RESPONSE
			response: function (response) {
				//Success
				requestCount -= 1;

				return response || $q.when(response);
			},
			responseError: function (rejection) {
				//Failure
				requestCount -= 1;
				if (rejection.status === 401) {
					//Unauthorised
					$location.search('e', 401);
					$location.search('al', true);

					$window.location.reload();
				} else if (rejection.status === 400) {
					let ErrorService = $injector.get('ErrorService');
					if (rejection.data.Type === 'ARGUMENTS') {
						ErrorService.argumentServerError(rejection.data);
					} else if (rejection.data.Type === 'PERMISSIONS') {
						ErrorService.permissionsServerError(rejection.data);
					} else if (rejection.data.Type === 'UNIQUE') {
						ErrorService.uniqueServerError(rejection.data);
					} else {
						ErrorService.customServerError(rejection.data);
					}
				} else if (rejection.status === 404) {
					let ErrorService = $injector.get('ErrorService');
					ErrorService.clientError({message: "End point cannot be found", stack: ''}, 'AngularJs');
				} else if (rejection.status === 500) {
					let ErrorService = $injector.get('ErrorService');
					ErrorService.serverError(rejection.data);
				}

				return $q.reject(rejection);
			}
		};
	}
})
();
