(function () {
	'use strict';

	angular
		.module('core.dialogs')
		.config(NewProcessConfig);

	NewProcessConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
	function NewProcessConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addRouter('new.process', {
			route: function (StateParameters, Views, ViewService, ProcessService) {
				let process = StateParameters.process;
				let v = 'new.' + process;
				if (!(typeof Views.getView(v) === 'undefined' && typeof Views.getDialog(v) === 'undefined'))
					return v;

				return 'no_view'
			}
		});

		ViewConfiguratorProvider.addConfiguration('new.process', {
		    onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				},
				DateColumns: function (ProcessService, StateParameters) {
					return ProcessService.getColumns(StateParameters.process).then(function (columns) {
						let l = [];
						angular.forEach(columns, function (c) {
							if (c.DataType == 3 || c.DataType == 13) {
								l.push(c);
							}
						});

						return l;
					});
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				},
				Backgrounds: function (Backgrounds) {
					return Backgrounds.getBackgrounds();
				},
				Unions: function (InstanceUnionsService) {
				    return InstanceUnionsService.getUnions();
                }
			},
            onChange: function (p, params, resolves) {
                if (p === 'modelPortfolioId') {
                    let i = resolves.Portfolios.length;
                    while (i--) {
                        let portfolio = resolves.Portfolios[i];
                        if (portfolio.ProcessPortfolioId == params.modelPortfolioId) {
                            params.modelProcess = portfolio.Process;
                            break;
                        }
                    }
                }
            },
			tabs: {
				default: {
					background: function (resolves) {
						return {
							type: 'select',
							label: 'BACKGROUND',
							options: resolves.Backgrounds,
							optionValue: 'background'
						};
					},
					toast: function () {
						return {
							type: 'translation',
							label: 'TOAST'
						};
					},
					process: function (resolves) {
						return {
							type: 'select',
							label: 'PROCESS',
							options: resolves.Processes,
							optionName: 'LanguageTranslation',
							optionValue: 'ProcessName'
						};
					}
				},
				model: {
					modelPortfolioId: function (resolves) {
						return {
							type: 'select',
							label: 'MODEL_PORTFOLIO_ID',
							options: resolves.Portfolios,
							optionName: 'LanguageTranslation',
							optionValue: 'ProcessPortfolioId'
						};
					},
					modelStartDate: function (resolves) {
						return {
							type: 'select',
							options: resolves.DateColumns,
							label: 'MODEL_START_DATE',
							optionName: 'LanguageTranslation',
							optionValue: 'Name'
						};
					},
					modelEndDate: function (resolves) {
						return {
							type: 'select',
							label: 'MODEL_END_DATE',
							options: resolves.DateColumns,
							optionName: 'LanguageTranslation',
							optionValue: 'Name'
						};
					},
					modelItemColumn: function () {
						return {
							type: 'string',
							label: 'MODEL_ITEM_COLUMN'
						};
					},
                    modelDateCopyType: function ()  {
                        return {
                            type: 'select',
                            label: 'MODEL_DATE_COPY_TYPE',
							options: [{name: 'Default', value: 0}, {name: 'Helsinki', value: 1}],
                            optionName: 'name',
                            optionValue: 'value'
                        };
                    },
                    modelCopyUnion: function (resolves) {
                        return {
                            type: 'select',
                            label: 'MODEL_COPY_UNION',
							options: resolves.Unions,
                            optionName: 'LanguageTranslation',
                            optionValue: 'InstanceUnionId'
                        };
                    },
                    modelCopyInputType: function ()  {
                        return {
                            type: 'select',
                            label: 'MODEL_COPY_INPUT_TYPE',
							options: [{name: 'Table', value: 0}, {name: 'Process', value: 1}],
                            optionName: 'name',
                            optionValue: 'value'
                        };
                    }
				}
			}
		});
	}

})();