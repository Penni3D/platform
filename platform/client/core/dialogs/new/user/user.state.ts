﻿(function () {
	'use strict';

	angular
		.module('core.dialogs')
		.config(NewUserConfig)
		.controller('NewUserController', NewUserController);

	NewUserConfig.$inject = ['ViewsProvider'];

	function NewUserConfig(ViewsProvider) {
		ViewsProvider.addDialog('new.user', {
			controller: 'NewUserController',
			template: 'core/dialogs/new/user/user.html',
			resolve: {
				UniqueColumn: function (ProcessService) {
					return ProcessService.getColumn('user').then(function (columns) {
						let i = columns.length;
						while (i--) {
							if (columns[i].Name == 'login') {
								return columns[i];
							}
						}
					});
				}
			}
		});
	}

	NewUserController.$inject = [
		'MessageService',
		'$scope',
		'StateParameters',
		'ProcessService',
		'$rootScope',
		'UniqueService',
		'CalendarService',
		'UniqueColumn',
		'Common',
		'ViewService',
		'Translator'
	];

	function NewUserController(
		MessageService,
		$scope,
		StateParameters,
		ProcessService,
		$rootScope,
		UniqueService,
		CalendarService,
		UniqueColumn,
		Common,
		ViewService,
		Translator
	) {
		let uniqueUsername = false;
		let validPassword = true;
		$scope.canCreate = false;
		$scope.pid = UniqueService.get('user-password', true);
		$scope.rid = UniqueService.get('user-password', true);
		$scope.useSSOOnly = true;
		
		$scope.activeOptions = [
			{
				name: 'ACTIVE_USER',
				value: 1
			},
			{
				name: 'INACTIVE_USER',
				value: 0
			}
		];

		$scope.user = {
			base_calendar_id: 1,
			date_format: 'dd.mm.yy',
			first_day_of_week: 1,
			timezone: 'FLE Standard Time_120',
			locale_id: $rootScope.clientInformation.locale_id,
			active: 1,
			password: ""
		};
		$scope.background = StateParameters.background;
		$scope.actionText = 'CREATE';
		$scope.create = function () {
			$scope.canCreate = false;
			if ($scope.useSSOOnly) $scope.user.password = "";
			ProcessService.newItem('user', {Data: $scope.user}).then(function (itemId) {
				ProcessService.getItemData(StateParameters.process, itemId, true).then(function (item) {
					let p = {
						params: {
							process: StateParameters.process,
							itemId: itemId,
							tabId: undefined
						}
					};

					let state = 'process.tab';

					if (item.Data.current_state) {
						if (_.startsWith(item.Data.current_state, 'tab')) {
							p.params.tabId = item.Data.current_state.split('_')[1];
						} else {
							state = item.Data.current_state;
						}
					}

					MessageService.closeDialog();
					MessageService.toast('USER_CREATED', 'confirm');
					ViewService.view(state, p);
				});
			});
		};
		
		$scope.getValidation = function() {
			if ($scope.user.login && !uniqueUsername) {
				$scope.validated = false;
				return Translator.translation('USER_INVALID_USERNAME');	
			}
			
			if ($scope.useSSOOnly) {
				$scope.validated = true;
				return "";
			} 
			
			let r = "";
			if (r == "" && !$scope.user.password.length) r = "PASSWORD_INVALID_EMPTY";
			if (r == "" && $scope.user.password !== $scope.rPassword) r =  "PASSWORD_INVALID_SAME";
			if (r == "") r = Common.isValidPassword($scope.user.password, false, true);
			$scope.validated = r == "";
			return Translator.translation(r);
		}
		$scope.passwordChanged = function () {
			validPassword = !$scope.validated;
			$scope.fieldChanged();
		};
		$scope.validated = false;
		$scope.fieldChanged = function () {
			$scope.actionText = 'CREATE';
			$scope.canCreate = $scope.userForm.$valid && validPassword && uniqueUsername;
		};

		let tick;
		$scope.usernameChanged = function () {
			$scope.actionText = 'CREATE';
			if ($scope.user.login) {
				clearTimeout(tick);
				tick = setTimeout(function () {
					ProcessService.isUnique('user', UniqueColumn.ItemColumnId, $scope.user.login).then(function (isUnique) {
						uniqueUsername = isUnique;
						$scope.fieldChanged();
					});
				}, 1000);
			}
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};
	}
})();
