﻿(function () {
	'use strict';

	angular
		.module('core.dialogs')
		.config(NewDefaultConfig)
		.controller('NewDefaultController', NewDefaultController);

	NewDefaultConfig.$inject = ['ViewsProvider'];

	function NewDefaultConfig(ViewsProvider) {
		ViewsProvider.addDialog('new.default', {
			template: 'core/dialogs/new/default/default.html',
			controller: 'NewDefaultController',
			resolve: {
				DialogContainers: function (ProcessService, StateParameters) {
					if (StateParameters.hasOwnProperty('containerId')) {
						return ProcessService.getDialogContainersWithSpecificContainer(StateParameters.process, StateParameters.containerId);
					}
					return ProcessService.getDialogContainers(StateParameters.process);
				},
				CopyInputType: function (StateParameters) {
					return angular.isDefined(StateParameters.modelCopyInputType) ?
						StateParameters.modelCopyInputType : 0
				}
			},
			afterResolve: {
				Rows: function ($q, StateParameters, PortfolioService, CopyInputType) {
					if (StateParameters.modelPortfolioId &&
						StateParameters.modelProcess &&
						CopyInputType === 0
					) {
						return PortfolioService
							.getPortfolioData(
								StateParameters.modelProcess,
								StateParameters.modelPortfolioId,
								{local: true},
								{limit: 0})
							.then(function (portfolioData) {
								return portfolioData.rowdata;
							});
					} else {
						return [];
					}
				},
				PortfolioColumns: function (PortfolioService, StateParameters, CopyInputType) {
					if (StateParameters.modelPortfolioId &&
						StateParameters.modelProcess &&
						!StateParameters.modelItemColumn &&
						CopyInputType === 0
					) {
						return PortfolioService.getPortfolioColumns(
							StateParameters.modelProcess,
							StateParameters.modelPortfolioId
						);
					} else {
						return [];
					}
				}
			}
		});
	}

	NewDefaultController.$inject = [
		'$scope',
		'ProcessService',
		'ViewService',
		'StateParameters',
		'MessageService',
		'DialogContainers',
		'Rows',
		'PortfolioColumns',
		'Configuration',
		'$window',
		'$timeout',
		'CopyInputType',
		'Translator',
		'Backgrounds'
	];

	function NewDefaultController(
		$scope,
		ProcessService,
		ViewService,
		StateParameters,
		MessageService,
		DialogContainers,
		Rows,
		PortfolioColumns,
		Configuration,
		$window,
		$timeout,
		CopyInputType,
		Translator,
		Backgrounds
	) {

		let isMobile = $window.innerWidth < 1024;
		let copyFromId = 0;
		let listCols = [];
		let toastMessage = Translator.translation(StateParameters.toast);

		$scope.copyFromId = [];

		if (StateParameters.modelProcess) {
			$scope.selections = {};
			$scope.rows = Rows;
			$scope.copyModel = true;
			$scope.copyModelFromPortfolio =
				StateParameters.modelPortfolioId &&
				StateParameters.modelProcess &&
				StateParameters.modelItemColumn;

			$scope.modelProcess = StateParameters.modelProcess;
			$scope.modelPortfolioId = typeof StateParameters.modelPortfolioId === 'undefined' ?
				0 : StateParameters.modelPortfolioId;

			$scope.modelCopyInputType = CopyInputType;
			$scope.copyModelEndDate = StateParameters.modelDateCopyType !== 0;

			$scope.modelChange = function (itemId) {
				let i = $scope.rows.length;
				while (i--) {
					let r = $scope.rows[i].item_id;

					if (itemId != r) {
						$scope.selections[r] = false;
					}
				}

				copyFromId = $scope.selections[itemId] ? itemId : 0;
			};
		}

		$scope.creating = false;
		$scope.background = StateParameters.background ? StateParameters.background : Backgrounds.getRandomBackground().background;
		$scope.portfolioColumns = PortfolioColumns;
		$scope.process = StateParameters.process;
		$scope.openerItemId = StateParameters.openerItemId;
		$scope.index = 0;
		$scope.steps = [];
		$scope.data = {
			Data: {
				copy_date: new Date(),
				copy_to_start_date: StateParameters.modelStartDate,
				copy_to_end_date: StateParameters.modelEndDate,
				CopyModelProcess: StateParameters.modelProcess,
				CopyModelItemColumn: StateParameters.modelItemColumn,
				CopyModelInstanceUnionId: StateParameters.modelCopyUnion
			}
		};

		angular.extend($scope.data.Data, StateParameters.overwriteData);

		if ($scope.copyModelEndDate) {
			$scope.data.copy_date_2 = new Date(new Date().setMonth(new Date().getMonth() + 1));
		}

		angular.forEach(StateParameters.Data, function (key, value) {
			$scope.data.Data[value] = key;
		});

		if (angular.isDefined(DialogContainers)) {
			$scope.inputs = [];
			$scope.lists = DialogContainers.lists;
			$scope.texts = DialogContainers.texts;

			for (let i = 0, l = DialogContainers.inputs.length; i < l; i++) {
				let inputs = DialogContainers.inputs[i];

				let groupInputs = [];
				for (let j = 0, l_ = inputs.length; j < l_; j++) {
					let input = inputs[j];

					let dt = input.ItemColumn.DataType;
					if (dt == 0 || dt == 1 || dt == 2 ||
						dt == 3 || dt == 4 || dt == 5 ||
						dt == 6 || dt == 7 || dt == 9 ||
						dt == 13 || dt == 17 || dt == 19
					) {
						groupInputs.push(input);

						if (dt == 6) {
							listCols.push(input.ItemColumn);
						}
					}
				}

				if (groupInputs.length) {
					$scope.inputs.push(groupInputs);
				}
			}

			let i = listCols.length;
			while (i--) {
				updateSublist(listCols[i]);
			}

			if ($scope.inputs.length) {
				generateSteps();
				updateSteps();
			} else {
				createProcess();
			}
		}

		$scope.nextPage = function () {
			if ($scope.index == $scope.inputs.length - 1) {
				createProcess();
			} else {
				$scope.index++;
				setNextButtonText();
				updateSteps();
			}
		};

		$scope.previousPage = function () {
			if ($scope.index > 0) {
				$scope.index--;
			}

			setNextButtonText();
			updateSteps();
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.listChange = function (field) {
			updateSublist(field);
		};

		$timeout(function () {
			setNextButtonText();
		});

		$scope.isValidForm = function () {
			return $scope.newProcessForm.$valid;
		};

		function setNextButtonText() {
			$scope.nextButtonText = $scope.index === $scope.inputs.length - 1 ?
				toastMessage ? 'SUBMIT' : 'CREATE' : 'NEXT';
		}

		function createProcess() {
			if (copyFromId) {
				$scope.data.Data.copy_from_id = copyFromId;
			} else if ($scope.copyFromId.length) {
				$scope.data.Data.copy_from_id = $scope.copyFromId[0];
			}

			$scope.creating = true;

			ProcessService.newItem(StateParameters.process, $scope.data, StateParameters.actionId).then(function (itemId) {
				if (toastMessage) {
					MessageService.closeDialog();
					MessageService.toast(toastMessage, 'confirm');
				} else {
					ProcessService.getItemData(StateParameters.process, itemId, true).then(function (item) {
						let p = {
							params: {
								process: StateParameters.process,
								itemId: itemId,
								tabId: undefined
							}
						};

						let state = 'process.tab';

						if (item.Data.current_state) {
							if (_.startsWith(item.Data.current_state, 'tab')) {
								p.params.tabId = item.Data.current_state.split('_')[1];
							} else {
								state = item.Data.current_state;
							}
						}

						MessageService.closeDialog();
						ViewService.view(state, p);
					});
				}
			});
		}

		function updateSublist(field) {
			let sl = DialogContainers.sublists[field.DataAdditional];
			let i = sl.length;
			while (i--) {
				let sublist = sl[i];
				let sld = DialogContainers.sublistsData[sublist];
				if (typeof sld === 'undefined') {
					continue;
				}

				let sublistData = [];
				let sublistIds = [];

				angular.forEach($scope.data.Data[field.Name], function (parentId) {
					for (let j = 0, l = sld[parentId].length; j < l; j++) {
						let sublistItem = sld[parentId][j];

						sublistData.push(sublistItem);
						sublistIds.push(sublistItem.item_id);
					}
				});

				$scope.lists[sublist] = sublistData;
				let j = listCols.length;
				while (j--) {
					let list = listCols[j];
					if (list.DataAdditional == sublist) {
						let n = list.Name;
						angular.forEach($scope.data.Data[n], function (id) {
							if (sublistIds.indexOf(id) === -1) {
								$scope.data.Data[n].splice($scope.data.Data[n].indexOf(id), 1);
							}
						});

						updateSublist(list);
						break;
					}
				}
			}
		}

		function generateSteps() {
			let count = $scope.inputs.length;

			if (isMobile && count > 3) {
				count = 3;
			} else if (count > 5) {
				count = 5;
			}

			for (let i = 0; i < count; i++) {
				$scope.steps.push({});
			}
		}

		//First Slot, Second Slot, Main Slot, Fourth Slot, Final Slot
		function updateSteps() {
			let i = $scope.steps.length;
			while (i--) {
				let s = $scope.steps[i];
				s.inactive = true;
				s.decision = false;
				s.hidden = false;
				s.index = i + 1;
			}

			let inputs = $scope.inputs.length;
			let steps = $scope.steps.length;

			if (inputs > steps) {
				let firstSlot = $scope.steps[0];
				let secondSlot = $scope.steps[1];
				let mainSlot = $scope.steps[2];

				if (isMobile) {
					mainSlot.index = inputs;

					if (inputs - $scope.index < 4) {
						let firstOfLast = inputs - 3;
						firstSlot.inactive = false;
						firstSlot.hidden = false;
						firstSlot.decision = $scope.index === firstOfLast;
						firstSlot.index = firstOfLast + 1;

						secondSlot.inactive = $scope.index < firstOfLast + 1;
						secondSlot.decision = $scope.index === firstOfLast + 1;
						secondSlot.index = firstOfLast + 2;

						mainSlot.inactive = $scope.index < firstOfLast + 2;
						mainSlot.decision = $scope.index === firstOfLast + 2;
					} else {
						firstSlot.inactive = false;
						firstSlot.hidden = false;
						firstSlot.decision = true;
						firstSlot.index = $scope.index + 1;

						secondSlot.hidden = true;
					}
				} else {
					let fourthSlot = $scope.steps[3];
					let lastSlot = $scope.steps[4];

					firstSlot.decision = $scope.index === 0;
					firstSlot.inactive = false;

					lastSlot.decision = $scope.index === inputs - 1;
					lastSlot.inactive = !lastSlot.decision;
					lastSlot.index = inputs;

					if ($scope.index < 3) {
						//First steps
						secondSlot.inactive = $scope.index < 1;
						secondSlot.decision = $scope.index === 1;

						mainSlot.inactive = $scope.index < 2;
						mainSlot.decision = $scope.index === 2;

						fourthSlot.hidden = true;
					} else if (inputs - $scope.index < 4) {
						//Last steps
						let firstOfLast = inputs - 3;
						secondSlot.inactive = false;
						secondSlot.hidden = true;

						mainSlot.inactive = false;
						mainSlot.decision = $scope.index === firstOfLast;
						mainSlot.index = firstOfLast + 1;

						fourthSlot.inactive = $scope.index < firstOfLast + 1;
						fourthSlot.decision = $scope.index === firstOfLast + 1;
						fourthSlot.index = firstOfLast + 2;
					} else {
						//Somewhere in between
						secondSlot.inactive = false;
						secondSlot.hidden = true;

						mainSlot.inactive = false;
						mainSlot.decision = true;
						mainSlot.index = $scope.index + 1;

						fourthSlot.hidden = true;
					}
				}
			} else {
				let i = steps;
				while (i--) {
					let s = $scope.steps[i];
					s.inactive = $scope.index < i;
					s.decision = $scope.index === i;
				}
			}
		}
	}
})();