﻿(function () {
	'use strict';

	angular
		.module('core.process')
		.directive('portfolioFilters', PortfolioFiltersDirective);

	PortfolioFiltersDirective.$inject = [
		'Common',
		'PortfolioService',
		'ViewService',
		'Translator',
		'ProcessService',
		'$rootScope',
		'TagsService',
		'MessageService',
		'ApiService',
		'SelectorService'
	];

	function PortfolioFiltersDirective(
		Common,
		PortfolioService,
		ViewService,
		Translator,
		ProcessService,
		$rootScope,
		TagsService,
		MessageService,
		ApiService,
		SelectorService
	) {
		return {
			restrict: 'E',
			templateUrl: 'core/portfolios/filter/portfolioFilters.html',
			scope: {
				portfolioColumns: '<',
				portfolioListFilterValues: '=',
				portfolioColumnGroups: '<?',
				onChange: '&?',
				onClickFilter: '&?',
				isVisible: '=',
				reloadNavigation: '&?',
				processProgress: '<?',
				hideProgress: '<?',
				hideFilterButton: '<?',
				hideSaveButton: '<?',
				hideFilters: '<?',
				hideColumns: '<?',
				hideIfOneTab: '<?',
				minimal: '<?',
				showArchive: '<?',
				onSave: '&?',
				excluded: '='
			},
			link: function (scope, element, attrs) {
				scope.timeMachineEnabled = false;
				scope.savedTimeMachines = [];

				let saveTimeMachine = function (params) {
					PortfolioService.addTimeMachineSave(attrs.process, params).then(function (s) {
						s.Name = s.Name + ' (' + moment(s.Date).format("DD/MM/YYYY") + ')';
						scope.savedTimeMachines.push(s);
					});
				};

				scope.newTimeMachine = function () {
					let newData = {Name: '', Process: attrs.process, Date: scope.filters.archive};
					let save = function () {
						saveTimeMachine(newData);
						newData = {Name: '', Process: attrs.process, Date: scope.filters.archive};
					};

					let content = {
						buttons: [{text: 'DISCARD', modify: 'cancel'}, {
							type: 'primary',
							onClick: save,
							text: 'SAVE',
							modify: 'create'
						}],
						inputs: [{type: 'string', label: 'TIMEMACHINE_NAME', model: 'Name'}],
						title: 'ADD_NEW_TIMEMACHINE'
					};

					MessageService.dialog(content, newData);
				};

				let portfolioId = Number(attrs.portfolioId);

				let portfolioSettings = PortfolioService.getActiveSettings(portfolioId);
				let sublisted = [];
				let process = attrs.process;
				let columnGroup = {};
				scope.sortTranslations = {};


				for (let i = 0, l = scope.portfolioColumns.length; i < l; i++) {
					let pc = scope.portfolioColumns[i];
					let ic = pc.ItemColumn;

					let t = Translator.translation(pc.ShortNameTranslation);
					ic.translation = t ? t : Translator.translation(ic.LanguageTranslation);

					if (ic.DataType == 5) {
						if (typeof ic.DataAdditional2 === 'string' && ic.DataAdditional2.length) {
							let r = angular.fromJson(ic.DataAdditional2);

							if (typeof r === 'object' && r) {
								ic.portfolioId = r.portfolioId;
								ic.preload = r.showAll;
							}
						}
					}

					if(ic.DataType == 16 && ic.SubDataType == 5){
						if (typeof ic.SubDataAdditional2 === 'string' && ic.SubDataAdditional2.length) {
							let r = angular.fromJson(ic.SubDataAdditional2);
							if (typeof r === 'object' && r) {
								let selector = SelectorService.getProcessSelector(ic.SubDataAdditional);
								ic.portfolioId = selector.ProcessPortfolioId;
								ic.preload = r.showAll;
							}
						}
					}

					scope.sortTranslations[ic.Name] = ic.translation;
				}

				scope.tags = TagsService.get(process);
				scope.options = {};
				scope.storedOptions = {};
				scope.filters = portfolioSettings.filters;
				scope.columns = _.map(angular.copy(scope.portfolioColumns), function (pc) {
					return pc.ItemColumn;
				});

				scope.groupedColumns = [];



				scope.selectedColumns = {};
				scope.selectedGroupedColumns = {};

				scope.filterCols = [];
				scope.selects = [];
				scope.combinedPhases = [];
				scope.userListLanguage = 'list_item_' + $rootScope.clientInformation.locale_id.toLowerCase().replace('-', '_');

				_.remove(scope.filters.sort, function (s) {
					return !_.find(scope.columns, function (c) {
						if (s.column === c.Name) {
							return true;
						}
					});
				});

				if (scope.portfolioColumnGroups) {
					for (let i = 0, l = scope.portfolioColumnGroups.length; i < l; i++) {
						let g = scope.portfolioColumnGroups[i];
						scope.portfolioColumnGroups[i].columns = [];

						for (let j = 0, l_ = scope.portfolioColumns.length; j < l_; j++) {
							let c = scope.portfolioColumns[j];

							if (g.ProcessPortfolioColumnGroupId === c.ProcessPortfolioColumnGroupId) {
								columnGroup[c.ItemColumn.Name] = g.ProcessPortfolioColumnGroupId;
								g.columns.push(c);
							}
						}

						if (g.columns.length) {
							for (let j = 0, l_ = g.columns.length; j < l_; j++) {
								for (let k = 0, l__ = scope.columns.length; k < l__; k++) {
									if (g.columns[j].ItemColumn.Name === scope.columns[k].Name) {
										scope.columns.splice(k, 1);
										break;
									}
								}
							}

							scope.selectedGroupedColumns[g.ProcessPortfolioColumnGroupId] = [];
							scope.groupedColumns.push(g);
						}
					}
				}

				if (scope.hideProgress) {
					portfolioSettings.filters.phase = [];
				} else {
					let previousPhase;
					angular.forEach(scope.processProgress, function (phase) {

						if (previousPhase &&
							Translator.translation(phase.LanguageTranslation) === previousPhase.translation
						) {
							previousPhase.combined.push(phase.ProcessGroupId)
						} else {
							let p = {
								translation: Translator.translation(phase.LanguageTranslation),
								type: phase.Type,
								processGroupId: phase.ProcessGroupId,
								combined: [phase.ProcessGroupId]
							};

							previousPhase = p;
							scope.combinedPhases.push(p);
						}
					});




					let findSelectedPhases = function() {

						_.each(scope.combinedPhases, function(p){
							p.filtered = false;
						})

						let i = scope.combinedPhases.length;
						while (i--) {
							let phase = scope.combinedPhases[i];
							if (Array.isArray(scope.filters.phase) && scope.filters.phase.length) {
								let j = scope.filters.phase.length;
								while (j--) {
									let k = phase.combined.length;
									while (k--) {
										if (scope.filters.phase[j] == phase.combined[k]) {
											phase.filtered = true;
											break;
										}
									}

									if (k !== -1) {
										break;
									}
								}
							} else {
								phase.filtered = true;
							}
						}
					}

					findSelectedPhases();
					$rootScope.$on('notifyFilterPhases', function (event, parameters) {
						findSelectedPhases();
					});

					scope.phaseChange = function (phase) {
						if (typeof scope.filters.phase === 'undefined' || scope.filters.phase.length) {
							phase.filtered = !phase.filtered;
						} else {
							let i = scope.combinedPhases.length;
							while (i--) {
								let g = scope.combinedPhases[i];
								g.filtered = g.combined[0] == phase.combined[0];
							}
						}

						let i = scope.combinedPhases.length;
						while (i--) {
							if (!scope.combinedPhases[i].filtered) {
								break;
							}
						}

						scope.filters.phase = [];
						if (i !== -1) {
							let i = scope.combinedPhases.length;
							while (i--) {
								let phase = scope.combinedPhases[i];

								if (phase.filtered) {
									let j = phase.combined.length;
									while (j--) {
										scope.filters.phase.push(phase.combined[j]);
									}
								}
							}
						}

						if (!scope.filters.phase.length) {
							let i = scope.combinedPhases.length;
							while (i--) {
								scope.combinedPhases[i].filtered = true;
							}
						}

						scope.tick();
					};
				}


				scope.filterCols.push({
					col: undefined,
					type: 'text'
				});
				if (scope.showArchive) {
					scope.filterCols.push({
						col: undefined,
						type: 'timemachine'
					});
					scope.filterCols.push({
						col: undefined,
						type: 'timemachine_list'
					});
					scope.timeMachineEnabled = true;
				}

				if (scope.timeMachineEnabled) {
					PortfolioService.getSavedTimeMachines(attrs.process).then(function (timeMachines) {
						scope.savedTimeMachines.push({
							Date: undefined,
							Name: "Today",
						});
						_.each(timeMachines, function (machine) {
							machine.Name = machine.Name + ' (' + moment(machine.Date).format("DD/MM/YYYY") + ')';
							scope.savedTimeMachines.push(machine);
						});
					});
				}

				for (let i = 0, l = scope.portfolioColumns.length; i < l; i++) {
					let column = scope.portfolioColumns[i];
					let ic = column.ItemColumn;
					if (column.UseInFilter) {
						let dt = Common.getRelevantDataType(ic);

						ic.dataType = dt;
						ic.dataAdditional = Common.getRelevantDataAdditional(ic);

						if (dt == 5 || dt == 6 || dt == 19) {
							scope.filterCols.push({
								col: ic,
								type: 'select'
							});
							scope.selects.push(ic);
						} else if (dt == 1 || dt == 2 || dt == 14) {
							scope.filterCols.push({
								col: ic,
								type: 'number'
							});
						} else if (dt == 3) {
							scope.filterCols.push({
								col: ic,
								type: 'date'
							});
						} else if (dt == 13){
							scope.filterCols.push({
								col: ic,
								type: 'date_time'
							});
						}
					}
				}

				updateOptions();

				angular.forEach(scope.selects, function (ic) {
					if (sublisted.indexOf(ic.ItemColumnId) === -1) {
						if (ic.dataType == 19) {
							scope.options[ic.ItemColumnId] = [];
						} else {
							if (ic.DataType == 16) {
								ApiService.v1api('settings/RegisteredSubQueries/subqueryValues').get([ic.SubDataAdditional, ic.DataAdditional, ic.ItemColumnId]).then(function (items) {

									scope.options[ic.ItemColumnId] = items;

									let fl = scope.portfolioListFilterValues && scope.portfolioListFilterValues[ic.Name + "_str"];
									if (fl) {
										scope.storedOptions[ic.ItemColumnId] = items;
										updateFilterValueOptions();
									} else {
										scope.options[ic.ItemColumnId] = items;
									}
								});
							} else {
								ProcessService.getListItems(ic.dataAdditional).then(function (items) {
									//Remove Items Not in Use or where the user group setting does not match user's user groups
									let listItemsToRemove = [];
									_.each(items, function (listItem) {
										if (listItem.in_use == "0" ||
											(listItem.user_group && listItem.user_group.length > 0 &&
												!$rootScope.clientInformation.user_groups.some(
													r => listItem.user_group.includes(r)
												))) {
											listItemsToRemove.push(listItem.item_id);
										}
									});

									_.each(listItemsToRemove, function (removeId) {
										if (removeId != scope.model) {
											items.splice(_.findIndex(items, function (o) {
												return o.item_id == removeId;
											}), 1);
										}
									});
									scope.options[ic.ItemColumnId] = items;
								});
							}
						}
					}
				});

				scope.$watch('excluded', function() {
					if (scope.excluded){
						if(!scope.filters.hasOwnProperty('excludeItemIds'))scope.filters['excludeItemIds'] = [];
						_.each(scope.excluded, function(id){
							scope.filters.excludeItemIds.push(id);
						})
						scope.instantTick();
					}
				});
				
				scope.$watch('portfolioListFilterValues', function () {
					updateFilterValueOptions();
				});

				scope.$watch(function () {
					return portfolioSettings.filters.select;
				}, function (newOptions, oldOptions) {
					if (newOptions === oldOptions) return;
					updateOptions();
				}, true);

				scope.columnChange = function () {

					scope.filters.columns = [];

					for (let i = 0, l = scope.portfolioColumns.length; i < l; i++) {
						let name = scope.portfolioColumns[i].ItemColumn.Name;
						if (scope.selectedColumns[name]) {
							continue;
						}

						if (scope.portfolioColumnGroups) {
							let j = scope.portfolioColumnGroups.length;

							while (j--) {
								let groupId = scope.portfolioColumnGroups[j].ProcessPortfolioColumnGroupId;
								let group = scope.selectedGroupedColumns[groupId];
								if (typeof group != 'undefined' && group.indexOf(name) !== -1) {
									break;
								}
							}

							if (j === -1) {
								scope.filters.columns.push(name);
							}
						} else {
							scope.filters.columns.push(name);
						}
					}

					PortfolioService.saveActiveSettings(portfolioId);
				};

				scope.saveFilters = function () {
					if (scope.onSave) {
						scope.onSave();
					} else {
						ViewService.view('configure.portfolioSimple', {
							params: {
								NavigationMenuId: attrs.navigationMenuId,
								portfolioId: portfolioId,
								isNew: true,
								process: process
							},
							locals: {
								CurrentSettings: {
									Name: Translator.translate('PORTFOLIO_NEW'),
									Value: portfolioSettings,
								},
								ReloadNavigation: function () {
									scope.reloadNavigation();
								},
								RefreshParent: function(){
									return;
								}
							}
						});
					}
				};

				scope.addOrder = function (event) {
					let orderableColumns = _.map(scope.portfolioColumns, function (pc) {
						let ic = pc.ItemColumn;

						return {
							column: ic.Name,
							name: ic.translation,
							dataType: ic.DataType,
							onClick: function () {
								scope.filters.sort.push({
									column: ic.Name,
									order: 'asc'
								});

								scope.instantTick();
							}
						};
					});

					_.remove(orderableColumns, function (c) {
						return _.find(scope.filters.sort, function (s) {
							return c.column === s.column || c.dataType == 60;
						});
					});

					MessageService.menu(orderableColumns, event.currentTarget);
				};

				scope.shiftSortLeft = function (index) {
					let sort = scope.filters.sort;
					sort.splice(index - 1, 0, sort.splice(index, 1)[0]);
					scope.tick();
				};

				scope.shiftSortRight = function (index) {
					let sort = scope.filters.sort;
					sort.splice(index + 1, 0, sort.splice(index, 1)[0]);
					scope.tick();
				};

				scope.removeSort = function (index) {
					scope.filters.sort.splice(index, 1);
					scope.tick();
				};

				scope.$watchCollection('filters.columns', function () {
					if (scope.filters.columns) {
						for (let i = 0, l = scope.portfolioColumns.length; i < l; i++) {
							let column = scope.portfolioColumns[i];
							let name = column.ItemColumn.Name;
							let isVisible = scope.filters.columns.indexOf(name) === -1;
							let columnGroupId = columnGroup[name];

							if (typeof columnGroupId === 'undefined') {
								scope.selectedColumns[name] = isVisible;
							} else {
								let groupColumns = scope.selectedGroupedColumns[columnGroupId];
								let columnIndex = groupColumns.indexOf(name);

								if (isVisible) {
									if (columnIndex === -1) {
										groupColumns.push(name);
									}
								} else if (columnIndex !== -1) {
									groupColumns.splice(i, 1);
								}
							}
						}
					}
				});

				scope.saveTimeMachineDate = function () {
					scope.instantTick();
					PortfolioService.saveActiveSettings(portfolioId);
				};

				scope.excludeRows = function () {
						let eRowData = {'eIds': scope.filters.excludeItemIds};
						let content = {
							buttons: [
								{
									text: 'CANCEL',
									modify: 'cancel'

								},
								{
									type: 'primary',
									modify: 'create',
									text: 'SAVE',
									onClick: function () {
										scope.filters.excludeItemIds = eRowData.eIds;
										scope.instantTick();
									}
								}
							],
							inputs: [{
								type: 'process',
								model: 'eIds',
								label: 'SELECT_EXCLUDED_ROWS',
								max:0,
								min:0,
								process: attrs.process
							}],
							title: 'EXCLUDE_ROWS'
						};
						MessageService.dialog(content, eRowData);
				};

				scope.clearFilters = function () {
					PortfolioService.getSavedSettings(portfolioId).then(function (SavedPortfolios) {
						let defaultPortfolio: any = undefined;
						_.each(SavedPortfolios, function (p) {
							if (p.Public == 3) {
								defaultPortfolio = p;
								return;
							}
						});

						PortfolioService.clearActiveSettings(portfolioId);
						if (defaultPortfolio) {
							angular.forEach(defaultPortfolio.Value.filters, function (value, key) {
								scope.filters[key] = angular.copy(value);
							});

							if (defaultPortfolio.Value.filters.hasOwnProperty('excludeItemIds')) {
								scope.filters['excludeItemIds'] = angular.copy(defaultPortfolio.Value.filters['excludeItemIds'])
							}

							scope.priorization = angular.copy(defaultPortfolio.Value.priorization);
						} else {
							portfolioSettings.recursive = -1;

							if (!scope.hideProgress) {
								let i = scope.combinedPhases.length;
								while (i--) {
									scope.combinedPhases[i].filtered = true;
								}
							}

							scope.filters.columns = [];
							for (let i = 0, l = scope.portfolioColumns.length; i < l; i++) {
								let column = scope.portfolioColumns[i];
								if (!column.IsDefault) {
									scope.filters.columns.push(column.ItemColumn.Name);
								}
							}
						}

						if (scope.filters.hasOwnProperty('excludeItemIds')) {
							scope.filters.excludeItemIds = [];
						}

						filterChanged();
					});
				};

				let bouncer;
				scope.tick = function () {
					clearTimeout(bouncer);
					bouncer = setTimeout(function () {
						filterChanged();
					}, 1500);
				};

				scope.instantTick = function () {
					filterChanged();
				};

				scope.filter = function () {
					scope.visible = false;

					typeof scope.onClickFilter === 'function' && typeof scope.onClickFilter() === 'function' ?
						scope.onClickFilter()() : filterChanged();
				};

				function updateFilterValueOptions() {
					angular.forEach(scope.selects, function (ic) {
						if (ic.DataType == 16) {
							let fl = scope.portfolioListFilterValues && scope.portfolioListFilterValues[ic.Name + "_str"];
							if (fl) {
								let result = [];
								_.each(scope.storedOptions[ic.ItemColumnId], function (i) {
									if (_.includes(fl, i.item_id)) result.push(i);
								});
								scope.options[ic.ItemColumnId] = result;
							}
						}
					});
				}

				function updateOptions() {
					sublisted = [];
					angular.forEach(scope.selects, function (parent) {
						angular.forEach(scope.selects, function (child) {
							if (ProcessService.hasSublistData(process, parent.DataAdditional, child.DataAdditional) &&
								typeof scope.filters.select[parent.ItemColumnId] !== 'undefined'
							) {
								let childColumnId = child.ItemColumnId;
								scope.options[childColumnId] = [];
								let j = scope.filters.select[parent.ItemColumnId].length;
								while (j--) {
									scope.options[childColumnId] = scope.options[childColumnId]
										.concat(ProcessService.getSublistData(
											process,
											parent.DataAdditional,
											scope.filters.select[parent.ItemColumnId][j], child.DataAdditional)
										);
									if (scope.options[childColumnId].length > 0) {
										_.each(scope.options[childColumnId], function (item) {
											if (item.hasOwnProperty('list_item') && !item.hasOwnProperty(scope.userListLanguage)) {
												item[scope.userListLanguage] = item.list_item;
											}
										});
									}
								}

								angular.forEach(scope.filters.select[childColumnId], function (selectedValue) {
									let k = scope.options[childColumnId].length;
									while (k--) {
										if (scope.options[childColumnId][k].item_id == selectedValue) {
											break;
										}
									}

									if (k === -1) {
										scope.filters.select[childColumnId].splice(j, 1);
									}
								});

								if (sublisted.indexOf(child.ItemColumnId) === -1) {
									sublisted.push(child.ItemColumnId);
								}
							}
						});
					});
				}

				function filterChanged() {
					clearTimeout(bouncer);
					if (typeof scope.onChange === 'function' && typeof scope.onChange() === 'function') {
						scope.onChange()();
					}

					$rootScope.$emit('portfoliofilter-changed', portfolioId);
				}
			},
			controller: function ($scope) {
				$scope.showSave = typeof $scope.reloadNavigation === 'function';
			}
		};
	}
})();