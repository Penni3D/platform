(function () {
	'use strict';

	angular
		.module('core')
		.config(PortfolioExportConfig)
		.controller('PortfolioExportController', PortfolioExportController);

	PortfolioExportConfig.$inject = ['ViewsProvider'];
	function PortfolioExportConfig(ViewsProvider) {
		ViewsProvider.addDialog('export.portfolio', {
			controller: 'PortfolioExportController',
			template: 'core/portfolios/export/export.html',
			resolve: {
				Templates: function (StateParameters, AttachmentService)  {
					return AttachmentService.getControlAttachments(StateParameters.portfolioId, 'portfolioTemplates');
				}
			}
		});
	}

	PortfolioExportController.$inject = ['$scope', 'StateParameters', 'MessageService', 'PortfolioService', 'Templates'];
	function PortfolioExportController($scope, StateParameters, MessageService, PortfolioService, Templates) {
		$scope.templates = Templates;
		$scope.export = function () {
			MessageService.toast('BG_PROCESS_STARTED')
			PortfolioService.getPortfolioPdf(
				StateParameters.process,
				StateParameters.portfolioId,
				$scope.templateId,
				{ name: StateParameters.title + ' - Docx'}
			);

			$scope.cancel();
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};
	}
})();