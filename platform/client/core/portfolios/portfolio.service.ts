(function () {
	'use strict';

	angular
		.module('core.process')
		.service('PortfolioService', PortfolioService)
		.provider('PortfolioView', PortfolioView);

	PortfolioService.$inject = [
		'$q',
		'ApiService',
		'ProcessService',
		'Configuration',
		'Common',
		'Translator',
		'FileSaver',
		'$rootScope',
		'BackgroundProcessesService',
		'CalendarService'
	];

	function PortfolioService(
		$q,
		ApiService,
		ProcessService,
		Configuration,
		Common,
		Translator,
		FileSaver,
		$rootScope,
		BackgroundProcessesService,
		CalendarService
	) {
		let self = this;
		let Portfolios = ApiService.v1api('meta/ProcessPortfolios');

		let Items = ApiService.v1api('meta/Items');
		let ProcessPortfolioColumns = ApiService.v1api('settings/ProcessPortfolioColumns');
		let Tasks = ApiService.v1api('features/Tasks');


		let portfolios = {};
		let portfoliosPromises = {};
		let portfolioColumns = {};
		let portfolioColumnGroups = {};
		let portfolioColumnsPromises = {};
		let portfolioPhases = {};

		let processProgress = {};
		let portfolioUpdateCallbacks = {};
		let portfolioDefaultFilters = {};

		let portfolioXref = {};
		self.getUnionXRef = function (process: string) {
			return portfolioXref[process];
		}

		self.setUnionXref = function (process: string, value) {
			portfolioXref[process] = value;
		}

		self.setPortfolioDefaultFilter = function (portfolioId: number, filters: object) {
			portfolioDefaultFilters[portfolioId] = filters;
		}

		self.getPortfolioDefaultFilter = function (portfolioId: number) {
			if (!portfolioDefaultFilters[portfolioId]) {
				return self.getSavedSettings(portfolioId).then(function (SavedPortfolios) {

					let activeSettings = self.getActiveSettings(portfolioId);

					let defaultP = {
						Value: {
							filters: {
								text: '',
								select: {},
								range: {},
								dateRange: {},
								page: 0,
								limit: activeSettings.filters && activeSettings.filters.limit ? activeSettings.filters.limit : 10,
								phase: [],
								sort: [],
								columns: undefined
							},
							board: {
								startDate: new Date()
							},
							rows: {},
							calendar: {
								visibleDays: 60
							},
							showInMenu: false,
							recursive: -1
						}
					};
					for (let i = 0; i < SavedPortfolios.length; i++) {
						if (SavedPortfolios[i].Public == 3) {
							self.setPortfolioDefaultFilter(portfolioId, SavedPortfolios[i]);
							return SavedPortfolios[i]
							/*	defaultP = SavedPortfolios[i];
								break;*/
						}
					}

					portfolioDefaultFilters[portfolioId] = defaultP;
					return defaultP;

				})
			} else {
				let d = $q.defer();
				d.resolve(portfolioDefaultFilters[portfolioId]);
				return d.promise;
			}
		}

		function portfolioDefaultConfigurations(portfolioId = 0) {
			let r = {
				filters: {
					text: '',
					select: {},
					range: {},
					dateRange: {},
					page: 0,
					limit: 25,
					phase: [],
					sort: [],
					columns: undefined
				},
				board: {
					startDate: new Date()
				},
				rows: {},
				calendar: {
					visibleDays: 60
				},
				showInMenu: false,
				recursive: -1
			};

			if (portfolioId > 0) {
				let p = portfolios[portfolioId];
				if (p && p.OrderItemColumnName != null) {
					r.filters.sort.push({column: p.OrderItemColumnName, order: p.OrderDirectionName});
				}
			}

			return r;
		}

		self.getSavedTimeMachines = function (process) {
			return Portfolios.get([process, 'savedTimeMachines']);
		};

		self.addTimeMachineSave = function (process, timeMachineSave) {
			return Portfolios.new([process, 'savedTimeMachines'], timeMachineSave);
		};

		self.invalidatePortfolio = function (portfolioId) {
			delete portfolios[portfolioId];
		};

		self.invalidateProgress = function (portfolioId) {
			delete portfolioPhases[portfolioId];
		};

		self.getSavedSettings = function (portfolioId) {
			return Configuration.getSavedPreferences('portfolios', portfolioId);
		};

		self.setSavedSettings = function (portfolioId, name, share, settings, filters?, userGroups?, defaultView?, dashboardId?) {
			return Configuration.newPreferences('portfolios', portfolioId, name, share, settings, filters, userGroups, defaultView, dashboardId ? dashboardId : null);
		};

		self.saveSavedSettings = function (portfolioId, configurationId, name, share, settings, filters?, userGroups?, defaultView?, dashboardId?) {
			return Configuration.savePreferences('portfolios', portfolioId, configurationId, name, share, settings, filters, userGroups, defaultView, dashboardId ? dashboardId : null);
		};

		self.removeSavedSettings = function (portfolioId, configurationId, userId) {
			return Configuration.deleteSavedPreferences('portfolios', portfolioId, configurationId, userId);
		};

		self.saveActiveSettings = function (portfolioId) {
			if (typeof portfolioId === 'undefined') {
				return;
			}

			return Configuration.saveActivePreferences('portfolios', portfolioId);
		};

		self.clearActiveSettings = function (portfolioId) {
			let portfolioSettings = Configuration.getActivePreferences('portfolios')[portfolioId];

			if (typeof portfolioSettings === 'undefined') {
				return;
			}

			let filters = portfolioSettings.filters;

			filters.text = '';

			angular.forEach(filters.select, function (filter, key) {
				filters.select[key] = [];
			});

			angular.forEach(filters.range, function (filter, key) {
				filters.range[key].start = null;
				filters.range[key].end = null
			});

			angular.forEach(filters.dateRange, function (filter, key) {
				filters.dateRange[key].start = null;
				filters.dateRange[key].end = null
			});

			portfolioSettings.chartFilters = {};

			filters.sort = [];
			filters.phase = [];
		};

		self.getActiveSettings = function (portfolioId) {
			let portfolioSettings = Configuration.getActivePreferences('portfolios');
			portfolioSettings[portfolioId] = _.defaultsDeep(portfolioSettings[portfolioId], portfolioDefaultConfigurations(portfolioId));
			return portfolioSettings[portfolioId];
		};

		self.hasActiveFilters = function (portfolioId) {
			let activeFilters = self.getActiveSettings(portfolioId).filters;

			if (activeFilters.text || activeFilters.phase.length) {
				return true;
			}

			let f = false;

			angular.forEach(activeFilters.select, function (s) {
				if (s.length) {
					f = true;
				}
			});

			if (f) {
				return f;
			}

			angular.forEach(activeFilters.range, function (r) {
				if (!(isNaN(r.start) || isNaN(r.end))) {
					f = true;
				}
			});

			if (f) {
				return f;
			}

			angular.forEach(activeFilters.dateRange, function (d) {
				if (d.start && d.end) {
					f = true;
				}
			});

			return f;
		};

		self.getPortfolios = function (process, type) {
			return Portfolios.get([process, type]).then(function (result) {
				let i = result.length;
				while (i--) {
					let portfolio = result[i];
					portfolios[portfolio.ProcessPortfolioId] = portfolio;
				}

				let sortedPortfolios = result.sort(function (a, b) {
					return Translator.translation(a.LanguageTranslation) < Translator.translation(b.LanguageTranslation) ?
						-1 : 1
				});

				return angular.copy(sortedPortfolios);
			});
		};

		self.getGroups = function (process, portfolioId, type, reload) {
			if (Common.isFalse(reload) && portfolioPhases[portfolioId]) {
				let d = $q.defer();
				d.resolve(angular.copy(portfolioPhases[portfolioId]));
				return d.promise;
			}

			return self.getPortfolio(process, portfolioId, type).then(function (portfolio) {
				return ProcessService.getProcessGroups(process).then(function (processGroups) {
					for (let i = 0, l = processGroups.length; i < l; i++) {
						let group = processGroups[i];
						group.translation = Translator.translation(group.LanguageTranslation);
					}

					if (portfolio.HiddenProgressArrows.length) {
						let r = [];
						for (let i = 0, l = processGroups.length; i < l; i++) {
							let group = processGroups[i];

							let hiddenGroups = angular.fromJson(portfolio.HiddenProgressArrows);
							let j = hiddenGroups.length;
							while (j--) {
								if (hiddenGroups[j] == group.ProcessGroupId) {
									break;
								}
							}

							if (j === -1) {
								r.push(group);
							}
						}

						portfolioPhases[portfolioId] = r;
					} else {
						portfolioPhases[portfolioId] = processGroups;
					}

					return angular.copy(portfolioPhases[portfolioId]);
				});
			});
		};

		self.getPortfolio = function (process, portfolioId, type, reload) {
			if (Common.isFalse(reload) && portfolios[portfolioId]) {
				let d = $q.defer();
				d.resolve(angular.copy(portfolios[portfolioId]));
				return d.promise;
			}

			if (typeof portfoliosPromises[portfolioId] === 'undefined') {
				if (typeof type === 'undefined') {
					type = 0;
				}

				let p = Portfolios.get([process, type, portfolioId]).then(function (portfolio) {
					delete portfoliosPromises[portfolioId];

					portfolios[portfolioId] = portfolio;
					return angular.copy(portfolios[portfolioId]);
				});

				portfoliosPromises[portfolioId] = p;

				return p;
			} else {
				return portfoliosPromises[portfolioId];
			}
		};

		function setProcessProgress(process, itemId, states) {
			if (typeof processProgress[process] === 'undefined') {
				processProgress[process] = {};
			}

			if (typeof processProgress[process][itemId] === 'undefined') {
				processProgress[process][itemId] = {};
			}

			let itemProgress = processProgress[process][itemId];
			angular.forEach(states, function (group, id) {
				if (typeof itemProgress[id] === 'undefined') {
					itemProgress[id] = [];
				} else {
					itemProgress[id].length = 0;
				}

				itemProgress[id].push(group.s)
			});
		}

		function constructDynamicPortfolioRows(rows, process, portfolioId) {
			angular.forEach(rows.rowstates, function (states, itemId) {
				setProcessProgress(process, itemId, states.groupStates);
			});

			let pp = [];
			let previousPhase;
			if (portfolioPhases[portfolioId]) {
				for (let i = 0, l = portfolioPhases[portfolioId].length; i < l; i++) {
					let phase = portfolioPhases[portfolioId][i];
					if (!previousPhase || phase.translation !== previousPhase.translation) pp.push(phase);
					previousPhase = phase;
				}
			}

			for (let i = 0, l = rows.rowdata.length; i < l; i++) {
				let item = rows.rowdata[i];

				let k = 0, m = 0;
				for (let j = 0, l_ = portfolioColumns[portfolioId].length; j < l_; j++) {
					let ic = portfolioColumns[portfolioId][j];
					let name = ic.ItemColumn.Name;

					if (ic.UseInSelectResult == 5) {
						let itemId = typeof item.actual_item_id === 'undefined' ? item.item_id : item.actual_item_id;
						if (pp[k]) {
							let phase = processProgress[process][itemId][pp[k].ProcessGroupId];
							if (typeof phase === 'undefined') {
								item[name] = '';
							} else {
								item[name] = phase[phase.length - 1];
							}
						}
						k++;
					} else if (ic.UseInSelectResult == 8) {
						if (Array.isArray(item.bar_icons)) {
							let icon = item.bar_icons[m];

							if (typeof icon === 'undefined') {
								item[name] = {};
							} else {
								item[name] = icon;
							}

							m++;
						}
					}
				}
			}
		}

		function constructDynamicPortfolioColumns(process, portfolioId, itemColumns) {
			let pp = [];
			let previousPhase;
			for (let i = 0, l = portfolioPhases[portfolioId].length; i < l; i++) {
				let phase = portfolioPhases[portfolioId][i];
				if (!previousPhase || phase.translation !== previousPhase.translation) {
					pp.push(phase);
				}

				previousPhase = phase;
			}

			let activeSettings = self.getActiveSettings(portfolioId);
			let hiddenColumns = activeSettings.filters.columns;

			let i = itemColumns.length;
			while (i--) {
				let ic = itemColumns[i];
				let columnIsVisible = hiddenColumns.indexOf(ic.ItemColumn.Name) === -1;

				if (ic.UseInSelectResult == 5) {
					for (let j = 0, l = pp.length; j < l; j++) {
						let dc = angular.copy(ic);
						dc.ItemColumn.Name = dc.ItemColumn.Name + '_' + j;
						dc.ProcessGroupId = pp[j].ProcessGroupId;
						dc.ItemColumn.LanguageTranslation = pp[j].LanguageTranslation;

						if (dc.Width == null || typeof dc.Width === 'undefined') {
							dc.Width = 56;
						}

						if (!columnIsVisible) {
							hiddenColumns.push(dc.ItemColumn.Name);
						}

						itemColumns.splice(i + j + 1, 0, dc);
					}

					itemColumns.splice(i, 1);
				} else if (ic.UseInSelectResult == 8) {
					for (let j = 0, l = 5; j < l; j++) {
						let dc = angular.copy(ic);
						dc.ItemColumn.Name = dc.ItemColumn.Name + '_' + j;
						dc.ItemColumn.LanguageTranslation = 'M' + (j + 1);

						if (dc.Width == null || typeof dc.Width === 'undefined') {
							dc.Width = 56;
						}

						if (!columnIsVisible) {
							hiddenColumns.push(dc.ItemColumn.Name);
						}

						itemColumns.splice(i + j + 1, 0, dc);
					}

					itemColumns.splice(i, 1);
				}
			}
		}

		self.getPortfolioColumnGroups = function (process, portfolioId, reload) {
			if (Common.isFalse(reload) && portfolioColumnGroups[portfolioId]) {
				let d = $q.defer();
				d.resolve(angular.copy(portfolioColumnGroups[portfolioId]));
				return d.promise;
			}

			return ProcessPortfolioColumns.get(['ColumnGroups', process, portfolioId]).then(function (groups) {
				portfolioColumnGroups[portfolioId] = groups;
				return angular.copy(groups);
			});
		};

		let dontForceReload = true;
		self.clearColumnCache = function () {
			dontForceReload = false;
		}

		self.getPortfolioColumns = function (process, portfolioId, reload) {
			if (Common.isFalse(reload) && dontForceReload && portfolioColumns[portfolioId]) {
				let d = $q.defer();
				d.resolve(angular.copy(portfolioColumns[portfolioId]));
				return d.promise;
			}
			dontForceReload = true;
			if (typeof portfolioColumnsPromises[portfolioId] === 'undefined') {
				let promises = [];

				promises.push(ProcessPortfolioColumns.get([process, portfolioId])
					.then(function (results) {
						let portfolioSettings = Configuration.getActivePreferences('portfolios');
						let s = portfolioSettings[portfolioId];

						if (typeof s === 'undefined' || typeof s.filters.columns === 'undefined') {
							let a = [];

							for (let i = 0, l = results.length; i < l; i++) {
								if (!results[i].IsDefault && i) {
									a.push(results[i].ItemColumn.Name);
								}
							}

							if (typeof s === 'undefined') {
								portfolioSettings[portfolioId] = {filters: {columns: a}};
							} else {
								s.filters.columns = a;
							}
						} else {
							let hiddenIndex = s.filters.columns.indexOf(results[0].ItemColumn.Name);

							if (hiddenIndex !== -1) {
								s.filters.columns.splice(hiddenIndex, 1);
							}
						}

						portfolioColumns[portfolioId] = results;
						return results;
					})
				);

				promises.push(self.getGroups(process, portfolioId));

				let allPromises = $q.all(promises).then(function (resolves) {
					delete portfolioColumnsPromises[portfolioId];
					let pc = resolves[0];
					constructDynamicPortfolioColumns(process, portfolioId, pc);

					return angular.copy(pc);
				});

				portfolioColumnsPromises[portfolioId] = allPromises;

				return allPromises;
			} else {
				return portfolioColumnsPromises[portfolioId];
			}
		};

		self.getSimplePortfolioData = function (process, portfolioId, filters) {
			if (typeof filters === 'undefined') filters = {};

			let query = {
				offset: filters.offset ? filters.offset : 0,
				limit: filters.limit ? filters.limit : 0,
				text: undefined,
				sort: undefined,
				filters: filters.filters
			};

			if (filters.text) query.text = filters.text;
			if (filters.sort) query.sort = filters.sort;

			return Items
				.new([process, 'portfolio', portfolioId], query)
				.then(function (rows) {
					angular.forEach(rows.processes, function (data, key) {
						rows.processes[key] = ProcessService.setData({Data: data}, data.process, data.item_id).Data;
					});

					let i = rows.rowdata.length;
					while (i--) {
						let row = rows.rowdata[i];
						rows.rowdata[i] = ProcessService.setData({Data: row}, process, row.item_id).Data;
					}

					return rows;
				});
		};

		self.getPortfolioData = function (process, portfolioId, options, filters, forceToOrderByBeginning?: any, readOnly: boolean = false) {
			if (typeof options === 'undefined') options = {};
			return self.getPortfolioQueryObject(process, portfolioId, options, filters).then(function (queries) {
				if(typeof filters != "undefined") queries.onlyCount = filters.onlyCount;
				let completion = function (forcedColumn: any) {
					if (forcedColumn) {
						let newSort = [];
						if (queries.sort) {
							let dir = "asc";
							for (let s of queries.sort) {
								if (s.column == forcedColumn.Name)
									dir = s.order;
								else
									newSort.push(s);
							}
							newSort.unshift({column: forcedColumn.Name, order: dir});
							queries.sort = newSort;
						} else {
							queries.sort = [{column: forcedColumn.Name, order: "asc"}];
						}
					}

					if(!queries.sort.length){
						queries.sort = [];
						queries.sort.push({
							column: "item_id",
							order: "asc"
						})
					}

					return Items
						.new([process, 'portfolio', portfolioId], queries)
						.then(function (rows) {
							if (rows.unionData) self.setUnionXref(process, rows.unionData);
							if (Common.isFalse(options.local)) self.saveActiveSettings(portfolioId);

							if (!readOnly) {
								angular.forEach(rows.processes, function (data, key) {
									rows.processes[key] = ProcessService.setData({Data: data}, data.process, data.item_id).Data;
								});
							}

							constructDynamicPortfolioRows(rows, process, portfolioId);

							let i = rows.rowdata.length;
							if (!readOnly) {
								while (i--) {
									let row = rows.rowdata[i];
									row.itemId = row.item_id;

									if (typeof options.archive === 'undefined') {
										let itemId = Common.isFalse(options.recursive) ? row.item_id : row.actual_item_id;
										rows.rowdata[i] = ProcessService.setData({Data: row}, process, itemId).Data;
									} else {
										ProcessService.forceHistoryDate(process, row.item_id, options.archive, true);
									}
								}
							}
							return rows;
						});

				}

				//If id for forcing order column to beginning, then get the column before getting the results
				let forcedOrderColumn = forceToOrderByBeginning;
				if (forcedOrderColumn && typeof forcedOrderColumn === "number") {
					return self.getPortfolioColumns(process, portfolioId, false).then(function (columns) {
						for (let c of columns) {
							if (c.ItemColumn.ItemColumnId === forcedOrderColumn) {
								forcedOrderColumn = c.ItemColumn;
								break;
							}
						}
						return completion(forcedOrderColumn);
					});
				} else {
					return completion(forcedOrderColumn);
				}
			});
		};


		self.getPortfolioQueryObject = function (process, portfolioId, options, filters) {
			filters = typeof filters === 'object' ? angular.copy(filters) : {};
			options = typeof options === 'object' ? angular.copy(options) : {};

			let portfolioFilters = {}
			let aSettings = self.getActiveSettings(portfolioId);
			if (!options.chartForceFlatMode)
				portfolioFilters = Common.isTrue(options.local) ?
					filters : aSettings.filters;

			return self.getPortfolioColumns(process, portfolioId).then(function (portfolioColumns) {
				let defaultFilters = portfolioDefaultConfigurations();

				if (filters === undefined) filters = {};
				if (filters.sort === undefined){
					filters.sort = (aSettings.hasOwnProperty('filters') && aSettings.filters.hasOwnProperty('sort'))
						? aSettings.filters.sort
						: [{column: 'item_id', order: 'asc'}];
				}

				let f = {
					filters: {},
					limit: defaultFilters.filters.limit,
					offset: defaultFilters.filters.limit * defaultFilters.filters.page,
					sort: filters.sort,
					recursive: Common.isTrue(defaultFilters.recursive),
					phases: undefined,
					text: undefined,
					restrictions: undefined,
					context: undefined,
					archiveDate: undefined,
					itemsRestriction: undefined,
					excludeItemIds: portfolioFilters.hasOwnProperty("excludeItemIds") && portfolioFilters.excludeItemIds.length ? portfolioFilters.excludeItemIds : []
				};

				removeUnnecessaryColumns(portfolioColumns, portfolioFilters.select);
				removeUnnecessaryColumns(portfolioColumns, portfolioFilters.range);
				removeUnnecessaryColumns(portfolioColumns, portfolioFilters.dateRange);

				let phase = portfolioPhases[portfolioId];
				let filterPhases = [];
				angular.forEach(portfolioFilters.phase, function (groupId, i) {
					let j = phase.length;
					while (j--) {
						if (phase[j].ProcessGroupId == groupId) {
							break;
						}
					}

					if (j === -1) {
						portfolioFilters.phase.splice(i, 1);
					} else {
						filterPhases.push(groupId);
					}
				});

				if (filterPhases.length) f.phases = filterPhases;

				angular.forEach(portfolioFilters.select, function (columns, columnId) {
					if (columns.length) f.filters[columnId] = columns;
				});

				angular.forEach(portfolioFilters.range, function (column, columnId) {
					if (column.start || column.start === 0) {
						ensureFilterObject(f, columnId);
						f.filters[columnId].push(column.start);
					}

					if (column.end || column.end === 0) {
						ensureFilterObject(f, columnId);
						f.filters[columnId].push(column.end);

						if (f.filters[columnId].length === 1) {
							f.filters[columnId].unshift(null);
						}
					}
				});

				angular.forEach(portfolioFilters.dateRange, function (column, columnId) {
					let dataType = _.find(portfolioColumns, function (o) {
						return o.ItemColumn.ItemColumnId == columnId;
					}).ItemColumn.DataType;
					if (column.start) {
						ensureFilterObject(f, columnId);
						if (dataType == 3) {
							f.filters[columnId].push(moment(column.start).format('YYYY-MM-DD'));
						} else {
							f.filters[columnId].push(CalendarService.dateTimeToUTCSql(column.start));
						}
					}

					if (column.end) {
						ensureFilterObject(f, columnId);
						if (dataType == 3) {
							f.filters[columnId].push(moment(column.end).format('YYYY-MM-DD'));
						} else {
							f.filters[columnId].push(CalendarService.dateTimeToUTCSql(column.end));
						}

						if (f.filters[columnId].length === 1) {
							f.filters[columnId].unshift(null);
						}
					}
				});

				if (portfolioFilters.text) f.text = portfolioFilters.text;
				if (_.isEmpty(f.filters) && portfolioFilters.filters) f.filters = portfolioFilters.filters;

				let sortByColumns = [];
				angular.forEach(portfolioFilters.sort, function (sortColumn) {
					if (sortColumn.column === 'order_no' ||
						sortColumn.column === 'item_id' ||
						_.find(portfolioColumns, function (pc) {
							return pc.ItemColumn.Name === sortColumn.column;
						})
					) {
						sortByColumns.push({
							column: sortColumn.column,
							order: sortColumn.order ? sortColumn.order : 'asc'
						});
					}
				});
				if (sortByColumns.length) f.sort = sortByColumns;
				if (portfolioFilters.limit || portfolioFilters.limit == 0) {
					f.limit = portfolioFilters.limit;
					if (portfolioFilters.page) {
						f.offset = portfolioFilters.page * portfolioFilters.limit;
					}
				}
				if (typeof options.recursive !== 'undefined') f.recursive = Common.isTrue(options.recursive);
				if (options.restrictions) f.restrictions = options.restrictions;
				if (options.context) f.context = options.context;
				if (options.archive) f.archiveDate = options.archive;
				if (options.restrictItems) f.itemsRestriction = options.restrictItems;
				if (!f.archiveDate && portfolioFilters.archive) f.archiveDate = portfolioFilters.archive;
				else if (!f.archiveDate && options.archiveDate) f.archiveDate = options.archiveDate;
				return f;
			});
		};

		//Yläreunan 'download portfolio'
		self.getPortfolioExcel = function (process, portfolioId, options, optColumns?) {
			return self.getPortfolioQueryObject(process, portfolioId, options).then(function (query) {
				query.limit = 0;
				query.offset = 0;
				let bgParams = {'q': JSON.stringify(query)};
				bgParams["PortfolioId"] = portfolioId;
				bgParams["Process"] = process;
				bgParams["FileName"] = options.name ? options.name : "Keto";
				bgParams["Type"] = 1;
				bgParams["FromPortfolio"] = true;
				bgParams["StrictMode"] = true;

				//Logic involved with adding only shown portfolio columns to excel
				if (typeof optColumns != 'undefined') {
					let shownColumns = [];
					_.each(optColumns, function (col) {
						if (col.visible == true) shownColumns.push(col.ItemColumn.ItemColumnId);
					})
					if (shownColumns.length > 0) bgParams["ShownColumns"] = shownColumns;
				}

				return BackgroundProcessesService.executeOnDemand("GetDataAsExcel", bgParams);
			});
		};

		self.getPortfolioCsv = function (process, portfolioId, options, optColumns?) {
			return self.getPortfolioQueryObject(process, portfolioId, options).then(function (query) {
				query.limit = 0;
				query.offset = 0;
				let bgParams = { 'q': JSON.stringify(query) };
				bgParams["PortfolioId"] = portfolioId;
				bgParams["Process"] = process;
				bgParams["FileName"] = options.name ? options.name : "Keto";
				bgParams["Type"] = 1;
				bgParams["FromPortfolio"] = true;
				bgParams["StrictMode"] = true;
				bgParams["Mode"] = 'CSV';

				//Logic involved with adding only shown portfolio columns to excel
				if (typeof optColumns != 'undefined') {
					let shownColumns = [];
					_.each(optColumns, function (col) {
						if (col.visible == true) shownColumns.push(col.ItemColumn.ItemColumnId);
					})
					if (shownColumns.length > 0) bgParams["ShownColumns"] = shownColumns;
				}

				return BackgroundProcessesService.executeOnDemand("GetDataAsExcel", bgParams);
			});
		};

		self.getPortfolioPdf = function (process, portfolioId, templateId, options, singleItemId = 0, images = undefined) {
			return self.getPortfolioQueryObject(process, portfolioId).then(function (query) {
				if (singleItemId > 0) query.itemsRestriction = [singleItemId];
				query.limit = 0;
				if (images) query.portfolioPdfImageUpload = images;

				let bgParams = {'q': JSON.stringify(query)};
				bgParams["PortfolioId"] = portfolioId;
				bgParams["Process"] = process;
				bgParams["FileName"] = options.name ? options.name : "Keto";
				bgParams["AttachmentId"] = templateId;
				return BackgroundProcessesService.executeOnDemand("PdfDownload", bgParams);
			});
		};

		self.getTaskStatus = function (rowId) {
			return ApiService.v1api('features/Tasks/Statuses').get(rowId);
		};

		self.subscribePortfolioUpdate = function (scope, callback, process) {
			if (typeof portfolioUpdateCallbacks[process] === 'undefined') {
				portfolioUpdateCallbacks[process] = [];
			}

			let callbacks = portfolioUpdateCallbacks[process];
			callbacks.push(callback);

			scope.$on('$destroy', function () {
				let i = callbacks.length;
				while (i--) {
					if (callbacks[i] === callback) {
						callbacks.splice(i, 1);
						break;
					}
				}
			});
		};

		self.notifyPortfolioUpdate = function (process, hard) {
			if (typeof portfolioUpdateCallbacks[process] === 'undefined') {
				return;
			}

			let callbacks = portfolioUpdateCallbacks[process];

			let i = callbacks.length;
			while (i--) {
				let fn = callbacks[i];
				if (typeof fn === 'function') {
					fn(hard);
				}
			}
		};

		self.getTaskData = function (process, itemId, states, processGroupId, owner, portfolioId, archiveDates, restrictRowId = 0) {
			let archiveDate = ProcessService.hasHistoryCondition(process, states, itemId, processGroupId, archiveDates);

			return Tasks.get([portfolioId, itemId, owner, archiveDate, restrictRowId]).then(function (result) {
				let tasks = result.task;
				let returnTasks = [];

				for (let i = 0, l = tasks.length; i < l; i++) {
					let task = tasks[i];

					let t = {
						Data: task
					};

					if (archiveDate) {
						ProcessService.forceHistoryDate('task', task.item_id, archiveDate, true);
					} else {
						task = ProcessService.setData(t, 'task', task.item_id).Data;
					}

					returnTasks.push(task);
				}

				angular.forEach(result.processes, function (row) {
					ProcessService.setData({Data: row}, row.process, row.item_id);
				});

				return {
					tasks: returnTasks,
					archiveDate: archiveDate
				};
			});
		};

		self.setPortfoliosVisibility = function (savedSetting, UserGroups, Rights, ItemColumns, tab, publicTabs) {
			if (savedSetting.Public == 4 && savedSetting.VisibleForUserGroup && savedSetting.VisibleForUserGroup != "[]") {
				let groupParsed = JSON.parse(savedSetting.VisibleForUserGroup);
				for (let i = 0; i < groupParsed.length; i++) {
					if (UserGroups.includes(groupParsed[i]) || $rootScope.me.item_id == savedSetting.UserId) {
						publicTabs.push(tab);
						break;
					}
				}
			} else if (savedSetting.Public == 2 && savedSetting.VisibleForFilter && savedSetting.VisibleForFilter != "") {
				let icParsed = JSON.parse(savedSetting.VisibleForFilter);
				let icIds = Object.keys(icParsed)
				let icId = icIds[0];
				let icName = _.find(ItemColumns, function (o) {
					return o.ItemColumnId == icId;
				}).Name;
				if ($rootScope.me[icName] && (icParsed[icId][0] == $rootScope.me[icName][0] || $rootScope.me.item_id == savedSetting.UserId)) {
					publicTabs.push(tab);
				}
			} else {
				publicTabs.push(tab);
			}
		}

		self.cloneChart = function(chartId : number,
		                           portfolioId: number,
		                           translation: object,
		                           dashId: number,
		                           fromPortfolioId: number,
		                           fromDashboardId: number,
		                           process: string){
			return Portfolios.new(["cloneChart", process], {ChartId: chartId,
				ToPortfolioId: portfolioId,
				Translation: translation,
				ToDashboardId: dashId,
				FromPortfolioId: fromPortfolioId,
				FromDashboardId: fromDashboardId}
			);
		}

		function removeUnnecessaryColumns(portfolioColumns, filters) {
			angular.forEach(filters, function (filter, itemColumnId) {
				let i = portfolioColumns.length;
				while (i--) {
					let c = portfolioColumns[i];
					if (c.UseInFilter && c.ItemColumn.ItemColumnId == itemColumnId) {
						break;
					}
				}

				if (i === -1) {
					delete filters[itemColumnId];
				}
			});
		}

		function ensureFilterObject(filter, columnId) {
			if (typeof filter.filters[columnId] === 'undefined') {
				filter.filters[columnId] = [];
			}
		}
	}

	PortfolioView.$inject = [];

	function PortfolioView() {
		this.$get = function () {
			return {};
		};

		this.getView = function () {
			return {
				controller: 'PortfolioController',
				template: 'core/portfolios/portfolio.html',
				params: {
					portfolioView: 1
				},
				resolve: {
					PortfolioPhases: function (PortfolioService, StateParameters) {
						return PortfolioService.getGroups(StateParameters.process, StateParameters.portfolioId);
					},
					Portfolio: function (PortfolioService, StateParameters, Common, ProcessService, $rootScope, LocalStorageService) {
						return PortfolioService.getSavedSettings(StateParameters.portfolioId).then(function (SavedPortfolios) {
							let defaultPortfolio: any = undefined;
							_.each(SavedPortfolios, function (p) {
								if (p.Public == 3) {
									defaultPortfolio = p;
									return;
								}
							});
							return PortfolioService.getPortfolio(StateParameters.process, StateParameters.portfolioId).then(function (portfolio) {
								let recursiveQuery = (portfolio.Recursive > 1 && LocalStorageService.get('pViewTf_' + StateParameters.portfolioId) == 1 ||(portfolio.Recursive > 1 && portfolio.PortfolioDefaultView == 1))
								let recursive = portfolio.Recursive > 1;
								let filters = undefined;

								if (StateParameters.titleExtension && StateParameters.titleExtension != "") {
									angular.forEach(Object.keys(portfolio.LanguageTranslation), function (key) {
										portfolio.LanguageTranslation[key] = portfolio.LanguageTranslation[key] + " - " + StateParameters.titleExtension;
									});
								}

								let chartForceFlatMode = false;
								if (StateParameters.chartForceFlatMode) {
									chartForceFlatMode = true;
									recursive = false;
								}
								if (PortfolioService.hasActiveFilters(StateParameters.portfolioId) && portfolio.RecursiveFilterType === 1) recursive = false;

								let options = {
									recursive: recursiveQuery,
									restrictions: undefined,
									local: undefined,
									chartForceFlatMode: chartForceFlatMode
								};

								if (!PortfolioService.hasActiveFilters(StateParameters.portfolioId) && defaultPortfolio && defaultPortfolio.Value && defaultPortfolio.Value.filters) {
									filters = defaultPortfolio.Value.filters;

									let ac = PortfolioService.getActiveSettings(StateParameters.portfolioId);
									ac.filters = filters;

									PortfolioService.saveActiveSettings(StateParameters.portfolioId);
									options.local = true;
								} else {
									if (StateParameters.restrictions) options.restrictions = StateParameters.restrictions;
									if (StateParameters.local) options.local = true;
								}

								let filterCopy = angular.copy(PortfolioService.getActiveSettings(StateParameters.portfolioId));
								//Restore from State
								if (StateParameters.activeSettings) {
									filters = {};
									angular.forEach(StateParameters.activeSettings.filters, function (value, key) {
										filters[key] = angular.copy(value);
										if (!StateParameters.activeSettings.filters.hasOwnProperty('excludeItemIds')) {
											filters['excludeItemIds'] = [];
										}
									});
									PortfolioService.saveActiveSettings(StateParameters.portfolioId);
									options.local = true;
								}

								if (StateParameters.restrictions) options.restrictions = StateParameters.restrictions;

								if (StateParameters.frontpage == true) {
									return PortfolioService.getPortfolioDefaultFilter(StateParameters.portfolioId).then(function (filters_) {
										filters_.Value.filters.onlyCount = StateParameters.onlyCount;
										options.local = true;
										return PortfolioService
											.getPortfolioData(
												StateParameters.process,
												StateParameters.portfolioId,
												options,
												filters_.Value.filters,
												StateParameters.flatGroupOwnerColumnId
											).then(function (portfolioData) {

												//frontpage portfolio widget stuff
												let pi = {};
												pi[StateParameters.portfolioId] = portfolioData.rowcount;
												$rootScope.$emit('notifyFrontpageRows', pi);
												let i = portfolioData.rowdata.length;
												//

												while (i--) {
													let row = portfolioData.rowdata[i];
													if (Common.isFalse(recursive)) {
														row.stored_parent_item_id = row.parent_item_id;
														row.parent_item_id = 0;
													} else {
														portfolioData.rowdata[i] = ProcessService.setData(
															{Data: row},
															StateParameters.process,
															row.item_id
														).Data;
													}
												}
												portfolio.Rows = portfolioData;
												return portfolio;
											});
									})
								} else {
									return PortfolioService
										.getPortfolioData(
											StateParameters.process,
											StateParameters.portfolioId,
											options,
											StateParameters.activeSettings ? filterCopy.filters : filters,
											StateParameters.flatGroupOwnerColumnId
										).then(function (portfolioData) {
											let i = portfolioData.rowdata.length;
											while (i--) {
												let row = portfolioData.rowdata[i];


												if (Common.isFalse(recursiveQuery)) {
													row.stored_parent_item_id = row.parent_item_id;
													row.parent_item_id = 0;
												} else {
													portfolioData.rowdata[i] = ProcessService.setData(
														{Data: row},
														StateParameters.process,
														row.item_id
													).Data;
												}
											}

											portfolio.Rows = portfolioData;
											return portfolio;
										});
								}

							});
						});
					},
					SavedSettings: function (PortfolioService, StateParameters) {
						return PortfolioService.getSavedSettings(StateParameters.portfolioId);
					},
					Dashboards: function (SettingsService, StateParameters) {
						return SettingsService.ProcessDashboards(StateParameters.process, StateParameters.portfolioId);
					},
					Templates: function (StateParameters, ApiService) {
						return ApiService.v1api('meta/ControlAttachments').get([StateParameters.portfolioId, 'portfolioTemplates']);
					},
					Rights: function (StateParameters, ViewConfigurator) {
						return ViewConfigurator.getRights('portfolio', StateParameters.NavigationMenuId);
					},
					PortfolioColumnGroups: function (PortfolioService, StateParameters) {
						return PortfolioService.getPortfolioColumnGroups(
							StateParameters.process,
							StateParameters.portfolioId
						);
					},
					PortfolioColumns: function (PortfolioService, StateParameters) {
						return PortfolioService
							.getPortfolioColumns(StateParameters.process, StateParameters.portfolioId)
							.then(function (portfolioColumns) {
								let r = {
									columns: portfolioColumns,
									boardTitleColumn: undefined,
									boardDescriptionColumn: undefined,
									boardColorColumn: undefined,
									boardChipColumn: undefined,
									groupTitleColumn: undefined,
									groupHorizontalColumn: undefined,
									groupVerticalColumn: undefined,
									groupDescriptionColumn: undefined,
									groupColorColumn: undefined,
									groupChipsColumn: undefined,
									flatGroupOwnerColumnId: undefined,
									flatGroupColumns: undefined,
									costCenterColumn: undefined
								};

								let i = portfolioColumns.length;
								while (i--) {
									let ic = portfolioColumns[i].ItemColumn;
									let columnId = ic.ItemColumnId;
									if(ic.Name == "cost_center") r.costCenterColumn = columnId;
									if (columnId == StateParameters.boardTitleColumnId) r.boardTitleColumn = ic;
									if (columnId == StateParameters.boardDescriptionColumnId) r.boardDescriptionColumn = ic;
									if (columnId == StateParameters.boardColorColumnId) r.boardColorColumn = ic;
									if (columnId == StateParameters.boardChipColumnId) r.boardChipColumn = ic;
									if (columnId == StateParameters.groupTitleColumnId) r.groupTitleColumn = ic;
									if (columnId == StateParameters.groupHorizontalColumnId) r.groupHorizontalColumn = ic;
									if (columnId == StateParameters.groupVerticalColumnId) r.groupVerticalColumn = ic;
									if (columnId == StateParameters.groupDescriptionColumnId) r.groupDescriptionColumn = ic;
									if (columnId == StateParameters.groupColorColumnId) r.groupColorColumn = ic;
									if (columnId == StateParameters.groupChipColumnId) r.groupChipsColumn = ic;
									if (columnId == StateParameters.flatGroupOwnerColumnId) r.flatGroupOwnerColumnId = ic;
									if (StateParameters.flatGroupColumns && StateParameters.flatGroupColumns.includes(columnId)) {
										if (!r.flatGroupColumns) r.flatGroupColumns = [];
										r.flatGroupColumns.push(ic);
									}
								}

								return r;
							});
					},
					PortfolioActions: function (SettingsService, StateParameters) {
						return SettingsService.getProcessPortfolioActions(StateParameters.process, StateParameters.portfolioId);
					}
				},
				afterResolve: {
					Sublists: function (ProcessService, StateParameters, $rootScope, PortfolioColumns) {
						if ($rootScope.frontpage) return {};

						let found = false;
						_.each(PortfolioColumns.columns, function (col) {
							if (col.UseInFilter && col.ItemColumn.DataType == 6 || col.ItemColumn.SubDataType == 6) {
								found = true;
								return;
							}
						});
						if (!found) return {};
						return ProcessService.getSublists(StateParameters.process, StateParameters.portfolioId);
					},
					ActiveSettings: function (PortfolioService, StateParameters) {
						let ac = PortfolioService.getActiveSettings(StateParameters.portfolioId);
						if (StateParameters.activeSettings) {

							if (!StateParameters.activeSettings.filters.hasOwnProperty('excludeItemIds')) {
								ac.filters['excludeItemIds'] = [];
							}
							//Ajoitusongelma, koska tässä katsotaan Stateparameters.activefilters joka on vanha.
							//Se asetetaan uudeksi vasta tämän resolven jälkeen -> johtaa tilanteeseen jossa filter-
							//areassa filterit ovat väärin (vaikka portfolion data on ladattu oikein)

							/*angular.forEach(StateParameters.activeSettings.filters, function (value, key) {
								ac.filters[key] = angular.copy(value);
								if (!StateParameters.activeSettings.filters.hasOwnProperty('excludeItemIds')) {
									ac.filters['excludeItemIds'] = [];
								}
							});*/
							PortfolioService.saveActiveSettings(StateParameters.portfolioId);
						}

						return ac;
					},
					PortfolioColumns: function (PortfolioColumns) {
						return PortfolioColumns.columns;
					},
					BoardTitleColumn: function (PortfolioColumns) {
						return PortfolioColumns.boardTitleColumn;
					},
					BoardDescriptionColumn: function (PortfolioColumns) {
						return PortfolioColumns.boardDescriptionColumn;
					},
					BoardChipColumn: function (PortfolioColumns) {
						return PortfolioColumns.boardChipColumn;
					},
					BoardColorColumn: function (PortfolioColumns) {
						return PortfolioColumns.boardColorColumn;
					},
					Legends: function (PortfolioColumns, ProcessService, Common, Colors, $q) {
						let promises = [];
						let legends = {
							board: undefined,
							group: undefined
						};

						let getLegend = function (colorColumn) {
							if (colorColumn) {
								let dt = Common.getRelevantDataType(colorColumn);

								if (dt == 6) {
									return ProcessService
										.getListItems(Common.getRelevantDataAdditional(colorColumn))
										.then(function (items) {
											let blocks = [];
											for (let i = 0, l = items.length; i < l; i++) {
												let item = items[i];
												if (item.list_item && item.list_symbol_fill) {
													let b = {
														translation: item.list_item,
														background: Colors.getColor(item.list_symbol_fill).background
													};

													blocks.push(b);
												}
											}

											return blocks;
										});
								}
							}

							let d = $q.defer();
							d.resolve([]);
							return d.promise;
						};

						if (PortfolioColumns.boardColorColumn) {
							promises.push(getLegend(PortfolioColumns.boardColorColumn).then(function (colors) {
								legends.board = colors;
							}));
						}

						if (PortfolioColumns.groupColorColumn) {
							promises.push(getLegend(PortfolioColumns.groupColorColumn).then(function (colors) {
								legends.group = colors;
							}));
						}

						return $q.all(promises).then(function () {
							return legends;
						});
					},

					GroupTitleColumn: function (PortfolioColumns) {
						return PortfolioColumns.groupTitleColumn;
					},
					GroupHorizontalColumn: function (PortfolioColumns) {
						return PortfolioColumns.groupHorizontalColumn;
					},
					GroupVerticalColumn: function (PortfolioColumns) {
						return PortfolioColumns.groupVerticalColumn;
					},
					GroupDescriptionColumn: function (PortfolioColumns) {
						return PortfolioColumns.groupDescriptionColumn;
					},
					GroupColorColumn: function (PortfolioColumns) {
						return PortfolioColumns.groupColorColumn;
					},
					GroupChipColumn: function (PortfolioColumns) {
						return PortfolioColumns.groupChipsColumn;
					},
					flatGroupOwnerColumnId: function (PortfolioColumns) {
						return PortfolioColumns.flatGroupOwnerColumnId;
					},
					FlatGroupColumns: function (PortfolioColumns) {
						return PortfolioColumns.flatGroupColumns;
					},
					UserGroups: function (UserService) {
						return UserService.getUsersGroups().then(function (groups) {
							return groups;
						});
					},
					ItemColumns: function (ProcessActionsService) {
						return ProcessActionsService.getItemColumns('user');
					},
					CostCenterColumnId: function(PortfolioColumns){
						return PortfolioColumns.costCenterColumn;
					}
				}
			};
		};

		this.getViewConfigurations = function () {
			return {
				onSetup: {
					ItemColumns: function (ProcessService, StateParameters, PortfolioService, $q) {
						let promises = [];
						let columns = [];

						promises.push(ProcessService.getColumns(StateParameters.process).then(function (itemColumns) {
							angular.forEach(itemColumns, function (column) {
								if (column.StatusColumn) {
									columns.push(column);
								}
							});
						}));

						promises.push(PortfolioService.getPortfolioColumns(StateParameters.process, StateParameters.portfolioId).then(function (portfolioColumns) {
							for (let i = 0, l = portfolioColumns.length; i < l; i++) {
								columns.push(portfolioColumns[i].ItemColumn);
							}
						}));

						return $q.all(promises).then(function () {
							return _.uniqBy(columns, function (column) {
								return column.ItemColumnId;
							});
						});
					},
					GetDataType: function (Common) {
						return Common.getRelevantDataType;
					},
					Processes: function (ProcessesService) {
						return ProcessesService.get(0);
					},
					Portfolios: function (PortfolioService) {
						return PortfolioService.getPortfolios('allocation');
					},
					ProcessPortfolios: function (PortfolioService) {
						return PortfolioService.getPortfolios();
					},
					ExcelFields: function (ProcessService, StateParameters, $filter) {
						return ProcessService.getColumns(StateParameters.process).then(function (columns) {
							let ret = [];
							angular.forEach(columns, function (key) {
								if (key.DataType <= 6 || key.DataType === 16 || key.DataType === 20) {
									ret.push(key);
								}
							});
							return $filter('orderBy')(ret);
						});
					}
				},
				tabs: {
					DEFAULT: {
						splitSize: function () {
							return {
								type: 'select',
								options: [{name: 'VIEW_SPLIT_EVEN', value: 'even'}, {
									name: 'VIEW_SPLIT_WIDER',
									value: 'wider'
								}, {name: 'VIEW_SPLIT_NARROWER', value: 'narrower'}],
								label: 'PORTFOLIO_SPLIT_SIZE'
							}
						},
						flatGroupOwnerColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'FLAT_GROUP_OWNER'
							};
						},
						flatGroupColumns: function (resolves) {
							return {
								type: 'multiselect',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'FLAT_GROUP_COLUMNS'
							};
						}
					},
					BOARD: {
						boardTitleColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_BOARD_TITLE'
							};
						},
						boardDate: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'Name',
								label: 'PORTFOLIO_BOARD_DATE'
							};
						},
						boardDescriptionColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_BOARD_DESCRIPTION'
							};
						},
						boardColorColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_BOARD_COLOR'
							};
						},
						boardChipColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_BOARD_CHIPS'
							};
						}
					},
					GROUP: {
						groupTitleColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_GROUP_TITLE'
							};
						},
						groupHorizontalColumnId: function (resolves) {
							return {
								type: 'select',
								options: _.filter(resolves.ItemColumns, function (ic) {
									return resolves.GetDataType(ic) == 6
								}),
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_GROUP_HORIZONTAL'
							};
						},
						groupVerticalColumnId: function (resolves) {
							return {
								type: 'select',
								options: _.filter(resolves.ItemColumns, function (ic) {
									return resolves.GetDataType(ic) == 6
								}),
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_GROUP_VERTICAL'
							};
						},
						groupDescriptionColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_GROUP_DESCRIPTION'
							};
						},
						groupColorColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_GROUP_COLOR'
							};
						},
						groupChipColumnId: function (resolves) {
							return {
								type: 'select',
								options: resolves.ItemColumns,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_GROUP_CHIPS'
							};
						}
					},
					PRIORIZATION: {
						priorizationGraphPos: function () {
							return {
								type: 'select'
								, options: [
									{name: 'PRIORIZATION_SETUP_GRAPHPOS_SPLIT', value: 'split'}
									, {name: 'PRIORIZATION_SETUP_GRAPHPOS_BELOW', value: 'below'}
									, {name: 'PRIORIZATION_SETUP_GRAPHPOS_TAB', value: 'tab'}
								]
								, label: 'PRIORIZATION_SETUP_GRAPHPOS'
							}
						},
						priorizationOffsetInterval: function () {
							return {
								type: 'select'
								, options: [
									{value: 'day', name: 'PRIORIZATION_OFFSET_INTERVAL_DAY'}
									, {value: 'month', name: 'PRIORIZATION_OFFSET_INTERVAL_MONTH'}
									, {value: 'year', name: 'PRIORIZATION_OFFSET_INTERVAL_YEAR'}
									, {value: 'quarter', name: 'PRIORIZATION_OFFSET_INTERVAL_QUARTER'}
									, {value: 'week', name: 'PRIORIZATION_OFFSET_INTERVAL_WEEK'}
								]
								, label: 'PRIORIZATION_SETUP_OFFSET_INTERVAL'
							}
						}
					},
					Resources: {
						FilteringPortfolioId: function (resolves) {
							return {
								label: 'WT_STATUS_FILTERING_PORTFOLIO',
								type: 'select',
								options: resolves.Portfolios,
								optionValue: 'ProcessPortfolioId',
								optionName: 'LanguageTranslation'
							};
						},
						AdditionalFields: function (resolves) {
							return {
								type: 'select',
								options: resolves.ExcelFields,
								optionName: 'LanguageTranslation',
								optionValue: 'ItemColumnId',
								label: 'PORTFOLIO_RESOURCES_ADDITIONAL_FIELDS',
								max: 0
							};
						},
						DisableFte: function(){
							return {
								type: 'select',
								options: [{name: 'TRUE', value: 1}, {name: 'FALSE', value: 0}],
								optionName: 'name',
								optionValue: 'value',
								label: 'PORTFOLIO_DISABLE_FTE',
								max: 1
							};
						}
					}
				}
			}
		};
	}
})();