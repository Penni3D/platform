(function () {
	'use strict';

	angular
		.module('core.process')
		.config(PortfolioConfigurationConfig)
		.controller('PortfolioConfigurationController', PortfolioConfigurationController);

	PortfolioConfigurationConfig.$inject = ['ViewsProvider'];

	function PortfolioConfigurationConfig(ViewsProvider) {
		ViewsProvider.addDialog('configure.portfolioSimple', {
			template: 'core/portfolios/configuration/portfolioConfigurationSimple.html',
			controller: 'PortfolioConfigurationController',
			resolve: {
				SavedSettings: function (PortfolioService, StateParameters) {
					return PortfolioService.getSavedSettings(StateParameters.portfolioId);
				},
				Rights: function (ViewConfigurator, StateParameters) {
					return ViewConfigurator.getRights('portfolio', StateParameters.NavigationMenuId);
				},
				UserProcessColumns: function (ProcessService) {
					return ProcessService.getColumns('user', true).then(function (cols) {
						return _.filter(cols, function (c) {
							return c.DataType == 6 || c.DataType == 5
						});
					});
				},
				UserGroups: function (SettingsService) {
					return SettingsService.UserGroups('user_group');
				},
				Dashboards: function (SettingsService, StateParameters) {
					return SettingsService.ProcessDashboards(StateParameters.process, StateParameters.portfolioId);
				},

				Portfolio: function(PortfolioService, StateParameters){
					return PortfolioService.getPortfolio(StateParameters.process, StateParameters.portfolioId);
				}
			}
		});
	}

	PortfolioConfigurationController.$inject = [
		'$scope',
		'CurrentSettings',
		'PortfolioService',
		'MessageService',
		'ReloadNavigation',
		'StateParameters',
		'SavedSettings',
		'Common',
		'Rights',
		'UserProcessColumns',
		'Translator',
		'ListsService',
		'$rootScope',
		'UserGroups',
		'UserService',
		'Dashboards',
		'RefreshParent',
		'Portfolio'
	];

	function PortfolioConfigurationController(
		$scope,
		CurrentSettings,
		PortfolioService,
		MessageService,
		ReloadNavigation,
		StateParameters,
		SavedSettings,
		Common,
		Rights,
		UserProcessColumns,
		Translator,
		ListsService,
		$rootScope,
		UserGroups,
		UserService,
		Dashboards,
		RefreshParent,
		Portfolio
	) {

		$scope.upc = UserProcessColumns;
		$scope.selectedFilterColumns = []; //Item id array for selected filter columns
		$scope.listItemCollection = {}; //Dictionary <list prosess name, list items>
		$scope.selections = {}; //Final dataset dictionary, <item column id, array<selected item ids>
		$scope.userGroupSelections = [];
		$scope.filterColumns_ = [];
		$scope.userGroups = UserGroups;

		$scope.visibilityList = [
			{
				name: Translator.translate('PORTFOLIO_CONFIGURATION_PUBLIC'),
				value: 1
			},
			{
				name: Translator.translate('PORTFOLIO_CONFIGURATION_TO_ME'),
				value: 0
			},
			{
				name: Translator.translate('PORTFOLIO_CONFIGURATION_TO_FILTERED'),
				value: 2
			},
			{
				name: Translator.translate('PORTFOLIO_CONFIGURATION_TO_USER_GROUP'),
				value: 4
			}
		];

		$scope.dashboards = Dashboards;
		let showRelations = Rights.indexOf('relations') !== -1;
		let showProportional = Rights.indexOf('proportional') !== -1;
		let showPriorisation = Rights.indexOf('priorisation') !== -1;
		let showBoard = Rights.indexOf('board') !== -1;
		let showGroup = Rights.indexOf('group') !== -1
		let showRecursive = Portfolio.Recursive != 0;
		let showResource = Rights.indexOf('resource') !== -1;
		let showSundial = Rights.indexOf('sundial') !== -1;
		$scope.portfolioViews = [{'name': 'PORTFOLIO_FV', 'value': 0}, {'name': 'DASHBOARDS_IN_PORTFOLIO', 'value':69}, {'name': 'KEEP_CURRENT_VIEW', 'value':-1}];

		if(showRecursive) $scope.portfolioViews.push({'name': 'PORTFOLIO_RECURSIVE_V', 'value': 1})
		if(showBoard) $scope.portfolioViews.push({'name': 'PORTFOLIO_BV', 'value': 5})
		if(showGroup) $scope.portfolioViews.push({'name': 'PORTFOLIO_GV', 'value': 7})
		if(showRelations) $scope.portfolioViews.push({'name': 'PORTFOLIO_RV', 'value': 2})
		if(showProportional) $scope.portfolioViews.push({'name': 'PORTFOLIO_PV', 'value': 3})
		if(showPriorisation) $scope.portfolioViews.push({'name': 'PORTFOLIO_P', 'value': 4})
		if(showSundial) $scope.portfolioViews.push({'name': 'PORTFOLIO_SD', 'value': 6})
		if(showResource) $scope.portfolioViews.push({'name': 'PORTFOLIO_RS', 'value': 8})

		//This function adds selectable inputs based on selected filter columns. This also takes care of adding list items
		//to selected list item columns and deletes selections, if filter column is unchecked
		$scope.setFilterColumns = function () {
			$scope.filterColumns_ =
				_.filter($scope.upc, function (upc) {
					return _.includes($scope.selectedFilterColumns, upc.ItemColumnId);
				});
			_.each($scope.filterColumns_, function (column) {
				if (column.DataType == 6) {
					ListsService.getProcessItems(column.DataAdditional, 'order').then(function (items) {
						$scope.listItemCollection[column.DataAdditional] = items;
					});
				}
			});
			_.each(Object.keys($scope.selections), function (value) {
				if (!_.includes($scope.selectedFilterColumns, Number(value))) {
					delete $scope.selections[Number(value)];
				}
			});
		};

		$scope.changeVisibility = function (v) {
			$scope.listPublic = v;
		};

		let visibilityFilters = {};
		//Checks if portfolio save has existing filters and selects them in UI
		if (CurrentSettings.VisibleForFilter) {
			visibilityFilters = JSON.parse(CurrentSettings.VisibleForFilter);
			$scope.selections = visibilityFilters;
		}
		
		
		if(CurrentSettings.VisibleForUserGroup) {
			$scope.userGroupSelections = JSON.parse(CurrentSettings.VisibleForUserGroup);
		}

		//This pissy "convert strings to ints" loop has to be done. Otherwise setFilterColumns
		// function's _.filter(_.includes)... does not work xd
		_.each(Object.keys(visibilityFilters), function (idAsString) {
			$scope.selectedFilterColumns.push(Number(idAsString));
		});

		$scope.setFilterColumns();

		let existingTranslations = [];
		for (let i = 0, l = SavedSettings.length; i < l; i++) {
			let s = SavedSettings[i];
			if (s.ConfigurationId != CurrentSettings.ConfigurationId) {
				existingTranslations.push(s.Name);
			}
		}

		$scope.title = CurrentSettings.translation;
		$scope.name = CurrentSettings.Name;
		$scope.public = CurrentSettings.Public ? CurrentSettings.Public : 0;
		$scope.isNew = StateParameters.isNew;
		$scope.showPublic = Rights.indexOf('public') !== -1;
		$scope.showDefault = Rights.indexOf('default') !== -1;
		$scope.defaultView = CurrentSettings.PortfolioDefaultView;

		$scope.listPublic = 0;

		if(CurrentSettings.Public == 1) $scope.listPublic = 1;
		if(CurrentSettings.Public == 4) $scope.listPublic = 4;
		if(CurrentSettings.Public == 2) $scope.listPublic = 2;

		if ($scope.showPublic == false) {
			$scope.visibilityList = [
				{
					name: Translator.translate('PORTFOLIO_CONFIGURATION_TO_ME'),
					value: 0
				}
			];
		}

		if ($scope.showDefault) {
			$scope.visibilityList.push({
				name: Translator.translate('PORTFOLIO_CONFIGURATION_TO_DEFAULT'),
				value: 3
			});
		}

		$scope.saving = false;
		$scope.deleting = false;

		$scope.save = 'SAVE';
		$scope.delete = 'DELETE';

		$scope.changeName = function(newname) {
			$scope.name = newname;
			$scope.checkClashes();
		};
		
		$scope.checkClashes = function() {
			let c = Common.clashedTranslations($scope.name, existingTranslations);
			if (c && $scope.listPublic != 3) {
				$scope.notUnique = true;
				$scope.clashes = c;
			} else {
				$scope.notUnique = false;
				$scope.clashes = [];
			}
		};

		$scope.checkInputs = function(){
			if($scope.defaultView == null) return false;
			if($scope.listPublic == 4 && $scope.userGroupSelections.length != 0){
				return true;
			} else if($scope.listPublic == 2 && !_.isEmpty($scope.selections)){

				let icIds = Object.keys($scope.selections)
				let icId = icIds[0];

				if($scope.selections[icId].length == 0){
					return false;
				} else return true;

			} else if($scope.listPublic == 0 || $scope.listPublic == 1){
				if($scope.defaultView != 10){
					return true;
				} else {
					return $scope.dashboard != null;
				}
			}
			return false;
		}

		$scope.creator = "";

		if(CurrentSettings.UserId != null){
			UserService.getUser(CurrentSettings.UserId).then(function(j){
				$scope.creator = j.primary;
			})
		}
		$scope.dashboard = null;

		$scope.replaceSettings = function(){
			MessageService.confirm('FILTERS_CONFIRM_REPLACE', function () {
				PortfolioService.saveSavedSettings(
					StateParameters.portfolioId,
					CurrentSettings.ConfigurationId,
					$scope.name,
					$scope.listPublic,
					StateParameters.ActiveSettings,
					!_.isEmpty($scope.selections) ? $scope.selections :null,
					$scope.userGroupSelections,
					$scope.defaultView
				);
				RefreshParent();
			});
		}

		$scope.saveSettings = function () {
			$scope.checkClashes();
			if ($scope.notUnique) return;

			$scope.saving = true;
			$scope.save = 'SAVING';
			CurrentSettings.Value.showInMenu = true;
			if (!_.isEmpty($scope.selections) && $scope.listPublic == 2) {
				CurrentSettings.VisibleForFilter = JSON.stringify($scope.selections);
			} else {
				CurrentSettings.VisibleForFilter = null;
			}

			if($scope.listPublic == 4){
				CurrentSettings.VisibleForUserGroup = JSON.stringify($scope.userGroupSelections);
			} else {
				CurrentSettings.VisibleForUserGroup = "[]";
			}

			let p;
			if (!$scope.showPublic) $scope.listPublic = 0;
			if ($scope.isNew) {
				p = PortfolioService.setSavedSettings(
					StateParameters.portfolioId,
					$scope.name,
					$scope.listPublic,
					CurrentSettings.Value,
					CurrentSettings.VisibleForFilter,
					CurrentSettings.VisibleForUserGroup,
					$scope.defaultView,
					$scope.dashboard
				);
			} else {
				p = PortfolioService.saveSavedSettings(
					StateParameters.portfolioId,
					CurrentSettings.ConfigurationId,
					$scope.name,
					$scope.listPublic,
					CurrentSettings.Value,
					CurrentSettings.VisibleForFilter,
					CurrentSettings.VisibleForUserGroup,
					$scope.defaultView,
					$scope.dashboard
				);
			}

			PortfolioService.setPortfolioDefaultFilter(StateParameters.portfolioId, {'Value': CurrentSettings.Value});
			p.then(function () {
				MessageService.closeDialog();
				ReloadNavigation();
			});
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.deleteSettings = function () {
			MessageService.confirm('CONFIRM_DELETE', function () {
				$scope.deleting = true;
				$scope.delete = 'DELETING';
				PortfolioService
					.removeSavedSettings(StateParameters.portfolioId, CurrentSettings.ConfigurationId, CurrentSettings.UserId)
					.then(function () {
						MessageService.closeDialog();
						ReloadNavigation();
					});
			});
		};
	}
})();
