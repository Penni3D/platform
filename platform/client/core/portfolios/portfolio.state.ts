var updateAll = function () {

};
(function () {
	'use strict';

	angular
		.module('core.process')
		.config(PortfolioConfig)
		.controller('PortfolioController', PortfolioController)
		.controller('PortfolioExcelDataController', PortfolioExcelDataController);

	PortfolioConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider', 'PortfolioViewProvider'];

	function PortfolioConfig(ViewsProvider, ViewConfiguratorProvider, PortfolioViewProvider) {
		ViewConfiguratorProvider.addConfiguration('portfolio', PortfolioViewProvider.getViewConfigurations());
		ViewsProvider.addView('portfolio', PortfolioViewProvider.getView());
		ViewsProvider.addFeature('portfolio', 'portfolio', 'FEATURE_PORTFOLIO', ['read']);
		ViewsProvider.addPage(
			'portfolio',
			'PAGE_PORTFOLIO',
			['read', 'public', 'default', 'ignoreRights', 'excel', 'csv', 'pdf', 'relations', 'proportional', 'priorisation', 'board', 'sundial', 'group', 'actions', 'exportdata', 'config', 'resource']
		);
	}

	PortfolioController.$inject = [
		'$scope',
		'ProcessService',
		'PortfolioService',
		'ActiveSettings',
		'ToolbarService',
		'PortfolioColumns',
		'ViewService',
		'StateParameters',
		'Portfolio',
		'SidenavService',
		'SavedSettings',
		'Dashboards',
		'PortfolioPhases',
		'UserService',
		'Common',
		'Colors',
		'Rights',
		'$timeout',
		'CalendarService',
		'$q',
		'MessageService',
		'BoardTitleColumn',
		'BoardDescriptionColumn',
		'BoardColorColumn',
		'BoardChipColumn',
		'Legends',
		'PortfolioColumnGroups',
		'Translator',
		'GroupTitleColumn',
		'GroupHorizontalColumn',
		'GroupVerticalColumn',
		'GroupDescriptionColumn',
		'GroupColorColumn',
		'GroupChipColumn',
		'PortfolioActions',
		'$rootScope',
		'UserGroups',
		'ItemColumns',
		'flatGroupOwnerColumnId',
		'FlatGroupColumns',
		'LocalStorageService',
		'ExcelImportService',
		'BackgroundProcessesService',
		'AttachmentService',
		'AllocationService',
		'RMService',
		'CostCenterColumnId'
	];

	function PortfolioController(
		$scope,
		ProcessService,
		PortfolioService,
		ActiveSettings,
		ToolbarService,
		PortfolioColumns,
		ViewService,
		StateParameters,
		Portfolio,
		SidenavService,
		SavedSettings,
		Dashboards,
		PortfolioPhases,
		UserService,
		Common,
		Colors,
		Rights,
		$timeout,
		CalendarService,
		$q,
		MessageService,
		BoardTitleColumn,
		BoardDescriptionColumn,
		BoardColorColumn,
		BoardChipColumn,
		Legends,
		PortfolioColumnGroups,
		Translator,
		GroupTitleColumn,
		GroupHorizontalColumn,
		GroupVerticalColumn,
		GroupDescriptionColumn,
		GroupColorColumn,
		GroupChipColumn,
		PortfolioActions,
		$rootScope,
		UserGroups,
		ItemColumns,
		flatGroupOwnerColumnId,
		FlatGroupColumns,
		LocalStorageService,
		ExcelImportService,
		BackgroundProcessesService,
		AttachmentService,
		AllocationService,
		RMService,
		CostCenterColumnId
	) {
		if (StateParameters.frontpage) {
			let p = ViewService.getView(StateParameters.ParentViewId);
			if (p && typeof p.scope.setIcon === 'function') {
				p.scope.setIcon(StateParameters.ViewId, Portfolio.Rows.rowcount);
			}
		}

		let ap = StateParameters.process == 'user' ? 'resources' : 'projects'
		let allocationQuery = false;
		$scope.inactiveStatus = AllocationService.getActiveSettings(ap).status;

		$scope.toggleStatus = function (sta) {
			allocationQuery = true;
			AllocationService.toggleFilterStatus(ap, sta);
			$scope.getRows();
		}
		$scope.allocProcess = StateParameters.process;
		$scope.activeSettings = ActiveSettings;
		$scope.costCenterItemColumnId = CostCenterColumnId;
		let iconExists = false;

		let dashboardCollection = {};
		//Alareunan export napin lataus
		$scope.exportExcel = function () {
			return PortfolioService.getPortfolioQueryObject(StateParameters.process, StateParameters.portfolioId, {}).then(function (query) {
				query.limit = 0;
				query.offset = 0;
				let bgParams = {};
				bgParams["PortfolioId"] = StateParameters.portfolioId;
				bgParams["FileName"] = $scope.header.title + ' - Excel';
				bgParams["Process"] = StateParameters.process;
				bgParams["StrictMode"] = false;
				bgParams["Type"] = 1;
				bgParams["FromPortfolio"] = true;
				bgParams["q"] = JSON.stringify(query);
				return BackgroundProcessesService.executeOnDemand("GetDataAsExcel", bgParams);
			});
		}

		$scope.toggleFte = function (modeIdentifier) {
			let fte = modeIdentifier == 'hours' ? false : true;
			let name1 = StateParameters.process == 'user' ? 'resource_allocation' : 'project_allocation'
			let name2 = StateParameters.process == 'user' ? 'resource_allocation_fte' : 'project_allocation_fte'
			AllocationService.setFte(name1, fte);
			AllocationService.setFte(name2, fte);
			AllocationService.setModeName(modeIdentifier);
			$scope.isFteOn = fte
			ViewService.refresh(StateParameters.ViewId);
		}

		$scope.importExcel = function () {
			let promises = [];
			promises.push(ExcelImportService.getFile(StateParameters.process, 'Excel_' + $rootScope.clientInformation.item_id));

			return $q.all(promises).then(function (resolves) {
				if (resolves[0] && resolves[0][0]) AttachmentService.deleteAttachment(resolves[0][0].AttachmentId);

				MessageService.dialogAdvanced({
					template: 'core/portfolios/views/portfolioExcelExportImport.html',
					controller: 'PortfolioExcelDataController',
					locals: {
						Files: [],
						StateParameters: StateParameters,
						ExportExcel: $scope.exportExcel
					}
				});
			});
		}

		ProcessService.ignoreColumn(StateParameters.process, 'parent_item_id', true);

		let activeTab = {
			$$activeTab: undefined
		};
		let recursive = Portfolio.Recursive > 1;
		let activeFilters = ActiveSettings.filters;
		StateParameters.activeSettings = angular.copy(ActiveSettings);
		StateParameters.activeTitle = "No filter";


		let options = {
			recursive: recursive,
			restrictions: undefined,
			local: undefined
		};
		if (StateParameters.restrictions) {
			options.local = true;
			options.restrictions = StateParameters.restrictions;
		}

		let showSumRow = false;
		let flatLock = false;

		let showRelations = Rights.indexOf('relations') !== -1;
		let showProportional = Rights.indexOf('proportional') !== -1;
		let showPriorisation = Rights.indexOf('priorisation') !== -1;
		let showBoard = Rights.indexOf('board') !== -1;
		let showResources = Rights.indexOf("resource") !== -1;
		let showGroup = Rights.indexOf('group') !== -1 && GroupHorizontalColumn;
		let showExcel = Rights.indexOf('excel') !== -1;
		let showCsv = Rights.indexOf('csv') !== -1;
		let showPdf = Rights.indexOf('pdf') !== -1;
		let showActions = Rights.indexOf('actions') !== -1;
		let showSundial = Rights.indexOf('sundial') !== -1;
		$scope.showExport = Rights.indexOf('exportdata') !== -1;
		let configure = Rights.indexOf('config') !== -1;

		$scope.edit = false;
		if (configure) {
			ToolbarService.setConfigureFunction(function () {
				$scope.edit = !$scope.edit;
				if ($scope.edit) {
					let params = StateParameters;
					params.limited = true;
					params.ProcessPortfolioId = Portfolio.ProcessPortfolioId;
					params.type = 'portfolios';
					ViewService.view('settings.portfolios.fields', {params: params}, {split: true, size: 'wider'});
				}
			});
		}

		let updateBoardData = function () {
		};

		let sumRow = {
			order_no: 'zzzzzzzzzz',
			task_type: 4,
			parent_item_id: 0,
			item_id: 'sumRow',
			process: StateParameters.process
		};

		//sumRow = ProcessService.setData({ Data: sumRow }, StateParameters.process, 'sumRow').Data;
		$scope.frontpage = typeof StateParameters.frontpage === 'undefined' ? false : StateParameters.frontpage;
		$scope.showFilters = false;

		$scope.portfolioDashboards = {
			process: StateParameters.process,
			Dashboards: Dashboards
		};

		$scope.groupTitleColumn = GroupTitleColumn;
		$scope.groupHorizontalColumn = GroupHorizontalColumn;
		$scope.groupVerticalColumn = GroupVerticalColumn;
		$scope.groupDescriptionColumn = GroupDescriptionColumn;
		$scope.groupColorColumn = GroupColorColumn;
		$scope.groupChipColumn = GroupChipColumn;

		$scope.query = activeFilters;
		SidenavService.setNavigatePortfolioObj('limit', $scope.query.limit);
		$scope.parentItemId = 0;
		$scope.portfolioId = StateParameters.portfolioId;
		$scope.portfolioColumns = [];
		$scope.portfolioColumnsAndReportColumns = [];
		$scope.StateParameters = StateParameters;
		$scope.totalRows = Portfolio.Rows.rowcount;


		$scope.actualRowCount = Portfolio.Rows.actual_row_count;
		$scope.visualMode = Portfolio.hasOwnProperty('PortfolioDefaultView') && Portfolio.PortfolioDefaultView != null ? Portfolio.PortfolioDefaultView : StateParameters.portfolioView;

		if ($scope.visualMode == "") $scope.visualMode = 0;

		$scope.filterListValues = Portfolio.Rows.filterList;
		$scope.processGroups = PortfolioPhases;
		$scope.navigationMenuId = StateParameters.NavigationMenuId;
		$scope.portfolioColumnGroups = PortfolioColumnGroups;

		let groupedColumns = {};
		for (let i = 0, l = PortfolioColumns.length; i < l; i++) {
			let c = PortfolioColumns[i];
			$scope.portfolioColumnsAndReportColumns.push(c);
			if (c.ReportColumn) continue;

			if (FlatGroupColumns) {
				for (let fg of FlatGroupColumns) {
					if (fg.Name == c.ItemColumn.Name) c.flat = true;
				}
			}

			let groupId = c.ProcessPortfolioColumnGroupId;
			if (groupId) {
				let j = 0;
				let l_ = 0;
				for (j = 0, l_ = PortfolioColumnGroups.length; j < l_; j++) {
					if (PortfolioColumnGroups[j].ProcessPortfolioColumnGroupId === groupId) {
						if (typeof groupedColumns[groupId] === 'undefined') groupedColumns[groupId] = [];
						c.grouped = true;
						groupedColumns[groupId].push(c);
						break;
					}
				}

				if (j === l_) $scope.portfolioColumns.push(c);
			} else {
				$scope.portfolioColumns.push(c);
			}

			//Prevent Sumup columns from saving
			if (c.UseInSelectResult == 6 || c.UseInSelectResult == 9 || c.UseInSelectResult == 10 || c.UseInSelectResult == 11 || c.UseInSelectResult == 12) {
				ProcessService.ignoreColumn(StateParameters.process, c.ItemColumn.Name, true);
			}

			if (c.SumColumn) {
				sumRow[c.ItemColumn.Name] = Portfolio.Rows[c.ItemColumn.Name];
				showSumRow = true;
			}
		}

		for (let i = 0, l = PortfolioColumnGroups.length; i < l; i++) {
			let columns = groupedColumns[PortfolioColumnGroups[i].ProcessPortfolioColumnGroupId];
			if (columns) {
				for (let j = 0, l_ = columns.length; j < l_; j++) {
					$scope.portfolioColumns.push(columns[j]);
				}
			}
		}

		$timeout(function () {
			$scope.ready = true;
		});
		$scope.showArchive = Portfolio.TimeMachine;
		if ($scope.frontpage) {
			$scope.hideProcessProgress = true;
		} else {
			ToolbarService.setFirstSlot(Portfolio.LanguageTranslation);
			ToolbarService.setPathPortfolio(Portfolio.Process, Portfolio.ProcessPortfolioId);
			setNavigation(SavedSettings);
			$scope.hideProcessProgress = !Portfolio.ShowProgressArrows;
		}

		if (!StateParameters.frontpage || (StateParameters.frontpage && Portfolio.Rows.rowcount > 10))
			ViewService.footbar(StateParameters.ViewId, {
				template: 'core/portfolios/portfolioFootbar.html',
				scope: $scope
			});


		$scope.reloadNavigation = function () {
			return PortfolioService.getSavedSettings(StateParameters.portfolioId).then(setNavigation);
		};

		$scope.rules = {};

		$scope.calendarIn = function () {
			if ($scope.visualMode === 5) {
				$scope.boardOptions.columns = 'month';
			} else if ($scope.visualMode === 8) {
				allCA["allocation-feature"].calendarZoomIn();
			}
		};

		$scope.calendarOut = function () {
			if ($scope.visualMode === 5) {
				$scope.boardOptions.columns = 'quartal';
			} else if ($scope.visualMode === 8) {
				allCA["allocation-feature"].calendarZoomOut();
			}
		};

		$scope.calendarTodays = function () {
			if ($scope.visualMode === 8) {
				allCA["allocation-feature"].calendarToday();
			}
		}

		$scope.actionsEnabled = showActions && !StateParameters.frontpage;
		$scope.actionsExecuting = false;
		$scope.portfolioActions = PortfolioActions;

		//Enable Long Press
		if ($scope.actionsEnabled) for (let r of Portfolio.Rows.rowdata) r.has_write_right = true;

		let executeQueue = [];
		let execute = function () {

			let rowIds = [];
			let actionId = 0;

			_.each(executeQueue, function (row) {
				if (_.isNumber(row.item_id)) {
					rowIds.push(row.item_id);
				} else {
					if (row.hasOwnProperty('actual_item_id')) {
						rowIds.push(row.actual_item_id)
					} else {
						let indexOf_ = row.item_id.indexOf("_");
						rowIds.push(Number(row.item_id.substring(indexOf_ + 1, row.item_id.length + 1)));
					}
				}

				actionId = row.action;
			})

			BackgroundProcessesService.executeOnDemand("DoActionForSelectedItemIds", {
				'Ids': rowIds,
				'Action': actionId,
				'Process': StateParameters.process
			}, 'BG_ACTIONS_TITLE');

			$rootScope.$emit('notifyShowLoading', {show: false});
			$scope.actionsExecuting = false;
			$scope.clickSelStop();

		};


		$scope.isFteOn = AllocationService.getUseFte(StateParameters.process != 'user'
			? 'project_allocation'
			: 'resource_allocation');

		$scope.excludedItemIds = undefined;
		$scope.excludeSelected = function () {
			let temp = [];
			_.each($scope.getRMSelectedRows(), function (row) {
				temp.push(row.data.actual_item_id ? row.data.actual_item_id : row.data.item_id);
			});
			$scope.excludedItemIds = temp;
			$scope.showSelection = false;
		}

		$scope.executeAction = function (action) {
			MessageService.confirm('CONFIRM_PORTFOLIO_ACTION', function () {
				$rootScope.$emit('notifyShowLoading', {show: true});
				$scope.actionsExecuting = true;
				executeQueue = [];
				$scope.curentActionExecution = 0;

				_.each($scope.getRMSelectedRows(), function (row) {
					executeQueue.push({item_id: row.data.item_id, action: action});
				});
				$scope.totalActionExecution = executeQueue.length;
				execute();
			});
		};


		$scope.getActionName = function (name) {
			let n = JSON.parse(name);
			return n[$rootScope.clientInformation["locale_id"]];
		};
		$scope.options = {
			hideOpenAll: false,
			process: StateParameters.process,
			selectable: $scope.actionsEnabled,
			sumDatas: true,
			openAll: true,
			readOnly: !$scope.actionsEnabled,
			featuretteWidth: StateParameters.splitSize,
			showUserName: true,
			headerClick: function (columnName) {
				let i = $scope.portfolioColumns.length;
				while (i--) {
					let ic = $scope.portfolioColumns[i].ItemColumn;

					if (columnName == ic.Name && ic.DataType < 60) {
						let columnIndex = _.findIndex(activeFilters.sort, ['column', ic.Name]);
						let sortColumn;

						if (columnIndex === -1) {
							sortColumn = {
								column: ic.Name,
								order: 'asc'
							};
						} else {
							sortColumn = activeFilters.sort.splice(columnIndex, 1)[0];
							if (columnIndex === 0) {
								sortColumn.order = sortColumn.order === 'desc' ? 'asc' : 'desc';
							} else {
								sortColumn.order = 'asc';
							}
						}

						if (activeFilters.sort.length > 3) activeFilters.sort.splice(-1, 1);
						activeFilters.sort.unshift(sortColumn);

						$scope.getRows();
						break;
					}
				}
			}
		};

		if (Portfolio.OpenSplit == 0 || Portfolio.OpenSplit == 1) {
			$scope.options.rowClick = function (id, process?) {
				if (id == 'sumRow') return;
				if (!process) process = StateParameters.process;
				let row = ProcessService.getCachedItemData(process, id).Data;
				if (row) openRow(row, Portfolio.OpenSplit == 1);
			};
		} else {
			$scope.options.menuItems = {
				default: [
					{
						name: Translator.translate('PORTFOLIO_SPLIT_QUICK'),
						isActive: function (itemId) {
							return itemId !== 'sumRow';
						},
						onClick: function (id) {
							let row = ProcessService.getCachedItemData(StateParameters.process, id).Data;
							openRow(row, true);
						}
					},
					{
						name: Translator.translate('PORTFOLIO_SPLIT_OPEN'),
						isActive: function (itemId) {
							return itemId !== 'sumRow';
						},
						onClick: function (id) {
							let row = ProcessService.getCachedItemData(StateParameters.process, id).Data;
							openRow(row, false);
						}
					}]
			};

			if (Portfolio.OpenSplit == 2) {
				$scope.options.rowDblClick = function (id) {
					if (id == 'sumRow') return;
					let row = ProcessService.getCachedItemData(StateParameters.process, id).Data;
					openRow(row, false);
				};
			} else {
				$scope.options.rowDblClick = function (id) {
					if (id == 'sumRow') return;
					let row = ProcessService.getCachedItemData(StateParameters.process, id).Data;
					openRow(row, true);
				};
			}
		}

		let filterList = {
			color: PortfolioService.hasActiveFilters(StateParameters.portfolioId) ?
				Colors.getColor('accent').rgb : '',
			icon: 'filter_list',
			name: 'FILTER',
			onClick: function () {
				$scope.showFilters = !$scope.showFilters;

				if ($scope.showFilters) {
					ViewService.scrollTop(StateParameters.ViewId);
				}
			}
		};
		$scope.header = {
			title: Translator.translation(Portfolio.LanguageTranslation),
			icons: [filterList]
		};


		if (LocalStorageService.get('portfolioExtTitle_' + Portfolio.ProcessPortfolioId)) {
			$scope.header.extTitle = LocalStorageService.get('portfolioExtTitle_' + Portfolio.ProcessPortfolioId);
		}

		if (StateParameters.restrictions) {
			$scope.header = {
				title: Translator.translation(Portfolio.LanguageTranslation)
			};
		}

		$scope.colorBlocks = Legends;

		if (showBoard && StateParameters.boardTitleColumnId && StateParameters.boardDate) {
			$scope.boardOptions = ActiveSettings.board;
			$scope.boardDate = StateParameters.boardDate;

			let formatData = function (card, column) {
				let d = $q.defer();
				let dataType = Common.getRelevantDataType(column);
				let value = card[column.Name];

				if (dataType == 1 || dataType == 2) {
					d.resolve(Common.formatNumber(value));
				} else if (dataType == 13) {
					d.resolve(CalendarService.formatDateTime(value));
				} else if (dataType == 3) {
					d.resolve(CalendarService.formatDate(value));
				} else if (dataType == 5 || dataType == 6) {
					let da = Common.getRelevantDataAdditional(column);

					if (Array.isArray(value)) {
						ProcessService.getItems(da, value).then(function (items) {
							let r = [];
							for (let i = 0, l = items.length; i < l; i++) {
								r.push(items[i].primary);
							}

							d.resolve(r);
						});
					} else {
						d.resolve('');
					}
				} else {
					d.resolve(value);
				}

				return d.promise;
			};

			let joinListProcess = function (column, arrayValues) {
				let d = Common.getRelevantDataType(column);
				return d == 5 || d == 6 ? _.join(arrayValues, ', ') : arrayValues;
			};

			updateBoardData = function () {
				angular.forEach($scope.portfolioRows, function (card) {
					formatData(card, BoardTitleColumn).then(function (value) {
						card.PortfolioBoardTitle = joinListProcess(BoardTitleColumn, value);
					});

					if (BoardDescriptionColumn) {
						formatData(card, BoardDescriptionColumn).then(function (value) {
							card.PortfolioBoardDescription = joinListProcess(BoardDescriptionColumn, value);
						});
					}

					if (BoardChipColumn) {
						formatData(card, BoardChipColumn).then(function (items) {
							let c = [];
							let d = Common.getRelevantDataType(BoardChipColumn);

							if (d == 5 || d == 6) {
								for (let i = 0, l = items.length; i < l; i++) {
									c.push({
										name: items[i]
									});
								}
							} else {
								c.push({
									name: items
								});
							}

							card.PortfolioBoardChips = c;
						});
					}

					if (BoardColorColumn) {
						let value = card[BoardColorColumn.Name];
						if (Common.getRelevantDataType(BoardColorColumn) == 6) {
							ProcessService.getItems(Common.getRelevantDataAdditional(BoardColorColumn), value)
								.then(function (items) {
									let r = [];
									let i = items.length;
									while (i--) {
										let c = items[i].list_symbol_fill;
										if (c) {
											r.push(c);
										}
									}

									if (r.length === 1) {
										card.PortfolioBoardColor = r[0];
									}
								});
						} else if (value) {
							card.PortfolioBoardColor = value;
						}
					}
				});
			};

			if (!StateParameters.frontpage) {
				$scope.$watch('boardOptions', function (newOptions, oldOptions) {
					if (newOptions === oldOptions) {
						return;
					}

					PortfolioService.saveActiveSettings(StateParameters.portfolioId);
				}, true);
			}
		}

		$scope.cardClick = function (card) {
			openRow(card, Portfolio.OpenSplit == 1);
		};

		if (showExcel) {
			ViewService.addDownloadable('PORTFOLIO_EE', function () {
				//	$scope.header.extTitle = '';
				PortfolioService.getPortfolioExcel(
					StateParameters.process,
					StateParameters.portfolioId,
					{recursive: recursive, name: $scope.header.title + ' - Excel'});
			}, StateParameters.ViewId);
			ViewService.addDownloadable('PORTFOLIO_EE_ONLY_SHOWN', function () {
				//	$scope.header.extTitle = '';
				PortfolioService.getPortfolioExcel(
					StateParameters.process,
					StateParameters.portfolioId,
					{recursive: recursive, name: $scope.header.title + ' - Excel'},
					$scope.portfolioColumns);
			}, StateParameters.ViewId);
		}

		if (showCsv) {
			ViewService.addDownloadable('PORTFOLIO_CSV', function () {
				//	$scope.header.extTitle = '';
				PortfolioService.getPortfolioCsv(
					StateParameters.process,
					StateParameters.portfolioId,
					{recursive: recursive, name: $scope.header.title + ' - CSV'});
			}, StateParameters.ViewId);
		}

		if (showPdf) {
			ViewService.addDownloadable('PORTFOLIO_PDF', function () {
				let params = {
					params: {
						portfolioId: StateParameters.portfolioId,
						process: StateParameters.process,
						title: $scope.header.title
					}
				};
				ViewService.view('export.portfolio', params);
			}, StateParameters.ViewId);
		}

		$scope.onPageChange = function () {
			$scope.getRows(true);
		};

		$scope.onPerPageChange = function () {
			$scope.getRows();
		};


		$scope.getRows = function (pageChange, updateOnly) {
			if (updateOnly) {
				ViewService.lock(StateParameters.ViewId);
			} else {
				//	$scope.header.extTitle = '';
				$scope.ready = false;

				if (!pageChange) {
					$scope.query.page = 0;
				}
			}

			if (PortfolioService.hasActiveFilters(StateParameters.portfolioId)) {
				if (Portfolio.RecursiveFilterType === 1) recursive = false;
				filterList.color = Colors.getColor('accent').rgb;
			} else {
				if (Portfolio.RecursiveFilterType === 1 && Portfolio.Recursive > 1 && !flatLock) recursive = true;
				filterList.color = '';
			}

			let filterObj = allocationQuery ? ActiveSettings.filters : {
				limit: $scope.query.limit,
				page: $scope.query.page
			}

			PortfolioService
				.getPortfolioData(
					StateParameters.process,
					StateParameters.portfolioId,
					{
						local: StateParameters.frontpage || StateParameters.restrictions || allocationQuery,
						restrictions: StateParameters.restrictions ? StateParameters.restrictions : undefined,
						recursive: StateParameters.restrictions ? false : recursive
					},
					filterObj,
					flatGroupOwnerColumnId)
				.then(function (portfolioData) {
					allocationQuery = false;
					let result = [];
					orderPortfolioRows(angular.copy(portfolioData.rowdata), '0', result);
					SidenavService.setNavigatePortfolioObj('rows', result)
					SidenavService.setNavigatePortfolioObj('limit', $scope.query.limit);
					SidenavService.setNavigatePortfolioObj('restrictions', StateParameters.restrictions);
					$scope.filterListValues = portfolioData.filterList;

					let i = portfolioData.rowdata.length;
					while (i--) {
						let row = portfolioData.rowdata[i];
						if ($scope.actionsEnabled) row.has_write_right = true;
						if (recursive) {
							portfolioData.rowdata[i] = ProcessService.setData(
								{Data: row},
								StateParameters.process,
								row.item_id
							).Data;
						} else {
							row.stored_parent_item_id = row.parent_item_id;
							row.parent_item_id = 0;
						}
					}

					if (showSumRow) {
						let i = $scope.portfolioColumns.length;
						while (i--) {
							let column = $scope.portfolioColumns[i];
							if (column.SumColumn) {
								sumRow[column.ItemColumn.Name] = portfolioData[column.ItemColumn.Name];
							}
						}

						ProcessService.notifyChanged(StateParameters.process, 'sumRow');
					}

					if (updateOnly) {
						ViewService.unlock(StateParameters.ViewId);
					} else {
						$scope.ready = true;
						$scope.totalRows = portfolioData.rowcount;
						$scope.actualRowCount = portfolioData.actual_row_count;
						updateRows(portfolioData.rowdata, portfolioData.processes);
					}
				});
		};

		showCalendar();
		updateRows(Portfolio.Rows.rowdata, Portfolio.Rows.processes);

		ProcessService.subscribeNewItem($scope, $scope.getRows, StateParameters.process);
		PortfolioService.subscribePortfolioUpdate($scope, function (isHard) {
			$scope.getRows(false, !isHard);
		}, StateParameters.process);

		function openSpecialTask(task, split) {
			return ViewService.openSpecialTask(task, split);
		}

		function setTotalRows() {
			let limit_ = ActiveSettings && ActiveSettings.filters ? ActiveSettings.filters.limit : 25;

			if (Portfolio.Recursive == 0) {
				return $scope.totalRows
			} else if (Portfolio.Recursive != 0 && $scope.actualRowCount < limit_) {
				return $scope.rows.length - 1;
			}
			return $scope.actualRowCount;
		}

		function openRow(row, split) {
			if (row.process && row.process == "task") {
				openSpecialTask(row, split).then(function (result) {
					SidenavService.setNavigatePortfolioObj('rows', angular.copy($scope.rows));
					SidenavService.setNavigatePortfolioObj('total_rows', setTotalRows());
					if (!result) openRowFinally(row, split);
				});
			} else {
				openRowFinally(row, split);
			}
		}


		let orderPortfolioRows = function (rows, parentItemId, result) {
			let parents = _.filter(rows, function (row) {
				return row.parent_item_id == parentItemId;
			})
			_.each(parents, function (parent) {
				result.push(parent);
				_.each(rows, function (row2) {

					if (row2.parent_item_id == parent.itemId) {
						result.push(row2);
						orderPortfolioRows(rows, row2.itemId, result)
					}
				})
			});
		}

		function openRowFinally(row, split) {
			LocalStorageService.store('pViewTf', s);
			let result = [];
			orderPortfolioRows(angular.copy($scope.rows), '0', result);
			SidenavService.setNavigatePortfolioObj('rows', result);
			SidenavService.setNavigatePortfolioObj('total_rows', setTotalRows());

			function completeOpen(currentPortfolio, columns, unionResolution) {
				//If alternative jump target has been specified -- use it if stars are aligned
				if (currentPortfolio.IsUnionPortfolio == false && currentPortfolio.OpenRowItemColumnId > 0 && typeof currentPortfolio.OpenRowProcess == 'string' && currentPortfolio.OpenRowProcess.length > 0) {
					let colname = "";
					_.each(columns, function (col) {
						if (col.ItemColumn.ItemColumnId === currentPortfolio.OpenRowItemColumnId) {
							colname = col.ItemColumn.Name;
							return;
						}
					});

					let additionalFeaturetteParams = undefined;
					if (currentPortfolio.AlsoOpenCurrent == 1) {
						additionalFeaturetteParams = {
							process: row.process,
							itemId: row.itemId,
							title: row.title,
							subState: currentPortfolio.SubDefaultState,
							size: currentPortfolio.OpenWidth
						};
					}

					//Child's sub state is not a tab

					if (currentPortfolio.SubDefaultState.length > 0 && _.isNaN(Number(currentPortfolio.SubDefaultState)) && currentPortfolio.SubDefaultState != '1stTab' && currentPortfolio.SubDefaultState != 'lastTab') {
						let toState = {
							view: 'process.tab',
							ownerIdCol: null,
							params: {
								process: currentPortfolio.OpenRowProcess,
								itemId: row[colname != 'parent_item_id' ? colname : 'stored_parent_item_id'],
								tabId: null
							}
						};

						currentPortfolio.DefaultState = currentPortfolio.SubDefaultState

						ViewService.resolveTransition(row, toState, currentPortfolio);

						ViewService.view(toState.view, {params: toState.params}, {
							split: false,
							size: currentPortfolio.OpenWidth,
							replace: true
						})

						if (currentPortfolio.AlsoOpenCurrent != 0) {
							let params2 = {
								process: row.process,
								itemId: row.itemId,
								tabId: null,
								additionalFeaturetteParams: additionalFeaturetteParams
							};

							ViewService.view('process.tab', {params: params2}, {
								split: true,
								size: StateParameters.splitSize,
								replace: true
							});
						}

					} else {
						if (unionResolution) {
							let ud = PortfolioService.getUnionXRef(StateParameters.process);
							let xref = ud[row.process + row.union_portfolio_id];
							_.each(xref, function (o, k) {
								if (o.Name === colname) {
									colname = k.replace("_" + row.union_portfolio_id, "");
									return;
								}
							});
						}
						let colData = row[colname];
						if (colname.length > 0 && typeof colData != 'undefined') {
							let params = {
								process: currentPortfolio.OpenRowProcess,
								itemId: colData,
								tabId: currentPortfolio.SubDefaultState == 'lastTab' ? null : currentPortfolio.SubDefaultState,
								additionalFeaturetteParams: additionalFeaturetteParams
							};

							if (!colData || colData == 0) {
								ProcessService.getItem(row.process, row.itemId, true).then(function (updatedRow) {
									params.itemId = updatedRow[colname];
									ViewService.view('process.tab', {params: params});
								});
							} else {
								ViewService.view('process.tab', {params: params});
							}
							return;
						}
					}
					return;
				}

				let toState = {
					view: 'process.tab',
					ownerIdCol: null,
					params: {
						process: currentPortfolio.IsUnionPortfolio ? row.process : StateParameters.process,
						itemId: ProcessService.getActualItemId(row.actual_item_id ? row.actual_item_id : row.item_id),
						tabId: null
					}
				};

				ViewService.resolveTransition(row, toState, currentPortfolio);

				split ?
					ViewService.view(toState.view, {params: toState.params}, {
						split: true,
						size: StateParameters.splitSize
					}) :
					ViewService.view(toState.view, {params: toState.params});
			}

			if (Portfolio.IsUnionPortfolio && row.union_portfolio_id && row.union_portfolio_id > 0) {
				PortfolioService.getPortfolio(row.process, row.union_portfolio_id).then(function (targetPortfolio) {
					PortfolioService.getPortfolioColumns(row.process, row.union_portfolio_id).then(function (targetColumns) {
						completeOpen(targetPortfolio, targetColumns, true);
					});
				});
			} else {
				completeOpen(Portfolio, $scope.portfolioColumnsAndReportColumns, false);
			}
		}

		$scope.cardRows = {};

		function updateRows(pRows, pProcesses) {
			$scope.portfolioRows = pRows;
			$scope.cardRows = angular.copy(pRows);
			SidenavService.setNavigatePortfolioObj('portfolio_id', StateParameters.portfolioId)
			SidenavService.setNavigatePortfolioObj('p', activeFilters.page);
			$scope.portfolioProcesses = pProcesses;
			updateBoardData();

			let actualRows = [];
			let previousValues = {};
			let useFlatGrouping = false;
			if (FlatGroupColumns && FlatGroupColumns.length > 0) {
				useFlatGrouping = true;
				for (let fc of FlatGroupColumns) {
					previousValues[fc.Name] = "";
				}
			}

			for (let i = 0, l = pRows.length; i < l; i++) {
				let row = pRows[i];
				if (useFlatGrouping) {
					for (let fc of FlatGroupColumns) {
						if (_.isEqual(previousValues[fc.Name], row[fc.Name])) {
							row[fc.Name] = "";
							row.addclass = "flat-col-group";
						} else {
							previousValues[fc.Name] = row[fc.Name];
							row.addclass = "flat-col-group first";
						}
					}
				}
				actualRows.push(row);
			}
			if (showSumRow && actualRows.length != 0) actualRows.push(sumRow);
			$scope.rows = actualRows;
		}

		function showCalendar() {
			$scope.showCalendar = ($scope.visualMode === 8) || !!(Portfolio.CalendarType && !$scope.frontpage);
			$scope.options.showCalendar = $scope.showCalendar;

		}

		$scope.calendarDates = {
			s: AllocationService.portfolioResourceCalendardates[StateParameters.portfolioId] ? AllocationService.portfolioResourceCalendardates[StateParameters.portfolioId].s : LocalStorageService.get('date_range_portfolio_' + Portfolio.ProcessPortfolioId) ? LocalStorageService.get('date_range_portfolio_' + Portfolio.ProcessPortfolioId).s : undefined,
			e: AllocationService.portfolioResourceCalendardates[StateParameters.portfolioId] ? AllocationService.portfolioResourceCalendardates[StateParameters.portfolioId].e : LocalStorageService.get('date_range_portfolio_' + Portfolio.ProcessPortfolioId) ? LocalStorageService.get('date_range_portfolio_' + Portfolio.ProcessPortfolioId).e : undefined,
		};

		let setHeader = function () {
			$scope.header.title = Translator.translation(Portfolio.LanguageTranslation);
			if ($scope.visualMode == 8 && typeof $scope.calendarDates.s != 'undefined' && typeof $scope.calendarDates.e != 'undefined') {
				$scope.header.title = $scope.header.title + "( " + CalendarService.formatDate($scope.calendarDates.s) + " - " + CalendarService.formatDate($scope.calendarDates.e) + ")";
			}
		}

		function handleRangeIcon(type) {
			if (type == 8 && !iconExists) {
				iconExists = true;
				ViewService.addDownloadable('GET_ALLOCATION_EXCEL_PORTFOLIO', function () {
					downloadExcel();
				}, StateParameters.ViewId);
				$scope.header.icons.push({
					icon: 'settings', onClick: function () {
						let mm = [{
							name: Translator.translate('ALLOCATION_SHOW_NORMAL'),
							onClick: function () {
								$scope.toggleFte('hours')
							}
						},
							{
								name: Translator.translate('ALLOCATION_SHOW_DAY_MODE'),
								onClick: function () {
									$scope.toggleFte(StateParameters.process == 'user' ? 'resource_allocation' : 'project_allocation')
								}
							},
							{
								name: Translator.translate('ALLOCATION_SHOW_FTE'),
								onClick: function () {
									$scope.toggleFte(StateParameters.process == 'user' ? 'resource_allocation_fte' : 'project_allocation_fte')
								}
							},
							{
								name: Translator.translate('ALLOCATION_SET_DATE_RANGE'),
								onClick: function () {
									let model = angular.copy($scope.calendarDates);
									let endsAfter = 'endsAfter';
									let params = {
										title: 'ALLOCATION_LIMIT_TITLE',
										inputs: [
											{
												type: 'daterange',
												label: 'ALLOCATION_LIMIT_DATE',
												modelStart: 's',
												modelEnd: 'e'
											},
										],
										buttons: [
											{
												type: 'secondary',
												text: 'MSG_CANCEL'
											},
											{
												type: 'primary',
												text: 'CLEAR',
												onClick: function () {
													$scope.calendarDates = {
														s: undefined,
														e: undefined
													};
													StateParameters.endsAfter = model.endsAfter;
													AllocationService.portfolioResourceCalendardates[StateParameters.portfolioId] = $scope.calendarDates;
													setHeader();
												}
											},
											{
												type: 'primary',
												text: 'FILTER',
												onClick: function () {
													StateParameters.endsAfter = model.endsAfter;
													$scope.calendarDates = model;
													AllocationService.portfolioResourceCalendardates[StateParameters.portfolioId] = $scope.calendarDates;
													setHeader();
													LocalStorageService.store('date_range_portfolio_' + Portfolio.ProcessPortfolioId, $scope.calendarDates);
												}
											}]
									};
									MessageService.dialog(params, model);
								}
							}
						]

						if (StateParameters.DisableFte && StateParameters.DisableFte == 1) {
							mm.splice(0, 1);
							mm.splice(0, 1);
							mm.splice(0, 1);
						}

						MessageService.menu(mm, event.currentTarget);
					},
				})

			} else if (type != 8 && iconExists) {
				iconExists = false;
				ViewService.removeDownloadable(StateParameters.ViewId);
				$scope.header.icons.splice($scope.header.icons.length - 1, 1)
			}
		}

		function downloadExcel() {
			AllocationService.setDateRange(Portfolio.LanguageTranslation[$rootScope.clientInformation.locale_id], StateParameters.process, Portfolio.ProcessPortfolioId, ActiveSettings.filters, StateParameters.AdditionalFields, AllocationService.getModeName(), []);
		}

		$scope.dashb = null;
		$scope.chartProcess = StateParameters.process;

		setSelectedViewType("portfolio", null);

		function setNavigation(settings) {
			if (!$scope.frontpage) {
				let sidenav = [];

				if (recursive || showBoard || showGroup || showRelations || showProportional || showPriorisation || showSundial || showResources) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_FV', //"Flat view",
							icon: 'format_align_justify',
							onClick: function () {
								lockFlat(0, true);
								$scope.visualMode = 0;
								showCalendar();
								ViewService.showFootbar(StateParameters.ViewId);
								setHeader();
							},
							$$activeTab: $scope.visualMode == 0,
							type: 0,
							subtype: 0
						}
						]
					});

				}

				if (recursive) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_RECURSIVE_V',//"Recursive view",
							icon: 'format_align_center',
							onClick: function () {
								unlockFlat(1, true);
								$scope.visualMode = 1;
								showCalendar();
								ViewService.showFootbar(StateParameters.ViewId);
								setHeader();
							},
							$$activeTab: $scope.visualMode == 1 && !flatLock,
							type: 1,
							subtype: 1
						}]
					});

				}

				if (showGroup) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_GV',//"Group view",
							icon: 'view_week',
							onClick: function () {
								$scope.showCalendar = false;
								lockFlat(7, true);
								$scope.visualMode = 7;
								ViewService.showFootbar(StateParameters.ViewId);
								setHeader();
							},
							type: 7,
							$$activeTab: $scope.visualMode == 7
						}]
					});

				}

				if (showResources) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_RS',//"roadmap",
							icon: 'equalizer',
							onClick: function () {
								lockFlat(8, true);
								$scope.visualMode = 8;
								showCalendar();
								setHeader();
								// ViewService.showFootbar(StateParameters.ViewId);
							},
							type: 8,
							$$activeTab: $scope.visualMode == 8
						}]
					});
				}

				if (showBoard) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_BV',//"roadmap",
							icon: 'view_compact',
							onClick: function () {
								lockFlat(5, true);
								$scope.visualMode = 5;
								showCalendar();
								ViewService.showFootbar(StateParameters.ViewId);
								setHeader();
							},
							type: 5,
							$$activeTab: $scope.visualMode == 5
						}]
					});

				}

				if (showRelations) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_RV',//"Relations view",
							icon: 'share',
							onClick: function () {
								unlockFlat(2, true);

								$scope.visualMode = 2;
								setHeader();
								ViewService.hideFootbar(StateParameters.ViewId);
							},
							type: 2,
							$$activeTab: $scope.visualMode == 2
						}]
					});

				}

				if (showProportional) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_PV',//"Proportional view",
							icon: 'bubble_chart',
							onClick: function () {
								unlockFlat(3, true);
								$scope.visualMode = 3;
								setHeader();
								ViewService.hideFootbar(StateParameters.ViewId);
							},
							type: 3,
							$$activeTab: $scope.visualMode == 3
						}]
					});

				}

				if (showPriorisation) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_P',//"Prioritization",
							icon: 'format_list_numbered',
							onClick: function () {
								unlockFlat(4, true);
								setHeader();
								$scope.visualMode = 4;
								ViewService.hideFootbar(StateParameters.ViewId);
							},
							type: 4,
							$$activeTab: $scope.visualMode == 4
						}]
					});

				}

				if (showSundial) {
					sidenav.push({
						tabs: [{
							name: 'PORTFOLIO_SD',//"Prioritization",
							icon: 'format_list_numbered',
							onClick: function () {
								lockFlat(6, true);
								setHeader();
								$scope.visualMode = 6;
								ViewService.showFootbar(StateParameters.ViewId);
							},
							type: 6,
							$$activeTab: $scope.visualMode == 6
						}]
					});
				}

				if (!_.isEmpty(Dashboards)) {
					let nav = {
						name: 'DASHBOARDS_IN_PORTFOLIO',
						icon: "pie_chart",
						tabs: []
					};

					_.each(Dashboards, function (dashboard) {
						nav.tabs.push({
							name: Translator.translation(dashboard.LanguageTranslation),
							onClick: function () {
								for (let a of dashboard.ActualCharts) {
									a.DashboardPortfolioId = StateParameters.portfolioId;
								}
								$scope.visualMode = 69;
								$scope.dashb = dashboard;
								unlockFlat(69, true);
							},
							$$activeTab: $scope.visualMode == 69
						})
					})

					sidenav.push(nav);
				}

				let publicTabs = [];
				let myTabs = [];

				angular.forEach(settings, function (s) {
					if (s.Value.showInMenu || s.Public == 3) {
						let t = {
							hideSplit: true,
							name: Translator.translation(s.Name),
							menu: undefined,
							$$activeTab: s.ConfigurationId == LocalStorageService.get('selectedConfiguration_' + s.Identifier),
							onClick: function () {
								_.each(publicTabs, function (tab) {
									tab.$$activeTab = false;
								})

								_.each(myTabs, function (tab2) {
									tab2.$$activeTab = false;
								})
								activeTab = t;
								activeTab.$$activeTab = true;
								LocalStorageService.store('selectedConfiguration_' + s.Identifier, s.ConfigurationId);
								recursive = s.PortfolioDefaultView == 1

								if (s.Public == 3) PortfolioService.clearActiveSettings(StateParameters.portfolioId);

								angular.forEach(s.Value.filters, function (value, key) {
									ActiveSettings.filters[key] = angular.copy(value);
									if (!s.Value.filters.hasOwnProperty('excludeItemIds')) {
										ActiveSettings.filters['excludeItemIds'] = [];
									}
								});

								ActiveSettings.priorization = angular.copy(s.Value.priorization);
								$scope.header.extTitle = t.name;
								LocalStorageService.store('portfolioExtTitle_' + s.Identifier, t.name);

								if (s.DashBoardId != 0 && Dashboards.length) {
									$scope.dashb = _.find(Dashboards, function (o) {
										return o.Id == s.DashBoardId;
									});
									unlockFlat(69, true);
									$scope.visualMode = 69;
								} else {
									setSelectedViewType(t.portfolioDefaultView, s.ConfigurationId);
								}

								$scope.getRows();

								$rootScope.$emit('notifyFilterPhases', {});

								if (s.Value.chartFilters) {
									ActiveSettings.chartFilters = s.Value.chartFilters;
								} else {
									ActiveSettings.chartFilters = {};
								}

								StateParameters.activeSettings = angular.copy(ActiveSettings);
								StateParameters.activeTitle = t.name;
							},

							portfolioDefaultView: s.PortfolioDefaultView
						};

						if (s.Public == 0 && s.UserId == UserService.getCurrentUserId()) {
							myTabs.push(t);
						} else {
							PortfolioService.setPortfoliosVisibility(s, UserGroups, Rights, ItemColumns, t, publicTabs);
						}

						if ((Rights.indexOf('public') !== -1 && (s.Public == 1 || s.Public == 2 || s.Public == 4)) || (s.UserId == UserService.getCurrentUserId() && s.Public != 3)) {
							t.menu = [{
								icon: 'settings',
								name: 'CONFIGURE',
								onClick: function () {
									ViewService.view('configure.portfolioSimple', {
										params: {
											NavigationMenuId: $scope.navigationMenuId,
											portfolioId: StateParameters.portfolioId,
											isNew: false,
											process: StateParameters.process,
											ActiveSettings: ActiveSettings
										},
										locals: {
											CurrentSettings: s,
											ReloadNavigation: $scope.reloadNavigation,
											RefreshParent: refreshParent
										}
									});
								}
							}];
						}
					}
				});

				if (myTabs.length || publicTabs.length) sidenav.push({
					name: "FILTERS",
					tabs: [],
					showGroup: true,
					expand: false,
					Subtitle: true
				});
				if (myTabs.length) sidenav.push({name: 'PORTFOLIO_SAVED', showGroup: true, tabs: myTabs, expand: true});
				if (publicTabs.length) sidenav.push({
					name: 'PORTFOLIO_SAVED_PUBLIC',
					showGroup: true,
					tabs: publicTabs,
					expand: true,
				});
				SidenavService.navigation(sidenav, undefined, true);
			}
		}

		function refreshParent() {
			ViewService.refresh(StateParameters.ViewId);
		}

		function lockFlat(s, getRows?) {
			handleRangeIcon(s);
			flatLock = true;
			if (s != 10) LocalStorageService.store('pViewTf_' + Portfolio.ProcessPortfolioId, s);
			SidenavService.setActivePortfolioTab(s, flatLock);
			if (recursive && getRows) {
				recursive = false;
				$scope.getRows();
			}
		}

		function unlockFlat(s, getRows?) {
			handleRangeIcon(s);
			flatLock = false;
			LocalStorageService.store('pViewTf_' + Portfolio.ProcessPortfolioId, s);
			SidenavService.setActivePortfolioTab(s, flatLock);
			if ($scope.visualMode != 1 && getRows) {
				recursive = true;
				$scope.getRows();
			}
		}

		let s;

		function setSelectedViewType(fromWhere, configurationId) {
			if (fromWhere == "portfolio") {
				if (typeof LocalStorageService.get('pViewTf_' + Portfolio.ProcessPortfolioId) != 'undefined' && !$scope.frontpage) {
					s = LocalStorageService.get('pViewTf_' + Portfolio.ProcessPortfolioId)
					LocalStorageService.clearKey('pViewTf_' + Portfolio.ProcessPortfolioId);
				} else {
					s = $scope.visualMode;
				}
			} else {
				s = fromWhere;
				if (s != 10) LocalStorageService.store('pViewTf_' + Portfolio.ProcessPortfolioId, s);

			}

			if (s == -1 && $scope.visualMode == 69) $scope.visualMode = 0;

			if (s == 100) {
				lockFlat(s);
				$scope.visualMode = 1;
				showCalendar();
				ViewService.showFootbar(StateParameters.ViewId);
			} else if (s == 0) {
				lockFlat(s);
				$scope.visualMode = 0;
				showCalendar();
				ViewService.showFootbar(StateParameters.ViewId);
			} else if (s == 1) {
				unlockFlat(s);
				$scope.visualMode = 1;
				showCalendar();
				ViewService.showFootbar(StateParameters.ViewId);
			} else if (s == 7) {
				$scope.showCalendar = false;
				lockFlat(s);
				$scope.visualMode = 7;
				ViewService.showFootbar(StateParameters.ViewId);
			} else if (s == 5) {
				lockFlat(s, true);
				$scope.visualMode = 5;
				showCalendar();
				ViewService.showFootbar(StateParameters.ViewId);
			} else if (s == 2) {
				unlockFlat(s);

				$scope.visualMode = 2;
				ViewService.hideFootbar(StateParameters.ViewId);
			} else if (s == 3) {
				unlockFlat(s);

				$scope.visualMode = 3;
				ViewService.hideFootbar(StateParameters.ViewId);
			} else if (s == 4) {
				unlockFlat(s);

				$scope.visualMode = 4;
				ViewService.hideFootbar(StateParameters.ViewId);
			} else if (s == 6) {
				lockFlat(s);
				$scope.visualMode = 6;
				ViewService.showFootbar(StateParameters.ViewId);
			} else if (s == 8) {
				lockFlat(8, true);
				$scope.visualMode = 8;
				showCalendar();
			}
			setHeader();
		}
	}

	PortfolioExcelDataController.$inject = ['$scope', '$rootScope', 'Files', 'StateParameters', 'ExcelImportService', 'ExportExcel', 'MessageService', 'Translator', '$q', 'BackgroundProcessesService', 'AttachmentService'];

	function PortfolioExcelDataController($scope, $rootScope, Files, StateParameters, ExcelImportService, ExportExcel, MessageService, Translator, $q, BackgroundProcessesService, AttachmentService) {
		$scope.files = Files;
		$scope.process = StateParameters.process;
		$scope.group = 'Excel_' + $rootScope.clientInformation.item_id;
		$scope.fileId = 0;
		$scope.loading = false;

		$scope.filesChanged = function () {
			let promises = [];
			promises.push(ExcelImportService.getFile(StateParameters.process, $scope.group));

			return $q.all(promises).then(function (resolves) {
				MessageService.dialogAdvanced({
					template: 'core/portfolios/views/portfolioExcelExportImport.html',
					controller: 'PortfolioExcelDataController',
					locals: {
						Files: resolves[0],
						StateParameters: StateParameters,
						ExportExcel: ExportExcel
					}
				});
			});
		}

		$scope.cancel = function () {
			if ($scope.files.length > 0) AttachmentService.deleteAttachment($scope.files[0].AttachmentId);

			MessageService.closeDialog();
		}

		$scope.statustext = "";

		$scope.loaded = false;
		$scope.exportData = function () {
			if ($scope.files.length > 0) {
				$scope.statustext = Translator.translate("VIEW_PLEASE_WAIT");
				let bgParams = {'Type': 2};
				bgParams['FileId'] = $scope.files[0].AttachmentId;
				bgParams['Process'] = StateParameters.process;
				bgParams['PortfolioId'] = StateParameters.portfolioId;
				bgParams['q'] = {}
				bgParams["FileName"] = StateParameters.process + " portfolio excel";
				BackgroundProcessesService.executeOnDemand("SetDataWithExcel", bgParams, 'BG_PROCESS_STARTED_2');
				MessageService.closeDialog();
			}
		}
	}
})();