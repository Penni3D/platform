(function () {
	'use strict';
	angular
		.module('core.process')
		.directive('portfolioSundial', portfolioSundialDirective);

	portfolioSundialDirective.$inject = ['PortfolioService', 'ViewService', 'Common', 'Colors', 'ProcessService', '$q'];

	function portfolioSundialDirective(PortfolioService, ViewService, Common, Colors, ProcessService, $q) {
		return {
			restrict: 'E',
			templateUrl: 'core/portfolios/views/portfolioSundial.html',
			replace: true,
			scope: {
				process: '@',
				portfolioId: '@',
				rows: '<',
				processes: '<',
				columns: '=',
				onChange: '&'
			},
			controller: function ($scope, $element) {
				// Variables
				var width = 650;
				var height = 650;
				var radius = Math.min(width, height) / 2;
				var color = d3.scaleOrdinal(d3.schemeCategory20b);
				var color2 = d3.scaleOrdinal(d3.schemeCategory20b);

				d3.selectAll('button').style("background-color",
					color2()
				);

				// Size our <svg> element, add a <g> element, and move translate 0,0 to the center of the element.
				d3.select("svg").remove();
				var g = d3.select($element[0]).append("svg")
					.attr('width', width)
					.attr('height', height)
					.append('g')
					.attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

				// Create our sunburst data structure and size it.
				var partition = d3.partition()
					.size([2 * Math.PI, radius]);
				
				function draw(data) {
					// Find the root node, calculate the node.value, and sort our nodes by node.value
					$scope.root = d3.hierarchy(data)
						.sum(function (d) { return d.size; })
						.sort(function (a, b) { return b.value - a.value; });

					// Calculate the size of each arc; save the initial angles for tweening.
					partition($scope.root);
					var arc = d3.arc()
						.startAngle(function (d) { d.x0s = d.x0; return d.x0; })
						.endAngle(function (d) { d.x1s = d.x1; return d.x1; })
						.innerRadius(function (d) { return d.y0; })
						.outerRadius(function (d) { return d.y1; });

					// Add a <g> element for each node; create the slice variable since we'll refer to this selection many times
					var slice = g.selectAll('g.node').data($scope.root.descendants(), function(d) { if (d.data.level < 3) return d.data.name; return ""; }); // .enter().append('g').attr("class", "node");
					$scope.newSlice = slice.enter().append('g').attr("class", "node").merge(slice);
					slice.exit().remove();

					// Append <path> elements and draw lines based on the arc calculations. Last, color the lines and the slices.
					slice.selectAll('path').remove();
					$scope.newSlice.append('path').attr("display", function (d) { return d.depth ? null : "none"; })
						.attr("d", arc)
						.style('stroke', '#fff')
						.style("fill", function (d) { return d.data.color });

					// Populate the <text> elements with our data-driven titles.
					slice.selectAll('text').remove();
					$scope.newSlice.append("text")
						.attr("transform", function(d) {
							return "translate(" + arc.centroid(d) + ")rotate(" + computeTextRotation(d) + ")"; })
						.attr("dx", "-50")
						.attr("dy", ".5em")
						.text(function(d) { if (d.data.level < 3) return d.data.name; return "";  });

					$scope.newSlice.on("click", highlightSelectedSlice);
				}

				d3.selectAll(".showSelect").on("click", showTopTopics);
				d3.selectAll(".sizeSelect").on("click", sliceSizer);

				// Redraw the Sunburst Based on User Input
				function highlightSelectedSlice(c,i) {
					var clicked = c;
					var rootPath = clicked.path($scope.root).reverse();
					rootPath.shift(); // remove root node from the array

					$scope.newSlice.style("opacity", 0.4);
					var doit = false;
					$scope.newSlice.filter(function(d) {
						if (d === clicked && d.prevClicked) {
							d.prevClicked = false;
							$scope.newSlice.style("opacity", 1);
							return true;

						} else if (d === clicked) {
							d.prevClicked = true;
							doit = true;
							return true;
						} else {
							d.prevClicked = false;
							return (rootPath.indexOf(d) >= 0);
						}
					}).style("opacity", 1);

					//TODO: get rid of all this
					var d = c;
					//Clear Filters
					if (typeof d.data.item_id !== 'undefined') {
						var portfolioSettings = PortfolioService.getActiveSettings($scope.portfolioId);
						portfolioSettings.filters.text = '';

						angular.forEach(portfolioSettings.filters.select, function (filter, key) {
							portfolioSettings.filters.select[key] = [];
						});

						angular.forEach(portfolioSettings.filters.range, function (filter, key) {
							portfolioSettings.filters.range[key].start = null;
							portfolioSettings.filters.range[key].end = null
						});

						angular.forEach(portfolioSettings.filters.dateRange, function (filter, key) {
							portfolioSettings.filters.range[key].start = null;
							portfolioSettings.filters.range[key].end = null
						});
						portfolioSettings.filters.phase = [];

						//Set Portfolio Filters
						if (doit && d.data.item_id > 0) {
							if (d.data.is_parent) {
								portfolioSettings.filters.select[col1_item_column_id] = [d.data.item_id];
							} else {
								portfolioSettings.filters.select[col1_item_column_id] = [d.parent.data.item_id];
								portfolioSettings.filters.select[col2_item_column_id] = [d.data.item_id];
							}
						}

						$scope.onChange()();
					}

				};

				// Redraw the Sunburst Based on User Input
				function sliceSizer(r, i) {

					// Determine how to size the slices.
					if (this.value === "size") {
						$scope.root.sum(function (d) { return d.size; });
					} else {
						$scope.root.count();
					}
					$scope.root.sort(function(a, b) { return b.value - a.value; });

					partition($scope.root);

					$scope.newSlice.selectAll("path").transition().duration(750).attrTween("d", arcTweenPath);
					$scope.newSlice.selectAll("text").transition().duration(750).attrTween("transform", arcTweenText);
				};

				// Redraw the Sunburst Based on User Input
				function showTopTopics(r, i) {
					//alert(this.value);
					var showCount;

					// Determine how to size the slices.
					if (this.value === "top3") {
						showCount = 3;
					} else if (this.value === "top6") {
						showCount = 6;
					} else {
						showCount = 100;
					}

					var showNodes = JSON.parse(JSON.stringify(allNodes));
					showNodes.children.splice(showCount, (showNodes.children.length - showCount));

					drawSunburst(showNodes);

				};        
				
				function hexToRgb(hex) {
					// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
					var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
					hex = hex.replace(shorthandRegex, function(m, r, g, b) {
						return r + r + g + g + b + b;
					});

					var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
					return result ? {
						r: parseInt(result[1], 16),
						g: parseInt(result[2], 16),
						b: parseInt(result[3], 16)
					} : null;
				}
				
				function arcTweenPath(a, i) {

					var oi = d3.interpolate({ x0: a.x0s, x1: a.x1s }, a);

					function tween(t) {
						var b = oi(t);
						a.x0s = b.x0;
						a.x1s = b.x1;
						return arc(b);
					}

					return tween;
				}
				
				function arcTweenText(a, i) {

					var oi = d3.interpolate({ x0: a.x0s, x1: a.x1s }, a);
					function tween(t) {
						var b = oi(t);
						return "translate(" + arc.centroid(b) + ")rotate(" + computeTextRotation(b) + ")";
					}
					return tween;
				}
				
				function computeTextRotation(d) {
					var angle = (d.x0 + d.x1) / Math.PI * 90;

					// Avoid upside-down labels
					return (angle < 90 || angle > 270) ? angle : angle + 180;  // labels as rims
					//return (angle < 180) ? angle - 90 : angle + 90;  // labels as spokes
				}

				var promises = [];

				var list1 = 'list_list2';
				var list3 = 'list_reports';

				var col1 = 'strategy_connection';
				var col1_item_column_id = 1731;

				var col2 = 'connected_strategy';
				var col2_item_column_id = 1781;

				var col3 = 'type';
				
				var name_col = 'idea_name';
				var size_col = 'actual_total';
				var color_col = 'risk';

			
				//promises.push(ProcessService.getSublists('development_process'));
				promises.push(ProcessService.getListItems(list1));
				//promises.push(PortfolioService.getPortfolioData('development_process',20));
				promises.push(ProcessService.getListItems(list3));

				var r = {
					"item_id": 0,
					"name": "",
					"children": [],
					"color": Colors.getColor('accent-2').rgb,
					"level": 0
				};

				$q.all(promises).then(function (result) {
					//var structure = result[0];
					var parentListItems = result[0];

					var childListItems = [];
					var childListIds = [];

					angular.forEach($scope.rows, function (row) {
						if (row.connected_strategy) {
							angular.forEach(row.connected_strategy, function(id) {
								childListIds.push(id);
							});
						}
					});

					var colorarray = [
						{
							color: "#1b5e20",
							childcolors: ["#2e7d32","#388e3c","#43a047","#4caf50"]
						},
						{
							color: "#0d47a1",
							childcolors: ["#1565c0","#1976d2","#1e88e5","#2196f3"]
						},
						{
							color: "#b71c1c",
							childcolors: ["#c62828","#d32f2f","#e53935","#f44336"]
						}
					];
					var cx = 0;
					var mx = 0;
					angular.forEach(_.uniq(childListIds), function (id) {
						
						var row = $scope.processes[id];
						var i = {
							item_id: angular.isDefined(row.actual_item_id) ? row.actual_item_id : row.item_id,
							list_item: row.idea_name,
							list_symbol_fill: 'amber'
						};
						mx += 1;
						cx += 1;						
						childListItems.push(i);
					});
					
					var colorItems = result[1];
					var cache = {};
					angular.forEach(parentListItems, function (item) {
						cache[item.item_id] = item;
					});
					angular.forEach(childListItems, function (item) {
						cache[item.item_id] = item;
					});
					angular.forEach(colorItems, function (item) {
						cache[item.item_id] = item;
					});
					
					cx = 0;mx=0;
					angular.forEach(parentListItems, function (item) {
						var item_id = item.item_id;
						var childs = {
							item_id: item_id,
							is_parent: true,
							name: cache[item_id].list_item,
							children: [],
							//color: hexToRgb(colorarray[mx].color),
							color: Colors.getColor(cache[item_id].list_symbol_fill).rgb,
							level: 1
						};
						mx += 1;
						angular.forEach(childListItems, function (subitem) {
							
							
							var sub_item_id = subitem.item_id;
							var subchilds = {
								item_id: sub_item_id,
								is_parent: false,
								name: cache[sub_item_id].list_item,
								children: [],
								color: Colors.getColor(cache[sub_item_id].list_symbol_fill).rgb,
								level: 2
							};

							//Check that this strategy has been connected to this company as least once
							var add = false;
							angular.forEach($scope.rows, function (pro) {
								var company_id = 0;
								var strategy_id = 0;
								var type_id = 0;
								if (pro[col2] != null && pro[col1].length > 0) company_id = pro[col1][0];
								if (pro[col2] != null && pro[col2].length > 0) strategy_id = pro[col2][0];
								if (pro[col3] != null && pro[col3].length > 0) type_id = pro[col3][0];

								if (company_id === item_id && strategy_id === sub_item_id && type_id === 4573) {
									add = true;
									return false;
								}
							});
							
							if (add) {
								childs.children.push(subchilds);
								angular.forEach($scope.rows, function (row) {
									angular.forEach(row[col2], function (selection) {
										if (selection === sub_item_id) {
											var c = Colors.getColor('grey').rgb;
											if (row[color_col][0]) {
												c = Colors.getColor(cache[row[color_col][0]].list_symbol_fill).rgb;
											}
											var o = {
												name: row[name_col],
												size: row[size_col],
												color: c,
												level: 3
											};
											subchilds.children.push(o);
										}
									});
								});
							}

						});
						r.children.push(childs);
					});
					draw(r);
				});

			}
		};
	}
})();
