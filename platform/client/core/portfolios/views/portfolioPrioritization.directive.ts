(function () {
	'use strict';
	angular
		.module('core.process')
		.directive('portfolioPrioritization', portfolioPrioritizationDirective);

	portfolioPrioritizationDirective.$inject = ['PortfolioService', 'ViewService', 'Common', 'ChartColourService', 'Translator', 'SettingsService', '$compile', '$templateRequest', '$sce'];

	function portfolioPrioritizationDirective(PortfolioService, ViewService, Common, ChartColourService, Translator, SettingsService, $compile, $templateRequest, $sce) {
		return {
			restrict: 'E',
			scope: {
				process: '@',
				portfolioId: '@',
				rows: '<',
				columns: '=',
				stateparams: '='
			},
			link: function ($scope, $element, $attrs) {
				let templateURL = 'core/portfolios/views/portfolioPrioritization.html';
				if ($scope.stateparams && $scope.stateparams.priorizationGraphPos === 'tab')
					templateURL = 'core/portfolios/views/portfolioPrioritizationTabs.html';

				$templateRequest(templateURL).then(function (html) {
					var template = angular.element(html);
					$element.append(template);
					$compile(template)($scope);
				});
			},
			controller: function ($scope) {
				_.each($scope.rows, function (item) {
					if (typeof item.actual_item_id === 'undefined' && item.item_id)
						item.actual_item_id = item.item_id;
				});
				$scope.legend = "";
				$scope.generateLegend = function () {
					setTimeout(function () {
						let chart = Common.getChartHandle('prio-bar-chart');
						if (chart) $scope.legend = $sce.trustAsHtml(chart.generateLegend());
					}, 0);
				}

				$scope.settingsVisible = false;

				$scope.displayMode = 'below';
				// if ($scope.stateparams && $scope.stateparams.priorizationGraphPos)
				// 	$scope.displayMode = $scope.stateparams.priorizationGraphPos;
				//if (activeSettings.priorization && activeSettings.priorization.useGraphPos) $scope.displayMode = 'split'; else $scope.displayMode = 'below';
				
				$scope.offsetInterval = 'day';
				if ($scope.stateparams && $scope.stateparams.priorizationOffsetInterval)
					$scope.offsetInterval = $scope.stateparams.priorizationOffsetInterval;

				$scope.selectedAddCharts = null;
				$scope.availableAddCharts = null;
				SettingsService.ProcessDashboardGetChartsForProcess($scope.process, true).then(function (charts) {
					$scope.availableAddCharts = charts;
					$scope.availableAddCharts.unshift({
						Variable: "PRIORITISATION_COMPARISON_CHART",
						Id: -1
					});
					$scope.availableAddCharts.unshift({
						Variable: "PRIORITISATION_COMPARISON_BUBBLES",
						Id: -2
					});
				});

				let orderNos = Common.fixOrders($scope.columns.length);
				$scope.hiddenRows = {};
				$scope.useOffsetDays = false;
				$scope.offsets = {};

				PortfolioService.getPortfolioColumnGroups($scope.process, $scope.portfolioId, false).then(function (groups) {
					let i = 0;
					$scope.itemColumns = _.map($scope.columns, function (item) {
						let thisColumnGroupName = "";

						if (item.ProcessPortfolioColumnGroupId != null && item.ProcessPortfolioColumnGroupId != 0) {
							thisColumnGroupName = _.find(groups, function (c) {
								return c.ProcessPortfolioColumnGroupId == item.ProcessPortfolioColumnGroupId;
							}).LanguageTranslation[$scope.$root.clientInformation.locale_id];
						}
						let t = item.ShortNameTranslation != null && item.ShortNameTranslation.hasOwnProperty($scope.$root.clientInformation.locale_id) ? item.ShortNameTranslation[$scope.$root.clientInformation.locale_id] + ' [' + thisColumnGroupName + ']' : Translator.translation(item.ItemColumn.LanguageTranslation);

						let dt = Common.getRelevantDataType(item.ItemColumn);
						let isNumber = dt == 1 || dt == 2 || dt == 14;

						let suffix = item.Validation && item.Validation.Suffix ? item.Validation.Suffix : item.ItemColumn.Validation.Suffix;

						return {
							Name: item.ItemColumn.Name,
							translation: t,
							OrderNo: orderNos[i++],
							IsNumber: isNumber,
							Suffix: suffix,
							DataType: item.ItemColumn.DataType,
							SubDataType: item.ItemColumn.SubDataType
						};
					});

					$scope.numberColumns = _.filter($scope.itemColumns, function (item) {
						return item.IsNumber;
					});

					
					//If Name column is not selected -- find it
					if (!$scope.selections.nameColumn) $scope.selections.nameColumn = _.get(_.find($scope.itemColumns, function (item) {
						return item.Name.toLowerCase() == 'title';
					}), 'Name');
					
					//If Value fields are not selected -- take first 2
					if (!$scope.selections.valueFields) $scope.selections.valueFields = _.map($scope.numberColumns.slice(0,3), function (item) {
						return item.Name;
					});

					if (!$scope.selectedAddCharts) $scope.selectedAddCharts = [-1];
				});

				let activeSettings = PortfolioService.getActiveSettings($scope.portfolioId);
				
				$scope.operators = [
					{
						variable: 'SUM',
						name: 'SUM',
						fn: function (values) {
							return _.sum(values);
						}
					},
					{
						variable: 'AVG',
						name: 'AVG',
						fn: function (values) {
							let sum = _.sum(values);
							return sum / values.length
						}
					},
					{
						variable: 'MEDIAN',
						name: 'MEDIAN',
						fn: function (values) {
							return findMedian(values);
						}
					}
				];

				$scope.sortOptions = [
					{
						name: 'SORT_BY_DESC',
						value: 1
					},
					{
						name: 'SORT_BY_ASC',
						value: 2
					}
				];

				function fetchSettings() {
					$scope.selections = activeSettings.priorization;
					$scope.selectedAddCharts = activeSettings.priorization ? activeSettings.priorization.SelectedAddCharts : null;
					$scope.hiddenRows = activeSettings.priorization && activeSettings.priorization.HiddenRows ? activeSettings.priorization.HiddenRows : {};
					$scope.useOffsetDays = activeSettings.priorization && activeSettings.priorization.useOffsets ? activeSettings.priorization.useOffsets : false;
					$scope.offsets = activeSettings.priorization && activeSettings.priorization.offsets ? activeSettings.priorization.offsets : {};
					$scope.useGraphPos = activeSettings.priorization && activeSettings.priorization.useGraphPos ? activeSettings.priorization.useGraphPos : false;
				}

				fetchSettings();

				if (!$scope.selections) {
					$scope.selections = {
						operator: $scope.operators[0].name,
						operatorsForColumns: {}
					};
				}


				function storeSettings() {
					activeSettings.priorization = $scope.selections;
					activeSettings.priorization.SelectedAddCharts = $scope.selectedAddCharts;
					activeSettings.priorization.HiddenRows = $scope.hiddenRows;
					activeSettings.priorization.useOffsets = $scope.useOffsetDays;
					activeSettings.priorization.offsets = $scope.offsets;
					activeSettings.priorization.useGraphPos = $scope.useGraphPos;

					PortfolioService.saveActiveSettings($scope.portfolioId);
				}


				function updateCharts() {
					let listToUse = [];
					if (_.isEmpty($scope.rowsSelected)) { // this practically is never the case
						listToUse = $scope.itemsThatCutTheLimit;
					} else {
						listToUse = $scope.rowsSelected;
					}
					listToUse = _.sortBy(listToUse, '$index');

					$scope.chartsAvailable = false;
					$scope.showBubbleChart = false;
					
					if (_.isEmpty($scope.valueColumns) === false) {
						let columnNames = _.map($scope.valueColumns, 'Name');
						let colTranslations = _.map(columnNames, function (col) {
							let colObj = _.find($scope.itemColumns, {Name: col});
							return colObj ? colObj.translation : col;
						});
						let suffixes = _.map(columnNames, function (col) {
							let colObj = _.find($scope.itemColumns, {Name: col});
							return colObj ? colObj.Suffix : null;
						});
						
						// bar chart
						if ($scope.selectedAddCharts.includes(-1)) {
							$scope.chartsAvailable = true;

							let joinBarY = true;
							for (let e = 0; e < suffixes.length; e++)
								if (suffixes[e] == null || (suffixes.length > e + 1 && suffixes[e] !== suffixes[e + 1]))
									joinBarY = false;

							let labels = [];
							_.each(listToUse, function (item) {
								labels.push(item.$index);
							});
							let yAxes = [];
							let datasetOverride = _.map(colTranslations, function (label, index) {
								if (joinBarY === false || index === 0) {
									let axisKey = 'y-axis-' + (index);
									let position = 'left';
									if (index >= 1) {
										position = 'right';
									}
									yAxes.push({
										id: axisKey,
										type: 'linear',
										display: true,
										position: position,
										gridLines: {
											display: true,
											drawBorder: false,
											lineWidth: 2,
											color: '#f0f1f4',
											zeroLineWidth: 2,
											zeroLineColor: '#f0f1f4'
										},
										ticks: {
											fontFamily: 'Roboto',
											fontSize: 12,
											fontColor: '#676a6e',
											beginAtZero: true
											, callback: function (value, ix, vals) {
												return Common.formatNumber(value, 0) + (suffixes[index] !== null ? ' ' + suffixes[index] : '');
											}
										},
										//scaleLabel: {
										//	display: suffixes[index] !== null,
										//	labelString: suffixes[index]
										//}
									});
									return {yAxisID: axisKey}
								}
							});


							let data = [];
							_.each(columnNames, function () {
								data.push([]);
							});

							_.each(listToUse, function (item) {
								_.each(columnNames, function (name, index) {
									data[index].push(item[name]);
								});
							});


							let barChartOptions = {
								legend: {display: false},
								legendCallback: function (chart) {
									return Common.getChartLegend(chart);
								},
								title: {display: true},
								maintainAspectRatio: false,
								scales: {
									yAxes: yAxes,
									xAxes: [
										{
											id: 'x-axis-1',
											display: true,
											gridLines: {display: false, drawBorder: true}
										}
									]
								},
								tooltips: {
									callbacks: {
										title: function (what) {
											let nameColumn = $scope.selections.nameColumn;
											let index = what[0].index;
											return listToUse[index][nameColumn];
										},
										label: function (tooltipItem, data) {
											return colTranslations[tooltipItem.datasetIndex] + ': ' + Common.formatNumber(tooltipItem.value, 0) + (suffixes[tooltipItem.datasetIndex] !== null ? ' ' + suffixes[tooltipItem.datasetIndex] : '');
										}
									}
								}
							};

							$scope.barChart = {
								type: 'bar',
								data: data,
								labels: labels,
								series: colTranslations,
								options: barChartOptions,
								datasetOverride: datasetOverride
							};
						}

						// bubble chart
						if ($scope.selectedAddCharts.includes(-2) && _.size($scope.valueColumns) > 1) {
							$scope.showBubbleChart = true;

							let colRef = {
								0: 'x',
								1: 'y',
								2: 'r'
							};

							let largestRadius = undefined;

							let bubbleData = _.map(listToUse, function (row) {
								let item = {};
								_.each(columnNames, function (name, index) {
									let axis = colRef[index];
									item[axis] = row[name];

									if (index === 2) {
										if (typeof largestRadius === 'undefined' || row[name] > largestRadius) {
											largestRadius = row[name];
										}
									}
								});

								return item;
							});

							if (typeof largestRadius === 'undefined') {
								largestRadius = 11;
							}

							largestRadius = Math.ceil(largestRadius) * 100;

							let maxRadius = 15;
							let minRadius = 5;
							let bracketCount = maxRadius - minRadius;


							let step = largestRadius / bracketCount;
							let brackets = [];
							for (let i = bracketCount - 1; i >= 0; i--) {
								brackets.push(Math.floor(largestRadius));
								largestRadius -= step;
							}

							brackets.reverse();

							angular.forEach(bubbleData, function (item) {
								if (typeof item.r === 'undefined') {
									item.r = minRadius;
								} else {
									for (let i = 0; i < bracketCount; i++) {
										if ((item.r * 100) < brackets[i]) {
											item.r = i + 5;
											break;
										}
									}

									if (i == bracketCount) {
										item.r = maxRadius;
									}
								}
							});

							let bubbleChartOptions = {
								scales: {
									xAxes: [{
										scaleLabel: {
											display: true,
											labelString: colTranslations[0]
										},
										ticks: {
											callback: function (value, ix, vals) {
												return Common.formatNumber(value, 0) + (suffixes[0] !== null ? ' ' + suffixes[0] : '');
											}
										}
									}],
									yAxes: [{
										scaleLabel: {
											display: true,
											labelString: colTranslations[1]
										},
										ticks: {
											callback: function (value, ix, vals) {
												return Common.formatNumber(value, 0) + (suffixes[1] !== null ? ' ' + suffixes[1] : '');
											}
										}
									}]
								},
								maintainAspectRatio: false,
								tooltips: {
									callbacks: {
										title: function (what) {
											let nameColumn = $scope.selections.nameColumn;
											let index = what[0].index;
											return listToUse[index][nameColumn];
										},
										label: function (what) {
											let target = listToUse[what.index];
											let tooltip = [];

											_.each(columnNames, function (name, index) {
												tooltip.push(colTranslations[index] + ': ' + Common.formatNumber(target[name], 0) + (suffixes[index] !== null ? ' ' + suffixes[index] : ''));
											});

											return tooltip;
										}
									}
								}
							};

							$scope.bubbleChart = {
								type: 'bubble',
								data: bubbleData,
								series: colTranslations,
								options: bubbleChartOptions,
								labels: null,
								datasetOverride: null
							};
						}
					}

					$scope.additionalCharts = [];
					_.each($scope.availableAddCharts, function (item) {
						if ($scope.selectedAddCharts !== null && $scope.selectedAddCharts.indexOf(item.Id) !== -1 && item.Id > 0) {
							let chart_item = [];
							chart_item['chart_id'] = item.Id;
							chart_item['selected_item_ids'] = _.join(_.map(listToUse, function (row) {
								return row.actual_item_id;
							}), ",");
							chart_item['chart_name'] = item.Variable;
							chart_item['process'] = $scope.process;
							chart_item['options'] = $scope.useOffsetDays ? {
								'offsets': $scope.offsets,
								'offset_interval': $scope.offsetInterval
							} : null;

							$scope.additionalCharts.push(chart_item);
						}
					});
					$scope.generateLegend();
				}

				function sortItems() {
					let selections = angular.copy($scope.selections.operatorsForColumns);
					let keysToSortBy = [];
					let itemsThatDontCutTheLimit = [];
					let itemsThatAreHidden = [];
					let columnOrder = {};
					let filtered = false;

					_.each(Object.keys(selections), function (objKey) { // remove any not visible column limits from comparison
						if ($scope.valueColumns.includes(undefined) === false && !_.find($scope.valueColumns, function (o) {
							return o.Name === objKey
						}))
							delete selections[objKey];
					});
					_.each($scope.selections.valueFields, function (col, index) {
						columnOrder[col] = index;
					});

					let sumsByKey = {};
					_.each(selections, function (item, key) {
						if (item.cumulativeSumLimit) {
							filtered = true;
							sumsByKey[key] = item.cumulativeSumLimit;
						}
					});

					let itemsThatCutTheLimit = _.filter($scope.rows, function (row) {
						let doesItCut = true;

						if ($scope.hiddenRows[row.item_id]) {
							itemsThatAreHidden.push(row);

							return false;
						}

						_.each(selections, function (rowSelection, key) {
							if (rowSelection.sortBy && !_.find(keysToSortBy, {key: key})) {
								keysToSortBy.push({
									key: key,
									desc: rowSelection.sortBy === 1,
									order: columnOrder[key]
								});
							}

							let limit = rowSelection.limit;
							if (_.isNumber(limit) && !_.isNaN(limit)) {
								filtered = true;

								if (rowSelection.sortBy === 2) {
									if (+row[key] > limit) {
										doesItCut = false;
									}
								} else {
									if (limit > +row[key]) {
										doesItCut = false;
									}
								}
							}
						});

						if (!doesItCut) {
							itemsThatDontCutTheLimit.push(row);
						}
						return doesItCut;
					});

					if (!_.isEmpty(keysToSortBy)) {
						keysToSortBy = _.sortBy(keysToSortBy, 'order');
						let orderFunction = function (v1, v2) {
							for (let i = 0, l = keysToSortBy.length; i < l; i++) {
								let k = keysToSortBy[i].key;
								let d = v1[k] - v2[k];

								if (keysToSortBy[i].desc) {
									d *= -1;
								}

								if (d) {
									return d;
								}
							}
						};

						$scope.itemsThatDontCutTheLimit = itemsThatDontCutTheLimit.sort(orderFunction);
						$scope.itemsThatCutTheLimit = itemsThatCutTheLimit.sort(orderFunction);
						$scope.itemsThatAreHidden = itemsThatAreHidden.sort(orderFunction);
					} else {
						let lowerSort = function (o) {
							let v = o[$scope.selections.nameColumn];
							return typeof v === 'string' ? v.toLowerCase() : v;
						};

						$scope.itemsThatDontCutTheLimit = _.sortBy(itemsThatDontCutTheLimit, lowerSort);
						$scope.itemsThatCutTheLimit = _.sortBy(itemsThatCutTheLimit, lowerSort);
						$scope.itemsThatAreHidden = _.sortBy(itemsThatAreHidden, lowerSort);
					}

					let leftOvers = [];

					$scope.itemsThatCutTheLimit = _.filter($scope.itemsThatCutTheLimit, function (row) {
						let doesItCut = true;
						_.each(sumsByKey, function (sumByKey, key) {
							sumsByKey[key] -= +row[key];

							if (sumsByKey[key] < 0) {
								doesItCut = false;
							}
						});
						if (doesItCut) {
							return true;
						} else {
							leftOvers.push(row);
							return false;
						}
					});

					$scope.itemsThatDontCutTheLimit = _.concat(leftOvers, $scope.itemsThatDontCutTheLimit);

					let indexNo = 0;

					_.each($scope.itemsThatCutTheLimit, function (item) {
						indexNo++;
						item.$index = indexNo;
					});

					_.each($scope.itemsThatDontCutTheLimit, function (item) {
						indexNo++;
						item.$index = indexNo;
					});

					_.each($scope.itemsThatAreHidden, function (item) {
						indexNo++;
						item.$index = indexNo;
					});

					$scope.filtered = filtered;
					updateCharts();
				}

				function buildColumnList() {
					let nameColumn = findColumn($scope.selections.nameColumn);
					$scope.valueColumns = _.chain($scope.selections.valueFields)
						.map(function (colName) {
							let key = 'operatorsForColumns.' + colName + '.operator';
							if (_.isUndefined(_.get($scope.selections, 'operatorsForColumns.' + colName))) {
								_.set($scope.selections, 'operatorsForColumns.' + colName + '.sortBy', 1);
							}
							if (!_.has($scope.selections, key)) {
								$scope.selections.operatorsForColumns = $scope.selections.operatorsForColumns || {};
								_.set(
									$scope.selections, key,
									_.first($scope.operators).name);
							}
							return findColumn(colName);
						})
						.value();

					$scope.visibleColumns = _.uniqBy(_.concat([], nameColumn, $scope.valueColumns), 'Name');

					let valuesForColumns = {};
					let valuesForSelectedRows = {};
					_.each($scope.rows, function (row) {
						_.each($scope.selections.operatorsForColumns, function (colName, value) {
							valuesForColumns[value] = valuesForColumns[value] || [];
							valuesForColumns[value].push(+row[value]);

							valuesForSelectedRows[value] = valuesForSelectedRows[value] || [];
							if (!$scope.hiddenRows[row.item_id]) {
								valuesForSelectedRows[value].push(+row[value]);
							}
						});
					});

					_.each($scope.selections.operatorsForColumns, function (colName, value) {
						let operator = _.find($scope.operators, {name: $scope.selections.operator});
						if (operator) {
							let valuesForKey = valuesForColumns[value];
							let valuesForSelected = valuesForSelectedRows[value];
							if (!_.isEmpty(valuesForKey)) {
								valuesForColumns[value] = Math.round(100 * operator.fn(valuesForKey)) / 100;
							} else {
								valuesForColumns[value] = null;
							}

							if (!_.isEmpty(valuesForSelected)) {
								valuesForSelectedRows[value] = Math.round(100 * operator.fn(valuesForSelected)) / 100;
							} else {
								valuesForSelectedRows[value] = null;
							}
						} else {
							valuesForColumns[value] = null;
							valuesForSelectedRows[value] = null;
						}
					});

					$scope.values = valuesForColumns;
					$scope.valuesForSelected = valuesForSelectedRows;

					sortItems();
				}

				function refresh() {
					$scope.rowsSelected = _.filter($scope.rows, function (row) {
						if ($scope.hiddenRows[row.item_id]) {
							return false;
						}

						return true;
					});
					$scope.offsets = _.pickBy($scope.offsets, _.isFinite);

					buildColumnList();
					storeSettings();
				}

				refresh();

				$scope.formatNumber = function (value, column) {
					if (!column) return '';
					return column.IsNumber ? Common.formatNumber(value[column.Name], 2) : value[column.Name];
				};

				let timer;
				$scope.selectionsChanged = function (faster) {
					clearTimeout(timer);
					timer = setTimeout(refresh, faster ? 150 : 600);
					if (activeSettings.priorization && activeSettings.priorization.useGraphPos) $scope.displayMode = 'below'; else $scope.displayMode = 'split';
					
				};
				$scope.selectionsChanged(true);

				function findColumn(colName) {
					return _.find($scope.itemColumns, function (item) {
						return item.Name === colName;
					});
				}

				function findMedian(valuesForKey) {
					let sortedList = _.sortBy(valuesForKey);
					let listLength = sortedList.length;

					if (listLength % 2 === 0) {
						return (sortedList[(listLength / 2) - 1] + sortedList[listLength / 2]) / 2;
					}

					return sortedList[(listLength - 1) / 2];
				}

				$scope.syncOrder = function (current, prev) {
					let valueFields = $scope.selections.valueFields;
					let l = valueFields.length;


					for (let i = 0; i < l; i++) {
						if (valueFields[i] === current.Name) {
							valueFields.splice(i, 1);
							break;
						}
					}

					if (prev) {
						while (l--) {
							if (prev.Name === valueFields[l]) {
								valueFields.splice(++l, 0, current.Name);
								break;
							}
						}
					} else {
						valueFields.unshift(current.Name);
					}

					buildColumnList();
					storeSettings();
				};

				$scope.showRow = function (row) {
					if (typeof $scope.hiddenRows[row.item_id] !== 'undefined')
						delete $scope.hiddenRows[row.item_id];
					$scope.selectionsChanged(true);
					storeSettings();
				};
				$scope.hideRow = function (row) {
					$scope.hiddenRows[row.item_id] = true;
					$scope.selectionsChanged(true);
					storeSettings();
				};
				$scope.clearHiddens = function () {
					$scope.hiddenRows = {};
					$scope.selectionsChanged(true);
					storeSettings();
				};
				$scope.showClearHiddens = function () {
					return Object.keys($scope.hiddenRows).length > 0;
				};
				$scope.clearOffsets = function () {
					$scope.offsets = {};
					$scope.selectionsChanged(true);
					storeSettings();
				};
				$scope.showClearOffsets = function () {
					return Object.keys($scope.offsets).length > 0;
				};

				$scope.fullColors = _.map(ChartColourService.getColours(['cyan', 'orange', 'pink', 'purple', 'red']), function (col) {
					return {
						backgroundColor: col,
						pointBackgroundColor: col,
						hoverBackgroundColor: col,
						fill: false
					};
				});

				$scope.toggleSettings = function () {
					$scope.settingsVisible = !$scope.settingsVisible;
				}
			}
		};
	}
})();
