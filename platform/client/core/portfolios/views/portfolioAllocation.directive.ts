(function () {
	'use strict';

	angular
		.module('core.process')
		.directive('portfolioAllocation', PortfolioAllocation);

	PortfolioAllocation.$inject = ['AllocationService', '$rootScope', 'CellFactoryService', 'PortfolioService'];

	function PortfolioAllocation(AllocationService, $rootScope, CellFactoryService, PortfolioService) {
		return {
			scope: {
				rows: '=',
				process: '=',
				startDate: "=",
				endDate: "=",
				costCenter: "@?",
				activeSettings: "="
			},
			templateUrl: 'core/portfolios/views/portfolioAllocation.html',
			controller: function ($scope) {

			},
			link: function (scope) {
				CellFactoryService.reset();
				scope.$watchCollection('[startDate, endDate]', function (newValues, oldValues) {
					let diffTime = Math.abs(newValues[1] - newValues[0]);
					let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

					if (diffDays > 730) {
						let d = new Date(scope.startDate);
						d.setDate(scope.startDate.getDate() + 730);
						scope.endDate = d;
					}

					if (typeof newValues[0] != 'undefined' && newValues[1] != 'undefined' && (newValues[0] != oldValues[0] || newValues[1] != oldValues[1])){
						loadData();
					}
				});

				if (scope.process === "user") {
					scope.AMRules = {
						parents: {
							51: ['root'], //User under root
							52: [51], //Project under user
							50: [52] //allocation under project
						}
					};
					scope.allocationPortfolioColumns = [
						{
							ItemColumn: {
								DataType: 0,
								Name: 'primary',
								LanguageTranslation: 'ALLOCATION_NAME'
							},
							Width: 434,
							UseInSelectResult: 1
						},
						{
							UseInSelectResult: 0,
							ItemColumn: {
								Name: 'cost_center',
								LanguageTranslation: 'ALLOCATION_COST_CENTER',
								DataType: 6,
								DataAdditional: 'list_cost_center',
								showType: 'inline-block'
							},
							Width: 0
						},
						{
							ItemColumn: {
								DataType: 6,
								Name: 'organisation',
								LanguageTranslation: 'ALLOCATION_ORGANISATION',
								DataAdditional: 'organisation',
								showType: 'inline-block'
							},
							Width: 0,
							UseInSelectResult: 2
						},
						{
							UseInSelectResult: 0,
							ItemColumn: {
								Name: 'status_translation',
								LanguageTranslation: 'ALLOCATION_STATUS',
								DataType: 0,
								showType: 'inline-block'
							},
							Width: 0
						},
						{
							ItemColumn: {
								DataType: 0,
								Name: 'process_translation',
								LanguageTranslation: 'ALLOCATION_PROCESS',
								showType: 'inline-block'
							},
							Width: 0,
							UseInSelectResult: 2
						},
						{
							ItemColumn: {
								DataType: 0,
								Name: 'allocation_total',
								LanguageTranslation: 'ALLOCATION_TOTAL',
								showType: 'inline-block'
							},
							Width: 0,
							UseInSelectResult: 2
						}
					];
					scope.AMOptions = {
						calendarSteps: [15, 60, 360],
						inheritDates: false,
						showCalendar: true,
						sortable: false,
						selectable: false,
						hasDragRights: false,
						hasRequestRights: false,
						hasSendRights: false,
						hasWriteRights: false,
						hasManagerRights: false,
						hasOwnerRights: false,
						multipleResources: true,
						allowTaskJump: true,
						allowProjectJump: true,
						userResourcesMode: true,
						openRows: [],
						disableAutoOpen: true,
						ownerType: 'resource_allocation',
						fteDivider: 1
					};
				} else {
					scope.AMRules = {
						parents: {
							52: ['root'],
							51: [52],
							50: [51]
						}
					};
					scope.allocationPortfolioColumns = [
						{
							ItemColumn: {
								DataType: 0,
								Name: 'primary',
								LanguageTranslation: 'ALLOCATION_NAME'
							},
							Width: 384,
							UseInSelectResult: 1
						},
						{
							ItemColumn: {
								DataType: 0,
								Name: 'secondary',
								LanguageTranslation: 'ALLOCATION_NAME'
							},
							Width: 384,
							UseInSelectResult: 1
						},
						{
							UseInSelectResult: 0,
							ItemColumn: {
								Name: 'cost_center',
								LanguageTranslation: 'ALLOCATION_COST_CENTER',
								DataType: 6,
								DataAdditional: 'list_cost_center',
								showType: 'inline-block'
							},
							Width: 0
						},
						{
							ItemColumn: {
								DataType: 6,
								Name: 'organisation',
								LanguageTranslation: 'ALLOCATION_ORGANISATION',
								DataAdditional: 'organisation',
								showType: 'inline-block'
							},
							Width: 0,
							UseInSelectResult: 2
						},
						{
							UseInSelectResult: 0,
							ItemColumn: {
								Name: 'status_translation',
								LanguageTranslation: 'ALLOCATION_STATUS',
								DataType: 0,
								showType: 'inline-block'
							},
							Width: 0
						},
						{
							ItemColumn: {
								DataType: 0,
								Name: 'process_translation',
								LanguageTranslation: 'ALLOCATION_PROCESS',
								showType: 'inline-block'
							},
							Width: 0,
							UseInSelectResult: 2
						},
						{
							ItemColumn: {
								DataType: 0,
								Name: 'allocation_total',
								LanguageTranslation: 'ALLOCATION_TOTAL',
								showType: 'inline-block'
							},
							Width: 0,
							UseInSelectResult: 2
						}];
					scope.AMOptions = {
						calendarSteps: [15, 60, 360],
						inheritDates: false,
						ownerId: 0,
						showCalendar: true,
						sortable: false,
						selectable: false,
						hasWriteRights: false,
						hasManagerRights: true,
						multipleProjects: true,
						allowTaskJump: true,
						allowProjectJump: true,
						ownerType: 'project_allocation'
					};
				}
				scope.ready = false;

				function loadData() {
					let itemIds = scope.rows.filter(r => !isNaN(r.item_id)).map(r => r.item_id);
					let s = typeof scope.startDate == 'undefined' ? moment().add(-3, 'months').format('YYYY-MM-DD') : moment(scope.startDate).format('YYYY-MM-DD');
					let e = typeof scope.endDate == 'undefined' ? moment().add(1, 'years').format('YYYY-MM-DD') : moment(scope.endDate).format('YYYY-MM-DD');
					if (scope.process === "user") {
						let ids = [];
						_.each(itemIds, function(id){
							ids.push({'Process': 'user', 'ItemId': id})
						})
						if(scope.activeSettings.filters && scope.activeSettings.filters.select && scope.activeSettings.filters.select[scope.costCenter]){
							_.each(scope.activeSettings.filters.select[scope.costCenter], function(ccId){
								ids.push({'Process': 'list_cost_center', 'ItemId': ccId})
							})
						}
						AllocationService.getResourcesAllocationDataLimited(ids, s, e).then(function (response) {
							let allRows = _.flatten(_.map(response, function (r) {
								return r.rowdata;
							}));
							let sorted = _.orderBy(allRows, ['primary'], ['desc']);
							scope.allocationRows = AllocationService.filterAllocationsByStatus('resources', AllocationService.addTotals(sorted, 0, 10, false, false, true, "resource_allocation"));
							scope.ready = true;
						});
					} else {
						AllocationService.getProjectsAllocationsData(itemIds, s, e).then(function (response) {
							scope.allocationRows = AllocationService.filterAllocationsByStatus('projects', AllocationService.addTotals(response.rowdata, 0, 10, false, false, true, "project_allocation"));
							scope.ready = true;
						});
					}
				}

				loadData();
			}
		}
	}
})();