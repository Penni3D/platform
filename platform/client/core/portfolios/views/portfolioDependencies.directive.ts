(function () {
	'use strict';
	angular
	.module('core.process')
	.directive('portfolioDependencies', PortfolioDependenciesDirective);

	PortfolioDependenciesDirective.$inject = ['PortfolioService', 'ViewService', 'Common', 'Colors'];
	function PortfolioDependenciesDirective(PortfolioService, ViewService, Common, Colors) {
		return {
			restrict: 'E',
			templateUrl: 'core/portfolios/views/portfolioDependencies.html',
			replace: true,
			scope: {
				portfolioId: '=',
				rows: '@'
			},
			controller: function ($scope, $element) {
				var tree = {};
				var StateParameters = {}; //STATEPARAMETRIN ARVOT VÄLITETTÄVÄ DIREKTIIVILLE
				var $card = $element.closest('content');
				var svgHeight = 0;

				var onResize = function () {
					$element.height($card.height());
				};

				Common.subscribeContentResize($scope, onResize);
				PortfolioService
					.getPortfolioData(
						StateParameters.process,
						StateParameters.portfolioId)
					.then(function(rows) {
						_.each(rows.rowdata,function(r) {
							if (!_.isArray(r.children)) {
								r.children = [];
							}
							r.children = r.children.concat(_.filter(rows.rowdata, ['parent_item_id', r.item_id]));
						});

						var treeData = {
							"item_id": 0,
							"name": StateParameters.menuItem,
							"children": rows.rowdata
						};

						tree.treeIteration = 0;
						var levelWidth = [1];
						function childCount(level, n) {
							if (n.children && n.children.length > 0) {
								if (levelWidth.length <= level + 1) levelWidth.push(0);

								levelWidth[level + 1] += n.children.length;
								n.children.forEach(function (d) {
									childCount(level + 1, d);
								});
							}
						};
						childCount(0, treeData);
						svgHeight = d3.max(levelWidth) * 45;

						tree.svgScale = 0.33;
						if (svgHeight > $element.height()) {
							tree.svgScale = 1 - ($element.height() / svgHeight);
						}

						tree.svg = d3
							.select($element[0])
							.append("svg")
							.attr("preserveAspectRatio", "xMinYMin meet")
							.attr("viewBox", "0 0 640 480")
							.call(d3.zoom().scaleExtent([0.2, 2.0]).on("zoom", function () {
								tree.svg.attr("transform", d3.event.transform, d3.zoomIdentity.translate(100, 50).scale(tree.svgScale,tree.svgScale));
							}))
							.attr("transform","translate(100,50) scale(" + tree.svgScale + "," + tree.svgScale + ")")
							.append("g");


						tree.treemap = d3.tree().size([$card.width(), svgHeight]);

						// Assigns parent, children, height, depth
						tree.root = d3.hierarchy(treeData, function (d) {
							return d.children;
						});

						tree.root.x0 = 0;
						tree.root.y0 = 0;

						update(tree);
					});

				onResize();
			}
		};

		// Collapse the node and all it's children
		function collapse(d) {
			if (d.children) {
				d._children = d.children;
				d._children.forEach(collapse);
				d.children = null;
			}
		}

		function update(tree) {
			// Assigns the x and y position for the nodes
			var root = tree.root;
			var svg = tree.svg;
			var treemap = tree.treemap;
			var treeData = treemap(root);
			var duration = 750;

			// Compute the new tree layout. 
			var nodes = treeData.descendants(),
				links = treeData.descendants().slice(1);

			// Normalize for fixed-depth.
			nodes.forEach(function (d) { d.y = d.depth * 180; });
			var node = svg.selectAll('g.node')
				.data(nodes, function (d) {
					return d.item_id || (d.item_id = ++tree.treeIteration);
				});

			// Enter any new modes at the parent's previous position.
			var nodeEnter = node.enter().append('g')
				.attr('class', 'node')
				.attr("transform", function (d) {
					return "translate(" + root.y0 + "," + root.x0 + ")";
				})
				.on('click', click);

			// Add Circle for the nodes
			nodeEnter.append('circle')
				.attr('class', 'node')
				.attr('r', 1e-6)
				.style("fill", function (d) {
					return d._children ? "#fff" : Colors.getColor('grey').rgb;
				});

			// Add labels for the nodes
			nodeEnter.append('text')
				.attr("dy", ".35em")
				.attr("x", function (d) {
					return d.children || d._children ? -13 : 13;
				})
				.attr("text-anchor", function (d) {
					return d.children || d._children ? "end" : "start";
				})
				.text(function (d) {
					var n = d.data.name;
					if (n && n.length > 14) {
						n = n.substring(0, 14) + "...";
					}
					return n;
				});

			// UPDATE
			var nodeUpdate = nodeEnter.merge(node);

			// Transition to the proper position for the node
			nodeUpdate.transition()
				.duration(duration)
				.attr("transform", function (d) {
					return "translate(" + d.y + "," + d.x + ")";
				});

			// Update the node attributes and style
			nodeUpdate.select('circle.node')
				.attr('r', 10)
				.style("fill", function (d) {
					return d._children ? "#fff" : Colors.getColor('grey').rgb;
				})
				.attr('cursor', 'pointer');


			// Remove any exiting nodes
			var nodeExit = node.exit().transition()
				.duration(duration)
				.attr("transform", function (d) {
					return "translate(" + root.y + "," + root.x + ")";
				})
				.remove();

			// On exit reduce the node circles size to 0
			nodeExit.select('circle')
				.attr('r', 1e-6);

			// On exit reduce the opacity of text labels
			nodeExit.select('text')
				.style('fill-opacity', 1e-6);

			// ****************** links section ***************************

			// Update the links...
			var link = svg.selectAll('path.link')
				.data(links, function (d) { return d.item_id; });

			// Enter any new links at the parent's previous position.
			var linkEnter = link.enter().insert('path', "g")
				.attr("class", "link")
				.attr('d', function (d) {
					var o = { x: root.x0, y: root.y0 };
					return diagonal(o, o)
				});

			// UPDATE
			var linkUpdate = linkEnter.merge(link);

			// Transition back to the parent element position
			linkUpdate.transition()
				.duration(duration)
				.attr('d', function (d) {
					return diagonal(d, d.parent)
				});

			// Remove any exiting links
			var linkExit = link.exit().transition()
				.duration(duration)
				.attr('d', function (d) {
					var o = { x: root.x, y: root.y }
					return diagonal(o, o)
				})
				.remove();

			// Store the old positions for transition.
			nodes.forEach(function (d) {
				d.x0 = d.x;
				d.y0 = d.y;
			});

			// Toggle children on click.
			function click(d) {
				if (d.children) {
					d._children = d.children;
					d.children = null;
				} else {
					d.children = d._children;
					d._children = null;
				}

				update(tree);
			}

			// Creates a curved (diagonal) path from parent to the child nodes
			function diagonal(s, d) {
				return "M " + s.y + " " + s.x + " C " + ((s.y + d.y) / 2) + " " + s.x + ", " + ((s.y + d.y) / 2) + " " + d.x + ", " + d.y + " " + d.x;
			}
		}
	}
})();
