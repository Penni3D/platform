(function () {
	'use strict';
	angular
		.module('core.process')
		.directive('portfolioProportions', portfolioProportionsDirective);

	portfolioProportionsDirective.$inject = ['PortfolioService', 'ViewService', 'Common'];
	function portfolioProportionsDirective(PortfolioService, ViewService, Common) {
		return {
			restrict: 'E',
			templateUrl: 'core/portfolios/views/portfolioProportions.html',
			replace: true,
			scope: {
				portfolioId: '=',
				rows: '@'
			},
			controller: function ($scope, $element) {
				var StateParameters = {}; //STATEPARAMETRIN ARVOT VÄLITETTÄVÄ DIREKTIIVILLE
				var $card = $element.closest('content');
				var root;

				var onResize = function () {
					$element.height($card.height());
					draw($element, root, $card.height());
				};

				Common.subscribeContentResize($scope, onResize);
				PortfolioService
					.getPortfolioData(
						StateParameters.process,
						StateParameters.portfolioId)
					.then(function (rows) {
						//Arrange items to parent - child order
						_.each(rows.rowdata, function (r) {
							if (!_.isArray(r.children)) {
								r.children = [];
							}
							r.size = r.wsjf;
							r.children = r.children.concat(_.filter(rows.rowdata, ['parent_item_id', r.item_id]));
						});

						//Add first node
						var treeData = {
							"item_id": 0,
							"name": StateParameters.menuItem,
							"children": rows.rowdata
						};

						//Draw for the first time
						root = treeData;
						setTimeout(function () {
							draw($element, root, $card.height());
						}, 0);
					});
			}
		};

		function draw($element, root, cardheight) {
			d3.select("svg").remove();
			var svg = d3.select($element[0]).append("svg")
				.attr("preserveAspectRatio", "xMinYMin meet")
				.attr("width", cardheight)
				.attr("height", cardheight)

			var margin = 0, diameter = cardheight;

			var g = svg.append("g").attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

			var color = d3.scaleLinear()
				.domain([-1, 5])
				.range(["hsl(0,0%,90%)", "hsl(0,0%,45%)"])
				.interpolate(d3.interpolateHcl);

			var pack = d3.pack()
				.size([diameter - margin, diameter - margin])
				.padding(2);

			root = d3.hierarchy(root)
				.sum(function (d) {
					return d.size;
				})
				.sort(function (a, b) {
					return b.value - a.value;
				});

			var focus = root,
				nodes = pack(root).descendants(),
				view;
			var circle = g.selectAll("circle")
				.data(nodes)
				.enter().append("circle")
				.attr("class", function (d) {
					return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root";
				})
				.style("fill", function (d) {
					if (d.children) {
						switch (d.depth) {
							case 0:
								return "#fff";
							case 1:
								return "#e0e0e0";
							case 2:
								return "#bdbdbd";
							case 3:
								return "#9e9e9e";
							case 4:
								return "#757575";
							default:
								return "red";
						}
					} else {
						return null;
					}
				})
				.style("stroke", function (d) {
					if (d.depth == 0) {
						return "#e0e0e0";
					}
					return null;
				})
				.style("stroke-width", "2px")
				.on("mouseover", function (d) {
					/*
					d3.select("#tooltip")
						.style("left", d.x + "px")
						.style("top", d.y + "px")
						.select("#value")
						.text(d.data.name);
					d3.select("#tooltip").classed("hidden", false);
					*/
				})
				.on("mouseout", function () {
					//d3.select("#tooltip").classed("hidden", true);
				})
				.on("click", function (d) {
					if (focus !== d) zoom(d), d3.event.stopPropagation();
				});

			var text = g.selectAll("text")
				.data(nodes)
				.enter().append("text")
				.attr("class", "label")
				.style("fill-opacity", function (d) {
					return d.parent === root ? 1 : 0;
				})
				.style("display", function (d) {
					return d.parent === root ? "inline" : "none";
				})
				.text(function (d) {
					if (!d.children) {
						if (d.depth == 1) {
							return "";
						}
					}
					var n = d.data.name;
					if (n && n.length > 24) {
						n = n.substring(0, 24) + "...";
					}
					return n;
				});

			var node = g.selectAll("circle,text");

			svg
				.style("background", "#fff")
				.on("click", function () {
					zoom(root);
				});

			zoomTo([root.x, root.y, root.r * 2 + margin]);

			function zoom(d) {
				var focus0 = focus;
				focus = d;

				var transition = d3.transition()
					.duration(d3.event.altKey ? 7500 : 750)
					.tween("zoom", function (d) {

						var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin]);
						return function (t) {
							zoomTo(i(t));
						};
					});

				transition.selectAll("text")
					.filter(function (d) {
						return d.parent === focus || this.style.display === "inline";
					})
					.style("fill-opacity", function (d) {
						return d.parent === focus ? 1 : 0;
					})
					.on("start", function (d) {
						if (d.parent === focus) this.style.display = "inline";
					})
					.on("end", function (d) {
						if (d.parent !== focus) this.style.display = "none";
					});
			}

			function zoomTo(v) {
				var k = diameter / v[2];
				view = v;
				node.attr("transform", function (d) {
					return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")";
				});
				circle.attr("r", function (d) {
					return d.r * k;
				});
			}
		}
	}
})();
