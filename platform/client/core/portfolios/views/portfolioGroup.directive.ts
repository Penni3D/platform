(function () {
	'use strict';

	angular
		.module('core.process')
		.directive('portfolioGroup', PortfolioGroup);

	PortfolioGroup.$inject = ['ProcessService', 'Common', 'CalendarService', '$q', 'Colors', 'Translator'];
	function PortfolioGroup(ProcessService, Common, CalendarService, $q, Colors, Translator) {
		return {
			scope: {
				cards: '=',
				cardTitle: '<',
				cardHorizontal: '<?',
				cardVertical: '<?',
				cardDescription: '<?',
				cardChips: '<?',
				cardColor: '<?',
				onClick: '&?'
			},
			templateUrl: 'core/portfolios/views/portfolioGroup.html',
			controller: function ($scope) {
				$scope.columns = [];
				$scope.rows = [];
			},
			link: function (scope) {
				scope.cardClick = function (card) {
					if (typeof scope.onClick === 'function' && typeof scope.onClick() === 'function') {
						scope.onClick()(card);
					}
				};

				ProcessService
					.getListItems(Common.getRelevantDataAdditional(scope.cardHorizontal), true)
					.then(function (columns) {
						scope.columns = _.map(columns, function (c) {
							return {
								itemId: c.itemId,
								primary: c.primary
							};
						});

						scope.columns.push({
							primary: Translator.translate('PORTFOLIO_GROUP_UNSPECIFIED')
						});

						if (scope.cardVertical) {
							return ProcessService
								.getListItems(Common.getRelevantDataAdditional(scope.cardVertical), true)
								.then(function (rows) {
									scope.rows = _.map(rows, function (row) {
										return {
											itemId: row.itemId,
											primary: row.primary,
											columns: getRowColumns()
										};
									});

									scope.rows.push({
										primary: Translator.translate('PORTFOLIO_GROUP_UNSPECIFIED'),
										columns: getRowColumns()
									})
								});
						} else {
							scope.rows.push({
								columns: getRowColumns()
							});
						}
				}).then(function () {
					scope.$watch('cards', function () {
						resetRowsContent();
						initializeCards();
					}, true);
				});

				function getRowColumns() {
					return _.times(scope.columns.length, function () {
						return [];
					});
				}

				function initializeCards() {
					angular.forEach(scope.cards, function (card) {
						formatData(card, scope.cardTitle).then(function (value) {
							card.PortfolioGroupTitle = joinListProcess(scope.cardTitle, value);
						});

						if (scope.cardDescription) {
							formatData(card, scope.cardDescription).then(function (value) {
								card.PortfolioGroupDescription = joinListProcess(scope.cardDescription, value);
							});
						}

						if (scope.cardColor) {
							let d = Common.getRelevantDataType(scope.cardColor);
							let value = card[scope.cardColor.Name];
							if (d == 6) {
								let da = Common.getRelevantDataAdditional(scope.cardColor);

								ProcessService.getItems(da, value).then(function (items) {
									for (let i = 0, l = items.length; i < l; i++) {
										let c = items[i].list_symbol_fill;
										if (c) {
											card.PortfolioGroupColor = Colors.getColor(c).background;
											break;
										}
									}

									if (!card.PortfolioGroupColor) {
										card.PortfolioGroupColor = Colors.getColor('lightGrey').background;
									}
								});
							} else if (value) {
								card.PortfolioGroupColor = value;
							}
						}

						if (scope.cardChips) {
							formatData(card, scope.cardChips).then(function (items) {
								let c = [];
								let d = Common.getRelevantDataType(scope.cardChips);

								if (d == 5 || d == 6) {
									for (let i = 0, l = items.length; i < l; i++) {
										c.push(items[i]);
									}
								} else {
									c.push(items);
								}

								card.PortfolioGroupChips = c;
							});
						}

						determinePosition(card);
					});
				}

				scope.isColVisible = function(column) {
					if (!column.itemId) {
						let result = false;
						_.each(scope.rows, function (r) {
							if (_.last(r.columns).length > 0) {
								result = true;
								return;
							}
						});
						return result;
					}
					return true;
				};

				scope.isRowVisible = function(row) {
					if (!row.itemId) {
						let result = false;
						_.each(row.columns, function(r) {
							if (r.length > 0) {
								result = true;
								return;
							}
						});
						return result;
					}
					return true;
				};
				
				function resetRowsContent() {
					angular.forEach(scope.rows, function (row) {
						row.columns = getRowColumns();
					});
				}

				scope.hasUndefinedRow = false;
				function determinePosition(card) {
					let horizontalValue = card[scope.cardHorizontal.Name];
					let verticalValue = scope.cardVertical ? card[scope.cardVertical.Name] : undefined;

					if (Array.isArray(horizontalValue)) {
						let i = scope.columns.length;
						while (i--) {
							let column = scope.columns[i];
							if (column.itemId == horizontalValue[0]) {
								if (Array.isArray(verticalValue)) {
									let j = scope.rows.length;
									while (j--) {
										let row = scope.rows[j];
										if (row.itemId == verticalValue[0]) {
											row.columns[i].push(card);
											return;
										}
									}
								}

								_.last(scope.rows).columns[i].push(card);
								return;
							}
						}
					}

					if (Array.isArray(verticalValue)) {
						let j = scope.rows.length;
						while (j--) {
							let row = scope.rows[j];
							if (row.itemId == verticalValue[0]) {
								_.last(row.columns).columns.push(card);
								return;
							}
						}
					}

					_.last(_.last(scope.rows).columns).push(card);
				}

				function formatData(card, column) {
					let d = $q.defer();
					let dataType = Common.getRelevantDataType(column);
					let value = card[column.Name];

					if (dataType == 1 || dataType == 2) {
						d.resolve(Common.formatNumber(value));
					} else if (dataType == 13) {
						d.resolve(CalendarService.formatDateTime(value));
					} else if (dataType == 3) {
						d.resolve(CalendarService.formatDate(value));
					} else if (dataType == 5 || dataType == 6) {
						let da = Common.getRelevantDataAdditional(column);

						if (Array.isArray(value)) {
							ProcessService.getItems(da, value).then(function (items) {
								let r = [];
								for (let i = 0, l = items.length; i < l; i++) {
									r.push(items[i].primary);
								}

								d.resolve(r);
							});
						} else {
							d.resolve('');
						}

					} else {
						d.resolve(value);
					}

					return d.promise;
				}

				let joinListProcess = function (column, arrayValues) {
					let d = Common.getRelevantDataType(column);
					return d == 5 || d == 6 ? _.join(arrayValues, ', ') : arrayValues;
				};
			}
		};
	}
})();