(function () {
	'use strict';

	angular
		.module('core')
		.service('BodyStackerService', BodyStackerService);

	BodyStackerService.$inject = [];
	function BodyStackerService() {
		let stacks = [];
		let self = this;

		self.add = function (view) {
			let i = stacks.length;
			while (i--) {
				if (stacks[i].id == view.id) {
					stacks.splice(i, 1);
					break;
				}
			}

			stacks.push(view);
		};

		self.get = function () {
			return stacks.length > 0 ? stacks.pop() : false;
		};

		self.remove = function (id) {
			let i = stacks.length;
			while (i--) {
				if (stacks[i].id == id) {
					stacks.splice(i, 1);
					break;
				}
			}
		};
	}
})();