(function () {
	'use strict';

	angular
		.module('core')
		.controller('UserCardController', UserCardController);


	UserCardController.$inject = ['$scope', 'User', 'Users', 'UserService', '$q', 'AllocationService', 'Configuration'];

	function UserCardController($scope, User, Users, UserService, $q, AllocationService, Configuration) {
		$scope.loading = true;

		$scope.showRes = Configuration.getClientConfigurations('Main application', 'Disable Resourcemanagement') != 'true';

		if (User) {
			UserService.getUser(User).then(function (user) {
				$scope.user = user;
				$scope.loading = false;

				AllocationService.getUsersQuickLook([user.item_id]).then(function (result) {
					$scope.user.cells = result;
				});
			});
		} else if (Users) {
			$scope.users = [];
			let promises = [];
			let ids = [];
			angular.forEach(Users.split(','), function (user) {
				ids.push(user);
				promises.push(UserService.getUser(user));
			});

			$q.all(promises).then(function (r) {
				$scope.users = r;
				$scope.loading = false;
				AllocationService.getUsersQuickLook(ids).then(function (result) {
					for (let u of $scope.users) {
						u.cells = result.filter(e => e.ItemId == u.itemId);
					}
				});
			});
		}
	}
})();