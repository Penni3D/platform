﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('UserService', UserService);

	UserService.$inject = ['$q', '$rootScope', 'ProcessService', 'MessageService', 'HTMLService', '$sanitize', 'Sanitizer', 'ApiService'];
	function UserService($q, $rootScope, ProcessService, MessageService, HTMLService, $sanitize, Sanitizer, ApiService) {
		let self = this;
		let States = ApiService.v1api('navigation/States');

		self.card = function (event) {
			let $currentTarget = $(event.currentTarget);
			let p = {
				template: 'core/common/user/userCard.html',
				controller: 'UserCardController',
				locals: {
					User: $currentTarget.attr('process-id'),
					Users: $currentTarget.attr('process-ids')
				}
			};

			MessageService.card(p, $currentTarget);
		};
		
		self.getUser = function (itemId, force = false) {
			return ProcessService.getItem('user', itemId, force).then(function (user) {
				prepareUser(user);
				return user;
			});
		};

		self.getUsers = function (itemIds) {
			let promises = [];

			for (let i = 0, l = itemIds.length; i < l; i++) {
				promises.push(self.getUser(itemIds[i]));
			}

			return $q.all(promises).then(function (users) {
				return users;
			});
		};
		
		self.getCurrentUser = function () {
			return self.getUser($rootScope.clientInformation.item_id);
		};

		self.getCurrentUserId = function () {
			return $rootScope.clientInformation.item_id;
		};

		function prepareUser(user) {
			if (user.active <= 0) {
				return;
			}

			user.name = user.first_name + ' ' + user.last_name;

			if (user.photo) {
				user.photoUrl = 'v1/meta/AttachmentsDownload/' + user.photo.split(',')[0] + '/3';
				user.hasPhoto = true;
			} else {
				user.hasPhoto = false;
			}
		}

		function getCardOpener() {
			return HTMLService.initialize('div').class('card-opener')
				.inner(HTMLService.initialize().icon('open_in_new', 'white'));
		}

		function constructAvatar(user, mode, size) {
			let avatar = HTMLService.initialize();
			let avatarSize = 'circle';

			if (typeof mode === 'undefined') {
				mode = 0;
			}

			if (size == 'small') {
				avatarSize += ' avatar-small circle-small';
			} else if (size == 'smaller') {
				avatarSize += ' avatar-smaller';
			} else {
				avatarSize += ' circle-large'
			}

			if (user.hasPhoto) {
				avatar
					.element('img')
					.attr('src', $sanitize(user.photoUrl))
					.class('avatar ' + avatarSize)
					.inner(getCardOpener());
			} else {
				let splittedPrimary = user.primary.split(' ');
				let f = splittedPrimary[0] ? splittedPrimary[0][0] : '';
				let s = splittedPrimary[1] ? splittedPrimary[1][0] : '';

				avatar
					.element('div')
					.attr('tooltip', Sanitizer(user.primary))
					.class('avatar avatar-text ' + avatarSize)
					.attr('process-id', Sanitizer(user.item_id))
					.content(Sanitizer(f + s))
					.inner(getCardOpener());
			}

			switch (mode) {
				case 0:
					return avatar.getObject();
				case 1:
					if (user.hasPhoto) {
						return avatar.getObject();
					}

					break;
				case 2:
					if (!user.hasPhoto) {
						return avatar.getObject();
					}

					break;
			}

			return $();
		}

		self.getAvatarById = function (userId, mode, size) {
			if (typeof userId === 'undefined') {
				let d = $q.defer();
				d.resolve($());
				return d.promise;
			}

			return self.getUser(userId).then(function (user) {
				return constructAvatar(user, mode, size);
			});
		};

		self.getAvatars = function (userIds, options) {
			let promises = [];
			let $users = $('<div class="avatar-multiple">');
			let l = userIds.length;
			let max = l;

			options = angular.copy(options);

			if (typeof options.limit !== 'undefined' && l > options.limit) {
				max = options.limit;
			}

			for (let i = 0; i < max; i++) {
				promises.push(self.getAvatarById(userIds[i], options.mode, options.size));
			}

			return $q.all(promises).then(function (avatars) {
				for (let i = 0; i < max; i++) {
					$users.append(avatars[i]);
				}

				if (l > max) {
					let size = 'circle';
					if (options.size === 'small') {
						size += 'avatar-small circle-small';
					} else if (options.size === 'smaller') {
						size += ' avatar-smaller';
					} else {
						size += ' circle-large'
					}

					let processIds = _.join(
						_.filter(userIds, function (k, i) {
							let index = i + 1;
							return index > max && index <= l;
						}), ',');

					$users.append(
						HTMLService
							.initialize('div')
							.class('avatar avatar-text avatar-multiple-more ' + size)
							.attr('process-ids', Sanitizer(processIds))
							.content('...')
							.inner(getCardOpener()).getHTML());
				}

				$users.css({
					'max-width': max * 40,
					'min-width': max * 40
				});

				return $users;
			});
		};

		self.getAvatar = function (user, mode) {
			let d = $q.defer();

			if (typeof user === 'undefined') {
				d.resolve($());
			} else {
				d.resolve(constructAvatar(user, mode));
			}

			return d.promise;
		};

		self.getUsersGroups = function(){
			return States.get(["usersGroups"]);
		}
	}

})();
