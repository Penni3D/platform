﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('ToolbarService', ToolbarService);

	ToolbarService.$inject = ['$rootScope', 'LocalStorageService', 'Translator', 'ViewService', 'SidenavService', 'MessageService'];

	function ToolbarService($rootScope, LocalStorageService, Translator, ViewService, SidenavService, MessageService) {
		let self = this;
		let allowToSet = true;
		let slots = LocalStorageService.get('toolbarPath') ?
			LocalStorageService.get('toolbarPath') :
			{
				1: '',
				2: '',
				3: ''
			};


		self.subscribePath = function (scope, callback) {
			let handler = $rootScope.$on('notifyToolbarPath', callback);
			scope.$on('$destroy', handler);
		};

		function notifyPath() {
			$rootScope.$emit('notifyToolbarPath');
		}

		self.getPathSlots = function () {
			let result = [Translator.translate('FRONTPAGE')];
			if (slots[1].length) result.push(slots[1]);
			if (slots[2].length) result.push(slots[2]);
			if (slots[3].length) result.push(slots[3]);
			return result;
		}


		self.setPathPortfolio = function(process, portfolioId){
			let currentActivePortfolio = {
				'ProcessPortfolioId': portfolioId,
				'Process': process
			};
			LocalStorageService.store('currentActivePortfolio', currentActivePortfolio);
		}

		self.getPathPortfolio = function(){
			return  LocalStorageService.get('currentActivePortfolio') ?
				LocalStorageService.get('currentActivePortfolio') :
				{
					'ProcessPortfolioId': 0,
					'Process': ""
				};

		}

		self.getPath = function () {
			let path = Translator.translate('FRONTPAGE');
			let hl;

			let fs = slots[1];
			let ss = slots[2];
			let ts = slots [3];

			if (fs.length) {
				if (ss.length) {
					path = path + ' > ' + fs + ' >';

					if (ts.length) {
						path += ' ' + ss + ' >';
						hl = ts;
					} else {
						hl = ss;
					}
				} else if (ts.length) {
					path += ' ' + fs + ' >';
					hl = ts;
				} else {
					path = path + ' >';
					hl = fs;
				}
			} else if (ss.length) {
				path = path + ' >';
				if (ts.length) {
					path += ' ' + ss + ' >';
					hl = ts;
				} else {
					hl = ss;
				}
			} else if (ts.length) {
				path = path + ' >';
				hl = ts;
			}

			LocalStorageService.store('toolbarPath', slots);

			return {path: path, highlight: hl};
		};

		self.setFirstSlot = function (value) {
			if (value && allowToSet) {
				slots[1] = Translator.translation(value);
				slots[2] = '';
				slots[3] = '';

				notifyPath();

			}
		};

		self.setSecondSlot = function (value) {
			if (value && allowToSet) {
				slots[2] = Translator.translation(value);
				slots[3] = '';

				notifyPath();
			}
		};

		self.setThirdSlot = function (value) {
			if (value && allowToSet) {
				slots[3] = Translator.translation(value);
				notifyPath();
			}
		};

		let debugFunction = undefined;
		self.setDebugFunction = function (callback) {
			if (allowToSet) debugFunction = callback;
		}
		self.hasDebugFunction = function () {
			return !!debugFunction;
		}
		self.executeDebugFunction = function () {
			if (debugFunction) debugFunction();
		}

		let configureFunction = undefined;
		self.setConfigureFunction = function (callback) {
			if (allowToSet) configureFunction = callback;
		}
		self.hasConfigureFunction = function () {
			return !!configureFunction;
		}
		self.executeConfigureFunction = function () {
			if (configureFunction) configureFunction();
		}

		let deleteFunction = undefined;
		self.setDeleteFunction = function (callback) {
			if (allowToSet) deleteFunction = callback;
		}
		self.hasDeleteFunction = function () {
			return !!deleteFunction;
		}
		self.executeDeleteFunction = function () {
			if (deleteFunction) deleteFunction();
		}
		self.hasQuickNavigation = function () {
			let rows = SidenavService.getNavigatePortfolio('rows')
			return rows && rows.length > 0;
		}

		self.currentQuickNavPosition = function () {
			let activeItemId = ViewService.getActiveItemId();
			let rows = SidenavService.getNavigatePortfolio("rows");
			let index = _.findIndex(rows, function (o) {
				return o[o.hasOwnProperty('actual_item_id') ? 'actual_item_id' : 'item_id'] == activeItemId;
			});
			if(index == -1) return null;
			return index + 1;
			//return rows[index].ROWID;
		}
		self.currentQuickNavTotal = function () {
			return SidenavService.getNavigatePortfolio('total_rows');
		}

		self.executeConditionOutcomes = function(itemId: number, process: string){
			ViewService.view('conditionResultDialog', {
				params: {
					ItemId: itemId,
					Process: process
				}
			});


		}

		$rootScope.$on('viewChange', function (event, parameters) {
			switch (parameters.action) {
				case 'PRE-NEW':
					allowToSet = true;
					slots[2] = '';
					deleteFunction = undefined;
					debugFunction = undefined;
					configureFunction = undefined;

					if (!parameters.view.includes("process.")) SidenavService.setNavigatePortfolioObj('rows', [])
					if (!ViewService.getParentView()) slots[1] = '';

					break;
				case 'PRE-REROUTE':
					allowToSet = true;
					break;
				case 'PRE-SPLIT':
					allowToSet = false;
					break;
				case 'REROUTE':
				case 'NEW':
					notifyPath();
					break;
			}
		});
	}
})();
