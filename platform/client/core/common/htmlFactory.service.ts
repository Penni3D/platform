(function () {
	'use strict';

	angular
		.module('core')
		.service('HTMLService', HTMLService);

	HTMLService.$inject = [];
	function HTMLService() {
		function Methods(ele) {
			let heart = '';
			let element = '';
			let elementClosed = false;
			let elementFinished = false;
			let self = this;

			self.element = function (ele) {
				if (element.length) {
					self.finish();
					elementFinished = false;
					elementClosed = false;
				}

				heart += '<' + ele;
				element = ele;

				return self;
			};

			self.attr = function (attribute, value) {
				if (typeof value === 'undefined') {
					if (typeof attribute !== 'undefined') {
						heart += ' ' + attribute;
					}
				} else {
					heart += ' ' + attribute + '="' + value + '"';
				}

				return self;
			};

            self.attrNGConditional = function (condition, attribute, value?) {
                if (typeof condition == 'undefined') condition = true;
                if (typeof value == 'undefined') value = true;
                if (typeof attribute !== 'undefined') {
                    heart += ' ng-attr-' + attribute + '="{{' + condition + ' ? ' + value + ' : undefined}}"';
                }
                return self;
            };
			
			self.attrConditional = function (condition, attribute, value?) {
				if (condition === false || isFalse(condition)) return self;
				return self.attr(attribute, value);
			};

			self.style = function (value) {
				let v = '';
				if (isFalse(value)) return self;

				if (Array.isArray(value)) {
					for (let i = 0, l = value.length; i < l; i++) {
						v += value[i] + ';';
					}
				} else {
					v = value;
				}

				return self.attr('style', v);
			};

			self.class = function (value) {
				return getAttr('class', value);
			};

			self.ngClass = function (value) {
				return getAttr('ng-class', value);
			};

			self.ngStyle = function (value) {
				return getAttr('ng-style', value);
			};

			self.if = function (value) {
				return getAttr('ng-if', value);
			};

			self.bind = function (value) {
				return getAttr('ng-bind', value);
			};

			self.click = function (value) {
				return getAttr('ng-click', value);
			};

			self.tabindex = function (value) {
				return getAttr('tabindex', value);
			};

			self.ariaLabel = function (value) {
				return getAttr('aria-label', value);
			};

			self.ariaRole = function (value) {
				return getAttr('role', value);
			};

			self.repeat = function (value) {
				return getAttr('ng-repeat', value);
			};

			self.inner = function (child) {
				self.close();
				self.content(child.getHTML());

				return self;
			};

			self.close = function () {
				if (!elementClosed && heart.length) {
					heart += '>';
					elementClosed = true;
				}

				return self;
			};

			self.content = function (content) {
				self.close();
				heart += content;

				return self;
			};

			self.finish = function () {
				if (heart.length) {
					self.close();
					if (!elementFinished) {
						heart += '</' + element + '>';
						elementFinished = true;
					}
				}

				return self;
			};

			self.getHTML = function () {
				self.finish();
				return heart;
			};

			self.getObject = function () {
				return $(self.getHTML());
			};

			self.button = function () {
				return self.element('input-button');
			};

			self.icon = function (icon, cls) {
				let c = 'material-icons';
				if (typeof cls !== 'undefined') c += ' ' + cls;
				return self.element('i').attr("aria-hidden","true").class(c).content(icon);
			};

			function getAttr(attr, value) {
				if (isFalse(value)) return self;
				return self.attr(attr, value);
			}

			if (typeof ele !== 'undefined') {
				self.element(ele);
			}
		}

		this.initialize = function (element) {
			return new Methods(element);
		};

		function isFalse(value) {
			return typeof value === 'undefined' || value === null || (typeof value === 'string' && !value.length);
		}
	}
})();