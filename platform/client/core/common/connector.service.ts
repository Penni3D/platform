(function () {
	'use strict';

	angular
		.module('core')
		.service('ConnectorService', ConnectorService);

	ConnectorService.$inject = ['MessageService', '$http'];
	function ConnectorService(MessageService, $http) {
		let self = this;
		let connection;
		let terminated = false;

		self.initializeHub = function (sessiontoken, path) {
			if (path !== '') path = '/' + path;

			connection = new signalR.HubConnectionBuilder()
				.withUrl(path + '/apiHub')
				.configureLogging(signalR.LogLevel.Information)
				.build();

			//If connection is closed, try to reconnect for 10 times and then show a message which allows to try 10 times more.
			connection.onclose(function () {
				if (terminated) return;

				let l = 0;

				//Reconnector function
				let a = function(response) {
					let c = response.data;

					connection.qs = {
						token: c.token
					};

					return connection.start().then(function () {
						return self.connect(c.token);
					});
				};

				//Connection problem
				//You have been inactive for awhile or your connection has been interrupted.

				//Reconnection loop function
				let url = 'v1/core/ClientInformation';
				let f = function () {
					l += 1;
					$http.get(url).then(function (response) {
						a(response).then(function () {
							MessageService.removeBackdrop();
						});
					}, function () {
						if (l % 10) {
							MessageService.backdrop();
							setTimeout(f, 2000);
						} else {
							MessageService.dialog({
								title: 'CONNECTION_PROBLEM',
								message: 'CONNECTION_INACTIVE',
								buttons: [{
									type: 'primary',
									text: 'CONTINUE',
									onClick: function () {
										$http.get(url).then(function (response) {
											a(response);
										}, function () {
											setTimeout(f, 1000);
										});
									}
								}]
							});
						}
					});
				};

				setTimeout(f,300);
			});

			return connection.start();
		};

		self.connect = function (sessiontoken) {
			return connection.invoke('Connect', sessiontoken);
		};

		self.listen = function (methodName, callback) {
			connection.on(methodName, callback);
		};

		self.disconnect = function() {
			terminated = true;
			return connection.stop();
		};
		
		self.invoke = function (methodName, args) {
			if (typeof connection === 'undefined') {
				return;
			}

			if (connection.connection.connectionState === 1) {
				return connection.invoke.apply(connection, $.merge([methodName], $.makeArray(args)));
			}
		};
	}
})();