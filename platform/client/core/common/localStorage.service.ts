(function () {
	'use strict';

	angular
		.module('core')
		.service('LocalStorageService', LocalStorageService);

	LocalStorageService.$inject = ['$location'];

	function LocalStorageService($location) {
		let self = this;
		let path = $location.absUrl().split($location.host())[1];
		let identifier = path.match(/\/[a-zA-Z]+\//g);

		if (identifier === null) identifier = '/local/';

		function getPathKey(key) {
			return identifier + ':' + key;
		}

		self.store = function (key, value) {
			if (localStorage) {
				let item = typeof value === 'object' ? angular.toJson(value) : value;
				localStorage.setItem(getPathKey(key), item);
				return item;
			}
		};

		self.get = function (key) {
			if (localStorage) {
				let item = localStorage.getItem(getPathKey(key));
				if (item)
					return (_.startsWith(item, '{') && _.endsWith(item, '}')) || (_.startsWith(item, '[') && _.endsWith(item, ']'))
						? angular.fromJson(item) : item;

			}
		};

		self.clear = function () {
			if (localStorage)
				localStorage.clear();
		};

		self.clearKey = function (key) {
			if (localStorage){
				window.localStorage.removeItem(key);
			}
		}
	}

})();