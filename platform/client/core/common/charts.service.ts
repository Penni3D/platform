(function () {
	'use strict';

	angular
		.module('core')
		.service('ChartsService', ChartsService);

	ChartsService.$inject = ['Configuration'];
	function ChartsService(Configuration) {
		let chartFilterStore = {};
		
		this.setChartFilters = function (portfolioId: number, type: string, chartId: string, chartProcess: string, linkId: number, filters: any) {
			let id = type + '_' + chartId + '_' + chartProcess + '_' + linkId;
			if (type == "portfolio") {
				let portfolioSettings = Configuration.getActivePreferences('portfolios');
				if (portfolioId in portfolioSettings) {
					let activeSettings = portfolioSettings[portfolioId];
					if (!(activeSettings.chartFilters)) activeSettings.chartFilters = {};
					activeSettings.chartFilters[id] = filters;
					Configuration.setActivePreferences(portfolioId, activeSettings);
					Configuration.saveActivePreferences('portfolios', portfolioId);
				}
			}
			else {//meta,frontPage			
				if (portfolioId in chartFilterStore == false) chartFilterStore[portfolioId] = {}
				chartFilterStore[portfolioId][id] = filters;
			}
		}

		this.getChartFilters = function (portfolioId: number, type: string, chartId: string, chartProcess: string, linkId: number): any {
			let filterSet = null;
			let id = type + '_' + chartId + '_' + chartProcess + '_' + linkId;
			if (type == "portfolio") {
				let portfolioSettings = Configuration.getActivePreferences('portfolios');
				if (portfolioId in portfolioSettings) {
					let activeSettings = portfolioSettings[portfolioId];

					if (activeSettings.chartFilters && Object.keys(activeSettings.chartFilters).length > 0 && id in activeSettings.chartFilters) filterSet = activeSettings.chartFilters[id];
				}
			}
			else { //meta,frontPage		
				if (portfolioId in chartFilterStore) {
					filterSet = chartFilterStore[portfolioId][id];
				}
			}
			return filterSet;
		}
	}
})();