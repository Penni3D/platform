(function () {
	'use strict';

	angular
		.module('core')
		.service('CommentService', CommentService);

	CommentService.$inject = ['ApiService', 'ProcessService', '$q'];
	function CommentService(ApiService, ProcessService, $q) {
		var self = this;
		var comments = {};
		var ProcessComments = ApiService.v1api('meta/ProcessComments');

		self.get = function (process, itemId, identifier) {
			return ProcessComments.get([process, itemId, identifier]).then(function (data) {
				angular.forEach(data.processes, function (process) {
					ProcessService.setData({ Data: process }, 'user', process.item_id);
				});

				var orderedComments = [];
				var promises = [];
				angular.forEach(data.comments, function (comment) {
					promises.push(ProcessService
						.getItem('user', comment.Sender)
						.then(function (user) {
							comment.senderName = user.primary;
							var id = comment.CommentId;

							if (typeof comments[id] === 'undefined') {
								comments[id] = comment;
							} else {
								angular.forEach(comment, function (value, key) {
									comments[id][key] = value;
								});
							}

							comments[id].Date = new Date(comments[id].Date);
							orderedComments.push(comments[id]);
						})
					);
				});

				return $q.all(promises).then(function () {
					return orderedComments.sort(function (a, b) {
						return a.Date.getTime() - b.Date.getTime()
					});
				});
			});
		};

		self.delete = function (process, itemId, identifier, commentId) {
			return ProcessComments.delete([process, itemId, identifier, commentId]);
		};

		self.post = function (process, itemId, identifier, comment) {
			return ProcessComments.new([process, itemId, identifier], { Data: comment }).then(function (comment) {
				comment.Date = new Date(comment.Date);
				comments[comment.CommentId] = comment;
				return comments[comment.CommentId];
			});
		};
	}

})();