(function () {
	'use strict';

	angular
		.module('core')
		.config(TagsConfig)
		.service('TagsService', TagsService);

	TagsConfig.$inject = ['GlobalsProvider', 'ViewConfiguratorProvider'];
	function TagsConfig(GlobalsProvider, ViewConfiguratorProvider) {
		GlobalsProvider.addGlobal('tags', []);

		ViewConfiguratorProvider.addConfiguration('tags', {
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('instance_tags');
				}
			},
			tabs: {
				default: {
					tagsPortfolioId: function (resolves) {
						return {
							type: 'select',
							options: resolves.Portfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation',
							label: 'TAGS_PORTFOLIO_ID'
						};
					}
				}
			}
		});

	}

	TagsService.$inject = ['PortfolioService', 'ViewConfigurator', 'ProcessService'];
	function TagsService(PortfolioService, ViewConfigurator, ProcessService) {
		var tags = [];
		var tagsByGroup = {};

		var self = this;

		self.fetchTags = function () {
			var tagsSettings = ViewConfigurator.getGlobal('tags');

			if (typeof tagsSettings === 'undefined' || typeof tagsSettings.Params.tagsPortfolioId === 'undefined') {
				return;
			}

			return PortfolioService.getPortfolioData(
				'instance_tags',
				ViewConfigurator.getGlobal('tags').Params.tagsPortfolioId
			).then(function (response) {
				angular.forEach(response.processes, function (item) {
					ProcessService.setData({ Data: item }, item.process, item.item_id);
				});

				var i = response.rowdata.length;
				while (i--) {
					var row = response.rowdata[i];
					var j = row.tag_group.length;

					if (j === 0) {
						tags.push(row.tag_name);
					} else {
						while (j--) {
							var groupName = response.processes[row.tag_group[j]].tag_group;

							if (typeof tagsByGroup[groupName] === 'undefined') {
								tagsByGroup[groupName] = [];
							}

							tagsByGroup[groupName].push(row.tag_name);
						}
					}
				}
			});
		};

		self.get = function (group) {
			if (typeof group === 'undefined') {
				return tags;
			}

			if (typeof tagsByGroup[group] === 'undefined') {
				return [];
			}

			return tagsByGroup[group]
		};
	}
})();