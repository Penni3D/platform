(function () {
	'use strict';

	angular
		.module('core')
		.service('SelectorService', SelectorService);

	SelectorService.$inject = ['ApiService', '$injector', '$timeout', '$q'];
	function SelectorService(ApiService, $injector, $timeout, $q) {
		let self = this;
		let selectors = {};

		self.getSelectors = function () {
			return selectors;
		};

		self.getProcessSelector = function (process) {
			if (typeof selectors[process] === 'undefined') return;
			return typeof selectors[process].overwrite === 'undefined' ?
				angular.copy(selectors[process].default) : angular.copy(selectors[process].overwrite);
		};

		self.fetchSelectors = function () {
			let ProcessServicePromise = $q.defer();
			let ProcessService;

			$timeout(function () {
				ProcessService = $injector.get('ProcessService');
				ProcessServicePromise.resolve();
			});

			return ProcessServicePromise.promise.then(function () {
				return  ApiService.v1api('meta/ProcessPortfolios/default').get().then(function (data) {
					let sels = data.portfolios;
					let processes = data.processes;
					for (let i = 0, l = sels.length; i < l; i++) {
						let selection = { 
						    Primary: [], Secondary: [], StartDate: undefined, EndDate: undefined, Tertiary: undefined 
						};
						let selector = sels[i];

						for (let j = 0, l_ = selector.ProcessPortfolioColumns.length; j < l_; j++) {
							let p = selector.ProcessPortfolioColumns[j];

							if (p.UseInSelectResult == 1) {
								selection.Primary.push(p.ItemColumn);
							} else if (p.UseInSelectResult == 2) {
								selection.Secondary.push(p.ItemColumn);
							} else if (p.UseInSelectResult == 3) {
								selection.StartDate = p.ItemColumn;
							} else if (p.UseInSelectResult == 4) {
								selection.EndDate = p.ItemColumn;
							} else if (p.UseInSelectResult == 6) {
								selection.Tertiary = p.ItemColumn;
							} else if (p.UseInSelectResult == 12) {
								selection.Tertiary = p.ItemColumn;
							}
						}

						angular.forEach(selection, function (value, key) {
							selector[key] = value;
						});

						if (typeof selectors[selector.Process] === 'undefined') {
							selectors[selector.Process] = {};
						}

						let processSelector = selectors[selector.Process];
						if (selector.ProcessPortfolioType == 1) {
							processSelector.default = selector;
						} else {
							processSelector.overwrite = selector;
						}
					}

					angular.forEach(processes, function (item, itemId) {
						ProcessService.setData({ Data: item }, item.process, itemId);
					});

					return data;
				});
			});
		};
	}
})();