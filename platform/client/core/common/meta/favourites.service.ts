(function () {
	'use strict';

	angular
		.module('core')
		.service('FavouritesService', FavouritesService);

	FavouritesService.$inject = ['ProcessService', 'ApiService', '$q'];
	function FavouritesService(ProcessService, ApiService, $q) {
		var self = this;
		var favourites = {};
		var Items = ApiService.v1api('meta/Items');
		
		self.getAllFavourites = function(processes) {
			Items.new('favourites',processes).then(function (results) {
				_.forIn(results, function(value, key) {
					getProcessFavourites(key, value);
				});
			});
		};
		
		var getProcessFavourites = function (process, favs) {
			if (typeof favourites[process] === 'undefined') favourites[process] = {};
			var processFavourites = favourites[process];
			
			var promises = [];

			for (var i = 0, l = favs.length; i < l; i++) {
				var fav = favs[i];

				processFavourites[fav.item_id] = ProcessService.setData({ Data: fav }, process, fav.item_id).Data;
				promises.push(ProcessService.prepareItem(process, processFavourites[fav.item_id]));
			}

			return $q.all(promises).then(function () {
				return favourites[process];
			});
		};

		self.getFavourites = function (process) {
			return typeof process === 'undefined' ? favourites : favourites[process];
		};

		self.addFavourite = function (process, itemId) {
			if (typeof favourites[process] === 'undefined') {
				favourites[process] = {};
			}

			var processFavourites = favourites[process];
			return Items.new([process, 'favourites'], itemId).then(function (result) {
				processFavourites[itemId] = ProcessService.setData({ Data: result }, process, itemId).Data;
				return ProcessService.prepareItem(process, processFavourites[itemId]);
			});
		};

		self.removeFavourite = function (process, itemId) {
			return Items.delete([process, 'favourites', itemId]).then(function () {
				delete favourites[process][itemId];
			});
		};
	}
})();