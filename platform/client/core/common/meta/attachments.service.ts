﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('AttachmentService', AttachmentService);

	AttachmentService.$inject = ['ApiService', 'Colors', 'Common', 'MessageService', 'Translator', '$window', 'FileSaver', 'BackgroundProcessesService'];

	function AttachmentService(ApiService, Colors, Common, MessageService, Translator, $window, FileSaver, BackgroundProcessesService) {
		let self = this;

		let Attachments = ApiService.v1api('meta/Attachments');
		let ControlAttachments = ApiService.v1api('meta/ControlAttachments');

		let Links = ApiService.v1api('meta/Links');
		let ControlLinks = ApiService.v1api('meta/ControlLinks');

		self.getTemplatesByProcess = function(process){
			return Attachments.get([process]);
		}

		self.getControlAttachments = function (parentId, control, archiveDate) {
			return ControlAttachments.get([parentId, control, archiveDate]);
		};

		self.getItemAttachments = function (itemId, itemColumnId, archiveDate) {
			return Attachments.get([itemId, itemColumnId, archiveDate]);
		};

		self.deleteAttachment = function (id) {
			return Attachments.delete(id);
		};

		self.getExcel = function (name, rows, fullSet?) {
			let dataObject = {
				Name: name,
				Rows: []
			};

			if(typeof fullSet == 'undefined') {
				for (let i = 0, l = rows.length; i < l; i++) {
					let row = {
						Cells: []
					};

					for (let j = 0, l_ = rows[i].length; j < l_; j++) {
						let bgColor = rows[i][j].backgroundColor;
						let cell = {
							Value: rows[i][j].value,
							BackgroundColor: undefined
						};

						if (bgColor) {
							let c = Colors.getColor(bgColor);
							cell.BackgroundColor = {
								Red: c.r,
								Green: c.g,
								Blue: c.b
							};
						}

						row.Cells.push(cell);
					}

					dataObject.Rows.push(row);
				}
			} else {
				dataObject = fullSet;
			}

			let bgParams = {'Excel': dataObject, 'FileName': name};
			return BackgroundProcessesService.executeOnDemand("GetExcelWithClientData", bgParams);

		};

		self.getControlLinks = function (parentId, control, archiveDate) {
			return ControlLinks.get([parentId, control, archiveDate]);
		};

		self.addControlLink = function (parentId, control, name, url) {
			return ControlLinks.new([parentId, control], {
				name: name,
				url: url
			});
		};

		self.addItemLink = function (itemId, itemColumnId, name, url) {
			return Links.new([itemId, itemColumnId], {
				name: name,
				url: url
			});
		};

		self.getItemLinks = function (itemId, itemColumnId, archiveDate) {
			return Links.get([itemId, itemColumnId, archiveDate]);
		};

		self.deleteLink = function (id) {
			return Links.delete(id);
		};

		self.showAttachment = function (attachmentId, archiveDate) {
			if(typeof  attachmentId == 'undefined') return;
			let params = {
				controller: 'FileLightbox',
				locals: {
					Id: attachmentId,
					ArchiveDate: archiveDate
				},
				template: 'core/ui/input/bare/file/fileLightbox.html'
			};

			MessageService.dialogAdvanced(params);
		};

		self.showVideo = function (stuff, archiveDate){
			let params = {
				controller: 'FileLightboxVideo',
				locals: {
					Attachment: stuff,
					ArchiveDate: archiveDate
				},
				template: 'core/ui/input/bare/file/fileShowVideo.html'
			};

			MessageService.dialogAdvanced(params);
		}

		let allowedPrefixes = ['http://', 'https://', 'ftp://', 'm-files://'];
		self.goToUrl = function (url) {
			MessageService.dialog({
				title: 'LINK_INPUT_LEAVING_TITLE',
				message: Translator.translate('LINK_INPUT_LEAVING') + '\n' + url,
				buttons: [
					{
						type: 'warn',
						text: 'LINK_INPUT_PROCEED',
						onClick: function () {
							let hasPrefix = false;
							_.each(allowedPrefixes, function (pre) {
								if (_.startsWith(url, pre)) hasPrefix = true;
							});
							if (!hasPrefix) url = 'http://' + url;

							$window.open(url, '_blank');
						}
					},
					{
						text: 'MSG_CANCEL'
					}]
			});

		};

		let onDemandFiles = {};

		self.setOnDemandFiles = function(files, itemId){
			onDemandFiles[itemId] = files;
		}

		self.getOnDemandFiles = function(itemId){
			if(!onDemandFiles[itemId]) return [];
			return onDemandFiles[itemId];
		}
		self.clearOnDemandFiles = function(itemId, identifier?){
			if(typeof identifier == 'undefined') onDemandFiles[itemId] = [];
			else {
				let fileToRemove = _.findIndex(onDemandFiles[itemId], function(o) { return o.file.lastModified == identifier; });
				onDemandFiles[itemId].splice(fileToRemove,1);
			}
		}
	}

})();