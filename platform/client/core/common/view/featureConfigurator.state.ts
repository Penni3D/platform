(function () {
	'use strict';

	angular
		.module('core.init')
		.config(FeatureConfiguratorConfig)
		.controller('FeatureConfiguratorController', FeatureConfiguratorController);

	FeatureConfiguratorConfig.$inject = ['ViewsProvider'];
	function FeatureConfiguratorConfig(ViewsProvider) {
		ViewsProvider.addDialog('featureConfigurator', {
			template: 'core/common/view/featureConfigurator.html',
			controller: 'FeatureConfiguratorController',
			resolve: {
				Setup: function (View, Common, StateParameters) {
					if (typeof View.onSetup === 'undefined') {
						return;
					}

					return Common.resolve(View.onSetup, { StateParameters: StateParameters });
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				}
			}
		});
	}

	FeatureConfiguratorController.$inject = [
		'$scope',
		'StateParameters',
		'Processes',
		'Setup',
		'MessageService',
		'ViewConfigurator',
		'View'
	];

	function FeatureConfiguratorController(
		$scope,
		StateParameters,
		Processes,
		Setup,
		MessageService,
		ViewConfigurator,
		View
	) {
		$scope.processes = Processes;
		$scope.proceed = false;
		$scope.params = {};
		$scope.tabs = {};
		$scope.processDefined = false;

		if (typeof View.defaults === 'undefined') {
			View.defaults = {};
		}

		angular.forEach(View.tabs, function (options, tab) {
			$scope.tabs[tab] = {};

			angular.forEach(options, function (value, key) {
				var v = value(Setup);
				var d = View.defaults[key];

				if (v.type === 'select' || v.type === 'boolean') {
					$scope.params[key] = d;
					v.singleValue = true;
				} else if (v.type === 'multiselect') {
					$scope.params[key] = Array.isArray(d) ? d : [];
				} else {
					$scope.params[key] = d;
				}

				if (typeof v.visible === 'undefined') {
					v.visible = true;
				}

				$scope.tabs[tab][key] = v;
			});
		});

		if (StateParameters.process) {
			$scope.processDefined = true;
			$scope.process = StateParameters.process;
			$scope.proceed = true;

			var existing = ViewConfigurator.getConfiguration(StateParameters.view, StateParameters.process);
			angular.forEach(existing, function (conf, key) {
				$scope.params[key] = conf;
			});
		}

		$scope.onChange = function (name) {
			if (typeof View.onChange === 'function') {
				View.onChange(name, $scope.params, Setup, $scope.tabs);
			}
		};

		angular.forEach($scope.tabs, function (tab) {
			angular.forEach(tab, function (option, key) {
				if ($scope.params[key]) {
					$scope.onChange(key);
				}
			})
		});

		$scope.close = function () {
			MessageService.closeDialog();
		};

		$scope.finish = function () {
			delete $scope.params.view;

			if (typeof View.onFinish === 'function') {
				View.onFinish($scope.params, Setup);
			}

			var p = {};
			p[$scope.process] = {};
			p[$scope.process][StateParameters.view] = {
				value: $scope.params,
				process: $scope.process
			};

			ViewConfigurator.setConfiguration(p).then(function () {
				MessageService.closeDialog();
			});
		};

		$scope.processChange = function () {
			$scope.proceed = true;
		};
	}

})();