(function () {
	'use strict';

	angular
		.module('core.init')
		.config(ViewHelperConfig)
		.controller('ViewHelperController', ViewHelperController)
		.provider('ViewHelper', ViewHelper);

	ViewHelper.$inject = [];

	function ViewHelper() {
		this.$get = function ($injector, Configuration, $timeout, ProcessService, Views, $q) {
			let ViewService;
			let ViewHelperService;

			$timeout(function () {
				ViewService = $injector.get('ViewService');
				ViewHelperService = $injector.get('ViewHelperService');
			});
			return {
				showHelp: function (view, params) {
					if (view == 'process.tab') view = view + '.' + (params ? params.tabId : 0);
					if (view == 'portfolio') view = view + '.' + (params ? params.portfolioId : 0);
					let v = ViewHelperService.getViewHelp(view);
					if (typeof params == 'undefined') return;

					if (typeof v === 'undefined') {
						ViewService.view('viewHelper', {
							params: params
						});
					}
					let o = {};
					if (typeof v != 'undefined') {
						o = $injector.invoke(v.tabs)
						ViewService.view('viewHelper', {
							params: angular.extend({
								tabs: o,
								name: view,
							}, params)
						});
					}
				},
				hasHelpFeature: function (view, params?) {
					//Determines if header bar help icon is shown
					if (view != 'frontpage' && typeof params != 'undefined' && params.hasOwnProperty('process') && params.hasOwnProperty('itemId')) {
						return ProcessService.getActions(params.process, params.itemId).then(function (actions) {
							let featureStateName = _.find(Views.getFeatures(), function (p) {
								return p.view == view;
							});
							let stateKey = "";
							if (view == 'process.tab'){
								stateKey = 'meta'
								view = view + "." + params.tabId;
							}
							else stateKey = featureStateName ? featureStateName.name : '';
							//If feature has no conditions, returns false;
							if (!actions['States'].hasOwnProperty(stateKey)) return false;
							if (actions['States'][stateKey]['States'].includes('help')) {
								//If feature has help state and the user fulfills it (usually admin), returns true. This will allow
								//all users that are qualified to add helps to do so
								params.$$helpRight = true;
								return true;
							}
							//If no right to add helps then show help icon if helps are existing
							let v = ViewHelperService.getViewHelp(view);
							return typeof v !== 'undefined';
						});
					} else {
						let d = $q.defer();
						let f = function() {
							let v = ViewHelperService.getViewHelp(view);
							return typeof v !== 'undefined';
						}
						d.resolve(f());
						return d.promise;
					}
				},

				hasHelpPage: function (view, rights, params) {
					if (view == 'portfolio'){
						view = view + "." + params.portfolioId;
					}

					if(rights.includes('help')){
						params.$$helpRight = true;
						return true;
					}

					let v = ViewHelperService.getViewHelp(view);
					return typeof v !== 'undefined';
				},

				isShown: function (view) {
					return Configuration.getActivePreferences('views', view).isShown;
				},
				setShown: function (view) {
					let conf = Configuration.getActivePreferences('views', view);
					if (!conf.isShown) {
						conf.isShown = true;
						Configuration.saveActivePreferences('views', view);
					}
				}
			};
		}
	}

	ViewHelperConfig.$inject = ['ViewsProvider'];

	function ViewHelperConfig(ViewsProvider) {
		ViewsProvider.addDialog('viewHelper', {
			template: 'core/common/view/viewHelper.html',
			controller: 'ViewHelperController',
			resolve: {
				Helpers: function (SettingsService) {
					return SettingsService.getHelpers().then(function (helpers) {
						return helpers;
					});
				}
			}, afterResolve: {
				ThisHelper: function (Helpers, StateParameters) {
					let hit = {};
					if (StateParameters.tabs) {
						_.each(Helpers, function (helper) {
							if (helper.InstanceHelperId == StateParameters.tabs[0].instanceHelperId) hit = helper;
						});
					}
					return hit;
				}
			}
		});
	}

	ViewHelperController.$inject = [
		'$scope',
		'MessageService',
		'StateParameters',
		'ViewHelper',
		'SettingsService',
		'ThisHelper',
		'Helpers',
		'ViewHelperService',
		'$rootScope',
		'Translator',
		'NotificationService',
		'SaveService'
	];

	function ViewHelperController(
		$scope,
		MessageService,
		StateParameters,
		ViewHelper,
		SettingsService,
		ThisHelper,
		Helpers,
		ViewHelperService,
		$rootScope,
		Translator,
		NotificationService,
		SaveService
	) {

		$scope.thisHelper = ThisHelper;
		$scope.helperIsEmpty = _.isEmpty($scope.thisHelper);
		$scope.helpers = {};
		$scope.activeTab = 0;
		$scope.ul = angular.copy($rootScope.clientInformation.locale_id);
		let originalLanguage = angular.copy($rootScope.clientInformation.locale_id);

		$scope.checkNameTranslation = function(instanceHelperId){
			return _.isEmpty($scope.helpers[instanceHelperId].LanguageTranslationName);
		}

		NotificationService.getLanguages().then(function(b){
			$scope.languages = b;
		});

		$scope.helperSave = function(changed){
			_.each(changed, function(c){
				$scope.helpers[c.id].OrderNo = c.orderNo;
				if(_.findIndex(changedHelps, function(o) { return o.InstanceHelperId == $scope.helpers[c.id].InstanceHelperId }) == -1){
					changedHelps.push($scope.helpers[c.id])
				}
			})
			SaveService.tick();
		}

		$scope.deleteTab = function(id){
			$scope.deleteHelper(id);
		}

		if (StateParameters.tabs && StateParameters.tabs.length > 0) {
			_.each(StateParameters.tabs, function (helper) {
				$scope.helpers[helper.instanceHelperId] = {};
			});
		}

		_.each(Helpers, function (helper) {
			if ($scope.helpers.hasOwnProperty(helper.InstanceHelperId)) {
				$scope.helpers[helper.InstanceHelperId] = helper;
			}
		});

		let changedHelps = [];

		$scope.modifyHelp = function (tab_) {
			if (_.findIndex(changedHelps, function (o) {
				return o.InstanceHelperId == tab_.instanceHelperId;
			}) == -1) {
				changedHelps.push($scope.helpers[tab_.instanceHelperId]);
			}
		}

		$scope.canSave = false;
		$scope.adminMode = StateParameters.hasOwnProperty('$$helpRight') ? true : false;
		$scope.adminButton = angular.copy($scope.adminMode);
		$scope.showVideo = true;

		let dummyText = {html: "", delta: {ops:[{insert: ""}]}};

		$scope.initLanguage = function(){
			if(!$scope.helpers['0'].DescVariable[$scope.ul]){
				$scope.helpers['0'].DescVariable[$scope.ul] = dummyText;
			}

			for(let prop in $scope.helpers['0'].LanguageTranslationName) {
				if ($scope.helpers['0'].LanguageTranslationName.hasOwnProperty(prop) && !$scope.helpers['0'].LanguageTranslationName[$scope.ul]) {
					$scope.helpers['0'].LanguageTranslationName[$scope.ul] = $scope.helpers['0'].LanguageTranslationName[prop];
					break;
				}
			}
		}

		$scope.addNewHelp = function () {

			$scope.helpers[0].Identifier = StateParameters.$$helpName;
			$scope.helpers[0].Platform = false;

			let l = angular.copy($scope.helpers[0].DescVariable);

			if(_.isEmpty($scope.helpers[0].DescVariable[$rootScope.clientInformation.locale_id])){
				$scope.helpers[0].DescVariable[$rootScope.clientInformation.locale_id] = dummyText;
			}

			$scope.helpers[0].DescVariable = JSON.stringify($scope.helpers[0].DescVariable);

			if ($scope.helpers[0].Identifier == 'process.tab') $scope.helpers[0].Identifier = $scope.helpers[0].Identifier + '.' + StateParameters.tabId;
			if($scope.helpers[0].Identifier == 'portfolio') $scope.helpers[0].Identifier = $scope.helpers[0].Identifier + '.' + StateParameters.portfolioId;

			SettingsService.addHelper($scope.helpers[0]).then(function (newHelper) {

				let contentVar = newHelper.MediaType == 0 ? '<video src="' + newHelper.VideoLink + '" controls preload="metadata" loop class="stretch">' +
					'</video><p class="simple-padding">' + l[originalLanguage].html + '</p>' : '<p class="simple-padding">' + l[originalLanguage].html + '</p>'
				$scope.createNew = false;
				let v = {
					instanceHelperId: newHelper.InstanceHelperId,
					mediaType: newHelper.MediaType,
					name: $scope.helpers[0].LanguageTranslationName[$rootScope.clientInformation.locale_id],
					LanguageTranslationName: $scope.helpers[0].LanguageTranslationName,
					LanguageTranslationDescription: JSON.parse($scope.helpers[0].DescVariable),
					content: contentVar,
					OrderNo: newHelper.OrderNo
				};

				newHelper.DescVariable = JSON.parse(newHelper.DescVariable);
				newHelper.LanguageTranslationDescription = JSON.parse($scope.helpers[0].DescVariable);
				$scope.helpers[newHelper.InstanceHelperId] = newHelper;
				$scope.helpers[0] = {};
				$scope.tabs.push(v);

				ViewHelperService.getViewsHelpCollection();
			});
		};

		SaveService.subscribeSave($scope, function () {
			if (changedHelps.length) {
				$scope.saveHelper();
			}
		});

		$scope.saveHelper = function () {
			let updates = [];
			_.each(changedHelps, function(changedHelp){
				_.find($scope.tabs, function(o) { return o.instanceHelperId == changedHelp.InstanceHelperId; }).content = changedHelp.DescVariable[$scope.ul].html;
				let u = angular.copy(changedHelp);
				u.DescVariable = JSON.stringify(u.DescVariable);
				u.LanguageTranslationDescription[$scope.ul] = u.DescVariable;
				updates.push(u);
			});
			
			SettingsService.saveHelper(updates).then(function () {
				Translator.fetch($scope.ul, true);
				ViewHelperService.getViewsHelpCollection();
				changedHelps = [];
			});
		};

		$scope.newhelp = false;
		$scope.ab = true;
		$scope.edit = false;

		$scope.editHelp = function () {
			$scope.showVideo = false;
			$scope.edit = !$scope.edit;
			if($scope.edit == false){
				$scope.saveHelper();
				$scope.showVideo = true;
			}
		};

		$scope.deleteHelper = function (helperId) {
			SettingsService.deleteHelper(helperId).then(function () {
				ViewHelperService.getViewsHelpCollection();
			});
		};

		$scope.addButtonAllow = function (helperId) {
			$scope.activeTab = helperId;
			if (helperId == 0) {
				$scope.ab = true;
			} else {
				$scope.ab = false;
			}
		};
		$scope.createNew = false;

		$scope.showCreateNew = function () {
			$scope.createNew = true;
			$scope.adminMode = true;
		};

		$scope.doNotShow = true;
		$scope.tabs = StateParameters.tabs;

		if (typeof $scope.tabs == 'undefined') {
			$scope.tabs = [];
		}

		$scope.cancelCreation = function () {
			$scope.createNew = false;
		};

		$scope.close = function () {
			if ($scope.doNotShow) {
				ViewHelper.setShown(StateParameters.name);
			}
			MessageService.closeDialog();
		};
	}

})();