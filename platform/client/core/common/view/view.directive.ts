﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.directive('view', View);

	View.$inject = ['$compile', 'ViewService', '$controller', '$templateCache'];
	function View($compile, ViewService, $controller, $templateCache) {
		return {
			scope: false,
			replace: true,
			restrict: 'A',
			link: function (scope, element) {
				var parameters = ViewService.getCurrentLocals();
				var template = $templateCache.get(parameters.template);
				var locals = parameters.locals;
				locals.$scope = scope;

				var controller = $controller(parameters.controller, locals);

				element.html($compile(template)(scope));
				element.data('$ngControllerController', controller);
				element.children().data('$ngControllerController', controller);
			}
		};
	}
})();