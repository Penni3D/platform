﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('ViewHelperService', ViewHelperService);

	ViewHelperService.$inject = ['ApiService', '$rootScope', 'Translator'];

	function ViewHelperService(ApiService, $rootScope, Translator) {
		let self = this;
		let helperApi = ApiService.v1api('meta/helpers');
		let views = {};
		self.getViewsHelpCollection = function () {
			views = {};
			let userLanguage = $rootScope.clientInformation.locale_id;
			return helperApi.get().then(function (result) {
				//Iterate results and create intermediary result
				_.each(result, function (h) {
					if (h.DescVariable.indexOf('html') > -1) h.DescVariable = JSON.parse(h.DescVariable);

					let someLanguage = "";
					if(h.DescVariable.hasOwnProperty(userLanguage)){
						someLanguage = userLanguage
					} else {
						_.each(h.DescVariable, function(value, key){
							someLanguage = key;
						})
					}
					let contentVar = h.MediaType == 0 ? '<video src="' + h.VideoLink + '" controls preload="metadata" loop class="stretch">' +
						'</video><p class="simple-padding">' + h.DescVariable[someLanguage].html + '</p>' : '<p class="simple-padding">' + h.DescVariable[someLanguage].html + '</p>'

					if (typeof views[h.Identifier] === 'undefined') views[h.Identifier] = [];

					let t1 = {};
					let t2 = {};
					t1[userLanguage] = Translator.translate(h.NameVariable);
					t2[userLanguage] = Translator.translate(h.DescVariable);

					if(views.hasOwnProperty(h.Identifier)) {
						views[h.Identifier].push({
							instanceHelperId: h.InstanceHelperId,
							mediaType: h.MediaType,
							name: h.NameVariable,
							LanguageTranslationName: t1,
							LanguageTranslationDescription: t2,
							content: contentVar,
							DescVariable: h.DescVariable,
							NameVariable: h.NameVariable,
							VideoLink: h.VideoLink,
							OrderNo: h.OrderNo
						});
					}
				});

				//Convert results to final form
				_.each(views, function (value, key) {
					let a = angular.copy(views[key]);
					views[key] = {
						tabs: function () { return a; }
					};
				});
			});
		};

		self.getViewHelp = function (viewId) {
			return views[viewId];
		};
	}
})();
