﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.provider('Views', Views)
		.service('ViewService', ViewService);

	Views.$inject = [];

	function Views() {
		let views = {};
		let dialogs = {};
		let routers = {};

		let features = {};
		let pages = {};
		let orderedPages = [];
		let orderedFeatures = [];

		this.addView = function (view, relay) {
			if (views[view] &&
				views[view].override &&
				!relay.override
			) {
				return;
			}

			views[view] = angular.copy(relay);
		};

		this.addFeature = function (feature, name, variable, rights) {
			features[feature] = {
				name: name,
				variable: variable,
				states: angular.copy(rights),
				view: feature,
				translation: undefined
			};
		};

		this.addPage = function (page, variable, rights) {
			if (rights && rights.length) rights.push('help');
			pages[page] = {
				variable: variable,
				states: angular.copy(rights),
				view: page
			};
		};

		this.addDialog = function (dialog, relay) {
			if (typeof relay.locals === 'undefined') {
				relay.locals = {};
			}

			dialogs[dialog] = angular.copy(relay);
		};

		this.addRouter = function (router, relay) {
			if (typeof relay.locals === 'undefined') {
				relay.locals = {};
			}

			routers[router] = angular.copy(relay);
		};

		this.$get = function (Translator) {
			let translated = false;

			function translate() {
				if (translated) {
					return;
				}

				angular.forEach(features, function (f) {
					f.translation = Translator.translate(f.variable);
					orderedFeatures.push(f);
				});

				angular.forEach(pages, function (p) {
					p.translation = Translator.translate(p.variable);
					orderedPages.push(p);
				});

				orderedFeatures.sort(function (a, b) {
					return a.translation < b.translation ? -1 : 1;
				});

				orderedPages.sort(function (a, b) {
					return a.translation < b.translation ? -1 : 1;
				});

				translated = true;
			}

			return {
				getView: function (view) {
					return angular.copy(views[view]);
				},
				getDialog: function (dialog) {
					return angular.copy(dialogs[dialog]);
				},
				getRouter: function (router) {
					return angular.copy(routers[router]);
				},
				getFeatureByName: function (name) {
					for (let f in features) {
						if (features.hasOwnProperty(f) && features[f].name == name) {
							translate();
							return angular.copy(features[f]);
						}
					}
				},
				getFeatures: function () {
					translate();
					return angular.copy(orderedFeatures);
				},
				getPages: function () {
					translate();
					return angular.copy(orderedPages);
				},
				getPage: function (page) {
					translate();
					return angular.copy(pages[page]);
				}
			};
		};
	}

	ViewService.$inject = [
		'Views',
		'$rootScope',
		'UniqueService',
		'$compile',
		'$q',
		'LocalStorageService',
		'$window',
		'ViewHistoryService',
		'MessageService',
		'$templateCache',
		'SaveService',
		'Common',
		'$location',
		'BodyStackerService',
		'ViewHelper',
		'Translator',
		'$injector',
		'HTMLService',
		'ViewConfigurator',
		'Configuration',
		'$sanitize',
		'Sanitizer',
		'ErrorService',
		'FileSaver',
		'AttachmentService',
		'PortfolioService',
		'ProcessService'
	];

	function ViewService(
		Views,
		$rootScope,
		UniqueService,
		$compile,
		$q,
		LocalStorageService,
		$window,
		ViewHistoryService,
		MessageService,
		$templateCache,
		SaveService,
		Common,
		$location,
		BodyStackerService,
		ViewHelper,
		Translator,
		$injector,
		HTMLService,
		ViewConfigurator,
		Configuration,
		$sanitize,
		Sanitizer,
		ErrorService,
		FileSaver,
		AttachmentService,
		PortfolioService,
		ProcessService
	) {

		let self = this;
		let currentViews = {};
		let advancedViews = {};
		let parameters = {};
		let frontPage;
		let manualTrigger = false;
		let order = [];
		let viewQueue = [];
		let viewLoadInProgress = false;

		let viewStatus = LocalStorageService.get('viewStatus');
		let viewLevels = [];

		if (viewStatus && viewStatus.parents && viewStatus.parents.length) {
			viewLevels = viewStatus.parents;
		}

		let MAX_VIEW_SMALL = 1;
		let MAX_VIEW_NORMAL = 2;
		let MAX_VIEW_LARGE = 3;

		let VIEW_SMALL = 1024;
		let VIEW_NORMAL = 2560;

		Common.subscribeContentResize($rootScope, function () {
			fitViews(false);
		});

		function fitViews(newView) {
			let w = $window.innerWidth;
			let len = order.length;

			let s = MAX_VIEW_SMALL;
			let m = MAX_VIEW_NORMAL;
			let l = MAX_VIEW_LARGE;

			if (newView) {
				s--;
				m--;
				l--;
			}

			function c(s) {
				for (let i = 0; len > s; i++) {
					closeView(order.pop(), true);
					len--;
				}
			}

			if (w < VIEW_SMALL && len > s) {
				c(s);
			} else if (w < VIEW_NORMAL && len > m) {
				c(m);
			} else if (len > l) {
				c(l);
			}
		}

		self.widthMode = function () {
			let w = $window.innerWidth;

			if (w < 1024) {
				return 'SMALL';
			} else if (w < 2560) {
				return 'NORMAL';
			}

			return 'LARGE';
		};

		self.backdrop = function (viewId, params) {
			let view = currentViews[viewId];

			if (typeof view === 'undefined' || typeof view.$content === 'undefined') {
				return;
			}

			if (view.backdrop) {
				self.removeBackdrop(viewId);
			}

			let $backdrop = $('<div class="backdrop no-select">');
			$backdrop.append($compile($sanitize(params.template))(params.scope ? params.scope : view.scope));

			view.$content.after($backdrop).addClass('backdropped');
			view.backdrop = $backdrop;
		};

		self.removeBackdrop = function (viewId) {
			let view = currentViews[viewId];

			if (typeof view === 'undefined' || typeof view.backdrop === 'undefined') {
				return;
			}

			view.$content.removeClass('backdropped');
			view.backdrop.remove();

			delete view.backdrop;
		};

		self.openSpecialTask = function (task, split) {
			return ProcessService.getProcess(task.owner_item_id).then(function (p) {
				let complete = function (task, p) {
					let params = {
						process: p,
						title: task.primary,
						itemId: task.owner_item_id,
						rowId: ProcessService.getActualItemId(task.actual_item_id == undefined ? task.item_id : task.actual_item_id),
						jumpToKanban: null,
						openRow: undefined,
						cardId: undefined
					};
					let view = "process.gantt";
					if (task.owner_item_type === 'backlog') {
						self.view('process.backlog', {params: params});
					} else {
						if (task.task_type == 21) {
							params.jumpToKanban = true;
							self.view(view, {params: params});
						} else if (task.parent_task_type == 21 && task.task_type == 1) {
							params.jumpToKanban = true;
							params.rowId = task.stored_parent_item_id;
							self.view(view, {params: params});
						} else if (task.parent_task_type == 1 && task.task_type == 0) {
							params.jumpToKanban = true;
							params.cardId = task.item_id;
							params.rowId = task.card_sprint_item_id;
							self.view(view, {params: params});
						} else if (task.task_type == 1 && split == false) {
							params.openRow = params.rowId;
							self.view(view, {params: params});
						} else {
							return false;
						}
					}
					return true;
				};

				if (!task.parent_task_type && task.parent_item_id) {
					return ProcessService.getProcess(task.parent_item_id).then(function (tp) {
						if (tp == "task") {
							return ProcessService.getItem('task', task.parent_item_id).then(function (op) {
								task.parent_task_type = op.task_type;
								return complete(task, p);
							});
						} else {
							return complete(task, p);
						}
					});
				} else {
					return complete(task, p);
				}
			});
		};

		function showDialog(name, relay, options) {
			let dialog = Views.getDialog(name);

			setLocals(
				relay.locals,
				angular.extend(
					{},
					dialog.params,
					ViewConfigurator.getConfiguration(
						name,
						relay.params && relay.params.process ? relay.params.process : undefined),
					relay.params
				)
			);

			parameters.options = options;
			let $dialogContent = HTMLService.initialize('dialog-content')
				.inner(HTMLService.initialize('dialog-container')
					.inner(HTMLService.initialize('div').class('loading-screen align-column')
						.inner(HTMLService.initialize('loader')
							.element('h2').class('h6').content(Sanitizer(Translator.translate('VIEW_PLEASE_WAIT')))
						)
					)
				).getObject();

			MessageService.dialogAdvanced(options, true).then(function (d) {
				$compile($dialogContent)(d.scope).appendTo(d.dialog);
				resolveView(name, dialog).then(function (locals) {
					$dialogContent.remove();
					d.dialog.attr('view', '');

					parameters.locals = locals;
					parameters.controller = dialog.controller;
					parameters.template = dialog.template;

					$compile(d.dialog)(d.scope);

					viewLoaded();
				}, function () {
					viewLoaded();
				});
			});
		}

		function setLocals(locals, params) {
			parameters.locals = locals;
			parameters.StateParameters = angular.copy(params);
		}

		function getLocals() {
			return angular.extend({}, parameters.locals, {StateParameters: parameters.StateParameters});
		}

		self.getCurrentLocals = function () {
			return parameters;
		};

		function viewLoaded() {
			setTimeout(function () {
				let v = viewQueue.shift();

				if (v) {
					if (v.type === 'normal') {
						showView(v.name, v.relay, v.options, {reroute: v.reroute});
					} else if (v.type === 'advanced') {
						showViewAdvanced(v.name, v.relay, v.options);
					}
				} else {
					viewLoadInProgress = false;
				}
			});
		}

		let previousParentResolve = '';

		function resolveParent(name) {
			let splits = name.split('.');
			if (splits.length > 1) {
				let parentView = Views.getView(splits[0]);
				if (parentView && !(splits[0] == "settings" && previousParentResolve == "settings")) {
					previousParentResolve = splits[0];
					return resolveLayers(parentView, getLocals());
				}
			} else {
				previousParentResolve = "";
			}

			let d = $q.defer();
			d.resolve(getLocals());
			return d.promise;
		}

		function getPrevView(viewId) {
			let i = order.indexOf(viewId);
			if (i === -1) {
				return;
			}

			return currentViews[order[i - 1]];
		}

		function prevToolbarHighlight(viewId) {
			let view = getPrevView(viewId);
			if (typeof view === 'undefined') {
				return;
			}

			view.$header.addClass('swap-highlight');
		}

		function removePrevToolbarHighlight(viewId) {
			let view = getPrevView(viewId);
			if (typeof view === 'undefined') {
				return;
			}

			view.$header.removeClass('swap-highlight');
		}

		self.shift = function (viewId) {
			let index = order.indexOf(viewId);

			if (index > 0) {
				let current = currentViews[viewId];
				let previous = getPrevView(viewId);

				current.$view.css({
					opacity: 0,
					transform: 'translateX(-8px)'
				});

				previous.$view.css({
					opacity: 0,
					transform: 'translateX(8px)'
				});

				setTimeout(function () {
					previous.$view.before(current.$view);

					if (index === 1) {
						previous.$header.find('.swap-position').first().addClass('visible');
						current.$header.find('.swap-position').first().removeClass('visible');
					}

					setTimeout(function () {
						current.$view.css({opacity: '', transform: ''});
						previous.$view.css({opacity: '', transform: ''});
					});

					$rootScope.$emit('viewChange', {
						view: current.name,
						target: previous.name,
						action: 'SHIFT',
						viewId: viewId
					});
				}, 300);

				removePrevToolbarHighlight(viewId);

				let t = order[index - 1];
				order[index - 1] = order[index];
				order[index] = t;
			}

			rememberViews();
		};

		self.getParentView = function () {
			if (viewLevels.length < 2) {
				return;
			}

			return viewLevels[viewLevels.length - 2];
		};

		let downloadables = {};
		let fallBackDownloadables = [];
		self.addDownloadable = function (name, f, viewId) {
			if (!viewId) {
				fallBackDownloadables.push({"name": name, "onClick": f})
			} else {
				if (!downloadables[viewId]) downloadables[viewId] = [];
				downloadables[viewId].push({"name": name, "onClick": f});
			}
		};

		self.removeDownloadable = function (viewId, index?) {
			if (!index) {
				downloadables[viewId].splice(downloadables[viewId].length - 1, 1)
			} else {
				downloadables[viewId].splice(index, 1)
			}
		}

		self.router = function (name, relay, options) {
			let router = Views.getRouter(name);

			if (typeof router === 'undefined') {
				return;
			}

			let r = router.route;
			setLocals(relay.locals, relay.params);

			if (typeof r === 'function') {
				r = $injector.invoke(r, null, getLocals());
			}

			showView(r, relay, options, {});
		};

		let activeItemId = 0;

		self.getActiveItemId = function () {
			return activeItemId;
		}

		function showView(name, relay, options, params) {
			//Add old view to layout routes collection
			_.each(currentViews, function (view) {
				if (!view.params.deleted) {
					self.addLayoutRoute(view);
				} else {
					Common.notifyViewLoaded();
				}
			});

			let tabIdOverride = "";
			if (name == '1stTab' || name == 'lastTab') {
				tabIdOverride = name;
				name = 'process.tab';
			} else if (!isNaN(parseInt(name))) {
				tabIdOverride = name;
				name = 'process.tab';
			} else if (name == 'process.tab' && relay.params.additionalFeaturetteParams && relay.params.additionalFeaturetteParams.hasOwnProperty('subState') && relay.params.additionalFeaturetteParams.subState != '') {
				tabIdOverride = relay.params.additionalFeaturetteParams.subState;
			}

			if (name === 'no_view') {
				viewLoaded();
				ProcessService.getDialogContainers(relay.params.process).then(function (dialogContents) {
					MessageService.creationContainerFields = [];
					let fields = [];
					if (dialogContents.inputs) {
						_.each(dialogContents.inputs, function (levelOfFields) {
							_.each(levelOfFields, function (field) {
								fields.push(field);
							})
						})
					}
					MessageService.creationContainerFields = fields;
					if (!(!dialogContents.inputs.length && !dialogContents.texts.length)) {
						self.view('new.default', {params: relay.params});
						return
					}

					let data = {
						Data: {
							copy_date: new Date(),
							copy_to_start_date: relay.params.modelStartDate,
							copy_to_end_date: relay.params.modelEndDate,
							CopyModelProcess: relay.params.modelProcess,
							CopyModelItemColumn: relay.params.modelItemColumn,
							CopyModelInstanceUnionId: relay.params.modelCopyUnion
						}
					};


					ProcessService.newItem(relay.params.process, data).then(function (itemId) {
						ProcessService.getItemData(relay.params.process, itemId, true).then(function (item) {
							let p = {
								params: {
									process: relay.params.process,
									itemId: itemId,
									tabId: undefined
								}
							};

							let state = 'process.tab';

							if (item.Data.current_state) {
								if (_.startsWith(item.Data.current_state, 'tab')) {
									p.params.tabId = item.Data.current_state.split('_')[1];
								} else {
									state = item.Data.current_state;
								}
							}

							self.view(state, p);

						});
					});
				})
				return;
			}
			let view = Views.getView(name);

			if (typeof view === 'undefined') {
				if (Views.getDialog(name)) {
					showDialog(name, relay, options);
				} else if (Views.getRouter(name)) {
					fallBackDownloadables = [];
					self.router(name, relay, options);

				} else {
					fallBackDownloadables = [];
					MessageService.msgBox('VIEW_MISSING_FAIL');
					viewLoaded();
				}

				return;
			} else {
				fallBackDownloadables = [];
			}

			setLocals(
				relay.locals,
				angular.extend(
					{},
					view.params,
					ViewConfigurator.getConfiguration(
						name,
						relay.params && relay.params.process ? relay.params.process : undefined),
					relay.params)
			);


			let defaultOptions = angular.extend({}, view.options, options);
			let rememberView = true;

			if (typeof options.size === 'undefined' && view.options) {
				defaultOptions.size = view.options.size;
			}

			if (defaultOptions.size === 'narrower') {
				defaultOptions.size = 'narrower';
			} else if (defaultOptions.size === 'wider') {
				defaultOptions.size = 'wider';
			} else {
				defaultOptions.size = '';
			}

			if (relay.locals && Object.keys(relay.locals).length) {
				rememberView = false;
			} else if (typeof defaultOptions.remember !== 'undefined') {
				rememberView = Common.isTrue(defaultOptions.remember);
			}

			let viewId = UniqueService.get('view', true);
			let $replaceTargetView;
			let orderPosition = 0;

			if (Common.isTrue(defaultOptions.split)) {
				if (defaultOptions.replace) {
					var targetViewId = -1;
					let c = true;

					angular.forEach(currentViews, function (view, viewId) {
						if (c && (view.name === defaultOptions.replace || viewId === defaultOptions.replace)) {
							targetViewId = viewId;

							if (defaultOptions.position === 'first') {
								c = false;
							}
						}
					});

					let i = order.indexOf(targetViewId);
					if (i !== -1) {
						if (i === 0) {
							defaultOptions.position = 'first';
						} else {
							$replaceTargetView = currentViews[order[i - 1]].$view;
							orderPosition = i;
						}
					}

					closeView(targetViewId, true);
				}

				let p = defaultOptions.preventOn;
				if (p) {
					let c = true;
					angular.forEach(currentViews, function (view, key) {
						if (c) {
							for (let i = 0, l = p.length; i < l; i++) {
								if (p[i] === view.name && key != targetViewId) {
									MessageService.toast('VIEW_SPLIT_FAIL', 'warn');
									c = false;

									break;
								}
							}
						}
					});

					if (!c) {
						viewLoaded();
						return;
					}
				}

				let f = false;
				angular.forEach(currentViews, function (v) {
					if (v.name === name) {
						let open = true;
						angular.forEach(parameters.StateParameters, function (value, key) {
							if (v.params[key] === value) {
								return;
							}

							open = false;
						});

						if (open) {
							MessageService.toast('VIEW_DUPLICATE_FAIL', 'warn');
							f = true;
						}
					}
				});

				if (f) {
					viewLoaded();
					return;
				}

				fitViews(true);
			} else {
				angular.forEach(currentViews, function (view, viewId) {
					closeView(viewId, true)
				});
			}

			if (defaultOptions.replace == 'featurette' && relay.params.hasOwnProperty('size')) {
				defaultOptions.size = relay.params.size;
			}

			let scope = $rootScope.$new(true);
			let $view = HTMLService.initialize('div').class(defaultOptions.size).attr('id', viewId).getObject();

			if (tabIdOverride != "") parameters.StateParameters.tabId = tabIdOverride;
			parameters.StateParameters.$$helpName = name;

			let viewObject = {
				name: name,
				options: defaultOptions,
				scope: scope,
				$view: $view,
				locals: parameters.locals,
				relay: angular.copy(relay.params),
				params: parameters.StateParameters,
				remember: rememberView
			};


			currentViews[viewId] = viewObject;
			viewObject.params.ViewId = viewId;


			if (Common.isFalse(defaultOptions.split)) {
				if (typeof view.level === 'undefined') {
					viewLevels.splice(0, viewLevels.length, viewObject)
				} else {
					if (viewLevels.length < view.level) {
						viewLevels.push(viewObject);
					} else {
						viewLevels.length = viewLevels.length - 1;
						viewLevels.push(viewObject);
					}
				}
			}

			if (name === 'frontpage') {
				frontPage = viewId;
			} else {
				closeView(frontPage, true);

				let html = HTMLService.initialize()
					.element('h1').class('h5').content('{{(header.title | translate) | RemoveBrackets}} {{header.extTitle | translate}}')
					.element('span').class('flex');

				html
					.button().if('hasHelp').click('ShowHelp()').attr('tooltip', 'HELP').attr('type', 'icon')
					.inner(HTMLService.initialize().icon('help_outline'));

				scope.ShowHelp = function () {
					ViewHelper.showHelp(name, viewObject.params);
				};


				html
					.button().if('header.beforeDownloadIcons[0] && !header.beforeDownloadIcons[0].hideThis').click('header.beforeDownloadIcons[0].onClick($event)').attr('type', 'icon')
					.inner(HTMLService.initialize('div')
						.tabindex(0)
						.class('material-icons aria-detect')
						.attr('tooltip', '{{header.beforeDownloadIcons[0].tooltip()}}')
						.bind('::header.beforeDownloadIcons[0].icon')
					)
					.element('span').if('header.beforeDownloadIcons[0]').content('{{header.beforeDownloadIcons[1].tooltip()}}')

				html
					.button().if('header.beforeDownloadIcons[1] && !header.beforeDownloadIcons[1].hideThis').click('header.beforeDownloadIcons[1].onClick($event)').attr('type', 'icon')
					.inner(HTMLService.initialize('div')
						.tabindex(0)
						.class('material-icons aria-detect')
						.attr('tooltip', '{{header.beforeDownloadIcons[1].tooltip()}}')
						.bind('::header.beforeDownloadIcons[1].icon')
					)

				//Default Capture Menu
				html
					.button()
					.click('Capture($event)').attr('tooltip', 'VIEW_CAPTURE').attr('type', 'icon')
					.inner(HTMLService.initialize('div')
						.tabindex(0)
						.class('material-icons aria-detect')
						.inner(HTMLService.initialize().icon('cloud_download'))
					);
				scope.Capture = function (event) {
					let fileMenu = [];
					if ($("content-footbar table-pager div.btn").length > 0) {
						fileMenu.push(
							{
								name: 'EXPORT_TO_PDF_PRINT', onClick: function () {
									capture(viewId, 'print');
								}
							}
						);
					}
					fileMenu.push(
						{
							name: 'EXPORT_TO_PDF_CAPTURE', onClick: function () {
								capture(viewId, 'capture');
							}
						},
						{
							name: 'EXPORT_TO_PNG', onClick: function () {
								capture(viewId, 'png');
							}
						}
					);

					if (downloadables[viewId]) {
						for (let v of downloadables[viewId]) {
							fileMenu.push({name: v.name, onClick: v.onClick});
						}
					} else if (fallBackDownloadables.length) {
						for (let v of fallBackDownloadables) {
							fileMenu.push({name: v.name, onClick: v.onClick});
						}
					}

					let hasXhr = false;

					if (parameters && parameters.locals && parameters.locals.Navigation) {
						for (let nav of parameters.locals.Navigation) {
							if (nav.processGroupId == parameters.StateParameters.processGroupId) {
								for (let tab of nav.tabs) {
									if (tab.params && (tab.params.tabId == parameters.StateParameters.tabId || tab.params.title == parameters.StateParameters.title)) {
										if (tab.templatePortfolioId > 0) {
											hasXhr = true;
											AttachmentService.getControlAttachments(tab.templatePortfolioId, 'portfolioTemplates').then(function (atts) {
												for (let file of atts) {
													fileMenu.push({
														name: file.Name, onClick: function () {
															self.generateWord(viewId, parameters.StateParameters.process,
																tab.templatePortfolioId,
																file.AttachmentId,
																file.Name,
																parameters.StateParameters.itemId
															);
														}
													});
												}
												MessageService.menu(fileMenu, event.currentTarget);
											});
										}
									}
								}
							}
						}
					}
					if (!hasXhr) MessageService.menu(fileMenu, event.currentTarget);
				};

				//All dynamic menu items
				html
					.button().repeat('icon in header.icons').click('icon.onClick($event)').attr('type', 'icon')
					.inner(HTMLService.initialize('div')
						.tabindex(0)
						.class('material-icons aria-detect')
						.attr('tooltip', '{{::icon.tooltip}}')
						.ngClass('::icon.class')
						.ngStyle('{ color: icon.color }')
						.bind('::icon.icon')
					)
					.button().click('ShowHeaderMenu($event)').attr('type', 'icon').if('header.menu.length')
					.inner(HTMLService.initialize().icon('more_vert'))
					.button().click('CloseView()').if('ShowClose').attr('no-waves').attr('type', 'icon')
					.inner(HTMLService.initialize().icon('close'));

				let $header = HTMLService.initialize('content-header').inner(html).getObject().appendTo($view);
				let $control = HTMLService.initialize('div').class('swap-position')
					.inner(HTMLService.initialize('div')
						.class('material-icons aria-detect')
						.click('ShiftLeft()')
						.content('swap_horiz')
					).getObject();

				$control.prependTo($header);

				if (order.length) {
					if (defaultOptions.position === 'first') {
						currentViews[order[0]].$header.find('.swap-position').addClass('visible');
					} else {
						$control.addClass('visible');
					}

					angular.forEach(currentViews, function (view) {
						view.scope.ShowClose = true;
					});
				} else {
					scope.ShowClose = false;
				}

				$control.hover(function () {
					prevToolbarHighlight(viewId);
				}, function () {
					removePrevToolbarHighlight(viewId);
				});

				viewObject.$header = $header;

				scope.ShiftLeft = function () {
					self.shift(viewId, 'left');
				};

				scope.ShowHeaderMenu = function (event) {
					MessageService.menu(scope.header.menu, event.currentTarget);
				};

				scope.CloseView = function () {
					setTimeout(function () {
						$(".highlight-permanent").removeClass('highlight-permanent').addClass('highlight-permanent-remove');
					}, 110);
					self.close(viewId);
				};
			}

			let $loader = HTMLService.initialize('div').class('loading-screen align-column')
				.inner(HTMLService.initialize('loader')
					.element('h2').class('h6').content(Sanitizer(Translator.translate('VIEW_LOADING_CONTENT')))
				)
				.getObject().appendTo($view);

			let $content = $('#main-container-transition');

			if (defaultOptions.replace) {
				if ($replaceTargetView) {
					$replaceTargetView.after($view);
					order.splice(orderPosition, 0, viewId);
				} else {
					if (defaultOptions.position === 'first') {
						order.unshift(viewId);
						$view.prependTo($content);
					} else {
						order.push(viewId);
						$view.appendTo($content);
					}
				}
			} else if (defaultOptions.position === 'first') {
				$view.prependTo($content);
				order.unshift(viewId);
			} else {
				$view.appendTo($content);
				order.push(viewId);
			}

			setTimeout(function () {
				$view.addClass('transition-in');
				viewObject.sensor = new ResizeSensor($view, function () {
					Common.notifyContentResize();
				});
			}, 75);

			$compile($view)(scope);

			function reject(error) {
				$loader.html(getViewFailure(error, scope));

				$rootScope.$emit('viewChange', {
					view: name,
					action: 'FAIL',
					viewId: viewId,
					split: defaultOptions.split
				});
			}

			let splitEmit = false;
			if (defaultOptions.split && order.length > 1) {
				splitEmit = true;
				$rootScope.$emit('viewChange', {
					view: name,
					action: 'PRE-SPLIT',
					viewId: viewId
				});
			} else if (params.reroute) {
				$rootScope.$emit('viewChange', {
					view: name,
					action: 'PRE-REROUTE',
					viewId: viewId
				});
			} else {
				$rootScope.$emit('viewChange', {
					view: name,
					action: 'PRE-NEW',
					viewId: viewId
				});
			}

			resolveView(name, view, viewId).then(function (locals) {
				let $viewContent = name === 'frontpage' ?
					$('<div view>') : $('<content view header="header">');

				if (Object.keys(currentViews).length === 1) {
					if (params.history && rememberView) {
						manualTrigger = true;
						$location.hash(ViewHistoryService.add(viewObject));
					}
				}

				//Help Icon for all 'elements' inside a process (so if view object has process and item id)
				if (viewObject.params && viewObject.params.hasOwnProperty('process') && viewObject.params.hasOwnProperty('itemId')) {
					ViewHelper.hasHelpFeature(name, viewObject.params ? viewObject.params : undefined).then(function (result) {
						scope.hasHelp = result;
					});
				} else {
					ViewConfigurator.getRights(name, viewObject.params.NavigationMenuId).then(function (rights) {
						scope.hasHelp = ViewHelper.hasHelpPage(name, rights, viewObject.params ? viewObject.params : undefined);
					});
				}

				// let $fab = HTMLService.initialize('input-button').attr('type', 'fab').click('ScrollTop()')
				// 	.inner(HTMLService.initialize().icon('keyboard_arrow_up'))
				// 	.getObject();

				parameters.locals = locals;
				parameters.controller = view.controller;
				parameters.template = view.template;

				viewObject.$content = $viewContent;
				//viewObject.$fab = $fab;

				scope.header = {};
				scope.ScrollTop = function () {
					self.scrollTop(viewId);
				};

				$compile($viewContent)(scope).appendTo($view);
				//$viewContent.after($compile($fab)(scope));

				if (!params.initialisation) {
					rememberViews();
				}

				// $viewContent.on('scroll', function () {
				// 	if ($viewContent.scrollTop() > 254) {
				// 		$fab.addClass('transition-in');
				// 	} else {
				// 		$fab.removeClass('transition-in');
				// 	}
				// });

				setTimeout(function () {
					$loader.addClass('transition-out');
					$viewContent.addClass('transition-in');
					Common.notifyContentResize();

					if (options.scrollTop) {
						$viewContent.scrollTop(options.scrollTop);
					}
					if (!ViewHelper.isShown(name) &&
						Common.isFalse(Configuration.getClientConfigurations('Main application', 'Disable autohelp'))
					) {
						ViewHelper.hasHelpFeature(name).then(function (result) {
							if (result) ViewHelper.showHelp(name, true);
						});
					}
					setTimeout(function () {
						$loader.remove();
					}, 225);


					if (splitEmit) {
						$rootScope.$emit('viewChange', {
							view: name,
							action: 'SPLIT',
							viewId: viewId,
							parameters: viewObject.params
						});
					} else if (params.reroute) {
						$rootScope.$emit('viewChange', {
							view: name,
							action: 'REROUTE',
							viewId: viewId
						});
					} else {
						$rootScope.$emit('viewChange', {
							view: name,
							action: 'NEW',
							viewId: viewId,
							parameters: viewObject.params
						});
					}
				}, 150);

				viewLoaded();
			}, function (error) {
				reject(error);
				viewLoaded();
			});
		}

		function bypassViews() {
			let i = viewQueue.length;
			while (i--) {
				viewQueue.splice(i, 1);
			}
		}

		function captureCanvas(viewId, view) {
			return html2canvas(view.$content[0], {
				width: view.$content[0].scrollWidth,
				height: view.$content[0].scrollHeight,
				onclone: function (clone) {
					let d = $q.defer();
					let $clone = $(clone);

					$clone.find('body').addClass('capture');
					$clone
						.find('#' + viewId + ' content[view]')
						.css({
								'min-width': view.$content[0].scrollWidth,
								'min-height': 0,
								'max-height': 'none',
								'height': 'auto',
								'overflow': 'visible',
								'display': 'inline-block'
							}
						);

					setTimeout(function () {
						d.resolve();
					}, 550);

					return d.promise;
				}
			})
		}

		function resizeImage(img, maxWidth, maxHeight) {
			if (!(maxWidth && maxHeight && maxWidth > 0 && maxHeight > 0)) return img;

			let canvas = document.createElement("canvas")
			let ctx = canvas.getContext("2d")
			let canvasCopy = document.createElement("canvas")
			let copyContext = canvasCopy.getContext("2d")
			let ratio = 1

			if (img.width > maxWidth)
				ratio = maxWidth / img.width
			else if (img.height > maxHeight)
				ratio = maxHeight / img.height

			canvasCopy.width = img.width
			canvasCopy.height = img.height
			copyContext.drawImage(img, 0, 0)

			canvas.width = img.width * ratio
			canvas.height = img.height * ratio
			ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height)

			return canvas;
		}

		self.generateWord = function (viewId, process, portfolioId, fileId, fileName, itemId) {
			MessageService.toast('PORTFOLIO_EXPORTING_PDF');

			//Save images
			let tabs = [];
			$("#" + viewId).find(".tab-elements").first().children().each(function () {
				tabs.push($(this));
			});

			let iterateTabs = function (index) {
				let $tab = tabs[index];
				if (!$tab)
					tabsComplete();
				else
					setTimeout(function () {
						$tab.trigger("click");
						iterateTabs(index + 1);
					}, 250);
			};

			let tabsComplete = function () {
				let images = {};
				$('canvas[template-tag]').each(function (index, canvas) {
					let tag = $(canvas).attr("template-tag");
					let dpr = window.devicePixelRatio || 1;
					if (!images[tag]) images[tag] = [];
					canvas = resizeImage(canvas, canvas.width / dpr, canvas.height / dpr);
					images[tag].push(canvas.toDataURL("image/png", 1.0).split(",")[1]);
				});

				$('img[template-tag]').each(function (index, img) {
					let tag = $(img).attr("template-tag");
					if (!images[tag]) images[tag] = [];
					images[tag].push(img.currentSrc.split(",")[1]);
				});

				$('template-text').each(function (index, text) {
					let tag = $(text).attr("template-tag") + "-labels";
					if (!images[tag]) images[tag] = [];
					images[tag].push($(text).html());
				});
				PortfolioService.getPortfolioPdf(
					process,
					portfolioId,
					fileId,
					{name: fileName},
					itemId,
					images
				);
			};

			iterateTabs(0);
		}

		function capture(viewId, mode) {
			let view = currentViews[viewId];

			if (typeof view === 'undefined') {
				return;
			}

			if (typeof view.scope.OnBeforeCapture === 'function') {
				view.scope.OnBeforeCapture();
			}

			MessageService.toast('CAPTURING');
			view.$content[0].scrollTop = 0;

			let width = view.$content[0].scrollWidth;
			let height = view.$content[0].scrollHeight;
			$rootScope.$emit('notifyShowLoading', {show: true});

			setTimeout(function () {
				if (typeof currentViews[viewId] === 'undefined') return;

				let complete = function () {
					if (typeof currentViews[viewId] === 'undefined') return;
					if (typeof view.scope.OnAfterCapture === 'function') view.scope.OnAfterCapture();
					$rootScope.$emit('notifyShowLoading', {show: false});
				};

				if (mode === 'png') {
					if (view.params.process && view.params.itemId) {
						ProcessService.getItem(view.params.process, view.params.itemId).then(function (pap) {
							if (!view.scope.header.title) view.scope.header.title = "";
							let myArr = view.scope.header.title.split("-");
							let title = pap.primary && myArr.length > 1
								? pap.primary + " - " + myArr[1] + " - " + myArr[0]
								: view.scope.header.title;

							captureCanvas(viewId, view).then(function (canvas) {
								canvas.toBlob(function (blob) {
									FileSaver.saveAs(blob, title + '.png');
								}, 'image/png');
								complete();
							});
						})
					} else {
						captureCanvas(viewId, view).then(function (canvas) {
							canvas.toBlob(function (blob) {
								FileSaver.saveAs(blob, view.scope.header.title + '.png');
							}, 'image/png');
							complete();
						});

					}
				}

				if (mode == 'capture') {
					let orientation = "p";
					if (width > height) orientation = "l";
					const pdf = new jsPDF(orientation, "px", [width, height]);

					captureCanvas(viewId, view).then(function (canvas) {

						if (view.params.process && view.params.itemId) {
							ProcessService.getItem(view.params.process, view.params.itemId).then(function (pap) {
								let myArr = view.scope.header.title.split("-");
								let title = pap.primary && myArr.length > 1
									? pap.primary + " - " + myArr[1] + " - " + myArr[0]
									: view.scope.header.title;
								const imgData = canvas.toDataURL('image/png', 1.0);
								const imgProps = pdf.getImageProperties(imgData);
								let pdfWidth = pdf.internal.pageSize.getWidth();
								let pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
								pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight, undefined, 'fast');
								pdf.save(title + '.pdf');
								complete();
							});
						} else {
							const imgData = canvas.toDataURL('image/png', 1.0);
							const imgProps = pdf.getImageProperties(imgData);
							let pdfWidth = pdf.internal.pageSize.getWidth();
							let pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
							pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight, undefined, 'fast');
							pdf.save(view.scope.header.title + '.pdf');
							complete();
						}
					});
				}

				if (mode === 'print') {
					let cf = function (index) {
						let content = $("content div row-manager");
						if (content.length == 0) {
							//Page not loaded
							setTimeout(function () {
								cf(index);
							}, 100);
						} else {
							if (btn_next.attr("disabled") == 'disabled' && last_done) {
								//Complete loop
								pdf.save(view.scope.header.title + '.pdf');
								if (btn_prev.attr("disabled") != 'disabled') $(to_first_page).trigger("click");
								complete();
							} else {
								//Capture
								if (btn_next.attr("disabled") == 'disabled') last_done = true;

								captureCanvas(viewId, view).then(function (canvas) {
									const imgData = canvas.toDataURL('image/png', 1.0);
									const imgProps = pdf.getImageProperties(imgData);
									orientation = "p";
									if (canvas.width > canvas.height) orientation = "l";
									if (index > 0) pdf.addPage();
									let pdfWidth = pdf.internal.pageSize.getWidth();
									let pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
									pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight, undefined, 'fast');
									//Next page if available
									if (btn_next.attr("disabled") != 'disabled') $(btn_next).trigger("click");
									setTimeout(function () {
										cf(1);
									}, 500);
								});
							}
						}
					};

					let orientation = "p";
					if (width > height) orientation = "l";
					let pdf = new jsPDF(orientation, "px", "a4");

					let last_done = false;
					let btn_prev = $("content-footbar table-pager div.btn").first();
					let btn_next = $("content-footbar table-pager div.btn").last();
					let to_first_page = $("content-footbar table-pager span#to_first_page");

					if (btn_next.length > 0) {
						//There are pages
						if (btn_prev.attr("disabled") != 'disabled') {
							$(to_first_page).trigger("click");
							setTimeout(function () {
								cf(0);
							}, 500);
						} else {
							cf(0);
						}
					} else {
						//There is no pages
						pdf = new jsPDF(orientation, "px", [width, height]);
						captureCanvas(viewId, view).then(function (canvas) {
							const imgData = canvas.toDataURL('image/png', 1.0);
							const imgProps = pdf.getImageProperties(imgData);
							let pdfWidth = pdf.internal.pageSize.getWidth();
							let pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
							pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight, undefined, 'fast');
							pdf.save(view.scope.header.title + '.pdf');

							complete();
						});
					}
				}
			}, 350);
		}

		self.resolveTransition = function (row, toState, Portfolio) {
			let feature = Portfolio.DefaultState;
			if (typeof row.current_state === 'string' && row.current_state !== '' && (!feature || Portfolio.UseDefaultState == 0)) {
				if (row.current_state.indexOf('{') === -1) {
					let state = row.current_state;
					if (state.indexOf('tab') === -1) {
						toState.view = state;
					} else {
						toState.params.tabId = state.split('_')[1];
					}
				} else {
					toState = angular.fromJson(row.current_state);
					toState.params.itemId = row[toState.ownerIdCol];
				}
			} else if (feature) {
				toState.view = feature;
			}
		};
		self.test = function () {
			return angular.copy(parameters.StateParameters);
		};
		self.view = function (name, relay, options, params) {
			if (relay && relay.params && relay.params.itemId) {
				activeItemId = Number(relay.params.itemId);
			}

			if (name == 'nothing') name = 'process.tab';
			if (typeof relay !== 'object') relay = {};

			relay.params = angular.copy(relay.params);
			params = typeof params === 'object' ? angular.copy(params) : {history: true, initilization: false};
			options = typeof options === 'object' ? angular.copy(options) : {};

			let view = Views.getView(name);
			if (view && (Common.isFalse(options.split) || (view.options && Common.isFalse(view.options.split))))
				bypassViews();

			if (viewLoadInProgress || viewQueue.length) {
				let v = {
					type: 'normal',
					name: name,
					relay: relay,
					options: options,
					history: params.history,
					initialisation: params.initialisation
				};

				view ? viewQueue.push(v) : viewQueue.unshift(v);
			} else {
				viewLoadInProgress = true;
				var hasSaved = false; //Stick with VAR
				var lockTimer = false; //Stick with VAR

				self.lockViews();
				setTimeout(function () {
					lockTimer = true;

					if (hasSaved) {
						self.unlockViews();
					}
				}, 600);

				SaveService.bypassTick(true).then(function () {
					hasSaved = true;
					if (lockTimer) self.unlockViews();


					showView(name, relay, options, {
						history: params.history,
						initialisation: params.initialisation
					});

					//Open featurette to the right, if additional view is defined
					if (relay && relay.params && relay.params.additionalFeaturetteParams) {
						self.view('featurette',
							{
								params: relay.params.additionalFeaturetteParams
							},
							{
								split: true
							}
						);
					}
				}, function () {
					viewLoaded();

					MessageService.dialog({
						title: 'VIEW_SAVE_FAIL_TITLE',
						message: 'VIEW_SAVE_FAIL',
						buttons: [
							{
								text: 'VIEW_SAVE_FAIL_BACK'
							},
							{
								type: 'primary',
								text: 'VIEW_SAVE_FAIL_CONTINUE',
								onClick: function () {
									showView(name, relay, options, {
										history: params.history,
										initialisation: params.initialisation
									});
								}
							}
						]
					});
				});
			}
		};

		self.lock = function (viewId) {
			let view = currentViews[viewId];
			if (typeof view === 'undefined' || view.name === 'frontpage') {
				return;
			}

			let h = HTMLService.initialize('div').class('loading-screen align-column view-lock')
				.inner(HTMLService.initialize('loader').class('contrast'))
				.getHTML();

			self.backdrop(viewId, {
				template: h
			});
		};

		self.lockViews = function () {
			angular.forEach(currentViews, function (view, viewId) {
				self.lock(viewId);
			});
		};

		self.unlockViews = function () {
			angular.forEach(currentViews, function (view, viewId) {
				self.unlock(viewId);
			});
		};

		self.unlock = function (viewId) {
			if (typeof currentViews[viewId] === 'undefined') {
				return;
			}

			self.removeBackdrop(viewId);
		};

		function showViewAdvanced(name, relay, options) {
			let viewId = Common.isFalse(options.extend) ?
				options.targetId : options.targetId + '-' + UniqueService.get('view');

			if (advancedViews[viewId]) {
				closeView(viewId);
			}

			if (Common.isTrue(options.extend) && options.threshold) {
				let w = $window.innerWidth;

				if (w > VIEW_NORMAL &&
					(options.threshold === 'large' || options.threshold === 'normal') ||
					(w > VIEW_SMALL && options.threshold === 'normal')
				) {
					options.split = true;
					self.view(name, relay, options);
					viewLoaded();

					return;
				}
			}

			let view = Views.getView(name);

			if (typeof view === 'undefined') {
				viewLoaded();
				return;
			}

			setLocals(
				relay.locals,
				angular.extend(
					{},
					view.params,
					ViewConfigurator.getConfiguration(
						name,
						relay.params && relay.params.process ? relay.params.process : undefined
					),
					relay.params)
			);

			let scope = $rootScope.$new(true);
			let parent = findClosestCurrentView(Common.isFalse(options.extend) ? options.ParentViewId : options.targetId);

			if (typeof parent == 'undefined') {
				viewLoaded();
			}

			let $target;
			let $loader = $compile(
				HTMLService.initialize('div').class('advanced-loading-screen')
					.inner(HTMLService.initialize('loader'))
					.getHTML())(scope);

			if (Common.isFalse(options.extend)) {
				$target = $('#' + viewId);
				$target.html($loader);
			} else if (currentViews[options.targetId]) {
				let targetView = currentViews[options.targetId];

				$target = $('#' + options.targetId);
				$loader.appendTo($target);

				setTimeout(function () {
					targetView.$content.css('display', 'none');
					targetView.$content.addClass('transition-out');

					self.hideFootbar(options.ParentViewId);
				}, 75);
			} else {
				return;
			}

			if (!parent) {
				return;
			}

			$target.css({
				'position': 'relative',
				'min-height': 128
			});
			parent.scope.$on('$destroy', function () {
				closeView(viewId);
			});

			let viewObject = {
				name: name,
				scope: scope,
				$target: $target,
				locals: parameters.locals,
				relay: angular.copy(relay.params),
				params: parameters.StateParameters,
				$content: $loader,
				parent: parent
			};

			advancedViews[viewId] = viewObject;

			parameters.StateParameters.ViewId = viewId;
			parameters.StateParameters.ParentViewId = options.ParentViewId;

			resolveView(name, view, viewId).then(function (locals) {
				if (typeof advancedViews[viewId] === 'undefined') {
					return;
				}

				let $viewContent;
				if (Common.isFalse(options.extend)) {
					$viewContent = $('<div view>');
				} else {
					let removeView = function () {
						$viewContent.addClass('transition-out');

						setTimeout(function () {
							closeView(viewId);
							targetView.$content.css('display', 'block');
							self.showFootbar(options.ParentViewId);
						}, 300)
					};

					scope.CloseView = removeView;

					$viewContent = $('<content view class="view-extended">');
					BodyStackerService.add({id: viewId, remove: removeView});
				}

				viewObject.$content = $viewContent;
				parameters.locals = locals;
				parameters.controller = view.controller;
				parameters.template = view.template;

				$compile($viewContent)(scope).appendTo($target);

				viewObject.sensor = new ResizeSensor($target, function () {
					Common.notifyContentResize();
				});

				setTimeout(function () {
					$viewContent.addClass('transition-in');
					$loader.addClass('transition-out');

					Common.notifyContentResize();
					setTimeout(function () {
						$loader.remove();
						$target.css('min-height', '');
					}, 225);

					if (Common.isTrue(options.extend)) {
						$compile(
							HTMLService.initialize('i')
								.class('material-icons view-extended-close pointer')
								.click('CloseView()')
								.content('close')
								.getHTML()
						)(scope).appendTo($viewContent);
					}
				}, 75);

				viewLoaded();
			}, function (error) {
				$target.html(getViewFailure(error, scope));
				viewLoaded();
			});
		}

		function getViewFailure(error, scope) {
			scope.error = error;

			let failureContent = HTMLService.initialize()
				.icon('error')
				.element('h2').class('h6');

			if (error.config && error.data && error.data.Info) {
				scope.showMore = function () {
					scope.showError = true;
				};

				failureContent
					.content('{{::error.data.Variable || \'VIEW_FAIL\' | translate}}')
					.button().attr('type', 'warn').if('!showError').class('align-item-end').click('showMore()')
					.content(Sanitizer(Translator.translate('VIEW_SHOW_ERROR')))
					.element('div').if('showError').inner(
					HTMLService.initialize()
						.element('div').class('description body-1').content(Sanitizer(error.config.url))
						.element('div').class('description body-1')
						.content(Sanitizer(Translator.translate(error.data.Info.description)) + "<br><br>" + Sanitizer(error.data.Info.stacktrace))
				);
			} else if (error.customError) {
				failureContent.content('{{::error.customError | translate}}');
			} else {
				if (!(error && error.status === 404)) {
					ErrorService.clientError(error, 'AngularJS');
				}

				failureContent.content(Sanitizer(Translator.translate('VIEW_FAIL')));
			}

			return $compile(
				HTMLService.initialize('div').class('loading-failure align-column')
					.inner(failureContent)
					.getObject())(scope);
		}

		function resolveView(name, view, viewId) {
			return resolveParent(name).then(function (parentLocals) {
				if (typeof viewId !== 'undefined' && typeof self.getView(viewId) === 'undefined') return {};
				return resolveLayers(view, parentLocals);
			});
		}

		function resolveLayers(view, parentLocals) {
			if (view.beforeResolve) {
				return Common.resolve(view.beforeResolve, parentLocals).then(function (beforeLocals) {
					return Common.resolve(view.resolve, beforeLocals).then(function (locals) {
						return Common.resolve(view.afterResolve, locals);
					});

				});
			}

			return Common.resolve(view.resolve, parentLocals).then(function (locals) {
				return Common.resolve(view.afterResolve, locals);
			});
		}

		function findClosestCurrentView(viewId) {
			let curView = currentViews[viewId];
			if (typeof curView === 'undefined') {
				let advView = advancedViews[viewId];
				return typeof advView === 'undefined' ? undefined : findClosestCurrentView(advView.params.ParentViewId);
			}

			return curView;
		}

		self.goBack = function () {
			window.history.back();
		};

		self.notification = function (viewId, message) {
			let view = currentViews[viewId];
			if (typeof view === 'undefined') {
				return;
			}

			let $notification = HTMLService.initialize('content-notification')
				.inner(HTMLService.initialize('content-notification-content')
					.class('body-1').content(Sanitizer(message)))
				.inner(HTMLService.initialize('input-button')
					.attr('type', 'icon').attr('minimal').click('dismiss()').class('close-notification')
					.inner(HTMLService.initialize().icon('close'))
				).getObject();

			if (view.notification) {
				view.notification.dismiss();
			}

			let scope = view.scope.$new();
			let finalTimer;
			let timeoutTimer;
			let transitionTimer;
			let initialTimer = setTimeout(function () {
				view.$content.before($compile($notification)(scope));
				scope.dismiss = view.notification.dismiss;

				transitionTimer = setTimeout(function () {
					$notification.addClass('transition-in');
				}, 75);

				timeoutTimer = setTimeout(function () {
					scope.dismiss();
				}, 9000);
			}, 150);

			view.notification = {
				$notification: $notification,
				dismiss: function () {
					$notification.removeClass('transition-in');
					clearTimeout(initialTimer);
					clearTimeout(transitionTimer);
					clearTimeout(finalTimer);
					clearTimeout(timeoutTimer);

					setTimeout(function () {
						$notification.remove();
					}, 150);

					scope.$destroy();
					delete view.notification;
				}
			};
		};

		self.hideFootbar = function (viewId) {
			if (currentViews[viewId]) {
				let view = currentViews[viewId];
				let footbar = view.footbar;

				if (typeof footbar === 'undefined') {
					return;
				}


				let mod = 0;
				if (Common.isMobile()) mod = -46;

				let p = 84 - mod;
				let v = 174 - mod;

				footbar.$content.css('display', 'none');
				let maxP = 'calc(100% - ' + p + 'px)';
				let maxV = 'calc(100vh - ' + v + 'px)';

				view.$content
					.css({
						'min-height': maxP,
						'height': maxP,
						'max-height': maxP
					})
					.css({
						'min-height': maxV,
						'height': maxV,
						'max-height': maxV
					});

				//view.$fab.css('bottom', 0);
			} else if (advancedViews[viewId]) {
				let view = advancedViews[viewId];
				let footbar = view.footbar;

				if (typeof footbar === 'undefined') {
					return;
				}

				//view.parent.$fab.css('bottom', 0);
				footbar.$content.css('display', 'none');
			}
		};

		self.showFootbar = function (viewId) {
			if (currentViews[viewId]) {
				let view = currentViews[viewId];
				let footbar = view.footbar;

				if (typeof footbar === 'undefined') {
					return;
				}

				let mod = 0;
				if (Common.isMobile()) mod = -46;

				let p = 142 - mod;
				let v = 232 - mod;

				footbar.$content.css('display', 'block');
				let maxP = 'calc(100% - ' + p + 'px)';
				let maxV = 'calc(100vh - ' + v + 'px)';

				view.$content
					.css({
						'min-height': maxP,
						'height': maxP,
						'max-height': maxP
					})
					.css({
						'min-height': maxV,
						'height': maxV,
						'max-height': maxV
					});

				//view.$fab.css('bottom', 58);
			} else if (advancedViews[viewId]) {
				let view = advancedViews[viewId];
				let footbar = view.footbar;

				if (typeof footbar === 'undefined') {
					return;
				}

				//view.parent.$fab.css('bottom', 58);
				footbar.$content.css('display', 'block');
			}
		};

		self.removeFootbar = function (viewId) {
			if (currentViews[viewId]) {
				let footbar = currentViews[viewId].footbar;

				if (typeof footbar === 'undefined') {
					return;
				}

				self.hideFootbar(viewId);
				footbar.$content.remove();
				footbar.scope.$destroy();

				delete currentViews[viewId].footbar;
			} else if (advancedViews[viewId]) {
				let view = advancedViews[viewId];
				let footbar = view.footbar;

				if (typeof footbar === 'undefined') {
					return;
				}

				self.hideFootbar(viewId);

				view.parent.$content.off('scroll.' + viewId);
				view.$content.children('content-footbar-pusher').remove();
				footbar.$content.remove();
				footbar.scope.$destroy();
				delete view.footbar;
			}
		};

		self.footbar = function (viewId, params) {
			let $footbar = $('<content-footbar><divider></divider></content-footbar>');
			let $template = $templateCache.get(params.template);

			if (currentViews[viewId]) {
				let view = currentViews[viewId];

				if (view.footbar) {
					self.removeFootbar(viewId);
				}

				let scope = typeof params.scope === 'undefined' ? view.scope : params.scope;
				view.footbar = {
					scope: scope.$new(),
					$content: $footbar
				};

				setTimeout(function () {
					$footbar.appendTo(view.$view);
					self.showFootbar(viewId);

					$compile($('<div>').html($template))(view.footbar.scope).appendTo($footbar);
				});
			} else if (advancedViews[viewId]) {
				let view = advancedViews[viewId];
				let scope = typeof params.scope === 'undefined' ? view.scope : params.scope;

				view.footbar = {
					scope: scope.$new(),
					$content: $footbar
				};

				$footbar.addClass('floating-footbar');

				setTimeout(function () {
					if (view.footbar) {
						setFootbarPosition();
						$footbar.appendTo(view.$target);
						$compile($('<div>').html($template))(view.footbar.scope).appendTo($footbar);
						$('<content-footbar-pusher>').appendTo(view.$content);

						setTimeout(function () {
							self.showFootbar(viewId);
						}, 75);
					}
				});

				let $vContent = view.$content;
				let $pContent = view.parent.$content;

				let pHeight = $pContent.height();
				let cHeight = $vContent.height();

				let setFootbarPosition = function () {
					let translate = pHeight + $pContent.offset().top - cHeight - $vContent.offset().top + 24;
					let max = 128 - cHeight;

					translate = translate < max ? max : translate;
					translate = translate > 0 ? 0 : translate;
					$footbar.css('transform', 'translateY(' + translate + 'px)');
				};

				view.parent.$content.on('scroll.' + viewId, setFootbarPosition);
				Common.subscribeContentResize(view.footbar.scope, function () {
					pHeight = $pContent.height();
					cHeight = $vContent.height();

					setFootbarPosition();
				});
			} else {
				return;
			}

			return $footbar;
		};

		self.scrollTop = function (viewId) {
			let view = currentViews[viewId];

			if (typeof view === 'undefined') {
				return;
			}

			view.$content.animate({scrollTop: 0}, 300, 'easeOutCubic');
		};

		self.viewAdvanced = function (name, relay, options) {
			if (name == 'nothing') name = 'process.tab';
			if (typeof relay !== 'object') relay = {};

			relay.params = angular.copy(relay.params);
			options = angular.copy(options);

			if (viewLoadInProgress || viewQueue.length) {
				viewQueue.push({
					type: 'advanced',
					name: name,
					relay: relay,
					options: options
				});
			} else {
				viewLoadInProgress = true;
				showViewAdvanced(name, relay, options);
			}
		};

		self.closeViews = function () {
			angular.forEach(currentViews, function (view, viewId) {
				self.close(viewId, true);
			});
		};

		self.success = function (resolve, message) {
			let d = $q.defer();

			if (typeof message === 'undefined') {
				d.resolve(resolve);
			} else {
				MessageService.toast(message);
				d.resolve(resolve);
			}

			return d.promise;
		};

		self.fail = function (message) {
			let d = $q.defer();
			d.reject({customError: Translator.translate(message)});
			return d.promise;
		};

		function forgetViews(name) {
			let views = LocalStorageService.get('restoreView');
			if (views) {
				let i = views.length;
				while (i--) {
					if (views[i].name === name) {
						views.splice(i, 1);
						break;
					}
				}
			}

			LocalStorageService.store('restoreView', views);
		}

		function rememberViews() {
			let parameters = [];
			for (let i = 0, l = order.length; i < l; i++) {
				let v = currentViews[order[i]];
				if (v.remember) {
					parameters.push({name: v.name, params: v.relay});
				}
			}

			let parents = [];
			for (let i = 0, l = viewLevels.length; i < l; i++) {
				let parent = viewLevels[i];
				parents.push({
					name: parent.name,
					params: parent.relay
				});
			}

			LocalStorageService.store('viewStatus', {parents: parents});
			LocalStorageService.store('restoreView', parameters);
		}

		self.addLayoutRoute = function (view) {
			let v = view.params;
			v.name = view.name;

			if (v.name == "frontpage") v.title = Translator.translate("FRONTPAGE");
			v.signature = createSignature(v);

			let exists = _.findIndex(_layoutRoutes, function (o) {
				return v.signature === o.signature;
			});
			if (exists == -1) _layoutRoutes.unshift(v);

			Common.notifyViewLoaded();
		}

		function createSignature(view) {
			return view.name +
				"_" + view.itemId +
				"_" + view.process +
				"_" + view.portfolioId +
				"_" + view.title +
				"_" + view.activeTitle +
				"_" + view.phaseName;
		}

		let _layoutRoutes = [];
		Configuration.getSavedPreferences('routeFavourites', 1).then(function (p) {
			if (p.length > 0) {
				//Get items into cache
				_.each(p[0].Value, function (route) {
					if (route.name == 'process.tab' || (route.process && route.itemId > 0)) ProcessService.getItem(route.process, route.itemId);
				});
				_layoutRoutes = p[0].Value;
			}
		});
		self.getLayoutRoutes = function () {
			return _.orderBy(_layoutRoutes, ['fav'], ['asc']);
		}

		self.deleteLayoutRoute = function (itemId) {
			let i = _layoutRoutes.length
			while (i--) {
				if (_layoutRoutes[i].itemId && _layoutRoutes[i].itemId == itemId) {
					_layoutRoutes.splice(i, 1);
				}
			}
		}

		self.refreshOthers = function (excludeViewId) {
			angular.forEach(currentViews, function (view, viewId) {
				if (viewId != excludeViewId) self.refresh(viewId, undefined, 'first');
			});
		}

		$rootScope.$on('RefreshViews', function (event, opt) {
			angular.forEach(self.getViews().views, function (view, viewId) {
				self.refresh(viewId);
			});
		});

		self.refresh = function (viewId, params, position = undefined) {
			params = angular.copy(params);
			let view = currentViews[viewId];

			if (view) {
				let relay = {
					params: angular.extend(view.relay, params),
					locals: view.locals
				};
				let n = view.name;

				self.close(viewId, true).then(function () {
					let sTop = (view && view.$content) ? view.$content.scrollTop() : 0;
					setTimeout(function () {
						self.view(n, relay,
							{
								split: true,
								size: view.options.size,
								scrollTop: sTop,
								position: position
							},
							{
								history: false
							});
					}, 300);
				});
			} else if (advancedViews[viewId]) {
				let view = advancedViews[viewId];

				let relay = {
					params: angular.extend(view.relay, params),
					locals: view.locals
				};

				let n = view.name;
				let parent = view.parent;

				self.close(viewId, true).then(function () {
					self.viewAdvanced(n, relay,
						{
							targetId: viewId,
							ParentViewId: parent.params.ViewId,
							scrollTop: view.$content.scrollTop()
						});
				});
			}
		};

		self.getView = function (viewId) {
			if (typeof currentViews[viewId] === 'undefined') {
				return advancedViews[viewId];
			}

			return currentViews[viewId];
		};

		self.getViews = function () {
			return {
				views: currentViews,
				advancedViews: advancedViews
			};
		};

		function closeView(viewId, inside: boolean = false, animation: boolean = false) {
			let d = $q.defer();
			if (currentViews[viewId]) {
				let view = currentViews[viewId];

				let removeElement = function () {
					removePrevToolbarHighlight(viewId);
					view.scope.$destroy();
					view.$view.remove();

					if (view.sensor) {
						view.sensor.detach();
					}

					forgetViews(view.name);
					delete currentViews[viewId];

					for (let i = 0, l = order.length; i < l; i++) {
						if (order[i] === viewId) {
							if (i === 0 && l > 1) {
								currentViews[order[1]].$header.find('.swap-position').removeClass('visible');
							}

							order.splice(i, 1);
							break;
						}
					}

					if (Object.keys(currentViews).length === 1 && !inside) {
						currentViews[order[0]].scope.ShowClose = false;
					}

					d.resolve();
				};

				if (animation) {
					view.$view.addClass('transition-out');
					setTimeout(function () {
						removeElement();
					}, 300);
				} else {
					removeElement();
				}
			} else if (advancedViews[viewId]) {
				let view = advancedViews[viewId];
				view.scope.$destroy();

				if (view.sensor) {
					view.sensor.detach();
				}

				view.$content.remove();

				self.removeFootbar(viewId);

				Common.notifyContentResize();
				delete advancedViews[viewId];

				d.resolve();
			}

			return d.promise;
		}

		self.close = function (viewId, reload) {
			let name = currentViews[viewId] ? currentViews[viewId].name : undefined;
			let orders = order.length - 1;
			let emit = !reload && name;

			self.lock(viewId);

			return SaveService.bypassTick(true).then(function () {
				closeView(viewId, false, true).then(function () {
					self.unlock(viewId);
					if (emit) {
						$rootScope.$emit('viewChange', {
							view: name,
							viewId: viewId,
							action: 'CLOSE',
							views: orders
						});
					}
				});
			}, function () {
				let p = {
					title: 'VIEW_SAVE_FAIL_TITLE',
					message: 'VIEW_SAVE_FAIL',
					buttons: [
						{
							text: 'VIEW_SAVE_FAIL_BACK'
						},
						{
							type: 'primary',
							text: 'VIEW_SAVE_FAIL_CONTINUE',
							onClick: function () {
								closeView(viewId, false, true).then(function () {
									if (emit) {
										$rootScope.$emit('viewChange', {
											view: name,
											viewId: viewId,
											action: 'CLOSE',
											views: orders
										});
									}
								});
							}
						}
					]
				};

				MessageService.dialog(p);
			});

		};

		self.frontpage = function () {
			self.view('frontpage', {});
		};

		self.initialize = function () {
			let view = Views.getView('main.layout');

			resolveView('main.layout', view).then(function (locals) {
				parameters.locals = locals;
				parameters.controller = view.controller;
				parameters.template = view.template;

				let $launchScreen = $('launch-screen').addClass('transition-out');
				let scope = $rootScope.$new(true);
				$compile($('<div view>'))(scope).appendTo($(document.body));

				setTimeout(function () {
					$launchScreen.remove();
				}, 750);

				let views = LocalStorageService.get('restoreView');


				if (views && views.length) {
					angular.forEach(views, function (view) {
						if (view.hasOwnProperty('SplitOptions')) {
							if (view.SplitOptions.AlsoOpenCurrent) {
								view.params['additionalFeaturetteParams'] = {
									subState: view.SplitOptions.SubDefaultState,
									size: view.SplitOptions.OpenWidth,
									itemId: view.SplitOptions.ParentItemId,
									process: view.SplitOptions.OpenRowProcess
								}
							}
							view.name = view.SplitOptions.SubDefaultState
						}
						self.view(view.name, {params: view.params}, {split: true}, {initialisation: true});
					});
				} else {
					self.frontpage();
				}
			});
		};

		self.getViewIdFromElement = function (ele) {
			ele = $(ele);
			let result = "";
			ele.closest('div.transition-in').each(function (i, e) {
				let id = $(e).attr('id');
				if (id && id.indexOf("view-") > -1) {
					result = id;
					return false;
				}
			});
			return result
		}

		self.reroute = function (viewId, name) {
			if (currentViews[viewId]) {
				let v = currentViews[viewId];

				viewQueue.unshift({
					type: 'normal',
					name: name,
					relay: {params: v.params, locals: v.locals},
					options: v.options,
					reroute: true
				});
			} else if (advancedViews[viewId]) {
				let v = advancedViews[viewId];

				viewQueue.unshift({
					type: 'advanced',
					name: name,
					relay: {params: v.relay, locals: v.locals},
					options: v.options
				});
			} else {
				return;
			}

			closeView(viewId, true, false);
			viewLoaded();
		};

		$rootScope.$on('$locationChangeStart', function (event, newUrl) {
			if (!manualTrigger) {
				let o = BodyStackerService.get();
				if (o) {
					o.remove();
					event.preventDefault();
				} else {
					let newUuid = newUrl.split('/#')[1];
					if (newUuid) {
						ViewHistoryService.get(newUuid).then(function (history) {
							let view = history.view;
							let params = history.parameters;
							ViewHistoryService.setUuid(newUuid);
							self.view(view, {params: params}, {split: false}, {history: false});
						});
					}
				}
			}

			manualTrigger = false;
		});
	}
})();