(function () {
	'use strict';

	angular
		.module('core.init')
		.config(PageConfiguratorConfig)
		.controller('PageConfiguratorController', PageConfiguratorController);

	PageConfiguratorConfig.$inject = ['ViewsProvider'];
	function PageConfiguratorConfig(ViewsProvider) {
		ViewsProvider.addDialog('pageConfigurator', {
			template: 'core/common/view/pageConfigurator.html',
			controller: 'PageConfiguratorController',
			resolve: {
				Setup: function (StateParameters, Common, View) {
					if (typeof View.onSetup === 'undefined') {
						return;
					}

					return Common.resolve(View.onSetup, { StateParameters: StateParameters });
				}
			}
		});
	}

	PageConfiguratorController.$inject = [
		'$scope',
		'StateParameters',
		'Setup',
		'MessageService',
		'SaveParameters',
		'Common',
		'ViewConfigurator',
		'View'
	];

	function PageConfiguratorController(
		$scope,
		StateParameters,
		Setup,
		MessageService,
		SaveParameters,
		Common,
		ViewConfigurator,
		View
	) {
		$scope.params = {};
		$scope.tabs = {};

		if (typeof View.defaults === 'undefined') {
			View.defaults = {};
		}

		angular.forEach(View.tabs, function (options, tab) {
			$scope.tabs[tab] = {};

			angular.forEach(options, function (value, key) {
				var v = value(Setup);
				var d = View.defaults[key];

				if (v.type === 'select' || v.type === 'boolean') {
					$scope.params[key] = d;
					v.singleValue = true;
				} else if (v.type === 'multiselect') {
					$scope.params[key] = Array.isArray(d) ? d : [];
				} else {
					$scope.params[key] = d;
				}

				if (typeof v.visible === 'undefined') {
					v.visible = true;
				}

				$scope.tabs[tab][key] = v;
			});
		});

		angular.forEach(StateParameters, function (conf, key) {
			$scope.params[key] = conf;
		});

		$scope.onChange = function (name) {
			if (typeof View.onChange === 'function') {
				View.onChange(name, $scope.params, Setup, $scope.tabs);
			}
		};

		angular.forEach($scope.tabs, function (tab) {
			angular.forEach(tab, function (option, key) {
				if ($scope.params[key]) {
					$scope.onChange(key);
				}
			})
		});


		$scope.close = function () {
			MessageService.closeDialog();
		};

		$scope.finish = function () {

			delete $scope.params.view;

			if (typeof View.onFinish === 'function') {
				View.onFinish($scope.params, Setup);
			}

			if (Common.isTrue(View.global)) {
				var p = {
					null: {}
				};
				p.null[StateParameters.view] = {
					value: $scope.params
				};

				ViewConfigurator.setConfiguration(p).then(function () {
					MessageService.closeDialog();
				});

				//$scope.params = {};
				SaveParameters($scope.params);
			} else {
				SaveParameters($scope.params);
				MessageService.closeDialog();
			}
		};
	}
})();