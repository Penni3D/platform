﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.service('ViewHistoryService', ViewHistoryService);

	ViewHistoryService.$inject = ['$q'];
	function ViewHistoryService($q) {
		let self = this;
		let curUuid = '';
		let backToTheFuture = false;
		let history = [];

		self.setUuid = function (uuid) {
			curUuid = uuid;
		};

		self.add = function (view) {
			if (history.length > 49) {
				history.shift();
			}

			if (backToTheFuture) {
				let i = history.length;
				while (i--) {
					if (history[i].uuid == curUuid) {
						history.length = i + 1;
						break;
					}
				}
			}

			backToTheFuture = false;
			let uuid = self.getUuid().substr(0, 8);
			curUuid = uuid;

			history.push({
				uuid: uuid,
				view: view.name,
				parameters: angular.extend({}, view.params)
			});
			return uuid;
		};

		self.get = function (uuid) {
			let d = $q.defer();
			backToTheFuture = true;

			if (curUuid == uuid) {
				d.reject();
			} else {
				let i = history.length;
				while (i--) {
					if (history[i].uuid == uuid) {
						d.resolve(history[i]);
						break;
					}
				}
			}

			return d.promise;
		};

		self.getUuid = function () {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
				let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		};
	}
})();