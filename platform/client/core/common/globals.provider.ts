(function () {
	'use strict';

	angular
		.module('core.init')
		.provider('Globals', Globals);

	Globals.$inject = [];
	function Globals() {
		var globals = {};

		this.addGlobal = function (global, rights) {
			globals[global] = {
				view: global,
				states: angular.copy(rights)
			};
		};

		this.$get = function () {
			return {
				getGlobals: function () {
					return angular.copy(globals);
				},
				getGlobal: function (global) {
					return angular.copy(globals[global]);
				}
			};
		};
	}

})();