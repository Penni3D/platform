(function () {
	'use strict';

	angular
		.module('core')
		.service('Sanitizer', Sanitizer);

	Sanitizer.$inject = [];
	function Sanitizer() {
		var map = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;'
		};

		function sanitizer(value) {
			if (typeof value === 'string') {
				return replace(value);
			} else if (typeof value === 'number' || typeof value === 'boolean') {
				return value;
			} else if (Array.isArray(value)) {
				var values = '';
				for (var i = 0, l = value.length; i < l; i++) {
					values += sanitizer(value[i]) + ' ';
				}

				values = values.replace(/\s{2,}/g, ' ').slice(0, -1);
				return replace(values);
			}

			return '';
		}

		function replace(string) {
			return string.replace(/[&<>"]/ig, function (s) {
				return map[s];
			});
		}

		return sanitizer;
	}

})();