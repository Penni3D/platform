(function () {
	'use strict';

	angular
		.module('core.init')
		.provider('Backgrounds', Backgrounds)
		.provider('FrontpageBackgrounds', FrontpageBackgrounds)
		.provider('Colors', Colors);

	FrontpageBackgrounds.$inject = [];

	function FrontpageBackgrounds() {
		var fb = {
			'snow': {name: 'Snow', background: 'bg1.jpg'},
			'sand': {name: 'Sand', background: 'bg2.jpg'},
			'mountains': {name: 'Mountains', background: 'bg3.jpg'},
			'water': {name: 'Water', background: 'bg4.jpg'},
			'forest': {name: 'Forest', background: 'bg5.jpg'},
			'gradient': {name: 'Gradient', background: 'bg6.png'},
		};
		this.getBackgrounds = function () {
			return angular.copy(fb);
		};
		this.$get = function () {
			return {
				getBackgrounds: function () {
					return angular.copy(fb);
				},
				getBackground: function (code) {
					if (typeof fb[code] === 'undefined') {
						var def = {
							name: '',
							background: ''
						};

						angular.forEach(fb, function (background) {
							if (background.name === code) {
								def = angular.copy(background);
							}
						});

						return def;
					}

					return angular.copy(fb[code]);
				}
			}
		}
	}

	Backgrounds.$inject = [];

	function Backgrounds() {
		var templates = [
			{name: 'keto', count: 1},
			{name: 'keto-w', count: 6},
			{name: 'keto-y', count: 3}
		];

		var b = {}
		// 	'banner-1': {name: 'Banner-1', background: 'banner-1'},
		// 	'banner-2': {name: 'Banner-2', background: 'banner-2'}
		// };

		for (var i = 0, l = templates.length; i < l; i++) {
			var t = templates[i];

			for (var j = 1; j <= t.count; j++) {
				var s = t.name + '-' + j;

				b[s] = {
					name: s[0].toUpperCase() + s.slice(1),
					background: 'background-' + s
				};
			}
		}

		this.add = function (name, value) {
			b[name] = {
				name: name[0].toUpperCase() + name.slice(1),
				background: value
			};
		};

		this.getBackgrounds = function () {
			return angular.copy(b);
		};

		this.$get = function () {
			return {
				getBackgrounds: function () {
					return angular.copy(b);
				},
				getRandomBackground: function() {
					var backgrounds = [];
					angular.forEach(b, function (value) {
						backgrounds.push(value);
					});
					return angular.copy(backgrounds[Math.floor(Math.random() * backgrounds.length)]);
				},
				getBackgroundsArray: function () {
					var backgrounds = [];
					angular.forEach(b, function (value) {
						backgrounds.push(value);
					});

					return angular.copy(backgrounds);
				},
				getBackground: function (code) {
					if (typeof b[code] === 'undefined') {
						var def = {
							name: '',
							background: ''
						};

						angular.forEach(b, function (background) {
							if (background.name === code) {
								def = angular.copy(background);
							}
						});

						return def;
					}

					return angular.copy(b[code]);
				}
			};
		};
	}

	Colors.$inject = [];

	function Colors() {
		let c = {
			'accent-2': {name: 'accent-2', r: 172, g: 112, b: 172},
			accent: {name: 'accent', r: 255, g: 233, b: 124},
			alert: {name: 'alert', r: 244, g: 138, b: 96},
			warn: {name: 'warn', r: 243, g: 189, b: 64},
			red: {name: 'red', r: 244, g: 67, b: 53},
			pink: {name: 'pink', r: 233, g: 30, b: 99},
			fuchsia: {name: 'fuchsia', r: 255, g: 119, b: 255},
			purple: {name: 'purple', r: 156, g: 39, b: 176},
			deepPurple: {name: 'deep purple', r: 103, g: 58, b: 183},
			indigo: {name: 'indigo', r: 63, g: 81, b: 181},
			blue: {name: 'blue', r: 33, g: 150, b: 243},
			lightBlue: {name: 'light blue', r: 3, g: 169, b: 244},
			cyan: {name: 'cyan', r: 0, g: 188, b: 212},
			teal: {name: 'teal', r: 77, g: 182, b: 172},
			green: {name: 'green', r: 76, g: 175, b: 80},
			lightGreen: {name: 'light green', r: 139, g: 195, b: 74},
			lime: {name: 'lime', r: 205, g: 220, b: 57},
			yellow: {name: 'yellow', r: 255, g: 235, b: 59},
			amber: {name: 'amber', r: 255, g: 193, b: 7},
			orange: {name: 'orange', r: 255, g: 152, b: 0},
			brown: {name: 'brown', r: 121, g: 85, b: 72},
			lightGrey: {name: 'light grey', r: 238, g: 238, b: 238},
			grey: {name: 'grey', r: 189, g: 189, b: 189},
			darkGrey: {name: 'dark grey', r: 158, g: 158, b: 158}
		};

		angular.forEach(c, function (item, key) {
			item.rgb = 'rgb(' + item.r + ',' + item.g + ',' + item.b + ')';
			item.orgb = 'rgba(' + item.r + ',' + item.g + ',' + item.b + ',0.54)';
			item.hex = '#' + componentToHex(item.r) + componentToHex(item.g) + componentToHex(item.b);
			item.ohex = '#' + componentToHex(item.r) + componentToHex(item.g) + componentToHex(item.b) + '8A';
			item.background = 'background-' + key;
			item.obackground = item.background + '-opaque';
		});

		c.icon = {
			name: 'icon',
			r: 0,
			g: 0,
			b: 0,
			rgb: 'rgba(0,0,0,0.54)',
			orgb: 'rgba(0,0,0,0.26)',
			hex: '#0000008A',
			ohex: '#00000042'
		};

		c.text = {
			name: 'text',
			r: 0,
			g: 0,
			b: 0,
			rgb: 'rgba(0,0,0,0.87)',
			orgb: 'rgba(0,0,0,0.54)',
			hex: '#000000DE',
			ohex: '#0000008A'
		};

		function componentToHex(c) {
			let hex = c.toString(16);
			return hex.length === 1 ? '0' + hex : hex;
		}

		function getColor(code) {
			if (typeof c[code] === 'undefined') {
				let def = {
					name: '',
					background: '',
					rgb: '',
					orgb: '',
					hex: '',
					ohex: ''
				};

				angular.forEach(c, function (color) {
					if (color.name === code) {
						def = angular.copy(color);
					}
				});

				return def;
			}

			return angular.copy(c[code]);
		}

		this.getColors = function () {
			return angular.copy(c);
		};

		this.$get = function () {
			return {
				getColors: function () {
					return angular.copy(c);
				},
				getColor: getColor,
				getColorsArray: function () {
					let colours = [];

					angular.forEach(c, function (value) {
						colours.push(value);
					});

					return angular.copy(colours);
				}
			};
		};
	}
})();