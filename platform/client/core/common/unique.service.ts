﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('UniqueService', UniqueService);

	UniqueService.$inject = [];
	function UniqueService() {
		var self = this;
		var unique = {};

		self.get = function (tag, append) {
			if (typeof unique[tag] === 'undefined') {
				unique[tag] = 0;
			}

			var u = (unique[tag]++).toString();
			if (append === true) {
				u = tag + '-' + u;
			}

			return u;
		};
	}

})();
