﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('Common', CommonService)
		.filter('isTrue', IsTrue)
		.filter('isFalse', IsFalse)
		.filter('RemoveBrackets', RemoveBrackets)
		.directive('bindHtmlCompile', ['$compile', function ($compile) {
			return {
				restrict: 'A',
				link: function (scope, element, attrs) {
					scope.$watch(function () {
						return scope.$eval(attrs.bindHtmlCompile);
					}, function (value) {
						// Incase value is a TrustedValueHolderType, sometimes it
						// needs to be explicitly called into a string in order to
						// get the HTML string.
						element.html(value && value.toString());
						// If scope is provided use it, otherwise use parent scope
						var compileScope = scope;
						if (attrs.bindHtmlScope) {
							compileScope = scope.$eval(attrs.bindHtmlScope);
						}
						$compile(element.contents())(compileScope);
					});
				}
			};
		}]);

	IsTrue.$inject = ['Common'];

	function IsTrue(Common) {
		return function (value) {
			return Common.isTrue(value);
		};
	}

	function RemoveBrackets() {
		return function (value) {
			return value.replace("[", "").replace("]", " ");
		}
	}

	IsFalse.$inject = ['Common'];

	function IsFalse(Common) {
		return function (value) {
			return Common.isFalse(value);
		};
	}

	CommonService.$inject = ['$rootScope', 'Translator', '$injector', '$q', 'Configuration', 'MessageService', 'HTMLService'];

	function CommonService($rootScope, Translator, $injector, $q, Configuration, MessageService, HTMLService) {
		let self = this;

		self.resolve = function (resolves, parentLocals) {
			let locals = angular.extend({}, resolves);
			if (typeof parentLocals === 'undefined') parentLocals = {};

			angular.forEach(locals, function (local, key) {
				locals[key] = angular.isString(local) ?
					$injector.get(local) : $injector.invoke(local, null, parentLocals);
			});

			return $q.all(locals).then(function (locals) {
				return angular.extend(parentLocals, locals);
			});
		};

		self.jsonParse = function(json_){
			if(json_ == null) return json_;
			let previousChar = [",", ":", "{", "}", "\\"];
			let parsedReplace = angular.copy(json_);
			try {
				let modifier = 0;
				for(let i = 0; i < json_.length; i++){
					//CASE where "body text" has double quotation (") marks in it
					if(json_[i] == "\x22" && !previousChar.includes(json_[i - 1])&& !previousChar.includes(json_[i +1])){
						console.log("Found corrupted double quotation marks (\"\), trying to replace.")
						parsedReplace = parsedReplace.substring(0, i + modifier) + "\\\"" + parsedReplace.substring(i + modifier + 1);
						modifier++;
					}
					//more cases/logics can be added here
					if(false){
						modifier++;
					}
				}
				return JSON.parse(parsedReplace);
			} catch (error) {
				console.log("Could not parse " + json_)
				console.log("Errors:")
				console.error(error.name);
				console.error(error.message);
				return null;
			}
		}

		self.isTrue = function (param) {
			if (typeof param === 'boolean') {
				return param;
			}

			if (typeof param === 'number') {
				return param === 1;
			}

			if (typeof param === 'string') {
				switch (param.toLowerCase()) {
					case 'false':
					case '0':
					case '':
						return false;
					default:
						return true;
				}
			}

			return false;
		};

		self.clashedTranslations = function (translationObject, translationObjects) {
			let conflicts = {};
			angular.forEach(translationObjects, function (tObject, i) {
				if (typeof tObject === "string" && typeof translationObject === "string") {
					if (tObject === translationObject) conflicts[translationObject] = translationObject;
				} else {
					angular.forEach(tObject, function (translation, key) {
						if (typeof translationObject === 'undefined') {
							angular.forEach(translationObjects, function (translation_, j) {
								if (i !== j && typeof translation_[key] === 'string' && translation_[key].toLowerCase() === translation.toLowerCase()) {
									conflicts[key] = translation;
								}
							});
						} else if (typeof translationObject[key] === 'string' && translationObject[key].toLowerCase() === translation.toLowerCase()) {
							conflicts[key] = translation;
						}
					});
				}
			});

			return Object.keys(conflicts).length ? conflicts : false;
		};

		self.getRelevantDataType = function (itemColumn) {
			if (typeof itemColumn === 'object') {
				let dt = Number(itemColumn.DataType);

				return dt === 16 || dt === 20 ? Number(itemColumn.SubDataType) : dt;
			} else if (typeof itemColumn === 'number') {
				return itemColumn;
			}

			return 0;
		};

		self.getRelevantDataAdditional = function (itemColumn) {
			if (typeof itemColumn !== 'object') {
				return '';
			}

			return itemColumn.DataType == 16 || itemColumn.DataType == 20 ?
				itemColumn.SubDataAdditional : itemColumn.DataAdditional;
		};

		self.isFalse = function (param) {
			return !self.isTrue(param);
		};

		self.rounder = function (value, decimals) {
			if (typeof decimals === 'string') {
				decimals = Number(decimals);

				if (isNaN(decimals)) {
					decimals = 0;
				}
			} else if (typeof decimals !== 'number') {
				decimals = 0;
			}

			if (decimals < 0) {
				let multiplier = Math.pow(10, -decimals);
				return Number(Math.round(value / multiplier) * multiplier);
			} else {
				return Number(Math.round(Number(value + 'e' + decimals)) + 'e-' + decimals);
			}
		};

		self.getChartHandle = function (id) {
			let chart = $("#" + id);
			if (chart.data()) return chart.data().$isolateScopeNoTemplate.chart;
			return undefined;
		}
		self.getCanvasBreakPoint = function () {
			let w = $('#main-container-transition > div').first().width();
			let nw = 0;
			if (w > 800) nw = 120;
			if (w > 1000) nw = 100;
			if (w > 1400) nw = 80;
			return nw;
		}
		self.getCanvasResponsiveSize = function (initialValue: number, $scope: any, $element: any, isWide: boolean, isFront: boolean = false): number {
			let w = $('#main-container-transition > div').first().width();
			let nw = 180;
			if (isWide) {
				if (w > 800) nw = 120;
				if (w > 1000) nw = 100;
				if (w > 1400) nw = 80;
			} else {
				if (w > 800 || $element[0].offsetWidth < 400) nw = 280;
				if (w > 1000) nw = 180;
			}

			//This checks if we are on the frontpage and chart is not full width
			if (isFront && nw < 100 && w / $element[0].offsetWidth > 2.0) nw = 180;

			if (initialValue != nw) return nw;
			return initialValue;
		}
		self.getChartLegend = function (chart, type = "") {
			let ul = HTMLService.initialize('ul');
			let i = 0;
			if (type == "pie") {
				for (let ds of chart.data.labels) {
					let li = HTMLService.initialize('li')
					li.click("legendOnClick(" + i + ")");
					li.inner(HTMLService.initialize('div').style("background-color:" + chart.data.datasets[0].backgroundColor[i]));
					li.inner(HTMLService.initialize('span').content(ds));
					ul.inner(li);
					i++;
				}
			} else if (type == "bubble") {
				for (let ds of chart.data.labels) {
					let li = HTMLService.initialize('li')
					li.click("legendOnClick(" + i + ")");
					li.inner(HTMLService.initialize('div').style("background-color:" + ds.split("|")[1]));
					li.inner(HTMLService.initialize('span').content(ds.split("|")[0]));
					ul.inner(li);
					i++;
				}
			} else {
				for (let ds of chart.data.datasets) {
					let li = HTMLService.initialize('li')
					li.click("legendOnClick(" + i + ")");
					li.inner(HTMLService.initialize('div').style("background-color:" + ds.backgroundColor));
					li.inner(HTMLService.initialize('span').content(ds.label));
					ul.inner(li);
					i++;
				}
			}
			return ul.getHTML();
		}

		self.subscribeContentResize = function (scope, callback) {
			let handler = $rootScope.$on('notifyContentResize', callback);
			scope.$on('$destroy', handler);
		};

		self.subscribeEditMode = function (scope, callback) {
			let handler = $rootScope.$on('editModeChanged', callback);
			scope.$on('$destroy', handler);
		};

		self.subscribeViewLoaded = function (scope, callback) {
			let handler = $rootScope.$on('notifyViewLoaded', callback);
			scope.$on('$destroy', handler);
		};
		self.notifyViewLoaded = function () {
			$rootScope.$emit('notifyViewLoaded');
		};

		let resizeTimer;
		self.notifyContentResize = function () {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function () {
				$rootScope.$emit('notifyContentResize');
			}, 75);
		};

		let passwordToastTick;
		self.isValidPassword = function (password, allowEmpty = false, returnVariable = false) {
			if (typeof password === 'string') {
				if (!password.length && allowEmpty == false) return false;
				if (!password.length && allowEmpty == true) return true;

				let req = Configuration.getServerConfigurations('passwordpolicy');

				//Check Min Length Requirement
				if (isNaN(Number(req.minlength.value)) || password.length <= Number(req.minlength.value)) {
					if (returnVariable) {
						return 'PASSWORD_INVALID_MIN';
					} else {
						showPasswordToast('PASSWORD_INVALID_MIN');
						return false;
					}
				}

				//Check Case Requirement
				if (self.isTrue(req.requirecase.value)) {
					let i = password.length;
					while (i--) {
						let c = password[i];
						if (c !== c.toLowerCase()) break;
					}

					if (i === -1) {
						if (returnVariable) {
							return 'PASSWORD_INVALID_UPPER';
						} else {
							showPasswordToast('PASSWORD_INVALID_UPPER');
							return false;
						}
					}
				}

				//Check Number Requirement
				if (self.isTrue(req.requirenumber.value)) {
					let i = password.length;
					while (i--) {
						if (Number.isInteger(Number(password[i]))) break;
					}

					if (i === -1) {
						if (returnVariable) {
							return 'PASSWORD_INVALID_NUMBER';
						} else {
							showPasswordToast('PASSWORD_INVALID_NUMBER');
							return false;
						}
					}
				}

				//Check Complexity Requirement
				if (self.isTrue(req.requirespecial.value)) {
					let specialRegex = /[^A-Z a-z0-9]/
					if (!specialRegex.test(password)) {
						if (returnVariable) {
							return 'PASSWORD_INVALID_SPECIAL';
						} else {
							showPasswordToast('PASSWORD_INVALID_SPECIAL');
							return false;
						}
					}
				}

				//Check Max Length Requirement
				if (isNaN(Number(req.maxlength.value)) || password.length >= Number(req.maxlength.value)) {
					if (returnVariable) {
						return 'PASSWORD_INVALID_MAX';
					} else {
						showPasswordToast('PASSWORD_INVALID_MAX');
						return false;
					}
				}
			}
			if (returnVariable) return '';
			clearTimeout(passwordToastTick);
			return true;
		};

		function showPasswordToast(toast) {
			clearTimeout(passwordToastTick);
			passwordToastTick = setTimeout(function () {
				MessageService.toast(toast, 'warn');
			}, 600);
		}

		let _limiter = undefined;
		self.getDelimiter = function () {
			if (_limiter) return _limiter;

			let dateFormat = $rootScope.me.date_format;
			if (typeof dateFormat === 'string' && (dateFormat.indexOf('/') > 0 || dateFormat.indexOf('-') > 0)) {
				_limiter = ".";
				return _limiter;
			}

			_limiter = ",";
			return _limiter;
		};

		self.formatNumber = function (n, decimals) {
			let code = 'fi-FI';

			let dateFormat = $rootScope.clientInformation.date_format;

			if (typeof n === 'string') {
				n = n.replace(",", ".");
			}

			if (typeof dateFormat === 'string') {
				if (dateFormat.indexOf('/') > 0 || dateFormat.indexOf('-') > 0) {
					code = 'en-GB';
				}
			}

			let r = self.rounder(n, decimals);
			if (isNaN(r)) {
				return '';
			}

			return Number(r).toLocaleString(code);
		};

		self.getLocalMonthsOfYear = function () {
			let r = [];
			for (let i = 0; i < 12; i++) {
				r.push(Translator.translate('CAL_MONTHS_LONG_' + i));
			}

			return r;
		};

		self.getLocalShortMonthsOfYear = function () {
			let r = [];
			for (let i = 0; i < 12; i++) {
				r.push(Translator.translate('CAL_MONTHS_SHORT_' + i));
			}

			return r;
		};

		self.getLocalMinDaysOfWeek = function () {
			let r = [];
			for (let i = $rootScope.clientInformation.first_day_of_week; i < 7; i++) {
				r.push(Translator.translate('CAL_DAYS_MIN_' + i));
			}

			if ($rootScope.clientInformation.first_day_of_week == 1) {
				r.push(Translator.translate('CAL_DAYS_MIN_0'));
			}

			return r;
		};

		self.getLocalShortDaysOfWeek = function () {
			let r = [];
			for (let i = $rootScope.clientInformation.first_day_of_week; i < 7; i++) {
				r.push(Translator.translate('CAL_DAYS_SHORT_' + i));
			}

			if ($rootScope.clientInformation.first_day_of_week == 1) {
				r.push(Translator.translate('CAL_DAYS_SHORT_0'));
			}

			return r;
		};

		self.getDefaultWidth = function (dataType) {
			switch (dataType) {
				case 1:
				case 2:
					return 128;
				case 3:
				case 5:
				case 6:
				case 13:
					return 96;
				default:
					return 256;
			}
		};

		self._base64ToArrayBuffer = function (base64) {
			let bStr = window.atob(base64);
			let len = bStr.length;
			let bytes = new Uint8Array(len);
			for (let i = 0; i < len; i++) {
				bytes[i] = bStr.charCodeAt(i);
			}

			return bytes;
		};


		let charList = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		let middleCharacter = charList[charList.length / 2];
		let firstCharacter = charList[0];

		// return an order code that will fit between two given parameters
		self.getOrderNoBetween = function (previous, next) {
			if (typeof previous === 'undefined' && typeof next === 'undefined') {
				return middleCharacter;
			}

			let previousLength = 0;
			let previousIndex;
			let previousBase;
			let nextLength = 0;
			let nextIndex;
			let nextBase;

			if (typeof previous !== 'undefined') {
				previous = String(previous);
				previousLength = previous.length;
				previousIndex = charList.indexOf(previous[previousLength - 1]);
				previousBase = previous.substr(0, previousLength - 1);
			}

			if (typeof next !== 'undefined') {
				next = String(next);
				nextLength = next.length;
				nextIndex = charList.indexOf(next[nextLength - 1]);
				nextBase = next.substr(0, nextLength - 1);
			}

			if (previousBase == nextBase && previousIndex + 1 == nextIndex) {
				return previous + middleCharacter;
			}

			if (previousLength > nextLength) {
				if (previousIndex < charList.length - 1) {
					return previousBase + charList[previousIndex + 1];
				} else {
					return previous + middleCharacter;
				}
			}

			if (nextIndex > 1) {
				return nextBase + charList[nextIndex - 1];
			} else {
				return nextBase + firstCharacter + middleCharacter;
			}
		};

		self.fixOrders = function (len) {
			let temp = [];
			let l = charList.length;
			let step = self.rounder(l / len);

			if (step < 1) {
				step = 1;
			}

			for (let i = 1; i < l; i += step) {
				temp.push(charList[i]);
			}

			let runner = 0;
			while (temp.length < len) {
				let newOrder = self.getOrderNoBetween(temp[runner], temp[runner + 1]);
				temp.push(newOrder);

				if (runner + 2 < temp.length) {
					runner += 2;
				} else {
					runner = 0;
				}

				temp.sort();
			}

			return temp;
		};

		self.orderNoFromArray = function (array) {
			let beforeOrderNo;
			let afterOrderNo;

			angular.forEach(array, function (value) {
				if (beforeOrderNo < value.OrderNo || typeof beforeOrderNo === 'undefined')
					beforeOrderNo = value.OrderNo;
			});

			return self.getOrderNoBetween(beforeOrderNo, afterOrderNo);
		};

		self.stopEvents = function (e) {
			e.stopPropagation();
			e.preventDefault();
			e.cancelBubble = true;
			e.returnValue = false;

			return false;
		};

		self.removeSelection = function () {
			if (window.getSelection) {
				if (window.getSelection().empty) {  // Chrome
					window.getSelection().empty();
				} else if (window.getSelection().removeAllRanges) {  // Firefox
					window.getSelection().removeAllRanges();
				}
				// @ts-ignore
			} else if (document.selection) {  // Ignored because of sucks -- this is for IE
				// @ts-ignore
				document.selection.empty();
			}
		};

		self.getIndexOf = function (array, value, firstKey, secondKey?) {
			let i = array.length;
			while (i--) {
				if (typeof secondKey === 'undefined') {
					if (array[i][firstKey] == value) {
						return i;
					}
				} else {
					if (array[i][secondKey][firstKey] == value) {
						return i;
					}
				}
			}
		};

		self.convertArraytoString = function (array, key, secondKey?) {
			let resultString = '';
			if (Array.isArray(array)) {
				for (let i = 0, l = array.length; i < l; i++) {
					let a = array[i];

					if (typeof key === 'undefined') {
						if (resultString.length) {
							resultString += ',' + a;
						} else {
							resultString = '' + a;
						}
					} else {
						if (typeof secondKey === 'undefined') {
							if (resultString.length) {
								resultString += ',' + a[key];
							} else {
								resultString = '' + a[key];
							}
						} else {
							if (resultString.length) {
								resultString += ',' + a[key][secondKey];
							} else {
								resultString = '' + a[key][secondKey];
							}
						}
					}
				}

				return resultString;
			} else if (typeof array !== 'undefined' || array !== null) {
				return array;
			}

			return null;
		};

		let dataTypes = {
			0: 'DATA_NVARCHAR',
			1: 'DATA_INT',
			2: 'DATA_FLOAT',
			3: 'DATA_DATE',
			4: 'DATA_TEXT',
			5: 'DATA_PROCESS',
			6: 'DATA_LIST',
			7: 'DATA_PASSWORD',
			8: 'DATA_BUTTON',
			9: 'DATA_RATING',
			10: 'DATA_ATTACHMENT',
			11: 'DATA_JOIN',
			12: 'DATA_COMMENT',
			13: 'DATA_DATETIME',
			14: 'DATA_FORMULA',
			15: 'DATA_LINK',
			16: 'DATA_SUBQUERY',
			17: 'DATA_DESCRIPTION',
			18: 'DATA_PORTFOLIO',
			19: 'DATA_TAGS',
			20: 'DATA_EQUATION',
			21: 'DATA_RM',
			22: 'DATA_MATRIX',
			23: 'DATA_RICH',
			24: 'DATA_TRANSLATION',
			25: 'DATA_CHART',
			60: 'DATA_PROGRESS',
			65: 'DATA_MILESTONE'
		};

		self.getDataTypes = function () {
			let a = [];
			angular.forEach(dataTypes, function (translation, dt) {
				a.push({
					value: dt,
					name: Translator.translate(translation)
				});
			});

			return a;
		};

		self.getDataTypeTranslation = function (dt) {
			dt = Number(dt);
			return Translator.translate(dataTypes[dt]);
		};

		self.formatVariableName = function (translation) {
			return translation.replace(/ /g, "_").replace(/[ä]/g, 'a').replace(/[ö]/g, 'o').replace(/[å]/g, 'a').replace(/[^a-zA-Z0-9_]/g, '').toLowerCase();
		};

		self.stripHtmlTags = function (html, keepSome?) {
			let allowedTags = ['strong', 'li', 'ul', 'i', 'u', 'br'];
			if (keepSome) {
				_.each(allowedTags, function (tag) {
					let reStart = new RegExp(`<${tag}>`, 'g');
					let reEnd = new RegExp(`</${tag}>`, 'g');
					html = html.replace(reStart, '-' + tag + '-')
					html = html.replace(reEnd, '-/' + tag + '-')
				})
			}
			let div = document.createElement('div');
			div.innerHTML = html;
			let innerText = div.innerText;

			if (keepSome) {
				_.each(allowedTags, function (tag) {
					let reStart = new RegExp(`-${tag}-`, 'g');
					let reEnd = new RegExp(`-/${tag}-`, 'g');
					innerText = innerText.replace(reStart, '<' + tag + '>')
					innerText = innerText.replace(reEnd, '</' + tag + '>')
				})
			}
			return innerText;
		}

		self.isMobile = function() {
			return /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
		}
		
	}
})();
