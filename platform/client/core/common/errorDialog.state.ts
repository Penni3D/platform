﻿(function () {
	'use strict';

	angular
		.module('core.dialogs')
		.controller('ErrorDialogController', ErrorDialogController);

	ErrorDialogController.$inject = ['$scope', 'MessageService', 'Backgrounds', 'ErrorService'];

	function ErrorDialogController($scope, MessageService, Backgrounds, ErrorService) {
		$scope.currentNotification = 1;
		$scope.undreadNotifications = ErrorService.getErrors();
		$scope.notification = $scope.undreadNotifications[0];
		$scope.background = Backgrounds.getRandomBackground().background;


		$scope.close = function () {
			MessageService.closeDialog();
		};

		$scope.next = function () {
			if ($scope.currentNotification < $scope.undreadNotifications.length) {
				$scope.currentNotification += 1;
				$scope.notification = $scope.undreadNotifications[$scope.currentNotification - 1];
			}
		}

		$scope.prev = function () {
			if ($scope.currentNotification > 1) {
				$scope.currentNotification -= 1;
				$scope.notification = $scope.undreadNotifications[$scope.currentNotification - 1];
			}
		}

		$scope.read = function () {
			ErrorService.clearError($scope.currentNotification - 1);
			$scope.undreadNotifications = ErrorService.getErrors();
			$scope.notification = $scope.undreadNotifications[$scope.undreadNotifications.length - 1];
		};
	}
})();