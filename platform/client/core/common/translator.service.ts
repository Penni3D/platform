(function () {
	'use strict';

	angular
		.module('core')
		.service('Translator', Translator)
		.directive('translate', TranslateDirective)
		.filter('translate', TranslateFilter)
		.filter('translateObject', TranslateObject);


	Translator.$inject = ['Sanitizer', '$http', '$rootScope'];
	function Translator (Sanitizer, $http, $rootScope) {
		let self = this;

		let activeLocale = '';
		let languages = {};
		
		const logMissing = false;

		self.fetch = function (locale, isForced) {
			let url = 'v1/language/Translations/' + locale;
			if (isForced) url += '/true';
			return $http.get(url).then(function (response) {
				languages[locale] = response.data;
			});
		};

		self.use = function (locale) {
			activeLocale = locale;
			if (typeof languages[activeLocale] === 'undefined') languages[activeLocale] = {};
		};

		self.translate = function (key) {
			if (activeLocale == "") return key;
			if (typeof languages[activeLocale][key] === 'undefined') {
				if (key || key === 0 || key === false) {
                    if (logMissing) console.log('"' + key.toString().toUpperCase() + '"' + ': "Translate me",');
				    return key;   
                }
				return '';
			}

			return languages[activeLocale][key];
		};

		self.translation = function (object) {
			if (typeof object === 'string' || typeof object === 'number') return self.translate(object);
			if (object === null) return '';
			if (typeof object === 'object') {
				let localTranslation = object[$rootScope.clientInformation.locale_id];
				let instanceTranslation = object[$rootScope.clientInformation.default_locale_id];

				if (hasTranslation(localTranslation)) return localTranslation;
				if (hasTranslation(instanceTranslation)) return instanceTranslation;
				let anyTranslation = '';

				angular.forEach(object, function (t) {
					if (hasTranslation(t)) anyTranslation = t;
				});

				return anyTranslation;
			}
			return '';
		};

		function hasTranslation(value) {
			return value || value === 0 || value === false;
		}
	}

	TranslateDirective.$inject = ['Translator', 'Sanitizer'];
	function TranslateDirective(Translator, Sanitizer) {
		return {
			link: function (scope, element, attrs) {
				scope.$watch(attrs.translate, function () {
					element.text(Sanitizer(Translator.translate(attrs.translate)));
				});
			}
		};
	}

	TranslateFilter.$inject = ['Translator', '$interpolate'];
	function TranslateFilter(Translator, $interpolate) {
		return function (key, interpolation) {
			let translated = Translator.translate(key);

			if (typeof interpolation === 'object') {
				return $interpolate(translated)(interpolation);
			}

			return translated;
		};
	}

	TranslateObject.$inject = ['Translator', '$interpolate'];
	function TranslateObject(Translator, $interpolate) {
		return function (object, interpolation) {
			let translated = Translator.translation(object);

			if (typeof interpolation === 'object') {
				return $interpolate(translated)(interpolation);
			}

			return translated;
		};
	}
})();