﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('ApiService', ApiService);

	ApiService.$inject = ['$http', '$q'];
	function ApiService($http, $q) {
		let self = this;

		function Resource(url) {
			this.base = '';
			this.url = url.replace('/\/$/', '');
		}

		function addParametersToUrl(url, params) {
			if (Array.isArray(params)) {
				for (var i = 0, l = params.length; i < l; i++) {
					if (!(params[i] === null || params[i] === '' || typeof params[i] === 'undefined')) {
						url = url + '/' + params[i];
					}
				}
			} else {
				if (!(params === null || params === '' || typeof params === 'undefined')) {
					url = url + '/' + params;
				}
			}

			return url;
		}
		
		function getTimeStamp() {
			let t = moment().utc(false);
			return t.format('YYYY-MM-DD HH:mm:ss') + ':' + t.milliseconds();
		}
		
		Resource.prototype = {
			get: function (params?) {
				let d = $q.defer();
				let url = addParametersToUrl(this.base + this.url, params);
				
				let parameters = angular.copy(params);
				$http.defaults.headers.get["RequestInitiated"] = getTimeStamp();
				$http.get(url).then(function (response) {
					d.resolve(response.data, response);
				}, function (error) {
					d.reject(error, parameters);
				});

				return d.promise;
			},
			new: function (params, data) {
				let d = $q.defer();
				let url = addParametersToUrl(this.base + this.url, params);

				let payload = angular.copy(data);
				let parameters = angular.copy(params);
				$http.defaults.headers.post["RequestInitiated"] = getTimeStamp();
				$http.post(url, payload).then(function (response) {
					d.resolve(response.data, response);
				}, function (error) {
					d.reject(error, parameters, payload);
				});

				return d.promise;
			},
			save: function (params, data) {
				let d = $q.defer();
				let url = addParametersToUrl(this.base + this.url, params);
				
				let payload = angular.copy(data);
				let parameters = angular.copy(params);
				$http.defaults.headers.put["RequestInitiated"] = getTimeStamp();
				$http.put(url, data).then(function (response) {
					d.resolve(response.data, response);
				}, function (error) {
					d.reject(error, parameters, payload);
				});

				return d.promise;
			},
			delete: function (params) {
				let d = $q.defer();
				let url = addParametersToUrl(this.base + this.url, params);

				let parameters = angular.copy(params);
				$http.delete(url).then(function (response) {
					d.resolve(response.data, response);
				}, function (error) {
					d.reject(error, parameters);
				});

				return d.promise;
			}
		};

		self.api = function (url) {
			return new Resource(url);
		};

		self.v1api = function(url){
			url = 'v1/' + url
			return new Resource(url);
		};
	}
})();
