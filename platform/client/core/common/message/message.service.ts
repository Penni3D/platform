﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('MessageService', MessageService);

	MessageService.$inject = [
		'$compile',
		'$rootScope',
		'$q',
		'$controller',
		'$templateCache',
		'Translator',
		'BodyStackerService',
		'HTMLService',
		'Sanitizer',
		'Colors',
		'UniqueService'
	];

	function MessageService(
		$compile,
		$rootScope,
		$q,
		$controller,
		$templateCache,
		Translator,
		BodyStackerService,
		HTMLService,
		Sanitizer,
		Colors,
		UniqueService
	) {
		let self = this;

		let KEYS = {
			UP: 38,
			DOWN: 40,
			LEFT: 37,
			RIGHT: 39,
			ENTER: 13,
			ESC: 27
		};

		let veryLongDelay = 90000;
		let longDelay = 9000;
		let shortDelay = 5000;
		let $mainBody = $(document.body);
		let dialogScope;
		let $dialog = $('<tz-dialog tabindex="0">');
		let $mainContainerTransition;
		let stacks = [];

		let lockTools = false;
		let toolsScope = $rootScope.$new(true);
		let $toolsCatcher = $compile(
			HTMLService.initialize('tools-catcher').class('pointer').click('openTools()')
				.inner(HTMLService.initialize().icon('keyboard_arrow_up'))
				.getHTML())(toolsScope);
		let $tools;

		$rootScope.$on('viewChange', function (event, parameters) {
			if (parameters.action === 'PRE-NEW') {
				lockTools = parameters.view === 'frontpage';
				self.closeTools();
			} else if (
				(parameters.action === 'FAIL' && !parameters.split) ||
				(parameters.action === 'CLOSE' && !parameters.views)
			) {
				self.closeTools();
			}
		});

		let $backdrop = HTMLService.initialize('div')
			.class('backdrop')
			.click('onClick()')
			.attr('ng-swipe-right', 'rightSwipe()')
			.attr('ng-swipe-left', 'leftSwipe()')
			.getObject();

		let backdropScope = $rootScope.$new(true);
		$compile($backdrop)(backdropScope);

		let $toast = $('<toast class="align-row space-between">').appendTo($mainBody);
		let toastTickInitial;
		let toastTickFinal;
		let toastTickTimeout;
		let toastTickSlide;

		let $scrollCatcher = $('<scroll-catcher>').on('click', function (event, params) {
			if (params) {
				let i = stacks.length;
				while (i--) {
					let s = stacks[i];
					if (_.startsWith(s.id, params)) self.removeElement(s.$handler, s.id, s.scope);
				}
			} else {
				let s = stacks[stacks.length - 1];
				if (s) self.removeElement(s.$handler, s.id, s.scope);
			}
		});

		self.triggerCatcher = function (params) {
			$scrollCatcher.trigger('click', params);
		};

		self.backdrop = function (onClick, onSwipe) {
			backdropScope.onClick = function () {
				if (typeof onClick === 'function') onClick();
			};

			backdropScope.rightSwipe = function () {
				if (onSwipe && typeof onSwipe.right === 'function') onSwipe.right();
			};

			backdropScope.leftSwipe = function () {
				if (onSwipe && typeof onSwipe.left === 'function') onSwipe.left();
			};

			$mainBody.css('overflow-y', 'hidden');
			$backdrop.appendTo($mainBody);
			setTimeout(function () {
				$backdrop.addClass('transition-in');
			}, 75);
		};

		self.removeBackdrop = function () {
			$backdrop.detach();
			$backdrop.removeClass('transition-in');
			$mainBody.css('overflow-y', '');
		};

		let restoreFocusTo = undefined;
		
		self.element = function ($handler, params, $target, scope) {
			$handler.on('mousedown mouseup dblclick click', function (event) {
				event.stopPropagation();
			});

			$scrollCatcher.trigger('click', params.id);
			$scrollCatcher.appendTo($mainBody);

			$handler.appendTo($mainBody);
			restoreFocusTo = $target;
			
			setTimeout(function () {
				let removeElement = function () {
					self.removeElement($handler, params.id, scope);
				};

				$handler.addClass('transition-in');
				$handler.on('keydown', function (e) {
					if (e.which === KEYS.ESC) removeElement();
				});

				let offset = params.override == true ? {top: params.top, left: params.left} : $target.offset();
				let shiftTop = typeof params.shiftTop === 'undefined' ? 0 : params.shiftTop;
				let shiftLeft = typeof params.shiftLeft === 'undefined' ? 0 : params.shiftLeft;

				//LEFT COMPENSATE
				let lCompensate = window.innerWidth - $handler.width() - offset.left - 24 - shiftLeft;
				if (lCompensate < 0) {
					$handler.css('left', offset.left + lCompensate + shiftLeft);
				} else {
					let l = offset.left + shiftLeft;
					if (l < 24) {
						l = 24;
					}

					$handler.css('left', l);
				}

				//BOTTOM COMPENSATE
				if (!params.preventTopAlign) {
					let hHeight = $handler.outerHeight();
					let wHeight = window.innerHeight;
					let hCompensate = wHeight - hHeight - offset.top - 24 - shiftTop;
					let paddings = parseInt($handler.css('padding-top')) + parseInt($handler.css('padding-bottom'));

					if (hCompensate < 0) {
						let t = offset.top + hCompensate + shiftTop;
						if (t < 24) {
							t = 24;
						}

						let h = hHeight - paddings;
						if (wHeight < h + 64) {
							h = wHeight - 64;
						}

						$handler.css(
							{
								'top': t,
								'max-height': h,
								'overflow-y': 'auto'
							});
					} else {
						let t = offset.top + shiftTop;
						if (t < 24) {
							t = 24;
						}

						$handler.css(
							{
								'top': t,
								'max-height': hHeight + hCompensate - paddings,
								'overflow-y': 'auto'
							});
					}
				}

				if (params.keyboard) {
					self.navigateElement($handler, params.keyboard);
				}

				if (typeof params.focus === 'undefined') {
					$handler.focus();
				} else {
					if (params.focus.prevent !== true) {
						params.focus.target().focus();
					}
				}

				BodyStackerService.add({id: params.id, remove: removeElement});
				stacks.push({id: params.id, scope: scope, $handler: $handler});
			}, 75);
		};

		self.removeElement = function ($handler, id, scope) {
			_.each(scope.preselectedItems, function(item){
				scope.onSelectFinally(item);
			})
			$handler.removeClass('transition-in');
			$handler.off('mousedown mouseup dblclick click keydown');

			BodyStackerService.remove(id);
			let i = stacks.length;
			while (i--) {
				if (stacks[i].id == id) {
					stacks.splice(i, 1);
				}
			}

			setTimeout(function () {
				$handler.remove();
				let l = stacks.length;

				if (l) {
					stacks[l - 1].$handler.before($scrollCatcher);
				} else {
					$scrollCatcher.detach();
				}
				if (typeof scope !== 'undefined') scope.$destroy();
				if (restoreFocusTo) {
					if (restoreFocusTo.find('.restore-to:not(".ng-hide")').length > 0) {
						restoreFocusTo.find('.restore-to:not(".ng-hide")').first().focus();
					} else {
						if (restoreFocusTo.find("input").first()) restoreFocusTo.find("input").first().focus();	
					}
				}
				
				
			}, 225);
		};

		self.creationContainerFields = [];

		self.navigateElement = function ($handler, keyboard) {
			$handler.focus();
			let focus = typeof keyboard.index === 'undefined' ? -1 : keyboard.index;

			if (keyboard.autofocus) {
				$handler.find('.menu-item-' + focus).addClass('hovered');
			}

			$handler.on('keydown', function (event) {
				if (event.which === KEYS.DOWN) {
					if (focus === -1) {
						focus = 0;
						$handler.find('.menu-item-' + focus).addClass('hovered');
					} else {
						let $menuItem = $handler.find('.menu-item-' + focus);
						let $nextItem = $menuItem.next();

						if ($nextItem.length) {
							focus++;
							$menuItem.removeClass('hovered');
							$nextItem.addClass('hovered');
						}
					}
				} else {
					let $menuItem = $handler.find('.menu-item-' + focus);

					if (event.which === KEYS.UP) {
						let $prevItem = $menuItem.prev();
						if ($prevItem.length) {
							focus--;
							$menuItem.removeClass('hovered');
							$prevItem.addClass('hovered');
						}
					} else if (event.which === KEYS.ENTER) {
						setTimeout(function () {
							$menuItem.trigger('click');
						});
					}
				}
			});
		};

		self.menu = function (items, target, options) {
			if (typeof options === 'undefined') {
				options = {};
			}

			let params = {
				id: UniqueService.get('menu', true),
				override: true,
				keyboard: true,
				top: undefined,
				left: undefined,
				shiftLeft: undefined,
				shiftTop: undefined
			};

			if (options.override === true) {
				params.top = target.top;
				params.left = target.left;
			} else {
				let offset = $(target).offset();
				params.top = offset.top;
				params.left = offset.left;
			}

			let menuScope = $rootScope.$new(true);
			menuScope.items = angular.copy(items);

			let $selectMenu = $compile('<tz-menu tabindex="0">')(menuScope);

			if (window.innerWidth / 2 < params.left) {
				params.shiftLeft = -88;
			}

			self.element($selectMenu, params, undefined, menuScope);
		};

		self.card = function (params, element, big = false) {
			let $card = $('<card>');
			let cardScope = $rootScope.$new(true);
			let locals = params.locals;

			if (!locals) {
				locals = {};
			}

			locals.$scope = cardScope;

			let controller = $controller(params.controller, locals);
			let $target = $(element);
			let offset = $target.offset();

			$card.html($compile($templateCache.get(params.template))(cardScope));
			$card.data('$ngControllerController', controller);
			$card.children().data('$ngControllerController', controller);

			if(big)$card.addClass('big');

			let p = {
				id: UniqueService.get('card', true),
				override: true,
				top: offset.top,
				left: offset.left,
				shiftLeft: undefined,
				shiftTop: undefined
			};

			if (window.innerWidth / 2 < p.left) {
				p.shiftLeft = -304;
			}

			if (window.innerHeight / 2 < p.top) {
				p.shiftTop = -$card.outerHeight() + 40;
			}

			self.element($card, p, $target, cardScope);
		};

		self.closeCard = function () {
			self.triggerCatcher('card');
		};

		function showDialog($dialog) {
			setTimeout(function () {
				$dialog.addClass('transition-in');
				$dialog.focus();

				//Disable Tabindexes
				$dialog.tabindexstore = $("[tabindex=0]");
				$dialog.tabindexstore.each(function () {
					$(this).attr("tabindex", '-1');
				});
				$dialog.find("[tabindex]").each(function() {
					$(this).attr("tabindex", '0');
				});
				//Focus on the first element
				if(self.allowFieldFocus()) $dialog.find("[tabindex=0]").first().focus();

			}, 75);

			self.backdrop();
			$dialog.appendTo($mainBody);
		}

		self.allowFieldFocus = function(){
			if(self.creationContainerFields[0]) {
				//more datatypes may be implemented if needed
				return self.creationContainerFields[0].ItemColumn.DataType != 5
			}
			return true;
		}

		self.closeDialog = function () {
			let d = $q.defer();
			if (dialogScope) dialogScope.$destroy();

			//Re-enable Tabindexes
			if ($dialog.tabindexstore) {
				$dialog.tabindexstore.each(function () {
					$(this).attr("tabindex", '0');
				});
			}
			$dialog.removeClass('transition-in');

			setTimeout(function () {
				$dialog.remove();
				$dialog.empty();
				$dialog.off('keydown');
				$dialog.removeClass('dialog-large');

				self.removeBackdrop();
				d.resolve();
			}, 225);

			return d.promise;
		};

		self.dialogAdvanced = function (params, resolve?) {
			if (typeof params === 'undefined') {
				params = {};
			}

			let templateUrl = params.template;
			let controllerName = params.controller;
			let wider = params.wider;

			return self.closeDialog().then(function () {
				let scope = $rootScope.$new(true);
				dialogScope = scope;

				if (typeof resolve === 'undefined') {
					resolve = false;
				}

				if (wider) {
					$dialog.addClass('dialog-large');
				}

				if (!resolve) {
					let locals = params.locals;

					if (typeof locals === 'undefined') {
						locals = {};
					}

					locals.$scope = scope;

					let controller = $controller(controllerName, locals);

					$dialog.html($compile($templateCache.get(templateUrl))(scope));
					$dialog.data('$ngControllerController', controller);
					$dialog.children().data('$ngControllerController', controller);
				}

				$dialog.on('keydown', function (e) {
					if (e.which === KEYS.ESC) {
						self.closeDialog();
					}
				});

				showDialog($dialog);

				return {dialog: $dialog, scope: scope};
			});
		};

		self.dialog = function (params, model, onChange, modify) {
			let d = $q.defer();
			let background = params.background;
			let title = params.title;
			let message = params.message;
			let inputs = params.inputs;
			let buttons = params.buttons;

			self.closeDialog().then(function () {
				let $dialogContent = $('<dialog-content>').appendTo($dialog);
				let $dialogButtons = $('<dialog-actions>').appendTo($dialog);
				let scope = $rootScope.$new(true);
				scope.dialogEdit = true;
				dialogScope = scope;

				function removeDialog(resolve?) {
					self.closeDialog(scope).then(function () {
						d.resolve(resolve);
						$dialog.removeClass('dialog-small');
					});
				}

				if (background) {
					$dialogContent.append('<dialog-background class="' + Sanitizer(background) + '">');
				}

				if (typeof title === 'undefined') {
					$dialog.addClass('dialog-small');
				} else {
					$dialogContent.append(
						'<dialog-title><h2 class="ellipsis">' + Sanitizer(Translator.translate(title)) + '</h2></dialog-title>'
					)
				}

				let $container = $('<dialog-container>').appendTo($dialogContent);

				if (message) {
					$container.append('<p class="body-1">' + Sanitizer(Translator.translate(message)) + '</p>');
				}

				if (inputs) {
					let $inputs = HTMLService.initialize('input-smart')
						.repeat('input in ::dialogInputs')
						.attr('manual-save')
						.attr('tabindex', '{{::$index}}')
						.attr('modify', 'dialogEdit && modifies[input.modify]')
						.attr('type', '{{::input.type}}')
						.attr('process', '{{::input.process}}')
						.attr('label', '{{::input.label}}')
						.attr('model', 'dialogModel[input.model]')
						.attr('model-start', 'dialogModel[input.modelStart]')
						.attr('model-end', 'dialogModel[input.modelEnd]')
						.attr('on-change', 'onChange')
						.attr('on-change-parameters', 'input.model')
						.attr('options', '::input.options')
						.attr('option-name', '{{::input.optionName}}')
						.attr('option-value', '{{::input.optionValue}}')
						.attr('option-icon', '{{::input.optionIcon}}')
						.attr('portfolio-id', '{{::input.portfolioId}}')
						.attr('parent', '{{::input.parent}}')
						.attr('self', '{{::input.self}}')
						.attr('min', '{{::input.min}}')
						.attr('max', '{{::input.max}}')
						.attr('step', '{{::input.step}}')
						.attr('option-false', '{{::input.optionFalse}}')
						.attr('option-true', '{{::input.optionTrue}}')
						.attrNGConditional('::input.unselectable', 'unselectable', 'input.unselectable')
						.attr('ng-attr-single-value', '{{::input.singleValue || undefined}}')
						.getHTML();

					scope.onChange = onChange;
					scope.dialogModel = model;
					scope.dialogInputs = inputs;
					$compile($inputs)(scope).appendTo($container);
				}

				scope.dialogOnClick = function (button) {
					if (!scope.dialogEdit) {
						return;
					}

					if (typeof button.onClick === 'function') {
						let p = button.onClick();

						if (p) {
							scope.dialogEdit = false;

							p.then(function (resolve) {
								removeDialog(resolve);
							}, function () {
								scope.dialogEdit = true;
							});
						} else {
							removeDialog();
						}
					} else {
						removeDialog();
					}
				};

				scope.dialogButtons = buttons;

				let bHtml = HTMLService.initialize('span').class('flex')
					.button()
					.repeat('button in ::dialogButtons')
					.attr('type', '{{::button.type}}')
					.click('dialogOnClick(button)');

				if (typeof modify === 'undefined') {
					bHtml.attr('modify', 'dialogEdit');
				} else {
					scope.modifies = modify;
					bHtml.attr('modify', 'dialogEdit && modifies[button.modify]');
				}

				bHtml.bind('::button.text | translate');

				let $buttons = bHtml.getObject().appendTo($dialogButtons);

				$dialog.on('keydown', function (e) {
					if (e.which === KEYS.ESC) {
						removeDialog();
					}
				});

				$compile($buttons)(scope);
				showDialog($dialog);
			});

			return d.promise;
		};

		self.footer = function (buttons) {
			if (lockTools) return;
			if (!$tools) {
				$tools = $compile(
					HTMLService.initialize('tools')
						.inner(HTMLService.initialize('input-button')
							.repeat('button in buttons')
							.attr('tooltip', '{{::button.name}}')
							.attr("tabindex", "-1")
							.click('onClick(button)')
							.attr('type', 'icon')
							.attr('minimal')
							.attr('no-waves')
							.inner(HTMLService.initialize('i').class('material-icons').bind('::button.icon')))
						.getHTML())(toolsScope);
			}

			if (typeof $mainContainerTransition === 'undefined') {
				$mainContainerTransition = $('#main-container-transition');

				toolsScope.openTools = function () {
					self.backdrop();
					$tools.addClass('slide-up');
				};

				toolsScope.onClick = function (button) {
					self.hideTools();
					button.onClick();
				};
			}

			$mainContainerTransition.append($toolsCatcher).append($tools);
			toolsScope.buttons = angular.copy(buttons);
		};

		self.hideTools = function () {
			if ($tools) {
				$tools.removeClass('slide-up');
				self.removeBackdrop();
			}
		};

		self.closeTools = function () {
			if ($tools) {
				lockTools = false;
				$tools.detach();
				$toolsCatcher.detach();
				self.hideTools();
			}
		};

		function toastElement(params, delay) {
			$toast.removeClass('slide-down');
			clearTimeout(toastTickInitial);
			clearTimeout(toastTickFinal);
			clearTimeout(toastTickTimeout);
			clearTimeout(toastTickSlide);

			toastTickInitial = setTimeout(function () {
				$toast.css('display', '');
				let toast = HTMLService.initialize()
					.element('div')
					.class(Colors.getColor(params.color).background)
					.element('span')
					.class('body-2 ellipsis')
					.content(Sanitizer(Translator.translate(params.message)))
					.getHTML();

				$toast.html(toast);

				if (params.button) {
					let $toastButton = $('<a class="pointer no-select">' + Sanitizer(Translator.translate(params.button.text)) + '</a>')
						.appendTo($toast);

					$toastButton.on('click', function (e) {
						e.stopPropagation();
						clearTimeout(toastTickTimeout);
						clearTimeout(toastTickFinal);

						$toastButton.off('click.toastButton');
						$toast.removeClass('slide-down');

						params.button.onClick();
						$rootScope.$apply();

						setTimeout(function () {
							$toast.css('display', 'none');
						}, 150);
					});
				}

				toastTickSlide = setTimeout(function () {
					$toast.addClass('slide-down');
				}, 75);

				toastTickTimeout = setTimeout(function () {
					$toast.removeClass('slide-down');
					toastTickFinal = setTimeout(function () {
						$toast.css('display', 'none');
					}, 150);
				}, delay);
			}, 150);
		}

		self.saveSuccess = function () {
			let toast = {
				message: 'MSG_SUCCESS_SAVE'
			};

			self.toastShort(toast);
		};

		self.deleteSuccess = function () {
			let toast = {
				message: 'MSG_SUCCESS_DELETE'
			};

			self.toastShort(toast);
		};

		self.toast = function (message, type) {
			let toast = {
				message: message,
				color: undefined
			};

			if (type === 'alert') {
				toast.color = 'red';
			} else if (type === 'warn') {
				toast.color = 'amber';
			} else if (type === 'confirm') {
				toast.color = 'green';
			}

			self.toastShort(toast);
		};

		self.msgBox = function (message, title?) {
			let dialog = {
				title: title,
				message: message,
				buttons: [{text: 'OK', type: 'primary'}]
			};

			return self.dialog(dialog);
		};

		self.confirm = function (message, yesF, type?) {
			if (typeof type === 'undefined') {
				type = 'primary';
			}

			let dialog = {
				message: message,
				buttons: [
					{text: 'CANCEL'},
					{onClick: yesF, text: 'MSG_CONFIRM', type: type}
				]
			};

			return self.dialog(dialog);
		};

		self.yesNo = function (message, yesF, noF) {
			let dialog = {
				message: message,
				buttons: [{onClick: noF, text: 'MSG_NO'}, {onClick: yesF, text: 'MSG_YES', type: 'primary'}]
			};

			return self.dialog(dialog);
		};

		self.yesNoCancel = function (message, yesF, noF) {
			let dialog = {
				message: message,
				buttons: [
					{text: 'MSG_CANCEL'},
					{onClick: noF, text: 'MSG_NO'},
					{onClick: yesF, text: 'MSG_YES', type: 'primary'}
				]
			};

			return self.dialog(dialog);
		};

		self.toastShort = function (params) {
			toastElement(params, shortDelay);
		};

		self.toastLong = function (params) {
			toastElement(params, longDelay);
		};

		self.toastVeryLong = function (params) {
			toastElement(params, veryLongDelay);
		};
	}
})();
