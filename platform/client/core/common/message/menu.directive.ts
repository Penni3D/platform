(function () {
	'use strict';

	angular
		.module('core')
		.directive('tzMenu', TzMenu)
		.directive('menu', Menu);

	TzMenu.$inject = ['$compile', 'MessageService', 'HTMLService', 'Translator', 'Sanitizer', '$sanitize', 'Common'];
	function TzMenu($compile, MessageService, HTMLService, Translator, Sanitizer, $sanitize, Common) {
		return {
			link: function (scope, element) {
				let html = '';
				let bottomHtml = '<divider></divider>';
				let bottomContent = false;
				for (let i = 0, l = scope.items.length; i < l; i++) {
					if (typeof scope.items[i].html === 'undefined') {
						let name = Sanitizer(Translator.translation(scope.items[i].name));
						html += HTMLService.initialize('div')
							.tabindex("0")
							.class('aria-detect ellipsis pointer no-select menu-item-' + i)
							.attr('tooltip', name)
							.click('onClick(' + i + ', $event)')
							.ngClass('{ disabled: items[' + i  +'].disabled }')
							.content(name)
							.getHTML();
					} else {
						bottomContent = true;
						bottomHtml += HTMLService.initialize('div')
							.class('no-select')
							.click('onClick(' + i + ', $event)')
							.content($sanitize(scope.items[i].html))
							.getHTML();
					}
				}

				if (bottomContent) html += bottomHtml;
				$compile(html)(scope).appendTo(element);

				scope.onClick = function (i, event) {
					let item = scope.items[i];

					if( Common.isFalse(item.disabled)) {
						if (typeof item.onClickArgument === 'undefined') {
							item.onClick(event);
						} else {
							item.onClick(item.onClickArgument, event);
						}

						MessageService.triggerCatcher('menu');
					}
				};
			}
		}
	}

	Menu.$inject = ['MessageService'];
	function Menu(MessageService) {
		return {
			scope: {
				options: '<'
			},
			template:
				'<input-button type="icon" ng-click="openMenu($event)">' +
					'<i class="material-icons" aria-hidden="true">more_vert</i>' +
				'</input-button>',
			controller: function ($scope, $element) {
				$scope.openMenu = function (event) {
					MessageService.menu($scope.options, event.currentTarget);
				};

				if (!Array.isArray($scope.options)) {
					$scope.$destroy();
					$element.remove();
				}
			}
		}
	}
})();