﻿(function () {
	'use strict';

	angular
		.module('core.modules', [
			'ngMessages',
			'chart.js',
			'ngFileUpload',
			'ngSanitize',
			'ngTouch'
			]);

})();