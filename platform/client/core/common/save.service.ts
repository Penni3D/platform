﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('SaveService', SaveService);

	SaveService.$inject = ['$rootScope', '$q', 'MessageService'];
	function SaveService($rootScope, $q, MessageService) {
		let self = this;
		self.saving = false;

		let saveQueue = [];
		let promises = [];
		let tickTimeout;

		self.subscribeSave = function (scope, callback) {
			let handler = $rootScope.$on('notifySave', function () {
				promises.push(callback());
			});
			scope.$on('$destroy', handler);
		};

		self.subscribeToolbarSaved = function (scope, callback) {
			let handler = $rootScope.$on('notifySaveToolbar', callback);
			scope.$on('$destroy', handler);
		};

		self.subscribeToolbarSaveFailure = function (scope, callback) {
			let handler = $rootScope.$on('notifySaveToolbarFailed', callback);
			scope.$on('$destroy', handler);
		};

		function notifySave() {
			$rootScope.$emit('notifySave');
		}

		self.notifyActionNeeded = function() {
			$rootScope.$emit('notifyActionNeeded');
		}

		function notifyToolbarSaved() {
			$rootScope.$emit('notifySaveToolbar');
		}

		function notifyToolbarSaveFailure() {
			$rootScope.$emit('notifySaveToolbarFailed');
		}

		function save() {
			let l = saveQueue.length;
			if (l) {
				let previousSave = saveQueue[l - 1];

				if (previousSave.inProgress) {
					let s = {
						inProgress: false,
						hasFailed: false,
						promise: previousSave.promise.finally(function () {
							s.inProgress = true;
							s.hasFailed = previousSave.hasFailed;

							return waitForSave(s.hasFailed);
						})
					};

					saveQueue.push(s);
					return s.promise;
				}

				return previousSave.promise;
			} else {
				let s = {
					inProgress: true,
					hasFailed: false,
					promise: waitForSave().catch(function () {
						s.hasFailed = true;
					})
				};

				saveQueue.push(s);

				return s.promise;
			}
		}

		function waitForSave(hasFailed) {
			notifySave();

			let p = promises;
			promises = [];

			return $q.all(p)
				.then(function () {
					if (!hasFailed) {
						if (!inTransition) $rootScope.$broadcast('inputChart-changed');
						notifyToolbarSaved();
					}
				}, function () {
					notifyToolbarSaveFailure();
				})
				.finally(function () {
					saveQueue.shift();
					inTransition = false;
					if (!saveQueue.length) {
						self.saving = false;
					}
				});
		}

		let inTransition = false;
		self.bypassTick = function (inTransit = false) {
			if (self.saving) MessageService.toast('API_SAVING');
			self.saving = true;
			clearTimeout(tickTimeout);
			inTransition = inTransit;
			return save();
		};

		self.tick = function (force) {
			self.saving = true;
			clearTimeout(tickTimeout);
			tickTimeout = setTimeout(save, force ? 0 : 3000);
		};

		window.onbeforeunload = function () {
			if (self.saving) return true;
		};
	}
})();
