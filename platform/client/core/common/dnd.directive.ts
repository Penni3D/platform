(function () {
	'use strict';

	angular
		.module('core')
		.directive('draggableProcess', DraggableProcess)
		.directive('droppableProcess', DroppableProcess)
		.directive('sortable', Sortable);

	DraggableProcess.$inject = ['$rootScope'];
	function DraggableProcess($rootScope) {
		return {
			scope: true,
			link: function (scope, element, attrs) {
				var process = attrs.draggableProcess;

				element.draggable({
					cancel: '',
					connectWith: ".main-container",
					appendTo: document.body,
					scroll: true,
					opacity: 1,
					delay: 50,
					dropOnEmpty: false,
					forceHelperSize: true,
					forcePlaceholderSize: true,
					helper: function () {
						return $(this).find('.primary').clone().attr('dragging-process', process);
					},
					start: function () {
						$rootScope.$broadcast('DragProcessStart', process);
						scope.$apply();
					},
					stop: function () {
						$rootScope.$broadcast('DragProcessStop', process);
						scope.$apply();
					}
				});
			}
		};
	}

	DroppableProcess.$inject = ['$parse'];
	function DroppableProcess($parse) {
		return {
			scope: true,
			link: function (scope, element, attrs) {
				element.droppable({
					accept: '[draggable-process]',
					drop: function (event, ui) {
						var itemId = ui.draggable.attr('item-id');
						if(itemId == '[]') return;
						var process = ui.draggable.attr('draggable-process');
						if(itemId.includes("[")){
							var l = itemId.split("[");
							var l2 = l[1].split("]")
							var l3 = l2[0].split(",");
							_.each(l3, function(id){
								$parse(attrs.droppableProcess)(scope)(process, id);
							})
						} else {
							$parse(attrs.droppableProcess)(scope)(process, itemId);
						}
					}
				});
			}
		};
	}

	Sortable.$inject = ['$parse', 'Sanitizer'];
	function Sortable($parse, Sanitizer) {
		return {
			scope: false,
			link: function (scope, element, attrs) {
				var connectWith = attrs.connect ? attrs.connect : '';
				var handler = attrs.handler ? attrs.handler : '';

				element.sortable({
					helper: function (event, ui) {
						var $original = ui.children();
						var $helper = ui.clone();
						$helper.children().each(function (index) {
							$(this).css({
								'width': $original.eq(index).outerWidth(),
								'height': $original.eq(index).outerHeight()
							});
						});

						return $helper;
					},
					handle: handler,
					connectWith: connectWith,
					forceHelperSize: true,
					forcePlaceholderSize: true,
					stop: function (event, ui) {
						var previousObject;
						var nextObject;
						var parentObject;

						var $previous = ui.item.prev();
						var $next = ui.item.next();
						var $parent = ui.item.parent();

						var self = $parse(ui.item.attr('sort'))(angular.element(ui.item).scope());

						if ($previous) {
							previousObject = $parse($previous.attr('sort'))(angular.element($previous).scope());
						}

						if ($next) {
							nextObject = $parse($next.attr('sort'))(angular.element($next).scope());
						}

						if ($parent) {
							parentObject = $parse($parent.attr('parent'))(angular.element($parent).scope());
						}

						if (attrs.onSort) {
							$parse(attrs.onSort)(scope)
							(self, previousObject, nextObject, parentObject, $parse(attrs.parent)(scope));
						}
					}
				});
			}
		}
	}
})();