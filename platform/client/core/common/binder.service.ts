﻿(function () {
	'use strict';

	angular
		.module('core')
		.service('BinderService', BinderService);

	BinderService.$inject = ['Sanitizer', 'Translator', '$rootScope', 'Common'];

	function BinderService(Sanitizer, Translator, $rootScope, Common) {
		let timeout;
		let $tooltip = $('<tooltip class="caption">').appendTo(document.body);
		let visible = false;

		$(document.body).on('mouseleave.tooltip', '[tooltip]', function () {
			clearTimeout(timeout);
			visible = false;
			removeTooltip();
		}).on('mouseover.tooltip', '[tooltip]', function (event) {
			if (!visible) {
				let $target = $(event.currentTarget);
				$target.on('mouseup', function () {
					clearTimeout(timeout);
					removeTooltip();
					visible = false;
				});

				timeout = setTimeout(function () {
					showTooltip($target);
				}, 500);
			}
		});

		function showTooltip($target) {
			let text = $target.attr('tooltip');
			if (!text) return;
			if (text.includes('html') && text.includes('delta')) {
				let t = JSON.parse(text);
				if (t[$rootScope.clientInformation.locale_id]) {
					$tooltip.html(Common.stripHtmlTags(t[$rootScope.clientInformation.locale_id].html, true));
				} else {
					$tooltip.html(Common.stripHtmlTags(t.html, true));
				}
			} else {
				$tooltip.html(Sanitizer(Translator.translate(text)));
			}
			$tooltip.css('display', '');

			let offset = $target.offset();

			let left = offset.left;
			if (left + $tooltip.width() > window.innerWidth - 16) {
				left = offset.left - $tooltip.width() + 16;
			}

			let top = offset.top - $tooltip.height() - 16;
			if (top <= 0) top = offset.top + 36;

			visible = true;
			$tooltip.css({
				'left': left,
				'top': top,
				'max-width': ''
			});

			setTimeout(function () {
				$tooltip.addClass('transition-in');
			}, 75);
		}

		function removeTooltip() {
			$tooltip.removeClass('transition-in');

			setTimeout(function () {
				$tooltip.css('display', 'none');
			}, 150);
		}
	}
})();
