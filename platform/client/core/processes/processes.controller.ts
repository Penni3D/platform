﻿﻿(function () {
	'use strict';

	angular
		.module('core.processes')
		.controller('ProcessesController', ProcessesController)
		.controller('ProcessesDialogController', ProcessesDialogController);

	ProcessesController.$inject = [
		'$filter',
		'$scope',
		'Processes',
		'ProcessesService',
		'ViewService',
		'MessageService',
		'SaveService',
		'Common',
		'ProcessActions',
		'Colors',
		'StateParameters',
		'Translator',
		'ProcessActionsService'
	];

	function ProcessesController(
		$filter,
		$scope,
		Processes,
		ProcessesService,
		ViewService,
		MessageService,
		SaveService,
		Common,
		ProcessActions,
		Colors,
		StateParameters,
		Translator,
		ProcessActionsService
	) {
		var changedItems = [];
		$scope.colours = Colors.getColorsArray();
		$scope.processes = $filter('orderBy')(Processes, function (process) {
            return Translator.translation(process.LanguageTranslation);
		});
		$scope.actions = ProcessActions;
		$scope.selectedRows = [];

		SaveService.subscribeSave($scope, function () {
			var l = changedItems.length;
			if (l) {
				var processArray = [];
				for (var i = 0; i < l; i++) {
					var j = $scope.processes.length;
					while (j--) {
						var process = $scope.processes[j];
						if (process.ProcessName == changedItems[i]) {
							processArray.push(process);
							break;
						}
					}
				}

				return ProcessesService.save(processArray).then(function () {
					changedItems = [];
				});
			}
		});

		$scope.fNumber = function(numb: number):string {
		    return Common.formatNumber(numb); 
        };

		let updateActions = function(){
			ViewService.refresh(StateParameters.ViewId);
		};

		ViewService.footbar(StateParameters.ViewId, {
			template: 'core/processes/processesFooter.html',
			scope: $scope
		});

		$scope.addNewProcess = function () {
			var params = {
				template: 'core/processes/processesDialog.html',
				controller: 'ProcessesDialogController',
				locals: {
					ProcessType: StateParameters.processType,
					Processes: $scope.processes,
					UpdateActions: updateActions
				}
			};

			MessageService.dialogAdvanced(params);
		};

		$scope.deleteProcesses = function () {
			if ($scope.selectedRows.length) {
				MessageService.confirm('PROCESS_DELETE_CONFIRMATION', function () {
					var p = ProcessesService.delete($scope.selectedRows.join(','));
					p.then(function () {
						var i = $scope.selectedRows.length;
						while (i--) {
							var j = $scope.processes.length;
							while (j--) {
								var process = $scope.processes[j];
								if (process.ProcessName == $scope.selectedRows[i]) {
									$scope.processes.splice(j, 1);
									break;
								}
							}
						}
						$scope.selectedRows.length = 0;
					});
					return p;
				});
			}
		}

		$scope.header = {
			title: StateParameters.title
		};

		$scope.openProcess = function (process) {
			var view = 'settings.summary';
			  if(StateParameters.processType == 4) {
				view = 'settings.matrix'
			}
			ViewService.view(view, {params: {processType: StateParameters.processType, process: process}});
		};

        $scope.openProcessImport = function (process) {
            var view = 'ExcelImport';
            ViewService.view(view, {params: {processType: StateParameters.processType, process: process}});
        };
        
        $scope.onChange = function (name) {
			if (changedItems.indexOf(name) === -1) {
				changedItems.push(name);
			}
		};

		$scope.processType = StateParameters.processType;

		$scope.compareOptions = [
			{ value: 0, name: Translator.translate('LIST_COMPARE0') },
			{ value: 1, name: Translator.translate('LIST_COMPARE1') },
			{ value: 2, name: Translator.translate('LIST_COMPARE2') },
			{ value: 3, name: Translator.translate('LIST_COMPARE3') }];
	}

	ProcessesDialogController.$inject = [
		'$scope',
		'ProcessesService',
		'MessageService',
		'ProcessType',
		'Common',
		'Processes',
		'SettingsService',
		'ProcessActionsService',
		'ListsService',
		'$q',
		'$rootScope',
		'MatrixProcessService',
		'Translator',
		'Views',
		'UpdateActions'
	];

	function ProcessesDialogController(
		$scope,
		ProcessesService,
		MessageService,
		ProcessType,
		Common,
		Processes,
		SettingsService,
		ProcessActionsService,
		ListsService,
		$q,
		$rootScope,
		MatrixProcessService,
		Translator,
		Views,
		UpdateActions
	) {
		$scope.newPhases = {};

		$scope.processType = ProcessType;

		let mappedSystemLanguages = _.map($rootScope.languages, 'value');

		let ensureBothTranslations = function(mainObject){

			let hasTranslations = [];
			let backupTranslation = "";

			_.each(mainObject.LanguageTranslation, function(lt, langKey){
				hasTranslations.push(langKey);
				backupTranslation = lt;
			});

			_.each(mappedSystemLanguages, function(language){
				if(!_.includes(hasTranslations, language)){
					mainObject["LanguageTranslation"][language] = backupTranslation;
				}
			});
		};

		$scope.features = Views.getFeatures();
		let processWizard = {
			ExecuteStoredProcedure: 0,
			LanguageTranslation: {},
			ProcessType: 0,
			Variable: "$scope.variable"
		};

		$scope.wizardGoing = false;

		$scope.newWizard = function(){
			$scope.wizardGoing = true;
			$scope.addNewPhase();
			$scope.addNewPhase();
			$scope.addNewPhase();

			processWizard.LanguageTranslation = $scope.languageTranslation;
			processWizard.Variable = $scope.variable;
			ensureBothTranslations(processWizard);

		};

		$scope.wizardExecuting = false;

		$scope.wizardExecution = function(){
			$scope.wizardExecuting = true;
				ProcessesService.initializeWizard(processWizard).then(function(p) {
					Processes.push(p);
					_.each($scope.phaseRows, function(phaseRow){
						ensureBothTranslations(phaseRow);
					});

					ProcessesService.executeWizard(p.ProcessName,  $scope.phaseRows).then(function(newActionId){
						p.NewRowActionId = newActionId;
							UpdateActions();
							MessageService.closeDialog();

					});
				});
		};

		
		var tickTimeout;
		$scope.disabled = true;
		$scope.buttonText = 'SAVE';
		$scope.languageTranslation = {};
		$scope.variable = '';

		$scope.translationChange = function () {
			$scope.disabled = true;
			var translation = Translator.translation($scope.languageTranslation);
			if (translation) {
				$scope.variable = Common.formatVariableName(translation);
				checkVariableUniqueness();
			}
		};

		$scope.variableChange = function () {
			$scope.disabled = true;

			if ($scope.variable) {
				$scope.variable = Common.formatVariableName($scope.variable);
				checkVariableUniqueness();
			}
		};
		$scope.hasDuplicateTranslation = true;

		function checkVariableUniqueness() {
			if ($scope.variable.length > 50) {
				$scope.variable = $scope.variable.substr(0, 50);
			}

			clearTimeout(tickTimeout);
			tickTimeout = setTimeout(function () {
				ProcessesService.isUniqueVariable($scope.variable).then(function (isUnique) {
					if (isUnique) {
						$scope.disabled = false;
						$scope.buttonText = Translator.translate('SAVE');
						$scope.hasDuplicateTranslation = false;
					} else {
						$scope.buttonText = Translator.translate('ALREADY_IN_USE');
						$scope.hasDuplicateTranslation = true;
					}
				});
			}, 1500);
		}

		$scope.wizardDisable = true;
		$scope.deletePhaseRow = function (index) {
            $scope.phaseRows.splice(index, 1);
            $scope.arePhasesOk(); //check if remaining phase rows are ok
		};

		// In this function we do some initialisions that dont depend on anything else basically
		$scope.startWizard = function () {
			$scope.addNewPhase();
			$scope.addNewPhase();
			$scope.addNewPhase();
			$scope.executeStoredProcedure = 0;
			$scope.wizardDisable = false;
			$scope.save();
		//	createPhaseList();
		};

		var orderIterator = 1;
		$scope.phaseRows = [];

		$scope.setFeatures = function(phase){
			let selectedFeatures = [];

			_.each(phase.$$thisFeatures, function(featureName){
				let p = _.find($scope.features, function(o){
					return o.name == featureName;
				});

				let feature = {Feature: "", DefaultState: ""};
				feature.Feature = p.name;
				feature.DefaultState = p.view;
				selectedFeatures.push(feature);
			});

			phase.SelectedFeatures = selectedFeatures;
		};

		$scope.addNewPhase = function () {
			var newPhase = {
				LanguageTranslation: {},
				OrderNo: orderIterator,
				TabCount: 0,
				SelectedFeatures: []
			};
			//ensureBothTranslations(newPhase)
			orderIterator++;
            $scope.phaseRows.push(newPhase);
            $scope.phasesOk = false; //empty new phase cannot be ok before user edit
		};


		$scope.executeStoredProcedure = 1;

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		//This save function has if clauses to let the user choose if he/she wants to use the wizard functionality or not.
		$scope.save = function () {

			if($scope.hasDuplicateTranslation) return;
			var data = {
				ProcessType: ProcessType,
				Variable: $scope.variable,
				LanguageTranslation: $scope.languageTranslation,
				ExecuteStoredProcedure: $scope.executeStoredProcedure
			};

			if (!$scope.wizardGoing) {
				ProcessesService.new(data).then(function (process) {
					if(process.ProcessType == 4){
						MatrixProcessService.insertMatrixColumns($scope.variable);
					}
					Processes.push(process);
					MessageService.closeDialog();
				}, function () {
					$scope.disabled = false;
				});
			}
		};
	}
})();
