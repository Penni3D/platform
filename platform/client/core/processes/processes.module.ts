﻿(function () {
	'use strict';

	angular
		.module('core.processes', [])
		.config(ProcessesConfig);

	ProcessesConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function ProcessesConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('processes', {
			controller: 'ProcessesController',
			template: 'core/processes/processes.html',
			resolve: {
				Processes: function (ProcessesService, StateParameters) {
					return ProcessesService.get(StateParameters.processType);
				},
				ProcessActions: function (ProcessActionsService) {
					return ProcessActionsService.getProcessActions();
				}
			}
		});

		ViewConfiguratorProvider.addConfiguration('processes', {
			tabs: {
				DEFAULT: {
					processType: function () {
						return {
							type: 'select',
							options: [
								{name: 'CUSTOMER_PROCESSES', value: 0}, {name: 'SUPPORT_PROCESSES', value: 1},
								{name: 'DATA_PROCESSES', value: 2}, {name: 'LINK_PROCESSES', value: 3},
								{name: 'MATRIX_PROCESSES', value: 4},
								{name: 'TABLE_PROCESSES', value: 5},
								{name: 'UNION_PROCESSES', value: 6}
							],
							label: 'PROCESSES_PROCESS_TYPE'
						}
					}
				}
			},
			defaults: {
				processType: 0
			}
		});

		ViewsProvider.addPage('processes', 'PROCESSES', ['read', 'write']);
	}
})();