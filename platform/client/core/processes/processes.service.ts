﻿(function () {
	'use strict';

	angular
		.module('core.processes')
		.service('ProcessesService', ProcessesService);

	ProcessesService.$inject = ['ApiService', 'ProcessService', 'Translator'];
	function ProcessesService(ApiService, ProcessService, Translator) {
		let self = this;

		let Processes = ApiService.v1api('settings/Processes');
		let ItemColumns = ApiService.v1api('meta/ItemColumns');
		let ProcessWizard = ApiService.v1api('settings/ProcessWizard')

		self.save = function (processes) {
			return Processes.save('', processes);
		};

		self.new = function (process) {
			return Processes.new(process.ExecuteStoredProcedure, process);
		};

		self.get = function (processType, getLists = false) {
			return Processes.get([processType, getLists ? true : ""]).then(function (processes) {
				return processes.sort(function (a, b) {
					return Translator.translation(a.LanguageTranslation) < Translator.translation(b.LanguageTranslation) ?
						-1 : 1;
				});
			});
		};

		self.delete = function (process) {
			return Processes.delete(process);
		};

		self.isUniqueVariable = function (variable) {
			return Processes.get([variable, 'unique']).then(function (response) {
				return !!response;
			});
		};

		self.getSearchResults = function(searchWord, types, process){
			return Processes.new(['search', escape(searchWord), process], types);
		};

		self.getColumns = function () {
			return ItemColumns.get().then(function (columns) {
				let i = columns.length;
				while (i--) {
					let column = columns[i];
					columns[i] = ProcessService.setColumn(column, column.Process, column.ItemColumnId);
				}

				return columns;
			});
		};

		self.initializeWizard = function(processObj){
			return ProcessWizard.new(["init", 0], processObj);
		};

		self.executeWizard = function(processName, listOfPhases){
			return ProcessWizard.new([processName], listOfPhases);
		}
	}

})();