﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.provider('Configuration', Configuration)
		.provider('ViewConfigurator', ViewConfigurator);

	ViewConfigurator.$inject = ['ConfigurationProvider'];

	function ViewConfigurator(ConfigurationProvider) {
		let views = {};

		this.addConfiguration = function (view, options) {
			if (views[view]) {
				console.log("Duplicate view configuration. Existing one is overwritten.");
			}

			let c = {};
			c[view] = {
				format: 'string',
				default: '{}'
			};


			ConfigurationProvider.addConfiguration('View Configurator', c);
			views[view] = angular.copy(options);
		};

		this.$get = function ($injector, Configuration, ApiService, $q, Widgets) {
			let Globals = ApiService.v1api('core/globals');
			let Frontpage = ApiService.v1api('core/frontpage');
			let Pages = ApiService.v1api('navigation/Pages');

			let rights = {
				global: {},
				specific: {}
			};
			let widgets = [];
			let globals = [];

			return {
				configureFeature: function (feature, params) {
					if (typeof views[feature] === 'undefined') {
						return;
					}

					$injector.get('ViewService').view('featureConfigurator', {
						params: angular.extend({view: feature}, params),
						locals: {
							View: views[feature]
						}
					});
				},
				configurePage: function (page, params, onSave) {
					if (typeof views[page] === 'undefined') {
						return;
					}

					let o = typeof onSave === 'function' ? onSave : function () {
					};
					$injector.get('ViewService').view('pageConfigurator', {
						params: angular.extend({view: page}, this.getConfiguration(page), params),
						locals: {
							SaveParameters: o,
							View: views[page]
						}
					});
				},
				isConfigureable: function (view) {
					return typeof views[view] !== 'undefined';
				},
				getConfiguration: function (view, process) {
					let c = views[view];

					if (typeof c === 'undefined') {
						return {};
					}

					return angular.extend(
						{},
						angular.copy(c.defaults),
						angular.fromJson(Configuration.getClientConfigurations('View Configurator', process, view)));
				},
				setConfiguration: function (payload) {
					let p = {
						'View Configurator': payload
					};

					return Configuration.saveClientConfiguration(p);
				},
				getGlobal: function (global) {
					let i = globals.length;
					while (i--) {
						if (globals[i].DefaultState == global) {
							return globals[i];
						}
					}

					return {};
				},
				getGlobals: function () {
					return globals;
				},
				fetchGlobals: function () {
					return Globals.get().then(function (g) {
						globals = g;
					});
				},
				fetchWidgets: function () {
					let localWidgets = Widgets.getWidgets();
					return Frontpage.get().then(function (f) {
						for (let w of f) {
							if (w.Color == null) {
								let found = localWidgets.find(f => f.view === w.DefaultState);
								if (found) w.Color = found.params.Color;
							}
						}
						widgets = f;
					});
				},
				getWidgets: function () {
					return widgets;
				},
				removeWidgets: function (widgetIds) {
					let ids = String(widgetIds).split(',');
					let i = ids.length;
					while (i--) {
						let id = ids[i];

						let j = widgets.length;
						while (j--) {
							let w = widgets[j];

							if (w.InstanceMenuId == id) {
								widgets.splice(j, 1);
								break;
							}
						}
					}
				},
				addWidget: function (widget) {
					widgets.push(widget);
				},
				clearRights: function (view) {
					delete rights.global[view];
					delete rights.specific[view];
				},
				getRights: function (view, menuId) {
					if (typeof menuId === 'undefined') {
						if (typeof rights.global[view] === 'undefined') {
							return Pages.get(view).then(function (r) {
								rights.global[view] = r;
								return rights.global[view];
							});
						}

						let d = $q.defer();
						d.resolve(rights.global[view]);
						return d.promise;
					} else {
						if (typeof rights.specific[menuId] === 'undefined') {
							return Pages.get([view, menuId]).then(function (r) {
								rights.specific[view] = r;
								return rights.specific[view];
							});
						}

						let d = $q.defer();
						d.resolve(rights.specific[view]);
						return d.promise;
					}
				}
			};
		};
	}

	Configuration.$inject = [];

	function Configuration() {
		let categories = {};
		let configurations = {};
		let serverConfigurations = {};
		let activePreferences = {};

		this.addConfiguration = function (key, params) {
			if (typeof categories[key] === 'undefined') {
				categories[key] = angular.copy(params);
			} else {
				angular.forEach(params, function (p, v) {
					if (categories[key][v] && key !== 'View Configurator') {
						alert("Duplicate configuration. Existing one is overwritten.");
					}

					categories[key][v] = p;
				});
			}
		};

		this.$get = function (ApiService) {
			let Configuration = ApiService.v1api('configuration');
			let changedPreferences = {};
			let saveTimer;

			let isJSON = function (str: string) {
				try {
					JSON.parse(str);
				} catch (e) {
					return false;
				}
				return true;
			}

			return {
				fetchActivePreferences: function () {
					return Configuration.get().then(function (configurations) {
						let i = configurations.length;
						while (i--) {
							let uc = configurations[i];

							if (typeof activePreferences[uc.Group] === 'undefined')
								activePreferences[uc.Group] = {};

							if (isJSON(uc.Value))
								activePreferences[uc.Group][uc.Identifier] = angular.fromJson(uc.Value);

						}
					});
				},
				getActivePreferences: function (group, identifier) {
					if (typeof identifier === 'undefined') {
						if (typeof activePreferences[group] === 'undefined') {
							activePreferences[group] = {};
						}

						return activePreferences[group];
					} else {
						if (typeof activePreferences[group] === 'undefined') {
							activePreferences[group] = {};
						}

						let groupPreferences = activePreferences[group];
						if (typeof groupPreferences[identifier] === 'undefined') {
							groupPreferences[identifier] = {};
						}

						return groupPreferences[identifier];
					}
				},
				setChangedPreferences: function (group, identifiers) {
					changedPreferences[group] = identifiers;
				},
				setActivePreferences: function (group, value) {
					activePreferences[group] = value;
				},
				saveActivePreferences: function (group, identifier) {
					if (typeof changedPreferences[group] === 'undefined') {
						changedPreferences[group] = [];
					}

					let changedGroup = changedPreferences[group];
					let i = changedGroup.length;
					while (i--) {
						if (changedGroup[i] == identifier) {
							break;
						}
					}

					if (i === -1) {
						changedGroup.push(identifier);
					}

					clearTimeout(saveTimer);
					saveTimer = setTimeout(function () {
						angular.forEach(changedPreferences, function (value, group) {
							let p = {};

							let i = value.length;
							while (i--) {
								p[value[i]] = activePreferences[group][value[i]];
							}

							Configuration.save(group, p);
						});

						changedPreferences = {};
					}, 3000);
				},
				deleteSavedPreferences: function (group, identifier, id, userId) {
					return Configuration.delete(['saved', group, identifier, id, userId]);
				},
				getSavedPreferences: function (group, identifier) {
					return Configuration.get(['saved', group, identifier]).then(function (configurations) {
						let i = configurations.length;
						while (i--) {
							let uc = configurations[i];
							uc.Value = angular.fromJson(uc.Value);
							uc.Name = angular.fromJson(uc.Name);
						}
						return configurations;
					});
				},
				savePreferences: function (group, identifier, id, name, share, data, filtered?, userGrouped?, defaultView?, dashboardId?) {
					let payload = {
						value: data,
						public: share,
						name: angular.toJson(name),
						visible_for_filter: filtered,
						visible_for_usergroup: userGrouped,
						portfolio_default_view: defaultView,
						dashboard_id: dashboardId
					};

					return Configuration.save(['saved', group, identifier, id], payload);
				},
				newPreferences: function (group, id, name, share, data, filtered?, userGrouped?, defaultView?, dashboardId?) {
					let payload = {
						value: data,
						public: share,
						name: angular.toJson(name),
						visible_for_filter: filtered,
						visible_for_usergroup: userGrouped,
						portfolio_default_view: defaultView,
						dashboard_id: dashboardId

					};

					return Configuration.new(['saved', group, id], payload).then(function (conf) {
						conf.Value = angular.fromJson(conf.Value);
						conf.Name = angular.fromJson(conf.Name);

						return conf;
					});
				},
				fetchServerConfigurations: function () {
					return Configuration.get('server').then(function (configurations) {
						serverConfigurations = configurations;
					});
				},
				fetchClientConfigurations: function () {
					return Configuration.get('client').then(function (response) {
						let r = {};
						let i = response.length;
						while (i--) {
							let conf = response[i];

							if (typeof r[conf.Category] === 'undefined') {
								r[conf.Category] = {};
							}

							let clientCategory = r[conf.Category];

							if (typeof clientCategory[conf.Process] === 'undefined') {
								clientCategory[conf.Process] = {};
							}

							clientCategory[conf.Process][conf.Name] = {
								value: conf.Value,
								process: conf.Process
							};
						}

						configurations = r;
					});
				},
				getConfigurations: function () {
					return configurations;
				},
				getCategories: function () {
					return categories;
				},
				saveServerConfiguration: function (payload) {
					return Configuration.new('server', payload);
				},
				saveClientConfiguration: function (payload) {
					return Configuration.new('client', payload).then(function () {
						angular.forEach(payload, function (category, c) {
							angular.forEach(category, function (process, p) {
								angular.forEach(process, function (settings, s) {

									if (typeof configurations[c] === 'undefined') {
										configurations[c] = {};
									}

									if (typeof configurations[c][p] === 'undefined') {
										configurations[c][p] = {};
									}

									if (typeof configurations[c][p][s] === 'undefined') {
										configurations[c][p][s] = {};
									}

									let clientConfs = configurations[c][p][s];
									clientConfs.process = settings.process;
									clientConfs.value = typeof settings.value === 'object' ?
										angular.toJson(settings.value) : settings.value;

								});
							});
						});

					});
				},
				getServerConfigurations: function (name) {
					if (typeof name === 'undefined') {
						return serverConfigurations;
					}

					let r = {};
					angular.extend(r, serverConfigurations.dynamic[name], serverConfigurations.static[name]);

					return r;
				},
				getClientConfigurations: function (key, process, option) {
					let returnObject;
					let conf = configurations[key];

					if (option) {
						if (conf &&
							conf[process] &&
							conf[process][option]
						) {
							returnObject = conf[process][option].value;
						} else if (conf &&
							conf.null &&
							conf.null[option]
						) {
							returnObject = conf.null[option].value;
						} else {
							returnObject = categories[key][option].default;
						}
					} else {
						let option = process;
						if (conf &&
							conf.null &&
							conf.null[option]
						) {
							returnObject = conf.null[option].value;
						} else {
							returnObject = categories[key][option].default;
						}
					}

					return angular.copy(returnObject);
				}
			};
		};
	}

})();