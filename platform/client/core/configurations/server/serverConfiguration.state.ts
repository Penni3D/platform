﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.config(ServerConfigurationConfig);

	ServerConfigurationConfig.$inject = ['ViewsProvider'];
	function ServerConfigurationConfig(ViewsProvider) {
		ViewsProvider.addView('configuration.server', {
			template: 'core/configurations/server/serverConfiguration.html',
			controller: 'ServerConfigurationController',
			resolve: {
				ServerConfigurations: function (Configuration) {
					return Configuration.getServerConfigurations();
				},
				Domains: function (Configuration) {
					var domains = Configuration.getServerConfigurations();
					var d = [];
					for (var i = 0, l = domains.domain.length; i < l; i++) {
						d.push({ name: domains.domain[i] });
					}

					return d;
				}
			}
		});
	}

})();
