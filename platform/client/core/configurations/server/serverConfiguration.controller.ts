﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.controller('ServerConfigurationController', ServerConfigurationController);

	ServerConfigurationController.$inject = [
		'$scope',
		'ServerConfigurations',
		'Configuration',
		'Domains',
		'SaveService',
		'SidenavService',
		'StateParameters'
	];

	function ServerConfigurationController(
		$scope,
		ServerConfigurations,
		Configuration,
		Domains,
		SaveService,
		SidenavService,
		StateParameters
	) {

		$scope.model = ServerConfigurations.dynamic;
		$scope.configurations = ServerConfigurations;
		$scope.oldConf = angular.copy($scope.configurations);
		$scope.edit = true;

		SidenavService.navigation([{
			name: 'Settings',
			tabs: [{
				name: "Client configurations",
				view: "configuration.client",
				params: StateParameters
			},
			{
				name: "Server configurations",
				view: "configuration.server",
				params: StateParameters
			}]
		}]);

		SaveService.subscribeSave($scope, function () {
			return Configuration.saveServerConfiguration($scope.model);
		});

		$scope.header = {
			title: 'SERVER_CONFIGURATIONS',
			icons: []
		};

		$scope.selectedRows = [];
		$scope.domains = Domains;
	}
})();
