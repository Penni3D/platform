﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.config(ClientConfigurationConfig);

	ClientConfigurationConfig.$inject = ['ViewsProvider'];
	function ClientConfigurationConfig(ViewsProvider) {
		ViewsProvider.addView('configuration.client',
			{
				template: 'core/configurations/client/clientConfiguration.html',
				controller: 'ClientConfigurationController',
				resolve: {
					Categories: function (Configuration) {
						var categories = Configuration.getCategories();
						var t = {};
						angular.forEach(categories, function (category, key) {
							t[key] = { null: {} };

							angular.forEach(category, function (name, key_) {
								t[key][null][key_] = name;
							});
						});

						return t;
					},
					ClientConfigurations: function (Configuration) {
						return Configuration.getConfigurations();
					},
					Processes: function (ProcessesService, $filter) {
						return ProcessesService.get().then(function (processes) {
							var t = [];
							for (var i = 0, l = processes.length; i < l; i++) {
								var process = processes[i];
								t.push({ name: $filter('translate')(process.ProcessName), value: process.ProcessName });
							}

							return t;
						});
					}
				}
			});

		ViewsProvider.addPage('configuration.client', 'PAGE_CONFIGURATION_CLIENT', ['read', 'write']);
	}
})();
