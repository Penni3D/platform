﻿(function () {
	'use strict';

	angular
		.module('core.init')
		.controller("ClientConfigurationController", ClientConfigurationController);

	ClientConfigurationController.$inject = [
		'$scope',
		'Categories',
		'Configuration',
		'ClientConfigurations',
		'Processes',
		'SaveService',
		'SidenavService',
		'StateParameters'
	];

	function ClientConfigurationController(
		$scope,
		Categories,
		Configuration,
		ClientConfigurations,
		Processes,
		SaveService,
		SidenavService,
		StateParameters
	) {

		$scope.categories = Categories;
		$scope.model = ClientConfigurations;
		$scope.processes = Processes;
		$scope.edit = true;
		
		SidenavService.navigation([{
			name: 'Settings',
			tabs: [{
				name: "Client configurations",
				view: "configuration.client",
				params: StateParameters
			},
			{
				name: "Server configurations",
				view: "configuration.server",
				params: StateParameters
			}]
		}]);

		SaveService.subscribeSave($scope, function () {
			return Configuration.saveClientConfiguration($scope.model);
		});

		$scope.header = {
			title: 'CLIENT_CONFIGURATIONS',
			icons: []
		};
	}
})();
