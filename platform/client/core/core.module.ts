/// <reference path="../types/client.d.ts" />
(function () {
	'use strict';

	angular
		.module('core.init', []);

	angular
		.module('core', [
			'core.modules',
			'core.states',
			'core.chartconfig',
			'core.exceptionHandler',
			'ngFileSaver'
		])
		.config(CoreConfig)
		.run(CoreRun)
		.filter('isEmptyObject', function () {
			return function (obj) {
				if (typeof obj === 'undefined' || typeof obj !== 'object') {
					return true;
				}

				return Object.keys(obj).length === 0;
			};
		});

	//Exception Handling 1/2
	angular
		.module('core.exceptionHandler', [])
		.factory('$exceptionHandler', ExceptionHandler);


	ExceptionHandler.$inject = ['$log', '$injector'];

	function ExceptionHandler($log, $injector) {
		return function (exception, cause) {
			let ErrorService = $injector.get('ErrorService');

			if (typeof exception === 'string' && exception.indexOf("Possibly unhandled rejection") === -1) {
				ErrorService.clientError(exception, 'Angular');
			}

			//Output the error
			$log.warn(exception, cause);
		};
	}

	//CONFIGURE APPLICATION
	CoreConfig.$inject = [
		'$httpProvider',
		'$locationProvider',
		'ConfigurationProvider',
		'ViewsProvider',
		'GlobalsProvider',
		'$sanitizeProvider'
	];

	function CoreConfig(
		$httpProvider,
		$locationProvider,
		ConfigurationProvider,
		ViewsProvider,
		GlobalsProvider,
		$sanitizeProvider
	) {
		//Caching Control for (mainly) IE11
		$httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
		$httpProvider.defaults.headers.common['Pragma'] = 'no-cache';
		$httpProvider.defaults.cache = false;

		if (!$httpProvider.defaults.headers.get) $httpProvider.defaults.headers.get = {};

		$httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
		$sanitizeProvider
			.addValidElements(['loader'])
			.addValidAttrs(['id', 'droppable-process', 'ng-show', 'style']);

		try {
			history.pushState("", document.title, window.location.pathname + window.location.search);
		} catch (e) {

		}

		ConfigurationProvider.addConfiguration('Main application',
			{
				'Environment banner': {
					format: 'string',
					default: ''
				},
				'Disable animations': {
					format: 'string',
					default: 'false'
				},
				'Disable right sidenav': {
					format: 'string',
					default: 'false'
				},
				'Disable all navs': {
					format: 'string',
					default: 'false'
				},
				'Disable Resourcemanagement': {
					format: 'string',
					default: 'false'
				},
				'Disable conversation': {
					format: 'string',
					default: 'false'
				},
				'Disable autohelp': {
					format: 'string',
					default: 'false'
				},
				'Lock left sidenavigation on high resolution devices': {
					format: 'string',
					default: 'true'
				},
				'Lock right sidenavigation on high resolution devices': {
					format: 'string',
					default: 'true'
				},
				'Domain path': {
					format: 'string',
					default: '/'
				},
				'Application name': {
					format: 'string',
					default: 'KETO'
				},
				'Login Header': {
					format: 'string',
					default: ''
				},
				'Login Content (html)': {
					format: 'text',
					default: ''
				}
			});
		GlobalsProvider.addGlobal('profile', ['read']);
		GlobalsProvider.addGlobal('impersonate', ['delegate', 'impersonate', 'admin']);
		GlobalsProvider.addGlobal('diagnostics', ['read']);
		GlobalsProvider.addGlobal('settings', ['read', 'write', 'delete']);

		//Start monitoring HTTP Statuses
		$httpProvider.interceptors.push('httpInterceptor');

		//Remove the # from URL -- Beware of old browsers!
		$locationProvider.html5Mode({enabled: true, requireBase: false});

		//Initialize Translator
		let forceLanguage = new RegExp('[\?&]l=([^&#]*)').exec(window.location.href);
		ViewsProvider.addView('main', {
			resolve: {
				ClientConfigurations: function (Configuration) {
					return Configuration.fetchClientConfigurations();
				},
				Globals: function (ViewConfigurator) {
					return ViewConfigurator.fetchGlobals();
				},
				Frontpage: function (ViewConfigurator) {
					return ViewConfigurator.fetchWidgets();
				},
				UserPreferences: function (Configuration) {
					return Configuration.fetchActivePreferences();
				},
				ServerConfigurations: function (Configuration) {
					return Configuration.fetchServerConfigurations();
				},
				DefaultSelectors: function (SelectorService) {
					return SelectorService.fetchSelectors();
				},
				Languages: function (ApiService, $rootScope) {
					return ApiService.v1api('meta/SystemList/LANGUAGES').get().then(function (languages) {
						$rootScope.languages = languages;
					});
				},
				ClientInformation: function (CoreService, Translator, $rootScope) {
					return CoreService.getClientInformation().then(function (c) {
						$rootScope.dateFormat = c.date_format;
						$rootScope.clientInformation = c;

						return c;
					});
				}
			},
			afterResolve: {
				Tags: function (TagsService) {
					return TagsService.fetchTags();
				},
				Translations: function (Translator, $rootScope) {
					let ci: Client.ClientInformationModel = $rootScope.clientInformation;
					let defaultLanguage = ci.locale_id;
					let isForcedLanguage = false;

					if (forceLanguage && forceLanguage[1]) {
						defaultLanguage = forceLanguage[1];
						ci.locale_id = defaultLanguage;
						isForcedLanguage = true;
					}

					Translator.use(defaultLanguage);
					return Translator.fetch(ci.locale_id, isForcedLanguage);
				}
			}
		});

		ViewsProvider.addView('main.layout', {
			controller: 'LayoutController',
			template: 'core/layout/layout.html',
			resolve: {
				Profile: function (ViewConfigurator) {
					return ViewConfigurator.getRights('profile');
				},
				Impersonate: function (ViewConfigurator) {
					return ViewConfigurator.getRights('impersonate');
				},
				VersionRight: function (ViewConfigurator) {
					return ViewConfigurator.getRights('settings');
				},
				StickyRights: function (ViewConfigurator, SidenavService) {
					return ViewConfigurator.getRights('sticky').then(function (rights) {
						SidenavService.setStickyRights(rights);
					});
				},
				UnreadNotifications: function (NotificationService) {
					return NotificationService.getUnreadUserNotifications();
				},
				Delegates: function (CoreService) {
					return CoreService.getDelegates();
				},
				SignalR: function (UserService, ConnectorService, ApiService, $rootScope, Configuration, $q, MessageService, $window, ProcessService, InputService, NotificationService) {
					return UserService.getCurrentUser().then(function (user) {
						$rootScope.me = user;
						let ci = $rootScope.clientInformation;

						//Connect to Hub
						return ConnectorService
							.initializeHub(
								ci.token,
								Configuration.getServerConfigurations('hostingenvironment').virtualdirectory.value)
							.then(function () {
								return ConnectorService.connect(ci.token).then(function () {
									//Add SignalR Listeners here
									ProcessService.startActionListener();
									ConnectorService.listen("NotificationsChanged", function (parameters) {
										if(parameters.UserItemId){
											if ($rootScope.me.item_id == parameters.UserItemId) NotificationService.notifyNotificationNeeded(parameters);
										} else {
											_.each(parameters, function(parameter){
												if ($rootScope.me.item_id == parameter.UserItemId) NotificationService.notifyNotificationNeeded(parameter);
											})
										}
									});
									InputService.startHelpUpdateListener();
									return ConnectorService.listen('logoutExisting', function (item_id) {
										if (item_id == $rootScope.me.item_id) return MessageService
											.msgBox('LAYOUT_LOGOUT_FORCED')
											.then(function () {
												return ApiService.v1api('core/ClearCookies').get().then(function () {
													$window.location.href = $window.location.href.split("#")[0];
												});
											});
									});
								});
							});
					});
				},
				Helpers: function (ViewHelperService) {
					ViewHelperService.getViewsHelpCollection();
				},
				MyTasks: function () {
					return [];
				},
				Restore: function ($location, LocalStorageService, ApiService, MessageService) {
					let to = $location.search().view;
					$location.search('');
					if (typeof to === 'undefined' && typeof LocalStorageService.get('urlCode') == 'undefined') {
						return;
					} else if (typeof to === 'undefined' && typeof LocalStorageService.get('urlCode') != 'undefined'){
						to = LocalStorageService.get('urlCode');
						LocalStorageService.clearKey('urlCode')
					}

					return ApiService.v1api('core/links').get(to).then(function (params) {

						LocalStorageService.store('restoreView', params);
					}, function () {
						let dialog = {
							title: 'Shared link',
							message: 'The provided link has been expired.',
							buttons: [{
								type: 'primary', text: 'Ok'
							}]
						};

						return MessageService.dialog(dialog);
					});
				}
			},
			afterResolve: {
				StickyNavigation: function (SidenavService) {
					return SidenavService.fetchSavedSettings();
				},
				NavNavigation: function (SidenavService) {
					return SidenavService.fetchStaticNavigation();
				}
			}
		});

		ViewsProvider.addFeature('meta', 'meta', 'FEATURE_META',
			[
				'read', 'history', 'write',
				'insert', 'delete', 'configure', 'help',
				'past', 'active', 'future',
				'red', 'yellow', 'green',
				'gray', 'debug', 'accent',
				'accent-2', 'icon']);
	}

	//START APPLICATION
	CoreRun.$inject = ['$rootScope', 'ViewService', '$window', 'ErrorService', 'BinderService'];

	function CoreRun($rootScope, ViewService, $window, ErrorService, BinderService) {
		// @ts-ignore
		Waves.init();
		ViewService.initialize();

		//Exception Handling 2/2
		$window.onerror = function (errorMsg, url, lineNumber) {
			ErrorService.clientError({message: errorMsg, stack: url + ':' + lineNumber}, 'RegularJs');
		};
	}
})();