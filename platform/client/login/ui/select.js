﻿(function () {
	'use strict';

	angular
		.module('login')
		.directive('selectInput', SelectInput)
		.directive('buttonInput', ButtonInput)
		.service('InputService', InputService);

	SelectInput.$inject = ['InputService', '$sanitize'];
	function SelectInput(InputService, $sanitize) {
		var modelValue = function (scope, element, option) {
			$(element).find('.input-value').html($('<span>' + $sanitize(option[scope.optionName]) + '</span>'));
		};

		return {
			scope: { modify: '<', label: '@', model: '=ngModel', options: '<', optionName: '@', optionValue: '@' },
			link: function (scope, elem) {
				$(elem).on('click', function (event) {
					event.stopPropagation();
				});
			},
			controller: function ($scope, $timeout, $element) {
				$timeout(function () {
					var l = $scope.options.length;
					if (!$scope.multiselect) {
						var i;
						for (i = 0; i < l; i++) {
							if ($scope.model == $scope.options[i][$scope.optionValue]) {
								$scope.selected = $scope.options[i];
								break;
							}
						}
					}
					modelValue($scope, $element, $scope.selected);

				}, 0);

				var opt;
				$scope.openMenu = function (event) {
					if ($scope.modify) {
						InputService.selectMenu($scope, event);
					}
				};

				$scope.selectOption = function (option) {
					$scope.selected = option;
					$scope.model = option[$scope.optionValue];
					InputService.removeSelectMenu();
					opt = option;
				};

				$scope.$watchCollection('model', function (o, n) {
					if (o != n) {
						modelValue($scope, $element, opt);
					}
				});
			},
			template:
				'<input-container ng-click="openMenu($event)">' +
					'<input id="select-input" readonly ng-model="model" ng-class="{readonly: modify == false}"/>' +
					'<div class="input-value-"></div>' +
					'<label ng-bind="::label" for="select-input"></label>' +
					'<i class="material-icons" aria-hidden="true">expand_more</i>' +
				'</input-container>'
		};
	}

	ButtonInput.$inject = [];
	function ButtonInput() {
		return {
			transclude: true,
			replace: true,
			template: '<a class="btn" ng-transclude></a>',
			link: function (scope, elem) {
				elem.addClass("btn-secondary");
				Waves.attach(elem, ['waves-button']);
			}
		};
	}


	InputService.$inject = ['$compile', '$timeout'];
	function InputService($compile, $timeout) {
		var self = this;
		var $selectMenu = $('<div class="select-input-menu" ng-keydown="keyPress($event)" tabindex=0></div>');

		var KEYS = {
			UP: 38,
			DOWN: 40,
			LEFT: 37,
			RIGHT: 39,
			ENTER: 13
		};

		self.removeSelectMenu = function () {
			$selectMenu.removeClass('transition-in');
			$(document).off("click.selectInputMenu");
			$($selectMenu).off("click");
			setTimeout(function () {
				$selectMenu.remove();
			}, 300);
		};

		self.selectMenu = function (scope, event) {
			$(document).off("click.selectInputMenu");
			$($selectMenu).off("click");
			$selectMenu.remove();

			var element = angular.element(event.currentTarget);
			var offset = element.offset();
			var width = element.width();
			var focus = -1;

			$selectMenu.css('left', offset.left);
			$selectMenu.css('min-width', width);

			scope.keyPress = function (event) {
				if (focus != -1) {
					$($selectMenu).find('#select-input-item-' + focus).removeClass('hovered');

					if (event.which == KEYS.DOWN && focus !== scope.options.length - 1) {
						focus++;
					} else if (event.which == KEYS.UP && focus !== 0) {
						focus--;
					} else if (event.which == KEYS.ENTER) {
						$timeout(function () {
							$($selectMenu).find('#select-input-item-' + focus).trigger('click');
						}, 0);
					}
				} else {
					focus = 0;
				}

				$($selectMenu).find('#select-input-item-' + focus).addClass('hovered');
			};

			var i, l = scope.options.length;
			for (i = 0; i < l; i++) {
				if (scope.model == scope.options[i][scope.optionValue]) {
					focus = i;
					break;
				}
			}

			var $selectMenuOptions = $(
				'<input-button' +
				' id="select-input-item-{{::$index}}"' +
				' ng-repeat="option in ::options"' +
				' ng-click="selectOption(option)"' +
				' ng-class="{"selected": option === selected}">{{option[optionName]}}' +
				'</input-button>');

			$($selectMenu).on('click', function (event) {
				event.stopPropagation();
			});
			$selectMenu.html($selectMenuOptions);
			$compile($selectMenu)(scope).appendTo('body');

			setTimeout(function () {
				$selectMenu.addClass('transition-in');
				$($selectMenu).focus();
				$(document).on("click.selectInputMenu", self.removeSelectMenu);
			}, 75);

			$timeout(function () {
				var o;
				var s = 0;
				if (!scope.multiselect) {
					var index = scope.options.indexOf(scope.selected);
					if (index != -1 && index < 3) {
						s = index * 48;
					} else if (index != -1 && index >= 3) {
						s = 96;
						$($selectMenu).scrollTop((index - 2) * 48);
					}
				}

				var h = $selectMenu.outerHeight() + 16 + offset.top - $(window).outerHeight() - s;
				o = h / 48;
				if (h > 0) {
					$selectMenu.css('top', offset.top + 6 - Math.ceil(o) * 48 - s);
				} else {
					$selectMenu.css('top', offset.top + 6 - s);
				}
			}, 0);
		};
	}

})();