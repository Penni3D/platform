﻿(function () {
		'use strict';

		angular
			.module('login', ['ngSanitize'])
			.config(LoginConfig)
			.controller('BasicLoginController', BasicLoginController)
			.factory('HttpService', HttpService)
			.service('ViewRestoreService', ViewRestoreService)
			.service('ToastService', ToastService);

		LoginConfig.$inject = ['$locationProvider'];

		function LoginConfig($locationProvider) {
			$locationProvider.html5Mode({enabled: true, requireBase: false});
		}

		BasicLoginController.$inject = ['$scope', '$window', 'HttpService', 'ToastService', '$location', '$sce', 'ViewRestoreService'];

		function BasicLoginController($scope, $window, HttpService, ToastService, $location, $sce, ViewRestoreService) {
			$scope.loadingDone = "";
			let to = $location.search().view;
			$location.search('');
			if(typeof to != 'undefined') ViewRestoreService.save('urlCode', to)
			HttpService.config('default').then(function (response) {
				$scope.data = response.data;
				if ($scope.getQueryStringParameter('code') !== null) {
					showToast($scope.data.LOGIN_LOGIN_FAILED_AZURE);
				}
				let error = $location.search().e;
				if (error == 401) {
					$location.search('e', null)
					showToast($scope.data.LOGIN_VIEW_SESSION_EXPIRED);
				} else if (error == 403) {
					//ADFS no account
					showToast($scope.data.LOGIN_LOGIN_FAILED_AZURE);
				}

				$scope.hidden = $scope.data.BasicAuthenticationHidden == 'True';
				$scope.passwordResetEnabled = $scope.data.BasicAuthenticationPasswordReset == 'True';
				$scope.loadingDone = "loaded";
			});

			$scope.credentials = {domain: 'default'};
			$scope.modify = true;
			$scope.disabled = false;
			$scope.changepassword = 0;
			$scope.twofactorcode = '';
			$scope.hidden = false;
			$scope.passwordResetEnabled = false;

			$scope.getQueryStringParameter = function (name, url) {
				if (!url) url = window.location.href;
				name = name.replace(/[\[\]]/g, '\\$&');
				let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
					results = regex.exec(url);

				if (!results) return null;
				if (!results[2]) return '';

				return decodeURIComponent(results[2].replace(/\+/g, ' '));
			};

			if ($scope.getQueryStringParameter('er') !== null) {
				$scope.changepassword = 2;
			}

			function showToast(message) {
				let toast = {
					message: message
				};

				ToastService.toast(toast);
			}

			$scope.secondfactorCancel = function () {
				$scope.secondfactor = false;
			};

			$scope.ketoAuthCancel = function () {
				$scope.ketoauth = false;
			};

			$scope.getHtml = function (html) {
				return $sce.trustAsHtml(html);
			};

			$scope.submitFactorCode = function () {
				showToast($scope.data.LOGIN_LOGIN_SUCCESS);
				$scope.disabled = true;

				let authentication = {
					Username: $scope.credentials.username,
					Password: $scope.credentials.password,
					Instance: $scope.credentials.domain,
					TwoFactorCode: $scope.credentials.twofactorcode.toString().replace(/\s/g, '')
				};

				HttpService.twofactor(authentication).then(function (response) {
					if (response.data === 'BasicAuthentication_Success') {
						reload();
					} else {
						let message = response.data === 'BasicAuthentication_SecondFactorFailed' ?
							$scope.data.LOGIN_LOGIN_FAILED_TWO_FACTOR : '';

						showToast(message);
						$scope.disabled = false;
					}
				});
			};

			$scope.submitKetoAuth = function () {
				HttpService.ketoauthphase1(
					{
						returnUrl: new URL(location.pathname, location.href).href,
						login: $scope.credentials.username
					}
				).then(function (response) {
					if (response.data.url === "") {
						let message = $scope.data.USER_KETOAUTH_ERROR;
						showToast(message);
						$scope.disabled = false;
					} else {
						document.location.href = response.data.url;
					}
				});
			}

			$scope.login = function () {
				if (!$scope.credentials.username || !$scope.credentials.password) {
					showToast($scope.data.LOGIN_FIELD_INCOMPLETE);
				} else {
					showToast($scope.data.LOGIN_LOGIN_SUCCESS);
					$scope.disabled = true;

					let authentication = {
						Username: $scope.credentials.username,
						Password: $scope.credentials.password,
						Instance: $scope.credentials.domain
					};

					HttpService.auth(authentication).then(function (response) {
						if (response.data === "BasicAuthentication_Success" ||
							response.data === "LDAPAuthentication_Success"
						) {
							reload();
						} else {
							let message = '';

							if (response.data === "BasicAuthentication_LoginEmpty") {
								message = $scope.data.LOGIN_LOGIN_FAILED_EMPTY_USERNAME;
							} else if (response.data === "BasicAuthentication_PasswordEmpty") {
								message = $scope.data.LOGIN_LOGIN_FAILED_EMPTY_PASSWORD;
							} else if (response.data === "Failed") {
								message = $scope.data.LOGIN_LOGIN_FAILED_INCORRECT_LOGIN;
							} else if (response.data === "BasicAuthentication_SecondFactorRequired") {
								$scope.secondfactor = true;
							} else if (response.data === "BasicAuthentication_SecondKetoAuthRequired") {
								$scope.ketoauth = true;
							} else if (response.data === "BasicAuthentication_LoginTriesExceeded") {
								message = $scope.data.LOGIN_FAILED_TRIES_EXCEEDED;
							}

							if (message) {
								showToast(message);
							}

							$scope.disabled = false;
						}
					});
				}
			};

			$scope.showAuth = function () {
				$scope.hidden = !$scope.hidden;
			};

			$scope.boxCount = function (ele) {
				let r = 0;
				if ($scope.data && ($scope.data.AzureADAuthentication === 'True' || $scope.data.SamlAuthentication === 'True' || $scope.data.GoogleAuthentication === 'True')) r += 1;
				if ($scope.data && $scope.data.BasicAuthentication === 'True' && !$scope.hidden) r += 1;
				if ($scope.data && $scope.data.addText !== "") r += 1;

				if (ele === 'class') return 'bc' + r;
				return '';
			};

			$scope.isIntra = function() {
				return document.location.href.toLowerCase().indexOf("intra.ketoapps.com") > 0;
			}

			$scope.checkPassword = function (pass) {
				if (pass.length < 6) return false;
				return /\d/.test(pass);
			};

			$scope.passwordResetMode = function (v) {
				$scope.changepassword = v;
			};

			$scope.passwordReset = function (m) {
				if (m === 0) {
					if (!$scope.credentials.username) {
						showToast($scope.data.LOGIN_FIELD_INCOMPLETE);
					} else {
						$scope.disabled = true;
						HttpService.reset($scope.credentials.username).then(function () {
							showToast($scope.data.LOGIN_RESET_DONE);
							$scope.passwordResetMode(0);
							$scope.disabled = false;
						});
					}
				} else if (m === 1) {
					if (!$scope.credentials.resetPassword || !$scope.credentials.resetRepassword) {
						showToast($scope.data.LOGIN_LOGIN_FAILED_EMPTY_PASSWORD);
					} else if ($scope.credentials.resetPassword != $scope.credentials.resetRepassword) {
						showToast($scope.data.LOGIN_PASSWORDS_NOT_SAME);
					} else {
						HttpService.change($scope.getQueryStringParameter('er'), $scope.credentials.resetPassword).then(function (response) {
							showToast($scope.data[response.data]);
							if (response.data === "LOGIN_PASSWORDS_CHANGED") $scope.passwordResetMode(0);
						});
					}
				}
			};

			$scope.ssologin = function () {
				var view = $scope.getQueryStringParameter('view');
				if (localStorage && view) localStorage.setItem("ssoView", view);

				if ($scope.data.AzureADAuthentication === 'True') {
					HttpService.azureurl().then(function (response) {
						document.location.href = response.data;
					});
				} else if ($scope.data.SamlAuthentication === 'True') {
					if ($scope.data.ssoUrl === "sso") {
						document.location.href = location.origin + location.pathname + 'sso' + location.search;
					} else {
						document.location.href = $scope.data.ssoUrl;
					}
				}
			};

			let reload = function () {
				$window.location.reload();
			};

			$scope.domains = [
				{
					name: 'Keto',
					domain: 'default'
				},
				{
					name: 'Improlity',
					domain: 'improlity'
				}
			];
		};

		HttpService.$inject = ['$http'];

		function HttpService($http) {
			return {
				config: function (domain) {
					return $http.get('v1/core/login/LoginConfiguration/' + domain);
				},
				twofactor: function (content) {
					return $http.post('v1/core/login/TwoFactor', content);
				},
				auth: function (content) {
					return $http.post('v1/core/login', content);
				},
				reset: function (login) {
					return $http.post('v1/core/login/RequestReset', '"' + login + '"');
				},
				change: function (token, password) {
					return $http.post('v1/core/login/CommitReset/' + token, '"' + encodeURI(password) + '"');
				},
				azureurl: function () {
					return $http.post('v1/core/login/AzureURL', {});
				},
				ketoauthphase1: function (payload) {
					return $http.post('v1/core/login/KetoAuthRequestPhase1', payload);
				}
			};
		}

		ViewRestoreService.$inject = ['$location']

		function ViewRestoreService($location){

			let path = $location.absUrl().split($location.host())[1];
			let identifier = path.match(/\/[a-zA-Z]+\//g);

			if (identifier === null) identifier = '/local/';

			function getPathKey(key) {
				return identifier + ':' + key;
			}

			this.save = function(key, value){
				if (localStorage) {
					let item = typeof value === 'object' ? angular.toJson(value) : value;
					localStorage.setItem(getPathKey(key), item);
				}
			}
		}


		ToastService.$inject = ['$sanitize'];

		function ToastService($sanitize) {
			let $toast;
			let $toastMessage;
			let toastTickInitial;
			let toastTickFinal;
			let toastTickTimeout;

			this.toast = function (params) {
				if ($toast) $toast.removeClass('slide-down');

				clearTimeout(toastTickInitial);
				clearTimeout(toastTickFinal);
				clearTimeout(toastTickTimeout);

				toastTickInitial = setTimeout(function () {
					if ($toast) $toast.remove();
					$toast = $('<toast class="align-row"></toast>').appendTo('body');
					$toastMessage = $('<span class="body-2">' + $sanitize(params.message) + '</span>').appendTo($toast);

					setTimeout(function () {
						$toast.addClass('slide-down');
					}, 75);

					toastTickTimeout = setTimeout(function () {
						$toast.removeClass('slide-down');
						toastTickFinal = setTimeout(function () {
							$toast.remove();
						}, 150);
					}, 7500);
				}, 150);
			};
		}
	}
)();
