﻿(function () {
	'use strict';

	angular
		.module('core')
		.config(DocumentationConfig)
		.controller('DocumentationController', DocumentationController);


	DocumentationConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
	function DocumentationConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('documentation', {
			controller: 'DocumentationController',
			template: 'documentation/documentation.html'
		});

		ViewConfiguratorProvider.addConfiguration('documentation', {
			onSetup: {
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				}
			},
			tabs: {
				default: {
					title: function () {
						return {
							type: 'string',
							label: 'DOCUMENTATION'
						}
					}
				}
			}
		});

		ViewsProvider.addPage('documentation', 'PAGE_DOCUMENTATION', ['read', 'write']);
	}

	DocumentationController.$inject = ['$scope', 'MessageService', 'ViewService', 'StateParameters'];
	function DocumentationController($scope, MessageService, ViewService, StateParameters) {

		ViewService.addDownloadable("VIEW_MISSING_FAIL", function(){
			alert(100);
		}, StateParameters.ViewId);
	}
})();
