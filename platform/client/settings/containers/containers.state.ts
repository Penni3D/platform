﻿(function () {
    'use strict';

    angular
        .module('core.settings')
        .config(ContainersConfig);

    ContainersConfig.$inject = ['ViewsProvider'];
    function ContainersConfig(ViewsProvider) {
        ViewsProvider.addView('settings.containers', {
            controller: 'UnLinkedElementsMainView',
            template: 'settings/containers/unLinkedElementMainView.html',
            resolve: {
                ProcessTabs: function (SettingsService, StateParameters, Translator) {
                    return SettingsService.ProcessGroupTabs(StateParameters.process).then(function (processGroups) {
                        return SettingsService.ProcessTabs(StateParameters.process).then(function (result) {
                            let tabs = [];
                            let unique = {};
                            angular.forEach(result, function (tab) {
                                if (!unique[tab.Variable]) {
                                    tabs.push(tab);
                                    unique[tab.Variable] = tab;
                                }

                                angular.forEach(processGroups, function (key) {
                                    if (key.ProcessTab.ProcessTabId == tab.ProcessTabId) {
                                        tab.Phase = key.ProcessGroup.Translation;
                                    }
                                });
                            });

                            if (tabs.length > 0) {
                                if (tabs[0].ProcessTabId != 0)
                                    tabs.splice(0, 0, { ProcessTabId: 0, Translation: Translator.translate('CONTAINERS_NOT_IN_PHASE'), Phase: Translator.translate('CONTAINERS_NOT_IN_PHASE') });
                            }
                            else {
                                tabs.push({ ProcessTabId: 0, Translation: Translator.translate('CONTAINERS_NOT_IN_PHASE'), Phase: Translator.translate('CONTAINERS_NOT_IN_PHASE') });
                            }

                            return tabs;

                        });
                    });
                },
                CreationContainers: function (SettingsService, StateParameters) {
                    return SettingsService.ProcessPortfolioDialog(StateParameters.process);
                },
                ProcessContainers: function (SettingsService, StateParameters, $filter, Translator) {
                    return SettingsService.ProcessContainers(StateParameters.process).then(function (data) {
                        return $filter('orderBy')(data, function (d) { return Translator.translation(data.LanguageTranslation)});
                    });
                }
            }
        });

        ViewsProvider.addView('settings.containers.tabs', {
            controller: 'ContainerColumnsController',
            template: 'settings/containers/containersTabs.html',
            resolve: {
                ProcessContainers: function (SettingsService, StateParameters) {
                    let result = {container: {}, subprocesses: [], processContainers: []};
                    let subprocesses = [];
                    SettingsService.ProcessTabSubprocessess(StateParameters.process, StateParameters.ProcessTabId).then(function (data) {
                        subprocesses = data;
                    });

                    let processContainers = [];
                       SettingsService.ProcessContainers(StateParameters.process).then(function (data) {
                           processContainers = data;
                       });
                    if (StateParameters.ProcessTabId > 0) {
                        let tabs;
                        let phases;

                        return SettingsService.ProcessTabContainers(StateParameters.process).then(function (data) {
                            tabs = data;
                            return SettingsService.ProcessGroupTabs(StateParameters.process).then(function (data) {
                                phases = data;
                                return SettingsService.ProcessTabContainers(StateParameters.process, StateParameters.ProcessTabId).then(function (containers) {
                                    let resultContainer = {};
                                    resultContainer['top'] = [];
                                    resultContainer['left'] = [];
                                    resultContainer['right'] = [];
                                    resultContainer['buttons'] = [];
                                    resultContainer['bottom'] = [];
                                    angular.forEach(containers, function (container) {
                                        container.phases = [];
                                        angular.forEach(tabs, function (tab) {
                                            if (tab.ProcessContainer.ProcessContainerId == container.ProcessContainer.ProcessContainerId) {
                                                let i = 0;
                                                angular.forEach(phases, function (phase) {
                                                    if (phase.ProcessTab.ProcessTabId == tab.ProcessTab.ProcessTabId) {
                                                        if (phase.ProcessGroup.Translation != null && phase.ProcessGroup.MaintenanceName.length > 0 && phase.ProcessTab.MaintenanceName.length > 0) {
                                                            i += 1;
                                                            container.phases.push(phase.ProcessGroup.Translation + " " + "'" + phase.ProcessGroup.MaintenanceName + "'" + " " + "-" + " " + tab.ProcessTab.Translation + " " + "'" + phase.ProcessTab.MaintenanceName + "'");
                                                        } else if (phase.ProcessGroup.Translation != null && phase.ProcessGroup.MaintenanceName.length == 0 && phase.ProcessTab.MaintenanceName.length > 0) {
                                                            i += 1;
                                                            container.phases.push(phase.ProcessGroup.Translation + " " + "-" + " " + tab.ProcessTab.Translation + " " + "'" + phase.ProcessTab.MaintenanceName + "'");
                                                        } else if (phase.ProcessGroup.Translation != null && phase.ProcessGroup.MaintenanceName.length > 0 && phase.ProcessTab.MaintenanceName.length == 0) {
                                                            i += 1;
                                                            container.phases.push(phase.ProcessGroup.Translation + " " + "'" + phase.ProcessGroup.MaintenanceName + "'" + "-" + " " + tab.ProcessTab.Translation);
                                                        }
                                                    }
                                                });
                                            }
                                        });

                                        if (container.ProcessContainerSide == 1) {
                                            resultContainer['left'].push(container);
                                        }
                                        else if (container.ProcessContainerSide == 2) {
                                            resultContainer['right'].push(container);
                                        }
                                        else if (container.ProcessContainerSide == 3) {
                                            resultContainer['buttons'].push(container);
                                        } else if (container.ProcessContainerSide == 4) {
                                            resultContainer['top'].push(container);
                                        } else if (container.ProcessContainerSide == 5) {
                                            resultContainer['bottom'].push(container);
                                        }
                                    });

                                    result.container = resultContainer;
                                    result.subprocesses = subprocesses;
                                    result.processContainers = processContainers;
                                    return result;
                                });
                            });
                        });
                    }
                    else {
                        return SettingsService.ProcessContainersNotInTabs(StateParameters.process).then(function (containers) {
                            let resultContainer = {};
                            resultContainer['left'] = [];
                            angular.forEach(containers, function (key) {
                                resultContainer['left'].push({ 'ProcessContainer': key, 'ProcessContainerSide': 1, 'ProcessTabContainerId': 0 });
                            });
                            result.container = resultContainer;
                            result.subprocesses = subprocesses;
                            return result;
                        });
                    }
                },
                SubQueries: function (ApiService) {
                    return ApiService.v1api('settings/RegisteredSubQueries/queryTypes').get();
                },
                ProcessContainerColumns: function (SettingsService, StateParameters, $rootScope) : Client.ContainerColumn {
                    return SettingsService.ProcessContainerColumns(StateParameters.process).then(function(colsByContainer){
                        _.each(colsByContainer, function(value){
                           _.each(value, function(column){
                               if(column.HelpTextTranslation != ''){
                                   let parsed = JSON.parse(column.HelpTextTranslation);
                                   if(column.HelpTextTranslation != null && parsed.hasOwnProperty($rootScope.clientInformation.locale_id)){
                                       column.$$toolTip = parsed;
                                   } else {
                                       column.$$toolTip = '';
                                   }
                               } else {
                                   column.$$toolTip = '';
                               }
                           });
                        });
                        return colsByContainer;
                    });
                },
                ItemColumnsData: function (SettingsService, StateParameters, Common, $filter) {
                    return SettingsService.ItemColumns(StateParameters.process).then(function (columns) {
                        return $filter('orderBy')(columns, 'Translation');
                    });
                },
                Lists: function (SettingsService, $filter, Translator) {
                    return SettingsService.ListProcesses().then(function (process) {
                        return $filter('orderBy')(process, function (p) { return Translator.translation(p.LanguageTranslation)});
                    });
                },
                Processes: function (ProcessesService, $filter, Translator) {
                    return ProcessesService.get().then(function (processes) {

                        return $filter('orderBy')(processes, function (process) { return Translator.translation(process.LanguageTranslation)});
                    });
                },
                ItemColumns: function (SettingsService, StateParameters, $rootScope, $filter, Views, $q, ProcessService, Translator) {
                    let promises = [];
                    promises.push(SettingsService.ItemColumnsInContainers(StateParameters.process));
                    promises.push(SettingsService.ProcessTabs(StateParameters.process));
                    promises.push(ProcessService.getProcessGroups(StateParameters.process, true));
                    return $q.all(promises).then(function (response) {

                        let columns = response[0];
                        let views = [];
                        views.push({ 'name': 'Frontpage', 'value': 'frontpage' });
                        views.push({ 'name': 'Page: Calendar', 'value': 'calendar' });

                        let tabs = response[1];
                        angular.forEach(_.uniqBy(tabs, 'ProcessTabId'), function (tab : Client.ProcessTab) {
                            let mName = tab.MaintenanceName ? " ' " + tab.MaintenanceName + " ' " : "";
                            views.push({ 'name': 'Tab: ' + Translator.translation(tab.LanguageTranslation) + mName, 'value': 'tab_' + tab.ProcessTabId });
                        });

                        let processFeatures = [];
                        angular.forEach(response[2], function (value) {
                            processFeatures.push(value.SelectedFeatures);

                        });

                        processFeatures = _.uniqBy(_.flatten(processFeatures), 'Feature');
                        angular.forEach(processFeatures, function (feature) {
                            if (feature.DefaultState && feature.DefaultState != "process.user") {
                                views.push({ 'name': 'Feature: ' + feature.Feature, 'value': feature.DefaultState });
                            }
                        });

                        views = $filter('orderBy')(views, 'name');

                        views.push({ 'name': 'Close', 'value': 'close' });
                        views.push({ 'name': 'Refresh', 'value': 'refresh' });
                        views.push({ 'name': 'Refresh all', 'value': 'refresh_all' });
                        views.push({ 'name': 'Nothing', 'value': 'nothing' });
                        views.push({ 'name': 'Goto new item', 'value': 'new_item' });
                        views.push({ 'name': 'Goto new item by replacing', 'value': 'new_item_replace' });
                        views.push({ 'name': 'Refresh all and invalidate cache', 'value': 'refresh_all_cache' });

                        let subProcessColumns = {};
                        let relativeColumns = [];
                        angular.forEach(columns, function (column) {
                            angular.forEach(column, function (col) {
                                if (col.DataType == 11) {

                                    SettingsService.ItemColumns(col.DataAdditional).then(function (data) {
                                        subProcessColumns[col.DataAdditional] = data;
                                    });
                                }

                                if (col.DataType == 5 && col.InputMethod == 0) {
                                    relativeColumns.push(col);
                                }
                            });
                        });

                        return {
                            'relativeColumns': relativeColumns,
                            'itemColumns': columns,
                            'subProcessColumns': subProcessColumns,
                            'views': views
                        };
                    });
                },

                Languages: function (NotificationService) {
                    return NotificationService.getLanguages();
                },
            }
        });
    }
})();