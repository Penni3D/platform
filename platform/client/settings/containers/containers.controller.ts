(function () {
	'use strict';

	angular
		.module('core.settings')
		.controller('UnLinkedElementsMainView', UnLinkedElementsMainView)
		.controller('ContainerColumnsController', ContainerColumnsController)
		.controller('ItemColumnsEditController', ItemColumnsEditController)
		.controller('ContainersAddNewColumnController', ContainersAddNewColumnController)
		.controller('ContainerColumnsValidationController', ContainerColumnsValidationController)
		.controller('HelpTextController', HelpTextController);


	UnLinkedElementsMainView.$inject = ['$scope', 'SettingsService', 'MessageService', 'Common', 'SaveService', 'StateParameters', 'ProcessTabs', 'ViewService', 'Translator', 'CreationContainers', 'ProcessContainers', '$rootScope'];

	function UnLinkedElementsMainView($scope, SettingsService, MessageService, Common, SaveService, StateParameters, ProcessTabs, ViewService, Translator, CreationContainers, ProcessContainers, $rootScope) {

		$scope.creationContainers = CreationContainers;
		$scope.selectedColumns = [];
		$scope.edit = true;
		$scope.fieldsNotInContainer = Translator.translate('FIELDS_NOT_IN_CONTAINER');
		$scope.tabsNotInPhase = Translator.translate('TABS_NOT_IN_PHASE');
		let newValuesArray = [];
		let changedItems = [];

		$scope.header = {
			title: Translator.translate('SETTINGS_VIEW_UNLINKED_ELEMENTS')
		};
		StateParameters.activeTitle = $scope.header.title;

		$scope.ProcessContainers = [];
		$scope.ProcessContainers = ProcessContainers;

		SaveService.subscribeSave($scope, function () {
			saveCreationContainers();
		});

		SettingsService.subscribeNewContainer($scope, function () {
			SettingsService.ProcessContainers(StateParameters.process).then(function (data) {
				$scope.ProcessContainers = data;
			});
		});

		let saveCreationContainers = function () {
			let CreationContainersSave = [];
			angular.forEach(changedItems, function (item) {
				if (angular.isDefined($scope.creationContainers[item.index].ProcessContainerId) && $scope.creationContainers[item.index].ProcessContainerId > 0) {
					CreationContainersSave.push($scope.creationContainers[item.index]);
				}
			});
			if (CreationContainersSave.length > 0) {
				SettingsService.ProcessPortfolioDialogSave(StateParameters.process, CreationContainersSave);
			}
			newValuesArray = [];
			changedItems = [];
		};

		$scope.deleteCreationContainer = function (containerId, index) {
			$scope.creationContainers.splice(index, 1);
			SettingsService.CreationContainerDelete(StateParameters.process, containerId);
		};

		$scope.addportfolioDialogColumns = function () {
			let beforeOrderNo = '';
			let afterOrderNo = '';
			angular.forEach($scope.creationContainers, function (column) {
				if (angular.isUndefined(beforeOrderNo)) {
					beforeOrderNo = column.OrderNo;
				} else if (beforeOrderNo < column.OrderNo) {
					beforeOrderNo = column.OrderNo;
				}
			});

			let max_order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			let newCreationCont = {OrderNo: "", LanguageTranslation: {}, ProcessContainerId: 0};
			newCreationCont.OrderNo = max_order_no;
			newCreationCont.LanguageTranslation = {};
			newCreationCont.LanguageTranslation[$rootScope.clientInformation.locale_id] = '';
			if ($scope.ProcessContainers.length > 0) {
				newCreationCont.ProcessContainerId = $scope.ProcessContainers[0].ProcessContainerId;
				$scope.creationContainers.push(newCreationCont);
				SettingsService.ProcessPortfolioDialogAdd(StateParameters.process, [newCreationCont]).then(function (data) {
					angular.forEach(data, function (key) {
						let index = Common.getIndexOf($scope.creationContainers, key.ProcessContainerId, 'ProcessContainerId');
						$scope.creationContainers[index] = key;
					});
				});
			} else {
				MessageService.toast('NO_CONTAINERS_IN_PROCESS', 'warn');
			}
		};

		$scope.syncOrder = function (current, previous, next) {
			let beforeOrderNo;
			let afterOrderNo;

			if (angular.isDefined(previous)) {
				beforeOrderNo = previous.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}

			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			let i = $scope.creationContainers.length;
			while (i--) {
				if ($scope.creationContainers[i].ProcessPortfolioDialogId == current.ProcessPortfolioDialogId) {
					$scope.changeValue({
						id: current.ProcessPortfolioDialogId,
						index: i,
						containerId: current.ProcessContainerId
					});
					break;
				}
			}
			saveCreationContainers();
		};

		$scope.changeValue = function (params) {
			newValuesArray.push({id: params.id, index: params.index, containerId: params.containerId});
			let noDuplicateObjects = {};
			_.each(newValuesArray, function (item) {
				noDuplicateObjects[item.id + '_' + item.index] = item;
			});

			let nonDuplicateArray = [];
			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};

		let changedGroupTabArray = [];
		changedItems['tab'] = [];
		$scope.edit = true;
		$scope.tabs = ProcessTabs;
		$scope.selectedTabHighlight = null;
		$scope.selectedRows = [];

		SaveService.subscribeSave($scope, function () {
			saveContainers();
		});

		let saveContainers = function () {
			let processTabsArray = [];
			angular.forEach(changedItems['tab'], function (tab) {
				let processTabsArray = [];
				let index = Common.getIndexOf($scope.tabs, tab.ProcessTabId, 'ProcessTabId');
				processTabsArray.push($scope.tabs[index]);
				SettingsService.ProcessTabsSave(StateParameters.process, processTabsArray);
			});

			if (processTabsArray.length > 0) {
				SettingsService.ProcessContainerColumnsSave(StateParameters.process, processTabsArray);
			}

			newValuesArray = [];
			changedItems['tab'] = [];
		};

		$scope.changeGroupTabValue = function (params) {
			changedGroupTabArray.push({ProcessTabId: params.ProcessTabId});
			let noDuplicateObjects = {};
			let l = changedGroupTabArray.length;

			for (let i = 0; i < l; i++) {
				let item = changedGroupTabArray[i];
				noDuplicateObjects[item.ProcessTabId] = item;
			}

			let nonDuplicateArray = [];
			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems['tab'] = nonDuplicateArray;
		};

		$scope.showContainers = function (ProcessTabId, index) {
			$scope.selectedTabHighlight = index;
			$scope.ProcessTabId = ProcessTabId;
			setTimeout(function () {
				ViewService.viewAdvanced('settings.containers.tabs', {
					params: {
						process: StateParameters.process,
						ProcessTabId: ProcessTabId
					}
				}, {targetId: 'notInUseView', ParentViewId: StateParameters.ViewId});
			}, 0);
		};
		if (StateParameters.fromContainerSearch) {
			$scope.showContainers(0, 0);
		}

		$scope.goToFields = function (ProcessTabId, index) {
			$scope.selectedTabHighlight = index;
			$scope.ProcessTabId = ProcessTabId;
			setTimeout(function () {
				ViewService.viewAdvanced('settings.fields.tabs', {
					params: {
						process: StateParameters.process,
						ProcessTabId: 0
					}
				}, {targetId: 'notInUseView', ParentViewId: StateParameters.ViewId});
			}, 0);
		};
		if (StateParameters.fromFieldSearch) {
			$scope.goToFields(0, 0);
		}

		$scope.showTabs = function () {
			setTimeout(function () {
				ViewService.viewAdvanced('settings.tabs', {
					params: {
						process: StateParameters.process
					}
				}, {targetId: 'notInUseView', ParentViewId: StateParameters.ViewId});
			}, 0);
		};
		if (StateParameters.fromTabSearch) {
			$scope.showTabs();
		}
	}

	ContainerColumnsController.$inject = ['$scope',
		'SaveService',
		'ProcessContainers',
		'ProcessContainerColumns',
		'ItemColumnsData',
		'MessageService',
		'StateParameters',
		'Common',
		'ItemColumnService',
		'ViewService',
		'Lists',
		'Processes',
		'SettingsService',
		'$rootScope',
		'Translator',
		'Languages',
		'SubQueries'
	];

	function ContainerColumnsController(
		$scope,
		SaveService,
		ProcessContainers,
		ProcessContainerColumns: Array<Client.ContainerColumn>,
		ItemColumnsData: Array<Client.ItemColumn>,
		MessageService,
		StateParameters,
		Common,
		ItemColumnService,
		ViewService,
		Lists,
		Processes,
		SettingsService,
		$rootScope,
		Translator,
		Languages,
		SubQueries
	) {

		$scope.mylang = $rootScope.clientInformation.locale_id;
		let originalProcess = angular.copy(StateParameters.process);

		$scope.showThisContainerConditions = function (containerId, translation) {
			ViewService.view('debug.settings', {
				params: {
					process: StateParameters.process,
					selectedUiObjectId: containerId,
					selectedUiObjectType: 'ProcessContainerIds',
					translation: Translator.translation(translation),
					noConditions: 'NO_CONTAINER_CONDITIONS',
					title: 'CONTAINER_CONDITIONS',
					ownKey: 'containerIds'
				}
			});
		};

		$scope.columns = [
			{name: 'LEFT', value: 'left'},
			{name: 'RIGHT', value: 'right'},
			{name: 'BUTTONS', value: 'buttons'},
			{name: 'TOP', value: 'top'},
			{value: 'bottom', name: 'BOTTOM'}
		];

		let changedContColumns = [];
		let changedItemColumns = [];
		$scope.columnConnector = {};

		let originalMaintenanceNames = [];

		let isThisSublist = function (listName) {
			let answer = false;
			for (let i = 0; i < Lists.length; i++) {
				if (_.includes(Lists[i].SubProcesses, listName)) {
					answer = true;
				}
			}
			return answer;
		};

		$scope.showList = function (listName) {
			let myList;
			for (let i = 0; i < Lists.length; i++) {
				if (Lists[i].ProcessName == listName) {
					myList = Lists[i];
					break;
				}
			}

			let thisList2 = {Process: {}};
			StateParameters.Recursive = myList.ProcessType;
			StateParameters.process = myList.ProcessName;
			StateParameters.listTranslation = myList.LanguageTranslation[$rootScope.clientInformation.locale_id];
			StateParameters.adminPage = true;
			StateParameters.orderable = myList.ListOrder;
			StateParameters.thisList = thisList2;
			StateParameters.thisList.Process = myList;
			StateParameters.thisList.Process.isSublist = isThisSublist(myList.ProcessName);
			ViewService.view('listsEdit', {params: StateParameters}, {split: true});

		};

		$scope.processContainerColumns = ProcessContainerColumns;

		_.each($scope.processContainerColumns, function (containers) {
			_.each(containers, function (cc) {
				let cols = {ItemColumnId: null, DataType: null, Translation: '', DataTypeName: ''};
				cols.ItemColumnId = cc.ItemColumn.ItemColumnId;
				cols.DataType = cc.ItemColumn.DataType;


				if (cc.ItemColumn.LanguageTranslationRaw.hasOwnProperty($scope.mylang)) {
					cols.Translation = cc.ItemColumn.LanguageTranslationRaw;
				} else {
					let transCopy = angular.copy(cc.ItemColumn.LanguageTranslationRaw);
					transCopy[$scope.mylang] = cc.ItemColumn.Name;
					cols.Translation = transCopy;
				}


				cols.DataTypeName = Common.getDataTypeTranslation(cc.ItemColumn.DataType);
				$scope.columnConnector[cc.ProcessContainerColumnId] = cols;
			});
		});

		$scope.containers = ProcessContainers.container;
		$scope.processContainers = ProcessContainers.processContainers;
		$scope.itemColumns = ItemColumnsData;
		$scope.userLanguage = $rootScope.clientInformation.locale_id;

		$scope.ProcessTabId = StateParameters.ProcessTabId;
		$scope.dataTypes = Common.getDataTypes();

		let combineMaintenanceNames = function () {
			originalMaintenanceNames = angular.copy($scope.processContainers);
			angular.forEach($scope.processContainers, function (value) {
				if (value.MaintenanceName.length == 0) {
					value.MaintenanceName = Translator.translation(value.LanguageTranslation);
				} else {
					value.MaintenanceName = Translator.translation(value.LanguageTranslation) + ' ' + '\'' + value.MaintenanceName + '\'';
				}
			});
		};
		combineMaintenanceNames();

		SaveService.subscribeSave($scope, function () {
			if (changedContColumns.length > 0) {
				ItemColumnService.saveContainerColumn(StateParameters.process, changedContColumns).then(function () {
					changedContColumns = [];
				});
			}
			if (changedItemColumns.length > 0) {
				ItemColumnService.saveItemColumn(StateParameters.process, changedItemColumns).then(function () {
					changedItemColumns = [];
				});
			}
		});

		let checkTranslationLength = function (transObject) {
			if (transObject[$scope.userLanguage]) {
				let translation = transObject[$scope.userLanguage];
				if (translation.length > 0) {
					return true;
				}
			}
		};

		let newContainer = {
			LanguageTranslation: {},
			ProcessContainerSide: null,
			identifier: 'new',
			RightContainer: false
		};

		// let rightContainer

		$scope.addContainer = function (side) {

			let save = function () {
				newContainerSave();
			};

			let modifications = {
				create: false,
				cancel: true
			};
			let t = false;

			let onChange = function () {
				if (newContainer.LanguageTranslation) {
					t = checkTranslationLength(newContainer.LanguageTranslation);
				}
				if ((newContainer.ProcessContainerSide != null && t) || StateParameters.ProcessTabId == 0) {
					modifications.create = true;
				}
			};

			let option = [{value: 1, name: 'LEFT'},
				{value: 2, name: 'RIGHT'},
				{value: 3, name: 'BUTTONS'},
				{value: 4, name: 'TOP'},
				{value: 5, name: 'BOTTOM'}];

			let hit = _.find(option, function (p) {
				return p.name == side.name
			}).value;

			if (typeof hit != 'undefined') newContainer.ProcessContainerSide = hit;

			let inputs;

			if ($scope.ProcessTabId > 0) {
				inputs = [{
					type: 'translation',
					label: 'PROCESS_CONTAINER_NAME',
					model: 'LanguageTranslation'
				}, {
					type: 'select',
					label: 'COLUMNS',
					model: 'ProcessContainerSide',
					options: option,
					optionName: 'name',
					optionValue: 'value',
					singleValue: true
				}];
			} else {
				inputs = [{type: 'translation', label: 'PROCESS_CONTAINER_NAME', model: 'LanguageTranslation'},
					{type: 'checkbox', label: 'RIGHT_CONTAINER', model: 'RightContainer'}
				];
			}

			let content = {
				buttons: [{modify: 'cancel', text: 'CANCEL'}, {
					type: 'primary',
					onClick: save,
					text: 'SAVE',
					modify: 'create'

				}],
				inputs: inputs,
				title: 'ADD_NEW_CONTAINER'
			};

			MessageService.dialog(content, newContainer, onChange, modifications)
		};

		let copiedContainer = {processContainerId: null, ProcessContainerSide: 1, identifier: 'copy'};

		$scope.copyContainer = function (column, copyType) {
			let containersToCopy = [];
			let tt = "";

			if (copyType == 'existing') {
				tt = 'ADD_EXISTING_CONTAINER';
				angular.forEach($scope.processContainers, function (container) {
					let exists = false;
					angular.forEach($scope.containers, function (side) {
						angular.forEach(side, function (value) {
							if (value.ProcessContainer.ProcessContainerId == container.ProcessContainerId) {
								exists = true;
							}
						});
					});
					if (!exists) {
						containersToCopy.push(container);
					}
				});
			} else {
				containersToCopy = $scope.processContainers;
				tt = 'CLONE_CONTAINER';
			}

			let modifications = {
				create: false,
				cancel: true
			};

			let onChange = function () {
				if (copiedContainer.processContainerId != null) {
					modifications.create = true;
				}
			};

			let save = function () {
				copyContainerSave(copyType);
			};
			let option = [{value: 1, name: 'LEFT'},
				{value: 2, name: 'RIGHT'},
				{value: 3, name: 'BUTTONS'},
				{value: 4, name: 'TOP'},
				{value: 5, name: 'BOTTOM'}];

			let hit = _.find(option, function (p) {
				return p.name == column.name
			}).value;

			if (typeof hit != 'undefined') copiedContainer.ProcessContainerSide = hit;

			let content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE', modify: 'create'}],
				inputs: [{
					type: 'select',
					label: 'CONTAINER',
					model: 'processContainerId',
					options: containersToCopy,
					optionName: 'MaintenanceName',
					optionValue: 'ProcessContainerId',
					singleValue: true
				}, {
					type: 'select',
					label: 'COLUMNS',
					model: 'ProcessContainerSide',
					options: option,
					optionName: 'name',
					optionValue: 'value',
					singleValue: true
				}],
				title: tt
			};

			MessageService.dialog(content, copiedContainer, onChange, modifications)
		};

		let getColumnSide = function (side) {
			let column;
			if (side == 1) {
				column = 'left';
			} else if (side == 2) {
				column = 'right';
			} else if (side == 3) {
				column = 'buttons';
			} else if (side == 4) {
				column = 'top';
			} else if (side == 5) {
				column = 'bottom';
			}
			return column;
		};

		let newContainerSave = function () {
			let columnSide = getColumnSide(newContainer.ProcessContainerSide);
			let max_order_no = SettingsService.giveOrderNumber($scope.containers, columnSide);

			let container: Client.ProcessContainer = {
				LanguageTranslation: newContainer.LanguageTranslation,
				MaintenanceName: '',
				ProcessContainerId: 0
			};

			let tab: Client.ProcessTab = {
				ProcessTabId: $scope.ProcessTabId
			};

			let tabContainer: Client.TabContainer = {
				OrderNo: max_order_no,
				ProcessTabContainerId: 0,
				ProcessTab: tab,
				ProcessContainerSide: newContainer.ProcessContainerSide,
				ProcessContainer: container
			};

			if (newContainer.RightContainer == true) {
				container.RightContainer = 1;
			}

			SettingsService.ProcessContainersAdd(originalProcess, container).then(function (cont: Client.ProcessContainer) {
				tabContainer.ProcessContainer = cont;
				tabContainer.OrderNo = max_order_no;
				if ($scope.ProcessTabId > 0) {
					SettingsService.ProcessTabContainersAdd(originalProcess, tabContainer).then(function (cont_: Client.TabContainer) {
						if (angular.isUndefined($scope.containers[columnSide])) {
							$scope.containers[columnSide] = [];
						}
						$scope.containers[columnSide].push(cont_);
					});
				} else {
					$scope.containers['left'].push({
						'ProcessContainer': cont,
						'ProcessContainerSide': 1,
						'ProcessTabContainerId': 0
					});
				}
				newContainer = {
					LanguageTranslation: {},
					ProcessContainerSide: null,
					identifier: 'new',
					RightContainer: false
				};
			});
		};

		let copyContainerSave = function (copyType) {
			let columnSide = getColumnSide(copiedContainer.ProcessContainerSide);
			let max_order_no = SettingsService.giveOrderNumber($scope.containers, columnSide);

			let container: Client.ProcessContainer = {
				ProcessContainerId: parseFloat(copiedContainer.processContainerId)
			};

			let tab: Client.ProcessTab = {
				ProcessTabId: $scope.ProcessTabId
			};

			let newContainerId;

			if (copyType == 'existing') {
				let tabContainer: Client.TabContainer = {
					OrderNo: max_order_no,
					ProcessTabContainerId: 0,
					ProcessTab: tab,
					ProcessContainerSide: copiedContainer.ProcessContainerSide,
					ProcessContainer: container
				};

				SettingsService.ProcessTabContainersAdd(originalProcess, tabContainer).then(function (newContainer: Client.TabContainer) {
					let index = Common.getIndexOf($scope.processContainers, copiedContainer.processContainerId, 'ProcessContainerId');
					newContainer.ProcessContainer = $scope.processContainers[index];
					newContainer.ProcessContainer.MaintenanceName = originalMaintenanceNames[index].MaintenanceName;
					$scope.containers[columnSide].push(newContainer);
					newContainerId = newContainer.ProcessContainer.ProcessContainerId;
					SettingsService.ProcessContainerColumns(originalProcess, copiedContainer.processContainerId).then(function (column) {
						$scope.processContainerColumns[newContainerId] = column;
					});
				});
			} else {
				let containerTranslation = _.find($scope.processContainers, function (c) {
					return c.ProcessContainerId == container.ProcessContainerId
				}).LanguageTranslation[$rootScope.clientInformation.locale_id];
				let tabContainer = {
					from_process_container_id: container.ProcessContainerId,
					to_process_tab_id: $scope.ProcessTabId,
					from_process_tab_id: $scope.ProcessTabId,
					to_container_side: copiedContainer.ProcessContainerSide,
					to_order_no: max_order_no,
					translation: containerTranslation
				};
				SettingsService.ProcessContainerCopy(StateParameters.process, tabContainer).then(function (cloned) {
					let temporary = {ProcessContainer: {}};
					temporary.ProcessContainer = cloned;
					$scope.containers[columnSide].push(temporary);
					newContainerId = cloned.ProcessContainerId;
					SettingsService.ProcessContainerColumns(StateParameters.process, copiedContainer.processContainerId).then(function (column) {
						$scope.processContainerColumns[newContainerId] = column;
					});
				});
			}
		};

		let nameData = {MaintenanceName: "", LanguageTranslation: null, RightContainerOld: false, HideTitle: [0]};
		$scope.changeContainerName = function (container, column, ProcessTabId) {
			nameData = {
				LanguageTranslation: container.ProcessContainer.LanguageTranslation,
				MaintenanceName: container.ProcessContainer.MaintenanceName,
				RightContainerOld: container.ProcessContainer.RightContainer,
				HideTitle: [container.ProcessContainer.HideTitle]
			};

			let save = function () {
				return saveContainerName(container);
			};

			let del = function () {
				if (ProcessTabId > 0 && container.ProcessContainer.ProcessContainerId > 0) {
					$scope.deleteProcessContainerFromTab(container.ProcessTabContainerId, column);
				}
				if (ProcessTabId == 0 && container.ProcessContainer.ProcessContainerId > 0) {
					$scope.deleteProcessContainer(container.ProcessContainer.ProcessContainerId);
				}
			};

			let HideTitles = [{value: 0, name: 'PROCESS_CONTAINER_HIDE_TITLE_SHOW'},
				{value: 1, name: 'PROCESS_CONTAINER_HIDE_TITLE_HIDE'},
				{value: 2, name: 'PROCESS_CONTAINER_HIDE_TITLE_HIDE_BUT_KEEP_SPACE'}];

			let content = {
				buttons: [
					{text: "DELETE", type: 'warn', onClick: del},
					{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{type: 'translation', label: 'PROCESS_CONTAINER_NAME', model: 'LanguageTranslation'},
					{type: 'string', label: 'MAINTENANCE_NAME', model: 'MaintenanceName'},
					{
						type: 'select',
						label: 'PROCESS_CONTAINER_HIDE_TITLE',
						model: 'HideTitle',
						options: HideTitles,
						optionName: 'name',
						optionValue: 'value'
					}
				],
				title: 'CHANGE_CONTAINER_NAME'
			};

			if (ProcessTabId == 0) {
				content.inputs.push({type: 'checkbox', label: 'RIGHT_CONTAINER', model: 'RightContainerOld'});
			}

			MessageService.dialog(content, nameData)
		};

		let saveContainerName = function (container) {
			container.ProcessContainer.MaintenanceName = nameData.MaintenanceName;
			container.ProcessContainer.LanguageTranslation = nameData.LanguageTranslation;
			container.ProcessContainer.RightContainer = Number(nameData.RightContainerOld);
			container.ProcessContainer.HideTitle = Number(nameData.HideTitle);
			nameData = {MaintenanceName: "", LanguageTranslation: null, RightContainerOld: false, HideTitle: [0]};
			return SettingsService.ProcessContainersSave(StateParameters.process, [container.ProcessContainer]);
		};

		$scope.deleteProcessContainer = function (ProcessContainerId) {
			MessageService.confirm('DELETE_CONFIRMATION_FINAL', function () {
				SettingsService.ProcessContainersDelete(StateParameters.process, ProcessContainerId).then(function () {
					let index = Common.getIndexOf($scope.containers['left'], ProcessContainerId, 'ProcessContainerId', 'ProcessContainer');
					$scope.containers['left'].splice(index, 1);
				});
			});
		};

		$scope.deleteProcessContainerFromTab = function (ProcessTabContainerId, column) {
			MessageService.confirm('CONTAINER_DISCONNECT', function () {
				SettingsService.ProcessTabContainersDelete(StateParameters.process, ProcessTabContainerId).then(function () {
					let index = Common.getIndexOf($scope.containers[column], ProcessTabContainerId, 'ProcessTabContainerId');
					$scope.containers[column].splice(index, 1);
				});
			});
		};

		$scope.changeOrderField = function (current, prev, next, parent) {
			let beforeOrderNo;
			let afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}

			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			current.ProcessContainerId = parseInt(parent);
			SettingsService.ProcessContainerColumnsSave(StateParameters.process, [current]);
		};

		$scope.changeOrderContainer = function (current, prev, next, newParent) {
			let beforeOrderNo;
			let afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}
			if (!current.ProcessTab) {
				current.ProcessTab = {};
			}

			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			current.ProcessContainerSide = parseInt(newParent);
			SettingsService.ProcessTabContainersSave(StateParameters.process, current);
		};

		$scope.deleteField = function (fieldItem, ProcessContainerId, ProcessContainerColumnId) {
			MessageService.confirm('FIELD_DISCONNECT', function () {
				let index = Common.getIndexOf($scope.processContainerColumns[ProcessContainerId], ProcessContainerColumnId, 'ProcessContainerColumnId');
				$scope.processContainerColumns[ProcessContainerId].splice(index, 1);
				SettingsService.ProcessContainerColumnsDelete(StateParameters.process, ProcessContainerColumnId);
			}, 'warn');
		};

		let updateConnection = function (itemColumnId, containerColumnId, dataType, Variable) {
			let newValues = {ItemColumnId: null, DataType: null, Translation: '', DataTypeName: ''};
			newValues.ItemColumnId = itemColumnId;
			newValues.DataType = dataType;
			newValues.Translation = Variable;
			newValues.DataTypeName = Common.getDataTypeTranslation(dataType);
			$scope.columnConnector[containerColumnId] = newValues;
		};

		StateParameters.$$originalProcess = angular.copy(StateParameters.process);

		$scope.addProcessContainerColumn = function (ProcessContainerId) {

			let max_order_no = SettingsService.giveOrderNumber($scope.processContainerColumns, ProcessContainerId);

			let parameters = {
				'StateParameters': StateParameters,
				'ProcessContainerId': ProcessContainerId,
				'Process': StateParameters.process,
				'OrderNo': max_order_no,
				'ViewId': StateParameters.ViewId
			};

			let params = {
				template: 'settings/containers/itemColumnAddDialog.html',
				controller: 'ContainersAddNewColumnController',
				locals: {
					DialogParameters: parameters,
					ItemColumnsData: ItemColumnsData,
					Lists: Lists,
					Processes: Processes,
					ProcessContainerColumns: $scope.processContainerColumns,
					UnlinkedColumns: [],
					UpdateConnection: updateConnection,
					SubQueries: SubQueries
				},
				wider: true
			};
			MessageService.dialogAdvanced(params);
		};

		$scope.showProperties = function (itemColumn_id, ProcessContainerId, column, processContainerColumnId) {
			let itemColumn = $scope.itemColumns[Common.getIndexOf($scope.itemColumns, itemColumn_id, "ItemColumnId")];
			let parameters = {
				'ProcessContainerId': ProcessContainerId,
				'ProcessContainerColumnId': processContainerColumnId,
				'itemColumn': itemColumn,
				'column': column,
				'process': StateParameters.process
			};

			let params = {
				params: parameters,
				locals: {
					DialogParameters: parameters,
					Lists: Lists,
					Processes: Processes,
					UpdateConnection: updateConnection
				}
			};
			ViewService.view('settings.fields.properties', params, {split: true});
		};


		//This function handles case when item column in container column is changed or when container column specific settings (like validation) is changed
		$scope.modifyContainerColumn = function (_parameters) {
			if (_parameters.NewItemColumnId) {
				let index = Common.getIndexOf($scope.itemColumns, _parameters.NewItemColumnId, 'ItemColumnId');
				$scope.columnConnector[_parameters.ProcessContainerColumn.ProcessContainerColumnId].ItemColumnId = $scope.itemColumns[index].ItemColumnId;
				$scope.columnConnector[_parameters.ProcessContainerColumn.ProcessContainerColumnId].DataType = $scope.itemColumns[index].DataType;
				_parameters.ProcessContainerColumn.ItemColumn = $scope.itemColumns[index];
			}
			if (Common.getIndexOf(changedContColumns, _parameters.ProcessContainerColumn.ProcessContainerColumnId, 'ProcessContainerColumnId') == undefined) {
				changedContColumns.push(_parameters.ProcessContainerColumn);
			}
			SaveService.tick();
		};

		//This function handles case when 'universal item column settings' (like status column) is changed
		$scope.modifyItemColumn = function (params) {
			let itemColumn = params.ProcessContainerColumn.ItemColumn;
			if (Common.getIndexOf(changedItemColumns, itemColumn.ItemColumnId, 'ItemColumnId') == undefined) {
				changedItemColumns.push(itemColumn);
			}
			SaveService.tick();
		};

		$scope.editValidation = function (processContainerColumn, values, ProcessContainerId) {
			processContainerColumn.ItemColumn = _.find($scope.itemColumns, function (changedItemColumn) {
				return changedItemColumn.ItemColumnId == processContainerColumn.ItemColumn.ItemColumnId
			}); //we do this because otherwise itemcolumns objects might be in missync from right side editing
			let dialParameters = {
				'ProcessContainerColumn': processContainerColumn,
				'ProcessContainerId': ProcessContainerId
			};

			let itemColumn = $scope.itemColumns[Common.getIndexOf($scope.itemColumns, processContainerColumn.ItemColumn.ItemColumnId, "ItemColumnId")];

			let params = {
				template: 'settings/containers/containerColumnsValidation.html',
				controller: 'ContainerColumnsValidationController',
				locals: {
					DialogParameters: dialParameters,
					ProcessContainerColumns: $scope.processContainerColumns,
					StateParameters: StateParameters,
					ItemColumn: itemColumn
				}
			};

			MessageService.dialogAdvanced(params);
		};


		$scope.editHelpText = function (processContainerColumn, ProcessContainerId) {
			let dialParameters = {
				'ProcessContainerColumn': processContainerColumn,
				'ProcessContainerId': ProcessContainerId,
				'ModifyContainerColumn': $scope.modifyContainerColumn
			};

			let params = {
				template: 'settings/containers/helpTextEdit.html',
				controller: 'HelpTextController',
				locals: {
					DialogParameters: dialParameters,
					StateParameters: StateParameters,
					Languages: Languages
				}
			};
			MessageService.dialogAdvanced(params);
		};
	}

	HelpTextController.$inject = ['$scope',
		'DialogParameters',
		'Translator',
		'StateParameters',
		'ItemColumnService',
		'MessageService',
		'$rootScope',
		'Languages'
	];

	function HelpTextController(
		$scope,
		DialogParameters,
		Translator,
		StateParameters,
		ItemColumnService,
		MessageService,
		$rootScope,
		Languages
	) {

		$scope.selectedLanguage = $rootScope.clientInformation.locale_id;
		$scope.helpText = JSON.parse(DialogParameters.ProcessContainerColumn.HelpTextTranslation);
		$scope.languages = Languages;

		if ($scope.helpText === null) {
			$scope.helpText = {};
		}

		let i = Languages.length;
		while (i--) {
			let l = Languages[i].value;

			$scope.helpText[l] = typeof $scope.helpText[l] === 'undefined' ?
				{html: '', delta: undefined} : angular.fromJson($scope.helpText[l]);
		}

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.save = function () {
			DialogParameters.ProcessContainerColumn.$$toolTip = $scope.helpText;
			DialogParameters.ProcessContainerColumn.HelpTextTranslation = JSON.stringify($scope.helpText);
			DialogParameters.ModifyContainerColumn({'ProcessContainerColumn': DialogParameters.ProcessContainerColumn});

			MessageService.closeDialog();
		}
	}

	ContainerColumnsValidationController.$inject = ['$scope',
		'DialogParameters',
		'Translator',
		'StateParameters',
		'ItemColumnService',
		'MessageService',
		'ProcessContainerColumns',
		'ItemColumn'
	];

	function ContainerColumnsValidationController(
		$scope,
		DialogParameters,
		Translator,
		StateParameters,
		ItemColumnService,
		MessageService,
		ProcessContainerColumns,
		ItemColumn
	) {

		let emptyValidation = {
			AfterMonths: null,
			BeforeMonths: null,
			DateRange: null,
			CalendarConnection: null,
			Label: "{}",
			Min: null,
			Max: null,
			ParentId: null,
			Precision: null,
			ReadOnly: null,
			Required: null,
			Rows: null,
			Spellcheck: null,
			Suffix: null,
			Unselectable: null
		};

		$scope.parameters = DialogParameters;
		$scope.containerColumn = $scope.parameters.ProcessContainerColumn;
		if ($scope.containerColumn.Validation == null) $scope.containerColumn.Validation = emptyValidation;
		$scope.validation = $scope.containerColumn.Validation;
		$scope.columnDataType = $scope.containerColumn.ItemColumn.DataType;
		$scope.subDataType = $scope.containerColumn.ItemColumn.SubDataType;
		$scope.dialogTranslation = Translator.translation($scope.containerColumn.ItemColumn.LanguageTranslationRaw);
		$scope.label;
		$scope.labelItemColumn;
		$scope.itemColumnValidation = ItemColumn.Validation;
		$scope.fromParentValidation = false;
		$scope.oneMandatory;
		$scope.matrixScoreType;
		$scope.matrixScoreColumn;
		$scope.showComment;

		let matrixOptions = null;

		let checkIfMatrixOptions = function () {
			if ($scope.columnDataType == 22) {
				if ($scope.containerColumn.ItemColumn.DataAdditional2 != null) {
					matrixOptions = JSON.parse($scope.containerColumn.ItemColumn.DataAdditional2);
					if (matrixOptions.hasOwnProperty('OnlyOneMandatory')) $scope.oneMandatory = matrixOptions.OnlyOneMandatory;
					$scope.matrixScoreType = matrixOptions.Type;
					$scope.matrixScoreColumn = matrixOptions.ItemColumnId;
					$scope.showComment = matrixOptions.ShowComment;
					$scope.showLastModified = matrixOptions.ShowLastModified;
					$scope.hideScore = matrixOptions.HideScore;
					$scope.hideHeader = matrixOptions.HideHeader;
				}
			}
		};
		checkIfMatrixOptions();

		$scope.atLeastOneMandatory = function () {
			let matrixValues = {
				'Type': $scope.matrixScoreType,
				'ItemColumnId': $scope.matrixScoreColumn,
				'ShowComment': $scope.showComment,
				'OnlyOneMandatory': $scope.oneMandatory
			};
			$scope.containerColumn.ItemColumn.DataAdditional2 = JSON.stringify(matrixValues);
			icChanged = true;
		}

		let validationWhileCanceled = angular.copy($scope.validation);

		$scope.readOnlyRequired = [{name: 'PORTFOLIO_GROUP_UNSPECIFIED', value: null}, {
			name: 'FALSE',
			value: false
		}, {name: 'TRUE', value: true}];


		$scope.backToDefaultValidation = function () {
			// MessageService.confirm('DELETE_CONFIRMATION_FINAL', function () {
			$scope.containerColumn.Validation = emptyValidation;
			$scope.validation = emptyValidation;
			//   });

		};

		let icChanged = false;


		$scope.itemColumnChanged = function () {
			icChanged = true;
		};

		//validation tarkastelu -blokki
		$scope.needsMinValidation = false;
		$scope.needsMaxValidation = false;

		if ($scope.columnDataType == 0 ||  // nvarchar
			$scope.columnDataType == 1 ||  // int
			$scope.columnDataType == 2 ||  // float
			$scope.columnDataType == 16 || // subquery
			$scope.columnDataType == 20    // equation
		) $scope.needsMinValidation = true;


		if ($scope.columnDataType != 3 &&  // date
			$scope.columnDataType != 7 &&  // password
			$scope.columnDataType != 8 &&  // button
			$scope.columnDataType != 9 &&  // rating
			$scope.columnDataType != 10 && // attachment
			$scope.columnDataType != 11 && // join
			$scope.columnDataType != 13 && // datetime
			$scope.columnDataType != 15 && // link
			$scope.columnDataType != 18 && // portfolio
			$scope.columnDataType != 21 && // lite manager
			$scope.columnDataType != 22 && // matrix
			$scope.columnDataType != 60 && // progress
			$scope.columnDataType != 65    // milestone
		) $scope.needsMaxValidation = true;

		if ($scope.containerColumn.Validation.Label != '') {
			$scope.label = JSON.parse($scope.containerColumn.Validation.Label);
		} else {
			$scope.label = {};
		}
		if (ItemColumn.Validation.Label != '') {
			$scope.labelItemColumn = JSON.parse(ItemColumn.Validation.Label);
		} else {
			$scope.labelItemColumn = {};
		}


		$scope.dateOptions = [];
		if ($scope.columnDataType == 3) {
			angular.forEach(ProcessContainerColumns[$scope.parameters.ProcessContainerId], function (value) {
				if (value.ItemColumn.DataType == 3)
					$scope.dateOptions.push(value.ItemColumn);
			});
		}

		$scope.regex = [{value: '', name: 'undefined'}, {value: 1, name: 'EMAIL'}, {
			value: 2,
			name: 'CUSTOM'
		}];

		$scope.save = function () {
			$scope.containerColumn.Validation.Label = JSON.stringify($scope.label);
			$scope.itemColumnValidation.Label = JSON.stringify($scope.labelItemColumn);
			ItemColumnService.saveContainerColumn(StateParameters.process, [$scope.containerColumn]).then(function () {
				if (icChanged) {
					ItemColumnService.saveItemColumn(StateParameters.process, [ItemColumn]);
				}

				MessageService.closeDialog();
				$scope.parameters = {};
				icChanged = false;
			});
		};

		$scope.cancel = function () {
			$scope.containerColumn.Validation = validationWhileCanceled;
			MessageService.closeDialog();
		};
	}

	ItemColumnsEditController.$inject = ['$scope',
		'DialogParameters',
		'Translator',
		'StateParameters',
		'ItemColumnService',
		'MessageService',
		'ProcessActions',
		'ProcessActionsDialogs',
		'Equations',
		'Lists',
		'SubQueries',
		'Processes',
		'ItemColumns',
		'SettingsService',
		'Common',
		'$q',
		'ProcessActionsService',
		'$rootScope',
		'$filter',
		'ViewService',
		'ApiService',
		'ListsService',
		'Colors',
		'SaveService',
		'UpdateConnection',
		'ProcessItemColumns',
		'ProcessService',
		'SQValidation',
		'DashboardCharts',
		'PortfolioService',
		'NotificationService',
		'Templates'
	];

	function ItemColumnsEditController(
		$scope,
		DialogParameters,
		Translator,
		StateParameters,
		ItemColumnService,
		MessageService,
		ProcessActions,
		ProcessActionsDialogs,
		Equations,
		Lists,
		SubQueries,
		Processes,
		ItemColumns,
		SettingsService,
		Common,
		$q,
		ProcessActionsService,
		$rootScope,
		$filter,
		ViewService,
		ApiService,
		ListsService,
		Colors,
		SaveService,
		UpdateConnection,
		ProcessItemColumns,
		ProcessService,
		SQValidation,
		DashboardCharts,
		PortfolioService,
		NotificationService,
		Templates
	) {

		$scope.typeOptions = [{name: 'RM_ROOT', value: 'root'}];
		$scope.RMTypes = [];
		$scope.numberOfRMTypes = 0;

		$scope.templates = Templates;
		$scope.listOptionsLoaded = {};
		$scope.listOptions = {};
		$scope.equationValid = SQValidation;
		$scope.notificationPortfolios = [];
		$scope.draggables = [{name: Translator.translate('TRUE'), value: 'yes'}, {name: Translator.translate('FALSE'), value: 'no'}]

		$scope.languages = [];
		$scope.changeRichOptions = function () {
			if (typeof $scope.richPortfolioId != 'undefined') {
				let richValues = {'PortfolioId': $scope.richPortfolioId};
				$scope.Column.DataAdditional = JSON.stringify(richValues);
			} else {
				$scope.Column.DataAdditional = null;
			}
			$scope.changeDataAdditional();
		}

		if (DialogParameters.itemColumn.DataType == 23) {
			if (DialogParameters.itemColumn.DataAdditional != null) {
				DialogParameters.itemColumn.DataAdditional = JSON.parse(DialogParameters.itemColumn.DataAdditional);
				$scope.richPortfolioId = DialogParameters.itemColumn.DataAdditional.PortfolioId;
			}
			PortfolioService.getPortfolios('notification').then(function (portfolios) {
				$scope.notificationPortfolios = portfolios;
			})
		}

		$scope.richTemplates = [];


		$scope.SQRowVisCollection = {};
		$scope.matrixProcesses = _.filter(Processes, function (o) {
			return o.ProcessType == 4
		});
		$scope.checkCache = {};
		$scope.userLanguage = $rootScope.clientInformation.locale_id;

		$scope.checkSQRowVisibilityCondition = function (q) {
			if (q.Name.toString() == '@FlipParentChild') {
				for (let v of $scope.query["@SourceProcessColumnId"].Values) {
					if (v.item_column_id == $scope.query["@SourceProcessColumnId"].Value) {
						let r = v.result.split(":");
						let process = r[0].toString().trim();

						if (r[0].toString().trim() != StateParameters.process) return false;

						if ($scope.checkCache[v.item_column_id]) {
							if ($scope.checkCache[v.item_column_id] == "pending") return false;
							return $scope.checkCache[v.item_column_id];
						}

						$scope.checkCache[v.item_column_id] = "pending";
						SettingsService.ItemColumn(process, v.item_column_id).then(function (result) {
							$scope.checkCache[v.item_column_id] = result.Name.toString().toLowerCase().indexOf('item_id') > -1;
							return $scope.checkCache[v.item_column_id]
						});

						return false;
					}
				}
				return false;
			}
			let result = q.OptionType < 6 || (q.OptionType >= 6 && q.Values && q.Values.length > 0);
			if (result) result = $scope.checkSQChildRow(q.Name.toString());
			$scope.SQRowVisCollection[q.Name.toString()] = q.Value && q.Value != 0;

			return result;
		};

		$scope.conts = [];

		let findParentContainers = function (processName) {
			SettingsService.ProcessContainers(processName).then(function (data) {
				$scope.conts = $filter('orderBy')(data, function (d) {
					return Translator.translation(data.LanguageTranslation)
				});
			});
		}

		$scope.checkSQChildRow = function (name: string): boolean {
			if (name == "@OptionalList1ItemId") return $scope.SQRowVisCollection["@OptionalList1ColumnId"];
			if (name == "@OptionalList2ItemId") return $scope.SQRowVisCollection["@OptionalList2ColumnId"];
			if (name == "@OptionalDateType") return $scope.SQRowVisCollection["@OptionalDateId"];
			if (name == "@OptionalDateOffset") return $scope.SQRowVisCollection["@OptionalDateId"];
			if (name == "@OptionalOrderDirection") return $scope.SQRowVisCollection["@OptionalOrderColumnId"];

			return true;
		};

		angular.forEach(Equations, function (e, key) {
			if (e.ColumnType == 0 && e.SqlFunction == 10 && e.ItemColumnId) { // case_when_list

				ProcessService.getColumn(StateParameters.process, e.ItemColumnId).then(function (iCol) {

					if (iCol.DataType == 6) {
						ProcessService.getItems(iCol.DataAdditional).then(function (listOptions) {
							$scope.listOptions[e.ItemColumnEquationId] = listOptions;
							$scope.listOptionsLoaded[e.ItemColumnEquationId] = true;
						});
					}

					if (iCol.SubDataType == 6) {
						ProcessService.getItems(iCol.SubDataAdditional).then(function (listOptions) {
							$scope.listOptions[e.ItemColumnEquationId] = listOptions;
							$scope.listOptionsLoaded[e.ItemColumnEquationId] = true;
						});
					}

				});
			}

			if (e.ColumnType == 3 && e.SqlParam1) { // SEQ_LIST_VALUE
				ProcessService.getItems(e.SqlParam1).then(function (listOptions) {
					$scope.listOptions[e.ItemColumnEquationId] = listOptions;
					$scope.listOptionsLoaded[e.ItemColumnEquationId] = e.SqlParam1;
				});
			}
		});

		$scope.Column = DialogParameters.itemColumn;
		if ($scope.Column.DataType == 21 && $scope.Column.DataAdditional != null) findParentContainers($scope.Column.DataAdditional)
		$scope.needsProcess = false;

		let isProcessInvolved = function () {
			if ($scope.Column.DataType == 5 ||
				$scope.Column.DataType == 11 ||
				$scope.Column.DataType == 18 ||
				$scope.Column.DataType == 21 ||
				$scope.Column.DataType == 22 ||
				$scope.Column.DataType == 65) $scope.needsProcess = true;
			else $scope.needsProcess = false;
		};
		isProcessInvolved();

		$scope.defaultViews = ItemColumns['defaultViews'];

		//Matrix Datatype specific functionalities

		$scope.matrixScoreType;
		$scope.matrixScoreColumn;
		$scope.showComment;
		$scope.oneMandatory;

		let checkIfMatrixOptions = function () {
			if ($scope.Column.DataType == 22) {
				if ($scope.Column.DataAdditional2 != null) {
					let matrixOptions = JSON.parse($scope.Column.DataAdditional2);
					$scope.matrixScoreType = matrixOptions.Type;
					$scope.matrixScoreColumn = matrixOptions.ItemColumnId;
					$scope.showComment = matrixOptions.ShowComment;
					$scope.showLastModified = matrixOptions.ShowLastModified;
					$scope.hideScore = matrixOptions.HideScore;
					$scope.hideHeader = matrixOptions.HideHeader;
					if (matrixOptions.hasOwnProperty('OnlyOneMandatory')) $scope.oneMandatory = matrixOptions.OnlyOneMandatory;
				}
			}
		};

		checkIfMatrixOptions();

		$scope.changeMatrixScoreOptions = function () {
			let matrixValues = {
				'Type': $scope.matrixScoreType,
				'ItemColumnId': $scope.matrixScoreColumn,
				'ShowComment': $scope.showComment,
				'OnlyOneMandatory': $scope.oneMandatory,
				'ShowLastModified': $scope.showLastModified,
				'HideScore': $scope.hideScore,
				'HideHeader': $scope.hideHeader
			};
			$scope.Column.DataAdditional2 = JSON.stringify(matrixValues);
			$scope.changeDataAdditional();
		};
		//----------------------------------------------------------------------

		$scope.Column.DataAdditionalOld = angular.copy(DialogParameters.itemColumn.DataAdditional);
		$scope.dataAdditionalChangeConfirmed = true;
		$scope.listProcesses = Lists;
		$scope.processes = Processes;
		$scope.processActions = ProcessActions;
		$scope.views = ItemColumns['views'];

		let titleText = Translator.translate('FIELD_DATA') + ' ' + Translator.translation($scope.Column.LanguageTranslation);

		$scope.equationDelete = function (equationId) {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				SettingsService.ItemColumnEquationsDelete(StateParameters.process, [equationId]).then(function () {
					let index = Common.getIndexOf($scope.equations, equationId, 'ItemColumnEquationId');
					$scope.equations.splice(index, 1);
					prepareEquationsCombined();
				});
			});
		};

		$scope.header = {
			title: titleText
		};
		ViewService.footbar(StateParameters.ViewId, {template: 'settings/containers/containersFooter.html'});

		$scope.parentSelectTypes = [
			{
				name: 'DEFAULT',
				value: 0
			},
			{
				name: 'PARENT_SELECT_TYPE_1',
				value: 1
			}];

		$scope.equations = Equations;

		// start equation interface renewal functions
		$scope.EQgetFilteredCols = function (params) {
			let CombiCols = [];

			if (params && params.limit_datatypes) {
				angular.forEach(params.limit_datatypes, function (datatype_id) {
					angular.forEach(ProcessItemColumns, function (col) {
						if (col.DataType == datatype_id || ([16, 20].indexOf(col.DataType) !== -1 && col.SubDataType == datatype_id)) {
							CombiCols.push(col);
						}
					});
				});
			} else {
				CombiCols = angular.copy(ProcessItemColumns);
			}

			if (params && params.selected_ids) {
				angular.forEach(params.selected_ids, function (selected_id) {
					if (_.map(CombiCols, function (item) {
						return item.ItemColumnId;
					}).indexOf(selected_id) === -1) {
						angular.forEach(ProcessItemColumns, function (col) {
							if (col.ItemColumnId == selected_id) {
								CombiCols.push(col);
							}
						});
					}
				});
			}

			if (!params || !params.no_aliases) {
				for (let i = 0; i < $scope.equations.length; i++) {
					CombiCols.push({
						ItemColumnId: $scope.equations[i].SqlAlias,
						LanguageTranslation: "Alias: " + $scope.equations[i].SqlAlias
					});
				}
			}

			return CombiCols;
		};

		function prepareEquationsCombined() {
			for (let i = 0; i < $scope.equations.length; i++) {
				$scope.equations[i].combinedTypeFunction = $scope.equations[i].ColumnType + '_' + $scope.equations[i].SqlFunction;
				$scope.equations[i].combinedTypeFunctionInitial = $scope.equations[i].ColumnType + '_' + $scope.equations[i].SqlFunction;

				if ($scope.equations[i].ColumnType === 0) {
					$scope.equations[i].combinedId = null;
					$scope.equations[i].combinedParamOne = $scope.equations[i].SqlParam1;
					$scope.equations[i].combinedParamTwo = $scope.equations[i].SqlParam2;
					$scope.equations[i].combinedParamThree = $scope.equations[i].SqlParam3;

					if ($scope.equations[i].ItemColumnId) {
						$scope.equations[i].combinedId = $scope.equations[i].ItemColumnId;

					} else if (!$scope.equations[i].ItemColumnId) { //  && _.map($scope.equations, function (item) { return item.SqlAlias; }).indexOf($scope.equations[i].SqlParam1) > -1
						$scope.equations[i].combinedId = $scope.equations[i].SqlParam1;
						$scope.equations[i].combinedParamOne = $scope.equations[i].SqlParam2;
						$scope.equations[i].combinedParamTwo = $scope.equations[i].SqlParam3;
						$scope.equations[i].combinedParamThree = null;
					}
				}
			}

			$scope.EQCombinedCols = angular.copy(ProcessItemColumns);
			for (let i = 0; i < $scope.equations.length; i++) {
				$scope.EQCombinedCols.push({
					ItemColumnId: $scope.equations[i].SqlAlias,
					LanguageTranslation: "Alias: " + $scope.equations[i].SqlAlias
				});
			}

			$scope.EQCombinedLimitedCols = {};
			for (let i = 0; i < $scope.equations.length; i++) {
				if ([0, 2].indexOf($scope.equations[i].ColumnType) !== -1 && [1, 2, 3, 4].indexOf($scope.equations[i].SqlFunction) !== -1) { // string types
					$scope.EQCombinedLimitedCols[$scope.equations[i].SqlAlias] = $scope.EQgetFilteredCols({
						limit_datatypes: [0, 4],
						selected_ids: [$scope.equations[i].combinedId]
					});
				}
				if ([0, 2].indexOf($scope.equations[i].ColumnType) !== -1 && [8, 9, 11].indexOf($scope.equations[i].SqlFunction) !== -1) { // date types
					$scope.EQCombinedLimitedCols[$scope.equations[i].SqlAlias] = $scope.EQgetFilteredCols({
						limit_datatypes: [3, 13],
						selected_ids: [$scope.equations[i].combinedId, $scope.equations[i].combinedParamOne]
					});
				}
				if ([0, 2].indexOf($scope.equations[i].ColumnType) !== -1 && [10].indexOf($scope.equations[i].SqlFunction) !== -1) { // list types
					$scope.EQCombinedLimitedCols[$scope.equations[i].SqlAlias] = $scope.EQgetFilteredCols({
						limit_datatypes: [6],
						selected_ids: [$scope.equations[i].combinedId],
						no_aliases: true
					});
				}
				if ([0, 2].indexOf($scope.equations[i].ColumnType) !== -1 && [13].indexOf($scope.equations[i].SqlFunction) !== -1) { // only aliases
					$scope.EQCombinedLimitedCols[$scope.equations[i].SqlAlias] = $scope.EQgetFilteredCols({
						limit_datatypes: [-1]
					});
				}
			}

			$scope.EQCombinedScope = [{value: 'minute', name: 'SEQ_DATEADD_MINUTE'}
				, {value: 'hour', name: 'SEQ_DATEADD_HOUR'}
				, {value: 'day', name: 'SEQ_DATEADD_DAY'}
				, {value: 'week', name: 'SEQ_DATEADD_WEEK'}
				, {value: 'month', name: 'SEQ_DATEADD_MONTH'}
				, {value: 'quarter', name: 'SEQ_DATEADD_QUARTER'}
				, {value: 'year', name: 'SEQ_DATEADD_YEAR'}
			];
		};
		prepareEquationsCombined();
		$scope.prepareSaveEquationsCombined = function (params) {
			let save_equation = params.save_equation;

			save_equation.ColumnType = parseInt(save_equation.combinedTypeFunction.toString().split('_')[0]);
			save_equation.SqlFunction = parseInt(save_equation.combinedTypeFunction.toString().split('_')[1]);

			if (save_equation.combinedTypeFunctionInitial !== save_equation.combinedTypeFunction) {
				save_equation.SqlParam1 = save_equation.combinedParamOne = null;
				save_equation.SqlParam2 = save_equation.combinedParamTwo = null;
				save_equation.SqlParam3 = save_equation.combinedParamThree = null;
				save_equation.ItemColumnId = save_equation.combinedId = null;
			}
			save_equation.combinedTypeFunctionInitial = save_equation.combinedTypeFunction;

			if (save_equation.ColumnType === 0) {
				if (parseInt(params.clearParams) > 0) {
					if (parseInt(params.clearParams) <= 1)
						save_equation.combinedParamOne = null;
					if (parseInt(params.clearParams) && params.clearParams <= 2)
						save_equation.combinedParamTwo = null;
					if (parseInt(params.clearParams) && params.clearParams <= 3)
						save_equation.combinedParamThree = null;
				}

				save_equation.SqlParam1 = save_equation.combinedParamOne;
				save_equation.SqlParam2 = save_equation.combinedParamTwo;
				save_equation.SqlParam3 = save_equation.combinedParamThree;
				if (save_equation.combinedId && parseInt(save_equation.combinedId) != save_equation.combinedId) { // if alias
					save_equation.SqlParam1 = save_equation.combinedId;
					save_equation.SqlParam2 = save_equation.combinedParamOne;
					save_equation.SqlParam3 = save_equation.combinedParamTwo;
					save_equation.ItemColumnId = null;
				} else if (save_equation.combinedId && parseInt(save_equation.combinedId) == save_equation.combinedId) { // if column id
					save_equation.ItemColumnId = save_equation.combinedId;
				}
			} else if (save_equation.ColumnType === 3) {
				if (parseInt(params.clearParams) > 0) {
					if (parseInt(params.clearParams) <= 1)
						save_equation.SqlParam1 = null;
					if (parseInt(params.clearParams) <= 2)
						save_equation.SqlParam2 = null;
					if (parseInt(params.clearParams) <= 3)
						save_equation.SqlParam3 = null;
				}
			}

			$scope.checkValidation(save_equation);
			prepareEquationsCombined();
		};
		// end equation interface renewal functions

		$scope.selectedRows = [];
		$scope.cols = ProcessItemColumns;

		$scope.charts = DashboardCharts;
		$scope.filteredCols = angular.copy($scope.cols);
		$scope.filteredCols.push({
			LanguageTranslation: "item_id",
			ItemColumnId: 0,
			Name: "item_id"
		});

		let validateChars = function (equation, allowedChars, allowedAliases?) {
			allowedChars = allowedChars.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
			allowedAliases = "";
			let allowedAliasesArr = [];
			for (let i = 0; i < $scope.equations.length; i++) {
				allowedAliases += $scope.equations[i].SqlAlias;
				allowedAliasesArr.push($scope.equations[i].SqlAlias);
			}
			allowedAliases = allowedAliases.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');

			let regex1 = RegExp('^[' + allowedChars + ']*$');
			let regex2 = RegExp('^[' + allowedAliases + ']*$');

			let alias = '';
			let aliases = [];
			let isValidEquation = true;
			let isAliasesValid = false;
			if (equation != null && equation.length > 0) {
				for (let i = 0; i < equation.length; i++) {
					let ch = equation.charAt(i);
					if (regex1.test(ch)) {
						if (alias.length > 0 && aliases.indexOf(alias) === -1) {
							aliases.push(alias);
						}
						alias = '';
						isValidEquation = true;
					} else if (regex2.test(ch)) {
						alias += ch;
					} else {
						isValidEquation = false;
						break
					}
				}
				if (alias.length > 0 && aliases.indexOf(alias) === -1) {
					aliases.push(alias);
				}
				if (aliases.length > 0) {
					for (let i = 0; i < aliases.length; i++) {
						if (allowedAliasesArr.indexOf(aliases[i]) === -1) {
							isAliasesValid = false;
							break;
						} else {
							isAliasesValid = true;
						}
					}
				}
			}
			return (isValidEquation && isAliasesValid);
		};

		let validateCaseEquation = function (e) {
			let allowedChars = '+-/*<>=! 1234567890';
			e.caseEquationValid = validateChars(e.SqlParam1, allowedChars);
		};

		let validateEquation = function () {
			let p = $q.defer();
			let allowedChars = '+ ';
			if ($scope.subDataType != 0) {
				allowedChars += '-/*()';
			}
			$scope.equationValid = -1;
			let isValid = validateChars($scope.Column.DataAdditional, allowedChars);
			if (isValid) {
				// SettingsService.ItemColumnEquationsTest(StateParameters.process, StateParameters.itemColumn.ItemColumnId, $scope.Column.DataAdditional).then(function (isEquationValid) {
				$scope.equationValid = 1;
				p.resolve();

				// });  this class needs to be fixed on the server at some point pls
			} else {
				$scope.equationValid = 0;
				p.resolve();
			}
			return p.promise;
		};

		if ($scope.Column.DataType != 16) validateEquation();

		for (let i = 0; i < $scope.equations.length; i++) {
			if ($scope.equations[i].ColumnType == 2 && $scope.equations[i].SqlFunction == 5) {
				validateCaseEquation($scope.equations[i]);
			}
		}

		let equationsSave = function () {
			let changedRows = [];
			for (let i = 0; i < $scope.equations.length; i++) {
				if ($scope.equations[i].changed) {
					changedRows.push($scope.equations[i]);
				}
			}
			return validateEquation().then(function () {
				return SettingsService.ItemColumnEquationsSave(StateParameters.process, changedRows).then(function () {
					return SettingsService.ItemColumnsSave(StateParameters.process, [$scope.Column]);
				});
			})
		};

		$scope.addEquation = function () {
			SettingsService.ItemColumnEquationsAdd(StateParameters.process, {ParentItemColumnId: $scope.Column.ItemColumnId}).then(function (e) {
				$scope.equations.push(e);
				prepareEquationsCombined();
			});
		};

		$scope.checkValidation = function (e) {
			e.changed = true;
			$scope.listOptionsLoaded[e.ItemColumnEquationId] = false;

			if (e.ColumnType == 1) {
				if (e.SqlParam1) e.SqlParam1 = e.SqlParam1.replace("_", " ");
				if (e.SqlParam2) e.SqlParam2 = e.SqlParam2.replace("_", " ");
				if (e.SqlParam3) e.SqlParam3 = e.SqlParam3.replace("_", " ");
			}
			if (e.ColumnType == 2 && e.SqlFunction == 5) {
				validateCaseEquation(e)
			} else {
				e.caseEquationValid = true;
			}

			if (e.ColumnType == 0 && e.SqlFunction == 10 && e.ItemColumnId) { // case_when_list

				ProcessService.getColumn(StateParameters.process, e.ItemColumnId).then(function (iCol) {

					if (iCol.DataType == 6) {
						ProcessService.getItems(iCol.DataAdditional).then(function (listOptions) {
							$scope.listOptions[e.ItemColumnEquationId] = listOptions;
							$scope.listOptionsLoaded[e.ItemColumnEquationId] = true;
						});
					}

					if (iCol.SubDataType == 6) {
						ProcessService.getItems(iCol.SubDataAdditional).then(function (listOptions) {
							$scope.listOptions[e.ItemColumnEquationId] = listOptions;
							$scope.listOptionsLoaded[e.ItemColumnEquationId] = true;
						});
					}

				});
			}

			if (e.ColumnType == 3 && e.SqlParam1) { // SEQ_LIST_VALUE
				ProcessService.getItems(e.SqlParam1).then(function (listOptions) {
					$scope.listOptions[e.ItemColumnEquationId] = listOptions;
					$scope.listOptionsLoaded[e.ItemColumnEquationId] = e.SqlParam1;
				});
			}

			if (e.ColumnType == 0 && e.SqlFunction == 11) { // dateadd
				if (e.ItemColumnId && !(e.SqlParam1)) // if column selected and empty param1 - otherwise param1 might be wanted to contain row alias
					e.SqlParam1 = 'day';
			}
		};

		$scope.processActionDialogs = ProcessActionsDialogs;
		$scope.subQueries = _.orderBy(SubQueries, [item => Translator.translation(item.TranslationKey)]);

		let findDialogType = function (dialogId) {
			let suvas = 1;
			_.each($scope.processActionDialogs, function (dialog: Client.ProcessDialog) {
				if (dialog.ProcessActionDialogId == dialogId) {
					suvas = dialog.Type;
				}
			});
			return suvas;
		};

		let getActionTranslation = function (id) {
			let translation = '';
			angular.forEach(ProcessActions, function (value) {
				if (value.ProcessActionId == id) {
					translation = Translator.translation(value.LanguageTranslation);
				}
			});
			return translation;
		};

		let getDialogTranslation = function (id) {
			let translation = '';
			angular.forEach($scope.processActionDialogs, function (value) {
				if (value.ProcessActionDialogId == id) {
					translation = Translator.translation(value.LanguageTranslation[$rootScope.me.locale_id]);
				}
			});
			return translation;
		};

		$scope.createNewActionOrDialog = function (itemType) {
			let params = StateParameters;
			if (itemType == 'action') {
				let newActionForButton = {'process': "", LanguageTranslation: {}};
				newActionForButton.process = StateParameters.process;
				newActionForButton.LanguageTranslation[$rootScope.clientInformation.locale_id] = $scope.Column.Translation + ' ' + 'action';
				ProcessActionsService.ProcessActionAdd(StateParameters.process, newActionForButton).then(function (action) {
					SettingsService.ProcessActionsGet(StateParameters.process).then(function (actions) {
						angular.forEach(actions, function (key) {
							key.Translation = key.LanguageTranslation[$rootScope.me.locale_id];
						});
						actions = $filter('orderBy')(actions, 'Translation');
						$scope.processActions = actions;
					});
					$scope.Column.ProcessActionId = action.ProcessActionId;
					params.ProcessActionId = action.ProcessActionId;
					params.UserLanguageTranslation = action.LanguageTranslation[$rootScope.clientInformation.locale_id];
					setChangedColumnForSave();
					ViewService.view('settings.processActionRows', {params: params}, {split: true});
				});
			} else if (itemType == 'dialog') {
				let newDialogForButton = {'process': "", LanguageTranslation: {}, Type: 0};
				newDialogForButton.process = StateParameters.process;
				newDialogForButton.LanguageTranslation[$rootScope.clientInformation.locale_id] = $scope.Column.LanguageTranslation[$rootScope.clientInformation.locale_id] + ' ' + 'confirm dialog';
				newDialogForButton.Type = 2;
				ProcessActionsService.ProcessActionDialogAdd(StateParameters.process, newDialogForButton).then(function (dialog) {
					SettingsService.getDialogSettings(StateParameters.process).then(function (dialogs) {
						angular.forEach(dialogs, function (key) {
							key.Translation = key.LanguageTranslation[$rootScope.clientInformation.locale_id];

						});
						$scope.processActionDialogs = dialogs;
						$scope.Column.ProcessDialogId = dialog.ProcessActionDialogId;
						params.UserLanguageTranslation = dialog.LanguageTranslation[$rootScope.clientInformation.locale_id];
						params.ProcessActionDialogId = dialog.ProcessActionDialogId;
						params.dialogType = dialog.Type;
						setChangedColumnForSave();
						ViewService.view('settings.processActions.dialogEditor', {params: params}, {split: true});
					});

				});
			} else {
				let newDialogForButton = {'process': "", LanguageTranslation: {}, Type: 0};
				newDialogForButton.process = StateParameters.process;
				newDialogForButton.LanguageTranslation[$rootScope.clientInformation.locale_id] = $scope.Column.Translation + ' ' + 'info dialog';
				newDialogForButton.Type = 1;
				ProcessActionsService.ProcessActionDialogAdd(StateParameters.process, newDialogForButton).then(function (dialog) {
					SettingsService.getDialogSettings(StateParameters.process).then(function (dialogs) {
						angular.forEach(dialogs, function (key) {
							key.Translation = key.LanguageTranslation[$rootScope.clientInformation.locale_id];
						});
						dialogs = $filter('orderBy')(dialogs, 'Translation');

						$scope.processActionDialogs = dialogs;
					});
					$scope.Column.doneDialog = dialog.ProcessActionDialogId;
					params.UserLanguageTranslation = dialog.LanguageTranslation[$rootScope.clientInformation.locale_id];
					params.ProcessActionDialogId = dialog.ProcessActionDialogId;
					params.dialogType = dialog.Type;
					$scope.changeProperties();
					ViewService.view('settings.processActions.dialogEditor', {params: params}, {split: true});
				});
			}
			MessageService.closeDialog();
		};

		$scope.goToDialog = function (ic, typeName) {
			let id = typeName == 'normal' ? ic.ProcessDialogId : ic.doneDialog;
			let dialogTranslation = getDialogTranslation(id);
			let params = StateParameters;
			params.UserLanguageTranslation = dialogTranslation;
			params.ProcessActionDialogId = id;
			params.dialogType = findDialogType(id);
			ViewService.view('settings.processActions.dialogEditor', {params: params}, {
				split: true,
				replace: 'settings.fields.properties'
			});
		};

		$scope.goToAction = function (button) {
			let actionTranslation = getActionTranslation(button.ProcessActionId);
			let params = StateParameters;
			params.ProcessActionId = button.ProcessActionId;
			params.UserLanguageTranslation = actionTranslation;
			ViewService.view('settings.processActionRows', {params: params}, {
				split: true,
				replace: 'settings.fields.properties'
			});
		};

		$scope.processFilterOptions = function (itemColumnId, n) {
			let ind;
			let items;
			if (n == 1) {
				ind = Common.getIndexOf($scope.filteredCols, itemColumnId, 'ItemColumnId');
				items = $scope.filteredCols;
				$scope.Column.fromProcessColumnName = items[ind].Name;
				$scope.Column.fromProcessColumnId = items[ind].ItemColumnId;
			} else {
				ind = Common.getIndexOf($scope.filteredsProcessColumns, itemColumnId, 'ItemColumnId');
				items = $scope.filteredsProcessColumns;
				$scope.Column.targetProcessColumnName = items[ind].Name;
				$scope.Column.targetProcessColumnId = items[ind].ItemColumnId;
			}
			$scope.changeProperties();

		};

		$scope.filteredsProcessColumns = [];
		let findFilteredColumns = function (processToFind) {
			$scope.filteredsProcessColumns = [];
			SettingsService.ItemColumns(processToFind).then(function (cols_) {
				$scope.filteredsProcessColumns = cols_
			});
		};

		let getProcessPortfolios = function () {
			if ($scope.Column.DataAdditional != null && $scope.Column.DataType == 5 || $scope.Column.DataType == 18 || $scope.Column.DataType == 21 || $scope.Column.DataType == 20) {
				let processParameter = $scope.Column.DataType != 20 ? $scope.Column.DataAdditional : $scope.Column.InputName
				$scope.optionsLoaded = false;
				ApiService.v1api('meta/ProcessPortfolios').get([processParameter]).then(function (pp) {
					$scope.portfolios = pp;
					$scope.optionsLoaded = true;
				});
			}
		};

		$scope.dataType = Common.getDataTypes();

		$scope.inputMethod = [{value: 0, name: 'INPUT_METHOD_DATATYPE'}, {
			value: 1,
			name: 'INPUT_METHOD_LIST'
		}, {value: 2, name: 'INPUT_METHOD_SWITCH'}, {
			value: 3,
			name: 'INPUT_METHOD_PARENT'
		}];

		if ($scope.Column.DataType == 8) {
			if (angular.isDefined($scope.Column.DataAdditional2) && $scope.Column.DataAdditional2 != '' && $scope.Column.DataAdditional2 != null) {

				let myTest = JSON.parse($scope.Column.DataAdditional2);
				$scope.Column.submitView = myTest.initialState;
				$scope.Column.afterFunctions = myTest.viewActions;
				$scope.Column.openView = myTest.currentState;
				$scope.Column.doneDialog = myTest.optionalDoneDialog;
				$scope.selectedTemplates = myTest.selectedTemplates;
			}
			if ($scope.Column.SubDataAdditional2 == null) {
				$scope.Column.SubDataAdditional2 = false;
			}

		} else if ($scope.Column.DataType == 16 && $scope.Column.DataAdditional != null) {
			$scope.optionsLoaded = false;
			ApiService.v1api('settings/RegisteredSubQueries').get([StateParameters.process, $scope.Column.DataAdditional, $scope.Column.ItemColumnId]).then(function (query) {
				$scope.optionsLoaded = true;
				$scope.query = query;
			});
		} else if (($scope.Column.DataType == 5 || $scope.Column.DataType == 65) && $scope.Column.DataAdditional != null) {
			$scope.optionsLoaded = false;
			let relativeOptions = [];
			ApiService.v1api('meta/ItemColumns').get([$scope.Column.DataAdditional]).then(function (relCols) {
				angular.forEach(relCols, function (relCol) {
					if ($scope.Column.DataType == 65) {
						relativeOptions.push(relCol)
					} else {
						if (relCol.DataType == 5 && relCol.DataAdditional == StateParameters.process) {
							relativeOptions.push(relCol)
						}
					}
				});
				$scope.relativeParentColumns = relativeOptions;
				$scope.optionsLoaded = true;
			});
		}

		if ($scope.Column.DataType == 20 && $scope.Column.SubDataType == 5) {
			if ($scope.Column.SubDataAdditional2 != "") {
				let daObj2 = JSON.parse($scope.Column.SubDataAdditional2);
				$scope.Column.canOpen = Common.isTrue(daObj2.canOpen);
				$scope.Column.openWidth = daObj2.openWidth;
				$scope.Column.showAll = daObj2.showAll;
				$scope.Column.IsTable = daObj2.IsTable;
				getProcessPortfolios();
			}
		}

		if ($scope.Column.DataType == 5) {
			$scope.Column.IsTable = false;
			$scope.Column.portfolioId = 0;
			$scope.Column.showAll = false;
			$scope.Column.canOpen = false;
			$scope.Column.advancedFilter = false;
			$scope.Column.luceneScore = 0;
			$scope.Column.luceneMaxCount = 10;

			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.IsTable = Common.isTrue(daObj.table);
					$scope.Column.showAll = Common.isTrue(daObj.showAll);
					$scope.Column.canOpen = Common.isTrue(daObj.canOpen);
					$scope.Column.advancedFilter = Common.isTrue(daObj.advancedFilter);
					$scope.Column.openWidth = daObj.openWidth;
					$scope.Column.portfolioId = daObj.portfolioId;

					$scope.Column.fromProcessColumnName = daObj.fromProcessColumnName;
					$scope.Column.fromProcessColumnId = daObj.fromProcessColumnId;
					$scope.Column.targetProcessName = daObj.targetProcessName;
					$scope.Column.targetProcessColumnId = daObj.targetProcessColumnId;
					$scope.Column.targetProcessName = daObj.targetProcessName;

					findFilteredColumns($scope.Column.targetProcessName);

					if (angular.isDefined(daObj.luceneScore)) $scope.Column.luceneScore = daObj.luceneScore;
					if (angular.isDefined(daObj.luceneMaxCount)) $scope.Column.luceneMaxCount = daObj.luceneMaxCount;
				}
			}
		}

		if ($scope.Column.DataType == 6) {
			$scope.listsColumns = [];
			ProcessService.getColumns($scope.Column.DataAdditional, true).then(function (columns) {
				$scope.listsColumns = columns;
				$scope.listsColumns[1] = {Name: 'item_id', Variable: 'item_id'};
			});

			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.fromProcessColumnName = daObj.fromProcessColumnName;
					$scope.Column.fromProcessColumnId = daObj.fromProcessColumnId;
					$scope.Column.targetProcessName = daObj.targetProcessName;
					$scope.Column.targetProcessColumnId = daObj.targetProcessColumnId;
					$scope.Column.targetProcessColumnName = daObj.targetProcessColumnName;
					$scope.Column.targetProcessName = daObj.targetProcessName;
					$scope.Column.targetListColumn = daObj.targetListColumn;
					findFilteredColumns($scope.Column.targetProcessName);
				}
			}
		}

		if ($scope.Column.DataType == 5 || $scope.Column.DataType == 6) {
			$scope.listFk = [
				{value: 0, name: 'FK_NO'},
				{value: 2, name: 'FK_CHILD'},
				{value: 3, name: 'FK_PARENT'}
			];
		}

		if ($scope.Column.DataType < 5) {
			$scope.listIndex = [
				{value: 0, name: 'INDEX_NO'},
				{value: 1, name: 'INDEX_UNIQUE'},
				{value: 2, name: 'INDEX_NON_UNIQUE'}
			];
		}

		if ($scope.Column.DataType == 5 || $scope.Column.DataType == 18 || $scope.Column.DataType == 21) {
			if ($scope.Column.DataAdditional != null) {
				findParentContainers($scope.Column.DataAdditional);
				getProcessPortfolios();
				if ($scope.Column.DataType == 21) {
					ProcessActionsService.getProcessActions($scope.Column.DataAdditional).then(function (actions) {
						$scope.actions = actions;
					});
				}
			}
		}

		if ($scope.Column.DataType == 25) {
			$scope.Column.selectedChart = $scope.Column.DataAdditional;
		}

		$scope.dateColumns = [];
		$scope.parentColumns = [];
		if ($scope.Column.DataType == 18 || $scope.Column.DataType == 21) {
			_.each(ItemColumns.itemColumns, function (i) {
				_.each(i, function (c) {
					if (c.DataType == 3 || c.DataType == 13) {
						if (!$scope.dateColumns.find(cc => cc.ItemColumnId == c.ItemColumnId)) $scope.dateColumns.push(c);
					}
				});
			});

			SettingsService.ItemColumns(StateParameters.process).then(function (cols_) {
				$scope.parentColumns = cols_
			});
		}

		if ($scope.Column.DataType == 18) {
			$scope.Column.portfolioId = 0;
			$scope.Column.portfolioColumnId = 0;
			$scope.Column.canOpen = false;
			$scope.Column.showSum = false;
			$scope.Column.showSumCumulative = false;
			$scope.Column.showAddDelete = false;
			$scope.Column.useDialog = false;
			$scope.Column.ignoreExcelExport = false;

			if ($scope.Column.DataAdditional != null) {
				ProcessActionsService.getProcessActions($scope.Column.DataAdditional).then(function (actions) {
					$scope.actions = actions;
				});
			}

			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.openWidth = daObj.openWidth;
					$scope.Column.portfolioId = daObj.portfolioId;
					$scope.Column.portfolioColumnId = daObj.itemColumnId;
					$scope.Column.canOpen = daObj.canOpen;
					$scope.Column.showSum = daObj.showSum;
					$scope.Column.showSumCumulative = daObj.showSumCumulative;
					$scope.Column.showAddDelete = daObj.showAddDelete;
					$scope.Column.actionId = daObj.actionId;
					$scope.Column.useDialog = daObj.useDialog;
					$scope.Column.archiveFieldName = daObj.archiveFieldName;
					$scope.Column.parentFieldName = daObj.parentFieldName;
					$scope.Column.ignoreExcelExport = daObj.ignoreExcelExport;
				}
			}
		}

		if ($scope.Column.DataType == 16 && $scope.Column.SubDataType == 5) {
			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.canOpen = daObj.canOpen;
					$scope.Column.openWidth = daObj.openWidth;
				}
			}

			if ($scope.Column.SubDataAdditional2 != null && $scope.Column.SubDataAdditional2 != "") {
				let daObj2 = JSON.parse($scope.Column.SubDataAdditional2);
				if (daObj2 != null) {
					$scope.Column.showAll = daObj2.showAll;
					$scope.Column.IsTable = daObj2.IsTable;
				}
			} else {
				$scope.Column.showAll = false;
				$scope.Column.IsTable = false;
			}
		}

		//Cache enabled for sq
		if ($scope.Column.DataType == 16) {
			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.cacheEnabled = daObj.cacheEnabled;
				}
			}
		}

		if ($scope.Column.DataType == 9) {
			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.ratingPlaceholder = daObj.placeholder;
					$scope.Column.ratingTotal = daObj.total;
					$scope.Column.ratingAmount = daObj.amount;
					$scope.Column.ratingTrueIcon = daObj.trueIcon;
					$scope.Column.ratingFalseIcon = daObj.falseIcon;
					$scope.Column.ratingColor = daObj.color;

				}
			}

			$scope.colors = Colors.getColorsArray();
			$scope.symbols = [];
			ListsService.getSystemLists('SYMBOLS').then(function (symbols) {
				$scope.symbols = symbols;
			});
		}

		if ($scope.Column.DataType == 21) {
			$scope.RMTypes = [];

			if ($scope.Column.DataAdditional2 != null) {
				let da = angular.fromJson($scope.Column.DataAdditional2);
				$scope.RMTypes = da.definitions;
				$scope.numberOfRMTypes = $scope.RMTypes.length;

				$scope.Column.useDialog = da.options.useDialog;
				$scope.Column.openWidth = da.options.openWidth;
				$scope.Column.portfolioId = da.options.portfolioId;
				$scope.Column.portfolioColumnId = da.options.ownerColumnId;
				$scope.Column.archiveFieldName = da.options.archiveFieldName;
				$scope.Column.parentFieldName = da.options.parentFieldName;
				$scope.Column.ignoreExcelExport = da.options.ignoreExcelExport;
				$scope.Column.forceAllOpen = da.options.forceAllOpen;

				for (let i = 0, l = $scope.RMTypes.length; i < l; i++) {
					let t = $scope.RMTypes[i];
					$scope.typeOptions.push({
						name: t.name,
						value: t.type
					});
				}
			}

		}
		if ($scope.Column.DataType == 10) {
			$scope.crops = [
				{value: 0, name: 'CROP_ENABLED_NO'},
				{value: 1, name: 'CROP_ENABLED_MANUAL'},
				{value: 2, name: 'CROP_ENABLED_AUTO'}
			];

			$scope.Column.cropEnabled = false;
			$scope.Column.bigThumbNails = false;
			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.cropEnabled = daObj.cropEnabled;
					$scope.Column.bigThumbNails = Common.isTrue(daObj.bigThumbNails);
				}
			}
		}
		if ($scope.Column.DataType == 12) {
			$scope.Column.note = false;
			$scope.Column.infinite = false;
			$scope.Column.preventatt = false;

			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.note = Common.isTrue(daObj.note);
					$scope.Column.infinite = Common.isTrue(daObj.infinite);
					$scope.Column.preventatt = Common.isTrue(daObj.preventatt);
				}
			}
		}

		if ($scope.Column.DataType == 65) {
			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.endDateColumnId = daObj.endDateColumnId;
					$scope.Column.milestoneColumnId = daObj.milestoneColumnId;
					$scope.Column.descriptionColumnId = daObj.descriptionColumnId;
					$scope.Column.dependencyColumnId = daObj.dependencyColumnId;
				}
			}
		}

		if ($scope.Column.DataType == 8) {
			if ($scope.Column.DataAdditional != null && $scope.Column.DataAdditional === 'ignoreFormValidation') {
				$scope.Column.IgnoreValidation = true;
			} else {
				$scope.Column.IgnoreValidation = false;
			}
		}

		if ($scope.Column.DataType == 17) {
			if ($scope.Column.DataAdditional2 != null) {
				let daObj = JSON.parse($scope.Column.DataAdditional2);
				if (daObj != null) {
					$scope.Column.isAlert = daObj.isAlert;
				}
			}
			if ($scope.Column.DataAdditional != null) {
				$scope.contentLabel = JSON.parse($scope.Column.DataAdditional)
			} else {
				$scope.contentLabel = {};
			}
			NotificationService.getLanguages().then(function (b) {
				$scope.languages = b;
				let i = $scope.languages.length;
				while (i--) {
					let l = $scope.languages[i].value;
					$scope.contentLabel[l] = typeof $scope.contentLabel[l] === 'undefined' ?
						{html: '', delta: undefined} : angular.fromJson($scope.contentLabel[l]);
				}
			});
		}

		$scope.options = [];
		$scope.optionsLoaded = false;

		$scope.changeDataAdditional = function () {
			if ($scope.Column.DataType != 16) $scope.dataAdditionalChangeConfirmed = false;
			if ($scope.Column.DataType == 11 && $scope.Column.DataAdditional != null && angular.isUndefined($scope.options[$scope.Column.DataAdditional])) {
				$scope.optionsLoaded = false;
				SettingsService.ItemColumns($scope.Column.DataAdditional).then(function (data) {
					$scope.options[$scope.Column.DataAdditional] = $filter('orderBy')(data, function (d) {
						return Translator.translation(d.LanguageTranslation)
					});
					$scope.optionsLoaded = true;
				});
			} else if ($scope.Column.DataType == 16 && $scope.Column.DataAdditional != null) {

				$scope.optionsLoaded = false;
				$scope.Column.DataAdditional2 = null;

				if ($scope.Column.DataAdditional == "GenericProcessAggregateFunction") $scope.Column.SubDataType = 5;

				ApiService.v1api('settings/RegisteredSubQueries').get([StateParameters.process, $scope.Column.DataAdditional, $scope.Column.ItemColumnId]).then(function (query) {
					$scope.optionsLoaded = true;
					$scope.query = query;
				});
			} else if (($scope.Column.DataType == 5 || $scope.Column.DataType == 65) && $scope.Column.DataAdditional != null) {


				findParentContainers($scope.Column.DataAdditional);

				$scope.optionsLoaded = false;
				let relativeOptions = [];
				ApiService.v1api('meta/ItemColumns').get([$scope.Column.DataAdditional]).then(function (relCols) {
					angular.forEach(relCols, function (relCol) {
						if ($scope.Column.DataType == 65) {
							relativeOptions.push(relCol)
						} else {
							if (relCol.DataType == 5 && relCol.DataAdditional == StateParameters.process) {
								relativeOptions.push(relCol)
							}
						}
					});
					$scope.relativeParentColumns = relativeOptions;
					$scope.optionsLoaded = true;
				});
			} else if (($scope.Column.DataType == 18 || $scope.Column.DataType == 21) && $scope.Column.DataAdditional != null) {
				getTableOptions();
				ProcessActionsService.getProcessActions($scope.Column.DataAdditional).then(function (actions) {
					$scope.actions = actions;
				});
			} else if ($scope.Column.DataType == 17) {
				$scope.Column.DataAdditional = JSON.stringify($scope.contentLabel);
			}

			$scope.changeProperties();
			getProcessPortfolios();
			//setChangedColumnForSave();
		};
		let getTableOptions = function () {
			$scope.optionsLoaded = false;
			let promises = [];
			promises.push(ApiService.v1api('meta/ProcessPortfolios').get([$scope.Column.DataAdditional]).then(function (pp) {
				$scope.portfolios = pp;
			}));
			promises.push(ApiService.v1api('meta/ItemColumns').get([$scope.Column.DataAdditional]).then(function (relCols) {
				$scope.relativePortfolioColumns = relCols;
			}));
			$q.all(promises).then(function () {
				$scope.optionsLoaded = true;
			});
		};
		if ($scope.Column.DataType == 18 || $scope.Column.DataType == 21) {
			getTableOptions();
		}

		SaveService.subscribeSave($scope, function () {
			if (!$scope.allowSave()) return;
			if ($scope.equations.length > 0) equationsSave();
			setChangedColumnForSave();
		});

		$scope.cancel = function () {
			MessageService.closeDialog();
		};


		let setViewStates = function () {
			let da2ForButton = {
				currentState: undefined,
				initialState: undefined,
				optionalDoneDialog: null,
				optionalDoneDialogType: null,
				viewActions: [],
				selectedTemplates: []
			};

			if (typeof $scope.Column.doneDialog != 'undefined') {
				da2ForButton.optionalDoneDialog = $scope.Column.doneDialog;

				if ($scope.Column.doneDialog != null && $scope.Column.doneDialog != "") {
					da2ForButton.optionalDoneDialogType = _.find($scope.processActionDialogs, function (o) {
						return o.ProcessActionDialogId == $scope.Column.doneDialog;
					}).Type;
				}
			}

			if (typeof $scope.Column.openView != 'undefined') {
				da2ForButton.currentState = $scope.Column.openView;
			}
			if (typeof $scope.Column.submitView != 'undefined') {
				da2ForButton.initialState = $scope.Column.submitView;
			}
			if ($scope.Column.afterFunctions && $scope.Column.afterFunctions.length > 0) {
				da2ForButton.viewActions = $scope.Column.afterFunctions;
			}

			if (typeof $scope.selectedTemplates != 'undefined') {
				da2ForButton.selectedTemplates = $scope.selectedTemplates;
			}

			$scope.Column.DataAdditional2 = JSON.stringify(da2ForButton);
		};

		$scope.changeProperties = function () {
			isProcessInvolved();
			UpdateConnection($scope.Column.ItemColumnId, DialogParameters.ProcessContainerColumnId, $scope.Column.DataType, $scope.Column.LanguageTranslationRaw);
			if ($scope.Column.DataType == 1) {
				if ($scope.Column.UseFk) $scope.Column.UseFk = 1; else $scope.Column.UseFk = 0;
			} else if ($scope.Column.DataType == 8) {
				setViewStates();
				if ($scope.Column.IgnoreValidation) {
					$scope.Column.DataAdditional = 'ignoreFormValidation';
				} else {
					$scope.Column.DataAdditional = null;
				}
			} else if ($scope.Column.DataType == 9) {
				$scope.Column.DataAdditional2 = JSON.stringify({
					'total': $scope.Column.ratingTotal,
					'placeholder': $scope.Column.ratingPlaceholder,
					'amount': $scope.Column.ratingAmount,
					'color': $scope.Column.ratingColor,
					'trueIcon': $scope.Column.ratingTrueIcon,
					'falseIcon': $scope.Column.ratingFalseIcon
				});
			} else if ($scope.Column.DataType == 10) {
				$scope.Column.DataAdditional2 = JSON.stringify({
					'cropEnabled': $scope.Column.cropEnabled,
					'bigThumbNails': $scope.Column.bigThumbNails
				});

			} else if ($scope.Column.DataType == 25) {
				$scope.Column.DataAdditional = $scope.Column.selectedChart;
			} else if ($scope.Column.DataType == 20 && $scope.Column.SubDataType == 5) {
				$scope.Column.SubDataAdditional2 = JSON.stringify({
					'canOpen': $scope.Column.canOpen,
					'openWidth': $scope.Column.openWidth,
					'showAll': $scope.Column.showAll,
					'IsTable': $scope.Column.IsTable
				});
			} else if ($scope.Column.DataType == 5) {
				$scope.Column.DataAdditional2 = JSON.stringify({
					'showAll': $scope.Column.showAll,
					'portfolioId': $scope.Column.portfolioId,
					'canOpen': $scope.Column.canOpen,
					'openWidth': $scope.Column.openWidth,
					'table': $scope.Column.IsTable,
					'advancedFilter': $scope.Column.advancedFilter,
					'luceneScore': $scope.Column.luceneScore,
					'luceneMaxCount': $scope.Column.luceneMaxCount,
					'fromProcessColumnName': $scope.Column.fromProcessColumnName,
					'fromProcessColumnId': $scope.Column.fromProcessColumnId,
					'targetProcessColumnName': $scope.Column.targetProcessColumnName,
					'targetProcessColumnId': $scope.Column.targetProcessColumnId,
					'targetProcessName': $scope.Column.targetProcessName

				});
				if (typeof $scope.Column.targetProcessName != "undefined") findFilteredColumns($scope.Column.targetProcessName);
				if ($scope.Column.RelativeColumnId != null) {
					$scope.Column.InputMethod = 3
				} else {
					$scope.Column.InputMethod = 0
				}
			} else if ($scope.Column.DataType == 6) {
				$scope.Column.DataAdditional2 = JSON.stringify({
					'fromProcessColumnName': $scope.Column.fromProcessColumnName,
					'fromProcessColumnId': $scope.Column.fromProcessColumnId,
					'targetProcessColumnName': $scope.Column.targetProcessColumnName,
					'targetProcessColumnId': $scope.Column.targetProcessColumnId,
					'targetProcessName': $scope.Column.targetProcessName,
					'targetListColumn': $scope.Column.targetListColumn
				});
			} else if ($scope.Column.DataType == 12) {
				$scope.Column.DataAdditional2 = JSON.stringify({
					'note': $scope.Column.note,
					'infinite': $scope.Column.infinite,
					'preventatt': $scope.Column.preventatt
				});
			} else if ($scope.Column.DataType == 17) {
				let da2 = JSON.parse($scope.Column.DataAdditional2);
				if (da2 == null) da2 = {};
				da2.isAlert = $scope.Column.isAlert;
				$scope.Column.DataAdditional2 = JSON.stringify(da2);
			} else if ($scope.Column.DataType == 16 && $scope.Column.SubDataType == 5) {
				let da2 = JSON.parse($scope.Column.DataAdditional2);

				if (da2 == null) da2 = {};

				da2.canOpen = $scope.Column.canOpen;
				da2.openWidth = $scope.Column.openWidth;
				da2.cacheEnabled = $scope.Column.cacheEnabled;
				$scope.Column.DataAdditional2 = JSON.stringify(da2);

				$scope.Column.SubDataAdditional2 = JSON.stringify({
					'showAll': $scope.Column.showAll,
					'IsTable': $scope.Column.IsTable
				})
			} else if ($scope.Column.DataType == 16) {
				let da2 = JSON.parse($scope.Column.DataAdditional2);
				if (da2 == null) da2 = {};
				da2.cacheEnabled = $scope.Column.cacheEnabled;
				$scope.Column.DataAdditional2 = JSON.stringify(da2);

			} else if ($scope.Column.DataType == 18) {
				$scope.Column.DataAdditional2 = JSON.stringify({
					'itemColumnId': $scope.Column.portfolioColumnId,
					'portfolioId': $scope.Column.portfolioId,
					'openWidth': $scope.Column.openWidth,
					'showSum': $scope.Column.showSum,
					'showSumCumulative': $scope.Column.showSumCumulative,
					'showAddDelete': $scope.Column.showAddDelete,
					'actionId': $scope.Column.actionId,
					'canOpen': $scope.Column.canOpen,
					'useDialog': $scope.Column.useDialog,
					'archiveFieldName': $scope.Column.archiveFieldName,
					'parentFieldName': $scope.Column.parentFieldName,
					'ignoreExcelExport': $scope.Column.ignoreExcelExport
				});
			} else if ($scope.Column.DataType == 21) {
				let validTypes = [];
				let sortable = false;
				for (let i = 0, l = $scope.RMTypes.length; i < l; i++) {
					let t = $scope.RMTypes[i];
					if (t.name && t.type) {
						validTypes.push(t);
						if (t.draggable) {
							sortable = true;
						}

						if(t.actionId && t.actionId == "true"){
							delete t['actionId']
						}

						if(t.containerId && t.containerId == "true"){
							delete t['containerId']
						}
					}
				}
				
				$scope.Column.DataAdditional2 = angular.toJson({
					definitions: validTypes,
					options: {
						useDialog: $scope.Column.useDialog,
						sortable: sortable,
						openWidth: $scope.Column.openWidth,
						portfolioId: $scope.Column.portfolioId,
						ownerColumnId: $scope.Column.portfolioColumnId,
						archiveFieldName: $scope.Column.archiveFieldName,
						parentFieldName: $scope.Column.parentFieldName,
						ignoreExcelExport: $scope.Column.ignoreExcelExport,
						forceAllOpen: $scope.Column.forceAllOpen
					}
				});
			} else if ($scope.Column.DataType == 65) {
				$scope.Column.DataAdditional2 = JSON.stringify({
					'endDateColumnId': $scope.Column.endDateColumnId,
					'milestoneColumnId': $scope.Column.milestoneColumnId,
					'descriptionColumnId': $scope.Column.descriptionColumnId,
					'dependencyColumnId': $scope.Column.dependencyColumnId
				});
			}
			SaveService.tick();
		};
		let newValuesArray = [];
		let changedItems = [];

		let setChangedColumnForSave = function () {
			newValuesArray.push({
				ItemColumnId: $scope.Column.ItemColumnId,
				ProcessContainerId: DialogParameters.ProcessContainerId,
				column: $scope.Column,
			});

			let noDuplicateObjects = {};

			let l = newValuesArray.length;
			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.type + '_' + item.ItemColumnId + '_' + item.ProcessContainerId + '_' + item.column] = item;
			}
			let nonDuplicateArray = [];
			for (let item in noDuplicateObjects) {
				if (angular.isUndefined(nonDuplicateArray)) {
					nonDuplicateArray = [];
				}
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
			if (changedItems.length > 0) saveItemColumn();
		};

		let queryRefreshRequired = false;

		let saveItemColumn = function () {
			let itemColumnArray = [];
			angular.forEach(changedItems, function (item) {
				itemColumnArray.push(item.column);
			});

			if ($scope.Column.DataType == 23 && $scope.Column.DataAdditional != null && typeof $scope.Column.DataAdditional != 'undefined') {
				$scope.Column.DataAdditional = JSON.stringify($scope.Column.DataAdditional);
			}

			SettingsService.ItemColumnsSave(StateParameters.process, itemColumnArray).then(function () {
				if (changedItems.length > 0) SettingsService.notifyItemColumnSave();
				if ($scope.Column.DataType == 16 && queryRefreshRequired) {
					getNewSubQueryValues().then(function () {
						$scope.validateSubquery();
					});
					queryRefreshRequired = false;
				} else if ($scope.Column.DataType == 16) {
					$scope.validateSubquery();
				}

				SettingsService.ProcessContainerColumns(StateParameters.process).then(function (columns) {
					angular.forEach(columns, function (key) {
						angular.forEach(key, function (key2) {
							if (key2.Validation != null && key2.Validation.length > 0) {
								let result = '';

								let validation = JSON.parse(key2.Validation);

								angular.forEach(validation, function (key3, value3) {
									result += value3 + ': ' + key3 + ', ';
								});
								key2.ValidationModified = result.substring(0, result.length - 2);
							}
						});
					});
				});
				changedItems = [];
			});
		};

		$scope.confirmDataAdditionalChange = function () {
			$scope.dataAdditionalChangeConfirmed = true;
		};

		$scope.allowSave = function () {
			return true;
		};

		let getNewSubQueryValues = function () {
			return ApiService.v1api('settings/RegisteredSubQueries').get([StateParameters.process, $scope.Column.DataAdditional, $scope.Column.ItemColumnId]).then(function (query) {
				$scope.optionsLoaded = true;
				$scope.query = query;
			});
		};

		$scope.validateSubquery = function () {
			$scope.equationValid = -1;
			ApiService.v1api('settings/RegisteredSubQueries/test').get([StateParameters.process, $scope.Column.DataAdditional, $scope.Column.ItemColumnId]).then(function (result) {
				$scope.equationValid = 0;
				if (result) $scope.equationValid = 1;
			});
		};

		$scope.changeSubQueryVal = function (q) {
			if (q.ReQuery) {
				queryRefreshRequired = true;
				SaveService.bypassTick().then(function () {
					$scope.optionsLoaded = false;
				});
			}
			let da2 = {};
			angular.forEach($scope.query, function (q) {
				da2[q.Name] = q.Value;
			});

			da2.canOpen = $scope.Column.canOpen;
			da2.openWidth = $scope.Column.openWidth;
			da2.cacheEnabled = $scope.Column.cacheEnabled;

			$scope.Column.DataAdditional2 = JSON.stringify(da2);
			$scope.changeProperties();
		};

		$scope.updateRMTypes = function () {
			let dif = $scope.numberOfRMTypes - $scope.RMTypes.length;
			if (dif > 0) {
				while (dif--) {
					$scope.RMTypes.push({
						type: '',
						name: '',
						rules: []
					});
				}
			} else {
				while (dif++) {
					$scope.RMTypes.pop();
				}
			}

			$scope.updateTypeOptions();
		};

		$scope.updateTypeOptions = function () {
			$scope.typeOptions = [{name: 'ROOT', value: 'root'}];

			for (let i = 0, l = $scope.RMTypes.length; i < l; i++) {
				let t = $scope.RMTypes[i];

				if (t.name && t.type) {
					$scope.typeOptions.push({
						name: t.name,
						value: t.type
					});
				}
			}

			let i = $scope.RMTypes.length;
			while (i--) {
				let j = $scope.RMTypes[i].rules.length;
				while (j--) {

					let k = $scope.typeOptions.length;
					while (k--) {
						if ($scope.typeOptions[k].value == $scope.RMTypes[i].rules[j]) {
							break;
						}
					}

					if (k === -1) {
						$scope.RMTypes[i].rules.splice(j, 1);
					}
				}
			}

			$scope.changeProperties();
		};
	}

	ContainersAddNewColumnController.$inject = ['$rootScope', '$scope', 'DialogParameters', 'MessageService', 'SettingsService', 'ItemColumnsData', 'Lists', 'Processes', 'Common', '$filter', 'ProcessContainerColumns', 'Translator', 'UpdateConnection', 'UnlinkedColumns', 'ProcessService', 'ListsService', 'ViewService', 'PortfolioService', 'ItemColumnService', 'ProcessesService', 'ProcessActionsService', 'SubQueries'];

	function ContainersAddNewColumnController($rootScope, $scope, DialogParameters, MessageService, SettingsService, ItemColumnsData, Lists, Processes, Common, $filter, ProcessContainerColumns, Translator, UpdateConnection, UnlinkedColumns, ProcessService, ListsService, ViewService, PortfolioService, ItemColumnService, ProcessesService, ProcessActionsService, SubQueries) {

		$scope.connectingColumns = [];
		SettingsService.getLookupConnectorColumns(DialogParameters.StateParameters.$$originalProcess).then(function(lCols){
			$scope.connectingColumns = lCols;
		});

		let parameters = DialogParameters;
		let tickTimeout;
		let selectedConnectingColumn;
		$scope.selectedParentProcess = [];
		$scope.parentsColumns = [];
		$scope.selectedParentColumn = [];
		$scope.connectingItemColumn = [];
		$scope.selectedChildColumn = [];
		$scope.portfolios = [];
		$scope.selectedPortfolio = [];
		$scope.subQueries = SubQueries;
		$scope.chooseQuery = true;

		$scope.matrixProcesses = _.filter(Processes, function (o) {
			return o.ProcessType == 4
		});

		$scope.giveOptions = function () {
			if ($scope.DataType != 22) return $scope.Processes;
			return $scope.matrixProcesses;
		}

		$scope.listTypes = [];
		let adminItemId = 0;
		ListsService.getProcessItems('list_admin_types', 'order').then(function (r) {
			$scope.listTypes = r;
			adminItemId = _.find($scope.listTypes, function (o) {
				return o.is_admin == 1;
			}).item_id;
		})

		let templatesByDataType = {
			0: 'GenericStringAggregateFunction',
			1: 'GenericNumberAggregateFunction',
			2: 'GenericNumberAggregateFunction',
			3: 'GenericDateAggregateFunction',
			4: 'GenericLongStringAggregateFunction',
			5: 'GenericProcessAggregateFunction',
			6: 'GenericListAggregateFunction',
			13: 'GenericDateAggregateFunction'
		};

		$scope.operationType = 3;
		$scope.aggregates = [
			{value: 1, name: 'SUM'},
			{value: 2, name: 'AVG'},
			{value: 3, name: 'MAX'},
			{value: 4, name: 'MIN'},
			{value: 5, name: 'COUNT'}
		];

		let setSqSettings = function () {
			if ($scope.DataType == 16 && $scope.DataAdditional == 'GenericNumberAggregateFunction') {
				return '{"@GenericAggregateFunction":' + $scope.operationType[0] + ',"@SourceProcessColumnId":' + $scope.connectingItemColumn[0] + ',"@SourceColumnId":' + $scope.selectedParentColumn[0] + '}';
			}
			return '{"@SourceProcessColumnId":' + $scope.connectingItemColumn[0] + ',"@SourceColumnId":' + $scope.selectedParentColumn[0] + '}';
		}

		$scope.setDataTableSettings = function () {
			$scope.DataAdditional2 = '{"portfolioId":' + $scope.selectedPortfolio[0] + ',"itemColumnId":' + $scope.selectedChildColumn[0] + '}';
		}

		let setConnectingColumn = function () {
			_.each(ItemColumnsData, function (ic) {
				if (ic.Name == 'owner_item_id') {
					selectedConnectingColumn = ic;
					$scope.connectingItemColumn = [ic.ItemColumnId];
					$scope.getParentProcess();
				}
			})
		}

		$scope.getParentProcess = function () {
			if (selectedConnectingColumn == null) {
				_.each(ItemColumnsData, function (ic) {
					if (ic.ItemColumnId == $scope.connectingItemColumn[0]) selectedConnectingColumn = ic;
				})
			}
			if (selectedConnectingColumn.DataType == 1) {
				ProcessService.deduceParentProcess(DialogParameters.Process, selectedConnectingColumn.Name).then(function (name) {
					if (name.length) {
						$scope.selectedParentProcess = [_.find(Processes, function (o) {
							return o.ProcessName == name;
						}).ProcessName];
						$scope.getParentColumns();
					}
				});
			}
		}

		$scope.getParentColumns = function () {
			if ($scope.selectedParentProcess[0].length == 0) return;
			SettingsService.ItemColumns($scope.selectedParentProcess[0]).then(function (pCols) {
				$scope.parentsColumns = pCols;
			})
		}

		$scope.getChildColumns = function () {
			if ($scope.DataAdditional.length == 0) return;
			SettingsService.ItemColumns($scope.DataAdditional).then(function (cCols) {
				$scope.childColumns = cCols;
				_.each(cCols, function (ic) {
					if (ic.Name == 'owner_item_id') {
						$scope.selectedChildColumn = [ic.ItemColumnId];
					}
				})
			})
			PortfolioService.getPortfolios($scope.DataAdditional).then(function (portfolios) {
				$scope.portfolios = portfolios;
			})

		}

		$scope.required = null;
		$scope.readOnly = null;
		$scope.useConditions = null;

		$scope.dataDescriptions = {
			'TEXT': [{title: 'DATA_NVARCHAR', name: 'SHORT_TEXT_DESC', value: 0},
				{title: 'DATA_TEXT', name: 'LONG_TEXT_DESC', value: 4},
				{title: 'DATA_RICH', name: 'RICH_TEXT_DESC', value: 23},
				{title: 'DATA_DESCRIPTION', name: 'DESC_TEXT_DESC', value: 17},
				{title: 'DATA_TRANSLATION', name: 'TRANS_TEXT_DESC', value: 24}],
			'NUMBERS': [{title: 'DATA_INT', name: 'INT_DESC', value: 1},
				{title: 'DATA_FLOAT', name: 'FLOAT_DESC', value: 2},
				{title: 'DATA_DATE', name: 'DATE_DESC', value: 3},
				{title: 'DATA_DATETIME', name: 'DATE_TIME_DESC', value: 13}],
			'SELECTIONS': [{title: 'DATA_PROCESS', name: 'PROCESS_DESC', value: 5},
				{title: 'DATA_MATRIX', name: 'MATRIX_DESC', value: 22},
				{title: 'DATA_TAGS', name: 'TAG_DESC', value: 19},
				{title: 'DATA_RATING', name: 'RATING_DESC', value: 9}],
			'REFERENCES': [
				{title: 'DATA_ATTACHMENT', name: 'ATTACHMENT_DESC', value: 10},
				{title: 'DATA_LINK', name: 'LINKS_DESC', value: 15},
				{title: 'DATA_CHART', name: 'CHART_DESC', value: 25}],
			'OTHER': [{title: 'EQUATION', name: 'EQ_DESC', value: 20},
				{title: 'DATA_BUTTON', name: 'BUTTON_DESC', value: 8},
				{title: 'DATA_PROGRESS', name: 'PROGRESS_DESC', value: 60},
				{title: 'DATA_MILESTONE', name: 'MILESTONE_DESC', value: 65},
				{title: 'DATA_COMMENT', name: 'COMMENT_DESC', value: 12}]
		};

		$scope.selected = null;

		$scope.selectDataType = function (v) {
			$scope.DataType = v;
		}

		$scope.setQueryType = function () {
			if ($scope.selectedParentColumn.length == 0) return
			let c = _.find($scope.parentsColumns, function (o) {
				return o.ItemColumnId == $scope.selectedParentColumn[0];
			});
			$scope.DataAdditional = templatesByDataType[c.DataType]
			$scope.DataAdditional2 = setSqSettings();
		}
		$scope.newListName = {};
		$scope.listOrderBy = [];
		$scope.touched = false;
		$scope.newProcessVariable = "";

		$scope.setVariable = function (type: number) {
			$scope.touched = true;
			if (angular.isDefined($scope.newListName[$rootScope.me.locale_id]) && $scope.newListName[$rootScope.clientInformation.locale_id].length > 0) {
				$scope.newProcessVariable = type == 1
					? 'list_' + Common.formatVariableName($scope.newListName[$rootScope.clientInformation.locale_id])
					: Common.formatVariableName($scope.newListName[$rootScope.clientInformation.locale_id]);
				SettingsService.checkDuplicateVariable('process', $scope.newProcessVariable).then(function (result) {
					if (result != '') {
						$scope.disabledList = false;
					} else {
						$scope.disabledList = 'in_use';
					}
				});
			}
		}

		$scope.newListData = {
			LanguageTranslation: null,
			LanguageTranslationRaw: null,
			ListProcess: 1,
			SystemProcess: 0,
			Variable: null,
			ParentProcess: null,
			ListType: null,
			Compare: 0
		};

		$scope.orderByColumns = [{
			'LanguageTranslation': 'ORDER_NO',
			'Name': 'order_no'
		}, {'LanguageTranslation': 'LIST_ITEM', 'Name': 'list_item'}];
		let l;
		let addNewList = function () {
			$scope.newListData.LanguageTranslation = $scope.newListName;
			$scope.newListData.LanguageTranslationRaw = $scope.newListName;
			$scope.newListData.Variable = 'list_' + $scope.newProcessVariable;
			$scope.newListData.ListOrder = $scope.listOrderBy.length ? $scope.listOrderBy[0] : null;
			$scope.newListData.ListType = $scope.listType[0];
			ListsService.ListProcessesSave('', $scope.newListData).then(function (response) {
				l = response;
				Lists.push(response);
				if ($scope.addItems) goToListItems();
			});
		}

		$scope.addItemColumn = false;
		$scope.disabled = true;
		$scope.unLinked = parameters.Unlinked;
		if ($scope.unLinked == true) {
			$scope.addItemColumn = true;
		}

		let findColumnTranslation = function (itemColumnId) {
			let columnToFind = _.find($scope.ItemColumnsData, function (o) {
				return o.ItemColumnId == itemColumnId;
			});
			if (columnToFind.LanguageTranslationRaw.hasOwnProperty($rootScope.clientInformation.locale_id)) {
				return columnToFind.LanguageTranslationRaw[$rootScope.clientInformation.locale_id]
			}
			return columnToFind.Name;
		}

		$scope.LanguageTranslationRaw = {};
		$scope.Variable = null;
		$scope.ItemColumnsData = ItemColumnsData;
		$scope.Lists = Lists;
		$scope.Processes = Processes;
		$scope.ItemColumnIds = [];
		$scope.dataType = Common.getDataTypes();


		let selectedType = null;

		$scope.changeDialogValue = function (type) {
			if ($scope.DataAdditional == 'create_new') {
				$scope.useNewList = true;
			} else {
				$scope.useNewList = false;
				if ($scope.DataType == 18) $scope.getChildColumns()
			}

			$scope.disabled = true;
			if (type == 'AddItemColumn') {
				$scope.DataType = 0;
				$scope.Variable = null;
				$scope.LanguageTranslation = {};
				$scope.ItemColumnIds = [];
			}
			if (type != 'AddItemColumn') {
				$scope.disabled = false;
			}
			if (type == 'DataType') {
				$scope.ItemColumnIds = [];
				if ($scope.DataType == 16) setConnectingColumn();
			}

			if ($scope.addItemColumn == true) {

				if (type == 'Translation' && angular.isDefined($scope.LanguageTranslationRaw[$rootScope.clientInformation.locale_id]) && $scope.LanguageTranslationRaw[$rootScope.clientInformation.locale_id].length > 0) {
					$scope.Variable = Common.formatVariableName($scope.LanguageTranslationRaw[$rootScope.clientInformation.locale_id]);
				} else if (type == 'Variable') {
					$scope.Variable = Common.formatVariableName($scope.Variable);
				}

				if (angular.isDefined($scope.Variable) && $scope.Variable != null && $scope.Variable.length > 50) {
					$scope.Variable = $scope.Variable.substr(0, 50);
				}

				if (angular.isDefined($scope.Variable) && $scope.Variable != null && $scope.Variable.length > 0) {
					clearTimeout(tickTimeout);
					tickTimeout = setTimeout(function () {
						SettingsService.checkDuplicateVariable('itemColumn', $scope.Variable, parameters.StateParameters.$$originalProcess).then(function (data) {
							if (data != '') {
								$scope.Variable = data;
								$scope.disabled = false;
							} else
								$scope.disabled = 'in_use';
						});

					}, 500);
				}
			}

			if ($scope.Variable == null && type != 'ItemColumn') {
				$scope.disabled = 'in_use';
			}

			return $scope.disabled;
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		let whatsMyDataType = function (itemColumnId) {
			return _.find($scope.ItemColumnsData, function (o) {
				return o.ItemColumnId == itemColumnId;
			}).DataType;
		};

		$scope.listTab = false;

		$scope.setAsAdvancedTable = function () {
			$scope.DataType = 21;
		}

		let originalLists = angular.copy(Lists);
		let originalProcesses = angular.copy(Processes);
		$scope.setSettings = function (identifier: string) {

			selectedType = identifier;

			if (identifier == 'sq') {
				$scope.DataType = 16;
				setConnectingColumn();
			}

			if (identifier == 'existing') {
				$scope.addItemColumn = false;
			} else {
				$scope.addItemColumn = true;
			}
			if (identifier == 'list') {
				$scope.DataType = 6;
				$scope.listTab = true;
				if (_.findIndex($scope.Lists, function (o) {
					return o.ProcessName == 'create_new';
				}) == -1) {
					$scope.Lists.unshift({ProcessName: 'create_new', LanguageTranslation: 'CREATE_NEW_LIST'})
				}
				$scope.useNewList = true;
				$scope.DataAdditional = 'create_new';
			} else {
				$scope.Lists = originalLists;
			}

			if (identifier == 'table') {
				$scope.DataType = 18;
				$scope.tableType = [1];
				$scope.useNewList = true;
				$scope.Processes.unshift({ProcessName: 'create_new', LanguageTranslation: 'CREATE_NEW_TABLE_PROCESS'})
				$scope.DataAdditional = 'create_new';
			} else {
				$scope.Processes = originalProcesses;
			}
		}

		let goToListItems = function () {
			let nl = {Level: 0, Process: ""};
			nl.Level = 0;
			nl.Process = l;
			let p = {
				'Recursive': false,
				'process': l.ProcessName,
				'thisList': nl,
				'adminPage': true
			}
			ViewService.view('listsEdit', {params: p}, {split: true});
		}

		let executeSave = function () {
			if($scope.DataAdditional == "") $scope.DataAdditional = null;
			if ($scope.addItemColumn == true && $scope.disabled == false) {
				$scope.disabled = true;
				if ($scope.Variable.length > 0 && $scope.LanguageTranslationRaw[$rootScope.clientInformation.locale_id] != '') {

					let ItemColumn: Client.ItemColumn = {
						Process: parameters.StateParameters.$$originalProcess,
						ProcessContainerIds: [parameters.ProcessContainerId],
						ItemColumnId: 0,
						OrderNo: parameters.OrderNo,
						LanguageTranslationRaw: $scope.LanguageTranslationRaw,
						LanguageTranslation: $scope.LanguageTranslationRaw,
						Variable: $scope.Variable,
						DataType: $scope.DataType,
						InConditions: $scope.useConditions == true ? true : false
					};

					if (angular.isDefined($scope.DataAdditional) && $scope.DataAdditional.length > 0) {
						ItemColumn.DataAdditional = $scope.DataAdditional == 'create_new' ? $scope.newProcessVariable : $scope.DataAdditional;
					}

					if ($scope.DataType == 6 && $scope.newProcessVariable && $scope.newProcessVariable.length){
						addNewList();
						ItemColumn.DataAdditional = 'list_' + ItemColumn.DataAdditional;
					}

					if ($scope.DataType == 21) {
						$scope.DataAdditional2 = '{"definitions":[{"type":"1","name":{"en-GB":"New row"},"rules":["root"]}],"options":{"sortable":false, "portfolioId":' + $scope.selectedPortfolio[0] + ',"ownerColumnId":' + $scope.selectedChildColumn[0] + '}}';
					}

					if (angular.isDefined($scope.DataAdditional2) && $scope.DataAdditional2.length > 0) {
						ItemColumn.DataAdditional2 = $scope.DataAdditional2;
					}

					SettingsService.ItemColumnsAdd(parameters.StateParameters.$$originalProcess, ItemColumn).then(function (column: Client.ItemColumn) {
						if ($scope.unLinked == true) {
							UnlinkedColumns.push(column);
						}
						SettingsService.ProcessContainerColumns(parameters.StateParameters.$$originalProcess, parameters.ProcessContainerId).then(function (data) {
							ProcessContainerColumns[parameters.ProcessContainerId] = data;
							_.each(ProcessContainerColumns[parameters.ProcessContainerId], function (containerColumn) {
								if (containerColumn.ItemColumn.ItemColumnId == column.ItemColumnId) {
									UpdateConnection(containerColumn.ItemColumn.ItemColumnId, containerColumn.ProcessContainerColumnId, column.DataType, column.LanguageTranslationRaw);

									if ($scope.readOnly != null || $scope.required != null) {
										containerColumn.Validation.ReadOnly = $scope.readOnly;
										containerColumn.Validation.Required = $scope.required;
										ItemColumnService.saveContainerColumn(parameters.StateParameters.$$originalProcess, [containerColumn]);
									}
								}
							});

							column.Translation = Translator.translation(column.LanguageTranslation) + ' [' + Common.getDataTypeTranslation(column.DataType) + ']';
							column.TranslationRaw = Translator.translation(column.LanguageTranslationRaw) + ' [' + Common.getDataTypeTranslation(column.DataType) + ']';
							$scope.ItemColumnsData.push(column);
							$scope.ItemColumnsData = $filter('orderBy')($scope.ItemColumnsData, 'translation');
							MessageService.closeDialog();
						});
					});
				}
			} else {
				if ($scope.ItemColumnIds.length > 0 && !$scope.disabled) {
					$scope.disabled = true;
					let newColumns = [];
					angular.forEach($scope.ItemColumnIds, function (value) {
						let newColumn = {
							OrderNo: parameters.OrderNo,
							ProcessContainerId: parameters.ProcessContainerId,
							ProcessContainerColumnId: 0,
							Process: parameters.Process,
							ItemColumn: {
								ItemColumnId: value,
								DataType: whatsMyDataType(value),
								Validation: {
									AfterMonths: null,
									BeforeMonths: null,
									DateRange: null,
									CalendarConnection: null,
									Label: "{}",
									Min: null,
									Max: null,
									ParentId: null,
									Precision: null,
									ReadOnly: null,
									Required: null,
									Rows: null,
									Spellcheck: null,
									Suffix: null,
									Unselectable: null
								}
							}
						};
						newColumns.push(newColumn);

					});
					SettingsService.ProcessContainerColumnsAdd(parameters.Process, newColumns).then(function (columns) {
						_.each(columns, function (column) {
							let translation_ = {}
							translation_[$rootScope.clientInformation.locale_id] = findColumnTranslation(column.ItemColumn.ItemColumnId);
							UpdateConnection(column.ItemColumn.ItemColumnId, column.ProcessContainerColumnId, column.ItemColumn.DataType, translation_);
						});

						if (angular.isUndefined(ProcessContainerColumns[parameters.ProcessContainerId])) {
							ProcessContainerColumns[parameters.ProcessContainerId] = [];
						}
						for (let i = 0; i < columns.length; i++) {
							ProcessContainerColumns[parameters.ProcessContainerId].push(columns[i]);
						}

						setTimeout(function () {
							MessageService.closeDialog();
						}, 500);
					});
				}
			}
		}

		$scope.save = function () {
			if (($scope.DataType == 21 || $scope.DataType == 18) && $scope.DataAdditional == 'create_new') {
				let data = {
					ProcessType: 5,
					Variable: $scope.newProcessVariable,
					LanguageTranslation: $scope.newListName,
					ExecuteStoredProcedure: 1
				};
				ProcessesService.new(data).then(function (process) {
					Processes.push(process);
					PortfolioService.getPortfolios().then(function (portfolios) {
						let portfolioId = _.find(portfolios, function(o) { return o.Process == process.ProcessName; }).ProcessPortfolioId;
						ProcessActionsService.getItemColumns(process.ProcessName).then(function (columns) {
							let pCols = [];
							_.each(columns, function(col, key){
								if(col.Name != "title" && col.Name != "current_state"){
								pCols.push({
									ProcessPortfolioId: portfolioId,
									OrderNo: key,
									ItemColumn: {ItemColumnId: col.ItemColumnId},
									ProcessPortfolioColumnGroupId: 0,
									Width: 250,
									IsDefault: true,
									UseInFilter: true
								})
								}
							});

							let condition = {
								LanguageTranslation: {'en-GB': 'meta read'},
								Process: process.ProcessName,
								States: ['meta.read'],
								Features: ['meta'],
								ConditionId: 0,
								Translation: {},
								OrderNo: 'U',
								ConditionGroupId: 0
							};
							SettingsService.ConditionsAdd(process.ProcessName, condition).then(function (response) {
								SettingsService.ProcessPortfolioColumnsAdd(process.ProcessName, portfolioId, pCols).then(function () {
									$scope.DataAdditional = process.ProcessName;
									let m = _.find(columns, function (c) {
										return c.Name == 'owner_item_id'
									})
									$scope.selectedPortfolio[0] = portfolioId;
									$scope.selectedChildColumn[0] = m.ItemColumnId;
									$scope.DataAdditional2 = '{\"itemColumnId\":' + m.ItemColumnId + ',"portfolioId":' + portfolioId + '}';
									executeSave();
								})
							});
						});
					});
				});
			} else {
				executeSave();
			}
		}

		$scope.checkInputs = function(){
			if(selectedType == 'newItemColumn' && $scope.DataType != null){
				return true;
				//return (($scope.DataType != 22 && $scope.DataType != 5) ||($scope.DataType == 22 || $scope.DataType == 5))
			}
			if(selectedType == 'existing' && $scope.ItemColumnIds.length) return true;
			if(selectedType == 'list'){
				if($scope.DataAdditional.length && $scope.DataAdditional != 'create_new') return true;
				if($scope.DataAdditional == 'create_new' && !_.isEmpty($scope.newListName) && $scope.listOrderBy.length && $scope.newProcessVariable.length && $scope.listType.length) return true;
			}
			if(selectedType == 'table'){
				if($scope.DataAdditional.length && $scope.DataAdditional != 'create_new' && $scope.selectedChildColumn.length && $scope.selectedPortfolio.length) return true;
				if($scope.DataAdditional == 'create_new' && !_.isEmpty($scope.newListName) && $scope.newProcessVariable.length) return true;
			}
			if(selectedType == 'sq'){
				if($scope.chooseQuery == true && typeof $scope.DataAdditional != 'undefined' && $scope.DataAdditional.length) return true;
				let stuffSelected = $scope.connectingItemColumn.length && $scope.selectedParentColumn.length && $scope.selectedParentProcess.length;
				if(stuffSelected && $scope.DataAdditional != 'GenericNumberAggregateFunction') return true;
				if(stuffSelected && $scope.DataAdditional == 'GenericNumberAggregateFunction' && $scope.operationType.length) return true;
			}
			return false;
		}

		$scope.clearQuerySettings = function(){
			$scope.selectedPortfolio = [];
			$scope.selectedChildColumn = [];
			$scope.selectedParentColumn = [];
			$scope.DataAdditional2 = undefined;
		}
	}
})();