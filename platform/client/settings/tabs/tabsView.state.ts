
	(function () {
		'use strict';

		angular
			.module('core.settings')
			.config(TabsSettingsConfig);

		TabsSettingsConfig.$inject = ['ViewsProvider'];

		function TabsSettingsConfig(ViewsProvider) {
			ViewsProvider.addView('settings.tabs', {
				controller: 'TabsSettingsController',
				template: 'settings/tabs/tabsView.html',
				resolve: {
					TabsNotInPhases: function (SettingsService, StateParameters) {
						return SettingsService.ProcessGroupTabs(StateParameters.process).then(function (processGroups) {
							return SettingsService.ProcessTabs(StateParameters.process).then(function (result) {
								var tabs = [];
								var unique = {};
								angular.forEach(result, function (tab) {
									if (!unique[tab.Variable] && tab.ProcessTabGroups.length == 0) {
										tabs.push(tab);
										unique[tab.Variable] = tab;
									}
								});
								return tabs;
							});
						});
					}
				}
			});
		}
	})();