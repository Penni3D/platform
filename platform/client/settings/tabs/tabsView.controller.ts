(function () {
	'use strict';

	angular
		.module('core.settings')
		.controller('TabsSettingsController', TabsSettingsController);

	TabsSettingsController.$inject = ['$scope', 'SettingsService', 'MessageService', 'Common', 'SaveService', 'StateParameters', 'TabsNotInPhases', 'ViewService'];
	function TabsSettingsController($scope, SettingsService, MessageService, Common, SaveService, StateParameters, TabsNotInPhases, ViewService) {

		$scope.tabsNotInPhase = TabsNotInPhases;

		$scope.deleteTab = function(processTabId, tabIndex){
			MessageService.confirm("DELETE_CONFIRMATION", function () {
				SettingsService.ProcessTabsDelete(StateParameters.process, processTabId);
				$scope.tabsNotInPhase.splice(tabIndex, 1);
			});
		};

		var newValuesArray = [];
		var changedGroupTabArray = [];
		var changedItems = [];
		changedItems['tab'] = [];

		SaveService.subscribeSave($scope, function () {
			saveTabs();
		});

		var saveTabs = function () {
			var processTabsArray = [];
			angular.forEach(changedItems['tab'], function (tab) {
				var processTabsArray = [];
				var index = Common.getIndexOf($scope.tabsNotInPhase, tab.ProcessTabId, 'ProcessTabId');
				processTabsArray.push($scope.tabsNotInPhase[index]);
				SettingsService.ProcessTabsSave(StateParameters.process, processTabsArray);
			});
			newValuesArray = [];
			changedItems['tab'] = [];
		};

		$scope.showContainers = function (ProcessTabId, index) {
			$scope.selectedTabHighlight = index;
			$scope.ProcessTabId = ProcessTabId;
			setTimeout(function () {
				ViewService.viewAdvanced('settings.fields.tabs', {
					params: {
						process: StateParameters.process,
						ProcessTabId: ProcessTabId
					}
				}, {targetId: 'containersView-' + ProcessTabId, ParentViewId: StateParameters.ViewId});
			}, 0);
		};

		$scope.changeTabTranslation= function (params) {
			changedGroupTabArray.push({ProcessTabId: params.ProcessTabId});
			var noDuplicateObjects = {};
			var l = changedGroupTabArray.length;

			for (var i = 0; i < l; i++) {
				var item = changedGroupTabArray[i];
				noDuplicateObjects[item.ProcessTabId] = item;
			}

			var nonDuplicateArray = [];
			for (var item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems['tab'] = nonDuplicateArray;
		};
	}
})();