﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.config(PortfoliosConfig);

	PortfoliosConfig.$inject = ['ViewsProvider'];

	function PortfoliosConfig(ViewsProvider) {
		ViewsProvider.addView('settings.portfolios', {
			controller: 'PortfoliosSettingsController',
			template: 'settings/portfolios/portfolios.html',
			resolve: {
				Portfolios: function (SettingsService, StateParameters) {
					return SettingsService.ProcessPortfolios(StateParameters.process, 0).then(function (portfolios) {
						angular.forEach(portfolios, function (portfolio) {
							SettingsService.ProcessPortfoliosUserGroups(StateParameters.process, portfolio.ProcessPortfolioId).then(function (groups) {
								portfolio.ProcessUserGroupIds = groups;
							});
						});
						return portfolios;
					});
				},
				PortfoliosSelects: function (SettingsService, StateParameters) {
					return SettingsService.ProcessPortfolios(StateParameters.process, 1);
				},
				PortfoliosSelectsHide: function (SettingsService, StateParameters) {
					return SettingsService.ProcessPortfolios(StateParameters.process, 10);
				},
				ProcessConditions: function (ProcessService, StateParameters, Translator) {
					return ProcessService.getConditions(StateParameters.process).then(function (conditions) {
						let ret = {};
						let i = conditions.length;
						while (i--) {
							let c = conditions[i];
							c.translation = Translator.translation(c.LanguageTranslation);
							ret[c.ConditionId] = c;
						}
						return ret;
					});
				},
				Charts: function (StateParameters, SettingsService) {
					return SettingsService.ProcessDashboardGetChartsForProcess(StateParameters.process);
				},
				Dashboards: function (SettingsService, StateParameters) {
					return SettingsService.ProcessDashboards(
						StateParameters.process,
						StateParameters.portfolioId);
				},
				Features: function (SettingsService, StateParameters) {
					return SettingsService.ProcessGroups(StateParameters.process).then(function (groups) {
						let features = [];
						angular.forEach(groups, function (group) {
							angular.forEach(group.SelectedFeatures, function (feature) {
								features.push(feature);
							});
						});

						return _.uniqBy(features, 'DefaultState');
					});
				},
				UserGroups: function (SettingsService) {
					return SettingsService.UserGroups('user_group');
				},
				ProcessTabs: function (SettingsService, StateParameters) {
					return SettingsService.ProcessTabs(StateParameters.process).then(function(tabs){
						return _.uniqBy(tabs, 'ProcessTabId')
					});
				},
				ProcessContainers: function (SettingsService, StateParameters) {
					return SettingsService.ProcessContainers(StateParameters.process);
				},
				ItemColumns: function (SettingsService, StateParameters) {
					return SettingsService.ItemColumns(StateParameters.process).then(function (data) {
						let dateInputs = [];
						let idInputs = [];
						angular.forEach(data, function (key) {
							if (key.DataType == 3) dateInputs.push(key);
							if (key.DataType == 1 || (key.DataType == 16 && key.SubDataType == 1)) idInputs.push(key);
						});

						return {ItemColumns: data, dateInputs: dateInputs, idInputs: idInputs};
					});
				},
				Processes: function (ProcessesService, $filter) {
					return ProcessesService.get().then(function (processes) {
						let t = [];
						for (let i = 0, l = processes.length; i < l; i++) {
							let process = processes[i];
							t.push({name: $filter('translate')(process.ProcessName), value: process.ProcessName});
						}
						return t;
					});
				},
			},
		});

		ViewsProvider.addView('settings.portfolios.fields', {
			controller: 'PortfoliosFieldsSettingsController',
			template: 'settings/portfolios/portfoliosFields.html',
			resolve: {
				PortfolioColumns: function (SettingsService, StateParameters) {
					return SettingsService.ProcessPortfolioColumns(StateParameters.process, StateParameters.ProcessPortfolioId);
				},
				PortfolioGroups: function (SettingsService, StateParameters) {
					return SettingsService.ProcessPortfolioGroups(StateParameters.process, StateParameters.ProcessPortfolioId);
				},
				LayoutProcessGroup: function (SettingsService, StateParameters) {
					return SettingsService.ProcessGroups(StateParameters.process);
				},
				ItemColumns: function (SettingsService, StateParameters, Common, Translator, $rootScope) {
					return SettingsService.ItemColumns(StateParameters.process).then(function (data) {
						let result = [];
						let userLanguage = $rootScope.clientInformation.locale_id;

						angular.forEach(data, function (key) {
							if (key.Validation.Label != null && key.Validation.Label.length != 0 && JSON.parse(key.Validation.Label)[userLanguage] != null) {
								if (key.DataType < 7 || key.DataType == 13 || key.DataType == 14 || key.DataType == 16 || key.DataType == 18 || key.DataType == 19 || key.DataType == 20 || key.DataType == 24 || key.DataType == 60 || key.DataType == 65 || key.DataType == 23 || key.DataType == 22) {
									key.Translation = Translator.translation(key.LanguageTranslation) + ' - ' + Common.getDataTypeTranslation(key.DataType) + ' (' + JSON.parse(key.Validation.Label)[userLanguage] + ')';
									result.push(key);
								}
							} else if (key.DataType < 7 || key.DataType == 13 || key.DataType == 14 || key.DataType == 16 || key.DataType == 18 || key.DataType == 19 || key.DataType == 20 || key.DataType == 24 || key.DataType == 60 || key.DataType == 65 || key.DataType == 23 || key.DataType == 22) {
								key.Translation = Translator.translation(key.LanguageTranslation) + ' - ' + Common.getDataTypeTranslation(key.DataType) + '';
								result.push(key);
							}
						});
						return result;
					});
				},
				PortfolioColumnGroups: function (SettingsService, StateParameters, $rootScope, Translator) {
					return SettingsService.GetPortfolioColumnGroups(StateParameters.process, StateParameters.ProcessPortfolioId).then(function (response) {
						response.splice(0, 0, {ProcessPortfolioColumnGroupId: 0, LanguageTranslation: {}});
						response[0].LanguageTranslation[$rootScope.me.locale_id] = Translator.translate("PORTFOLIO_COLUMNS_NOT_IN_GROUP");
						return response;
					});
				},
				ProcessActions: function (ProcessActionsService, StateParameters) {
					return ProcessActionsService.getProcessActions(StateParameters.process);
				},
				ProcessPortfolioActions: function (SettingsService, StateParameters) {
					return SettingsService.getProcessPortfolioActions(StateParameters.process, StateParameters.ProcessPortfolioId).then(function (rows) {
						_.each(rows, function (row) {
							if (row.ButtonName != "") row.ButtonName = JSON.parse(row.ButtonName);
						});
						return rows;
					});
				},

				Portfolio: function (SettingsService, StateParameters) {
					return SettingsService.GetProcessPortfolio(StateParameters.process, StateParameters.ProcessPortfolioId)
				},

				Lists: function (SettingsService, $filter, Translator) {
					return SettingsService.ListProcesses().then(function (process) {
						return $filter('orderBy')(process, function (p) { return Translator.translation(p.LanguageTranslation)});
					});
				},
			},
			afterResolve: {
				SimpleDataTables: function (ItemColumns, Portfolio, PortfolioService, $q) {


					let downloadSettings = {
						'HasDownloadRows': false,
						'Parsed': "",
						'TableProcessPortfolios': {},
						'SimpleDataTables': [],
						'FormattedRows': {}
					};

					let formattedRows;
					let parsed;
					let hasDownloadRows = false;
					let tableAttachProcesses = [];


					if (!_.isEmpty(Portfolio.DownloadOptions)) {
						downloadSettings.HasDownloadRows = true;
						downloadSettings.Parsed = JSON.parse(Portfolio.DownloadOptions);
					}

					_.each(ItemColumns, function (itemColumn) {
						if (itemColumn.DataType == 18 || itemColumn.DataType == 22) {
							if (itemColumn.DataAdditional != null) {
								downloadSettings.TableProcessPortfolios[itemColumn.DataAdditional] = [];
								tableAttachProcesses.push(itemColumn.DataAdditional);
							}
							downloadSettings.SimpleDataTables.push(itemColumn);
							if (downloadSettings.HasDownloadRows) {
								if (downloadSettings.Parsed[itemColumn.ItemColumnId]) {
									downloadSettings.FormattedRows[itemColumn.ItemColumnId] = {};
									downloadSettings.FormattedRows[itemColumn.ItemColumnId].Rows = downloadSettings.Parsed[itemColumn.ItemColumnId].Rows;
									downloadSettings.FormattedRows[itemColumn.ItemColumnId].Include = downloadSettings.Parsed[itemColumn.ItemColumnId].Include;
									downloadSettings.FormattedRows[itemColumn.ItemColumnId].TemplateTag = downloadSettings.Parsed[itemColumn.ItemColumnId].TemplateTag;
									downloadSettings.FormattedRows[itemColumn.ItemColumnId].PortfolioId = downloadSettings.Parsed[itemColumn.ItemColumnId].PortfolioId;
								}
							}
						}
					});

					if (tableAttachProcesses.length > 0) {
						let promises = [];
						_.each(tableAttachProcesses, function (process) {
							downloadSettings.TableProcessPortfolios[process] = [];
							promises.push(PortfolioService.getPortfolios(process).then(function (p) {
								return p;
							}));
						});
						return $q.all(promises).then(function (resolves) {
							_.each(resolves, function (processPortfolios) {
								_.each(processPortfolios, function (portfolio) {
									downloadSettings.TableProcessPortfolios[portfolio.Process].push(portfolio);
								})
							});
							return downloadSettings;
						});
					} else {
						return downloadSettings;
					}
				}
			}
		});

		ViewsProvider.addDialog('settings.portfolios.progress', {
			controller: 'PortfoliosProgressSettingsController',
			template: 'settings/portfolios/portfolioProgress.html',
			resolve: {
				ProcessGroups: function (ProcessService, StateParameters) {
					return ProcessService.getProcessGroups(StateParameters.process);
				}
			}
		});

	}
})();
