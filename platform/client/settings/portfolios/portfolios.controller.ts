﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.controller('PortfoliosSettingsController', PortfoliosSettingsController)
		.controller('PortfoliosFieldsSettingsController', PortfoliosFieldsSettingsController)
		.controller('PortfolioStateSetController', PortfolioStateSetController)
		.controller('PortfoliosProgressSettingsController', PortfoliosProgressSettingsController);


	PortfolioStateSetController.$inject = ['$scope', 'Portfolio', 'Processes', 'IdColumns', 'SplitOptions', 'Features', 'SettingsService', 'MessageService', 'SavePortfolios', 'ChangeValue', 'Translator'];

	function PortfolioStateSetController($scope, Portfolio, Processes, IdColumns, SplitOptions, Features, SettingsService, MessageService, SavePortfolios, ChangeValue, Translator) {

		$scope.statesByProcess = {};

		$scope.portfolio = Portfolio;
		$scope.processes = Processes;
		$scope.splitOptions = SplitOptions;
		$scope.features = Features;
		$scope.idColumns = IdColumns;

		$scope.widthOptions = [{name: 'VIEW_SPLIT_EVEN', value: 'even'}, {
			name: 'VIEW_SPLIT_WIDER',
			value: 'wider'
		}, {name: 'VIEW_SPLIT_NARROWER', value: 'narrower'}, {name: 'VIEW_SPLIT_FULL', value: 'full'}];

		$scope.cancel = function () {
			MessageService.closeDialog();
		}

		let clarifyData = function () {
			if (Portfolio.OpenRowProcess.length == 0) Portfolio.OpenRowProcess = 0;
			if (Portfolio.OpenRowItemColumnId.length == 0) Portfolio.OpenRowItemColumnId = 0;
			if (Portfolio.OpenSplit.length == 0) Portfolio.OpenSplit = 0;


			if (Portfolio.SubDefaultState.length == 0) {
				Portfolio.SubDefaultState = '';
			}
			if (Portfolio.DefaultState.length == 0) {
				Portfolio.DefaultState = '';
			}
		}

		$scope.save = function () {
			clarifyData();
			ChangeValue({id: Portfolio.ProcessPortfolioId, type: 'portfolios'});
			SavePortfolios();
			MessageService.closeDialog();
		};

		$scope.changeInsteadProcess = function () {
			if (!$scope.statesByProcess.hasOwnProperty(Portfolio.OpenRowProcess) && Portfolio.OpenRowProcess != '') {
				let tabs_ = [];
				let fs = [];
				SettingsService.ProcessTabs(Portfolio.OpenRowProcess).then(function (tabs) {
					_.each(tabs, function (t) {
						t.DefaultState = t.ProcessTabId;
						t.Feature = Translator.translate(t.Variable) + ' - ' + t.MaintenanceName;
					})
					SettingsService.ProcessGroups(Portfolio.OpenRowProcess).then(function (groups) {
						let features = [];
						angular.forEach(groups, function (group) {
							angular.forEach(group.SelectedFeatures, function (feature) {
								features.push(feature);
							});
						});
						fs = _.uniqBy(features, 'DefaultState');
						tabs_ = _.uniqBy(tabs, 'ProcessTabId');
						$scope.statesByProcess[Portfolio.OpenRowProcess] = fs.concat(tabs_);

						$scope.features.push({DefaultState: "1stTab", Feature: "1ST_TAB"});
						$scope.features.push({DefaultState: "lastTab", Feature: "LAST_TAB"});
						$scope.statesByProcess[Portfolio.OpenRowProcess].unshift({
							DefaultState: "1stTab",
							Feature: "1ST_TAB"
						})
						$scope.statesByProcess[Portfolio.OpenRowProcess].unshift({
							DefaultState: "lastTab",
							Feature: "LAST_TAB"
						})
					});
				});
			}
		}
		$scope.changeInsteadProcess();
	}

	PortfoliosSettingsController.$inject = [
		'$rootScope',
		'$scope',
		'$filter',
		'Common',
		'MessageService',
		'SettingsService',
		'ProcessContainers',
		'UserGroups',
		'SaveService',
		'ViewService',
		'StateParameters',
		'ItemColumns',
		'Features',
		'Portfolios',
		'PortfoliosSelects',
		'PortfoliosSelectsHide',
		'Dashboards',
		'Charts',
		'PortfolioService',
		'Translator',
		'ProcessConditions',
		'Processes',
		'ProcessTabs'
	];

	function PortfoliosSettingsController(
		$rootScope,
		$scope,
		$filter,
		Common,
		MessageService,
		SettingsService,
		ProcessContainers,
		UserGroups,
		SaveService,
		ViewService,
		StateParameters,
		ItemColumns,
		Features,
		Portfolios,
		PortfoliosSelects,
		PortfoliosSelectsHide,
		Dashboards,
		Charts,
		PortfolioService,
		Translator,
		ProcessConditions,
		Processes,
		ProcessTabs) {

		$scope.selectedRows = [];
		$scope.edit = true;
		$scope.containers = ProcessContainers;
		$scope.userGroups = [];
		$scope.portfolios = [];
		$scope.features = Features;
		$scope.features.push({DefaultState: "1stTab", Feature: "1ST_TAB"});
		$scope.features.push({DefaultState: "lastTab", Feature: "LAST_TAB"});

		_.each(ProcessTabs, function (t) {
			t.DefaultState = t.ProcessTabId;
			t.Feature = t.Variable;
			$scope.features.push(t);
		})

		$scope.idColumns = ItemColumns.idInputs;
		$scope.Processes = Processes;

		let newValuesArray = [];
		let changedItems = [];
		let data = {};
		let userLanguage = $rootScope.clientInformation.locale_id;

		$scope.calendarType = [{value: 0, name: 'NO_CALENDAR'},
			{value: 1, name: 'DEFAULT_CALENDAR'}];

		$scope.eachPortfoliosConditions = {};
		let findPortfoliosConditions = function (portfolios) {
			angular.forEach(portfolios, function (portfolio) {
				$scope.eachPortfoliosConditions[portfolio.ProcessPortfolioId] = _.filter(ProcessConditions, function (o) {
					return _.includes(o.ProcessPortfolioIds, portfolio.ProcessPortfolioId);
				});
			});
		};
		findPortfoliosConditions(Portfolios);
		findPortfoliosConditions(PortfoliosSelects);

		$scope.showThisPortfolioConditions = function (portfolioId, translation) {
			ViewService.view('debug.settings', {
				params: {
					process: StateParameters.process,
					selectedUiObjectId: portfolioId,
					selectedUiObjectType: 'ProcessPortfolioIds',
					translation: Translator.translation(translation),
					noConditions: 'NO_PORTFOLIO_CONDITIONS',
					title: 'PORTFOLIO_CONDITION'
				}
			});
		};

		$scope.splitOptions = [
			{
				name: 'PORTFOLIO_SPLIT_NORMAL',
				value: 0
			},
			{
				name: 'PORTFOLIO_SPLIT_SPLIT',
				value: 1
			},
			{
				name: 'PORTFOLIO_SPLIT_DOUBLE_NORMAL',
				value: 2
			},
			{
				name: 'PORTFOLIO_SPLIT_DOUBLE_SPLIT',
				value: 3
			}
		];

		_.extend($scope.portfolios, {
			portfolios: Portfolios,
			selects: PortfoliosSelects,
			dashboards: Dashboards,
			charts: Charts
		});

		_.extend($scope.portfolios.selects, PortfoliosSelectsHide);


		$scope.orderDirectionOptions = [{name: 'ASCENDING', value: 0}, {name: 'DESCENDING', value: 1}];
		$scope.recursiveFilter = [{value: 0, name: 'RECURSIVE_FILTER_TYPE_0'}, {
			value: 1,
			name: 'RECURSIVE_FILTER_TYPE_1'
		}, {value: 2, name: 'RECURSIVE_FILTER_TYPE_2'}]

		$scope.itemColumns = $filter('orderBy')(ItemColumns.ItemColumns, function (column) {
			return Translator.translation(column.LanguageTranslation)
		});
		$scope.dateInputs = $filter('orderBy')(ItemColumns.dateInputs, function (input) {
			return Translator.translation(input.LanguageTranslation)
		});

		SaveService.subscribeSave($scope, function () {
			saveCharts();
			saveDashboards();
			savePortfolios();
		});

		$scope.modifyProgress = function (portfolio) {
			ViewService.view('settings.portfolios.progress', {
				params: {
					portfolioId: portfolio.ProcessPortfolioId,
					process: StateParameters.process
				},
				locals: {
					Portfolio: portfolio,
					OnChange: $scope.changeValue
				}
			});
		};

		$scope.openStateSetDialog = function (portfolio) {
			let params = {
				template: 'settings/portfolios/portfolioStateSet.html',
				controller: 'PortfolioStateSetController',
				locals: {

					StateParameters: StateParameters,
					Portfolio: portfolio,
					Processes: $scope.Processes,
					IdColumns: $scope.idColumns,
					SplitOptions: $scope.splitOptions,
					Features: $scope.features,
					SavePortfolios: savePortfolios,
					ChangeValue: $scope.changeValue

				}
			};
			MessageService.dialogAdvanced(params);
		};

		$scope.modifyCalendars = function (calendar) {
			data = calendar;

			let save = function () {
				$scope.changeValue({id: calendar.ProcessPortfolioId, type: 'portfolios'});
				savePortfolios();
				data = '';
			};

			let content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{
					type: 'select',
					label: 'CALENDAR_SELECT',
					model: 'CalendarType',
					singleValue: 'true',
					options: $scope.calendarType,
					optionValue: 'value',
					optionName: 'name'
				},
					{
						type: 'select',
						label: 'VACATION_START_DATE',
						model: 'CalendarStartId',
						singleValue: 'true',
						options: $scope.dateInputs,
						optionValue: 'ItemColumnId',
						optionName: 'LanguageTranslation'
					},
					{
						type: 'select',
						label: 'VACATION_END_DATE',
						model: 'CalendarEndId',
						singleValue: 'true',
						options: $scope.dateInputs,
						optionValue: 'ItemColumnId',
						optionName: 'LanguageTranslation'
					}
				],
				title: 'CHOOSE_DATES'
			};
			MessageService.dialog(content, calendar);
		};

		function deleteDashboards(dashboardsToDelete) {
			let deleteIds = Common.convertArraytoString(dashboardsToDelete, 'id');
			SettingsService.ProcessDashboardsDelete(StateParameters.process, deleteIds);
			angular.forEach(dashboardsToDelete, function (row) {
				let index = Common.getIndexOf($scope.portfolios[row.type], row.id, 'Id');
				$scope.portfolios[row.type].splice(index, 1);
			});
		}

		function deleteCharts(chartsToDelete) {
			let deleteIds = Common.convertArraytoString(chartsToDelete, 'id');
			SettingsService.ProcessDashboardChartDelete(StateParameters.process, deleteIds);
			angular.forEach(chartsToDelete, function (row) {
				let index = Common.getIndexOf($scope.portfolios[row.type], row.id, 'Id');
				$scope.portfolios[row.type].splice(index, 1);
			});
		}

		function deletePortfolios(portfoliosToDelete) {
			let deleteIds = Common.convertArraytoString(portfoliosToDelete, 'id');
			SettingsService.ProcessPortfoliosDelete(StateParameters.process, deleteIds);
			angular.forEach(portfoliosToDelete, function (row) {
				let index = Common.getIndexOf($scope.portfolios[row.type], row.id, 'ProcessPortfolioId');
				$scope.portfolios[row.type].splice(index, 1);
			});
		}

		$scope.deletePortfoliosAndCharts = function () {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				let portfoliosToDelete = [];
				let dashboardsToDelete = [];
				let chartsToDelete = [];
				_.each($scope.selectedRows, function (item) {
					if (item.type === 'dashboards') {
						dashboardsToDelete.push(item);
					} else if (item.type === 'charts') {
						chartsToDelete.push(item);
					} else {
						portfoliosToDelete.push(item);
					}
				});
				if (!_.isEmpty(dashboardsToDelete)) {
					deleteDashboards(dashboardsToDelete);
				}
				if (!_.isEmpty(chartsToDelete)) {
					deleteCharts(chartsToDelete);
				}
				if (!_.isEmpty(portfoliosToDelete)) {
					deletePortfolios(portfoliosToDelete);
				}
				$scope.selectedRows = [];
			});
		};

		function findUpdatedItems(items) {
			return _.map(items, function (item) {
				return _.find($scope.portfolios[item.type], {Id: item.id});
			});
		}

		let saveCharts = function () {
			let updated = findUpdatedItems(changedItems.charts);
			_.each(updated, function (chart) {
				SettingsService.ProcessDashboardChartSave(StateParameters.process, chart);
			});
			changedItems.charts = [];
		};

		let saveDashboards = function () {
			let updated = findUpdatedItems(changedItems.dashboards);
			_.each(updated, function (dash) {
				dash.onlyPortfolio = 1;
				SettingsService.ProcessDashboardSave(StateParameters.process, dash);
			});
			changedItems.dashboards = [];
		};

		function findAndCleanPortfolioItem(item) {


			let index = Common.getIndexOf($scope.portfolios[item.type], item.id, 'ProcessPortfolioId');
			let portfolio = $scope.portfolios[item.type][index];

			portfolio.CalendarType = Common.convertArraytoString(portfolio.CalendarType);
			portfolio.CalendarStartId = Common.convertArraytoString(portfolio.CalendarStartId);
			portfolio.CalendarEndId = Common.convertArraytoString(portfolio.CalendarEndId);
			portfolio.DefaultState = Common.convertArraytoString(portfolio.DefaultState);
			portfolio.UseDefaultState = portfolio.UseDefaultState == false ? 0 : 1//Common.convertArraytoString(portfolio.UseDefaultState);
			portfolio.AlsoOpenCurrent = portfolio.AlsoOpenCurrent == false ? 0 : 1

			if (portfolio.CalendarType === null) portfolio.CalendarType = 0;
			if (portfolio.CalendarStartId === null) portfolio.CalendarStartId = 0;
			if (portfolio.CalendarEndId === null) portfolio.CalendarEndId = 0;
			if (portfolio.DefaultState === null) portfolio.DefaultState = '';
			if (portfolio.UseDefaultState === null) portfolio.UseDefaultState = 0;


			if (typeof portfolio.HiddenProgressArrows !== 'string') portfolio.HiddenProgressArrows = '';

			return portfolio;
		}

		let savePortfolios = function () {
			let processPortfolioArray = [];
			angular.forEach(changedItems.portfolios, function (item) {
				PortfolioService.invalidatePortfolio(item.id);
				processPortfolioArray.push(findAndCleanPortfolioItem(item));
			});
			angular.forEach(changedItems.selects, function (item) {
				PortfolioService.invalidatePortfolio(item.id);
				processPortfolioArray.push(findAndCleanPortfolioItem(item));
			});

			if (!_.isEmpty(processPortfolioArray)) {
				SettingsService.ProcessPortfoliosSave(StateParameters.process, processPortfolioArray);
			}

			newValuesArray = [];
			changedItems.portfolios = [];
			changedItems.selects = [];
		};

		$scope.header = {
			title: 'STATE_SETTINGS_PROCESS_PORTFOLIOS',
		};
		StateParameters.activeTitle = $scope.header.title;
		ViewService.footbar(StateParameters.ViewId, {template: 'settings/portfolios/portfolioFootbar.html'});

		angular.forEach(UserGroups, function (group) {
			$scope.userGroups.push({'name': group.usergroup_name, 'value': group.item_id});
		});

		$scope.showDashboard = function (dashboard) {
			let params = _.extend({}, StateParameters, {dashboard: dashboard});
			ViewService.view('settings.dashboards.setup', {params: params});
		};

		$scope.showChart = function (chart) {
			let params = _.extend({}, StateParameters, {chart: chart});
			ViewService.view('settings.dashboardcharts.fields', {params: params});
		};

		$scope.showPortfolio = function (processPortfolioId, type) {
			let params = StateParameters;
			params.ProcessPortfolioId = processPortfolioId;
			params.type = type;
			ViewService.view('settings.portfolios.fields', {params: params});
		};

		$scope.showPortfolioDialog = function () {
			ViewService.view('settings.portfolios.dialog', {params: StateParameters}, true);
		};

		let data;

		$scope.addDashboard = function () {
			data = {LanguageTranslation: {}};

			let discard = function () {
			};

			let save = function () {
				addDashboardSave();
			};

			let content = {
				buttons: [{onClick: discard, text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{type: 'translation', label: 'DASHBOARD_NAME', model: 'LanguageTranslation'}],
				title: 'ADD_NEW_DASHBOARD'
			};
			MessageService.dialog(content, data);
		};

		$scope.addChart = function () {
			data = {LanguageTranslation: {}};

			let discard = function () {
			};

			let save = function () {
				addChartSave();
			};

			let content = {
				buttons: [{onClick: discard, text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{type: 'translation', label: 'DASHBOARD_CHART_NAME', model: 'LanguageTranslation'}],
				title: 'ADD_NEW_DASHBOARD_CHART'
			};
			MessageService.dialog(content, data);
		};

		let addDashboardSave = function () {
			let defaultPortfolio = _.find($scope.portfolios.portfolios);
			if (!defaultPortfolio) {
				return;
			}
			let dummy = {
				Name: data.LanguageTranslation[$rootScope.me.locale_id],
				LanguageTranslation: data.LanguageTranslation,
				ProcessPortfolioId: defaultPortfolio.ProcessPortfolioId
			};
			SettingsService.ProcessDashboardsAdd(StateParameters.process, dummy)
				.then(function (dashboard) {
					$scope.portfolios.dashboards.push(dashboard);
				});
		};

		let addChartSave = function () {
			let defaultPortfolio = _.find($scope.portfolios.portfolios);
			if (!defaultPortfolio) {
				return;
			}
			let dummy = {
				Name: data.LanguageTranslation[$rootScope.me.locale_id],
				LanguageTranslation: data.LanguageTranslation,
				JsonData: '{}',
				ProcessPortfolioId: defaultPortfolio.ProcessPortfolioId
			};
			SettingsService.ProcessDashboardChartAdd(StateParameters.process, dummy)
				.then(function (chart) {
					$scope.portfolios.charts.push(chart);
				});
		};

		$scope.cloneChart = function(){
			ViewService.view('settings.portfolios.cloneCharts', {params: StateParameters});
		}

		$scope.copyPortfolio = function () {
			data = {ProcessPortfolioId_: null, LanguageTranslation: {}};
			let save = function () {
				copyPortfolioSave();
			};
			let content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{
					type: 'select',
					label: 'PORTFOLIO',
					model: 'ProcessPortfolioId_',
					options: Portfolios,
					optionName: 'LanguageTranslation',
					optionValue: 'ProcessPortfolioId',
					singleValue: true
				},
					{type: 'translation', label: 'PROCESS_GROUP_NAME', model: 'LanguageTranslation'}
				],
				title: 'COPY_PORTFOLIO'
			};
			MessageService.dialog(content, data)
		};

		let copyPortfolioSave = function () {
			let selectedId = data.ProcessPortfolioId_;
			let newPortfolio = angular.copy(_.find(Portfolios, function (o) {
				return o.ProcessPortfolioId == selectedId;
			}));
			if (data.LanguageTranslation[userLanguage]) {
				newPortfolio.LanguageTranslation[userLanguage] = data.LanguageTranslation[userLanguage];
			} else {
				newPortfolio.LanguageTranslation[userLanguage] = 'Unnamed copy';
			}
			SettingsService.ProcessPortfolioCopy(StateParameters.process, selectedId, newPortfolio).then(function (portfolio_) {
				$scope.portfolios['portfolios'].push(portfolio_)
			});
		};

		$scope.addProcessPortfolio = function (type) {
			data = {LanguageTranslation: {}, type: type};

			let discard = function () {
			};

			let save = function () {
				saveProcessPortfolio();
			};

			let content = {
				buttons: [{onClick: discard, text: 'Discard'}, {type: 'primary', onClick: save, text: 'Save'}],
				inputs: [{type: 'translation', label: 'PROCESS_CONTAINER_NAME', model: 'LanguageTranslation'}],
				title: 'Add new portfolio'
			};
			MessageService.dialog(content, data);
		};

		let saveProcessPortfolio = function () {
			let ProcessPortfolioType = 0;
			if (data.type === 'portfolios') {
				ProcessPortfolioType = 0;
			} else if (data.type === 'selects') {
				ProcessPortfolioType = 10;
			}

			// if (data.UseDefaultState == true) data.UseDefaultState = 1;
			// if (data.UseDefaultState == false) data.UseDefaultState = 0;

			let portfolio = {
				ProcessPortfolioType: ProcessPortfolioType,
				ProcessPortfolioId: 0,
				LanguageTranslation: data.LanguageTranslation,
				IgnoreRights: true
			};
			SettingsService.ProcessPortfoliosAdd(StateParameters.process, portfolio).then(function (portfolio) {
				$scope.portfolios[data.type].push(portfolio);
				data = {};
			});
		};

		$scope.changeValue = function (params) {
			newValuesArray.push({id: params.id, type: params.type});
			let noDuplicateObjects = {};
			_.each(newValuesArray, function (item) {
				noDuplicateObjects[item.type + '_' + item.id] = item;
			});

			let nonDuplicateArray = {};

			for (let item in noDuplicateObjects) {
				let type = noDuplicateObjects[item].type;
				if (typeof nonDuplicateArray[type] == 'undefined') {
					nonDuplicateArray[type] = [];
				}
				nonDuplicateArray[type].push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};
		$scope.portfolioViews = [{'name': 'PORTFOLIO_FV', 'value': 0}, {
			'name': 'PORTFOLIO_RECURSIVE_V',
			'value': 1
		}, {'name': 'PORTFOLIO_GV', 'value': 7}, {'name': 'PORTFOLIO_BV', 'value': 5}, {
			'name': 'PORTFOLIO_RV',
			'value': 2
		}, {'name': 'PORTFOLIO_PV', 'value': 3}, {'name': 'PORTFOLIO_P', 'value': 4}, {
			'name': 'PORTFOLIO_SD',
			'value': 6
		}]
	}

	PortfoliosFieldsSettingsController.$inject = [
		'$q',
		'$scope',
		'MessageService',
		'SettingsService',
		'PortfolioColumns',
		'Common',
		'SaveService',
		'ItemColumns',
		'StateParameters',
		'$filter',
		'PortfolioColumnGroups',
		'$rootScope',
		'Translator',
		'ViewService',
		'PortfolioService',
		'ProcessPortfolioActions',
		'ProcessActions',
		'Portfolio',
		'SimpleDataTables',
		'Lists'
	];

	function PortfoliosFieldsSettingsController($q, $scope, MessageService, SettingsService, PortfolioColumns, Common, SaveService, ItemColumns, StateParameters, $filter, PortfolioColumnGroups, $rootScope, Translator, ViewService, PortfolioService, ProcessPortfolioActions, ProcessActions, Portfolio, SimpleDataTables, Lists) {

		$scope.simpleDataTables = [];
		$scope.tableProcessPortfolios = SimpleDataTables.TableProcessPortfolios;
		$scope.formattedRows = SimpleDataTables.FormattedRows;
		$scope.simpleDataTables = SimpleDataTables.SimpleDataTables;
		let downloadChanged = false;

		$scope.saveDownloadOptions = function () {
			downloadChanged = true;
			SaveService.tick();
		};

		$scope.portfolioColumnGroups = PortfolioColumnGroups;
		$scope.portfolioColumns = PortfolioColumns;
		$scope.selectedColumns = [];
		$scope.selectedActions = [];
		$scope.edit = true;
		$scope.portfolioType = StateParameters.type;
		let newValuesArray = [];
		let changedItems = [];
		$scope.summambleColumns = {};
		$scope.userLanguage = $rootScope.clientInformation.locale_id;
		$scope.changedActions = {};
		$scope.translateDT = function (dt: number): string {
			return Common.getDataTypeTranslation(dt);
		};

		$scope.portfolioActions = ProcessPortfolioActions;
		$scope.processActions = ProcessActions;
		ViewService.footbar(StateParameters.ViewId, {template: 'settings/portfolios/portfolioFieldsFootbar.html'});

		$scope.editGroup = function (group) {
			let nameData = {
				LanguageTranslation: angular.copy(group.LanguageTranslation),
				ShowType: angular.copy(group.ShowType)
			};

			let save = function () {
				group.ShowType = nameData.ShowType;
				group.LanguageTranslation = nameData.LanguageTranslation;
				$scope.changeGroupName(group);
			};

			let del = function () {
				$scope.deleteColumnGroup(group.ProcessPortfolioColumnGroupId)
			};

			let content = {
				buttons: [
					{text: "DELETE", type: 'warn', onClick: del},
					{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{
					type: 'translation',
					label: 'PROCESS_CONTAINER_NAME',
					model: 'LanguageTranslation'
				},
					{
						type: 'select',
						label: 'GROUP_SHOW_TYPE',
						model: 'ShowType',
						singleValue: true,
						options: $scope.showTypes,
						optionName: 'name',
						optionValue: 'value'
					}
				],
				title: 'CHANGE_CONTAINER_NAME'
			};

			MessageService.dialog(content, nameData);
		}

		$scope.addProcessPortfolioAction = function () {
			SettingsService.newProcessPortfolioAction(StateParameters.process, StateParameters.ProcessPortfolioId).then(function (newrow) {
				$scope.portfolioActions.push(newrow);
			});
		};

		$scope.saveProcessPortfolioActions = function (portfolioAction) {
			if (typeof portfolioAction != 'undefined' && portfolioAction != null)
				$scope.changedActions[portfolioAction.PortfolioActionId] = portfolioAction;
		};

		$scope.addColumns = function (groupId) {
			data = {LanguageTranslation: null, ProcessPortfolioColumnId: null, ProcessPortfolioColumnGroupId: groupId};
			let save = function () {
				$scope.addColumnsSave();
				data = {};
			};
			let content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{
					type: 'multiselect',
					label: 'COLUMNS',
					model: 'ProcessPortfolioColumnId',
					options: PortfolioColumns,
					optionValue: 'ProcessPortfolioColumnId',
					optionName: 'ItemColumn.LanguageTranslation',
					singleValue: true
				}
				],
				title: 'MOVE_PGROUP_COLUMNS'
			};
			MessageService.dialog(content, data);
		};

		$scope.addColumnsSave = function () {
			let newGroupId = data.ProcessPortfolioColumnGroupId;
			let oldGroupId;
			let changedColumns = [];

			for (let i = 0; i < data.ProcessPortfolioColumnId.length; i++) {
				let match = _.find(PortfolioColumns, function (o) {
					return o.ProcessPortfolioColumnId == data.ProcessPortfolioColumnId[i]
				});
				oldGroupId = angular.copy(match.ProcessPortfolioColumnGroupId);
				match.ProcessPortfolioColumnGroupId = newGroupId;
				angular.forEach($scope.columnsByGroup[oldGroupId], function (value, index) {
					if (value.ProcessPortfolioColumnId == match.ProcessPortfolioColumnId) {
						$scope.columnsByGroup[oldGroupId].splice(index, 1);
						if (typeof $scope.columnsByGroup[newGroupId] === 'undefined') {
							$scope.columnsByGroup[newGroupId] = [];
						}
						$scope.columnsByGroup[newGroupId].push(match);
					}
				});
				changedColumns.push(match);
			}
			SettingsService.ProcessPortfolioColumnsSave(StateParameters.process, StateParameters.ProcessPortfolioId, changedColumns).then(function () {
				PortfolioService.clearColumnCache();
				ViewService.refreshOthers(StateParameters.ViewId);
			});
		};

		$scope.itemColumns = $filter('orderBy')(ItemColumns, function (column) {
			return Translator.translation(column.LanguageTranslation)
		});

		$scope.columnsByGroup = {};

		let createGroupCollection = function () {
			$scope.columnsByGroup = {};
			let columnGroupIds = _.map($scope.portfolioColumnGroups, 'ProcessPortfolioColumnGroupId');
			angular.forEach(columnGroupIds, function (groupId) {
				$scope.columnsByGroup[groupId] = [];
			});
		};
		createGroupCollection();

		let setColumnsForGroups = function () {
			for (let i = 0; i < PortfolioColumns.length; i++) {
				if ($scope.columnsByGroup[PortfolioColumns[i].ProcessPortfolioColumnGroupId]) {
					$scope.columnsByGroup[PortfolioColumns[i].ProcessPortfolioColumnGroupId].push(PortfolioColumns[i]);
				}
			}
		};
		setColumnsForGroups();

		$scope.checkSummambleColumns = function () {
			angular.forEach($scope.itemColumns, function (column) {
				if (column.DataType == 1 || column.DataType == 2 || column.DataType == 16 || column.DataType == 20) {
					$scope.summambleColumns[column.ItemColumnId] = true;
				}
			});
		};
		$scope.checkSummambleColumns();

		let changedGroups = [];

		$scope.changeGroupName = function (group) {
			if (changedGroups.indexOf(group.ProcessPortfolioColumnGroupId) === -1) {
				changedGroups.push(group.ProcessPortfolioColumnGroupId);
			}
			SaveService.tick();
		};

		$scope.ProcessPortfolioId = StateParameters.ProcessPortfolioId;
		let data = {};

		SaveService.subscribeSave($scope, function () {

			for (let i = 0; i < changedGroups.length; i++) {
				for (let j = 0; j < PortfolioColumnGroups.length; j++) {
					if (changedGroups[i] === PortfolioColumnGroups[j].ProcessPortfolioColumnGroupId) {
						SettingsService.UpdatePortfolioColumnGroups(StateParameters.process, PortfolioColumnGroups[j]);
						break;
					}
				}
			}

			if (downloadChanged) {
				Portfolio.DownloadOptions = JSON.stringify($scope.formattedRows);
				SettingsService.ProcessPortfoliosSave(StateParameters.process, [Portfolio]).then(function () {
					downloadChanged = false;
				});
			}
			return $q.when(savePortfolios());
		});


		let savePortfolioActions = function () {
			let r = [];
			_.each($scope.changedActions, function (item) {
				let additem = angular.copy(item);
				additem.ButtonName = JSON.stringify(additem.ButtonName);
				r.push(additem);
			});
			SettingsService.updateProcessPortfolioAction(StateParameters.process, r);
			$scope.changedActions = {};
		};

		$scope.deleteColumnGroup = function (groupId) {
			MessageService.confirm('DELETE_COLUMN_GROUP', function () {
				SettingsService.DeleteColumnGroups(StateParameters.process, groupId);
				let index = Common.getIndexOf(PortfolioColumnGroups, groupId, 'ProcessPortfolioColumnGroupId');
				angular.forEach($scope.columnsByGroup[groupId], function (oldColumn) {
					$scope.columnsByGroup[0].push(oldColumn);
				});
				PortfolioColumnGroups.splice(index, 1);
			});
		};

		let savePortfolios = function () {
			let portfolioColumnsAdd = [];
			let portfolioColumnsSave = [];
			let newIndex = [];

			savePortfolioActions();

			angular.forEach(changedItems, function (item) {
				angular.forEach($scope.portfolioColumns, function (value, key) {
					if (value.ProcessPortfolioColumnId === item.columnid) {
						item.index = key;
					}
				});
				$scope.portfolioColumns[item.index].ProcessPortfolioId = StateParameters.ProcessPortfolioId;

				if (angular.isDefined($scope.portfolioColumns[item.index].ProcessPortfolioColumnId) && $scope.portfolioColumns[item.index].ProcessPortfolioColumnId > 0) {
					portfolioColumnsSave.push($scope.portfolioColumns[item.index]);
				} else {
					portfolioColumnsAdd.push($scope.portfolioColumns[item.index]);
					newIndex.push(item);
				}
			});
			if (portfolioColumnsAdd.length > 0) {
				SettingsService.ProcessPortfolioColumnsAdd(StateParameters.process, StateParameters.ProcessPortfolioId, portfolioColumnsAdd).then(function (data) {
					PortfolioService.clearColumnCache();
					ViewService.refreshOthers(StateParameters.ViewId);
					for (let i = 0; i < newIndex.length; i++) {
						$scope.portfolioColumns[newIndex[i].index] = data[i];
					}
				});
			}
			if (portfolioColumnsSave.length > 0) {
				SettingsService.ProcessPortfolioColumnsSave(StateParameters.process, StateParameters.ProcessPortfolioId, portfolioColumnsSave).then(function () {
					PortfolioService.clearColumnCache();
					ViewService.refreshOthers(StateParameters.ViewId);
				});
			}
			newValuesArray = [];
			changedItems = [];
		};

		$scope.deleteItems = function () {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				$scope.deleteActions();
				$scope.deleteColumns();
			});
		};

		$scope.deleteColumns = function () {
			if ($scope.selectedColumns.length > 0) {
				let portfolioColumns = Common.convertArraytoString($scope.selectedColumns, 'ProcessPortfolioColumnId');

				if (portfolioColumns != null) {
					SettingsService.ProcessPortfolioColumnsDelete(StateParameters.process, StateParameters.ProcessPortfolioId, portfolioColumns).then(function () {
						PortfolioService.clearColumnCache();
						ViewService.refreshOthers(StateParameters.ViewId);
					});
				}

				angular.forEach($scope.selectedColumns, function (column) {
					let index = Common.getIndexOf($scope.portfolioColumns, column.ProcessPortfolioColumnId, 'ProcessPortfolioColumnId');
					let index2 = Common.getIndexOf($scope.columnsByGroup[column.ColumnGroup], column.ProcessPortfolioColumnId, 'ProcessPortfolioColumnId');
					$scope.portfolioColumns.splice(index, 1);
					$scope.columnsByGroup[column.ColumnGroup].splice(index2, 1);

				});

				$scope.selectedColumns = [];
			}
		};

		$scope.deleteActions = function () {
			if ($scope.selectedActions.length > 0) {
				_.each($scope.selectedActions, function (action) {
					let index = Common.getIndexOf($scope.portfolioActions, action.id, 'PortfolioActionId');
					$scope.portfolioActions.splice(index, 1);
					SettingsService.removeProcessPortfolioAction(StateParameters.process, action.id);
				});
				$scope.selectedActions = [];
			}
		};

		$scope.limited = StateParameters.limited;
		$scope.showChecks = true;
		$scope.showAdv = true;

		if (StateParameters.limited) {
			$scope.showChecks = true;
			$scope.showAdv = false;
		}
		$scope.loading = false;
		let p = PortfolioService.getPortfolio(StateParameters.process, StateParameters.ProcessPortfolioId).then(function (p) {
			$scope.header = {
				title: Translator.translate('PAGE_PORTFOLIO') + " - " + Translator.translate(p.Variable),
				icons: [{
					icon: 'settings', onClick: function () {
						MessageService.menu([{
							name: $scope.showChecks ? Translator.translate('PORTFOLIO_SHOW_ADV') : Translator.translate('PORTFOLIO_SHOW_CHECKS'),
							onClick: function () {
								$scope.loading = true;
								setTimeout(function () {
									$scope.showChecks = !$scope.showChecks;
									$scope.showAdv = !$scope.showChecks;
									$scope.loading = false;
								}, 10);
							}
						}, {
							name: Translator.translate('PORTFOLIO_SHOW_ALL'),
							onClick: function () {
								$scope.loading = true;
								setTimeout(function () {
									$scope.showChecks = true;
									$scope.showAdv = true;
									$scope.loading = false;
								}, 10);
							}
						}

						], event.currentTarget);
					},
				},]
			};

			StateParameters.activeTitle = $scope.header.title;
		});


		let addNewPortfolioColumn = function () {
			data.ProcessPortfolioId = StateParameters.ProcessPortfolioId;
			let newData = [];
			let lastOrderNo = data.OrderNo;
			angular.forEach(data.ItemColumnId, function (key) {

				let ic = _.find($scope.itemColumns, function (o) {
					return o.ItemColumnId == key;
				});

				if (_.findIndex($scope.portfolioColumns, function (column) {
					return column.ItemColumn.ItemColumnId == key;
				}) == -1) {
					lastOrderNo = Common.getOrderNoBetween(lastOrderNo, undefined);
					newData.push({
						ProcessPortfolioId: StateParameters.ProcessPortfolioId,
						OrderNo: lastOrderNo,
						ItemColumn: {ItemColumnId: key},
						ProcessPortfolioColumnGroupId: data.ProcessPortfolioColumnGroupId,
						Width: setWidthByDataType(ic),
						IsDefault: ic.DataType == 6 ? true : false,
						UseInFilter: ic.DataType == 6 ? true : false
					});
				}
			});


			SettingsService.ProcessPortfolioColumnsAdd(StateParameters.process, StateParameters.ProcessPortfolioId, newData).then(function (result) {
				PortfolioService.clearColumnCache();
				ViewService.refreshOthers(StateParameters.ViewId);
				angular.forEach(result, function (value) {
					value.Priority = 0;
					if (!$scope.columnsByGroup[value.ProcessPortfolioColumnGroupId]) {
						$scope.columnsByGroup[value.ProcessPortfolioColumnGroupId] = [];
					}
					$scope.columnsByGroup[value.ProcessPortfolioColumnGroupId].push(value);
					$scope.portfolioColumns.push(value);
					if (findNewColumnDataType(value.ItemColumn.ItemColumnId) == 23) {
						value.ReportColumn = true;
						value.ItemColumn.DataType = 23;
						$scope.changeValue({
							id: value.ItemColumn.ItemColumnId,
							index: $scope.portfolioColumns.length - 1,
							columnid: value.ProcessPortfolioColumnId
						});
						SaveService.tick();
					}
				});
			});
		};

		let setWidthByDataType = function (ic_) {
			if (ic_.DataType == 60) return 50; //Progress arrows
			else if (ic_.DataType == 3 || ic_.DataType == 13) return 85; //Dates
			else if (ic_.DataType == 1 || ic_.DataType == 2 || ic_.DataType == 14 || ic_.DataType == 20 || (ic_.DataType == 16 && (ic_.SubDataType == 1 || ic_.SubDataType == 2))) return 80; //Numbers
			else if (ic_.DataType == 6) { //List
				let listShowType = _.find(Lists, function (l) {
					return l.ProcessName == ic_.DataAdditional;
				});
				if (listShowType && listShowType.PortfolioShowType == 2) return 50; //Show type: symbol
				return 150; //Show type: text or both
			} else if (ic_.Name == "serial" || ic_.DataType == 5) return 150;
			return null;
		};

		let findNewColumnDataType = function (itemColumnId) {
			return _.find($scope.itemColumns, function (o) {
				return o.ItemColumnId == itemColumnId;
			}).DataType;
		};

		let calcFilteredColumns = function () {
			let notAddedColumns = [];
			let portfolioColumnIds = [];
			angular.forEach($scope.portfolioColumns, function (portfolioColumn) {
				portfolioColumnIds.push(portfolioColumn.ItemColumn.ItemColumnId);
			});
			angular.forEach($scope.itemColumns, function (column) {
				if (!_.includes(portfolioColumnIds, column.ItemColumnId)) {
					notAddedColumns.push(column);
				}
			});
			return notAddedColumns;
		};

		$scope.addPortfolioColumns = function (portfolioId, portfolioGroupId) {
			let filteredColumns = calcFilteredColumns();
			let beforeOrderNo = '';
			let afterOrderNo = '';
			angular.forEach($scope.portfolioColumns, function (column) {
				if (angular.isUndefined(beforeOrderNo)) {
					beforeOrderNo = column.OrderNo;
				} else if (beforeOrderNo < column.OrderNo) {
					beforeOrderNo = column.OrderNo;
				}
			});

			data.ItemColumnId = null;
			data.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			data.ProcessPortfolioColumnGroupId = portfolioGroupId;

			let discard = function () {
			};

			let save = function () {
				addNewPortfolioColumn();
				data = {ItemColumnId: null};
			};

			let content = {
				buttons: [{onClick: discard, text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{
					type: 'multiselect',
					label: 'Item Column',
					model: 'ItemColumnId',
					optionValue: 'ItemColumnId',
					optionName: 'Translation',
					options: filteredColumns,
					validation: {'max': '0'}
				}],
				title: 'ADD_NEW_PORTFOLIO_COLUMN'
			};
			MessageService.dialog(content, data);
		};


		$scope.syncOrder = function (current, prev, next, parent) {
			let beforeOrderNo;
			let afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}

			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			let i = $scope.portfolioColumns.length;
			while (i--) {
				if ($scope.portfolioColumns[i].ProcessPortfolioColumnId == current.ProcessPortfolioColumnId) {
					$scope.changeValue({id: current.ProcessPortfolioColumnId, index: i});
					break;
				}
			}
			current.ProcessPortfolioColumnGroupId = parent.ProcessPortfolioColumnGroupId;
			savePortfolios();
		};

		$scope.updateColumnGroup = function (columnGroup) {
			SettingsService.UpdatePortfolioColumnGroups(StateParameters.process, columnGroup);
		};

		$scope.changeOrderColumnGroup = function (current, prev, next) {
			let beforeOrderNo;
			let afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}
			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}
			if (prev.OrderNo) {
				current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			} else {
				current.OrderNo = Common.getOrderNoBetween(0, afterOrderNo);
			}
			$scope.updateColumnGroup(current);
		};

		$scope.changeValue = function (params) {
			newValuesArray.push({id: params.id, index: params.index, columnid: params.columnid});

			let noDuplicateObjects = {};
			let l = newValuesArray.length;

			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.id] = item;
			}

			let nonDuplicateArray = [];
			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};

		$scope.showTypes = [
			{name: 'SHOW_UNGROUPED_HIDE_GROUP', value: 0},
			{name: 'SHOW_GROUPED_SHOW_GROUP', value: 1},
			{name: 'SHOW_GROUPED_HIDE_GROUP', value: 2},
			{name: 'SHOW_UNGROUPED_SHOW_GROUP', value: 3}
		];

		$scope.reportColumnTypes = [
			{name: 'VISIBLE_BOTH', value: 0},
			{name: 'VISIBLE_IN_EXCEL', value: 1},
			{name: 'HIDDEN', value: 2}
		]

		$scope.addColumnGroup = function () {
			data = {LanguageTranslation: {}};

			let save = function () {
				$scope.addColumnGroupSave();
			};

			let content = {
				buttons: [{text: 'DISCARD', modify: 'cancel'}, {
					type: 'primary',
					onClick: save,
					text: 'SAVE',
					modify: 'create'
				}],
				inputs: [{type: 'translation', label: 'PGROUP_NAME', model: 'LanguageTranslation'}],
				title: 'CREATE_NEW_PGROUP'
			};

			MessageService.dialog(content, data);
		};

		$scope.addColumnGroupSave = function () {
			let portfolioGroup = {
				OrderNo: undefined,
				LanguageTranslation: undefined,
				ProcessPortfolioId: undefined
			};
			if (!data.LanguageTranslation[$scope.userLanguage]) {
				data.LanguageTranslation[$scope.userLanguage] = 'unnamed group';
			}
			portfolioGroup.LanguageTranslation = data.LanguageTranslation;
			portfolioGroup.ProcessPortfolioId = StateParameters.ProcessPortfolioId;
			let beforeOrderNo = '';
			let afterOrderNo = '';
			angular.forEach($scope.portfolioColumnGroups, function (group) {
				if (angular.isUndefined(beforeOrderNo)) {
					beforeOrderNo = group.OrderNo;
				} else if (beforeOrderNo < group.OrderNo) {
					beforeOrderNo = group.OrderNo;
				}
			});

			portfolioGroup.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);

			let newGroup = [];
			newGroup.push(portfolioGroup);
			SettingsService.ProcessPortfolioColumnsGroupsAdd(StateParameters.process, newGroup).then(function (group) {
				$scope.portfolioColumnGroups.push(group[0]);
			});
		};
	}

	PortfoliosProgressSettingsController.$inject = [
		'$scope',
		'Portfolio',
		'ProcessGroups',
		'OnChange',
		'MessageService',
		'SaveService',
		'PortfolioService'
	];

	function PortfoliosProgressSettingsController(
		$scope,
		Portfolio,
		ProcessGroups,
		OnChange,
		MessageService,
		SaveService,
		PortfolioService
	) {
		$scope.showArrows = Portfolio.ShowProgressArrows;
		$scope.groups = ProcessGroups;
		$scope.selected = [];

		let hiddenArrows = Portfolio.HiddenProgressArrows.length ?
			angular.fromJson(Portfolio.HiddenProgressArrows) : '';
		angular.forEach(ProcessGroups, function (group, key) {
			if (Array.isArray(hiddenArrows)) {
				let i = hiddenArrows.length;
				while (i--) {
					if (hiddenArrows[i] == group.ProcessGroupId) {
						$scope.selected[key] = false;
						break;
					}
				}

				if (i === -1) {
					$scope.selected[key] = true;
				}
			} else {
				$scope.selected[key] = true;
			}
		});

		$scope.cancel = MessageService.closeDialog;
		$scope.save = function () {
			let hideArrows = [];
			angular.forEach(ProcessGroups, function (group, index) {
				if ($scope.selected[index] === false) {
					hideArrows.push(group.ProcessGroupId);
				}
			});

			Portfolio.HiddenProgressArrows = angular.toJson(hideArrows);
			Portfolio.ShowProgressArrows = $scope.showArrows;

			OnChange({
				id: Portfolio.ProcessPortfolioId,
				type: 'portfolios'
			});

			PortfolioService.invalidateProgress(Portfolio.ProcessPortfolioId);
			SaveService.tick();
			MessageService.closeDialog();
		}
	}

})();
