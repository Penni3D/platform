(function () {
	'use strict';
	angular
		.module('core.settings')
		.config(ClonePortfolioChartsConfig)
		.controller('ClonePortfolioChartsController', ClonePortfolioChartsController);

	ClonePortfolioChartsConfig.$inject = ['ViewsProvider'];

	function ClonePortfolioChartsConfig(ViewsProvider) {
		ViewsProvider.addDialog('settings.portfolios.cloneCharts', {
			controller: 'ClonePortfolioChartsController',
			template: 'settings/portfolios/portfolioCloneCharts.html',
			resolve: {
				Charts: function (SettingsService) {
					return SettingsService.ProcessDashboardGetCharts().then(function(dash){
						let c = {};
						_.each(dash, function(dashboard){
							if(!c[Number(dashboard.ProcessPortfolioId)]) c[Number(dashboard.ProcessPortfolioId)] = [];
							c[Number(dashboard.ProcessPortfolioId)].push(dashboard);
						})
						return c;
					});
				},
				Portfolios: function (PortfolioService, StateParameters) {
					return PortfolioService.getPortfolios(StateParameters.process);
				},
				Dashboards: function (SettingsService, StateParameters) {
					return SettingsService.ProcessDashboards(StateParameters.process).then(function(dashboards){
						let ds = {};
						_.each(dashboards, function(dashboard){
							if(!ds[dashboard.ProcessPortfolioId]) ds[dashboard.ProcessPortfolioId] = [];
							ds[dashboard.ProcessPortfolioId].push(dashboard);
						})
						return ds;
					});
				}
			}, afterResolve:{
				Links: function(SettingsService, Charts){
					return SettingsService.DashboardRelations().then(function(dl){
						let p = {};
						_.each(dl, function(value, key){
							_.each(value, function(id){
								let t = "";
								_.each(Charts, function(chart){
									_.each(chart, function(ch){
										if(ch.Id == id) t = ch.LanguageTranslation;
									})
								})
								let o = {
									'Id': id,
									'Translation':t
								}
								if(!p[key]) p[key] = [];
								p[key].push(o);
							})
						})
						return p;
					});
				}
			}
		})
	}

	ClonePortfolioChartsController.$inject = ['$scope', '$rootScope', 'StateParameters', 'Portfolios', 'PortfolioService', 'MessageService', 'Dashboards', 'Links'];

	function ClonePortfolioChartsController($scope, $rootScope, StateParameters, Portfolios, PortfolioService, MessageService, Dashboards, Links) {

		$scope.portfolios = Portfolios;
		$scope.dashboards = Dashboards;
		$scope.links = Links;
		$scope.userLanguage = $rootScope.clientInformation.locale_id;

		$scope.clone = function (){
			if(!$scope.translation[$scope.userLanguage]){
				$scope.translation[$scope.userLanguage] = "Copy of chart"
			}
			PortfolioService.cloneChart($scope.selectedChart,
				$scope.toPortfolio,
				$scope.translation,
				$scope.dashboardId,
				$scope.fromPortfolio,
				$scope.fromDashboardId,
				StateParameters.process
				);
			MessageService.closeDialog();
		}

		$scope.cancel = function(){
			MessageService.closeDialog();
		}
	}
})();