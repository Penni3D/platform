(function () {
    'use strict';

    angular
        .module('core.settings')
        .controller('DashboardSetupController', DashboardSetupController);

    DashboardSetupController.$inject = [
        '$rootScope',
        '$scope',
        'StateParameters',
        'SettingsService',
        'Dashboard',
        'Charts',
        'UserGroups',
        'SaveService',
        'Common',
        'ChartLinkData',
        'Processes',
        'ProcessColumns',
        'ProcessService',
        'Translator'
    ];
    function DashboardSetupController(
	    $rootScope,
        $scope,
        StateParameters,
        SettingsService,
        Dashboard,
        Charts,
        UserGroups,
        SaveService,
        Common,
        ChartLinkData,
        Processes,
        ProcessColumns,
        ProcessService,
        Translator
        ) {
            $scope.localeId = $rootScope.me.locale_id;
            $scope.process = StateParameters.process;
            $scope.dashboard = Dashboard;
        $scope.charts = Charts;
        $scope.processes = Processes; 

        $scope.linkData = [];
        $scope.originalLinkData = angular.copy(ChartLinkData);

        $scope.linkChartChangeProcess = 0;                          
        $scope.tempChartArr = [];

        $scope.linkedFieldOptions = {};
        $scope.linkedChartOptions = {};
        $scope.linkElements = {};

        $scope.linkedChartPortfolioModes = [{ LanguageTranslation: Translator.translate('CHART_LINK_SUB_PROCESS'), Value: 0 }, { LanguageTranslation: Translator.translate('CHART_LINK_MAIN_PROCESS'), Value: 1 }];

        let allowedLinkedColumns = [];

        $scope.toggleHide = function(){
            SaveService.tick();
        }

        angular.forEach(ChartLinkData, function (element) {
            element.link_process = [element.link_process];
            element.link_process_field = [element.link_process_field];
            element.source_field = [element.source_field];

            if (element.chart_ids) {
                element.chart_ids = element.chart_ids.split(",").map(Number) || "";
            }

            if (element.portfolio_mode != null) {
                element.portfolio_mode = [element.portfolio_mode];
            }
            else if (element.portfolio_mode == null) {
                element.portfolio_mode = [0];
            }

            if (element.link_process != null && element.link_process != "") {
                $scope.linkElements[element.link_process] = {};

                ProcessService.getColumns(element.link_process).then(function (columns) {
                    angular.forEach(columns, function (column) {
                        if ((column.DataType == 6 || column.DataType == 5 || column.SubDataType == 5 || column.SubDataType == 6) || column.DataType == 1 || column.SubDataType == 1) {
                            allowedLinkedColumns.push(column);
                        }
                    });
                    columns[0] = $scope.itemIdObj;
                    $scope.linkElements[element.link_process].link_process_column_option = columns;
                    $scope.linkedFieldOptions[ChartLinkData.indexOf(element)] = allowedLinkedColumns;
                });

                SettingsService.ProcessDashboardGetChartsForProcess(element.link_process).then(function (charts) {
                    $scope.linkElements[element.link_process].link_process_chart_option = charts;
                    $scope.linkedChartOptions[ChartLinkData.indexOf(element)] = charts;               
                });

            }
        });

        $scope.itemIdObj = { Name: "item_id", LanguageTranslation: "item_id" };
        $scope.linkData = angular.copy(ChartLinkData);
        $scope.userGroups = UserGroups;

        var allowedColumns = [];
        allowedColumns.push($scope.itemIdObj);
        angular.forEach(ProcessColumns, function (column) {
            if (column.DataType == 1 || column.SubDataType == 1 || column.DataType == 5 || column.SubDataType == 5 || column.DataType == 6 || column.SubDataType == 6) {
                allowedColumns.push(column);
            }
        });       

        $scope.processColumns = allowedColumns;
       // $scope.processColumns[0] = $scope.itemIdObj;

        $scope.linkChartIds = [];

        angular.forEach($scope.linkData, function (chart) {
            if(chart.hide == null) chart.hide = false;
            angular.forEach(chart.chart_ids, function (id) {
                $scope.linkChartIds.push(id);
            });         
        });       

        $scope.chartSelectionChanged = function () {
            var chartIds = [];
            angular.forEach($scope.dashboard.Charts, function (chart) {
                chartIds.push(chart.Id);
            });

            var tempData = [];
            if ($scope.linkChartChangeProcess == 0) {
                tempData = $scope.chartsSelected;
            }
            else {
                tempData = $scope.tempChartArr;
            }

            var newSelections = _.chain(tempData)
                    .map(function(chartId){
                        return _.find(Charts, {Id: chartId});
                    })
                    .without(null)
                    .value();
            var index = 0;
            var index2 = 0;
            angular.forEach(newSelections, function (selection) {
                if (selection == null) {
                    if ($scope.linkChartChangeProcess == 0) {
                        newSelections[newSelections.indexOf(selection)] = $scope.dashboard.Charts[index];
                    }
                    else {
                        newSelections[newSelections.indexOf(selection)] = $scope.tempChartArr[index2];
                    } 
                    index += 1;
                }
                index2 += 1;
            });
                _.each(newSelections, function(chart, index){
                    if(!chart.OrderNo) {
                        var previous = newSelections[index - 1];
                        var prevOrderNo = previous && previous.OrderNo;
                        var next = newSelections[index +1];
                        var nextOrderNo = next && next.OrderNo;

                        chart.OrderNo = Common.getOrderNoBetween(prevOrderNo, nextOrderNo);
                    }
                });
            $scope.dashboard.Charts = newSelections;
            $scope.linkChartChangeProcess = 0;
            };

            $scope.chartsSelected = _.map(Dashboard.Charts, function(chart) {
                return chart.Id;
            });

            $scope.syncOrder = function(current, prev, next){
                var beforeOrderNo;
                var afterOrderNo;

                if (angular.isDefined(prev)) {
                    beforeOrderNo = prev.OrderNo;
                }

                if (angular.isDefined(next)) {
                    afterOrderNo = next.OrderNo;
                }

                current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
                //SettingsService.ProcessDashboardSave(
                //    StateParameters.process,
                //    $scope.dashboard
                //);
                SaveService.tick();
        };

        $scope.addLinkRow = function () {
            SettingsService.CreateChartLinkAndUpdateData(StateParameters.process, $scope.dashboard.Id).then(function (data) {
                $scope.linkData.push(data[data.length - 1]);
            });
        }

        $scope.linkProcessChanged = function (index) {
            if ($scope.linkElements[$scope.linkData[index].link_process] == null) {
                ProcessService.getColumns($scope.linkData[index].link_process).then(function (columns) {
                    columns[0] = $scope.itemIdObj;
                    $scope.linkedFieldOptions[index] = columns;
                });

                SettingsService.ProcessDashboardGetChartsForProcess($scope.linkData[index].link_process).then(function (charts) {
                    $scope.linkedChartOptions[index] = charts;
                });
            }
            else {
                $scope.linkedFieldOptions[index] = $scope.linkElements[$scope.linkData[index].link_process].link_process_column_option;
                $scope.linkedChartOptions[index] = $scope.linkElements[$scope.linkData[index].link_process].link_process_chart_option;
            }
        }

            $scope.deleteLinkRow = function (index) {
                $scope.linkData.splice(index, 1);

                delete $scope.linkedFieldOptions[index];
                delete $scope.linkedChartOptions[index];

                var linkedFieldOptions = {};
                var linkedChartOptions = {};
                var index = 0;
                angular.forEach(angular.copy($scope.linkedFieldOptions), function (element) {
                    linkedFieldOptions[index] = element;
                    index++;
                });
                index = 0;
                angular.forEach(angular.copy($scope.linkedChartOptions), function (element) {
                    linkedChartOptions[index] = element;
                    index++;
                });
                $scope.linkedFieldOptions = linkedFieldOptions;
                $scope.linkedChartOptions = linkedChartOptions;

                SaveService.tick();
            }

            $scope.LinkChartSelectionChanged = function (index) {
                $scope.linkChartChangeProcess = 1;
            }

        SaveService.subscribeSave($scope, function () {
            $scope.dashboard["linkData"] = $scope.linkData;
            $scope.dashboard["process"] = StateParameters.process;
            $scope.dashboard["originalLinkData"] = angular.copy($scope.originalLinkData);
            return SettingsService.ProcessDashboardSave(StateParameters.process, $scope.dashboard).then(function (result) {
                if ($scope.linkChartChangeProcess == 1) {
                    var tempLinkChartIds = [];
                    angular.forEach($scope.linkData, function (chart) {
                        angular.forEach(chart.chart_ids, function (id) {
                            tempLinkChartIds.push(id);
                        });
                    });
                    var toBeRemoved = [];
                    angular.forEach($scope.linkChartIds, function (id) {
                        if (tempLinkChartIds.indexOf(id) == -1) {
                            toBeRemoved.push(id);
                        }
                    });

                    $scope.tempChartArr = [];
                    $scope.tempChartArr = $scope.dashboard.Charts.filter(function (x) {
                        return toBeRemoved.indexOf(x.Id) == -1;
                    });

                    var linkedChartOptions = [];
                    angular.forEach($scope.linkedChartOptions, function (LinkRow) {
                        angular.forEach(LinkRow, function (chart) {
                            if (linkedChartOptions.indexOf(chart) == -1) linkedChartOptions.push(chart);
                        });
                    });

                    angular.forEach(tempLinkChartIds, function (id) {
                        if ($scope.linkChartIds.indexOf(id) == -1) {
                            var inserTableChart = linkedChartOptions.filter(function (x) {
                                return x.Id == id;
                            });
                            inserTableChart[0].DashboardPortfolioId = $scope.linkData[0].dashboard_portfolio_id;
                            $scope.tempChartArr.push(inserTableChart[0]);
                        }
                    });

                    angular.forEach(toBeRemoved, function (id) {
                        $scope.dashboard.Charts.slice(id, 1);
                    });
                    $scope.linkChartIds = tempLinkChartIds;
                    $scope.chartSelectionChanged();
                }
            });
                
         });
    }
})();
