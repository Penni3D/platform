(function () {
    'use strict';

    angular
        .module('core.settings')
        .config(DashboardsConfig);

    DashboardsConfig.$inject = ['ViewsProvider'];
    function DashboardsConfig(ViewsProvider) {
        ViewsProvider.addView('settings.dashboards.setup', {
            controller: 'DashboardSetupController',
            template: 'settings/dashboards/dashboard-setup.html',
            resolve: {
                Dashboard: function(StateParameters, SettingsService){
                    return SettingsService.ProcessDashboardGet(StateParameters.process, StateParameters.dashboard.Id);
                },
                Charts: function(StateParameters, SettingsService) {
                    return SettingsService.ProcessDashboardGetChartsForProcess(StateParameters.process);
                },
                UserGroups: function (ApiService) {
	                return ApiService.v1api('meta/Items/user_group').get();
                },
                ChartLinkData: function (StateParameters, SettingsService) {
                    return SettingsService.GetChartLinkData(StateParameters.process, StateParameters.dashboard.Id);
                },
                Processes: function (ProcessesService) {
                    return ProcessesService.get();
                },
                ProcessColumns: function (ProcessService, StateParameters) {
                    return ProcessService.getColumns(StateParameters.process);
                }
            }
        });

        ViewsProvider.addView('settings.dashboardcharts.fields', {
            controller: 'DashboardChartFieldsSettingsController',
            template: 'settings/dashboards/dashboard-chart-fields.html',
            resolve: {
                Chart: function(StateParameters, SettingsService){
                    return SettingsService.ProcessDashboardGetChart(StateParameters.process, StateParameters.chart.Id);
                },
                ChartTypes: function(SettingsService) {
                    return SettingsService.ProcessDashboardGetTypes();
                },
                ItemColumns: function (ApiService, StateParameters) {
		            return ApiService.v1api('meta/ItemColumns/' + StateParameters.process).get();
		        }
            }
        });
    }
})();
