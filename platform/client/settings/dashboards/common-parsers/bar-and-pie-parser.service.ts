(function () {
    'use strict';
    
    angular
        .module('core.settings')
        .factory('BarPieParserFactory', BarPieParserFactory);

    BarPieParserFactory.$inject = ['Translator','$rootScope'];
    
    function BarPieParserFactory(Translator,$rootScope) {
        return function (response) {
            var values = [];
            var labels = [];
            var processItemIds = [];
            var labelFieldName,
                itemIdField,
                valueField;
            var idsFix = {};
            if (response.chartFields.OriginalData.chartType == 'pie') {
                _.each(response.data, function (item) {
                    if (item.selected_item_id != null) {
                        _.each(item.selected_item_id.toString().split(","), function (selected_item_id_v) {

                            if (idsFix[selected_item_id_v]) {
                                var oldList = "'" + idsFix[selected_item_id_v].replace(new RegExp(",", 'g'), "','") + "'";

                                _.each(item["ids"].split(","), function (splitItem) {
                                    if (oldList.indexOf("'" + splitItem + "'") == -1) {
                                        idsFix[selected_item_id_v] += "," + splitItem;
                                    }
                                });

                            }
                            else {
                                idsFix[selected_item_id_v] = item["ids"];
                            }
                            var selected = response.data.filter(function (x) {
                                if (x.selected_item_id != null) {
                                    var formatted = "'" + x.selected_item_id.toString().replace(",", "','") + "'";
                                    return formatted.indexOf("'" + selected_item_id_v + "'") > -1;
                                }
                                else {
                                    return false;
                                }
                            });
                            var idsFixFormatted = "'" + idsFix[selected_item_id_v].replace(new RegExp(",", 'g'), "','") + "'";
                            
                            _.each(selected, function (selected_item) {
                                _.each(selected_item["ids"].split(","), function (splitItem) {
                                    if (idsFixFormatted.indexOf("'" + splitItem + "'") == -1) {
                                        idsFix[selected_item_id_v] += "," + splitItem;
                                    }
                                });
                            });
                        });
                    }
                    else {
                        idsFix["undefined"] = item.ids;
                    }
                });               
            }
            if(response.chartFields.Grouping) {
                labelFieldName = response.chartFields.GroupByFieldProperty || response.chartFields.GroupByField;
                itemIdField = 'selected_item_id';
                valueField = 'sum';
                var dataBySelectedItemId = {};
                _.each(response.data, function (item) {
                    var selectedItemId = item[itemIdField];
                    var ids = _.split(selectedItemId, ',');

                    _.each(ids, function (id) {
                        dataBySelectedItemId[id] = dataBySelectedItemId[id] || {
                            title: '' + id,
                            sub_title: '' + item.sub_title,
                            selected_item_id: '' + id,
                            sum: 0,
                            ids: item.ids
                        };
                        dataBySelectedItemId[id].sum += item.val;
                    });
                });

                response.data = _.values(dataBySelectedItemId);
            } else {
                labelFieldName= response.chartFields.LabelField;
                itemIdField = 'item_id';
                valueField = response.chartFields.ValueField;
            }

            let ingnoreZeros = 0;
            if (response.chartFields.OriginalData.chartType == 'pie') {
                ingnoreZeros = response.chartFields.OriginalData.ignoreZeros || 0;
            }

            let undefinedName = Translator.translate('CHART_COMMON_UNDEFINED');
            if (response.chartFields.OriginalData.undefinedCustomName != null && response.chartFields.OriginalData.undefinedCustomName[$rootScope.clientInformation["locale_id"]] != null && response.chartFields.OriginalData.undefinedCustomName[$rootScope.clientInformation["locale_id"]] != "") undefinedName = response.chartFields.OriginalData.undefinedCustomName[$rootScope.clientInformation["locale_id"]];

            var colors = [];
            _.each(response.data, function (item) {
                if (!_.isNull(item[valueField]) || response.chartFields.ShowNulls) {
                    if (ingnoreZeros == 1 && item[valueField] != 0 && !_.isNull(item[valueField]) || ingnoreZeros == 0) {
                        values.push(item[valueField] ? item[valueField] : 0);
                        if (response.chartFields.Grouping) {
                            processItemIds.push(item.selected_item_id);
                        }
                        else {
                            processItemIds.push(item.item_id);
                        }
                        if (response.relatedItems && Object.keys(response.relatedItems).length>0 &&
                            response.relatedItems[parseInt(item[itemIdField])] &&
                            labelFieldName) {

                            let labelFieldWithLanguage = String('list_item_' + $rootScope.clientInformation["locale_id"]).toLowerCase().replace("-", "_");
                            if (labelFieldName == "list_item" && response.relatedItems[parseInt(item[itemIdField])].hasOwnProperty(labelFieldWithLanguage)) labelFieldName = labelFieldWithLanguage;
                            labels.push(response.relatedItems[parseInt(item[itemIdField])][labelFieldName]);                          
                            colors.push(response.relatedItems[parseInt(item[itemIdField])]["list_symbol_fill"]);
                        } else {
                            if (response.chartFields.OriginalData.groupResults && response.chartFields.OriginalData.chartType == 'pie' && [5, 6].indexOf(response.fieldTypes.groupByFieldType.data_type)>-1) {
                                var undefinedValue = response.chartFields.OriginalData.showUndefined || 0;
                                
                                if (undefinedValue == 1) {
                                    labels.push(undefinedName);
                                }
                                else {
                                    if (item.selected_item_id == "") {
                                        values.splice(-1, 1);
                                    }
                                }
                            }
                            else {
                                labels.push(item.selected_item_id);
                                if (response.relatedItems && Object.keys(response.relatedItems).length > 0 && response.relatedItems[parseInt(item[itemIdField])]) {
                                    colors.push(response.relatedItems[parseInt(item[itemIdField])]["list_symbol_fill"]);
                                }
                            }
                        }
                    }
                }
            });

            if (response.chartFields.OriginalData.groupResults && response.chartFields.OriginalData.chartType == 'pie' && response.order) {
                let relatedItems = [];
                let undefined = response.chartFields.OriginalData.showUndefined[0] || 0;
                angular.forEach(response.relatedItems, function (obj) {
                    relatedItems.push(obj);
                });

                let listFieldWithLanguage = String('list_item_' + $rootScope.clientInformation["locale_id"]).toLowerCase().replace("-", "_");
                if (response.order == "list_item" && relatedItems.length>0 && relatedItems[0].hasOwnProperty(listFieldWithLanguage)) response.order = listFieldWithLanguage;
                relatedItems = relatedItems.sort(dynamicSort(response.order));

                let orderedData = [];
                let newOrder = [];
                for (let i = 0; i < processItemIds.length; i++) {
                    let index = _.findIndex(relatedItems, function (x) {
                        return x.item_id == processItemIds[i];
                    });

                    if (index > -1 || undefined == 1) {
                        orderedData.push({ index: index, values: values[i], labels: labels[i], processItemIds: processItemIds[i], colors: colors[i] });
                        newOrder.push(index);
                    }
                }

                newOrder = newOrder.sort(sortNumber);

                if (newOrder.indexOf(-1) > -1) {
                    newOrder.shift();
                    newOrder.push(-1);
                }

                values = [];
                labels = [];
                processItemIds = [];
                colors = [];

                for (let i = 0; i < newOrder.length; i++) {
                    let data = orderedData.filter(function (x) {
                        return x.index == newOrder[i];
                    });

                    if (data.length > 0) {
                        values.push(data[0].values);
                        labels.push(data[0].labels);
                        processItemIds.push(data[0].processItemIds);
                        if (data[0].colors != null) colors.push(data[0].colors);
                    }
                }
            }

            let thresholdData = [];
            if (response.chartFields.OriginalData.thresholdInUse && response.chartFields.OriginalData.thresholdInUse[0] > 0) {
                //thresholdType
                let thresholdType = response.chartFields.OriginalData.thresholdType[0] || 0;

                let thresholdValue;
                if (thresholdType == 0) {
                    thresholdValue = response.chartFields.OriginalData.thresholdValue || 0;
                }
                else {
                    let totalVal = 0;
                    for (let i = 0; i < values.length; i++) {
                        totalVal += values[i];
                    }
                    thresholdValue = totalVal * (response.chartFields.OriginalData.thresholdValue / 100);
                }

                let groupVals = 0;
                let groupIndexes = [];
                let groupIds = [];

                for (let i = 0; i < values.length; i++) {
                    if (values[i] <= thresholdValue && labels[i] != undefinedName) {
                        groupVals += values[i];
                        groupIndexes.push(i);
                        groupIds.push(processItemIds[i]);
                    }
                }
                groupIndexes = groupIndexes.reverse();

                if (groupIndexes.length > 0) {
                    for (let i = 0; i < groupIndexes.length; i++) {
                        values.splice(groupIndexes[i], 1);
                        labels.splice(groupIndexes[i], 1);
                        processItemIds.splice(groupIndexes[i], 1);
                        colors.splice(groupIndexes[i], 1);
                    }
                    thresholdData.push({ value: groupVals, ids: groupIds, name: response.chartFields.OriginalData.thresholdName[Object.keys(response.chartFields.OriginalData.thresholdName)[0]] });
                }
            }

            let undefinedData = {};
            if (response.chartFields.OriginalData.chartType == 'pie') {
                const orderType = response.chartFields.OriginalData.orderType || 0;
                let valueObjArr = [];
                for (let i = 0; i < values.length; i++) {
                    valueObjArr.push({ value: values[i], originalIndex: i });
                }
                if (orderType == 2) {
                    valueObjArr = valueObjArr.sort(compareValueObj);
                }
                else if(orderType == 1) {
                    valueObjArr = valueObjArr.sort(compareValueObj).reverse();
                }

                const valuesT = [];
                const labelsT = [];
                const idsT = [];
                const colorsT = [];

                angular.forEach(valueObjArr, function (obj) { //K�sittele  other ja undefined t�ss�
                    if (labels[obj.originalIndex] != undefinedName) {
                        valuesT.push(obj.value);
                        labelsT.push(labels[obj.originalIndex]);
                        idsT.push(processItemIds[obj.originalIndex]);
                        colorsT.push(colors[obj.originalIndex]);
                    }
                    else {
                        undefinedData = { value: obj.value, label: undefinedName, ids: processItemIds[obj.originalIndex]}
                    }
                });
                values = valuesT;
                labels = labelsT;
                processItemIds = idsT;
                colors = colorsT;
            }
            let groupInUse = false;
            let undefinedInUse = false;
            if (response.chartFields.OriginalData.chartType == 'pie') {
                if (thresholdData.length > 0) {
                    values.push(thresholdData[0].value);
                    labels.push(thresholdData[0].name);
                    processItemIds.push(thresholdData[0].ids);
                    groupInUse = true;
                }

                if (Object.keys(undefinedData).length > 0) {
                    values.push(undefinedData.value);
                    labels.push(undefinedData.label);
                    processItemIds.push(undefinedData.ids);
                    undefinedInUse = true;
                }
            }

            const returnObj =  {
                values: values,
                labels: labels,
                series: ['Serie A'],
                processItemIds: processItemIds,
                colors: colors,
                groupInUse: groupInUse,
                undefinedInUse: undefinedInUse,
                chartConfig: response.chartFields,
                filterData: response.filterData
            };
            if (response.chartFields.OriginalData.chartType == 'pie') {
                returnObj["idsFix"] = idsFix;
            }

            if (response.chartFields.OriginalData.mainProcessLinkMode) returnObj["mainProcessLinkMode"] = true;

            return returnObj;
        };
    }
    function dynamicSort(property) {
        let sortOrder = 1;

        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }

        return function (a, b) {
            if (sortOrder == -1) {
                if (typeof b[property] == 'number') {
                    return b[property] - a[property];
                }
                else {
                    if (property == "order_no") {
                        if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                            return -1;
                        } else if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                            return 1;
                        }
                        return b[property].localeCompare(a[property]);
                    }
                    else {
                        return b[property].localeCompare(a[property]);
                    }
                }

            } else {
                if (typeof b[property] == 'number') {
                    return a[property] - b[property];
                }
                else {
                    if (property == "order_no") {
                        if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                            return -1;
                        } else if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                            return 1;
                        }
                        return a[property].localeCompare(b[property]);
                    }
                    else {
                        return a[property].localeCompare(b[property]);
                    }
                }
            }
        }
    }
    function startsWithUppercase(str) {
        return str.substr(0, 1).match(/[A-Z\u00C0-\u00DC]/);
    }
    function sortNumber(a, b) {
        return a - b;
    }
    function compareValueObj(i, j) {
        let a = i.value;
        let b = j.value;

        let result = 0;
        if (a > b) {
            result = 1;
        } else if (a < b) {
            result = -1;
        }
        return result;
    }
})();
