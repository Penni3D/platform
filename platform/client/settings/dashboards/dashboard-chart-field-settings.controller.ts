(function () {
	'use strict';

	angular
		.module('core.settings')
		.controller('DashboardChartFieldsSettingsController', DashboardChartFieldsSettingsController);

	DashboardChartFieldsSettingsController.$inject = [
		'$q',
		'$rootScope',
		'$scope',
		'StateParameters',
		'SettingsService',
		'ChartTypes',
		'Chart',
		'ItemColumns',
		'COLOUR_SCHEMES',
		'DashboardChartTypeConfig',
		'Colors',
		'ProcessService',
		'MessageService'
	];

	function DashboardChartFieldsSettingsController(
		$q,
		$rootScope,
		$scope,
		StateParameters,
		SettingsService,
		ChartTypes,
		Chart,
		ItemColumns,
		COLOUR_SCHEMES,
		DashboardChartTypeConfig,
		Colors,
		ProcessService,
		MessageService) {

		$scope.randomColumn = function (dataTypes, cols) {
			let candidates = _.filter(cols, function (col) {
				return dataTypes.includes(col.DataType) || (col.DataType == 16 && dataTypes.includes(col.SubDataType));
			});
			return candidates[Math.floor(Math.random() * candidates.length)];
		}

		$scope.arrayPush = function (name, value) {
			let array = $scope.selections[name] = [];
			array.push(value);
		}

		$scope.deleteTab = function (id) {
			$scope.deleteMixedRow(id - 1);
		}
		$scope.deleteMultiTab = function (id) {
			$scope.deleteMultiListRow(id - 1);
		}

		$scope.listArrayItemExists = function (array, value: string) {
			//Checks that the list has the value assigned by the function call, if not then tries to return Name as it seems obvious.
			// Used because 'list_item' doesn't exist in all lists for some reason
			if (array.filter(function (e) {
				return e.Name === value;
			}).length > 0)
				return value;
			else
				return 'Name';
		}

		$scope.previews = [
			{
				title: "Pie",
				img: "chart-preview-pie.png",
				enabled: true,
				basedOn: "pie",
				onSelect: function () {
					$scope.resetPieSelections();
					$scope.selections.groupResults = true;
					$scope.arrayPush("pieType", 0);
					$scope.arrayPush("groupWithFunction", "sum");

					$scope.arrayPush("groupField", $scope.randomColumn([6], $scope.chartOptions.groupByFields.Values).Name);
					$scope.arrayPush("groupFieldProperty", "list_item");
					$scope.arrayPush("valueField", $scope.randomColumn([1, 2], $scope.chartOptions.valueFields.Values).Name);
				}
			},
			{
				title: "Donut",
				img: "chart-preview-donut.png",
				enabled: true,
				basedOn: "pie",
				onSelect: function () {
					$scope.resetPieSelections();
					$scope.selections.groupResults = true;
					$scope.arrayPush("pieType", 1);
					$scope.arrayPush("groupWithFunction", "sum");
					$scope.arrayPush("groupField", $scope.randomColumn([6], $scope.chartOptions.groupByFields.Values).Name);
					$scope.arrayPush("groupFieldProperty", "list_item");
					$scope.arrayPush("valueField", $scope.randomColumn([1, 2], $scope.chartOptions.valueFields.Values).Name);
				}
			},
			{
				title: "Bubble",
				img: "chart-preview-bubble.png",
				enabled: true,
				basedOn: "bubble",
				onSelect: function () {
					$scope.resetBubbleSelections();
					setTimeout(function () {
						$scope.arrayPush("labelField", $scope.randomColumn([6], $scope.chartOptions.groupByFields.Values).Name);
						$scope.arrayPush("nameField", $scope.randomColumn([6], $scope.chartOptions.groupByFields.Values).Name);
						$scope.arrayPush("valueField", $scope.randomColumn([6], $scope.chartOptions.valueFields.Values).Name);
						$scope.arrayPush("value2Field", $scope.randomColumn([1, 2], $scope.chartOptions.valueFields.Values).Name);
					}, 100);

					setTimeout(function () {
						$scope.arrayPush("labelFieldProperty", "list_item");
						$scope.arrayPush("nameFieldProperty", "list_item");
						$scope.arrayPush("valueFieldProperty", "list_item");
					}, 100);
				}
			},
			{
				title: "Scatter",
				img: "chart-preview-scatter.png",
				enabled: true,
				basedOn: "bubble",
				onSelect: function () {
					$scope.resetBubbleSelections();
					$scope.arrayPush("labelField", $scope.randomColumn([6], $scope.chartOptions.groupByFields.Values).Name);
					$scope.arrayPush("nameField", $scope.randomColumn([6], $scope.chartOptions.groupByFields.Values).Name);
					$scope.arrayPush("valueField", $scope.randomColumn([6], $scope.chartOptions.valueFields.Values).Name);
					$scope.arrayPush("value2Field", $scope.randomColumn([1, 2], $scope.chartOptions.valueFields.Values).Name);
					$scope.arrayPush("disperse", 1);

					setTimeout(function () {
						$scope.arrayPush("labelFieldProperty", "list_item");
						$scope.arrayPush("nameFieldProperty", "list_item");
						$scope.arrayPush("valueFieldProperty", "list_item");
					}, 100);

				}
			},
			{
				title: "Network",
				img: "chart-preview-network.png",
				enabled: false
			},
			{
				title: "Area",
				img: "chart-preview-area.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];
					$scope.xAxisSourceChanged();
					$scope.selections.mixedData[0].lineFillType = [2];
					$scope.selections.mixedData[0].lineGraphicStyle = ["straightLine"];
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [1];
					$scope.selections.mixedData[0].useCount = [1];
				}
			},
			{
				title: "Curved Area",
				img: "chart-preview-curved-area.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];

					$scope.selections.mixedIndexes = [0];

					$scope.selections.mixedData[0].lineFillType = [2];
					$scope.selections.mixedData[0].lineGraphicStyle = ["curvedLine"];
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [1];
					$scope.selections.mixedData[0].useCount = [1];
				}
			},
			{
				title: "Stacked Area",
				img: "chart-preview-stacked-area.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];
					$scope.selections.mixedData[0].lineFillType = [2];
					$scope.selections.mixedData[0].lineGraphicStyle = ["curvedLine"];
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [1];
					$scope.selections.mixedData[0].useCount = [1];
					$scope.selections.mixedData[0].stack = 1;

					$scope.selections.mixedIndexes.push(1);
					$scope.selections.mixedData[1] = angular.copy($scope.selections.mixedData[0]);
					$scope.selections.mixedData[1].lineGraphicStyle = ["straightLine"];
					$scope.selections.mixedData[1].customColor = ["indigo"];
					$scope.selections.mixedData[1].stack = 1;
				}
			},
			{
				title: "Line",
				img: "chart-preview-line.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {

					$scope.resetMixedSelections();


					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];

					$scope.xAxisSourceChanged();

					$scope.selections.mixedData = [];
					$scope.selections.mixedData.push({
						lineFillType: [0],
						lineGraphicStyle: ["straightLine"],
						mode: [5],
						type: [1],
						useCount: [1]
					});
				}
			},
			{
				title: "Multiline",
				img: "chart-preview-multi-line.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];

					$scope.xAxisSourceChanged();

					$scope.selections.mixedData[0].lineFillType = [0];
					$scope.selections.mixedData[0].lineGraphicStyle = ["straightLine"];
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [1];
					$scope.selections.mixedData[0].useCount = [1];


					$scope.selections.mixedIndexes.push(1);
					$scope.selections.mixedData[1] = angular.copy($scope.selections.mixedData[0]);
					$scope.selections.mixedData[1].lineGraphicStyle = ["curvedLine"];
					$scope.selections.mixedData[1].customColor = ["indigo"];
					$scope.selections.mixedData[1].stack = 1;
				}
			},
			{
				title: "Bar & Line",
				img: "chart-preview-bar-line.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];

					$scope.xAxisSourceChanged();

					$scope.selections.mixedData[0].lineFillType = [0];
					$scope.selections.mixedData[0].lineGraphicStyle = ["straightLine"];
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [1];
					$scope.selections.mixedData[0].useCount = [1];


					$scope.selections.mixedIndexes.push(1);
					$scope.selections.mixedData[1] = angular.copy($scope.selections.mixedData[0]);
					$scope.selections.mixedData[0].type = [2];
					$scope.selections.mixedData[1].customColor = ["indigo"];
					$scope.selections.mixedData[1].stack = 1;
				}
			},
			{
				title: "Timeline",
				img: "chart-preview-timeline.png",
				enabled: false

			},
			{
				title: "Bar",
				img: "chart-preview-bar.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];

					$scope.xAxisSourceChanged();
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [2];
					$scope.selections.mixedData[0].useCount = [1];


				}
			},
			{
				title: "Grouped Bar",
				img: "chart-preview-grouped-bar.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];

					$scope.xAxisSourceChanged();
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [2];
					$scope.selections.mixedData[0].useCount = [1];
					$scope.selections.mixedIndexes.push(1);

					$scope.selections.mixedData[1] = angular.copy($scope.selections.mixedData[0]);


				}
			},
			{
				title: "Stacked Bar",
				img: "chart-preview-stacked-bar.png",
				enabled: true,
				basedOn: "mixed",
				onSelect: function () {
					$scope.resetMixedSelections();
					$scope.selections.mixedDataCommon.xaxis = [$scope.randomColumn([3], $scope.chartOptions.groupByFields.Values).Name];
					$scope.selections.mixedDataCommon.dateType = [3];

					$scope.xAxisSourceChanged();
					$scope.selections.mixedData[0].mode = [5];
					$scope.selections.mixedData[0].type = [2];
					$scope.selections.mixedData[0].useCount = [1];
					$scope.selections.mixedData[0].stack = [1];
					$scope.selections.mixedIndexes.push(1);

					$scope.selections.mixedData[1] = angular.copy($scope.selections.mixedData[0]);


				}
			},
			{
				title: "Resource Load",
				img: "chart-preview-resource-load.png",
				enabled: true,
				basedOn: "resourceLoad",
				onSelect: function () {
					$scope.selections = {
						"chartType": "resourceLoad",
						"baseColor": [],
						"resourceDataCommon": {
							"dateType": [1],
							"showLegend": [1],
							"showPlanned": [1],
							"plannedType": [0],
							"plannedColor": [],
							"showActual": [1],
							"actualType": [0],
							"actualColor": [],
							"showLoad": [0],
							"loadType": [0],
							"loadColor": [],
							"clickable": [1],
							"showCapacity": [1],
							"capacityType": [0],
							"capacityColor": [],
							"zoom": [],
							"useCustomTimeScale": []
						},
						"fullWidth": true,
						"xAxisLabel": {},
						"yAxisLabel": {}
					}
				}
			},
			{
				title: "Pivot",
				img: "chart-preview-pivot.png",
				enabled: true,
				basedOn: "pivot",
				onSelect: function () {
					$scope.selections = {
						"chartType": "pivot",
						"baseColor": [],
						"pivotCommon": {
							"xAxisLevel1": [$scope.randomColumn([6], $scope.pivotXAxisOptions1).Name],
							"xAxisLevel1Extra": ["list_item"],
							"showSumX1": [1],
							"ShowUndefinedX1": [1],
							"ShowAllItemsX1": [1],
							"xAxisLevel2": [],
							"xAxisLevel2Extra": [],
							"showSumX2": [],
							"ShowUndefinedX2": [],
							"ShowAllItemsX2": [],
							"xAxisLevel3": [],
							"xAxisLevel3Extra": [],
							"showSumX3": [],
							"ShowUndefinedX3": [],
							"ShowAllItemsX3": [],
							"yAxisLevel1": [$scope.randomColumn([6], $scope.pivotYAxisOptions1).Name],
							"yAxisLevel1Extra": ["list_item"],
							"showSumY1": [1],
							"ShowUndefinedY1": [1],
							"ShowAllItemsY1": [1],
							"yAxisLevel2": [],
							"yAxisLevel2Extra": [],
							"ShowUndefinedY2": [],
							"ShowAllItemsY2": [],
							"yAxisLevel3": [],
							"yAxisLevel3Extra": [],
							"ShowUndefinedY3": [],
							"ShowAllItemsY3": [],
							"value": [$scope.randomColumn([1], $scope.pivotValueOptions).Name],
							"aggregateType": [0],
							"cumulativeEnd": [],
							"ignoreZeros": [1],
							"ignoreNulls": [1],
							"drillDown": [1],
							"excelInUse": [1],
							"decimalNumber": "0",
							"columnWidth": "60"
						},
						"xAxisLabel": {},
						"yAxisLabel": {},
						"fullWidth": true
					}
				}
			},
			{
				title: "Multilist",
				img: "chart-preview-multilist.png",
				enabled: true,
				basedOn: "multiList",
				onSelect: function () {
					$scope.selections = {
						"chartType": "multiList",
						"multiListIndexes": [0, 1],
						"baseColor": [],
						"multiListData": {
							"0": {
								"nameField": {
									"en-GB": "Item 1",
									"fi-FI": "Item 1"
								},
								"sourceList": [$scope.randomColumn([6], $scope.legendListOptions).Name],
								"listColors": [1]
							},
							"1": {
								"nameField": {
									"en-GB": "Item 2",
									"fi-FI": "Item 2"
								},
								"sourceList": [$scope.randomColumn([6], $scope.legendListOptions).Name],
								"listColors": [1]
							}
						},
						"multiListDataCommon": {
							"listType": [1],
							"showUndefined": [0],
							"legendList": [""],
							"clickable": [1],
							"horizontalBars": [1],
							"reverseOrder": [],
							"hideHorizontalGridLines": [1],
							"hideVerticalGridLines": [0]
						},
						"xAxisLabel": {},
						"yAxisLabel": {}
					}
				}
			},
			{
				title: "CircularVisualization",
				img: "chart-preview-progress.png",
				enabled: true,
				basedOn: "circular",
				onSelect: function () {
					$scope.selections =
					{
						"chartType": "circular",
						"baseColor": [],
						"circularData": [{
							"value1": [$scope.randomColumn([1, 2], $scope.circularValueOptions).Name],
							"value2": [],
							"ignoreNulls": [],
							"ignoreZeros": [],
							"xAxisLabel": {},
							"yAxisLabel": {},
							"aggregateType": [],
							"title": {},
							"suffix": {},
							"decimals": []
						}]
					}
					$scope.selections.circularData[0].title[($rootScope.clientInformation["locale_id"])] = "Item 1";
                }
			},
			{
				title: "Waterfall",
				img: "chart-preview-waterfall.png",
				enabled: false
			},
			{
				title: "Bullet",
				img: "chart-preview-bullet.png",
				enabled: false
			},
			{
				title: "Boxplot",
				img: "chart-preview-boxplot.png",
				enabled: false
			},
			{
				title: "Chord",
				img: "chart-preview-chord.png",
				enabled: false
			},
			{
				title: "Heatmap",
				img: "chart-preview-heatmap.png",
				enabled: false
			},
			{
				title: "Sankey",
				img: "chart-preview-sankey.png",
				enabled: false
			},
			{
				title: "Sunburst",
				img: "chart-preview-sunburst.png",
				enabled: false
			},
			{
				title: "Treemap",
				img: "chart-preview-treemap.png",
				enabled: false
			}
		];

		$scope.resetMixedSelections = function () {
			$scope.selections = {
				"chartType": "mixed",
				"mixedIndexes": [0],
				"mixedData": {
					"0": {
						"lineFillType": [0],
						"lineGraphicStyle": undefined,
						"mode": [5],
						"type": [1],
						"useCount": [1],
						"nameField": {},
						"ignoreNulls": [],
						"ignoreZeros": [],
						"dataSource": [],
						"linePointStyle": [],
						"customColor": []
					}
				},
				"baseColor": [],
				"mixedDataCommon": {
					"showLegend": [0],
					"xaxis": undefined,
					"dateType": [0],
					"zoom": [],
					"cumulativeEnd": [],
					"useListMode": [],
					"clickable": [],
					"showTable": [],
					"hideHorizontalGridLines": [],
					"hideVerticalGridLines": [],
					"horizontalBars": [],
					"amountPrefix": {},
					"toolTipSum": [],
					"showNearest": []
				},
				"xAxisLabel": {},
				"yAxisLabel": {}
			}
		}

		$scope.resetPieSelections = function () {
			$scope.selections = {
				"chartType": "pie",
				"baseColor": [],
				"groupWithFunction": ["sum"],
				"groupField": ["resources"],
				"groupFieldProperty": ["list_item"],
				"labelField": [],
				"labelFieldProperty": [],
				"valueFieldProperty": [],
				"dateRangeField": [],
				"showUndefined": [],
				"undefinedCustomName": {},
				"clickable": [],
				"ignoreZeros": [],
				"customColor": [],
				"groupResults": true,
				"pieType": [1],
				"valueField": [],
				"listColors": [],
				"orderType": [],
				"outLabelsInUse": [],
				"showOutIndex": [],
				"showOutLabel": [],
				"showOutValue": [],
				"showOutPercentage": [],
				"thresholdInUse": [],
				"thresholdType": [],
				"thresholdName": {},
				"thresholdColor": []
			}
		}

		$scope.resetBubbleSelections = function () {
			$scope.selections = {
				"chartType": "bubble",
				"baseColor": [],
				"labelFieldProperty": [],
				"valueFieldProperty": [],
				"use_custom_scale": [0],
				"clickable": [1],
				"colorList": [],
				"colorListField": [],
				"useLegend": [0],
				"legendListField": [],
				"autoScale": [],
				"disperse": [],
				"labelField": [],
				"valueField": [],
				"value2Field": [],
				"nameField": [],
				"nameFieldProperty": [],
				"value2FieldProperty": [],
				"x_axis_min": 0,
				"x_axis_max": 0,
				"x_step_size": 0,
				"y_axis_min": 0,
				"y_axis_max": 0,
				"y_step_size": 0
			}
		}

		$scope.colours = _.map(COLOUR_SCHEMES, function (value, key) {
			return {
				Name: key,
				Translation: value.translationKey
			};
		});
		$scope.process = StateParameters.process;
		$scope.chart = Chart;
		$scope.chartTypes = ChartTypes;
		$scope.selections = {};
		$scope.selectionOptions = {};
		$scope.mixedSelections = {};
		$scope.multiSelections = {};
		$scope.groupingFunctionOptions = [
			{name: 'sum', translation: 'SUM'},
			{name: 'count', translation: 'COUNT'}
		];

		$scope.filterSelectionOptions = [];
		$scope.filterTable = [];

		$scope.legendListOptions = [];
		$scope.xAxesAllowed = [];
		$scope.xAxesAllowedTypes = [];
		$scope.xAxesAllowedNames = [];
		$scope.xAxesAllowedCumulatives = [];
		$scope.dataSourceAllowed = [];
		$scope.dataSourceList = [];

		$scope.selectionOptions.listOptions = [];
		$scope.selectionOptions.radiusOptions = [];
		$scope.selectionOptions.colorListOptions = [];
		$scope.selectionOptions.nameListOptions = [];
		$scope.selectionOptions.colorListFieldOptions = [];
		$scope.selectionOptions.YesNoOption = [];

		$scope.selectionOptions.clickModes = []; //bubble

		//Pivot
		$scope.pivotAxisOptions = [];

		$scope.pivotXAxisOptions1 = [];
		$scope.pivotXAxisOptions2 = [];
		$scope.pivotXAxisOptions3 = [];

		$scope.pivotYAxisOptions1 = [];
		$scope.pivotYAxisOptions2 = [];
		$scope.pivotYAxisOptions3 = [];

		$scope.specialSumOptions = [];
		//

		$scope.xAxisLevel1ExtraOptions = [];
		$scope.xAxisLevel2ExtraOptions = [];
		$scope.xAxisLevel3ExtraOptions = [];

		$scope.yAxisLevel1ExtraOptions = [];
		$scope.yAxisLevel2ExtraOptions = [];
		$scope.yAxisLevel3ExtraOptions = [];

		$scope.pivotValueOptions = [];

		$scope.aggregateTypes = [];
		$scope.tableOptions = [];

		$scope.pieTypes = [];
		$scope.orderTypes = [];

		$scope.thresholdTypes = [];

		$scope.pivotTimeOptions = [];
		//Pivot ends

		//Resource load
		$scope.resourceLoadTimeOptions = [];
		$scope.resourceLoadYesNOptions = [];
		$scope.resourceItemTypes = [];
		$scope.customColor = [];

		//Circular visualization
		$scope.circularValueOptions = [];

		let debouncedSave = _.debounce(saveDashboard, 1000);

		function getProcessColumns(process) {
			return SettingsService.ItemColumns(process);
		}

		function setFieldForGivenSelection(property) {
			let fieldName = property + 'Field';
			let fieldProperties = property + 'FieldProperties';
			let fieldProperty = property + 'FieldProperty';

			let column = _.find(ItemColumns, {
				Name: _.first($scope.selections[fieldName])
			});
			if (column) {
				if (column.DataAdditional &&
					(column.DataType === 6 || column.DataType === 5 ||
						((column.DataType === 16 || column.DataType === 20) && (column.SubDataType === 6 || column.SubDataType === 5)))
				) {

					let dataA = column.DataType === 16 || column.DataType === 20 ? column.SubDataAdditional : column.DataAdditional;

					getProcessColumns(dataA)
						.then(function (processColumns) {
							$scope[fieldProperties] = processColumns;

							let itemsInTheList = _.filter($scope.selections[fieldProperty], function (item) {
								return _.find(processColumns, {Name: item});
							});
							if (_.size(itemsInTheList) === 0) {
								itemsInTheList = null;
							}
							$scope.selections[fieldProperty] = itemsInTheList;
							if (!$scope.selections[fieldProperty]) {
								$scope.selections[fieldProperty] = [_.first(processColumns).Name];
							}
						});
				} else {
					$scope[fieldProperties] = null;
					$scope.selections[fieldProperty] = null;
				}
			}
		}

		$scope.groupFieldChanged =
			$scope.subGroupFieldChanged =
				$scope.valueFieldChanged =
					$scope.value2FieldChanged =
						$scope.nameFieldChanged =
							$scope.labelFieldChanged = function () {
								console.log('should not come here');
							};

		$scope.mainFieldChanged = function (propertyName) {
			setFieldForGivenSelection(propertyName);
		};

		$scope.chartNameChanged = function () {
			debouncedSave();
		};

		$scope.chartChanged = function () {
			if (_.size($scope.selections.groupByField) > 0) {
				let item = _.find(ItemColumns, {
					'Name': $scope.selections.groupByField[0]
				});
				if (item && item.DataAdditional) {
					SettingsService.ItemColumns(item.DataAdditional)
						.then(function (resp) {

							$scope.groupByProperties = _.filter(resp, function (item) {
								return item.InputMethod === 0;
							});
						});
				}
			}
		};
		$scope.cancelChange = function () {
			$scope.changing = false;
		}
		$scope.chartTypeChanged = function (preview) {
			let chartType = $scope.selections.chartType;
			let dialog = {
				message: "CHART_COMMON_TYPE_CHANGE_WARNING",
				buttons: [
					{onClick: cancelConfirm, text: 'CANCEL'},
					{onClick: yesConfirm, text: 'MSG_CONFIRM', type: 'warn'}
				]
			};
			if ($scope.oldType != null) {
				return MessageService.dialog(dialog);
			} else if ($scope.oldType == null) {
				yesConfirm();
			}

			function yesConfirm() {
				$scope.selections = {};
				$scope.selections.chartType = chartType;
				$scope.changing = false;
				getFieldsForChartType().then(function () {
					preview.onSelect();
					// setTimeout(function () {
					//	
					// }, 1000);
					saveDashboard();
				});

			}

			function cancelConfirm() {
				$scope.changing = false;
				$scope.selections.chartType = $scope.oldType;
			}
		}

		$scope.$watch('selections', function (newVal, oldVal) {
			if (oldVal.chartType && oldVal.chartType == "bubble") {
				
				if (oldVal.valueFieldProperty == null) oldVal.valueFieldProperty = [];
				if (oldVal.nameFieldProperty == null) oldVal.nameFieldProperty = [];
				if (oldVal.value2FieldProperty == null) oldVal.value2FieldProperty = [];
				if (oldVal.labelFieldProperty == null) oldVal.labelFieldProperty = [];
            }
			if (_.isEqual(newVal, oldVal)==false && newVal.chartType == oldVal.chartType) {
				debouncedSave();
			}
		}, true);


		function getFieldsForChartType() {
			let chartType = _.isArray($scope.selections.chartType) ?
				_.first($scope.selections.chartType) :
				$scope.selections.chartType;
			if (chartType == "mixed") {
				setMixedLogic();
			}
			if (chartType == "multiList") {
				setMultiListLogic();
			}

			if ($scope.selections.chartType) {
				$scope.oldType = $scope.selections.chartType;
			} else {
				$scope.oldType = null;
			}

			if (chartType) {

				$scope.chartFieldConfiguration = DashboardChartTypeConfig.getFieldConfiguration(chartType);
				return SettingsService.ProcessDashboardGetFields(StateParameters.process, chartType)
					.then(function (response) {
						$scope.chartOptions = response;

						if (!($scope.selections.filters)) $scope.selections.filters = [];
						angular.forEach($scope.chartOptions.valueFields.Values, function (columnItem) { //Filter options list,process
							if (columnItem.DataType == 5 || columnItem.SubDataType == 5 || columnItem.DataType == 6 || columnItem.SubDataType == 6) {
								$scope.filterSelectionOptions.push(columnItem);
							}
						});

						angular.forEach($scope.selections.filters, function (filter) {
							$scope.filterTable.push({ name: [filter], orderNo: $scope.filterTable.length });
						});

						$scope.addFilterRow = function () {
							if (Array.isArray($scope.selections.filters) == false) $scope.selections.filters = [];
							$scope.filterTable.push({ name: null, orderNo: $scope.filterTable.length });
						}
						$scope.deleteFilterRow = function (index) {
							$scope.filterTable.splice(index, 1);
							$scope.saveQuickFilters();
						}

						$scope.saveQuickFilters = function () {
							let tempArr = [];
							for (let i = 0; i < $scope.filterTable.length; i++) {
								if ($scope.filterTable[i].name.length > 0 && $scope.filterTable[i].name[0] && $scope.filterTable[i].name[0] != "") tempArr.push($scope.filterTable[i].name[0]);
							}
							$scope.selections.filters = angular.copy(tempArr);
                        }
						if (chartType == "bubble") {
							$scope.selectionOptions.radiusOptions.push({
								LanguageTranslation: "CHART_BUBBLE_PROCESS_ITEM_COUNT",
								Name: "processItemCount"
							})

							angular.forEach($scope.chartOptions.valueFields.Values, function (columnItem) {
								if (columnItem.DataType == 1 || columnItem.DataType == 2 || columnItem.DataType == 5 || columnItem.DataType == 6 || columnItem.SubDataType == 1 || columnItem.SubDataType == 2 || columnItem.SubDataType == 6) {
									$scope.selectionOptions.listOptions.push(columnItem);
								}
								if (columnItem.DataType == 1 || columnItem.DataType == 2 || columnItem.SubDataType == 1 || columnItem.SubDataType == 2) {
									$scope.selectionOptions.radiusOptions.push(columnItem);
								}
								if (columnItem.DataType == 6 || columnItem.SubDataType == 6) {
									$scope.selectionOptions.colorListOptions.push(columnItem);
								}
								if (columnItem.DataType == 0 || columnItem.SubDataType == 0 || columnItem.DataType == 5 || columnItem.DataType == 6 || columnItem.SubDataType == 6) {
									$scope.selectionOptions.nameListOptions.push(columnItem);
								}
								//nameListOptions
							});
							$scope.selectionOptions.YesNoOption = [
								{value: 1, LanguageTranslation: 'CHART_COMMON_YES'},
								{value: 0, LanguageTranslation: 'CHART_COMMON_NO'}
							];

							$scope.selectionOptions.clickModes = [
								{ value: 1, LanguageTranslation: 'VIEW_SPLIT_NARROWER'},
								{ value: 2, LanguageTranslation: 'VIEW_SPLIT_EVEN' },
								{ value: 3, LanguageTranslation: 'VIEW_SPLIT_WIDER' },
								{ value: 0, LanguageTranslation: 'VIEW_SPLIT_FULL' }
							];

							if ($scope.selections.colorList != null || $scope.selections.colorList != "") {
								let columnItem = $scope.selectionOptions.colorListOptions.filter(function (x) {
									return x.Name == $scope.selections.colorList;
								});
								if (columnItem.length > 0) {
									let listProcessName = "";
									if (columnItem[0].DataType == 20) {
										listProcessName = columnItem[0].InputName;
									} else {
										if (columnItem[0].DataType == 16) {
											listProcessName = columnItem[0].SubDataAdditional;
										} else {
											listProcessName = columnItem[0].DataAdditional;
										}

									}

									ProcessService.getColumns(listProcessName, true).then(function (response) {
										angular.forEach(response, function (item) {
											$scope.selectionOptions.colorListFieldOptions.push(item);
										});
									});
								}
							}
						}

						if (chartType == "pie") {
							$scope.pieTypes = [{value: 1, LanguageTranslation: 'CHART_PIE_DOUGHNUT'}, {
								value: 2,
								LanguageTranslation: 'CHART_PIE_HALF_DOUGHNUT'
							}, {value: 0, LanguageTranslation: 'CHART_PIE_TRADITIONAL'}];
							$scope.orderTypes = [{value: 0, LanguageTranslation: 'CHART_PIE_NORMAL'}, {
								value: 1,
								LanguageTranslation: 'CHART_PIE_FROM_BIGGEST'
							}, {value: 2, LanguageTranslation: 'CHART_PIE_FROM_SMALLEST'}];
							
							let colorAObj = Colors.getColors();
							//let colorArr = Object.keys(colorAObj).map(name => ({ name, value: colorAObj[name] }));
							//$scope.selections["customColor"] = colorArr;
							$scope.selections["customColor"] = Object.keys(colorAObj).map(name => ({
								name,
								value: colorAObj[name],
								color: name,
								symbol: "lens"
							}));

							$scope.thresholdTypes = [{
								value: 0,
								LanguageTranslation: 'CHART_PIE_THRESHOLD_NUMBER'
							}, {value: 1, LanguageTranslation: 'CHART_PIE_THRESHOLD_PERCENT'}];
						}

						if (chartType == "circular") {
							angular.forEach($scope.chartOptions.valueFields.Values, function (columnItem) {
								if ([1, 2].indexOf(columnItem.dataType) > 0 || [1, 2].indexOf(columnItem.SubDataType) > 0) {
									$scope.circularValueOptions.push(columnItem);
								}
							});

							$scope.tableOptions = [{
								LanguageTranslation: 'CHART_COMMON_YES',
								Value: 1
							}, { LanguageTranslation: 'CHART_COMMON_NO', Value: 0 }];

							$scope.aggregateTypes = [{
								LanguageTranslation: "CHART_COMMON_SUM",
								Value: 0
							}, {
								LanguageTranslation: "CHART_COMMON_MIN",
								Value: 1
							}, {
								LanguageTranslation: "CHART_COMMON_MAX",
								Value: 2
							}, { LanguageTranslation: "CHART_COMMON_AVERAGE", Value: 3 }];
                        }

						if (chartType == "mixed") {
							$scope.defaultValueFields = $scope.chartOptions.valueFields.Values;
							//alert(defaultValueFields.length);
							//$scope.xAxesAllowed.push({ LanguageTranslation:"",Name:"noSelection"});

							angular.forEach($scope.defaultValueFields, function (columnItem) {
								if (columnItem.DataType == 1 || columnItem.SubDataType == 1 || columnItem.DataType == 2 || columnItem.SubDataType == 2) {
									$scope.dataSourceAllowed.push(columnItem);
								}
								if (columnItem.DataType == 6 || columnItem.SubDataType == 6) {
									$scope.dataSourceList.push(columnItem);
								}
								if (columnItem.DataType == 0 || columnItem.SubDataType == 0 || columnItem.DataType == 3 || columnItem.SubDataType == 3 || columnItem.DataType == 13 || columnItem.SubDataType == 13 || columnItem.DataType == 5 || columnItem.SubDataType == 5 || columnItem.DataType == 6 || columnItem.SubDataType == 6) {
									$scope.xAxesAllowed.push(columnItem);
								}
								if (columnItem.SubDataType == 3) {
									$scope.xAxesAllowedTypes.push(3);
									$scope.xAxesAllowedCumulatives.push(columnItem);
								} else if (columnItem.SubDataType == 13) {
									$scope.xAxesAllowedTypes.push(13);
									$scope.xAxesAllowedCumulatives.push(columnItem);
								} else {
									$scope.xAxesAllowedTypes.push(columnItem.DataType);
									if (columnItem.DataType == 3) {
										$scope.xAxesAllowedCumulatives.push(columnItem);
									}
									if (columnItem.DataType == 13) {
										$scope.xAxesAllowedCumulatives.push(columnItem);
									}
								}
							});

							let fieldIndex = $scope.chartOptions.valueFields.Values.findIndex(function (x) {
								if ($scope.selections.mixedDataCommon && $scope.selections.mixedDataCommon.xaxis) {
									return x.Name == $scope.selections.mixedDataCommon.xaxis[0];
								} else {
									return -1;
								}
							});

							if ($scope.selections.mixedDataCommon && $scope.selections.mixedDataCommon.xaxis != null && fieldIndex != -1 && ($scope.selections.mixedDataCommon.xaxis[0] != "noSelection" || $scope.selections.mixedDataCommon.xaxis[0] != null)) {
								//if ($scope.defaultValueFields[fieldIndex].DataType == 16 && $scope.defaultValueFields[fieldIndex].SubDataType == 3) {
								//    subquaryDate = true;
								//}

								$scope.showExtraDateFields = $scope.xAxesAllowedTypes[fieldIndex] == 3 || $scope.defaultValueFields[fieldIndex].SubDataType == 3 || $scope.xAxesAllowedTypes[fieldIndex] == 13 || $scope.defaultValueFields[fieldIndex].SubDataType == 13;
								$scope.showExtraListFields = $scope.xAxesAllowedTypes[fieldIndex] == 6 || ($scope.defaultValueFields[fieldIndex].DataType == 16 && $scope.defaultValueFields[fieldIndex].SubDataType == 6);
								$scope.showExtraProcessFields = $scope.defaultValueFields[fieldIndex].DataType == 5 || $scope.defaultValueFields[fieldIndex].SubDataType == 5;
								if ($scope.showExtraListFields || $scope.showExtraProcessFields) {
									$scope.showIgnoreEmpties = true;
								}
								else {
									$scope.showIgnoreEmpties = false;
                                }
							}
						}

						if (chartType == "multiList") {
							$scope.defaultValueFields = $scope.chartOptions.valueFields.Values;
							$scope.legendListOptions.push({LanguageTranstation: "", Name: ""});
							angular.forEach($scope.defaultValueFields, function (columnItem) {
								if (columnItem.DataType == 6 || columnItem.SubDataType == 6) {
									$scope.legendListOptions.push(columnItem);
								}
							});
						}
						if (chartType == 'resourceLoad') {
							$scope.resourceLoadTimeOptions = [{
								LanguageTranslation: 'CHART_MIXED_WEEK',
								value: 0
							}, {
								LanguageTranslation: 'CHART_MIXED_MONTH',
								value: 1
							}, {
								LanguageTranslation: 'CHART_MIXED_QUARTER',
								value: 2
							}, {LanguageTranslation: 'CHART_MIXED_YEAR', value: 3}];
							$scope.resourceLoadYesNOptions = [{
								LanguageTranslation: 'CHART_COMMON_YES',
								Value: 1
							}, {LanguageTranslation: 'CHART_COMMON_NO', Value: 0}];
							$scope.resourceItemTypes = [{
								LanguageTranslation: 'CHART_COMMON_LINE',
								value: 0
							}, {LanguageTranslation: 'CHART_COMMON_BAR', value: 1}];

							let colorAObj = Colors.getColors();
							$scope.customColor = Object.keys(colorAObj).map(name => ({
								name,
								value: colorAObj[name],
								color: name,
								symbol: "lens"
							}));
						}
						if (chartType == "pivot") {
							angular.forEach($scope.chartOptions.valueFields.Values, function (columnItem) {
								if (columnItem.DataType == 5 || columnItem.SubDataType == 5 || columnItem.DataType == 6 || columnItem.SubDataType == 6 || columnItem.DataType == 3 || columnItem.SubDataType == 3) {
									$scope.pivotXAxisOptions1.push(columnItem);
									$scope.pivotYAxisOptions1.push(columnItem);
									if ($scope.selections.pivotCommon.xAxisLevel1 == null || $scope.selections.pivotCommon.xAxisLevel1[0] != columnItem.Name) {
										$scope.pivotXAxisOptions2.push(columnItem);
									}
									if ($scope.selections.pivotCommon.xAxisLevel1 == null && $scope.selections.pivotCommon.xAxisLevel2 == null || $scope.selections.pivotCommon.xAxisLevel1[0] != columnItem.Name && $scope.selections.pivotCommon.xAxisLevel2[0] != columnItem.Name) {
										$scope.pivotXAxisOptions3.push(columnItem);
									}

									if ($scope.selections.pivotCommon.yAxisLevel1 == null || $scope.selections.pivotCommon.yAxisLevel1[0] != columnItem.Name) {
										$scope.pivotYAxisOptions2.push(columnItem);
									}
									if ($scope.selections.pivotCommon.yAxisLevel1 == null && $scope.selections.pivotCommon.yAxisLevel2 == null || $scope.selections.pivotCommon.yAxisLevel1[0] != columnItem.Name && $scope.selections.pivotCommon.yAxisLevel2[0] != columnItem.Name) {
										$scope.pivotYAxisOptions3.push(columnItem);
									}
								}
								if (columnItem.DataType == 1 || columnItem.DataType == 2 || columnItem.SubDataType == 1 || columnItem.SubDataType == 2) {
									$scope.pivotValueOptions.push(columnItem);
								}
								if (columnItem.DataType == 3 || columnItem.SubDataType == 3) {
									$scope.xAxesAllowedCumulatives.push(columnItem);
								}
							});

							$scope.pivotTimeOptions = [{
								LanguageTranslation: "DASHBOARD_PIVOT_YEAR",
								Name: 0
							}, {
								LanguageTranslation: "DASHBOARD_PIVOT_QUARTER",
								Name: 1
							}, {
								LanguageTranslation: "DASHBOARD_PIVOT_MONTH",
								Name: 2
							}, {LanguageTranslation: "DASHBOARD_PIVOT_WEEK", Name: 3}];

							let selectedColumn = $scope.pivotXAxisOptions1.filter(function (x) {
								return x.Name == $scope.selections.pivotCommon.xAxisLevel1;
							});
							if ($scope.selections.pivotCommon.xAxisLevel2.length == 0 && selectedColumn.length > 0 && (selectedColumn[0].DataType == 3 || selectedColumn[0].SubDataType == 3)) {
								$scope.aggregateTypes = [{
									LanguageTranslation: "CHART_COMMON_SUM",
									Value: 0
								}, {
									LanguageTranslation: "CHART_COMMON_MIN",
									Value: 1
								}, {
									LanguageTranslation: "CHART_COMMON_MAX",
									Value: 2
								}, {
									LanguageTranslation: "CHART_COMMON_AVERAGE",
									Value: 3
								}, {LanguageTranslation: "CHART_COMMON_CUMULATIVE", Value: 4}];
							} else {
								$scope.aggregateTypes = [{
									LanguageTranslation: "CHART_COMMON_SUM",
									Value: 0
								}, {
									LanguageTranslation: "CHART_COMMON_MIN",
									Value: 1
								}, {
									LanguageTranslation: "CHART_COMMON_MAX",
									Value: 2
								}, {LanguageTranslation: "CHART_COMMON_AVERAGE", Value: 3}];
							}

							$scope.tableOptions = [{
								LanguageTranslation: 'CHART_COMMON_YES',
								Value: 1
							}, { LanguageTranslation: 'CHART_COMMON_NO', Value: 0 }];

							$scope.specialSumOptions = [{
								LanguageTranslation: 'CHART_COMMON_YES',
								Value: 1
							}, { LanguageTranslation: 'CHART_PIVOT_YES_GROUPED', Value: 2 }, { LanguageTranslation: 'CHART_COMMON_NO', Value: 0 }];

							$scope.axisSelectionChanged = function (option) {
								$scope.GetAxisFieldOptions(option);

								switch (option) {
									case 1:
										$scope.selections.pivotCommon.xAxisLevel1Extra = 0;
										let selectedColumn = $scope.pivotXAxisOptions1.filter(function (x) {
											return x.Name == $scope.selections.pivotCommon.xAxisLevel1;
										});

										if ($scope.selections.pivotCommon.xAxisLevel2.length == 0 && $scope.aggregateTypes.length == 4 && selectedColumn.length > 0 && (selectedColumn[0].DataType == 3 || selectedColumn[0].SubDataType == 3)) {
											$scope.aggregateTypes.push({
												LanguageTranslation: "CHART_COMMON_CUMULATIVE",
												Value: 4
											});
										} else if (($scope.selections.pivotCommon.xAxisLevel2.length > 0 && $scope.aggregateTypes.length == 5) || ($scope.selections.pivotCommon.xAxisLevel2.length == 0 && selectedColumn[0].DataType != 3 && selectedColumn[0].SubDataType != 3 && $scope.aggregateTypes.length == 5)) {
											$scope.aggregateTypes.splice($scope.aggregateTypes.length - 1, 1);
										}
										break;
									case 2:
										$scope.selections.pivotCommon.xAxisLevel2Extra = 0;
										let selectedColumn = $scope.pivotXAxisOptions1.filter(function (x) {
											return x.Name == $scope.selections.pivotCommon.xAxisLevel1;
										});
										if ($scope.selections.pivotCommon.xAxisLevel2.length == 0 && $scope.aggregateTypes.length == 4 && selectedColumn.length > 0 && (selectedColumn[0].DataType == 3 || selectedColumn[0].SubDataType == 3)) {
											$scope.aggregateTypes.push({
												LanguageTranslation: "CHART_COMMON_CUMULATIVE",
												Value: 4
											});
										} else if (($scope.selections.pivotCommon.xAxisLevel2.length > 0 && $scope.aggregateTypes.length == 5) || ($scope.selections.pivotCommon.xAxisLevel2.length == 0 && selectedColumn.length > 0 && selectedColumn[0].DataType != 3 && selectedColumn[0].SubDataType != 3 && $scope.aggregateTypes.length == 5)) {
											$scope.aggregateTypes.splice($scope.aggregateTypes.length - 1, 1);
										}
										break;
									case 3:
										$scope.selections.pivotCommon.xAxisLevel3Extra = 0;
										break;
									case 4:
										$scope.selections.pivotCommon.yAxisLevel1Extra = 0;
										break;
									case 5:
										$scope.selections.pivotCommon.yAxisLevel2Extra = 0;
										break;
									case 6:
										$scope.selections.pivotCommon.yAxisLevel3Extra = 0;
										break;
									default:
									// Just placeholder for now
								}

								let xChanged = false; // Changes sub level options
								let yChanged = false;

								if (option > 0 && option > 4) {
									xChanged = true
									$scope.pivotXAxisOptions2 = [];
									$scope.pivotXAxisOptions3 = [];
								} else {
									yChanged = true;
									$scope.pivotYAxisOptions2 = [];
									$scope.pivotYAxisOptions3 = [];
								}


								angular.forEach($scope.pivotXAxisOptions1, function (columnItem) {
									if (xChanged) {
										if ($scope.selections.pivotCommon.xAxisLevel1 == null || $scope.selections.pivotCommon.xAxisLevel1[0] != columnItem.Name) {
											$scope.pivotXAxisOptions2.push(columnItem);
										}
										if ($scope.selections.pivotCommon.xAxisLevel1 == null && $scope.selections.pivotCommon.xAxisLevel2 == null || $scope.selections.pivotCommon.xAxisLevel1[0] != columnItem.Name && $scope.selections.pivotCommon.xAxisLevel2[0] != columnItem.Name) {
											$scope.pivotXAxisOptions3.push(columnItem);
										}
									}
									if (yChanged) {
										if ($scope.selections.pivotCommon.yAxisLevel1 == null || $scope.selections.pivotCommon.yAxisLevel1[0] != columnItem.Name) {
											$scope.pivotYAxisOptions2.push(columnItem);
										}
										if ($scope.selections.pivotCommon.yAxisLevel1 == null && $scope.selections.pivotCommon.yAxisLevel2 == null || $scope.selections.pivotCommon.yAxisLevel1[0] != columnItem.Name && $scope.selections.pivotCommon.yAxisLevel2[0] != columnItem.Name) {
											$scope.pivotYAxisOptions3.push(columnItem);
										}
									}
								});

							}
							$scope.GetAxisFieldOptions = function (option) {
								//selections.pivotCommon.xAxisLevel1

								let extraField = "";
								let selectionValue = 0;

								switch (option) {
									case 1:
										selectionValue = $scope.selections.pivotCommon.xAxisLevel1[0] || 0;
										extraField = "xAxisLevel1ExtraOptions";
										break;
									case 2:
										selectionValue = $scope.selections.pivotCommon.xAxisLevel2[0] || 0;
										extraField = "xAxisLevel2ExtraOptions";
										break;
									case 3:
										selectionValue = $scope.selections.pivotCommon.xAxisLevel3[0] || 0;
										extraField = "xAxisLevel3ExtraOptions";
										break;
									case 4:
										selectionValue = $scope.selections.pivotCommon.yAxisLevel1[0] || 0;
										extraField = "yAxisLevel1ExtraOptions";
										break;
									case 5:
										selectionValue = $scope.selections.pivotCommon.yAxisLevel2[0] || 0;
										extraField = "yAxisLevel2ExtraOptions";
										break;
									case 6:
										selectionValue = $scope.selections.pivotCommon.yAxisLevel3[0] || 0;
										extraField = "yAxisLevel3ExtraOptions";
										break;
									default:
									// Just placeholder for now
								}
								if (selectionValue == 0) return false;

								// columnItem.DataType == 3 || columnItem.SubDataType == 3
								let columnItem = $scope.pivotXAxisOptions1.filter(function (x) {
									return x.Name == selectionValue;
								});

								if (columnItem.length > 0) {
									if (columnItem[0].DataType == 3 || columnItem[0].SubDataType == 3) {
										$scope[extraField] = $scope.pivotTimeOptions;
									} else {
										let processName = "";
										if (columnItem[0].DataType == 6 || columnItem[0].DataType == 5) {

											processName = columnItem[0].DataAdditional;
										} else if (columnItem[0].DataType == 20 && (columnItem[0].SubDataType == 6 || columnItem[0].SubDataType == 5) || columnItem[0].DataType == 16) {
											processName = columnItem[0].SubDataAdditional;
										}

										ProcessService.getColumns(processName, true).then(function (response) {
											$scope[extraField] = [];

											angular.forEach(response, function (item) {
												$scope[extraField].push(item);
											});
										});
									}
								}

								//ProcessService.getColumns(listProcessName, true).then(function (response) {
								//    angular.forEach(response, function (item) {
								//        $scope.selectionOptions.colorListFieldOptions.push(item);
								//    });
								//});
							}

							for (let i = 1; i < 7; i++) {
								$scope.GetAxisFieldOptions(i);
							}
						}
					});
			} else {
				return $q.when();
			}
		}

		// Select element wants an array so array we give
		function parseJsonData() {
			$scope.selections = JSON.parse($scope.chart.JsonData);
			setFieldForGivenSelection('group');
			setFieldForGivenSelection('value');
			setFieldForGivenSelection('label');
			setFieldForGivenSelection('value2');
			setFieldForGivenSelection('name');
			setFieldForGivenSelection('subGroup');
		}

		// Select element returns array, but we need to pluck that single item out before we save it
		function setJsonData() {
			let cloned = _.clone($scope.selections);
			cloned.chartType = _.isArray(cloned.chartType) ? _.first(cloned.chartType) : cloned.chartType;
			$scope.chart.JsonData = JSON.stringify(cloned);
		}

		function saveDashboard() {
			setJsonData();
			SettingsService.ProcessDashboardChartSave(StateParameters.process, $scope.chart)
				.then(function () {
					$rootScope.$emit('portfoliofilter-changed', +$scope.chart.ProcessPortfolioId);
				});
		}

		function setMixedLogic() {
			$scope.getmixedDataPropertyLength = function () {
				if ($scope.selections.mixedData) {
					return Object.keys($scope.selections.mixedData).length;
				} else {
					return 0;
				}
			}

			$scope.mixedSelections["types"] = [{
				LanguageTranslation: 'CHART_COMMON_LINE',
				value: 1
			}, {LanguageTranslation: 'CHART_COMMON_BAR', value: 2}];
			$scope.mixedSelections["dateTypes"] = [{
				LanguageTranslation: 'CHART_MIXED_DAY',
				value: 1
			}, {LanguageTranslation: 'CHART_MIXED_WEEK', value: 2}, {
				LanguageTranslation: 'CHART_MIXED_MONTH',
				value: 3
			}, {LanguageTranslation: 'CHART_MIXED_QUARTER', value: 4}, {
				LanguageTranslation: 'CHART_MIXED_YEAR',
				value: 5
			}];
			$scope.mixedSelections["modes"] = [{
				LanguageTranslation: 'CHART_COMMON_SUM',
				value: 1
			}, {LanguageTranslation: 'CHART_COMMON_MAX', value: 2}, {
				LanguageTranslation: 'CHART_COMMON_MIN',
				value: 3
			}, {LanguageTranslation: 'CHART_COMMON_AVERAGE', value: 4}, {
				LanguageTranslation: 'CHART_COMMON_CUMULATIVE',
				value: 5
			}, {LanguageTranslation: 'CHART_MIXED_LAST_VALUE', value: 6}];
			$scope.mixedSelections["modesLite"] = [{
				LanguageTranslation: 'CHART_COMMON_SUM',
				value: 1
			}, {LanguageTranslation: 'CHART_COMMON_MAX', value: 2}, {
				LanguageTranslation: 'CHART_COMMON_MIN',
				value: 3
			}];
			$scope.mixedSelections["lineFillTypes"] = [{
				LanguageTranslation: 'CHART_MIXED_NO_FILL',
				value: 0
			}, {LanguageTranslation: 'CHART_MIXED_TRANSPARENT', value: 1}, {
				LanguageTranslation: 'CHART_MIXED_SOLID',
				value: 2
			}];
			$scope.mixedSelections["legendOptions"] = [{
				LanguageTranslation: 'CHART_COMMON_YES',
				value: 0
			}, {LanguageTranslation: 'CHART_COMMON_NO', value: 1}];
			let colorAObj = Colors.getColors();
			$scope.mixedSelections["customColor"] = Object.keys(colorAObj).map(name => ({
				name,
				value: colorAObj[name],
				color: name,
				symbol: "lens"
			}));

			$scope.mixedSelections["lineGraphicStyles"] = [{
				LanguageTranslation: "CHART_MIXED_INVISIBLE",
				value: "invisible"
			}, {
				LanguageTranslation: 'CHART_MIXED_CURVED_LINE',
				value: 'curvedLine'
			}, {
				LanguageTranslation: 'CHART_MIXED_CURVED_DASHED_LINE',
				value: 'curvedDashedLine'
			}, {
				LanguageTranslation: 'CHART_MIXED_STRAIGHT_LINE',
				value: 'straightLine'
			}, {
				LanguageTranslation: 'CHART_MIXED_STRAIGHT_DASHED_LINE',
				value: 'straightDashedLine'
			}, {
				LanguageTranslation: 'CHART_MIXED_STRAIGHT_STEPPED_LINE',
				value: 'steppedLine'
			}, {LanguageTranslation: 'CHART_MIXED_STRAIGHT_STEPPED_DASHED_LINE', value: 'steppedDashedLine'}]


			$scope.mixedSelections["linePointStyles"] = [{
				LanguageTranslation: 'CHART_MIXED_CIRCLE',
				value: 'circle'
			}, {LanguageTranslation: 'CHART_MIXED_CROSS', value: 'cross'}, {
				LanguageTranslation: 'CHART_MIXED_CROSSROT',
				value: 'crossRot'
			}, {LanguageTranslation: 'CHART_MIXED_DASH', value: 'dash'}, {
				LanguageTranslation: 'CHART_MIXED_LINE',
				value: 'line'
			}, {
				LanguageTranslation: 'CHART_MIXED_RECT',
				value: 'rect'
			}, {
				LanguageTranslation: 'CHART_MIXED_RECTROUNDED',
				value: 'rectRounded'
			}, {LanguageTranslation: 'CHART_MIXED_RECTROT', value: 'rectRot'}, {
				LanguageTranslation: 'CHART_MIXED_STAR',
				value: 'star'
			}, {LanguageTranslation: 'CHART_MIXED_TRIANGLE', value: 'triangle'}];

			$scope.mixedSelections["gradientOrder"] = [{
				LanguageTranslation: "CHART_MIXED_GRADIENT_ORDER_1",
				value: "0"
			}, {LanguageTranslation: "CHART_MIXED_GRADIENT_ORDER_2", value: "1"}];
			$scope.mixedSelections["gradientPointers"] = [{
				LanguageTranslation: "",
				value: "0"
			}, {
				LanguageTranslation: "CHART_MIXED_GRADIENT_POINTER_CIRCLE",
				value: "1"
			}, {
				LanguageTranslation: "CHART_MIXED_GRADIENT_POINTER_CIRCLE_FILLED",
				value: "2"
			}, {
				LanguageTranslation: "CHART_MIXED_GRADIENT_POINTER_ARROW",
				value: "3"
			}, {LanguageTranslation: "CHART_MIXED_GRADIENT_POINTER_PERCENTAGE", value: "4"}];

			$scope.mixedSelections["tableOptions"] = [{
				LanguageTranslation: 'CHART_COMMON_YES',
				value: 1
			}, {LanguageTranslation: 'CHART_COMMON_NO', value: 0}];
			$scope.mixedSelections["zoomSelections"] = [{
				LanguageTranslation: 'CHART_COMMON_IN_USE',
				value: 1
			}, {LanguageTranslation: 'CHART_COMMON_NOT_IN_USE', value: 2}];
			if (typeof ($scope.selections.mixedIndexes) == 'undefined') $scope.selections["mixedIndexes"] = [0];
			for (let i = 0; i < $scope.selections.mixedIndexes.length; i++) {
				$scope.selections.mixedIndexes[i] = i;
			}

			if ($scope.selections.mixedDataCommon && $scope.selections.mixedDataCommon.showLegend && $scope.selections.mixedDataCommon.showLegend.length == 0) {
				$scope.selections.mixedDataCommon.showLegend.push(0);
			}
			let indexCount = 0;
			let newObject = {};
			angular.forEach($scope.selections.mixedData, function (content) {
				newObject[indexCount] = content;
				indexCount += 1;
			});
			$scope.selections.mixedData = newObject;
		}

		function setMultiListLogic() {
			$scope.multiSelections["listTypes"] = [{name: 'Amount', value: 0}, {name: 'Percent', value: 1}];
			$scope.multiSelections["commonYesNo"] = [{name: 'Yes', value: 1}, {name: 'No', value: 0}];

			if (typeof ($scope.selections.multiListIndexes) == 'undefined') {
				$scope.selections["multiListIndexes"] = [0];
			}

			for (let i = 0; i < $scope.selections.multiListIndexes.length; i++) {
				$scope.selections.multiListIndexes[i] = i;
			}
		}

		$scope.addMixedRow = function () {
			let length = $scope.selections["mixedIndexes"].length;
			let lastIndex = $scope.selections["mixedIndexes"][length - 1];
			$scope.selections["mixedIndexes"].push(lastIndex + 1);
		};

		$scope.addCircular = function () {
			$scope.selections.circularData.push({ value1: [], value2: [], ignoreNulls: [], ignoreZeros: [], aggregateType: [], title: {}, suffix: {}, decimals:[]});
		}
		$scope.deleteCircular = function (index) {
			$scope.selections.circularData.splice(index, 1);
        }

		$scope.deleteMixedRow = function (index) {
			delete $scope.selections.mixedData[index];
			$scope.selections.mixedIndexes.splice(index, 1);

			let newObject = {};
			let indexCount = 0;
			angular.forEach($scope.selections.mixedData, function (content) {
				newObject[indexCount] = content;
				indexCount += 1;
			});
			$scope.selections.mixedData = newObject;

			for (let i = 0; i < $scope.selections.mixedIndexes.length; i++) {
				$scope.selections.mixedIndexes[i] = i;
			}
		};

		$scope.addMultiListRow = function () {
			let length = $scope.selections["multiListIndexes"].length;
			let lastIndex = $scope.selections["multiListIndexes"][length - 1];
			$scope.selections["multiListIndexes"].push(lastIndex + 1);
		};

		$scope.deleteMultiListRow = function (index) {
			delete $scope.selections.multiListData[index];
			$scope.selections.multiListIndexes.splice(index, 1);

			let newObject = {};
			let indexCount = 0;
			angular.forEach($scope.selections.multiListData, function (content) {
				newObject[indexCount] = content;
				indexCount += 1;
			});
			$scope.selections.multiListData = newObject;

			for (let i = 0; i < $scope.selections.multiListIndexes.length; i++) {
				$scope.selections.multiListIndexes[i] = i;
			}
		};

		$scope.xAxisSourceChanged = function xAxisSourceChanged() {
			let fieldIndex = $scope.chartOptions.valueFields.Values.findIndex(function (x) {
				return x.Name == $scope.selections.mixedDataCommon.xaxis[0];
			});

			if ($scope.selections.mixedDataCommon.xaxis == null || $scope.selections.mixedDataCommon.xaxis == 0 || $scope.selections.mixedDataCommon.xaxis == 'noSelection') {
				$scope.showExtraDateFields = false;
				$scope.showExtraListFields = false;
			} else {
				let subDataType = $scope.chartOptions.valueFields.Values[fieldIndex].SubDataType;
				$scope.showExtraDateFields = $scope.xAxesAllowedTypes[fieldIndex] == 3 || subDataType == 3 || $scope.xAxesAllowedTypes[fieldIndex] == 13 || subDataType == 13;
				$scope.showExtraListFields = $scope.xAxesAllowedTypes[fieldIndex] == 6 || ($scope.defaultValueFields[fieldIndex].DataType == 16 && $scope.defaultValueFields[fieldIndex].SubDataType == 6);
				$scope.showExtraProcessFields = $scope.defaultValueFields[fieldIndex].DataType == 5 || $scope.defaultValueFields[fieldIndex].SubDataType == 5;
				if ($scope.showExtraListFields || $scope.showExtraProcessFields) {
					$scope.showIgnoreEmpties = true;
				}
				else {
					$scope.showIgnoreEmpties = false;
				}
			}
		};

		$scope.colorListChanged = function () {
			$scope.selectionOptions.colorListFieldOptions = [];
			$scope.selections.colorListField = null;
			if ($scope.selections.colorList != null || $scope.selections.colorList != "") {
				let columnItem = $scope.selectionOptions.colorListOptions.filter(function (x) {
					return x.Name == $scope.selections.colorList;
				});
				if (columnItem.length > 0) {
					let listProcessName = "";
					if (columnItem[0].DataType == 20) {
						listProcessName = columnItem[0].InputName;
					} else {
						if (columnItem[0].DataType == 16) {
							listProcessName = columnItem[0].SubDataAdditional;
						} else {
							listProcessName = columnItem[0].DataAdditional;
						}

					}
					ProcessService.getColumns(listProcessName, true).then(function (response) {
						angular.forEach(response, function (item) {
							$scope.selectionOptions.colorListFieldOptions.push(item);
						});
					});
				}
			}
		}

		$scope.changing = false;
		$scope.changeType = function () {
			$scope.changing = true;
		}

		$scope.choosePreview = function (preview) {
			$scope.oldType = angular.copy($scope.selections.chartType);
			$scope.selections.chartType = preview.basedOn;
			$scope.header.title = "CHART EDIT " + $scope.selections.chartType;
			$scope.chartTypeChanged(preview);
		}

		function initController() {
			parseJsonData();
			getFieldsForChartType();
			$scope.header = {
				title: "CHART EDIT " + $scope.selections.chartType
			}
		}


		initController();
	}
})();
