(function () {
    'use strict';
    angular
    .module('core.settings')
    .constant("COLOUR_SCHEMES", {
        red: {
            translationKey: 'COLOUR_RED',
            colours: {
                '50': '#FFEBEE',
                '100': '#FFCDD2',
                '200': '#EF9A9A',
                '300': '#E57373',
                '400': '#EF5350',
                '500': '#F44336',
                '600': '#E53935',
                '700': '#D32F2F',
                '800': '#C62828',
                '900': '#B71C1C'
            }
        },
        pink: {
            translationKey: 'COLOUR_PINK',
            colours: {
                '50': '#FCE4EC',
                '100': '#F8BBD0',
                '200': '#F48FB1',
                '300': '#F06292',
                '400': '#EC407A',
                '500': '#E91E63',
                '600': '#D81B60',
                '700': '#C2185B',
                '800': '#AD1457',
                '900': '#880E4F'
            }
        },
        purple: {
            translationKey: 'COLOUR_PURPLE',
            colours: {
                '50': '#F3E5F5',
                '100': '#E1BEE7',
                '200': '#CE93D8',
                '300': '#BA68C8',
                '400': '#AB47BC',
                '500': '#9C27B0',
                '600': '#8E24AA',
                '700': '#7B1FA2',
                '800': '#6A1B9A',
                '900': '#4A148C'
            }
        },
        blue: {
            translationKey: 'COLOUR_BLUE',
            colours: {
                '50': '#E3F2FD',
                '100': '#BBDEFB',
                '200': '#90CAF9',
                '300': '#64B5F6',
                '400': '#42A5F5',
                '500': '#2196F3',
                '600': '#1E88E5',
                '700': '#1976D2',
                '800': '#1565C0',
                '900': '#0D47A1'
            }
        },
        cyan: {
            translationKey: 'COLOUR_CYAN',
            colours: {
                '50': '#E0F7FA',
                '100': '#B2EBF2',
                '200': '#80DEEA',
                '300': '#4DD0E1',
                '400': '#26C6DA',
                '500': '#00BCD4',
                '600': '#00ACC1',
                '700': '#0097A7',
                '800': '#00838F',
                '900': '#006064'
            }
        },
        deepOrange: {
            translationKey: 'COLOUR_DEEP_ORANGE',
            colours: {
                '50': '#FBE9E7',
                '100': '#FFCCBC',
                '200': '#FFAB91',
                '300': '#FF8A65',
                '400': '#FF7043',
                '500': '#FF5722',
                '600': '#F4511E',
                '700': '#E64A19',
                '800': '#D84315',
                '900': '#BF360C'
            }
        },
        orange: {
            translationKey: 'COLOUR_ORANGE',
            colours: {
                '50': '#FFF3E0',
                '100': '#FFE0B2',
                '200': '#FFCC80',
                '300': '#FFB74D',
                '400': '#FFA726',
                '500': '#FF9800',
                '600': '#FB8C00',
                '700': '#F57C00',
                '800': '#EF6C00',
                '900': '#E65100'
            }
        }
    })
    .service('ChartColourService', ChartColourService);

    ChartColourService.$inject = [
        'COLOUR_SCHEMES'
    ];
    function ChartColourService(COLOUR_SCHEMES){
        var defaultOrderOfColourKeys = [
            '500',
            '300',
            '700',
            '100',
            '900',
            '400',
            '600',
            '200',
            '800',
            '50'
        ];
        function getColourMixForKeys(colourKeys) {
            var colours = [];
            _.each(defaultOrderOfColourKeys, function(key){
                _.each(colourKeys, function(colorKey){
                    colours.push(COLOUR_SCHEMES[colorKey].colours[key]);
                });
           });
           return colours;
        }
        function hexToRgb(hex) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        }

        return {
            /**
             * Converts #FFFFFF formatted color to rgba(r, b, g, a) format
             * with given opacity
             **/
            convertToRgbaWithOpacity: function(hexColor, opacity) {
                var rgbs = hexToRgb(hexColor);
                return 'rgba(' + rgbs.r + ', ' + rgbs.g + ', ' + rgbs.b + ', ' + opacity + ')';
            },

            /**
             * Returns list of colours with preferred colours in front
             **/
            getColours: function(preferred){
                var selectedColors = [];

                if(!_.isEmpty(preferred) && _.isArray(preferred)){
                    selectedColors = preferred;
                }
                var colours = getColourMixForKeys(selectedColors);
                var restOfTheColours = _.filter(_.keys(COLOUR_SCHEMES), function(c){
                    return _.indexOf(selectedColors, c) === -1;
                });
                colours = _.concat(colours, getColourMixForKeys(restOfTheColours));
                return colours;
            }
        };
    }
})();
