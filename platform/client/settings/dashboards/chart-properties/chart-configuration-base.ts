(function () {
    'use strict';

    angular
        .module('core.settings')
        .service('DashboardChartTypeConfig', DashboardChartTypeConfig);

    function DashboardChartTypeConfig() {
        var configs = {},
            dataParserFunctions = {},
            fieldConfigurations = {};

        var factory = {
            addConfig: function(chartType, config) {
                if(configs[chartType]) {
                    throw ('Config for chart type ' + key + '  already set');
                }
                configs[chartType] = config;
            },

            getConfig: function(chartType) {
                return angular.copy(configs[chartType]);
            },

            addDataParser: function(chartType, parserFunction) {
                if(dataParserFunctions[chartType]) {
                    throw ('DataParser for chart type ' + key + '  already set');
                }
                dataParserFunctions[chartType] = parserFunction;
            },

            getDataParser: function(chartType) {
                return dataParserFunctions[chartType];
            },

            addFieldConfiguration: function(chartType, configuration){
                if(fieldConfigurations[chartType]) {
                    throw ('Field configurations for chart type ' + key + '  already set');
                }
                fieldConfigurations[chartType] = configuration;
            },

            getFieldConfiguration: function(chartType) {
                return fieldConfigurations[chartType];
            }

        };

        return factory;
    }
})();
