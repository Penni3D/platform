(function () {
    'use strict';

    var CHART_NAME = 'bar';

    angular
        .module('core.settings')
        .run(BarChartConfiguration);

    BarChartConfiguration.$inject = [
        'DashboardChartTypeConfig',
        'BarPieParserFactory',
		'Translator'
    ];
    function BarChartConfiguration(DashboardChartTypeConfig, BarPieParserFactory, Translator) {
        DashboardChartTypeConfig.addFieldConfiguration(CHART_NAME, {
            templateUrl: 'settings/dashboards/chart-properties/bar/bar-chart-config-fields.html'
        });
        
        DashboardChartTypeConfig.addConfig(CHART_NAME, {
            chartOptions: {
                title: { display: true, text: 'Title'},
                scales: {
                    xAxes: [{
                        ticks: {
                            autoSkip: false
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        
        

        DashboardChartTypeConfig.addDataParser(CHART_NAME, function(response){
        	var unkownText = Translator.translate('DASHBOARD_CHART_UNKNOWN_LABEL');

            if (!response.stacked) {
				return BarPieParserFactory(response);
			}
						
            var mainIds = [];
            var subIds = [];
            var dataGrouped = {};
			var sums = {};
			var totalSum = 0;
			
			_.each(response.data, function (dataRow) {
				var mainId = +dataRow['title'] || 0;
				var subId = +dataRow['sub_title'] || 0;
				mainIds.push(mainId);
				subIds.push(subId);
			});
			
			mainIds = _.uniq(mainIds);
			subIds = _.uniq(subIds);
			
            _.each(response.data, function (dataRow) {
            	var value = +dataRow.val;
            	totalSum += value;
				var mainId = +dataRow['title'];
				var subId = +dataRow['sub_title'];
				
				sums[mainId] = sums[mainId] || 0;
				sums[mainId] += value;
				
				dataGrouped[mainId] = dataGrouped[mainId] || {};
				dataGrouped[mainId][subId] = value;
			});
            
			var dataSet = _.map(subIds,  function (subId) {
				return _.map(mainIds,  function (mainId) {
					return _.get(dataGrouped, mainId + '.' + subId) || 0;
				});
			});
            
            var labels = _.map(mainIds,  function (id) {
				
            	var labelObj = response.mainLabels[id];
				if (labelObj){
					return labelObj[response.chartFields.GroupByFieldProperty] || unkownText;
				}
				return unkownText;
			});
            
            var series = _.map(subIds, function (subId) {
				var serieObj = response.subLabels[subId];
				if (serieObj) {
					return serieObj[response.chartFields.SubGroupByFieldProperty] || unkownText;
				}
				return unkownText;
			});
            
            var returnObj = {
				values: dataSet,
				labels: labels,
				series: series,
                stackedYAxis: true,
				stackedXAxis: true
            };
            
            return returnObj;
        } );
    }
})();
