(function () {
    'use strict';

    let CHART_NAME = 'bubble';

    angular
        .module('core.settings')
        .run(BubbleChartConfiguration);

    BubbleChartConfiguration.$inject = [
        'DashboardChartTypeConfig', '$rootScope'
    ];
    function BubbleChartConfiguration(DashboardChartTypeConfig,$rootScope) {
        DashboardChartTypeConfig.addFieldConfiguration(CHART_NAME, {
            templateUrl: 'settings/dashboards/chart-properties/bubble/bubble-chart-config-fields.html'
        });

        DashboardChartTypeConfig.addConfig(CHART_NAME, {
            chartOptions: {
                title: { display: true, text: 'Title'},
	            scaleShowVerticalLines: false, 
                scales: {
                    xAxes: [{
	                    gridLines: {
		                    display: true,
		                    drawBorder: false,
		                    lineWidth: 2,
		                    color: '#f0f1f4',
		                    zeroLineWidth: 2,
		                    zeroLineColor: '#f0f1f4'
	                    },
                    	ticks: {
		                    fontFamily: 'Roboto',
		                    fontSize: 12,
		                    fontColor: '#676a6e',
		                    min: 0,
                            autoSkip: false
	                    }
                    }],
                    yAxes: [{
	                    gridLines: {
		                    display: true,
		                    drawBorder: false,
		                    lineWidth: 2,
		                    color: '#f0f1f4',
		                    zeroLineWidth: 2,
		                    zeroLineColor: '#f0f1f4'
	                    },
                        ticks: {
	                        fontFamily: 'Roboto',
	                        fontSize: 12,
	                        fontColor: '#676a6e',
                            min: 0
                        }
                    }]
                },
	            tooltips:{
                	callbacks:{
		                label: function(tooltipItem, data) {
                            let dataPoint = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                return dataPoint.name + ': (' + tooltipItem.xLabel + ', ' + tooltipItem.yLabel + ', ' + dataPoint.originalR + ')';
		                }
	                }
                }
            }
        });

        DashboardChartTypeConfig.addDataParser(CHART_NAME, function (response) {
            let linkField = null;
            //response.linkField
            if (response.chartFields.OriginalData.mainProcessLinkMode) linkField = response.chartFields.OriginalData.link_process_field || null;

        	let xPropertyType = _.get(response, 'fieldTypes.labelFieldType.data_type');
            let yPropertyType = _.get(response, 'fieldTypes.valueFieldType.data_type');
            let namePropertyType = _.get(response, 'fieldTypes.nameFieldType.data_type');
        	let xLabelConfigs = {};
            let yLabelConfigs = {};
	        let reverseConfigs = {};
            let labels = [];
           
            let xAxisIsList = false;
            let yAxisIsList = false;

            if (response.fieldTypes.horizontalList != null) {
                xAxisIsList = true;
            }
            if (response.fieldTypes.verticalList != null) {
                yAxisIsList = true;
            }

            let disperse = 0;
            if (response.chartFields.OriginalData.disperse != null) disperse = parseInt(response.chartFields.OriginalData.disperse)

	        let xStepSize = (xPropertyType == 5) || (xPropertyType == 6) || (xPropertyType == 4) || (xPropertyType == 0) ? 1 : null;
            let yStepSize = (yPropertyType == 5) || (yPropertyType == 6) || (yPropertyType == 4) || (yPropertyType == 0) ? 1 : null;

	        let nameField = response.chartFields.NameField;
            let valueField = response.chartFields.ValueField;
            let value2Field = response.chartFields.Value2Field;
	        let valueFieldProperty = response.chartFields.ValueFieldProperty;
	        let labelField = response.chartFields.LabelField;
            let labelFieldProperty = response.chartFields.LabelFieldProperty;
            let nameFieldProperty = response.chartFields.NameFieldProperty;

            if (value2Field == "processItemCount") {
                for (let i = 0; i < response.data.length; i++) {
                    if (response.data[i][valueField] != null && response.data[i][labelField] != null) {
                        response.data[i]["processItemCount"] = response.data.filter(function (x) { return x[labelField] == response.data[i][labelField] && x[valueField] == response.data[i][valueField]; }).length;
                    }
                    else {
                        response.data[i]["processItemCount"] = 0;
                    }
                }
            }

            if (xPropertyType == 5) {
                let foundItems = [];
                angular.forEach(response.data, function (row) {
                    if (row[labelField] != null || parseInt(row[labelField]) != 0) {
                        foundItems.push(parseInt(row[labelField]));
                    }                
                });
                let validListItems = response.labelItems.filter(function (x) {
                    return x.in_use != 0 || foundItems.indexOf(x.item.id) > -1;
                });

                response.labelItems = validListItems;
            }
            if (yPropertyType == 5) {
                let foundItems = [];
                angular.forEach(response.data, function (row) {
                    if (row[labelField] != null || parseInt(row[valueField]) != 0) {
                        foundItems.push(parseInt(row[valueField]));
                    }
                });
                let validListItems = response.valueItems.filter(function (x) {
                    return x.in_use = 1 || foundItems.indexOf(x.item.id) > -1;
                });

                response.valueItems = validListItems;
            }

            let fieldWithLanguage = String('list_item_' + $rootScope.clientInformation["locale_id"]).toLowerCase().replace("-", "_");
            if (xPropertyType == 6) {               
                if (response.labelItemsOrder == "list_item" && response.labelItems.length>0 && response.labelItems[0].hasOwnProperty(fieldWithLanguage)) response.labelItemsOrder = fieldWithLanguage;
                response.labelItems = response.labelItems.sort(dynamicSort(response.labelItemsOrder));
            }
            if (yPropertyType == 6) {
                if (response.valueItemsOrder == "list_item" && response.valueItems.length > 0 &&  response.valueItems[0].hasOwnProperty(fieldWithLanguage)) response.valueItemsOrder = fieldWithLanguage;
                response.valueItems = response.valueItems.sort(dynamicSort(response.valueItemsOrder));
            }
                             
            if (labelFieldProperty == "list_item" && response.labelItems.length > 0 && response.labelItems[0].hasOwnProperty(fieldWithLanguage)) {              
                labelFieldProperty = fieldWithLanguage;
            }
            if (valueFieldProperty == "list_item" && response.valueItems.length > 0 && response.valueItems[0].hasOwnProperty(fieldWithLanguage)) {
                valueFieldProperty = fieldWithLanguage;
            }

	        let xLabels = _.map(response.labelItems, function (i) {
                return i[labelFieldProperty];
            });
            let yLabels = _.map(response.valueItems, function (i) {
                return i[valueFieldProperty];
            });

            //if list or process mode
            if (xPropertyType == 5 || xPropertyType == 6) {
                xLabels.push("");
            }
            if (yPropertyType == 5 || yPropertyType == 6) {
                yLabels.push("");
            }

            let nameLabels = _.map(response.nameItems, function (i) {
                return i[nameFieldProperty];
            });

            let colors = response.colorItems;
            let colorOrder = response.order;

            if (colors && colors.length>0) colors = colors.sort(dynamicSort(colorOrder));
            let legendColors = [];
            let legendNames = [];

            let ledendNameField = "list_item";
            if (response.chartFields.OriginalData.legendListField != null ||response.chartFields.OriginalData.legendListField != "") ledendNameField = response.chartFields.OriginalData.legendListField;
            if (ledendNameField == "list_item" && response.colorItems && response.colorItems.length > 0 && response.colorItems[0].hasOwnProperty(fieldWithLanguage)) {
                ledendNameField = fieldWithLanguage;
            }

            angular.forEach(colors, function (item) {
                if (item[response.chartFields.OriginalData.colorListField]!=null) legendColors.push(item[response.chartFields.OriginalData.colorListField]);
                if (item[response.chartFields.OriginalData.legendListField] != null) legendNames.push(item[ledendNameField]);
            });

            if (colors != null && colors.length == 0) { colors = null;}

	        _.each(response.labelItems, function (item, index) {
	            xLabelConfigs[item.item_id] = index;
				reverseConfigs[item.item_id] = index;
            });
            _.each(response.valueItems, function (item, index) {
                yLabelConfigs[item.item_id] = index;
                reverseConfigs[item.item_id] = index;
            });

            let greatestValues=
                {
                    x: 0,
                    y: 0,
                    r: 0
                };

            let colorData = [];
            let data = _.chain(response.data)
                .map(function (dataItem, ind) {
                    let x;
                    let y;
                    let xLabel;
                    let listName;
                    let yLabel;
                    let id;
                    if (xPropertyType == 5 || xPropertyType == 6) {
                        id = dataItem[labelField];
                        x = +xLabelConfigs[+id];
                        xLabel = xLabels[x];
                    } else {
                        x = dataItem[labelField];
                        xLabel = dataItem[labelField];
                    }
                    
                    if (yPropertyType == 5 || yPropertyType == 6){
                        id = dataItem[valueField];
                        y = +yLabelConfigs[+id];
                        yLabel = yLabels[y];
                    } else {
                        y = dataItem[valueField];
                        yLabel = dataItem[valueField];
                    }//nameFieldType

                    if (namePropertyType == 5 || namePropertyType == 6) {
                        listName = nameLabels[x];
                    } 
                    let color = "";
                    let legend = "";
                    if (colors != null && colors.length > 0) {
                        let colorIdArr = dataItem[response.chartFields.OriginalData.colorList];
                        if (colorIdArr != null) {
                            let listItem = colors.filter(function (x) {
                                return x.item_id == colorIdArr;
                            });
                            if (listItem.length > 0) {
                                color = listItem[0][response.chartFields.OriginalData.colorListField];
                            }
                        }
                    }
                    
                    let r = +dataItem[value2Field];
                    
                    if (x > greatestValues.x){greatestValues.x = x}
                    if (y > greatestValues.y){greatestValues.y = y}
                    if (r > greatestValues.r) { greatestValues.r = r }

                    labels.push(dataItem['item_id']);
                    let name = dataItem[nameField];
                    if (listName != null) name = listName;

                    if (colors != null) {
                        colorData.push(color);
                    }
                    let xExtra = 0;
                    let yExtra = 0;
                    if (xPropertyType === 5 || xPropertyType == 6) {
                        xExtra = 0.5;
                        if (disperse == 1) {
                            xExtra = parseFloat(Math.floor(Math.random() * 9) + 1)/10;
                        }
                    }
                    if (yPropertyType === 5 || yPropertyType == 6) {
                        yExtra = 0.5;
                        if (disperse == 1) {
                            yExtra = parseFloat(Math.floor(Math.random() * 9) + 1) / 10;
                        }
                    }

                    return { x: x + xExtra, y: y + yExtra, r: r, name: name, xLabel: xLabel, yLabel: yLabel, item_id: dataItem["item_id"], color: color, linkItemId: dataItem[linkField]};
                })
                .sortBy('r')
                .reverse()
                .value();

            let xMin;
            let yMin;
            if (xStepSize === 1){
                xMin = -0.3;
            }
            if (yStepSize === 1){
                yMin = -0.3;
            }
            let scalingFactor = 35 / greatestValues.r;
            _.each(data,  function (val) {
                val.originalR = val.r;
                val.r = val.r * scalingFactor;
            });

            data.push({ item_id: -1000, name: "", originalR: 0, r: 0, y: yLabels.length - 1, x: xLabels.length - 1, xLabel: "", yLabel: ""}); //This row's fuction is to force chart to show all list items defined in arrays

            let autoScale = response.chartFields.OriginalData.autoScale || 0;

            return {
                values: data,
                xAxisCallback: function(what, a, b){
                    if (_.isEmpty(xLabels)){
                        return what;
                    }
                    return _.get(xLabels, what);
                },
                yAxisCallback: function(what, a, b){
                    if (_.isEmpty(yLabels)){
                        return what;
                    }
                    return _.get(yLabels, what);
                },
                legendColors: legendColors,
                legendNames: legendNames,
                xStepSize: xStepSize,
                yStepSize: yStepSize,
                tooltipLabelCallback: true,
                padding: 35,
                colors: colorData,
                xPropertyType: xPropertyType,
                yPropertyType: yPropertyType,
                greatestRadius: greatestValues.r,
                scalingFactor: scalingFactor,
                autoScale: autoScale,
                disperse: disperse,
                linkField: linkField,
                chartConfig: response.chartFields,
                filterData: response.filterData
            };
        });      
    }
    function dynamicSort(property) {
        let sortOrder = 1;

        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }

        return function (a, b) {
            if (sortOrder == -1) {
                if (typeof b[property] == 'number') {
                    return b[property] - a[property];
                }
                else {
                    if (property == "order_no") {
                        if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                            return -1;
                        } else if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                            return 1;
                        }
                        return b[property].localeCompare(a[property]);
                    }
                    else {
                        return b[property].localeCompare(a[property]);
                    }
                }

            } else {
                if (typeof b[property] == 'number') {
                    return a[property] - b[property];
                }
                else {
                    if (property == "order_no") {
                        if (startsWithUppercase(a[property]) && !startsWithUppercase(b[property])) {
                            return -1;
                        } else if (startsWithUppercase(b[property]) && !startsWithUppercase(a[property])) {
                            return 1;
                        }
                        return a[property].localeCompare(b[property]);
                    }
                    else {
                        return a[property].localeCompare(b[property]);
                    }
                }
            }
        }
    }
    function startsWithUppercase(str) {
        return str.substr(0, 1).match(/[A-Z\u00C0-\u00DC]/);
    }
})();
