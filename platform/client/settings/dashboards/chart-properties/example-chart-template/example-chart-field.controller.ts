(function () {
	'use strict';

	var CHART_NAME = 'exampleChart';

	angular
		.module('core.settings')
		.run(exampleChartConfiguration);

	
	exampleChartConfiguration.$inject = ['DashboardChartTypeConfig'];
	
	function exampleChartConfiguration(DashboardChartTypeConfig) {
		
		return; // Remove this row to make this appear in type list

		// This is templateUrl for configuration page html
		// DashboardChartTypeConfig.addFieldConfiguration(CHART_NAME, {
		// 	templateUrl: 'settings/dashboards/chart-properties/example/example-chart-config-fields.html'
		// });
		//
		//
		// // This will be used as 'chart-options' for current chart (google Chart.js for documentation)
		// DashboardChartTypeConfig.addConfig(CHART_NAME, {
		// 	chartOptions: {
		// 		title: { display: true, text: 'Title'},
		// 		scaleShowVerticalLines: false,
		// 		scales: {
		// 			xAxes: [{
		// 				gridLines: {
		// 					display: false
		// 				},
		// 				ticks: {
		// 					min: 0,
		// 					autoSkip: false
		// 				}
		// 			}],
		// 			yAxes: [{
		// 				ticks: {
		// 					min: 0
		// 				}
		// 			}]
		// 		},
		// 		tooltips:{
		// 			callbacks:{
		// 				label: function(tooltipItem, data) {
		// 					var dataPoint = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		// 					return dataPoint.name + ': (' + tooltipItem.xLabel + ', ' + tooltipItem.yLabel + ', ' + dataPoint.originalR + ')';
		// 				}
		// 			}
		// 		}
		// 	}
		// });

		
		// This is data parser for this chart type. The reveives data returned by GetDataFor-method of this datatype
		// DashboardChartTypeConfig.addDataParser(CHART_NAME, function(response) {
		//	
		// 	var returnObj =  {
		// 		values: values,
		// 		labels: labels,
		// 		series: ['Serie A']
		// 	};
		// 	return returnObj;
		// });
	}
})();
