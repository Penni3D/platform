(function () {
    'use strict';

    var CHART_NAME = 'pie';

    angular
        .module('core.settings')
        .run(PieChartConfiguration);

    PieChartConfiguration.$inject = [
        'DashboardChartTypeConfig',
        'BarPieParserFactory'
    ];
    function PieChartConfiguration(DashboardChartTypeConfig, BarPieParserFactory) {
        DashboardChartTypeConfig.addFieldConfiguration(CHART_NAME, {
            templateUrl: 'settings/dashboards/chart-setup/dashboard-chart-basic-config-fields.html'
        });

        DashboardChartTypeConfig.addConfig(CHART_NAME, {
            chartOptions: {
                // legend: {
                //     display: true,
                //     position: 'right'
                //  },
                title: { display: true, text: 'Title'}
            }
        });

        DashboardChartTypeConfig.addDataParser(CHART_NAME, BarPieParserFactory);
    }
})();
