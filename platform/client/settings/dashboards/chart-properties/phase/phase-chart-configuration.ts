(function () {
    'use strict';

    var CHART_NAME = 'phase';

    angular
        .module('core.settings')
        .run(PhaseChartConfiguration);

    PhaseChartConfiguration.$inject = [
        'DashboardChartTypeConfig'
    ];
    function PhaseChartConfiguration(DashboardChartTypeConfig) {
        DashboardChartTypeConfig.addFieldConfiguration(CHART_NAME, {
            templateUrl: 'settings/dashboards/chart-properties/phase/config-fields.html'
        });

        DashboardChartTypeConfig.addConfig(CHART_NAME, {
            chartOptions: {
                legend: {
                    display: true,
                    position: 'right'
                 },
                title: { display: true, text: 'Title'},
                scales: {
	                xAxes: [{
		                ticks: {
			                autoSkip: false
		                }
	                }],
                }
            }
        });

        DashboardChartTypeConfig.addDataParser(CHART_NAME, function(response) {
            return response;
        });
    }
})();
