(function () {
	'use strict';

	var CHART_NAME = 'pivot';

    angular
        .module('core.settings')
        .run(pivotTableConfiguration);
	
    pivotTableConfiguration.$inject = ['DashboardChartTypeConfig'];
	
    function pivotTableConfiguration(DashboardChartTypeConfig) {

		// This is templateUrl for configuration page html
		DashboardChartTypeConfig.addFieldConfiguration(CHART_NAME, {
			templateUrl: 'settings/dashboards/chart-properties/pivot/pivot-table-config-fields.html'
		});
		
		// This will be used as 'chart-options' for current chart (google Chart.js for documentation)
		DashboardChartTypeConfig.addConfig(CHART_NAME, {
			chartOptions: {
			title: { display: true, text: 'Title'}			
			}
		});

	

        DashboardChartTypeConfig.addDataParser(CHART_NAME, function (response) {
            return response;
        });
	}
})();
