(function () {
    'use strict';

    var CHART_NAME = 'stacked_lines';

    angular
        .module('core.settings')
        .run(StackedLinesChartConfiguration);

    StackedLinesChartConfiguration.$inject = [
        'DashboardChartTypeConfig',
        'CalendarService'
    ];
    
    function StackedLinesChartConfiguration(DashboardChartTypeConfig, CalendarService) {
        DashboardChartTypeConfig.addFieldConfiguration(CHART_NAME, {
            templateUrl: 'settings/dashboards/chart-setup/dashboard-chart-basic-config-fields.html'
        });
        
        var groupValueByActualId = {};
        DashboardChartTypeConfig.addConfig(CHART_NAME, {
            chartOptions: {
                legend: { display: true },
                title: { display: true },
                scales: {
	                xAxes: [{
		                ticks: {
			                autoSkip: false
		                }
	                }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        function createGroupedSums(dataSet, groupByField, valueField) {
            var groupedSums = {};
            _.each(dataSet, function(item){
                var currentKey = groupValueByActualId[item.item_id];
                if(currentKey){
                    groupedSums[currentKey] = groupedSums[currentKey] || 0;
                    groupedSums[currentKey] += +item[valueField];
                }
            });
            return groupedSums;
        }

        DashboardChartTypeConfig.addDataParser(CHART_NAME, function(response) {
            var values = {};
            var foundKeys = {};
            var namesForColumnNames = {};
            
            _.each(response.values, function(valueObject, date){
                valueObject.$$data = JSON.parse(valueObject.data);
                _.each(valueObject.$$data, function(item){
                    var itemIdField = response.chartFields.Grouping ? item.selected_item_id : item.item_id;
                    var sumField = response.chartFields.Grouping ? item.sum : item[response.chartFields.ValueField];
                    foundKeys[itemIdField] = [];
                    values[date] = values[date] || {};
                    values[date][itemIdField] = sumField;
                    namesForColumnNames[itemIdField] = itemIdField;
                    if(valueObject.relatedItems && valueObject.relatedItems[itemIdField]) {
                        if(response.chartFields.GroupByFieldProperty) {
                            namesForColumnNames[itemIdField] = valueObject.relatedItems[itemIdField][response.chartFields.GroupByFieldProperty];
                        } else {
                            namesForColumnNames[itemIdField] = valueObject.relatedItems[itemIdField];
                        }
                    }
                });
            });
            
            var valueArrays = [], labels = [], series = [];
            _.each(values, function(val, key){
                labels.push(CalendarService.formatDate(new Date(key)));
                _.each(_.keys(foundKeys), function(value){
                    foundKeys[value].push(val[value]);
                });
            });
            _.each(foundKeys, function(item, key){
                series.push(key);
                valueArrays.push(item);
            });
            var returnObj =  {
                values: valueArrays,
                labels: labels,
                series: _.map(series, function(item){ return namesForColumnNames[item] || item;})
            };
            return returnObj;
        });
    }
})();
