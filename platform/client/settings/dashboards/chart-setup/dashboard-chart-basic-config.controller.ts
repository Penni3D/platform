(function () {
    'use strict';

    angular
        .module('core.settings')
        .controller('DashboardChartBasicConfigController', DashboardChartBasicConfigController);

    DashboardChartBasicConfigController.$inject = [
        '$scope'
    ];
    function DashboardChartBasicConfigController(
        $scope) {

        $scope.fieldConfig = {
            labelField: {},
            labelFieldProperty: {},
            valueField: {},
            valueFieldProperty: {},
            showNulls: {},
            showLabels: {},
            groupResults: {},
            groupWithFunction: {},
            groupField: {},
            groupFieldProperty: {},
            dateRangeField: {},
            historyColumnCount: {}
        };

        $scope.groupingFunctionOptions = [
            { name: 'sum', LanguageTranslation: 'SUM'},
            { name: 'count', LanguageTranslation: 'COUNT'}
        ];

        $scope.YesNoOption = [
            { value: '1', LanguageTranslation: 'CHART_COMMON_YES' },
            { value: '0', LanguageTranslation: 'CHART_COMMON_NO' }
        ];

        $scope.chartType = [JSON.parse($scope.chart.JsonData).chartType][0];

        // Select element wants an array so array we give
        function parseJsonData(){
            var selections = JSON.parse($scope.chart.JsonData);
            selections.chartType = [selections.chartType];

            $scope.selections = selections;
            setFieldForGivenSelection('group');
            setFieldForGivenSelection('value');
            setFieldForGivenSelection('label');
	        setFieldForGivenSelection('value2');
	        setFieldForGivenSelection('name');
        }

        // Select element returns array, but we need to pluck that single item out before we save it
        function setJsonData() {
            var cloned = _.clone($scope.selections);
            cloned.chartType = _.first(cloned.chartType);
            $scope.chart.JsonData = JSON.stringify(cloned);
        }


        function initController(){
        }
        
        initController();
    }
})();
