(function () {
	'use strict';
	angular
		.module('core.settings')
		.directive('dashboardChart', ProcessDashboardChartDirective);

	ProcessDashboardChartDirective.$inject = [
		'SettingsService',
		'DashboardChartTypeConfig',
		'PortfolioService',
		'$rootScope',
		'ChartColourService',
		'$q',
		'Colors',
		'ViewService',
		'Common',
		'$sce',
		'ChartsService'
	];

	function ProcessDashboardChartDirective(
		SettingsService,
		DashboardChartTypeConfig,
		PortfolioService,
		$rootScope,
		ChartColourService,
		$q,
		Colors,
		ViewService,
		Common,
		$sce,
		ChartsService
	) {
		return {
			restrict: 'E',
			scope: {
				data: '=',
				process: '=',
				inMeta: '=',
				options: '=?'
			},
			template: '<div style="max-width:{{maxWidth}}px;">' +
				'<div ng-if="loading"><loader class="small"></loader><h2>{{::\'LOADING_CONTENT\' | translate}}</h2></div>' +
				'<div ng-if="!loading">' +
				'<chart-header>{{chartOptions.title.text}}</chart-header>' +
				'<div ng-include src="templateUrl"></div>' +
				'</div>' +
				'</div>',
			link: function (scope, element, attrs) {
				let dataParser;
				scope.loading = true;
				let localeId = $rootScope.clientInformation.locale_id;
				let title = _.get(scope.data, 'LanguageTranslation.' + localeId);
				scope.legend = "";
				
				scope.generateLegend = function () {
					setTimeout(function () {
						let chart = Common.getChartHandle("chart-" + scope.data.Id + "-" + scope.chartData.chartType);
						if (chart) scope.legend = $sce.trustAsHtml(chart.generateLegend());
					}, 0);
				}

				function loadData() {
					if (_.isEmpty(scope.chartType)) {
						return;
					}
					let settings;

					if (scope.data.DashboardPortfolioId > 0) {
						settings = PortfolioService.getActiveSettings(scope.data.DashboardPortfolioId);
					} else {
						settings = PortfolioService.getActiveSettings(scope.data.ProcessPortfolioId);
					}

					let dashboardFilters = {};

					_.chain(settings.filters.select)
						.map(function (filter, key) {
							if (!_.isEmpty(filter)) {
								dashboardFilters[key] = filter;
								return key + ';' + _.join(filter, '-');
							}
						})
						.filter(function (val) {
							return !_.isEmpty(val);
						})
						.join(',')
						.value();

					for (let i = 0; i < Object.keys(settings.filters.dateRange).length; i++) {
						let start = null;
						let end = null;

						if (settings.filters.dateRange[Object.keys(settings.filters.dateRange)[i]].start) start = settings.filters.dateRange[Object.keys(settings.filters.dateRange)[i]].start;
						if (settings.filters.dateRange[Object.keys(settings.filters.dateRange)[i]].end) end = settings.filters.dateRange[Object.keys(settings.filters.dateRange)[i]].end;

						dashboardFilters[Object.keys(settings.filters.dateRange)[i]] = [moment(start).toISOString('YYYY-MM-DD'), moment(end).toISOString('YYYY-MM-DD')];
					}

					for (let i = 0; i < Object.keys(settings.filters.range).length; i++) {
						dashboardFilters[Object.keys(settings.filters.range)[i]] = [settings.filters.range[Object.keys(settings.filters.range)[i]].start, settings.filters.range[Object.keys(settings.filters.range)[i]].end];
					}

					if (settings.filters.phase.length > 0) {
						dashboardFilters["phase"] = settings.filters.phase;
					}

					let xAxisLabelTranslations = {};
					let yAxisLabelTranslations = {};

					if (settings.filters && settings.filters.archive) {
						dashboardFilters["archive"] = settings.filters.archive;
					}

					//archive date will be overridden here if given in input-chart element
					if (scope.data.archive != "" && scope.data.archive != null && Date.parse(scope.data.archive) != null) {
						dashboardFilters["archive"] = scope.data.archive;
					}

					if (Array.isArray(scope.data.filterItemIds)) {
						let tempStr = scope.data.filterItemIds.toString();
						if (tempStr.length > 0) {
							dashboardFilters["filterItemIds"] = tempStr;
						} else {
							dashboardFilters["filterItemIds"] = "0";
						}
					} else {
						dashboardFilters["filterItemIds"] = scope.data.filterItemIds || "0";
					}

					if ($rootScope.frontpage == undefined) {
						dashboardFilters["inFrontPage"] = false;
					}
					else {
						dashboardFilters["inFrontPage"] = true;
					}
					if (scope.inMeta == undefined) {
						dashboardFilters["inMeta"] = false;
					}
					else {
						dashboardFilters["inMeta"] = true;
					}

					if (["resourceLoad","bubble","pie"].includes(scope.chartType) && settings.chartFilters && Object.keys(settings.chartFilters).length > 0) {
						let type = "portfolio";
						if (scope.inMeta != undefined) {
							type = "meta";
						}
						else if ($rootScope.frontpage != undefined) {
							type = "frontPage";
                        }

						let startpart = type + "_" + scope.data.Id + "_" + scope.data.chartProcess;
						let arr = [];
						for (let i = 0; i < Object.keys(settings.chartFilters).length; i++) {
							if (Object.keys(settings.chartFilters)[i].includes(startpart)) {
								arr.push({ key: Object.keys(settings.chartFilters)[i], data: settings.chartFilters[Object.keys(settings.chartFilters)[i]] });
                            }
						}
						if (arr.length > 0) {
							dashboardFilters["quickFilterPossibilities"] = arr;
                        }
                    }
					
					if (scope.options != null && scope.options.offsets != null && Object.keys(scope.options.offsets).length > 0) {
						dashboardFilters["offsets"] = scope.options.offsets;
					}
					if (scope.options != null && scope.options.offset_interval != null && Object.keys(scope.options.offset_interval).length > 0) {
						dashboardFilters["offset_interval"] = scope.options.offset_interval;
					}

					dashboardFilters["dashboardId"] = scope.data.DashboardId;

					SettingsService.ProcessDashboardsData(
						scope.process,
						scope.data.Id,
						settings.filters.text,
						'',
						dashboardFilters)
						.then(function (resp) {
							if (!_.isEmpty(resp.error)) {
								return $q.reject(resp.error);
							}
							xAxisLabelTranslations = _.get(resp, 'chartFields.OriginalData.showXAxisLabel') && _.get(resp, 'chartFields.OriginalData.xAxisLabel');
							yAxisLabelTranslations = _.get(resp, 'chartFields.OriginalData.showYAxisLabel') && _.get(resp, 'chartFields.OriginalData.yAxisLabel');

							if (_.has(xAxisLabelTranslations, localeId)) {
								_.set(scope.chartOptions, 'scales.xAxes[0].scaleLabel', {
									display: true,
									fontStyle: 'bold',
									labelString: _.get(xAxisLabelTranslations, localeId)
								});
							}
							if (_.has(yAxisLabelTranslations, localeId)) {
								_.set(scope.chartOptions, 'scales.yAxes[0].scaleLabel', {
									display: true,
									fontStyle: 'bold',
									labelString: _.get(yAxisLabelTranslations, localeId)
								});
							}
							return resp;
						})
						.then(dataParser)
						.then(function (response) {						
							if (["bubble", "pie"].includes(scope.chartData.chartType)) {
								if (scope.chartData.chartType == "pie") {
									scope.$on('chart-create', function (event, chart) {
										let chartId = chart.canvas.id;
										if ("chart-" + scope.data.Id + "-" + scope.chartData.chartType == chartId) {
											scope.chartObj = chart;
                                        }
									});
									scope.filterAreaClick = function () {
										if (scope.filtersVisible) {
											scope.filtersVisible = false;
											scope.chartObj.update();
										}
										else {
											scope.filtersVisible = true;
                                        }
									}
                                }


								scope.filtersVisible = false;
								scope.quickFilterInUse = false;

								scope.langKey = $rootScope.clientInformation["locale_id"];
								scope.filterData = {};

								scope.chartData.filterData = response.filterData;
								scope.chartConfig = response.chartConfig;

								scope.filterPortfolio = scope.chartConfig.ProcessPortfolioId;
								if (scope.chartConfig.OriginalData.main_process_portfolio_id && scope.chartConfig.OriginalData.main_process_portfolio_id > 0) scope.filterPortfolio = scope.chartConfig.OriginalData.main_process_portfolio_id;

								scope.filterKeyParams = {};
								scope.filterKeyParams.chartId = scope.chartConfig.OriginalData.chartId;
								scope.filterKeyParams.linkId = scope.chartConfig.OriginalData.linkId || 0;
								scope.filterKeyParams.chartProcess = scope.chartConfig.OriginalData.chartProcess;

								if (scope.chartData.inMeta) {
									scope.filterKeyParams.type = "meta"
								}
								else if ($rootScope.frontpage) {
									scope.filterKeyParams.type = "frontPage"
								}
								else {
									scope.filterKeyParams.type = "portfolio"
								}

								if (Object.keys(scope.chartData.filterData).length > 0) {
									scope.quickFilterInUse = true;
								}
								else {
									scope.quickFilterInUse = false;
								}

								let cf = ChartsService.getChartFilters(scope.filterPortfolio, scope.filterKeyParams.type, scope.filterKeyParams.chartId, scope.filterKeyParams.chartProcess, scope.filterKeyParams.linkId);
								if (cf && scope.chartData.filterData && Object.keys(scope.chartData.filterData).length > 0) {
									scope.filterData = cf;
								}

								let quickFiltersInEffect = false;
								angular.forEach(Object.keys(scope.chartData.filterData), function (key) {
									if (scope.filterData[key] != null && scope.filterData[key].length > 0) quickFiltersInEffect = true;
								});

								if (quickFiltersInEffect) {
									scope.filterActive = true;
								}
								else {
									scope.filterActive = false;
								}
							}


							scope.listColors = response.colors;
							scope.chartData.data = response;
							let defaultColorIndex = 0;
							let listColors = scope.chartData.listColors || 0;
							if (scope.chartData.chartType == 'pie' && listColors == 1 && scope.chartData.groupResults) {
								let defaultColors = angular.copy(scope.chartOptions.colors);
								scope.chartOptions.colors = [];
								let colorArr = Colors.getColors();

								for (let i = 0; i < response.colors.length; i++) {
									let colorSelection = "";
									let colorName = response.colors[i];
									if (colorName != null) {

										if (colorName.indexOf(' ') > -1) {
											let splitted = colorName.split(" ");
											colorSelection = colorArr[splitted[0] + splitted[1].charAt(0).toUpperCase() + splitted[1].slice(1)];
										} else {
											colorSelection = colorArr[colorName];
										}

										let color = {
											backgroundColor: colorSelection.hex,
											pointBackgroundColor: colorSelection.hex,
											hoverBackgroundColor: colorSelection.hex,
											fill: 'origin'
										};
										scope.chartOptions.colors.push(color);
									} else {
										if (defaultColors.length > defaultColorIndex) {
											scope.chartOptions.colors.push(defaultColors[defaultColorIndex]);
											defaultColorIndex += 1;
										}
									}
								}
							}

							if (scope.chartData.chartType == 'pie') {
								let totalItems = response.labels.length;
								if (response.groupInUse) {
									totalItems = totalItems - 1;
								}

								if (response.undefinedInUse) {
									totalItems = totalItems - 1;
								}

								scope.chartOptions.colors.length = totalItems;

								if (response.groupInUse) {
									let thresholdColor = scope.chartData.thresholdColor;
									if (thresholdColor != null && thresholdColor.length > 0) {
										let color = {
											backgroundColor: thresholdColor,
											pointBackgroundColor: thresholdColor,
											hoverBackgroundColor: thresholdColor,
											fill: 'origin'
										};
										scope.chartOptions.colors.push(color);
									} else {
										scope.chartOptions.colors.push(null);
									}
								}
								if (response.undefinedInUse) {
									let color = {
										backgroundColor: "lightgrey",
										pointBackgroundColor: "lightgrey",
										hoverBackgroundColor: "lightgrey",
										fill: 'origin'
									};
									scope.chartOptions.colors.push(color);
								}
							}


							if (scope.chartData.chartType == 'bubble') {
								//scope.mouseCoordinates = {};
								//let canvas = document.getElementById("chart-" + scope.data.Id + "-" + scope.chartData.chartType);//.getContext("2d");
								//canvas.addEventListener('mousemove', function (evt) {
								//	scope.mouseCoordinates = getMousePos(canvas, evt);
								//}, false);

								scope.clickData = { x: null, y: null, items: [] };

								scope.chartOptions.onResize = function (chart, size) {
									//Savagely removed because of the styling efforts
									updateChartBasedOnTickSize(chart, scope, response);
								};

								//scope.$on('chart-create', function (event, chart) {
								//	//Savagely removed because of the styling efforts
								//	updateChartBasedOnTickSize(chart, scope, response);
								//});

								scope.firstRun = true;
								scope.chartOptions.animation = {
									"onProgress": function () {
										if (scope.firstRun) {
											updateChartBasedOnTickSize(this.chart, scope, response);
											scope.firstRun = false;
										}
									}
								}
							}

							if (scope.chartData.chartType == 'bubble') {
								let bubbleColors = scope.bubbleColors;
								let copyFromColors = angular.copy(bubbleColors);
								let itemCount = response.values.length - 1;
								if (bubbleColors.length < itemCount) {
									while (bubbleColors.length < itemCount) {
										for (let i = 0; i < copyFromColors.length; i++) {
											bubbleColors.push(copyFromColors[i]);
										}
									}
								}
								scope.chartOptions.colors = bubbleColors;
							}

							if (scope.chartData.chartType == 'bubble' && response.colors != null && response.colors.length > 0) {
								scope.chartOptions.colors = [];
								let colorArr = Colors.getColors();
								let colorArrHandled = [];
								angular.forEach(Object.keys(colorArr), function (color) {
									colorArrHandled.push(colorArr[color]);
								});

								angular.forEach(response.values, function (obj) {
									let color = obj.color;
									let colorItem = colorArrHandled.filter(function (x) {
										return x.name == color;
									});

									if (colorItem.length > 0) {
										let overrideColor = {
											backgroundColor: colorItem[0].hex,
											pointBackgroundColor: colorItem[0].hex,
											hoverBackgroundColor: colorItem[0].hex,
											fill: 'origin'
										};
										scope.chartOptions.colors.push(overrideColor);
									} else {
										scope.chartOptions.colors.push(null);
									}
								});
							}

							let useOutLabels = scope.chartData.outLabelsInUse || 0;
							if (scope.chartData.chartType == 'pie' && useOutLabels == 1) {
								let showOutIndex = scope.chartData.showOutIndex || 0;
								let showOutLabel = scope.chartData.showOutLabel || 0;
								let showOutValue = scope.chartData.showOutValue || 0;
								let showOutPercentage = scope.chartData.showOutPercentage || 0;

								let handledLabels = [];
								angular.forEach(response.labels, function (label) {
									if (label.length > 25) {
										let str = label.substring(0, 21) + "...";
										handledLabels.push(str);
									} else {
										handledLabels.push(label);
									}
								});
								let lastItemIndex = 0;
								if (showOutLabel == 1) {
									lastItemIndex = 1;
								}
								if (showOutValue == 1) {
									if (lastItemIndex == 1) {
										for (let i = 0; i < handledLabels.length; i++) {
											let decimals = numberOfDecimals(response.values[i]);
											handledLabels[i] = handledLabels[i] + " " + Common.formatNumber(response.values[i], decimals) //" %v";
										}
									} else {
										for (let i = 0; i < handledLabels.length; i++) {
											let decimals = numberOfDecimals(response.values[i]);
											handledLabels[i] = Common.formatNumber(response.values[i], decimals) //"%v";
										}
									}
									lastItemIndex = 2;
								}
								if (showOutPercentage == 1) {
									if (lastItemIndex == 1) {
										for (let i = 0; i < handledLabels.length; i++) {
											handledLabels[i] = handledLabels[i] + " %p";
										}
									} else if (lastItemIndex == 2) {
										for (let i = 0; i < handledLabels.length; i++) {
											handledLabels[i] = handledLabels[i] + " (%p)";
										}
									} else {
										for (let i = 0; i < handledLabels.length; i++) {
											handledLabels[i] = "%p";
										}
									}
									lastItemIndex = 3;
								}
								if (lastItemIndex == 0 && showOutIndex == 0) {
									for (let i = 0; i < handledLabels.length; i++) {
										handledLabels[i] = "";
									}
								} else if (lastItemIndex == 0 && showOutIndex == 1) {
									for (let i = 0; i < handledLabels.length; i++) {
										handledLabels[i] = (i + 1).toString();
									}
								} else if (showOutIndex == 1 && lastItemIndex > 0) {
									for (let i = 0; i < handledLabels.length; i++) {
										handledLabels[i] = (i + 1).toString() + " | " + handledLabels[i];
									}
								}

								if (showOutIndex == 1) {
									for (let i = 0; i < handledLabels.length; i++) {
										scope.chartData.data.labels[i] = (i + 1).toString() + ". " + scope.chartData.data.labels[i];
									}
								}
								scope.chartOptions.zoomOutPercentage = 40;
								scope.chartOptions.plugins = {
									outlabels: {
										text: handledLabels,
										backgroundColor: "white",
										borderRadius: 2,
										borderWidth: 2,
										color: 'black',
										stretch: 45,
										display: true,
										font: {
											resizable: true,
											minSize: 12,
											maxSize: 18
										}
									}
								};
							} else if (scope.chartData.chartType == 'pie') {
								scope.chartOptions.layout.padding = { left: 0, right: 0, top: 8, bottom: 0 };
								scope.chartOptions.zoomOutPercentage = 0.01;
								scope.chartOptions.plugins = {
									outlabels: {
										text: ""
									}
								};
							}

							if (scope.chartData.chartType == 'pie') {
								_.set(scope.chartOptions, 'tooltips.callbacks', {
									label: function (value) {
										let decimals = numberOfDecimals(response.values[value.index]) || 0;
										return scope.chartData.data.labels[value.index] + ": " + Common.formatNumber(response.values[value.index], decimals);
									}
								});
							}

							if (["bubble", "pie"].includes(scope.chartData.chartType)) {
								scope.filterChanged = function () {
									ChartsService.setChartFilters(scope.filterPortfolio, scope.filterKeyParams.type, scope.filterKeyParams.chartId, scope.filterKeyParams.chartProcess, scope.filterKeyParams.linkId, scope.filterData);

									setChartData();
									setConfig();
									loadData();
								}
							}

							let clickableTemp = scope.chartData.clickable || 0;
							if (scope.chartData.chartType == 'pie' && clickableTemp == 1) {
								scope.chartOptions.onClick = function (e) {
									let selectedElement = this.getElementAtEvent(e);
									if (selectedElement.length > 0) {
										let filterIds;
										let filter;
										if (scope.chartData.groupResults) {
											if (response.processItemIds[selectedElement[0]._index] == "") {
												filterIds = response.idsFix["undefined"];
											} else {
												if (Array.isArray(response.processItemIds[selectedElement[0]._index]) && response.processItemIds[selectedElement[0]._index].length > 1 || Array.isArray(response.processItemIds[selectedElement[0]._index]) == false && response.processItemIds[selectedElement[0]._index].split(",").length > 1) {
													let idArr = [];
													let processItems;
													if (scope.chartData.groupResults) {
														processItems = response.processItemIds[selectedElement[0]._index];
													}

													angular.forEach(processItems, function (id) {
														angular.forEach(response.idsFix[id].split(","), function (item_id) {
															idArr.push(item_id);
														});
													});
													filterIds = idArr.join();
												} else {
													filterIds = response.idsFix[response.processItemIds[selectedElement[0]._index].toString()];
												}
											}

											filter = "";
											if (filterIds.indexOf(",") > -1) {
												filter = filterIds.split(",").map(Number);
											} else if (filterIds.indexOf(",") == -1 && filterIds.length > 0) {
												filter = [Number(filterIds)];
											} else if (filterIds.length == 0) {
												filter = [];
											}
										} else {
											filter = response.processItemIds[selectedElement[0]._index];
										}

										let drillDownPortfolio = scope.data.ProcessPortfolioId;
										let drillDownProcess = scope.data.chartProcess;

										if (response.mainProcessLinkMode) {
											drillDownPortfolio = scope.data.DashboardPortfolioId;
											drillDownProcess = scope.process;
										}

										if (scope.chartData.groupResults && filter.length > 1 || ((scope.chartData.groupResults == null || scope.chartData.groupResults == false) && filter.length > 1)) {
											ViewService.view('portfolio', {
												params: {
													portfolioId: drillDownPortfolio,
													restrictions: { 0: filter },
													process: drillDownProcess,
													chartForceFlatMode: true,
													titleExtension: scope.chartOptions.title.text + ": " + response.labels[selectedElement[0]._index]
												}
											},
												{
													split: true, size: 'normal'
												}
											);
										} else {
											ViewService.view('process.tab',
												{
													params: {
														itemId: filter,
														process: drillDownProcess
													}
												});
										}
									}
								};
							}

							if (scope.chartData.chartType == 'bubble' && response.legendNames && response.legendNames.length > 0 && scope.chartData.useLegend == 1) {
								scope.chartData.data.labels = [];
								for (let i = 0; i < response.legendNames.length; i++) {
									scope.chartData.data.labels.push(response.legendNames[i] + "|" + response.legendColors[i]);
								}
								scope.generateLegend();
							}

							if (scope.chartData.chartType == 'bubble' && clickableTemp == 1) {
								scope.chartOptions.onClick = function (e, elements) {

									if (elements.length > 0) {
										let clickedElement = scope.chartData.data.values[elements[0]._index];
										let xTicks = elements[0]._chart.scales["x-axis-0"].ticks;
										let yTicks = elements[0]._chart.scales["y-axis-0"].ticks;

										let xTickSize = 1;
										let yTickSize = 1;

										if (xTicks.length > 1 && typeof xTicks[0] !== 'string') xTickSize = xTicks[0] - xTicks[1];
										if (yTicks.length > 1 && typeof yTicks[0] !== 'string') yTickSize = yTicks[0] - yTicks[1];

										let yTop = this.chart.chartArea.top;
										let yBottom = this.chart.chartArea.bottom;

										let yMin = this.chart.scales['y-axis-0'].min;
										let yMax = this.chart.scales['y-axis-0'].max;
										let newY = 0;

										//if (e.offsetY <= yBottom && e.offsetY >= yTop) {
										newY = Math.abs((e.offsetY - yTop) / (yBottom - yTop));
										newY = (newY - 1) * -1;
										newY = newY * (Math.abs(yMax - yMin)) + yMin;
										//}

										let xTop = this.chart.chartArea.left;
										let xBottom = this.chart.chartArea.right;
										let xMin = this.chart.scales['x-axis-0'].min;
										let xMax = this.chart.scales['x-axis-0'].max;
										let newX = 0;

										//if (e.offsetX <= xBottom && e.offsetX >= xTop) {
										newX = Math.abs((e.offsetX - xTop) / (xBottom - xTop));
										newX = newX * (Math.abs(xMax - xMin)) + xMin;
										//}

										let xScale = this.chart.scales['x-axis-0'];
										let yScale = this.chart.scales['y-axis-0'];
										let xTickLength = (xScale.getPixelForTick(1) - xScale.getPixelForTick(0)) / xTickSize;
										let yTickLength = (yScale.getPixelForTick(1) - yScale.getPixelForTick(0)) / yTickSize;

										let idArr = [];

										let idField = "item_id";
										let drillDownPortfolio = scope.data.ProcessPortfolioId;
										let drillDownProcess = scope.data.chartProcess;

										if (response.linkField) {
											idField = "linkItemId";
											drillDownPortfolio = scope.data.DashboardPortfolioId;
											drillDownProcess = scope.process;
										}

										for (let i = 0; i < scope.chartData.data.values.length; i++) {
											if (pointInCircle(newX * xTickLength, newY * yTickLength, scope.chartData.data.values[i].x * xTickLength, scope.chartData.data.values[i].y * yTickLength, scope.chartData.data.values[i].r + 1)) {
												let tempArr = scope.chartData.data.values[i][idField].toString().split();
												for (let i = 0; i < tempArr.length; i++) {
													if (idArr.includes(tempArr[i]) == false) idArr.push(tempArr[i]);
												}
											}
										}

										if (idArr.length > 1) {
											ViewService.view('portfolio', {
												params: {
													portfolioId: drillDownPortfolio,
													restrictions: { 0: idArr },
													process: drillDownProcess,
													chartForceFlatMode: true,
													titleExtension: scope.chartOptions.title.text //+ ": " + response.labels[selectedElement[0]._index]
												}
											},
												{
													split: true, size: 'normal'
												}
											);
										} else if (idArr.length == 1) {
											let type = "process.tab";
											let split = false;
											let size = "";
											let clickMode = scope.chartData.clickMode || 0;

											let clickParams = {
												itemId: idArr[0],
												forceReadOnly: false,
												process: drillDownProcess,
												disableNextPrev: true
											}

											switch (parseInt(clickMode)) {
												case 0:
													// Default is fine												
													break;
												case 1:
													split = true;
													size = "narrower";
													type = "featurette";
													clickParams.title = clickedElement.name;
													break;
												case 2:
													split = true;
													size = "normal";
													type = "featurette";
													clickParams.title = clickedElement.name;
													break;
												case 3:
													split = true;
													size = "wider";
													type = "featurette";
													clickParams.title = clickedElement.name;
													break;
											}

											ViewService.view(type,
												{
													params: clickParams
												},
												{
													split: split, size: size
												}
											);
										}
									}
								}
							}

							let fieldProperties = scope.chartData;

							if (response.customCallback) {
								_.set(scope.chartOptions, 'scales.xAxes[0].ticks.callback', response.customCallback);
							}
							if (response.xAxisCallback) {
								_.set(scope.chartOptions, 'scales.xAxes[0].ticks.callback', response.xAxisCallback);
							}
							if (response.yAxisCallback) {
								_.set(scope.chartOptions, 'scales.yAxes[0].ticks.callback', response.yAxisCallback);
							}
							if (response.yStepSize) {
								_.set(scope.chartOptions, 'scales.yAxes[0].ticks.stepSize', response.yStepSize);
							}
							if (response.xStepSize) {
								_.set(scope.chartOptions, 'scales.xAxes[0].ticks.stepSize', response.xStepSize);
							}
							if (response.xLabel) {
								_.set(scope.chartOptions, 'scales.xAxes[0].scaleLabel', {
									display: true,
									labelString: response.xLabel
								});
							}
							if (response.yLabel) {
								_.set(scope.chartOptions, 'scales.yAxes[0].scaleLabel', {
									display: true,
									labelString: response.yLabel
								});
							}
							if (response.tooltipLabelCallback) {
								let tooltipLabelCallback = function (what, where, why) {
									let index = what.index;
									let data = where.datasets[what.datasetIndex].data[index];
									let name = data.name;
									let xLabel = data.xLabel || 0;
									let yLabel = data.yLabel || 0;
									let originalR = data.originalR || 0;

									return name + ' (' + xLabel + ', ' + yLabel + ', ' + originalR + ')';
								};
								_.set(scope.chartOptions, 'tooltips.callbacks', {
									label: tooltipLabelCallback
								});
							}
							if (response.padding) {
								_.set(scope.chartOptions, 'layout.padding', response.padding);
							}
							if (title) {
								_.set(scope.chartOptions, 'title.text', title);
							}
							let useCustomScale = fieldProperties.use_custom_scale || 0;
							if (scope.chartType == 'bubble' && useCustomScale == 1) {
								let xStepSize = fieldProperties.x_step_size || 0;
								let yStepSize = fieldProperties.y_step_size || 0;
								let xMin = fieldProperties.x_axis_min || 0;
								let xMax = fieldProperties.x_axis_max || 0;
								let yMin = fieldProperties.y_axis_min || 0;
								let yMax = fieldProperties.y_axis_max || 0;

								if (response.xPropertyType == 1 || response.xPropertyType == 2) {
									_.set(scope.chartOptions, 'scales.xAxes[0].ticks.stepSize', xStepSize);
									_.set(scope.chartOptions, 'scales.xAxes[0].ticks.min', xMin);
									_.set(scope.chartOptions, 'scales.xAxes[0].ticks.max', xMax);
								}

								if (response.yPropertyType == 1 || response.yPropertyType == 2) {
									_.set(scope.chartOptions, 'scales.yAxes[0].ticks.stepSize', yStepSize);
									_.set(scope.chartOptions, 'scales.yAxes[0].ticks.min', yMin);
									_.set(scope.chartOptions, 'scales.yAxes[0].ticks.max', yMax);
								}
							}
						})

						.catch(function (whaat) {
							let translationKeyPrefix = 'DASHBOARD_CHART_ERROR_';
							let errorInfo = _.get(whaat, 'information.description');
							scope.errorMessageTranslation = translationKeyPrefix + (errorInfo ? errorInfo : 'DEFAULT');
							scope.templateUrl = 'settings/dashboards/dashboard-chart-configuration-error-page.html';
						})
						.finally(function () {
							if (scope.chartData.showLabels) scope.generateLegend();
							scope.loading = false;
						});
				}

				function setChartData() {
					let chartData = JSON.parse(scope.data.JsonData);
					scope.data.json = chartData;

					if (scope.inMeta) {
						scope.maxWidth = '1600';
					} else if (chartData.fullWidth) {
						scope.maxWidth = '';
					} else {
						scope.maxWidth = '712';
					}
					if (scope.chartType !== chartData.chartType) {
						scope.loading = true;
					}
					scope.chartType = chartData.chartType;
					if (_.isEmpty(scope.chartType)) {
						return;
					}

					scope.chartData = _.extend({}, chartData, {chartType: chartData.chartType});
				}

				function setConfig() {
					if (_.isEmpty(scope.chartType)) {
						return;
					}

					let h = Common.getCanvasResponsiveSize(scope.height, scope, element, true);

					if (scope.chartType == "pie") {
						if (h == 120) scope.height = 400;
						if (h == 100) scope.height = 420;
						if (h == 80) scope.height = 460;
					} else if (scope.chartType == "bubble") {
						if (h > 120) h = 120;
						scope.height = 80 / h * 512 + "px";
					} else {
						scope.height = h;
					}

					let defaultDataParser = function (data) {
						return data;
					};
					dataParser = DashboardChartTypeConfig.getDataParser(scope.chartType) || defaultDataParser;

					let config = DashboardChartTypeConfig.getConfig(scope.chartType);

					if (config && config.chartOptions && config.chartOptions.title) {
						config.chartOptions.title.text = scope.data.Name;
					}
					config.chartOptions.title.display = false;

					let selectedColors = scope.chartData.baseColor || ['blue'];
					config.chartOptions.colors = _.map(ChartColourService.getColours(selectedColors, true), function (col) {
						return {
							backgroundColor: col,
							pointBackgroundColor: col,
							hoverBackgroundColor: col,
							fill: 'origin'
						};
					});
					if (scope.chartType == "bubble") {
						if (selectedColors.length == 0) {
							scope.bubbleColors = angular.copy(config.chartOptions.colors);
						} else {
							let tempColors = [];
							for (let i = 0; i < (10 * selectedColors.length); i++) {
								tempColors.push(config.chartOptions.colors[i]);
							}
							scope.bubbleColors = tempColors;
						}
					}


					config.chartOptions.layout = {
						padding: {
							top: 50,
							bottom: 50
						}
					};
					if (!config.chartOptions.legend) {
						config.chartOptions.legend = {
							display: false, //scope.chartData.showLabels,
							position: 'top',
							fullWidth: true
						};
					} else {
						config.chartOptions.legend.position = config.chartOptions.legend.position || 'top';
						config.chartOptions.legend.display = false //scope.chartData.showLabels;
					}
					config.chartOptions.legendCallback = function (chart) {
						return Common.getChartLegend(chart, scope.chartType);
					};
					config.chartOptions.maintainAspectRatio = false;
					config.chartOptions.responsive = true;
					scope.chartOptions = config && config.chartOptions || {legend: {display: true}};

					scope.chartOptions.tooltips = scope.chartOptions.tooltips || {};
					scope.chartOptions.tooltips.mode = 'point';
					if (config.templateUrl) {
						scope.templateUrl = config.templateUrl;
					} else {
						scope.templateUrl = 'settings/dashboards/chart-properties/' + scope.chartType + '/' + scope.chartType + '.template.html';
					}

					if (scope.chartType == "pie") {
						let pieType = scope.chartData.pieType || [1];
						let customRotation = scope.chartData.rotation || 0;

						switch (pieType[0]) {
							case 0:
								scope.chartOptions.rotation = -0.5 * Math.PI + (customRotation / 180 * Math.PI);
								break;
							case 1:
								scope.chartOptions.cutoutPercentage = scope.chartData.cutOutPercentage || 50;
								scope.chartOptions.rotation = -0.5 * Math.PI + (customRotation / 180 * Math.PI);
								break;
							case 2:
								scope.chartOptions.cutoutPercentage = scope.chartData.cutOutPercentage || 50;
								scope.chartOptions.rotation = 1 * Math.PI;
								scope.chartOptions.circumference = 1 * Math.PI;
								break;
						}
					}
				}

				setChartData();
				setConfig();
				loadData();

				let listener = $rootScope.$on('portfoliofilter-changed', function (event, val) {
					if ((scope.data.DashboardPortfolioId > -1 && val === scope.data.DashboardPortfolioId) || val === scope.data.ProcessPortfolioId) {
						setChartData();
						setConfig();
						loadData();
					}
				});

				scope.$on('$destroy', function () {
					listener();
					//listener2();
				});

				function fullColorHex(r, g, b) {
					let red = rgbToHex(r);
					let green = rgbToHex(g);
					let blue = rgbToHex(b);
					return red + green + blue;
				}

				function getMousePos(canvas, evt) {
					var rect = canvas.getBoundingClientRect();
					return {
						x: evt.clientX - rect.left,
						y: evt.clientY - rect.top
					};
				}

				function pointInCircle(x, y, cx, cy, radius) {
					let distancesquared = (x - cx) * (x - cx) + (y - cy) * (y - cy);
					return distancesquared <= (radius * radius);
				}

				function updateChartBasedOnTickSize(chartInstance, scope, response) {
					if (response.xPropertyType == 5 || response.xPropertyType == 6) {
						let xScale = chartInstance.scales['x-axis-0'];
						let xLabelOffset = (xScale.getPixelForTick(1) - xScale.getPixelForTick(0)) / 2;
						chartInstance.options.scales.xAxes[0].ticks.minor.labelOffset = xLabelOffset;
					}

					if (response.yPropertyType == 5 || response.yPropertyType == 6) {
						let yScale = chartInstance.scales['y-axis-0'];
						let yLabelOffset = (yScale.getPixelForTick(0) - yScale.getPixelForTick(1)) / 2;
						chartInstance.options.scales.yAxes[0].ticks.minor.labelOffset = yLabelOffset;
					}

					//chartInstance.update();

					if (response.autoScale == 1) {
						let xScale = chartInstance.scales['x-axis-0'];
						let yScale = chartInstance.scales['y-axis-0'];
						let xAxisColumnSize = Math.abs((xScale.getPixelForTick(1) - xScale.getPixelForTick(0)));
						let yAxisColumnSize = Math.abs((yScale.getPixelForTick(0) - yScale.getPixelForTick(1)));
						let oldScalingFactor = response.scalingFactor;
						let greatestRadius = response.greatestRadius * oldScalingFactor;

						let maxSizeInColumn = xAxisColumnSize;
						if (yAxisColumnSize < xAxisColumnSize) maxSizeInColumn = yAxisColumnSize;

						//let reScale = false;
						let newScalingFactor = 1;

						if ((greatestRadius * 2) > maxSizeInColumn * 0.75) {
							//reScale = true;
							newScalingFactor = (maxSizeInColumn * 0.75) / (greatestRadius * 2);
						}

						//if (reScale) {
							for (let i = 0; i < scope.chartData.data.values.length; i++) {
								let origninalR = scope.chartData.data.values[i].originalR;
								let newValue = origninalR * oldScalingFactor * newScalingFactor;
								if (newValue < 7 && newValue != 0) newValue = 7;
								scope.chartData.data.values[i].r = newValue;
							}
						//}
					}				
				}

				function numberOfDecimals(value) {
					if (Math.floor(value.valueOf()) === value.valueOf()) return 0;
					return value.toString().split(".")[1].length || 0;
				}
			}
		};
	}
})();
