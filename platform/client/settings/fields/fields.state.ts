﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.config(FieldsSettingsConfig);

	FieldsSettingsConfig.$inject = ['ViewsProvider'];

	function FieldsSettingsConfig(ViewsProvider) {

		ViewsProvider.addView('settings.fields.relations', {
			controller: 'ItemColumnRelationsController',
			template: 'settings/fields/fieldsRelations.html',
			resolve: {
				ConditionRelations: function (StateParameters, SettingsService) {
					return SettingsService.ItemColumnConditions(StateParameters.itemColumnId)
				},
				ItemColumnActions: function (ProcessActionsService, StateParameters) {
					return ProcessActionsService.getItemColumnActions(StateParameters.itemColumnId);
				},
			}
		});

		ViewsProvider.addView('settings.fields.properties', {
			template: 'settings/containers/itemColumnProperties.html',
			controller: 'ItemColumnsEditController',
			resolve: {
				Templates: function (AttachmentService, StateParameters) {
					return AttachmentService.getTemplatesByProcess(StateParameters.process).then(function (f) {
						return _.uniqBy(f, 'Name');
					});
				},
				ProcessActions: function (SettingsService, StateParameters, $filter, Translator) {
					return SettingsService.ProcessActionsGet(StateParameters.process).then(function (actions) {
						actions = $filter('orderBy')(actions, function (action) {
							return Translator.translation(action.LanguageTranslation)
						});
						angular.forEach(actions, function (key) {
							key.Translation = Translator.translation(key.LanguageTranslation);
						});
						return actions
					});
				},
				ProcessActionsDialogs: function (SettingsService, StateParameters, $filter, Translator): Array<Client.ProcessDialog> {
					return SettingsService.getDialogSettings(StateParameters.process).then(function (dialogs: Array<Client.ProcessDialog>) {
						dialogs = $filter('orderBy')(dialogs, function (translation) {
							return Translator.translation(translation.LanguageTranslation)
						});
						angular.forEach(dialogs, function (key) {
							key.Translation = Translator.translation(key.LanguageTranslation);
						});
						return dialogs
					});
				},
				Equations: function (StateParameters, SettingsService) {
					return SettingsService.ItemColumnEquations(StateParameters.process, StateParameters.itemColumn.ItemColumnId).then(function (e) {
						for (let i = 0; i < e.length; i++) {
							e[i].changed = false;
							if (e[i].ColumnType == 0 && e[i].SqlFunction == 10) {
								e[i].SqlParam1 = JSON.parse(e[i].SqlParam1);
							}
							if (e[i].ColumnType == 3 && e[i].SqlParam1) {
								e[i].SqlParam2 = JSON.parse(e[i].SqlParam2);
							}
						}
						return e;
					})
				},
				FieldsSystemList: function (SettingsService) {
					return SettingsService.SystemList();
				},
				SubQueries: function (ApiService) {
					return ApiService.v1api('settings/RegisteredSubQueries/queryTypes').get();
				},
				ProcessItemColumns: function (SettingsService, StateParameters) {
					return SettingsService.ItemColumns(StateParameters.process);
				},
				DashboardCharts: function (SettingsService, StateParameters) {
					return SettingsService.ProcessDashboardGetChartsForProcess(StateParameters.process, true);
				},
				ItemColumns: function (SettingsService, StateParameters, $rootScope, $filter, Views, $q, ProcessService, Translator) {
					let promises = [];
					promises.push(SettingsService.ItemColumnsInContainers(StateParameters.process));
					promises.push(SettingsService.ProcessTabs(StateParameters.process));
					promises.push(ProcessService.getProcessGroups(StateParameters.process, true));
					return $q.all(promises).then(function (response) {

						let columns = response[0];
						let views = [];
						let defaultViews = [];
						views.push({'name': 'Frontpage', 'value': 'frontpage'});
						views.push({'name': 'Page: Calendar', 'value': 'calendar'});
						views.push({'name': 'Nothing', 'value': 'nothing'});
						let tabs = response[1];
						angular.forEach(_.uniqBy(tabs, 'ProcessTabId'), function (tab: Client.ProcessTab) {
							let mName = tab.MaintenanceName ? " ' " + tab.MaintenanceName + " ' " : "";
							views.push({
								'name': 'Tab: ' + Translator.translation(tab.LanguageTranslation) + mName,
								'value': 'tab_' + tab.ProcessTabId
							});
						});

						let processFeatures = [];
						angular.forEach(response[2], function (value) {
							processFeatures.push(value.SelectedFeatures);

						});

						processFeatures = _.uniqBy(_.flatten(processFeatures), 'Feature');

						angular.forEach(processFeatures, function (feature) {
							if (feature.DefaultState && feature.DefaultState != "process.user") {
								views.push({'name': 'Feature: ' + feature.Feature, 'value': feature.DefaultState});
							}
						});

						views = $filter('orderBy')(views, 'name');

						defaultViews.push({'name': Translator.translate('ADD_BUTTON_ACTION_1'), 'value': 'close'});
						defaultViews.push({'name': Translator.translate('ADD_BUTTON_ACTION_2'), 'value': 'refresh'});
						defaultViews.push({
							'name': Translator.translate('ADD_BUTTON_ACTION_3'),
							'value': 'refresh_all'
						});
						defaultViews.push({
							'name': Translator.translate('ADD_BUTTON_ACTION_4'),
							'value': 'refresh_tables'
						});
						defaultViews.push({'name': Translator.translate('ADD_BUTTON_ACTION_5'), 'value': 'new_item'});
						defaultViews.push({
							'name': Translator.translate('ADD_BUTTON_ACTION_6'),
							'value': 'new_item_replace'
						});
						defaultViews.push({
							'name': Translator.translate('ADD_BUTTON_ACTION_7'),
							'value': 'refresh_all_cache'
						});

						let subProcessColumns = {};
						let relativeColumns = [];
						angular.forEach(columns, function (column) {
							angular.forEach(column, function (col) {
								if (col.DataType == 11) {

									SettingsService.ItemColumns(col.DataAdditional).then(function (data) {
										subProcessColumns[col.DataAdditional] = data;
									});
								}

								if (col.DataType == 5 && col.InputMethod == 0) {
									relativeColumns.push(col);
								}
							});
						});

						return {
							'relativeColumns': relativeColumns,
							'itemColumns': columns,
							'subProcessColumns': subProcessColumns,
							'views': views,
							'defaultViews': defaultViews
						};
					});
				},
			},
			afterResolve: {
				SQValidation: function (StateParameters, ApiService) {
					if (StateParameters.itemColumn.DataType == 16) {
						return ApiService.v1api('settings/RegisteredSubQueries/donetest').get([StateParameters.process, StateParameters.itemColumn.DataAdditional, StateParameters.itemColumn.ItemColumnId]).then(function (result) {
							if (result) return 1;
							return 0;
						});
					} else {
						return -1;
					}
				},
			}
		});

		ViewsProvider.addView('settings.fields.tabs', {
			controller: 'UnLinkedItemColumnsController',
			template: 'settings/fields/unLinkedItemColumns.html',
			resolve: {
				FieldsProcessPortfolios: function (SettingsService) {
					return SettingsService.ProcessPortfolios();
				},
				Processes: function (ProcessesService, $filter, Translator) {
					return ProcessesService.get().then(function (processes) {
						return $filter('orderBy')(processes, function (process) {
							return Translator.translation(process.LanguageTranslation)
						});
					});
				},
				SubQueries: function (ApiService) {
					return ApiService.v1api('settings/RegisteredSubQueries/queryTypes').get();
				},
				ProcessContainers: function (SettingsService, StateParameters, Translator) {
					let tabs;
					let phases;
					if (StateParameters.ProcessTabId > 0) {
						return SettingsService.ProcessTabContainers(StateParameters.process).then(function (data) {
							tabs = data;
							return SettingsService.ProcessGroupTabs(StateParameters.process).then(function (data) {
								phases = data;
								return SettingsService.ProcessTabContainers(StateParameters.process, StateParameters.ProcessTabId).then(function (containers) {
									let resultContainer = [];
									resultContainer['left'] = [];
									resultContainer['right'] = [];
									resultContainer['buttons'] = [];
									resultContainer['top'] = [];
									resultContainer['bottom'] = [];

									angular.forEach(containers, function (container) {
										container.phases = "";
										angular.forEach(tabs, function (tab) {
											if (tab.ProcessContainer.ProcessContainerId == container.ProcessContainer.ProcessContainerId) {
												angular.forEach(phases, function (phase) {
													if (phase.ProcessTab.ProcessTabId == tab.ProcessTab.ProcessTabId) {
														if (Translator.translation(phase.ProcessGroup.LanguageTranslation) != null) {
															container.phases += Translator.translation(phase.ProcessGroup.LanguageTranslation) + " -  " + Translator.translation(tab.ProcessTab.LanguageTranslation) + ", ";
														}
													}
												});
											}
										});

										if (container.phases.substring(container.phases.length - 2) == ',') {
											container.phases = container.phases.substring(0, container.phases.length - 2);
										}

										if (container.ProcessContainerSide == 1) {
											resultContainer['left'].push(container);
										} else if (container.ProcessContainerSide == 2) {
											resultContainer['right'].push(container);
										} else if (container.ProcessContainerSide == 3) {
											resultContainer['buttons'].push(container);
										} else if (container.ProcessContainerSide == 4) {
											resultContainer['top'].push(container);
										} else if (container.ProcessContainerSide == 5) {
											resultContainer['bottom'].push(container);
										}
									});
									return resultContainer;
								});
							});
						});
					} else {
						return SettingsService.ProcessContainersNotInTabs(StateParameters.process).then(function (containers) {
							let resultContainer = {};
							resultContainer['left'] = [];
							resultContainer['left'].push({
								'ProcessContainer': {
									'ProcessContainerId': 0,
									'Translation': Translator.translate('FIELDS_NOT_IN_CONTAINER')
								}, 'ProcessContainerSide': 1, 'ProcessTabContainerId': 0
							});
							return resultContainer;
						});
					}

				},
				ItemColumns: function (SettingsService, StateParameters, $filter, Views, Translator) {
					return SettingsService.ItemColumnsInContainers(StateParameters.process).then(function (columns) {
						let views = [];
						views.push({'name': 'Frontpage', 'value': 'frontpage'});
						views.push({'name': 'Page: Calendar', 'value': 'calendar'});

						return SettingsService.ProcessTabs(StateParameters.process).then(function (tabs: Array<Client.ProcessTab>) {
							angular.forEach(_.uniqBy(tabs, 'ProcessTabId'), function (tab) {
								let mName = tab.MaintenanceName ? " ' " + tab.MaintenanceName + " ' " : "";
								views.push({
									'name': 'Tab: ' + Translator.translation(tab.LanguageTranslation) + mName,
									'value': 'tab_' + tab.ProcessTabId
								});
							});

							angular.forEach(Views.getFeatures(), function (feature) {
								if (feature.view && feature.view != "process.user") {
									views.push({'name': 'Feature: ' + feature.translation, 'value': feature.view});
								}
							});

							views = $filter('orderBy')(views, 'name');

							views.push({'name': 'Close', 'value': 'close'});
							views.push({'name': 'Refresh', 'value': 'refresh'});
							views.push({'name': 'Refresh all', 'value': 'refresh_all'});
							views.push({'name': 'Nothing', 'value': 'nothing'});
							views.push({'name': 'Goto new item', 'value': 'new_item'});
							views.push({'name': 'Goto new item by replacing', 'value': 'new_item_replace'});
							views.push({'name': 'Refresh all and invalidate cache', 'value': 'refresh_all_cache'});


							let subProcessColumns = {};
							let relativeColumns = [];
							angular.forEach(columns, function (column) {
								angular.forEach(column, function (col) {
									if (col.DataType == 11) {

										SettingsService.ItemColumns(col.DataAdditional).then(function (data) {
											subProcessColumns[col.DataAdditional] = data;
										});
									}

									if (col.DataType == 5 && col.InputMethod == 0) {
										relativeColumns.push(col);
									}
								});
							});

							return {
								'relativeColumns': relativeColumns,
								'itemColumns': columns,
								'subProcessColumns': subProcessColumns,
								'views': views
							};
						});
					});
				},
				ProcessContainerColumns: function (SettingsService, StateParameters) {
					return SettingsService.ProcessContainerColumns(StateParameters.process);
				},

				ProcessActions: function (SettingsService, StateParameters, $filter, Translator) {
					return SettingsService.ProcessActionsGet(StateParameters.process).then(function (actions) {
						actions = $filter('orderBy')(actions, function (action) {
							return Translator.translation(action.LanguageTranslation)
						});
						angular.forEach(actions, function (key) {
							key.Translation = Translator.translation(key.LanguageTranslation);
						});
						return actions;
					});
				},
				ProcessActionDialogs: function (SettingsService, StateParameters, $filter, Translator) {
					return SettingsService.getDialogSettings(StateParameters.process).then(function (actions) {
						actions = $filter('orderBy')(actions, function (action) {
							return Translator.translation(action.LanguageTranslation)
						});
						angular.forEach(actions, function (key) {
							key.Translation = Translator.translation(key.LanguageTranslation);
						});
						return actions;
					});
				},
				ItemColumnsData: function (SettingsService, StateParameters, Common, $filter, Translator) {
					return SettingsService.ItemColumns(StateParameters.process).then(function (columns) {
						angular.forEach(columns, function (key) {
							key.Translation = Translator.translation(key.LanguageTranslation) + ' [' + Common.getDataTypeTranslation(key.DataType) + ']';
						});
						return $filter('orderBy')(columns, 'Translation');
					});
				},
				Lists: function (SettingsService, $filter, Translator) {
					return SettingsService.ListProcesses().then(function (process) {
						return $filter('orderBy')(process, function (p) {
							return Translator.translation(p.LanguageTranslation)
						});
					});
				},
			}
		});

		ViewsProvider.addView('settings.fields.joinColumns', {
			controller: 'ItemColumnJoinsSettingsController',
			template: 'settings/fields/itemColumnJoins.html',
			resolve: {
				ItemColumnJoins: function (SettingsService, StateParameters) {
					return SettingsService.ItemColumnJoins(StateParameters.process, StateParameters.ItemColumnId);
				},
				ParentColumns: function (SettingsService, StateParameters) {
					return SettingsService.ItemColumns(StateParameters.process);
				},
				SubColumns: function (SettingsService, StateParameters) {
					if (!angular.isUndefined(StateParameters.SubProcess)) {
						return SettingsService.ItemColumns(StateParameters.SubProcess);
					}
				}
			}
		});
	}
})();