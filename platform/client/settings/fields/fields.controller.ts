﻿(function () {

	'use strict';

	angular
		.module('core.settings')
		.controller('UnLinkedItemColumnsController', UnLinkedItemColumnsController)
		.controller('ItemColumnJoinsSettingsController', ItemColumnJoinsSettingsController)
		.controller('ItemColumnRelationsController', ItemColumnRelationsController);

	UnLinkedItemColumnsController.$inject = ['$rootScope', '$filter', '$scope', 'Processes', 'ProcessContainers', 'ProcessContainerColumns', 'ItemColumns', 'SettingsService', 'MessageService', 'FieldsProcessPortfolios', 'Common', 'SaveService', 'StateParameters', 'ViewService', 'ItemColumnsData', 'Lists', 'SubQueries'];

	function UnLinkedItemColumnsController($rootScope, $filter, $scope, Processes, ProcessContainers, ProcessContainerColumns, ItemColumns, SettingsService, MessageService, FieldsProcessPortfolios, Common, SaveService, StateParameters, ViewService, ItemColumnsData, Lists, SubQueries) {

		$scope.containers = ProcessContainers;

		$scope.showRelations = function (itemId, translation) {
			let params = StateParameters;
			params.itemColumnId = itemId;
			params.Translation = translation;
			ViewService.view('settings.fields.relations', {params: params}, {split: true});
		};

		$scope.processContainerColumns = ProcessContainerColumns;
		$scope.processContainers = ProcessContainers.processContainers;
		let changedItems = {};
		let newValuesArray = [];

		$scope.type = StateParameters.type;
		$scope.ProcessTabId = StateParameters.ProcessTabId;
		$scope.columns = [{name: 'LEFT', value: 'left'}, {
			name: 'RIGHT',
			value: 'right'
		}, {name: 'BUTTONS', value: 'buttons'}, {name: 'TOP', value: 'top'}, {value: 'bottom', name: 'BOTTOM'}];
		$scope.ProcessTabId = StateParameters.ProcessTabId;
		$scope.processes = Processes;

		$scope.portfolios = FieldsProcessPortfolios;

		$scope.itemColumns = ItemColumns['itemColumns'];

		$scope.dataType = Common.getDataTypes();

		$scope.inputMethod = [{value: 0, name: 'INPUT_METHOD_DATATYPE'}, {
			value: 1,
			name: 'INPUT_METHOD_LIST'
		}, {value: 2, name: 'INPUT_METHOD_SWITCH'}, {
			value: 3,
			name: 'INPUT_METHOD_PARENT'
		}];

		$scope.edit = true;

		let updateConnection = function (itemColumnId, containerColumnId, dataType, Variable) {
			if (typeof containerColumnId != 'undefined') {
				let newValues = {ItemColumnId: null, DataType: null, Translation: '', DataTypeName: ''};
				newValues.ItemColumnId = itemColumnId;
				newValues.DataType = dataType;
				newValues.Translation = Variable;
				newValues.DataTypeName = Common.getDataTypeTranslation(dataType);
				$scope.columnConnector[containerColumnId] = newValues;
			}
		};
		
		StateParameters.$$originalProcess = angular.copy(StateParameters.process);

		$scope.addItemColumn = function (ProcessContainerId) {


			let max_order_no = SettingsService.giveOrderNumber($scope.processContainerColumns, ProcessContainerId);

			let parameters = {
				'StateParameters': StateParameters,
				'ProcessContainerId': ProcessContainerId,
				'Process': StateParameters.process,
				'OrderNo': max_order_no,
				'ViewId': StateParameters.ViewId,
				'Unlinked': true
			};

			let params = {
				template: 'settings/containers/itemColumnAddDialog.html',
				controller: 'ContainersAddNewColumnController',
				locals: {
					DialogParameters: parameters,
					ItemColumnsData: ItemColumnsData,
					Lists: Lists,
					Processes: Processes,
					ProcessContainerColumns: $scope.processContainerColumns,
					UnlinkedColumns: $scope.itemColumns[0],
					UpdateConnection: updateConnection,
					SubQueries: SubQueries
				},
				wider:true
			};
			MessageService.dialogAdvanced(params);
		};


		SaveService.subscribeSave($scope, function () {
			if ((angular.isDefined(changedItems['fields']) && changedItems['fields'].length > 0)) {
				saveItemColumns();
			}
		});

		let saveItemColumns = function () {
			let itemColumnArray = [];
			if (changedItems['fields']) {
				angular.forEach(changedItems['fields'], function (item) {
					let index = Common.getIndexOf($scope.itemColumns[item.ProcessContainerId], item.ItemColumnId, 'ItemColumnId');
					itemColumnArray.push($scope.itemColumns[item.ProcessContainerId][index]);
				});
				SettingsService.ItemColumnsSave(StateParameters.process, itemColumnArray).then(function () {
					changedItems['fields'] = [];
				});
			}
			newValuesArray = [];
		};

		$scope.changeValue = function (params) {
			newValuesArray.push({
				ItemColumnId: params.ItemColumnId,
				ProcessContainerId: params.ProcessContainerId,
				type: params.type,
				column: params.column
			});
			let noDuplicateObjects = {};
			let l = newValuesArray.length;

			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.type + '_' + item.ProcessContainerId + '_' + item.ItemColumnId] = item;
			}

			let nonDuplicateArray = {};

			for (let item in noDuplicateObjects) {
				let type = noDuplicateObjects[item].type;
				if (typeof nonDuplicateArray[type] == 'undefined') {
					nonDuplicateArray[type] = [];
				}
				nonDuplicateArray[type].push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};

		$scope.deleteProcessContainer = function (ProcessContainerId) {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				SettingsService.ProcessContainersDelete(StateParameters.process, ProcessContainerId).then(function () {
					let index = Common.getIndexOf($scope.containers['left'], ProcessContainerId, 'ProcessContainerId', 'ProcessContainer');
					$scope.containers['left'].splice(index, 1);
				});
			});
		};

		$scope.deleteProcessContainerFromTab = function (ProcessTabContainerId, column) {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				SettingsService.ProcessTabContainersDelete(StateParameters.process, ProcessTabContainerId).then(function () {
					let index = Common.getIndexOf($scope.containers[column], ProcessTabContainerId, 'ProcessTabContainerId');
					$scope.containers[column].splice(index, 1);
				});
			});
		};

		$scope.showJoin = function (column) {
			if (column.DataType == 11) {
				let params = StateParameters;
				params.SubProcess = column.DataAdditional;
				params.ItemColumnId = column.ItemColumnId;
				ViewService.view('settings.fields.joinColumns', {params: params}, true);
			}
		};

		$scope.changeView = function () {
			setTimeout(function () {
				ViewService.viewAdvanced('settings.containers.tabs', {
					params: {
						process: StateParameters.process,
						ProcessTabId: $scope.ProcessTabId,
						type: $scope.type
					}
				}, {targetId: 'containersView-' + $scope.ProcessTabId, ParentViewId: StateParameters.ParentViewId});
			}, 0);
		};


		$scope.changeOrderContainer = function (current, prev, next, parent) {
			let beforeOrderNo;
			let afterOrderNo;
			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}

			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			current.ProcessContainerSide = parseInt(parent);
			SettingsService.ProcessTabContainersSave(StateParameters.process, current);
		};

		$scope.deleteField = function (ProcessContainerId, ItemColumnId) {
			let index = Common.getIndexOf($scope.itemColumns[ProcessContainerId], ItemColumnId, 'ItemColumnId');
			$scope.itemColumns[ProcessContainerId].splice(index, 1);
		};


		$scope.deleteItemColumnCompletely = function (itemColumnId) {
			MessageService.confirm('DELETE_CONFIRMATION_FINAL', function () {
				SettingsService.ItemColumnsInContainersDelete(StateParameters.process, itemColumnId);
				let deletedIndex = Common.getIndexOf($scope.itemColumns[0], itemColumnId, 'ItemColumnId');
				$scope.itemColumns[0].splice(deletedIndex, 1);
			}, 'warn');
		};

		$scope.showProperties = function (itemColumn, ProcessContainerId, column) {
			let parameters = {
				'ProcessContainerId': ProcessContainerId,
				'itemColumn': itemColumn,
				'column': column,
				'process': StateParameters.process,
			};

			let params = {
				params: parameters,
				locals: {
					DialogParameters: parameters,
					Lists: Lists,
					Processes: Processes,
					UpdateConnection: updateConnection
				}
			};
			ViewService.view('settings.fields.properties', params, {split: true});
		};
	}

	ItemColumnJoinsSettingsController.$inject = ['$scope', 'MessageService', 'SettingsService', 'Common', 'SaveService', 'StateParameters', 'ItemColumnJoins', 'ParentColumns', 'SubColumns'];

	function ItemColumnJoinsSettingsController($scope, MessageService, SettingsService, Common, SaveService, StateParameters, ItemColumnJoins, ParentColumns, SubColumns) {
		$scope.portfolioJoinColumns = ItemColumnJoins;
		$scope.parentColumns = ParentColumns;
		$scope.subColumns = SubColumns;
		$scope.dateTypes = [{type: 0, name: 'YEAR'}, {
			type: 1,
			name: 'MONTH'
		}, {type: 2, name: 'DAY'}];
		$scope.ParentColumnType = [{type: 0, name: 'ITEM_ID'}, {
			type: 1,
			name: 'COLUMN'
		}, {type: 2, name: 'DATE'}];


		$scope.dateOffsets = [];
		for (let i = 0 - 50; i++; i < 100) {
			$scope.dateOffsets.push({offset: i});
		}

		$scope.selectedColumns = [];
		$scope.edit = true;

		let newValuesArray = [];
		let changedItems = [];
		$scope.ProcessPortfolioId = StateParameters.ProcessPortfolioId;

		let deletePortfolioJoinColumns = function () {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				let deleteIds = Common.convertArraytoString($scope.selectedColumns, 'id');
				SettingsService.ItemColumnJoinsDelete(StateParameters.process, deleteIds).then(function () {
					angular.forEach($scope.selectedColumns, function (row) {
						let index = Common.getIndexOf($scope.portfolioJoinColumns, row.id, 'ProcessPortfolioJoinColumnId');
						$scope.portfolioJoinColumns.splice(index, 1);
					});
					$scope.selectedColumns = [];
				});
			});
		};

		$scope.header = {
			title: 'STATE_SETTINGS_PROCESS_PORTFOLIO_JOIN_COLUMNS',
			menu: [
				{
					name: 'DELETE',
					onClick: deletePortfolioJoinColumns
				}
			]
		};
		StateParameters.activeTitle = $scope.header.title;

		SaveService.subscribeSave($scope, function () {
			savePortfolios();
		});

		let savePortfolios = function () {
			let portfolioColumnsAdd = [];
			let portfolioColumnsSave = [];
			angular.forEach(changedItems, function (item) {
				item.column.ItemColumnId = StateParameters.ItemColumnId;
				if (angular.isDefined(item.column.ItemColumnJoinId) && item.column.ItemColumnJoinId > 0) {
					portfolioColumnsSave.push(item.column);
				} else {
					portfolioColumnsAdd.push(item.column);
				}
			});

			if (portfolioColumnsAdd.length > 0) {
				SettingsService.ItemColumnJoinsAdd(StateParameters.process, portfolioColumnsAdd);
			}
			if (portfolioColumnsSave.length > 0) {
				SettingsService.ItemColumnJoinsSave(StateParameters.process, portfolioColumnsSave);
			}

			newValuesArray = [];
			changedItems = [];
		};

		$scope.changeValue = function (params) {
			newValuesArray.push({id: params.id, column: params.column});
			let noDuplicateObjects = {};
			let l = newValuesArray.length;

			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.id] = item;
			}

			let nonDuplicateArray = [];
			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}

			changedItems = nonDuplicateArray;
		};

		$scope.addPortfolioJoinColumns = function () {
			$scope.portfolioJoinColumns.push({
				ProcessPortfolioJoinId: StateParameters.ProcessPortfolioJoinId,
				ParentColumnType: 0,
				ParentItemColumnId: null,
				ParentAdditionalJoin: {},
				SubColumnType: 0,
				SubItemColumnId: null,
				SubAdditionalJoin: {}
			});
		};

		//Is this used on anything?
		/*
                $scope.addContainer = function () {
                    data = {LanguageTranslation: {}, ProcessContainerSide: null};

                    var save = function () {
                        addNewContainerSave();
                    };

                    var option = [{value: 1, name: 'left'}, {value: 2, name: 'right'}, {value: 3, name: 'buttons'}];

                    var content = {
                        buttons: [{text: 'Discard'}, {type: 'primary', onClick: save, text: 'Save'}],
                        inputs: [{
                            type: 'language',
                            label: 'PROCESS_CONTAINER_NAME',
                            model: 'LanguageTranslation'
                        }, {
                            type: 'select',
                            label: 'COLUMNS',
                            model: 'ProcessContainerSide',
                            options: option,
                            optionName: 'name',
                            optionValue: 'value',
                            singleValue: true
                        }],
                        title: 'Edit title'
                    };

                    MessageService.dialog(content, data)
                };*/

		//Is this used on anything?

		/*	var addNewContainerSave = function () {
                var column;

                if (data.ProcessContainerSide == 1) {
                    column = 'left';
                } else if (data.ProcessContainerSide == 2) {
                    column = 'right';
                } else if (data.ProcessContainerSide == 3) {
                    column = 'buttons';
                } else if (data.ProcessContainerSide == 4) {
                    column = 'top';
                } else if (data.ProcessContainerSide == 5) {
                    column = 'bottom';
                } else {
                    column = 'left';
                }

                var beforeOrderNo;
                var afterOrderNo;
                angular.forEach($scope.containers[column], function (container) {
                    if (angular.isUndefined(beforeOrderNo)) {
                        beforeOrderNo = container.OrderNo;
                    } else if (beforeOrderNo < container.OrderNo) {
                        beforeOrderNo = container.OrderNo;
                    }
                });
                var max_order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);

                var container = {};
                container.ProcessTabContainerId = 0;
                container.ProcessTab = {};
                container.ProcessTab.ProcessTabId = $scope.ProcessTabId;
                container.ProcessContainer = {};
                container.ProcessContainerSide = data.ProcessContainerSide;
                container.ProcessContainer.OrderNo = max_order_no;
                container.ProcessContainer.LanguageTranslation = data.LanguageTranslation;

                SettingsService.ProcessContainersAdd(StateParameters.process, container.ProcessContainer).then(function (cont) {
                    container.ProcessContainer = cont;
                    container.OrderNo = max_order_no;
                    SettingsService.ProcessTabContainersAdd(StateParameters.process, container).then(function (cont_) {

                        if (angular.isUndefined($scope.containers[column])) {
                            $scope.containers[column] = [];
                        }

                        $scope.containers[column].push(cont_);
                    });
                });
                data = {};
            };*/
	}

	ItemColumnRelationsController.$inject = ['$rootScope', '$scope', 'ConditionRelations', 'ItemColumnActions', 'Common', 'StateParameters', 'MessageService', 'ViewService', 'Translator'];

	function ItemColumnRelationsController($rootScope, $scope, ConditionRelations, ItemColumnActions, Common, StateParameters, MessageService, ViewService, Translator) {

		let headerText = Translator.translate('ITEM_COLUMN_RELATIONS') + ' - ' + Translator.translation(StateParameters.Translation);

		$scope.header = {
			title: headerText,
		};

		$scope.conditionRelations = ConditionRelations;
		$scope.actionRelations = ItemColumnActions;

		$scope.showAction = function (actionId, actionTranslation) {
			let params = StateParameters;
			MessageService.closeDialog();
			params.ProcessActionId = actionId;
			params.UserLanguageTranslation = actionTranslation;
			ViewService.view('settings.processActionRows', {params: params}, {split: true});
		};

		$scope.showCondition = function (condition) {
			let params = {process: "", ConditionTranslation: ""};
			params.process = StateParameters.process;
			params.ConditionTranslation = condition.Translation;
			ViewService.view('settings.conditions.fields',
				{
					params: params,
					locals: {
						NewCondition: condition
					}
				},
				{
					split: true,
					storeInCookies: false
				});
			MessageService.closeDialog();
		};
	}
})();
