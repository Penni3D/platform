﻿(function () {
	'use strict';

	angular
		.module('core.settings', [])
		.service('SettingsService', SettingsService)
		.config(SettingsConfig);


	SettingsConfig.$inject = ['ViewsProvider'];

	function SettingsConfig(ViewsProvider) {
		ViewsProvider.addView('settings', {
			resolve: {
				Navigation: function (StateParameters, SidenavService, ApiService, Translator, SettingsService) {
					if (StateParameters.processType == 4) return;

					let ProcessGroupsQuery = ApiService.v1api('settings/ProcessGroups');
					ProcessGroupsQuery.get(StateParameters.process).then(function (result) {
						let phaseTabs = [];
						let portfolioTabs = [];
						let chartTabs = [];

						_.each(result, function (item) {
							let params = angular.copy(StateParameters);
							params.ProcessGroupId = item.ProcessGroupId;
							params.phaseName = Translator.translation(item.LanguageTranslation) + ' ' + item.MaintenanceName;
							phaseTabs.push({
								name: Translator.translation(item.LanguageTranslation) + ' ' + item.MaintenanceName,
								view: "settings.phases.tabs",
								params: params
							})
						});

						SettingsService.ProcessPortfolios(StateParameters.process, 0).then(function (portfolios) {
							angular.forEach(portfolios, function (portfolio) {
								let params = angular.copy(StateParameters);
								params.ProcessPortfolioId = portfolio.ProcessPortfolioId;
								params.type = 'portfolios';
								portfolioTabs.push({
									name: Translator.translation(portfolio.LanguageTranslation),
									view: "settings.portfolios.fields",
									params: params
								})
							});


							SettingsService.ProcessDashboardGetChartsForProcess(StateParameters.process).then(function (charts) {
								angular.forEach(charts, function (chart) {
									let params = _.extend({}, angular.copy(StateParameters), {chart: chart});
									chartTabs.push({
										name: Translator.translation(chart.LanguageTranslation),
										view: "settings.dashboardcharts.fields",
										params: params
									})
								});

								let n1 = [
									{name: 'SETTINGS_VIEW_SUMMARY', view: 'settings.summary'},
									{name: 'SETTINGS_VIEW_REST_DOCUMENTATION', view: 'settings.restdocumentation'},
									{name: 'SETTINGS_VIEW_PHASES', view: 'settings.phases'},
									{name: 'SETTINGS_VIEW_CONDITIONS', view: 'settings.conditions'},
									{name: 'SETTINGS_VIEW_NCONDITIONS', view: 'settings.notificationconditions'},
									{name: 'SETTINGS_VIEW_ACTIONS', view: 'settings.processActions'},
									{name: 'SETTINGS_VIEW_UNLINKED_ELEMENTS', view: 'settings.containers'},
									{name: 'SETTINGS_VIEW_PORTFOLIOS', view: 'settings.portfolios'},
									{name: 'SETTINGS_VIEW_ALL_PORTFOLIOS', view: ''},
									{name: 'SETTINGS_VIEW_ALL_CHARTS', view: ''}
								];
								
								if (StateParameters.processType === 6) {
									n1 = [
										{name: 'SETTINGS_VIEW_PORTFOLIOS', view: 'settings.portfolios'},
										{name: 'SETTINGS_VIEW_ALL_PORTFOLIOS', view: ''},
										{name: 'SETTINGS_VIEW_CONDITIONS', view: 'settings.conditions'}
									];
								}

								let nav = [];
								_.each(n1, function (menuitem) {
									let tab = [{
										name: menuitem.name,
										view: menuitem.view,
										params: StateParameters
									}];

									let index = 1;

									if (menuitem.name === 'SETTINGS_VIEW_PHASES') tab = phaseTabs;
									if (menuitem.name === 'SETTINGS_VIEW_ALL_PORTFOLIOS') tab = portfolioTabs;
									if (menuitem.name === 'SETTINGS_VIEW_ALL_CHARTS') tab = chartTabs;

									nav.push({
										expanded: true,
										name: menuitem.name,
										tabs: tab
									});
								});

								SidenavService.navigation(nav, {index: 1});
							});
						});
					});
				}
			}
		});
	}

	SettingsService.$inject = ['$q', 'ApiService', '$rootScope', 'Common', 'Translator', 'Views', 'PortfolioService', 'ViewService'];

	function SettingsService($q, ApiService, $rootScope, Common, Translator, Views, PortfolioService, ViewService) {
		let self = this;

		let selectedContainerRows;
		let selectedFieldsRows;

		let ConditionsQuery = ApiService.v1api('settings/Conditions');
		let NotificationConditionsQuery = ApiService.v1api('settings/NotificationConditions');
		//let NotificationsQuery = ApiService.v1api('model/process/Notifications');
		let ProcessContainersNotInTabsQuery = ApiService.v1api('settings/ProcessContainersNotInTabs');
		let ItemColumnsInContainersQuery = ApiService.v1api('settings/itemColumnsInContainers');
		let ItemColumnsQuery = ApiService.v1api('meta/itemColumns');
		let ItemsQuery = ApiService.v1api('meta/Items');
		let ListProcessesQuery = ApiService.v1api('meta/listProcesses');
		let OperatorsQuery = ApiService.v1api('meta/Operators');
		let ProcessContainerColumnsQuery = ApiService.v1api('settings/ProcessContainerColumns');
		let ProcessContainersQuery = ApiService.v1api('settings/ProcessContainers');
		let ProcessContainerCopyQuery = ApiService.v1api('settings/ProcessContainerCopy');
		let ProcessGroupCountsQuery = ApiService.v1api('settings/ProcessGroupCounts');
		let ProcessGroupTabsQuery = ApiService.v1api('settings/ProcessGroupTabs');
		let ProcessGroupTabsCopyQuery = ApiService.v1api('settings/ProcessGroupTabsCopy');
		let ProcessGroupsQuery = ApiService.v1api('settings/ProcessGroups');
		let ProcessPortfolioColumnsQuery = ApiService.v1api('settings/ProcessPortfolioColumns');
		let ProcessPortfolioGroupsQuery = ApiService.v1api('settings/ProcessPortfolioGroups');
		let ProcessPortfolioUserGroupsQuery = ApiService.v1api('meta/ProcessPortfolioUserGroups');
		let ProcessPortfoliosQuery = ApiService.v1api('meta/ProcessPortfolios');
		
		
		let ProcessDashboarsdsQuery = ApiService.v1api('meta/ProcessDashboards');
		let ProcessDashboarsdChartsQuery = ApiService.v1api('settings/ProcessDashboardCharts');
		let ProcessDashboardChartFieldsQuery = ApiService.v1api('settings/RegisteredCharts');
		
		
		
		let ProcessTabContainersQuery = ApiService.v1api('settings/ProcessTabContainers');
		let ProcessTabGroupsQuery = ApiService.v1api('settings/ProcessTabGroups');
		let ProcessTabSubprocessesQuery = ApiService.v1api('settings/ProcessTabSubprocesses');
		let ProcessTabsQuery = ApiService.v1api('settings/ProcessTabs');
		let ProcessesQuery = ApiService.v1api('settings/processes');
		let SystemListQuery = ApiService.v1api('meta/SystemList');
		let ProcessSubprocessesQuery = ApiService.v1api('settings/ProcessSubprocesses');
		let ProcessPortfolioJoinsQuery = ApiService.v1api('settings/ProcessPortfolioJoins');
		let ItemColumnJoinsQuery = ApiService.v1api('settings/ItemColumnJoins');
		let ProcessTabsButtonsQuery = ApiService.v1api('settings/ProcessTabsButtons');
		let ProcessTabHierarchyListsQuery = ApiService.v1api('settings/ProcessTabHierarchyLists');
		let ProcessPortfolioDialogQuery = ApiService.v1api('settings/ProcessPortfolioDialogs');
		let ItemColumnEquationsQuery = ApiService.v1api('settings/ItemColumnEquations');

		let ProcessPortfolioActionsQuery = ApiService.v1api('settings/ProcessPortfolioActions');
		let ProcessActionsQuery = ApiService.v1api('settings/ProcessActions');
		let ProcessActionDialog = ApiService.v1api('settings/ProcessActions/Dialogs');
		let DuplicateVariable = ApiService.v1api('settings/DuplicateVariable');
		let ProcessGroupNavigation = ApiService.v1api('settings/ProcessGroupNavigation');
		let ConditionsGroups = ApiService.v1api('settings/ConditionsGroups');
		let Helpers = ApiService.v1api('meta/helpers');
		let RestDocs = ApiService.v1api('meta/RestDocs');

		self.getLookupConnectorColumns = function(process){
			return ItemColumnsQuery.get([process, 'lookup_connectors']);
		}

		self.getProcessFeatures = function (process) {
			return ProcessesQuery.get(['processFeatures', process]);
		};

		self.getProcessPortfolioActions = function (process, portfolioId) {
			return ProcessPortfolioActionsQuery.get([process, portfolioId]);
		};
		self.removeProcessPortfolioAction = function (process, portfolioActionId) {
			return ProcessPortfolioActionsQuery.delete([process, portfolioActionId]);
		};
		self.updateProcessPortfolioAction = function (process, portfolioActions) {
			return ProcessPortfolioActionsQuery.save([process], portfolioActions);
		};
		self.newProcessPortfolioAction = function (process, portfolioId) {
			return ProcessPortfolioActionsQuery.new([process, portfolioId]);
		};

		self.subscribeNewContainer = function (scope, callback) {
			let handler = $rootScope.$on('notifyNewContainer', callback);
			scope.$on('$destroy', handler);
		};

		self.notifyNewContainer = function () {
			$rootScope.$emit('notifyNewContainer');
		};

		self.subscribeItemColumnSave = function (scope, callback) {
			let handler = $rootScope.$on('notifyItemColumnSave', callback);
			scope.$on('$destroy', handler);
		};

		self.notifyItemColumnSave = function () {
			$rootScope.$emit('notifyItemColumnSave');
		};

		self.getProcessGroupNavigation = function (processGroupId) {
			return ProcessGroupNavigation.get([processGroupId]);
		};

		self.saveProcessGroupNavigation = function (process, processGroupId, data) {
			return ProcessGroupNavigation.save([process, processGroupId], data);
		};

		self.deleteProcessGroupNavigation = function (process, processGroupId, processTabId) {
			return ProcessGroupNavigation.delete([process, processGroupId, processTabId]);
		};

		self.ProcessPortfolioDialog = function (process, dialogId?) {
			return ProcessPortfolioDialogQuery.get([process, dialogId]);
		};

		self.ProcessPortfolioContainersDelete = function (process, dialogContainerIds) {
			return ProcessPortfolioDialogQuery.delete([process, dialogContainerIds]);
		};

		self.ProcessPortfolioDialogAdd = function (process, data, dialogId?) {
			return ProcessPortfolioDialogQuery.new([process, dialogId], data);
		};

		self.ProcessPortfolioDialogSave = function (process, data, dialogId?) {
			return ProcessPortfolioDialogQuery.save([process, dialogId], data);
		};

		self.ProcessTabHierarchyLists = function (process) {
			return ProcessTabHierarchyListsQuery.get([process]);
		};

		self.getProcessMap = function (process) {
			return ProcessesQuery.get(["map", process]);
		};

		self.ProcessDelete = function (process) {
			return ProcessesQuery.delete([process]);
		};

		self.ProcessItems = function (process, id) {
			return ItemsQuery.get([process, id]);
		};

		self.ProcessSubprocesses = function (process) {
			return ProcessSubprocessesQuery.get([process]);
		};

		self.ProcessItemsParentList = function (process, itemType, parentItemId) {
			return ItemsQuery.get([process, itemType, parentItemId]);
		};

		self.ItemColumnsInContainers = function (process, containerId?) {
			return ItemColumnsInContainersQuery.get([process, containerId]);
		};

		self.ItemColumnsInContainersDelete = function (process, item_column_id) {
			return ItemColumnsInContainersQuery.delete([process, item_column_id]);
		};
		self.ItemColumnsInContainersDeleteMany = function (process, idsString) {
			return ItemColumnsInContainersQuery.delete([process, idsString, 'columns']);
		};

		self.ProcessContainers = function (process) {
			return ProcessContainersQuery.get([process]);
		};
		self.ProcessContainersNotInTabs = function (process) {
			return ProcessContainersNotInTabsQuery.get([process]);
		};

		self.ProcessContainersAdd = function (process, Data) {
			return ProcessContainersQuery.new([process], Data);
		};

		self.ProcessContainersSave = function (process, Data) {
			return ProcessContainersQuery.save([process], Data);
		};

		self.ProcessGroups = function (process, processGroupId) {
			return ProcessGroupsQuery.get([process, processGroupId]);
		};

		self.ItemColumns = function (process) {
			return ItemColumnsQuery.get([process]);
		};

		self.ItemColumn = function (process, ItemColumnId) {
			return ItemColumnsQuery.get([process, ItemColumnId]);
		};

		self.ProcessGroupsAdd = function (process, group) {
			return ProcessGroupsQuery.new([process], group);
		};

		self.ProcessGroupsSave = function (process, group) {
			return ProcessGroupsQuery.save([process], group);
		};

		self.ProcessGroupsDelete = function (process, ProcessGroup) {
			return ProcessGroupsQuery.delete([process, ProcessGroup]);
		};

		self.DeletePhaseFeatures = function (groupId, featuresToDelete) {
			return ProcessGroupsQuery.new([groupId, "featuredelete"], featuresToDelete);
		}

		self.ProcessGroupTabs = function (process, tabId?) {
			return ProcessGroupTabsQuery.get([process, tabId]);
		};

		self.ProcessGroupTabsCopy = function (process, data) {
			return ProcessGroupTabsCopyQuery.new([process], data);
		};


		self.ProcessGroupTabsDelete = function (process, tabId) {
			return ProcessGroupTabsQuery.delete([process, tabId]);
		};

		self.ProcessTabsSave = function (process, Process) {
			return ProcessTabsQuery.save([process], Process);
		};


		self.ProcessTabsButtons = function (process, tabId) {
			return ProcessTabsButtonsQuery.get([process, tabId]);
		};

		self.ProcessTabsButtonsAdd = function (process, tabId, data) {
			return ProcessTabsButtonsQuery.new([process, tabId], data);
		};

		self.ProcessTabsButtonsSave = function (process, tabId, data) {
			return ProcessTabsButtonsQuery.save([process, tabId], data);
		};

		self.ProcessTabsButtonsDelete = function (process, tabId, processTabColumnIds) {
			return ProcessTabsButtonsQuery.delete([process, tabId, processTabColumnIds]);
		};

		self.ListProcesses = function (processType?, process?) {
			return ListProcessesQuery.get([processType, process]);
		};

		self.ListProcessesGet = function (processType, process) {
			return ListProcessesQuery.get([processType, process]);
		};

		self.SystemList = function (systemList) {
			return SystemListQuery.get(systemList);
		};

		self.ProcessPortfolios = function () {
			return ProcessPortfoliosQuery.get();
		};

		self.GetProcessPortfolio = function (process, portfolioId) {
			return ProcessPortfoliosQuery.get([process, 0, portfolioId]);
		};

		self.ProcessDashboardGet = function (process, dashboardId, hide = false) {
			if(hide){
				return ProcessDashboarsdsQuery.get([process, dashboardId, "hide"]);
			} else {
				return ProcessDashboarsdsQuery.get([process, dashboardId]);
			}
		};

		self.ProcessDashboardGetCharts = function () {
			return ProcessDashboarsdChartsQuery.get();
		};

		self.DashboardRelations = function () {
			return ProcessDashboarsdChartsQuery.get("relations");
		};

		self.ProcessDashboardGetChartsForProcess = function (process, includeChartsFromLinkedSubProcesses = false) {
			if (includeChartsFromLinkedSubProcesses) return ProcessDashboarsdChartsQuery.get(['with', 'links', process]);
			return ProcessDashboarsdChartsQuery.get([process]);
		};

		self.ProcessDashboardGetChart = function (process, chartId) {
			return ProcessDashboarsdChartsQuery.get([process, chartId]);
		};

		self.GetChartLinkData = function (process, dashboardId) {
			return ProcessDashboarsdsQuery.get([process, 'linkedChartsData', dashboardId]);
		};

		self.CreateChartLinkAndUpdateData = function (process, dashboardId) {
			return ProcessDashboarsdsQuery.get([process, 'linkedChartsNew', dashboardId]);
		};

		//self.deleteChartLink = function (link_id, chart_ids, dashboard_id ) {
		//    return ProcessDashboarsdsQuery.get([process, 'deleteChartLink', link_id, chart_ids, dashboard_id]);
		//};

		self.ProcessDashboardChartAdd = function (process, chart) {
			return ProcessDashboarsdChartsQuery.new([process], chart || {});
		};

		self.ProcessDashboardChartSave = function (process, chart) {
			return ProcessDashboarsdChartsQuery.save([process, chart.id], chart);
		};

		self.ProcessDashboardChartDelete = function (process, chartIds) {
			return ProcessDashboarsdChartsQuery.delete([process, chartIds]);
		};

		self.ProcessDashboards = function (process, portfolioId) {
			if (portfolioId) {
				return ProcessDashboarsdsQuery.get([process, 'portfolio', portfolioId]);
			} else {
				return ProcessDashboarsdsQuery.get(process);
			}
		};

		self.ProcessDashboardsAdd = function (process, dashboard) {
			return ProcessDashboarsdsQuery.new([process], dashboard || {});
		};

		self.ProcessDashboardSave = function (process, dashboard) {
			return ProcessDashboarsdsQuery
				.save([process], dashboard);
		};

		self.ProcessDashboardsDelete = function (process, dashboardIds) {
			return ProcessDashboarsdsQuery.delete([process, dashboardIds]);
		};

		self.ProcessDashboardsData = function (process, dashboardId, filterText, filters, dashboardFilters) {
			let text = _.isEmpty(filterText) ? ' ' : filterText;
			if (angular.isUndefined(dashboardFilters) || dashboardFilters == null) {
				return ProcessDashboardChartFieldsQuery.get([process, 'data', dashboardId, text, filters]);
			} else {
				return ProcessDashboardChartFieldsQuery.new([process, 'data', dashboardId, text, filters], dashboardFilters);
			}
		};

		self.ProcessDashboardGetTypes = function () {
			return ProcessDashboardChartFieldsQuery.get(['charttypes']);
		};

		self.ProcessDashboardGetFields = function (process, chartType) {
			return ProcessDashboardChartFieldsQuery.get([process, chartType]);
		};

		self.ItemColumnsAdd = function (process, ItemColumns) {
			return ItemColumnsQuery.new([process], ItemColumns);
		};

		self.ItemColumnsSave = function (process, ItemColumns) {
			return ItemColumnsQuery.save([process], ItemColumns);
		};

		self.ItemColumnsDelete = function (process, ItemColumns, ItemColumnId) {
			return ItemColumnsQuery.delete([process, ItemColumns, ItemColumnId]);
		};

		self.ProcessTabs = function (process, processId?): Client.ProcessTab {
			return ProcessTabsQuery.get([process, processId]);
		};

		self.ProcessGroupCounts = function (process) {
			return ProcessGroupCountsQuery.get([process]);
		};

		self.Processes = function () {
			return ProcessesQuery.get();
		};

		self.ProcessTabPhasesAdd = function (process, Process) {
			return ProcessTabGroupsQuery.new([process], Process);
		};

		self.ProcessTabPhasesSave = function (process, Process) {
			return ProcessTabGroupsQuery.save([process], Process);
		};

		self.ProcessContainerColumns = function (process, containerId?) {
			return ProcessContainerColumnsQuery.get([process, containerId]);
		};

		self.ProcessContainerColumnsDelete = function (process, ProcessContainerColumnId) {
			return ProcessContainerColumnsQuery.delete([process, ProcessContainerColumnId]);
		};

		self.ProcessContainerColumnsAdd = function (process, Process) {
			return ProcessContainerColumnsQuery.new([process], Process);
		};

		self.ProcessContainerColumnsSave = function (process, Process) {
			return ProcessContainerColumnsQuery.save([process], Process);
		};

		self.ProcessSubprocessDelete = function (process, tabId, processTabSubprocessId) {
			return ProcessTabSubprocessesQuery.delete([process, tabId, processTabSubprocessId]);
		};

		self.ProcessTabsAdd = function (process, Process) {
			return ProcessTabsQuery.new([process], Process);
		};

		//self.ProcessTabsSave = function (process, Process) {
		//    return ProcessTabsQuery.save([process], Process);
		//};

		self.ProcessTabSubprocessess = function (process, tabId) {
			return ProcessTabSubprocessesQuery.get([process, tabId]);
		};

		self.ProcessTabContainers = function (process, tabId?) {
			return ProcessTabContainersQuery.get([process, tabId]);
		};

		self.ProcessTabContainersAdd = function (process, TabContainer): Client.TabContainer {
			return ProcessTabContainersQuery.new([process], TabContainer);
		};

		self.ProcessContainerCopy = function (process, data) {
			return ProcessContainerCopyQuery.new([process], data);
		};

		self.ProcessTabContainersSave = function (process, TabContainer) {
			return ProcessTabContainersQuery.save([process], TabContainer);
		};

		self.ProcessTabContainersDelete = function (process, TabContainerId) {
			return ProcessTabContainersQuery.delete([process, TabContainerId]);
		};

		self.ProcessContainersAdd = function (process, Process) {
			return ProcessContainersQuery.new([process], Process);
		};

		self.ProcessContainersSave = function (process, Process) {
			return ProcessContainersQuery.save([process], Process);
		};

		self.ProcessTabSubprocessesAdd = function (process, Process) {
			return ProcessTabSubprocessesQuery.new([process], Process);
		};

		self.ProcessTabSubprocessesSave = function (process, Process) {
			return ProcessTabSubprocessesQuery.save([process], Process);
		};

		self.ProcessTabSubprocessessDelete = function (process, tabId, processTabSubprocessId) {
			return ProcessTabSubprocessesQuery.delete([process, tabId, processTabSubprocessId]);
		};

		self.ProcessContainersDelete = function (process, containerId) {
			return ProcessContainersQuery.delete([process, containerId]);
		};

		self.ProcessTabsDelete = function (process, ProcessTabId) {
			return ProcessTabsQuery.delete([process, ProcessTabId]);
		};

		self.UserGroups = function (process) {
			return ItemsQuery.get([process]);
		};

		self.Conditions = function (process, condition_id) {
			return ConditionsQuery.get([process, condition_id]);
		};

		self.UiParentChildRelations = function (process) {
			return ConditionsQuery.get([process, "phaseids"]);
		};

		self.ItemColumnConditions = function (itemColumnId) {
			return ConditionsQuery.get(["relatedconditions", itemColumnId]);
		};

		self.ConditionHierarchy = function (process) {
			return ConditionsQuery.get([process, "hierarchy"]);
		};

		self.ConditionGroupDelete = function (conditiongroupid) {
			return ConditionsQuery.delete(conditiongroupid);
		};

		self.NewConditionGroups = function (process, Condition) {
			return ConditionsQuery.save([process, 'conditionGroupTabs'], Condition);
		};

		self.ConditionsAdd = function (process, Condition) {
			return ConditionsQuery.new([process], Condition);
		};

		self.ConditionsSave = function (process, Condition) {
			return ConditionsQuery.save([process], Condition);
		};

		self.ConditionsCopy = function (process, Condition) {
			return ConditionsQuery.new([process, 'copy'], Condition);
		};

		self.ConditionsDelete = function (process, ConditionId) {
			return ConditionsQuery.delete([process, ConditionId]);
		};

		self.NotificationConditions = function (process, condition_id) {
			return NotificationConditionsQuery.get([process, condition_id]);
		};

		self.NotificationConditionsAdd = function (process, Condition) {
			return NotificationConditionsQuery.new([process], Condition);
		};

		self.NotificationConditionsSave = function (process, Condition) {
			return NotificationConditionsQuery.save([process], Condition);
		};

		self.NotificationConditionsDelete = function (process, ConditionId) {
			return NotificationConditionsQuery.delete([process, ConditionId]);
		};

		// self.getNotifications = function (min, max, portfolioId) {
		// 	return NotificationsQuery.get([min, max, portfolioId]);
		// };


		self.Operators = function () {
			return OperatorsQuery.get();
		};

		self.ProcessPortfolios = function (process, portfolioType) {
			return ProcessPortfoliosQuery.get([process, portfolioType]);
		};

		self.ProcessPortfoliosAdd = function (process, Portfolio) {
			return ProcessPortfoliosQuery.new([process], Portfolio);
		};

		self.ProcessPortfoliosSave = function (process, Portfolio) {
			return ProcessPortfoliosQuery.save([process], Portfolio);
		};

		self.ProcessPortfoliosDelete = function (process, portfolioId) {
			return ProcessPortfoliosQuery.delete([process, portfolioId]);
		};

		self.CreationContainerDelete = function (process, containerId) {
			return ProcessPortfolioDialogQuery.delete([process, containerId]);
		};

		self.ProcessPortfoliosUserGroups = function (process, portfolioId) {
			return ProcessPortfolioUserGroupsQuery.get([process, portfolioId]);
		};

		self.ProcessPortfolioColumns = function (process, portfolioId) {
			return ProcessPortfolioColumnsQuery.get([process, portfolioId]);
		};

		self.ProcessPortfolioColumnsAdd = function (process, portfolioId, columns) {
			return ProcessPortfolioColumnsQuery.new([process, portfolioId], columns);
		};

		self.ProcessPortfolioColumnsSave = function (process, portfolioId, columns) {
			return ProcessPortfolioColumnsQuery.save([process, portfolioId], columns)
		};

		self.ProcessPortfolioColumnsDelete = function (process, portfolioId, columns) {
			return ProcessPortfolioColumnsQuery.delete([process, portfolioId, columns]);
		};

		self.ProcessPortfolioColumnsGroupsAdd = function (process, columns) {
			return ProcessPortfolioColumnsQuery.new([process], columns);
		};

		self.GetPortfolioColumnGroups = function (process, portfolioId) {
			return ProcessPortfolioColumnsQuery.get(['columngroups', process, portfolioId]);
		};

		self.ProcessPortfolioCopy = function (process, fromCopyId, Portfolio) {
			return ProcessPortfoliosQuery.new(['copy', process, fromCopyId], Portfolio);
		};

		self.UpdatePortfolioColumnGroups = function (process, columnGroup) {
			return ProcessPortfolioColumnsQuery.save(['columngroups', process], columnGroup);
		};

		self.DeleteColumnGroups = function (process, groupId) {
			return ProcessPortfolioColumnsQuery.delete(['columngroups', process, groupId]);
		};

		self.ProcessPortfolioGroups = function (process, portfolioId) {
			return ProcessPortfolioGroupsQuery.get([process, portfolioId]);
		};

		self.ProcessPortfolioGroupsAdd = function (process, portfolioId, group) {
			return ProcessPortfolioGroupsQuery.new([process, portfolioId], group);
		};

		self.ProcessPortfolioGroupsSave = function (process, portfolioId, group) {
			return ProcessPortfolioGroupsQuery.save([process, portfolioId], group);
		};


		self.ProcessPortfolioGroupsDelete = function (process, portfolioId, group) {
			return ProcessPortfolioGroupsQuery.delete([process, portfolioId, group]);
		};


		self.ListProcessesSave = function (process, ListProcesses) {
			return ListProcessesQuery.new([process], ListProcesses);
		};

		self.ListProcessesAdd = function (process, ListProcesses) {
			return ListProcessesQuery.new([process], ListProcesses);
		};

		self.ListProcessesSave = function (process, ListProcesses) {
			return ListProcessesQuery.save([process], ListProcesses);
		};


		/*	self.ListProcessesSubItemAdd = function (process, parentItemId, itemColumns) {
				return ListProcessesSubItemQuery.new([process, parentItemId], itemColumns);
			};
	
			self.ListProcessesSubItemSave = function (process, parentItemId, itemColumns) {
				return ListProcessesSubItemQuery.new([process, parentItemId], itemColumns);
			};*/
		/*
				self.ProcessItemsAdd = function (process, ParentItems) {
					return ProcessItemsQuery.new([process], ParentItems);
				};
		
				self.ProcessItemsSave = function (process, ParentItems) {
					return ProcessItemsQuery.save([process], ParentItems);
				};*/

		self.DeleteProcessItems = function (process, ParentItems) {
			return ItemsQuery.delete([process, ParentItems]);
		};

		self.getProcessGroupTabs = function () {
			return self.processGroupTabs;
		};

		self.ProcessPortfolioJoins = function (process, portfolioId) {
			return ProcessPortfolioJoinsQuery.get([process, portfolioId]);
		};

		self.ProcessPortfolioJoinsAdd = function (process, joins) {
			return ProcessPortfolioJoinsQuery.new([process], joins);
		};

		self.ProcessPortfolioJoinsSave = function (process, joins) {
			return ProcessPortfolioJoinsQuery.save([process], joins);
		};

		self.ProcessPortfolioJoinsDelete = function (process, joins) {
			return ProcessPortfolioJoinsQuery.delete([process, joins]);
		};

		self.ItemColumnJoins = function (process, portfolioJoinId) {
			return ItemColumnJoinsQuery.get([process, portfolioJoinId]);
		};

		self.ItemColumnJoinsAdd = function (process, columns) {
			return ItemColumnJoinsQuery.new([process], columns);
		};

		self.ItemColumnJoinsSave = function (process, columns) {
			return ItemColumnJoinsQuery.save([process], columns);
		};

		self.ItemColumnJoinsDelete = function (process, columns) {
			return ItemColumnJoinsQuery.delete([process, columns]);
		};

		self.ProcessActionsGet = function (process) {
			return ProcessActionsQuery.get([process]);
		};

		self.getDialogSettings = function(process){
			return ProcessActionDialog.get([process]);
		}

		self.getProcessActionDialogs = function (process, dialogId?, itemId?) {
			return ProcessActionDialog.get([process, dialogId, itemId, "trans"]);
		};

		self.getProcessActionDialogsNew = function (process, itemId?) {
			return ProcessActionDialog.get([process, itemId, 'metadialog']);
		};

		self.saveProcessDialog = function (process, dialog) {
			return ProcessActionDialog.save([process], dialog);
		};

		self.checkDuplicateVariable = function (type, variable, process) {
			if (angular.isUndefined(process)) process = '%20';

			return DuplicateVariable.get([type, variable, process]);
		};

		self.setDeletedRows = function (rows, type) {
			if (type == 'container') {
				selectedContainerRows = rows;
			} else if (type == 'fields') {
				selectedFieldsRows = rows;
			}
		};

		self.getDeletedRows = function (type) {
			if (type == 'container') {
				return selectedContainerRows;
			} else if (type == 'fields') {
				return selectedFieldsRows;
			}
		};

		self.notifyDeletedRows = function (type) {
			$rootScope.$emit('deleteFields', type);
		};

		self.subscribeDeletedRows = function (scope, callback) {
			let handler = $rootScope.$on('deleteFields', callback);
			scope.$on('$destroy', handler);
		};

		self.getItemColumnItems = function (ProcessName, itemId, column) {
			return ItemsQuery.get([ProcessName, itemId, column]);
		};

		self.getConditionsGroups = function (process) {
			return ConditionsGroups.get([process]);
		};

		self.addConditionsGroups = function (process, data) {
			return ConditionsGroups.new([process], data);
		};

		self.saveConditionsGroups = function (process, data) {
			return ConditionsGroups.save([process], data);
		};

		self.findFeatureTranslation = function (featureName, featureList) {
			for (let i = 0; i < featureList.length; i++) {
				if (featureList[i].name == featureName) {
					return Translator.translation(featureList[i].variable);
				}
			}
			return featureName;
		};

		self.syncTabWithNavigation = function (navigationArray, matchedElement, property) {
			let p = _.find(navigationArray, function (o) {
				return o.ProcessTabId == matchedElement.ProcessTab.ProcessTabId && o.Type == 0;
			});
			if (typeof p != 'undefined') {
				return p[property];
			} else if (typeof p == 'undefined' && property == 'OrderNo') {
				return 'U';
			}
			return null;
		};

		self.syncFeatureWithNavigation = function (navigationArray, featureName, property) {
			let p = _.find(navigationArray, function (o) {
				return o.Variable == featureName
			});
			if (typeof p != 'undefined') {
				return p[property];
			} else if (typeof p == 'undefined' && property == 'OrderNo') {
				if (navigationArray.length) {
					return Common.getOrderNoBetween(navigationArray[navigationArray.length - 1].OrderNo, undefined);
				}
				return 'U';
			}
			return null;
		};

		self.findFeatureCustomName = function (navigationArray, featureName) {
			let p = _.find(navigationArray, function (o) {
				return o.Variable == featureName
			});
			if (typeof p != 'undefined') {
				return Translator.translate(p.LanguageTranslation);
			}
			return {};
		};

		self.refreshProcessGroupNavigation = function (process, ProcessGroupId) {
			let d = $q.defer();
			let allFeatures = Views.getFeatures();

			ProcessGroupNavigation.get([ProcessGroupId]).then(function (navigation) {
				let groupItems = [];
				let getTabs = function () {
					return self.ProcessGroupTabs(process, ProcessGroupId).then(function (tabs) {
						angular.forEach(tabs, function (tab) {
							groupItems.push({
								ProcessTabId: tab.ProcessTab.ProcessTabId,
								Variable: tab.ProcessTab.Variable,
								Translation: navigation.length ? self.findFeatureCustomName(navigation, tab.ProcessTab.Variable) : tab.ProcessTab.LanguageTranslation,
								DefaultState: 'process.tab',
								Type: 0,
								Icon: self.syncTabWithNavigation(navigation, tab, 'Icon'),
								TemplatePortfolioId: self.syncTabWithNavigation(navigation, tab, 'TemplatePortfolioId'),
								ProcessTab: tab.ProcessTab,
								OrderNo: self.syncTabWithNavigation(navigation, tab, 'OrderNo'),
								ProcessTabGroups: tab.ProcessTabGroups,
								ProcessGroupTabId: tab.ProcessGroupTabId
							});
						});
					});
				};

				let getFeatures = function () {
					return self.ProcessGroups(process, ProcessGroupId).then(function (features) {
						angular.forEach(features.SelectedFeatures, function (feature) {
							groupItems.push({
								ProcessTabId: ProcessGroupId,
								Variable: feature.Feature,
								Translation: self.findFeatureTranslation(feature.Feature, allFeatures),
								DefaultState: feature.DefaultState,
								Type: 1,
								Icon: self.syncFeatureWithNavigation(navigation, feature.Feature, 'Icon'),
								TemplatePortfolioId: self.syncFeatureWithNavigation(navigation, feature.Feature, 'TemplatePortfolioId'),
								OrderNo: self.syncFeatureWithNavigation(navigation, feature.Feature, 'OrderNo'),
								LanguageTranslation: self.findFeatureCustomName(navigation, feature.Feature)
							});
						});
						return groupItems;
					});
				};

				getTabs()
					.then(getFeatures)
					.then(function (data) {
						return d.resolve(_.sortBy(data, [function (o) {
							return o.OrderNo;
						}]));
					});
			});

			return d.promise;
		};
		self.ItemColumnEquations = function (process, itemColumn) {
			return ItemColumnEquationsQuery.get([process, itemColumn]);
		};

		self.ItemColumnEquationsAdd = function (process, column) {
			return ItemColumnEquationsQuery.new([process], column);
		};

		self.ItemColumnEquationsSave = function (process, columnsToC) {
			let columns = angular.copy(columnsToC);

			angular.forEach(columns, function (v, k) {
				if (v.ColumnType == 0 && v.SqlFunction == 10) {
					v.SqlParam1 = JSON.stringify(v.SqlParam1);
				}
				if (v.ColumnType == 3 && v.SqlParam1) {
					v.SqlParam2 = JSON.stringify(v.SqlParam2);
				}
			});
			return ItemColumnEquationsQuery.save([process], columns);
		};

		self.ItemColumnEquationsDelete = function (process, columns) {
			return ItemColumnEquationsQuery.delete([process, columns]);
		};

		self.ItemColumnEquationsTest = function (process, itemColumnId, equation) {
			return ItemColumnEquationsQuery.new(['testEquation', process, itemColumnId], {'e': equation});
		};

		self.giveOrderNumber = function (collection, key) {
			let beforeOrderNo;
			let afterOrderNo;
			angular.forEach(collection[key], function (element) {
				if (angular.isUndefined(beforeOrderNo) || beforeOrderNo < element.OrderNo) {
					beforeOrderNo = element.OrderNo;
				}
			});
			return Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
		};

		self.checkTranslationLength = function (transObject, userLanguage) {
			if (transObject[userLanguage]) {
				let translation = transObject[userLanguage];
				if (translation.length > 0) {
					return true;
				}
			}
		};

		self.addHelper = function (h) {
			return Helpers.new([''], h);
		};

		self.saveHelper = function (helpers) {
			return Helpers.save([''], helpers);
		};

		self.getHelpers = function () {
			let userLanguage = $rootScope.clientInformation.locale_id;
			return Helpers.get().then(function (helpers) {
				_.each(helpers, function (h) {
					//		let t1 = {};
					let t2 = {};
					//		t1[userLanguage] = Translator.translate(h.NameVariable);
					t2[userLanguage] = Translator.translate(h.DescVariable);
					//		h.LanguageTranslationName = t1;
					h.LanguageTranslationDescription = t2;
					h.DescVariable = JSON.parse(h.DescVariable);
				});
				return helpers;
			});
		};

		self.deleteHelper = function (helperId) {
			return Helpers.delete([helperId]);
		}

		self.getProcessViews = function(processes, statesByProcess){
			let promises = [];
			_.each(processes, function(p){
				let processToFind = p;
				if (!statesByProcess.hasOwnProperty(processToFind) && processToFind != '') {
					let tabs_ = [];
					let fs = [];
					promises.push(self.ProcessTabs(processToFind).then(function (tabs) {
						_.each(tabs, function (t) {
							t.DefaultState = t.ProcessTabId;
							t.Feature = Translator.translate(t.Variable) + ' - ' + t.MaintenanceName;
						})
						promises.push(self.ProcessGroups(processToFind).then(function (groups) {
							let features = [];
							angular.forEach(groups, function (group) {
								angular.forEach(group.SelectedFeatures, function (feature) {
									features.push(feature);
								});
							});
							fs = _.uniqBy(features, 'DefaultState');
							tabs_ = _.uniqBy(tabs, 'ProcessTabId');
							statesByProcess[processToFind] = fs.concat(tabs_);
							statesByProcess[processToFind].unshift({DefaultState: "1stTab", Feature: "1ST_TAB"})
							statesByProcess[processToFind].unshift({DefaultState: "lastTab", Feature: "LAST_TAB"})
						}));
					}));
				}})
			return $q.all(promises).then(function () {
				return statesByProcess;
			})
		}

		self.GetAllRestDocs = function () {
			return RestDocs.get();
		};

		self.GetRestDocs = function (process) {
			return RestDocs.get([process]);
		};
	}
})();
