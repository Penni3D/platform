﻿(function () {
    'use strict';

    angular
        .module('core.settings')
        .config(PhasesConfig);

    PhasesConfig.$inject = ['ViewsProvider'];
    function PhasesConfig(ViewsProvider) {

        ViewsProvider.addView('settings.phases', {
            controller: 'PhasesSettingsController',
            template: 'settings/phases/phases.html',
            resolve: {
                ProcessGroups: function (SettingsService, StateParameters, ViewConfigurator, Translator) {
                    return SettingsService.ProcessGroups(StateParameters.process).then(function (phases) {
                        var result = {};
                        var features = [];
                        angular.forEach(phases, function (group) {
                            group.Features = [];
                            angular.forEach(group.SelectedFeatures, function (feature) {
                                group.Features.push(feature.Feature);
                                if (ViewConfigurator.isConfigureable(feature.DefaultState) !== false) {
                                    features.push({name: feature.Feature, view: feature.DefaultState});
                                }
                            });

							SettingsService.ProcessGroupTabs(StateParameters.process, group.ProcessGroupId).then(function (tabs) {
								var resultString = "";
								angular.forEach(tabs, function (a) {
									if (resultString.length == 0 && a['ProcessTab'].MaintenanceName.length > 0) {
										resultString = Translator.translation(a['ProcessTab']['LanguageTranslation']) + " " + "'" + a['ProcessTab']['MaintenanceName'] + "'";
									} else if (resultString.length == 0 && a['ProcessTab'].MaintenanceName.length == 0) {
										resultString = Translator.translation(a['ProcessTab']['LanguageTranslation']);
									} else if (resultString.length > 0 && a['ProcessTab'].MaintenanceName.length > 0) {
										resultString += ',' + " " + Translator.translation(a['ProcessTab']['LanguageTranslation']) + " " + "'" + a['ProcessTab']['MaintenanceName'] + "'";
									}
									else {									
										resultString += ',' + " " + Translator.translation(a['ProcessTab']['LanguageTranslation']);
                                    }
                                });
                                group.Tabs = resultString;
                            });
                        });
                        result.phases = phases;
                        result.features = _.uniqBy(features, 'view');
                        return result;
                    });

                },
                ItemColumns: function (SettingsService, StateParameters) {
                    return SettingsService.ItemColumnsInContainers(StateParameters.process);
                },
                Processes: function (ProcessesService) {
                    return ProcessesService.get();
                }
            }
        });

        ViewsProvider.addView('settings.phases.tabs', {
            controller: 'PhasesTabsSettingsController',
            template: 'settings/phases/phasesTabs.html',
            resolve: {
                LayoutTabs: function (SettingsService, StateParameters) {
                    return SettingsService.ProcessTabs(StateParameters.process);
                },
                Buttons: function (SettingsService, StateParameters) {
                    return SettingsService.ItemColumnsInContainers(StateParameters.process).then(function (columns) {
                        var buttons = [];
                        angular.forEach(columns, function (column) {
                            angular.forEach(column, function (col) {
                                if (col.DataType == 8) {
                                    buttons.push(col);
                                }
                            });
                        });
                        return buttons;
                    });
                },
                ProcessGroupTabs: function (SettingsService, StateParameters) {
					return SettingsService.ProcessGroupTabs(StateParameters.process, StateParameters.ProcessGroupId).then(function (tabs) {
                        var result = [];
                        angular.forEach(tabs, function (tab) {
                            tab.ProcessTabId = tab.ProcessTab.ProcessTabId;
                            result.push(tab);
                        });
                        return result;
                    });
                },
                ProcessTabs: function (SettingsService, StateParameters, $filter, Translator) {
                    return SettingsService.ProcessTabs(StateParameters.process).then(function (tabs) {
                        var newarr = [];
                        var unique = {};
                        angular.forEach(tabs, function (key) {
                            if (!unique[key.ProcessTabId]) {
                                newarr.push(key);
                                unique[key.ProcessTabId] = key;
                            }
                        });
                        newarr = $filter('orderBy')(newarr, function (a) { return Translator.translation(a.LanguageTranslation) });

                        return newarr;
                    });
                },
	            TabsAndFeaturesInOrder: function(SettingsService, StateParameters){
		            return SettingsService.refreshProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId).then(function(tabsAsNavigation){
			            return tabsAsNavigation
		            })
	            },
	            Symbols: function(SettingsService) {
		            return SettingsService.SystemList('SYMBOLS');
	            },
	            Portfolios: function (SettingsService, StateParameters) {
		            return SettingsService.ProcessPortfolios(StateParameters.process, 0).then(function (portfolios) {
			            angular.forEach(portfolios, function (portfolio) {
				            SettingsService.ProcessPortfoliosUserGroups(StateParameters.process, portfolio.ProcessPortfolioId).then(function (groups) {
					            portfolio.ProcessUserGroupIds = groups;
				            });
			            });
			            return portfolios;
		            });
	            },

	            Phase: function(SettingsService, StateParameters){
		            return SettingsService.ProcessGroups(StateParameters.process, StateParameters.ProcessGroupId).then(function (phase) {
			            return phase;
		            });
	            }
            }
        });

        ViewsProvider.addView('settings.phases.tabsButtons', {
            controller: 'PhasesButtonSettingsController',
            template: 'settings/phases/phasesTabsButtons.html',
            resolve: {
                Buttons: function (SettingsService, StateParameters, $filter, Translator) {
                    return SettingsService.ItemColumnsInContainers(StateParameters.process).then(function (columns) {
                        var buttons = [];
                        angular.forEach(columns, function (column) {
                            angular.forEach(column, function (col) {
                                if (col.DataType == 8) {
                                    buttons.push(col);
                                }
                            });
                        });
                        return $filter('orderBy')(buttons, function (button) { return Translator.translation(button.LanguageTranslation )});
                    });
                },
                ProcessTabsButtons: function (SettingsService, StateParameters, $filter, Translator) {
                    return SettingsService.ProcessTabsButtons(StateParameters.process, StateParameters.ProcessTabId).then(function (buttons) {
                        return $filter('orderBy')(buttons, function (button) { return Translator.translation(button.LanguageTranslation) });

                    });
                },
                Views: function (SettingsService, StateParameters, $filter, Views, Translator) {
                    var views = [];
                    views.push({ 'name': 'Frontpage', 'value': 'frontpage' });
                    views.push({ 'name': 'Page: Calendar', 'value': 'calendar' });

                    return SettingsService.ProcessTabs(StateParameters.process).then(function (tabs) {
                        angular.forEach(tabs, function (tab) {
                            views.push({ 'name': 'Tab: ' + Translator.translation(tab.LanguageTranslation), 'value': 'tab_' + tab.ProcessTabId });
                        });

                        angular.forEach(Views.getFeatures(), function (feature) {
                            if (feature.view && feature.view != "process.user") {
                                views.push({ 'name': 'Feature: ' + feature.translation, 'value': feature.view });
                            }
                        });

                        views = $filter('orderBy')(views, 'name');

                        return views;
                    });
                },
                ProcessActions: function (SettingsService, StateParameters) {
                    return SettingsService.ProcessActionsGet(StateParameters.process);
                },
                ProcessActionDialogs: function (SettingsService, StateParameters) {
                    return SettingsService.getDialogSettings(StateParameters.process);
                }
            }
        });


        ViewsProvider.addView('settings.phases.menuOrder', {
            controller: 'PhasesMenuOrderController',
            template: 'settings/phases/phasesMenuOrder.html',
            resolve: {
                ProcessGroupTabs: function (SettingsService, StateParameters) {
                    return SettingsService.refreshProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId)
                },
                Symbols: function(SettingsService) {
                    return SettingsService.SystemList('SYMBOLS');
                },
	            Portfolios: function (SettingsService, StateParameters) {
		            return SettingsService.ProcessPortfolios(StateParameters.process, 0).then(function (portfolios) {
			            angular.forEach(portfolios, function (portfolio) {
				            SettingsService.ProcessPortfoliosUserGroups(StateParameters.process, portfolio.ProcessPortfolioId).then(function (groups) {
					            portfolio.ProcessUserGroupIds = groups;
				            });
			            });
			            return portfolios;
		            });
	            },
            }
		});

		ViewsProvider.addDialog('debug.settings', {
			controller: 'SettingsConditionsDebugController',
			template: 'settings/settingsdebug/debug.html',
			resolve: {
				ProcessConditions: function (ProcessService, StateParameters, Translator) {
					return ProcessService.getConditions(StateParameters.process).then(function (conditions) {
						var ret = {};
						var i = conditions.length;
						while (i--) {
							var c = conditions[i];
							c.translation = Translator.translation(c.LanguageTranslation);
							ret[c.ConditionId] = c;
						}
						return ret;
					});
				},
				UiParentChildRelations: function (SettingsService, StateParameters) {
					return SettingsService.UiParentChildRelations(StateParameters.process);
				},
				ConditionHierarchy: function (SettingsService, StateParameters) {
					return SettingsService.ConditionHierarchy(StateParameters.process);
				}
			}
		});
    }

})();
