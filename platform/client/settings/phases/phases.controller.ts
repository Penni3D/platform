﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.controller('PhasesTabsSettingsController', PhasesTabsSettingsController)
		.controller('PhasesButtonSettingsController', PhasesButtonSettingsController)
		.controller('PhasesMenuOrderController', PhasesMenuOrderController)
		.controller('PhasesAddButtonController', PhasesAddButtonController)
		.controller('PhasesCopyTabController', PhasesCopyTabController);

	
	PhasesTabsSettingsController.$inject = ['$scope', '$rootScope', 'SettingsService', 'MessageService', 'Common', 'SaveService', 'StateParameters', 'ViewService', 'ProcessGroupTabs', 'Buttons', 'ProcessTabs', 'Backgrounds', 'Translator', 'TabsAndFeaturesInOrder', 'ViewConfigurator', 'Symbols', 'Portfolios', 'Views', 'Phase'];
	function PhasesTabsSettingsController($scope, $rootScope, SettingsService, MessageService, Common, SaveService, StateParameters, ViewService, ProcessGroupTabs, Buttons, ProcessTabs, Backgrounds, Translator, TabsAndFeaturesInOrder, ViewConfigurator, Symbols, Portfolios, Views, Phase) {

		$scope.selectedTabHighlight = null;
		$scope.symbols = Symbols;
		$scope.portfolios = Portfolios;

		$scope.features = Views.getFeatures();
		$scope.fullFeatures = angular.copy($scope.features);
		$scope.tabsAndFeaturesInOrder = TabsAndFeaturesInOrder;

		//Ensure all features have a name
		let locale = $rootScope.clientInformation.locale_id;

		let ensureNames = function() {
			for (let f of $scope.tabsAndFeaturesInOrder) {
				if (f.Type == 1 && (_.isEmpty(f.LanguageTranslation) || f.LanguageTranslation[locale] == "")) {
					f.LanguageTranslation[locale] = f.Translation;
				}
			}
		};

		ensureNames();
		
		$scope.changeCustomName = function(params){
			let thisFeatureInPhase = _.find(Phase.SelectedFeatures, function(o) { return o.Feature == params.Variable; });
			thisFeatureInPhase.LanguageTranslation = params.LanguageTranslation;
			Phase.$$changed = true;
			SaveService.tick();
		};
		_.each(Phase.SelectedFeatures, function (name) {
			let index = _.findIndex($scope.features, function(p){
				return p.name == name.Feature;
			});
			$scope.features.splice(index, 1);
		});

		var changedGroupTabArray = [];
		var changedGroupTabs = [];
		var changedTags = [];
		$scope.ProcessTabId = '';
		$scope.buttons = Buttons;

		$scope.edit = true;
		$scope.selectedTab = [];

		$scope.backgrounds = Backgrounds.getBackgrounds();
		$scope.processTabs = ProcessTabs;
		$scope.processGroupsTabs = ProcessGroupTabs;

		let featureAddSave = function(featureD) {
			if(featureD.SelectedFeatures.length == 0) return;
			SettingsService.ProcessGroups(StateParameters.process, StateParameters.ProcessGroupId).then(function (phase) {
				_.each(featureD.SelectedFeatures, function (name) {
					let featureToFind = _.find($scope.features, function (o) {
						return o.name == name;
					});

					let newFeature = {Feature: featureToFind.name, DefaultState: featureToFind.view};
					phase.SelectedFeatures.push(newFeature);
					Phase.SelectedFeatures.push(newFeature);

					let index = _.findIndex($scope.features, function(p){
						return p.name == name;
					});

					$scope.features.splice(index, 1);
				});
				SettingsService.ProcessGroupsSave(StateParameters.process, [phase]).then(function () {
					SettingsService.refreshProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId).then(function(newNavigation){
						$scope.tabsAndFeaturesInOrder = newNavigation;
						ensureNames();
						SettingsService.saveProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId,  $scope.tabsAndFeaturesInOrder);
					});
				})
			})
		};

		$scope.addFeatures = function(){

			let featureData = {SelectedFeatures: null};

			let save = function () {
				featureAddSave(featureData);
				featureData = {SelectedFeatures: null};
			};

			let content = {
				buttons: [{ text: 'CANCEL', modify: 'cancel' }, { type: 'primary', onClick: save, text: 'SAVE', modify: 'create' }],
				inputs: [
					{
						type: 'multiselect',
						label: 'SELECT_FEATURE',
						model: 'SelectedFeatures',
						options: $scope.features,
						optionValue: 'name',
						optionName: 'translation',
						singleValue: true
					}
				],
				title: 'ADD_NEW_FEATURE_PHASE'
			};
			MessageService.dialog(content, featureData);
		};

		let setPhaseOnTabs = function(){
			_.each($scope.tabsAndFeaturesInOrder, function(element){
				if(element.Type == 0){
					element.ProcessGroup = _.find($scope.processGroupsTabs, function(p){return p.ProcessTab.ProcessTabId == element.ProcessTabId} ).ProcessGroup
				} else {
					if (ViewConfigurator.isConfigureable(element.DefaultState) !== false) {
						element.$$showConfiguration = true;
					}
				}
			});
		};

		setPhaseOnTabs();
		var data = {};

		SaveService.subscribeSave($scope, function () {
			if (!angular.isUndefined(changedGroupTabs[0])) {
				saveTabs();
			}

			if(Phase.$$changed){
				SettingsService.ProcessGroupsSave(StateParameters.process, [Phase]).then(function () {
					Phase.$$changed = false;
				})
			}
		});

		$scope.editConfiguration = function (feature) {
			ViewConfigurator.configureFeature(feature, {process: StateParameters.process});
		};

		$scope.showThisTabConditions = function (tab) {
			let objectId = tab.Type == 0 ? tab.ProcessTabId : tab.Variable;
			let objectType = tab.Type == 0 ? 'ProcessTabIds' : 'Features';
			let titleName = tab.Type == 0 ? 'TAB_CONDITIONS' : 'FEATURE_CONDITIONS';


			ViewService.view('debug.settings', {
				params: {
					process: StateParameters.process,
					selectedUiObjectId: objectId,
					selectedUiObjectType: objectType,
					translation: Translator.translation(tab.Translation),
					noConditions: 'NO_TAB_CONDITIONS',
					title: titleName,
					ownKey: 'tabIds'
				}
			});
		};

		let saveTabs = function () {
			let processTabsArray = [];
			let index = '';
			let indexNew = '';
			angular.forEach(changedGroupTabs, function (tab) {
				index = Common.getIndexOf($scope.processTabs, tab.ProcessTabId, 'ProcessTabId');
				indexNew = Common.getIndexOf($scope.tabsAndFeaturesInOrder, tab.ProcessTabId, 'ProcessTabId');
				$scope.processTabs[index].OrderNo = $scope.tabsAndFeaturesInOrder[indexNew].OrderNo;
				$scope.processTabs[index].LanguageTranslation = $scope.tabsAndFeaturesInOrder[indexNew].Translation;
				if(tab.Type == 0){
					$scope.processTabs[index].Background = $scope.tabsAndFeaturesInOrder[indexNew].ProcessTab.Background;
					$scope.processTabs[index].MaintenanceName = $scope.tabsAndFeaturesInOrder[indexNew].ProcessTab.MaintenanceName;
				}
				processTabsArray.push($scope.processTabs[index]);
			});
			SettingsService.ProcessTabsSave(StateParameters.process, processTabsArray);
			changedGroupTabs = [];
		};

		let deleteProcessTabs = function () {

			let areTabs = [];
			let areFeatures = [];

			_.each($scope.selectedTab, function(ele){
				if(ele.Type == 0){
					areTabs.push(ele);
				} else {
					areFeatures.push($scope.tabsAndFeaturesInOrder[ele.Index]);
				}
			});

			let deleteGroupTabIds = Common.convertArraytoString(areTabs, 'ProcessGroupTabId');
			if (deleteGroupTabIds != null && areTabs.length > 0) {
				SettingsService.ProcessGroupTabsDelete(StateParameters.process, deleteGroupTabIds).then(function () {
					angular.forEach(areTabs, function (tab) {
						let index = Common.getIndexOf($scope.tabsAndFeaturesInOrder, tab.ProcessGroupTabId, 'ProcessGroupTabId');
						$scope.tabsAndFeaturesInOrder.splice(index, 1);
						SettingsService.saveProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId,  $scope.tabsAndFeaturesInOrder);
					});
					$scope.selectedTab = [];
				});
			}

			let selectedRows = SettingsService.getDeletedRows('container');
			let deleteIds;
			if (angular.isDefined(selectedRows) && selectedRows.length > 0) {
				deleteIds = Common.convertArraytoString(selectedRows, 'ProcessContainerColumnId');
				SettingsService.ProcessContainerColumnsDelete(StateParameters.process, deleteIds);
				SettingsService.setDeletedRows('', 'container');
				SettingsService.notifyDeletedRows('container');
			}

			selectedRows = SettingsService.getDeletedRows('fields');
			if (angular.isDefined(selectedRows) && selectedRows.length > 0) {
				deleteIds = Common.convertArraytoString(selectedRows, 'ItemColumnId');
				SettingsService.ProcessContainerColumnsDelete(StateParameters.process, deleteIds);
				SettingsService.setDeletedRows('', 'fields');
				SettingsService.notifyDeletedRows('fields');
			}

			SettingsService.ProcessGroups(StateParameters.process, StateParameters.ProcessGroupId).then(function (phase) {
				let featuresToDelete = [];

				_.each(areFeatures, function(feature_){
					let index = _.findIndex(phase.SelectedFeatures, function(o){
						return o.Feature == feature_.Variable
					});

					let index2 = _.findIndex($scope.tabsAndFeaturesInOrder, function(x){
						return x.Variable == feature_.Variable
					});

					featuresToDelete.push(phase.SelectedFeatures[index]);
					$scope.tabsAndFeaturesInOrder.splice(index2, 1);

					phase.SelectedFeatures.splice(index,1);
					let returnedFeature = _.find($scope.fullFeatures, function(o) { return o.name ==  feature_.Variable; });
				    $scope.features.push(returnedFeature);
				});
					SettingsService.DeletePhaseFeatures(StateParameters.ProcessGroupId, featuresToDelete).then(function () {
						SettingsService.refreshProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId).then(function(newNavigation){
							$scope.tabsAndFeaturesInOrder = newNavigation;
							ensureNames();
							SettingsService.saveProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId,  $scope.tabsAndFeaturesInOrder);
						});
					})

			});
		};

		ViewService.footbar(StateParameters.ViewId, {
			template: 'settings/phases/phasesTabsFooter.html',
			scope: $scope
		});

		$scope.deleteTabs = function () {
			MessageService.confirm("DELETE_CONFIRMATION", deleteProcessTabs, 'warn');
		};

		$scope.header = {
			title: StateParameters.phaseName,
			menu: [
				{
					name: 'CONDITION',
					icon: 'lock',
					onClick: function () {
						ViewService.view('debug.tab', {
							params: StateParameters
						});
					}
				}
			]
		};
		StateParameters.activeTitle = "PROCESS_GROUPS";

		$scope.userLanguage = $rootScope.me.locale_id;
		var checkTranslationLength = function (string) {
			if (string[$scope.userLanguage]) {
				var translation = string[$scope.userLanguage];
				if (translation.length > 0) {
					return true;
				}
			}
		};

		$scope.addNewTab = function () {
			data = {
				LanguageTranslation: {}
			};
			let modifications = {
				create: false,
				cancel: true
			};

			let onChange = function () {
				if (data.LanguageTranslation) {
					modifications.create = checkTranslationLength(data.LanguageTranslation);
				}
			};

			let content = {
				buttons: [{ text: 'CANCEL', modify: 'cancel' }, { type: 'primary', onClick: $scope.addNewTabSave, text: 'SAVE', modify: 'create' }],
				inputs: [
					{ type: 'translation', label: 'PROCESS_GROUP_NAME', model: 'LanguageTranslation' },
				],
				title: 'ADD_NEW_TAB'
			};
			MessageService.dialog(content, data, onChange, modifications);
		};

		$scope.addNewTabSave = function () {

			let processGroupId = StateParameters.ProcessGroupId;

			let beforeOrderNo = 'U';
			let afterOrderNo = 'W';

			angular.forEach($scope.tabsAndFeaturesInOrder, function (tab) {
				if (angular.isUndefined(beforeOrderNo)) {
					beforeOrderNo = tab.OrderNo;
				}
				else if (beforeOrderNo < tab.OrderNo) {
					beforeOrderNo = tab.OrderNo;
				}
			});

			let max_order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);

			let ProcessTab = {
				OrderNo: max_order_no,
				ProcessTabId: 0,
				ProcessGroupId: processGroupId,
				LanguageTranslation: data.LanguageTranslation,
			};
			SettingsService.ProcessTabsAdd(StateParameters.process, ProcessTab).then(function (tab) {

				let processGroupTab = {
					ProcessGroupTabId: 0,
					ProcessGroup: {
						ProcessGroupId: processGroupId
					},
					ProcessTab: tab,
					OrderNo: tab.OrderNo,
				};
				saveAndSetTab(processGroupId, processGroupTab);
			});
		};

		let saveAndSetTab = function(processGroupId, processGroupTab){
			SettingsService.ProcessTabPhasesAdd(StateParameters.process, processGroupTab).then(function () {
				SettingsService.refreshProcessGroupNavigation(StateParameters.process, processGroupId).then(function(refreshedNavi){
					$scope.tabsAndFeaturesInOrder = refreshedNavi;
					ensureNames();
					SettingsService.saveProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId, $scope.tabsAndFeaturesInOrder);
					SettingsService.ProcessGroupTabs(StateParameters.process, processGroupId).then(function (tabs) {
						$scope.processGroupsTabs = [];
						angular.forEach(tabs, function (tab) {
							let process = tab;
							process.ProcessTabId = process.ProcessTab.ProcessTabId;
							$scope.processGroupsTabs.push(process);
							$scope.processTabs.push(process);
						});
						setPhaseOnTabs();
					});
				});
			});
		};

		$scope.copyTabSave = function () {

			let beforeOrderNo;
			let afterOrderNo;

			angular.forEach($scope.tabsAndFeaturesInOrder, function (tab) {
				if (angular.isUndefined(beforeOrderNo)) {
					beforeOrderNo = tab.OrderNo;
				}
				else if (beforeOrderNo < tab.OrderNo) {
					beforeOrderNo = tab.OrderNo;
				}
			});

			let max_order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);

			let parameters = {
				StateParameters: StateParameters,
				container_id: 0,
				process: StateParameters.process,
				max_order_no: max_order_no,
				ProcessGroupId: StateParameters.ProcessGroupId,
				ViewId: StateParameters.ViewId
			};

			let params = {
				template: 'settings/phases/phasesCopyTabDialog.html',
				controller: 'PhasesCopyTabController',
				locals: {
					DialogParameters: parameters,
					ProcessGroupTabs: $scope.processTabs,
					SaveAndSetTab: saveAndSetTab,
					SetProcessGroupsTabs: function (tabs) {
						$scope.processGroupsTabs = tabs;
					},
					ProcessTabId: $scope.processTabId
				}
			};
			MessageService.dialogAdvanced(params);

		};

		$scope.changeTab = function (ProcessTabId, index, ProcessTabGroupId) {
			$scope.selectedTabHighlight = index;
			$scope.ProcessTabId = ProcessTabId;
			setTimeout(function () {
				ViewService.viewAdvanced('settings.containers.tabs', { params: { process: StateParameters.process, ProcessTabId: ProcessTabId, type: 'phase', ProcessTabGroupId: ProcessTabGroupId } }, { targetId: 'containersView-' + ProcessTabId, ParentViewId: StateParameters.ViewId });
			}, 0);
		};
		if (StateParameters.OpenTab){
			$scope.changeTab(StateParameters.OpenTab, 0, StateParameters.ProcessTabGroupId);
		}

		$scope.changeButtons = function (ProcessTabId, index) {
			$scope.selectedTabHighlight = index;
			$scope.ProcessTabId = ProcessTabId;
			ViewService.view('settings.phases.tabsButtons', { params: { process: StateParameters.process, ProcessTabId: ProcessTabId } }, { split: true });
		};


		$scope.changeGroupTabValue = function (params) {

			changedGroupTabArray.push({ ProcessTabId: params.ProcessTabId, Type: params.Type });
			var noDuplicateObjects = {};
			var l = changedGroupTabArray.length;

			for (var i = 0; i < l; i++) {
				var item = changedGroupTabArray[i];
				noDuplicateObjects[item.ProcessTabId] = item;
			}

			var nonDuplicateArray = [];
			for (var item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedGroupTabs = nonDuplicateArray;
		};

		$scope.changeTag = function (tab) {
			if (changedTags.indexOf(tab.ProcessGroupTabId) === -1) {
				changedTags.push(tab.ProcessGroupTabId)
			}
		};

		SaveService.subscribeSave($scope, function () {
			angular.forEach(changedTags, function (value) {
				for (var i = 0, l = $scope.processGroupsTabs.length; i < l; i++) {
					if (value == $scope.processGroupsTabs[i].ProcessGroupTabId) {
						SettingsService.ProcessTabPhasesSave(StateParameters.process, $scope.processGroupsTabs[i]);
					}
				}
			});
			changedTags = [];
		});

		$scope.changeOrderGroupTab = function (current, prev, next) {
			var beforeOrderNo;
			var afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}

			var orderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			current.OrderNo = orderNo;

			if(current.Type == 0){
				current.ProcessTab.OrderNo = orderNo;
				$scope.changeGroupTabValue({ ProcessTabId: current.ProcessTab.ProcessTabId });
				saveTabs();
				setPhaseOnTabs();
				SettingsService.ProcessTabPhasesSave(StateParameters.process, current);
			}
			SettingsService.saveProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId, $scope.tabsAndFeaturesInOrder);
		};

		$scope.saveNavigationLevel = function(){

			_.each($scope.tabsAndFeaturesInOrder, function(tab){
				if(tab.TemplatePortfolioId == "true")tab.TemplatePortfolioId = null;
			})
			
			SettingsService.saveProcessGroupNavigation(StateParameters.process, StateParameters.ProcessGroupId, $scope.tabsAndFeaturesInOrder);
		}
	}

	PhasesButtonSettingsController.$inject = ['$scope', 'SaveService', 'SettingsService', 'MessageService', 'StateParameters', 'ProcessTabsButtons', 'Buttons', 'Common', 'Views', 'ProcessActions', 'ProcessActionDialogs'];
	function PhasesButtonSettingsController($scope, SaveService, SettingsService, MessageService, StateParameters, ProcessTabsButtons, Buttons, Common, Views, ProcessActions, ProcessActionDialogs) {
		$scope.tabButtons = ProcessTabsButtons;
		$scope.selectedRows = [];
		$scope.buttons = Buttons;
		var changedItems = [];
		var changedItemsArray = [];

		var deleteButtons = function () {
			var deleteIds = Common.convertArraytoString($scope.selectedRows, 'ProcessTabColumnId');
			SettingsService.ProcessTabsButtonsDelete(StateParameters.process, StateParameters.ProcessTabId, deleteIds);

			angular.forEach($scope.selectedRows, function (button) {
				var index = Common.getIndexOf($scope.tabButtons, button.ProcessTabColumnId, 'ProcessTabColumnId');
				$scope.tabButtons.splice(index, 1);
			});
			$scope.selectedRows = [];
		};

		$scope.header = {
			title: 'BUTTONS',
			menu: [
				{
					name: 'DELETE',
					onClick: function () {
						MessageService.confirm("DELETE_CONFIRMATION", deleteButtons)
					}
				}
			]
		};

		$scope.addButton = function () {
			var parameters = {
				stateParameters: StateParameters,
				container_id: 0,
				process: StateParameters.process
			};

			var params = {
				template: 'settings/phases/phasesAddButtonDialog.html',
				controller: 'PhasesAddButtonController',
				locals: {
					DialogParameters: parameters,
					Buttons: Buttons,
					tabButtons: $scope.tabButtons,
					Views: Views,
					ProcessActions: ProcessActions,
					ProcessActionDialogs: ProcessActionDialogs
				}
			};
			MessageService.dialogAdvanced(params);
		};

		SaveService.subscribeSave($scope, function () {
			if (changedItems.length > 0) {
				saveButtons();
			}
		});

		var saveButtons = function () {
			var processTabsArraySave = [];
			angular.forEach(changedItems, function (button) {
				var index = Common.getIndexOf($scope.tabButtons, button.ProcessTabColumnId, 'ProcessTabColumnId');
				processTabsArraySave.push($scope.tabButtons[index]);
			});

			SettingsService.ProcessTabsButtonsSave(StateParameters.process, StateParameters.ProcessTabId, processTabsArraySave);

			changedItems = [];
		};

		$scope.changeValue = function (params) {
			changedItemsArray.push({ ProcessTabColumnId: params.ProcessTabColumnId, ItemColumnId: params.ItemColumnId });
			var noDuplicateObjects = {};
			var l = changedItemsArray.length;

			for (var i = 0; i < l; i++) {
				var item = changedItemsArray[i];
				noDuplicateObjects[item.ProcessTabColumnId] = item;
			}

			var nonDuplicateArray = [];
			for (var item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};

		$scope.syncOrder = function (current, prev, next) {
			var beforeOrderNo;
			var afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}

			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			SettingsService.ProcessTabsButtonsSave(StateParameters.process, StateParameters.ProcessTabId, [current]);
		};

	}


	PhasesCopyTabController.$inject = ['$rootScope', '$scope', 'SettingsService', 'MessageService', 'DialogParameters', 'ProcessGroupTabs', 'ProcessTabId', 'SetProcessGroupsTabs', 'Translator', 'SaveAndSetTab'];
	function PhasesCopyTabController($rootScope, $scope, SettingsService, MessageService, DialogParameters, ProcessGroupTabs, ProcessTabId, SetProcessGroupsTabs, Translator, SaveAndSetTab) {
		$scope.ProcessTabId = ProcessTabId;
		$scope.disabled = true;
		$scope.ProcessGroupTabs = ProcessGroupTabs;
		$scope.clone = false;
		$scope.LanguageTranslation = {};
		$scope.changeValue = function () {
			$scope.disabled = true;

			if ($scope.clone == false) {
				if ($scope.ProcessTabId > 0) $scope.disabled = false;
			}
			else {
				if ($scope.ProcessTabId > 0 && angular.isDefined($scope.LanguageTranslation[$rootScope.me.locale_id]) && $scope.LanguageTranslation[$rootScope.me.locale_id] != '') $scope.disabled = false;
			}

			return $scope.disabled;
		};

		var combineMaintenanceNames = function () {
			angular.forEach($scope.ProcessGroupTabs, function (value, key) {
				if (typeof value.MaintenanceName == 'undefined' || value.MaintenanceName.length == 0) {
					value.MaintenanceName = Translator.translation(value.LanguageTranslation);
				} else {
					value.MaintenanceName = Translator.translation(value.LanguageTranslation) + " " + "'" + value.MaintenanceName + "'";
				}
			});
		};
		combineMaintenanceNames();

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.save = function () {
			if (!$scope.changeValue()) {
				if (!$scope.clone) {
					var processTabGroup = {
						ProcessGroupTabId: 0,
						ProcessGroup: {
							ProcessGroupId: DialogParameters.ProcessGroupId
						},
						ProcessTab: {
							ProcessTabId: $scope.ProcessTabId
						},
						OrderNo: DialogParameters.max_order_no
					};
					    SaveAndSetTab(DialogParameters.ProcessGroupId,processTabGroup);
							setTimeout(function ()  {
								MessageService.closeDialog();
							}, 200);

				}
				else {
					var ProcessTab = {
						OrderNo : DialogParameters.max_order_no,
						ProcessTabId : 0,
						ProcessGroupId : DialogParameters.ProcessGroupId,
						LanguageTranslation: $scope.LanguageTranslation,
						MaintenanceName: null
					};

					SettingsService.ProcessTabsAdd(DialogParameters.process, ProcessTab).then(function (tab) {
						var processTabGroup = {
							ProcessGroupTabId: 0,
							ProcessGroup: {
								ProcessGroupId: DialogParameters.ProcessGroupId
							},
							ProcessTab: tab,
							OrderNo: DialogParameters.max_order_no,
							Tag: '',
						};

							var newTab = {from_tab_id: $scope.ProcessTabId, to_group_id: DialogParameters.ProcessGroupId, new_tab_id: tab.ProcessTabId };
							SettingsService.ProcessGroupTabsCopy(DialogParameters.process, newTab).then(function () {
								SaveAndSetTab(DialogParameters.ProcessGroupId,processTabGroup);
									setTimeout(function ()  {
										MessageService.closeDialog();
									}, 200);
							});
					});
				}
			}
		}

	}

	PhasesAddButtonController.$inject = ['$rootScope', '$scope', 'SettingsService', 'Common', 'MessageService', 'DialogParameters', 'Buttons', 'tabButtons', 'Views', 'ProcessActions', 'ProcessActionDialogs'];
	function PhasesAddButtonController($rootScope, $scope, SettingsService, Common, MessageService, DialogParameters, Buttons, tabButtons, Views, ProcessActions, ProcessActionDialogs) {

		var tickTimeout;
		$scope.disabled = true;
		$scope.buttons = Buttons;
		$scope.tabButtons = tabButtons;

		$scope.views = Views;
		$scope.processActions = ProcessActions;
		$scope.processActionDialogs = ProcessActionDialogs;

		$scope.disabled = true;

		$scope.data = {};
		$scope.addNewButton = false;

		$scope.changeDialogValue = function (type) {
			if ($scope.addNewButton) {
				$scope.disabled = true;
				if (type == 'Translation') {
					$scope.data.Variable = Common.formatVariableName($scope.data.LanguageTranslation[$rootScope.me.locale_id]);
				}
				else if (type == 'Variable') {
					$scope.data.Variable = Common.formatVariableName($scope.data.Variable);
				}

				if (type == 'Translation' || type == 'Variable') {
					if ($scope.data.Variable.length > 50) {
						$scope.data.Variable = $scope.data.Variable.substr(0, 50);
					}

					clearTimeout(tickTimeout);
					tickTimeout = setTimeout(function () {
						SettingsService.checkDuplicateVariable('itemColumn', $scope.data.Variable, DialogParameters.process).then(function (data) {
							if (data != '') {
								$scope.data.Variable = data;
								$scope.disabled = false;
							}
							else
								$scope.disabled = 'in_use';
						});
					}, 500);
				}
			}
			else {
				if ($scope.data.ItemColumnId > 0) {
					$scope.disabled = false;
				}
			}
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		$scope.save = function () {
			if (!$scope.disabled) {
				var ItemColumn = {
					ProcessName: DialogParameters.stateParameters.process,
					ProcessContainerIds: [0],
					ItemColumnId: 0,
					LanguageTranslationRaw: $scope.data.LanguageTranslationRaw,
					Variable: $scope.data.Variable,
					DataType: 8
				};

				if ($scope.addNewButton) {
					SettingsService.ItemColumnsAdd(DialogParameters.stateParameters.process, ItemColumn).then(function (column) {
						column.DataAdditional2 = $scope.data.submitView + ";" + $scope.data.openView;
						column.ProcessActionId = $scope.data.ProcessActionId;
						column.ProcessDialogId = $scope.data.ProcessDialogId;
						$scope.buttons.push(column);

						SettingsService.ItemColumnsSave(DialogParameters.stateParameters.process, [column]).then(function () {
							addButton(column.ItemColumnId);

							setTimeout(function () {
								MessageService.closeDialog();
							}, 500);
						});

					});
				}
				else {
					addButton($scope.data.ItemColumnId);
				}

			}
		};

		var addButton = function (ItemColumnId) {

			var data = {};

			var beforeOrderNo;
			var afterOrderNo;

			angular.forEach($scope.tabButtons, function (key) {
				if (key.OrderNo > beforeOrderNo) {
					beforeOrderNo = key.OrderNo;
				}
			});

			data.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			data.ItemColumnId = ItemColumnId;

			SettingsService.ProcessTabsButtonsAdd(DialogParameters.stateParameters.process, DialogParameters.stateParameters.ProcessTabId, data).then(function (result) {
				$scope.tabButtons.push(result);
				setTimeout(function () {
					MessageService.closeDialog();
				}, 500);
			});

		}

		$scope.changeCheckbox = function () {
			$scope.disabled = true;
		}
	}


	PhasesMenuOrderController.$inject = ['$scope','SettingsService', 'StateParameters', 'Common', 'ProcessGroupTabs', 'Translator', 'Symbols', 'SaveService','ProcessGroup', 'Portfolios', '$rootScope'];
	function PhasesMenuOrderController($scope, SettingsService, StateParameters, Common, ProcessGroupTabs, Translator, Symbols, SaveService, ProcessGroup, Portfolios, $rootScope) {
		$scope.Group = ProcessGroup;
		$scope.ProcessGroupTabs = ProcessGroupTabs;
		$scope.edit = true;
		$scope.type = [{ name: 'FALSE', value: 0 }, { name: 'TRUE', value: 1 }, { name: 'NONE', value: 2 }];
		$scope.portfolios = Portfolios;
		$scope.userLanguage = $rootScope.clientInformation.locale_id;

		$scope.header = {
			title: 'PHASE_MENU_ORDER',
			menu: []
		};
		$scope.featureName = function (f) {
			return Translator.translate("FEATURE_" + f.toString().toUpperCase());
		};
		$scope.symbols = Symbols;

		$scope.syncOrder = function (current, prev, next) {
			var beforeOrderNo;
			var afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}

			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);

			SettingsService.saveProcessGroupNavigation(StateParameters.process, $scope.Group.ProcessGroupId, $scope.ProcessGroupTabs);
		};

		$scope.typeChanged = function () {
			changed = true;
			SaveService.tick();
		};

		var changed = false;
		SaveService.subscribeSave($scope, function() {
			if (!changed) {
				return;
			}

			SettingsService.ProcessGroupsSave(StateParameters.process, [$scope.Group]);

			changed = false;

			return SettingsService.saveProcessGroupNavigation(StateParameters.process, $scope.Group.ProcessGroupId, $scope.ProcessGroupTabs);
		});


		$scope.changedIcon = function() {
			changed = true;
			SaveService.tick();
		};
	}
})();
