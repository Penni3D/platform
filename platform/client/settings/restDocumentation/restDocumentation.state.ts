﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.config(RestDocumentationConfig)
		.controller('RestDocumentationController', RestDocumentationController);

	RestDocumentationConfig.$inject = ['ViewsProvider'];
	function RestDocumentationConfig(ViewsProvider) {
		ViewsProvider.addView('settings.restdocumentation', {
			controller: 'RestDocumentationController',
			template: 'settings/restDocumentation/restDocumentation.html',
			resolve: {
				RestData: function (SettingsService, StateParameters) {
					return SettingsService.GetRestDocs(StateParameters.process);
				}
			}
		});
	}

	RestDocumentationController.$inject = ['$scope', 'StateParameters', 'ViewService', 'RestData', 'SettingsService'];
	function RestDocumentationController($scope, StateParameters, ViewService, RestData, SettingsService) {
		$scope.header = { title: 'REST_DOCUMENTATION_TITLE' };

		$scope.restData = RestData;

		$scope.ParseJson = function (json) {
			return JSON.stringify(JSON.parse(json), null, 3);
		};

		ViewService.footbar(StateParameters.ViewId, {
			template: 'settings/restDocumentation/restDocumentationFooter.html',
			scope: $scope
		});

		$scope.exportAll = function () {
			SettingsService.GetAllRestDocs().then(function (restData) {
				let a = document.createElement("a");
				let file = new Blob([JSON.stringify(angular.copy(restData), null, '\t')], { type: 'plain/text' });
				a.href = URL.createObjectURL(file);
				a.download = 'REST_ALL.json';
				a.click();
			});
		};

		$scope.export = function () {
			let a = document.createElement("a");
			let file = new Blob([JSON.stringify(angular.copy($scope.restData), null, '\t')], { type: 'plain/text' });
			a.href = URL.createObjectURL(file);
			a.download = 'REST_' + StateParameters.process + '.json';
			a.click();
		};

		$scope.print = function () {

			alert('print');

		};
	}
})();