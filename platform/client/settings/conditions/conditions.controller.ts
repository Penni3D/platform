﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.controller('ConditionsSettingsController', ConditionsSettingsController)
		.controller('ConditionsSentenceController', ConditionsSentenceController)
		.controller('ConditionsRestrictionsController', ConditionsRestrictionsController)
		.controller('ConditionsFieldsSettingsController', ConditionsFieldsSettingsController);

	ConditionsSettingsController.$inject = ['$rootScope', '$scope', 'MessageService', 'SettingsService', 'Condition', 'GroupsProcessGroup',
		'LayoutProcessGroup', 'ProcessContainers', 'UserGroups', 'Common', 'ProcessPortfolios', 'SaveService', 'ViewService', 'StateParameters', '$filter', 'States', 'UiParentChildRelations', 'ConditionsGroups', 'Views', 'Translator'];

	function ConditionsSettingsController($rootScope, $scope, MessageService, SettingsService, Condition, GroupsProcessGroup,
	                                      LayoutProcessGroup, ProcessContainers, UserGroups, Common, ProcessPortfolios, SaveService, ViewService, StateParameters, $filter, States, UiParentChildRelations, ConditionsGroups, Views, Translator) {

		let userLanguage = $rootScope.clientInformation.locale_id;

		$scope.searchText = "";
		$scope.searchResults = [];
		//If elements are chosen from the footbar
		$scope.footBarSearch = false;
		//If sorting is chosen with the button
		$scope.sortMode = false;
		//If ordered alphabetically
		$scope.alphaSearch = false;
		$scope.inactiveStatus = {
			'phase': false,
			'tab': false,
			'container': false,
			'portfolio': false,
			'user_group': false
		};


		let elements = ['phase', 'tab', 'container', 'portfolio', 'user_group'];
		let chosenElements = [];
		$scope.toggleConditions = function (element) {
			// @ts-ignore
			if (!chosenElements.includes(element)) {
				chosenElements.push(element);
				$scope.inactiveStatus[element] = false;
			} else {
				$scope.inactiveStatus[element] = true;
				chosenElements.splice(_.findIndex(chosenElements, function (o) {
					return o == element;
				}, 1));
			}

			if (chosenElements.length == 0 || chosenElements.length == 5) {
				chosenElements = [];
				$scope.footBarSearch = false;
				$scope.alphaSearch = false;
				$scope.sortMode = false;
				$scope.inactiveStatus['phase'] = false;
				$scope.inactiveStatus['tab'] = false;
				$scope.inactiveStatus['container'] = false;
				$scope.inactiveStatus['portfolio'] = false;
				$scope.inactiveStatus['user_group'] = false;
				return;
			}

			if (chosenElements.length == 1) {
				_.each(elements, function (e) {
					if (e != chosenElements[0]) {
						$scope.inactiveStatus[e] = true;
					}
				})
			}
			$scope.searchConditions(chosenElements);
		}

		$scope.searchConditions = function (chosenElements) {
			$scope.searchResults = []
			if (!chosenElements) {
				//	$scope.sortMode = false;
				if ($scope.searchText.length == 0) $scope.alphaSearch = false;
				if ($scope.searchText.length < 3) return
				$scope.sortMode = false;
				_.each(Condition['conditionsArray'], function (condition) {
					_.each(condition.LanguageTranslation, function (translation) {
						if (translation.toLowerCase().includes($scope.searchText.toLowerCase()) && !$scope.searchResults.includes(condition)) {
							$scope.searchResults.push(condition)
						}
					})
				})
			} else {
				$scope.footBarSearch = true;
				$scope.sortMode = false;
				_.each(chosenElements, function (element) {
					switch (element) {
						case "phase":
							_.each(Condition['conditionsArray'], function (condition) {
								if (condition.ProcessGroupIds.length) $scope.searchResults.push(condition)
							})
							break;
						case "tab":
							_.each(Condition['conditionsArray'], function (condition) {
								if (condition.ProcessTabIds.length) $scope.searchResults.push(condition)
							})
							break;
						case "container":
							_.each(Condition['conditionsArray'], function (condition) {
								if (condition.ProcessContainerIds.length) $scope.searchResults.push(condition)
							})
							break;
						case "portfolio":
							_.each(Condition['conditionsArray'], function (condition) {
								if (condition.ProcessPortfolioIds.length) $scope.searchResults.push(condition)
							})
							break;
						case "user_group":
							_.each(Condition['conditionsArray'], function (condition) {
								if (condition.ItemIds.length) $scope.searchResults.push(condition)
							})
							break;
						default:
					}
				})

				if ($scope.alphaSearch) {
					$scope.searchResults = _.sortBy($scope.searchResults, [function (o) {
						if (o.LanguageTranslation[userLanguage]) {
							return o.LanguageTranslation[userLanguage].toUpperCase();
						}
					}]);
				}
			}
		}

		$scope.manualSortedGroups = {};

		$scope.conditionSortDialog = function () {
			$scope.footBarSearch = false;
			$scope.alphaSearch = false;
			let data = {'mode': ['phases']};
			let opts = [{name: Translator.translate('PROCESS_GROUPS'), value: 'phases'},
				{name: Translator.translate('FEATURES_F'), value: 'features'},
				{name: Translator.translate('STATES'), value: 'states'},
				{name: Translator.translate('ALPHABETICAL'), value: 'alphabetical'}];
			let content = {
				buttons: [
					{
						text: 'CANCEL',
						modify: 'cancel'

					},
					{
						type: 'primary',
						text: 'DO_SORTING',
						onClick: function () {
							sortConditions(data.mode);
						}
					}
				],
				inputs: [
					{
						type: 'select',
						options: opts,
						optionName: 'name',
						optionValue: 'value',
						model: 'mode',
						label: 'SELECT_SORT_MODE',
					},

				],
				title: 'SORT_CONDITIONS'
			};
			MessageService.dialog(content, data);
		}

		$scope.giveCustomGroupName = function (identifier) {
			if (identifier == 0) return Translator.translate("CONDITIONS_NOT_IN_GROUP");
			if (modeName == "phases") {
				let thisGroup = _.find(GroupsProcessGroup, function (o) {
					return o.ProcessGroupId == identifier;
				});
				if (thisGroup.LanguageTranslation[userLanguage]) return thisGroup.LanguageTranslation[userLanguage] + " (" + thisGroup.MaintenanceName + ")";
				else return "NO_TRANSLATION";
			} else if (modeName == "features") {
				let rValue = _.find($scope.features, function (f) {
					return f.name == identifier
				});

				if (typeof rValue != "undefined") return rValue.variable;
				return "";

				return Translator.translate(rValue)
			} else if (modeName == "states") {
				let rValue = _.find(Condition['stateTranslations'], function (s) {
					return s.stateName == identifier
				});
				if (typeof rValue != "undefined") return rValue.translation;
				return "";
			}
		}

		$scope.clearSortConditions = function () {
			$scope.sortMode = false;
			$scope.alphaSearch = false;
		}

		let modeName = "";
		let sortConditions = function (mode) {
			$scope.footBarSearch = false;
			$scope.searchText = "";
			$scope.sortMode = true;
			$scope.manualSortedGroups = {};
			modeName = mode[0];
			if (modeName == "features") {
				$scope.manualSortedGroups[0] = [];
				_.each(Condition['conditionsArray'], function (condition) {
					if (condition.Features.length) {
						_.each(condition.Features, function (feature) {
							if (!$scope.manualSortedGroups[feature]) $scope.manualSortedGroups[feature] = [];
							$scope.manualSortedGroups[feature].push(condition)
						})
					} else {
						$scope.manualSortedGroups[0].push(condition)
					}
				})
			} else if (modeName == "phases") {
				$scope.manualSortedGroups["0"] = [];
				_.each(UiParentChildRelations.data.tabIds, function (value, key) {
					$scope.manualSortedGroups[key] = [];
				})
				_.each(Condition['conditionsArray'], function (condition) {
					if (condition.ProcessGroupIds.length) {
						_.each(condition.ProcessGroupIds, function (phaseId) {
							$scope.manualSortedGroups[phaseId].push(condition)
						})
					} else {
						if (condition.ProcessTabIds.length) {
							_.each(condition.ProcessTabIds, function (tabId) {
								_.each(UiParentChildRelations.data.tabIds, function (tabIds, phaseId2) {
									_.each(tabIds, function (tabId2) {
										if (tabId == tabId2) $scope.manualSortedGroups[phaseId2].push(condition)
									})
								})
							})
						} else {
							_.each(condition.ProcessContainerIds, function (containerId) {
								let parentFound = false;
								_.each(UiParentChildRelations.data.containerIds, function (containerIds, tabId3) {
									_.each(containerIds, function (containerId2) {
										if (containerId == containerId2) {
											parentFound = true;
											_.each(UiParentChildRelations.data.tabIds, function (ti, key) {
												if (ti.includes(Number(tabId3)) && !$scope.manualSortedGroups[key].includes(condition)) {
													$scope.manualSortedGroups[key].push(condition)
												}
											})
										}
									})
								})
								if (!parentFound) {
									$scope.manualSortedGroups["0"].push(condition);
								}
							})
						}
					}
				})
			} else if (mode == "states") {
				$scope.manualSortedGroups[0] = [];
				_.each(Condition['conditionsArray'], function (condition) {
					if (condition.States.length) {
						_.each(condition.States, function (state) {
							if (!$scope.manualSortedGroups[state]) $scope.manualSortedGroups[state] = [];
							$scope.manualSortedGroups[state].push(condition)
						})
					} else {
						$scope.manualSortedGroups[0].push(condition)
					}
				})
			} else if (mode == "alphabetical") {
				$scope.alphaSearch = true;
				$scope.searchResults = _.sortBy(angular.copy(Condition['conditionsArray']), [function (o) {
					if (o.LanguageTranslation[userLanguage]) {
						return o.LanguageTranslation[userLanguage].toUpperCase();
					}
				}]);
			}
		}

		$scope.edit = true;
		$scope.conditions = Condition['conditions'];
		$scope.conditionsArray = Condition['conditionsArray'];
		$scope.conditionStates = Condition['states'];

		$scope.states = States;
		$scope.processGroups = GroupsProcessGroup;
		$scope.processTabs = $filter('orderBy')(LayoutProcessGroup, function (group) {
			return Translator.translation(group.LanguageTranslation)
		});
		$scope.processContainers = $filter('orderBy')(ProcessContainers, function (container) {
			return Translator.translation(container.LanguageTranslation)
		});
		$scope.processPortfolios = $filter('orderBy')(ProcessPortfolios, function (portfolios) {
			return Translator.translation(portfolios.LanguageTranslation)
		});
		$scope.userGroups = $filter('orderBy')(UserGroups, 'usergroup_name');
		$scope.conditionGroups = ConditionsGroups;
		$scope.conditionGroupsByName = {};

		_.each(ConditionsGroups, function (group) {
			$scope.conditionGroupsByName[group.ConditionGroupId] = group.LanguageTranslation[userLanguage];
		})


		$scope.selectedRows = [];

		let ids = UiParentChildRelations;

		$scope.fromList = StateParameters.FromList;

		$scope.view = 'meta';

		$scope.changeView = function (type) {
			$scope.view = type;
		};

		let newValuesArray = [];
		let changedItems = [];

		$scope.orderSave = function (changed) {
			_.each(changed, function (changedConditionGroup) {
				let c = _.find($scope.conditionGroups, function (sameGroup) {
					return sameGroup.ConditionGroupId == changedConditionGroup.id;
				})
				if (typeof c != 'undefined') {
					c.OrderNo = changedConditionGroup.orderNo
					changedGroups.push(c)
				}
			})
			SaveService.tick();
		}

		$scope.features = Views.getFeatures();
		$scope.sortCondition = function (condition, previous, next) {
			let order;
			if (typeof previous === 'undefined') {
				order = Common.getOrderNoBetween(undefined, next.OrderNo);

			} else if (typeof next === 'undefined') {
				order = Common.getOrderNoBetween(previous.OrderNo);

			} else {
				order = Common.getOrderNoBetween(previous.OrderNo, next.OrderNo);
			}
			condition.OrderNo = order;
			$scope.changeValue(condition);
			SaveService.tick();
		};

		SaveService.subscribeSave($scope, function () {
			if (changedItems.length > 0) saveCondition();
			if (changedGroups.length > 0) saveConditionGroups();
		});

		let checkTranslationLength = function (string) {
			if (string[userLanguage]) {
				let translation = string[userLanguage];
				if (translation.length > 0) return true;
			}
		};

		$scope.addCondition = function (id) {
			let newConditionData = {LanguageTranslation: null, ConditionGroupId: id};

			let save = function () {
				addConditionSave(newConditionData);
				newConditionData = {LanguageTranslation: null, ConditionGroupId: id};
			};

			let modifications = {
				create: false,
				cancel: true
			};

			let onChange = function () {
				if (newConditionData.LanguageTranslation) {
					modifications.create = checkTranslationLength(newConditionData.LanguageTranslation);
				}
			};

			let content = {
				buttons: [{text: 'DISCARD', modify: 'cancel'}, {
					type: 'primary',
					onClick: save,
					text: 'SAVE',
					modify: 'create'
				}],
				inputs: [{type: 'translation', label: 'CONDITION_NAME', model: 'LanguageTranslation'}],
				title: 'ADD_CONDITION'
			};

			MessageService.dialog(content, newConditionData, onChange, modifications)
		};

		let addConditionSave = function (d) {

			let condition = {
				LanguageTranslation: d.LanguageTranslation,
				Process: StateParameters.process,
				States: [],
				Features: [],
				ConditionId: 0,
				Translation: {},
				OrderNo: null,
				ConditionGroupId: 0
			};

			condition.Translation = Translator.translation(condition.LanguageTranslation);

			let beforeOrderNo;
			let afterOrderNo;

			angular.forEach($scope.conditions[d.ConditionGroupId], function (key) {
				if (beforeOrderNo < key.OrderNo || typeof beforeOrderNo === 'undefined')
					beforeOrderNo = key.OrderNo;
			});

			condition.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);

			condition.ConditionGroupId = d.ConditionGroupId;
			SettingsService.ConditionsAdd(StateParameters.process, condition).then(function (response) {
				if (angular.isUndefined($scope.conditions[response.ConditionGroupId])) $scope.conditions[response.ConditionGroupId] = [];
				$scope.conditions[response.ConditionGroupId].push(response);
				$scope.deleteButtonAllow(condition.ConditionGroupId);
				$scope.conditionsArray.push(response);
			});
			newValuesArray = [];
			changedItems = [];
		};

		$scope.addNewGroup = function () {
			let newGroupData = {LanguageTranslation: {}};

			let save = function () {
				addNewGroupSave(newGroupData);
				newGroupData = {LanguageTranslation: {}};
			};

			let modifications = {
				create: false,
				cancel: true
			};

			let onChange = function () {
				modifications.create = checkTranslationLength(newGroupData.LanguageTranslation);
			};
			let content = {
				buttons: [{text: 'DISCARD', modify: 'cancel'}, {
					type: 'primary',
					onClick: save,
					text: 'SAVE',
					modify: 'create'
				}],
				inputs: [{type: 'translation', label: 'CONDITION_NAME', model: 'LanguageTranslation'}],
				title: 'ADD_NEW_CONDITION_GROUP'
			};

			MessageService.dialog(content, newGroupData, onChange, modifications);
		};

		let addNewGroupSave = function (groupD) {
			let conditionGroup = {LanguageTranslation: groupD.LanguageTranslation};
			SettingsService.addConditionsGroups(StateParameters.process, conditionGroup).then(function (data) {
				data.CurrentTranslation = Translator.translation(data.LanguageTranslation);
				$scope.thisProcessGroups.push(data);
				$scope.conditionGroups.push(data);
			});
		};

		$scope.thisProcessGroups = [];
		$scope.thisProcessGroups.push($scope.conditionGroups[0]);


		let actualGroups = [];

		angular.forEach($scope.conditionGroups, function (value) {
			if (value.Process == StateParameters.process) {
				$scope.thisProcessGroups.push(value);
			}
			actualGroups = angular.copy($scope.thisProcessGroups);
		});

		let currentLanguageTranslations = [];

		let getConditionGroupTranslations = function (language) {
			angular.forEach($scope.thisProcessGroups, function (value, key) {
				currentLanguageTranslations.push(value.LanguageTranslation[language]);
			})
		};
		getConditionGroupTranslations(userLanguage);

		let getTranslatedConditionGroups = function () {
			angular.forEach($scope.thisProcessGroups, function (value, key) {
				for (let i = 0; i < currentLanguageTranslations.length; i++) {
					if (key == i) {
						value.CurrentTranslation = currentLanguageTranslations[i];
					}
				}
			});
		};
		getTranslatedConditionGroups();

		$scope.selectedGroupId = 0;
		$scope.deleteButtonAllow = function (groupid) {
			$scope.allowed = checkIfConditions(groupid);
			$scope.selectedGroupId = groupid;
		};

		let checkIfConditions = function (groupid) {
			if (($scope.conditions[groupid] && $scope.conditions[groupid].length == 0) && groupid != 0) {
				return false;
			} else if (!$scope.conditions[groupid] && groupid != 0) {
				return false;
			} else {
				return true;
			}
		};

		$scope.deleteConditionGroup = function (group) {
			let result = checkIfConditions(group.ConditionGroupId);
			if (result == false) {
				SettingsService.ConditionGroupDelete(group.ConditionGroupId).then(function () {
					angular.forEach($scope.thisProcessGroups, function (value, key) {
						if (group.ConditionGroupId == value.ConditionGroupId) {
							$scope.thisProcessGroups.splice(key, 1);
							$scope.selectedGroupId = 0;
							$scope["tabs-groupTabs"].switch(0);
						}
					});
				});
			} else {
				MessageService.msgBox("ERROR_CONDITION_GROUP_DELETE");
			}
		};

		$scope.copyCondition = function () {
			let copyConditionData = {LanguageTranslation: null, ConditionId: null, ConditionGroupId: null};

			let save = function () {
				copyConditionSave(copyConditionData);
				copyConditionData = {LanguageTranslation: null, ConditionId: null, ConditionGroupId: null};
			};
			let modifications = {
				create: false,
				cancel: true
			};
			let onChange = function () {
				modifications.create = checkTranslationLength(copyConditionData.LanguageTranslation);
			};
			let content = {
				buttons: [{text: 'DISCARD', modify: 'cancel'}, {
					type: 'primary',
					onClick: save,
					text: 'SAVE',
					modify: 'create'
				}],
				inputs: [{
					type: 'select',
					label: 'SELECT_CONDITION',
					model: 'ConditionId',
					options: $scope.conditionsArray,
					optionValue: 'ConditionId',
					optionName: 'LanguageTranslation',
					singleValue: true
				},
					{type: 'translation', label: 'CONDITION_NAME', model: 'LanguageTranslation'},
					{
						type: 'select',
						label: 'SELECT_CONDITION_GROUP',
						model: 'ConditionGroupId',
						options: $scope.thisProcessGroups,
						optionValue: 'ConditionGroupId',
						optionName: 'CurrentTranslation',
						singleValue: true
					}],
				title: 'COPY_CONDITION'
			};

			MessageService.dialog(content, copyConditionData, onChange, modifications);
		};

		$scope.moveCondition = function () {
			let movetronData = {LanguageTranslation: null, ConditionId: null, ConditionGroupId: null};

			let save = function () {
				movedConditionSave(movetronData);
				movetronData = {LanguageTranslation: null, ConditionId: null, ConditionGroupId: null};
			};

			let content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [{
					type: 'multiselect',
					label: 'SELECT_CONDITION',
					model: 'ConditionId',
					options: $scope.conditionsArray,
					optionValue: 'ConditionId',
					optionName: 'LanguageTranslation',
					singleValue: true
				},
					{
						type: 'select',
						label: 'SELECT_CONDITION_GROUP',
						model: 'ConditionGroupId',
						options: $scope.thisProcessGroups,
						optionValue: 'ConditionGroupId',
						optionName: 'CurrentTranslation',
						singleValue: true
					},
				],
				title: 'MOVE_CONDITION'
			};

			MessageService.dialog(content, movetronData);
		};


		let copyConditionSave = function (copyData) {

			let condition: Client.Condition = angular.copy(_.find($scope.conditionsArray, {ConditionId: copyData.ConditionId}));

			condition.Variable = null;
			condition.ConditionId = 0;
			condition.LanguageTranslation = copyData.LanguageTranslation;
			condition.Translation = condition.LanguageTranslation[$rootScope.me.locale_id];
			condition.ConditionGroupId = copyData.ConditionGroupId;

			let beforeOrderNo;
			let afterOrderNo;

			angular.forEach($scope.conditions[copyData.ConditionGroupId], function (key) {
				if (beforeOrderNo < key.OrderNo || typeof beforeOrderNo === 'undefined')
					beforeOrderNo = key.OrderNo;
			});

			condition.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);

			SettingsService.ConditionsCopy(StateParameters.process, condition).then(function (respond) {

				if (angular.isUndefined($scope.conditions[respond.ConditionGroupId])) $scope.conditions[respond.ConditionGroupId] = [];
				$scope.conditions[respond.ConditionGroupId].push(respond);
				$scope.conditionsArray.push(respond);
				$scope.changeFeatureValue({
					ConditionId: respond.ConditionId,
					index: _.findIndex($scope.conditions[respond.ConditionGroupId], {ConditionId: respond.ConditionId}),
					features: respond.Features,
					ConditionGroupId: respond.ConditionGroupId
				});
			});
		};

		let movedConditionSave = function (movingData) {

			let conditionsForNewTab = [];
			let index;
			let conditionKey;

			for (let i = 0; i < movingData.ConditionId.length; i++) {
				let condition: Client.Condition = (_.find($scope.conditionsArray, {ConditionId: movingData.ConditionId[i]}));
				angular.forEach($scope.conditions, function (value) {
					angular.forEach(value, function (value2, key2) {
						if (value2.ConditionId == movingData.ConditionId[i]) {
							index = value2.ConditionGroupId;
							conditionKey = key2;
							$scope.conditions[index].splice(conditionKey, 1);
							$scope.deleteButtonAllow(value2.ConditionGroupId);
						}
					});
				});

				if ($scope.conditions[movingData.ConditionGroupId]) {
					$scope.conditions[movingData.ConditionGroupId].push(condition);
					$scope.conditions[movingData.ConditionGroupId] = _.uniq($scope.conditions[movingData.ConditionGroupId]);
				} else {
					let keyToAdded = {};
					keyToAdded[movingData.ConditionGroupId] = [condition];
					_.extend($scope.conditions, keyToAdded);
					$scope.conditions[movingData.ConditionGroupId] = _.uniq($scope.conditions[movingData.ConditionGroupId]);
				}
				condition.ConditionGroupId = movingData.ConditionGroupId;
				conditionsForNewTab.push(condition);
			}

			SettingsService.NewConditionGroups(StateParameters.process, conditionsForNewTab);
			SettingsService.ConditionsSave(StateParameters.process, conditionsForNewTab);
		};

		let saveCondition = function () {
			let conditionArray = [];
			angular.forEach(changedItems, function (item) {
				let index = Common.getIndexOf($scope.conditions[item.ConditionGroupId], item.ConditionId, 'ConditionId');
				$scope.conditions[item.ConditionGroupId][index].Process = StateParameters.process;
				conditionArray.push($scope.conditions[item.ConditionGroupId][index]);
			});
			SettingsService.ConditionsSave(StateParameters.process, conditionArray);
			newValuesArray = [];
			changedItems = [];
		};

		let setConditionsArray = function (row) {
			angular.forEach($scope.conditionsArray, function (value, key) {
				if (row.id == value.ConditionId) {
					$scope.conditionsArray.splice(key, 1);
				}
			});
		};

		$scope.deleteCondition = function () {
			MessageService.confirm("CONDITION_DELETE_ROWS", function () {
				let deleteIds = Common.convertArraytoString($scope.selectedRows, 'id');

				SettingsService.ConditionsDelete(StateParameters.process, deleteIds).then(function () {

					angular.forEach($scope.selectedRows, function (row) {
						setConditionsArray(row);
						var index = Common.getIndexOf($scope.conditions[row.ConditionGroupId], row.id, 'ConditionId');
						$scope.conditions[row.ConditionGroupId].splice(index, 1);
						$scope.deleteButtonAllow(row.ConditionGroupId);
					});
					$scope.selectedRows = [];
				});
			}, 'warn');
		};
		$scope.parseComma = function (text, index, len) {
			//if (index === len - 1) return text;
			return text;
		};

		$scope.header = {
			title: 'PROCESS_CONDITIONS_PAGE_TITLE',
			menu: [],
			icons: [
				{
					icon: 'settings', onClick: function () {
						let mm = [];
						mm.push({
							name: Translator.translate('COPY_CONDITION'),
							onClick: function () {
								$scope.copyCondition();
							}
						});

						mm.push({
							name: Translator.translate('MOVE_CONDITION'),
							onClick: function () {
								$scope.moveCondition();
							}
						});

						mm.push({
							name: Translator.translate('ADD_NEW_CONDITION_GROUP'),
							onClick: function () {
								$scope.addNewGroup();
							}
						});

						MessageService.menu(mm, event.currentTarget);
					},
				}
			]
		};
		StateParameters.activeTitle = $scope.header.title;

		ViewService.footbar(StateParameters.ViewId, {
			template: 'settings/conditions/conditionsFooter.html',
			scope: $scope
		});

		$scope.showCondition = function (condition, conditionId, conditionGroupId, langvariable) {
			let params = StateParameters;
			let userLanguageTranslation = Translator.translation(langvariable);
			params.conditionId = conditionId;
			params.ConditionGroupId = conditionGroupId;
			params.ConditionTranslation = userLanguageTranslation;
			params.Test = $scope.tabTranslationIdMatches;
			params.Condition = condition;
			let rootCollection = $scope.searchResults.length > 0 ? $scope.searchResults : $scope.conditions[conditionGroupId];
			$scope.selectedCondition = _.findIndex(rootCollection, function(o) { return o.ConditionId == condition.ConditionId; });

			ViewService.view('settings.conditions.fields', {
				params: params,
				locals: {
					NewCondition: condition,
					ConditionTabTranslations: $scope.tabTranslationIdMatches,
					ConditionContainerTranslations: $scope.containerTranslationIdMatches
				}
			}, {split: true, remember: false});
		};

		$scope.changeValue = function (params) {
			if (params.Phase == true) {
				let tabIds = calculatePhaseTabs(params.Condition);
				let conditionTabs = params.Condition.ProcessTabIds;
				params.Condition.ProcessTabIds = _.uniq(savedTabs(tabIds, conditionTabs));

				let containerIds = calculateTabContainers(params.Condition);
				let conditionContainers = params.Condition.ProcessContainerIds;
				params.Condition.ProcessContainerIds = _.uniq(savedContainers(containerIds, conditionContainers));
			}

			newValuesArray.push(params);
			let noDuplicateObjects = {};
			let l = newValuesArray.length;

			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.ConditionId] = item;
			}

			let nonDuplicateArray = [];
			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;

			angular.forEach(changedItems, function (item) {
				let index = Common.getIndexOf($scope.conditions[item.ConditionGroupId], item.ConditionId, 'ConditionId');
				$scope.conditions[item.ConditionGroupId][index].Process = StateParameters.process;
			});
		};

		let calculatePhaseTabs = function (array) {
			if (array.ProcessGroupIds.length == 0) {
				return array.ProcessTabIds;
			} else {
				let tabs = [];
				angular.forEach(ids.data.tabIds, function (value, key) {
					for (let i = 0; i < array.ProcessGroupIds.length; i++) {
						if (array.ProcessGroupIds[i] == key && !_.includes(array.ProcessGroupIds, key)) {
							angular.forEach(value, function (value2) {
								tabs.push(value2);
							});
						}
					}
				});
				return tabs;
			}
		};

		let calculateTabContainers = function (array) {
			if (array.ProcessGroupIds.length == 0) {
				return array.ProcessContainerIds;
			} else {
				let containers = [];
				angular.forEach(ids.data.containerIds, function (value, key) {
					for (let i = 0; i < array.ProcessTabIds.length; i++) {
						if (array.ProcessTabIds[i] == key && !_.includes(array.ProcessTabIds, key)) {
							angular.forEach(value, function (value2) {
								containers.push(value2);
							});
						}
					}
				});
				return containers;
			}
		};

		let savedTabs = function (tabids, conditiontabs) {
			if (conditiontabs == null) {
				conditiontabs = [];
			}
			let idArray = [];
			for (let i = 0; i < tabids.length; i++) {
				for (let j = 0; j < conditiontabs.length; j++) {
					if (tabids[i] == conditiontabs[j]) {
						idArray.push(tabids[i]);
					}
				}
			}
			return idArray;
		};

		let savedContainers = function (containerids, conditioncontainers) {
			if (conditioncontainers == null) {
				conditioncontainers = [];
			}
			let containerArray = [];
			for (let i = 0; i < containerids.length; i++) {
				for (let j = 0; j < conditioncontainers.length; j++) {
					if (containerids[i] == conditioncontainers[j]) {
						containerArray.push(containerids[i]);
					}
				}
			}
			return containerArray;
		};

		$scope.changeFeatureValue = function (params) {
			$scope.conditionStates[params.ConditionId] = [];
			let stateList = [];
			angular.forEach(params.features, function (feature) {
				angular.forEach($scope.states[feature], function (state) {
					stateList.push({
						stateName: state,
						translation: Translator.translate("CONDITION_" + state.toString().toUpperCase().replace(feature.toUpperCase() + ".", "")) + " (" + Translator.translate("FEATURE_" + feature.toUpperCase()) + ")"
					});
				});
			});

			$scope.conditionStates[params.ConditionId] = stateList;
			$scope.changeValue(params);
			SaveService.tick();
		};

		let combineMaintenanceNames = function (items) {
			angular.forEach(items, function (value) {
				if (value.MaintenanceName.length == 0) {
					value.MaintenanceName = Translator.translation(value.LanguageTranslation);
				} else {
					value.MaintenanceName = Translator.translation(value.LanguageTranslation) + ' ' + '\'' + value.MaintenanceName + '\'';
				}
			});
			return items;
		};

		$scope.processGroups = combineMaintenanceNames($scope.processGroups);
		$scope.processTabs = combineMaintenanceNames($scope.processTabs);
		$scope.processContainers = combineMaintenanceNames($scope.processContainers);

		$scope.tabTranslationIdMatches = {};
		let matchTabIdTranslation = function () {
			angular.forEach($scope.processTabs, function (tab) {
				$scope.tabTranslationIdMatches[tab.ProcessTabId] = tab.MaintenanceName;
			});
		};
		matchTabIdTranslation();

		$scope.containerTranslationIdMatches = {};
		let matchContainerIdTranslations = function () {
			angular.forEach($scope.processContainers, function (container) {
				$scope.containerTranslationIdMatches[container.ProcessContainerId] = container.MaintenanceName;
			});
		};
		matchContainerIdTranslations();


		$scope.groupTranslationIdMatches = {};
		let matchGroupIdTranslations = function () {
			angular.forEach($scope.processGroups, function (group) {
				$scope.groupTranslationIdMatches[group.ProcessGroupId] = group.MaintenanceName;
			});
		};
		matchGroupIdTranslations();


		$scope.portfolioTranslationIdMatches = {};
		let matchPortfolioIdTranslations = function () {
			angular.forEach($scope.processPortfolios, function (portfolio) {
				$scope.portfolioTranslationIdMatches[portfolio.ProcessPortfolioId] = portfolio.LanguageTranslation[userLanguage];
			});
		};
		matchPortfolioIdTranslations();


		$scope.userGroupTranslationIdMatches = {};
		let matchUserGroupIdTranslations = function () {
			angular.forEach($scope.userGroups, function (userGroup) {
				$scope.userGroupTranslationIdMatches[userGroup.item_id] = userGroup.usergroup_name;
			});
		};
		matchUserGroupIdTranslations();


		$scope.toggleCondition = function (condition, conditionGroupId) {
			condition.Disabled = !condition.Disabled;
			let params = {
				ConditionId: condition.ConditionId,
				ConditionGroupId: conditionGroupId
			};
			$scope.changeValue(params);
			SaveService.tick();
		};

		$scope.openDialog = function (conditionId, condition, index, parent_index, conditionGroupId) {
			let params = {
				template: 'settings/conditions/conditionsRestrictions.html',
				controller: 'ConditionsRestrictionsController',
				locals: {
					ConditionId: conditionId,
					Process: StateParameters.process,
					Condition: condition,
					Index: index,
					Parent_Index: parent_index,
					ProcessGroups: $scope.processGroups,
					ProcessTabs: $scope.processTabs,
					ProcessContainers: $scope.processContainers,
					ProcessPortfolios: $scope.processPortfolios,
					TabsForPhases: $scope.tabsForPhases,
					UserGroups: $scope.userGroups,
					UiParentChildRelations: ids,
					StateParameters: StateParameters,
					Conditions: $scope.conditions,
					ConditionGroupId: conditionGroupId,
					CalculatePhaseTabs: calculatePhaseTabs,
					CalculatePhaseContainers: calculateTabContainers,
					SavedTabs: savedTabs
				}
			};
			MessageService.dialogAdvanced(params);
		};

		$scope.syncOrder = function (prev, next, current, ConditionGroupId) {
			let beforeOrderNo;
			let afterOrderNo;

			if (!angular.isUndefined(prev.index)) {
				beforeOrderNo = $scope.conditions[prev.parent_index][prev.index].OrderNo;
			}

			if (!angular.isUndefined(next.index)) {
				afterOrderNo = $scope.conditions[next.parent_index][next.index].OrderNo;
			}

			$scope.conditions[current.parent_index][current.index].OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			let condition = $scope.conditions[current.parent_index][current.index];
			condition.ConditionGroupId = ConditionGroupId;
			SettingsService.ConditionsSave(StateParameters.process, [condition]);
		};

		let changedGroups = [];

		$scope.changeGroupName = function () {
			let gid = Common.getIndexOf($scope.thisProcessGroups, $scope.selectedGroupId, "ConditionGroupId");
			let groupData = {
				LanguageTranslation: $scope.thisProcessGroups[gid].LanguageTranslation,
				Group: $scope.thisProcessGroups[gid],
				ConditionGroupId: $scope.selectedGroupId
			};

			let contentButtons = [
				{text: 'CANCEL'},
				{
					type: 'primary',
					onClick: function () {
						groupData.Group.LanguageTranslation = groupData.LanguageTranslation;
						if (changedGroups[groupData.ConditionGroupId]) {
							changedGroups[groupData.ConditionGroupId] = groupData.Group;
						} else {
							changedGroups.push(groupData.Group);
						}
						SaveService.tick();
					},
					text: 'SAVE'
				}
			];

			if ($scope.selectedGroupId != 0) {
				contentButtons.unshift({
					text: "DELETE",
					type: "warn",
					onClick: function () {
						$scope.deleteConditionGroup(groupData.Group);
					}
				});
			}

			let content = {
				buttons: contentButtons,
				inputs: [
					{type: 'translation', label: 'CONDITION_GROUP_NAME', model: 'LanguageTranslation'}
				],
				title: "CHANGE_CONDITION_GROUP"
			};
			MessageService.dialog(content, groupData);
		};

		let saveConditionGroups = function () {
			SettingsService.saveConditionsGroups(StateParameters.process, changedGroups).then(function () {
				changedGroups = [];
			});
		}
	}

	ConditionsSentenceController.$inject = ['$scope',
		'MessageService',
		'SettingsService',
		'Condition',
		'ItemColumns',
		'UserColumns',
		'Operators',
		'SaveService',
		'ProcessSubprocesses',
		'StateParameters',
		'Common',
		'Translator',
		'NewCondition',
		'GroupsProcessGroup',
		'UserGroups',
		'ProcessPortfolios',
		'$rootScope',
		'ConditionUsages',
		'ProcessActions',
		'OtherProcessesConditions',
		'NotificationConditions',
		'ConditionTabTranslations',
		'ConditionContainerTranslations',
		'Processes',
		'ViewService',
		'ConditionResultService'
	];

	function ConditionsSentenceController($scope,
	                                      MessageService,
	                                      SettingsService,
	                                      Condition,
	                                      ItemColumns,
	                                      UserColumns,
	                                      Operators,
	                                      SaveService,
	                                      ProcessSubprocesses,
	                                      StateParameters,
	                                      Common,
	                                      Translator,
	                                      NewCondition,
	                                      GroupsProcessGroup,
	                                      UserGroups,
	                                      ProcessPortfolios,
	                                      $rootScope,
	                                      ConditionUsages,
	                                      ProcessActions,
	                                      OtherProcessesConditions,
	                                      NotificationConditions,
	                                      ConditionTabTranslations,
	                                      ConditionContainerTranslations,
	                                      Processes,
	                                      ViewService,
	                                      ConditionResultService) {

		$scope.condition = NewCondition;
		$scope.conditionGroupId = StateParameters.ConditionGroupId;
		$scope.conditionTabTranslations = ConditionTabTranslations;
		$scope.conditionContainerTranslations = ConditionContainerTranslations;

		let userLanguage = $rootScope.me.locale_id;

		if (!angular.isUndefined(Condition.ItemColumns)) {
			$scope.itemColumnsParent = Condition.ItemColumns;
		} else {
			$scope.itemColumnsParent = [];
		}

		$scope.itemColumns = [];
		$scope.conditionUsages = ConditionUsages;

		let datatypes = [0, 1, 2, 3, 5, 6, 13, 16];

		_.each(ItemColumns.columns, function (item) {
			if (_.includes(datatypes, item.DataType)) {
				$scope.itemColumns.push(item);
			}
		});

		let cache = ConditionResultService.getResultCache();


		$scope.giveConditionName = function (conditionId, process) {
			let conditionArray = process != ""
				? OtherProcessesConditions
				: Condition.condition;
			let p = _.find(Processes, function (p2) {
				return p2.ProcessName == process;
			})
			let condition = _.find(conditionArray, function (o) {
				return o.ConditionId == conditionId;
			})

			let processTranslation = "";

			if (p) {
				processTranslation = p.LanguageTranslation[userLanguage]
					? p.LanguageTranslation[userLanguage]
					: p.Variable;
				processTranslation = " (" + processTranslation + ")";
			}
			return Translator.translate(condition.Variable) + " " + processTranslation;
		}

		$scope.giveActionName = function (actionId) {
			let action = _.find(ProcessActions, function (a) {
				return a.ProcessActionId == actionId;
			});
			return Translator.translate(action.Variable);
		}

		$scope.giveNotificationConditionName = function (nConditionId) {
			let nc = _.find(NotificationConditions, function (n) {
				return n.NotificationConditionId == nConditionId;
			});
			return Translator.translate(nc.Variable);
		}

		$scope.conditionResults = [{'value': 1, 'name': Translator.translate('CONDITION_TRUE')},
			{'value': 0, 'name': Translator.translate('CONDITION_FALSE')},
			{'value': 2, 'name': ''}]

		$scope.activeResult = $scope.conditionResults[2];
		$scope.evaluationText = Translator.translate("CONDITION_NOT_EVALUATED")

		let evaluateConditionStraight = function () {
			if ((cache['UserItemId'] == 0 && cache['ItemId'] == 0) || StateParameters.process != cache['Process'] || cache['ItemId'].length == 0) return;
			ConditionResultService.getConditionResult(StateParameters.process, NewCondition.ConditionId, cache['ItemId'], cache['UserItemId']).then(function (result) {
				$scope.evaluationText = Translator.translate("CONDITION_IS_EVALUATED")
				if (result == "True") {
					$scope.activeResult = $scope.conditionResults[0];
				} else {
					$scope.activeResult = $scope.conditionResults[1];
				}
			})
		}

		evaluateConditionStraight();

		$scope.evaluateCondition = function () {
			let evaluateData = {
				'user': cache['UserItemId'].length ? cache['UserItemId'] : [$rootScope.me.item_id],
				'item_id': cache['ItemId'].length && cache['Process'] == StateParameters.process ? cache['ItemId'] : ""
			};
			let content = {
				buttons: [
					{
						text: 'CANCEL',
						modify: 'cancel'

					},
					{
						type: 'primary',
						modify: 'create',
						text: 'EVALUATE_CONDITION',
						onClick: function () {
							if (evaluateData.user.length == 0 || evaluateData.item_id.length == 0) {
								$scope.activeResult = $scope.conditionResults[2];
								$scope.evaluationText = Translator.translate("CONDITION_NOT_EVALUATED")
								return;
							}

							ConditionResultService.getConditionResult(StateParameters.process, NewCondition.ConditionId, evaluateData.item_id, evaluateData.user).then(function (result) {
								$scope.evaluationText = Translator.translate("CONDITION_IS_EVALUATED")
								ConditionResultService.setResultCache(evaluateData.user, evaluateData.item_id, StateParameters.process);
								if (result == "True") {
									$scope.activeResult = $scope.conditionResults[0];
								} else {
									$scope.activeResult = $scope.conditionResults[1];
								}
							})
						}
					}
				],
				inputs: [
					{
						type: 'process',
						process: StateParameters.process,
						model: 'item_id',
						label: 'EVALUATED_ROW',
						max: 1
					},
					{
						type: 'process',
						process: 'user',
						model: 'user',
						label: 'EVALUATED_USER',
						max: 1
					},
				],
				title: 'EVALUATE_CONDITION'
			};
			MessageService.dialog(content, evaluateData);
		}

		$scope.disableConditionGroup = function (group) {
			group.Disabled = !group.Disabled;
			newValuesArray.push(group);
			changedItems = newValuesArray;
			SaveService.tick();

		};

		$scope.translations = {
			'UserGroups': {},
			'Phases': {},
			'Portfolios': {}
		};

		_.each(NewCondition.ItemIds, function (userGroupId) {
			_.each(UserGroups, function (ug) {
				if (userGroupId == ug.item_id) {
					$scope.translations['UserGroups'][userGroupId] = ug.usergroup_name;
				}
			});
		});

		_.each(NewCondition.ProcessPortfolioIds, function (portfolioId) {
			_.each(ProcessPortfolios, function (portfolio) {
				if (portfolio.ProcessPortfolioId == portfolioId) {
					$scope.translations['Portfolios'][portfolioId] = portfolio.LanguageTranslation[userLanguage];
				}
			});
		});

		_.each(NewCondition.ProcessGroupIds, function (phaseId) {
			_.each(GroupsProcessGroup, function (phase) {
				if (phase.ProcessGroupId == phaseId) {
					$scope.translations['Phases'][phaseId] = phase.LanguageTranslation[userLanguage] + ' \' ' + phase.MaintenanceName + ' \' ';
				}
			});
		});

		$scope.list = ItemColumns.lists;
		$scope.list2 = ItemColumns.lists2;
		$scope.listUserColumns = ItemColumns.listUserColumns;
		$scope.listUserColumns2 = ItemColumns.listUserColumns;
		$scope.varcharOptions = ItemColumns.varcharOptions;
		$scope.processOptions = ItemColumns.processOptions;
		$scope.listOptions = ItemColumns.listOptions;
		$scope.subqueryListOptions = ItemColumns.subqueryListOptions;

		$scope.conditions = Condition.condition;
		$scope.edit = true;
		$scope.operators = Operators;
		$scope.processSubprocesses = ProcessSubprocesses;

		$scope.selectedFields = [];
		$scope.conditionTypes = [];
		$scope.conditionTypes.push({ConditionTypeId: 1, ConditionTypeName: 'CONDITION'});
		$scope.conditionTypes.push({ConditionTypeId: 2, ConditionTypeName: 'EXISTING_CONDITION'});
		$scope.conditionTypes.push({ConditionTypeId: 3, ConditionTypeName: 'SIGNED_IN_USER_CONDITION'});
		$scope.conditionTypes.push({ConditionTypeId: 4, ConditionTypeName: 'PARENT_PROCESS_CONDITION'});
		$scope.conditionTypes.push({ConditionTypeId: 5, ConditionTypeName: 'PARENT_USER_CONDITION'});
		//	$scope.conditionTypes.push({ ConditionTypeId: 6, ConditionTypeName: 'List user condition' });
		$scope.conditionTypes.push({ConditionTypeId: 7, ConditionTypeName: 'DATE_DIFF_CONDITION'});
		$scope.conditionTypes.push({ConditionTypeId: 8, ConditionTypeName: 'PARENT_PROCESS_EXISTING_CONDITION'});

		$scope.listColumns = [];
		$scope.dateColumns = [];

		angular.forEach(ItemColumns.columns, function (key) {

			if (key['DataType'] == 6) {
				$scope.listColumns.push(key);
			} else if (key['DataType'] == 3) {
				$scope.dateColumns.push(key);

			}
		});
		$scope.userColumns = UserColumns;
		$scope.listUserColumns = Condition.ListColumns;

		let newValuesArray = [];
		let changedItems = [];

		let saveConditionFields = function () {
			let conditionArray = [];
			$scope.condition.Process = StateParameters.process;
			conditionArray.push($scope.condition);
			return SettingsService.ConditionsSave(StateParameters.process, conditionArray);
		};

		$scope.parseComma = function (text, index, len) {
			if (index === len - 1) return text;
			return text + ', ';
		};

		SaveService.subscribeSave($scope, function () {
			if (changedItems.length > 0) {
				return saveConditionFields();
			}
		});
		$scope.deleteConditionFields = function () {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				$scope.selectedFields = _.sortBy($scope.selectedFields, 'index');
				_.reverse($scope.selectedFields);
				angular.forEach($scope.selectedFields, function (value) {
					$scope.condition.ConditionJSON[value.parent_index].OperationGroup.Operations.splice(value.index, 1);

					if ($scope.condition.ConditionJSON[value.parent_index].OperationGroup.Operations.length === 0) {
						$scope.condition.ConditionJSON.splice(value.parent_index, 1);
					}
					if ($scope.condition.ConditionJSON.length === 0) {
						$scope.condition.ConditionJSON = null;
					}
				});
				$scope.selectedFields = [];
				saveConditionFields();
			});
		};

		let headerText = Translator.translate('PROCESS_CONDITIONS_PAGE_TITLE') + ' - ' + StateParameters.ConditionTranslation;
		$scope.header = {
			title: headerText,
			menu: []
		};
		ViewService.footbar(StateParameters.ViewId, {
			template: 'settings/conditions/conditionsFieldsFooter.html',
			scope: $scope
		});

		$scope.addOperation = function (operations, index, doNotOpenDialog) {
			var operation = {};
			operations.push(operation);
			if (!doNotOpenDialog) {
				$scope.openDialog(null, null, operations.length - 1, index);
			}
		};

		$scope.addCondition = function () {
			let condition = {OperatorId: 1, OperationGroup: {Operations: []}};
			if ($scope.condition.ConditionJSON == null) {
				$scope.condition.ConditionJSON = [];
			}
			$scope.condition.ConditionJSON.push(condition);
		};

		$scope.deleteCondition = function (condition) {
			MessageService.confirm('DELETE_CONFIRMATION', function () {
				$scope.condition.ConditionJSON.splice($scope.condition.ConditionJSON.indexOf(condition), 1);
				if ($scope.condition.ConditionJSON.length === 0) {
					$scope.condition.ConditionJSON = null;
				}
				saveConditionFields();
			});
		};

		$scope.changeValue = function (params) {
			if (!angular.isUndefined(params)) {
				if (!angular.isUndefined(params['process'])) {
					if (angular.isUndefined($scope.itemColumnsParent[params.process])) {
						if (params.process.length > 0) {
							SettingsService.ItemColumns(params.process).then(function (columns) {
								$scope.itemColumnsParent[params.process] = columns;
							});
						}
					}
				}
				if (!angular.isUndefined(params['itemColumnId']) && angular.isUndefined(params['type'])) {
					if (angular.isUndefined($scope.listUserColumns[params.itemColumnId])) {
						angular.forEach($scope.itemColumns, function (key) {
							if (key['ItemColumnId'] == params.itemColumnId) {
								if (key.DataAdditional != null && key.DataAdditional.length > 0) {
									params.condition.listProcess = key.DataAdditional;
									SettingsService.ItemColumns(key.DataAdditional).then(function (columns) {
										var cols = [];
										angular.forEach(columns, function (key2) {
											if (key2.DataType == 5 && key2.DataAdditional == 'user') {
												cols.push(key2);
											}
										});
										$scope.listUserColumns[key.ItemColumnId] = cols;
									});
								}
							}
						});
					}
				} else if (!angular.isUndefined(params['itemColumnId']) && angular.isDefined(params['type'])) {
					var index = Common.getIndexOf($scope.itemColumns, params['itemColumnId'], 'ItemColumnId');
					if ($scope.itemColumns[index].DataType == 6) {
						SettingsService.getItemColumnItems($scope.itemColumns[index].DataAdditional).then(function (columns) {
							$scope.list[params['itemColumnId']] = columns;
						});
					}
				}
			}
			newValuesArray.push({id: StateParameters.conditionIndex});
			changedItems = newValuesArray;
		};
		$scope.operatorNames = {
			1: Translator.translate('AND'),
			2: Translator.translate('OR')
		};

		$scope.groupOperators = [
			{OperatorId: 1, OperatorName: 'CONDITION_AND'},
			{OperatorId: 2, OperatorName: 'CONDITION_OR'}
		];

		$scope.comparisonTypes = {
			1: Translator.translate('LESS_THAN'),
			2: Translator.translate('LESS_OR_EQUAL'),
			3: Translator.translate('EQUAL_TO'),
			4: Translator.translate('GREATER_THAN'),
			5: Translator.translate('GREATER_OR_EQUAL'),
			6: Translator.translate('NOT_EQUAL_TO')

		};

		$scope.conditionTexts = {
			1: Translator.translate('VALUE_OF_FIELD'),
			2: Translator.translate('VALUE_OF_EX_CONDITION'),
			3: Translator.translate('VALUE_OF_FIELD'),
			4: Translator.translate('VALUE_OF_PARENT_PROCESS'),
			5: Translator.translate('USER_CHOSEN'),
			6: Translator.translate('SIGNED_USER_CHOSEN'),
			7: Translator.translate('DAY_DIFFERENCE'),
			8: Translator.translate('VALUE_OF_PARENT_EX_CONDITION')

		};

		$scope.getItemColumnTranslation = getItemColumnTranslation;
		$scope.getConditionTranslation = getConditionTranslation;
		$scope.getUserColumnTranslation = getUserColumnTranslation;
		$scope.getDateColumnTranslation = getDateColumnTranslation;
		$scope.getSublistTranslation = getSublistTranslation;
		$scope.getParentConditionTranslation = getParentConditionTranslation;


		$scope.parentProcessConditions = {};
		let parentProcesses = [];

		let getParentConditionTranslations = function () {
			angular.forEach($scope.condition.ConditionJSON, function (conditionGroup) {
				_.each(conditionGroup.OperationGroup.Operations, function (operation) {
					if (operation.Condition) {
						var process = operation.Condition.ParentProcess;
					}
					if (process && !_.includes(parentProcesses, process)) {
						parentProcesses.push(process);
					}
				});
			});

			_.each(parentProcesses, function (process) {
				SettingsService.Conditions(process).then(function (pConditions) {
					$scope.parentProcessConditions[process] = pConditions;
				});
			});
		};

		getParentConditionTranslations();

		function getItemColumnTranslation(itemcolumnid) {
			for (let i = 0, l = $scope.itemColumns.length; i < l; i++) {
				if ($scope.itemColumns[i].ItemColumnId == itemcolumnid) {

					return $scope.itemColumns[i].TranslationWithType;
				}
			}
			return '';
		}

		function getConditionTranslation(conditionid) {
			for (let i = 0, l = $scope.conditions.length; i < l; i++) {
				if ($scope.conditions[i].ConditionId == conditionid) {
					return Translator.translation($scope.conditions[i].LanguageTranslation);
				}
			}
			return '';
		}

		function getParentConditionTranslation(conditionId_, process) {
			let conditions = $scope.parentProcessConditions[process];
			if (conditions) {
				for (let i = 0, l = conditions.length; i < l; i++) {
					if (conditions[i].ConditionId == conditionId_) {
						return Translator.translation(conditions[i].LanguageTranslation);
					}
				}
			}
		}

		function getDateColumnTranslation(itemcolumnid2) {
			for (let i = 0, l = $scope.dateColumns.length; i < l; i++) {
				if ($scope.dateColumns[i].ItemColumnId == itemcolumnid2) {
					return Translator.translation($scope.dateColumns[i].LanguageTranslation);
				}
			}

			return Translator.translation('TODAY');
		}

		function getSublistTranslation(sublistname, id2) {
			if (angular.isDefined($scope.listUserColumns2[id2])) {

				for (let i = 0; i < $scope.listUserColumns2[id2].length; i++) {
					if ($scope.listUserColumns2[id2][i].ItemColumnId == sublistname) {
						return Translator.translation($scope.listUserColumns2[id2][i].LanguageTranslation);
					}
				}
			}
			return '';
		}

		function getUserColumnTranslation(usercolumnid) {
			for (let i = 0, l = $scope.userColumns.length; i < l; i++) {
				if ($scope.userColumns[i].ItemColumnId == usercolumnid) {
					return Translator.translation($scope.userColumns[i].LanguageTranslation);
				}
			}
			return '';
		}

		$scope.getIdOfListValue = function (v) {
			if (!v) return "";
			if (v.toString().includes(',') > 0) v = '(' + v + ')';
			return v.toString().replace('[', "").replace(']', "").replace("," , ", ");
		}
		$scope.getNameOfListValue = function (operation) {
			let result = "";
			if (operation.Condition && $scope.list[operation.Condition.ItemColumnId]) {
				let listOfItemsForProcess = $scope.list[operation.Condition.ItemColumnId];
				for (let i = 0, l = listOfItemsForProcess.length; i < l; i++) {
					if (typeof operation.Condition.Value == "number" && listOfItemsForProcess[i].item_id === operation.Condition.Value) {
						return listOfItemsForProcess[i].list_item;
					}
					if (typeof operation.Condition.Value == "object" && operation.Condition.Value.includes(listOfItemsForProcess[i].item_id)) {
						result += listOfItemsForProcess[i].list_item + ", ";
					}
				}
			}
			return result.substring(0, result.length-2);
		};

		$scope.openDialog = function (conditionId, condition, index, parent_index, conditionGroupId) {
			let params = {
				template: 'settings/conditions/conditionsFieldsDialog.html',
				controller: 'ConditionsFieldsSettingsController',
				locals: {
					ConditionId: conditionId,
					ConditionGroupId: conditionGroupId,
					Process: StateParameters.process,
					ProcessSubprocesses: $scope.processSubprocesses,
					ItemColumns: _.filter($scope.itemColumns, {'InConditions': true}),
					ListColumns: _.filter($scope.listColumns, {'InConditions': true}),
					ListOptions: $scope.listOptions,
					SubqueryListOptions: $scope.subqueryListOptions,
					VarcharOptions: $scope.varcharOptions,
					ProcessOptions: $scope.processOptions,
					List: $scope.list,
					UserColumns: _.filter($scope.userColumns, {'InConditions': true}),
					ListUserColumns2: $scope.listUserColumns2,
					ListUserColumns: $scope.listUserColumns2,
					Conditions: $scope.conditions,
					ConditionTypes: $scope.conditionTypes,
					DateColumns: _.filter($scope.dateColumns, {'InConditions': true}),
					Operators: $scope.operators,
					Condition: $scope.condition,
					Index: index,
					Parent_Index: parent_index,
					AddOperation: $scope.addOperation,
					StateParameters: StateParameters,
					Processes: Processes,
					GetParentConditionTranslations: getParentConditionTranslations
				}
			};
			MessageService.dialogAdvanced(params);
		};

		for (let i = 0; i < $scope.itemColumns.length; i++) {
			let type = '';
			let translationKey = Common.getDataTypeTranslation($scope.itemColumns[i].DataType);
			if (!translationKey) {
				type = 'undefined';
			} else {
				type = translationKey;
			}

			$scope.itemColumns[i].TranslationWithType = Translator.translation($scope.itemColumns[i].LanguageTranslation) + ' [' + type + ']';
		}
	}

	ConditionsRestrictionsController.$inject = ['$scope',
		'$filter',
		'MessageService',
		'Process',
		'Common',
		'SettingsService',
		'Index',
		'Condition',
		'ProcessGroups',
		'ProcessTabs',
		'ProcessContainers',
		'ProcessPortfolios',
		'UserGroups',
		'UiParentChildRelations',
		'Conditions',
		'StateParameters',
		'CalculatePhaseTabs',
		'CalculatePhaseContainers',
		'SavedTabs'];

	function ConditionsRestrictionsController($scope,
	                                          $filter,
	                                          MessageService,
	                                          Process,
	                                          Common,
	                                          SettingsService,
	                                          Index,
	                                          Condition,
	                                          ProcessGroups,
	                                          ProcessTabs,
	                                          ProcessContainers,
	                                          ProcessPortfolios,
	                                          UserGroups,
	                                          UiParentChildRelations,
	                                          Conditions,
	                                          StateParameters,
	                                          CalculatePhaseTabs,
	                                          CalculatePhaseContainers,
	                                          SavedTabs) {

		$scope.process = Process;
		$scope.condition = Condition;
		$scope.processTabs = ProcessTabs;
		$scope.conditionId = Condition.ConditionId;
		$scope.processContainers = ProcessContainers;
		$scope.processPortfolios = ProcessPortfolios;
		$scope.userGroups = UserGroups;
		$scope.conditions = Conditions;
		$scope.index = Index;
		$scope.processGroups = ProcessGroups;

		$scope.handlePhaseStuff = function () {
			calculateTabsForPhases();
			$scope.calculateContainers(tabsForSelectedPhases);

			let tabIds = CalculatePhaseTabs(Condition);
			let conditionTabs = Condition.ProcessTabIds;
			Condition.ProcessTabIds = _.uniq(SavedTabs(tabIds, conditionTabs));

			let containerIds = CalculatePhaseContainers(Condition);
			let conditionContainers = Condition.ProcessContainerIds;
			Condition.ProcessContainerIds = _.uniq(savedContainers(containerIds, conditionContainers));
		}


		let ids = UiParentChildRelations;
		let newValuesArray = [];
		let changedItems = [];
		let tabsForSelectedPhases = [];
		$scope.tabsForPhases = [];

		let calculateTabsForPhases = function () {
			if ($scope.condition.ProcessGroupIds == null) $scope.condition.ProcessGroupIds = [];
			tabsForSelectedPhases = [];
			angular.forEach(ids.data.tabIds, function (value, key) {
				for (let i = 0; i < $scope.condition.ProcessGroupIds.length; i++) {
					if ($scope.condition.ProcessGroupIds[i] == key) {
						for (let j = 0; j < value.length; j++) {
							tabsForSelectedPhases.push(value[j]);
						}
					}
				}
			});

			$scope.tabsForPhases = [];
			angular.forEach($scope.processTabs, function (value, key) {
				for (let i = 0; i < tabsForSelectedPhases.length; i++) {
					if (tabsForSelectedPhases[i] == $scope.processTabs[key].ProcessTabId) {
						$scope.tabsForPhases.push(value);
					}
				}
			});
			$scope.tabsForPhases = $filter('orderBy')(_.uniqBy($scope.tabsForPhases, 'ProcessTabId'), 'OrderNo');

			if (_.isEmpty($scope.tabsForPhases)) {
				$scope.tabsForPhases = _.uniqBy($scope.processTabs, 'ProcessTabId');
			}
		}

		calculateTabsForPhases();


		$scope.compareContainers = function (array) {
			let listOfContainers = [];
			angular.forEach(ids.data.containerIds, function (value, key) {
				for (let i = 0; i < array.length; i++) {
					if (array[i] == key) {
						for (let j = 0; j < value.length; j++) {
							if (!_.includes(listOfContainers, value[j])) {
								listOfContainers.push(value[j]);
							}
						}
					}
				}
			});
			return listOfContainers;
		};

		$scope.containersForTabs = [];

		$scope.calculateContainers = function (array) {
			let listOfContainers = angular.copy($scope.compareContainers(array));
			$scope.containersForTabs = [];

			if (_.isEmpty(listOfContainers) && _.isEmpty($scope.condition.ProcessGroupIds)) {
				$scope.containersForTabs = $scope.processContainers;
			} else {
				angular.forEach($scope.processContainers, function (value, key) {
					for (let i = 0; i < listOfContainers.length; i++) {
						if (listOfContainers[i] == $scope.processContainers[key].ProcessContainerId) {
							$scope.containersForTabs.push(value);
						}
					}
				});
			}
		};

		let calculateTabContainers = function (array) {
			let containers = [];
			angular.forEach(ids.data.containerIds, function (value, key) {
				for (let i = 0; i < array.ProcessTabIds.length; i++) {
					if (array.ProcessTabIds[i] == key) {
						angular.forEach(value, function (value2) {
							containers.push(value2);
						});
					}
				}
			});
			return containers;
		};

		$scope.clearContainersForNewTabs = function () {
			if ($scope.condition.ProcessTabIds.length != 0) {
				$scope.calculateContainers($scope.condition.ProcessTabIds);
			} else {
				$scope.calculateContainers(tabsForSelectedPhases);
			}

			let containerIds = calculateTabContainers($scope.condition);
			let conditionContainers = $scope.condition.ProcessContainerIds;
			$scope.condition.ProcessContainerIds = savedContainers(containerIds, conditionContainers);
		};

		let savedContainers = function (containerids, conditioncontainers) {
			let containerArray = [];
			for (let i = 0; i < containerids.length; i++) {
				for (let j = 0; j < conditioncontainers.length; j++) {
					if (containerids[i] == conditioncontainers[j]) {
						containerArray.push(containerids[i]);
					}
				}
			}
			return containerArray;
		};

		$scope.cancel = function () {

			MessageService.closeDialog();
		};

		$scope.changeValue = function (params) {
			$scope.calculateContainers(tabsForSelectedPhases);
			newValuesArray.push(params);
			let noDuplicateObjects = {};
			let l = newValuesArray.length;

			for (let i = 0; i < l; i++) {
				let item = newValuesArray[i];
				noDuplicateObjects[item.ConditionId] = item;
			}

			let nonDuplicateArray = [];
			for (let item in noDuplicateObjects) {
				nonDuplicateArray.push(noDuplicateObjects[item]);
			}
			changedItems = nonDuplicateArray;
		};

		$scope.saveRestrictions = function (condition) {
			$scope.changeValue(condition);
			saveCondition();
		};

		let saveCondition = function () {
			let conditionArray = [];
			angular.forEach(changedItems, function (item) {
				let index = Common.getIndexOf($scope.conditions[item.ConditionGroupId], item.ConditionId, 'ConditionId');
				$scope.conditions.Process = StateParameters.process;
				conditionArray.push($scope.conditions[item.ConditionGroupId][index]);
			});
			SettingsService.ConditionsSave(StateParameters.process, conditionArray).then(function () {
				MessageService.closeDialog();
			});
			newValuesArray = [];
			changedItems = [];
		};
		$scope.calculateContainers(tabsForSelectedPhases);
	}


	ConditionsFieldsSettingsController.$inject = ['$scope', 'MessageService', 'Process', 'SettingsService', 'ItemColumns', 'VarcharOptions', 'ListColumns', 'ListOptions', 'Operators', 'List', 'UserColumns', 'ListUserColumns', 'Conditions', 'ConditionTypes', 'DateColumns', 'ConditionId', 'ConditionGroupId', 'Condition', 'Index', 'Parent_Index', 'ListUserColumns2', 'ProcessSubprocesses', 'ProcessOptions', 'StateParameters', 'SubqueryListOptions', 'Translator', 'ProcessService', 'Processes', 'GetParentConditionTranslations'];

	function ConditionsFieldsSettingsController($scope, MessageService, Process, SettingsService, ItemColumns, VarcharOptions, ListColumns, ListOptions, Operators, List, UserColumns, ListUserColumns, Conditions, ConditionTypes, DateColumns, ConditionId, ConditionGroupId, Condition, Index, Parent_Index, ListUserColumns2, ProcessSubprocesses, ProcessOptions, StateParameters, SubqueryListOptions, Translator, ProcessService, Processes, GetParentConditionTranslations) {

		ItemColumns = _.sortBy(ItemColumns, function (item) {
			return _.toUpper(item.Translation);
		});

		UserColumns.sort(function (a, b) {
			let aTranslation = Translator.translation(a.LanguageTranslation);
			let bTranslation = Translator.translation(b.LanguageTranslation);

			if (aTranslation != null && bTranslation != null) {
				let na = aTranslation.toUpperCase();
				let nb = bTranslation.toUpperCase();
				if (na < nb) {
					return -1;
				}
				if (na > nb) {
					return 1;
				}
				return 0;
			}
		});
		
		$scope.convertSingleToMulti = function(v) {
			if (typeof v === "object") return v;
			return [v];
		}

		$scope.setCurrentDate = function (p) {
			if ($scope.isCurrentDate == true && p.hasOwnProperty('ItemColumnId2')) {
				delete p.ItemColumnId2;
			}
		};

		$scope.processes = Processes;
		$scope.index = Index;
		$scope.userProcess = Process;
		$scope.itemColumns = ItemColumns;
		$scope.processSubprocesses = ProcessSubprocesses;
		$scope.listColumns = ListColumns;
		$scope.listOptions = ListOptions;
		$scope.subqueryListOptions = SubqueryListOptions;
		$scope.varcharOptions = VarcharOptions;
		$scope.processOptions = ProcessOptions;

		_.each(List, function (l) {
			AddEmptyChoice(l);
		});

		function AddEmptyChoice(list) {
			if (!list.find(f => f.item_id === 0)) {
				list.push({
					list_item: "Empty",
					item_id: 0,
					in_use: "1"
				});
			}
		}

		$scope.list = List;
		$scope.userColumns = UserColumns;
		$scope.listUserColumns = ListUserColumns;
		$scope.listUserColumns2 = ListUserColumns2;
		$scope.conditionTypes = ConditionTypes;
		$scope.dateColumns = DateColumns;
		$scope.conditions = Conditions;
		$scope.conditionId = ConditionId;
		$scope.conditionGroupId = StateParameters.ConditionGroupId;
		$scope.operators = Operators;
		$scope.condition = Condition;
		$scope.conditionItem = angular.copy($scope.condition['ConditionJSON'][Parent_Index]['OperationGroup']['Operations'][Index]);
		$scope.conditionItemType = $scope.conditionItem.ConditionTypeId;
		$scope.existingConditions = [];
		$scope.userListFields = [];
		
		if ($scope.conditionItem.ConditionTypeId == 1 && ($scope.listOptions[$scope.conditionItem.Condition.ItemColumnId] || $scope.subqueryListOptions[$scope.conditionItem.Condition.ItemColumnId])) {
			$scope.conditionItem.Condition.Value = $scope.convertSingleToMulti($scope.conditionItem.Condition.Value);
		}

		if (!_.isEmpty($scope.conditionItem.Condition)) {
			$scope.isCurrentDate = !$scope.conditionItem.Condition.hasOwnProperty('ItemColumnId2');
		} else $scope.isCurrentDate = false;

		$scope.getParentConditions = function () {
			return SettingsService.Conditions($scope.conditionItem.Condition.ParentProcess).then(function (conditions) {
				$scope.parentConditions = conditions;
				$scope.getColumnsForParentCondition();
			});
		};

		$scope.getColumnsForParentCondition = function () {
			ProcessService.getColumns(StateParameters.process).then(function (columns) {
				$scope.linkColumns = [];
				_.each(columns, function (c) {
					if (c.DataType == 5 || _.endsWith(c.Name, 'item_id')) {
						$scope.linkColumns.push(c)
					}
				});
			});
		};

		if ($scope.conditionItem.Condition && $scope.conditionItem.Condition != 'undefined' && $scope.conditionItem.Condition.ParentProcess) {
			$scope.getParentConditions();
		}

		for (let i = 0; i < $scope.conditions.length; i++) {
			if ($scope.conditions[i].ConditionId != $scope.condition.ConditionId) {
				$scope.existingConditions.push($scope.conditions[i]);
			}
		}
		$scope.matchingUserList = [];

		let findList = function (itemid) {
			let listName;
			angular.forEach(ItemColumns, function (value, key) {
				if (value.ItemColumnId == itemid) {
					listName = value.DataAdditional;
				}
			});
			return listName;
		};

		let checkUserList = function (listname) {
			$scope.matchingUserList = [];
			angular.forEach(UserColumns, function (value, key) {
				if (value.DataAdditional == listname) {
					$scope.matchingUserList.push(value);
				}
			});
		};

		angular.forEach(UserColumns, function (value, key) {
			if (value.DataType == 6) {
				$scope.userListFields.push(value);
			}
		});

		$scope.allUserItemOptions = [
			{name: 'Item column', value: 1},
			{name: 'Item id', value: 2},
			{name: 'SIGNED_IN_USER', value: 3}
		];

		$scope.processOptionsOptions = [
			{name: 'User field', value: 1},
			{name: 'SIGNED_IN_USER', value: 2}
		];

		$scope.oneUserItemOption = [
			{name: 'User field', value: 1}
		];

		$scope.booleanOptions = [{value: 'true', name: 'CONDITION_TRUE'}, {value: 'false', name: 'CONDITION_FALSE'}]

		$scope.comparisonOptions = [
			{value: 1, name: '<'},
			{value: 2, name: '<='},
			{value: 3, name: '='},
			{value: 4, name: '>'},
			{value: 5, name: '>='},
			{value: 6, name: '!='}
		];

		let dateColumns = [];
		let i, l = ItemColumns.length;
		for (i = 0; i < l; i++) {
			let ic = ItemColumns[i];
			if (ic.DataType == 3) {
				dateColumns.push(ic.ItemColumnId);
			}
		}

		$scope.userColumnFields = [];

		for (i = 0; i < l; i++) {
			let ib = ItemColumns[i];
			if (ib.DataType == 5 && ib.DataAdditional == 'user') {
				$scope.userColumnFields.push(ib);
			}
		}

		let isDate = function () {
			let i, l = dateColumns.length;
			for (i = 0; i < l; i++) {
				if ($scope.conditionItem.Condition && $scope.conditionItem.Condition.ItemColumnId == dateColumns[i]) {
					$scope.isDate = true;
					return;
				}
			}
			$scope.isDate = false;
		};

		$scope.cancel = function (index, condition_item) {
			if (_.isEmpty(condition_item)) {
				$scope.condition.ConditionJSON[StateParameters.ConditionGroupId].OperationGroup.Operations.splice(index, 1);
			}
			MessageService.closeDialog();
		};

		let checkIfGroupEmpty = function (cond) {
			_.each(cond.ConditionJSON, function (stack, ind) {
				if (stack.OperationGroup.Operations.length == 0) {
					cond.ConditionJSON.splice(ind, 1);
				}
			});
		};

		$scope.saveConditionFields = function () {
			let conditionArray = [];
			$scope.condition.Process = Process;
			$scope.condition['ConditionJSON'][Parent_Index]['OperationGroup']['Operations'][Index] = $scope.conditionItem;
			checkIfGroupEmpty($scope.condition);
			conditionArray.push($scope.condition);
			SettingsService.ConditionsSave(Process, conditionArray).then(function () {
				GetParentConditionTranslations();
				MessageService.closeDialog();
			});
		};

		$scope.operatorNames = [
			{OperatorId: 1, OperatorName: 'and'},
			{OperatorId: 2, OperatorName: 'or'}
		];

		$scope.changeValueType = function (ConditionId) {
			$scope.changeValue(ConditionId);
			if (typeof $scope.conditionItem.Condition == 'undefined') {
				$scope.conditionItem.Condition = {};
				$scope.conditionItem.Condition.ComparisonTypeId = 3;
			}
			$scope.conditionItem.ConditionTypeId = $scope.conditionItemType;
		};

		let listIds = [];
		angular.forEach(ListColumns, function (value, key) {
			listIds.push(value.ItemColumnId)
		});

		function setMatchingList() {
			if (!_.get($scope, 'conditionItem.Condition.ItemColumnId')) {
				return;
			}
			let itemColumn: Client.ItemColumn = _.find(ItemColumns, {ItemColumnId: $scope.conditionItem.Condition.ItemColumnId});
			if (itemColumn) {
				let dataType = itemColumn.DataType;
				let itemColumns;
				if (dataType === 6) {
					itemColumns = _.filter(UserColumns, function (i) {
						return i.DataType === dataType && i.DataAdditional === itemColumn.DataAdditional;
					});
				} else {
					itemColumns = _.filter(UserColumns, function (c) {
						return c.DataType === dataType ||
							(c.DataType === 16 && c.SubDataType === dataType) ||
							(c.DataType == 6 && dataType == 16 && itemColumn.Process == c.DataAdditional);
					});
				}
				$scope.matchingUserList = itemColumns;
			}
		}

		$scope.changeValue = function (ConditionParams) {
			if (ConditionParams && ConditionParams.hasOwnProperty('itemColumnId')) {
				let ic = _.find($scope.itemColumns, function (p) {
					return p.ItemColumnId == ConditionParams.itemColumnId
				});
				if (ic.DataType == 6) {
					$scope.list[ic.ItemColumnId] = [];
					SettingsService.getItemColumnItems(ic.DataAdditional).then(function (items) {
						//Tähän
						AddEmptyChoice(items);
						$scope.list[ic.ItemColumnId] = items;
					});

				} else if (ic.DataType == 16 && ic.SubDataType == 6) {
					SettingsService.getItemColumnItems(ic.SubDataAdditional).then(function (items) {
						//Tähän
						AddEmptyChoice(items);
						$scope.list[ic.ItemColumnId] = items;
					});
				}
			}

			if (ConditionParams && ConditionParams.userConditionColumnChanged == true) {
				$scope.matchingUserList = [];
				$scope.conditionItem.Condition.UserColumnId = null;

				setMatchingList();
			}

			if (!angular.isUndefined(ConditionParams)) {
				if (!angular.isUndefined(ConditionParams['ConditionType'])) {
					$scope.conditionItem = {'ConditionTypeId': ConditionParams['ConditionType']};
				}
				if (typeof $scope.conditionItem.Condition !== 'undefined') {
					$scope.conditionItem.Condition.Value = '';
					for (let i = 0; i < $scope.dateColumns.length; i++) {
						if ($scope.dateColumns[i].ItemColumnId == $scope.conditionItem.Condition.ItemColumnId) {
							$scope.conditionItem.Condition.Value = new Date();
							break;
						}
					}
				}
			}
			isDate();
		};
		isDate();


		function init() {
			setMatchingList();
		}

		init();
	}
})();

