﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.config(ConditionsConfig);

	ConditionsConfig.$inject = ['ViewsProvider'];
	function ConditionsConfig(ViewsProvider) {

		ViewsProvider.addView('settings.conditions', {
			controller: 'ConditionsSettingsController',
			template: 'settings/conditions/conditions.html',
			resolve: {
				Condition: function (SettingsService, StateParameters, Views, Translator) {
					return SettingsService.Conditions(StateParameters.process).then(function (conditions) : Array<Client.Condition> {
						let conditionStates = [];
						let conditionArray = {};
						let stateTranslations = [];
						angular.forEach(conditions, function (condition) {
							if (angular.isUndefined(conditionArray[condition.ConditionGroupId])) conditionArray[condition.ConditionGroupId] = [];
							conditionArray[condition.ConditionGroupId].push(condition);
							let noDuplicateObjects = [];
							let stateNames = [];
							let translations = [];
							angular.forEach(condition.Features, function (feature) {
								let f = Views.getFeatureByName(feature);
								if (f) {
									angular.forEach(f.states, function (state) {
										noDuplicateObjects.push(state.toString().toLowerCase());
										stateNames.push(feature.toString() + "." + state.toString().toLowerCase());
										translations.push(feature.toString());
									});
								}
							});
							
							let states = [];
							angular.forEach(noDuplicateObjects, function (key, i) {

								let ss = {stateName: stateNames[i], translation: Translator.translate("CONDITION_" + key.toString().toUpperCase()) + " (" + Translator.translate("FEATURE_" + translations[i].toUpperCase()) + ")" };
								states.push(ss);

								_.find(stateTranslations, function(o) { return o.stateName == stateNames[i]; });

								if(typeof _.find(stateTranslations, function(o) { return o.stateName == stateNames[i]; }) == 'undefined') stateTranslations.push(ss);
							});
							conditionStates[condition.ConditionId] = states;
						});
						return { 'conditions': conditionArray, 'states': conditionStates, 'conditionsArray': conditions, 'stateTranslations': stateTranslations};
					});
				},
				GroupsProcessGroup: function (SettingsService, StateParameters) {
					return SettingsService.ProcessGroups(StateParameters.process);
				},
				LayoutProcessGroup: function (SettingsService, StateParameters) {
					return SettingsService.ProcessTabs(StateParameters.process);
				},
				UserGroups: function (SettingsService) {
					return SettingsService.UserGroups('user_group');
				},
				ProcessPortfolios: function (SettingsService, StateParameters) {
					return SettingsService.ProcessPortfolios(StateParameters.process);
				},
				ProcessContainers: function (SettingsService, StateParameters) {
					return SettingsService.ProcessContainers(StateParameters.process);
				},
				ProcessContainerColumns: function (SettingsService, StateParameters) {
					return SettingsService.ProcessContainerColumns(StateParameters.process);
				},
				UiParentChildRelations: function (SettingsService, StateParameters) {
					return SettingsService.UiParentChildRelations(StateParameters.process);
				},

				States: function (Views) {
					let states = Views.getFeatures();
					let result = {};
					angular.forEach(states, function (value) {
						let stateResult = [];
						for (let s of value.states) {
							stateResult.push (value.name.toString() + "." + s.toString().toLowerCase());
						}
						result[value.name] = stateResult;
						
					});
					return result;
				},
				ConditionsGroups: function (SettingsService, StateParameters, Translator, $rootScope) {
					let result = [];

					return SettingsService.getConditionsGroups(StateParameters.process).then(function (data) {
						result = data;
						result.splice(0, 0, { ConditionGroupId: 0, LanguageTranslation: {} });
						result[0].LanguageTranslation[$rootScope.me.locale_id] = Translator.translate("CONDITIONS_NOT_IN_GROUP");
						return result;
					});
				}
			}
		});

		ViewsProvider.addView('settings.conditions.fields', {
			controller: 'ConditionsSentenceController',
			template: 'settings/conditions/conditionsFields.html',
			resolve: {
				Condition: function (SettingsService, StateParameters, $q) {
					return SettingsService.Conditions(StateParameters.process).then(function (condition) {
						let subProcessesArray = [];
						let itemColumnsArray = {};
						let listColumnsArray = {};
						let uniqueNames = [];
						let promises = [];

						$.each(subProcessesArray, function (i, el) {
							if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
						});

						return $q.all(promises).then(function () {
							return {
								'condition': condition,
								'ItemColumns': itemColumnsArray,
								'ListColumns': listColumnsArray
							};
						});
					});
				},
				Operators: function (SettingsService) {
					
					return SettingsService.Operators();
				},

				ItemColumns: function (SettingsService, StateParameters, $q) {

					let conditionItemColumns = [];

					//Check which item columns are already selected in condition
					_.each(StateParameters.Condition.ConditionJSON, function(jsonGroup){
						_.each(jsonGroup.OperationGroup.Operations, function(s){
							if(s.Condition && s.Condition != null && s.Condition.hasOwnProperty('ItemColumnId') && !_.includes(conditionItemColumns, s.Condition.ItemColumnId)){
								conditionItemColumns.push(s.Condition.ItemColumnId);
							}
						});
					});

					return SettingsService.ItemColumns(StateParameters.process).then(function (columns) {
						let lists = {};
						let listOptions = {};
						let subqueryListOptions = {};
						let varcharOptions = {};
						let processOptions = {};
						let listUserColumns = [];
						let promises = [];

						for (let key of columns) {
							if (key.InConditions != 1) continue; 
							
							//Get only relevant items (only items that belong to lists that are selected to condition
							if (key.DataType == 6 && key.DataAdditional != null) {
								listOptions[key.ItemColumnId] = key.DataType;
								lists[key.ItemColumnId] = [];
								if(_.includes(conditionItemColumns, key.ItemColumnId)){
									promises.push(SettingsService.getItemColumnItems(key.DataAdditional).then(function (list) {
										lists[key.ItemColumnId] = list;
									}));
									promises.push(SettingsService.ItemColumns(key.DataAdditional).then(function (cols) {
										let cols2 = [];
										angular.forEach(cols, function (key2) {
											if (key2.DataType == 5 && key2.DataAdditional == 'user') {
												cols2.push(key2);
											}
										});
										listUserColumns[key.ItemColumnId] = cols2;
									}));
								}
							}
							if (key.DataType == 0) {
								varcharOptions[key.ItemColumnId] = true;
							}
							if (key.DataType == 5 || (key.DataType == 16 && key.SubDataType == 5)) {
								processOptions[key.ItemColumnId] = true;
							}
							if (key.DataType == 16 && key.SubDataType == 6) {
								subqueryListOptions[key.ItemColumnId] = key.DataType;
								lists[key.ItemColumnId] = [];
								if (_.includes(conditionItemColumns, key.ItemColumnId)) {
									promises.push(SettingsService.getItemColumnItems(key.SubDataAdditional).then(function (list) {
										lists[key.ItemColumnId] = list;
									}));
									promises.push(SettingsService.ItemColumns(key.SubDataAdditional).then(function (cols) {
										let cols2 = [];
										angular.forEach(cols, function (key2) {
											if (key2.DataType == 5 && key2.DataAdditional == 'user') {
												cols2.push(key2);
											}
										});
										listUserColumns[key.ItemColumnId] = cols2;
									}));
								}
							}
						}

						return $q.all(promises).then(function(){
							return {
								'columns': columns,
								'lists': lists,
								'listOptions': listOptions,
								'listUserColumns': listUserColumns,
								'varcharOptions': varcharOptions,
								'processOptions': processOptions,
								'subqueryListOptions': subqueryListOptions

							};
						})
					});
				},
				UserColumns: function(SettingsService) {
					return SettingsService.ItemColumns("user");
				},
				ProcessSubprocesses: function (SettingsService, StateParameters, Translator) {
					let subprocessArray = [];
					return SettingsService.ProcessSubprocesses(StateParameters.process).then(function (subprocesses) {
						angular.forEach(subprocesses, function (key) {
							subprocessArray.push({ 'Process': key.Process.Variable, 'Translation': Translator.translation(key.Process.LanguageTranslation) });
						});
						return subprocessArray;
					});
				},
				GroupsProcessGroup: function (SettingsService, StateParameters) {
					return SettingsService.ProcessGroups(StateParameters.process);
				},
				UserGroups: function (SettingsService) {
					return SettingsService.UserGroups('user_group');
				},
				ProcessPortfolios: function (SettingsService, StateParameters) {
					return SettingsService.ProcessPortfolios(StateParameters.process);
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				},
				ConditionUsages: function(ApiService, StateParameters){
					return ApiService.v1api('settings/Conditions').get([StateParameters.process, StateParameters.conditionId, "usages"]);
				},
				ProcessActions: function (ProcessActionsService, StateParameters) {
					return ProcessActionsService.getProcessActions(StateParameters.process);
				},
				NotificationConditions: function (SettingsService, StateParameters) {
					return SettingsService.NotificationConditions(StateParameters.process);

				},
			},
			afterResolve: {
				OtherProcessesConditions: function(ConditionUsages, SettingsService, $q){
					let promises = [];
					if(ConditionUsages[2].length){
						_.each(ConditionUsages[2], function(usage){
							promises.push(SettingsService.Conditions(usage.OwnerElementParentProcess,usage.OwnerElementParentId))
						})
						return $q.all(promises)
					}
					return {}
				}
			}
		});
	}
})();