(function () {

	angular.module('core.settings')
		.config(MatrixProcessConfig)
		.controller('MatrixSettingsController', MatrixSettingsController)
		.service('MatrixProcessService', MatrixProcessService);
	MatrixProcessConfig.$inject = ['ViewsProvider'];

	function MatrixProcessConfig(ViewsProvider) {

		ViewsProvider.addView('settings.matrix', {
			controller: 'MatrixSettingsController',
			template: 'settings/matrixProcess/matrixProcess.html',
			resolve: {
				MatrixRows: function (StateParameters, MatrixProcessService) {
					return MatrixProcessService.getMatrixRows(StateParameters.process, false);
				}
			}
		});
	}

	MatrixProcessService.$inject = ['SettingsService', '$rootScope', 'ProcessService', 'Translator', 'ApiService', 'PortfolioService', 'SelectorService', '$q'];

	function MatrixProcessService(SettingsService, $rootScope, ProcessService, Translator, ApiService, PortfolioService, SelectorService, $q) {
		var self = this;

		var matrixDataProcess = 'matrix_process_row_data';
		self.insertMatrixColumns = function (processName) {
			var columns = [];
			var userLanguage = $rootScope.clientInformation.locale_id;
			var columnsToAdd = ['type', 'score', 'question_item_id', 'column_item_id', 'weighting', 'custom_width'];
			var dataTypes = [1, 2, 1, 1, 2, 2];
			for (var i = 0; i < columnsToAdd.length; i++) {
				var newColumn = {
					Process: processName,
					ItemColumnId: 0,
					OrderNo: 0,
					LanguageTranslationRaw: {},
					LanguageTranslation: {},
					Variable: columnsToAdd[i],
					DataType: dataTypes[i],
					StatusColumn: 1,
					ProcessContainerIds: [0]
				};

				newColumn.LanguageTranslationRaw[userLanguage] = columnsToAdd[i];
				newColumn.LanguageTranslation[userLanguage] = columnsToAdd[i];
				columns.push(newColumn);

			}
			let promises = [];
			angular.forEach(columns, function (newColumn) {
				promises.push(SettingsService.ItemColumnsAdd(processName, newColumn));
			});
			return $q.all(promises).then(function () {
				let nameColumn = {
					Process: processName,
					ItemColumnId: 0,
					OrderNo: 0,
					LanguageTranslationRaw: {},
					LanguageTranslation: {},
					Variable: 'name',
					DataType: 24,
					StatusColumn: 1,
					ProcessContainerIds: [0]
				};
				nameColumn.LanguageTranslationRaw[userLanguage] = 'name';
				nameColumn.LanguageTranslation[userLanguage] = 'name';

				let tooltipColumn = {
					Process: processName,
					ItemColumnId: 0,
					OrderNo: 0,
					LanguageTranslationRaw: {},
					LanguageTranslation: {},
					Variable: 'tooltip',
					DataType: 24,
					StatusColumn: 1,
					ProcessContainerIds: [0]
				};

				tooltipColumn.LanguageTranslationRaw[userLanguage] = 'tooltip';
				tooltipColumn.LanguageTranslation[userLanguage] = 'tooltip';


				let commentName = {
					Process: processName,
					ItemColumnId: 0,
					OrderNo: 0,
					LanguageTranslationRaw: {},
					LanguageTranslation: {},
					Variable: 'comment_name',
					DataType: 24,
					StatusColumn: 1,
					ProcessContainerIds: [0]
				};

				commentName.LanguageTranslationRaw[userLanguage] = 'comment_name';
				commentName.LanguageTranslation[userLanguage] = 'comment_name';

				SettingsService.ItemColumnsAdd(processName, nameColumn);
				SettingsService.ItemColumnsAdd(processName, tooltipColumn);
				SettingsService.ItemColumnsAdd(processName, commentName);
			});
		};

		self.matrixCache = {};

		self.getMatrixRows = function (process, reload) {
			return ProcessService.getItems(process, undefined, reload, 'order_no').then(function (response) {
				if(!self.matrixCache[process] || reload) {
					var matrixRows = {
						'columns': [],
						'rows': [],
						'customWidthRows': false,
						'customWidthColumns': false
					};

					var category = {
						name: Translator.translate('MATRIX_QUESTION'),
						custom_width: null
					};

					matrixRows['columns'].push(category);

					_.each(response, function (item) {
						if (item.type == 1) {
							if (item.custom_width != null){
								matrixRows['columns'][0].custom_width = item.custom_width;
								matrixRows['customWidthRows'] = true;
							}

							matrixRows['rows'].push(item);
							if (typeof item.$descriptions === 'undefined') {
								item.$descriptions = [];
							}

							_.each(response, function (matrixItem) {
								if (matrixItem.type == 3 && matrixItem.question_item_id == item.item_id) {
									_.each(response, function (findingParentColumn) {
										if (findingParentColumn.item_id == matrixItem.column_item_id) {
											matrixItem.$parentColumn = findingParentColumn;
										}
									});

									item.$descriptions.push(matrixItem)
								}
							});
						} else if (item.type == 2) {

							if (item.custom_width != null){
								matrixRows['customWidthColumns'] = true;
							}

							matrixRows['columns'].push(item);
						}
					});
					self.matrixCache[process] = matrixRows;
					return matrixRows;
				}
				return self.matrixCache[process];
			});
		};

		self.getMatrixRowData = function (parentId, parentColumnId, archiveDate) {
			return ProcessService.getColumns(matrixDataProcess).then(function(columns){
				var r = {};
				r[_.find(columns, ['Name', 'owner_item_id']).ItemColumnId] = [parentId];
				r[_.find(columns, ['Name', 'item_column_id']).ItemColumnId] = [parentColumnId];
				let filters = {local:true, restrictions: r};
				if(typeof archiveDate != 'undefined'){
					filters['archiveDate'] = archiveDate
				}
				var portfolioId = SelectorService.getProcessSelector(matrixDataProcess).ProcessPortfolioId;
				return PortfolioService
					.getPortfolioData(matrixDataProcess, portfolioId, filters)
					.then(function(allRows) {
						return allRows.rowdata;
					});
			});

		};

	}

	MatrixSettingsController.$inject = [
		'$scope',
		'SettingsService',
		'StateParameters',
		'MatrixRows',
		'MessageService',
		'ViewService',
		'$q',
		'$rootScope',
		'ProcessService',
		'Common',
		'Translator'
	];

	function MatrixSettingsController(
		$scope,
		SettingsService,
		StateParameters,
		MatrixRows,
		MessageService,
		ViewService,
		$q,
		$rootScope,
		ProcessService,
		Common,
		Translator
	) {

		let userLanguage = $rootScope.clientInformation.locale_id;

		$scope.syncOrder = function (current, prev, next) {
			var beforeOrderNo;
			var afterOrderNo;
			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.order_no;
			}
			if (angular.isDefined(next)) {
				afterOrderNo = next.order_no;
			}
			current.order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			$scope.saveRow(current);
		};
		ViewService.footbar(StateParameters.ViewId, {
			template: 'settings/matrixProcess/matrixProcessFooter.html',
			scope: $scope
		});
	
		$scope.setConditions = function () {
			ViewService.view('settings.conditions', {
					params: StateParameters,
					locals: {
						process: StateParameters.process
					}
				},
				{split: true, remember: false});
		};

		$scope.showToolTips = false;

		$scope.toggleTexts = function(){
			$scope.showToolTips = !$scope.showToolTips;
		};

		$scope.matrixRows = MatrixRows;

		let handleWidthIntegrity = function(width, type){
			if(type == 'rows'){
				_.each($scope.matrixRows['rows'], function(row){
					row.custom_width = width;
				})

				_.each($scope.matrixRows['columns'], function(column, index){
					if(index != "0") column.custom_width = null;
				})
			} else {
				_.each($scope.matrixRows['columns'], function(column, index){
					if(index != "0") column.custom_width = width;
				})
				_.each($scope.matrixRows['rows'], function(row){
					row.custom_width = null;
				})
			}


			_.each(MatrixRows.rows, function(row){
				$scope.saveRow(row);
			})

			_.each(MatrixRows.columns, function(c){
				if(c.item_id) $scope.saveRow(c);
			})

		}

		$scope.addMatrixColumn = function () {
			data = {Translation: null};
			data.type = 2;
			var save = function () {
				addQuestionSave(data);
			};
			var name;
			var score;
			var content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [
					{
						type: 'translation',
						label: 'MATRIX_COLUMN_NAME',
						model: 'name'
					},
					{
						type: 'decimal',
						precision: 3,
						label: 'MATRIX_COLUMN_SCORE',
						model: 'score'
					},
				],
				title: 'ADD_MATRIX_COLUMN'
			};

			MessageService.dialog(content, data);
		};
		$scope.header = {
			title: Translator.translate('MATRIX_NAME') + ": " + StateParameters.process
		};
		StateParameters.activeTitle = $scope.header.title;

		$scope.addMatrixQuestion = function () {

			data = {Translation: null};

			var save = function () {
				addQuestionSave(data);
			};
			var name;
			var w;
			data.type = 1;

			var content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [
					{type: 'translation', label: 'MATRIX_COLUMN_QUESTION', model: 'name'}
				],
				title: 'ADD_MATRIX_QUESTION'
			};

			MessageService.dialog(content, data);
		};

		let findTypeWidth = function(type){
			let w = null;
			_.each($scope.matrixRows[type], function(r){
				if(r.custom_width != null) w = r.custom_width;
			})
			return w;
		}

		var addQuestionSave = function (data) {
			var p = {
				Data: {
					name: data.name,
					score: data.score,
					type: data.type,
					weighting: data.w,
					custom_width: data.type == 3 ? null : findTypeWidth(data.type == 1 ? 'rows' : 'columns')
				}
			};
			var newColumns = [];
			ProcessService.newItem(StateParameters.process, p).then(function (newColumnId) {
				if (data.type == 1) {
					getMatrixData(newColumnId).then(function (data) {
						data.$descriptions = [];
						$scope.matrixRows['rows'].push(data);
					});
				}
				if (data.type == 1 && $scope.matrixRows['columns'].length > 1) {
					for (var i = 1; i < $scope.matrixRows['columns'].length; i++) {
						var descColumn = {
							Data: {
								name: data.name,
								score: data.score,
								type: data.type,

							}
						};

						descColumn.Data.question_item_id = newColumnId;
						descColumn.Data.column_item_id = $scope.matrixRows['columns'][i].item_id;
						descColumn.Data.type = 3;
						descColumn.score = null;
						descColumn.Data.name = {};
						descColumn.Data.name[userLanguage] = 'unnamed'
						newColumns.push(descColumn);
					}
					ProcessService.newItems(StateParameters.process, newColumns).then(function (newColumns) {
						fillDescriptions(newColumns);
					});
				} else if (data.type == 2) {
					getMatrixData(newColumnId).then(function (data) {
						$scope.matrixRows['columns'].push(data);
					});
					_.each($scope.matrixRows['rows'], function (category) {
						var descColumn = {
							Data: {
								name: data.name,
								score: data.score,
								type: data.type
							}
						};
						descColumn.Data.question_item_id = category.item_id;
						descColumn.Data.column_item_id = newColumnId;
						descColumn.Data.type = 3;
						descColumn.Data.score = null;
						descColumn.Data.name = {};
						descColumn.Data.name[userLanguage] = 'unnamed'
						newColumns.push(descColumn);
					});
					ProcessService.newItems(StateParameters.process, newColumns).then(function (newColumns) {
						fillDescriptions(newColumns);
					});
				}
			});

			var getMatrixData = function (newId) {
				return ProcessService.getItemData(StateParameters.process, newId).then(function (createdColumn) {
					return createdColumn.Data;
				});
			};
		};

		var fillDescriptions = function (createdColumns) {
			_.each(createdColumns, function (column) {
				_.each($scope.matrixRows['rows'], function (row) {
					if (row.item_id == column.Data.question_item_id) {
						row.$descriptions.push(column.Data);
					}
				});
			});
		};


		$scope.deleteRow = function (matrixRow) {
			MessageService.confirm('CONFIRM_DELETE', function () {
				var deleteIds = [];
				_.each(matrixRow.$descriptions, function (descriptionColumn) {
					deleteIds.push(descriptionColumn.item_id);
				});
				deleteIds.push(matrixRow.item_id);
				var index = Common.getIndexOf($scope.matrixRows['rows'], matrixRow.item_id, 'item_id');
				$scope.matrixRows['rows'].splice(index, 1);
				ProcessService.deleteItems(StateParameters.process, deleteIds);
			});
		};

		$scope.saveRow = function (descriptionColumn) {
			ProcessService.getItemData(StateParameters.process, descriptionColumn.item_id).then(function(cam){
				ProcessService.setDataChanged(StateParameters.process, descriptionColumn.item_id, true, cam.Data)

			});
		};

		$scope.deleteColumn = function(column){
			MessageService.confirm('CONFIRM_DELETE', function () {
				let cellsToDelete = [];
				_.each(MatrixRows.rows, function(rowSet){
					_.each(rowSet.$descriptions, function(matrixCell){
						if(matrixCell.column_item_id == column.item_id){
							cellsToDelete.push(matrixCell.item_id);
						}
					})
				})

				cellsToDelete.push(column.item_id);

				ProcessService.deleteItems(StateParameters.process, cellsToDelete).then(function () {
					cellsToDelete = [];
					ViewService.refresh(StateParameters.ViewId);
				});

			});
		}

		let cw = null;

		$scope.setQuestionWidths = function(column){
			let values = {custom_width: cw != null ? cw : column.custom_width};
			let save = function () {
				cw = values['custom_width'];
				handleWidthIntegrity(values.custom_width, 'rows')
			};

			let content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [
					{
						type: 'decimal',
						precision: 3,
						label: 'MATRIX_Q_COLUMN_CUSTOM_WIDTH',
						model: 'custom_width'
					},
				],
				title: 'EDIT_MATRIX_COLUMN'
			};
			MessageService.dialog(content, values);
		}

		$scope.modifyColumn = function (column) {
			var newValues = {name: column.name, score: column.score, custom_width: column.custom_width};
			var modify = {save: true};

			var save = function () {
				column.score = newValues.score;
				column.name = newValues.name;
				handleWidthIntegrity(newValues.custom_width, 'columns');
			};
			var onChange = function(){
				modify.save = !!(newValues.name && (newValues.score || newValues.score === 0));
			};
			var deleteColumn = function () {
				$scope.deleteColumn(column);
			};

			var content = {
				buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: deleteColumn, text: 'DELETE'}, {type: 'primary', onClick: save, text: 'SAVE'}],
				inputs: [
					{
						type: 'translation',
						label: 'MATRIX_COLUMN_NAME',
						model: 'name'
					},
					{
						type: 'decimal',
						precision: 3,
						label: 'MATRIX_COLUMN_SCORE',
						model: 'score'
					},
					{
						type: 'decimal',
						precision: 3,
						label: 'MATRIX_COLUMN_CUSTOM_WIDTH',
						model: 'custom_width'
					},
				],
				title: 'EDIT_MATRIX_COLUMN'
			};
			MessageService.dialog(content, newValues, onChange, modify);
		};
	}
})();