(function () {
    'use strict';

    angular
        .module('core.settings')
        .controller('ProcessActionsGroupEditDialogController', ProcessActionsGroupEditDialogController)

    ProcessActionsGroupEditDialogController.$inject = ['$q', '$scope', '$rootScope', 'AddNewActionRow', 'ChangeCreateGroupData', 'DeleteFunctionForRows', 'CreateGroupProcesses', 'ProcessActionsService', 'RefinedActionGroupsWithRows', 'MessageService', 'ProcessesService', 'EditGroupData', 'Common', 'StateParameters', 'ProcessActionRowsGroups'];

    function ProcessActionsGroupEditDialogController($q, $scope, $rootScope, AddNewActionRow, ChangeCreateGroupData, DeleteFunctionForRows, CreateGroupProcesses, ProcessActionsService, RefinedActionGroupsWithRows, MessageService, ProcessesService, EditGroupData, Common, StateParameters, ProcessActionRowsGroups) {

        $scope.editGroupData = EditGroupData;
        let copyGroupData = angular.copy($scope.editGroupData);
        $scope.oldType = angular.copy($scope.editGroupData.Type);
        $scope.oldProcessInGroup = angular.copy($scope.editGroupData.ProcessInGroup);
        $scope.createGroupProcesses = CreateGroupProcesses;
        let beforeOrderNo = Common.orderNoFromArray(ProcessActionRowsGroups);
        $scope.localeId = $rootScope.me.locale_id;
        let userLanguage = $rootScope.clientInformation.locale_id;
        let checkTranslationLength = function (string, string2) {
            if (string[userLanguage]) {
                let translation = string[userLanguage];
                if (translation.length > 0 && string2.length > 0) {
                    return true;
                }
            }
            return false;
        };
        $scope.setGroupOption = function () {
            if ($scope.editGroupData.Type[0] == "Set") {
                $scope.editGroupData.Type = 1;
                // $scope.editGroupData.LanguageTranslation = $scope.LanguageTranslation;
            } else if ($scope.editGroupData.Type[0] == "Create") {
                $scope.editGroupData.Type = 2;
                // $scope.editGroupData.LanguageTranslation = $scope.LanguageTranslation;
            }
        }
        $scope.deleteGroup = function (groupId: number) {
            MessageService.confirm("Are you sure you want to delete this group?", function () {
                ProcessActionsService.ProcessActionRowsGroupsDelete(StateParameters.process, groupId);
                delete RefinedActionGroupsWithRows[groupId];
                let index = _.findIndex(ProcessActionRowsGroups, function (o: Client.ProcessActionRow) {
                    return o.ProcessActionGroupId == groupId;
                });
                ProcessActionRowsGroups.splice(index, 1);
            });
        };

        $scope.createOptions = [
            {'Value': 1, 'Name': 'Set'},
            {'Value': 2, 'Name': 'Create'}];

        $scope.saveEdit = function () {
            checkTranslationLength($scope.editGroupData.LanguageTranslation, $scope.editGroupData.Type)
            let deleteCopy = angular.copy(RefinedActionGroupsWithRows[$scope.editGroupData.ProcessActionGroupId]);
            //need to send the group data like below for the function to work properly.
            let params = {
                group: undefined
            };
            params.group = $scope.editGroupData;
            if (copyGroupData.ProcessInGroup != $scope.editGroupData.ProcessInGroup) {
                DeleteFunctionForRows(deleteCopy, true);
            }
            if ($scope.oldType == 1 && $scope.editGroupData.Type == 2) {
                DeleteFunctionForRows(deleteCopy, true);
                AddNewActionRow($scope.editGroupData.ProcessActionGroupId, $scope.editGroupData.Type, $scope.editGroupData.ProcessInGroup);
            } else if ($scope.oldType == 2 && $scope.editGroupData.Type == 1) {
                DeleteFunctionForRows(deleteCopy, true);
            }
            if ($scope.editGroupData.Type == 2) {
                if (copyGroupData.ProcessInGroup != $scope.editGroupData.ProcessInGroup) {
                    AddNewActionRow($scope.editGroupData.ProcessActionGroupId, $scope.editGroupData.Type, $scope.editGroupData.ProcessInGroup);
                }
            }
            ChangeCreateGroupData(params);
            $scope.closeEditDialog();
        }

        $scope.modifications = {
            create: false,
            discard: true
        };
        
        $scope.discardChanges = function () {
            $scope.editGroupData = copyGroupData;
            
            
            $scope.closeEditDialog();
        }
        $scope.closeEditDialog = function () {
            MessageService.closeDialog();
        }
    }
})();