﻿(function () {
        'use strict';

        angular
            .module('core.settings')
            .config(ProcessActionsConfig)
            .controller('ProcessActionsController', ProcessActionsController)
            .controller('ProcessActionRowsController', ProcessActionRowsController)
            .controller('ProcessActionsDialogSettingsController', ProcessActionsDialogSettingsController)
            .service('ProcessActionsService', ProcessActionsService);

        ProcessActionsConfig.$inject = ['ViewsProvider'];

        function ProcessActionsConfig(ViewsProvider) {
            ViewsProvider.addView('settings.processActions', {
                controller: 'ProcessActionsController',
                template: 'settings/processActions/processActions.html',
                resolve: {
                    ProcessActions: function (ProcessActionsService, StateParameters) {
                        return ProcessActionsService.getProcessActions(StateParameters.process);
                    },
                    ProcessActionDialogs: function (SettingsService, StateParameters) {
                        return SettingsService.getDialogSettings(StateParameters.process);
                    },
                    UnUsedProcessActions: function (ProcessActionsService, StateParameters) {
                        return ProcessActionsService.getUnusedActions(StateParameters.process);
                    },
                    ButtonsForActions: function (ProcessActionsService, StateParameters) {
                        return ProcessActionsService.getButtonsForActions(StateParameters.process);
                    },
                    ProcessConditions: function (ProcessService, StateParameters) {
                        return ProcessService.getConditions(StateParameters.process);
                    },
                    ItemColumns: function (ProcessActionsService, StateParameters) {
                        let result = [];
                        return ProcessActionsService.getItemColumns(StateParameters.process).then(function (cols) {
                            for (let ic of cols) {
                                if (ic.Name.indexOf('_item_id') > -1) {
                                    result.push(ic);
                                }
                            }
                            return result;
                        });
                    }
                }
            });
            ViewsProvider.addView('settings.processActionRows', {
                controller: 'ProcessActionRowsController',
                template: 'settings/processActions/processActionRows.html',
                resolve: {
                    ProcessConditions: function (ProcessService, StateParameters) {
                        return ProcessService.getConditions(StateParameters.process);
                    },

                    HttpTemplates: function (PortfolioService, ViewConfigurator, Translator, ViewService) {
                        let notification = ViewConfigurator.getGlobal('httpRequest').Params
                        if (angular.isUndefined(notification) || !notification.httpRequestsPortfolioId) {
                            return ViewService.fail('Global http requests portfolio configuration is missing');
                        }
                        return PortfolioService.getSimplePortfolioData('notification', ViewConfigurator.getGlobal('httpRequest').Params.httpRequestsPortfolioId, {limit: 0}).then(function (notificationsRaw) {
                            let notifications = [];
                            angular.forEach(notificationsRaw.rowdata, function (notification) {
                                notifications.push({
                                    'Translation': Translator.translate(notification.name_variable),
                                    'ItemId': notification.item_id
                                });
                            });

                            return notifications;
                        });
                    },
                    HttpParsers: function (ApiService) {
                        return ApiService.v1api('settings/RegisteredHttpResponseParsers').get();
                    },

                    UserGroups: function (SettingsService) {
                        return SettingsService.UserGroups('user_group');
                    },

                    Views: function (SettingsService, StateParameters, Views, Translator) {
                        let views = [];
                        return SettingsService.ProcessTabs(StateParameters.process).then(function (tabs) {
                            angular.forEach(tabs, function (tab) {
                                views.push({
                                    viewId: 'process.tab_' + tab.ProcessTabId,
                                    viewName: 'Tab: ' + Translator.translation(tab.LanguageTranslation) + " " + "'" + tab.MaintenanceName + "'"
                                });
                            });
                            angular.forEach(Views.getFeatures(), function (f) {
                                views.push({viewId: f.view, viewName: 'Feature: ' + f.name});
                            });
                            return views;
                        });
                    },

                    EmailItemColumns: function (SettingsService, StateParameters, $q, Translator) {
                        let d = $q.defer();

                        SettingsService.ItemColumns(StateParameters.process).then(function (itemColumns) {
                            let promises = [];
                            let retval = [];
                            angular.forEach(itemColumns, function (col) {
                                let deffered = $q.defer();
                                if (col.DataType == 6) {
                                    if (col.DataAdditional != null && col.DataAdditional.length > 0) {
                                        SettingsService.ItemColumns(col.DataAdditional).then(function (listCols) {
                                            angular.forEach(listCols, function (listCol) {
                                                if (listCol.DataType == 5) {
                                                    listCol.ItemColumnId = col.ItemColumnId + ";" + col.DataAdditional + ";" + listCol.ItemColumnId;
                                                    listCol.Translation = Translator.translation(col.LanguageTranslation) + " - " + Translator.translation(listCol.LanguageTranslation);
                                                    retval.push(listCol);
                                                }
                                            });
                                            deffered.resolve(listCols);
                                        });
                                        promises.push(deffered.promise);
                                    }
                                } else if (col.DataType == 0 || col.DataType == 1 || col.DataType == 5 || (col.DataType == 16 && col.SubDataType == 5) || (col.DataType == 20 && col.SubDataType == 5)) {
                                    retval.push(col);
                                }
                            });
                            $q.all(promises).then(function () {
                                d.resolve(retval);
                            });
                        });


                        return d.promise;
                    },
                    ProcessActions: function (ProcessActionsService, StateParameters) {
                        return ProcessActionsService.getProcessActions(StateParameters.process);
                    },
                    ProcessActionRows: function (ProcessActionsService, StateParameters) {
                        let result = [];

                        return ProcessActionsService.getProcessActionRows(StateParameters.process, StateParameters.ProcessActionId).then(function (data) {
                            angular.forEach(data, function (key) {

                                _.each(key.ItemColumnIds, function (p, ind) {
                                    key.ItemColumnIds[ind] = parseInt(p);
                                })

                                try {
                                    if (typeof JSON.parse(key.Value) === 'object' || typeof JSON.parse(key.Value) === 'array') {
                                        key.Value = JSON.parse(key.Value);
                                    }
                                    if (typeof JSON.parse(key.Value2) === 'object' || typeof JSON.parse(key.Value2) === 'array') {
                                        key.Value2 = JSON.parse(key.Value2);
                                    }
                                } catch (e) {

                                }
                                result.push(key);
                            });

                            return result;
                        });
                    },

                    GroupStuff: function (ProcessActionsService, StateParameters) {
                        let s = {'groups': null, 'processes': null};
                        let processesArray = [];
                        return ProcessActionsService.getProcessActionRowsGroups(StateParameters.process, StateParameters.ProcessActionId).then(function (data) {
                            angular.forEach(data, function (value) {

                                if (!_.includes(processesArray, value.ProcessInGroup) && value.ProcessInGroup != null) {
                                    processesArray.push(value.ProcessInGroup);
                                }
                            })

                            s.groups = data;
                            s.processes = processesArray;

                            return s;
                        });
                    },
                    ItemColumns: function (ProcessActionsService, StateParameters, SettingsService, Common, Translator) {
                        return SettingsService.ItemColumns(StateParameters.process).then(function (data) {
                            let result = [];
                            result["itemColumns"] = Array<Client.ItemColumn>();
                            result["itemColumnsOptions"] = [];
                            result["statusColumns"] = [];
                            result["linkColumns"] = {};
                            angular.forEach(data, function (key) {
                                if (key.LanguageTranslation != undefined && !_.isEmpty(key.LanguageTranslation))
                                    key.Translation = Translator.translation(key.LanguageTranslation) + ' [' + Common.getDataTypeTranslation(key.DataType) + ']' + ' ' + '(ItemColumnId:' + key.ItemColumnId + ')';
                                else {
                                    key.Translation = Translator.translation(key.Name) + ' [' + Common.getDataTypeTranslation(key.DataType) + ']' + ' ' + '(ItemColumnId:' + key.ItemColumnId + ')';
                                }
                                result["itemColumns"].push(key);
                                result["itemColumnsOptions"][key.ItemColumnId] = {
                                    DataType: key.DataType,
                                    DataAdditional: key.DataAdditional
                                };
                                if (key.DataType == 0 || key.DataType == 1) {
                                    result["statusColumns"][key.ItemColumnId] = key;
                                }
                                if (key.DataType == 5) {
                                    result["linkColumns"][key.ItemColumnId] = key;
                                }

                            });
                            return result;
                        });
                    },
                    CustomActionsDict: function (ApiService) {
                        return ApiService.v1api('settings/RegisteredCustomActions').get();
                    },

                    UserColumns: function (ProcessActionsService, StateParameters, SettingsService, Common, Translator) {
                        return SettingsService.ItemColumns('user').then(function (data) {
                            let UserColsByDataType = {};


                            _.each(data, function (o) {
                                if (!UserColsByDataType.hasOwnProperty(o.DataType)) {
                                    UserColsByDataType[o.DataType] = [];
                                }
                                UserColsByDataType[o.DataType].push(o);
                            });
                            return UserColsByDataType;

                        });
                    },
                    ActionViews: function (SettingsService, StateParameters, Translator, Views) {
                        let actionViews = [];
                        let allFeatures = Views.getFeatures();
                        return SettingsService.ProcessTabs(StateParameters.process).then(function (tabs) {
                            angular.forEach(tabs, function (tab) {
                                actionViews.push({
                                    viewId: 'tab_' + tab.ProcessTabId,
                                    viewName: 'Tab: ' + Translator.translation(tab.LanguageTranslation) + " " + "'" + tab.MaintenanceName + "'"
                                });
                            });


                            return SettingsService.getProcessFeatures(StateParameters.process).then(function (features_) {
                                _.each(features_, function (feature) {
                                    if (_.findIndex(allFeatures, function (o) {
                                        return o.view == feature.state;
                                    }) != -1) {
                                        actionViews.push({
                                            viewId: feature.state,
                                            viewName: 'Feature:' + '' + _.find(allFeatures, function (o) {
                                                return o.view == feature.state
                                            }).translation
                                        })
                                    }
                                });
                                return actionViews;
                            });
                        });
                    },

                    Notifications: function (ViewConfigurator, Translator, ViewService, PortfolioService) {
                        let notification = ViewConfigurator.getGlobal('notification').Params;
                        if (angular.isUndefined(notification) || !notification.emailsPortfolioId) {
                            return ViewService.fail('Global email portfolio configuration is missing');
                        }
                        return PortfolioService.getSimplePortfolioData('notification', ViewConfigurator.getGlobal('notification').Params.emailsPortfolioId, {limit: 0}).then(function (notificationsRaw) {
                            let notifications = [];
                            angular.forEach(notificationsRaw.rowdata, function (notification) {
                                notifications.push({
                                    'Translation': Translator.translate(notification.name_variable),
                                    'ItemId': notification.item_id
                                });
                            });

                            return notifications;
                        });
                    },
                    Processes: function (ProcessesService) {
                        return ProcessesService.get();
                    },

                    LinkColumns: function(ProcessActionsService, StateParameters){
                        return ProcessActionsService.getLinkColumns(StateParameters.process);
                    }

                },
                afterResolve: {
                    CopyItemColumns: function (ProcessActionRows, $q, ApiService, StateParameters, Common) {
                        let d = $q.defer();
                        let foundCreateAction = false;
                        for (let i = 0; i < ProcessActionRows.length; i++) {
                            if (!foundCreateAction && ProcessActionRows[i].ProcessActionType === 3) {
                                foundCreateAction = true;
                                ApiService.v1api('meta/ItemColumns').get([ProcessActionRows[i].Value]).then(function (cols) {
                                    angular.forEach(cols, function (c) {
                                        if (c.DataType == 5 && c.DataAdditional == StateParameters.process && c.itemColumnId != null) {
                                            c.Translation = Common.getTranslatin(c.LanguageTranslation) + ' [' + Common.getDataTypeTranslation(c.DataType) + ']';
                                        }
                                    });
                                    d.resolve(cols);
                                });
                            }
                        }
                        if (!foundCreateAction) {
                            return [];
                        }
                        return d.promise;
                    },


                    Lists: function (ApiService, StateParameters) {
                        let m = ApiService.v1api('settings/ProcessActions/Rows');
                        return m.get([StateParameters.process, 'list_items']);
                    },

                    CustomActions: function (CustomActionsDict) {
                        let options = [];
                        let values = {};

                        angular.forEach(CustomActionsDict, function (value, key) {
                            options.push(key);
                            values[key] = value;
                        });
                        return {options: options, values: values};
                    },

                    ProcessActionRowsGroups: function (GroupStuff) {
                        return GroupStuff.groups;
                    },


                    ItemColumnsByProcess: function (GroupStuff, SettingsService, $q, Translator, Common) {

                        let promises = [];
                        let columns = {};
                        _.each(GroupStuff.processes, function (process) {
                            promises.push(SettingsService.ItemColumns(process).then(function (data) {
                                angular.forEach(data, function (key) {
                                    key.Translation = Translator.translation(key.LanguageTranslation) + ' [' + Common.getDataTypeTranslation(key.DataType) + ']' + ' ' + '(ItemColumnId:' + key.ItemColumnId + ')';

                                });
                                columns[process] = data;
                            }))
                        });
                        return $q.all(promises).then(function () {
                            return columns;
                        })
                    }
                }
            });
            ViewsProvider.addView('settings.processActions.dialogEditor', {
                controller: 'ProcessActionsDialogSettingsController',
                template: 'settings/processActions/processActionsDialog.html',
                resolve: {
                    Languages: function (SettingsService) {
                        return SettingsService.SystemList('LANGUAGES');
                    },
                    Translations: function (SettingsService, StateParameters) {
                        return SettingsService.getProcessActionDialogs(StateParameters.process, StateParameters.ProcessActionDialogId, 0);
                    },
                    ProcessContainers: function(SettingsService, StateParameters){
                        return SettingsService.ProcessContainers(StateParameters.process);
                    },
                    Dialog: function(SettingsService, StateParameters){
                        return SettingsService.getProcessActionDialogsNew(StateParameters.process, StateParameters.ProcessActionDialogId);
                    }
                }
            });

        }

        ProcessActionsController.$inject = ['$scope', 'ProcessActionsService', 'MessageService', 'SaveService', 'Common', 'ViewService', 'StateParameters', 'ProcessActions', 'ProcessActionDialogs', 'Translator', 'UnUsedProcessActions', 'ButtonsForActions', 'ProcessConditions', 'ItemColumns'];

        function ProcessActionsController($scope, ProcessActionsService, MessageService, SaveService, Common, ViewService, StateParameters, ProcessActions, ProcessActionDialogs, Translator, UnUsedProcessActions, ButtonsForActions, ProcessConditions, ItemColumns) {

            $scope.query = {
                limit: 25,
                page: 1
            };
            $scope.itemColumns = ItemColumns;
            $scope.ButtonsForActions = ButtonsForActions;
            $scope.unUsedActions = UnUsedProcessActions;
            $scope.ProcessConditions = ProcessConditions;
            $scope.unUsedIds = [];
            $scope.usedActions = [];
            let buttonFields = [];

            angular.forEach(UnUsedProcessActions, function (value, key) {
                //Why JSON parse? Input isnt even rich text on the web page. Causes a parse error
                if (value.ActionHistoryText && value.ActionHistoryText.length > 0) value.ActionHistoryText = JSON.parse(value.ActionHistoryText);
                $scope.unUsedIds.push(value.ProcessActionId);
            });

            angular.forEach(ProcessActions, function (value, key) {
                if (!_.includes($scope.unUsedIds, value.ProcessActionId)) {
                    //Why JSON parse? Input isnt even rich text on the web page. Causes a parse error
                    if (value.ActionHistoryText && value.ActionHistoryText.length > 0) value.ActionHistoryText = JSON.parse(value.ActionHistoryText);
                    $scope.usedActions.push(value);
                }
            });

            let buttonItem = {};

            angular.forEach($scope.usedActions, function (value, key) {
                angular.forEach(ButtonsForActions, function (value2, key2) {
                    if (value2.action_id == value.ProcessActionId) {
                        buttonItem[value2.item_column_id] = value2.button;
                        buttonFields.push(buttonItem);
                        buttonItem = {};
                    }
                });
                value.Buttons = buttonFields;
                buttonFields = [];
            });

            $scope.Items = ProcessActions;
            $scope.dialogs = ProcessActionDialogs;
            $scope.selectedRows = [];
            $scope.selectedDialogRows = [];
            $scope.edit = true;
            $scope.selectedAction = null;
            $scope.selectedDialog = null;
            $scope.dialogOptions = [{name: 'INFO', value: 1}, {name: 'CONFIRM', value: 2}, {name: 'CONFIRM_DIALOG', value: 3}, {name: 'CONTAINER_DIALOG', value: 4}];

            let newValuesArray = [];
            let changedItems = [];
            let newDialogValuesArray = [];
            let changedDialogItems = [];
            let data = {};

            SaveService.subscribeSave($scope, function () {
                if (changedItems.length > 0 || changedDialogItems.length > 0) {
                    saveProcessActions();
                }
            });

            $scope.addAction = function () {
                data = {Translation: null};

                let save = function () {
                    addActionSave();
                };

                let content = {
                    buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
                    inputs: [{type: 'translation', label: 'LIST_NAME', model: 'LanguageTranslation'}],
                    title: 'PROCESS_ACTIONS_TITLE'
                };
                MessageService.dialog(content, data);
            };

            $scope.addActionDialog = function () {
                data = {name: null};

                let save = function () {
                    addActionDialogSave();
                };

                let options = [{name: 'INFO', value: 1}, {name: 'CONFIRM', value: 2}, {name: 'CONFIRM_DIALOG', value: 3}, {name: 'CONTAINER_DIALOG', value: 4}];
                let content = {
                    buttons: [{text: 'DISCARD'}, {type: 'primary', onClick: save, text: 'SAVE'}],
                    inputs: [{type: 'translation', label: 'LIST_NAME', model: 'LanguageTranslation'}, {
                        type: 'select',
                        model: 'Type',
                        label: 'TYPE',
                        options: options,
                        optionName: 'name',
                        optionValue: 'value',
                        singleValue: true
                    }],
                    title: 'CREATE_NEW_DIALOG'
                };
                MessageService.dialog(content, data);
            };

            let addActionSave = function () {
                data.process = StateParameters.process;
                ProcessActionsService.ProcessActionAdd(data.process, data).then(function (response) {
                    $scope.unUsedActions.push(response);
                    data = {};
                });
            };

            let addActionDialogSave = function () {
                data.process = StateParameters.process;
                ProcessActionsService.ProcessActionDialogAdd(data.process, data).then(function (response) {
                    $scope.dialogs.push(response);
                    data = {};
                });
            };

            let saveProcessActions = function () {
                let processArray = [];
                let processDialogArray = [];
                let l = changedItems.length;
                for (let i = 0; i < l; i++) {
                    let item = changedItems[i];
                    let action = item.table == 2 ? angular.copy($scope.usedActions[item.index]) : angular.copy($scope.unUsedActions[item.index]);
                    action.ActionHistoryText = JSON.stringify(action.ActionHistoryText);
                    processArray.push(action);
                }
                angular.forEach(changedDialogItems, function (key) {
                    processDialogArray.push($scope.dialogs[key.index]);
                });

                if (changedItems.length > 0) {
                    ProcessActionsService.ProcessActionSave(StateParameters.process, processArray).then(function () {
                        newValuesArray = [];
                        changedItems = [];
                    });
                }

                if (changedDialogItems.length > 0) {
                    ProcessActionsService.ProcessActionDialogSave(StateParameters.process, processDialogArray).then(function () {
                        newDialogValuesArray = [];
                        changedDialogItems = [];
                    });
                }
            };

            $scope.deleteProcessActions = function () {
                MessageService.confirm("Are you sure you want to delete this?", function () {
                    if ($scope.selectedRows.length > 0) {
                        let deleteIds = Common.convertArraytoString($scope.selectedRows, 'ProcessActionId');
                        ProcessActionsService.ProcessActionDelete(StateParameters.process, deleteIds);
                        let index;
                        angular.forEach($scope.selectedRows, function (row) {
                            if (row.table == 1) {
                                let index = Common.getIndexOf($scope.unUsedActions, row.ProcessActionId, 'ProcessActionId');
                                $scope.unUsedActions.splice(index, 1);
                            } else if (row.table == 2) {
                                let index = Common.getIndexOf($scope.usedActions, row.ProcessActionId, 'ProcessActionId');
                                $scope.usedActions.splice(index, 1);
                            }
                        });
                        $scope.selectedRows = [];
                    }
                    if ($scope.selectedDialogRows.length > 0) {
                        let deleteIds = Common.convertArraytoString($scope.selectedDialogRows, 'ProcessActionDialogId');
                        ProcessActionsService.ProcessActionDialogDelete(StateParameters.process, deleteIds);
                        angular.forEach($scope.selectedDialogRows, function (row) {
                            let index = Common.getIndexOf($scope.dialogs, row.ProcessActionDialogId, 'ProcessActionDialogId');
                            $scope.dialogs.splice(index, 1);
                        });
                        $scope.selectedDialogRows = [];
                    }

                    MessageService.deleteSuccess();
                });
            };

            ViewService.footbar(StateParameters.ViewId, {
                template: 'settings/processActions/mainFooter.html',
                scope: $scope
            });


            $scope.header = {
                title: 'PROCESS_ACTIONS'
            };
            StateParameters.activeTitle = $scope.header.title;

            $scope.showAction = function (ProcessActionId, index, langvariable) {
                let userLanguageTranslation = Translator.translation(langvariable);
                let params = StateParameters;
                params.index = index;
                params.ProcessActionId = ProcessActionId;
                params.UserLanguageTranslation = userLanguageTranslation;

                $scope.selectedAction = index;
                ViewService.view('settings.processActionRows', {params: params}, {split: true});
            };

            $scope.showActionNew = function (ProcessActionId, index, langvariable) {
                let userLanguageTranslation = Translator.translation(langvariable);
                let params = StateParameters;
                params.index = index;
                params.ProcessActionId = ProcessActionId;
                params.UserLanguageTranslation = userLanguageTranslation;

                $scope.selectedAction = index;
                ViewService.view('settings.processActionRows', {params: params}, {split: true});
            };

            $scope.changeValue = function (params) {
                newValuesArray.push({index: params.index, ProcessActionId: params.process_action_id, table: params.table});
                let noDuplicateObjects = {};
                let l = newValuesArray.length;

                for (let i = 0; i < l; i++) {
                    let item = newValuesArray[i];
                    noDuplicateObjects[item.ProcessActionId] = item;
                }

                let nonDuplicateArray = [];
                for (let item in noDuplicateObjects) {
                    nonDuplicateArray.push(noDuplicateObjects[item]);
                }
                changedItems = nonDuplicateArray;
            };

            $scope.changeDialogValue = function (params) {
                newDialogValuesArray.push({index: params.index, ProcessActionDialogId: params.ProcessActionDialogId});
                let noDuplicateObjects = {};
                let l = newDialogValuesArray.length;

                for (let i = 0; i < l; i++) {
                    let item = newDialogValuesArray[i];
                    noDuplicateObjects[item.ProcessActionDialogId] = item;
                }

                let nonDuplicateArray = [];
                for (let item in noDuplicateObjects) {
                    nonDuplicateArray.push(noDuplicateObjects[item]);
                }
                changedDialogItems = nonDuplicateArray;
            };

            $scope.changeDialogText = function (ProcessActionDialogId, index, dialogType, transvariable) {
                let params = StateParameters;
                let translation = Translator.translation(transvariable);
                params.index = index;
                params.ProcessActionDialogId = ProcessActionDialogId;
                params.dialogType = dialogType;
                params.UserLanguageTranslation = translation;
                $scope.selectedDialog = index;
                ViewService.view('settings.processActions.dialogEditor', {params: params}, {split: true});
            };
        }


        ProcessActionRowsController.$inject = ['$q', 'Notifications', 'ItemColumnsByProcess', '$rootScope', 'ProcessConditions', 'ActionViews', 'ItemColumns', 'SaveService', 'Common', 'SettingsService', '$scope', 'ProcessActionRowsGroups', 'ViewService', 'ProcessActionsService', 'ProcessActions',
            'ProcessActionRows', 'StateParameters', 'Translator', 'MessageService', 'CopyItemColumns', 'ProcessesService', 'ProcessService', 'ListsService', 'Lists', 'UserColumns', 'EmailItemColumns', 'UserGroups', 'Views', '$filter', '$timeout', 'Processes', 'HttpTemplates', 'HttpParsers', 'CustomActions', 'LinkColumns'];

        function ProcessActionRowsController($q, Notifications, ItemColumnsByProcess, $rootScope, ProcessConditions, ActionViews, ItemColumns, SaveService, Common, SettingsService, $scope, ProcessActionRowsGroups, ViewService, ProcessActionsService, ProcessActions, ProcessActionRows,
                                             StateParameters, Translator, MessageService, CopyItemColumns, ProcessesService, ProcessService, ListsService, Lists, UserColumns, EmailItemColumns, UserGroups, Views, $filter, $timeout, Processes, HttpTemplates, HttpParsers, CustomActions, LinkColumns) {

            let headerText = Translator.translate('PROCESS_ACTIONS_TITLE') + " - " + StateParameters.UserLanguageTranslation;
            $scope.header = {
                title: headerText,
                menu: []
            };
            StateParameters.activeTitle = $scope.header.title;

            $scope.selectedLinkColumns = LinkColumns;

            $scope.customActions = CustomActions.options;
            $scope.customActionOptions = CustomActions.values;
            $scope.customAction = [{name: 'PROCESS_CUSTOM_ACTION', value: 12}];

            $scope.openStateSetDialog = function (actionRow, index) {
                let params = {
                    template: 'settings/processActions/mailActionViews.html',
                    controller: 'MailActionViewsController',
                    locals: {
                        StateParameters: StateParameters,
                        ActionRow: actionRow,
                        ItemColumns: ItemColumns,
                        Features: $scope.views,
                        Processes: Processes,
                        Index: index
                    }
                };
                MessageService.dialogAdvanced(params);
            };

            $scope.checkIfUserColumn = function (id) {
                let result = _.find($scope.userOptions, function (value, key) {
                    if (value.ItemColumnId == id) {
                        return true;
                    }
                });
                return result;
            };
            $scope.listCols = {};
            $scope.ungroupedRows = [];
            $scope.httpTemplates = $filter('orderBy')(HttpTemplates, function (h) {
                return Translator.translation(h.LanguageTranslation)
            });
            $scope.httpParsers = HttpParsers;
            _.each(ProcessActionRows, function (row) {
                $scope.listCols[row.ProcessActionRowId] = [];
                if (row.ProcessActionGroupId == 0) $scope.ungroupedRows.push(row);
            });

            $scope.itemColumns = ItemColumns.itemColumns;
            $scope.listItemColumns = _.filter($scope.itemColumns, {'DataType': 6});
            $scope.httpAction = [{name: 'HTTP_ACTION', value: 11}];

            let saveProcessActionsRow = function () {
                let processActionsRowArray = [];
                let l = nongroupedChanged.length;
                for (let i = 0; i < l; i++) {
                    let item = nongroupedChanged[i];
                    processActionsRowArray.push(_.find($scope.ungroupedRows, function (o) {
                        return o.ProcessActionRowId == item.ProcessActionRowId
                    }));
                }
                // this madness breaks value2 saving - jka 29.9.2021 - bsa approved
                _.each(processActionsRowArray, function(o){
                   if(o.Value2 != null && o.Value2 != "" && typeof o.Value2 != 'undefined') o.Value2 = JSON.stringify(o.Value2);
                })
                return ProcessActionsService.ProcessActionRowSave(StateParameters.process, processActionsRowArray).then(function () {
                    newValuesArray = [];
                    nongroupedChanged = [];
                });
            };

            let itemColumnDatatypes = {};
            let nongroupedChanged = [];

            let newValuesArray = [];

            $scope.notificationTypes = [{Type: 1, Name: 'TYPE_EMAIL'},
                {Type: 2, Name: 'TYPE_NOTIFICATION'},
                {Type: 3, Name: 'TYPE_EMAIL_AND_NOTIFICATION'}];

            $scope.changeValueNonGroup = function (params) {
                if (params.ValueType == 3 && params.action.ProcessActionType != 8) {
                    $scope.findDataTypeFields(params.action.ItemColumnId);
                } else if (params.ValueType == 3 && params.action.ProcessActionType == 8) {
                    $scope.findDataTypeFieldsCopy(params.action.ItemColumnId);
                }
                newValuesArray.push({index: params.index, ProcessActionRowId: params.ProcessActionRowId});
                let noDuplicateObjects = {};
                let l = newValuesArray.length;

                for (let i = 0; i < l; i++) {
                    let item = newValuesArray[i];
                    noDuplicateObjects[item.ProcessActionRowId] = item;
                }

                let nonDuplicateArray = [];
                for (let item in noDuplicateObjects) {
                    nonDuplicateArray.push(noDuplicateObjects[item]);
                }
                nongroupedChanged = nonDuplicateArray;

                if ($scope.ungroupedRows[params.index].ProcessActionType === 3 && $scope.ungroupedRows[params.index].Value != null) {
                    $scope.copyColsLoaded = false;
                    ProcessService.getColumns($scope.processActions[params.index].Value).then(function (cols) {
                        $scope.copyItemColumns = cols;
                        $scope.LinkColumns = ItemColumns.linkColumns;

                        let copyColiColOptions = [];
                        angular.forEach(cols, function (c) {
                            if (c.DataType == 5 && c.DataAdditional == StateParameters.process) {
                                c.Translation = Translator.translation(c.LanguageTranslation) + ' [' + Common.getDataTypeTranslation(c.DataType) + ']';
                                $scope.LinkColumns[c.ItemColumnId] = c;
                            }
                            copyColiColOptions[c.ItemColumnId] = {
                                DataType: c.DataType,
                                DataAdditional: c.DataAdditional
                            };
                            itemColumnDatatypes[c.ItemColumnId] = c.DataType;

                        });
                        $scope.copyColiColOptions = copyColiColOptions;
                        $scope.userOptionsCopy = _.filter(cols, {'DataType': 5, 'DataAdditional': 'user'});
                        $scope.dateOptionsCopy = _.filter(cols, {'DataType': 3});

                        $scope.copyColsLoaded = true;
                    });
                }

                if ($scope.ungroupedRows[params.index].ProcessActionType === 10 && $scope.ungroupedRows[params.index].Value != null) {
                    $scope.processColsLoaded[$scope.processActions[params.index].Value] = false;
                    if (angular.isUndefined($scope.processCols[$scope.processActions[params.index].Value])) {
                        ProcessService.getColumns($scope.processActions[params.index].Value).then(function (cols) {
                            $scope.processCols[$scope.processActions[params.index].Value] = cols;
                            $scope.processColsLoaded[$scope.processActions[params.index].Value] = true;
                        });
                    } else {
                        $scope.processColsLoaded[$scope.processActions[params.index].Value] = true;
                    }
                }
                SaveService.tick();
            };

            $scope.getListColumns = function (params) {
                $scope.listCols[params.ProcessActionRowId] = [];
                let selectedList: Client.ItemColumn = $scope.listItemColumns.find(obj => obj.ItemColumnId == params.ListProcess);
                if (selectedList.DataAdditional != null) {
                    ProcessService.getColumns(selectedList.DataAdditional, false).then(function (cols) {
                        $scope.listCols[params.ProcessActionRowId] = cols;
                    });
                }
            };

            $scope.views = Views;
            ViewService.footbar(StateParameters.ViewId, {
                template: 'settings/processActions/processActionsFooter.html',
                scope: $scope
            });
            let data = {};
            $scope.listItems = {};
            $scope.mailAction = [{name: 'WORKINGTIME_TRACKING_EMAIL_SEND', value: 5}];
            $scope.EmailItemColumns = EmailItemColumns;
            $scope.UserGroups = UserGroups;
            $scope.emailTemplates = $filter('orderBy')(Notifications, function (e) {
                return Translator.translation(e.LanguageTranslation)
            });

            $scope.addProcessAction = function (type) {

                data.ProcessActionId = StateParameters.ProcessActionId;
                if (type == 2) {
                    data.ProcessActionType = 5;
                }
                if (type == 3) {
                    data.ProcessActionType = 11;
                }
                if (type == 4) {
                    data.ProcessActionType = 12;
                }
                ProcessActionsService.ProcessActionRowAdd(StateParameters.process, data).then(function (response) {
                    response.Value = "";
                    $scope.ungroupedRows.push(response);
                    $scope.listItems[response.ProcessActionRowId] = [];
                    data = {};
                });
            };

            $scope.itemColumnsByProcess = ItemColumnsByProcess;
            $scope.processActionRows = ProcessActionRows;

            _.each($scope.processActionRows, function (paRow) {
                if (paRow.ListProcess != null && paRow.ListProcess.length > 0) {
                    let parameters = {'ListProcess': paRow.ListProcess, 'ProcessActionRowId': paRow.ProcessActionRowId};
                    $scope.getListColumns(parameters);
                }
            });

            $scope.processActionGroups = ProcessActionRowsGroups;
            $scope.actions = ProcessActions;
            $scope.listOptions = Lists;
            $scope.showDefault = true;
            $scope.userColumnsByDataType = UserColumns;

            $scope.userColumns = [];
            _.each(UserColumns, function (value) {
                _.each(value, function (o) {
                    $scope.userColumns.push(o);
                });
            });
            $scope.ItemColumnsOptions = ItemColumns.itemColumnsOptions;
            ProcessesService.get().then(function (o) {
                $scope.createGroupProcesses = o;
            });

            //Initialise current_state id into a variable.
            _.each($scope.itemColumns, function (ic) {
                if (ic.Name == 'current_state') $scope.currentStateId = ic.ItemColumnId;
            });

            let findProcessItemColumns = function (processName) {
                if (!$scope.itemColumnsByProcess.hasOwnProperty(processName)) {
                    return SettingsService.ItemColumns(processName).then(function (data) {
                        let result = [];
                        angular.forEach(data, function (key) {
                            key.Translation = Translator.translation(key.LanguageTranslation) + ' [' + Common.getDataTypeTranslation(key.DataType) + ']' + ' ' + '(ItemColumnId:' + key.ItemColumnId + ')';
                        });
                        $scope.itemColumnsByProcess[processName] = data;
                    })
                }
            }
            $scope.validListColumnsByName = {};
            let ValidateListOptions = function (ItemColumnId: number, processInGroup?: string) {

                let itemColumns = typeof processInGroup != 'undefined' ? $scope.itemColumnsByProcess[processInGroup] : $scope.itemColumns;


                let listProcess = _.find(itemColumns, function (o) {
                    return o.ItemColumnId == ItemColumnId
                }).DataAdditional;

                let result = [];
                let matchingColumns = angular.copy($scope.userColumnsByDataType[6]);
                _.each($scope.userColumnsByDataType[6], function (o) {
                    if (o.DataAdditional == listProcess) {
                        result.push(o);
                    }
                })
                _.each($scope.userColumnsByDataType[16], function (o) {
                    if (o.DataAdditional == listProcess) {
                        result.push(o);
                    }
                })
                $scope.validListColumnsByName[listProcess] = result;
            }
            $scope.itemColumnsByActionType = [];

            //returns the datatype of an item column.

            function initCreateProcessColumns(process) {
                SettingsService.ItemColumns(process).then(function (item) {
                    $scope.itemColumnsByProcess[process] = item;
                });
            }

            initCreateProcessColumns('user');
            $scope.returnDataType = function (o) {
                if (o.ItemColumnId != null && o.ProcessActionType != 3) {
                    if (o.ProcessActionType != 8) {
                        o.$$DataType = _.find($scope.itemColumns, function (i) {
                            return i.ItemColumnId == o.ItemColumnId;
                        }).DataType;
                        // let processInGroup = _.find($scope.processActionGroups, function (i) {
                        //     return i.ProcessActionGroupId == o.ProcessActionGroupId;
                        // }).ProcessInGroup;

                        if (o.$$DataType == 6 || o.$$DataType == 16 && o.ProcessActionType != 3) {
                            o.$$DataAdditional = _.find($scope.itemColumns, function (i) {
                                return i.ItemColumnId == o.ItemColumnId;
                            }).DataAdditional;
                            ValidateListOptions(o.ItemColumnId, undefined);
                        }

                    } else {
                        let processInGroup = _.find($scope.processActionGroups, function (i) {
                            return i.ProcessActionGroupId == o.ProcessActionGroupId;
                        }).ProcessInGroup;

                        o.$$DataType = _.find($scope.itemColumnsByProcess[processInGroup], function (i) {
                            return i.ItemColumnId == o.ItemColumnId;
                        }).DataType;

                        if ((o.$$DataType == 6 || o.$$DataType == 16) && o.ProcessActionType != 3) {
                            o.$$DataAdditional = _.find($scope.itemColumnsByProcess[processInGroup], function (i) {
                                return i.ItemColumnId == o.ItemColumnId;
                            }).DataAdditional;
                            ValidateListOptions(o.ItemColumnId, processInGroup);
                        }
                    }
                } else {

                    if (o.Value != null) {

                        if (o.ProcessActionType == 9) {
                            o.$$DataType = _.find($scope.itemColumns, function (i) {
                                return i.ItemColumnId == o.Name;
                            }).DataType;
                            if (o.$$DataType == 6 || o.$$DataType == 16 && o.ProcessActionType != 3) {
                                o.$$DataAdditional = _.find($scope.itemColumns, function (i) {
                                    return i.ItemColumnId == o.Value;
                                }).DataAdditional;
                                ValidateListOptions(o.Value, undefined);
                            }
                        }
                    }
                }
            }

            //Initialises the $$listOptions
            $scope.listOptionsInitiator = function () {
                _.forEach($scope.refinedActionGroupsWithRows, function (o) {
                    _.forEach($scope.refinedActionGroupsWithRows[o], function (u) {
                        $scope.listOptions[u] = {};
                    });
                })
            }

            //Initialises the $$DataType
            $scope.DataTypeInitiator = function () {
                _.forEach($scope.processActionRows, function (o) {
                    $scope.returnDataType(o)
                });
            };

            $scope.DataTypeInitiator();
            $scope.refinedActionGroupsWithRows = {};
            $scope.refinedActionGroupsWithRows[0] = [];
            angular.forEach($scope.processActionGroups, function (value, key) {
                let groupId = value.ProcessActionGroupId;
                $scope.refinedActionGroupsWithRows[groupId] = [];
            });

            $scope.processConditions = ProcessConditions;
            $scope.listOptionsInitiator();

            angular.forEach($scope.processActionRows, function (v, k) {
                $scope.refinedActionGroupsWithRows[v.ProcessActionGroupId].push(v);
            });


            //Process Action column
            $scope.ActionOptionsCollection = {
                1: [//SET group values
                    {'value': 0, 'name': Translator.translate('ACTIONS_SET_VALUE')},
                    {'value': 1, 'name': Translator.translate('ACTIONS_SET_INCREMENT')},
                    {'value': 2, 'name': Translator.translate('ACTIONS_SET_DECREMENT')},
                    {'value': 9, 'name': Translator.translate('ACTIONS_COPY_IN_PROCESS')}],

                2: [//Create Group values + Others?
                    {'value': 3, 'name': Translator.translate('ACTIONS_CREATE_NEW_PROCESS')},
                    {'value': 4, 'name': Translator.translate("ACTIONS_COPY_FROM_PARENT")},
                    {'value': 8, 'name': Translator.translate('ACTIONS_CREATE_SET')},

                ],

            };
            //Type column
            $scope.ActionValueOptions = {
                //DEFAULT with ALL
                0: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 1, 'name': Translator.translate('ACTIONS_DATE')},
                    {'valueType': 2, 'name': Translator.translate('ACTIONS_CURRENT_USER')},
                    {'valueType': 3, 'name': Translator.translate('ACTIONS_CURRENT_USER_FIELD')},
                    {'valueType': 4, 'name': 'NULL'}],

                //DATE
                1: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 1, 'name': Translator.translate('ACTIONS_DATE')},
                    {'valueType': 4, 'name': 'NULL'}],

                //Special only curr val and val
                2: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 2, 'name': Translator.translate('ACTIONS_CURRENT_USER')},
                    {'valueType': 4, 'name': 'NULL'}],

                //Special only curr user val and val
                3: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 3, 'name': Translator.translate('ACTIONS_CURRENT_USER_FIELD')},
                    {'valueType': 4, 'name': 'NULL'}],

                //Special NO DATE
                4: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 2, 'name': Translator.translate('ACTIONS_CURRENT_USER')},
                    {'valueType': 3, 'name': Translator.translate('ACTIONS_CURRENT_USER_FIELD')},
                    {'valueType': 4, 'name': 'NULL'}],

                //Special user/DATE?
                5: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 1, 'name': Translator.translate('ACTIONS_DATE')},
                    {'valueType': 2, 'name': Translator.translate('ACTIONS_CURRENT_USER')},
                    {'valueType': 4, 'name': 'NULL'}],

                //Special curr user fld + Date
                6: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 1, 'name': Translator.translate('ACTIONS_DATE')},
                    {'valueType': 3, 'name': Translator.translate('ACTIONS_CURRENT_USER_FIELD')},
                    {'valueType': 4, 'name': 'NULL'}],

                //Nothing but Value and NULL
                7: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 4, 'name': 'NULL'}],

                //Nothing but Value and Current user Value
                8: [{'valueType': 0, 'name': Translator.translate('ACTIONS_VALUE')},
                    {'valueType': 3, 'name': Translator.translate('ACTIONS_CURRENT_USER_FIELD')}]
            }

            $scope.validItemColumnsByDataType = {};

            function getItemColumnsByDataType(array, processInGroup?: string) {
                let r = [];
                if (typeof processInGroup == undefined || processInGroup == null) {
                    for (let ic of $scope.itemColumns) {
                        if (_.includes(array, ic.DataType)) {
                            r.push(ic);
                        }
                    }
                } else {
                    for (let ic of $scope.itemColumnsByProcess[processInGroup]) {
                        if (_.includes(array, ic.DataType)) {
                            r.push(ic);
                        }
                    }
                }
                return r;
            }
            // type = either action type or data type.
            function allowedDataTypesByDataType(type: number, isActionType: boolean = false): Array<number> {
                if (isActionType == false) {
                    if (type == 0) return [0, 4,18,21];
                    if (type == 1) return [0, 1, 4,18,21];
                    if (type == 2) return [0, 2, 4,18,21];
                    if (type == 3) return [3];
                    if (type == 4) return [0, 1, 2, 3, 4, 5, 6, 13, 15, 18,21];
                    if (type == 5) return [5];
                    if (type == 6) return [6];
                    if (type == 13) return [13];
                    if (type == 15) return [15];
                    if (type == 16) return [0, 1, 2, 3, 4, 5, 6, 13, 15, 23,18,21];
                    if (type == 20) return [0, 1, 2, 3, 4, 5, 6, 13, 15, 23,18,21];
                    if(type == 10) return [10];
                } else {
                    //set Value (Current process)
                    if (type == 0) return [0, 1, 2, 3, 4, 5, 6, 13, 15, 23, 60,10];
                    //Increase value
                    if (type == 1) return [1];
                    //Decrease value
                    if (type == 2) return [2];
                    //Create new
                    if (type == 3) return [1, 5]; //also needs the 1 (int) fields which give the item_id columns
                    //Copy from Parents' field
                    if (type == 4) return [0, 1, 2, 3, 4, 5, 6, 10, 13, 15, 16, 20, 18,21];
                    //Set Value IN new PROCESS
                    if (type == 8) return [0, 1, 2, 3, 4, 5, 6, 13, 15, 23, 60];
                    //Copy IN current process
                    if (type == 9) return [0, 1, 2, 3, 4, 5, 6, 10, 13, 15, 16, 20];

                    //Unusual types (additional)
                    if (type == 10) return [0, 1, 2, 3, 4, 5, 6, 15, 16, 20];
                    if (type == 99) return [6];
                }
            }

            $scope.findDataType = function (itemColumnId: number, tag: string) {
                let array = [];
                if (tag == 'user') {
                    array = $scope.userColumns;
                } else {
                    array = $scope.itemColumns;
                }
                let p = _.find(array, function (o) {
                    return o.ItemColumnId == itemColumnId
                })
                return typeof p != 'undefined' ? p.DataType : null
            }
            $scope.findValidItemColumns = function (dataType: number, processInGroup?: string) {
                if ($scope.validItemColumnsByDataType[dataType] == undefined) {
                    $scope.validItemColumnsByDataType[dataType] = getItemColumnsByDataType(allowedDataTypesByDataType(dataType, false), processInGroup);
                    return $scope.validItemColumnsByDataType[dataType];
                }
                return $scope.validItemColumnsByDataType[dataType];
            }

            $scope.currentStateOptions = ActionViews;
            _.forEach($scope.ActionOptionsCollection[0], function (val: number, key: number) {
                $scope.itemColumnsByActionType[key] = [];
            });

            function initializeActionTypes(array) {
                for (let at of [0, 1, 2, 3, 4, 8, 9, 10, 99]) {
                    array[at] = getItemColumnsByDataType(allowedDataTypesByDataType(at, true))
                }
            }

            initializeActionTypes($scope.itemColumnsByActionType);

            $scope.realDataDict = {};
            _.forEach(ProcessActionRows, function (value, key) {
                if (!$scope.realDataDict.hasOwnProperty(value.ProcessActionGroupId)) {
                    $scope.realDataDict[value.ProcessActionGroupId] = [];
                }
                $scope.realDataDict[value.ProcessActionGroupId].push(value);
            });


            //adding new rows to groups
            $scope.addNewActionRow = function (value: number, groupType: number, chosenProcessId: number) {
                let data = {ProcessActionId: StateParameters.ProcessActionId, ProcessActionGroupId: value};

                if (groupType == 1) {
                    ProcessActionsService.ProcessActionRowAdd(StateParameters.process, data).then(function (response) {
                        response.Value = "";

                        if(!$scope.refinedActionGroupsWithRows[response.ProcessActionGroupId])  $scope.refinedActionGroupsWithRows[response.ProcessActionGroupId] = [];
                        $scope.refinedActionGroupsWithRows[response.ProcessActionGroupId].push(response);
                        $scope.processActionRows.push(response)
                    });
                } else if (groupType == 2) {
                    data.ProcessActionType = 3;
                    ProcessActionsService.ProcessActionRowAdd(StateParameters.process, data).then(function (response) {
                        response.ProcessActionType = 3;
                        response.Value = chosenProcessId;
                        $scope.refinedActionGroupsWithRows[response.ProcessActionGroupId].push(response)
                        $scope.processActionRows.push(response)
                        changedItems.push(response);
                        SaveService.tick();
                    });
                }
            };

            $scope.selectedRows = [];
            $scope.selectRow = function (params) {
                if (_.find($scope.selectedRows, function (o) {
                    return o == params.tableRow;
                })) {
                    $scope.selectedRows.splice(_.findIndex($scope.selectedRows, function (o) {
                        return o == params.tableRow;
                    }), 1);
                } else {
                    $scope.selectedRows.push(params.tableRow);
                }
            }

            let deleteFunctionForRows = function (params) {
                let deleteItemsString: string;
                _.each(params, function (o) {
                    if (deleteItemsString != undefined) {
                        deleteItemsString += ',' + o.ProcessActionRowId.toString();
                    } else {
                        deleteItemsString = o.ProcessActionRowId.toString();
                    }
                    let spliceIndex = _.findIndex(!o.hasOwnProperty('Ungrouped') ? $scope.refinedActionGroupsWithRows[o.ProcessActionGroupId] : $scope.ungroupedRows, function (i: Client.ProcessActionRow) {
                        return i.ProcessActionRowId == o.ProcessActionRowId;
                    });
                    let arrayToSplice = !o.hasOwnProperty('Ungrouped') ? $scope.refinedActionGroupsWithRows[o.ProcessActionGroupId] : $scope.ungroupedRows
                    arrayToSplice.splice(spliceIndex, 1);
                });
                ProcessActionsService.ProcessActionRowDelete(StateParameters.process, deleteItemsString).then(function () {
                    deleteItemsString = undefined;
                });
            }
            //Deleting Rows from Action Groups, after dialog confirmation
            $scope.deleteRow = function (params, isFromEditDialog: boolean = false) {
                if (!isFromEditDialog) {
                    MessageService.confirm("Are you sure you want to delete the selected rows?", function () {

                        deleteFunctionForRows(params);
                    });
                    $scope.selectedRows = [];
                } else {

                    deleteFunctionForRows(params);
                }
            };
            //Field options here
            $scope.copyItemCols = [];
            //$scope.FieldOptionsCopy = [];
            $scope.copyItemCols = CopyItemColumns;
            //translations
            let userLanguage = $rootScope.clientInformation.locale_id;
            $scope.userLanguage = userLanguage;


            let checkTranslationLength = function (string, string2) {
                if (string[userLanguage]) {
                    let translation = string[userLanguage];
                    if (translation.length > 0 && string2.length > 0) {
                        return true;
                    }
                }
                return false;
            };
            let changedGroupOrder = [];

            function pushToChangedGroupOrder(params: object) {
                if (changedGroupOrder.indexOf(params) == -1)
                    changedGroupOrder.push(params);
                else changedGroupOrder[changedGroupOrder.indexOf(params)] = params;
            }

            $scope.syncOrder = function (current, prev, next, newParent) {


                let beforeOrderNo;
                let afterOrderNo;
                if (angular.isDefined(prev)) {
                    beforeOrderNo = prev.OrderNo;
                    pushToChangedGroupOrder(prev);
                }
                if (angular.isDefined(next)) {
                    afterOrderNo = next.OrderNo;
                    pushToChangedGroupOrder(next);
                }
                current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
                pushToChangedGroupOrder(current);


                SaveService.tick();
            };

            $scope.ProcessActionId = StateParameters.ProcessActionId;

            $scope.isNotFirstCreateColumn = function (params, index) {
                return !(index == 0 && params.Type == 2);
            }

            if($scope.processActionGroups.length == 0){

                let newGroupData = {
                    LanguageTranslation: {'en-GB': "New SET group", 'fi-FI': "Uusi SET-ryhmä"},
                    Process: StateParameters.process,
                    Type: 1,
                    OrderNo: 'U',
                    Variable: undefined,
                    ProcessInGroup: null,
                    InUse: 1,
                    ProcessActionId: StateParameters.ProcessActionId
                };

                ProcessActionsService.ProcessActionRowsGroupAdd(StateParameters.process, newGroupData).then(function (data) {
                    $scope.processActionGroups.push(data)
                    $scope.addNewActionRow(data.ProcessActionGroupId, 1);
                })
            }

            $scope.createNewGroup = function () {

                let params = {
                    template: 'settings/processActions/processActionsGroupCreationDialog.html',
                    controller: 'ProcessActionsGroupCreationDialogController',
                    locals: {
                        StateParameters: StateParameters,
                        ProcessActionRowsGroups: $scope.processActionGroups,
                        ProcessActionsService: ProcessActionsService,
                        ProcessesService: ProcessesService,
                        RefinedActionGroupsWithRows: $scope.refinedActionGroupsWithRows,
                        AddNewActionRow: $scope.addNewActionRow,
                        GetItemColumns: findProcessItemColumns
                    }
                };
                MessageService.dialogAdvanced(params);
            };
            $scope.editGroup = function (groupData) {
                let params = {
                    template: 'settings/processActions/processActionsGroupEditDialog.html',
                    controller: 'ProcessActionsGroupEditDialogController',
                    locals: {
                        StateParameters: StateParameters,
                        ProcessActionRowsGroups: $scope.processActionGroups,
                        RefinedActionGroupsWithRows: $scope.refinedActionGroupsWithRows,
                        CreateGroupProcesses: $scope.createGroupProcesses,
                        EditGroupData: groupData,
                        DeleteFunctionForRows: $scope.deleteRow,
                        AddNewActionRow: $scope.addNewActionRow,
                        ChangeCreateGroupData: $scope.changeCreateGroupData
                    }
                };
                MessageService.dialogAdvanced(params);
            };

            function emptyValues(params, isFromChangeType: boolean = true) {
                params.Value = null;
                params.ValueType = null;
                params.ConditionId = null;
                if (isFromChangeType) params.ItemColumnId = null;
            }

            // change the row type
            $scope.changeType = function (params) {

                emptyValues($scope.refinedActionGroupsWithRows[params.groupId][params.index]);
                if (params.tableRow.ProcessActionType == 3) {

                    params.tableRow.Value = params.processInGroup;
                }
                $scope.changeValue(params.tableRow);
            };

            //find list items
            $scope.findListItems = function (params) {



                let listName =  params.tableRow.$$DataAdditional//ItemColumns.itemColumnsOptions[params.tableRow.ItemColumnId].DataAdditional;
                $scope.refinedActionGroupsWithRows[params.groupId][params.index].$$listName = listName;
                $scope.refinedActionGroupsWithRows[params.groupId][params.index].$$DataAdditional = listName;
                if (!$scope.listOptions.hasOwnProperty(listName)) {
                    ListsService.getProcessItems(listName, 'order').then(function (o) {
                        $scope.listOptions[params.tableRow.ProcessActionRowId] = o;
                    });
                }
            }

            //Changing item column
            $scope.changeIc = function (params) {
                $scope.returnDataType(params.tableRow);

                let changeRow = _.find($scope.refinedActionGroupsWithRows[params.groupId], function (o) {
                    return o.ProcessActionRowId == params.tableRow.ProcessActionRowId;
                });
                if (params.tableRow.ProcessActionType == 8) {
                    $scope.refinedActionGroupsWithRows[params.groupId][params.index].$$DataType = _.find($scope.itemColumnsByProcess[params.group.ProcessInGroup], function (o) {
                        return o.ItemColumnId == params.tableRow.ItemColumnId;
                    }).DataType
                } else {
                    if(!params.hasOwnProperty('linkColumn')){
                        $scope.refinedActionGroupsWithRows[params.groupId][params.index].$$DataType = _.find($scope.itemColumns, function (o) {
                            return o.ItemColumnId == params.tableRow.ItemColumnId;
                        }).DataType;
                    }

                }
                if (params.tableRow.$$DataType == 6) {
                    $scope.findListItems(params);
                }
                if (changeRow != undefined && !params.hasOwnProperty('linkColumn')) emptyValues(changeRow, false);


                $scope.changeValue(changeRow);

                if(!params.hasOwnProperty('linkColumn')) $scope.returnDataType(params.tableRow.ItemColumnId)
            };

            let changedItems = [];

            $scope.clearCopyFields = function (params) {
                params.tableRow.ItemColumnId = null;
                params.tableRow.Value = null;

                $scope.changeValue(params);
            };

            $scope.clearTarget = function (params) {
                params.tableRow.Value = null;
                $scope.changeValue(params);
            }
            //Change values || Usually invoked when changing the Target Field.
            $scope.changeValue = function (params) {

                //current state hot fix
                if(params.ProcessActionType == 8 && params.ValueType == null) params.ValueType = 0;

                let copyProtection = {
                    Value: undefined,
                    $$DataType: undefined,
                    ValueType: 0,
                    ProcessActionRowId: undefined,
                    UserItemColumnId: undefined,
                    ConditionId: undefined
                };
                $scope.DataTypeInitiator();
                if (!params.tableRow) {
                    if (params.ValueType == 2) {
                        params.Value = [];
                        params.UserItemColumnId = null;
                    }
                } else {
                    if (params.tableRow.ValueType == 2) {
                        params.tableRow.Value = [];
                        params.tableRow.UserItemColumnId = null;
                    }
                }
                //We have 2 different situations when this function is accessed, this guarantees the copy.
                copyProtection = (params.tableRow != undefined) ? angular.copy(params.tableRow) : angular.copy(params);

                if (params.$$DataType == 6 && params.ValueType == 3) {
                    ValidateListOptions(params.tableRow.ItemColumnId, params.ProcessInGroup);
                }

                if (copyProtection.Value != undefined) {
                    if ((copyProtection.$$DataType == 6 || copyProtection.$$DataType == 5) && (copyProtection.ValueType == 0 || copyProtection.ValueType == 2 || copyProtection.ValueType == 3)) {
                        if (Array.isArray(copyProtection.Value)) {
                            copyProtection.Value = JSON.stringify(copyProtection.Value);
                        }
                    }
                }

                let changedItemsIndex = _.findIndex(changedItems, function (o) {
                    return o.ProcessActionRowId == copyProtection.ProcessActionRowId;
                })

                if (changedItemsIndex == -1) {
                    changedItems.push(copyProtection)
                } else {
                    changedItems[_.findIndex(changedItems, function (o) {
                        return o.ProcessActionRowId == copyProtection.ProcessActionRowId;
                    })] = copyProtection;
                }
                SaveService.tick();
            };
            let changedGroup = [];
            //Change title/translation for the group
            $scope.SaveGroup = function (params) {
                if (_.findIndex(changedGroup, function (o) {
                    return o.groupId == params.groupId;
                }) == -1) {
                    changedGroup.push(params);
                } else {
                    changedGroup.splice(_.findIndex(changedGroup, function (o) {
                        return o.groupId == params.groupId;
                    }), 1, params);
                }
                SaveService.tick();
            };
            $scope.changeCreateGroupData = function (params) {
                findProcessItemColumns(params.group.ProcessInGroup);
                let deleteArray = [];

                _.forEach($scope.refinedActionGroupsWithRows[params.group.ProcessActionGroupId], function (o) {
                    //If process action type == 4 OR 8, delete them if the ProcessInGroup has been changed.
                    if ((o.ProcessActionType == 4 || o.ProcessActionType == 8) && $scope.refinedActionGroupsWithRows[params.group.ProcessActionGroupId][0].Value != params.group.ProcessInGroup) {
                        deleteArray.push(o);
                    }
                })

                deleteFunctionForRows(deleteArray);
                deleteArray = [];
                _.forEach($scope.refinedActionGroupsWithRows[params.group.ProcessActionGroupId], function (o) {
                    if (o.ProcessActionType == 3) {
                        o.Value = params.group.ProcessInGroup;
                        changedItems.push(o);
                    }
                });
                $scope.SaveGroup(params.group)
            };


            let FormatListValues = function (actionRow: object) {
                let o = String(angular.copy(actionRow.Value));
                if(o.includes('[')) return;
                // @ts-ignore
                actionRow.Value = '[' + actionRow.Value + ']'
            }
            //Save
            SaveService.subscribeSave($scope, function () {
                if (changedItems.length > 0) {
                    for (let item of changedItems) {
                        if (item.$$DataType == 6 && (item.ProcessActionType != 9 && item.ProcessActionType != 4) && item.Value != null)
                            FormatListValues(item)
                    }


                    ProcessActionsService.ProcessActionRowSave(StateParameters.process, changedItems).then(function () {
                        changedItems = [];
                    });
                }
                if (changedGroup.length > 0) {
                    ProcessActionsService.updateProcessActionRowsGroups(StateParameters.process, changedGroup).then(function () {
                        changedGroup = [];
                    });
                }
                if (changedGroupOrder.length > 0) {
                    ProcessActionsService.updateProcessActionRowsGroups(StateParameters.process, changedGroupOrder).then(function () {
                        changedGroupOrder = [];
                    });
                }

                if (nongroupedChanged.length > 0) {
                    return saveProcessActionsRow();
                }

            });
        }


        ProcessActionsDialogSettingsController.$inject = ['$q', '$scope', 'SaveService', 'Languages', 'Translations', 'StateParameters', 'ProcessActionsService', 'Translator', 'SettingsService', 'Dialog', 'ProcessContainers'];

        function ProcessActionsDialogSettingsController($q, $scope, SaveService, Languages, Translations, StateParameters, ProcessActionsService, Translator, SettingsService, Dialog, ProcessContainers) {

            let newValuesArray = [];
            let changedItems = [];

            $scope.dialog = Dialog;
            $scope.processContainers = ProcessContainers;

            $scope.params = StateParameters;
            $scope.languages = Languages;
            $scope.translations = Translations;

            let headerText = Translator.translate('PROCESS_ACTION_DIALOG') + " - " + StateParameters.UserLanguageTranslation;

            $scope.header = {
                title: headerText,
                menu: []
            };
            StateParameters.activeTitle = $scope.header.title;

            $scope.changeContainer = function(){
                SettingsService.saveProcessDialog(StateParameters.process, [$scope.dialog]);
            };

            SaveService.subscribeSave($scope, function () {
                if (!angular.isUndefined(changedItems.length > 0)) {
                    return saveTranslation();
                }
            });

            let saveTranslation = function () {
                let changedItemsArray = [];
                angular.forEach(changedItems, function (key) {
                    changedItemsArray.push($scope.translations[key.language]);
                });
                return ProcessActionsService.ProcessActionDialogTranslationSave(StateParameters.process, StateParameters.ProcessActionDialogId, changedItemsArray);
            };

            $scope.changeTranslation = function (params) {
                newValuesArray.push({language: params.language});
                let noDuplicateObjects = {};
                let l = newValuesArray.length;

                for (let i = 0; i < l; i++) {
                    let item = newValuesArray[i];
                    noDuplicateObjects[item.language] = item;
                }

                let nonDuplicateArray = [];
                for (let item in noDuplicateObjects) {
                    nonDuplicateArray.push(noDuplicateObjects[item]);
                }
                changedItems = nonDuplicateArray;
            };
        }


        ProcessActionsService.$inject = ['$q', 'ApiService'];

        function ProcessActionsService($q, ApiService) {
            let self = this;

            self.ProcessActions = ApiService.v1api('settings/ProcessActions');
            self.ProcessActionRows = ApiService.v1api('settings/ProcessActions/Rows');
            self.ProcessActionDialog = ApiService.v1api('settings/ProcessActions/Dialogs');
            self.ItemColumns = ApiService.v1api('meta/ItemColumns');


            self.getProcessActions = function (process) {
                return self.ProcessActions.get([process]);
            };

            self.getUnusedActions = function (process) {
                return self.ProcessActions.get([process, 'unusedactions']);
            };

            self.getButtonsForActions = function (process) {
                return self.ProcessActions.get([process, 'buttonsforactions']);
            };

            self.getProcessActionRows = function (process, ProcessActionId) {
                return self.ProcessActionRows.get([process, ProcessActionId]);
            };


            self.getItemColumnActions = function (itemColumnId) {
                return self.ProcessActions.get(['itemColumns', itemColumnId]);
            };

            self.getItemColumns = function (process) {
                return self.ItemColumns.get([process]);
            };

            self.ProcessActionAdd = function (process, data) {
                return self.ProcessActions.new([process], data);
            };

            self.ProcessActionSave = function (process, data) {
                return self.ProcessActions.save([process], data);
            };

            self.ProcessActionDelete = function (process, ids) {
                return self.ProcessActions.delete([process, ids]);
            };

            self.ProcessActionRowAdd = function (process, data) {
                return self.ProcessActionRows.new([process], data);
            };

            self.ProcessActionRowSave = function (process, data) {
                return self.ProcessActionRows.save([process], data);
            };

            self.ProcessActionRowDelete = function (process, ids) {
                return self.ProcessActionRows.delete([process, ids]);
            };

            self.ProcessActionDialogAdd = function (process, data) {
                return self.ProcessActionDialog.new([process], data);
            };

            self.ProcessActionDialogSave = function (process, data) {
                return self.ProcessActionDialog.save([process], data);
            };

            self.ProcessActionDialogTranslationSave = function (process, ProcessActionDialogId, data) {
                return self.ProcessActionDialog.save([process, ProcessActionDialogId], data);
            };
            self.ProcessActionDialogDelete = function (process, ids) {
                return self.ProcessActionDialog.delete([process, ids]);
            };

            self.ProcessActionRowsGroupsDelete = function (process, processActionGroupId) {
                return self.ProcessActionRows.delete([process, processActionGroupId, 'groupDelete']);
            };

            self.ProcessActionRowsGroupAdd = function (process, data) {
                return self.ProcessActionRows.new([process, 'group'], data);
            };

            self.getProcessActionRowsGroups = function (process, ProcessActionId) {
                return self.ProcessActionRows.get([process, ProcessActionId, 'rowsGroups'])
            };

            self.updateProcessActionRowsGroups = function (process, data) {
                return self.ProcessActionRows.save([process, 'groupUpdate'], data)
            };

            self.GetCustomActionOptions = function (type) {
                return ApiService.v1api('settings/RegisteredCustomActions/CustomActionOptions').get([type]);
            };

            self.getLinkColumns = function (process){
                return ApiService.v1api('settings/ProcessActions/Rows').get([process, "link_columns"]);
            }
        }

    }

)();