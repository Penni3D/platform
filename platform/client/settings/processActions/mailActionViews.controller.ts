(function () {
	'use strict';
	angular
		.module('core.settings')
		.controller('MailActionViewsController', MailActionViewsController)
	MailActionViewsController.$inject = ['$scope',
		'ActionRow',
		'ItemColumns',
		'Features',
		'Processes',
		'MessageService',
		'Index',
		'SettingsService',
		'Translator',
		'ProcessActionsService',
		'StateParameters'
	];

	function MailActionViewsController($scope,
	                                   ActionRow,
	                                   ItemColumns,
	                                   Features,
	                                   Processes,
	                                   MessageService,
	                                   Index,
	                                   SettingsService,
	                                   Translator,
	                                   ProcessActionsService,
	                                   StateParameters) {


		$scope.features = Features;
		$scope.processes = Processes;
		$scope.features.push({viewId: "1stTab", viewName: "1ST_TAB"});
		$scope.features.push({viewId: "lastTab", viewName: "LAST_TAB"});
		$scope.idInputs = [];
		$scope.dateInputs = [];
		angular.forEach(ItemColumns.itemColumns, function (column) {
			if (column.DataType == 3) $scope.dateInputs.push(column);
			if (column.DataType == 1 || (column.DataType == 16 && column.SubDataType == 1)) $scope.idInputs.push(column);
		});

		$scope.cancel = function(){
			MessageService.closeDialog();
		}

		$scope.actionRow = ActionRow;

		$scope.splitOptions = [
			{name: 'PORTFOLIO_SPLIT_NORMAL', value: 0},
			{name: 'PORTFOLIO_SPLIT_SPLIT', value: 1},
			{name: 'PORTFOLIO_SPLIT_DOUBLE_NORMAL', value: 2},
			{name: 'PORTFOLIO_SPLIT_DOUBLE_SPLIT', value: 3}
		];

		$scope.widthOptions = [
			{name: 'VIEW_SPLIT_EVEN', value: 'even'},
			{name: 'VIEW_SPLIT_WIDER', value: 'wider'},
			{name: 'VIEW_SPLIT_NARROWER', value: 'narrower'},
			{name: 'VIEW_SPLIT_FULL', value: 'full'}
		];

		$scope.save = function () {
			ProcessActionsService.ProcessActionRowSave(StateParameters.process, [$scope.actionRow]).then(function () {
				MessageService.closeDialog();
			});
		};

		$scope.statesByProcess = {};

		$scope.changeInsteadProcess = function () {
			if(ActionRow.ActionRowEmailViewOptions == null) return
			if (!$scope.statesByProcess.hasOwnProperty(ActionRow.ActionRowEmailViewOptions.OpenRowProcess) && ActionRow.ActionRowEmailViewOptions.OpenRowProcess != '') {
				let tabs_ = [];
				let fs = [];
				SettingsService.ProcessTabs(ActionRow.ActionRowEmailViewOptions.OpenRowProcess).then(function (tabs) {
					_.each(tabs, function (t) {
						t.DefaultState = t.ProcessTabId;
						t.Feature = Translator.translate(t.Variable) + ' - ' + t.MaintenanceName;
					})
					SettingsService.ProcessGroups(ActionRow.ActionRowEmailViewOptions.OpenRowProcess).then(function (groups) {
						let features = [];
						angular.forEach(groups, function (group) {
							angular.forEach(group.SelectedFeatures, function (feature) {
								features.push(feature);
							});
						});
						fs = _.uniqBy(features, 'DefaultState');
						tabs_ = _.uniqBy(tabs, 'ProcessTabId');
						$scope.statesByProcess[ActionRow.ActionRowEmailViewOptions.OpenRowProcess] = fs.concat(tabs_);

						$scope.features.push({DefaultState: "1stTab", Feature: "1ST_TAB"});
						$scope.features.push({DefaultState: "lastTab", Feature: "LAST_TAB"});
						$scope.statesByProcess[ActionRow.ActionRowEmailViewOptions.OpenRowProcess].unshift({DefaultState: "1stTab", Feature: "1ST_TAB"})
						$scope.statesByProcess[ActionRow.ActionRowEmailViewOptions.OpenRowProcess].unshift({DefaultState: "lastTab", Feature: "LAST_TAB"})
					});
				});
			}
		}
		$scope.changeInsteadProcess();
	}
})();