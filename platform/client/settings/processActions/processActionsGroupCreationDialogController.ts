﻿(function () {
    'use strict';

    angular
        .module('core.settings')
        .controller('ProcessActionsGroupCreationDialogController', ProcessActionsGroupCreationDialogController)

    ProcessActionsGroupCreationDialogController.$inject = ['$q', 'AddNewActionRow', '$rootScope', '$scope', 'StateParameters', 'Common', 'ProcessActionRowsGroups', 'ProcessActionsService', 'MessageService', 'ProcessesService', 'RefinedActionGroupsWithRows', 'GetItemColumns'];

    function ProcessActionsGroupCreationDialogController($q, AddNewActionRow, $rootScope, $scope, StateParameters, Common, ProcessActionRowsGroups, ProcessActionsService, MessageService, ProcessesService, RefinedActionGroupsWithRows, GetItemColumns) {

        $scope.localeId = $rootScope.me.locale_id;
        $scope.createGroupProcesses = [];
        ProcessesService.get().then(function (o) {
            $scope.createGroupProcesses = o;
        });
        let userLanguage = $rootScope.clientInformation.locale_id;
        let checkTranslationLength = function (string, string2) {
            if (string[userLanguage]) {
                let translation = string[userLanguage];
                if (translation.length > 0 && string2.length > 0) {
                    return true;
                }
            }
            return false;
        };
        $scope.createOptions = {1: 'Set', 2: 'Create'};
        // create a new Action Group
        let beforeOrderNo = Common.orderNoFromArray(ProcessActionRowsGroups);
        let newGroupData = {
            LanguageTranslation: null,
            Process: StateParameters.process,
            Type: 1,
            OrderNo: undefined,
            Variable: undefined,
            ProcessInGroup: null,
            InUse: null,
            ProcessActionId: StateParameters.ProcessActionId
        };

        $scope.Type = ["Set"];

        $scope.setGroupOption = function () {
            if ($scope.Type[0] == "Set") {
                newGroupData.Type = 1;
                newGroupData.LanguageTranslation = $scope.LanguageTranslation;
            } else if ($scope.Type[0] == "Create") {
                newGroupData.Type = 2;
                newGroupData.LanguageTranslation = $scope.LanguageTranslation;
            }
        }
        $scope.save = function () {
            $scope.setGroupOption();
            checkTranslationLength(newGroupData.LanguageTranslation, newGroupData.Type)
            newGroupData.OrderNo = Common.getOrderNoBetween(beforeOrderNo, undefined);
            if (newGroupData.Type == 2) {
                if ($scope.gProcess.length > 0) {
                    newGroupData.ProcessInGroup = $scope.gProcess[0];
                } else {
                    return;
                }
            } else if (newGroupData.Type == 1) {
                newGroupData.ProcessInGroup = null;
            } else {
                return;
            }
            newGroupData.InUse = 1;
            ProcessActionsService.ProcessActionRowsGroupAdd(StateParameters.process, newGroupData).then(function (data) {
                RefinedActionGroupsWithRows[data.ProcessActionGroupId] = [];
                ProcessActionRowsGroups.push(data);
                if (data.Type == 2) {
                    AddNewActionRow(data.ProcessActionGroupId, data.Type, data.ProcessInGroup);
                    GetItemColumns(data.ProcessInGroup)
                }
                $scope.close();
            });
        };
        $scope.modifications = {
            create: false,
            cancel: true
        };

        $scope.close = function () {
            MessageService.closeDialog();
        }
    }
})();