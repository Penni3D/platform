﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.config(NotificationConditionsConfig);

	NotificationConditionsConfig.$inject = ['ViewsProvider'];
	function NotificationConditionsConfig(ViewsProvider) {

		ViewsProvider.addView('settings.notificationconditions', {
			controller: 'NotificationConditionsSettingsController',
			template: 'settings/notificationconditions/notificationconditions.html',
			resolve: {
				NotificationCondition: function (SettingsService, StateParameters) {
					return SettingsService.NotificationConditions(StateParameters.process);

				},
				Condition: function (SettingsService, StateParameters) {
					return SettingsService.Conditions(StateParameters.process);
				},
				Notifications: function (PortfolioService, ViewConfigurator, Translator) {
					return PortfolioService.getSimplePortfolioData('notification', ViewConfigurator.getGlobal('notification').Params.emailsPortfolioId, {limit: 0}).then(function (notificationsRaw) {
						let notifications = [];
						angular.forEach(notificationsRaw.rowdata, function (notification) {
							notifications.push({
								'Translation': Translator.translate(notification.name_variable),
								'ItemId': notification.item_id
							});
						});

						return notifications;
					});
				},
				ItemColumns: function (SettingsService, StateParameters, $q, Translator) {
					var d = $q.defer();

					SettingsService.ItemColumns(StateParameters.process).then(function (itemColumns) {
						var promises = [];
						var retval = [];
						angular.forEach(itemColumns, function (col) {
							var deffered = $q.defer();
							if (col.DataType == 6) {
								if(col.DataAdditional != null && col.DataAdditional.length > 0) {
									SettingsService.ItemColumns(col.DataAdditional).then(function (listCols) {
										angular.forEach(listCols, function (listCol) {
											if (listCol.DataType == 5) {
												listCol.ItemColumnId = col.ItemColumnId + ";" + col.DataAdditional + ";" + listCol.ItemColumnId;
												listCol.Translation = Translator.translation(col.Translation + " - " + listCol.Translation);
												retval.push(listCol);
											}
										});
										deffered.resolve(listCols);
									});
									promises.push(deffered.promise);
								}
							} else if (col.DataType == 0 || col.DataType == 1 || col.DataType == 5) {
								retval.push(col);
							}
						});
						$q.all(promises).then(function () {
							d.resolve(retval);
						});
					});



					return d.promise;
				},
				Views: function (SettingsService, StateParameters, Views, Translator) {
					var views = [];

					return SettingsService.ProcessTabs(StateParameters.process).then(function (tabs) {
						angular.forEach(tabs, function (tab) {
							views.push({ viewId: 'process.tab_' + tab.ProcessTabId, viewName: 'Tab: ' + Translator.translation(tab.LanguageTranslation) });
						});
						angular.forEach(Views.getFeatures(), function (f) {
							views.push({ viewId: f.view, viewName: 'Feature: ' + f.translation });
						});

						return views;
					});
				},
                UserGroups: function (SettingsService) {
                    return SettingsService.UserGroups('user_group');
                },
				ProcessActions: function (ProcessActionsService, StateParameters) {
					return ProcessActionsService.getProcessActions(StateParameters.process);
				},
			}
		});
	}
})();