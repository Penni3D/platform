﻿(function () {
    'use strict';

    angular
        .module('core.settings')
        .controller('NotificationConditionsSettingsController', NotificationConditionsSettingsController);


    NotificationConditionsSettingsController.$inject = ['$q', '$scope', 'MessageService', 'SettingsService', 'Condition', 'NotificationCondition', 'ItemColumns', 'Common', 'SaveService', 'ViewService', 'StateParameters', '$filter', 'Notifications', 'ListsService', 'Views', 'UserGroups', 'Translator', 'ProcessActions'];
    function NotificationConditionsSettingsController($q, $scope, MessageService, SettingsService, Condition, NotificationCondition, ItemColumns, Common, SaveService, ViewService, StateParameters, $filter, Notifications, ListsService, Views, UserGroups, Translator, ProcessActions) {
        $scope.notificationConditions = NotificationCondition;
        $scope.conditions = $filter('orderBy')(Condition, function (cond) { return Translator.translation(cond.LanguageTranslation)});
        $scope.emailTemplates = $filter('orderBy')(Notifications, function (notif) { return Translator.translation(notif.LanguageTranslation )});
        $scope.itemColumns = $filter('orderBy')(ItemColumns, function (column) { return Translator.translation(column.LanguageTranslation)});
        $scope.UserGroups = UserGroups;

        $scope.edit = true;
        
        $scope.selectedRows = [];
        $scope.views = Views;
        $scope.actions = ProcessActions;
        
        var newValuesArray = [];
        var changedItems = [];
        var data = {};


        ViewService.footbar(StateParameters.ViewId, {
            template: 'settings/notificationconditions/notificationconditionsfooter.html',
            scope: $scope
        });


        SaveService.subscribeSave($scope, function () {
            if (changedItems.length > 0) {
            	saveNotificationCondition();
            }
        });

        $scope.addNotificationCondition = function () {
            data = { LanguageTranslation: null };

            var save = function () {
                addNotificationConditionSave();
                data = {};
            };

            var content = {
                buttons: [{ text: 'DISCARD' }, { type: 'primary', onClick: save, text: 'SAVE' }],
                inputs: [{ type: 'translation', label: 'CONDITION_NAME', model: 'LanguageTranslation' }],
                title: 'ADD_CONDITION'
            }

            MessageService.dialog(content, data)
        };

        var addNotificationConditionSave = function () {
            var condition = {};
            condition.LanguageTranslation = data.LanguageTranslation;
            SettingsService.NotificationConditionsAdd(StateParameters.process, condition).then(function (response) {
                $scope.notificationConditions.push(response);
            });

            newValuesArray = [];
            changedItems = [];

        };

		var saveNotificationCondition = function () {
            var conditionArray = [];
            angular.forEach(changedItems, function (item) {
                var index = Common.getIndexOf($scope.notificationConditions, item.NotificationConditionId, 'NotificationConditionId');
                conditionArray.push($scope.notificationConditions[index]);
            });

            SettingsService.NotificationConditionsSave(StateParameters.process, conditionArray);

            newValuesArray = [];
            changedItems = [];

        };

        $scope.deleteNotificationCondition = function () {
            MessageService.confirm("CONFIRM_DELETE", function () {
                var deleteIds = $scope.selectedRows.join(',');
                SettingsService.NotificationConditionsDelete(StateParameters.process, deleteIds);

                angular.forEach($scope.selectedRows, function (row) {
                    var i = $scope.notificationConditions.length;
                    while(i--){
                        if($scope.notificationConditions[i].NotificationConditionId == row){
                            $scope.notificationConditions.splice(i, 1);
                            break;
                        }
                    }
                });
                $scope.selectedRows.length = 0;
            });
        };

        $scope.header = {
            title: 'PROCESS_CONDITIONS_PAGE_TITLE',
        };
	    StateParameters.activeTitle = $scope.header.title;

        $scope.changeValue = function (NotificationConditionId) {
            newValuesArray.push({ NotificationConditionId: NotificationConditionId });
            var noDuplicateObjects = {};
            var l = newValuesArray.length;

            for (var i = 0; i < l; i++) {
                var item = newValuesArray[i];
                noDuplicateObjects[item.NotificationConditionId] = item;
            }

            var nonDuplicateArray = [];
            for (var item in noDuplicateObjects) {
                nonDuplicateArray.push(noDuplicateObjects[item]);
            }
            changedItems = nonDuplicateArray;
        };
    }

})();
