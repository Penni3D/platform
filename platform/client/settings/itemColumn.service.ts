(function () {
    'use strict';

    angular
        .module('core')
        .service('ItemColumnService', ItemColumnService);

    ItemColumnService.$inject = ['$q', 'SettingsService', 'MessageService'];

    function ItemColumnService($q, SettingsService, MessageService) {
        let self = this;

        self.saveContainerColumn = function (process, containerColumns) {
            return SettingsService.ProcessContainerColumnsSave(process, containerColumns);
        };

        self.saveItemColumn = function(process, itemColumns){
          return SettingsService.ItemColumnsSave(process, itemColumns);
        };
       // let newContainer = {LanguageTranslation: {}, ProcessContainerSide: null};
    }
})();
