(function () {
	'use strict';

	angular
		.module('core.settings')
		.controller('SettingsConditionsDebugController', SettingsConditionsDebugController);

	SettingsConditionsDebugController.$inject = ['$scope', 'MessageService', 'StateParameters', 'ProcessConditions', 'ViewService', 'UiParentChildRelations', 'ConditionHierarchy', 'Translator'];

	function SettingsConditionsDebugController($scope, MessageService, StateParameters, ProcessConditions, ViewService, UiParentChildRelations, ConditionHierarchy, Translator) {

		let selectedUiObjectId = StateParameters.selectedUiObjectId;
		let selectedUiObjectType = StateParameters.selectedUiObjectType;
		$scope.isPortfolio = false;
		$scope.activeTranslation = StateParameters.translation;
		$scope.title = StateParameters.title;
		$scope.noConditions = StateParameters.noConditions;

		let parentIds = "";
		let ownIds = "";

		if (selectedUiObjectType == 'ProcessContainerIds') {
			parentIds = "ProcessTabIds";
			ownIds = "ProcessContainerIds";
		} else if (selectedUiObjectType == 'ProcessTabIds') {
			parentIds = "ProcessGroupIds";
			ownIds = "ProcessTabIds";
		}

		$scope.closeConditionDialog = function () {
			MessageService.closeDialog();
		};

		let parents = [];
		angular.forEach(ConditionHierarchy.data, function (value) {
			if (value.tab_id == selectedUiObjectId) {
				parents.push(value.process_group_id);
			} else if (value.container_id == selectedUiObjectId) {
				parents.push(value.tab_id);
			}
		});
		let parentConditions = [];
		let phaseParentsForContainers = [];
		if (selectedUiObjectType == 'ProcessContainerIds') {
			angular.forEach(ConditionHierarchy.data, function (value) {
				if (selectedUiObjectId == value.container_id) {
					phaseParentsForContainers.push(value.process_group_id);
				}
			});
			phaseParentsForContainers = _.uniq(phaseParentsForContainers);
		}

		let phaseConditionsForContainers = function () {
			angular.forEach(ProcessConditions, function (value) {
				for (let i = 0; i < phaseParentsForContainers.length; i++) {
					if (_.includes(value.ProcessGroupIds, phaseParentsForContainers[i]) && value.ProcessTabIds.length == 0) {
						parentConditions.push(value);
					}
				}
			});
		};

		let calculateParentConditions = function () {
			angular.forEach(ProcessConditions, function (value) {
				for (let i = 0; i < value[parentIds].length; i++) {
					for (let j = 0; j < parents.length; j++) {
						if (value[parentIds][i] == parents[j] && value[ownIds].length == 0) {
							parentConditions.push(value)
						}
					}
				}
			});
		};
		$scope.portfolioConditions = [];

		let calculatePortfolioConditions = function () {
			angular.forEach(ProcessConditions, function (condition) {
				if (_.includes(condition.ProcessPortfolioIds, selectedUiObjectId)) {
					$scope.portfolioConditions.push(condition);
				}
			});
		};

		if (selectedUiObjectType == 'ProcessTabIds') {
			calculateParentConditions();
		} else if (selectedUiObjectType == 'ProcessContainerIds') {
			calculateParentConditions();
			phaseConditionsForContainers();
		} else if (selectedUiObjectType == 'ProcessPortfolioIds') {
			$scope.isPortfolio = true;
			calculatePortfolioConditions();
		}

		let writeConditions = [];
		let readConditions = [];
		let historyConditions = [];
		let parentWriteConditions = [];
		let parentReadConditions = [];

		angular.forEach(ProcessConditions, function (value) {
			if (_.includes(value[selectedUiObjectType], selectedUiObjectId)) {
				_.each(value.States, function (s) {
					if (s.includes(".write") && !_.includes(writeConditions, value)) {
						writeConditions.push(value);
					}

					if (s.includes(".read") && !_.includes(readConditions, value)) {
						readConditions.push(value);
					}

					if (s.includes(".history") && !_.includes(historyConditions, value)) {
						historyConditions.push(value);
					}

				})
			}
		});

		angular.forEach(parentConditions, function (value) {

			_.each(value.States, function (s) {
				if (s.includes(".write") && !_.includes(parentWriteConditions, value)) {
					parentWriteConditions.push(value);
				}
				if (s.includes(".read") && !_.includes(parentReadConditions, value)) {
					parentReadConditions.push(value);
				}
			})
		});

		let writeCondition = Translator.translate('CONDITION_WRITE');
		let readCondition = Translator.translate('CONDITION_READ');
		let historyCondition = Translator.translate('CONDITION_HISTORY');
		let parentConditionWrite = Translator.translate('CONDITION_PARENT_WRITE');
		let parentConditionRead = Translator.translate('CONDITION_PARENT_READ');

		$scope.conditionTypes = {};

		$scope.conditionTypes[writeCondition] = writeConditions;
		$scope.conditionTypes[readCondition] = readConditions;
		$scope.conditionTypes[historyCondition] = historyConditions;
		$scope.conditionTypes[parentConditionWrite] = _.uniq(parentWriteConditions);
		$scope.conditionTypes[parentConditionRead] = _.uniq(parentReadConditions);

		if (selectedUiObjectType == 'ProcessContainerIds' || selectedUiObjectType == 'ProcessTabIds') {
			$scope.checkIfConditions = !(writeConditions.length == 0 && readConditions.length == 0 && historyConditions.length == 0 && parentWriteConditions.length == 0 && parentReadConditions.length);
		} else {
			$scope.checkIfConditions = !(writeConditions.length == 0 && readConditions.length == 0 && historyConditions.length == 0);
		}

		$scope.showCondition = function (conditionId) {
			let params = {
				process: StateParameters.process, conditionId: conditionId,
				ConditionTranslation: Translator.translation(ProcessConditions[conditionId].LanguageTranslation)
			};

			let condition = ProcessConditions[conditionId];
			params.Condition = condition;

			ViewService.view('settings.conditions.fields',
				{
					params: params,
					locals: {
						NewCondition: condition,
						ConditionTabTranslations: {},
						ConditionContainerTranslations: {}
					}
				},
				{
					split: true,
					remember: false
				});

			$scope.closeConditionDialog();
		};
	}
})();