﻿(function () {
	'use strict';

	angular
		.module('core.settings')
		.config(SummaryConfig)
		.controller('SummarySettingsController', SummarySettingsController);

	SummaryConfig.$inject = ['ViewsProvider'];

	function SummaryConfig(ViewsProvider) {
		ViewsProvider.addView('settings.summary', {
			controller: 'SummarySettingsController',
			template: 'settings/summary/summary.html',
			resolve: {
				ProcessTabs: function(SettingsService, StateParameters){
					return SettingsService.ProcessTabs(StateParameters.process).then(function (tabs){
						return tabs;
					});
				},
				ProcessGroups: function(SettingsService,StateParameters, Translator){
					return SettingsService.ProcessGroups(StateParameters.process).then(function (groups){
						_.each(groups, function(group){
							SettingsService.ProcessGroupTabs(StateParameters.process, group.ProcessGroupId).then(function (tabs) {
								var resultString = "";
								angular.forEach(tabs, function (a) {
									if (resultString.length == 0 && a['ProcessTab'].MaintenanceName.length > 0) {
										resultString = Translator.translation(a['ProcessTab']['LanguageTranslation']) + " " + "'" + a['ProcessTab']['MaintenanceName'] + "'";
									} else if (resultString.length == 0 && a['ProcessTab'].MaintenanceName.length == 0) {
										resultString = Translator.translation(a['ProcessTab']['LanguageTranslation']);
									} else if (resultString.length > 0 && a['ProcessTab'].MaintenanceName.length > 0) {
										resultString += ',' + " " + Translator.translation(a['ProcessTab']['LanguageTranslation']) + " " + "'" + a['ProcessTab']['MaintenanceName'] + "'";
									}
									else {
										resultString += ',' + " " + Translator.translation(a['ProcessTab']['LanguageTranslation']);
									}
								});
								group.Tabs = resultString;
							});
						});
						return groups;
					});
				}
			}
		});
	}

	SummarySettingsController.$inject = ['$scope', 'SettingsService', 'StateParameters', 'ViewService', 'Translator', 'ProcessesService', 'ProcessGroups', 'ProcessTabs', 'Common', 'SaveService', 'MessageService', '$rootScope'];

	function SummarySettingsController($scope, SettingsService, StateParameters, ViewService, Translator, ProcessesService, ProcessGroups, ProcessTabs, Common, SaveService, MessageService, $rootScope) {

		$scope.ProcessTitle = Translator.translate('SETTINGS_SUMMARY') + ": " + Translator.translate(StateParameters.process);
		$scope.header = {
			title: $scope.ProcessTitle
		};
		StateParameters.activeTitle = $scope.header.title;
		$scope.selectedRows = [];

		$scope.showThisPhaseConditions = function (phase) {
			ViewService.view('debug.settings', {
				params: {
					process: StateParameters.process,
					selectedUiObjectId: phase.ProcessGroupId,
					selectedUiObjectType: 'ProcessGroupIds',
					translation: Translator.translation(phase.LanguageTranslation),
					noConditions: 'NO_PHASE_CONDITIONS',
					title: 'PHASE_CONDITIONS'
				}
			});
		};

		$scope.showGroup = function (GroupId, name, maintenancename) {
			let params = StateParameters;
			params.ProcessGroupId = GroupId;
			params.phaseName = Translator.translation(name) + ' ' + maintenancename;
			ViewService.view('settings.phases.tabs', { params: params })
		};


		ViewService.footbar(StateParameters.ViewId, {
			template: 'settings/summary/summary.footer.html',
			scope: $scope
		});


		$scope.searchText = "";
		$scope.searchResults = [];
		$scope.isField = true;
		$scope.isTab = true;
		$scope.isContainer = true;
		$scope.processGroups = ProcessGroups;
		let searchTimer;

		let groupsToSave = [];


		$scope.deleteConfirm = function(){
			MessageService.confirm("DELETE_CONFIRMATION", $scope.deletePhases, 'warn');
		};

		$scope.deletePhases = function () {
			let deleteIds = Common.convertArraytoString($scope.selectedRows, 'id');
			SettingsService.ProcessGroupsDelete(StateParameters.process, deleteIds);
			angular.forEach($scope.selectedRows, function (row) {
				let index = Common.getIndexOf($scope.processGroups, row.id, 'ProcessGroupId');
				$scope.processGroups.splice(index, 1);
			});
			$scope.selectedRows = [];
		};


		$scope.saveIfNotSaving = function(p){
			if(_.findIndex(groupsToSave, function(o) { return o.ProcessGroupId == p.ProcessGroupId; }) == -1){
				groupsToSave.push(p);
				SaveService.tick();
			}
		};

		$scope.syncOrder = function (current, prev, next) {
			let beforeOrderNo;
			let afterOrderNo;

			if (angular.isDefined(prev)) {
				beforeOrderNo = prev.OrderNo;
			}

			if (angular.isDefined(next)) {
				afterOrderNo = next.OrderNo;
			}
			current.OrderNo = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			$scope.saveIfNotSaving(current);
		};
		$scope.userLanguage = $rootScope.me.locale_id;

		let pdata = {};
		$scope.addNewProcessGroup = function () {
			pdata = { LanguageTranslation: {} };

			let save = function () {
				addNewProcessGroupSave();
				pdata = {};
			};
			let modifications = {
				create: false,
				cancel: true
			};
			let onChange = function () {
				if (pdata.LanguageTranslation) {
					modifications.create = checkTranslationLength(pdata.LanguageTranslation);
				}
			};

			let content = {
				buttons: [{ text: 'CANCEL', modify: 'cancel' }, { type: 'primary', onClick: save, text: 'SAVE', modify: 'create' }],
				inputs: [{ type: 'translation', label: 'PROCESS_GROUP_NAME', model: 'LanguageTranslation' }],
				title: 'ADD_NEW_PHASE'
			};
			MessageService.dialog(content, pdata, onChange, modifications);
		};

		let addNewProcessGroupSave = function () {
			let beforeOrderNo;
			let afterOrderNo;
			angular.forEach($scope.processGroups, function (group) {
				if (angular.isUndefined(beforeOrderNo)) {
					beforeOrderNo = group.OrderNo;
				}
				else if (beforeOrderNo < group.OrderNo) {
					beforeOrderNo = group.OrderNo;
				}
			});
			let max_order_no = Common.getOrderNoBetween(beforeOrderNo, afterOrderNo);
			let processGroup = pdata;
			processGroup.TabCount = 0;
			processGroup.OrderNo = max_order_no;
			SettingsService.ProcessGroupsAdd(StateParameters.process, processGroup).then(function (response) {
				$scope.processGroups.push(response);
			});
		};

		let checkTranslationLength = function (string) {
			if (string[$scope.userLanguage]) {
				let translation = string[$scope.userLanguage];
				if (translation.length > 0) {
					return true;
				}
			}
		};

		SaveService.subscribeSave($scope, function () {
			if (groupsToSave.length > 0) {
				savePhases();
			}
		});

		let savePhases = function() {
			SettingsService.ProcessGroupsSave(StateParameters.process, groupsToSave).then(function () {
				groupsToSave = [];
			});
		};

		let findParentNames = function(id){
			let groupParents = _.find(ProcessGroups, function(o) { return o.ProcessGroupId === id; });
			let tabParents = _.find(ProcessTabs, function(t){return t.ProcessTabId === id; });
			if (groupParents){
				return Translator.translation(groupParents.LanguageTranslation) + " " + groupParents.MaintenanceName;
			} else if (tabParents) {
				return Translator.translation(tabParents.LanguageTranslation) + " " + tabParents.MaintenanceName;
			}
		};

		$scope.searchBoys = function(){
			clearTimeout(searchTimer);
			if ($scope.searchText.length < 1){
				return
			}
			searchTimer = setTimeout(function(){
				let types = [];
				if ($scope.isField){
					types.push('FIELD');
				}
				if ($scope.isTab){
					types.push('TAB');
				}
				if($scope.isContainer){
					types.push('CONTAINER');
				}
				if (types.length > 0){
					ProcessesService.getSearchResults($scope.searchText, types, StateParameters.process).then(function(result){
						angular.forEach(result, function(row){
							if (row.GroupParent != 0){
								row.parentGroupName = findParentNames(row.GroupParent);
							}
							if (row.TabParent != 0){
								row.tabParentName = findParentNames(row.TabParent);
							}
						});
						$scope.searchResults = result;
					});
				} else {
					$scope.searchResults = [];
				}
			},1000);
		};

		$scope.showElement = function(element) {
			if (element.Type === 'FIELD' && element.TabParent == 0) {
				let params = StateParameters;
				params.fromFieldSearch = true;
				ViewService.view('settings.containers', { params: params })
			}
			if ((element.Type === 'TAB' && element.GroupParent != 0) || (element.Type === 'CONTAINER' && element.TabParent != 0) || (element.Type === 'FIELD' && element.TabParent != 0)) {
				let params = StateParameters;
				params.ProcessGroupId = element.GroupParent;
				params.OpenTab = element.TabParent;
				ViewService.view('settings.phases.tabs', { params: params })
			}
			if (element.Type === 'TAB' && element.GroupParent == 0){
				let params = StateParameters;
				params.fromTabSearch = true;
				ViewService.view('settings.containers', { params: params })
			}
			if (element.Type === 'CONTAINER' && element.TabParent == 0){
				let params = StateParameters;
				params.fromContainerSearch = true;
				ViewService.view('settings.containers', { params: params })
			}
		};

		$scope.translateFeature = function(f) {
			return Translator.translate("FEATURE_" + f.toString().toUpperCase());
		};
		
		$scope.processTabs = [];
		$scope.edit = false;
		SettingsService.ProcessGroups(StateParameters.process).then(function (groups) {
			if (!angular.isUndefined(groups)) {
				$scope.processProgress = groups;
			} else {
				$scope.processProgress = [];
			}
			let count = 0;
			angular.forEach(groups, function (group) {
				if (group.Type == 0 || group.Type == 1) {
					group.Status = 'active';
					SettingsService.ProcessGroupTabs(StateParameters.process, group.ProcessGroupId).then(function (data) {
						$scope.processTabs[group.ProcessGroupId] = data;
					});
					count += 1;
				}
			});
			if (count < 5) {
				let count2 = 5 - count;
				for (let i = 0; i < count2; i++) {
					$scope.processProgress.push({Type: 0, Status: 'inactive', Translation: '&nbsp;'});
				}
			}
		});

		SettingsService.ProcessPortfolios(StateParameters.process, 0).then(function (portfolios) {
			angular.forEach(portfolios, function (portfolio) {
				SettingsService.ProcessPortfoliosUserGroups(StateParameters.process, portfolio.ProcessPortfolioId).then(function (groups) {
					portfolio.ProcessUserGroupIds = groups;
				});
			});
			$scope.portfolios = portfolios;
		});

		$scope.changeState = function (status) {
			if (status == 'instanceMenu') {
				ViewService.view(status, {params: StateParameters});
			}
			else {
				ViewService.view('settings.' + status, {params: StateParameters}, {split: false});
			}
		};

	}
})();