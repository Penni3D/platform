(function () {
	'use strict';

	angular
		.module('core')
		.config(BannerConfig)
		.controller('FrontpageBannerController', FrontpageBannerController);

	BannerConfig.$inject = ['ViewsProvider'];

	function BannerConfig(ViewsProvider) {
		ViewsProvider.addView('frontpage.banner', {
			controller: 'FrontpageBannerController',
			template: 'frontpage/banner.html',
			resolve: {
				Widgets: function (ViewConfigurator) {
					var r = [];
					var widgets = ViewConfigurator.getWidgets();
					for (var i = 0, l = widgets.length; i < l; i++) {
						var w = widgets[i];
						if (w.LinkType == 5) {
							r.push(w);
						}
					}

					return r;
				}
			}
		});
	}

	FrontpageBannerController.$inject = [
		'$scope',
		'ViewService',
		'Widgets',
		'StateParameters',
		'$timeout',
		'$rootScope'
	];

	function FrontpageBannerController(
		$scope,
		ViewService,
		Widgets,
		StateParameters,
		$timeout,
		$rootScope
	) {
		$scope.widgets = Widgets;
		$scope.iconValues = {};
		$scope.setIcon = function (viewId, value) {
			var v = '';

			if (value > 9) {
				v = '9+';
			} else if (value > 0) {
				v = value;
			}

			$scope.iconValues[viewId] = v;
		};
		
		$scope.hasRows = {};

		let loadWidget = function (index : number, giveOnlyCount : boolean) {

			if(hasDataAlready[$scope.widgets[index].InstanceMenuId] == true) return;

			$timeout(function () {
				ViewService.viewAdvanced($scope.widgets[index].DefaultState,
					{
						params: angular.extend({prevent: true, frontpage: true, onlyCount: giveOnlyCount}, $scope.widgets[index].Params)
					},
					{
						targetId: 'frontpage-' + $scope.widgets[index].InstanceMenuId,
						ParentViewId: StateParameters.ViewId
					}
				);
			});

			if(giveOnlyCount == false){
				hasDataAlready[$scope.widgets[index].InstanceMenuId] = true;
			}
		}

		$rootScope.$on('notifyFrontpageRows', function (event, parameters) {
			_.each(parameters, function(value, key){
				$scope.hasRows[key] = value;
			})
		});

		let hasDataAlready = {};

		$scope.loadWidgetData = function (widgetId : number) {
			let thisWidgetIndex = _.findIndex($scope.widgets, function (o) {
				// @ts-ignore
				return o.InstanceMenuId == widgetId;
			});

			if(thisWidgetIndex != 0){
				loadWidget(thisWidgetIndex, false)
			} else {
				if($scope.widgets[0].Params.portfolioId) $scope.hasRows[$scope.widgets[0].Params.portfolioId] = true;
			}
		}

		angular.forEach($scope.widgets, function (w, ind) {
			loadWidget(ind, ind == 0 ? false : true)
		});
	}
})();
