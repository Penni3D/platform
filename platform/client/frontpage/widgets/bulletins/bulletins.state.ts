﻿(function () {
    'use strict';

    angular
        .module('core.widgets')
        .config(BulletinsWidgetConfig)
        .controller('BulletinsWidgetController', BulletinsWidgetController);

    BulletinsWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];
    function BulletinsWidgetConfig(WidgetsProvider, ViewsProvider) {
        ViewsProvider.addView('widget.bulletins', {
            template: 'frontpage/widgets/bulletins/bulletins.html',
            controller: 'BulletinsWidgetController',
            resolve: {
                Bulletins: function (StateParameters, NotificationService) {
                    return NotificationService.getBulletins(0, StateParameters.bulletinsAtStart);
                },

                Bulletins2: function(StateParameters, NotificationService){
                    return NotificationService.getStrictBulletins().then(function(y){
                        return y;
                    });
                }
            },
            params: {
                bulletinsAtStart: 3
            }
        });

        WidgetsProvider.addSystemWidget('widget.bulletins', {
            name: 'BULLETINS',
            options: {
                bulletinsAtStart: function () {
                    return {
                        type: 'string',
                        label: 'NUMBER_OF_BULLETINS_SHOWN_AT_START'
                    };
                }
            }
        });
    }

    BulletinsWidgetController.$inject = ['$scope', 'StateParameters', 'Bulletins', 'NotificationService', 'Bulletins2'];
    function BulletinsWidgetController($scope, StateParameters, Bulletins, NotificationService, Bulletins2) {

        let total = Bulletins2.rowdata.length;

        $scope.columns = NotificationService.getColumns();
        $scope.bulletins = [];

        $scope.loadMore = function () {
            initializeBulletins(Bulletins2.rowdata);
        };

        let startIndex = 0;
        let endIndex = 3;

        initializeBulletins(Bulletins2.rowdata);

        function initializeBulletins(bulletins) {

            for(let i = startIndex; i < endIndex; i++){
                if(i < total){
                    $scope.bulletins.push(bulletins[i]);
                }
            }
            startIndex = startIndex + 3;
            endIndex = endIndex + 3;

            $scope.bulletinsLeft = total - $scope.bulletins.length;
        }
    }
})();