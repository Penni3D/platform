(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(PortfolioWidgetConfig)
		.controller('WidgetParentPortfolioController', WidgetParentPortfolioController);

	PortfolioWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];
	function PortfolioWidgetConfig(WidgetsProvider, ViewsProvider) {
		WidgetsProvider.addSystemWidget('widget.parentPortfolio', {
			name: 'PORTFOLIO_OPEN_PARENT',
			options: {
				portfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'PORTFOLIO'
					};
				},
				parentProcess: function (resolves) {
					return {
						type: 'select',
						options: resolves.Processes,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessName',
						label: 'PARENT_PROCESS'
					};
				},
				parentItemName: function () {
					return {
						type: 'select',
						options: [],
						optionName: 'LanguageTranslation',
						optionValue: 'Name',
						label: 'PARENT_ITEMID',
						modify: false
					};
				},
				openView: function () {
					return {
						type: 'select',
						options: [],
						optionName: 'Feature',
						optionValue: 'DefaultState',
						label: 'PARENT_FEATURE',
						modify: false
					};
				},
				openFeaturette: function () {
					return {
						type: 'boolean',
						label: 'SPLIT_ROW_WHEN_OPENED'
					};
				}
			},
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				},
				Processes: function (ProcessesService) {
					return ProcessesService.get();
				},
				Features: function (SettingsService) {
					return SettingsService.ProcessGroups;
				},
				ProcessColumns: function (ProcessService) {
					return ProcessService.getColumns;
				}
			},
			onChange: function (p, params, resolves, options) {
				if (p === 'parentProcess') {
					resolves.Features(params.parentProcess).then(function (groups) {
						let features = [];
						for (let i = 0, l = groups.length; i < l; i++) {
							let group = groups[i];
							for (let j = 0, l_ = group.SelectedFeatures.length; j < l_; j++) {
								features.push(group.SelectedFeatures[j]);
							}
						}

						features = _.uniqBy(features, 'DefaultState');

						let o = options.openView;
						o.modify = true;
						o.options = features;
					});
				}

				if (p === 'portfolioId') {
					let p = resolves.Portfolios;
					let i = p.length;
					while (i--) {
						if (p[i].ProcessPortfolioId == params.portfolioId) {
							params.process = p[i].Process;
							break;
						}
					}

					resolves.ProcessColumns(params.process).then(function (columns) {
						let r = [];
						angular.forEach(columns, function (v) {
							if (!v.ReportColumn) r.push(v);
						});

						let o = options.parentItemName;
						o.modify = true;
						o.options = r;
					});
				}
			}
		});

		ViewsProvider.addView('widget.parentPortfolio', {
			controller: 'WidgetParentPortfolioController',
			template: 'frontpage/widgets/parentPortfolio/parentPortfolio.html',
			resolve: {
				Portfolio: function (PortfolioService, StateParameters) {
					return PortfolioService
						.getPortfolio(StateParameters.process, StateParameters.portfolioId)
						.then(function (portfolio) {
							let recursive = portfolio.Recursive > 1;
							return PortfolioService
								.getPortfolioData(
									StateParameters.process,
									StateParameters.portfolioId,
									{
										local: true,
										recursive: recursive
									})
								.then(function (portfolioData) {
									portfolio.Rows = portfolioData;
									return portfolio;
								});
						});
				},
				PortfolioColumns: function (PortfolioService, StateParameters) {
					return PortfolioService.getPortfolioColumns(StateParameters.process, StateParameters.portfolioId);
				}
			},
			afterResolve: {
				PortfolioRows: function (Portfolio, PortfolioColumns, Common, CalendarService) {
					let portfolioRows = Portfolio.Rows.rowdata;

					let i = PortfolioColumns.length;
					while (i--) {
						let column = PortfolioColumns[i].ItemColumn;
						let j = portfolioRows.length;
						while (j--) {
							let row = portfolioRows[j];
							let dataType = Common.getRelevantDataType(column);

							if (PortfolioColumns[i].ReportColumn) continue;
							
							if (dataType == 1 || dataType == 2) {
								row[column.Name] = Common.formatNumber(row[column.Name]);
							} else if (dataType == 3) {
								row[column.Name] = CalendarService.formatDate(row[column.Name]);
							} else if (dataType == 6) {
								//TODO: FIX THIS SHIT UP -- WHILE LOOPS to ANGULAR EACH $q to ensure that list is available
							}
						}
					}

					return portfolioRows;
				}
			}
		});
	}

	WidgetParentPortfolioController.$inject = [
		'$scope',
		'PortfolioRows',
		'PortfolioColumns',
		'ViewService',
		'StateParameters',
		'ProcessService',
		'Portfolio',
		'Common'
	];
	function WidgetParentPortfolioController(
		$scope,
		PortfolioRows,
		PortfolioColumns,
		ViewService,
		StateParameters,
		ProcessService,
		Portfolio,
		Common
	) {
		let p = ViewService.getView(StateParameters.ParentViewId);
		if (p && typeof p.scope.setIcon === 'function') {
			p.scope.setIcon(StateParameters.ViewId, Portfolio.Rows.rowcount);
		}

		$scope.portfolioRows = PortfolioRows;
		$scope.portfolioColumns = [];
		for (let i = 0, l = PortfolioColumns.length; i < l; i++) {
			let c = PortfolioColumns[i];
			if (c.ReportColumn) continue;
			$scope.portfolioColumns.push(c);
		}
		

		$scope.openRow = function (row) {
			let toState = {
				view: StateParameters.openView ? StateParameters.openView : 'process.tab',
				params: {
					process: StateParameters.parentProcess,
					itemId: row[StateParameters.parentItemName],
					openRow: undefined
				}
			};

			if (Common.isTrue(StateParameters.openFeaturette)) toState.params.openRow = row.item_id;
			ViewService.view(toState.view, { params: toState.params });
		};
	}
})();
