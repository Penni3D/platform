(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(ChartWidgetConfig)
		.controller('ChartWidgetController', ChartWidgetController);

	ChartWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];
	function ChartWidgetConfig(WidgetsProvider, ViewsProvider) {
		ViewsProvider.addView('widget.chart', {
			controller: 'ChartWidgetController',
			template: 'frontpage/widgets/chart/chart.html',
			resolve: {
				Chart: function (SettingsService, StateParameters) {
					return SettingsService.ProcessDashboardGetChart(StateParameters.process, StateParameters.chartId);
				}
			}
		});

		WidgetsProvider.addSystemWidget('widget.chart', {
			name: 'CHARTS',
			options: {
				chartId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Charts,
						label: 'Charts',
						optionName: 'LanguageTranslation',
						optionValue: 'Id'
					}
				}
			},
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				},
				Charts: function (SettingsService) {
					return SettingsService.ProcessDashboardGetCharts();
				}
			},
			onFinish: function (params, resolves) {
				var c = resolves.Charts;
				var p = resolves.Portfolios;

				var i = c.length;
				while (i--) {
					if (c[i].Id == params.chartId) {
						var j = p.length;
						while (j--) {
							if (c[i].ProcessPortfolioId == p[j].ProcessPortfolioId) {
								params.process = p[j].Process;
								break;
							}
						}

						break;
					}
				}

			}
		});
	}

	ChartWidgetController.$inject = ['$scope', 'StateParameters', 'Chart'];
	function ChartWidgetController($scope, StateParameters, Chart) {
		$scope.process = StateParameters.process;
		$scope.chart = Chart;
	}
})();