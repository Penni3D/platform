(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(ChartsWidgetConfig)
		.controller('ChartsWidgetController', ChartsWidgetController);

	var chartLists = [
		{
			vp: 'WIDGET_CHARTS_FIRST_CHART_COLUMN',
			vt: 'WIDGET_CHARTS_FIRST_CHART_TITLE',
			p: 'firstChartColumn',
			t: 'firstChartTitle'
		},
		{
			vp: 'WIDGET_CHARTS_SECOND_CHART_COLUMN',
			vt: 'WIDGET_CHARTS_SECOND_CHART_TITLE',
			p: 'secondChartColumnName',
			t: 'secondChartTitle'
		},
		{
			vp: 'WIDGET_CHARTS_THIRD_CHART_COLUMN',
			vt: 'WIDGET_CHARTS_THIRD_CHART_TITLE',
			p: 'thirdChartColumnName',
			t: 'thirdChartTitle'
		},
		{
			vp: 'WIDGET_CHARTS_FOURTH_CHART_COLUMN',
			vt: 'WIDGET_CHARTS_FOURTH_CHART_TITLE',
			p: 'fourthChartColumnName',
			t: 'fourthChartTitle'
		}];

	ChartsWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];
	function ChartsWidgetConfig(WidgetsProvider, ViewsProvider) {
		var options = {
			navigationMenuId: function (resolves) {
				return {
					type: 'select',
					label: 'WIDGET_CHARTS_IM',
					options: resolves.NavigationMenu,
					optionName: 'LanguageTranslation',
					optionValue: 'InstanceMenuId'
				};
			},
			allowDrill: function () {
				return {
					type: 'boolean',
					label: 'WIDGET_CHARTS_DRILL'
				};
			},
			showNoData: function () {
				return {
					type: 'boolean',
					label: 'WIDGET_CHARTS_SHOW_EMPTY'
				};
			}
		};

		angular.forEach(chartLists, function (list) {
			options[list.p] = function () {
				return {
					type: 'select',
					label: list.vp,
					options: [],
					optionValue: 'ItemColumn.Name',
					optionName: 'ItemColumn.LanguageTranslation',
					modify: false
				};
			};

			options[list.t] = function () {
				return {
					type: 'translation',
					label: list.vt
				};
			};
		});

		ViewsProvider.addView('widget.charts', {
			controller: 'ChartsWidgetController',
			template: 'frontpage/widgets/chart/charts.html',
			resolve: {
				Rows: function (PortfolioService, StateParameters) {
					return PortfolioService
						.getSimplePortfolioData(
							StateParameters.process,
							StateParameters.portfolioId)
						.then(function (rows) {
							return rows.rowdata;
					});
				},
				ActiveFilters: function (PortfolioService, StateParameters) {
					return PortfolioService.getActiveSettings(StateParameters.portfolioId);
				},
				ListsColumns: function (StateParameters, $q, ProcessService, PortfolioService, Common) {
					return PortfolioService.getPortfolioColumns(StateParameters.process, StateParameters.portfolioId)
						.then(function (portfolioColumns) {
							var promises = [];
							var allLists = [];
							var lists = {};
							for (var i = 0, k = chartLists.length; i < k; i++) {
								var list = chartLists[i];
								var l = StateParameters[list.p];
								if (l) {
									var j = portfolioColumns.length;
									while (j--) {
										var ic = portfolioColumns[j].ItemColumn;
										var dt = Common.getRelevantDataType(ic);
										if (ic.Name == l && dt == 6) {
											allLists.push(Common.getRelevantDataAdditional(ic));
											break;
										}
									}
								}
							}

							angular.forEach(_.uniq(allLists), function (list) {
								promises.push(ProcessService
									.getListItems(list)
									.then(function (items) {
										lists[list] = items;
									})
								);
							});

							return $q.all(promises).then(function () {
								return {
									lists: lists,
									columns: portfolioColumns
								};
							});
						});
				}
			}
		});

		WidgetsProvider.addSystemWidget('widget.charts', {
			name: 'CHARTS_MULTIPLE',
			options: options,
			onSetup: {
				PortfolioColumns: function (PortfolioService) {
					return PortfolioService.getPortfolioColumns;
				},
				NavigationMenu: function (SidenavService) {
					var r = [];

					var links = SidenavService.getStaticLinks();
					var i = links.length;
					while (i--) {
						if (links[i].view === 'portfolio') {
							r.push(links[i]);
						}
					}

					return r;
				}
			},
			onChange: function (p, params, resolves, options) {
				if (p === 'navigationMenuId') {
					var nm = resolves.NavigationMenu;
					var j = nm.length;
					while (j--) {
						if (params.navigationMenuId == nm[j].InstanceMenuId) {
							params.process = nm[j].params.process;
							params.portfolioId = nm[j].params.portfolioId;

							break;
						}
					}

					var i = chartLists.length;
					while (i--) {
						var o = options[chartLists[i].p];
						o.modify = false;
						o.options = [];
					}

					resolves.PortfolioColumns(params.process, params.portfolioId).then(function (columns) {
						for (var i = 0, l = chartLists.length; i < l; i++) {
							var o = options[chartLists[i].p];
							o.modify = true;
							o.options = columns;
						}
					});
				}
			}
		});
	}

	ChartsWidgetController.$inject = [
		'$scope',
		'StateParameters',
		'Rows',
		'ListsColumns',
		'Common',
		'Colors',
		'Translator',
		'ActiveFilters',
		'SidenavService'
	];
	function ChartsWidgetController(
		$scope,
		StateParameters,
		Rows,
		ListsColumns,
		Common,
		Colors,
		Translator,
		ActiveFilters,
		SidenavService
	) {
		var noData = 'WIDGET_CHARTS_NO_DATA';
		var emptyColor = 'lightGrey';
		var fillItem = {};

		$scope.charts = [];

		angular.forEach(chartLists, function (list) {
			var column = StateParameters[list.p];
			if (column) {
				var calculations = {};
				var chart = {
					title: Translator.translation(StateParameters[list.t]),
					data: [],
					names: [],
					labels: [],
					colors: [],
					onClick: function (charts) {
						var clickedChart = charts[0];
						if (Common.isTrue(StateParameters.allowDrill) && clickedChart) {
							var i = ListsColumns.columns.length;
							while (i--) {
								var portfolioColumn = ListsColumns.columns[i];
								if (portfolioColumn.UseInFilter && portfolioColumn.ItemColumn.Name == column) {
									ActiveFilters.filters.text = '';
									ActiveFilters.filters.phase = [];

									angular.forEach(ActiveFilters.filters.select, function (filter, key) {
										ActiveFilters.filters.select[key] = [];
									});

									angular.forEach(ActiveFilters.filters.range, function (filter, key) {
										ActiveFilters.filters.range[key].start = null;
										ActiveFilters.filters.range[key].end = null
									});

									var itemId = fillItem[chart.names[clickedChart._index]];
									if (itemId) {
										angular.forEach(ActiveFilters.filters.select, function (v, k) {
											ActiveFilters.filters.select[k] = [];

											if (k == portfolioColumn.ItemColumn.ItemColumnId) {
												ActiveFilters.filters.select[k].push(itemId);
											}
										});
									} else {
										ActiveFilters.filters.text = chart.names[clickedChart._index];
									}

									SidenavService.staticNavigation(StateParameters.navigationMenuId);
									break;
								}
							}
						}
					}
				};

				var i = Rows.length;
				while (i--) {
					var data = Rows[i][column];
					if (Array.isArray(data) && data.length) {
						var j = data.length;
						while (j--) {
							var selection = data[j];
							angular.forEach(ListsColumns.lists, function (list) {
								var j = list.length;
								while (j--) {
									var item = list[j];
									if (item.item_id == selection) {
										if (typeof calculations[item.list_symbol_fill] === 'undefined') {
											calculations[item.list_symbol_fill] = 0;
										}

										fillItem[item.list_symbol_fill] = item.item_id;
										calculations[item.list_symbol_fill] += 1;
										break;
									}
								}
							});
						}
					} else if (data) {
						if (typeof calculations[data] === 'undefined') {
							calculations[data] = 0;
						}

						calculations[data] += 1;
					} else if (data == null && Common.isTrue(StateParameters.showNoData)) {
						if (typeof calculations[noData] === 'undefined') {
							calculations[noData] = 0;
						}

						calculations[noData] += 1;
					}
				}

				angular.forEach(calculations, function (value, key) {
					chart.data.push(value);
					chart.names.push(key);
					chart.labels.push(Translator.translate(key));

					var colour = Colors.getColor(key).rgb;
					if (colour) {
						chart.colors.push(colour);
					} else {
						chart.colors.push(Colors.getColor(emptyColor).rgb);
					}
				});

				if (!chart.data.length) {
					chart.data.push(1);
					chart.names.push("nodata");
					chart.labels.push(Translator.translate(noData));
					chart.colors.push(Colors.getColor(emptyColor).rgb);
				}

				$scope.charts.push(chart);
			}
		});
	}
})();