﻿(function () {
    'use strict';

    angular
        .module('core.widgets')
		.config(flexTimeTrackingConfig)
		.controller('flexTimeTrackingController', flexTimeTrackingController);

	flexTimeTrackingConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];
	function flexTimeTrackingConfig(WidgetsProvider, ViewsProvider) {       
		ViewsProvider.addView('widget.flexTimeTracking', {
			template: 'frontpage/widgets/flexTimeTracking/flexTimeTracking.html',
			controller: 'flexTimeTrackingController'
        });
    
		WidgetsProvider.addSystemWidget('widget.flexTimeTracking', {
			name: 'WIDGET_FLEX_TIME_TRACKING_TITLE'
        });
    }

	flexTimeTrackingController.$inject = ['$scope'];
	function flexTimeTrackingController($scope) {

	}
})();