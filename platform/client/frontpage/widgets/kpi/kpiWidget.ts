(function () {
	'use strict';

	angular
		.module('core.widgets')
		.service('KpiWidgetService', KpiWidgetService)
		.config(KpiWidgetConfig)
		.controller('KpiWidgetController', KpiWidgetController);

	KpiWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider','BackgroundsProvider'];

	function KpiWidgetConfig(WidgetsProvider, ViewsProvider, BackgroundsProvider) {
		WidgetsProvider.addSystemWidget('widget.kpiWidget', {
			name: 'KPI_WIDGET',
			options: {
				backgroundWidget: function () {
					return {
						type: 'select',
						options: BackgroundsProvider.getBackgrounds(),
						label: 'WIDGET_BUTTONS_WBI',
						optionValue: 'background'
					};
				},
				kpi1Title: function () {
					return {
						type: 'string',
						label: 'KPI 1 Title'
					};
				},
				kpi1suffix: function () {
					return {
						type: 'string',
						label: 'KPI 1 Suffix'
					};
				},
				kpi1PortfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'KPI 1 Portfolio'
					};
				},
				kpi1PortfolioColumn: function () {
					return {
						type: 'select',
						options: [],
						optionName: 'ItemColumn.Variable',
						optionValue: 'ItemColumn.Name',
						label: 'KPI 1 Column'
					};
				},
				kpi1Operation: function (resolves) {
					return {
						type: 'select',
						options: resolves.ColumnOperators,
						optionName: 'Name',
						optionValue: 'Value',
						label: 'KPI 1 Operator'
					};
				},
				kpi2Title: function () {
					return {
						type: 'string',
						label: 'KPI 2 Title'
					};
				},
				kpi2suffix: function () {
					return {
						type: 'string',
						label: 'KPI 1 Suffix'
					};
				},
				kpi2PortfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'KPI 2 Portfolio'
					};
				},
				kpi2PortfolioColumn: function () {
					return {
						type: 'select',
						options: [],
						optionName: 'ItemColumn.Variable',
						optionValue: 'ItemColumn.Name',
						label: 'KPI 2 Column'
					};
				},
				kpi2Operation: function (resolves) {
					return {
						type: 'select',
						options: resolves.ColumnOperators,
						optionName: 'Name',
						optionValue: 'Value',
						label: 'KPI 2 Operator'
					};
				},
				kpi3Title: function () {
					return {
						type: 'string',
						label: 'KPI 3 Title'
					};
				},
				kpi3suffix: function () {
					return {
						type: 'string',
						label: 'KPI 3 Suffix'
					};
				},
				kpi3PortfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'KPI 3 Portfolio'
					};
				},
				kpi3PortfolioColumn: function () {
					return {
						type: 'select',
						options: [],
						optionName: 'ItemColumn.Variable',
						optionValue: 'ItemColumn.Name',
						label: 'KPI 3 Column'
					};
				},
				kpi3Operation: function (resolves) {
					return {
						type: 'select',
						options: resolves.ColumnOperators,
						optionName: 'Name',
						optionValue: 'Value',
						label: 'KPI 3 Operator'
					};
				},
				kpi4Title: function () {
					return {
						type: 'string',
						label: 'KPI 4 Title'
					};
				},
				kpi4suffix: function () {
					return {
						type: 'string',
						label: 'KPI 4 Suffix'
					};
				},
				kpi4PortfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'KPI 4 Portfolio'
					};
				},
				kpi4PortfolioColumn: function () {
					return {
						type: 'select',
						options: [],
						optionName: 'ItemColumn.Variable',
						optionValue: 'ItemColumn.Name',
						label: 'KPI 4 Column'
					};
				},
				kpi4Operation: function (resolves) {
					return {
						type: 'select',
						options: resolves.ColumnOperators,
						optionName: 'Name',
						optionValue: 'Value',
						label: 'KPI 4 Operator'
					};
				},
				kpi5Title: function () {
					return {
						type: 'string',
						label: 'KPI 5 Title'
					};
				},
				kpi5suffix: function () {
					return {
						type: 'string',
						label: 'KPI 5 Suffix'
					};
				},
				kpi5PortfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'KPI 5 Portfolio'
					};
				},
				kpi5PortfolioColumn: function () {
					return {
						type: 'select',
						options: [],
						optionName: 'ItemColumn.Variable',
						optionValue: 'ItemColumn.Name',
						label: 'KPI 5 Column'
					};
				},
				kpi5Operation: function (resolves) {
					return {
						type: 'select',
						options: resolves.ColumnOperators,
						optionName: 'Name',
						optionValue: 'Value',
						label: 'KPI 5 Operator'
					};
				}

			},
			onChange: function (p, params, resolves, options) {
				if (p.indexOf("Portfolio") > -1 && p.indexOf("Column") == -1 && p.indexOf("Operation") == -1) {
					let portfolio = resolves.Portfolios.find(pid => {
						return pid.ProcessPortfolioId == params[p];
					});
					options[p.replace("PortfolioId", "Portfolio") + "Column"].options = portfolio.Columns.filter(co => {
						return co.ItemColumn.DataType == 1 ||
							co.ItemColumn.DataType == 2 ||
							(co.ItemColumn.DataType == 16 && (co.ItemColumn.SubDataType == 2 || co.ItemColumn.SubDataType == 1)) ||
							(co.ItemColumn.DataType == 20 && (co.ItemColumn.SubDataType == 2 || co.ItemColumn.SubDataType == 1));
					});
				}
			},
			onSetup: {
				Portfolios: function (PortfolioService, $q) {
					return PortfolioService.getPortfolios().then(function (portfolios) {
						let promises = [];
						let result = [];
						for (let p of portfolios) {
							promises.push(PortfolioService.getPortfolioColumns(p.Process, p.ProcessPortfolioId).then(function (columns) {
								p.Columns = columns;
								result.push(p);
							}));
						}

						return $q.all(promises).then(function () {
							return result;
						});
					});
				},
				ColumnOperators: function () {
					let ops = [];
					ops.push({Name: "Count", Value: "COUNT"});
					ops.push({Name: "Average", Value: "AVG"});
					ops.push({Name: "Sum", Value: "SUM"});
					ops.push({Name: "Min", Value: "MIN"});
					ops.push({Name: "Max", Value: "MAX"});
					return ops;
				}
			}
		});

		ViewsProvider.addView('widget.kpiWidget', {
			controller: 'KpiWidgetController',
			template: 'frontpage/widgets/kpi/kpiWidget.html',
			resolve: {
				KPIData: function (StateParameters, KpiWidgetService) {
					let objs = [];
					let c = "";
					let co;

					const ordered = Object.keys(StateParameters).sort().reduce(
						(obj, key) => {
							obj[key] = StateParameters[key];
							return obj;
						},
						{}
					);
					
					_.each(ordered, function (obj, key) {
						if (key.indexOf("kpi") > -1) {
							let i = key.substring(3, 4);
							if (i != c) {
								if (co) objs.push(co);
								co = {};
							}
							co[key.replace("kpi" + i, "")] = obj;
							c = i;
						} else {
							if (co) objs.push(co);
						}
					});
					if (co) objs.push(co);
					return KpiWidgetService.getKpiData(objs);
				}
			}
		});
	}

	function KpiWidgetService(ApiService) {
		let api = ApiService.v1api('features/KpiWidget');
		let getKpiData = function (KpiRequests) {
			for (let o of KpiRequests) {
				if (o.Operation == "") o.Operation = "SUM";
				if (o.PortfolioId == "") o.PortfolioId = 0;
				if (!o.Title) o.Title = "";
				if (!o.suffix) o.suffix = "";
			}
			return api.new([], KpiRequests);
		};

		return {
			getKpiData: getKpiData
		}
	}

	KpiWidgetController.$inject = [
		'$scope',
		'StateParameters',
		'KPIData',
		'Common',
		'UniqueService'
	];

	function KpiWidgetController(
		$scope,
		StateParameters,
		KPIData,
		Common,
		UniqueService
	) {
		$scope.id = UniqueService.get('widget-kpis', true);
		$scope.wide = StateParameters.ViewSize === "col-xs-12";
		
		if (!$scope.wide) {
			$scope.mainItem = KPIData[0];
			$scope.items = KPIData.splice(1, 4);
			setTimeout(function () {
				$('#' + $scope.id + ' .background').addClass(StateParameters.backgroundWidget);
			});
		} else {
			$scope.items = KPIData;
		}
		
		$scope.fn = function(v) {
			return Common.formatNumber(v, 0);
		}
	}
})();
