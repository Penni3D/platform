(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(DashboardWidgetConfig)
		.controller('DashboardWidgetController', DashboardWidgetController);

	DashboardWidgetConfig.$inject = ['ViewsProvider', 'WidgetsProvider'];
	function DashboardWidgetConfig(ViewsProvider, WidgetsProvider) {
		WidgetsProvider.addSystemWidget('widget.dashboard', {
			name: 'DASHBOARD',
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				},
				Dashboards: function (SettingsService) {
					return SettingsService.ProcessDashboards;
				},
				Translate: function (Translator) {
					return Translator.translation;
				}
			},
			onChange: function (p, params, resolves, options) {
				if (p === 'portfolioId') {
					let portfolios = resolves.Portfolios;
					let i = portfolios.length;
					while (i--) {
						if (params.portfolioId == portfolios[i].ProcessPortfolioId) {
							params.process = portfolios[i].Process;
							break;
						}
					}

					params.dashboardId = undefined;

					resolves.Dashboards(params.process, params.portfolioId).then(function (dashboards) {
						resolves.Dashes = dashboards;
						options.dashboardId.modify = true;
						options.dashboardId.options = dashboards;
					});
				}
			},
			options: {
				portfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'PORTFOLIO'
					};
				},
				dashboardId: function () {
					return {
						type: 'select',
						modify: false,
						options: [],
						optionName: 'LanguageTranslation',
						optionValue: 'Id',
						label: 'DASHBOARD'
					};
				}
			}
		});

		ViewsProvider.addView('widget.dashboard', {
			controller: 'DashboardWidgetController',
			template: 'frontpage/widgets/dashboard/dashboard.html',
			resolve: {
				Dashboard: function (SettingsService, StateParameters) {
					return SettingsService.ProcessDashboardGet(StateParameters.process, StateParameters.dashboardId);
				}
			}
		});
	}

	DashboardWidgetController.$inject = ['$scope', 'StateParameters', 'Dashboard'];
	function DashboardWidgetController($scope, StateParameters, Dashboard) {
		$scope.process= StateParameters.process;
		$scope.process = StateParameters.process;
		$scope.portfolioId = StateParameters.portfolioId;
		$scope.dashboard = Dashboard;
	}
})();
