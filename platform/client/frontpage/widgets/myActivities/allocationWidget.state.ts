﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(AllocationWidgetConfig)
		.controller('AllocationWidgetController', AllocationWidgetController)
		.service('AllocationWidgetService', AllocationWidgetService);

	AllocationWidgetController.$inject = ['$scope', 'ViewService', 'Allocation', 'StateParameters'];
	function AllocationWidgetController($scope, ViewService, Allocation, StateParameters) {
		$scope.approvableRequests = Allocation.data.approvableRequests;
		
		$scope.openRow = function (request) {
			ViewService.view('resourceAllocation', {
				params: {
					highlightRow: request.item_id
				}
			});
		};

		let p = ViewService.getView(StateParameters.ParentViewId);
		if (p && typeof p.scope.setIcon === 'function') {
			p.scope.setIcon(StateParameters.ViewId, $scope.approvableRequests.length);
		}
	}

	AllocationWidgetService.$inject = ['ApiService'];
	function AllocationWidgetService(ApiService) {
		let self = this;
		let MyActivities = ApiService.v1api('core/myActivities');

		self.allocationData = function () {
			return MyActivities.get('allocation');
		};
	}

	AllocationWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];
	function AllocationWidgetConfig(WidgetsProvider, ViewsProvider) {
		ViewsProvider.addView('widget.myAllocations', {
			template: 'frontpage/widgets/myActivities/allocationWidget.html',
			controller: AllocationWidgetController,
			resolve: {
				Allocation: function (AllocationWidgetService) {
					return AllocationWidgetService.allocationData();
				}
			}
		});

		WidgetsProvider.addSystemWidget('widget.myAllocations', {
			name: 'MY_ALLOCATIONS'
		});
	}
})();