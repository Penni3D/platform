﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(TaskWidgetConfig)
		.controller('TaskWidgetController', TaskWidgetController);

	TaskWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];

	function TaskWidgetConfig(WidgetsProvider, ViewsProvider) {
		ViewsProvider.addView('widget.myTasks', {
			template: 'frontpage/widgets/myActivities/taskWidget.html',
			controller: 'TaskWidgetController',
			beforeResolve: {
				Portfolio: function (PortfolioService, StateParameters) {
					return PortfolioService.getPortfolio("allocation_or_task", StateParameters.portfolioId).then(function (portfolio) {
						return portfolio;
					});
				}
			},
			resolve: {
				Allocations: function (AllocationService, $rootScope) {
					return AllocationService
						.getResourceAllocationData(undefined, $rootScope.me.item_id)
						.then(function (allocation) {
							return allocation.rowdata;
						});
				},
				CurrentUser: function (UserService) {
					return UserService.getCurrentUser();
				},
				PortfolioColumns: function (Common, StateParameters, Translator) {
					let cols = [];

					let t = {
						ItemColumn: {
							DataType: 0,
							Name: 'primary',
							LanguageTranslation: StateParameters.primary
						},
						Width: 334,
						UseInSelectResult: 1
					};

					let p = {
						ItemColumn: {
							DataType: 0,
							Name: 'secondary',
							LanguageTranslation: StateParameters.secondary
						},
						Width: 334,
						UseInSelectResult: 1
					};

					let at = {
						ItemColumn: {
							DataType: 0,
							Name: 'allocation_total',
							LanguageTranslation: Translator.translate('ALLOCATION_TOTAL')
						},
						Width: 100,
						UseInSelectResult: 11
					};

					cols.push(t);
					if (!Common.isTrue(StateParameters.showUser)) cols.push(p);
					cols.push(at);

					return cols;
				},
				Tasks: function (StateParameters, PortfolioService, $q, ProcessService, Portfolio) {
					if (StateParameters.portfolioId > 0) {
						return PortfolioService
							.getPortfolioData(Portfolio.Process, StateParameters.portfolioId, {local: true}, {limit: 100})
							.then(function (rows) {
								let promises = [];
								let i = rows.rowdata.length;
								while (i--) {
									promises.push(ProcessService.prepareItem(Portfolio.Process, rows.rowdata[i]));
								}
								return $q.all(promises).then(function () {
									for (let r of rows.rowdata) {
										if (r.process == 'allocation') {
											r.owner_item_id = r.process_item_id;
											r.process = 'task';
										}
									}
									return rows.rowdata;

								});
							});
					}


				}
			},
			afterResolve: {
				Tasks: function (Allocations, Tasks, CurrentUser, StateParameters, Common, ProcessService) {
					let i = Tasks.length;
					while (i--) {
						let r = Tasks[i];
						r.actual_item_id = r.item_id;

						let j = Allocations.length;
						while (j--) {
							let a = Allocations[j];

							if (a.task_item_id == r.item_id) {
								r.item_id = a.item_id;
								break;
							}
						}

						if (Common.isTrue(StateParameters.showUser)) {
							r.parent_item_id = CurrentUser.item_id + "_" + r.owner_item_id;
						} else {
							r.parent_item_id = 0;
						}
						ProcessService.ignoreColumn('task', 'parent_item_id', true);
					}


					if (Common.isTrue(StateParameters.showUser)) {
						for (let a of Allocations) {
							if (a.process != "task" && a.process != "user" && a.process != "allocation") {
								a.parent_item_id = CurrentUser.item_id;
								Tasks.unshift(a);
							}
						}

						let c = angular.copy(CurrentUser);
						c.parent_item_id = 0;

						Tasks.unshift(c);
					}
					return Tasks;
				}
			}
		});

		WidgetsProvider.addSystemWidget('widget.myTasks', {
			name: 'MY_TASKS',
			options: {
				portfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionValue: 'ProcessPortfolioId',
						optionName: 'LanguageTranslation',
						label: 'Portfolio'
					};
				},
				showUser: function () {
					return {
						type: 'boolean',
						label: 'SHOW_USER'
					};
				},
				primary: function () {
					return {
						type: 'translation',
						label: 'PRIMARY'
					};
				},
				secondary: function () {
					return {
						type: 'translation',
						label: 'SECONDARY'
					};
				}
			},
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('task').then(function (tp) {
						return PortfolioService.getPortfolios('allocation').then(function (ap) {
							let result = [];
							for (let t of tp) {
								result.push({
									ProcessPortfolioId: t.ProcessPortfolioId,
									LanguageTranslation: t.LanguageTranslation
								});
							}
							for (let t of ap) {
								result.push({
									ProcessPortfolioId: t.ProcessPortfolioId,
									LanguageTranslation: t.LanguageTranslation
								});
							}
							return result;
						});
					});
				}
			},
			onFinish: function (params) {
				params.process = 'task';
			}
		});
	}

	TaskWidgetController.$inject = ['$scope', 'PortfolioColumns', 'Tasks', 'ViewService', 'ProcessService', 'Allocations', 'AllocationService', 'StateParameters', 'CurrentUser', 'Portfolio'];

	function TaskWidgetController($scope, PortfolioColumns, Tasks, ViewService, ProcessService, Allocations, AllocationService, StateParameters, CurrentUser, Portfolio) {
		let clicked = false;

		let p = ViewService.getView(StateParameters.ParentViewId);
		if (p && typeof p.scope.setIcon === 'function') {
			p.scope.setIcon(StateParameters.ViewId, Tasks.length);
		}

		ViewService.footbar(StateParameters.ViewId, {template: 'core/allocation/views/allocationFootbar.html'});

		$scope.portfolioColumns = PortfolioColumns;
		$scope.AMOptions = {
			calendarSteps: [15, 60, 360],
			showCalendar: Allocations.length > 1,
			selectable: false,
			hideHeader: true,
			visibleColumns: ['primary', 'secondary', 'allocation_total'],
			openRows: [CurrentUser.item_id],
			rowClick: function (itemId) {
				if (clicked || CurrentUser.item_id == itemId || Portfolio.Process == 'allocation') {
					return;
				}

				clicked = true;
				angular.forEach($scope.tasks, function (task) {
					if (task.item_id == itemId) {
						if (task.process != "task") {
							if (task.process != "user") {
								let params = {
									process: task.linkedProcess ? task.linkedProcess : task.process,
									title: task.primary,
									itemId: ProcessService.getActualItemId(task.actual_item_id ? task.actual_item_id : task.item_id),
								};
								ViewService.view('process.gantt', {params: params});
							} else {
								clicked = false;
							}
							return;
						}
						ProcessService.getProcess(task.owner_item_id).then(function (p) {
							let params = {
								process: p,
								title: task.primary,
								itemId: task.owner_item_id,
								rowId: task.actual_item_id
							};

							if (task.owner_item_type === 'backlog') {
								ViewService.view('process.backlog', {params: params});
							} else {
								if (task.task_type == 21) {
									params.jumpToKanban = true;
									ViewService.view('process.gantt', {params: params});
								} else {
									ViewService.view('process.gantt', {params: params});
									let f = {
										process: 'task',
										title: params.title,
										itemId: params.rowId
									};

									ViewService.view('featurette', {params: f}, {split: true});
								}
							}

							clicked = false;
						});
					}
				});
			}
		};

		if (StateParameters.showUser) {
			$scope.AMRules = {
				parents: {
					51: ['root'], //User under root
					52: [51], //Project under user
					50: [51] //allocation under project
				}
			};
		} else {
			$scope.AMRules = {
				parents: {
					52: ['root'], //Project under root
					50: [51] //allocation under project
				}
			};
		}

		function orderByAllocation(rows) {
			let ret = [];
			let relevant = [];
			let notRelevant = [];

			let t = new Date();
			let relevantAllocations = AllocationService.getCalendarDataByDate(t.getFullYear(), t.getMonth() + 1, t.getDate());

			let i = rows.length;
			while (i--) {
				let r = rows[i];

				if (typeof relevantAllocations[r.item_id] === 'undefined') {
					notRelevant.push(r);
				} else {
					relevant.push(r);
				}
			}

			if (relevant.length) {
				ret.push(relevant);
			}

			if (notRelevant.length) {
				ret.push(notRelevant);
			}

			return ret;
		}

		function orderByStatus(rows, allocations) {
			let ret = [];
			let high = [];
			let med = [];
			let low = [];
			let unknowns = [];

			let i = rows.length;
			while (i--) {
				let r = rows[i];
				let j = allocations.length;

				while (j--) {
					let a = allocations[j];
					if (a.item_id == r.item_id) {
						if (a.status == 50) {
							high.push(r);
							break;
						} else if (a.status == 40) {
							med.push(r);
							break;
						} else if (a.status == 20 || a.status == 25 || a.status == 13) {
							low.push(r);
							break;
						}
					}
				}

				if (j === -1) {
					unknowns.push(r);
				}
			}

			if (high.length) {
				ret.push(high);
			}

			if (med.length) {
				ret.push(med);
			}

			if (low.length) {
				ret.push(low);
			}

			if (unknowns.length) {
				ret.push(unknowns);
			}

			return ret;
		}

		function orderByTitle(rows) {
			return rows.sort(function (a, b) {
				return a.title < b.title ? -1 : 1;
			});

		}

		function clearByStatus(rows, allocations) {
			let i = rows.length;
			while (i--) {
				let r = rows[i];
				let j = allocations.length;

				while (j--) {
					let a = allocations[j];
					if (a.item_id == r.item_id) {
						if (!(
							a.status == 55 ||
							a.status == 50 ||
							a.status == 40 ||
							a.status == 25 ||
							a.status == 20 ||
							a.status == 13 ||
							a.task_type == 51 || a.task_type == 52)
						) {
							rows.splice(i, 1);
						}

						break;
					}
				}
			}

			return rows;
		}

		function initialize() {
			clearByStatus(Tasks, Allocations);
			let a = orderByAllocation(Tasks);

			let rows = [];
			for (let i = 0, l = a.length; i < l; i++) {
				a[i] = orderByStatus(a[i], Allocations);
				orderByTitle(a[i]);

				for (let j = 0, l_ = a[i].length; j < l_; j++) {
					for (let k = 0, l__ = a[i][j].length; k < l__; k++) {
						rows.push(a[i][j][k]);
					}
				}
			}

			//Remove Parents With no Children
			let removeIndexes = [];
			for (let q = 0; q < rows.length; q++) {
				let p = rows[q];
				if (p.process != "task" && p.process != "user") {
					let children = false;
					for (let c of rows) {
						if (c.parent_item_id == p.item_id) {
							children = true;
							break;
						}
					}
					if (!children) removeIndexes.push(q);
				}
				
				//Fool addTotals function to think tasks are allocations
				p.linkedProcess = angular.copy(p.process);
				if (p.process == "task" && StateParameters.showUser == "true") {
					p.process = "allocation";
					p.status = 1;
				}
			}
			for (let i = removeIndexes.length - 1; i >= 0; i--) rows.splice(removeIndexes[i], 1);
			$scope.tasks = AllocationService.addTotals(rows, StateParameters.showUser == "true" ? CurrentUser.item_id : 0, 0, true, true);
			for (let row of rows) {
				if(row.process == 'allocation') row.process = "task";
			}
		}

		initialize();

		$scope.hideStatus = {
			draft: true,
			reject: true,
			action: true,
			release: true
		};
	}
})();