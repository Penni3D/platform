﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(TimeBreakdownWidgetConfig)
		.controller('TimeBreakdownWidgetController', TimeBreakdownWidgetController);

	TimeBreakdownWidgetController.$inject = [
		'$scope',
		'StateParameters',
		'ViewService',
		'ProcessService',
		'Configuration',
		'WorktimeHourStatusService',
		'ListItems',
		'$q',
		'Common',
		'ChartColourService',
		'LocalStorageService',
		'$rootScope'
	];

	function TimeBreakdownWidgetController($scope,
	                                       StateParameters,
	                                       ViewService,
	                                       ProcessService,
	                                       Configuration,
	                                       WorktimeHourStatusService,
	                                       ListItems,
	                                       $q,
	                                       Common,
	                                       ChartColourService,
	                                       LocalStorageService,
	                                       $rootScope) {
		$scope.users = [];
		$scope.data = [];
		$scope.weeks = [];
		$scope.colors = [];

		let fullColors = _.map(ChartColourService.getColours(['cyan', 'orange', 'pink', 'purple', 'red']));
		_.each(ListItems, function (item, index) {
			$scope.colors.push({
					backgroundColor: fullColors[index],
					pointBackgroundColor: fullColors[index],
					hoverBackgroundColor: fullColors[index],
					fill: true
				}
			);
		});

		$scope.activeTab = LocalStorageService.get("breakdown_" + $rootScope.me.item_id);

		$scope.changeActiveTab = function (tabno) {
			LocalStorageService.store("breakdown_" + $rootScope.me.item_id, tabno);
		};


		$scope.p = fullColors;
		$scope.piedata = [];
		$scope.pieSeries = [];

		let s = moment().startOf('week').add(-10, 'w');
		let e = moment().endOf('week');
		$scope.weekRange = {
			from: s,
			to: e
		};

		let l = $scope.weekRange.to.diff($scope.weekRange.from, 'week') + 2;
		let l2 = $scope.weekRange.from.clone().utc().startOf('isoWeek');

		for (let w = 0; w < l; w++) {
			$scope.weeks.push({w: l2.isoWeek(), d: l2.format('DD/MM'), o: l2.clone().utc()});
			l2.add(7, 'days');
		}


		let hoursByLocation = {};
		_.each(ListItems, function (listItem) {
			hoursByLocation[listItem.item_id] = {Hours: 0, Percentage: 0};
		});

		let totalTotal = 0;

		let calculateLocationPer = function () {
			_.each(ListItems, function (l) {
				hoursByLocation[l.item_id].Percentage = hoursByLocation[l.item_id].Hours / totalTotal * 100;
				$scope.piedata.push(Math.round(hoursByLocation[l.item_id].Percentage));
				$scope.pieSeries.push(l.list_item);
			});
		};

		let userHours = [];

		let getTableData = function () {
			let weeksForGraphs = angular.copy($scope.weeks);
			let thisWeek = moment().startOf('week');
			weeksForGraphs.push({d: thisWeek.add(1, 'days').format('DD/MM'), o: thisWeek.clone().utc()});
			let promises = [];
			angular.forEach(weeksForGraphs, function (week) {
				let startTime = moment(week.o._d).format('YYYY-MM-DD HH:mm:ss');
				let endTime = moment(week.o._d).add(6, 'days').format('YYYY-MM-DD HH:mm:ss');
				let p = (WorktimeHourStatusService.getWorkLoadDashboardData(startTime, endTime)).then(function (data) {
					userHours.push(data);
				});
				promises.push(p);

			});
			return $q.all(promises).then(function () {
				_.each(userHours, function (week) {
					_.each(week, function (location) {
						_.each(location, function (item) {
							totalTotal = totalTotal + item.Hours + item.AdditionalHours;
							if (hoursByLocation[item.CommentListItemId]) hoursByLocation[item.CommentListItemId].Hours =
								hoursByLocation[item.CommentListItemId].Hours +	item.Hours + item.AdditionalHours;

						})
						;
					});
				});
				calculateLocationPer();
			});
		};

		$scope.colours = [
			{
				backgroundColor: '#FFE97C',
				pointBackgroundColor: '#FFE97C',
				hoverBackgroundColor: '#FFE97C',
				fill: false
			},
			{
				backgroundColor: '#AC70AC',
				pointBackgroundColor: '#AC70AC',
				hoverBackgroundColor: '#AC70AC',
				fill: false
			}
		];

		$scope.override = [{
			data: [],
			stack: 1,
			fill: true
		}, {
			data: [],
			stack: 1,
			fill: true
		}];

		$scope.pieOptions = {
			responsive: false,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function (tooltipItem, data) {
						return data.labels[tooltipItem.index] + ": " + data.datasets[0].data[tooltipItem.index] + "%";
					}
				},
			},
			legend: {display: false, position: 'left'},
		};

		$scope.graph = {
			height: "400px", data: [[], []], labels: [], series: ['Hours', 'Additional Hours'], options: {
				tooltips: {
					callbacks: {
						label: function (tooltipItem, data) {
							return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
						}
					},
				},
				responsive: true,
				maintainAspectRatio: false,
				legend: {display: false},
				scales: {
					xAxes: [{
						ticks: {
							stacked: true,
							beginAtZero: true
						}
					}]
				}
			}
		};

		let f = moment().startOf('april');
		let t = moment().startOf('april').add(12, 'm');
		$scope.selections = {
			dateRange: {
				from: f,
				to: t
			},
			resolution: 1,
			filterValue: -1,
			selectValue: 0
		};
		$scope.getTitle = function (week, user_id) {
			let r = _.find($scope.data, function (o) {
				return o.Week === week && o.UserItemId === user_id;
			});

			if (r) {
				return 'Hours: ' + r.Hours + ', Additional Hours: ' + r.AdditionalHours;
			}
			return "";
		};

		$scope.openCurrentSheet = function () {
			ViewService.view('worktimeTracking');
		};

		// init and refresh
		function loadData() {
			let year = new Date().getFullYear();
			if (new Date().getMonth() < 4) year -= 1;

			WorktimeHourStatusService.getWidgetData(
				getDayMonthYearFormatFromMoment(moment([year, 1, 1]).add(2, 'months')),
				getDayMonthYearFormatFromMoment(moment([year, 1, 1]).add(14, 'months').add(-1, 'days'))
			).then(function (data) {
				let c = 0;
				let total = 0;
				_.each(data, function (item) {
					c += 1;
					var t = item.Title;
					if (t.length > 30) t = t.substring(0, 30) + "...";
					$scope.graph.labels.push(t);
					$scope.graph.data[0].push(item.Hours);
					$scope.graph.data[1].push(item.AdditionalHours);
					$scope.override[0].data.push(item.Hours);
					$scope.override[1].data.push(item.AdditionalHours);

					total += item.Hours + item.AdditionalHours;
				});
				let h = c * 30;
				if (h > 400) h = 500;
				if (h < 110) h = 110;

				$scope.graph.height = h + 30 + "px";
			});
		}

		function getDayMonthYearFormatFromMoment(momentObject) {
			return momentObject.format('DDMMYYYY');
		}

		loadData();
		getTableData();
	}

	TimeBreakdownWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];

	function TimeBreakdownWidgetConfig(WidgetsProvider, ViewsProvider) {
		ViewsProvider.addView('widget.myTimesheetBreakdown', {
			template: 'frontpage/widgets/timesheets/TimeBreakdownWidget.html',
			controller: 'TimeBreakdownWidgetController',
			resolve: {
				ListItems: function (ViewConfigurator, ProcessService) {
					let hourPageList = ViewConfigurator.getConfiguration('worktimeTracking');
					return ProcessService.getListItems(hourPageList.CommentList, true).then(function (listItems) {
						return _.sortBy(listItems, [function (o) {
							return o.item_id;
						}]);
					});
				},
			}
		});

		WidgetsProvider.addSystemWidget('widget.myTimesheetBreakdown', {
			name: 'MY_TIME_ACTIVITY_TYPES',
			onFinish: function (params) {
				params.process = 'task';
			}
		});
	}
})();