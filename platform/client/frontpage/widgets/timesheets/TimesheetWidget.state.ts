﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(TimesheetWidgetConfig)
		.controller('TimesheetWidgetController', TimesheetWidgetController);

	TimesheetWidgetController.$inject = [
		'$scope',
		'StateParameters',
		'ViewService',
		'ProcessService',
		'Configuration',
		'WorktimeHourStatusService',
		'ChartColourService',
		'UserService',
		'$rootScope',
		'CalendarService',
		'$q',
		'ViewConfigurator', 'ListItems', 'hourPageList','Common'
	];

	function TimesheetWidgetController($scope,
	                                   StateParameters,
	                                   ViewService,
	                                   ProcessService,
	                                   Configuration,
	                                   WorktimeHourStatusService,
	                                   ChartColourService,
	                                   UserService, $rootScope, CalendarService, $q,
	                                   ViewConfigurator, ListItems, hourPageList,Common
	) {
		$scope.thisWeekLocations = [];
		$scope.thisWeekPercentages = [];
		$scope.thisWeekColors = [];
		$scope.users = [];
		$scope.data = [];
		$scope.weeks = [];
		$scope.circular = {current: 0, total: 0};
		$scope.locations;
		$scope.colors = {};
		$scope.tableData = {};

		var fullColors = _.map(ChartColourService.getColours(['cyan', 'orange', 'pink', 'purple', 'red']));
		if (typeof hourPageList.CommentList != 'undefined' && hourPageList.CommentMode == 2) {
			$scope.locations = ListItems;
			_.each(ListItems, function (item, index) {
				$scope.colors[item.item_id] = {
					backgroundColor: fullColors[index],
					pointBackgroundColor: fullColors[index],
					hoverBackgroundColor: fullColors[index],
					fill: true
				};
			});
		}

		$scope.override = [{
			data: [],
			stack: 1,
			fill: true
		}, {
			data: [],
			stack: 1,
			fill: true
		}];

		var f = moment().startOf('week').add(-3, 'w');
		var t = moment().endOf('week');
		$scope.selections = {
			dateRange: {
				from: f,
				to: t
			},
			resolution: 1,
			filterValue: -1,
			selectValue: 0
		};
		$scope.getTitle = function (week, user_id) {
			var r = _.find($scope.data, function (o) {
				return o.Week === week && o.UserItemId === user_id;
			});

			if (r) {
				return 'Hours: ' + r.Hours + ', Additional Hours: ' + r.AdditionalHours;
			}
			return '';
		};

		$scope.getStatus = function (week, user_id) {
			var r = _.find($scope.data, function (o) {
				return o.Week === week && o.UserItemId === user_id;
			});
			if (r) {
				if (r.Status === 0) return 'inprogress';
				if (r.Status === 1) return 'submitted';
				if (r.Status === 2) return 'approved';
				if (r.Status === 3) return 'rejected';
			}
			return 'inprogress';
		};


		$scope.getWeekPercentage = function (week, location) {
			var r = $scope.tableData[week.d][location.item_id];
			if (!r) return " - ";
			return r + "%";
		};

		$scope.locationVisible = function (location) {
			var r = false;
			_.each($scope.tableData, function (week) {
				if (week[location.item_id]) {
					r = true;
					return;
				}
			});
			return r;
		};

		$scope.openTimesheet = function (week, user_item_id) {
			var r = _.find($scope.data, function (o) {
				return o.Week === week.w && o.UserItemId === user_item_id;
			});
			UserService.getUser(user_item_id).then(function (user) {
				ViewService.view('worktimeTracking',
					{
						params: {
							userId: user.item_id,
							userCalendarId: user.calendar_id,
							userName: user.name,
							fromDate: week.o,
							toDate: week.o.clone().add(7, 'days'),
							status: r && r.Status || 0
						}
					},
					{
						split: false
					});
			});
		};

		$scope.openCurrentSheet = function () {
			ViewService.view('worktimeTracking');
		};

		// init and refresh
		function loadData() {
			var l = $scope.selections.dateRange.to.diff($scope.selections.dateRange.from, 'week') + 1;
			var start = $scope.selections.dateRange.from.clone().utc().startOf('isoWeek');

			for (var w = 0; w < l; w++) {
				$scope.weeks.push({w: start.isoWeek(), d: start.format('DD/MM'), o: start.clone().utc()});
				start.add(7, 'days');
			}

			//UserService.getUser($rootScope.me.itemId).then(function (user) {
			WorktimeHourStatusService.getHourTotal(
				getDayMonthYearFormatFromMoment(moment(new Date()).utc().startOf('isoWeek')),
				getDayMonthYearFormatFromMoment(moment(new Date()).utc().endOf('isoWeek')),
				1)
				.then(function (data) {
					$scope.circular.current = data.hours; // + data.additional_hours;
					$scope.circular.total = data.total;
					$scope.circular.title = "out of " + data.total;
				});
			//  });

			WorktimeHourStatusService.getStatus(
				getDayMonthYearFormatFromMoment($scope.selections.dateRange.from),
				getDayMonthYearFormatFromMoment($scope.selections.dateRange.to),
				1)
				.then(function (data) {
					setData(data);
				});
		}


		function setData(data) {
			$scope.users = _.uniqBy(data.items, 'UserItemId');
			$scope.data = data.items;

		}

		function getDayMonthYearFormatFromMoment(momentObject) {
			return momentObject.format('DDMMYYYY');
		}

		loadData();

		var locationHours = {}; //contains hours for each location of each week
		var weeklyTotalHours = {};

		var setTableData = function () {
			_.each(userHours, function (weekData) {
				_.each(weekData, function (dataRow, week) {
					locationHours[week] = {};
					_.each(dataRow, function (hourItem) {
						locationHours[week][hourItem.CommentListItemId] = +(hourItem.Hours);
					});
				});
			});
			calculateTotalHours();
		};


		var userHours = [];
		var getTableData = function () {
			var weeksForGraphs = angular.copy($scope.weeks);
			var thisWeek = moment().startOf('week');
			weeksForGraphs.push({d: thisWeek.add(1, 'days').format('DD/MM'), o: thisWeek.clone().utc()});
			var promises = [];
			angular.forEach(weeksForGraphs, function (week) {
				var startTime = moment(week.o._d).format('YYYY-MM-DD HH:mm:ss');
				var endTime = moment(week.o._d).add(6, 'days').format('YYYY-MM-DD HH:mm:ss');
				var p = (WorktimeHourStatusService.getWorkLoadDashboardData(startTime, endTime)).then(function (data) {
					userHours.push(data);
				});
				promises.push(p);

			});
			return $q.all(promises).then(function () {
				setTableData();
			});
		};
		getTableData();

		function calculateTotalHours() {
			_.each(locationHours, function (weekHours, week_) {
				var runner = 0;
				weeklyTotalHours[week_] = runner;
				_.each(weekHours, function (value) {
					runner = runner + value;
				});
				weeklyTotalHours[week_] = runner;
			});
			calcHourPercentages();

			_.each($scope.locations, function (location) {
				if ($scope.locationVisible(location)) {
					var tw = moment().startOf('isoWeek').format('DD/MM');
					var p = $scope.tableData[tw][location.item_id];
					if (p) {
						$scope.thisWeekLocations.push(location.list_item);
						$scope.thisWeekPercentages.push(p);
						$scope.thisWeekColors.push($scope.colors[location.item_id].backgroundColor);
					}
				}
			});

		}

		function calcHourPercentages() {
			var totalAmount = 0;
			_.each(locationHours, function (location, key) {
				$scope.tableData[moment(key).format('DD/MM')] = {};
				_.each(location, function (amount, listItemId) {
					totalAmount = Common.formatNumber(amount / weeklyTotalHours[key] * 100); //matikkanero ratkaiskoon pyöristyksen
					$scope.tableData[moment(key).format('DD/MM')][listItemId] = totalAmount;
				})
			})
		}
	}

	TimesheetWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];

	function TimesheetWidgetConfig(WidgetsProvider, ViewsProvider) {
		ViewsProvider.addView('widget.myTimesheet', {
			template: 'frontpage/widgets/timesheets/TimesheetWidget.html',
			controller: 'TimesheetWidgetController',
			resolve: {
				ListItems: function (ViewConfigurator, ProcessService) {
					var hourPageList = ViewConfigurator.getConfiguration('worktimeTracking');
					return ProcessService.getListItems(hourPageList.CommentList).then(function (listItems) {
						return _.sortBy(listItems, [function(o) { return o.item_id; }]);
					});
				},
				hourPageList: function (ViewConfigurator) {
					return ViewConfigurator.getConfiguration('worktimeTracking');
				}
			}
		});

		WidgetsProvider.addSystemWidget('widget.myTimesheet', {
			name: 'MY_TIMESHEETS',
			onFinish: function (params) {
				params.process = 'task';
			}
		});
	}
})();