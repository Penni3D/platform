﻿(function () {
    'use strict';

    angular
        .module('core.widgets')
        .config(TimesheetApprovalWidgetConfig);

    TimesheetApprovalWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];

    function TimesheetApprovalWidgetConfig(WidgetsProvider, ViewsProvider) {
        ViewsProvider.addView('widget.myTimesheetApproval', {
            template: 'pages/workTimeTracking/worktime-hour-status.html',
            controller: 'WorktimeHourStatusController',
            resolve: {
                WorkTimeRights: function (ViewConfigurator) {
                    return ViewConfigurator.getRights('worktimeHourStatus').then(function(states) {
                        return {
                            download: _.includes(states, 'download'),
                            email: _.includes(states, 'email'),
                            approveAll: _.includes(states, 'approveAll')
                        };
                    });
                },
                PortfolioColumns: function() {
                	return [];
				},
                ViewMode: function () {
                    return "widget";
                },
	            WidgetShowAll: function(StateParameters) {
                	return (StateParameters && StateParameters.showWholeTeam && StateParameters.showWholeTeam === 1);
	            }
            }
        });

        WidgetsProvider.addSystemWidget('widget.myTimesheetApproval', {
            name: 'MY_TIMESHEETS_APPROVAL',
	        options: {
		        showWholeTeam: function () {
			        return {
				        type: 'select',
				        options: [{ name: "Yes", value: 1 },{ name: "No", value: 0 }],
				        optionName: 'name',
				        optionValue: 'value',
				        label: 'Show Whole Team'
			        };
		        }
	        },
            onFinish: function (params) {
                params.process = 'task';
            }
        });
    }
})();