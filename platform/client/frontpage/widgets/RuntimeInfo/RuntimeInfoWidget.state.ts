﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.service('RuntimeWidgetService', RuntimeWidgetService)
		.config(RuntimeInfoWidgetConfig)
		.controller('RuntimeInfoWidgetController', RuntimeInfoWidgetController);

	RuntimeInfoWidgetController.$inject = [
		'$q', '$scope', '$rootScope',
		'RuntimeWidgetService',
		'ConnectorService',
		'Common',
		'ApiService',
		'MessageService',
		'ViewService',
		'StateParameters',
		'UserFeatureService',
		'GetUserGroupsByItemId',
		'ChartColourService',
		'Translator',
		'ProcessesService',
		'SettingsService',
		'Views'
	];

	function RuntimeInfoWidgetController($q, $scope, $rootScope,
	                                     RuntimeWidgetService,
	                                     ConnectorService, Common, ApiService, MessageService, ViewService, StateParameters, UserFeatureService, GetUserGroupsByItemId, ChartColourService, Translator, ProcessesService, SettingsService, Views) {
		$scope.defaultColors = ChartColourService.getColours();

		$scope.uptimeTableMode = 0;
		$scope.loading = false;

		$scope.UserLicensingHasContendLoader = false;
		$scope.setUserLicencingLoader = function () {
			$scope.UserLicensingHasContendLoader = true;
		};
				
		//Check which tabs can be shown
		let loginUserGroups = Object.keys(GetUserGroupsByItemId);

		$scope.rights = {};
		$scope.rights.availability = false;
		$scope.rights.userLicensing = false;
		$scope.rights.performance = false;
		$scope.rights.eventLog = false;
		$scope.rights.admin = false;
		$scope.drillDown = 0;

		$scope.showUpdate = false;

		$scope.activeTab = 0;
		
		if ('refreshAction' in $rootScope.me) {
			let tabNum = $rootScope.me.refreshAction || 0;
			$scope.activeTab = tabNum;
			delete $rootScope.me.refreshAction;
		}

		$scope.changeActiveTab = function (tabno) {
			$scope.activeTab = tabno;
		};

		$scope.migrationRunning = false;

		$scope.fixConditions = function () {
			$scope.migrationRunning = true;
			let proPromises = [];
			let getPromises = [];
			let setPromises = [];
			let states = Views.getFeatures();
			let States = {};
			angular.forEach(states, function (value) {
				let stateResult = [];
				for (let s of value.states) {
					stateResult.push(value.name + "." + s);
				}
				States[value.name] = stateResult;
			});

			let types = [0, 1, 2, 3, 4, 5];
			for (let processType of types) {
				proPromises.push(ProcessesService.get(processType, true).then(function (result) {
					for (let process of result) {
						getPromises.push(SettingsService.Conditions(process.ProcessName).then(function (conditions) {
							let saveThese = [];
							angular.forEach(conditions, function (condition) {
								let fixedStates = [];
								let hasChanged = false;
								for (let feature of condition.Features) {
									let featureStates = States[feature];
									for (let state of condition.States) {
										angular.forEach(States[feature], function (fs) {
											if (fs.indexOf(state) > -1 || (state == 'meta.read' && fs.indexOf('.read') > -1)) {
												if (fixedStates.indexOf(fs) == -1) fixedStates.push(fs);
												hasChanged = true;
											}
										});
									}
								}
								if (hasChanged) {
									condition.States = fixedStates;
									saveThese.push(condition);
								}
							});

							setPromises.push({name: process.ProcessName, saveThese: saveThese});
						}));
					}
				}));
			}

			$q.all(proPromises).then(function () {
				$q.all(getPromises).then(function () {
					let f = function (i) {
						if (i > setPromises.length){
							$scope.migrationRunning = false;
							MessageService.msgBox('Condition State Migration Ready');
							return;
						}
						let p = setPromises[i];
						if(typeof  p != 'undefined'){
							SettingsService.ConditionsSave(p.name, p.saveThese).finally(function () {
								f(i + 1);
							});
						} else {
							$scope.migrationRunning = false;
							MessageService.msgBox('Condition State Migration Ready');
							return;
						}
					}
					f(0);
				});
			});
		}

		$scope.$watch("activeTab", function(newvalue, oldvalue) {
			if (oldvalue != newvalue) {
				$scope.updateData(newvalue);
			}
		});

		if (StateParameters.availabilityItemId && StateParameters.availabilityItemId.length > 0) {
			angular.forEach(StateParameters.availabilityItemId, function (allowedGroup) {
				if (loginUserGroups.indexOf(allowedGroup.toString()) > -1) {
					$scope.rights.availability = true;
					$scope.showUpdate = true;
				}
			});
		}
		if (StateParameters.userLicensingItemId && StateParameters.userLicensingItemId.length > 0) {
			angular.forEach(StateParameters.userLicensingItemId, function (allowedGroup) {
				if (loginUserGroups.indexOf(allowedGroup.toString()) > -1) {
					$scope.rights.userLicensing = true;
					$scope.showUpdate = true;
				}
			});
		}
		if (StateParameters.performanceItemId && StateParameters.performanceItemId.length > 0) {
			angular.forEach(StateParameters.performanceItemId, function (allowedGroup) {
				if (loginUserGroups.indexOf(allowedGroup.toString()) > -1) {
					$scope.rights.performance = true;
					$scope.showUpdate = true;
				}
			});
		}
		if (StateParameters.eventLogItemId && StateParameters.eventLogItemId.length > 0) {
			angular.forEach(StateParameters.eventLogItemId, function (allowedGroup) {
				if (loginUserGroups.indexOf(allowedGroup.toString()) > -1) {
					$scope.rights.eventLog = true;
					$scope.showUpdate = true;
				}
			});
		}
		if (StateParameters.adminItemId && StateParameters.adminItemId.length > 0) {
			angular.forEach(StateParameters.adminItemId, function (allowedGroup) {
				if (loginUserGroups.indexOf(allowedGroup.toString()) > -1) {
					$scope.rights.admin = true;
					$scope.showUpdate = true;
				}
			});
		}
		if (StateParameters.drilldownPortfolioId) {
			let portfolioId = StateParameters.drilldownPortfolioId || 0;
			if (portfolioId > 0) {
				$scope.drillDownPortfolioId = portfolioId;
			}
		}

		$scope.hideArr = [0, 0, 0];

		$scope.occurenceLabels = [];
		$scope.occurencSeries = ['Avarage duration ms', 'Avarage network ms'];
		$scope.occurenceOptions = {
			maintainAspectratio: true,
			responsive: true,
			legend: {
				display: true,
				onClick: function (e, legendItem) {
					var ci = this.chart;
					var index = legendItem.datasetIndex;

					if ($scope.hideArr[index] == 0) {
						$scope.hideArr[index] = 1;
					} else {
						$scope.hideArr[index] = 0;
					}

					if (index < 2) {
						var meta = ci.getDatasetMeta(index);
						if (meta.hidden === null || meta.hidden == false) {
							meta.hidden = true;
						} else {
							meta.hidden = false;
						}
					} else {
						if ($scope.showMonitor) {
							for (var i = 2; i < 26; i++) {
								var meta = ci.getDatasetMeta(i);
								if (meta.hidden === null || meta.hidden == false) {
									meta.hidden = true;
								} else {
									meta.hidden = false;
								}
							}
						}
					}
					ci.update();
					if (ci.legend.legendItems[index].hidden) {
						ci.legend.legendItems[index].hidden = false;
					} else {
						ci.legend.legendItems[index].hidden = true;
					}

					var count = 2;
					if ($scope.showMonitor) count = 3;

					for (var i = 0; i < count; i++) {
						if ($scope.hideArr[i] == 1) {
							ci.legend.legendItems[i].hidden = true;
						} else {
							ci.legend.legendItems[i].hidden = false;
						}
					}

				},
				labels: {
					generateLabels: function (chart) {
						let legends = [];

						legends.push({
							text: Translator.translate('ADMIN_CONSOLE_AVERAGE_DURATION'),
							datasetIndex: 0,
							fillStyle: "#0288d1",
							hidden: false
						});
						legends.push({
							text: Translator.translate('ADMIN_CONSOLE_AVERAGE_NETWORK'),
							datasetIndex: 1,
							fillStyle: "#e0e0e0",
							hidden: false
						});

						if ($scope.showMonitor) {
							legends.push({
								text: Translator.translate('ADMIN_CONSOLE_UPTIME'),
								datasetIndex: 2,
								fillStyle: "#388e3c",
								hidden: false
							});
						}
						return legends;
					}
				}
			},
			tooltips: {
				mode: "nearest",
				callbacks: {
					label: function (data) {
						if ($scope.barLabelValues["value_" + data.index + "_" + (data.datasetIndex - 2)] != null) {
							var value = $scope.barLabelValues["value_" + data.index + "_" + (data.datasetIndex - 2)];
							if (value == -100) {
								return "No data available";
							} else {
								return "Uptime: " + value.toFixed(2) + "%";
							}
						} else {
							if (data.datasetIndex == 0) {
								return "Avarage duration: " + data.value;
							} else {
								return "Avarage network: " + data.value;
							}

						}
					},
					title: function (data) {
						if ($scope.barLabels["value_" + data[0].index + "_" + (data[0].datasetIndex - 2)] != null) {
							var hourLabel = $scope.barLabels["value_" + data[0].index + "_" + (data[0].datasetIndex - 2)];
							return data[0].label + " (" + hourLabel + ")";
						} else {
							return data[0].label;
						}
					}
				}
			},
			scales: {
				yAxes: [
					{
						id: 'y-axis-1',
						display: true,
						position: 'left',
						gridLines: {display: false, drawBorder: true},
						ticks: {
							fontFamily: 'Roboto', fontSize: 12,
							//min: 0, max: 100, stepSize: 100
						},
						scaleLabel: {
							display: true,
							labelString: Translator.translate('ADMIN_CONSOLE_DELAY')
						}
					},
					{
						id: 'y-axis-2',
						display: true,
						position: 'right',
						gridLines: {display: false, drawBorder: true},
						ticks: {
							fontFamily: 'Roboto', fontSize: 12,
							min: 0, max: 24, stepSize: 2
						},
						scaleLabel: {
							display: true,
							labelString: Translator.translate('ADMIN_CONSOLE_UPTIME_HOURS')
						}
					}
				],
				xAxes: [
					{
						id: 'x-axis-1',
						display: true,
						position: 'left',
						gridLines: {display: false, drawBorder: true},
						ticks: {
							fontFamily: 'Roboto', fontSize: 12,
							//min: 0, max: 100, stepSize: 100
						},
						barPercentage: 0.4
					}
				]
			}
		};

		var color3 = "#e0e0e0";
		var color2 = "#0288d1";
		$scope.occurenceColors = [{
			backgroundColor: convertHex(color2, 30),
			pointBackgroundColor: color2,
			hoverBackgroundColor: color2,
			borderColor: color2,
			fill: true
		}, {
			backgroundColor: convertHex(color3, 30),
			pointBackgroundColor: color3,
			hoverBackgroundColor: color3,
			borderColor: color3,
			fill: true
		}];

		//DATABASE DATA
		$scope.dbLabels = [];
		$scope.dbData = [];
		$scope.dbRows = [];

		$scope.dbGrandTotalMB = 0;
		$scope.dbOnlineTotalMB = 0;
		$scope.dbHistoryTotalMB = 0;
		$scope.dbGrandTotalRows = 0;
		$scope.dbOnlineTotalRows = 0;
		$scope.dbHistoryTotalRows = 0;
		$scope.dbColoring = ["#ffe97c", "#e5e5e5", "#c7c7c7", "#c461e0"];
		$scope.formatNumber = function (v) {
			return Common.formatNumber(v, 0);
		};
		$scope.restart = function () {
			MessageService.confirm("Are you sure you want to recycle this application?", function () {
				$scope.loading = true;
				RuntimeWidgetService.restart().then(function () {
					setTimeout(x => {
						document.location.reload();	
					}, 2000);
				});	
			});
		};

		$scope.getOnlineStatus = function getOnlineStatus() {
			var api = ApiService.v1api('diagnostics/getOnlineStatus/');
			api.get().then(function (result) {
				$scope.loginsOnlineSessions = result.sessions;
				$scope.onlineIssues = result.issues.SuspiciousColumns.length + result.issues.Portfolios.length + Object.keys(result.issues.Adhoc).length;
				$scope.onlineErrors = result.errors;
				$scope.issues = result.issues;
				let p = ViewService.getView(StateParameters.ParentViewId);
				if (p && typeof p.scope.setIcon === 'function') {
					p.scope.setIcon(StateParameters.ViewId, $scope.onlineIssues);
				}
			});
		}
		
		$scope.fn = function(n) {
			return Common.formatNumber(n);
		}
		
		$scope.getAdhocValue = function(key, value) {
			return Translator.translate(key).replace("[X]", value);
		}

		function loadData(WidgetData) {
			var data = WidgetData;
			$rootScope.me.admindash = WidgetData;

			$scope.showDataBaseChart = true;
			if (data.InstanceChartData != null && data.InstanceChartData != "") {
				createInstanceUserChart($scope, data.InstanceChartData, data.InstanceChartTime, Translator);
			}

			//Logs
			$scope.logs = data.Logs;

			$scope.expandStack = function (i) {

				let content = {
					title: 'Stack Trace',
					message: data.Logs[i].stack,
					buttons: [{type: 'primary', text: 'Close'}],
				};
				MessageService.dialog(content);

			};

			$scope.formatLogDate = function (dt) {
				return moment(dt).format('YYYY-MM-DD HH:mm:ss');
			}


			//UserGroupActivity
			if (data.MonthlyLicenseUsers != null) {
				$scope.licenceChartLabels = []; // x-axis, months
				$scope.licenceChartSeries = []; // licenses (names)
				$scope.licenceChartData = [];   // actual data, multi dimension array of [series][x-axis]
				let licenceChartLabelIds = [];  // private license id to track indexes
				let licenceChartDataUsers = []; // user ids of each point, array [series][x-axis]

				angular.forEach(Object.keys(data.MonthlyLicenseUsers), function (year_month) {
					angular.forEach(data.MonthlyLicenseUsers[year_month], function (license_user_data) {
						if (licenceChartLabelIds.indexOf(license_user_data.license_id) === -1) { // determine which licenses should be shown and set possible order
							licenceChartLabelIds.push(license_user_data.license_id);

							$scope.licenceChartSeries.push(license_user_data.license_name);
						}
					});
				});
				if (licenceChartLabelIds.length > 0) {
					angular.forEach(Object.keys(data.MonthlyLicenseUsers), function (year_month) {
						var splitArr = year_month.split("-");
						$scope.licenceChartLabels.push(splitArr[1] + " / " + splitArr[0]);
					});

					for (let dataIx = 0; dataIx < licenceChartLabelIds.length; dataIx++) {
						$scope.licenceChartData.push([]);
						licenceChartDataUsers.push([]);
						angular.forEach(Object.keys(data.MonthlyLicenseUsers), function (year_month, ymIx) {
							$scope.licenceChartData[dataIx][ymIx] = 0;
							licenceChartDataUsers[dataIx][ymIx] = "";
							angular.forEach(data.MonthlyLicenseUsers[year_month], function (license_user_data) {
								if (license_user_data.license_id === licenceChartLabelIds[dataIx]) {
									$scope.licenceChartData[dataIx][ymIx] = license_user_data.user_count;
									licenceChartDataUsers[dataIx][ymIx] = license_user_data.user_ids;
								}
							});
						});
					}

					$scope.licenceChartColors = [];
					angular.forEach($scope.defaultColors, function (color, colorIx) {
						if (colorIx % 2 === 0) {
							$scope.licenceChartColors.push({
								backgroundColor: convertHex(color, 30),
								pointBackgroundColor: color,
								hoverBackgroundColor: color,
								borderColor: color,
								fill: true
							});
						}
					});

					$scope.licenceChartOptions = {
						legend: {
							display: true
						},
						scales: {
							yAxes: [
								{
									id: 'y-axis-1',
									type: 'linear',
									display: true,
									position: 'left'
								}
							]
						},
						onClick: function (event) {
							if ($scope.drillDown > 0) {
								const element = this.getElementAtEvent(event);
								if (element.length > 0) {
									const clickedUids = licenceChartDataUsers[element[0]._datasetIndex][element[0]._index];
									if (clickedUids.length > 0) {
										ViewService.view('portfolio',
											{
												params: {
													portfolioId: $scope.drillDown,
													restrictions: {0: clickedUids.split(',')},
													process: "user",
													chartForceFlatMode: true
												}
											},
											{split: true, size: 'normal'} // apparently can't split front page
										);
									}
								}
							}
						}
					}
				}
				$scope.UserLicensingHasContendLoader = false;
			}


			// AD data
			$scope.azureRunOngoin = 0;

			$scope.adDomains = data.Domains;
			$scope.showDomain = {};
			angular.forEach(data.Domains, function (domain) {
				$scope.showDomain[data.Domains.indexOf(domain)] = 0;
			});
			$scope.AzureData = [];

			angular.forEach(data.Domains, function (domain) {
				azureDataHandling(domain, data.AzureAdLogs, $scope, -1);
			});

			$scope.runAdForDomain = function (domain, index) {
				MessageService.toast("Azure AD sync started for domain " + domain);
				var api = ApiService.v1api('core/azureadsync');
				$scope.azureRunOngoin += 1;
				$scope.showDomain[index] = 1;
				ConnectorService.listen('AzureAdSyncCompleted', function (obj) {
					if (obj.error != null) {
						MessageService.toast("Azure AD sync prevented for domain " + obj.domain);
						$scope.AzureData[obj.index].additionalInformation = obj.error;
					} else {
						$rootScope.me.admindash.AzureAdLogs = obj.data;
						azureDataHandling(obj.domain, obj.data, $scope, obj.index);
						MessageService.toast("Azure AD sync completed for domain " + obj.domain);
					}
					$scope.showDomain[index] = 0;
				});
				api.get([domain, -1, index]);
			}

			//AD block ends

			//Occurence Data
			var dateArray = [];
			var currentDate = moment(Date.now()).add(-7, 'days');
			while (Date.parse(currentDate) <= Date.parse(moment(Date.now()).add(0, 'days'))) {
				dateArray.push(moment(currentDate).startOf('day'));
				$scope.occurenceLabels.push(moment(currentDate).format('DD.MM.YYYY'));
				currentDate = moment(currentDate).add(1, 'days');
			}

			var occurenceArr = [];
			_.each(data.Occurence, function (item) {
				occurenceArr.push(item);
			});

			$scope.occurenceData = [[], []];

			angular.forEach(dateArray, function (day) {
				var item = occurenceArr.filter(function (x) {
					return Date.parse(x.occurenceDate) == Date.parse(day);
				});
				if (item != null && item[0] != null) {
					$scope.occurenceData[0].push(item[0].avarageDuration);
					$scope.occurenceData[1].push(item[0].avarageNetwork);
				} else {
					$scope.occurenceData[0].push(0);
					$scope.occurenceData[1].push(0);
				}
			});

			$scope.occurenceOverride = [];
			var lineElementObj = {};
			lineElementObj["type"] = "line";
			$scope.occurenceOverride.push(lineElementObj);
			$scope.occurenceOverride.push(lineElementObj);

			$scope.monitorActive = false;
			$scope.showMonitor = false;

			$scope.chartType = "line";
			$scope.barModeScales = $scope.occurenceOptions["scales"].yAxes;

			$scope.barLabels = {};
			$scope.barLabelValues = {};

			$scope.formatMonitorTableNumbers = function (val) {
				if (parseFloat(val) < 100 && parseFloat(val) >= 99.995) {
					return parseFloat("99.99").toFixed(2);
				} else {
					return parseFloat(val).toFixed(2);
				}
			}

			if (data.MonitorStartTime != null) {
				$scope.monitorActive = true;
				$scope.MonitorStartTime = data.MonitorStartTime;
				calcMonitorData(data.UptimeData, $scope);
			}

			//Database Data
			_.each(data.DBDetails, function (item) {
				$scope.dbGrandTotalMB += parseInt(item.TotalMB);
				$scope.dbGrandTotalRows += parseInt(item.Rows);
				if (item.Name.indexOf('History') > -1) {
					$scope.dbHistoryTotalMB += parseInt(item.TotalMB);
					$scope.dbHistoryTotalRows += parseInt(item.Rows);
				} else {
					$scope.dbOnlineTotalMB += parseInt(item.TotalMB);
					$scope.dbOnlineTotalRows += parseInt(item.Rows);
				}

				$scope.dbData.push(item.TotalMB);
				$scope.dbLabels.push(item.Name);
			});
			$scope.dbRows = data.DBRows;
		}

		if ('admindash' in $rootScope.me) {
			// if ($rootScope.me.admindash && $rootScope.me.admindash.InstanceChartData) {
			// 	$scope.availabilityHasContend = true;
			// }
			// if ($rootScope.me.admindash && $rootScope.me.admindash.MonthlyLicenseUsers) {
			// 	$scope.UserLicensingHasContend = true;
			// }
			// if ($rootScope.me.admindash && $rootScope.me.admindash.DBDetails) {
			// 	$scope.performanceHasContend = true;
			// }
			// if ($rootScope.me.admindash && $rootScope.me.admindash.Logs) {
			// 	$scope.eventLogHasContend = true;
			// }

			loadData($rootScope.me.admindash);
		}
		//else {
		//    $scope.getOnlineSessionCount();
		//    RuntimeWidgetService.getModel().then(function (data) {
		//        loadData(data);
		//        if ($scope.loginsOnlineSessions2 != null) {
		//            $scope.loginsOnlineSessions = $scope.loginsOnlineSessions2;
		//        }            
		//    });
		//}

		
		$scope.createMonitor = function () {
			var tracker = ApiService.v1api('diagnostics/createMonitor');
			tracker.get().then(function (result) {
				if (result == 1) {
					MessageService.toast("Uptime monitor created successfully");
					delete $rootScope.me.admindash;
					$rootScope.me.refreshAction = $scope.activeTab;
					ViewService.refresh(StateParameters.ViewId);
				} else {
					MessageService.toast("Uptime monitor's creation failed");
				}
			});
		}

		$scope.getMonitorData = function () {
			var api = ApiService.v1api('diagnostics/getMonitorData');
			api.get().then(function (data) {
				calcMonitorData(data, $scope);
			});
		}

		$scope.updateData = function (num) {
			//$scope.getOnlineSessionCount();
			$scope.occurenceData = [[], []];
			$scope.occurenceLabels = [];
			$scope.licenceChartData = [];
			$scope.licenceChartLabels = [];

			RuntimeWidgetService.getModel(num).then(function (data) {
				if ($rootScope.me.admindash) {
					angular.forEach(Object.keys(data), function (key) {
						$rootScope.me.admindash[key] = data[key];
					});

					$scope.dbGrandTotalMB = 0;
					$scope.dbGrandTotalRows = 0;
					$scope.dbHistoryTotalMB = 0;
					$scope.dbHistoryTotalRows = 0;
					$scope.dbOnlineTotalMB = 0;
					$scope.dbOnlineTotalRows = 0;
					$scope.dbData = [];
					$scope.dbLabels = [];
					$scope.showDataBaseChart = false;
					loadData($rootScope.me.admindash);
				} else {
					loadData(data);
				}
				$scope.getOnlineStatus();
				// if (Object.keys(data).indexOf("OnlineSessions") > -1) {
				// 	$scope.loginsOnlineSessions = data["OnlineSessions"];
				// }
			});
		}
	}

	RuntimeInfoWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];

	function RuntimeInfoWidgetConfig(WidgetsProvider, ViewsProvider) {
		ViewsProvider.addView('widget.runTimeInfoWidget', {
			template: 'frontpage/widgets/RuntimeInfo/RuntimeInfoWidget.html',
			controller: 'RuntimeInfoWidgetController',
			resolve: {
				GetUserGroupsByItemId: function (UserFeatureService, $rootScope) {
					return UserFeatureService.getUserGroups($rootScope.clientInformation.item_id);
				}
			}
		});

		WidgetsProvider.addSystemWidget('widget.runTimeInfoWidget', {
			name: 'RUNTIMEINFO_WIDGET',
			Color: 'rgb(255, 152, 0)',
			onSetup: {
				TabUserGroups: function (ProcessService) {
					return ProcessService.getItems('user_group', undefined, true).then(function (groups) {
						var g = [];

						var i = groups.length;
						while (i--) {
							g.push({
								value: groups[i].item_id,
								name: groups[i].usergroup_name
							});
						}
						return g;
					});
				},
				UserProcessPortfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('user', 0).then(function (portfolios) {
						var g = [];
						var i = portfolios.length;
						while (i--) {
							g.push({
								value: portfolios[i].ProcessPortfolioId,
								LanguageTranslation: portfolios[i].LanguageTranslation
							});
						}
						return g;
					});
				}
			},
			options: {
				availabilityItemId: function (resolves) {
					return {
						type: 'multiselect',
						options: resolves.TabUserGroups,
						label: 'ADMIN_DASH_AVAILABILITY',
						optionValue: 'value',
						optionName: 'name'
					}
				},
				userLicensingItemId: function (resolves) {
					return {
						type: 'multiselect',
						options: resolves.TabUserGroups,
						label: 'ADMIN_DASH_USER_LICENSING',
						optionValue: 'value',
						optionName: 'name'
					}
				},
				performanceItemId: function (resolves) {
					return {
						type: 'multiselect',
						options: resolves.TabUserGroups,
						label: 'ADMIN_DASH_PERFORMANCE',
						optionValue: 'value',
						optionName: 'name'
					}
				},
				eventLogItemId: function (resolves) {
					return {
						type: 'multiselect',
						options: resolves.TabUserGroups,
						label: 'ADMIN_DASH_EVENT_LOG',
						optionValue: 'value',
						optionName: 'name'
					}
				},
				adminItemId: function (resolves) {
					return {
						type: 'multiselect',
						options: resolves.TabUserGroups,
						label: 'ADMIN_DASH_ADMIN',
						optionValue: 'value',
						optionName: 'name'
					}
				},
				drilldownPortfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.UserProcessPortfolios,
						label: 'ADMIN_DASH_DRILLDOWN_PORTFOLIO',
						optionValue: 'value',
						optionName: 'LanguageTranslation'
					}
				}
			},
			onFinish: function (params) {
				params.process = 'task';
			}
		});
	}

	RuntimeWidgetService.$inject = ['ApiService'];

	function RuntimeWidgetService(ApiService) {
		var api = ApiService.v1api('diagnostics/runtimeinfo');
		var getModel = function (num) {
			return api.get([num]);
		};
		var restart = function () {
			var r = ApiService.v1api('diagnostics/restart');
			return r.get();
		};

		return {
			getModel: getModel,
			restart: restart
		}
	}

	function azureDataHandling(domain, data, $scope, updateIndex) {
		//messageArr[data.Domains.indexOf(domain)] = {};
		var tempObj = {}; //messageArr[data.Domains.indexOf(domain)];

		var messageRow = data.filter(function (x) {
			return x.type == 1 && x.domain == domain;
		});
		if (messageRow.length) {
			var crashErrorRow = data.filter(function (x) {
				return x.type == 0 && x.domain == domain;
			});
			var crashErrorValid = false;

			if (crashErrorRow.length > 0) {
				var messageOccurence = Date.parse(messageRow[0].occurence);
				var crashOccurence = Date.parse(crashErrorRow[0].occurence);


				if (messageOccurence >= crashOccurence) {
					if (crashOccurence + 2000 > messageOccurence) {
						crashErrorValid = true;
					}
				} else {
					if (messageOccurence + 2000 > crashOccurence) {
						crashErrorValid = true;
					}
				}

				//t.setSeconds(t.getSeconds() + 10);
			}

			var tempArr = messageRow[0].message.split("||");

			var result = tempArr.filter(function (x) {
				return x.indexOf("Azure AD sync completed") > -1;
			});

			tempObj.message = tempArr;
			if (messageRow == null) {
				tempObj.result = "";
			} else if (result.length) {
				tempObj.result = "Successful";
			} else {
				tempObj.result = "Failure";
			}
			var timeLine = tempArr.filter(function (x) {
				return x.indexOf("Start time") > -1;
			});

			if (timeLine != null && timeLine.length > 0) {
				var times = timeLine[0].split(" - ");
				tempObj.start = times[0].replace("Start time:", "");
				tempObj.end = times[1].replace("End time:", "");
				;
			} else {
				tempObj.start = "";
				tempObj.end = "";
			}

			var usersAdded = tempArr.filter(function (x) {
				return x.indexOf("Users added") > -1 && x.indexOf("Users added to AD groups") == -1;
			});

			if (usersAdded.length > 0) {
				tempObj.usersAdded = usersAdded[0].replace("Users added:", "").replace(" ", "");
			} else {
				tempObj.usersAdded = 0;
			}

			var usersDisabled = tempArr.filter(function (x) {
				return x.indexOf("Users disabled") > -1;
			});
			if (usersDisabled.length > 0) {
				tempObj.usersDisabled = usersDisabled[0].replace("Users disabled:", "").replace(" ", "");
			} else {
				tempObj.usersDisabled = 0;
			}

			var userInfoChanged = tempArr.filter(function (x) {
				return x.indexOf("User info changed") > -1;
			});
			if (userInfoChanged.length > 0) {
				tempObj.userInfoChanged = userInfoChanged[0].replace("User info changed:", "").replace(" ", "");
				;
			} else {
				tempObj.userInfoChanged = 0;
			}

			var usersReactivated = tempArr.filter(function (x) {
				return x.indexOf("Users reactivated") > -1;
			});
			if (usersReactivated.length > 0) {
				tempObj.usersReactivated = usersReactivated[0].replace("Users reactivated:", "").replace(" ", "");
			} else {
				tempObj.usersReactivated = 0;
			}

			var adGroupsAdded = tempArr.filter(function (x) {
				return x.indexOf("Ad groups added") > -1;
			});
			if (adGroupsAdded.length > 0) {
				tempObj.adGroupsAdded = adGroupsAdded[0].replace("Ad groups added:", "").replace(" ", "");
			} else {
				tempObj.adGroupsAdded = 0;
			}

			var adGroupInfoChanged = tempArr.filter(function (x) {
				return x.indexOf("Ad groups info changed") > -1;
			});
			if (adGroupInfoChanged.length > 0) {
				tempObj.adGroupInfoChanged = adGroupInfoChanged[0].replace("Ad groups info changed:", "").replace(" ", "");
			} else {
				tempObj.adGroupInfoChanged = 0;
			}

			var usersAddedToAdGroup = tempArr.filter(function (x) {
				return x.indexOf("Users added to AD groups") > -1;
			});
			if (usersAddedToAdGroup.length > 0) {
				tempObj.usersAddedToAdGroup = usersAddedToAdGroup[0].replace("Users added to AD groups:", "").replace(" ", "");
			} else {
				tempObj.usersAddedToAdGroup = 0;
			}

			var usersRemovedFromAdGroup = tempArr.filter(function (x) {
				return x.indexOf("Users removed from AD groups") > -1;
			});
			if (usersRemovedFromAdGroup.length > 0) {
				tempObj.usersRemovedFromAdGroup = usersRemovedFromAdGroup[0].replace("Users removed from AD groups:", "").replace(" ", "");
			} else {
				tempObj.usersRemovedFromAdGroup = 0;
			}


			//Additionals 

			var additionalText = "";
			var additionalIndex = 0;

			// Crash error comes first
			if (crashErrorRow.length > 0 && crashErrorValid) {
				additionalText += "AzureAdSync crash message:\n"
				additionalText += crashErrorRow[0].message.replace("domain:" + domain + " - " + "AzureAdSync crash message: ", "");
				additionalIndex += 1;
			}

			// Then other selected errors
			var accessTokenFails = tempArr.filter(function (x) {
				return x.indexOf("Failed to receice access token for") > -1;
			});
			if (accessTokenFails.length > 0) {
				if (additionalIndex > 0) {
					additionalText += "\n\n";
				}
				additionalText += accessTokenFails[0];
				additionalIndex += 1;
			}

			var userReadFails = tempArr.filter(function (x) {
				return x.indexOf("Reading users from Azure AD failed:") > -1;
			});
			if (userReadFails.length > 0) {
				if (additionalIndex > 0) {
					additionalText += "\n\n";
				}
				additionalText += "Reading users from Azure AD failed:\n" + userReadFails[0].replace("Reading users from Azure AD failed: ", "");
				additionalIndex += 1;
			}

			var groupReadFails = tempArr.filter(function (x) {
				return x.indexOf("Reading groups from Azure AD failed:") > -1;
			});
			if (groupReadFails.length > 0) {
				if (additionalIndex > 0) {
					additionalText += "\n\n";
				}
				additionalText += "Reading groups from Azure AD failed:\n" + groupReadFails[0].replace("Reading groups from Azure AD failed: ", "");
				additionalIndex += 1;
			}

			// Some notifiactions
			var ADGroupsNotInSych = tempArr.filter(function (x) {
				return x.indexOf("AD groups not in sync") > -1;
			});
			if (ADGroupsNotInSych.length > 0) {
				if (additionalIndex > 0) {
					additionalText += "\n\n";
				}
				additionalText += "AD groups not in sync:\n" + ADGroupsNotInSych[0].replace("AD groups not in sync:", "");
				additionalIndex += 1;
			}

			var ADGroupsUsersNotFoundInUserTable = tempArr.filter(function (x) {
				return x.indexOf("AD groups users not found in user table:") > -1;
			});
			if (ADGroupsUsersNotFoundInUserTable.length > 0) {
				if (additionalIndex > 0) {
					additionalText += "\n\n";
				}


				additionalText += "AD groups users not found in user table:\n" + ADGroupsUsersNotFoundInUserTable[0].replace("AD groups users not found in user table:", "");
				additionalIndex += 1;
			}

			tempObj.additionalInformation = additionalText;

			if (updateIndex == -1) {
				$scope.AzureData.push(tempObj);
			} else {
				$scope.AzureData[updateIndex] = tempObj;
			}
		} else {
			tempObj = {
				result: "",
				start: "",
				end: "",
				usersAdded: "",
				usersDisabled: "",
				userInfoChanged: "",
				usersReactivated: "",
				adGroupsAdded: "",
				adGroupInfoChanged: "",
				usersAddedToAdGroup: "",
				usersRemovedFromAdGroup: "",
				additionalInformation: ""
			};
			if (updateIndex == -1) {
				$scope.AzureData.push(tempObj);
			} else {
				$scope.AzureData[updateIndex] = tempObj;
			}
		}
	}

	function convertHex(hex, opacity) {
		hex = hex.replace('#', '');
		var r = parseInt(hex.substring(0, 2), 16);
		var g = parseInt(hex.substring(2, 4), 16);
		var b = parseInt(hex.substring(4, 6), 16);

		var result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
		return result;
	}

	function calcMonitorData(data, $scope) {
		if (data.failed == false) {
			$scope.showMonitor = true;
			$scope.chartType = "bar";

			$scope.barLabels = {};
			$scope.barLabelValues = {};

			var monitorResults = data;
			$scope.uptimeMonths = monitorResults.months;
			$scope.uptimeLabels = monitorResults.monthLabels.reverse();

			if ($scope.MonitorStartTime != null && $scope.MonitorStartTime != "" && monthDiff(new Date($scope.MonitorStartTime), new Date()) > 6) {
				$scope.uptimeTableMode = 1;
			}

			if ($scope.MonitorStartTime != null && $scope.MonitorStartTime != "" && new Date($scope.MonitorStartTime).getFullYear() == new Date().getFullYear() && new Date($scope.MonitorStartTime).getMonth() == new Date().getMonth()) {
				$scope.uptimeTableMode = -1;
			}

			if ($scope.monitorActive) {
				if (parseFloat(monitorResults.last_365) < 100 && parseFloat(monitorResults.last_365) >= 99.995) {
					$scope.last365 = parseFloat("99.99").toFixed(2);
				} else {
					$scope.last365 = parseFloat(monitorResults.last_365).toFixed(2);
				}

				if (parseFloat(monitorResults.last_30) < 100 && parseFloat(monitorResults.last_30) >= 99.995) {
					$scope.last30 = parseFloat("99.99").toFixed(2);
				} else {
					$scope.last30 = parseFloat(monitorResults.last_30).toFixed(2);
				}


				if (parseFloat(monitorResults.today) < 100 && parseFloat(monitorResults.today) >= 99.995) {
					$scope.today = parseFloat("99.99").toFixed(2);
				} else {
					$scope.today = parseFloat(monitorResults.today).toFixed(2);
				}

				var valueArr = [];
				for (var j = 0; j < 24; j++) {
					valueArr.push([]);
				}

				var count = 0;
				for (var i = 0; i < monitorResults.values.length; i++) {
					valueArr[count].push(monitorResults.values[i]);
					count += 1;
					if (count == 24) {
						count = 0;
					}
				}

				if ($scope.monitorActive) {
					for (var j = 0; j < 24; j++) {
						$scope.occurenceData.push([]);
						var colors = [];
						for (var i = 0; i < 8; i++) {
							$scope.occurenceData[j + 2].push(1);
							var color = "";
							//var value = monitorResults["value_" + i + "_" + j];
							var value = parseFloat(valueArr[j][i]);
							//if (value > 100) value = 0;
							if (value > -1) {
								//value = 100;
								switch (true) {
									case 95 <= value:
										color = "#388e3c"
										break;
									case 85 <= value:
										color = "#64dd17"
										break;
									case 70 <= value:
										color = "#ffeb3b"
										break;
									default:
										color = "#f44336"
										break;
								}
							} else {
								color = "#eeeeee";
							}

							colors.push(color);

							var labelStart = j.toString();
							var labelEnd = (j + 1).toString();
							if (labelStart.length == 1) labelStart = "0" + labelStart;
							if (labelEnd.length == 1) labelEnd = "0" + labelEnd;
							$scope.barLabels["value_" + i + "_" + j] = labelStart + " - " + labelEnd;
							$scope.barLabelValues["value_" + i + "_" + j] = value;

						}

						var barElementObj = {};
						barElementObj["stack"] = 1;
						barElementObj["yAxisID"] = 'y-axis-2';

						barElementObj["backgroundColor"] = colors;
						barElementObj["pointBackgroundColor"] = colors;
						barElementObj["hoverBackgroundColor"] = colors;
						barElementObj["borderColor"] = colors;

						$scope.occurenceOverride.push(barElementObj);

					}
				}
			}
		}
	}

	function createInstanceUserChart($scope, data, startTime, Translator) {
		var uniquesUsersPerWeek = [];
		var instancesPerWeek = [];
		var instancesPerUser = {};
		$scope.instancesTotal = 0;

		for (var i = 0; i < 12; i++) {
			if (i < 8) {
				var loopItems = data.filter(function (x) {
					return x.category == "week_" + (1 + i);
				});
				if (loopItems.length > 0) {
					var instances = 0;
					var uniqueUsers = [];
					angular.forEach(loopItems, function (item) {
						instances += parseInt(item.count);
						if (uniqueUsers.indexOf(item.name) == -1) uniqueUsers.push(item.name);

						if (instancesPerUser[item.name] != null) {
							instancesPerUser[item.name].count += item.count;
						} else {
							instancesPerUser[item.name] = {name: item.name, count: item.count};
						}
					});
					instancesPerWeek.push(instances);
					uniquesUsersPerWeek.push(uniqueUsers.length);
				} else {
					instancesPerWeek.push(0);
					uniquesUsersPerWeek.push(0);
				}
			} else {
				switch (i) {
					case 8:
						var loopItems = data.filter(function (x) {
							return x.category == "today";
						});
						var uniqueUsers = [];
						angular.forEach(loopItems, function (item) {
							if (uniqueUsers.indexOf(item.name) == -1) uniqueUsers.push(item.name);
						});
						$scope.UniquesUsersToday = uniqueUsers.length;
						break;
					case 9:
						var loopItems = data.filter(function (x) {
							return x.category == "last_30";
						});
						var uniqueUsers = [];
						angular.forEach(loopItems, function (item) {
							if (uniqueUsers.indexOf(item.name) == -1) uniqueUsers.push(item.name);
						});
						$scope.UniquesUsersLast30 = uniqueUsers.length;
						break;
					case 10:
						var loopItems = data.filter(function (x) {
							return x.category == "last_365";
						});
						var uniqueUsers = [];
						angular.forEach(loopItems, function (item) {
							if (uniqueUsers.indexOf(item.name) == -1) uniqueUsers.push(item.name);
						});
						$scope.UniquesUsersLast365 = uniqueUsers.length;
						break;
					default:
						var loopItems = data.filter(function (x) {
							return x.category == "all";
						});
						var instances = 0;
						angular.forEach(loopItems, function (item) {
							instances += item.count || 0;
						});

						$scope.instancesTotal = instances;
				}
			}
		}
		var top5Values = [];
		angular.forEach(instancesPerUser, function (user) {
			top5Values.push(user);
		});
		top5Values.sort((a, b) => (a.count > b.count) ? 1 : -1)
		top5Values.reverse();

		if (top5Values.length > 5) top5Values.length = 5;

		$scope.top5Values = top5Values;
		$scope.instanceChartData = [];
		$scope.instanceChartData.push(instancesPerWeek);
		$scope.instanceChartData.push(uniquesUsersPerWeek);

		$scope.instanceChartSeries = [Translator.translate('ADMIN_CONSOLE_INSTANCE_REQUEST'), Translator.translate('ADMIN_CONSOLE_UNIQUE_USERS')];
		$scope.instanceChartLabels = [];
		for (var i = 0; i < 8; i++) {
			var d = new Date(startTime);
			d.setDate(d.getDate() + (-i * 7));
			$scope.instanceChartLabels.push(getWeekNumber(d));
		}
		$scope.instanceChartLabels.reverse();

		$scope.instanceChartDatasetOverride = [{yAxisID: 'y-axis-1'}, {yAxisID: 'y-axis-2'}];

		var color1 = 'rgba(66, 165, 245,.8)';
		var color2 = 'rgba(255, 213, 79,.8)';

		$scope.instanceChartColors = [{
			backgroundColor: color1,
			pointBackgroundColor: color1,
			hoverBackgroundColor: color1,
			fill: true
		}, {
			backgroundColor: color2,
			pointBackgroundColor: color2,
			hoverBackgroundColor: color2,
			fill: true
		}];

		$scope.instanceChartOptions = {
			maintainAspectratio: true,
			responsive: true,
			legend: {
				display: true
			},
			scales: {
				yAxes: [
					{
						id: 'y-axis-1',
						display: true,
						position: 'left',
						gridLines: {display: false, drawBorder: true},
						ticks: {
							fontFamily: 'Roboto', fontSize: 12,
						},
						scaleLabel: {
							display: true,
							labelString: Translator.translate('ADMIN_CONSOLE_INSTANCE_REQUEST')
						}
					},
					{
						id: 'y-axis-2',
						display: true,
						position: 'right',
						gridLines: {display: false, drawBorder: true},
						ticks: {
							fontFamily: 'Roboto', fontSize: 12
						},
						scaleLabel: {
							display: true,
							labelString: Translator.translate('ADMIN_CONSOLE_UNIQUE_USERS')
						}
					}
				],
				xAxes: [
					{
						id: 'x-axis-1',
						display: true,
						position: 'left',
						gridLines: {display: false, drawBorder: true},
						ticks: {
							fontFamily: 'Roboto', fontSize: 12,
							//min: 0, max: 100, stepSize: 100
						},
						scaleLabel: {
							display: true,
							labelString: Translator.translate('ADMIN_CONSOLE_WEEKS')
						}
					}
				]
			}
		};
	}

	function getWeekNumber(d) {
		d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
		d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
		var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
		var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
		return weekNo;
	}

	function dynamicSort(property) {
		var sortOrder = 1;

		if (property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}

		return function (a, b) {
			if (sortOrder == -1) {
				return b[property].localeCompare(a[property]);
			} else {
				return a[property].localeCompare(b[property]);
			}
		}
	}

	function monthDiff(d1, d2) {
		var months;
		months = (d2.getFullYear() - d1.getFullYear()) * 12;
		months -= d1.getMonth() + 1;
		months += d2.getMonth();
		return months <= 0 ? 0 : months;
	}


})();