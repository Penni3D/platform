(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(PortfoliosWidgetConfig)
		.controller('PortfoliosWidgetController', PortfoliosWidgetController);

	PortfoliosWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider'];
	function PortfoliosWidgetConfig(WidgetsProvider, ViewsProvider) {
		WidgetsProvider.addSystemWidget('widget.multiplePortfolios', {
			name: 'MULTIPLE_PORTFOLIOS',
			options: {
				selectedPortfolioId: function (resolves) {
					return {
						type: 'multiselect',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'Portfolio'
					};
				}
			},
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				}
			},
			onFinish: function (params, resolves) {
				params.portfolioIds = [];

				let s = params.selectedPortfolioId;
				let p = resolves.Portfolios;

				for (let i = 0, l = s.length; i < l; i++) {
					for (let j = 0, l_ = p.length; j < l_; j++) {
						if (s[i] === p[j].ProcessPortfolioId) {
							params.portfolioIds.push({
								process: p[j].Process,
								translation: p[j].LanguageTranslation,
								portfolioId: s[i]
							});

							break;
						}
					}
				}
			}
		});

		ViewsProvider.addView('widget.multiplePortfolios', {
			controller: 'PortfoliosWidgetController',
			template: 'frontpage/widgets/portfolio/portfolioMultiple.html'
		});
	}

	PortfoliosWidgetController.$inject = [
		'$scope',
		'ViewService',
		'UniqueService',
		'StateParameters'
	];
	function PortfoliosWidgetController(
		$scope,
		ViewService,
		UniqueService,
		StateParameters
	) {
		$scope.portfolios = StateParameters.portfolioIds;
		$scope.id = UniqueService.get('widget-portfolios', true);

		let loaded = {};

		$scope.loadPortfolio = function (portfolioId) {
			if (loaded[portfolioId]) {
				return;
			}

			loaded[portfolioId] = true;

			ViewService.viewAdvanced(
				'portfolio',
				{
					params: angular.extend({ prevent: true, frontpage: true }, $scope.portfolios[portfolioId])
				},
				{
					targetId: $scope.id + '-' + portfolioId,
					ParentViewId: StateParameters.ViewId
				}
			);
		};
	}
})();
