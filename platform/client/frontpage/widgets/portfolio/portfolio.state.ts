﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(PortfolioWidgetConfig);

	PortfolioWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider', 'PortfolioViewProvider'];
	function PortfolioWidgetConfig(WidgetsProvider, ViewsProvider, PortfolioViewProvider) {
		let portfolioConfigurations = PortfolioViewProvider.getViewConfigurations();
		let portfolioView = PortfolioViewProvider.getView();
		let emptyFunction = function () {
			return [];
		};

		portfolioView.resolve.PortfolioPhases = emptyFunction;
		portfolioView.resolve.SavedSettings = emptyFunction;
		portfolioView.resolve.Dashboards = emptyFunction;
		portfolioView.resolve.Templates = emptyFunction;
		portfolioView.resolve.Rights = emptyFunction;
		portfolioView.afterResolve.ActiveSettings = function (PortfolioService, StateParameters, PortfolioColumns) {
			let settings = PortfolioService.getActiveSettings(StateParameters.portfolioId);
			let filters = settings.filters;
			filters.columns = [];
			filters.page = 0;
		//	filters.limit = 25;

			for (let i = 0, l = PortfolioColumns.length; i < l; i++) {
				let column = PortfolioColumns[i];
				if (!column.IsDefault) {
					filters.columns.push(column.ItemColumn.Name);
				}
			}

			return settings;
		};

		let widgetConfiguration = {
			name: 'WIDGET_PORTFOLIO',
			options: {
				portfolioId: function (resolves) {
					return {
						type: 'select',
						options: resolves.Portfolios,
						optionName: 'LanguageTranslation',
						optionValue: 'ProcessPortfolioId',
						label: 'PORTFOLIO'
					};
				},
				portfolioView: function () {
					return {
						type: 'select',
						label: 'PORTFOLIO_VIEW',
						options: [
							{ variable: 'PORTFOLIO_FV', value: 1 },
							{ variable: 'PORTFOLIO_GV', value: 7 }],
						optionName: 'variable'
					};
				},
				color: function () {
					return {
						type: 'select',
						label: 'NOTIFICATION_COLOR',
						options: [
							{ variable: 'CONDITION_RED', value: '#f44335' },
							{ variable: 'CONDITION_GREEN', value: '#4caf50' },
							{ variable: 'CONDITION_YELLOW', value: '#ff9800' }],
						optionName: 'variable'
					};
				},
				disableIcon: function(){
					return {
						type: 'select',
						label: 'DISABLE_ICON',
						options: [
							{ variable: 'MSG_NO', value: 'false' },
							{ variable: 'MSG_YES', value: 'true' }],
						optionName: 'variable'
					};
				},
				hideIfNoRows: function(){
					return {
						type: 'select',
						label: 'HIDE_IF_NO_ROWS',
						options: [
							{ variable: 'MSG_NO', value: 'false' },
							{ variable: 'MSG_YES', value: 'true' }],
						optionName: 'variable'
					};
				},

			},
			onSetup: {
				Portfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios();
				},
				GetDataType: function (Common) {
					return Common.getRelevantDataType;
				},
				GetColumns: function (ProcessService) {
					return ProcessService.getColumns;
				},
				GetPortfolioColumns: function (PortfolioService) {
					return PortfolioService.getPortfolioColumns;
				},
				Resolve: function ($q) {
					return $q.all;
				}
			},
			onFinish: function (params) {
				params.frontpage = true;
			},
			onChange: function (p, params, resolves, options) {
				if (p === 'portfolioId') {
					let p = resolves.Portfolios;
					let i = p.length;
					while (i--) {
						if (p[i].ProcessPortfolioId == params.portfolioId) {
							let process = p[i].Process;
							params.process = process;

							let promises = [];
							let columns = [];

							promises.push(resolves.GetColumns(process).then(function (itemColumns) {
								angular.forEach(itemColumns, function (column) {
									if (column.StatusColumn) {
										columns.push(column);
									}
								});
							}));

							promises.push(resolves.GetPortfolioColumns(process, params.portfolioId)
								.then(function (portfolioColumns) {
									for (let i = 0, l = portfolioColumns.length; i < l; i++) {
										columns.push(portfolioColumns[i].ItemColumn);
									}
							}));

							resolves.Resolve(promises).then(function () {
								let itemColumns = _.uniqBy(columns, function (column) {
									return column.ItemColumnId;
								});

								let itemColumnParams = [
									'groupTitleColumnId', 'groupHorizontalColumnId', 'groupVerticalColumnId',
									'groupDescriptionColumnId', 'groupColorColumnId', 'groupChipColumnId'
								];

								let i = itemColumnParams.length;
								while (i--) {
									options[itemColumnParams[i]].options = itemColumns;
								}
							});

							break;
						}
					}
				}
			}
		};

		angular.forEach(portfolioConfigurations.tabs, function (tab, tabName: string) {
		    if (tabName !== 'BOARD') {
				angular.forEach(tab, function (options, param) {
					widgetConfiguration.options[param] = options;
				});
			}
		});

		WidgetsProvider.addSystemWidget('widget.portfolio', widgetConfiguration);
		ViewsProvider.addView('widget.portfolio', portfolioView);
	}
})();
