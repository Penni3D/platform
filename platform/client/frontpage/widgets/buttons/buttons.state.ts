﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.config(ButtonsWidgetConfig)
		.controller('ButtonsWidgetController', ButtonsWidgetController);

	ButtonsWidgetController.$inject = ['$scope', 'StateParameters', 'UniqueService', 'SidenavService'];
	function ButtonsWidgetController($scope, StateParameters, UniqueService, SidenavService) {
		$scope.onClick = function () {
			SidenavService.staticNavigation(StateParameters.navigationMenuId);
		};

		$scope.id = UniqueService.get('widget-buttons', true);
		$scope.text = StateParameters.text;
		$scope.description = StateParameters.description;

		setTimeout(function () {
			$('#' + $scope.id + ' .background').addClass(StateParameters.backgroundWidget);
		});
	}
	
	ButtonsWidgetConfig.$inject = ['WidgetsProvider', 'ViewsProvider', 'BackgroundsProvider'];
	function ButtonsWidgetConfig(WidgetsProvider, ViewsProvider, BackgroundsProvider) {
		var confs = {
			template: 'frontpage/widgets/buttons/buttons.html',
			controller: 'ButtonsWidgetController'
		};

		var options = {
			navigationMenuId: function (resolves) {
				return {
					type: 'select',
					label: 'WIDGET_BUTTONS_IM',
					options: resolves.NavigationMenu,
					optionName: 'LanguageTranslation',
					optionValue: 'InstanceMenuId',
					optionSelf: 'InstanceMenuId',
					optionParent: 'ParentInstanceMenuId'
				};
			},
			backgroundWidget: function () {
				return {
					type: 'select',
					options: BackgroundsProvider.getBackgrounds(),
					label: 'WIDGET_BUTTONS_WBI',
					optionValue: 'background'
				};
			},
			text: function () {
				return {
					type: 'translation',
					value: '',
					label: 'WIDGET_BUTTONS_TIA'
				};
			},
			description: function () {
				return {
					type: 'translation',
					value: '',
					label: 'WIDGET_BUTTONS_DT'
				};
			}
		};

		var resolves = {
			NavigationMenu: function (SidenavService) {
				return SidenavService.getStaticLinks();
			}
		};

		ViewsProvider.addView('widget.view', confs);
		ViewsProvider.addView('widget.new', confs);

		WidgetsProvider.addSystemWidget('widget.new', {
			name: 'CREATE_PROCESS',
			options: options,
			onSetup: resolves
		});

		WidgetsProvider.addSystemWidget('widget.view', {
			name: 'OPEN_VIEW',
			options: options,
			onSetup: resolves
		});
	}
})();
