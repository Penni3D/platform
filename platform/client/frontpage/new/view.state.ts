﻿(function () {
	'use strict';

	angular
		.module('core.widgets')
		.controller('FrontpageWidgetCreateController', FrontpageWidgetCreateController)
		.controller('FrontpageWidgetController', FrontpageWidgetController);

	FrontpageWidgetCreateController.$inject = [
		'$scope',
		'Translator',
		'Widgets',
		'MessageService',
		'$q',
		'Common',
		'MenuNavigationService',
		'LastNo',
		'ViewConfigurator',
		'AddWidget'
	];

	function FrontpageWidgetCreateController(
		$scope,
		Translator,
		Widgets,
		MessageService,
		$q,
		Common,
		MenuNavigationService,
		LastNo,
		ViewConfigurator,
		AddWidget
	) {

		$scope.systemWidgets = [];
		for (let w of Widgets.getSystemWidgets()) {
			w.params.translation = Translator.translate(w.params.name);
			$scope.systemWidgets.push(w);
		}
		$scope.systemWidgets = $scope.systemWidgets.sort(function (a, b) {
			return a.params.translation.localeCompare(b.params.translation);
		});
		
		$scope.customerWidgets = Widgets.getCustomerWidgets();
		$scope.options = {};
		$scope.params = {};
		$scope.size = 'col-xs-12 col-md-6';
		$scope.screenOption = [{name: 'WIDGET_VIEW_FS', value: 'col-xs-12'}, {
			name: 'WIDGET_VIEW_HS',
			value: 'col-xs-12 col-md-6'
		}];

		var resolves;
		$scope.selectWidget = function (w) {
			$scope.params = {};
			$scope.selectedWidget = w;
			$scope.options = {};
		};

		$scope.widgetSelected = function () {
			return typeof $scope.selectedWidget !== 'undefined';
		};

		$scope.nextText = Translator.translate('WIDGET_VIEW_NEXT'); //initialize button text. text is changed on setup -function
		$scope.setup = function () {
			if ($scope.showSetup) {
				if (typeof $scope.selectedWidget.params.onFinish === 'function') {
					$scope.selectedWidget.params.onFinish($scope.params, resolves);
				}

				var w = MenuNavigationService.getWidgetTemplate();
				w.DefaultState = $scope.selectedWidget.view;
				w.LanguageTranslation = $scope.title;
				w.OrderNo = LastNo;
				w.Params = angular.extend($scope.params, {ViewSize: $scope.size});

				MenuNavigationService.newWidgets([w]).then(function (widgets) {
					AddWidget(widgets[0]);
					MessageService.closeDialog();
				});
			} else if ($scope.widgetSelected()) {
				$scope.showSetup = true;
				$scope.loading = true;
				var d = $q.defer();

				if (typeof $scope.selectedWidget.params.onSetup === 'undefined') {
					d.resolve();
				} else {
					Common.resolve($scope.selectedWidget.params.onSetup).then(function (locals) {
						d.resolve(locals);
					}, function () {
						d.reject();
					});
				}

				d.promise.then(function (locals) {
					$scope.loading = false;
					resolves = locals;

					angular.forEach($scope.selectedWidget.params.options, function (value, key) {
						var v = value(resolves);

						if (v.type === 'select' || v.type === 'boolean') {
							$scope.params[key] = '';
							v.singleValue = true
						} else if (v.type === 'multiselect') {
							$scope.params[key] = Array.isArray(v.value) ? v.value : [];
						} else {
							$scope.params[key] = v.value;
						}

						if (typeof v.visible === 'undefined') {
							v.visible = true;
						}

						$scope.options[key] = v;
					});

					$scope.onChange = function (name) {
						if (typeof $scope.selectedWidget.params.onChange === 'function') {
							$scope.selectedWidget.params.onChange(name, $scope.params, locals, $scope.options);
						}
					};
				});
			}
			$scope.nextText = Translator.translate('WIDGET_VIEW_FINISH');
		};

		$scope.back = function () {
			$scope.showSetup = false;
			$scope.nextText = Translator.translate('WIDGET_VIEW_NEXT');
		};

		$scope.cancel = function () {
			MessageService.closeDialog();
		};
	}

	FrontpageWidgetController.$inject = [
		'Widget',
		'$scope',
		'Widgets',
		'MessageService',
		'Common',
		'$q',
		'Refresh'
	];

	function FrontpageWidgetController(
		Widget,
		$scope,
		Widgets,
		MessageService,
		Common,
		$q,
		Refresh
	) {
		$scope.widget = Widget;
		$scope.options = {};
		$scope.loading = true;
		$scope.screenOption = [{name: 'WIDGET_VIEW_FS', value: 'col-xs-12'}, {
			name: 'WIDGET_VIEW_HS',
			value: 'col-xs-12 col-md-6'
		}];

		$scope.cancel = function () {
			MessageService.closeDialog();
		};

		var widgets = Widgets.getWidgets();
		var widgetDefinition;
		var i = widgets.length;
		while (i--) {
			var w = widgets[i];
			if (Widget.DefaultState === w.view) {
				widgetDefinition = w;
				break;
			}
		}

		var d = $q.defer();
		if (typeof widgetDefinition.params.onSetup === 'undefined') {
			d.resolve();
		} else {
			Common.resolve(widgetDefinition.params.onSetup).then(function (locals) {
				d.resolve(locals);
			}, function () {
				d.reject();
			});
		}

		d.promise.then(function (locals) {
			$scope.loading = false;
			angular.forEach(widgetDefinition.params.options, function (value, key) {
				var v = value(locals);

				if (v.type === 'select' || v.type === 'boolean') {
					v.singleValue = true
				}

				if (typeof v.visible === 'undefined') {
					v.visible = true;
				}

				$scope.options[key] = v;
			});

			$scope.save = function () {
				if (typeof widgetDefinition.params.onFinish === 'function') {
					widgetDefinition.params.onFinish($scope.widget.Params, locals);
				}

				Refresh();
				MessageService.closeDialog();
			};

			$scope.onChange = function (name) {
				if (typeof widgetDefinition.params.onChange === 'function') {
					widgetDefinition.params.onChange(name, $scope.widget.Params, locals, $scope.options);
				}
			};

			angular.forEach(widgetDefinition.params.options, function (value, key) {
				if ($scope.widget.Params[key]) {
					$scope.onChange(key);
				}
			});
		}, function () {
			MessageService.closeDialog();
		});
	}
})();
