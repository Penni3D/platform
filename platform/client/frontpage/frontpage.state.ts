(function () {
	'use strict';

	angular
		.module('core')
		.controller('FrontpageController', FrontpageController)
		.config(FrontpageConfig);

	FrontpageConfig.$inject = ['ViewsProvider', 'GlobalsProvider', 'ViewConfiguratorProvider'];

	function FrontpageConfig(ViewsProvider, GlobalsProvider, ViewConfiguratorProvider) {
		ViewsProvider.addView('frontpage', {
			controller: 'FrontpageController',
			template: 'frontpage/frontpage.html',
			resolve: {
				Widgets: function (ViewConfigurator) {
					return ViewConfigurator.getWidgets();
				},
				Rights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('frontpage');
				},
				Logos: function (AttachmentService) {
					return AttachmentService.getControlAttachments('frontpage', 'banner');
				},
				TextBanner: function (AttachmentService) {
					return AttachmentService.getControlAttachments('frontpage', 'textbanner');
				}
			}
		});

		GlobalsProvider.addGlobal('frontpage', ['write']);
		ViewConfiguratorProvider.addConfiguration('frontpage', {
			onSetup: {
				BannerOptions: function () {
					return [
						{
							name: 'FP_UWB', value: 'logo'
						},
						{
							name: 'FP_UTB', value: 'text'
						},
						{
							name: 'FP_UWTB', value: 'textlogo'
						},
						{
							name: 'FP_NONE', value: 'none'
						}];
				},
				FrontpageBackgrounds: function (FrontpageBackgrounds) {
					return FrontpageBackgrounds.getBackgrounds();
				},
				Backgrounds: function (Backgrounds) {
					return Backgrounds.getBackgrounds();
				}
			},
			tabs: {
				default: {
					bannerMode: function (resolves) {
						return {
							type: 'select',
							options: resolves.BannerOptions,
							label: 'FP_BM'
						};
					},
					upperDescription: function () {
						return {
							type: 'string',
							label: 'FP_UD'
						};
					},
					lowerDescription: function () {
						return {
							type: 'text',
							label: 'FP_LD'
						};
					},
					background: function (resolves) {
						return {
							type: 'select',
							label: 'FP_BI',
							options: resolves.Backgrounds,
							optionValue: 'background'
						}
					},
					frontpageBackground: function (resolves) {
						return {
							type: 'select',
							label: 'FP_FBI',
							options: resolves.FrontpageBackgrounds,
							optionValue: 'background'
						}
					}
				}
			},
			defaults: {
				bannerMode: 'widgets',
				lowerDescription: '',
				upperDescription: ''
			}
		});
	}

	FrontpageController.$inject = [
		'$scope',
		'ToolbarService',
		'ViewService',
		'Configuration',
		'MessageService',
		'Common',
		'$rootScope',
		'SidenavService',
		'$timeout',
		'$q',
		'UniqueService',
		'MenuNavigationService',
		'Widgets',
		'ViewConfigurator',
		'Rights',
		'StateParameters',
		'Logos',
		'TextBanner',
		'Translator',
		'NotificationService'
	];

	function FrontpageController(
		$scope,
		ToolbarService,
		ViewService,
		Configuration,
		MessageService,
		Common,
		$rootScope,
		SidenavService,
		$timeout,
		$q,
		UniqueService,
		MenuNavigationService,
		Widgets,
		ViewConfigurator,
		Rights,
		StateParameters,
		Logos,
		TextBanner,
		Translator,
		NotificationService
	) {
		$rootScope.frontpage = true;
		$scope.hasCustomerLogo = false;
		$scope.customerLogoThumbnail = 0;

		$scope.$on('$destroy', function () {
			$rootScope.frontpage = false;
		});

		$scope.edit = false;

		$scope.widgets = Widgets;
		$scope.welcome = ViewConfigurator.getGlobal('frontpage');

		if (Rights.indexOf('write') !== -1) {
			ToolbarService.setConfigureFunction(function () {
				$scope.edit = !$scope.edit;
			});
		}

		let importantNotifications = NotificationService.getUnreadImportantNotifications();
		if (importantNotifications.length) {
			NotificationService.setImportantShown();
			ViewService.view('notification.view', {
				locals: {
					UnreadNotifications: importantNotifications
				}
			});
		}

		//Hide portfolio widget empty space if user has no rights no any shown portfolio
		$scope.rightsToPorfolios = false;
		let hideBannerAreaIfNoRights = function () {
			for (let i = 0; i < $scope.widgets.length; i++) {
				if ($scope.widgets[i].LinkType == 5) {
					$scope.rightsToPorfolios = true;
					break;
				}
			}
		}
		hideBannerAreaIfNoRights();

		$scope.showBulletins = [];
		$scope.currentBulletin = 0;
		$scope.currentBullet = {};
		$scope.localeId = $rootScope.me.locale_id;
		NotificationService.getImportantBulletins().then(function (result) {
			if (result.length > 0) {
				MessageService.backdrop();
				$scope.currentBulletin = 1;
				$scope.showBulletins = result;
				$scope.currentBullet = $scope.showBulletins[$scope.currentBulletin - 1];
			}
		});
		$scope.nextBulletin = function () {
			$scope.currentBulletin += 1;
			$scope.currentBullet = $scope.showBulletins[$scope.currentBulletin - 1];
		};
		$scope.prevBulletin = function () {
			$scope.currentBulletin -= 1;
			$scope.currentBullet = $scope.showBulletins[$scope.currentBulletin - 1];
		};
		$scope.closeBulletins = function () {
			MessageService.removeBackdrop();
			for (let n of $scope.showBulletins) {
				NotificationService.readBulletins(n.item_id);
			}
			$scope.showBulletins = [];
		};

		let i = Widgets.length;
		while (i--) {
			let widget = Widgets[i];
			widget.id = UniqueService.get('frontpage-widget', true);
			widget.translation = Translator.translation(widget.LanguageTranslation);
		}

		SidenavService.navigation();

		function setWelcomeBackground() {
			let d = $q.defer();
			$scope.bgImage = $scope.welcome.Params.frontpageBackground;
			$scope.showLogoArea = false;
			if ($scope.welcome.Params.bannerMode === 'logo') {
				$scope.showTextBanner = false;
				$scope.showLogoArea = true;
			} else if ($scope.welcome.Params.bannerMode === 'text') {
				$scope.showTextBanner = true;
				$scope.showLogoArea = false;
			} else if ($scope.welcome.Params.bannerMode === "none") {
				$scope.showTextBanner = false;
				$scope.showLogoArea = false;
			} else {
				$scope.showTextBanner = true;
				$scope.showLogoArea = true;
			}
			$scope.showTextBannerFull = $scope.showTextBanner && ($scope.welcome.Params.upperDescription == "" && $scope.welcome.Params.lowerDescription == "");


			$scope.showWidgetsBanner = Widgets.length > 0;
			if ($scope.showWidgetsBanner) {
				$timeout(function () {
					ViewService.viewAdvanced('frontpage.banner', undefined, {
						targetId: 'frontpage-banner-portfolio', ParentViewId: StateParameters.ViewId
					});

					$timeout(function () {
						d.resolve();
					});
				});
			} else {
				ViewService.close('frontpage-banner-portfolio');

				d.resolve();
			}

			return d.promise;
		}

		setWelcomeBackground().then(function () {
			$timeout(function () {
				for (let w of $scope.widgets) {
					if (w.LinkType == 5) continue;
					setTimeout(function () {
						ViewService.viewAdvanced(
							w.DefaultState,
							{params: w.Params},
							{targetId: w.id, ParentViewId: StateParameters.ViewId});
					}, w.Params.Delay && w.Params.Delay != "" ? parseInt(w.Params.Delay) : 0);
				}
			});
		});

		if (Logos.length) {
			$scope.hasCustomerLogo = Logos[Logos.length - 1].AttachmentId;
			$scope.customerLogoThumbnail = Logos[Logos.length - 1].AttachmentData.HasThumbnail ? 1 : 0;
		}

		if (TextBanner.length) {
			$scope.hasTextBanner = TextBanner[TextBanner.length - 1].AttachmentId;
			$scope.welcome.Params.background = 'custom-banner';
			$scope.textBannerStyle = "background-image:URL('v1/meta/AttachmentsDownload/" + $scope.hasTextBanner + "/0');";
		}

		$scope.logoOptions = {
			height: 120,
			maxWidth: 640
		};

		$scope.textBannerOptions = {};

		$scope.editWelcome = function () {
			ViewConfigurator.configurePage('frontpage', $scope.welcome.Params, function (params) {
				$scope.welcome.Params = params;
				MenuNavigationService.saveWidget($scope.welcome.InstanceMenuId, $scope.welcome);
				setWelcomeBackground();
			});
		};

		$scope.removeWidget = function (widget) {
			let y = function () {
				ViewService.close(widget.id);

				MenuNavigationService.deleteWidget(widget.InstanceMenuId);
			};

			MessageService.yesNo('FRONTPAGE_AYSYWTRTVFF', y);
		};

		$scope.addWidget = function () {
			MessageService.dialogAdvanced({
				controller: 'FrontpageWidgetCreateController',
				template: 'frontpage/new/view.html',
				locals: {
					LastNo: Common.getOrderNoBetween($scope.widgets.length ? $scope.widgets[$scope.widgets.length - 1].OrderNo : undefined),
					AddWidget: function (widget) {
						widget.id = UniqueService.get('frontpage-widget', true);
						widget.translation = Translator.translation(widget.LanguageTranslation);

						$timeout(function () {
							ViewService.viewAdvanced(
								widget.DefaultState,
								{params: widget.Params},
								{targetId: widget.id, ParentViewId: StateParameters.ViewId});
						});
					}
				}
			});
		};

		$scope.editWidget = function (widget) {
			MessageService.dialogAdvanced({
				controller: 'FrontpageWidgetController',
				template: 'frontpage/new/edit.html',
				locals: {
					Widget: widget,
					Refresh: function () {
						widget.translation = Translator.translation(widget.LanguageTranslation);
						MenuNavigationService.saveWidget(widget.InstanceMenuId, widget);
						ViewService.refresh(widget.id, widget.Params);
					}
				}
			});
		};


		$scope.shiftLeft = function (widget) {
			let i = $scope.widgets.length;
			let j = 0;
			while (i--) {
				if ($scope.widgets[i].id === widget.id) {
					j = i;
				} else if (j && $scope.widgets[i].LinkType === 4) {
					break;
				}
			}

			let view = $scope.widgets[j];
			let pView = $scope.widgets[i];

			$scope.widgets[i] = $scope.widgets[j];
			$scope.widgets[j] = pView;

			shift(pView, view);
		};

		$scope.shiftRight = function (widget) {
			let j = 0;
			let i = 0;
			for (i = 0; i < $scope.widgets.length; i++) {
				if ($scope.widgets[i].id === widget.id) {
					j = i;
				} else if (j && $scope.widgets[i].LinkType === 4) {
					break;
				}
			}

			let view = $scope.widgets[j];
			let nView = $scope.widgets[i];

			$scope.widgets[i] = $scope.widgets[j];
			$scope.widgets[j] = nView;

			shift(view, nView);
		};

		function shift(current, next) {
			let orderNo = current.OrderNo;
			current.OrderNo = next.OrderNo;
			next.OrderNo = current.OrderNo == orderNo ? Common.getOrderNoBetween(current.OrderNo) : orderNo;

			MenuNavigationService.saveWidgets([current, next]);

			let promises = [];
			promises.push(ViewService.close(current.id));
			promises.push(ViewService.close(next.id));

			$q.all(promises).then(function () {
				$timeout(function () {
					ViewService.viewAdvanced(
						current.DefaultState,
						{params: current.Params},
						{targetId: current.id, ParentViewId: StateParameters.ViewId});
					ViewService.viewAdvanced(
						next.DefaultState,
						{params: next.Params},
						{targetId: next.id, ParentViewId: StateParameters.ViewId});
				});
			});

		}

		$scope.shiftBanner = function (widget) {
			let i = $scope.widgets.length;
			while (i--) {
				if ($scope.widgets[i].id === widget.id) {
					break;
				}
			}

			let w = $scope.widgets[i];
			w.LinkType = 5;

			setWelcomeBackground();
			MenuNavigationService.saveWidget(w.InstanceMenuId, w);
		};
	}
})();