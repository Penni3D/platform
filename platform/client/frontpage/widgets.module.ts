﻿(function () {
	'use strict';

	angular
		.module('core.widgets', [])
		.config(WidgetsConfig)
		.provider('Widgets', Widgets);

	WidgetsConfig.$inject = ['ViewsProvider'];
	function WidgetsConfig(ViewsProvider) {
		ViewsProvider.addView('widget', {})
	}

	Widgets.$inject = [];
	function Widgets() {
		let customerWidgets = [];
		let systemWidgets = [];

		this.addWidget = function (widget, options) {
			customerWidgets.push({ view: widget, params: angular.copy(options) });
		};

		this.addSystemWidget = function (widget, options) {
			systemWidgets.push({ view: widget, params: angular.copy(options) });
		};

		this.$get = function () {
			return {
				getWidgets: function () {
					return angular.copy(systemWidgets.concat(customerWidgets));
				},
				getCustomerWidgets: function () {
					return angular.copy(customerWidgets);
				},
				getSystemWidgets: function () {
					return angular.copy(systemWidgets);
				}
			};
		};
	}
})();


