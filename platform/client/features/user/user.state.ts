﻿
(function () {
    'use strict';

    angular
        .module('core.features')
        .config(UserConfig)
        .service('UserFeatureService', UserFeatureService)
        .controller('UserController', UserController);

    UserConfig.$inject = ['ViewsProvider'];
    function UserConfig(ViewsProvider) {
		ViewsProvider.addView('process.user', {
			controller: 'UserController',
			template: 'features/user/user.html',
			level: 2,
			resolve: {
				UserInfo: function (ProcessService, StateParameters) {
					return ProcessService.getItem('user', StateParameters.itemId);
				},
				AllGroups: function (UserFeatureService, StateParameters) {
					return UserFeatureService.getGroups(StateParameters.process);
				},
				UserGroups: function (UserFeatureService, StateParameters) {
					return UserFeatureService.getUserGroups(StateParameters.itemId);
				},
                TwoFactor: function(UserFeatureService, StateParameters) {
                    return UserFeatureService.getTwoFactorInformation(StateParameters.itemId);
                }
			},
            afterResolve: {
			    Groups: function (States, AllGroups, UserGroups, StateParameters, Common) {
                    if (angular.isDefined(States.USER_ACCOUNT_INFORMATION) &&
                        States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId] !== 'undefined' > -1 &&
                        States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId].s.indexOf("writegroup") > -1
                    ) {
                        return AllGroups;
                    } else {
                        let groups = [];
                        angular.forEach(AllGroups, function(g) {
                            if (angular.isDefined(UserGroups[g.item_id]) && UserGroups[g.item_id].selected) {
                                groups.push(g);
                            }

                        });
                        return groups;
                    }
                }
            }
		});

		ViewsProvider.addFeature('process.user', 'USER_ACCOUNT_INFORMATION', "FEATURE_USER_ACCOUNT_INFORMATION", ['read', 'write', 'password', 'writegroup']);
	}

    UserFeatureService.$inject = ['ApiService'];
    function UserFeatureService(ApiService) {
        let self = this;

        let Groups = ApiService.v1api('meta/Items/user_group');
        let UserGroups = ApiService.v1api('meta/UserGroups');
        let Competence = ApiService.v1api('model/process/Competence');
        let TwoFactor = ApiService.v1api('core/TwoFactor');

        self.getGroups = function() {
			return Groups.get().then(function (respond) {
                // let index = _.findIndex(respond,  {item_id: 2});
                // respond.splice(index, 1);
                let groups = [];
                angular.forEach(respond, function(g) {
                    if (angular.isDefined(g.usergroup_name)) {
                        groups.push(g);
                    }
                });
                return groups;
            });
        };

        self.getTwoFactorInformation = function(itemId) {
            return TwoFactor.get(itemId);
        };
        self.toggleTwoFactor = function(itemId, enabled) {
            return TwoFactor.save([itemId,enabled]);
        };
	    self.toggleKetoAuth = function(itemId, enabled) {
		    return TwoFactor.save([itemId,'ketoauth',enabled]);
	    };
        self.getUserGroups = function (itemId) {
			return UserGroups.get(itemId);
        };

        self.addUserGroups = function (itemId, data) {
			return UserGroups.new([itemId], data);
        };

        self.deleteUserGroups = function (itemId, deleteIds) {
			return UserGroups.delete([itemId, deleteIds]);
        };

        self.updateGroup = function (id, payload) {
			return GroupData.save(id, payload);
        };

        self.getCompetence = function (itemId) {
            return Competence.get(itemId);
        };

        self.addCompetence = function (itemId, data) {
            return Competence.new([itemId], data);
        };

        self.deleteCompetence = function (itemId, deleteIds) {
            return Competence.delete([itemId, deleteIds]);
        };
    }

	UserController.$inject = [
		'$rootScope',
        '$scope',
		'StateParameters',
		'UserInfo',
		'Groups',
		'UserGroups',
		'UserFeatureService',
		'States',
		'ProcessService',
        'TwoFactor',
		'MessageService'
	];

    function UserController(
    	$rootScope,
        $scope,
		StateParameters,
		UserInfo,
		Groups,
		UserGroups,
		UserFeatureService,
		States,
		ProcessService,
        TwoFactor,
	    MessageService
	) {

        $scope.header = {
            title: StateParameters.title,
        };

        $scope.edit = false;
        $scope.passwordEdit = false;
        $scope.writeGroup = false;
        $scope.twofactor = TwoFactor;

        if (angular.isDefined(States.USER_ACCOUNT_INFORMATION) &&
	        States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId] !== 'undefined' > -1 &&
	        States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId].s.indexOf("write") > -1    
		) {
            $scope.edit = true;
            $scope.passwordEdit = true;
        } else if ($rootScope.clientInformation.item_id == StateParameters.itemId) {
            $scope.passwordEdit = true;
        }

        if (angular.isDefined(States.USER_ACCOUNT_INFORMATION) &&
	        States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId] !== 'undefined' > -1 &&
	        States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId].s.indexOf("password") > -1
        ) {
	        $scope.passwordEdit = true;
        }
        
        if (angular.isDefined(States.USER_ACCOUNT_INFORMATION) &&
            States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId] !== 'undefined' > -1 &&
            States.USER_ACCOUNT_INFORMATION.groupStates[StateParameters.processGroupId].s.indexOf("writegroup") > -1
        ) {
            $scope.writeGroup = true;
        } 
        
        $scope.data = UserInfo;
        $scope.groups = Groups;
        $scope.userGroups = UserGroups;
        
        $scope.activeOptions = [{ value: 0, name: 'USER_6_N' }, { value: 1, name: 'USER_7_Y' }];

        $scope.onChangeActive = function() {
        	if ($scope.data.active == 0) {
		        $scope.data.active = 1;
		        MessageService.confirm("USER_DEACTIVATE_CONFIRM", function() {
			        $scope.data.active = 0;
			        $scope.onChange();    	
		        }, "warn");
	        } else {
		        $scope.onChange();
	        }
        };
        
        $scope.onChangeGroup = function (itemId) {
            if ($scope.userGroups[itemId].selected === true) {
                UserFeatureService.addUserGroups(StateParameters.itemId, [itemId]);
            } else if ($scope.userGroups[itemId].selected === false) {
                UserFeatureService.deleteUserGroups(StateParameters.itemId, itemId);
            }
        };
        
        $scope.onChangeTwoFactor = function() {
            UserFeatureService.toggleTwoFactor(StateParameters.itemId, $scope.twofactor.enabled).then(function () {
                if ($scope.twofactor.enabled) {
                     UserFeatureService.getTwoFactorInformation(StateParameters.itemId).then(function (data) {
                         $scope.twofactor = data;
                     });
                } 
            });
        };

	    $scope.onChangeKetoAuth = function() {
		    UserFeatureService.toggleKetoAuth(StateParameters.itemId, $scope.twofactor.ketoAuth).then(function () {
				//ok
		    });
	    };
	    
		$scope.onChange = function () {
			ProcessService.setDataChanged('user', StateParameters.itemId, false);
        };
    }
})();
