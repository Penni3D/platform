(function(){
	'use strict';
	angular
		.module('core.features')
		.config(SimpleMonthlyAllocationConfig)
		.controller('SimpleMonthlyAllocationController', SimpleMonthlyAllocationController)
		.service('SimpleMonthlyAllocationService', SimpleMonthlyAllocationService);

	SimpleMonthlyAllocationConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
    function SimpleMonthlyAllocationConfig(ViewsProvider, ViewConfiguratorProvider) {

	    ViewsProvider.addView('process.simpleMonthlyAllocation', {
		    controller: 'SimpleMonthlyAllocationController',
		    template: 'features/simpleAllocation/simpleAllocation.html',
            resolve: {
		        WorkCategoryList: function (ListsService, StateParameters, $q) {
		            if (StateParameters.WorkCategoryList) {
                        return ListsService.getProcessItems(StateParameters.WorkCategoryList, 'order');
                    }
                    else {
		                return  $q.when([]);
                    }
                },
                Sums: function (SimpleMonthlyAllocationService, StateParameters) {
                    return SimpleMonthlyAllocationService.getSums(StateParameters.itemId, new Date().getFullYear(), StateParameters.process);
                }
            }
	    });
	    
		ViewsProvider.addFeature('process.simpleMonthlyAllocation', 'simple_monthly_allocation', 'FEATURE_SIMPLE_MONTHLY_ALLOCATION', ['read', 'write']);
		
        ViewConfiguratorProvider.addConfiguration('process.simpleMonthlyAllocation', {
            global: true,
            onSetup: {
                Lists: function (SettingsService) {
                    return SettingsService.ListProcesses();
                },
                BaseCalendars: function (CalendarService) {
                    return CalendarService.getBaseCalendars();
                }
            },
            defaults: {
                UserMode: 0,
                DateMode: 0,
                SumMode: 0,
                ShowWorkCategoryTotals: 0,
                LockHistory: 0,
                AllowMove: 0
            },
            tabs: {
                default: {
                    WorkCategoryList: function (resolves) {
                        return {
                            label: 'WORK_CATEGORY_LIST',
                            type: 'select',
                            options: resolves.Lists,
                            optionValue: 'ProcessName',
							optionName: 'LanguageTranslation'
                        };
                    },
                    UserMode: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_COMPETENCY', value: 0 }, { name: 'SIMPLE_ALLOCATION_USER', value: 1 }],
                            label: 'SIMPLE_ALLOCATION_USERMODE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    BaseCalendarId: function (resolves) {
                        return {
                            label: 'SIMPLE_ALLOCATION_BASECALENDAR',
                            type: 'select',
                            options: resolves.BaseCalendars,
                            optionValue: 'CalendarId',
                            optionName: 'CalendarName'
                        };
                    },
                    DateMode: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_MONTH_MODE', value: 0 }, { name: 'SIMPLE_ALLOCATION_WEEK_MODE', value: 1 }],
                            label: 'SIMPLE_ALLOCATION_DATE_MODE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    SumMode: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_SUM_BOTH', value: 0 },
                            { name: 'SIMPLE_ALLOCATION_SUM_TOTAL', value: 1 },
                            { name: 'SIMPLE_ALLOCATION_SUM_YEAR', value: 2 },
                            { name: 'SIMPLE_ALLOCATION_SUM_NONE', value: 3 }],
                            label: 'SIMPLE_ALLOCATION_SUM_MODE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    ShowWorkCategoryTotals: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'SIMPLE_ALLOCATION_SHOW_WORK_CATEGORY_TOTALS_YES', value: 0 },
                            { name: 'SIMPLE_ALLOCATION_SHOW_WORK_CATEGORY_TOTALS_NO', value: 1 }],
                            label: 'SIMPLE_ALLOCATION_SHOW_WORK_CATEGORY_TOTALS',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    LockHistory: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'FALSE', value: 0 }, { name: 'TRUE', value: 1 }],
                            label: 'ALLOCATION_SIMPLE_LOCK_HISTORY',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    AllowMove: function () {
                        return {
                            type: 'select',
                            options: [{ name: 'ALLOCATION_SIMPLE_ALLOW_MOVE_0', value: 0 }
                                    , { name: 'ALLOCATION_SIMPLE_ALLOW_MOVE_1', value: 1 }
                                    , { name: 'ALLOCATION_SIMPLE_ALLOW_MOVE_2', value: 2 }],
                            label: 'ALLOCATION_SIMPLE_ALLOW_MOVE',
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    },
                    titleWidth: function () {
                        return {
                            type: 'integer',
                            label: "SIMPLE_ALLOCATION_TITLE_WIDTH"
                        };
                    },
                    competencyWidth: function () {
                        return {
                            type: 'integer',
                            label: "SIMPLE_ALLOCATION_COMPETENCY_WIDTH"
                        };
                    },
                    workCategoryWidth: function () {
                        return {
                            type: 'integer',
                            label: "SIMPLE_ALLOCATION_WORK_CATEGORY_WIDTH"
                        };
                    }
                }
            }
        });
	}
	
    SimpleMonthlyAllocationController.$inject = ['$scope', 'SimpleMonthlyAllocationService', 'SaveService', 'Common', 'MessageService', 'StateParameters', 'WorkCategoryList', 'CalendarService', '$q', 'Sums', 'ViewService'];
    function SimpleMonthlyAllocationController($scope, SimpleMonthlyAllocationService, SaveService, Common, MessageService, StateParameters, WorkCategoryList, CalendarService, $q, Sums, ViewService) {
        $scope.workCategories = WorkCategoryList;
        let processItemId = StateParameters.itemId;
        $scope.rows = {};
        $scope.year = new Date().getFullYear();
        $scope.selectedRows = [];
        $scope.months = [];
        $scope.allowYearChange = true;
        $scope.sums = Sums;
        
        if (angular.isDefined(StateParameters.UserMode)) {
            $scope.usermode = StateParameters.UserMode;
        }
        else {
            $scope.usermode = 0;
        }

        if (angular.isDefined(StateParameters.BaseCalendarId)) {
            $scope.baseCalendar = StateParameters.BaseCalendarId;
        }
        else {
            $scope.baseCalendar = 1;
        }

        if (angular.isDefined(StateParameters.DateMode)) {
            $scope.dateMode = StateParameters.DateMode;
        }
        else {
            $scope.dateMode = 0;
        }
        
        if (angular.isDefined(StateParameters.SumMode)) {
            $scope.sumMode = StateParameters.SumMode;
        }
        else {
            $scope.sumMode = 0;
        }

        if (angular.isDefined(StateParameters.ShowWorkCategoryTotals) && StateParameters.ShowWorkCategoryTotals == 1) {
            $scope.ShowWorkCategoryTotals = false;
        }
        else {
            $scope.ShowWorkCategoryTotals = true;
        }

        if (angular.isDefined(StateParameters.LockHistory)) {
            $scope.lockHistory = StateParameters.LockHistory;
        }
        else {
            $scope.lockHistory = 0;
        }

        if (angular.isDefined(StateParameters.AllowMove)) {
            $scope.allowMove = StateParameters.AllowMove;
        }
        else {
            $scope.allowMove = 0;
        }

        if (angular.isDefined(StateParameters.titleWidth)) {
            $scope.titleWidth = StateParameters.titleWidth;
        }
        else {
            $scope.titleWidth = 150;
        }

        if (angular.isDefined(StateParameters.competencyWidth)) {
            $scope.competencyWidth = StateParameters.competencyWidth;
        }
        else {
            $scope.competencyWidth = 200;
        }
        
        if (angular.isDefined(StateParameters.workCategoryWidth)) {
            $scope.workCategoryWidth = StateParameters.workCategoryWidth;
        }
        else {
            $scope.workCategoryWidth = 150;
        }

        $scope.lock_month = 0;

        let checkMonthLock = function () {
            if ($scope.lockHistory === 1) {
                if (StateParameters.DateMode == 1) {
                    if ($scope.year < new Date().getUTCFullYear()) {
                        $scope.lock_month = 53;
                    }
                    else if ($scope.year === new Date().getUTCFullYear()) {
                        $scope.lock_month = moment().isoWeek() - 1;
                    }
                    else {
                        $scope.lock_month = 0;
                    }
                }
                else {
                    if ($scope.year < new Date().getUTCFullYear()) {
                        $scope.lock_month = 12;
                    }
                    else if ($scope.year === new Date().getUTCFullYear()) {
                        $scope.lock_month = new Date().getUTCMonth();
                    }
                    else {
                        $scope.lock_month = 0;
                    }
				}
            }
        }
        checkMonthLock();

        let fetchedYears = [];
        $scope.workingDays = {};
        $scope.workingDaysInMonth = {};
        let changedItems = [];
        let newValuesArray = [];
        let changedRows = [];
        
        $scope.quarter = moment().quarter();

        let initData = function() {
            $scope.months = [];

            if (StateParameters.DateMode == 1) {
                let quarter_start;
                let quarter_end;
                if ($scope.quarter == 1) {
                    quarter_start = moment($scope.year + '-01-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-03-31', "YYYY-MM-DD");
                }
                else if ($scope.quarter == 2) {
                    quarter_start = moment($scope.year + '-04-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-06-30', "YYYY-MM-DD");
				}
                else if ($scope.quarter == 3) {
                    quarter_start = moment($scope.year + '-07-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-09-30', "YYYY-MM-DD");
                }
                else {
                    quarter_start = moment($scope.year + '-10-01', "YYYY-MM-DD");
                    quarter_end = moment($scope.year + '-12-31', "YYYY-MM-DD");
                }
                quarter_start.startOf('isoWeek').add(3, 'days');
                quarter_end.startOf('isoWeek').add(3, 'days');

                let quarter_date = quarter_start;
                while (quarter_date <= quarter_end) {
                    if (quarter_date.quarter() == $scope.quarter) {
                        let week = quarter_date.isoWeek()
                        $scope.months.push(week);
                    }
                    quarter_date.add(7, 'days');
				}
            }
            else {
                for (let i = 1; i <= 12; i++) {
                    $scope.months.push(i);
                }
            }
        }

        let initWorkDays = function (user_item_id) {
            if (StateParameters.DateMode == 1) {
                let quarter_start = moment($scope.year + '-01-01', "YYYY-MM-DD").startOf('isoWeek').add(3, 'days');
                let quarter_end = moment($scope.year + '-12-31', "YYYY-MM-DD").startOf('isoWeek').add(3, 'days');
                while (quarter_start <= quarter_end) {
                    let week = quarter_start.isoWeek();
                    if (typeof $scope.workingDaysInMonth[user_item_id] === 'undefined') {
                        $scope.workingDaysInMonth[user_item_id] = {};
                    }
                    if (typeof $scope.workingDaysInMonth[user_item_id][$scope.year] === 'undefined') {
                        $scope.workingDaysInMonth[user_item_id][$scope.year] = {};
                    }
                    $scope.workingDaysInMonth[user_item_id][$scope.year][week] = getWeekWorkDays($scope.year, user_item_id, week).length;
                    quarter_start.add(7, 'days');
                }
            }
            else {
                for (let m = 1; m <= 12; m++) {
                    if ($scope.workingDays) {
                        if (typeof $scope.workingDaysInMonth[user_item_id] === 'undefined') {
                            $scope.workingDaysInMonth[user_item_id] = {};
                        }
                        if (typeof $scope.workingDaysInMonth[user_item_id][$scope.year] === 'undefined') {
                            $scope.workingDaysInMonth[user_item_id][$scope.year] = {};
                        }
                        $scope.workingDaysInMonth[user_item_id][$scope.year][m] = $scope.workingDays[user_item_id][$scope.year][m].length;
                    }
                }
            }
        }

        let getAllocations = function () {
            SimpleMonthlyAllocationService.getAllocations(processItemId, $scope.year, StateParameters.DateMode, StateParameters.process).then(function (response) {
                angular.forEach(response.allocations, function (key) {
                    if (key.resource_item_id > 0)
                        key.resource_item_id = [key.resource_item_id];
                    else
                        key.resource_item_id = [];
                });

                $scope.rows[$scope.year] = response.allocations;
                $scope.calculateMonthTotal();

                getCalendarEvents();
            });
        }
        
        let getCalendarEvents = function () {
            let year_start = $scope.year;
            let year_end = $scope.year;
            if (StateParameters.DateMode == 1) {
                year_start--;
                year_end++;
			}
            if (fetchedYears.indexOf('0_' + $scope.year) === -1) {
                let sd = new Date(year_start + '-01-01');
                let ed = new Date(year_end + '-12-31');
                fetchedYears.push('0_' + $scope.year);

                CalendarService.getBaseCalendar($scope.baseCalendar, sd, ed).then(function (dates) {
                    if (typeof $scope.workingDays[0] === 'undefined') {
                        $scope.workingDays[0] = {};
                    }

                    for (let year = year_start; year <= year_end; year++) {
                        let yearCalendar = dates[year];

                        if (typeof $scope.workingDays[0][year] === 'undefined') {
                            $scope.workingDays[0][year] = {};

                            angular.forEach(yearCalendar, function (month, m) {
                                if (typeof $scope.workingDays[0][year][m] === 'undefined') {
                                    $scope.workingDays[0][year][m] = [];

                                    angular.forEach(month, function (day, d) {
                                        if (Common.isFalse(day.notWork)) {
                                            $scope.workingDays[0][year][m].push({ index: d });
                                        }
                                    });
                                }
                            });
                        }
                    }

                    initWorkDays(0);
                });
            }

            if ($scope.usermode === 1) {
                angular.forEach($scope.rows[$scope.year], function (row) {
                    if (fetchedYears.indexOf(row.user_item_id + '_' + $scope.year) === -1) {
                        let sd = new Date(year_start + '-01-01');
                        let ed = new Date(year_end + '-12-31');
                        fetchedYears.push(row.user_item_id + '_' + $scope.year);

                        CalendarService.getUserCalendar(row.user_item_id, sd, ed).then(function (dates) {
                            if (typeof $scope.workingDays[row.user_item_id] === 'undefined') {
                                $scope.workingDays[row.user_item_id] = {};
                            }

                            for (let year = year_start; year <= year_end; year++) {
                                let yearCalendar = dates[year];

                                if (typeof $scope.workingDays[row.user_item_id][year] === 'undefined') {
                                    $scope.workingDays[row.user_item_id][year] = {};

                                    angular.forEach(yearCalendar, function (month, m) {
                                        if (typeof $scope.workingDays[row.user_item_id][year][m] === 'undefined') {
                                            $scope.workingDays[row.user_item_id][year][m] = [];

                                            angular.forEach(month, function (day, d) {
                                                if (Common.isFalse(day.notWork)) {
                                                    $scope.workingDays[row.user_item_id][year][m].push({ index: d });
                                                }
                                            });
                                        }
                                    });
                                }
                            }

                            initWorkDays(row.user_item_id);
                        });
                    }
                });
            }
        };

        $scope.totals = {};

        $scope.selectAllRows = function () {
            _.each($scope.rows[$scope.year], function (row) {
                if ($scope.selectedRowsAll) {
                    $scope.selectedRows[row.item_id] = $scope.selectedRowsAll;
                } else {
                    delete $scope.selectedRows[row.item_id];
                }
            });
        };

        let addAllocation = function () {
            if (angular.isUndefined($scope.rows[$scope.year])) $scope.rows[$scope.year] = [];

            if (angular.isUndefined($scope.totals['item'][$scope.year])) {
                $scope.totals['item'][$scope.year] = {};
            }
            
            let allocation = {
                process_item_id: processItemId,
                title: 'New allocation',
                resource_type: '',
                resource_item_id: 0,
                user_item_id: 0
            };
            
            if (StateParameters.usermode == 0) {
                allocation.resource_type = 'competency';
            }
            else {
                allocation.resource_type = 'user';
            }
            
            return SimpleMonthlyAllocationService.insertAllocation(allocation).then(function(data) {
                if (data.resource_item_id > 0) {
                    data.resource_item_id = [data.resource_item_id];
                }
                else {
                    data.resource_item_id = [];
                }
                
                $scope.rows[$scope.year].push(data);
            });
        };

        $scope.moveAllocation = function () {
            let move = function () {
                let shifts = [];
                angular.forEach($scope.selectedRows, function (value, item_id) {
                    if (value) {
                        let shift = {
                            process_item_id: processItemId,
                            date_mode: $scope.dateMode,
                            shift: data.shift,
                            move_history: data.history[0],
                            allocation_item_id: item_id
                        };
                        shifts.push(shift);
                    }
                });
                SimpleMonthlyAllocationService.moveAllocations(processItemId, $scope.year, StateParameters.DateMode, StateParameters.process, shifts).then(function (response) {
                    angular.forEach(response.allocations, function (key) {
                        if (key.resource_item_id > 0)
                            key.resource_item_id = [key.resource_item_id];
                        else
                            key.resource_item_id = [];
                    });

                    $scope.rows[$scope.year] = response.allocations;
                    $scope.calculateMonthTotal();

                    getCalendarEvents();
                });
            };

            let shift_label = '';
            if ($scope.dateMode === 0) {
                shift_label = 'ALLOCATION_SIMPLE_MOVE_MONTHS';
            }
            else {
                shift_label = 'ALLOCATION_SIMPLE_MOVE_WEEKS';
            }

            let content = {
                title: 'ALLOCATION_SIMPLE_MOVE_TITLE',
                inputs: [{ type: 'integer', label: shift_label, model: 'shift' }],
                buttons: [{ text: 'CANCEL' }, { type: 'primary', onClick: move, text: 'ALLOCATION_SIMPLE_MOVE_BUTTON' }]
            };

            let history_value = [0];

            if ($scope.allowMove === 1) {
                history_value = [1];
                let history_button = { type: 'select', label: 'ALLOCATION_SIMPLE_MOVE_HISTORY', model: 'history', options: [{ name: 'FALSE', value: 0 }, { name: 'TRUE', value: 1 }], optionName: 'name', optionValue: 'value' };
                content.inputs.push(history_button);
            }

            let data = { shift: 0, history: history_value };

            MessageService.dialog(content, data);
        };

        $scope.deleteAllocation = function () {
            MessageService.confirm('ALLOCATION_SIMPLE_DELETE_CONFIRM', function () {
                let ids = [];
                angular.forEach($scope.selectedRows, function (value, item_id) {
                    if (value) ids.push(item_id);
                });
                let deleteIds = Common.convertArraytoString(ids);
                SimpleMonthlyAllocationService.deleteAllocation(deleteIds).then(function () {
                    angular.forEach($scope.selectedRows, function (value, item_id) {
                        if (value) {
                            let index = _.findIndex($scope.rows[$scope.year], { 'item_id': item_id });
                            $scope.rows[$scope.year].splice(index, 1);
                        }
                    });
                    delete $scope.selectedRowsAll;
                    $scope.selectedRows = [];
                    $scope.calculateMonthTotal();
                });
            });
        };

        $scope.hasSelectedRows = function () {
            let result = false;
            angular.forEach($scope.selectedRows, function (value) {
                if (value) {
                    result = true;
                }
            });
            return result;
        };

        $scope.header = {
            title: StateParameters.title,
            icons: [
                {
                    icon: 'add',
                    onClick: addAllocation,
                    name: 'ALLOCATION_SIMPLE_ADD'
                }
            ]
        };

        ViewService.footbar(StateParameters.ViewId,
            {
                template: 'features/simpleAllocation/simpleAllocationFooter.html'
            });

	    $scope.changeYear = function (type) {
	        if (!$scope.allowYearChange) {
	            return;
            }
            if (type == '-') {
                $scope.year -= 1;
            }
            else if (type == '+') {
                $scope.year += 1;
            }

            if (StateParameters.DateMode == 1 || $scope.months.length == 0) {
                initData();
            }

            getAllocations();

            if (StateParameters.DateMode == 1) {
                SimpleMonthlyAllocationService.getSums(StateParameters.itemId, $scope.year, StateParameters.process).then(function (sums) {
                    $scope.sums = sums;
                });
            }

			getCalendarEvents();

            checkMonthLock();

            if (angular.isUndefined($scope.totals['item']) || angular.isUndefined($scope.totals['item'][$scope.year]) || angular.isUndefined($scope.totals['total']) || angular.isUndefined($scope.totals['total'][$scope.year])) {
                $scope.calculateMonthTotal();
            }
        };

        $scope.changeQuarter = function (type) {
            if (!$scope.allowYearChange) {
                return;
            }
            if (type == '-') {
                $scope.quarter -= 1;
            }
            else if (type == '+') {
                $scope.quarter += 1;
            }

            let year_changed = false;
            if ($scope.quarter == 0) {
                $scope.year--;
                $scope.quarter = 4;
                year_changed = true;
            }
            if ($scope.quarter == 5) {
                $scope.year++;
                $scope.quarter = 1;
                year_changed = true;
            }

            initData();
            
            if (year_changed) {
                getAllocations();

                if (StateParameters.DateMode == 1) {
                    SimpleMonthlyAllocationService.getSums(StateParameters.itemId, $scope.year, StateParameters.process).then(function (sums) {
                        $scope.sums = sums;
                    });
                }
            }

            getCalendarEvents();

            checkMonthLock();

            if (angular.isUndefined($scope.totals['item']) || angular.isUndefined($scope.totals['item'][$scope.year]) || angular.isUndefined($scope.totals['total']) || angular.isUndefined($scope.totals['total'][$scope.year])) {
                $scope.calculateMonthTotal();
            }
        };

        $scope.calculateMonthTotal = function (params) {
            $scope.allowYearChange = false;
            $scope.totals['item'] = {};
            $scope.totals['total'] = {};
            $scope.totals['total2'] = {};

            let workCategoryTotals = {}; 
            let workCategoryTotals2 = {};
            
            $scope.workCategoriesSelected = [];
            
            angular.forEach($scope.rows[$scope.year], function (key) {
                
                if (angular.isUndefined($scope.totals['item'][$scope.year])) $scope.totals['item'][$scope.year] = {};
                if (angular.isUndefined($scope.totals['item'][$scope.year][key.item_id])) $scope.totals['item'][$scope.year][key.item_id] = 0;
                if (angular.isUndefined($scope.totals['total'][$scope.year])) $scope.totals['total'][$scope.year] = {};
                if (angular.isUndefined($scope.totals['total'][$scope.year]['total'])) $scope.totals['total'][$scope.year]['total'] = 0;
                if (angular.isUndefined($scope.totals['total2'][$scope.year])) $scope.totals['total2'][$scope.year] = {};
                if (angular.isUndefined($scope.totals['total2'][$scope.year]['total'])) $scope.totals['total2'][$scope.year]['total'] = 0;

                if (angular.isDefined(key['month'])) {
                    angular.forEach(key['month'], function (key2, value2) {
                        if (angular.isUndefined($scope.totals['total'][$scope.year][value2])) $scope.totals['total'][$scope.year][value2] = 0;
                        if (angular.isUndefined($scope.totals['total2'][$scope.year][value2])) $scope.totals['total2'][$scope.year][value2] = 0;

                        $scope.totals['total'][$scope.year][value2] += parseFloat(key2);
                        $scope.totals['total'][$scope.year]['total'] += parseFloat(key2);
                        $scope.totals['item'][$scope.year][key.item_id]  += parseFloat(key2);

                        if (angular.isDefined(key.work_category_item_id) && key.work_category_item_id > 0) {
                            $scope.totals['total2'][$scope.year][value2] += parseFloat(key2);
                            $scope.totals['total2'][$scope.year]['total'] += parseFloat(key2);
                        }
                    });      
                }

                if ($scope.workCategories.length > 0 && angular.isDefined(key.work_category_item_id)) {
                    if (angular.isUndefined(workCategoryTotals[key.work_category_item_id])) {
                        workCategoryTotals[key.work_category_item_id] = 0;
                    }
                    if (angular.isUndefined(workCategoryTotals2[key.work_category_item_id])) {
                        workCategoryTotals2[key.work_category_item_id] = {};
                    }

                    angular.forEach(key['month'], function (key2, value2) {
                        if (angular.isUndefined( workCategoryTotals2[key.work_category_item_id][value2]) ) {
                          workCategoryTotals2[key.work_category_item_id][value2] = 0;
                        }
                        workCategoryTotals2[key.work_category_item_id][value2] += key2;
                    });
                    
                    workCategoryTotals[key.work_category_item_id] += $scope.totals['item'][$scope.year][key.item_id];
                }
                
                if (key.work_category_item_id > 0 && $scope.workCategoriesSelected.indexOf(key.work_category_item_id) == -1) {
                    $scope.workCategoriesSelected.push(key.work_category_item_id);
                }
            });
            if (typeof params !== 'undefined') {
                $scope.changeValue(params);
            }
            
            $scope.workCategoryTotals = workCategoryTotals;
            $scope.workCategoryTotals2 = workCategoryTotals2;
            $scope.allowYearChange = true;
        }
        
        $scope.formatNumber = function(value) {
            if (typeof value === 'undefined') value = 0;
            return Common.formatNumber(value);
        }
        
        SaveService.subscribeSave($scope, function () {
            saveData();
        });

        let saveData = function () {
            let promises = [];
            if (changedItems.length > 0) {
                let saveArray = [];
                for(let i = 0; i < changedItems.length; i++) {
                    let row = changedItems[i];
                    if (StateParameters.DateMode == 1) {
                        let monthsArray = {};
                        angular.forEach(row.months, function (value, week) {
                            let weekWorkDays = getWeekWorkDays(row.year, row.user_item_id, week);
                            let dailyEffort = value / weekWorkDays.length;
                            if (angular.isUndefined(monthsArray[row.item_id])) {
                                monthsArray[row.item_id] = {};
                            }
                            angular.forEach(weekWorkDays, function (weekDate) {
                                let datesArr = weekDate.split('-');
                                if (angular.isUndefined(monthsArray[row.item_id][datesArr[1]])) {
                                    monthsArray[row.item_id][datesArr[1]] = [];
                                }
                                monthsArray[row.item_id][datesArr[1]].push({ year: datesArr[0], day: datesArr[2], hours: dailyEffort });
                            });
                        });
                        angular.forEach(monthsArray, function (month_data, allocation_item_id) {
                            angular.forEach(month_data, function (day_data, month) {
                                saveArray.push({ allocation_item_id: allocation_item_id, year: row.year, month: month, dates: day_data });
                            });
                        });
                    }
                    else {
                        angular.forEach(row['months'], function (key, value) {
                            let datesArray = [];
                            for (let j = 0; j < $scope.workingDays[row.user_item_id][row.year][value].length; j++) {
                                let hour = (key / $scope.workingDays[row.user_item_id][row.year][value].length);
                                datesArray.push({ year: row.year, day: $scope.workingDays[row.user_item_id][row.year][value][j].index, hours: hour });
                            }
                            saveArray.push({ allocation_item_id: row.item_id, year: row.year, month: value, dates: datesArray });
                        });
                    }
                }

                let p = SimpleMonthlyAllocationService.saveAllocationHours(StateParameters.DateMode, saveArray).then(function() {
                    changedItems = [];
                    newValuesArray = [];
                });     
                promises.push(p);
            }

            if (changedRows.length > 0) {
                let p2 = SimpleMonthlyAllocationService.updateAllocation(changedRows).then(function() {
                    changedRows = [];
                });
                promises.push(p2);
            }
            return $q.all(promises).then(function(allPromisesResolved) {
                return SimpleMonthlyAllocationService.getSums(StateParameters.itemId, $scope.year, StateParameters.process).then(function (sums) {
                    $scope.sums = sums;
                    $scope.allowYearChange = true;
                });
            });
        }
        
        let getWeekWorkDays = function (year, user_item_id, week) {
            let weekDays = [];
            let week_start = moment().year(year).isoWeek(week).startOf('isoWeek');
            let week_end = moment().year(year).isoWeek(week).endOf('isoWeek');
            while (week_start <= week_end) {
                if ($scope.workingDays[user_item_id]) {
                    if ($scope.workingDays[user_item_id][week_start.year()]) {
                        if ($scope.workingDays[user_item_id][week_start.year()][week_start.month() + 1]) {
                            if ($scope.workingDays[user_item_id][week_start.year()][week_start.month() + 1].filter(x => x.index == week_start.date()).length > 0) {
                                weekDays.push(week_start.year() + '-' + (week_start.month() + 1) + '-' + (week_start.date()));
                            }
                        }
                    }
                }
                week_start.add(1, 'days');
            }
            return weekDays;
        }

        $scope.changeValue = function (params) {
            newValuesArray.push({ item_id: params.item_id, months: params.months, year: params.year, user_item_id: params.user_item_id });
            let noDuplicateObjects = {};
            let l = newValuesArray.length;

            for (let i = 0; i < l; i++) {
                let new_item = newValuesArray[i];
                noDuplicateObjects[new_item.item_id + '_' + new_item.year] = new_item;
            }

            let nonDuplicateArray = [];

            for (let item in noDuplicateObjects) {
                nonDuplicateArray.push(noDuplicateObjects[item]);
            }

            changedItems = nonDuplicateArray;
        };

        $scope.changeAllocation = function (data) {
            let row = angular.copy(data);
            if (typeof row !== 'undefined') {
                if (angular.isDefined(row.resource_item_id) && row.resource_item_id.length > 0) {

                    if (StateParameters.usermode == 0) {
                        row.resource_type = 'competency';
                        row.user_item_id = 0;
                        data.user_item_id = 0;
                    }
                    else {
                        row.resource_type = 'user';
                        row.user_item_id = row.resource_item_id[0];
                        data.user_item_id = row.user_item_id;
                    }
                    row.resource_item_id = row.resource_item_id[0];
                }
                else {
                    row.resource_item_id = 0;
                    row.user_item_id = 0;
                    data.user_item_id = 0;
                }
                let index = _.findIndex(changedRows, { item_id: row.item_id });
                if (index != -1)
                    changedRows[index] = row;
                else
                    changedRows.push(row);

                SaveService.tick();
            }

            $scope.calculateMonthTotal(data.item_id, data.month, row.user_item_id);

            getCalendarEvents();
        };
        
        $scope.changeYear();

        let workCategoriesDict = {};
        angular.forEach($scope.workCategories, function (key, value) {
            workCategoriesDict[key.item_id] = key;
        });
        $scope.workCategoriesDict = workCategoriesDict;
    }

    SimpleMonthlyAllocationService.$inject = ['ApiService'];
    function SimpleMonthlyAllocationService(ApiService) {
        let api = ApiService.v1api('resources/simple/allocations');

        this.getAllocations = function (processItemId, year, mode, process) {
            return api.get([processItemId, year, mode, process, '0', 0]);
	    };
    	
    	this.insertAllocation = function (allocationItem) {
            return api.new('', allocationItem).then(function(data) {
                return data;
            });
	    };
        
        this.updateAllocation = function (allocationItem) {
            return api.save('', allocationItem).then(function(data) {
                return data;
            });
        };

        this.deleteAllocation = function (allocationItemIds) {
            return api.delete(allocationItemIds);
	    };

        this.saveAllocationHours = function (mode, allocationHours) {
            return api.save(['hours', mode], allocationHours);
        };

        this.getSums = function (itemId, year, process) {
            return api.get(['sum', itemId, year, process, '0']);
        };

        this.moveAllocations = function (processItemId, year, mode, process, shifts) {
            return api.save(['move', processItemId, year, mode, process, '0', 0], shifts);
        };
    }
})();