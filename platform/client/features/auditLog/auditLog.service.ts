﻿(function () {
	'use strict';

	angular
        .module('core.pages')
		.service('AuditLogService', AuditLogService);

	AuditLogService.$inject = ['$rootScope', 'ApiService'];
	function AuditLogService($rootScope, ApiService) {
	    let self = this;
		let audit = ApiService.v1api('features/AuditRest');
		let status = ApiService.v1api('features/AuditRest/status');
		
		self.getLogs = function (process, item_id, count, params) {
			return audit.new([process, item_id, count], params);
		};

        self.getLogsWithLimit = function (process, item_id, count, limit, params) {
            return audit.new([process, item_id, count, limit], params);
        };
        self.getChanges = function(process, item_id, params) {
	        return audit.new([process, item_id], params);
        };
        self.getUsers = function (process, item_id) {
			return audit.get([process, item_id]);
		};
		self.getCount = function (process, item_id) {
			return status.get([process, item_id, 0]);
		};
		self.getStatus = function (process, item_id) {
			return status.get([process, item_id, 1]);
		};
	}
})();