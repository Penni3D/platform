﻿(function () {
	'use strict';

	angular
		.module('core.features')
		.config(AuditLogConfig)
		.controller('AuditLogController', AuditLogController);

	AuditLogConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function AuditLogConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('process.AuditLog', {
			defaults: {
				history_in_days: 30,
                history_limit: 10
			},
			tabs: {
				default: {
					view_mode: function (resolves) {
						var cols = [
							{ Name: "Simple", Translation: "Simple" },
							{ Name: "Detailed", Translation: "Detailed" }
						];
						return {
							type: 'select',
							options: cols,
							label: "View mode",
							optionValue: 'Name',
							optionName: 'Translation'
						}
					},
					history_in_days: function (resolves) {
						return {
							type: 'integer',
							label: "AUDIT_LOG_NUMBEROFDAYS"
						}
					},
                    history_limit: function (resolves) {
                        return {
                            type: 'integer',
                            label: "AUDIT_LOG_NUMBEROFROWS"
                        }
                    },
					add_process_name_1: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_NAME"
						}
					},
					add_process_parent_field_1: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_PARENT_FIELD"
						}
					},
					add_process_name_2: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_NAME"
						}
					},
					add_process_parent_field_2: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_PARENT_FIELD"
						}
					},
					add_process_name_3: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_NAME"
						}
					},
					add_process_parent_field_3: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_PARENT_FIELD"
						}
					},
					add_process_name_4: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_NAME"
						}
					},
					add_process_parent_field_4: function (resolves) {
						return {
							type: 'string',
							label: "AUDIT_LOG_ADDITIONAL_PROCESS_PARENT_FIELD"
						}
					}
				}
			}
		});

		ViewsProvider.addView('process.AuditLog', {
			controller: 'AuditLogController',
			template: 'features/auditLog/auditLog.html',
			resolve: {
				InitialAuditUsers: function (AuditLogService, StateParameters) {
					return AuditLogService.getUsers(StateParameters.process, StateParameters.itemId);
				},
				InitialAuditChanges: function (AuditLogService, StateParameters) {
					var p1 = StateParameters.add_process_name_1;
					var f1 = StateParameters.add_process_parent_field_1;

					var p2 = StateParameters.add_process_name_2;
					var f2 = StateParameters.add_process_parent_field_2;

					var p3= StateParameters.add_process_name_3;
					var f3 = StateParameters.add_process_parent_field_3;

					var p4 = StateParameters.add_process_name_4;
					var f4 = StateParameters.add_process_parent_field_4;

					var p = [{Key: p1, Value: f1}, {Key: p2, Value: f2}, {Key: p3, Value: f3} , {Key: p4, Value: f4}];
					return AuditLogService.getChanges(StateParameters.process, StateParameters.itemId, p);
				}
			}
		});

		ViewsProvider.addFeature('process.AuditLog', 'AuditLog', "FEATURE_AUDITLOG", ['read']);
	}

	AuditLogController.$inject = ['$scope', 'AuditLogService', 'InitialAuditUsers', 'InitialAuditChanges', 'CalendarService', 'Translator', 'StateParameters','$interval'];
	function AuditLogController($scope, AuditLogService, InitialAuditUsers, InitialAuditChanges, CalendarService, Translator, StateParameters, $interval) {
		$scope.loading = true;
		$scope.users = _.orderBy(InitialAuditUsers, ['occurence'], ['desc']);
		$scope.changes = InitialAuditChanges;
		
		$scope.formatDate = function (d) {
			return CalendarService.formatDate(d) + ' ' + moment(d).format('HH:mm');
		};
		$scope.formatProcess = function (d) {
			return Translator.translate(d);
		};
		$scope.formatChange = function (v) {                              
			if (v.indexOf('>>') > -1 && v.indexOf('<<') > -1) return $scope.formatDate(v.replace('>>','').replace('<<',''));
			return v.replace(/,\s*$/, "");
		};
		$scope.formatUser = function (d) {
			if (!d.length > 0) return "System";
			return d;
		};
		$scope.formatProgress = function(c,s) {
			if (s == 0 && c == 0) return "";
			return Math.round(s / c* 100,0) + "%";	
		};
		$scope.header = {
			title: Translator.translate('AUDITLOG') + Translator.translate('AUDITLOG_LAST') + StateParameters.history_in_days + Translator.translate('AUDITLOG_DAYS') + StateParameters.history_limit + Translator.translate('AUDITLOG_ROWS')
		};

		var p1 = StateParameters.add_process_name_1;
		var f1 = StateParameters.add_process_parent_field_1;
		
		var p2 = StateParameters.add_process_name_2;
		var f2 = StateParameters.add_process_parent_field_2;

		var p3= StateParameters.add_process_name_3;
		var f3 = StateParameters.add_process_parent_field_3;

		var p4 = StateParameters.add_process_name_4;
		var f4 = StateParameters.add_process_parent_field_4;

		var p = [{Key: p1, Value: f1}, {Key: p2, Value: f2}, {Key: p3, Value: f3} , {Key: p4, Value: f4}];

		$scope.simple = StateParameters.view_mode == 'Simple';	

		$scope.status = 0;
		$scope.count = 0;


		
		
		$scope.checkStatus = function() {
			if (!$scope.simple) {
			AuditLogService.getStatus(StateParameters.process, StateParameters.itemId).then(function (result) {
				$scope.status = result;
				if (!$scope.loading) $interval.cancel(interval);
			});
			}
		};			
		

		var interval = $interval( function(){$scope.checkStatus(); }, 500);
		//Get these after loading
		AuditLogService.getCount(StateParameters.process, StateParameters.itemId).then(function (result) {
			$scope.count = result;
			if ($scope.loading) {
				//$scope.checkStatus();
			}
		});

		if (!$scope.simple) {
			AuditLogService.getLogsWithLimit(StateParameters.process, StateParameters.itemId, StateParameters.history_in_days, StateParameters.history_limit, p).then(function (response) {
				$scope.data = _.orderBy(response, ['archive_date'], ['desc']);
				$scope.loading = false;
			});
		}

	}

})();