﻿(function () {
	'use strict';

	angular
		.module('core.features')
		.config(HistoryEditorConfig)
		.service('HistoryEditorService', HistoryEditorService)
		.controller('HistoryEditorController', HistoryEditorController);

	HistoryEditorConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function HistoryEditorConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('process.historyEditor', {
			onSetup: {
				ItemColumns: function (ProcessService, StateParameters, Common) {
					return ProcessService.getColumns(StateParameters.process, true).then(function (columns) {
						let c = [];

						_.each(columns, function (column) {
							let dt = Common.getRelevantDataType(column);
							if (dt == 5 || dt == 6) {
								column.dataType = dt;
								c.push(column);
							}
						});

						return c;
					});
				}
			},
			tabs: {
				default: {
					targetColumnId: function (resolves) {
						return {
							options: resolves.ItemColumns,
							type: 'multiselect',
							label: 'FIELD',
							optionValue: 'ItemColumnId',
							optionName: 'LanguageTranslation'
						}
					}
				}
			}
		});
		ViewsProvider.addView('process.historyEditor', {
			controller: 'HistoryEditorController',
			template: 'features/historyEditor/historyEditor.html',
			resolve: {
				ItemColumns: function (StateParameters, ProcessService) {
					let r = [];
					return ProcessService.getColumns(StateParameters.process, true).then(function (cols) {
						for (let id of StateParameters.targetColumnId) {
							_.each(cols, function (col) {
								if (col.ItemColumnId == id) {
									r.push(col);
									return;
								}
							});
						}
						return r;
					});
				}
			},
			afterResolve: {
				Options: function (ProcessService, ItemColumns) {
					let result = {};
					for (let c of ItemColumns) {
						ProcessService.getListItems(c.DataAdditional, true).then(function (items) {
							result[c.ItemColumnId] = items;
						});
					}
					return result;
				}
			}
		});

		ViewsProvider.addFeature('process.historyEditor', 'HISTORY_EDITOR', "FEATURE_HISTORY_EDITOR", ['read', 'write']);
	}

	HistoryEditorService.$inject = ['ApiService'];

	function HistoryEditorService(ApiService) {
		let self = this;

		let history = ApiService.v1api('features/HistoryEditorRest');

		self.removeFromHistory = function (itemId, itemColumnId, date) {
			return history.delete([itemId, itemColumnId, date]);
		}

		self.updateHistory = function (itemId, itemColumnId, archiveId, date1, date2, newValue) {
			return history.save([itemId, itemColumnId, archiveId, date1, date2, newValue]);
		}

		self.addHistory = function (itemId, itemColumnId, date, newValue) {
			return history.new([itemId, itemColumnId, date, newValue]);
		}

		self.getItemColumnHistory = function (itemId, itemColumnId) {
			return history.get(['process', itemId, itemColumnId]).then(function (respond) {
				return respond;
			});
		};
	}

	HistoryEditorController.$inject = [
		'$rootScope',
		'$scope',
		'StateParameters',
		'States',
		'HistoryEditorService',
		'ProcessService',
		'MessageService',
		'CalendarService',
		'Translator',
		'Options',
		'ItemColumns',
		'ViewService'
	];

	function HistoryEditorController(
		$rootScope,
		$scope,
		StateParameters,
		States,
		HistoryEditorService,
		ProcessService,
		MessageService,
		CalendarService,
		Translator,
		Options,
		ItemColumns,
		ViewService
	) {

		$scope.header = {
			title: StateParameters.title,
		};

		$scope.mode = 0;
		$scope.selectedChange = undefined;
		$scope.targetColumnOptions = [];
		$scope.targetColumn = undefined;
		$scope.newValue = [];
		$scope.newDate = undefined;
		$scope.changeDateTo = undefined;
		$scope.changeDateFrom = undefined;
		$scope.options = Options;

		_.each(ItemColumns, function (i) {
			$scope.targetColumnOptions.push({name: Translator.translate(i.DataAdditional), value: i.ItemColumnId});
		});

		$scope.fd = function (d) {
			return CalendarService.formatDateTime(moment(d).toDate(), undefined, true, true);
		}
		$scope.getType = function (type: number): string {
			if (type == 2) return Translator.translate("ARCHIVE_TYPE_2");
			if (type == 0) return Translator.translate("ARCHIVE_TYPE_0");
			return Translator.translate("ARCHIVE_TYPE_1");
		}
		$scope.getRows = function () {
			HistoryEditorService.getItemColumnHistory(StateParameters.itemId, $scope.targetColumn).then(function (result) {
				$scope.changes = result;
			});
		}
		$scope.changeTargetColumn = function () {
			$scope.newValue = [];
			$scope.selectedChange = undefined
			$scope.getRows();
		}
		$scope.selectChange = function (change) {
			if (change.ArchiveType == 0) {
				$scope.selectedChange = undefined;
				$scope.mode = 0;
				return;
			}
			$scope.selectedChange = change;
			$scope.newValue = [];
			$scope.newValue.push(change.SelectedItemId);
			$scope.changeDateFrom = change.Start;
			$scope.changeDateTo = change.Date;
			$scope.mode = 1;
		}
		$scope.selectAdd = function () {
			$scope.mode = 2;
		}
		$scope.removeArchive = function (date) {
			MessageService.confirm('ARCHIVE_CONFIRM_DELETE', function () {
				HistoryEditorService.removeFromHistory(StateParameters.itemId, $scope.targetColumn, date).then(function () {
					$scope.getRows();
				});
			});
		}
		$scope.updateArchive = function (nv, nd1, nd2) {
			MessageService.confirm('ARCHIVE_CONFIRM_CHANGE', function () {
				$scope.mode = 0;
				HistoryEditorService.updateHistory(StateParameters.itemId, $scope.targetColumn, $scope.selectedChange.ArchiveId , moment(nd1).format("YYYY-MM-DD"), moment(nd2).format("YYYY-MM-DD"), nv[0]).then(function () {
					$scope.getRows();
				});
			});
		}
		$scope.addArchive = function (nd, nv) {
			MessageService.confirm('ARCHIVE_CONFIRM_ADDITION', function () {
				$scope.mode = 0;
				HistoryEditorService.addHistory(StateParameters.itemId, $scope.targetColumn, moment(nd).format("YYYY-MM-DD"), nv).then(function () {
					$scope.getRows();
				});
			});
		}
		ViewService.footbar(StateParameters.ViewId, {
			template: 'features/historyEditor/historyEditor-footbar.html',
			scope: $scope
		});

	}
})();
