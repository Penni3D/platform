(function () {
	'use strict';

	angular
		.module('core.features')
		.config(UserGroupInfoConfig)

	UserGroupInfoConfig.$inject = ['ViewsProvider'];
	function UserGroupInfoConfig(ViewsProvider) {
		ViewsProvider.addView('process.userGroupInfo', {
			controller: 'UserGroupInfoController',
			template: 'pages/userGroupInfo/userGroupInfo.html',
			resolve: {
				UserGroups: function (ApiService, StateParameters) {
					return ApiService.v1api('userGroupInfo/userGroups').get([StateParameters.process, StateParameters.itemId]).then(function (userGroups) {
						return userGroups;
					});
				},
				Access: function(StateParameters, States) {
					if (angular.isDefined(States.userGroupInfo) && States.userGroupInfo.groupStates[StateParameters.processGroupId] !== 'undefined') {
						if (States.userGroupInfo.groupStates[StateParameters.processGroupId].s.indexOf("read") > -1)
							return true;
					}
					return false;
				}
			}
		});
		
		ViewsProvider.addFeature('process.userGroupInfo', 'userGroupInfo', "FEATURE_USER_GROUP_INFO", ['read', 'conditions']);
	}
})();