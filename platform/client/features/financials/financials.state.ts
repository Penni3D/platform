﻿(function () {
	'use strict';

	angular
		.module('core.features')
		.config(FinancialsConfig)
		.controller('FinancialsController', FinancialsController);

	FinancialsConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function FinancialsConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('process.financials', {
			onSetup: {
				Dashboards: function (SettingsService, StateParameters) {
					return SettingsService.ProcessDashboards(StateParameters.process);
				},
				BudgetRowPortfolios: function (PortfolioService) {
					return PortfolioService.getPortfolios('budget_row');
				}
			},
			defaults: {
				scope: 0,
				periods: 5,
				show_row_totals: 0,
				show_column_totals: 3,
				offset: 0,
				one_level_mode: 0,
				hide_titles: 0
			},
			tabs: {
				default: {
					scope: function () {
						return {
							type: 'select',
							options: [{value: 0, name: "FINANCIALS_CONFIG_SCOPE_YEAR"}
								, {value: 1, name: "FINANCIALS_CONFIG_SCOPE_QUARTAL"}
								, {value: 2, name: "FINANCIALS_CONFIG_SCOPE_MONTH"}],
							label: 'FINANCIALS_CONFIG_SCOPE',
							optionValue: 'value',
							optionName: 'name'
						};
					},
					periods: function () {
						return {
							type: 'integer',
							label: 'FINANCIALS_CONFIG_PERIODS'
						}
					},
					offset: function () {
						return {
							type: 'integer',
							label: 'FINANCIALS_CONFIG_OFFSET'
						}
					},
					show_row_totals: function () {
						return {
							type: 'select',
							options: [{value: 0, name: "FINANCIALS_CONFIG_SHOW_TOTAL_NONE"}
								, {value: 1, name: "FINANCIALS_CONFIG_SHOW_TOTAL_YEAR"}
								, {value: 2, name: "FINANCIALS_CONFIG_SHOW_TOTAL_TOTAL"}
								, {value: 3, name: "FINANCIALS_CONFIG_SHOW_TOTAL_BOTH"}],
							label: 'FINANCIALS_CONFIG_SHOW_TOTAL',
							optionValue: 'value',
							optionName: 'name'
						};
					},
					show_column_totals: function () {
						return {
							type: 'select',
							options: [{ value: 0, name: "FINANCIALS_CONFIG_SHOW_TOTALS_NONE" }
								, { value: 1, name: "FINANCIALS_CONFIG_SHOW_TOTALS_CUMULATIVE" }
								, { value: 2, name: "FINANCIALS_CONFIG_SHOW_TOTALS_TOTAL" }
								, { value: 3, name: "FINANCIALS_CONFIG_SHOW_TOTALS_BOTH" }],
							label: 'FINANCIALS_CONFIG_SHOW_TOTALS',
							optionValue: 'value',
							optionName: 'name'
						};
					},
					fieldsPortfolioId: function (resolves) {
						return {
							label: 'FINANCIALS_CONFIG_FIELDS_PORTFOLIO',
							type: 'select',
							options: resolves.BudgetRowPortfolios,
							optionValue: 'ProcessPortfolioId',
							optionName: 'LanguageTranslation'
						};
					},
					one_level_mode: function () {
						return {
							type: 'select',
							options: [{ value: 0, name: "FALSE" }
								, { value: 1, name: "TRUE" }],
							label: 'FINANCIALS_CONFIG_ONE_LEVEL_MODE',
							optionValue: 'value',
							optionName: 'name'
						};
					},
					hide_titles: function () {
						return {
							type: 'select',
							options: [{ value: 0, name: "FALSE" }
								, { value: 1, name: "TRUE" }],
							label: 'FINANCIALS_CONFIG_HIDE_TITLES',
							optionValue: 'value',
							optionName: 'name'
						};
					},
					show_last_modified: function () {
						return {
							type: 'select',
							options: [{ value: 0, name: "FINANCIALS_CONFIG_SHOW_LAST_MODIFIED_NONE" }
								, { value: 1, name: "FINANCIALS_CONFIG_SHOW_LAST_MODIFIED_DATETIME" }
								, { value: 2, name: "FINANCIALS_CONFIG_SHOW_LAST_MODIFIED_USER" }
								, { value: 3, name: "FINANCIALS_CONFIG_SHOW_LAST_MODIFIED_BOTH" }],
							label: 'FINANCIALS_CONFIG_SHOW_LAST_MODIFIED',
							optionValue: 'value',
							optionName: 'name'
						};
					},
					DashboardId: function (resolves) {
						return {
							type: 'select',
							options: resolves.Dashboards,
							label: 'FINANCIALS_CONFIG_DASHBOARD',
							optionValue: 'Id',
							optionName: 'LanguageTranslation'
						};
					}
				}
			}
		});

		ViewsProvider.addView('process.financials', {
			template: 'features/financials/financials.html',
			controller: 'FinancialsController',
			level: 2
		});

		ViewsProvider.addFeature('process.financials', 'financials', "FEATURE_FINANCIALS", ['financials_read_planned', 'financials_read_estimated', 'financials_read_committed', 'financials_read_actual', 'financials_write_planned', 'financials_write_estimated', 'financials_write_committed', 'financials_write_actual', 'financials_delete', 'help']);
	}
	FinancialsController.$inject = ['$rootScope', '$scope', 'States', 'StateParameters', 'ArchiveDate', 'ViewService', 'MessageService'];
	function FinancialsController($rootScope, $scope, States, StateParameters, ArchiveDate, ViewService, MessageService) {
		$scope.loading = true;

		$scope.processItemId = StateParameters.itemId;
		$scope.processGroupId = StateParameters.processGroupId;
		$scope.archiveDate = ArchiveDate;
		$scope.process = StateParameters.process;
		$scope.selected_rows = [];
		$scope.states = States;
		$scope.scope = 0;
		$scope.move = 0;
		$scope.prev_tooltip = '';
		$scope.next_tooltip = ''; 

		$scope.header = {
			title: StateParameters.title,
			icons: []
		};

		$scope.Init = function (scope, move, prev_tooltip, next_tooltip) {
			$scope.scope = scope; 
			$scope.move = move; 
			$scope.prev_tooltip = prev_tooltip; 
			$scope.next_tooltip = next_tooltip; 
		};

		$scope.ShowFooter = function () {
			ViewService.footbar(StateParameters.ViewId,
				{
					template: 'features/financials/financialsFooter.html'
				});
		};

		$scope.Move = function (move) {
			$rootScope.$broadcast('financials-move', { move: move });
		};

		$scope.DeleteRows = function () {
			MessageService.confirm("FINANCIALS_DELETE_CONFIRM", function () {
				$rootScope.$broadcast('financials-deleteRows');
			});
		};
	}
})();