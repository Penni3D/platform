﻿(function () {
    'use strict';

    angular
        .module('core.features')
        .config(BusinessCase3Config)
        .controller('BusinessCaseController3', BusinessCaseController3);

    BusinessCase3Config.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
    function BusinessCase3Config(ViewsProvider, ViewConfiguratorProvider) {
        ViewConfiguratorProvider.addConfiguration('process.bc3', {
            onSetup: {
                Containers: function (SettingsService, StateParameters) {
                    return SettingsService.ProcessContainers(StateParameters.process);
                },
                ContainerColumns: function (SettingsService, StateParameters) {
                    return SettingsService.ProcessContainerColumns(StateParameters.process);
                },
                Translation: function (Translator) {
                    return Translator.translate;
                }
            },
            tabs: {
                default: {
                    SelectedContainer: function (resolves) {
                        return {
                            type: 'select',
                            options: resolves.Containers,
                            optionValue: 'ProcessContainerId',
                            optionName: 'LanguageTranslation',
                            label: 'BC3_ITEM_CONTAINER'
                        };
                    },
                    AttachedColumnId: function (resolves) {
                        return {
                            type: 'select',
                            options: [],
                            optionValue: 'ItemColumnId',
                            optionName: 'LanguageTranslation',
                            label: '1. ' + resolves.Translation('BC3_ITEM_COLUMN')
                        };
                    },

                    AttachedTitle: function (resolves) {
                        return {
                            type: 'translation',
                            label: '1. ' + resolves.Translation('BC3_ITEM_COLUMN_TITLE')
                        };
                    },
                    AttachedColumnId2: function (resolves) {
                        return {
                            type: 'select',
                            options: [],
                            optionValue: 'ItemColumnId',
                            optionName: 'LanguageTranslation',
                            label: '2. ' + resolves.Translation('BC3_ITEM_COLUMN')
                        };
                    },

                    AttachedTitle2: function (resolves) {
                        return {
                            type: 'translation',
                            label: '2. ' + resolves.Translation('BC3_ITEM_COLUMN_TITLE')
                        };
                    },
                    AttachedColumnId3: function (resolves) {
                        return {
                            type: 'select',
                            options: [],
                            optionValue: 'ItemColumnId',
                            optionName: 'LanguageTranslation',
                            label: '3. ' + resolves.Translation('BC3_ITEM_COLUMN')
                        };
                    },

                    AttachedTitle3: function (resolves) {
                        return {
                            type: 'translation',
                            label: '3. ' + resolves.Translation('BC3_ITEM_COLUMN_TITLE')
                        };
                    }
                },
            },
            onChange: function (p, params, resolves, options) {
                let override = [];
                if (resolves.ContainerColumns[params.SelectedContainer]) {
                    _.each(resolves.ContainerColumns[params.SelectedContainer], function (d) {
                        override.push(d.ItemColumn);
                    })
                }
                options.default.AttachedColumnId.options = override
                options.default.AttachedColumnId2.options = override
                options.default.AttachedColumnId3.options = override
            },
        });

        ViewsProvider.addView('process.bc3', {
            controller: 'BusinessCaseController3',
            template: 'features/businessCase3/businessCase3.html',
            level: 2,
            resolve: {
                QuestionData: function (StateParameters, ApiService, ArchiveDate) {
                    var a = ApiService.v1api("features/businessCase3");
                    return a.get(StateParameters.itemId + '/' + ArchiveDate).then(function (response) {
                        return response.data;
                    });
                }
            }, afterResolve: {
                SelectedColumn: function (StateParameters, SettingsService) {
                    let column = {};
                    return SettingsService.ProcessContainerColumns(StateParameters.process, StateParameters.SelectedContainer).then(function (pCols) {
                        _.each(pCols, function (pCol) {
                            if (pCol.ItemColumn.ItemColumnId == StateParameters.AttachedColumnId) {
                                column = pCol;
                            }
                        })
                        return column;
                    })
                },
                SelectedColumn2: function (StateParameters, SettingsService) {
                    let column = {};
                    return SettingsService.ProcessContainerColumns(StateParameters.process, StateParameters.SelectedContainer).then(function (pCols) {
                        _.each(pCols, function (pCol) {
                            if (pCol.ItemColumn.ItemColumnId == StateParameters.AttachedColumnId2) {
                                column = pCol;
                            }
                        })
                        return column;
                    })
                },
                SelectedColumn3: function (StateParameters, SettingsService) {
                    let column = {};
                    return SettingsService.ProcessContainerColumns(StateParameters.process, StateParameters.SelectedContainer).then(function (pCols) {
                        _.each(pCols, function (pCol) {
                            if (pCol.ItemColumn.ItemColumnId == StateParameters.AttachedColumnId3) {
                                column = pCol;
                            }
                        })
                        return column;
                    })
                }
            }
        })

        ViewsProvider.addFeature('process.bc3', 'business_case3', "FEATURE_BUSINESS_CASE3", ['read', 'write', 'help']);
    }

    BusinessCaseController3.$inject = ['$scope', 'SaveService', 'ApiService', 'QuestionData', 'StateParameters', 'Translator', 'Data', 'SelectedColumn', 'SelectedColumn2', 'SelectedColumn3', 'ProcessService', '$rootScope', 'States', 'ArchiveDate'];
    function BusinessCaseController3($scope, SaveService, ApiService, QuestionData, StateParameters, Translator, Data, SelectedColumn, SelectedColumn2, SelectedColumn3, ProcessService, $rootScope, States, ArchiveDate) {
        $scope.question_A = 0;
        $scope.question_B = 0;
        $scope.question_C = 0;
        $scope.QuestionData = QuestionData;
        $scope.process = StateParameters.process;
        $scope.data = {};
        if (typeof ArchiveDate === 'undefined' || ArchiveDate === null) {
            $scope.data = Data.Data;
        }
        else {
            ProcessService.getArchiveData(StateParameters.process, StateParameters.itemId, ArchiveDate).then(function (data) {
                $scope.data = data.Data.Data;
            });
        }
        $scope.selectedColumn = SelectedColumn;
        $scope.selectedColumn2 = SelectedColumn2;
        $scope.selectedColumn3 = SelectedColumn3;

        $scope.listItems = {};
        $scope.columnTitle = "";
        $scope.columnTitle2 = "";
        $scope.columnTitle3 = "";
        $scope.hasAttachedColumn = !_.isEmpty($scope.selectedColumn);
        $scope.hasAttachedColumn2 = !_.isEmpty($scope.selectedColumn2);
        $scope.hasAttachedColumn3 = !_.isEmpty($scope.selectedColumn3);

        $scope.writeRight = States.business_case3.groupStates[StateParameters.processGroupId].s.includes('write');

        if (StateParameters.AttachedTitle != null && StateParameters.AttachedTitle[$rootScope.clientInformation.locale_id]) {
            $scope.columnTitle = StateParameters.AttachedTitle[$rootScope.clientInformation.locale_id];
        }

        if (StateParameters.AttachedTitle2 != null && StateParameters.AttachedTitle2[$rootScope.clientInformation.locale_id]) {
            $scope.columnTitle2 = StateParameters.AttachedTitle2[$rootScope.clientInformation.locale_id];
        }

        if (StateParameters.AttachedTitle3 != null && StateParameters.AttachedTitle3[$rootScope.clientInformation.locale_id]) {
            $scope.columnTitle3 = StateParameters.AttachedTitle3[$rootScope.clientInformation.locale_id];
        }

        if ($scope.hasAttachedColumn && $scope.selectedColumn.ItemColumn.DataType == 6) {
            ProcessService.getListItems($scope.selectedColumn.ItemColumn.DataAdditional).then(function (items) {
                $scope.listItems[$scope.selectedColumn.ItemColumn.DataAdditional] = items;
            })
        }

        if ($scope.hasAttachedColumn2 && $scope.selectedColumn2.ItemColumn.DataType == 6) {
            ProcessService.getListItems($scope.selectedColumn2.ItemColumn.DataAdditional).then(function (items) {
                $scope.listItems[$scope.selectedColumn2.ItemColumn.DataAdditional] = items;
            })
        }

        if ($scope.hasAttachedColumn3 && $scope.selectedColumn3.ItemColumn.DataType == 6) {
            ProcessService.getListItems($scope.selectedColumn3.ItemColumn.DataAdditional).then(function (items) {
                $scope.listItems[$scope.selectedColumn3.ItemColumn.DataAdditional] = items;
            })
        }

        $scope.dataChanged = function () {
            ProcessService.setDataChanged(StateParameters.process, StateParameters.itemId);
        }

        $scope.header = {
            title: StateParameters.title ? StateParameters.title : Translator.translate('BUSINESSCASE3')
        }

        $scope.calculateTotal = function (data) {
            var question_A = 0;
            var question_B = 0;
            var question_C = 0;

            angular.forEach(data, function (value, key) {
                if (data[key].choice == data[key].choice1) {
                    question_A += 1;
                }
                else if (data[key].choice == data[key].choice2) {
                    question_B += 1;
                }
                else if (data[key].choice == data[key].choice3) {
                    question_C += 1;
                }
            });
            $scope.question_A = question_A;
            $scope.question_B = question_B;
            $scope.question_C = question_C;
        };

        $scope.calculateTotal($scope.QuestionData);

        $scope.setChanged = function (obj) {
            $scope.calculateTotal($scope.QuestionData);
            obj.changed = true;
        };

        var bc3 = ApiService.v1api("features/businesscase3");
        SaveService.subscribeSave($scope, function () {
            return bc3.save(StateParameters.itemId, $scope.QuestionData);
        });
    }
})();