(function () {
    'use strict';

    angular
        .module('core.features')
        .config(BaselineManagerConfig)
        .service('BaselineManagerService', BaselineManagerService)
        .controller('BaselineManagerController', BaselineManagerController);

    BaselineManagerConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
    function BaselineManagerConfig(ViewsProvider, ViewConfiguratorProvider) {
        ViewConfiguratorProvider.addConfiguration('process.baselineManager', {
            onSetup: {
                Portfolios: function (PortfolioService, StateParameters) {
                    return PortfolioService.getPortfolios(StateParameters.process, 0);
                },
                Lists: function (SettingsService) {
                    return SettingsService.ListProcesses();
                }
            },            
            defaults: {
                BaselineTypes: 0
            },
            tabs: {
                default: {
					featuretteWidth: function () {
						return {
							type: 'select',
							options: [{name: 'VIEW_SPLIT_EVEN', value: 'even'}, {name: 'VIEW_SPLIT_WIDER', value: 'wider'}, {name: 'VIEW_SPLIT_NARROWER', value: 'narrower'}],
							label: "BASELINE_FEATURETTE_SIZE"
						}
					},
                    baselinePortfolioId: function (resolves) {
                        return {
                            type: 'select',
                            options: resolves.Portfolios,
                            label: 'BASELINE_PORTFOLIO_ID',
                            optionValue: 'ProcessPortfolioId',
                            optionName: 'LanguageTranslation'
                        }
                    },
                    BaselineList: function (resolves) {
                        return {
                            label: 'BASELINE_LIST',
                            type: 'select',
                            options: resolves.Lists,
                            optionValue: 'ProcessName',
                            optionName: 'LanguageTranslation'
                        };
                    },
                    BaselineTypes: function (resolves) {
                        return {
                            label: 'BASELINE_TYPES',
                            type: 'select',
                            options: [{name: 'ALL', value: 0}, {name: 'SYSTEM', value: 1}, {name: 'CUSTOMER', value: 2}],
                            optionValue: 'value',
                            optionName: 'name'
                        };
                    }
                }
            }
        });
        
		ViewsProvider.addView('process.baselineManager', {
			controller: 'BaselineManagerController',
			template: 'features/baselineManager/baselineManager.html',
			level: 2,
			resolve: {
			    BaselineDatesRaw: function(BaselineManagerService, StateParameters) {
                    return BaselineManagerService.getBaselineDates(StateParameters.process, StateParameters.itemId, StateParameters.BaselineTypes);
                },
                PortfolioColumns: function (PortfolioService, ViewService, StateParameters, Translator, $rootScope) {
                    // var portfolioId = StateParameters.risks_portfolio_id;
                    // if (portfolioId > 0) {
                    //     return PortfolioService.getPortfolioColumns('risks', portfolioId);
                    // } else {
                    //     return ViewService.fail("RISKS_CONFIG_ERROR")
                    // }
                    return PortfolioService.getPortfolioColumns(StateParameters.process, StateParameters.baselinePortfolioId).then(function (cols) {
                        var lt = {};
                        lt[$rootScope.clientInformation.locale_id] = Translator.translate('BASELINE_DATE');
                        var p = {
                            ItemColumn: {
                                DataType: 3,
                                Name: 'process_baseline_date',
                                LanguageTranslation: lt ,
                                showType: 'block'
                            },
                            UseInSelectResult: 4,
                            IsDefault: true,
                            ProcessPortfolioColumnGroupId: 0
                        };
                        
                        var lt2 = {};
                        lt2[$rootScope.clientInformation.locale_id] = Translator.translate('DESCRIPTION');
                        var p2 = {
                            ItemColumn: {
                                DataType: 0,
                                Name: 'process_baseline_description',
                                LanguageTranslation: lt2 ,
                                showType: 'block'
                            },
                            UseInSelectResult: 1,
                            IsDefault: true,
                            ProcessPortfolioColumnGroupId: 0
                        };

                        var lt3 = {};
                        lt3[$rootScope.clientInformation.locale_id] = Translator.translate('PREVIOUS_BASELINE_DATE');
                        var p3 = {
                            ItemColumn: {
                                DataType: 3,
                                Name: 'process_baseline_previous_date',
                                LanguageTranslation: lt3 ,
                                showType: 'block'
                            },
                            UseInSelectResult: 3,
                            ReportColumn: true,
                            ProcessPortfolioColumnGroupId: 0
                        };

                        
                        cols.unshift(p2);
                        cols.unshift(p3);
                        cols.unshift(p);
                        
                        PortfolioService.getActiveSettings(StateParameters.baselinePortfolioId).filters.columns = ['process_baseline_previous_date'];
                        
                        return cols;
                    });
                },
                PortfolioColumnGroups: function (PortfolioService, StateParameters) {
                    return PortfolioService.getPortfolioColumnGroups(
                        StateParameters.process,
                        StateParameters.baselinePortfolioId
                    );
                },
                BaselineList: function (ListsService, StateParameters, $q) {
                    if (StateParameters.BaselineList) {
                        var bDict = {};
                        return ListsService.getProcessItems(StateParameters.BaselineList, 'order').then(function (bIds) {
                            angular.forEach(bIds, function (key, val) {
                                bDict[key.item_id] = key;
                            });
                            return bDict;
                        })
                    } else {
                        return  $q.when({});
                    }
                }
			},
            afterResolve: {
			    BaselineDates: function (BaselineDatesRaw, BaselineList, ProcessService, StateParameters) {
                    angular.forEach(BaselineDatesRaw.dates, function (key, val) {
                        if (angular.isDefined(BaselineList[key.description])) {
                            key.description = BaselineList[key.description].list_item;
                        }
                    });
                    angular.forEach(BaselineDatesRaw.data, function (key, val) {

                        if (angular.isDefined(BaselineList[key.process_baseline_description])) {
                            key.process_baseline_description = BaselineList[key.process_baseline_description].list_item;
                            ProcessService.setData({ Data: key }, StateParameters.process, key.item_id);

                        }
                    });
                    return BaselineDatesRaw;
                }
            }
		});

		ViewsProvider.addFeature('process.baselineManager', 'BASELINE_MANAGER', "BASELINE_MANAGER", ['read', 'write', 'delete_baseline']);
	}

    BaselineManagerService.$inject = ['ApiService', 'ProcessService'];
    function BaselineManagerService(ApiService, ProcessService) {
        var self = this;
        var p = ApiService.v1api('meta/items');
        var b = ApiService.v1api('features/ProcessBaseline');

        
        self.getBaselineDates = function (process, itemId, type) {
            var historyDate = ProcessService.getHistoryDate(process, itemId);

            return p.get([process, 'baselineData', itemId, type]).then(function(baselineData) {
                var i, l = baselineData.dates.length;
                
                var previousDate = null;
                
                for (i = 0; i < l; i++) {
                    var row = baselineData.data[ baselineData.dates[i].process_baseline_id ];
                    row.process = process;
                    row.parent_item_id = itemId;
                    row.owner_item_id = itemId;
                    row.owner_type = process;
					row.process_baseline_date = baselineData.dates[i].baseline_date;
					row.task_type = 30;
					if (baselineData.dates[i].process_baseline_id == 0 && historyDate == null)
						row.task_type = 31;
					if (moment(row.process_baseline_date).format('YYYY-MM-DDTHH:mm:ss') == historyDate) { // historydate on jo moment().format('YYYY-MM-DDTHH:mm:ss')
                        row.task_type = 31;
					}

                    if(i == 0) {
                        row.process_baseline_previous_date = baselineData.start_date;
                    } else {
                        row.process_baseline_previous_date = previousDate;
                    }
                    previousDate = baselineData.dates[i].baseline_date;

                    row.process_baseline_description = baselineData.dates[i].description;

                    row.item_id = baselineData.dates[i].process_baseline_id + "_" + itemId;

                    ProcessService.setData({ Data: row }, process, baselineData.dates[i].process_baseline_id + "_" + itemId);
                }
                return baselineData;
            });
        }
        
        self.addBaseline = function(process, itemId, desc) {
            return b.new(['addBaseline', process, itemId], {'description': desc});
        }

        self.deleteBaseline = function(process, itemId) {
            return b.delete([process, itemId]);
        }
    }

    BaselineManagerController.$inject = [
		'$rootScope',
        '$scope',
		'StateParameters',
        'BaselineDates',
        'PortfolioColumns',
        'ViewService',
        'PortfolioColumnGroups',
        'BaselineManagerService',
        'MessageService',
        'ProcessService',
        'Translator',
        '$timeout',
        'States',
		'BaselineList',
		'BaselineDatesRaw'
	];
    function BaselineManagerController(
    	$rootScope,
        $scope,
		StateParameters,
        BaselineDates,
        PortfolioColumns,
        ViewService,
        PortfolioColumnGroups,
        BaselineManagerService,
        MessageService,
        ProcessService,
        Translator,
        $timeout,
        States,
		BaselineList,
		BaselineDatesRaw
	) {
        $scope.isLoaded = false;

		var hasWrite = (angular.isDefined(States.BASELINE_MANAGER) &&
            typeof States.BASELINE_MANAGER.groupStates[StateParameters.processGroupId] !== 'undefined' &&
            States.BASELINE_MANAGER.groupStates[StateParameters.processGroupId].s.indexOf("write") > -1)
		//hasWrite = true;

        var hasDelete = (angular.isDefined(States.BASELINE_MANAGER) &&
            typeof States.BASELINE_MANAGER.groupStates[StateParameters.processGroupId] !== 'undefined' &&
			States.BASELINE_MANAGER.groupStates[StateParameters.processGroupId].s.indexOf("delete_baseline") > -1)
		//hasDelete = true;

        $scope.rules = {
            parents: {
                30: ['root']
            },
            draggables: [30]
        };
        
        var menuItemsArr = [];
        
        menuItemsArr.push({
            name: Translator.translate("SELECT_BASELINE"),
            isActive: function(rowId) {
                return true;
            },
            onClick: function (rowId) {
                $scope.isLoaded = false;
				
				if (_baselineIds[rowId] > 0) {
                    ProcessService.forceHistoryDate(StateParameters.process, StateParameters.itemId, _baselineDates[rowId]);
				} else {
					ProcessService.forceHistoryDate(StateParameters.process, StateParameters.itemId, null);
					ProcessService.unforceHistoryDate(); // jostain syystä tämä ei taida toimia..
                }

				angular.forEach($scope.rows, function (scopeRowId) {
					if (scopeRowId.item_id == rowId) {
						scopeRowId.task_type = 31;
					} else {
						scopeRowId.task_type = 30;
					}

					ProcessService.setData({ Data: scopeRowId }, StateParameters.process, scopeRowId.item_id);
				});

				ProcessService.getItem(StateParameters.process, rowId).then(function (baselineRow) {
					baselineRow.task_type = 31;
					ProcessService.setData({ Data: baselineRow }, StateParameters.process, rowId);
				});

				$timeout(function () {
					$scope.isLoaded = true;
				}, 200);
            }
        });
        
        if(hasDelete) {
            menuItemsArr.push( {
                name: Translator.translate("DELETE"),
                isActive: function(rowId) {
                    if(_deletableRows.indexOf(rowId) >= 0) {
                        return true;
                    }
                    return false;
                },
                onClick: function (rowId) {
                    MessageService.confirm('DELETE_BASELINE', function () {
						$scope.isLoaded = false;
						$.each($scope.rows, function (index, item) {
							if (item.item_id == rowId)
								$scope.rows.splice(index, 1);
						});
                        //$scope.rows.splice($scope.rows.indexOf(rowId), 1);
                        BaselineManagerService.deleteBaseline(StateParameters.process, rowId.split('_')[0]).then(function(deleted) {
                            $scope.isLoaded = true;
                        });
                    });

                }
            });
        }

        $scope.options = { //Rowmanager options. chart is made with $scope.chartOptions
            process: StateParameters.process,
            ownerId: StateParameters.itemId,
            ownerType: StateParameters.process,
            showCalendar: true,
            calendarSteps: [15,30,60, 180, 360, 720],
            showUserName: true,
            sortable: false,
            featuretteWidth: StateParameters.featuretteWidth,
            selectable: false,
            // rowClick: function (id) {
            //    var row = ProcessService.getCachedItemData(StateParameters.process, id).Data;
            //    openRow(row);
            // },
            alias: {
                30: StateParameters.process
            },
            menuItems: {
                30: menuItemsArr
            },
            headerClick: function (columnName) {

            },
            readOnly: true,
            rowDblClick: function() {
                
            }
        };
        
        $scope.portfolioColumns = PortfolioColumns;
        $scope.portfolioColumnGroups = PortfolioColumnGroups;
        $scope.portfolioId = StateParameters.baselinePortfolioId;
        $scope.baselineDates = BaselineDates;
        
        $scope.header = {
            title: StateParameters.title,
            icons: []
        };

        var _deletableRows = [];
        var _baselineDates = {};
        var _baselineIds = {};

        var lastBaselineDate = new Date();
        
		function updateRows(r) {
			var _new_rows = [];
            _deletableRows = [];
            var i, l = r.dates.length;
            for (i = l-1; i >= 0; i--) {
                if(r.dates[i].process_group_id == null) {
                    _deletableRows.push(r.dates[i].process_baseline_id + "_" + StateParameters.itemId);
                }
                _baselineDates[r.dates[i].process_baseline_id + "_" + StateParameters.itemId] = r.dates[i].baseline_date;
				_baselineIds[r.dates[i].process_baseline_id + "_" + StateParameters.itemId] = r.dates[i].process_baseline_id;
				lastBaselineDate = r.dates[i].baseline_date;

				var row = r.data[r.dates[i].process_baseline_id];
				//row.process = StateParameters.process;
				//row.parent_item_id = StateParameters.itemId;
				////row.task_type = 30;
				//... nämä asetettu jo tuolla getBaselineDates, tämän tarvitsee vain järjestää indeksit rowmanagerille:
				_new_rows.push(ProcessService.setData({ Data: row }, StateParameters.process, r.dates[i].process_baseline_id + "_" + StateParameters.itemId).Data);
            }
            
			$scope.rows = _new_rows;
			//global_debug = $scope.rows;

            $scope.isLoaded = true;
        };
		updateRows(BaselineDatesRaw);

		$scope.addBaseline = function () { 
			// miten tätä käytetään?
            var data = {};
            //$scope.isLoaded = false;

            var addBaselineSave = function () {
                $scope.isLoaded = false;

                BaselineManagerService.addBaseline(StateParameters.process, StateParameters.itemId, data.desc).then(function (response) {
                    var row = response.data;
                    row.task_type = 30;
                    row.process = StateParameters.process;
                    row.parent_item_id = StateParameters.itemId;
                    row.owner_item_id = StateParameters.itemId;
                    row.owner_type = StateParameters.process;
                    row.process_baseline_date = response.baselineDate;
                    row.process_baseline_description = data.desc;

                    row.process_baseline_previous_date = lastBaselineDate;
                    lastBaselineDate = row.process_baseline_date;
                    
                    row.item_id = response.baselineId + "_" + StateParameters.itemId;
					row.actual_item_id = row.item_id;

                    _baselineDates[row.item_id] = response.baselineDate;
                    _baselineDates[row.item_id] = response.process_baseline_id;

                    _deletableRows.push(row.item_id);


					$scope.rows.splice(1, 0, row); //$scope.rows.splice(1, 0, row.item_id);
					ProcessService.setData({ Data: row }, StateParameters.process, row.item_id);

                    $scope.isLoaded = true;
                    
                    MessageService.closeDialog();
                });
            };

            var content = {
                buttons: [{ text: 'CANCEL' }, { type: 'primary', onClick: addBaselineSave, text: 'ADD' }],
                inputs: [{ type: 'string', label: 'DESCRIPTION', model: 'desc' }],
                title: "ADD_BASELINE"
            };
            MessageService.dialog(content, data)
        };

        ViewService.footbar(StateParameters.ViewId, {
            template: 'features/baselineManager/baselineManagerFooter.html',
            scope: $scope
        });
    }
})();
//var global_debug;