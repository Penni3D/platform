(function () {
	'use strict';

	angular
		.module('core.features')
		.service('HourAccumulationService', HourAccumulationService);

	HourAccumulationService.$inject = ['AllocationService', 'WorkTimeReportService', '$q','ApiService'];

	function HourAccumulationService(AllocationService, WorkTimeReportService, $q, ApiService) {
		let self = this;
		let Totals = ApiService.v1api('resources/allocation/totals');
		
		self.getAccumulationData = function (process, itemId, title, startDate, endDate, fromActuals = false) {
			let promises = [];
			let ua;
			let ta;
			let bo;
			let re;

			promises.push(
				AllocationService.getAllocationTotalById(
					itemId,
					startDate.format('YYYY-MM-DD'),
					endDate.format('YYYY-MM-DD'),
					1).then(function (userAllocation) {
					ua = userAllocation;
				}));

			promises.push(
				AllocationService.getAllocationTotalById(
					itemId,
					startDate.format('YYYY-MM-DD'),
					endDate.format('YYYY-MM-DD'),
					2).then(function (taskAllocation) {
					ta = taskAllocation;
				}));

			promises.push(
				AllocationService.getAllocationTotalById(
					itemId,
					startDate.format('YYYY-MM-DD'),
					endDate.format('YYYY-MM-DD'),
					3).then(function (bothAllocation) {
					bo = bothAllocation;
				}));

			if(!startDate.isValid()){
				startDate = moment().subtract(3, 'days');
			}

			if(!endDate.isValid()){
				endDate = moment().add(4, 'days');
			}

			promises.push(WorkTimeReportService.getWorkTimeReport(
				process,
				title,
				{
					basicHours: 'false',
					grouping: 'users',
					reportProcesses: [{process: itemId}],
					reportScope: 'task',
					reportUsers: [{user: 'all'}],
					resolution: 'day',
					targetEntityName: 'title',
					timeframeStart: startDate.toDate(),
					timeframeEnd: endDate.toDate(),
					lmApprovedHours: true,
					fromActuals: fromActuals
				}).then(function (report) {
				re = report.data;
			}));

			return $q.all(promises).then(function () {
				return {
					userAllocation: ua,
					taskAllocation: ta,
					bothAllocation: bo,
					report: re
				};
			});
		};


		self.getTimeAnalysis = function(itemId){

			return Totals.get([itemId, "new"]);

		}
	}
})();