﻿(function () {
	'use strict';

	angular
		.module('core.features')
		.config(hourAccumulationConfig)
		.controller('hourAccumulationController', hourAccumulationController);

	hourAccumulationConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];

	function hourAccumulationConfig(ViewsProvider, ViewConfiguratorProvider) {
		ViewConfiguratorProvider.addConfiguration('process.hourAccumulation', {
			onSetup: {
				ItemColumns: function (ProcessService, StateParameters, Common) {
					return ProcessService.getColumns(StateParameters.process).then(function (columns) {
						let c = [];

						_.each(columns, function (column) {
							let dt = Common.getRelevantDataType(column);
							if (dt == 1 || dt == 2 || dt == 3 || dt == 13) {
								column.dataType = dt;
								c.push(column);
							}
						});

						return c;
					});
				},
				ItemColumnsTask: function (ProcessService, Common, StateParameters) {
					return ProcessService.getColumns(StateParameters.process).then(function (columns) {
						let c = [];
						_.each(columns, function (column) {
							let dt = Common.getRelevantDataType(column);
							if (dt == 0 || dt == 4) c.push(column);
						});
						return c;
					});
				}
			},
			tabs: {
				default: {
					start_date_column: function (resolves) {
						return {
							type: 'select',
							options: resolves.ItemColumns,
							label: 'START_DATE',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						}
					},
					end_date_column: function (resolves) {
						return {
							type: 'select',
							options: resolves.ItemColumns,
							label: 'END_DATE',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						}
					},
					additional_compare_column: function (resolves) {
						let cols = [];
						_.each(resolves.ItemColumns, function (ic) {
							if (ic.dataType == 1 || ic.DataType == 2) cols.push(ic)
						});

						return {
							type: 'select',
							options: cols,
							label: 'HOURACCUMULATION_ADD',
							optionValue: 'Name',
							optionName: 'LanguageTranslation'
						}
					},
					title_column: function (resolves) {
						return {
							type: 'select',
							options: resolves.ItemColumnsTask,
							label: 'TITLE',
							optionValue: 'Name',
							optionName: 'LanguageTranslation',
							modify: true
						}
					},
					useActuals: function (resolves) {
						return {
							type: 'select',
							options: [{name: 'FALSE', value: 0}, {name: 'TRUE', value: 1}],
							label: 'HOURACCUMULATION_ACTUALS',
							optionValue: 'value',
							optionName: 'name'
						};
					},
					showSecondary: function(){
						return {
							type: 'select',
							options: [{name: 'FALSE', value: 0}, {name: 'TRUE', value: 1}],
							label: 'SHOW_SECONDARY_HOUR_ACCUMULATION',
							optionValue: 'value',
							optionName: 'name'
						};
					}
				}
			},
			defaults: {
				title_column: 'title',
				useActuals: 0,
				showSecondary: 0
			}

		});

		ViewsProvider.addView('process.hourAccumulation', {
			controller: 'hourAccumulationController',
			template: 'features/hourAccumulation/hourAccumulation.html',
			resolve: {
				StartDate: function (StateParameters, Data) {
					return (StateParameters.start_date_column ? moment(Data.Data[StateParameters.start_date_column]) : moment()).startOf('isoWeek');
				},
				EndDate: function (StateParameters, Data) {
					return (StateParameters.end_date_column ? moment(Data.Data[StateParameters.end_date_column]) : moment()).endOf('isoWeek')
				}, WorkTimeRights: function (ViewConfigurator) {
					return ViewConfigurator.getRights('worktimeHourStatus').then(function (states) {
						return {
							download: _.includes(states, 'download'),
							email: _.includes(states, 'email'),
							approveAll: _.includes(states, 'approveAll'),
							rebase: _.includes(states, 'rebase'),
							rebaseAll: _.includes(states, 'rebaseAll'),
							viewall: _.includes(states, 'viewall')
						};
					});
				},
				AnalysisData: function (HourAccumulationService, StateParameters) {
					return HourAccumulationService.getTimeAnalysis(StateParameters.itemId);
				}
			},
			afterResolve: {
				AccumulationData: function (HourAccumulationService, StateParameters, StartDate, EndDate) {
					return HourAccumulationService.getAccumulationData(
						StateParameters.process,
						StateParameters.itemId,
						StateParameters.title_column,
						StartDate,
						EndDate,
						StateParameters.useActuals == 1
					);
				},

				AllUsersWithTasks: function (AnalysisData, UserService, $q) {

					let data = {responsibles: [], users: []};
					let promises = [];
					_.each(AnalysisData, function (task) {
						_.each(task.TaskResponsibles, function (responsible) {
							if (!_.includes(data.responsibles, responsible)) data.responsibles.push(responsible)
						});
					})

					_.each(data.responsibles, function (id) {
						promises.push(UserService.getUser(id).then(function (u) {
							data.users.push(u);
						}));
					})

					return $q.all(promises).then(function () {
						return data;
					});
				}
			}
		});

		ViewsProvider.addFeature('process.hourAccumulation', 'hourAccumulation', 'FEATURE_HOURACCUMULATION', ['read']);
	}

	hourAccumulationController.$inject = [
		'$scope',
		'ViewService',
		'StateParameters',
		'Data',
		'CalendarService',
		'Common',
		'Colors',
		'Translator',
		'HourAccumulationService',
		'AccumulationData',
		'StartDate',
		'EndDate',
		'MessageService',
		'UserService',
		'WorkTimeRights',
		'ProcessService',
		'AnalysisData',
		'AllUsersWithTasks',
		'$q',
		'ApiService',
		'BackgroundProcessesService'
	];

	function hourAccumulationController(
		$scope,
		ViewService,
		StateParameters,
		Data,
		CalendarService,
		Common,
		Colors,
		Translator,
		HourAccumulationService,
		AccumulationData,
		StartDate,
		EndDate,
		MessageService,
		UserService,
		WorkTimeRights,
		ProcessService,
		AnalysisData,
		AllUsersWithTasks,
		$q,
		ApiService,
		BackgroundProcessesService
	) {
		let parentItemIds = [];
		let subPhases = [];
		let tasksToBeRemoved = [];
		let usersAndNames = {};

		$scope.budgetTotal = undefined;
		$scope.showSecondary = StateParameters.showSecondary;

		let parentChain = function (item, level, data): number {
			let p = _.find(data, function (o) {
				return o.item_id == item.recursionId;
			});
			if (p) return parentChain(p, level + 1, data);
			return level;
		}

		$scope.fn = function (n) {
			if (!n) return 0;
			return Common.formatNumber(n, 1);
		}

		let setParents = function (rowsToSet) {
			//This function adds phases on right places in task array
			let loopProtection = angular.copy(rowsToSet);
			let it = 0;
			_.each(parentItemIds, function (id) {
				let index = _.findIndex(rowsToSet, function (o) {
					return o.parentItemId == id;
				});

				let p = _.find(AnalysisData, function (o) {
					return o.ItemId == id;
				});

				let itemToAdd = {
					name: p.Title,
					columns: calculatePhaseNumbers(id, 'columns', rowsToSet),
					vs: calculatePhaseNumbers(id, 'vs', rowsToSet),
					total: calculatePhaseNumbers(id, 'total', rowsToSet),
					phase: true,
					parentType: p.ParentType,
					itemId: parseInt(id),
					item_id: parseInt(id),
					parentItemId: p.ParentItemId,
					recursionId: p.ParentItemId,
					order: p.OrderNo
				}
				loopProtection.splice(index + it, 0, itemToAdd)
				it++;
			});

			//We need to splice away the second level phase 'duplicates'. 
			let secondProtection = angular.copy(loopProtection);
			let v = 0;
			//For some reason the property name is name although it represents item id
			_.each(loopProtection, function (lp, ind) {
				if (_.includes(parentItemIds, lp.name)) {
					secondProtection.splice(ind + v, 1);
					v--;
				}
			});

			_.each(secondProtection, function (double) {
				if (_.includes(parentItemIds, double.hasOwnProperty("parentItemId") ? double.parentItemId.toString() : 0)) {
					double.isChild = true;
				}
			})

			$scope.reportRows = secondProtection;
		}
		$scope.showPhases = true;
		$scope.showUsers = true;

		let fixPhaseNumbers = function (item, parent, data) {
			if (parent) {
				parent.total += item.total;
				for (let i = 0; i <= item.columns.length - 1; i++) {
					parent.columns[i] += item.columns[i];
				}
			}

			_.each($scope.reportRows, function (subItem) {
				if (subItem.recursionId == item.item_id && subItem.phase) fixPhaseNumbers(subItem, item, data);
			});
		}
		let fixVSNumbers = function (item, parent, data) {
			if (parent) parent.vs += item.vs;
			_.each($scope.reportRows, function (subItem) {
				if (subItem.recursionId == item.item_id && subItem.phase) fixVSNumbers(subItem, item, data);
			});
		}

		let calculatePhaseNumbers = function (parenItemId, type, rowsToSet) {
			let totalAmount = 0;
			let columns_ = [];
			_.each(rowsToSet, function (o) {
				if (o.parentItemId == parenItemId && o.type == 'task') {
					totalAmount = parseFloat(o[type]) + totalAmount
					columns_.push(o.columns);
				}
			})

			let flattenedSums = [];
			let colsAmount = $scope.reportRows.length > 0 ? $scope.reportRows[0].columns.length : 0;
			for (let k = 1; k < colsAmount + 1; k++) {
				flattenedSums.push(0);
			}

			_.each(columns_, function (cols) {
				for (let i = 0; i < cols.length; i++) {
					flattenedSums[i] = flattenedSums[i] + cols[i];
				}
			});

			if (type == 'columns') {
				return flattenedSums;
			} else if (type == 'total') {
				let sum = 0;
				for (let j = 0; j < flattenedSums.length; j++) {
					sum = sum + flattenedSums[j];
				}
				return sum;
			}

			return totalAmount;
		}

		let monthTranslations = Common.getLocalShortMonthsOfYear();
		let greyColor = Colors.getColor('grey').rgb;
		let lightGreyColor = Colors.getColor('lightGrey').rgb;
		let accentColor = Colors.getColor('accent-2').rgb;
		let Items = ApiService.v1api('meta/Items');

		let barChartColors = {
			backgroundColor: accentColor,
			pointBackgroundColor: accentColor,
			hoverBackgroundColor: accentColor,
			fill: true
		};

		$scope.compareColumn = StateParameters.additional_compare_column;

		let startDate = StartDate;
		let endDate = EndDate;

		$scope.calendarDates = {
			s: startDate.toDate(),
			e: endDate.toDate()
		};

		$scope.spRelation = {
			plannedTitle: Translator.translate('HOURACCUMULATION_SPENT_PLANNED'),
			plannedRatio: 0,
			plannedHours: 0,
			total: 0
		};

		$scope.sbRelation = {
			plannedTitle: Translator.translate('HOURACCUMULATION_BUDGET_PLANNED'),
			plannedRatio: 0,
			plannedHours: 0,
			total: Data.Data[$scope.compareColumn]
		};

		$scope.HOUR_ACCUMULATION_BUDGET_INFO = Translator.translate("HOUR_ACCUMULATION_BUDGET_INFO");
		$scope.HOUR_ACCUMULATION_PLANNED_INFO = Translator.translate("HOUR_ACCUMULATION_PLANNED_INFO");

		$scope.groupByTask = false;
		$scope.reportTotalHours = 0;
		$scope.selectedCalendarResolution = endDate.diff(startDate, 'months') >= 9 ? 'months' : 'weeks';
		$scope.calendarResolutions = [
			{name: 'CALENDAR_WEEK_VIEW', value: 'weeks'},
			{name: 'CALENDAR_MONTH_VIEW', value: 'months'},
			{name: 'CALENDAR_QUARTAL_VIEW', value: 'quarter'},
			{name: 'CALENDAR_YEAR_VIEW', value: 'year'}
		];

		$scope.resolutionChanged = initializeData;
		$scope.groupingChanged = initializeData;

		$scope.barChartData = [[], []];
		$scope.barChartLabels = [];
		$scope.barChartColors = [];
		$scope.barChartOverride = [
			{
				label: Translator.translate('HOURACCUMULATION_SPENT'),
				borderWidth: 1,
				type: 'bar'
			},
			{
				label: Translator.translate('HOURACCUMULATION_HOURS'),
				borderWidth: 1,
				type: 'line',
				backgroundColor: [lightGreyColor],
				borderColor: [greyColor],
				pointBackgroundColor: [lightGreyColor]
			}
		];

		$scope.barChartOptions = {
			maintainAspectRatio: false,
			responsive: false,
			scales: {
				yAxes: [
					{
						id: 'y-axis-1',
						display: true,
						position: 'left',
						gridLines: {
							display: true,
							drawBorder: false,
							lineWidth: 2,
							color: '#f0f1f4',
							zeroLineWidth: 2,
							zeroLineColor: '#f0f1f4'
						},
						ticks: {
							fontFamily: 'Roboto',
							fontSize: 12,
							fontColor: '#676a6e'
						}
					}
				],
				xAxes: [
					{
						id: 'x-axis-1',
						display: true,
						position: 'left',
						gridLines: {
							display: false,
							drawBorder: true
						},
						ticks: {
							fontFamily: 'Roboto',
							fontSize: 12,
							fontColor: '#676a6e'
						}
					}
				]
			}
		};

		ViewService.footbar(StateParameters.ViewId, {
			template: 'features/hourAccumulation/hourAccumulation-footbar.html',
			scope: $scope
		});

		setHeader();
		initializeData(AccumulationData);

		function formatDate(date) {
			if ($scope.selectedCalendarResolution === 'months') {
				return monthTranslations[date.month()];
			} else if ($scope.selectedCalendarResolution === 'quarter') {

				return moment(date).quarter()
			} else if ($scope.selectedCalendarResolution === 'year') {
				return date.format('YYYY')
			}

			return date.format('DD/MM')
		}

		function setHeader() {
			if (StateParameters.useActuals != 1) {
				ViewService.addDownloadable("PORTFOLIO_EE", function () {
					window.open("time/WorkTimeReport/" +
						StateParameters.itemId + "/" +
						moment($scope.calendarDates.s).format("YYYY-MM-DD") + "/" +
						moment($scope.calendarDates.e).format("YYYY-MM-DD")
					);
				}, StateParameters.ViewId);
			}

			$scope.header = {
				title: (StateParameters.title ? StateParameters.title : StateParameters.name) + ' - ' +
					CalendarService.formatDate($scope.calendarDates.s) + ' - ' +
					CalendarService.formatDate($scope.calendarDates.e),
				icons: [{
					icon: 'date_range',
					onClick: function () {
						let model = angular.copy($scope.calendarDates);
						let params = {
							title: 'WORKINGTIME_TRACKING_CHANGE_DATESPAN',
							inputs: [
								{
									type: 'daterange',
									label: 'WORKINGTIME_TRACKING_DATE',
									modelStart: 's',
									modelEnd: 'e'
								}],
							buttons: [
								{
									type: 'secondary',
									text: 'MSG_CANCEL'
								},
								{
									type: 'primary',
									text: 'WORKINGTIME_TRACKING_SELECT',
									onClick: function () {
										$scope.calendarDates = model;
										startDate = moment(model.s);
										endDate = moment(model.e);

										return HourAccumulationService.getAccumulationData(
											StateParameters.process,
											StateParameters.itemId,
											StateParameters.title_column,
											startDate,
											endDate,
											StateParameters.useActuals == 1
										).then(function (accumulation) {
											setHeader();
											initializeData(accumulation);
										});
									}
								}]
						};

						MessageService.dialog(params, model);
					}
				}]
			};

			if (WorkTimeRights.download && StateParameters.useActuals != 1) ViewService.addDownloadable("HOURACCUMULATION_DOWNLOAD", function () {
				if ($scope.groupByTask) {
					MessageService.msgBox("This feature is only available when group by tasks is disabled.");
					return false;
				}
				let c = "";

				_.each($scope.reportRows, function (user) {
					if (user.item_id) c += user.item_id + ",";
				});
				if (c != "") {
					c = c.substring(0, c.length - 1);

					let bgParams = {};
					bgParams["FileName"] = "Timesheet status";
					bgParams["Process"] = "worktime_tracking";
					bgParams["v"] = 3;
					bgParams["To"] = moment($scope.calendarDates.e).format("YYYY-MM-DD");
					bgParams["From"] = moment($scope.calendarDates.s).format("YYYY-MM-DD");
					bgParams["Ids"] = c;
					bgParams["Pid"] = StateParameters.itemId;

					return BackgroundProcessesService.executeOnDemand("WorktimeExcelBg", bgParams);

				}
			}, StateParameters.ViewId);

		}

		let previousData;

		function initializeData(data) {
			$scope.loading = true;
			let allocationData;
			let reportData;

			if (data) {
				if ($scope.groupByTask) {
					allocationData = data.taskAllocation;
				} else {
					allocationData = data.userAllocation;
				}
				reportData = data.report;
			}


			if (typeof allocationData === 'undefined' || typeof reportData === 'undefined') {
				if ($scope.groupByTask) {
					allocationData = previousData.taskAllocation;
				} else {
					allocationData = previousData.userAllocation;
				}
				reportData = previousData.report;
			} else {
				previousData = data;
			}

			$scope.barChartLabels = [];
			$scope.barChartData = [[], []];
			$scope.columns = [];
			$scope.reportRows = [];
			$scope.reportTotalHours = 0;
			$scope.reportVSHours = 0;
			$scope.spRelation.total = 0;

			let manipulator;
			if ($scope.selectedCalendarResolution === 'months') {
				manipulator = 'month'
			} else if ($scope.selectedCalendarResolution === 'quarter') {
				manipulator = 'quarter'
			} else if ($scope.selectedCalendarResolution === 'year') {
				manipulator = 'year'
			} else {
				manipulator = 'isoWeek'
			}

			let dateRunner = startDate.clone();
			let previousYear = undefined;
			while (dateRunner.isSameOrBefore(endDate)) {
				let sd = dateRunner.clone().startOf(manipulator);
				let currentYear = sd.year();

				let column = {
					start: sd.toDate(),
					end: dateRunner.clone().endOf(manipulator).toDate(),
					name: formatDate(sd),
					total: 0
				};

				if (previousYear !== currentYear) {
					previousYear = currentYear;
					column.year = currentYear;
				}

				$scope.columns.push(column);
				$scope.barChartLabels.push(column.name);

				let matchingAllocations = _.filter(allocationData, function (a) {
					return moment(a.date).isBetween(column.start, column.end, undefined, '[]');
				});

				let total = _.sumBy(matchingAllocations, 'hours');

				$scope.barChartData[1].push(Common.rounder(total, 2));
				$scope.spRelation.total += total;

				dateRunner.add(1, $scope.selectedCalendarResolution);
			}

			let groupedReportRows = _.groupBy(_.sortBy(reportData, ['task_name'], ['asc']),
				$scope.groupByTask ? 'task_item_id' : 'user_item_id');

			if (!$scope.groupByTask) {
				_.each(AllUsersWithTasks.responsibles, function (id) {
					if (!groupedReportRows.hasOwnProperty(id)) groupedReportRows[id] = [];
				});
			}

			addTasksWithNoHours(angular.copy(groupedReportRows))

			function addTasksWithNoHours(copyOfReportRows) {
				if (!$scope.groupByTask) {
					_.each(copyOfReportRows, function (tasks, userItemId) {
						if (!usersAndNames.hasOwnProperty(userItemId)) {
							usersAndNames[userItemId] = tasks.length > 0 ? tasks[0].responsible_name : "";
						}
						_.each(AnalysisData, function (a) {


							_.each(a.TaskResponsibles, function (responsibleId) {

								if (!groupedReportRows.hasOwnProperty(responsibleId)) groupedReportRows[responsibleId] = [];

								if (responsibleId == userItemId && _.findIndex(tasks, function (o) {
									return o.task_item_id == a.ItemId
								}) == -1) {
									groupedReportRows[responsibleId].push({
										task_name: a.Title,
										hours_sum: 0,
										task_item_id: _.find(AnalysisData, function (e) {
											return e.Title == a.Title
										}).ItemId,
										user_item_id: responsibleId,
										responsible_name: _.find(AllUsersWithTasks.users, function (x) {
											return x.item_id == responsibleId
										}).primary
									});

								}
							})
						});
					});
				} else {
					_.each(AnalysisData, function (task) {
						if (!groupedReportRows.hasOwnProperty(task.ItemId) && task.TaskResponsibles.length > 0) {
							groupedReportRows[task.ItemId] = [];
							_.each(task.TaskResponsibles, function (userItemId) {
								groupedReportRows[task.ItemId].push({
									user_item_id: userItemId,
									responsible_name: _.find(AllUsersWithTasks.users, function (x) {
										return x.item_id == userItemId
									}).primary,
									hours_sum: 0,
									task_name: task.Title,
									task_item_id: _.find(AnalysisData, function (e) {
										return e.Title == task.Title
									}).ItemId
								})
							});
						}
					})
				}
			}

			function getBothAllocationData(task_item_id, resource_item_id) {
				let result = 0;

				_.each(_.filter(AccumulationData.bothAllocation, function (r) {
					return r.task_item_id == task_item_id && r.resource_item_id == resource_item_id;
				}), function (c) {
					if (moment(c.date).isBetween($scope.calendarDates.s, $scope.calendarDates.e, undefined, '[]'))
						result += c.hours;
				});

				return result;
			}

			function getRowName(parent, data) {
				let result = data[parent][0];
				if (result) {
					if ($scope.groupByTask) {
						return result.task_name;
					} else {
						return result.responsible_name;
					}
				}
				return parent;
			}

			function getRowId(parent, data) {
				let result = data[parent][0];
				if (result) {
					if ($scope.groupByTask) {
						return result.task_item_id;
					} else {
						return result.user_item_id;
					}
				}


				return parseInt(parent);
			}

			//Add items that have no time charged against them -- only planned
			if ($scope.groupByTask) {
				parentItemIds = [];
				subPhases = [];

				let uniquetasks = [];
				_.each(AnalysisData, function (row, key) {
					if (row.TaskType == 21 || (row.TaskType == 1 && row.ParentType != 21) || (row.TaskType == 2 && row.ParentType != 0)) {
						uniquetasks.push(key)
						if (row.TaskType == 2 && row.ParentType != 0) {
							tasksToBeRemoved.push(row.ItemId);
						}
					}
					if (row.TaskType == 2) {
						parentItemIds.push(key);
					}
				});
				_.each(uniquetasks, function (u) {
					let f = false;
					_.each(groupedReportRows, function (value, key) {
						if (u == key) f = true;
					});
					if (!f) {
						groupedReportRows[u] = [];
					}
				});
			} else {
				let uniqueusers = _.uniqBy(allocationData, function (a) {
					return a.resource_item_id
				});
				_.each(uniqueusers, function (u) {
					let f = false;
					_.each(groupedReportRows, function (value, key) {
						if (u.resource_item_id == key) f = true;
					});
					if (!f) {
						groupedReportRows[u.resource_item_id] = [];
					}
				});
			}

			if ($scope.groupByTask) {
				let taskUserCombos = {};
				_.each(AccumulationData.bothAllocation, function (a) {
					let key = a.task_item_id + '_' + a.resource_item_id;
					if (!taskUserCombos.hasOwnProperty(key)) taskUserCombos[key] = {
						task_item_id: a.task_item_id,
						resource_item_id: a.resource_item_id
					};
				})
				_.each(angular.copy(groupedReportRows), function (tasks, taskItemId) {
					let byTask = _.filter(taskUserCombos, function (x) {
						return x.task_item_id == taskItemId
					});
					_.each(byTask, function (bt) {

						//jos takista ei löydy bt.resource_item_id niin lisätään
						if (_.findIndex(tasks, function (o) {
							return o.user_item_id == bt.resource_item_id;
						}) == -1) {
							groupedReportRows[taskItemId].push({
								hours_sum: 0,
								responsible_name: _.find(AllUsersWithTasks.users, function (x) {
									return x.item_id == bt.resource_item_id
								}).primary,
								task_item_id: taskItemId,
								user_item_id: bt.resource_item_id,
							})
						}
					})

				});
			}

			$scope.reportRows = _.flatten(_.map(groupedReportRows, function (children, parent) {
				let rows = [];

				let ap = AnalysisData.hasOwnProperty(parseInt(parent)) ? AnalysisData[parent] : undefined;

				let userRow = {
					name: getRowName(parent, groupedReportRows),
					parentItemId: ap ? ap.ParentItemId : 0,
					parent: true,
					total: 0,
					vs: 0,
					item_id: getRowId(parent, groupedReportRows),
					columns: _.times($scope.columns.length, _.constant(0)),
					type: 'task',
					order: ap ? ap.OrderNo : '',
					recursionId: undefined,
					deleted: false
				};
				userRow.recursionId = userRow.parentItemId;

				rows.push(userRow);

				_.each(children, function (child) {
					let reportRows = child.hasOwnProperty('days_and_hours') ? JSON.parse(child.days_and_hours).root.wth2 : [];
					if (!Array.isArray(reportRows)) reportRows = [reportRows];

					let taskRow = {
						name: child[$scope.groupByTask ? 'responsible_name' : 'task_name'],
						total: child.hours_sum,
						type: 'user',
						vs: $scope.groupByTask ? getBothAllocationData(parent, child.user_item_id) : getBothAllocationData(child.task_item_id, parent),
						columns: _.times($scope.columns.length, _.constant(0)),
						parentItemId: AnalysisData.hasOwnProperty(parseInt(parent)) ? AnalysisData[parent].ParentItemId : 0,
						recursionId: userRow.item_id,
						order: child[$scope.groupByTask ? 'responsible_name' : 'task_name'],
						deleted: child.deleted == 1,
						item_id: child.user_item_id
					};
					if (child.deleted == 1) userRow.deleted = true;
					userRow.vs += taskRow.vs;

					for (let i = 0, l = $scope.columns.length; i < l; i++) {
						let column = $scope.columns[i];


						let rowsInRange = _.filter(reportRows, function (r) {
							return r && moment(r['@date']).isBetween(column.start, column.end, undefined, '[]');
						});

						let totalRowHours = _.sumBy(rowsInRange, function (h) {
							return parseFloat(h['@hours']);
						});
						taskRow.columns[i] += totalRowHours;
						userRow.columns[i] += totalRowHours;
						column.total += totalRowHours;
						userRow.total += totalRowHours;
					}

					rows.push(taskRow);
				});

				$scope.reportTotalHours += userRow.total;
				$scope.reportVSHours += userRow.vs;
				return rows;
			}));

			if ($scope.groupByTask) {
				setParents(_.sortBy($scope.reportRows, [function (o) {
					return o.parentItemId;
				}]));

				_.each($scope.reportRows, function (item) {
					item.level = parentChain(item, 0, $scope.reportRows);
				});

				_.each($scope.reportRows, function (item) {
					if (item.level == 0 && item.phase) fixPhaseNumbers(item, undefined, $scope.reportRows);
				});
				_.each($scope.reportRows, function (item) {
					if (item.level == 0 && item.phase) fixVSNumbers(item, undefined, $scope.reportRows);
				});
			}

			_.each($scope.columns, function (column) {
				$scope.barChartData[0].push($scope.fn(column.total));
				$scope.barChartColors.push(barChartColors)
			});

			$scope.spRelation.total = $scope.reportVSHours; //make same
			$scope.spRelation.plannedRatio = Math.round($scope.reportTotalHours / $scope.spRelation.total * 100);
			$scope.sbRelation.plannedRatio = Math.round($scope.reportTotalHours / $scope.sbRelation.total * 100);
			$scope.reportVSHours = Common.formatNumber($scope.reportVSHours);
			$scope.reportTotalHours = Common.formatNumber($scope.reportTotalHours);
			$scope.spRelation.total = Common.formatNumber($scope.spRelation.total, 0);

			if (!$scope.budgetTotal) $scope.budgetTotal = Common.formatNumber($scope.sbRelation.total, 0);

			//$scope.sbRelation.total = Common.formatNumber($scope.sbRelation.total, 0);

			function isNumeric(value) {
				return /^-{0,1}\d+$/.test(value);
			}

			function orderTree(parent_id, finalOrder, reportRows) {
				let s = _.filter($scope.reportRows, function (o) {
					return o.recursionId == parent_id
				});
				_.each(s, function (item) {
					finalOrder.push(angular.copy(item));
					orderTree(item.item_id, finalOrder, $scope.reportRows);
				});
			}

			//Find out all names
			let promises = [];
			_.each($scope.reportRows, function (item, i) {
				if (isNumeric(item.name)) {
					if ($scope.groupByTask) {
						promises.push(
							ProcessService.getItem("task", item.name).then(function (t) {
								item.name = t.title;
							})
						);
					} else {
						promises.push(
							UserService.getUser(item.name).then(function (u) {
								item.name = u.primary;
							})
						);
					}
				}
			});
			$q.all(promises).then(function () {
				//Ordering
				let finalOrder = [];
				if ($scope.groupByTask && $scope.showPhases) {
					$scope.reportRows = _.orderBy($scope.reportRows, ['order'], ['asc']);

					_.each(_.filter($scope.reportRows, function (o) {
						return o.level == 0
					}), function (item) {
						finalOrder.push(angular.copy(item));
						orderTree(item.item_id, finalOrder, $scope.reportRows);
					});
					_.each(finalOrder, function (r) {
						if (r.item_id && r.type == 'user') {
							let user = _.find(AllUsersWithTasks.users, function (o) {
								return o.item_id == r.item_id;
							});
							if(user)r.name = user.primary;
							if(user)r.$$secondary = user.secondary;
						} else {
							r.$$secondary = "";
						}
					})
					$scope.reportRows = finalOrder;
				} else if ($scope.groupByTask) {
					//No order
				} else {
					$scope.reportRows = _.orderBy($scope.reportRows, ['name'], ['asc']);

					_.each(_.filter($scope.reportRows, function (o) {
						return o.recursionId == 0
					}), function (item) {
						finalOrder.push(angular.copy(item));

						_.each(_.filter($scope.reportRows, function (o) {
							return o.recursionId == item.item_id
						}), function (subitem) {
							finalOrder.push(angular.copy(subitem));
						});
					});
					_.each(finalOrder, function (r) {
						if (r.item_id && r.type == 'task' && r.parent) {
							let user = _.find(AllUsersWithTasks.users, function (o) {
								return o.item_id == r.item_id;
							});
							if (typeof user != 'undefined') {
								r.name = user.primary;
								r.$$secondary = user.secondary
							}

						} else {
							r.$$secondary = "";
						}
					})
					$scope.reportRows = finalOrder;
				}
				$scope.loading = false;
			});
		}
	}
})();