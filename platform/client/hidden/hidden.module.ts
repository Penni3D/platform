﻿(function () {
	'use strict';

	angular
		.module('core.hidden', [])
		.config(HiddenConfig);

	HiddenConfig.$inject = ['ViewsProvider'];
	function HiddenConfig(ViewsProvider) {
		ViewsProvider.addView('hidden', {
			resolve: {
				Status: function (MessageService, $q) {
					var d = $q.defer();
					var confirmed = false;

					MessageService.confirm(
						'You are entering to a page that is restricted to admins only. ' +
						'Should you not the enter the page, please cancel the action',
						function () {
							confirmed = true;
						})
					.then(function () {
						if (confirmed) {
							d.resolve();
						} else {
							d.reject();
						}
					});
					return d.promise;
				}
			}
		});
	}
})();


