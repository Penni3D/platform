(function () {
	'use strict';

	angular
		.module('core.hidden')
		.config(HiddenRestTesterConfig)
		.controller('HiddenRestTesterController', HiddenRestTesterController)
		.service('HiddenRestTesterService', HiddenRestTesterService);

	HiddenRestTesterConfig.$inject = ['ViewsProvider'];
	function HiddenRestTesterConfig(ViewsProvider) {
		ViewsProvider.addView('hidden.resttester', {
			controller: 'HiddenRestTesterController',
			template: 'hidden/restTester/rest-tester.html',
			resolve: {
			}
		});
	}

	HiddenRestTesterService.$inject = ['$http', '$q'];
	function HiddenRestTesterService($http, $q){
		
		var actionTypes = {
			CUSTOM: 'custom',
			HTTP: 'http'
		};
		
		var requestsByCategory = {};
		var testSequences = {};

		var storedVariables = {};

		var defaultRequestItem = {
			validate: function(data){
				return data && data.status === 200;
			}
		};

		function checkRequiredFields(data, itemToSend){
			if(!itemToSend.requiredFields){
				return true;
			}
			_.each(itemToSend.requiredFields.arrays, function(requiredField){
				var item = _.get(data.data, requiredField);
				if(!_.isArray(item)){
					data.$$errors.push({ message: 'Expected ' + requiredField + ' to be array'});
				}
			});
			_.each(itemToSend.requiredFields.objects, function(requiredField){
				var item = _.get(data.data, requiredField);
				if(!_.isObject(item)){
					data.$$errors.push({ message: 'Expected ' + requiredField + ' to be object'});
				}
			});
			if(_.size(data.$$errors) > 0) {
				data.$$error = true;
			}
		}

		var sendRequest = function(requestItem, deferred) {
			var isHttp = (requestItem.actionType !== actionTypes.CUSTOM);
			var itemToSend = _.extend({}, defaultRequestItem, requestItem);
			var returnedData =  null;
			var deferred = deferred || $q.defer();
			var request;
			if (!isHttp){
				request = requestItem.execute();
			} else {
				var path;
				if(_.isFunction(itemToSend.path)){
					path = itemToSend.path();
				} else {
					path = itemToSend.path;
				}
				
				var payload = itemToSend.payload;
				if(_.isFunction(payload)) {
					payload = payload();
				}
				request = $http({
					method: itemToSend.method,
					url: path,
					data: payload
				})
			}
			
			request.then(function(data){
				data.$$errors = [];
				var obj = isHttp ? data.data : data;
				_.each(itemToSend.pickVariables, function(path, name){
					var propertyName = itemToSend.category + '.' + name;
					if(_.isString(path)){
						_.set(storedVariables, propertyName, _.get(obj, path));
					} else if(_.isFunction(path)) {
						_.set(storedVariables, propertyName, path(obj));
					}
				});
				var result = itemToSend.validate(data, false);
				if(result !== true) {
					data.$$error = true;
					data.$$errors.push({message: result});
				}
				checkRequiredFields(data, itemToSend);
				returnedData = data;
			})
			.catch(function(data, a){
				data.$$errors = [];
				var result = itemToSend.validate(data, true);
				if(result !== true) {
					data.$$error = true;
					data.$$errors.push({message: result});
				}
				checkRequiredFields(data, itemToSend);
				returnedData = data;
			})
			.finally(function(){
				requestItem.responses = requestItem.responses || [];
				requestItem.responses.push(returnedData);
				deferred.resolve();
			});
			return deferred.promise;
		};
		
		var addRequest = function(category, name, requestItem){
			requestsByCategory[category] = requestsByCategory[category] || {};
			requestsByCategory[category][name] = requestItem;
		};

		var getRequestItems = function(){
			return requestsByCategory;
		};

		var getVariables = function(category, name){
			if(!category){
				return storedVariables;
			} else {
				return storedVariables[category] || {};
			}
		};

		var runSequence = function(category, name){
			var requestItems = getRequestItems();
			if(!testSequences[category] || !testSequences[category][name]){
				throw "Sequence " + name + " does not exist";
			}
			var deferred = $q.defer();
			var sequencePromise = deferred.promise;
			var seq = testSequences[category][name];
			// clean errors and return requests
			var requests = _.map(seq.items, function(requestName){
				
				var item = requestItems[category][requestName];
				return item;
			});
			var first = _.head(requests);
			var theRest = _.without(requests, first);
			_.reduce(theRest, function(promise, req){
				return promise.then(function(){
					return sendRequest(req);
				})
			},
				sendRequest(first));
		};

		var addTestSequence = function(category, name, items){
			testSequences[category] = testSequences[category] || {};
			if(testSequences[category] && testSequences[category][name]){
				throw "Sequence " + name + " already exists";
			}
			testSequences[category][name] = {
				items: items,
				status: 0,
				results: [],
				errors: []
			};
		};

		var getSequences = function(){
			return testSequences;
		};

		return {
			addRequest: addRequest,
			getRequestItems: getRequestItems,
			getVariables: getVariables,
			getSequences: getSequences,
			addTestSequence: addTestSequence,
			runSequence: runSequence,
			sendRequest: sendRequest,
			actionType: actionTypes
			
		};
	}

	HiddenRestTesterController.$inject = ['$scope', 'HiddenRestTesterService'];
	function HiddenRestTesterController($scope, HiddenRestTesterService) {
		$scope.visibleCategories = {};
		$scope.sendRequest = function(requestItem){
			HiddenRestTesterService.sendRequest(requestItem);
		};
		
		$scope.runSequence = function(category, sequence){
			HiddenRestTesterService.runSequence(category, sequence);
		};

		$scope.joinArrayItems = function(items){
			return _.join(items, ', ');
		};

		$scope.requestItemsByCategory = HiddenRestTesterService.getRequestItems();
		$scope.variables = HiddenRestTesterService.getVariables();
		$scope.sequences = HiddenRestTesterService.getSequences();
	}

})();
