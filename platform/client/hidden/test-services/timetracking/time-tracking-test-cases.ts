﻿(function () {
	'use strict';

	angular.module('core.hidden').run(TimeTrackingRestTestCasesRestTestCases);

	TimeTrackingRestTestCasesRestTestCases.$inject = ['HiddenRestTesterService'];
	function TimeTrackingRestTestCasesRestTestCases(HiddenRestTesterService) {
		var baseUrl = '/model/process/timetracking/';
		var items = {
			getMyTaskList: {
				category: 'timetracking',
				title: 'GetListOfTasks',
				actionType: HiddenRestTesterService.actionType.HTTP,
				method: 'get',
				path: function(){
					return baseUrl;
				},
				validate: function (response) {
					if (response.status !== 200){
						return "Should not fail";
					}
					return true;
				}
			},
			insertTaskForMyList: {
				category: 'timetracking',
				title: 'Add New Task For My List',
				actionType: HiddenRestTesterService.actionType.HTTP,
				method: 'post',
				path: function(){
					return baseUrl;
				},
				payload: function(){
					var payload = {
						Hours: 2,
						UserItemId: 1
					};
					return payload;
				},
				validate: function (response) {
					if (response.status !== 200){
						return "Should not fail";
					}
					return true;
				}
			}
		};

		_.each(items, function(item, name){
			HiddenRestTesterService.addRequest('timetracking', name, item);
		});
	}
})();