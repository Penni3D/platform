﻿(function () {
	'use strict';

	angular.module('core.hidden').run(TaskRestTestCases);

	TaskRestTestCases.$inject = ['HiddenRestTesterService'];
	function TaskRestTestCases(HiddenRestTesterService) {
		var firstDate = '2017-10-04', secondDate = '2017-10-06';
		var firstHours = 7.5, secondHours = 4;
		var taskItemId = 5065;
		var items = {
			insertTaskHours: {
				category: 'tasks',
				title: 'Post hours for task',
				method: 'put',
				payload: [{
					"resource_item_id": 304,
					"task_item_id": taskItemId,
					"hours": firstHours,
					"date": firstDate
				}, {
					"resource_item_id": 304,
					"task_item_id": taskItemId,
					"hours": secondHours,
					"date": secondDate
				}],
				path: '/model/process/Durations/task/dates',
				pickVariables: {
					taskItemId: 'task_item_id',
					addedTask: function (data) {
						return data.data;
					}
				}
			},
			updateInsertedTask: {
				category: 'tasks',
				title: 'Update Task Hours (pretty much same as insert)',
				method: 'put',
				path: '/model/process/Durations/task/dates',
				payload: [{
					"resource_item_id": 304,
					"task_item_id": taskItemId,
					"hours": firstHours + 2,
					"date": firstDate
				}, {
					"resource_item_id": 304,
					"task_item_id": taskItemId,
					"hours": secondHours + 2,
					"date": secondDate
				}]
			},
			deleteTaskFromUser: {
				category: 'tasks',
				title: 'Remove all hours from tasks for user',
				method: 'delete',
				path: '/model/process/Durations/task/' + taskItemId + '/resource/' + 304
			},
			getTasks: {
				category: 'tasks',
				title: 'Get By Date And By Task',
				method: 'get',
				payload: '',
				path: '/model/process/Durations/bydate/bytask/' + taskItemId,
				validate: function (response, fromCatch) {
					if (_.isEmpty(response) || response.status !== 200){
						return false;
					}
					return true;
				},
				requiredFields: {
					arrays: ['rowdata'],
					objects: ['dates']
				}
			}
		};

		_.each(items, function(item, name){
			HiddenRestTesterService.addRequest('tasks', name, item);
		});

		HiddenRestTesterService.addTestSequence('tasks', 'TaskFlow', ['insertTaskHours', 'updateInsertedTask', 'getTasks']);
	}
})();