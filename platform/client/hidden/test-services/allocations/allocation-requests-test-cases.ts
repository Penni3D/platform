(function () {
	'use strict';

	angular.module('core.hidden').run(AllocationRequestsRestTestCases);

	AllocationRequestsRestTestCases.$inject = ['HiddenRestTesterService'];
	function AllocationRequestsRestTestCases(HiddenRestTesterService) {
		var items = {
            
        };

		_.each(items, function(item, name){
			HiddenRestTesterService.addRequest('allocationRequests', name, item);
		});
	}
})();