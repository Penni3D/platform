﻿(function () {
	'use strict';

	angular.module('core.hidden').run(AllocationRestTestCases);

	AllocationRestTestCases.$inject = ['HiddenRestTesterService', 'AllocationService'];
	function AllocationRestTestCases(HiddenRestTesterService, AllocationService) {
		var baseUrl = 'resources/';
		var allocationBaseUrl = baseUrl + 'allocation';
		var variablesToPost = {
			resourceItemId: 1,
			competencyItemId: 5127,
			costCenterForCompetency: 5103,
			processItemId: 1756,
			date1: '2017-11-11',
			hours1: 4.5,
			date2: '2017-11-12',
			hours2: 3
			
		};
		var commentTextToAdd = 'This is a new comment';

		var items = {
			insertAllocation: {
				category: 'allocations',
				title: 'Post new allocation',
				actionType: HiddenRestTesterService.actionType.CUSTOM,
				execute: function() {
					return AllocationService.createAllocation(
						variablesToPost.resourceItemId,
						variablesToPost.processItemId,
						'This is title',
						[
							{ "hours": variablesToPost.hours1, "date": variablesToPost.date1},
							{ "hours": variablesToPost.hours2, "date": variablesToPost.date2}
						]
					)
				},
				pickVariables: {
					allocationItemId: 'item_id',
					allocationItem: function(data){
						return data;
					}
				},
				validate: function (response, error) {
					if (error){
						return 'Should not fail';
					}
					if (response.resource_type !== 'user'){
						return 'Field \'resource_type\' should be \'user\'';
					}
					return true;
				}
			},
			updateDraft: {
				category: 'allocations',
				title: 'Changes title of the allocation',
				method: 'put',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					return allocationBaseUrl + '/' + variables.allocationItemId;
				},
				payload: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					var allocationItem = variables.allocationItem;
					allocationItem.title = 'New Title';
					return allocationItem;
				},
				validate: function(response){
					if(response.data.title !== 'New Title'){
						return 'Title should have changed';
					}
					return true;
				}
			},
			setAllocationStatusToRequested: {
				category: 'allocations',
				title: 'Changes status of allocation from draft to requested',
				method: 'put',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					return allocationBaseUrl + '/' + variables.allocationItemId;
				},
				payload: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					var allocationItem = variables.allocationItem;
					allocationItem.status = 20;
					return allocationItem;
				},
				validate: function(response){
					if(response.data.status !== 20){
						return 'Should have status 20';
					}
					return true;
				}
			},
			setAllocationStatusToApproved: {
				category: 'allocations',
				title: 'Changes status of allocation from requested to approved',
				method: 'post',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					return allocationBaseUrl +'/' + variables.allocationItemId + '/status/' + 50;
				}
			},
			addComment: {
				category: 'allocations',
				title: 'Adds comment to allocation',
				method: 'put',
				payload: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					var allocation = variables.allocationItem;
					allocation.NewComment = commentTextToAdd;
					return allocation;
				},
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					var allocation = variables.allocationItem;
					if(!allocation) {
						throw "Allocation is not inserted";
					}
					return allocationBaseUrl +'/' + allocation.item_id;
				}
			},
			updateDurationsOfAllocation: {
				category: 'allocations',
				title: 'Update hours for allocation',
				method: 'put',
				path: allocationBaseUrl + '/dates',
				payload: function() {
					var variables = HiddenRestTesterService.getVariables('allocations');
					var allocation = variables.allocationItem;
					var item = [
						{
							"allocation_item_id": allocation.item_id,
							"date": "2017-10-01",
							"hours": 8
						},
						{
							"allocation_item_id": allocation.item_id,
							"date": "2017-10-02",
							"hours": 9
						}
					];
					return item;
				}
			},
			getAllocationWithCommentsAndDurations: {
				category: 'allocations',
				title: 'Get posted allocation and ensure all the previous updates to be saved',
				method: 'get',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					var allocation = variables.allocationItem;
					if(!allocation) {
						throw "Allocation is not inserted";
					}
					return allocationBaseUrl +'/' + allocation.item_id;
				},
				validate: function(response, fromCatch) {
					if(fromCatch) {
						return 'Request should not fail';
					}
					var data = response.data;
					
					var hours = _.map(data.durations, 'hours');
					var lastComment = _.last(data.comments);
					if(!(_.includes(hours, 8) && _.includes(hours, 9))) {
						return 'Hours should be updated';
					}
					return true;
				}
			},
			deleteStoredAllocation: {
				category: 'allocations',
				title: 'Delete Stored Allocation',
				method: 'delete',
				payload: '',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					return  allocationBaseUrl +'/' + variables.allocationItemId;
				}
			},
			insertAllocationRequestForCompetency: {
				category: 'allocations',
				title: 'Request competency',
				method: 'post',
				payload: {
					"title": "Competency needed!",
					"resource_item_id": variablesToPost.competencyItemId,
					"process_item_id": variablesToPost.processItemId,
					"durations": [
						{
							"hours" : variablesToPost.hours1,
							"date" : variablesToPost.date1

						},
						{
							"hours" : variablesToPost.hours2,
							"date" : variablesToPost.date2
						}
					]
				},
				path: allocationBaseUrl,
				pickVariables: {
					competencyRequestItemId: 'item_id',
					competencyRequestItem: function(data){
						return data;
					}
				},
				validate: function (response) {
					if (response.status !== 200){
						return 'Should not fail';
					}
					if (response.data.resource_type !== 'competency'){
						return 'Should not fail';
					}
					return true;
				}
			},
			updateAllocationWithWrongCostCenterForCompetencyShouldFail: {
				category: 'allocations',
				title: 'Try to set competency request from draft to requested (wrong cost center id)',
				method: 'put',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					return allocationBaseUrl + '/' + variables.competencyRequestItemId;
				},
				payload: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					var competencyRequestItem = variables.competencyRequestItem;
					competencyRequestItem.title = 'Becoming a request';
					competencyRequestItem.cost_center = variablesToPost.processItemId;
					competencyRequestItem.status = 20;
					return competencyRequestItem;
				},
				validate: function (response) {
					if (response.status === 200){
						return "Should fail";
					}
					return true;
				}
			},
			updateAllocationForCompetencyShouldSucceed: {
				category: 'allocations',
				title: 'Try to set competency request from draft to requested',
				method: 'put',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					return allocationBaseUrl + '/' + variables.competencyRequestItemId;
				},
				payload: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					var competencyRequestItem = variables.competencyRequestItem;
					competencyRequestItem.title = 'Becoming a request';
					competencyRequestItem.cost_center = variablesToPost.costCenterForCompetency;
					competencyRequestItem.status = 20;
					return competencyRequestItem;
				},
				validate: function (response) {
					if (response.status !== 200){
						return "Should not fail";
					}
					if (response.data.resource_type !== 'competency'){
						return "Should be competency";
					}
					if (response.data.cost_center !== variablesToPost.costCenterForCompetency){
						return "Should have correct cost center value";
					}
					return true;
				}
			},
			
			updateCostCenterOfCompetencyRequest: {
				category: 'allocations',
				title: 'Change cost center of the inserted request',
				method: 'put',
				path: function () {
					var variables = HiddenRestTesterService.getVariables('allocations');
					return allocationBaseUrl + '/' + variables.competencyRequestItemId;
				},
				payload: function () {
					var variables = HiddenRestTesterService.getVariables('allocations');
					var competencyRequestItem = {
						cost_center: variablesToPost.costCenterForCompetency
					};
					return competencyRequestItem;
				},
				validate: function (response) {
					if (response.status !== 200) {
						return "Should not fail";
					}
					if (response.data.cost_center !== variablesToPost.costCenterForCompetency) {
						return "Should have correct cost center value";
					}
					return true;
				}
			},
			
			getAllocations: {
				category: 'allocations',
				title: 'Get Allocations',
				method: 'get',
				payload: '',
				path: baseUrl + 'bydate',
				validate: function (response, fromCatch) {
					if (_.isEmpty(response) || response.status !== 200){
						return false;
					}
					return true;
				},
				requiredFields: {
					arrays: ['rowdata'],
					objects: ['dates']
				},
				pickVariables:{
					items: function(data){
						return _.filter(data.data.rowdata, function(item){
							return item.process === 'allocation';
						});
					}
				}
			},
			deleteCompetencyRequest: {
				category: 'allocations',
				title: 'Delete Stored Competency Allocation Request',
				method: 'delete',
				payload: '',
				path: function(){
					var variables = HiddenRestTesterService.getVariables('allocations');
					return  allocationBaseUrl +'/' + variables.competencyRequestItemId;
				}
			},
			getUsersInCostCenters: {
				category: 'allocations',
				title: 'Get Cost Centers',
				method: 'get',
				path: allocationBaseUrl + '/costcenters',
				validate: function(response){
					var data = response.data;
					if(!_.isArray(data)) {
						return 'Response should be an array';
					}
					return true;
				}
			},
			getAllocatedUsersForProcess: {
				category: 'allocations',
				title: 'Get Allocations For Process',
				method: 'get',
				path: baseUrl + 'bydate/byprocess/' + variablesToPost.processItemId,
				validate: function(response){
					if(!_.has(response, 'data.dates') || !_.has(response, 'data.rowdata')) {
						return 'Should have properties \'dates\' and \'rowdata\'';
					}
					var data = response.data;
					return true;
				}
			}
		};

		_.each(items, function(item, name){
			HiddenRestTesterService.addRequest('allocations', name, item);
		});
		
		HiddenRestTesterService.addTestSequence('allocations', 'AllocationFlow', [
			'insertAllocation',
			'setAllocationStatusToRequested',
			'addComment',
			'updateDurationsOfAllocation',
			'getAllocationWithCommentsAndDurations',
			'deleteStoredAllocation',
			'insertAllocationRequestForCompetency',
			'updateAllocationWithWrongCostCenterForCompetencyShouldFail',
			'updateAllocationForCompetencyShouldSucceed',
			'deleteCompetencyRequest'
		]);
	}
})();