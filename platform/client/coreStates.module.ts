﻿(function () {
	'use strict';

	angular
		.module('core.states', [
			'core.dialogs',
			'core.process',
			'core.processes',
			'core.features',
			'core.pages',
			'core.widgets',
			'core.hidden',
			'core.allocation',
			'core.graphs',
			'core.rowmanager',
			'core.settings',
			'customer'
			]);

})();