﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.specialProcesses;
using Microsoft.AspNetCore.Http;
using User = Keto5.x.platform.server.processes.specialProcesses.User;

namespace Keto5.x.platform.server.security {
	public partial class Authentication : ConnectionBaseProcess {
		private User authenticatedUser;
		private State state;

		public Authentication(string instance) :
			base(instance, "user", new AuthenticatedSession(instance, 0, null)) { }

		public string getDomainCheck() {
			return
				"CASE WHEN domain = 'default' THEN '' ELSE LOWER(ISNULL(domain,'')) END = @domain";
		}

		public void SendResetPassword(int itemId, string host, bool reset = true) {
			var u = GetUser("", itemId, instance);
			SendResetPassword(u, host, reset);
		}

		public void SendResetPassword(string login, string host, bool reset = true) {
			var u = GetUser("", login, instance);
			SendResetPassword(u, host, reset);
		}

		private void SendResetPassword(DataRow user, string host, bool reset = true) {
			if (user == null) return;
			var address = Conv.ToStr(user["email"]);
			if (!Util.isEmailValid(address)) return;

			var token = Util.CreateToken(32);
			SetDBParam(SqlDbType.NVarChar, "token", token);
			SetDBParam(SqlDbType.NVarChar, "item_id", Conv.ToInt(user["item_id"]));
			var locale = Conv.ToStr(user["locale_id"]);

			var translations = new Translations(instance, locale);
			var subject = translations.GetTranslation((reset ? "RESET" : "INVITE") + "_EMAIL_SUBJECT");
			var body = translations.GetTranslation((reset ? "RESET" : "INVITE") + "_EMAIL_BODY");
			var link = host + "?er=" + token;
			var email = new Email(instance, 0, body + link, subject);

			email.Add(address, Email.AddressTypes.ToAdress);
			email.Send();

			//Set Tokens
			db.ExecuteUpdateQuery("UPDATE _" + instance +
			                      "_user SET password_reset = @token, password_reset_date = getutcdate() WHERE item_id = @item_id",
				DBParameters);
		}

		public string changePasswordWithResetToken(string token, string password) {
			var validation = PasswordValidation.GetPasswordValidationResult(password);
			if (!validation.passwordValid) return "LOGIN_COMPLEXITY_NOT_MET";

			SetDBParam(SqlDbType.NVarChar, "token", token);
			SetDBParam(SqlDbType.NVarChar, "password", HashPbkdf2.CreateHash(password));

			var u = db.GetDataRow("SELECT * FROM _" + instance +
			                      "_user WHERE password_reset = @token AND DATEADD(HOUR,1,password_reset_date) > getutcdate()",
				DBParameters);

			if (u != null) {
				db.ExecuteUpdateQuery("UPDATE _" + instance +
				                      "_user SET password_reset = null, password_reset_date = null, password = @password WHERE password_reset = @token",
					DBParameters);
				return "LOGIN_PASSWORDS_CHANGED";
			}

			return "LOGIN_PASSWORDS_CHANGE_FAILED";
		}

		public List<int> GetUserIdsDelegatedToImpersonateAs(AuthenticatedSession session) {
			SetDBParam(SqlDbType.Int, "item_id", session.item_id);
			var r = new List<int>();
			var sql = "SELECT item_id FROM item_data_process_selections idps " +
			          "WHERE item_column_id = (SELECT item_column_id FROM item_columns WHERE instance = @instance AND process='user' AND name='delegate_users') " +
			          "AND selected_item_id = @item_id";
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				r.Add(Conv.ToInt(row["item_id"]));
			}

			return r;
		}

		public AuthenticationResult authenticateAs(AuthenticatedSession session, int itemid) {
			//Check permission to impersonate - admin
			var sql1 = "SELECT COUNT(*) AS cnt FROM instance_menu im " +
			           "INNER JOIN instance_menu_rights imr ON imr.instance_menu_id = im.instance_menu_id " +
			           "INNER JOIN instance_menu_states ims ON ims.instance_menu_id = im.instance_menu_id " +
			           "INNER JOIN _" + instance +
			           "_user_group ug ON ug.item_id = imr.item_id " +
			           "INNER JOIN item_data_process_selections idps ON idps.item_id = ug.item_id AND idps.item_column_id = " +
			           "(SELECT item_column_id FROM item_columns WHERE process = 'user_group' AND name = 'user_id') " +
			           "WHERE im.default_state = 'impersonate' AND ims.state = 'admin' AND idps.selected_item_id = " +
			           session.item_id;
			var c1 = Conv.ToInt(db.ExecuteScalar(sql1,
				DBParameters));
			var admin = c1 > 0;

			//Check permission to impersonate - delegate
			var sql2 = "SELECT COUNT(*) AS cnt FROM instance_menu im " +
			           "INNER JOIN instance_menu_rights imr ON imr.instance_menu_id = im.instance_menu_id " +
			           "INNER JOIN instance_menu_states ims ON ims.instance_menu_id = im.instance_menu_id " +
			           "INNER JOIN _" + instance +
			           "_user_group ug ON ug.item_id = imr.item_id " +
			           "INNER JOIN item_data_process_selections idps ON idps.item_id = ug.item_id AND idps.item_column_id = " +
			           "(SELECT item_column_id FROM item_columns WHERE process = 'user_group' AND name = 'user_id') " +
			           "WHERE im.default_state = 'impersonate' AND ims.state = 'impersonate' AND idps.selected_item_id = " +
			           session.item_id;
			var c2 = Conv.ToInt(db.ExecuteScalar(sql2,
				DBParameters));
			var delegating = GetUserIdsDelegatedToImpersonateAs(session).Contains(itemid) && c2 > 0;

			if (admin == false && delegating == false) {
				Diag.Log(instance,
					"Impersonation Failed: User '" + session.item_id + "' has no impersonation rights.",
					session.item_id,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return AuthenticationResult.ImpersonateFail;
			}

			//Get user from DB
			SetDBParam(SqlDbType.Int, "item_id", itemid);
			var d = db.GetDataRow("SELECT * FROM " + tableName + " WHERE item_id = @item_id",
				DBParameters);

			if (d == null) {
				Diag.Log(instance,
					"Impersonation Failed: User '" + session.item_id + "' was unable to log in as '" + itemid + "'.",
					session.item_id,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return AuthenticationResult.ImpersonateFail;
			}

			authenticatedUser = new User(instance, (int)d["item_id"]);
			authenticatedUser.FillItem(d);

			Diag.Log(instance,
				"Impersonation Success: User '" + session.item_id + "' has logged in as '" + itemid + "'.",
				session.item_id,
				Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
			return AuthenticationResult.ImpersonateSuccess;
		}

		public enum AuthenticationResult {
			BasicAuthentication_Success = 10,
			BasicAuthentication_LoginEmpty = 11,
			BasicAuthentication_PasswordEmpty = 12,
			BasicAuthentication_LoginNonExistant = 13,
			BasicAuthentication_PasswordNonMatching = 14,
			BasicAuthentication_SecondFactorRequired = 29,
			BasicAuthentication_SecondFactorFailed = 30,
			BasicAuthentication_SecondKetoAuthRequired = 50,
			BasicAuthentication_SecondKetoAuthFailed = 51,
			BasicAuthentication_SecondKetoAuthSuccess = 52,
			BasicAuthentication_LoginTriesExceeded = 9,
			LDAPAuthentication_Success = 15,
			LDAPAuthentication_LoginEmpty = 16,
			LDAPAuthentication_PasswordEmpty = 17,
			LDAPAuthentication_LDAPCheckFailed = 18,
			LDAPAuthentication_LoginNonExistant = 19,
			AzureADAuthentication_Success = 20,
			AzureADAuthentication_Failure = 21,
			AzureADAuthentication_LoginNonExistant = 22,
			AnonymousAuthentication_AccountNotSet = 23,
			AnonymousAuthentication_AccountNotFound = 24,
			AnonymousAuthentication_Success = 25,
			IntegrationAuthentication_KeyInvalid = 26,
			IntegrationAuthentication_LoginFailed = 27,
			IntegrationAuthentication_Success = 28,
			ADFSAuthentication_Success = 31,
			ADFSAuthentication_LoginNonExistant = 32,
			ADFSNoUPNClaimFound = 33,
			ImpersonateSuccess = 34,
			ImpersonateFail = 35,
			GoogleAuthentication_Success = 36,
			GoogleAuthentication_LoginNonExistant = 37,
			GoogleAuthentication_TokenCheckFailed = 38,
			GoogleAuthentication_NoTokenProvided = 39,
			LegacyAuthentication_Success = 40,
			LegacyAuthentication_LoginNonExistant = 41,
			LegacyAuthentication_TokenCheckFailed = 42,
			LegacyAuthentication_NoTokenProvided = 43,
			InstanceNotValid = 98,
			NotImplemented = 99,
			Failed = 100
		}

		public AuthenticationResult LdapAuthentication(string login, string password, string domain) {
			return AuthenticationResult.NotImplemented;
			//authenticatedUser = null;

			//if (login == "") { return AuthenticationResult.LDAPAuthentication_LoginEmpty; }
			//if (password == "") { return AuthenticationResult.LDAPAuthentication_PasswordEmpty; }

			//SetDBParam(SqlDbType.NVarChar, "login", login.ToLower());
			//DataRow d = db.GetDataRow("SELECT * FROM " + tableName + " WHERE LOWER(login) = @login AND active > 0", DBParameters);

			//if (d == null) {
			//	Diag.Log(instance, "LDAP Authentication: Failure -- '" + login + "' does not exist.", 0, Diag.LogSeverities.warning, Diag.LogCategories.Authentication);
			//	return AuthenticationResult.LDAPAuthentication_LoginNonExistant;
			//}

			//bool adtrue = false;
			//try {
			//	PrincipalContext adcheck = new PrincipalContext(ContextType.Domain, Config.GetDomainValue(domain, "LDAPAuthentication", "DomainName"));
			//	adtrue = adcheck.ValidateCredentials(login, password);
			//} catch (Exception ex) {
			//	Diag.Log(instance, "LDAP Authentication: Failure -- '" + login + "' " + ex.Message, 0, Diag.LogSeverities.warning, Diag.LogCategories.Authentication);
			//	adtrue = false;
			//}

			//if (adtrue) {
			//	authenticatedUser = new User(instance, (int)d["item_id"]);
			//	authenticatedUser.FillItem(d);

			//	Diag.Log(instance, "LDAP Authentication: Success -- User '" + login + "'.", (int)d["item_id"], Diag.LogSeverities.message, Diag.LogCategories.Authentication);
			//	return AuthenticationResult.LDAPAuthentication_Success;
			//} else {
			//	Diag.Log(instance, "LDAP Authentication: Failure -- '" + login + "' does not match the password when checking against domain ''.", 0, Diag.LogSeverities.warning, Diag.LogCategories.Authentication);
			//	return AuthenticationResult.LDAPAuthentication_LDAPCheckFailed;
			//}
		}

		public bool WindowsAuthentication(HttpContext Cnt) {
			var identity = Cnt.User.Identity.Name;
			var domain = "";
			var login = "";

			if (identity == null || identity == "") {
				//Windows Authentication was unable to provide users identity
				return false;
			}

			//Separate domain and login
			var identitysplit = identity.Split('\\');
			domain = identitysplit[0];
			login = identitysplit[1];

			//Get instance
			var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
			var instance = conbase.GetInstance(Cnt.Request.Host.Host);

			if (Config.Instances.ContainsKey(instance) == false) {
				return false;
			}

			//Get user
			var d = GetUser(domain, login, instance);

			if (d == null) {
				Diag.Log(instance,
					"Windows Authentication: Failed -- User '" + identity +
					"' does not match any record in the database.", 0,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return false;
			}

			authenticatedUser = new User(instance, (int)d["item_id"]);
			authenticatedUser.FillItem(d);
			Diag.Log(instance, "Windows Authentication: Success -- User '" + identity + "'.", (int)d["item_id"],
				Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
			GrantAccessInHttpContext(instance, login, Cnt, domain);
			return true;
		}


		#region DB Queries

		private DataRow GetUser(int itemId, string instance) {
			var cacheRow = Cache.Get(instance, "authentication", "item_id_" + itemId);
			if (cacheRow != null) return (DataRow)cacheRow;

			SetDBParam(SqlDbType.Int, "item_id", itemId);
			cacheRow = db.GetDataRow(
				"SELECT item_id, token, locale_id, domain, login, timezone, date_format, first_day_of_week FROM _" +
				instance + "_user WHERE item_id = @item_id", DBParameters);

			Cache.Set(instance, "authentication", "item_id_" + itemId, cacheRow);

			return (DataRow)cacheRow;
		}

		private DataRow GetUser(string domain, string login, string instance) {
			if (domain == "") domain = Config.GetDefaultDomain();
			if (domain == "default") domain = "";

			SetDBParam(SqlDbType.NVarChar, "domain", domain.ToLower());
			SetDBParam(SqlDbType.NVarChar, "login", login.ToLower());
			return db.GetDataRow(
				"SELECT * FROM _" + instance +
				"_user WHERE LOWER(login) = @login AND " + getDomainCheck(),
				DBParameters);
		}

		private DataRow GetUser(string domain, int itemId, string instance) {
			SetDBParam(SqlDbType.Int, "item_id", itemId);
			return db.GetDataRow("SELECT * FROM _" + instance + "_user WHERE item_id = @item_id", DBParameters);
		}

		#endregion

		#region Cookies And Tokens

		public bool IsCookieValid(AuthenticatedSession currentSession, HttpContext Cnt) {
			var cookiedata = Conv.ToStr(Cnt.Request.Cookies[SecurityNaming.AuthorisationCookieName]);

			if (cookiedata != "") {
				//Read cookie
				var cookiearray = cookiedata.Split(new[] { '&' });
				var item_id = Conv.ToInt(cookiearray[0].Split(new[] { '=' })[1]);
				var token = Conv.ToStr(cookiearray[1].Split(new[] { '=' })[1]);
				var cookie_instance = Conv.ToStr(cookiearray[2].Split(new[] { '=' })[1]);

				if (Config.Instances.ContainsKey(cookie_instance) == false) {
					Diag.LogToConsole("User is trying to access an unrecognised instance '" + cookie_instance +
					                  "'. This might be because an instance is added into the instance_hosts table and the server has not been restarted or user has an old cookie.",
						Diag.LogSeverities.Warning);
					Diag.Log("?",
						"Cookie validity Check: Failure -- Item Id " + Conv.ToStr(item_id) +
						" is trying to access an unrecognised instance '" + cookie_instance + "'.", 0,
						Diag.LogSeverities.Warning,
						Diag.LogCategories.Authentication);
					return false;
				}

				if (IsCookieTokenValid(item_id, token, Cnt, cookie_instance, currentSession)) return true;

				if (currentSession != null)
					RevokeAccessInHttpContext(Cnt, item_id, currentSession.token);
				
				return false;
			}

			return false;
		}

		public User getUserByIntegrationToken(string key, string token, HttpContext context) {
			key = Conv.ToStr(key);
			token = Conv.ToStr(token);

			SetDBParam(SqlDbType.NVarChar, "key", key.ToLower());
			SetDBParam(SqlDbType.DateTime, "valid", DateTime.Now.ToUniversalTime());
			var userItemId = 0;

			if (Conv.ToInt(db.ExecuteScalar(
				    "SELECT item_id FROM _" + instance +
				    "_user WHERE login_tries > 3 AND DATEADD(minute, 10, last_login_try) > GetUTCDate() AND item_id IN (SELECT user_item_id FROM instance_integration_keys WHERE instance = @instance AND [key] = @key)",
				    DBParameters)) > 0) {
				return new User(instance, 0);
			}

			foreach (DataRow r in db.GetDatatable(
				         "SELECT k.user_item_id, t.token FROM instance_integration_tokens t INNER JOIN instance_integration_keys k ON " +
				         "k.instance = @instance AND k.[key] = t.[key] WHERE t.[key] = @key AND getutcdate() < t.valid_until ", DBParameters).Rows) {
				if (Conv.ToStr(r["token"]).Equals(token)) {
					userItemId = Conv.ToInt(r["user_item_id"]);
					break;
				}
			}

			//Invalid Key or Token
			if (userItemId == 0) {
				db.ExecuteUpdateQuery(
					"UPDATE _" + instance +
					"_user SET login_tries = ISNULL(login_tries, 0) + 1, last_login_try = GetUTCDate() WHERE item_id IN (SELECT user_item_id FROM instance_integration_keys WHERE instance = @instance AND [key] = @key)",
					DBParameters);

				Diag.Log(instance,
					"Integration Authentication: Failure -- key '" + key + "' and/or token '" + token + "' is invalid.",
					0,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return null;
			}

			SetDBParam(SqlDbType.Int, "item_id", userItemId);
			db.ExecuteUpdateQuery("UPDATE _" + instance + "_user SET login_tries = 0, last_login = GetUTCDate(), last_login_try = NULL WHERE item_id = @item_id", DBParameters);

			var d = GetUser(userItemId, instance);
			if (d != null) {
				var u = new User(instance, (int)d["item_id"]);
				u.FillItem(d);
				StoreToSession(u, context);
				return u;
			}

			Diag.Log(instance, "Integration Authentication: Failure -- User " + userItemId + " cannot be found", 0,
				Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
			return null;
		}

		private bool IsCookieTokenValid(int item_id, string token, HttpContext Context, string cookie_instance,
			AuthenticatedSession currentSession) {
			authenticatedUser = null;

			if (item_id == 0 || token.Length == 0 || cookie_instance.Length == 0) return false;
			
			var d = GetUser(item_id, cookie_instance);
			if (d == null || Conv.ToStr(d["token"]) != token) return false;

			//Session already exists with the same token
			if (currentSession != null) {
				if (currentSession.token == token) return true;
			}

			//If cookie password is valid, refresh session as the data has just been fetched
			authenticatedUser = new User(cookie_instance, item_id);
			authenticatedUser.FillItem(d);
			StoreToSession(authenticatedUser, Context);

			return true;
		}

		public string GetIntegrationToken(string key, string login, string password) {
			//Check that the key is valid
			SetDBParam(SqlDbType.NVarChar, "key", key.ToLower());
			var userItemId =
				Conv.ToInt(db.ExecuteScalar(
					"SELECT user_item_id FROM instance_integration_keys WHERE LOWER([key]) = @key AND LOWER(instance) = @instance",
					DBParameters));

			//Invalid Key
			if (userItemId == 0) {
				Diag.Log(instance, "Integration Authentication: Failure -- key '" + key + "' is invalid.", 0,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return AuthenticationResult.IntegrationAuthentication_KeyInvalid.ToString();
			}

			//Try login with credentials
			var ar = BasicAuthentication(Config.GetDefaultDomain(), login, password, null, 0, "Integration Authentication");
			if (!(ar == AuthenticationResult.BasicAuthentication_Success && userItemId == authenticatedUser.ItemId)) {
				Diag.Log(instance, "Integration Authentication: Failure -- key and user credentials do not match.",
					userItemId,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				if (ar == AuthenticationResult.BasicAuthentication_LoginTriesExceeded) return ar.ToString();
				return AuthenticationResult.IntegrationAuthentication_LoginFailed.ToString();
			}

			//Create token
			var token = Util.CreateToken(32);

			//Issue Token
			DBParameters.Clear();
			SetDBParam(SqlDbType.NVarChar, "key", key.ToLower());
			SetDBParam(SqlDbType.NVarChar, "token", token);
			SetDBParam(SqlDbType.DateTime, "valid", DateTime.Now.AddHours(1).ToUniversalTime());
			db.ExecuteInsertQuery(
				"INSERT INTO instance_integration_tokens ([key], token, valid_until, [type]) VALUES (@key, @token, @valid, 1)",
				DBParameters);

			return token;
		}

		#endregion

		#region Grant Access

		private void StoreToSession(User authenticatedUser, HttpContext Context) {
			var obj = authenticatedUser.GetMember("locale_id");
			if (DBNull.Value.Equals(obj)) {
				authenticatedUser.AddMember("locale_id",
					(string)db.ExecuteScalar("SELECT default_locale_id FROM instances WHERE instance = @instance",
						DBParameters));
			}

			obj = authenticatedUser.GetMember("timezone");
			if (DBNull.Value.Equals(obj)) {
				authenticatedUser.AddMember("timezone",
					Conv.ToStr(db.ExecuteScalar("SELECT default_timezone FROM instances WHERE instance = @instance",
						DBParameters)));
			}

			authenticatedUser.AddMember("wwwpath", Conv.ToStr(Context.Request.Host) + "/" + Config.VirtualDirectory);
			if (Context.Request.IsHttps) {
				authenticatedUser.AddMember("protocol", "https");
			} else {
				authenticatedUser.AddMember("protocol", "http");
			}

			var a = new AuthenticatedSession(authenticatedUser.instance, authenticatedUser.ItemId,
				authenticatedUser.GetMembers());

			var ug = new UserGroups(authenticatedUser.instance, a);
			var groups = ug.GetUserGroupsForAuthenticatedUser();
			authenticatedUser.AddMember("groups", groups);
			a.SetMember("groups", groups);

			var u = Conv.SerializeToString(a);
			state = new State("", Context);
			state.toSession("AuthenticatedUserData", u);
			ConnectedUsers.AddConnectedUser(a);
		}

		public void GrantAccessInHttpContext(HttpContext Context, string domain = "", bool anonymous = false) {
			GrantAccessInHttpContext(authenticatedUser.instance, Conv.ToStr(authenticatedUser.GetMember("login")),
				Context,
				domain, anonymous);
		}

		public void GrantAccessInHttpContext(string instance, string login, HttpContext Context, string domain = "",
			bool anonymous = false) {
			
			//Store item_id & token -- Creates authorized cookie
			Cache.Remove(instance, "authentication", "item_id_" + authenticatedUser.ItemId);
			var token = Util.CreateToken(32);

			//Do not change token as multiple anonymous accounts can use the same
			if (anonymous) {
				var existingToken = Conv.ToStr(authenticatedUser.GetMember("token"));
				if (existingToken != "") token = existingToken;
			}

			var Authorized =
				"item_id=" + Conv.ToStr(authenticatedUser.ItemId) +
				"&token=" + token +
				"&instance=" + instance;
			var c = new CookieOptions {
				Expires = DateTime.Now.AddHours(12),
				Secure = true,
				SameSite = SameSiteMode.Strict,
				HttpOnly = true,
				Path = Config.VirtualDirectory
			};

			Context.Response.Cookies.Append(SecurityNaming.AuthorisationCookieName, Authorized, c);

			if (anonymous == false) {
				//Update user's token into users table
				if (domain == "") domain = Config.GetDefaultDomain();
				if (domain == "default") domain = "";
				SetDBParam(SqlDbType.NVarChar, "login", login.ToLower());
				SetDBParam(SqlDbType.NVarChar, "domain", domain.ToLower());
				SetDBParam(SqlDbType.NVarChar, "token", token);

				//TODO: IS WHERE CORRECT HERE?
				db.ExecuteUpdateQuery(
					"UPDATE _" + instance +
					"_user SET token = @token,login_tries = 0,last_login = GetUTCDate(), last_login_try = null WHERE LOWER(login) = @login AND " +
					getDomainCheck(),
					DBParameters);
			}

			//Inform clients that user is logging in.. in order to force existing session out
			if (anonymous == false) {
				var SignalR = new ApiPush(Startup.SignalR);
				var parameters = new List<object>();
				parameters.Add(authenticatedUser.ItemId);
				SignalR.SendToAll("logoutExisting", parameters);
				Cache.RemoveDelegate(authenticatedUser.ItemId);
			}

			//Store user information to session -- Creates session cookie
			authenticatedUser.AddMember("token", token);
			StoreToSession(authenticatedUser, Context);
		}


		public void ClearCookies(HttpContext Context) {
			var c = new CookieOptions();
			c.Path = Config.VirtualDirectory;
			Context.Response.Cookies.Delete(SecurityNaming.SessionCookieName, c);
			Context.Response.Cookies.Delete(SecurityNaming.AuthorisationCookieName, c);
			Context.Session.Clear();
		}

		public void RevokeAccessInHttpContext(HttpContext Context, int item_id, string token) {
			ClearCookies(Context);

			//Invalidate current token
			if (instance != "") {
				SetDBParam(SqlDbType.Int, "item_id", item_id);
				SetDBParam(SqlDbType.NVarChar, "token", Util.CreateToken(32));
				db.ExecuteUpdateQuery("UPDATE _" + instance + "_user SET token = @token WHERE item_id = @item_id",
					DBParameters);
			}

			ConnectedUsers.RemoveConnectedUser(token);

			if (!string.IsNullOrEmpty(instance)) {
				Diag.Log(instance, "User " + item_id + " signed out.", item_id, Diag.LogSeverities.Message,
					Diag.LogCategories.Authentication);
			}
		}

		#endregion
	}

	public static class SecurityNaming {
		public static string AuthorisationCookieName = "ketoAuthorisation";
		public static string SessionCookieName = "ketoSession";
	}

	public static class DummySession {
		public static AuthenticatedSession Create(InstanceSchedule s, ConnectionBase cb) {
			cb.SetDBParam(SqlDbType.NVarChar, "@instance", s.Instance);
			var instanceRow = cb.db.GetDataRow("SELECT * FROM instances WHERE instance = @instance", cb.DBParameters);
			var p = new Dictionary<string, object>();
			p["locale_id"] = Conv.ToStr(instanceRow["default_locale_id"]);
			var instanceDefaultUser = Conv.ToInt(instanceRow["instance_default_user"]);
			var dummyAuth = new AuthenticatedSession(s.Instance, instanceDefaultUser, p);
			dummyAuth.SetMember("groups", new UserGroups(s.Instance, dummyAuth).GetUserGroupsForAuthenticatedUser());
			dummyAuth.SetMember("wwwpath", Conv.ToStr(instanceRow["wwwpath"]));
			dummyAuth.SetMember("protocol", "https");
			return dummyAuth;
		}

		public static AuthenticatedSession Create(InstanceSchedule s) {
			var conBase = new ConnectionBase(s.Instance, Create(s.Instance, ""));
			return Create(s, conBase);
		}

		public static AuthenticatedSession Create(string instance, string locale_id, int defaultUser = 0) {
			var p = new Dictionary<string, object> {
				["locale_id"] = locale_id
			};

			var instanceDefaultUser = defaultUser;
			var dummyAuth = new AuthenticatedSession(instance, instanceDefaultUser, p);
			dummyAuth.SetMember("groups", new UserGroups(instance, dummyAuth).GetUserGroupsForAuthenticatedUser());

			return dummyAuth;
		}
	}


	public static class ConnectedUsers {
		private static readonly ConcurrentDictionary<string, AuthenticatedSession> users =
			new ConcurrentDictionary<string, AuthenticatedSession>();

		private static readonly ConcurrentDictionary<string, string> token_xref =
			new ConcurrentDictionary<string, string>();

		private static readonly ConcurrentDictionary<int, string>
			item_id_xref = new ConcurrentDictionary<int, string>();

		public static void Clear() {
			token_xref.Clear();
			item_id_xref.Clear();
			users.Clear();
		}

		public static int GetConnectedUserCount() {
			return users.Count;
		}

		public static void AddConnectedUser(AuthenticatedSession session) {
			if (session != null) {
				try {
					if (!users.ContainsKey(session.token)) {
						users.TryAdd(session.token, session);
					} else {
						users[session.token] = session;
					}
				} catch (Exception e) {
					throw new CustomUniqueException("Concurrent Users Collection Access: " + e.Message);
				}
			}
		}

		public static string GetConnectionIdWithItemId(int item_id) {
			return item_id_xref[item_id];
		}

		public static bool IsUserConnected(int item_id) {
			return item_id_xref.ContainsKey(item_id);
		}

		public static List<string> GetConnectionsIdsWithItemIds(List<int> item_ids) {
			var result = new List<string>();
			foreach (var item_id in item_ids) {
				result.Add(GetConnectionIdWithItemId(item_id));
			}

			return result;
		}

		public static void RemoveConnectedUserWithConnectionId(string connectionId) {
			var u = GetConnectedUserByConnectionId(connectionId);
			if (u != null) RemoveConnectedUser(u.token);
		}

		public static void RemoveConnectedUser(string token) {
			if (users.ContainsKey(token)) {
				var tries = "";
				if (users[token].connection_id != null) {
					token_xref.TryRemove(users[token].connection_id, out tries);
				}

				item_id_xref.TryRemove(users[token].item_id, out tries);

				AuthenticatedSession tris;
				users.TryRemove(token, out tris);
			}
		}

		public static bool IsConnectedWithToken(string token) {
			return users.ContainsKey(token);
		}

		public static bool IsConnectedWithConnectionID(string connection_id) {
			if (token_xref.ContainsKey(connection_id)) {
				var token = token_xref[connection_id];
				return IsConnectedWithToken(token);
			}

			return false;
		}

		public static void BindConnectionID(string token, string connection_id) {
			users[token].connection_id = connection_id;
			token_xref.TryAdd(connection_id, token);
			if (item_id_xref.ContainsKey(users[token].item_id)) {
				item_id_xref[users[token].item_id] = connection_id;
			} else {
				item_id_xref.TryAdd(users[token].item_id, connection_id);
			}
		}

		public static AuthenticatedSession GetConnectedUser(string token) {
			return users[token];
		}

		public static AuthenticatedSession GetConnectedUserByConnectionId(string connection_id) {
			if (token_xref.ContainsKey(connection_id)) return GetConnectedUser(token_xref[connection_id]);
			return null;
		}
	}
}