using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using User = Keto5.x.platform.server.processes.specialProcesses.User;

namespace Keto5.x.platform.server.security {
	public partial class Authentication {
		public class KetoAuthRequestPhase1 {
			public string returnUrl { get; set; }
			public string login { get; set; }
			public string initialToken { get; set; }
		}

		public class KetoAuthRequestPhase3 {
			public string finalToken { get; set; }
		}

		public class KetoAuthResponsePhase1 {
			public string url { get; set; }
		}

		public class KetoAuthResponsePhase2 {
			public string redirectUrl { get; set; }
			public string finalToken { get; set; }
			public string login { get; set; }
			public string remoteLogin { get; set; }
		}

		public class KetoAuthResponsePhase3 {
			public bool success { get; set; }
			public string login { get; set; }

			public string remoteLogin { get; set; }
		}

		private const string intraURL = "https://intra.ketoapps.com/model/process/ketoauth";

		public KetoAuthResponsePhase1 KetoAuthRequest(KetoAuthRequestPhase1 requestContent) {
			try {
				var request = (HttpWebRequest) WebRequest.Create(intraURL + "/KetoAuthResponsePhase1");
				requestContent.initialToken = Guid.NewGuid().ToString();

				request.ContentType = "application/json";
				request.Method = "POST";
				request.KeepAlive = true;

				var httpdata = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(requestContent));
				using (var requestStream = request.GetRequestStream()) {
					requestStream.Write(httpdata, 0, httpdata.Length);
				}

				var httpresponse = "";
				using (var response = request.GetResponse() as HttpWebResponse) {
					if (response != null) {
						if ((int) response.StatusCode >= 200 && (int) response.StatusCode < 300) {
							using var stream = response.GetResponseStream();
							using var reader = new StreamReader(stream);
							httpresponse = reader.ReadToEnd();
						}
					}
				}
				
				Diag.Log(instance, "Keto Authentication: Transmitted (Phase 2/3) -- Redirecting", 0,
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
				
				return JsonConvert.DeserializeObject<KetoAuthResponsePhase1>(httpresponse);
			} catch (Exception e) {
				Diag.Log(instance, "Keto Authentication: Failed (Phase 2/3) -- " + e.Message, 0,
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
			}

			return new KetoAuthResponsePhase1() { url = "" };
		}

		public AuthenticationResult KetoAuthResponse(string code, string instance, HttpContext cnt) {
			//Second Pass -- Check from intra that the code is correct
			try {
				var request = (HttpWebRequest) WebRequest.Create(intraURL + "/KetoAuthResponsePhase3");
				var requestContent = new KetoAuthRequestPhase3 {finalToken = code};

				request.ContentType = "application/json";
				request.Method = "POST";
				request.KeepAlive = true;

				var httpdata = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(requestContent));
				using (var requestStream = request.GetRequestStream()) {
					requestStream.Write(httpdata, 0, httpdata.Length);
				}

				var httpresponse = "";
				using (var response = request.GetResponse() as HttpWebResponse) {
					if (response != null) {
						if ((int) response.StatusCode >= 200 && (int) response.StatusCode < 300) {
							using var stream = response.GetResponseStream();
							using var reader = new StreamReader(stream);
							httpresponse = reader.ReadToEnd();
						}
					}
				}

				var result = JsonConvert.DeserializeObject<KetoAuthResponsePhase3>(httpresponse);
				if (!result.success) {
					//Second Pass Failed
					Diag.Log(instance, "Keto Authentication: Failed (Phase 3/3) -- Intra rejected the request", 0,
						Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
					return AuthenticationResult.BasicAuthentication_SecondKetoAuthFailed;
				}
				
				var domain = Config.GetDefaultDomain();
				if (domain == "default") domain = "";
				
				SetDBParameter("@login", result.login);
				SetDBParameter("@domain", domain);
				var d = db.GetDataRow("SELECT * FROM _" + instance + "_user WHERE LOWER(login) = @login AND " + getDomainCheck() + " AND active > 0",
					DBParameters);
				authenticatedUser = new User(instance, (int) d["item_id"]);
				authenticatedUser.FillItem(d);
				
				GrantAccessInHttpContext(instance, result.login, cnt);
				Diag.Log(instance,
					"Keto Authentication: Success (Phase 3/3) -- Keto user '" + result.remoteLogin + "' as '" +
					result.login + "'.",
					Conv.ToInt(d["item_id"]),
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
				return AuthenticationResult.BasicAuthentication_SecondKetoAuthSuccess;
			} catch (Exception e) {
				//Second Pass Failed
				Diag.Log(instance, "Keto Authentication: Failed (Phase 3/3) -- " + e.Message + " - " + e.StackTrace , 0,
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
			}

			return AuthenticationResult.BasicAuthentication_SecondKetoAuthFailed;
		}
	}
}