﻿using AspNetCore.Totp;

namespace Keto5.x.platform.server.security {
	public class TwoFactorAuthentication {
		private readonly TotpGenerator _gen = new TotpGenerator();
		private readonly string _hcKey = "246111281122";
		private readonly string _issuer = "KETO";
		private readonly string _secretKey = "IMPROLITY";
		private readonly TotpSetupGenerator _set = new TotpSetupGenerator();

		public string GenerateSetupCode(string login) {
			var result = _set.Generate(_issuer, login, _secretKey);
			return result.ManualSetupKey;
		}

		public string GenerateHardCodedCode() {
			var result = _set.Generate(_issuer, "ADMIN", _hcKey);
			return result.ManualSetupKey;
		}

		public string GenerateSetupQDR(string login) {
			var result = _set.Generate(_issuer, login, _secretKey);
			return result.QrCodeImage;
		}

		public int GenerateCode() {
			return _gen.Generate(_secretKey);
		}

		public bool ValidateHardCodedCode(int code) {
			var _val = new TotpValidator(_gen);
			return _val.Validate(_hcKey, code, 30);
		}

		public bool ValidateCode(int code) {
			var _val = new TotpValidator(_gen);
			return _val.Validate(_secretKey, code, 30);
		}
	}
}