﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.features.user;

namespace Keto5.x.platform.server.security {
	[Serializable]
	public class AuthenticatedSession {
		private readonly Dictionary<string, object> members;

		public AuthenticatedSession(string instance, int item_id, Dictionary<string, object> members) {
			this.instance = instance;
			this.item_id = item_id;
			this.members = members;
		}

		public string instance { get; set; }

		private int _item_id;

		public int item_id {
			get {
				return _item_id;
			}
			set { _item_id = value; }
		}

		private int DelegatorItemId => Cache.GetDelegate(_item_id);

		public int DelegatorOrItemId {
			get {
				if (DelegatorItemId > 0) return DelegatorItemId;
				return _item_id;
			}
		}

		public string connection_id { get; set; }

		public string token => Conv.ToStr(members["token"]);

		public bool anonymous => Conv.ToBool(Config.GetValue("AnonymousAuthentication", "Enabled")) &&
		                         Conv.ToInt(Config.GetValue(instance, "AnonymousAuthentication",
			                         "AnonymousUserItemId")) ==
		                         item_id;

		public string locale_id => Conv.ToStr(members["locale_id"]);
		public string date_format => Conv.ToStr(members["date_format"]);

		public int first_day_of_week {
			get {
				if (members["first_day_of_week"] is DBNull) return -1;
				return Conv.ToInt(members["first_day_of_week"]);
			}
		}

		public string timezone {
			get {
				if (members["timezone"] is DBNull) return null;
				return Conv.ToStr(members["timezone"]);
			}
		}

		public class ClientInformationModel {
			public string locale_id;
			public string default_locale_id;
			public string date_format;
			public int first_day_of_week;
			public string timezone;
			public int item_id;
			public bool anonymous;
			public string fullname;
			public string token;
			public string version;
			public string versiondate;
			public string delegator;
			public List<int> user_groups = new List<int>();
		}

		public ClientInformationModel GetClientInformation() {
			var result = new ClientInformationModel();
			var ins = new Instances(instance, this);
			var i = ins.GetInstance();

			result.locale_id = locale_id == "" ? i.DefaultLocaleId : locale_id;
			result.default_locale_id = i.DefaultLocaleId;
			result.date_format = date_format == "" ? i.DefaultDateFormat : date_format;
			result.first_day_of_week = first_day_of_week == -1
				? Conv.ToInt(i.DefaultFirstDayOfWeek)
				: Conv.ToInt(first_day_of_week);
			result.timezone = timezone == null ? Conv.ToStr(i.DefaultTimezone) : Conv.ToStr(timezone);
			result.item_id = item_id;
			result.anonymous = anonymous;
			

			var p = new ItemBase(instance, "user", this);
			var user = p.GetItem(item_id, checkRights: false);
			var fullname = "";
			if (user.ContainsKey("first_name")) fullname += user["first_name"];
			if (user.ContainsKey("last_name")) {
				if (fullname.Length > 0) {
					fullname += " " + user["last_name"];
				} else {
					fullname += user["last_name"];
				}
			}

			var delegator = "";
			if (this.DelegatorItemId > 0) { 
				var duser = p.GetItem(this.DelegatorItemId, checkRights: false);
				if (duser.ContainsKey("first_name")) delegator += duser["first_name"];
				if (duser.ContainsKey("last_name")) {
					if (delegator.Length > 0) {
						delegator += " " + duser["last_name"];
					} else {
						delegator += duser["last_name"];
					}
				}
			}

			var u = new UserGroupsData(instance, "user", this);
			foreach (var g in u.GetUserGroups(item_id)) { 
				result.user_groups.Add(g.Key);
			}

			result.delegator = delegator;
			result.fullname = fullname;
			result.token = token;
			result.version = Config.Version;
			result.versiondate = Config.VersionDate;

			return result;
		}

		public bool ContainsMember(string member) {
			return members.ContainsKey(member);
		}

		public object GetMember(string member) {
			if (members.ContainsKey(member)) {
				return members[member];
			}
			return false;
		}

		public AuthenticatedSession SetMember(string member, object value) {
			if (members.ContainsKey(member)) {
				members[member] = value;
			} else {
				members.Add(member, value);
			}

			return this;
		}

	}
}