﻿namespace Keto5.x.platform.server.security {
	public class SessionBaseInfo {
		protected AuthenticatedSession AuthenticatedSession;
		protected string Instance;
		protected string Process;

		public SessionBaseInfo(string instance, string process, AuthenticatedSession session) {
			Instance = instance;
			Process = process;
			AuthenticatedSession = session;
		}
	}
}