using System;
using System.Data;
using Google.Apis.Auth;
using Keto5.x.platform.server.common;
using User = Keto5.x.platform.server.processes.specialProcesses.User;

namespace Keto5.x.platform.server.security {
	public partial class Authentication {
		public AuthenticationResult LegacyAuthentication(string domain, string token) {
			if (domain == "") domain = Config.GetDefaultDomain();
			if (domain == "default") domain = "";

			if (token != "") {
				token = Conv.Escape(token);

				if (token.Length != 64) { 
					Diag.Log(instance, "LegacyAuthentication: Failure -- Token check failed -- Token is not of correct format", 0,
						Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
					return AuthenticationResult.LegacyAuthentication_TokenCheckFailed;
				}
				
				//Get user from DB
				SetDBParam(SqlDbType.NVarChar, "token", token);
				SetDBParam(SqlDbType.NVarChar, "domain", domain.ToLower());
				var d = db.GetDataRow(
					"SELECT * FROM " + tableName +
					" WHERE token = @token AND " + getDomainCheck() + " AND active > 0",
					DBParameters);

				//Failed
				if (d == null) {
					Diag.Log(instance, "LegacyAuthentication: Failure -- '" + token + "' does match any user.", 0,
						Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
					return AuthenticationResult.LegacyAuthentication_LoginNonExistant;
				}

				//Succeeded
				authenticatedUser = new User(instance, (int) d["item_id"]);
				authenticatedUser.FillItem(d);

				Diag.Log(instance,
					"LegacyAuthentication: Success -- User with token '" + token + "' of hosted domain '" + domain + "'.",
					(int) d["item_id"],
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
				return AuthenticationResult.LegacyAuthentication_Success;
			}

			Diag.Log(instance, "LegacyAuthentication: Failure -- Token not provided", 0,
				Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
			return AuthenticationResult.LegacyAuthentication_NoTokenProvided;
		}
	}
}