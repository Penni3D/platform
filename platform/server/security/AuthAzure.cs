using System;
using System.Data;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Http;
using Microsoft.Identity.Client;
using User = Keto5.x.platform.server.processes.specialProcesses.User;

namespace Keto5.x.platform.server.security {
	public partial class Authentication {
		private IConfidentialClientApplication GetConfidentialClientApp(string domain) {
			var clientid = Config.GetDomainValue(domain, "AzureADAuthentication", "ClientID");
			var clientkey = Config.GetDomainValue(domain, "AzureADAuthentication", "ClientKey");
			var authority = Config.GetDomainValue(domain, "AzureADAuthentication", "AuthorityURL");
			var tenant = Config.GetDomainValue(domain, "AzureADAuthentication", "Tenant");
			var loginurl = Config.GetDomainValue(domain, "AzureADAuthentication", "LoginURL");
			return ConfidentialClientApplicationBuilder
				.Create(clientid)
				.WithAuthority(authority + tenant)
				.WithClientSecret(clientkey)
				.WithRedirectUri(loginurl)
				.Build();
		}

		readonly string[] _scopes = { "User.Read" };

		public string AzureADAuthenticationURL(string domain) {
			if (domain == "") domain = Config.GetDefaultDomain();
			if (domain == "default") domain = "";
		
			try {
				return GetConfidentialClientApp(domain).GetAuthorizationRequestUrl(_scopes).ExecuteAsync().Result
					.AbsoluteUri;
			} catch (Exception ex) {
				Diag.Log("Azure AD remote failure in GetAuthorizationRequestUrl process - ", ex.Message, 0,
					Diag.LogSeverities.Error,
					Diag.LogCategories.Authentication);
			}

			return "";
		}

		public AuthenticationResult AzureADAuthentication(HttpContext Cnt, string code) {
			var domain = Config.GetDefaultDomain();
			var upn = "";
			var app = GetConfidentialClientApp(domain);
			
			try {
				var authResult = app.AcquireTokenByAuthorizationCode(_scopes, code).ExecuteAsync();
				upn = authResult.GetAwaiter().GetResult().Account.Username;
			} catch (Exception ex) {
				Diag.Log(instance,
					"Azure AD remote failure after AcquireTokenByAuthorizationCode Process (1/2) - " +
					ex.Message, 0,
					Diag.LogSeverities.Error, Diag.LogCategories.Authentication);
				return AuthenticationResult.AzureADAuthentication_Failure;
			}

			try {
				if (Config.Instances.ContainsKey(instance) == false) {
					return AuthenticationResult.InstanceNotValid;
				}

				SetDBParam(SqlDbType.NVarChar, "login", upn.ToLower());
				var d = db.GetDataRow(
					"SELECT * FROM _" + instance + "_user WHERE LOWER(login) = @login AND active > 0",
					DBParameters);

				if (d == null) {
					Diag.Log(instance,
						"Azure AD Authentication: Failure -- User '" + upn +
						"' does not match any record in the database.", 0,
						Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
					return AuthenticationResult.AzureADAuthentication_LoginNonExistant;
				}

				authenticatedUser = new User(instance, (int)d["item_id"]);
				authenticatedUser.FillItem(d);
				var login = Conv.ToStr(d["login"]);
				GrantAccessInHttpContext(instance, login, Cnt, domain);

				Diag.Log(instance, "Azure AD Authentication: Success -- User '" + upn + "'.", (int)d["item_id"],
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
			} catch (Exception ex) {
				Diag.Log(instance,
					"Azure AD local failure after successful AcquireTokenByAuthorizationCode Process (2/2) with UPN '" +
					upn + "' - " +
					ex.Message, 0,
					Diag.LogSeverities.Error, Diag.LogCategories.Authentication);
				return AuthenticationResult.AzureADAuthentication_Failure;
			}

			return AuthenticationResult.AzureADAuthentication_Success;
		}
	}
}