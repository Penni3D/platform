using System;
using System.Data;
using Google.Apis.Auth;
using Keto5.x.platform.server.common;
using User = Keto5.x.platform.server.processes.specialProcesses.User;

namespace Keto5.x.platform.server.security {
	public partial class Authentication {
		public AuthenticationResult GoogleAuthentication(string domain, string token) {
			if (domain == "") domain = Config.GetDefaultDomain();
			if (domain == "default") domain = "";
			
			if (token != "") {
				var email = "";
				var hostedDomain = "";

				try {
					var validPayload = GoogleJsonWebSignature.ValidateAsync(token);
					if (validPayload == null) {
						Diag.Log(instance, "GoogleAuthentication: Failure -- Token check failed -- Token was: '" + token + "'", 0,
							Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
						return AuthenticationResult.GoogleAuthentication_TokenCheckFailed;
					}

					email = validPayload.Result.Email;
					hostedDomain = validPayload.Result.HostedDomain;
				} catch (Exception e) {
					Diag.Log(instance, "GoogleAuthentication: Failure -- Token check failed -- " + e.Message, 0,
						Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
					return AuthenticationResult.GoogleAuthentication_TokenCheckFailed;
				}

				//Get user from DB
				SetDBParam(SqlDbType.NVarChar, "login", email.ToLower());
				SetDBParam(SqlDbType.NVarChar, "domain", domain.ToLower());
				var d = db.GetDataRow("SELECT * FROM " + tableName + " WHERE LOWER(login) = @login AND " + getDomainCheck() + " AND active > 0",
					DBParameters);

				//Failed
				if (d == null) {
					Diag.Log(instance, "GoogleAuthentication: Failure -- '" + email + "' does not exist.", 0,
						Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
					return AuthenticationResult.GoogleAuthentication_LoginNonExistant;
				}

				//Succeeded
				authenticatedUser = new User(instance, (int) d["item_id"]);
				authenticatedUser.FillItem(d);

				Diag.Log(instance, "GoogleAuthentication: Success -- User '" + email + "' of hosted domain '" + hostedDomain + "'.", (int) d["item_id"],
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
				return AuthenticationResult.GoogleAuthentication_Success;
			}

			Diag.Log(instance, "GoogleAuthentication: Failure -- Token not provided", 0,
				Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
			return AuthenticationResult.GoogleAuthentication_NoTokenProvided;
		}
	}
}