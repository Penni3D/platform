﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.security {
	public static class RequestSecurity {
		public static void RequestPathCheck(IDictionary <string, object> actionArguments) {
			foreach (var k in actionArguments) {
				if (k.Value is string) 
					Conv.ToSql(k.Value, true);
			}
		}
	}
}