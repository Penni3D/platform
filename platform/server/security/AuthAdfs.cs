using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.specialProcesses;

namespace Keto5.x.platform.server.security {
	public partial class Authentication {
		public Authentication.AuthenticationResult AdfsAuthentication(string domain, Dictionary<string, string> claims) {
			if (domain == "") domain = Config.GetDefaultDomain();
			if (domain == "default") domain = "";
			
			if (claims.ContainsKey("login")) {
				var login = claims["login"];

				//Get user from DB
				SetDBParam(SqlDbType.NVarChar, "login", login.ToLower());
				SetDBParam(SqlDbType.NVarChar, "domain", domain.ToLower());
				
				var d = db.GetDataRow("SELECT * FROM " + tableName + " WHERE LOWER(login) = @login AND " + getDomainCheck() + " AND active > 0",
					DBParameters);

				//Failed
				if (d == null) {
					Diag.Log(instance, "AdfsAuthentication: Failure -- '" + login + "' does not exist.", 0,
						Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
					return Authentication.AuthenticationResult.ADFSAuthentication_LoginNonExistant;
				}

				//Succeeded
				authenticatedUser = new User(instance, (int) d["item_id"]);
				authenticatedUser.FillItem(d);

				Diag.Log(instance, "AdfsAuthentication: Success -- User '" + login + "'.", (int) d["item_id"],
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
				return Authentication.AuthenticationResult.ADFSAuthentication_Success;
			}

			Diag.Log(instance, "AdfsAuthentication: Failure -- UPN Claim cannot be found", 0,
				Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
			return Authentication.AuthenticationResult.ADFSNoUPNClaimFound;
		}
	}
}