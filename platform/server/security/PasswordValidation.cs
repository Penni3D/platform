﻿using System.Collections.Specialized;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;

namespace Keto5.x.platform.server.security {
	public static class PasswordValidation {
		public class PasswordValidationResult {
			public PasswordValidationResult() { }
			public bool minValid { get; set; }
			public bool maxValid { get; set; }
			public bool caseValid { get; set; }
			public bool numberValid { get; set; }
			public bool specialValid { get; set; }

			public CustomArgumentException validationException {
				get {
					var result = new StringCollection();
					if (!minValid) result.Add("Minimum length requirement");
					if (!maxValid) result.Add("Maximum length requirement");
					if (!caseValid) result.Add("Case requirement");
					if (!numberValid) result.Add("Contains number requirement");
					if (!specialValid) result.Add("Contains special char requirement");
					
					return new CustomArgumentException("Password validation failed with " + Conv.ToDelimitedString(result, ", "));
				}
			}

			public bool passwordValid => minValid && maxValid && caseValid && numberValid && specialValid;
		}

		public static bool IsPasswordValid(string password, bool throwExceptionIfNotValid = false, bool allowEmptyPassword = false) {
			if (allowEmptyPassword && password == "") return true;
			var result = GetPasswordValidationResult(password);
			if (throwExceptionIfNotValid && !result.passwordValid) throw result.validationException;
			return result.passwordValid;
		}

		public static string GetValidationRequirementString(Translations translations) { 
			var minLength = Conv.ToInt(Config.GetValue("PasswordPolicy", "MinLength"));
			var requireCase = Conv.ToBool(Config.GetValue("PasswordPolicy", "RequireCase"));
			var requireNumber = Conv.ToBool(Config.GetValue("PasswordPolicy", "RequireNumber"));
			var requireSpecial = Conv.ToBool(Config.GetValue("PasswordPolicy", "RequireSpecial"));
			var result = new StringCollection();
			if (minLength > 0) result.Add(translations.GetTranslation("LOGIN_COMPLEXITY_MIN").Replace("[x]", minLength.ToString()));
			if (requireCase) result.Add(translations.GetTranslation("LOGIN_COMPLEXITY_CASE"));
			if (requireNumber) result.Add(translations.GetTranslation("LOGIN_COMPLEXITY_NUMERIC"));
			if (requireSpecial) result.Add(translations.GetTranslation("LOGIN_COMPLEXITY_SPECIAL"));
			return Conv.ToDelimitedString(result, ", ");
		}

		public static PasswordValidationResult GetPasswordValidationResult(string password, bool allowEmptyPassword = false) {
			var minLength = Conv.ToInt(Config.GetValue("PasswordPolicy", "MinLength"));
			var maxLength = Conv.ToInt(Config.GetValue("PasswordPolicy", "MaxLength"));
			var requireCase = Conv.ToBool(Config.GetValue("PasswordPolicy", "RequireCase"));
			var requireNumber = Conv.ToBool(Config.GetValue("PasswordPolicy", "RequireNumber"));
			var requireSpecial = Conv.ToBool(Config.GetValue("PasswordPolicy", "RequireSpecial"));
			return new PasswordValidationResult {
				minValid = password.Length >= minLength,
				maxValid = password.Length <= maxLength,
				numberValid = !requireNumber || password.Any(char.IsDigit),
				specialValid = !requireSpecial || password.Any(ch => !char.IsLetterOrDigit(ch)),
				caseValid = !requireCase || password.Any(char.IsUpper)
			};
		}
	}
}