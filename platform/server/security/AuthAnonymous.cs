using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.specialProcesses;

namespace Keto5.x.platform.server.security {
	public partial class Authentication {
		public AuthenticationResult AnonymousAuthentication() {
			//Get anonymous user id set in configuration
			var anonymousId = Conv.ToInt(Config.GetValue(instance, "AnonymousAuthentication", "AnonymousUserItemId"));
			if (anonymousId == 0) {
				Diag.Log(instance,
					"Anonymous Authentication: Failure -- AnonymousUserItemId configuration parameter is not set.",
					0,
					Diag.LogSeverities.Error, Diag.LogCategories.Authentication);
				return AuthenticationResult.AnonymousAuthentication_AccountNotSet;
			}

			//Check that an account exists with the id
			SetDBParameters("item_id", anonymousId);
			var d = db.GetDataRow("SELECT * FROM " + tableName + " WHERE item_id = @item_id", DBParameters);
			if (d == null) {
				Diag.Log(instance,
					"Anonymous Authentication: Failure -- AnonymousUserItemId " + Conv.ToStr(anonymousId) +
					" doesn't match any user.",
					0,
					Diag.LogSeverities.Error, Diag.LogCategories.Authentication);
				return AuthenticationResult.AnonymousAuthentication_AccountNotFound;
			}

			authenticatedUser = new User(instance, (int) d["item_id"]);
			authenticatedUser.FillItem(d);

			Diag.Log(instance, "Anonymous Authentication:  -- User '" + d["login"] + "'.", (int) d["item_id"],
				Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
			return AuthenticationResult.AnonymousAuthentication_Success;
		}
	}
}