﻿using System;
using System.Security.Cryptography;

namespace Keto5.x.platform.server.security {
	internal class FailedToHashException : Exception {
		public FailedToHashException(string message, Exception ex) : base(message, ex) { }
	}

	internal class InvalidHashException : Exception {
		public InvalidHashException(string message) : base(message) { }
		public InvalidHashException(string message, Exception ex) : base(message, ex) { }
	}

	public class HashPbkdf2 {
		//In bytes
		private const int SaltSize = 24;
		private const int HashSize = 18;
		private const int Iterations = 64000; //32 000 iterations of SHA-1


		public static string CreateHash(string password) {
			var salt = new byte[SaltSize];

			try {
				using (var csp = new RNGCryptoServiceProvider()) {
					csp.GetBytes(salt);
				}
			} catch (Exception ex) {
				throw new FailedToHashException("Failed to hash the password.", ex);
			}

			var hash = Pbkdf2(password, salt, Iterations, HashSize);
			var retval = "" + Iterations + ',' + HashSize + ',' + Convert.ToBase64String(salt) + ',' + Convert.ToBase64String(hash);
			return retval;
		}

		public static bool VerifyPassword(string password, string saltedHash) {
			var splitHash = saltedHash.Split(',');

			if (splitHash.Length != 4) return false;

			int iterations;
			int hashSize;
			byte[] salt;
			byte[] hash;

			try {
				iterations = int.Parse(splitHash[0]);
				hashSize = int.Parse(splitHash[1]);
				salt = Convert.FromBase64String(splitHash[2]);
				hash = Convert.FromBase64String(splitHash[3]);

			} catch (Exception ex) {
				throw new InvalidHashException("Failed to parse the hash.", ex);
			}

			if (iterations < 1 || hashSize != hash.Length) {
				throw new InvalidHashException("Invalid hash type.");
			}

			var tempHash = Pbkdf2(password, salt, iterations, hashSize);
			return SlowEquals(hash, tempHash);
		}

		//To prevent timing attacks (side-channel attack)
		private static bool SlowEquals(byte[] a, byte[] b) {
			var diff = (uint)a.Length ^ (uint)b.Length;
			for (var i = 0; i < a.Length && i < b.Length; i++) {
				diff |= (uint)(a[i] ^ b[i]);
			}

			return diff == 0;
		}

		private static byte[] Pbkdf2(string password, byte[] salt, int iterations, int hashSize) {
			using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt)) {
				pbkdf2.IterationCount = iterations;
				return pbkdf2.GetBytes(hashSize);
			}
		}
	}
}
