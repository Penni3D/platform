using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using User = Keto5.x.platform.server.processes.specialProcesses.User;

namespace Keto5.x.platform.server.security {
	public partial class Authentication {
		public AuthenticationResult BasicAuthentication(string domain, string login, string password, HttpContext context, int twoFactorAuthentication = 0, string sourcename = "Basic Authentication") {
			if (domain == "") domain = Config.GetDefaultDomain();
			if (domain == "default") domain = "";
			if (Conv.ToBool(Config.GetDomainValue(domain, "BasicAuthentication", "Enabled")) == false)
				throw new CustomPermissionException("Basic authentication is disabled.");

			authenticatedUser = null;

			//Check if conditions are perfect
			if (Config.Instances.ContainsKey(instance) == false) return AuthenticationResult.InstanceNotValid;
			if (login == "") return AuthenticationResult.BasicAuthentication_LoginEmpty;
			if (password == "") return AuthenticationResult.BasicAuthentication_PasswordEmpty;

			//Get user from DB
			SetDBParam(SqlDbType.NVarChar, "login", login.ToLower());
			SetDBParam(SqlDbType.NVarChar, "domain", domain.ToLower());
			var d = db.GetDataRow("SELECT * FROM " + tableName + " WHERE LOWER(login) = @login AND " + getDomainCheck(),
				DBParameters);

			if (d == null) {
				Diag.Log(instance, sourcename + ": Failure -- '" + login + "' does not exist.", 0,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return AuthenticationResult.BasicAuthentication_LoginNonExistant;
			}

			if (Conv.ToInt(d["active"]) == 0) { 
				Diag.Log(instance, sourcename + ": Failure -- '" + login + "' is not active.", 0,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return AuthenticationResult.BasicAuthentication_LoginNonExistant;
			}

			if (Conv.ToInt(d["login_tries"]) > 3) {
				if (Conv.ToDateTime(d["last_login_try"]) != null &&
				    ((DateTime) d["last_login_try"]).AddMinutes(10) > DateTime.UtcNow) {
					Diag.Log(instance, sourcename + ": Failure -- '" + login + "' has exceeded its login tries.", 0,
						Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
					return AuthenticationResult.BasicAuthentication_LoginTriesExceeded;
				}
				//If 10 or more minutes since the last login attempt, clear login tries
				if (Conv.ToDateTime(d["last_login_try"]) != null &&
				           ((DateTime)d["last_login_try"]).AddMinutes(10) < DateTime.UtcNow) {

					SetDBParam(SqlDbType.Int, "item_id", (int) d["item_id"]);
					db.ExecuteUpdateQuery("UPDATE _" + instance +
					                      "_user SET login_tries = 0,last_login_try = GetUTCDate() WHERE item_id = @item_id",
						DBParameters);
				}
			}

			//Check that password matches
			if (!HashPbkdf2.VerifyPassword(Conv.ToStr(password), Conv.ToStr(d["password"]))) {
				SetDBParam(SqlDbType.Int, "item_id", (int) d["item_id"]);
				db.ExecuteUpdateQuery("UPDATE _" + instance +
				                      "_user SET login_tries = ISNULL(login_tries,0) + 1,last_login_try = GetUTCDate() WHERE item_id = @item_id",
					DBParameters);
				Diag.Log(instance, sourcename + ": Failure -- '" + login + "' password doesn't match.", 0,
					Diag.LogSeverities.Warning, Diag.LogCategories.Authentication);
				return AuthenticationResult.BasicAuthentication_PasswordNonMatching;
			}

			authenticatedUser = new User(instance, (int) d["item_id"]);
			authenticatedUser.FillItem(d);

			//Two Factor Authentication is Enabled?
			var ketoAuthRequired = Conv.ToInt(authenticatedUser.GetMember("ketoauth")) == 1;
			if (context != null && context.Request.Host.Host == "localhost") ketoAuthRequired = false;

			if (ketoAuthRequired) {
				if (twoFactorAuthentication == 0) {
					//First Pass -- Authentication success -- Wait for authentication against intra
					Diag.Log(instance, "Keto Authentication: Success (Phase 1/3) -- User '" + login + "'.",
						(int) d["item_id"],
						Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
					return AuthenticationResult.BasicAuthentication_SecondKetoAuthRequired;
				}
			}
			
			//Two Factor Authentication is Enabled?
			var twoFactorRequired = Conv.ToInt(authenticatedUser.GetMember("twofactor")) == 1;
			if (context != null && context.Request.Host.Host == "localhost") twoFactorRequired = false;
			if (ketoAuthRequired) twoFactorRequired = false;
			
			if (twoFactorRequired) {
				if (twoFactorAuthentication == 0) {
					//First Pass -- Authentication success -- Wait for 2nd Factor Code
					Diag.Log(instance, sourcename + ": Success (Phase 1/2) -- User '" + login + "'.",
						(int) d["item_id"],
						Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
					return AuthenticationResult.BasicAuthentication_SecondFactorRequired;
				}

				//Second Pass -- Check if code is correct
				var authcheck = new TwoFactorAuthentication();
				if (authcheck.ValidateCode(twoFactorAuthentication)) {
					Diag.Log(instance, sourcename + ": Success (Phase 2/2) -- User '" + login + "'.",
						(int) d["item_id"],
						Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
					return AuthenticationResult.BasicAuthentication_Success;
				}

				//Check Hardcoded Security Code
				if (authcheck.ValidateHardCodedCode(twoFactorAuthentication)) {
					Diag.Log(instance, sourcename + ": Success (Phase 2/2) -- Elevated Bypass with '" + login + "'.",
						(int) d["item_id"],
						Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
					return AuthenticationResult.BasicAuthentication_Success;
				}

				//Second Pass Failed
				Diag.Log(instance, sourcename + ": Failed (Phase 2/2) -- User '" + login + "'.",
					(int) d["item_id"],
					Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
				return AuthenticationResult.BasicAuthentication_SecondFactorFailed;
			}
			
			Diag.Log(instance, sourcename + ": Success -- User '" + login + "'.", (int) d["item_id"],
				Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
			return AuthenticationResult.BasicAuthentication_Success;
		}
	}
}