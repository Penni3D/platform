using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using BackgroundTasksSample.Services;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.backgroundProcesses;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.instanceSchedules {
	[Route("v1/settings/BackgroundProcessSettings")]
	public class BackgroundProcessSettings : PageBase {

		[HttpGet("")]
		public List<InstanceSchedule> Get() {
			CheckRight("read");
			var iss = new InstanceSchedules(instance, currentSession);
			return iss.GetInstanceSchedules();
		}

		[HttpGet("groups")]
		public List<InstanceScheduleGroup> GetGroups() {
			CheckRight("read");
			var cb = new ConnectionBase(instance, currentSession);
			var result = new List<InstanceScheduleGroup>();
			foreach (DataRow r in cb.db.GetDatatable("SELECT * FROM instance_schedule_groups ORDER BY order_no  ASC").Rows) {
				result.Add(new InstanceScheduleGroup(r, instance, currentSession));
			}
			return result;
		}

		[HttpPost("{groupId}")]
		public InstanceSchedule Add(int groupId) {
			CheckRight("write");
			var iss = new InstanceSchedules(instance, currentSession);
			return iss.AddInstanceSchedule(groupId);
		}

		[HttpPost("addgroup")]
		public InstanceScheduleGroup AddNewGroup() {
			CheckRight("write");
			var group = new InstanceScheduleGroup();
			var iss = new InstanceSchedules(instance, currentSession);
			return iss.AddInstanceScheduleGroup(group);
		}

		[HttpPut("")]
		public void Save([FromBody] List<InstanceSchedule> schedules) {
			CheckRight("write");
			var iss = new InstanceSchedules(instance, currentSession);
			iss.SaveInstanceSchedule(schedules);
		}

		[HttpPut("group")]
		public InstanceScheduleGroup SaveGroup([FromBody] InstanceScheduleGroup group) {
			CheckRight("write");
			var iss = new InstanceSchedules(instance, currentSession);
			return iss.SaveInstanceScheduleGroup(group);
		}

		[HttpDelete("{scheduleId}")]
		public void Delete(int scheduleId) {
			CheckRight("write");
			var iss = new InstanceSchedules(instance, currentSession);
			iss.DeleteInstanceSchedule(scheduleId);
		}

		[HttpPost("deletegroups")]
		public void DeleteGroups([FromBody]List<InstanceScheduleGroup> groups) {
			CheckRight("write");
			var iss = new InstanceSchedules(instance, currentSession);
			iss.DeleteInstanceScheduleGroups(groups);
		}

		[HttpPut("instance_default_user")]
		public void SaveInstanceDefaultUser([FromBody] InstanceSchedules.InstaceDefaults defaults) {
			CheckRight("write");
			var iss = new InstanceSchedules(instance, currentSession);
			iss.SaveInstanceDefaultUser(defaults);
		}

		[HttpGet("instance_defaults")]
		public InstanceSchedules.InstaceDefaults GetInstanceDefaults() {
			CheckRight("read");
			var iss = new InstanceSchedules(instance, currentSession);
			return iss.GetInstanceDefaults();
		}

		[HttpGet("logs/{scheduleId}")]
		public List<InstanceSchedules.ScheduleDate> GetBgLogs(int scheduleId) {
			CheckRight("read");
			var iss = new InstanceSchedules(instance, currentSession);
			return iss.GetBgLogResults(scheduleId);
		}

		[HttpGet("outcomes")]
		public Dictionary<int,string> GetBgOutcomes() {
			CheckRight("read");
			var result = new Dictionary<int,string>();
			var p = 0;
			foreach (var x in Enum.GetValues(typeof(BackgroundProcessResult.Outcomes))) {
				result.Add(p,x.ToString());
				p++;
			}
			return result;
		}

		[HttpPost("async_with_schedule")]
		public void AsyncExecuteWithSchedule([FromBody] InstanceSchedule instanceSchedule) {
			CheckRight("write");
			var a = new AsyncTask();
			a.ExecuteAsync(instanceSchedule, currentSession);
		}

		[HttpPost("async_with_schedule_group")]
		public async Task AsyncExecuteWithScheduleGroup([FromBody] List<InstanceSchedule> instanceSchedules) {
			CheckRight("write");
			var index = 0;
			foreach (var i in instanceSchedules) {
				var a = new AsyncTask();
				await a.ExecuteAsync(i, currentSession, index);
				index++;
			}
		}

		public BackgroundProcessSettings() : base("backgroundProcesses") { }
	}

}



