using System;
using System.Collections.Generic;
using System.Data;
using BackgroundTasksSample.Services;
using DocumentFormat.OpenXml.Spreadsheet;
using Google.Apis.Util;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.core.translationclient;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.pages.instanceSchedules
{
	public class InstanceSchedule
	{
		public InstanceSchedule() { }

		public InstanceSchedule(DataRow row) {
			if (row == null)
				return;
			ScheduleId = Conv.ToInt(row["schedule_id"]);
			Instance = Conv.ToStr(row["instance"]);
			Name = Conv.ToStr(row["name"]);
			ConfigOptions = Conv.ToStr(row["config_options"]);
			if (Conv.ToStr(row["schedule_json"]) != "") {
				ScheduleOptions = new ScheduleOptions(Conv.ToStr(row["schedule_json"]));
			} else {
				ScheduleOptions = null;
			}
			LastModified = (DateTime)row["last_modified"];
			Nickname = Conv.ToStr(row["nickname"]);
			Disabled = Conv.ToInt(row["disabled"]);
			Compare = Conv.ToInt(row["compare"]);
			OrderNo = Conv.ToStr(row["order_no"]);
			GroupId = Conv.ToInt(row["group_id"]);
		}

		public int ScheduleId { get; set; }
		public string Instance { get; set; }
		public string Name { get; set; }
		public ScheduleOptions ScheduleOptions { get; set; }

		public DateTime LastModified { get; }

		public string ConfigOptions { get; set; }

		public string Nickname { get; set; }

		public int Disabled { get; set; }

		public int Compare { get; set; }
		public string OrderNo { get; set; }

		public int? GroupId { get; set; }
	}


	public class ScheduleOptions
	{
		public ScheduleOptions() { }

		public ScheduleOptions(string nonParsedJson) {
			try {
				ScheduleOptions deserialized = JsonConvert.DeserializeObject<ScheduleOptions>(nonParsedJson);
				Slot = deserialized.Slot;
				Time = deserialized.Time;
				Weekday = deserialized.Weekday;
			} catch (Exception e) {
				Console.WriteLine(e.Message);
			}
		}

		public int Slot { get; set; }
		public string Time { get; set; }

		public string Weekday { get; set; }
	}

	public class InstanceScheduleGroup
	{
		public InstanceScheduleGroup() {

		}
		public InstanceScheduleGroup(DataRow r, string instance, AuthenticatedSession session) {
			GroupId = Conv.ToInt(r["group_id"]);
			Instance = Conv.ToStr(r["instance"]);
			OrderNo = Conv.ToStr(r["order_no"]);
			if (Conv.ToStr(r["schedule_json"]) != "") {
				ScheduleOptions = new ScheduleOptions(Conv.ToStr(r["schedule_json"]));
			} else {
				ScheduleOptions = null;
			}
			var Translations = new Translations(instance, session);
			Translation = Translations.GetInstanceTranslationDictionary(Conv.ToStr(r["variable"]));

			Schedules = new List<InstanceSchedule>();
			var cb = new ConnectionBase(instance, session);
			cb.SetDBParam(SqlDbType.Int, "@groupId", GroupId);

			foreach (DataRow r2 in cb.db.GetDatatable("SELECT * FROM instance_schedules WHERE group_id = @groupId", cb.DBParameters).Rows) {
				Schedules.Add(new InstanceSchedule(r2));
			}

			LanguageVariable = Conv.ToStr(r["variable"]);
			Disabled = Conv.ToInt(r["disabled"]);
		}

		public int GroupId { get; set; }
		public string Instance { get; set; }
		public string OrderNo { get; set; }
		public Dictionary<string, string> Translation { get; set; }
		public List<InstanceSchedule> Schedules { get; set; }

		public ScheduleOptions ScheduleOptions { get; set; }

		public string LanguageVariable { get; set; }

		public int Disabled { get; set; }

	}

	public class InstanceSchedules : ConnectionBase
	{
		private AuthenticatedSession session;

		public InstanceSchedules(string instance, AuthenticatedSession session) : base(instance, session) {
			this.instance = instance;
			this.session = session;
		}

		public List<InstanceSchedule> GetInstanceSchedules() {
			var result = new List<InstanceSchedule>();
			string sql = "SELECT * FROM instance_schedules ORDER BY order_no  ASC";
			foreach (DataRow r in db.GetDatatable(sql).Rows) {
				result.Add(new InstanceSchedule(r));
			}
			return result;
		}

		public void DeleteInstanceSchedule(int scheduleId) {
			SetDBParam(SqlDbType.Int, "@scheduleId", scheduleId);
			db.ExecuteDeleteQuery("DELETE FROM instance_schedules WHERE schedule_id = @scheduleId", DBParameters);
			TimedHostedService.ClearCache();
		}

		public InstanceSchedule AddInstanceSchedule(int groupId = 0) {
			var schedule = new InstanceSchedule();
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.DateTime, "@last_modified", DateTime.Now);
			SetDBParam(SqlDbType.Int, "@group_id", groupId);
			schedule.Name = null;
			schedule.ScheduleOptions = null;
			schedule.Instance = instance;
			schedule.ScheduleId =
				db.ExecuteInsertQuery(
					"INSERT INTO instance_schedules (instance,name,schedule_json, last_modified, order_no) VALUES (@instance,NULL,NULL,@last_modified, ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM instance_schedules WHERE ISNULL(group_id, 0) = @group_id ORDER BY order_no  DESC),null)),'U'))",
					DBParameters);

			if (groupId != 0) {
				SetDBParam(SqlDbType.Int, "@sid", schedule.ScheduleId);
				db.ExecuteUpdateQuery(
					"UPDATE instance_schedules SET group_id = @group_id, schedule_json = (SELECT schedule_json FROM instance_schedule_groups WHERE group_id = @group_id) WHERE schedule_id = @sid",
					DBParameters);
			}
			schedule.OrderNo =
				Conv.ToStr(db.ExecuteScalar("SELECT TOP 1 (order_no) FROM instance_schedules ORDER BY schedule_id DESC",
					DBParameters));
			return schedule;
		}

		public void SaveInstanceSchedule(List<InstanceSchedule> schedules) {
			foreach (var instanceSchedule in schedules) {
				if (instanceSchedule.GroupId == 0)
					instanceSchedule.GroupId = null;
				SetDBParam(SqlDbType.Int, "@schedule_id", instanceSchedule.ScheduleId);
				SetDBParam(SqlDbType.NVarChar, "@name", Conv.IfNullThenDbNull(instanceSchedule.Name));
				SetDBParam(SqlDbType.DateTime, "@last_modified", DateTime.Now);
				SetDBParam(SqlDbType.NVarChar, "@configOptions", Conv.IfNullThenDbNull(instanceSchedule.ConfigOptions));
				SetDBParam(SqlDbType.NVarChar, "@nickname", Conv.IfNullThenDbNull(instanceSchedule.Nickname));
				SetDBParam(SqlDbType.Int, "@disabled", instanceSchedule.Disabled);
				SetDBParam(SqlDbType.Int, "@compare", instanceSchedule.Compare);
				SetDBParam(SqlDbType.NVarChar, "@order_no", instanceSchedule.OrderNo);
				SetDBParam(SqlDbType.Int, "@groupId", Conv.IfNullThenDbNull(instanceSchedule.GroupId));
				if (instanceSchedule.ScheduleOptions == null) {
					db.ExecuteScalar(
						"UPDATE instance_schedules SET name = @name, nickname = @nickname, disabled = @disabled, compare = @compare, order_no = @order_no, config_options = @configOptions WHERE schedule_id = @schedule_id",
						DBParameters);
				} else {
					var classAsString = JsonConvert.SerializeObject(instanceSchedule.ScheduleOptions);
					SetDBParam(SqlDbType.NVarChar, "@json", classAsString);
					db.ExecuteScalar(
						"UPDATE instance_schedules SET name = @name, schedule_json = @json, last_modified = @last_modified, config_options = @configOptions, nickname = @nickname, disabled = @disabled, compare = @compare, order_no = @order_no, group_id = @groupId WHERE schedule_id = @schedule_id",
						DBParameters);
				}

				if (instanceSchedule.GroupId != 0) {
					db.ExecuteScalar(
						"UPDATE instance_schedules SET schedule_json = (SELECT schedule_json FROM instance_schedule_groups WHERE group_id = @groupId) WHERE group_id = @groupId",
						DBParameters);
				}

				TimedHostedService.ClearCache();
			}
		}

		public InstanceScheduleGroup SaveInstanceScheduleGroup(InstanceScheduleGroup group) {
			SetDBParam(SqlDbType.NVarChar, "@order_no", group.OrderNo);
			SetDBParam(SqlDbType.Int, "@group_id", group.GroupId);
			SetDBParam(SqlDbType.Int, "@disabled", group.Disabled);

			if (group.ScheduleOptions == null) {
				db.ExecuteScalar(
					"UPDATE instance_schedule_groups SET order_no = @order_no, disabled = @disabled WHERE group_id = @group_id",
					DBParameters);
			} else {
				var classAsString = JsonConvert.SerializeObject(group.ScheduleOptions);
				SetDBParam(SqlDbType.NVarChar, "@json", classAsString);
				db.ExecuteScalar(
					"UPDATE instance_schedule_groups SET order_no = @order_no, schedule_json = @json, disabled = @disabled WHERE group_id = @group_id",
					DBParameters);

				db.ExecuteScalar(
					"UPDATE instance_schedules SET schedule_json = @json WHERE group_id = @group_id",
					DBParameters);
			}

			var Translanslations = new Translations(instance, session);
			Translanslations.SaveInstanceTranslation(group.Translation, group.LanguageVariable);
			TimedHostedService.ClearCache();

			return group;
		}

		public void SaveInstanceDefaultUser(InstaceDefaults defaults) {
			SetDBParam(SqlDbType.Int, "@user_item_id", defaults.InstanceDefaultUser);
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@wwwpath", defaults.WwwPath);

			db.ExecuteScalar(
				"EXEC app_set_archive_user_id 0 UPDATE instances SET instance_default_user = @user_item_id, wwwpath = @wwwpath WHERE instance = @instance",
				DBParameters);
		}

		public InstaceDefaults GetInstanceDefaults() {
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			return new InstaceDefaults(
				db.GetDataRow("SELECT * FROM instances WHERE instance = @instance", DBParameters));
		}

		public List<ScheduleDate> GetBgLogResults(int scheduleId) {
			var result = new List<ScheduleDate>();
			SetDBParam(SqlDbType.Int, "@sid", scheduleId);
			var sql = "select TOP (25) * FROM instance_schedule_dates where schedule_id = @sid order by date desc";
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var scheduleDate = new ScheduleDate(row);
				SetDBParam(SqlDbType.Int, "@logId", Conv.ToInt(row["schedule_date_id"]));
				foreach (DataRow logRow in db.GetDatatable("SELECT * FROM instance_logs l LEFT JOIN _" + instance +"_user u ON l.user_item_id = u.item_id WHERE background_log_id = @logId", DBParameters).Rows) {
					scheduleDate.Info = new BgLogResult(logRow);
				}

				result.Add(scheduleDate);
			}
			return result;
		}

		public InstanceScheduleGroup AddInstanceScheduleGroup(InstanceScheduleGroup group) {

			var cb = new ConnectionBase(instance, session);
			var translations = new Translations(instance, session);
			var count = Conv.ToInt(cb.db.ExecuteScalar("SELECT COUNT(*) FROM instance_schedule_groups", DBParameters));

			SetDBParam(SqlDbType.NVarChar, "@variable", "variable_schedule_group_" + count);
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			var s = new Dictionary<string,string>()
			{
				{ "en-GB", "New Group" },
				{ "fi-FI", "Uusi ryhmä" },
			};

			translations.InsertInstanceTranslation(s, "variable_schedule_group_" + count);

			group.GroupId = cb.db.ExecuteInsertQuery(
				"INSERT INTO instance_schedule_groups (instance, schedule_json, order_no, variable) VALUES(@instance, null,ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM instance_schedule_groups ORDER BY order_no  DESC),null)),'U'), @variable)",
				DBParameters);

			group.Schedules = new List<InstanceSchedule>();
			group.LanguageVariable = "variable_schedule_group_" + count;
			group.Translation = s;
			group.OrderNo =
				Conv.ToStr(cb.db.ExecuteScalar("SELECT TOP 1 (order_no) FROM instance_schedule_groups ORDER BY group_id DESC",
					DBParameters));
			return group;
		}

		public void DeleteInstanceScheduleGroups(List<InstanceScheduleGroup> groups) {
			var cb = new ConnectionBase(instance, session);
			foreach (var group in groups) {
				SetDBParam(SqlDbType.Int, "@groupId", group.GroupId);
				cb.db.ExecuteUpdateQuery("UPDATE instance_schedules SET group_id = NULL, order_no = ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM instance_schedule_groups ORDER BY order_no  DESC),null)),'U') WHERE group_id = @groupId",
					DBParameters);
				cb.db.ExecuteDeleteQuery("DELETE FROM instance_schedule_groups WHERE group_id = @groupId",
					DBParameters);
				SetDBParam(SqlDbType.NVarChar, "@variable", group.LanguageVariable);
				cb.db.ExecuteDeleteQuery("DELETE FROM instance_translations WHERE variable = @variable", DBParameters);
			}
		}

		public class ScheduleDate
		{
			public BgLogResult Info { get; set; }
			public int Outcome { get; set; }

			public DateTime? Date { get; set; }

			public int ScheduleDateId { get; set; }

			public ScheduleDate(){}

			public ScheduleDate(DataRow r) {
				Outcome = Conv.ToInt(r["outcome"]);
				Info = new BgLogResult();
				Date = Conv.ToDateTime(r["date"]);
				ScheduleDateId = Conv.ToInt(r["schedule_date_id"]);
			}

		}

		public class BgLogResult
		{
			public string Message { get; set; }
			public DateTime? Occurence { get; set; }
			public UserResult User { get; set; }


			public BgLogResult(DataRow r) {
				Message = Conv.ToStr(r["message"]);
				Occurence = Conv.ToDateTime(r["occurence"]);
				User = new UserResult(r);
			}

			public BgLogResult(){}

		}

		public class UserResult
		{
			public UserResult(){}
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public int UserItemId  { get; set; }

			public UserResult(DataRow r) {
				FirstName = Conv.ToStr(r["first_name"]);
				LastName = Conv.ToStr(r["last_name"]);
				UserItemId = Conv.ToInt(r["user_item_id"]);
			}
		}

		public class InstaceDefaults
		{
			public InstaceDefaults(){}
			public int InstanceDefaultUser  { get; set; }
			public string WwwPath { get;  set;}

			public string DefaultCurrency { get; set; }

			public InstaceDefaults(DataRow r) {
				WwwPath = Conv.ToStr(r["wwwpath"]);
				InstanceDefaultUser = Conv.ToInt(r["instance_default_user"]);
				DefaultCurrency = Conv.ToStr(r["default_currency"]);
			}
		}
	}
}