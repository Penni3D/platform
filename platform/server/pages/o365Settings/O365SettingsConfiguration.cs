﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages.O365SettingsConfigurations {
    public class O365SettingsConfiguration {
        public O365SettingsConfiguration() {
        }
        
        public bool Enabled { get; set; }
        
        public string Tenant { get; set; }
        
        public string ClientId { get; set; }

        public string ClientKey { get; set; }

        public string LoginUrl { get; set; }

        

    }

    public class O365SettingsConfigurations : ConnectionBase {
        private AuthenticatedSession session;

        public O365SettingsConfigurations(string instance, AuthenticatedSession session) : base(instance, session) {
            this.instance = instance;
            this.session = session;
        }

        public O365SettingsConfiguration Get() {
       

            string sql = "SELECT * FROM instance_configurations WHERE category = 'azureadauthentication'";

            var i = new O365SettingsConfiguration();

            PropertyInfo[] properties = typeof(O365SettingsConfiguration).GetProperties();
            foreach (DataRow r in db.GetDatatable(sql).Rows) {        
                foreach (PropertyInfo p in properties) {
                
                    if (Conv.ToStr(r["name"]) == AppendAtPosition(Conv.ToStr(p.Name))) {
                        // Console.WriteLine(Conv.ToStr(p.PropertyType.Name));
                        //NOTE TO DEVELOPERS: IF MORE DATATYPES ARE ADDED TO CLASS PROPERTIES, PLEASE ADJUST THIS SWITCH ACCORDINGLY BEST REGARDS SALLARUU
                        switch (Conv.ToStr(p.PropertyType.Name)) {
                            
                            case "String":
                            p.SetValue(i, Conv.ToStr(r["value"]));
                                break;

                            case "Boolean":
                                p.SetValue(i, Conv.ToBool(r["value"]));
                                break;

                            case "Double":
                                p.SetValue(i, Conv.ToDouble(r["value"]));
                                break;

                            case "Integer":
                                p.SetValue(i, Conv.ToInt(r["value"]));
                                break;
                        }
                    }
                }
               
                var sdt = new DataTable();
                sdt = db.GetDatatable("SELECT * FROM instance_configurations WHERE category = 'azureadauthentication'", DBParameters);
          

            }
          
            return i;
        }

        public O365SettingsConfiguration Insert(O365SettingsConfiguration o365SettingsConfiguration) {

            //o365SettingsConfiguration.Enabled = false;
            //o365SettingsConfiguration.LoginUrl = "Terve";
            //o365SettingsConfiguration.Tenant = "Hello";
            //o365SettingsConfiguration.ClientId = "Kesä on kreisi";
            //o365SettingsConfiguration.ClientKey = "tsädääm";



            var i = new O365SettingsConfiguration();

         
            SetDBParam(SqlDbType.NVarChar, "@instance", instance);
            var sdt = new DataTable();
            sdt = db.GetDatatable("SELECT * FROM instance_configurations WHERE category = 'azureadauthentication'", DBParameters);
          //  var rows2 = sdt.Select("mies = " + xd);


            PropertyInfo[] properties = typeof(O365SettingsConfiguration).GetProperties();
            foreach (PropertyInfo property in properties) {               
                var exists = sdt.Select("name = '" + AppendAtPosition(property.Name) + "'");
                //Console.Write(exists.Length);

                SetDBParam(SqlDbType.NVarChar, "@name", AppendAtPosition(property.Name));

                SetDBParam(SqlDbType.NVarChar, "@value", property.GetValue(o365SettingsConfiguration));

                
                if (exists.Length == 0 && property.GetValue(o365SettingsConfiguration) != null) {
                    // Console.WriteLine(Conv.ToStr(property.Name));
                    //
                    // Console.WriteLine(property.GetValue(o365SettingsConfiguration));

                    db.ExecuteInsertQuery(" INSERT INTO instance_configurations(instance, process, group_name, category, name, value, server, domain) VALUES(@instance, null, null, 'azureadauthentication', @name, @value, 1, null)", DBParameters);

                    // INSERT INTO instance_configurations(instance, process, group_name, category, name, value, server, domain)
                    //   VALUES(@instance, @process, null, 'azureadauthentication', @name, @value, 1, null)
                } else {
                    if (property.GetValue(o365SettingsConfiguration) != null) {
                        db.ExecuteUpdateQuery(" UPDATE instance_configurations SET name = @name, value = @value WHERE category = 'azureadauthentication' and name = @name", DBParameters);

                    }

                }
            }

            return i;
        }
        public string AppendAtPosition(string baseString) {        
            var result = string.Concat(baseString.Select(c => char.IsUpper(c) ? "_" + c.ToString() : c.ToString()))
           .TrimStart();
            result = result.Remove(0, 1); 
            return result.ToLower();

        }

        public void Delete(string name) {
            
            SetDBParam(SqlDbType.NVarChar, "@name", AppendAtPosition(name));

            db.ExecuteDeleteQuery(" DELETE FROM instance_configurations WHERE name = @name ", DBParameters);
        }
        
    }

}