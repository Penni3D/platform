﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.pages.O365SettingsConfigurations;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.o365Settings {
    [Route("v1/core/[controller]")]
    public class O365SettingsRest : RestBase {
        [HttpGet("")]
        public O365SettingsConfiguration Get() {
            var iss = new O365SettingsConfigurations.O365SettingsConfigurations(instance, currentSession);
            return iss.Get();
        }

        [HttpPut("")]
        public void Save([FromBody] O365SettingsConfiguration o365SettingsConfiguration) {
            var iss = new O365SettingsConfigurations.O365SettingsConfigurations(instance, currentSession);
            iss.Insert(o365SettingsConfiguration);
            //..logiikkaa
        }

        [HttpDelete("{name}")]
        public void Delete(string name) {
           
            var iss = new O365SettingsConfigurations.O365SettingsConfigurations(instance, currentSession);
            iss.Delete(name);
        }

    }

}
