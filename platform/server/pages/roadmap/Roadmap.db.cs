﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages.roadmap {

	public class RoadmapDatas {
		public RoadmapDatas() {

		}

		public RoadmapDatas(DataRow row) {
			parent_item_id = (int)row["parent_item_id"];

			if (row["summary"] != DBNull.Value) {
				summary = (string)row["summary"];
			}

			if (row["header"] != DBNull.Value) {
				header = (string)row["header"];
			}

			if (row["region"] != DBNull.Value) {
				region = Conv.ToInt(row["region"]);
			} else {
				region = 0;
			}

			milestone_date = (DateTime)row["milestone_date"];

			if (row["milestone_approved_date"] != DBNull.Value) {
				milestone_approved_date = (DateTime)row["milestone_approved_date"];
			}

			if (row["show_internal"] != DBNull.Value) {
				show_internal = Convert.ToBoolean(row["show_internal"]);
			} else {
				show_internal = false;
			}

			if (row["show_external"] != DBNull.Value) {
				show_external = Convert.ToBoolean(row["show_external"]);
			} else {
				show_external = false;
			}
		}

		public string summary { get; set; }
		public int parent_item_id { get; set; }
		public string header { get; set; }
		public int region { get; set; }
		public bool show_internal { get; set; }
		public bool show_external { get; set; }
		public DateTime milestone_date { get; set; }
		public DateTime milestone_approved_date { get; set; }
		public int list_item_id { get; set; }
	}

	public class RoadmapData : ConnectionBase {
		private readonly AuthenticatedSession session;

		public RoadmapData(string instance, string process, AuthenticatedSession session) : base(instance, session) {
			this.instance = instance;
			this.session = session;
		}

		public List<RoadmapDatas> GetRoadmapData(string process, int year) {

			var retval = new List<RoadmapDatas>();

			var ib = new ItemBase(instance, process, session);

			var summary = "roadmap_summary";
			var header = "project_name";
			var show = "show_internal";
			var external = "show_external";

			SetDBParam(SqlDbType.Int, "@year", year);
			foreach (DataRow row in db.GetDatatable("SELECT ISNULL(pf." + summary + ",'') AS summary, ISNULL(pf." + header + ",'') AS header, (SELECT selected_item_id FROM item_data_process_selections WHERE item_id = pf.item_id AND item_column_id = 122) AS region, pf.hide_external, rfms.milestone_date, rfms.milestone_approved_date, rfms.parent_item_id, rfms.show_internal, rfms.show_external"
							    + " FROM _" + instance + "_synpulse_rf_milestones AS rfms"
							    + " LEFT JOIN _" + instance + "_" + process + " AS pf ON pf.item_id = rfms.parent_item_id"
							    + " WHERE (rfms." + show + " = 1  AND YEAR(rfms.milestone_date) = @year) OR (rfms." + external + " = 1  AND YEAR(rfms.milestone_approved_date) = @year) AND " + ib.EntityRepository.GetWhere(13), DBParameters).Rows) {
				var r = new RoadmapDatas(row);
				retval.Add(r);
			}

			return retval;
		}

		public List<RoadmapDatas> GetRoadmapData(string process, int year, DateTime archiveDate) {

			var retval = new List<RoadmapDatas>();
			var ib = new ItemBase(instance, process, session);

			var summary = "roadmap_summary";
			var header = "project_name";
			var show = "show_internal";
			var external = "show_external";

			SetDBParam(SqlDbType.Int, "@year", year);
			SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);

			foreach (DataRow row in db.GetDatatable("SELECT ISNULL(pf." + summary + ",'') AS summary, ISNULL(pf." + header + ",'') AS header, (SELECT selected_item_id FROM item_data_process_selections WHERE item_id = pf.item_id AND item_column_id = 122) AS region, pf.hide_external, rfms.milestone_date, rfms.milestone_approved_date, rfms.parent_item_id, rfms.show_internal, rfms.show_external"
							    + " FROM get__" + instance + "_synpulse_rf_milestones(@archiveDate) AS rfms"
							    + " RIGHT JOIN get__" + instance + "_" + process + "(@archiveDate) AS pf ON pf.item_id = rfms.parent_item_id"
							    + " WHERE (rfms." + show + " = 1  AND YEAR(rfms.milestone_date) = @year) OR (rfms." + external + " = 1  AND YEAR(rfms.milestone_approved_date) = @year) AND " + ib.EntityRepository.GetWhere(13), DBParameters).Rows) {
				var r = new RoadmapDatas(row);
				retval.Add(r);
			}

			return retval;
		}
	}
}
