﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.roadmap {
	[Route("v1/meta/[controller]")]
	public class roadmap : RestBase {
		public int endyear;
		public int startyear;


		[HttpGet("{process}/{year}/{type}")]
		public Dictionary<int, object> Get(string process, int year, int type) {
			var retval = new Dictionary<int, object>();
			var rm = new RoadmapData(instance, process, currentSession);

			//Nauti client konfig avaimen arvosta.
			// string header = getClientConfigKey("roadmap",process,"header");

			if (type == 0) {
				startyear = year - 2;
				endyear = year + 2;
			} else if (type == 1) {
				startyear = year - 1;
				endyear = year - 1;
			} else if (type == 2) {
				startyear = year + 1;
				endyear = year + 1;
			}

			while (startyear <= endyear) {
				var yearData = new List<RoadmapDatas>();
				yearData = rm.GetRoadmapData(process, startyear);
				retval.Add(startyear, yearData);
				startyear++;
			}

			return retval;
		}

		[HttpGet("{process}/{year}/{type}/{archiveDate}")]
		public Dictionary<int, object> Get(string process, int year, int type, DateTime archiveDate) {
			var retval = new Dictionary<int, object>();
			var rm = new RoadmapData(instance, process, currentSession);

			if (type == 0) {
				startyear = year - 2;
				endyear = year + 2;
			} else if (type == 1) {
				startyear = year - 1;
				endyear = year - 1;
			} else if (type == 2) {
				startyear = year + 1;
				endyear = year + 1;
			}

			while (startyear <= endyear) {
				var yearData = new List<RoadmapDatas>();
				yearData = rm.GetRoadmapData(process, startyear, archiveDate);
				retval.Add(startyear, yearData);
				startyear++;
			}

			return retval;
		}
	}
}