using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.recycleBin {
	[Route("v1/settings/RecycleBin")]
	public class RecycleBinRest : PageBase {
		public RecycleBinRest() : base("RecycleBin") { }

		[HttpGet("")]
		public List<RecycleBin.RecycleResult> GetData() {
			CheckRight("read");
			var recycled = new RecycleBin(instance, currentSession);
			return recycled.GetRecycleData();
		}

		[HttpGet("{process}")]
		public List<RecycleBin.RecycleResult> GetData(string process) {
			CheckRight("read");
			var recycled = new RecycleBin(instance, currentSession);
			return recycled.GetRecycleData(process);
		}

		[HttpPost("restore")]
		public List<ItemBase.RestoreResult> RestoreItem([FromBody] RecycleBin.RestoreRequestItem request) {
			CheckRight("write");
			var recycled = new RecycleBin(instance, currentSession);
			return recycled.RestoreItem(request, ItemBase.RestoreMode.FastGet);
		}
		[HttpPost("simulate")]
		public List<ItemBase.RestoreResult> SimulateRestore([FromBody] RecycleBin.RestoreRequestItem request) {
			CheckRight("write");
			var recycled = new RecycleBin(instance, currentSession);
			return recycled.RestoreItem(request, ItemBase.RestoreMode.GetAndTest);
		}
		[HttpPost("commit")]
		public List<ItemBase.RestoreResult> CommitRestore([FromBody] RecycleBin.RestoreRequestItem request) {
			CheckRight("write");
			var recycled = new RecycleBin(instance, currentSession);
			return recycled.RestoreItem(request, ItemBase.RestoreMode.Restore);
		}
	}
}