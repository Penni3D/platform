using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DocumentFormat.OpenXml.Drawing.Charts;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages.recycleBin {
	public class RecycleBin : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;
		private readonly Translations translations;

		public RecycleBin(string instance, AuthenticatedSession a) : base(instance, a) {
			authenticatedSession = a;
			translations = new Translations(instance, authenticatedSession);
		}

		public class RecycleResult {
			public int ArchiveId { get; set; }
			public int ItemId { get; set; }
			public DateTime? DeletedDate { get; set; }
			public string Process { get; set; }

			public int DeletedBy { get; set; }
			public List<string> Identifiers { get; set; }

			public RecycleResult(DataRow d, List<string> columnNames) {
				ArchiveId = Conv.ToInt(d["archive_id"]);
				ItemId = Conv.ToInt(d["item_id"]);
				DeletedDate = Conv.ToDateTime(d["archive_end"]);
				Process = Conv.ToStr(d["process"]);
				DeletedBy = Conv.ToInt(d["archive_userid"]);

				Identifiers = new List<string>();
				foreach (var col in columnNames) {
					Identifiers.Add(Conv.ToStr(d[col.ToLower().Replace("'?' as ", "")]));
				}
			}
		}

		public class RestoreRequestItem {
			public int ArchiveId;
			public int ItemId;
		}

		public List<RecycleResult> GetRecycleData(string limitProcess = "") {
			var result = new List<RecycleResult>();

			SetDBParam(SqlDbType.NVarChar, "@process", limitProcess);
			var limitWhere = limitProcess == "" ? "" : " AND process = @process";

			var primariesSql = "SELECT  " +
			                   " oq.process,  " +
			                   " ISNULL(CASE WHEN p.list_process = 1 THEN ISNULL(oq.colname, 'list_item') ELSE oq.colname END,'?') AS name  " +
			                   " FROM ( " +
			                   "       SELECT DISTINCT process, cols.* " +
			                   "       FROM items_deleted id " +
			                   "       LEFT JOIN ( " +
			                   "               SELECT ic.name as colname, ic.process AS colprocess, ppc.order_no as colorder " +
			                   "               FROM process_portfolio_columns ppc   " +
			                   "                   INNER JOIN ( " +
			                   "                   SELECT process, process_portfolio_id, variable, process_portfolio_type   " +
			                   "                   FROM process_portfolios   " +
			                   "                   WHERE process_portfolio_id IN(   " +
			                   "                   SELECT process_portfolio_id " +
			                   "                   FROM process_portfolios   " +
			                   "                   WHERE process_portfolio_type IN (1, 10) AND instance = @instance " +
			                   limitWhere + ")) pf   " +
			                   "                   ON pf.process_portfolio_id = ppc.process_portfolio_id   " +
			                   "               INNER JOIN item_columns ic   " +
			                   "               ON ic.item_column_id = ppc.item_column_id   " +
			                   "               WHERE use_in_select_result = 1  " +
			                   "       ) cols ON cols.colprocess = id.process " +
			                   "       WHERE instance = @instance" +
			                   " ) oq " +
			                   " LEFT JOIN processes p ON p.process = oq.process " +
			                   " ORDER BY oq.process ASC, colorder  ASC";

			var itemsSql =
				"SELECT TOP 100 item_id, process, deleted_date FROM items_deleted WHERE instance = @instance " +
				limitWhere + " ORDER BY deleted_date DESC";

			var primaryRows = db.GetDatatable(primariesSql, DBParameters);
			var deletedItems = db.GetDatatable(itemsSql, DBParameters);

			foreach (DataRow row in deletedItems.Rows) {
				SetDBParam(SqlDbType.DateTime, "@deleted_date", row["deleted_date"]);
				SetDBParam(SqlDbType.Int, "@item_id", row["item_id"]);

				var process = Conv.ToStr(row["process"]);
				var primaryCols = new List<string>();
				foreach (var col in primaryRows.Select("process = '" + process + "'")) {
					if (Conv.ToStr(col["name"]) == "?") {
						primaryCols.Add("'?' as unkown");
					} else {
						primaryCols.Add(Conv.ToStr(col["name"]));
					}
				}

				var dataSql =
					"SELECT TOP 1 archive_id, archive_end, archive_userid, item_id, '" + process + "' AS process, " +
					Modi.JoinListWithComma(primaryCols) + " FROM archive__" + instance + "_" + process +
					" WHERE item_id = @item_id AND archive_end >= @deleted_date ORDER BY archive_id ASC";

				result.Add(new RecycleResult(db.GetDataRow(dataSql, DBParameters), primaryCols));
			}

			return result;
		}

		public List<ItemBase.RestoreResult> RestoreItem(RestoreRequestItem item, ItemBase.RestoreMode mode) {
			var ib = new ItemBase(instance, "", authenticatedSession);
			return ib.Restore(item.ItemId, mode);
		}
	}
}