using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.pages.Currencies;
using Keto5.x.platform.server.pages.instanceSchedules;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.currencies
{
	[Route("v1/core/[controller]")]
	public class CurrenciesRest : PageBase
	{
		[HttpGet("")]
		public List<CurrencyValue> GetCurrencyValues() {
			CheckRight("read");
			var c = new CurrencyValues(instance, currentSession);
			return c.GetCurrencyValues();
		}

		[HttpGet("currencies")]
		public List<Currency> GetInstanceCurrencies() {
			CheckRight("read");
			var c = new Currencies.Currencies(instance, currentSession);
			return c.GetInstanceCurrencies();
		}

		[HttpGet("instance_defaults")]
		public InstanceSchedules.InstaceDefaults GetInstanceDefaults() {
			CheckRight("read");
			var iss = new InstanceSchedules(instance, currentSession);
			return iss.GetInstanceDefaults();
		}

		[HttpPost("")]
		public Currency InsertInstanceCurrency([FromBody] Currency currency) {
			CheckRight("read");
			var c = new Currencies.Currencies(instance, currentSession);
			return c.InsertNewCurrency(currency);
		}

		[HttpPut("")]
		public List<Currency> SaveInstanceCurrencies([FromBody] List<Currency> currencies) {
			CheckRight("write");
			var c = new Currencies.Currencies(instance, currentSession);
			return c.SaveCurrencies(currencies);
		}

		[HttpPut("default")]
		public bool SaveInstanceDefaultCurrency([FromBody] InstanceSchedules.InstaceDefaults defaults) {
			CheckRight("write");
			var c = new Currencies.Currencies(instance, currentSession);
			return c.SaveInstanceDefaultCurrency(defaults);
		}

		[HttpDelete("{currencyId}/{abbr}")]
		public void DeleteInstanceCurrency(int currencyId, string abbr) {
			CheckRight("write");
			var c = new Currencies.Currencies(instance, currentSession);
			c.DeleteInstanceCurrency(currencyId, abbr);
		}

		public CurrenciesRest() : base("currencies") { }
	}
}