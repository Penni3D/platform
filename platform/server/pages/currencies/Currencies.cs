using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages.Currencies {
    public class CurrencyValue {
        public CurrencyValue() { }

        public CurrencyValue(DataRow r) {
            ExchangeRate = Conv.ToDouble(r["exchange_rate"]);
            CurrencyId = Conv.ToInt(r["currency_id"]);
            Date = Conv.ToDateTime(r["date"]);
            CurrentDefault = Conv.ToInt(r["current_default"]);
            ListItemId = Conv.ToInt(r["item_id"]);
        }
        public double ExchangeRate { get; set; }

        public int CurrencyId { get; set; }

        public int CurrentDefault { get; set; }

        public int ListItemId  { get; set; }

        public DateTime? Date { get; set; }
    }

    public class Currency {
        public Currency() { }
        public Currency(DataRow r) {
            Abbreviation = Conv.ToStr(r["abbr"]);
            CurrencyId = Conv.ToInt(r["currency_id"]);
            Name = Conv.ToStr(r["name"]);
            ListItemId = Conv.ToInt(r["item_id"]);
            OldAbbreviation = Conv.ToStr(r["abbr"]);
        }
        public string Abbreviation { get; set; }
        public string Name { get; set; }

        public int CurrencyId { get; set; }
        public double CurrentExchangeRate { get; set; }

        public int ListItemId {get; set;}

        public Dictionary<string,string> Translations { get; set; }

        public string OldAbbreviation { get; set; }

    }

    public class CurrencyValues : ConnectionBase {
        private AuthenticatedSession session;
        public CurrencyValues(string instance, AuthenticatedSession session) : base(instance, session) {
            this.instance = instance;
            this.session = session;
        }
        public List<CurrencyValue> GetCurrencyValues() {
            var result = new List<CurrencyValue>();
            var cb = new ConnectionBase(instance, session);
            SetDBParam(SqlDbType.NVarChar, "@instance", instance);

            foreach (DataRow r in cb.db.GetDatatable("SELECT * FROM instance_currencies_values icv INNER JOIN instance_currencies ic ON ic.currency_id = icv.currency_id INNER JOIN _"+ instance + "_list_system_currencies sy ON sy.list_item = ic.abbr WHERE ic.instance = @instance", cb.DBParameters).Rows) {
                result.Add(new CurrencyValue(r));
            }
            return result;
        }
    }

    public class Currencies : ConnectionBase {
        private AuthenticatedSession session;
        public Currencies(string instance, AuthenticatedSession session) : base(instance, session) {
            this.instance = instance;
            this.session = session;
        }

        public List<Currency> GetInstanceCurrencies() {
            var result = new List<Currency>();
            var cb = new ConnectionBase(instance, session);
            var Translations = new Translations(instance, session);
            cb.SetDBParam(SqlDbType.NVarChar, "@instance", instance);
            foreach (DataRow r in cb.db.GetDatatable("SELECT ic.*, sy.item_id FROM instance_currencies ic INNER JOIN _" + instance + "_list_system_currencies sy ON sy.list_item = ic.abbr WHERE instance = @instance", cb.DBParameters).Rows) {
                var c = new Currency(r) {Translations = Translations.GetInstanceTranslations(Conv.ToStr(r["name"]))};
                cb.SetDBParam(SqlDbType.Int, "@ci", Conv.ToInt(r["currency_id"]));
                c.CurrentExchangeRate = Conv.ToDouble(cb.db.ExecuteScalar("SELECT exchange_rate FROM instance_currencies_values WHERE currency_id = @ci AND current_default = 1", cb.DBParameters));
                result.Add(c);
            }
            return result;
        }

        public Currency InsertNewCurrency(Currency newCurrency) {
            var cb = new ConnectionBase(instance, session);
            var Translations = new Translations(instance, session);

            var languageVariable = Translations.GetTranslationVariable("CURRENCY", newCurrency.Abbreviation, "VARIABLE");
            cb.SetDBParam(SqlDbType.NVarChar, "@instance", instance);
            cb.SetDBParam(SqlDbType.NVarChar, "@abbr", newCurrency.Abbreviation);
            cb.SetDBParam(SqlDbType.NVarChar, "@n", languageVariable);

            cb.db.ExecuteInsertQuery(
                "INSERT INTO instance_currencies (instance, [name], abbr) VALUES (@instance, @n, @abbr)",
                cb.DBParameters);
            Translations.InsertInstanceTranslation(newCurrency.Translations, languageVariable);
            newCurrency.CurrencyId =
                Conv.ToInt(cb.db.ExecuteScalar("SELECT TOP 1 currency_id FROM instance_currencies ORDER BY currency_id DESC"));

            var ib = new ItemBase(instance, "list_system_currencies", session);
            var newListItem = new Dictionary<string, object>();

            newListItem.Add("list_item", newCurrency.Abbreviation);

            ib.AddItem(newListItem);

            Cache.Initialize();

            newCurrency.OldAbbreviation = newCurrency.Abbreviation;
            return newCurrency;
        }

        public List<Currency> SaveCurrencies(List<Currency> currencies) {
            var cb = new ConnectionBase(instance, session);
            var Translations = new Translations(instance, session);
            foreach (var currency in currencies) {
                cb.SetDBParam(SqlDbType.Int, "@ci", currency.CurrencyId);
                cb.SetDBParam(SqlDbType.NVarChar, "@instance", instance);
                cb.SetDBParam(SqlDbType.NVarChar, "@abbr", currency.Abbreviation);
                Translations.SaveInstanceTranslation(currency.Translations, currency.Name);
                if (currency.OldAbbreviation != null) {
                    cb.SetDBParam(SqlDbType.NVarChar, "@oldAbbr", currency.OldAbbreviation);
                    cb.db.ExecuteUpdateQuery(
                        "UPDATE _" + instance +
                        "_list_system_currencies SET list_item = @abbr WHERE list_item = @oldAbbr", cb.DBParameters);
                }
                cb.db.ExecuteUpdateQuery(
                    "UPDATE instance_currencies SET abbr = @abbr WHERE currency_id = @ci AND instance = @instance",
                    cb.DBParameters);
                var oldDefault =
                    Conv.ToDouble(
                        cb.db.ExecuteScalar(
                            "SELECT exchange_rate FROM instance_currencies_values WHERE currency_id = @ci AND current_default = 1", cb.DBParameters));

                if (oldDefault != currency.CurrentExchangeRate) {
                    cb.SetDBParam(SqlDbType.Decimal, "@exchange_rate", currency.CurrentExchangeRate);
                    cb.SetDBParam(SqlDbType.Date, "@date", DateTime.Now);
                    cb.db.ExecuteUpdateQuery(
                        "UPDATE instance_currencies_values SET current_default = 0 WHERE currency_id = @ci",
                        cb.DBParameters);
                    cb.db.ExecuteInsertQuery(
                        "INSERT INTO instance_currencies_values (instance,currency_id,exchange_rate,date,current_default) VALUES(@instance, @ci,@exchange_rate, @date, 1)",
                        cb.DBParameters);
                }
            }
            return currencies;
        }

        public bool SaveInstanceDefaultCurrency(InstanceSchedules.InstaceDefaults defaults) {
            SetDBParam(SqlDbType.NVarChar, "@dc", defaults.DefaultCurrency);
            SetDBParam(SqlDbType.NVarChar, "@instance", instance);
            db.ExecuteScalar(
                "EXEC app_set_archive_user_id 0 UPDATE instances SET default_currency = @dc WHERE instance = @instance",
                DBParameters);
            return true;
        }

        public void DeleteInstanceCurrency(int currencyId, string abbr) {
            var cb = new ConnectionBase(instance, session);
            var ib = new ItemBase(instance, "list_system_currencies", session);
            cb.SetDBParam(SqlDbType.Int, "@ci", currencyId);
            cb.SetDBParam(SqlDbType.NVarChar, "@abbr", abbr);
            var listItemId = Conv.ToInt(cb.db.ExecuteScalar("SELECT item_id FROM _" + instance + "_list_system_currencies WHERE list_item = @abbr",cb.DBParameters));
            ib.Delete(listItemId);
            cb.db.ExecuteDeleteQuery("DELETE FROM instance_currencies WHERE currency_id = @ci", cb.DBParameters);
        }
    }
}