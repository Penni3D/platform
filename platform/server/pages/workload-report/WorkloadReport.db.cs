using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core.resourceManagement;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages {
	public enum WorkloadReportResolution {
		Week,
		Month
	}

	public enum WorkloadReportMode {
		ResourceApproval,
		Other
	}

	public class WorkloadReportSelections {
		public DateTime? from { get; set; }

		public DateTime? to { get; set; }

		public WorkloadReportResolution resolution { get; set; }

		public WorkloadReportMode mode { get; set; }

		public List<int> competencies { get; set; }

		public List<int> organizations { get; set; }

		public List<int> costCenter { get; set; }

		public string process { get; set; }
		public string processColumn { get; set; }
	}

	public class WorkloadReportService : ConnectionBase {
		private const string DateDivider = "/";
		private const string Year = "year";
		private const string Week = "week";
		private const string Month = "month";

		private readonly AuthenticatedSession _session;

		public WorkloadReportService(AuthenticatedSession currentSession) : base(currentSession.instance,
			currentSession) {
			_session = currentSession;
			instance = currentSession.instance;
		}

		/*   PUBLIC METHODS */

		public Dictionary<string, object> GetData(WorkloadReportSelections options) {
			if (options == null) {
				throw new CustomArgumentException("No selections");
			}

			if (options.from == null) {
				options.from = DateTime.Now.AddMonths(-2);
			}

			if (options.to == null) {
				options.to = DateTime.Now.AddMonths(2);
			}

			if (options.mode.Equals(WorkloadReportMode.ResourceApproval)) {
				return GetResourceApproval(options);
			}

			return GetHoursGroupedByField(options);
		}

		public Dictionary<string, object> GetOptions() {
			var p = new Processes(instance, _session);
			var dictionary = new Dictionary<string, object> {
				{"competencies", getCompetencies()},
				{"organizations", getOrganizations()},
				{"processes", p.GetProcesses(0)},
				{"costCenters", GetCostCenters()}
			};
			return dictionary;
		}


		/*   PRIVATE METHODS */


		// *********** QUERY STUFF FOR FETCHING OPTIONS
		private List<Dictionary<string, object>> getCompetencies() {
			var itemBase = new ItemBase(instance, "competency", _session);
			return db.DataTableToDictionary(itemBase.GetAllData());
		}

		private List<Dictionary<string, object>> getOrganizations() {
			var itemBase = new ItemBase(instance, "organisation", _session);
			return db.DataTableToDictionary(itemBase.GetAllData());
		}

		private List<Dictionary<string, object>> GetCostCenters() {
			var itemBase = new ItemBase(instance, "list_cost_center", _session);
			return db.DataTableToDictionary(itemBase.GetAllData());
		}

		private string GetQueryForProcessData(WorkloadReportSelections options) {
			var resolution = options.resolution.Equals(WorkloadReportResolution.Month) ? "MONTH" : "WEEK";
			var processTableName = GetProcessTableName(options.process);
			var allocationTableName = GetProcessTableName("allocation");
			var sql = "SELECT process.item_id, ad.date, ad.hours hours, DATEPART(YEAR, date) year, DATEPART(" +
			          resolution +
			          ", date) subDate " +
			          "FROM " + processTableName + " process " +
			          "JOIN " + allocationTableName + " alloc ON alloc.process_item_id = process.item_id " +
			          "JOIN allocation_durations ad ON ad.allocation_item_id = alloc.item_id " +
			          "WHERE date BETWEEN @from AND @to ";
			return sql;
		}

		private string GetQueryForColumnBasedData(WorkloadReportSelections options) {
			var processTable = GetProcessTableName(options.process);
			SetDBParameters("@instance", instance, "@process", options.process, "@colName", options.processColumn);
			var resolution = options.resolution.Equals(WorkloadReportResolution.Month) ? "MONTH" : "WEEK";
			var allocationTableName = GetProcessTableName("allocation");
			var sql =
				"SELECT idps.selected_item_id item_id, ad.date, ad.hours hours, DATEPART(YEAR, date) year, DATEPART(" +
				resolution + ", date) subDate " +
				"FROM " + processTable + " process " +
				"JOIN item_data_process_selections idps ON process.item_id = idps.item_id " +
				"JOIN item_columns ic ON ic.item_column_id = idps.item_column_id " +
				"JOIN " + allocationTableName + " alloc ON alloc.process_item_id = process.item_id " +
				"JOIN allocation_durations ad ON ad.allocation_item_id = alloc.item_id " +
				"WHERE ic.instance = @instance AND ic.process=@process AND ic.name = @colName " +
				"AND date BETWEEN @from AND @to ";
			return sql;
		}

		private string GetQueryForOptions(WorkloadReportSelections options) {
			var query = "";
			if (!string.IsNullOrEmpty(options.process)) {
				SetDBParameters("@from", options.from, "@to", options.to);
				if (!string.IsNullOrEmpty(options.processColumn)) {
					query = GetQueryForColumnBasedData(options);
				}

				query = GetQueryForProcessData(options);
			}

			if (string.IsNullOrEmpty(query)) {
				return "";
			}

			return "SELECT concat(q.subDate, concat('/', q.YEAR)) date, sum(q.hours) hours FROM (" +
			       query + ") q " +
			       "GROUP BY q.year, q.subDate " +
			       "ORDER BY q.year, q.subDate";
		}

		private List<object> PickItemIds(string query) {
			return db.GetDatatableDictionary(query).Select(s => s["item_id"]).ToList();
		}

		private string GetTaskDurationQueryByResolution(WorkloadReportSelections options, List<int> allTaskIds) {
			string monthOrWeekQuery;
			var groupingColumnsList = new List<string>();
			var yearPartForQuery = "datepart(YEAR, date) ";

			if (options.resolution.Equals(WorkloadReportResolution.Week)) {
				groupingColumnsList.Add(WorkloadReportResolution.Week.ToString());
				monthOrWeekQuery = "datepart(WEEK, date) ";
			} else {
				groupingColumnsList.Add(WorkloadReportResolution.Month.ToString());
				monthOrWeekQuery = "datepart(MONTH, date) ";
			}

			var concatHell = "CONCAT(CONCAT(" + monthOrWeekQuery + ",'/'), " + yearPartForQuery + ") ";

			var sql = "SELECT task_item_id, sum(hours) hours, dateValue FROM " +
			          "(SELECT " + concatHell + " dateValue, task_item_id, hours FROM task_durations " +
			          "WHERE hours > 0 AND task_item_id IN (" + string.Join(",", allTaskIds) + ") ) q " +
			          "GROUP BY task_item_id, dateValue";
			return sql;
		}


		private Dictionary<string, object> GetTaskData(WorkloadReportSelections options) {
			var process = options.process;
			var processTable = GetProcessTableName(process);
			var taskTable = GetProcessTableName("task");
			var mainLevelItems = "SELECT distinct item_id FROM " + processTable;
			var items = db.GetDatatableDictionary(mainLevelItems);

			var maineLevelIds = new List<int>();
			var childTree = new Dictionary<int, object>();
			foreach (var mainLevelItem in items) {
				var itemId = (int) mainLevelItem["item_id"];
				maineLevelIds.Add(itemId);
			}

			var taskTableName = GetProcessTableName("task");
			var sqlForAllTasks = "SELECT t.* FROM " + processTable + " process " +
			                     "JOIN " + taskTableName + " t ON t.owner_item_id = process.item_id";
			var tasks = db.GetDatatableDictionary(sqlForAllTasks, DBParameters);

			foreach (var task in tasks) {
				var parentId = (int) task["parent_item_id"];
				var itemId = (int) task["item_id"];
				var taskType = (int) task["task_type"];
				var effort = Conv.ToDouble(task["effort"]);
				var actualEffort = Conv.ToDouble(task["actual_effort"]);
				var startDate = Conv.ToDateTime(task["start_date"]);
				var endDate = Conv.ToDateTime(task["end_date"]);
				var actualStartDate = Conv.ToDateTime(task["actual_start_date"]);
				var actualEndDate = Conv.ToDateTime(task["actual_end_date"]);
				InsertChildParentRelation(
					childTree,
					parentId,
					itemId,
					taskType,
					effort,
					actualEffort,
					startDate,
					endDate,
					actualStartDate,
					actualEndDate);
			}

			var keys = childTree.Keys.ToList();
			foreach (var id in keys) {
				if (!maineLevelIds.Contains(id)) {
					childTree.Remove(id);
				}
			}

			var dictToReturn = new Dictionary<string, object>();

			if (!string.IsNullOrEmpty(options.processColumn)) {
				SetDBParameters("@fieldName", options.processColumn);
				var listItemName =
					"SELECT data_additional FROM item_columns WHERE INSTANCE=@instance AND process=@process AND name=@fieldName";
				var field = Conv.ToStr(db.ExecuteScalar(listItemName, DBParameters));
				var groupColumns = new ItemBase(instance, field, _session);

				dictToReturn.Add("listItems", groupColumns.GetAllData());
			}

			//dictToReturn.Add("durations", taskDurations);
			dictToReturn.Add("relations", childTree);
			dictToReturn.Add("processRows", GetProcessRowsWithListColumn(options));
			return dictToReturn;
		}


		private List<Dictionary<string, object>> GetProcessRowsWithListColumn(WorkloadReportSelections options) {
			var column = options.processColumn;
			var process = options.process;
			var processTable = GetProcessTableName(process);
			if (string.IsNullOrEmpty(column)) {
				var sql = "SELECT process.* FROM " + processTable + " process ";
				return db.GetDatatableDictionary(sql, DBParameters);
			} else {
				SetDBParameters(
					"@columnName", column,
					"@processName", process,
					"@instance", instance
				);
				var sql = "SELECT idps.selected_item_id AS _groupByColumn, process.* " +
				          "FROM " + processTable + " process " +
				          "JOIN ITEM_DATA_PROCESS_SELECTIONS idps ON process.item_id = idps.item_id " +
				          "JOIN item_columns ic ON ic.item_column_id = idps.item_column_id " +
				          "WHERE ic.INSTANCE = @instance AND ic.process=@processName AND ic.name = @columnName";
				return db.GetDatatableDictionary(sql, DBParameters);
			}
		}

		private void InsertChildParentRelation(
			Dictionary<int, object> childTree,
			int parentId,
			int itemId,
			int taskType,
			double effort,
			double actualEffort,
			DateTime? from,
			DateTime? to,
			DateTime? actualFrom,
			DateTime? actualTo) {
			if (!childTree.ContainsKey(parentId)) {
				childTree.Add(parentId, new Dictionary<string, object>());
			}

			var childrenDict = (Dictionary<string, object>) childTree[parentId];
			Dictionary<string, object> dictToAdd;
			if (childTree.ContainsKey(itemId)) {
				dictToAdd = (Dictionary<string, object>) childTree[itemId];
			} else {
				dictToAdd = new Dictionary<string, object>();
				childTree.Add(itemId, dictToAdd);
			}

			dictToAdd.Add("effort", effort);
			dictToAdd.Add("from", from);
			dictToAdd.Add("to", to);
			dictToAdd.Add("actual_effort", actualEffort);
			dictToAdd.Add("actual_from", actualFrom);
			dictToAdd.Add("actual_to", actualTo);
			dictToAdd.Add("task_type", taskType);

			childrenDict.Add(itemId.ToString(), dictToAdd);
		}

		private Dictionary<string, object> GetHoursGroupedByField(WorkloadReportSelections options) {
			var sql = GetQueryForOptions(options);
			var items = db.GetDatatableDictionary(sql, DBParameters);
			var returnDictionary = new Dictionary<string, object>();
			returnDictionary.Add("processData", items);
			returnDictionary.Add("options", options);
			returnDictionary.Add("taskDurations", GetTaskData(options));
			return returnDictionary;
		}

		// *********** QUERY STUFF FOR FETCHING REPORT DATA 
		/*private string _getSqlConcat(string field1, string field2){
			return "concat(concat(" + field1 + ", '" + DateDivider + "'), " + field2 + ") as date";
		}*/

		private Dictionary<string, object> GetResourceApproval(WorkloadReportSelections options) {
			var groupingColumnsList = new List<string>();
			string monthOrWeekQuery;
			var yearPartForQuery = "datepart(YEAR, date) AS year ";
			if (options.resolution.Equals(WorkloadReportResolution.Week)) {
				groupingColumnsList.Add(WorkloadReportResolution.Week.ToString());
				monthOrWeekQuery = "datepart(WEEK, date) AS  subValue";
			} else {
				groupingColumnsList.Add(WorkloadReportResolution.Month.ToString());
				monthOrWeekQuery = "datepart(MONTH, date) AS subValue";
			}

			groupingColumnsList.Add(Year);
			var groupingColumns = string.Join(", ", groupingColumnsList);

			var conditions = BuildCondition(options);


			var queryToFilterAllocations = BuildSelectQuery()
				.Columns("hours", "status", monthOrWeekQuery, yearPartForQuery)
				.From(Conv.ToSql("_" + instance + "_allocation"), "alloc")
				.Join(new Join("allocation_durations", "ad").On("ad.allocation_item_id", "alloc.item_id"))
				.Where(conditions);

			var queryForSumUp = BuildSelectQuery()
				.Columns("round(sum(hours), 1) hours", "status", "year", "subValue")
				.From(queryToFilterAllocations, "q")
				.GroupBy("status", "subValue", "year")
				.OrderBy("year", "subValue");

			var dictToReturn = new Dictionary<string, object>();
			dictToReturn.Add("values", queryForSumUp.Fetch());
			dictToReturn.Add("dateColumns", groupingColumns);

			var list = GetListOfTimeSpanSplitInPieces(
				options.resolution,
				(DateTime) options.from,
				(DateTime) options.to
			);
			dictToReturn.Add("dates", list);
			dictToReturn.Add("translations", GetAllocationStatusTranslationKeys());

			return dictToReturn;
		}

		private SqlCondition BuildCondition(WorkloadReportSelections options) {
			var fromParameter = SetDBParameter("@fromDate", options.from);
			var toParameter = SetDBParameter("@toDate", options.to);
			var conditions = new SqlCondition()
				.Column("ad.date")
				.Between(fromParameter, toParameter);

			conditions = conditions.And(
				new SqlCondition()
					.RawSql("alloc.status > 10"));

			if (options.costCenter != null && options.costCenter.Count > 0) {
				conditions = conditions.And(
					new SqlCondition()
						.Column("alloc.cost_center")
						.In(options.costCenter));
			}

			if (options.competencies != null && options.competencies.Count > 0) {
				conditions = conditions.And(
					new SqlCondition()
						.Column("alloc.resource_item_id")
						.In(options.competencies));
			}

			if (options.organizations != null && options.organizations.Count > 0) { }

			return conditions;
		}

		private Dictionary<int, string> GetAllocationStatusTranslationKeys() {
			var dict = new Dictionary<int, string>();
			foreach (AllocationStatus status in Enum.GetValues(typeof(AllocationStatus))) {
				dict.Add((int) status, status.ToString()); // change to translation keys when they exist 
			}

			return dict;
		}

		private int GetWeekNumber(DateTime dtPassed) {
			var ciCurr = CultureInfo.CurrentCulture;
			var weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
			return weekNum;
		}

		private string GetWithTwoDigits(int value) {
			if (value < 10) {
				return "0" + value;
			}

			return value.ToString();
		}

		private List<string> GetListOfTimeSpanSplitInPieces(WorkloadReportResolution resolution, DateTime startDate,
			DateTime endDate) {
			var list = new List<string>();
			var dateCursor = startDate;
			while (dateCursor < endDate) {
				DateTime next;
				string monthOrWeekPart;
				if (resolution.Equals(WorkloadReportResolution.Week)) {
					monthOrWeekPart = GetWithTwoDigits(GetWeekNumber(dateCursor));
					next = dateCursor.AddDays(7);
				} else {
					monthOrWeekPart = GetWithTwoDigits(dateCursor.Month);
					next = dateCursor.AddMonths(1);
				}

				list.Add(dateCursor.Year + DateDivider + monthOrWeekPart);
				dateCursor = next;
			}

			return list;
		}
	}
}