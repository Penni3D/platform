using System;
using System.Collections.Generic;
using System.Globalization;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages {
	[Route("v1/resources/[controller]")]
	public class WorkloadReport : RestBase {
		private WorkloadReportService GetReportService() {
			return new WorkloadReportService(currentSession);
		}

		[HttpPost]
		public IActionResult GetData([FromBody] WorkloadReportSelections options) {
			var service = GetReportService();
			return new ObjectResult(service.GetData(options));
		}

		[HttpGet("options")]
		public IActionResult GetSelectionOptions() {
			return new ObjectResult(GetReportService().GetOptions());
		}

		[HttpGet("{from}/{to}/{queryIndex}")]
		public IActionResult GetUserUtilisation(string from, string to, int queryIndex) {
			var service = new WorkloadUtilisationService(currentSession);
			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			var listOfItems = service.GetUtilisation(fromDate, toDate, 0, "", "", queryIndex);

			return new ObjectResult(listOfItems);
		}

		[HttpPost("competencies/{from}/{to}/{item_id}/{queryIndex}")]
		public List<WorkloadUtilisationService.WorkloadCompetencyItem> GetCompetencyUtilisation(string from, string to, int item_id, int queryIndex, [FromBody] List<int> competencyIds) {
			var service = new WorkloadUtilisationService(currentSession);
			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			return service.GetCompetencyUtilisation(fromDate, toDate, item_id, competencyIds, queryIndex);
		}

		[HttpGet("{from}/{to}/{portfolioId}/{filterText}/{filters}/{queryIndex}")]
		public IActionResult GetUserUtilisation(string from, string to, int portfolioId, string filterText,
			string filters, int queryIndex) {
			filters = filters?.Replace("@", ";");
			var service = new WorkloadUtilisationService(currentSession);
			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			var listOfItems = service.GetUtilisation(fromDate, toDate, portfolioId, filterText, filters, queryIndex);

			return new ObjectResult(listOfItems);
		}

		private static DateTime DayAndMonthToDateTime(string dateString) {
			if (string.IsNullOrEmpty(dateString)) {
				throw new CustomArgumentException("Date parameter is empty or null " + dateString);
			}

			try {
				return DateTime.ParseExact(dateString, "ddMMyyyy", CultureInfo.InvariantCulture);
			}
			catch (FormatException) {
				throw new CustomArgumentException("Can't convert " + dateString + " to DateTime");
			}
		}
	}
}