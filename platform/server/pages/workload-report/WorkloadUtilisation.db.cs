using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages {
	public class WorkloadUtilisationService : ConnectionBase {
		private readonly AuthenticatedSession _authenticatedSession;

		public WorkloadUtilisationService(AuthenticatedSession currentSession) : base(currentSession.instance,
			currentSession) {
			_authenticatedSession = currentSession;
			instance = currentSession.instance;
		}

		public List<WorkloadCompetencyItem> GetCompetencyUtilisation(DateTime start, DateTime end, int item_id, List<int> competencyIds = null, int queryIndex = 0) {
			var competencySql = "";
			if (competencyIds != null) {
				var cIds = string.Join(",", competencyIds.ToArray());
				competencySql = " WHERE c.item_id IN (" + (cIds == "" ? "0" : cIds) + ") ";
			}

			var a = "";
			var aa = "";
			if (item_id > 0) {
				a = "a.process_item_id = @item_id AND ";
				aa = "aa.process_item_id = @item_id AND ";
			}
			var sql = "SELECT * FROM (SELECT " +
					  "c.item_id, c.Name, " +
					  "ISNULL((" +
					  "        SELECT SUM(dd.hours) FROM _" + instance + "_user u " +
					  "        LEFT JOIN item_data_process_selections idps ON idps.item_id = u.item_id AND idps.item_column_id = " +
					  "        (SELECT item_column_id FROM item_columns where process = 'user' AND name = 'competency') " +
					  "        LEFT JOIN _" + instance + "_competency cc ON cc.item_id = idps.selected_item_id " +
					  "        LEFT JOIN _" + instance + "_allocation aa ON aa.resource_item_id = u.item_id " +
					  "        LEFT JOIN allocation_durations dd ON dd.allocation_item_id = aa.item_id " +
					  "        WHERE u.active = 1 AND idps.selected_item_id = c.item_id AND " + aa + "YEAR(@s) = YEAR(dd.date) AND MONTH(@s) = MONTH(dd.date) " +
					  "),0) AS user_hours, (" +
					  "        SELECT ISNULL(SUM(workinghours * days),0) FROM (" +
					  "        SELECT " +
					  "    CASE WHEN ca.work_hours = -1 THEN ISNULL(pc.work_hours,0) ELSE ISNULL(ca.work_hours,0) END AS workinghours," +
					  "    ISNULL(dbo.GetWorkingDaysForDateRange(pc.calendar_id,ca.calendar_id,DATEFROMPARTS(YEAR(@s),MONTH(@s),1),DATEADD(day,-1,DATEADD(month,1,DATEFROMPARTS(YEAR(@s),MONTH(@s),1)))),0) AS days " +
					  "        FROM _" + instance + "_user uu " +
					  "        LEFT JOIN item_data_process_selections idps2 ON idps2.item_id = uu.item_id AND idps2.item_column_id = " +
					  "        (SELECT item_column_id FROM item_columns where process = 'user' AND name = 'competency') " +
					  "        LEFT JOIN _" + instance + "_competency cc ON cc.item_id = idps2.selected_item_id " +
					  "        LEFT JOIN calendars ca ON ca.item_id = uu.item_id " +
					  "        LEFT JOIN calendars pc ON ca.parent_calendar_id = pc.calendar_id " +
					  "        WHERE idps2.selected_item_id = c.item_id AND uu.active = 1) iq" +
					  ") AS capacity," +
					  "ISNULL((SELECT SUM(d.hours) FROM _" + instance + "_allocation a " +
					  "LEFT JOIN allocation_durations d ON d.allocation_item_id = a.item_id " +
					  "WHERE a.resource_item_id = c.item_id AND " + a + "MONTH(d.date) = MONTH(@s) AND YEAR(d.date) = YEAR(@s) " +
					  "),0) AS competency_hours " +
					  "FROM _" + instance + "_competency c " +
					  competencySql +
					  "GROUP BY c.item_id, c.Name) iq WHERE iq.capacity > 0 AND (user_hours > 0 OR competency_hours > 0) " +
					  "ORDER BY name";

			var current = start;
			var result = new List<WorkloadCompetencyItem>();
			if (queryIndex > 0) {
				result.Add(new WorkloadCompetencyItem(queryIndex));
			}
			while (current < end) {
				SetDBParam(SqlDbType.DateTime, "@s", current);
				SetDBParam(SqlDbType.Int, "@item_id", item_id);
				foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
					result.Add(new WorkloadCompetencyItem(row, current.Month, current.Year));
				}
				current = current.AddMonths(1);
			}

			return result;
		}

		public List<WorkloadUtilisationItem> GetUtilisation(DateTime start, DateTime end,
			int portfolioId, string filterText, string filterSelections, int queryIndex) {
			var result = new List<WorkloadUtilisationItem>();

			if (queryIndex > 0) {
				result.Add(new WorkloadUtilisationItem(queryIndex));
			}

			var userIdsInString = "";

			if (portfolioId != 0) {
				var userTable = GetProcessTableName("user");
				var filters = new ItemFilterParser(filterSelections, null, _authenticatedSession.instance, "user",
					_authenticatedSession);
				var filtering = filters.Get();
				var _portfolioSqlQueryService = new PortfolioSqlService(
					_authenticatedSession.instance,
					_authenticatedSession,
					new ItemColumns(_authenticatedSession.instance, "user", _authenticatedSession),
					"user", "user");

				var userPortfolioId = portfolioId;
				SetDBParameter("@portfolioId", portfolioId);
				var assertPortfolioIdIsValidSql =
					"SELECT COUNT(*) FROM process_portfolios WHERE process = 'user' AND instance = @instance AND process_portfolio_id = @portfolioId";
				var idExists =
					Conv.ToInt(db.ExecuteScalar(assertPortfolioIdIsValidSql, DBParameters));
				if (idExists == 0) {
					return result;
				}

				var pSqls = _portfolioSqlQueryService.GetSql(userPortfolioId, filterText);

				var searchSql = pSqls.GetSearchSql();

				var sqlForUsers = "SELECT item_id FROM " + userTable + " pf WHERE 1=1 " +
								  filtering + searchSql;
				var users = db.GetDatatableDictionary(sqlForUsers);
				if (users.Count == 0) {
					return result;
				}

				var userIds = new List<int>();
				users.ForEach(x => userIds.Add(Conv.ToInt(x["item_id"])));
				userIdsInString = string.Join(",", userIds);
			}

			if (userIdsInString != "") {
				userIdsInString = "WHERE u.active = 1 AND c.item_id IN (" + userIdsInString + ")";
			}
			else {
				userIdsInString = "WHERE u.active = 1";
			}

			var conpetencySql = "";
			if (filterSelections != null) {
				var competencyItemColumnId = Conv.ToInt(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'user' AND name = 'competency'", DBParameters));
				foreach (var filter in filterSelections.Split(",")) {
					var parts = filter.Split(";");
					if (Conv.ToInt(parts[0]) == competencyItemColumnId) {
						conpetencySql = " AND co.item_id IN (0";
						foreach (var item in parts[1].Split("-")) {
							conpetencySql += ", " + item;
						}
						conpetencySql += " ) ";
						break;
					}
				}
			}

			for (var s = start; s < end; s = s.AddMonths(1)) {
				DBParameters.Clear();
				SetDBParam(SqlDbType.Int, "month", s.Month);
				SetDBParam(SqlDbType.Int, "year", s.Year);
				var sql = "SELECT " +
						  "work_hours * workingdays AS capacity, " +
						  "work_hours * workingdays - (actual_other_hours + actual_task_hours) AS remaining_capacity, " +
						  "STUFF((SELECT ',' + CAST(co.item_id AS NVARCHAR) FROM _" + instance + "_competency co INNER JOIN item_data_process_selections idps ON idps.selected_item_id = co.item_id WHERE idps.item_id = iq.item_id " + conpetencySql + " FOR XML PATH('')), 1, 1, '') AS competency_ids, " +
						  "* FROM ( " +
						  "SELECT " +
						  "c.item_id, u.first_name + ' ' + u.last_name AS name, " +
						  "CASE WHEN c.work_hours = -1 THEN ISNULL(pc.work_hours,0) ELSE ISNULL(c.work_hours,0) END AS work_hours, " +
						  "ISNULL(dbo.GetWorkingDaysForDateRange(pc.calendar_id,c.calendar_id,DATEFROMPARTS(@year,@month,1),DATEADD(day,-1,DATEADD(month,1,DATEFROMPARTS(@year,@month,1)))),0) AS workingdays, " +
						  "(SELECT ISNULL(SUM(hours),0) AS hours FROM _" + instance + "_allocation a " +
						  "	INNER JOIN allocation_durations d ON a.item_id = d.allocation_item_id " +
						  "WHERE a.status IN (13, 20, 25, 40, 50, 53) AND resource_item_id = c.item_id AND MONTH(date) = @month AND YEAR(date) = @year) AS planned_hours, " +
						  "(SELECT ISNULL(SUM(hours),0) FROM _" + instance + "_worktime_tracking_hours " +
						  "WHERE user_item_id = c.item_id AND MONTH(date) = @month AND YEAR(date) = @year AND process='task') AS actual_task_hours, " +
						  "(SELECT ISNULL(SUM(hours),0) FROM _" + instance + "_worktime_tracking_hours " +
						  "WHERE user_item_id = c.item_id AND MONTH(date) = @month AND YEAR(date) = @year AND NOT process='task') AS actual_other_hours " +
						  "FROM calendars c " +
						  "LEFT JOIN calendars pc ON c.parent_calendar_id = pc.calendar_id " +
						  "LEFT JOIN _" + instance + "_user u ON u.item_id = c.item_id " +
						  userIdsInString + ") iq";

				foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
					result.Add(new WorkloadUtilisationItem(s.Month, s.Year, row));
				}
			}

			return result;
		}

		public class WorkloadUtilisationItem {
			public WorkloadUtilisationItem(int month, int year, DataRow row) {
				UserItemId = Conv.ToInt(row["item_id"]);
				Month = month;
				Year = year;
				Name = Conv.ToStr(row["name"]);
				WorkingDays = Conv.ToInt(row["workingdays"]);
				Capacity = Conv.ToDouble(row["capacity"]);
				RemainingCapacity = Conv.ToDouble(row["remaining_capacity"]);
				WorkHours = Conv.ToDouble(row["work_hours"]);
				PlannedHours = Conv.ToDouble(row["planned_hours"]);
				ActualTaskHours = Conv.ToDouble(row["actual_task_hours"]);
				ActualOtherHours = Conv.ToDouble(row["actual_other_hours"]);
				CompetencyIds = new List<int>();
				if (Conv.ToStr(row["competency_ids"]).Length > 0) {
					foreach (var competency_id in Conv.ToStr(row["competency_ids"]).Split(",")) {
						CompetencyIds.Add(Conv.ToInt(competency_id));
					}
				}
			}

			public WorkloadUtilisationItem(int queryIndex) {
				Month = queryIndex;
			}

			public int UserItemId { get; set; }
			public string Name { get; set; }
			public int Month { get; set; }
			public int Year { get; set; }
			public double Capacity { get; set; }
			public double RemainingCapacity { get; set; }
			public double WorkHours { get; set; }
			public int WorkingDays { get; set; }
			public double PlannedHours { get; set; }
			public double ActualTaskHours { get; set; }
			public double ActualOtherHours { get; set; }
			public List<int> CompetencyIds { get; set; }
		}

		public class WorkloadCompetencyItem {
			public int Capacity;
			public int CompetencyHours;
			public int ItemId;
			public int Month;
			public string Name;
			public int UserHours;
			public int Year;

			public WorkloadCompetencyItem(DataRow r, int month, int year) {
				Month = month;
				Year = year;
				CompetencyHours = Conv.ToInt(r["competency_hours"]);
				UserHours = Conv.ToInt(r["user_hours"]);
				Name = Conv.ToStr(r["Name"]);
				ItemId = Conv.ToInt(r["item_id"]);
				Capacity = Conv.ToInt(r["capacity"]);
			}

			public WorkloadCompetencyItem(int queryIndex) {
				Month = queryIndex;
			}
		}
	}
}