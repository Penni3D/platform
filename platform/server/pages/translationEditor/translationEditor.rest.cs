﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.translationEditor {
	[Route("v1/language/[controller]")]
	public class TranslationsVariables : RestBase {
		[HttpGet("")]
		public Dictionary<string, Dictionary<string, string>> Get() {
			var td = new TranslationData(instance, currentSession);
			return td.GetTranslationVariables();
		}
	}

	[Route("v1/language/[controller]")]
	public class TranslationsData : RestBase {
		[HttpGet("")]
		public Dictionary<string, Dictionary<string, Dictionary<string, TranslationDatas>>> Get() {
			var td = new TranslationData(instance, currentSession);
			var retval = new Dictionary<string, Dictionary<string, Dictionary<string, TranslationDatas>>>();
			foreach (var lang in new SystemList(instance, currentSession).GetListItems("LANGUAGES")) {
				retval.Add(Conv.ToStr(lang["value"]), td.GetTranslationLanguageData(Conv.ToStr(lang["value"])));
			}

			return retval;
		}

		[HttpGet("{process}")]
		public Dictionary<string, Dictionary<string, TranslationDatas>> Get(string process) {
			var td = new TranslationData(instance, currentSession);
			var retval = new Dictionary<string, Dictionary<string, TranslationDatas>>();
			foreach (var lang in new SystemList(instance, currentSession).GetListItems("LANGUAGES")) {
				retval.Add(Conv.ToStr(lang["value"]), td.GetTranslationProcessData(process, Conv.ToStr(lang["value"])));
			}

			return retval;
		}

		[HttpPost("")]
		public void Post([FromBody] List<TranslationDatas> data) {
			var td = new TranslationData(instance, currentSession);
			foreach (var items in data) {
				td.Save(items);
			}
		}
	}
}