﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages.translationEditor {
	public class TranslationDatas {
		public TranslationDatas() {
		}

		public TranslationDatas(DataRow row) {
			code = (string)row["code"];
			variable = (string)row["variable"];
			translation = (string)row["translation"];
			process = Conv.ToStr(row["process"]);
		}

		public string code { get; set; }
		public string variable { get; set; }
		public string translation { get; set; }
		public string process { get; set; }
	}

	public class TranslationData : ConnectionBase {
		private AuthenticatedSession session;

		public TranslationData(string instance, AuthenticatedSession session) : base(instance, session) {
			this.instance = instance;
			this.session = session;
		}

		public Dictionary<string, Dictionary<string, string>> GetTranslationVariables() {
			var retval = new Dictionary<string, Dictionary<string, string>>();
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			foreach (DataRow row in db.GetDatatable("SELECT variable, process, translation FROM process_translations WHERE instance = @instance AND translation != 'LIST_ITEM' AND translation != 'LIST_SYMBOL' AND translation != 'LIST_SYMBOL_FILL' AND variable NOT LIKE 'condition%' AND variable NOT LIKE 'n_condition%' AND variable NOT LIKE 'n_body%' AND variable NOT LIKE 'n_title%' ORDER BY process ASC", DBParameters).Rows) {
				row["process"] = row["process"] == DBNull.Value ? "undefined" : row["process"];

				if (retval.ContainsKey((string)row["process"])) {
					if (!retval[(string)row["process"]].ContainsKey((string)row["variable"])) {
						retval[(string)row["process"]].Add((string)row["variable"], "");
					}
				} else {
					var data = new Dictionary<string, string>();
					data.Add((string)row["variable"], "");
					retval.Add((string)row["process"], data);
				}
			}
			return retval;
		}

		public Dictionary<string, Dictionary<string, TranslationDatas>> GetTranslationLanguageData(string code) {
			var retval = new Dictionary<string, Dictionary<string, TranslationDatas>>();
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@code", code);
			foreach (DataRow row in db.GetDatatable("SELECT code, variable, translation, process FROM process_translations WHERE instance = @instance AND code = @code AND translation != 'LIST_ITEM' AND translation != 'LIST_SYMBOL' AND translation != 'LIST_SYMBOL_FILL' AND variable NOT LIKE 'condition%' AND variable NOT LIKE 'n_condition%' AND variable NOT LIKE 'n_body%' AND variable NOT LIKE 'n_title%' ORDER BY process ASC", DBParameters).Rows) {
				var r = new TranslationDatas(row);

				row["process"] = row["process"] == DBNull.Value ? "undefined" : row["process"];

				if (retval.ContainsKey((string)row["process"])) {
					retval[(string)row["process"]].Add((string)row["variable"], r);
				} else {
					var data = new Dictionary<string, TranslationDatas>();
					data.Add((string)row["variable"], r);
					retval.Add((string)row["process"], data);
				}
			}
			return retval;
		}

		public Dictionary<string, TranslationDatas> GetTranslationProcessData(string process, string code) {
			var retval = new Dictionary<string, TranslationDatas>();
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.NVarChar, "@code", code);
			foreach (DataRow row in db.GetDatatable("SELECT code, variable, translation, process FROM process_translations WHERE instance = @instance AND process = @process AND code = @code AND translation != 'LIST_ITEM' AND translation != 'LIST_SYMBOL' AND translation != 'LIST_SYMBOL_FILL' AND variable NOT LIKE 'condition%' AND variable NOT LIKE 'n_condition%' AND variable NOT LIKE 'n_body%' AND variable NOT LIKE 'n_title%' ORDER BY process ASC", DBParameters).Rows) {
				var r = new TranslationDatas(row);
				retval.Add((string)row["variable"], r);
			}
			return retval;
		}

		public void Save(TranslationDatas data) {
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@variable", data.variable);
			SetDBParam(SqlDbType.NVarChar, "@translation", data.translation);
			SetDBParam(SqlDbType.NVarChar, "@code", data.code);
			SetDBParam(SqlDbType.NVarChar, "@process", data.process);
			var dt = db.GetDatatable("SELECT variable FROM process_translations WHERE instance = @instance AND process = @process AND variable=@variable AND code = @code", DBParameters);

			if (dt.Rows.Count > 0) {
				db.ExecuteUpdateQuery("UPDATE process_translations SET translation=@translation WHERE instance=@instance AND variable=@variable AND code=@code AND process=@process", DBParameters);
			} else {
				db.ExecuteUpdateQuery("INSERT INTO process_translations (instance, variable, process, translation, code) VALUES (@instance, @variable, @process, @translation, @code)", DBParameters);
			}

		}
	}

}
