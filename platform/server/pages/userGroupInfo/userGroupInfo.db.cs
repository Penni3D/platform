﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.processes.accessControl;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.pages.userGroupInfo {
	public class UserGroupInfo : PagesDb {
		private AuthenticatedSession session;
		private bool isPage = false;
		private string parentProcess = "";
		private int parentItemId = 0;

		public UserGroupInfo(bool page, string instance, AuthenticatedSession session) : base(instance, session) {
			this.instance = instance;
			this.session = session;
			this.isPage = page;
		}

		private bool HasRight(string key) {
			if (isPage) return HasRight(key, "userGroupInfo");
			var states = new States(instance, session);
			var featureStates = states.GetEvaluatedStates(parentProcess, "userGroupInfo", parentItemId, new List<string>{"userGroupInfo." + key});
			return featureStates.States.Contains("userGroupInfo." + key);
		}

		public List<UserGroup> GetUserGroups(string parentProcess, int parentItemId) {
			var userGroups = new List<UserGroup>();
			this.parentProcess = parentProcess;
			this.parentItemId = parentItemId;
			
			if (!HasRight("read")) throw new CustomPermissionException("No Read State");
			
			SetDBParam(SqlDbType.Int, "@parentItemId", parentItemId);

			var sql = "SELECT item_id, usergroup_name " +
			             "FROM _" + instance + "_user_group ";

			if (parentProcess == "user") {
				sql += "WHERE item_id IN ( " +
				       "	SELECT item_id " +
				       "	FROM item_data_process_selections " +
				       "	WHERE item_column_id = ( " +
				       "		SELECT item_column_id " +
				       "		FROM item_columns " +
				       "		WHERE instance = @instance " +
				       "		AND process = 'user_group' " +
				       "		AND[name] = 'user_id' " +
				       "	) " +
				       "	AND selected_item_id = @parentItemId " +
				       ")";
			} else if (parentProcess == "user_group") {
				sql += "WHERE item_id = @parentItemId ";
			} else if (parentProcess != "all") {
				return userGroups;
			} else if (!HasRight("admin")) {
				sql += "WHERE item_id NOT IN ( " +
				       "	SELECT item_id " +
				       "	FROM item_data_process_selections " +
				       "	WHERE item_column_id = ( " +
				       "		SELECT item_column_id " +
				       "		FROM item_columns " +
				       "		WHERE instance = @instance " +
				       "		AND process = 'user_group' " +
				       "		AND[name] = 'group_type' " +
				       "	) " +
				       "	AND selected_item_id = ( " +
				       "		SELECT item_id " +
				       "		FROM _" + instance + "_user_group_type " +
				       "		WHERE is_admin_group = 1 " +
				       "	) " +
				       ") ";
			}

			sql += "ORDER BY order_no  ASC";

			UserGroup userGroup;
			Category category;
			Process process;
			Item item;
			DataTable processes;

			foreach (DataRow ug in db.GetDatatable(sql, DBParameters).Rows) {
				userGroup = new UserGroup(Conv.ToInt(ug["item_id"]), Conv.ToStr(ug["usergroup_name"]));

				SetDBParam(SqlDbType.Int, "@userGroupItemId", Conv.ToInt(ug["item_id"]));

				// Access
				sql = "SELECT im.instance_menu_id, im.variable " +
				      "FROM instance_menu im " +
				      "INNER JOIN instance_menu sim ON sim.parent_id = im.instance_menu_id AND im.link_type IN (0, 1 ,2) " +
				      "INNER JOIN instance_menu_rights imr ON imr.instance_menu_id = sim.instance_menu_id AND imr.item_id = @userGroupItemId " +
				      "WHERE im.link_type IN (0, 1, 2) " +
				      "GROUP BY im.instance_menu_id, im.variable, im.order_no " +
				      "ORDER BY im.order_no  ASC";

				processes = db.GetDatatable(sql, DBParameters);

				if (processes.Rows.Count > 0) {
					category = new Category(0, "USER_GROUP_INFO_CATEGORY0");

					foreach (DataRow p in processes.Rows) {
						process = new Process(Conv.ToInt(p["instance_menu_id"]), Conv.ToStr(p["variable"]));

						SetDBParam(SqlDbType.Int, "@instance_menu_id", Conv.ToInt(p["instance_menu_id"]));

						sql = "SELECT im.instance_menu_id, im.variable, " +
						      "STUFF((SELECT ', ' + ims.state FROM instance_menu_states ims WHERE ims.instance_menu_id = im.instance_menu_id AND ims.item_id = @userGroupItemId GROUP BY ims.state ORDER BY ims.state ASC FOR XML PATH('')), 1, 2, '') AS states " +
						      "FROM instance_menu im " +
						      "INNER JOIN instance_menu_rights imr ON imr.instance_menu_id = im.instance_menu_id AND imr.item_id = @userGroupItemId " +
						      "WHERE im.link_type IN (0, 1, 2) AND im.parent_id = @instance_menu_id " +
						      "GROUP BY im.instance_menu_id, im.variable, im.order_no " +
						      "ORDER BY im.order_no  ASC";

						foreach (DataRow i in db.GetDatatable(sql, DBParameters).Rows) {
							item = new Item(Conv.ToInt(i["instance_menu_id"]), Conv.ToStr(i["variable"]), "",
								Conv.ToStr(i["states"]));
							process.items.Add(item);
						}

						category.processes.Add(process);
					}

					userGroup.categories.Add(category);
				}

				// FrontPage
				sql = "SELECT im.instance_menu_id, im.variable, im.link_type " +
				      "FROM instance_menu im " +
				      "INNER JOIN instance_menu_rights imr ON imr.instance_menu_id = im.instance_menu_id AND imr.item_id = @userGroupItemId " +
				      "WHERE im.link_type IN (4, 5) " +
				      "GROUP BY im.instance_menu_id, im.variable, im.link_type, im.order_no " +
				      "ORDER BY im.order_no  ASC";

				processes = db.GetDatatable(sql, DBParameters);

				if (processes.Rows.Count > 0) {
					category = new Category(1, "USER_GROUP_INFO_CATEGORY1");

					if (Conv.ToInt(processes.Compute("MAX(link_type)", "")) == 5) {
						process = new Process(0, "In tab");

						foreach (DataRow i in processes.Select("link_type = 5")) {
							item = new Item(Conv.ToInt(i["instance_menu_id"]), Conv.ToStr(i["variable"]), "", "");
							process.items.Add(item);
						}

						category.processes.Add(process);
					}

					if (Conv.ToInt(processes.Compute("MIN(link_type)", "")) == 4) {
						process = new Process(1, "Not in tab");

						foreach (DataRow i in processes.Select("link_type = 4")) {
							item = new Item(Conv.ToInt(i["instance_menu_id"]), Conv.ToStr(i["variable"]), "", "");
							process.items.Add(item);
						}

						category.processes.Add(process);
					}

					userGroup.categories.Add(category);
				}

				// Conditions
				if (HasRight("conditions")) {
					sql = "SELECT p.process, p.variable " +
					      "FROM  processes p " +
					      "INNER JOIN conditions c ON c.instance = p.instance AND c.process = p.process AND c.[disabled] = 0 " +
					      "INNER JOIN condition_user_groups cur ON cur.condition_id = c.condition_id AND cur.item_id = @userGroupItemId " +
					      "GROUP BY p.process, p.variable";

					processes = db.GetDatatable(sql, DBParameters);
					if (processes.Rows.Count > 0) {
						category = new Category(2, "USER_GROUP_INFO_CATEGORY2");

						var index = 0;
						foreach (DataRow p in processes.Rows) {
							process = new Process(index, Conv.ToStr(p["variable"]));

							SetDBParam(SqlDbType.NVarChar, "@process", Conv.ToStr(p["process"]));

							sql = "SELECT c.condition_id, c.variable, cg.variable AS group_variable, " +
							      "STUFF((SELECT ',' + cs.state FROM condition_states cs WHERE cs.condition_id = c.condition_id GROUP BY cs.state ORDER BY cs.state ASC FOR XML PATH('')), 1, 1, '') AS states " +
							      "FROM conditions c " +
							      "INNER JOIN condition_user_groups cur ON cur.condition_id = c.condition_id AND cur.item_id = @userGroupItemId " +
							      "LEFT JOIN conditions_groups_condition cgc ON cgc.condition_id = c.condition_id " +
							      "LEFT JOIN conditions_groups cg ON cg.condition_group_id = cgc.condition_group_id " +
							      "WHERE c.[disabled] = 0 AND c.instance = @instance AND c.process = @process " +
							      "GROUP BY c.condition_id, c.variable, c.order_no, cg.variable, cg.order_no " +
							      "ORDER BY cg.order_no  ASC, c.order_no  ASC";

							foreach (DataRow i in db.GetDatatable(sql, DBParameters).Rows) {
								item = new Item(Conv.ToInt(i["condition_id"]),
									Conv.ToStr(i["group_variable"]) == ""
										? "CONDITIONS_NOT_IN_GROUP"
										: Conv.ToStr(i["group_variable"]), Conv.ToStr(i["variable"]),
									Conv.ToStr(i["states"]));
								process.items.Add(item);
							}

							category.processes.Add(process);

							index++;
						}

						userGroup.categories.Add(category);
					}
				}

				// Portfolios
				sql = "SELECT p.process, p.variable " +
				      "FROM  processes p " +
				      "INNER JOIN process_portfolios pp ON pp.instance = p.instance AND pp.process = p.process " +
				      "INNER JOIN instance_user_configurations_saved iucs ON [group] = 'portfolios' AND [public] = 4 AND iucs.identifier = CAST(pp.process_portfolio_id AS NVARCHAR) " +
				      "AND REPLACE(REPLACE(iucs.visible_for_usergroup, '[', ','), ']', ',') LIKE '%,' + CAST(@userGroupItemId AS NVARCHAR) + ',%' " +
				      "GROUP BY p.process, p.variable";

				processes = db.GetDatatable(sql, DBParameters);

				if (processes.Rows.Count > 0) {
					category = new Category(3, "USER_GROUP_INFO_CATEGORY3");

					int index = 0;
					foreach (DataRow p in processes.Rows) {
						process = new Process(index, Conv.ToStr(p["variable"]));

						SetDBParam(SqlDbType.NVarChar, "@process", Conv.ToStr(p["process"]));

						sql = "SELECT pp.process_portfolio_id, pp.variable, iucs.[name] " +
						      "FROM process_portfolios pp " +
						      "INNER JOIN instance_user_configurations_saved iucs ON [group] = 'portfolios' AND [public] = 4 AND iucs.identifier = CAST(pp.process_portfolio_id AS NVARCHAR) " +
						      "AND REPLACE(REPLACE(iucs.visible_for_usergroup, '[', ','), ']', ',') LIKE '%,' + CAST(@userGroupItemId AS NVARCHAR) + ',%' " +
						      "WHERE pp.instance = @instance AND pp.process = @process " +
						      "GROUP BY pp.process_portfolio_id, pp.variable, iucs.[name] ";

						foreach (DataRow i in db.GetDatatable(sql, DBParameters).Rows) {
							item = new Item(Conv.ToInt(i["process_portfolio_id"]), Conv.ToStr(i["variable"]),
								JObject.Parse(Conv.ToStr(i["name"])), "");
							process.items.Add(item);
						}

						category.processes.Add(process);

						index++;
					}

					userGroup.categories.Add(category);
				}

				// Dashboards
				sql = "SELECT p.process, p.variable " +
				      "FROM  processes p " +
				      "INNER JOIN process_dashboards pd ON pd.instance = p.instance AND pd.process = p.process " +
				      "INNER JOIN process_dashboard_user_groups pdug ON pdug.process_dashboard_id = pd.process_dashboard_id AND pdug.user_group_id = @userGroupItemId " +
				      "GROUP BY p.process, p.variable";

				processes = db.GetDatatable(sql, DBParameters);

				if (processes.Rows.Count > 0) {
					category = new Category(4, "USER_GROUP_INFO_CATEGORY4");

					var index = 0;
					foreach (DataRow p in processes.Rows) {
						process = new Process(index, Conv.ToStr(p["variable"]));

						SetDBParam(SqlDbType.NVarChar, "@process", Conv.ToStr(p["process"]));

						sql = "SELECT pd.process_dashboard_id, pd.variable " +
						      "FROM process_dashboards pd " +
						      "INNER JOIN process_dashboard_user_groups pdug ON pdug.process_dashboard_id = pd.process_dashboard_id AND pdug.user_group_id = @userGroupItemId " +
						      "WHERE pd.instance = @instance AND pd.process = @process " +
						      "GROUP BY pd.process_dashboard_id, pd.variable ";

						foreach (DataRow i in db.GetDatatable(sql, DBParameters).Rows) {
							item = new Item(Conv.ToInt(i["process_dashboard_id"]), Conv.ToStr(i["variable"]), "", "");
							process.items.Add(item);
						}

						category.processes.Add(process);

						index++;
					}

					userGroup.categories.Add(category);
				}

				userGroups.Add(userGroup);
			}

			return userGroups;
		}
	}

	[Serializable]
	public class UserGroup {
		public int itemId;
		public string name;
		public List<Category> categories = new List<Category>();

		public UserGroup(int itemId, string name) {
			this.itemId = itemId;
			this.name = name;
		}
	}

	[Serializable]
	public class Category {
		public int type;
		public string name;
		public List<Process> processes = new List<Process>();

		public Category(int type, string name) {
			this.type = type;
			this.name = name;
		}
	}

	[Serializable]
	public class Process {
		public int ID;
		public string name;
		public List<Item> items = new List<Item>();

		public Process(int ID, string name) {
			this.ID = ID;
			this.name = name;
		}
	}

	[Serializable]
	public class Item {
		public int ID;
		public string name;
		public object sub_name;
		public string states;

		public Item(int ID, string name, object sub_name, string states) {
			this.ID = ID;
			this.name = name;
			this.sub_name = sub_name;
			this.states = states;
		}
	}
}