﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.userGroupInfo {
	[Route("v1/userGroupInfo")]
	public class UserGroupInfoRest : PageBase {
		public UserGroupInfoRest() : base("userGroupInfo") { }

		[HttpGet("userGroups/{parentProcess}/{parentItemId}")]
		public List<UserGroup> GetUserGroups(string parentProcess, int parentItemId) {
			var ugi = new UserGroupInfo(parentProcess == "all", instance, currentSession);
			return ugi.GetUserGroups(parentProcess, parentItemId);
		}
	}
}