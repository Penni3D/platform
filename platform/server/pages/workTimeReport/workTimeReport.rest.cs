﻿using System;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.workTimeReport {
	[Route("v1/time/[controller]")]
	public class WorkTimeReport : RestBase {
		[HttpGet("{WtrProcesses}")]
		public APIResult GetProcessesWithHours() {
			var wtrData = new WorkTimeReportData(instance, "WorkTimeReport", currentSession);
			APIResult.data = wtrData.GetProcessesWithWtrHours();
			return APIResult;
		}

		//string ProcessName is the process that the worktime report is about
		//string ProcessParameter is either task name column from customer process entity selector or translation value of "worktime_tracking_basic_tasks" list.
		//ReportOptions OptionParameters is a collection of values that determine what kind of report should be returned
		[HttpPost("{ProcessName}/{ProcessParameter}")] //In reality it GET:s data only. Post made because of body
		public APIResult GetProcessWorkTimeReport(string ProcessName, string ProcessParameter,
			[FromBody] ReportOptions optionParameters) {
			optionParameters.SetProcess(instance, currentSession, ProcessName);
			var wtrData = new WorkTimeReportData(instance, "WorkTimeReport", currentSession);
			APIResult.data = wtrData.GetWorkTimeReportByProcess(ProcessName, ProcessParameter, optionParameters);
			return APIResult;
		}

		[HttpGet("{ownerItemId}/{start}/{end}")]
		public void hourAccumulationExcel(int ownerItemId, DateTime start, DateTime end) {
			var wtrData = new WorkTimeReportData(instance, "WorkTimeReport", currentSession);
			wtrData.hourAccumulationExcel(HttpContext.Response, ownerItemId, start, end);

		}
	}
}