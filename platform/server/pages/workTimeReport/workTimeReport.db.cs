﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace Keto5.x.platform.server.pages.workTimeReport {
	public class ReportOptions {
		private Processes _processes;
		private string _targetEntityName;
		private string _targetEntityProcess;
		private bool _targetEntityValidated;
		public string basicHours;
		public string grouping;
		public List<Dictionary<string, object>> reportProcesses;
		public string reportScope;
		public List<Dictionary<string, object>> reportUsers;
		public string resolution;
		public DateTime? timeframeEnd;
		public DateTime? timeframeStart;
		public bool fromActuals = false;
		public bool lmApprovedHours; //line manager approved hours

		public string targetEntityName {
			get {
				if (_targetEntityValidated) {
					return _targetEntityName;
				}

				if (_processes != null && _targetEntityProcess != null) {
					_targetEntityValidated = _processes.ColumnExist(_targetEntityProcess, _targetEntityName);
					if (_targetEntityValidated) {
						return _targetEntityName;
					}

					throw new CustomArgumentException("TargetEntity cannot be validated " + _targetEntityProcess + "." +
					                                  _targetEntityName);
				}

				if (_processes == null) {
					return "";
				}

				throw new CustomArgumentException("TargetEntity cannot be validated " + _targetEntityProcess + "." +
				                                  _targetEntityName);
			}
			set {
				_targetEntityName = value;
				_targetEntityValidated = false;
			}
		}

		public void SetProcess(string instance, AuthenticatedSession session, string p) {
			_processes = new Processes(instance, session);
			_targetEntityProcess = p;
		}
	}

	public class WorkTimeReportData : ConnectionBase {
		private AuthenticatedSession session;

		public WorkTimeReportData(string instance, string process, AuthenticatedSession session) : base(instance,
			session) {
			this.instance = instance;
			this.session = session;
		}

		private string GetWorktimeTrackingHourTableName() {
			return Conv.ToSql("_" + instance + "_WORKTIME_TRACKING_HOURS");
		}

		private string GetTaskProcessColumnsToQuery(string TaskNameColumn, string alias = null) {
			//minimum required task columns for wtr
			//Did not intentionally use getItemColumnsForSql. It has few issues for this case. Mainly with item_id, order_no and alias combinations.
			var sql = "";
			if (alias == null) {
				sql = "item_id,owner_item_id," + TaskNameColumn;
			} else {
				sql = alias + ".item_id," + alias + ".owner_item_id," + alias + "." + TaskNameColumn;
			}

			return sql;
		}

		private string GetBasicTaskColumnsToQuery(string alias = null) {
			var sql = "";
			if (alias == null) {
				sql = "item_id,list_item";
			} else {
				sql = alias + ".item_id," + alias + ".list_item";
			}

			return sql;
		}

		public List<Dictionary<string, object>> GetProcessesWithWtrHours() {
			var result = new List<Dictionary<string, object>>();
			var sql = "SELECT i.process";
			sql = sql +
			      " ,(SELECT TOP 1 ic.name FROM process_portfolio_columns ppc INNER JOIN item_columns ic ON ic.item_column_id = ppc.item_column_id WHERE process_portfolio_id IN (SELECT MAX(process_portfolio_id) FROM process_portfolios pp WHERE pp.process = i.process AND process_portfolio_type IN(1,10)) AND use_in_select_result = 1) AS process_name_column"; //This subquery gets the primary item column from entity selector as "project name"
			sql = sql +
			      " ,(SELECT TOP 1 ic2.name FROM process_portfolio_columns ppc2 INNER JOIN item_columns ic2 ON ic2.item_column_id = ppc2.item_column_id WHERE ppc2.process_portfolio_id IN (SELECT MAX(process_portfolio_id) FROM process_portfolios pp WHERE pp.process = 'task' AND process_portfolio_type IN(1,10)) AND use_in_select_result = 1) AS parameter"; //parameter = task_name_column
			sql = sql + " FROM " + GetWorktimeTrackingHourTableName() + " wth";
			sql = sql + " INNER JOIN _" + Conv.ToSql(instance) + "_task t ON t.item_id = wth.task_item_id";
			sql = sql + " INNER JOIN items i ON t.owner_item_id = i.item_id";
			sql = sql + " WHERE wth.process = 'task' AND i.instance = '" + Conv.ToSql(instance) + "'";
			sql = sql + " GROUP BY i.process";
			sql = sql + " ORDER BY i.process ASC";
			var wtrProcessesTable = db.GetDatatable(sql, DBParameters);
			foreach (DataRow rowD in wtrProcessesTable.Rows) {
				//Use Linq to convert row into a dictionary
				var r = Enumerable.Range(0, rowD.Table.Columns.Count)
					.ToDictionary(
						i => rowD.Table.Columns[i].ColumnName,
						i => rowD[rowD.Table.Columns[i]]
					);
				result.Add(r);
			}

			return result;
		}

		//string ProcessName is the process that the worktime report is about
		//string parameter is either task name column from entity selector or translation value of "worktime_tracking_basic_tasks".
		//ReportOptions OptionParameters is a collection of values that determine what kind of report should be returned
		public List<Dictionary<string, object>> GetWorkTimeReportByProcess(string ProcessName, string ProcessParameter,
			ReportOptions OptionParameters) {
			var empty = new List<Dictionary<string, object>>();
			if (OptionParameters == null || OptionParameters.resolution == null) return empty;
			switch (OptionParameters.resolution.ToLower()) {
				//Wtr Report resolution types
				case "day":
					if (OptionParameters.basicHours.ToLower() == "true") {
						return GetBasicTasksDay(ProcessParameter, OptionParameters);
					} else {
						return GetProcessWtrDay(ProcessName, ProcessParameter, OptionParameters);
					}
				case "week":
					if (OptionParameters.basicHours.ToLower() == "true") {
						return GetBasicTaskWeek(ProcessParameter, OptionParameters);
					} else {
						return GetProcessWtrWeek(ProcessName, ProcessParameter, OptionParameters);
					}
				case "month":
					if (OptionParameters.basicHours.ToLower() == "true") {
						return GetBasicTasksMonth(ProcessParameter, OptionParameters);
					} else {
						return GetProcessWtrMonth(ProcessName, ProcessParameter, OptionParameters);
					}
			}

			return empty;
		}

		private List<Dictionary<string, object>> GetProcessWtrDay(string ProcessName, string TaskNameColumn,
			ReportOptions OptionParameters) {
			DBParameters.Clear();
			var dayReport = new List<Dictionary<string, object>>();
			if (OptionParameters.targetEntityName.Length > 0) {
				SetDBParam(SqlDbType.Date, "@timeframeStartDate", OptionParameters.timeframeStart);
				SetDBParam(SqlDbType.Date, "@timeframeEndDate", OptionParameters.timeframeEnd);
				var filterProcessIds = "";
				var filterUserIds = "";
				if (OptionParameters.reportUsers.Count > 0 &&
				    OptionParameters.reportUsers[0].ContainsValue("all") != true) {
					foreach (var ud in OptionParameters.reportUsers) {
						filterUserIds = filterUserIds + ud.First().Value + ",";
					}

					filterUserIds = filterUserIds.Substring(0, filterUserIds.Length - 1);
				}

				if (OptionParameters.reportProcesses.Count > 0 &&
				    OptionParameters.reportProcesses[0].ContainsValue("all") != true) {
					foreach (var pd in OptionParameters.reportProcesses) {
						filterProcessIds = filterProcessIds + pd.First().Value + ",";
					}

					filterProcessIds = filterProcessIds.Substring(0, filterProcessIds.Length - 1);
				}

				var sql = "";
				if (OptionParameters.grouping.ToLower() == "users") {
					sql = OptionParameters.fromActuals ? SqlDaysGroupByUsersUsingActuals(filterUserIds, filterProcessIds, OptionParameters, ProcessName,
						TaskNameColumn): SqlDaysGroupByUsers(filterUserIds, filterProcessIds, OptionParameters, ProcessName,
						TaskNameColumn);
				} else {
					sql = SqlDaysGroupByTarget(filterUserIds, filterProcessIds, OptionParameters, ProcessName,
						TaskNameColumn);
				}

				var DayReportTable = db.GetDatatable(sql, DBParameters);
				foreach (DataRow rowD in DayReportTable.Rows) {
					//Convert XML days_and_hours to JSON. Conversion must be done with C# to support SQL Server 2008
					var doc = new XmlDocument();
					var xmlData = rowD["days_and_hours"].ToString();
					xmlData = "<root>" + xmlData + "</root>";
					doc.LoadXml(xmlData);
					rowD["days_and_hours"] = JsonConvert.SerializeXmlNode(doc);

					//Use Linq to convert row into a dictionary
					var r = Enumerable.Range(0, rowD.Table.Columns.Count)
						.ToDictionary(
							i => rowD.Table.Columns[i].ColumnName,
							i => rowD[rowD.Table.Columns[i]]
						);
					dayReport.Add(r);
				}
			}

			return dayReport;
		}

		private string SqlDaysGroupByTarget(string filterUserIds, string filterProcessIds,
			ReportOptions OptionParameters, string ProcessName, string TaskNameColumn) {
			var sql = "SELECT ";
			sql = sql + " (SELECT wth2.date, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      "INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t2" +
			      " ON t2.item_id = wth2.task_item_id INNER JOIN _" + instance + "_" + ProcessName +
			      " p2 ON p2.item_id = t2.owner_item_id WHERE p.item_id = p2.item_id AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND wth2.process = 'task'";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " AND t2.item_id = t.item_id";
			}

			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t2.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY wth2.date";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,t2.item_id";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " ORDER BY wth2.date ASC FOR XML RAW ('wth2'))";
			sql = sql + " AS days_and_hours";
			sql = sql + ", SUM(wth.hours) as hours_sum";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name ,t." + TaskNameColumn +
				      " AS task_name, t.owner_item_id";
			} else {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name";
			}

			sql = sql + " FROM _" + instance + "_" + ProcessName + " p";
			sql = sql + " INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t";
			sql = sql + " ON t.owner_item_id = p.item_id";
			sql = sql + " INNER JOIN " + GetWorktimeTrackingHourTableName() + " wth ON t.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND p.item_id IS NOT NULL AND wth.process = 'task'";
			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY p." + OptionParameters.targetEntityName + ", p.item_id";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,t." + TaskNameColumn + ", t.item_id, t.owner_item_id";
			}

			sql = sql + " ORDER BY p." + OptionParameters.targetEntityName + " ASC";
			return sql;
		}

		private string SqlDaysGroupByUsersUsingActuals(string filterUserIds, string filterProcessIds,
			ReportOptions OptionParameters, string ProcessName, string TaskNameColumn) {
			var filters = " 1 = 1 ";
			if (filterUserIds.Length > 0) filters = filters + " AND user_item_id IN(" + filterUserIds + ")";
			if (filterProcessIds.Length > 0) filters = filters + " AND owner_item_id IN(" + filterProcessIds + ")";

			var sql = " SELECT  " +
			          "       task_item_id AS task_item_id, " +
			          "       user_item_id AS user_item_id, " +
			          "       (SELECT first_name + ' ' + last_name FROM _" + instance +
			          "_user WHERE item_id = user_item_id) AS responsible_name, " +
			          "       (SELECT ad.date, CAST(ad.hours / iq.c AS decimal(18,4)) AS hours FROM actual_durations ad WHERE ad.task_item_id = iq.task_item_id FOR XML RAW ('wth2')) AS days_and_hours, " +
			          "       iq.owner_name AS owner_name, " +
			          "       iq.title AS task_name, " +
			          "       iq.owner_item_id AS owner_item_id, " +
			          "       (SELECT CAST(SUM(ad.hours) / iq.c AS DECIMAL(18,4)) AS hours_sum FROM actual_durations ad WHERE ad.task_item_id = iq.task_item_id) AS hours_sum, " +
			          "       0 AS deleted " +
			          " FROM ( " +
			          "       SELECT  " +
			          "               t.item_id AS task_item_id,  " +
			          "               idps1.selected_item_id AS user_item_id, " +
			          "               t.owner_item_id AS owner_item_id, " +
			          "               t." + TaskNameColumn + " AS title, " +
			          "               (SELECT COUNT(*) FROM item_data_process_selections idps2 WHERE idps2.item_column_id = 35 AND idps2.item_id = t.item_id) AS c, " +
			          "               (SELECT title FROM " + Conv.ToSql("_" + instance + "_" + ProcessName) +
			          " WHERE item_id = t.owner_item_id) AS owner_name " +
			          "       FROM _" + instance + "_task t " +
			          "       LEFT JOIN item_data_process_selections idps1 ON idps1.item_column_id = 35 AND idps1.item_id = t.item_id " +
			          "       WHERE " + filters + " AND idps1.selected_item_id IS NOT NULL " +
			          " ) iq ";

			return sql;
		}

		private string SqlDaysGroupByUsers(string filterUserIds, string filterProcessIds,
			ReportOptions OptionParameters, string ProcessName, string TaskNameColumn) {
			var tc = "";
			if (OptionParameters.reportScope == "task") {
				tc = "t.item_id AS task_item_id,";
			}

			var sql = "SELECT " + tc +
			          " u.item_id AS user_item_id, (u.first_name + ' ' + u.last_name) AS responsible_name,";
			sql = sql + " (SELECT wth2.date, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      " INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t2" +
			      " ON t2.item_id = wth2.task_item_id INNER JOIN _" +
			      instance + "_" + ProcessName + " p2 ON p2.item_id = t2.owner_item_id INNER JOIN _" + instance +
			      "_user u2 ON u2.item_id = wth2.user_item_id WHERE p.item_id = p2.item_id AND u2.item_id = u.item_id AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND wth2.process = 'task'";
			if (OptionParameters.reportScope == "task") sql = sql + " AND t2.item_id = t.item_id";
			if (OptionParameters.lmApprovedHours == true) sql = sql + " AND wth2.line_manager_status  = 2";
			if (filterUserIds.Length > 0) sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			if (filterProcessIds.Length > 0) sql = sql + " AND t2.owner_item_id IN(" + filterProcessIds + ")";

			sql = sql + " GROUP BY wth2.date";
			if (OptionParameters.reportScope == "task") sql = sql + " ,t2.item_id";

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " ORDER BY wth2.date ASC FOR XML RAW ('wth2'))";
			sql = sql + " AS days_and_hours";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name, t." + TaskNameColumn +
				      " AS task_name, t.owner_item_id";
			} else {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum, deleted";
			sql = sql + " FROM _" + instance + "_" + ProcessName + " p";
			sql = sql + " INNER JOIN (SELECT 0 AS deleted, " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM _" + instance +
			      "_task UNION ALL SELECT 1 AS deleted, " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t";
			sql = sql + " ON t.owner_item_id = p.item_id";
			sql = sql + " INNER JOIN " + GetWorktimeTrackingHourTableName() + " wth ON t.item_id = wth.task_item_id";
			sql = sql + " INNER JOIN _" + instance + "_user u ON u.item_id = wth.user_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND p.item_id IS NOT NULL AND wth.process = 'task'";
			if (OptionParameters.lmApprovedHours == true) sql = sql + " AND wth.line_manager_status  = 2";
			if (filterUserIds.Length > 0) sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			if (filterProcessIds.Length > 0) sql = sql + " AND t.owner_item_id IN(" + filterProcessIds + ")";

			sql = sql + " GROUP BY u.item_id, u.first_name,u.last_name, p." + OptionParameters.targetEntityName +
			      ", p.item_id, deleted";
			if (OptionParameters.reportScope == "task")
				sql = sql + " ,t.owner_item_id, t.item_id, t." + TaskNameColumn;
			sql = sql + " ORDER BY (u.first_name + ' ' + u.last_name), u.item_id ASC, p." +
			      OptionParameters.targetEntityName + " ASC";

			return sql;
		}

		private string ConvertDate(DateTime? d) {
			if (d is null) return "";
			var delimeter = ".";
			if (session.locale_id == "en-GB") {
				delimeter = "/";
			}

			return d.Value.Day + delimeter + d.Value.Month + delimeter + d.Value.Year;
		}

		public void hourAccumulationExcel(HttpResponse r, int ownerItemId, DateTime start, DateTime end) {
			var sql =
				"SELECT t.item_id AS task_item_id, u.item_id AS user_item_id, t.title AS task_title, h.hours, h.additional_hours, u.first_name + ' ' + u.last_name AS user_name, h.date FROM _" +
				instance + "_task t " +
				"INNER JOIN _" + instance + "_worktime_tracking_hours h ON h.task_item_id = t.item_id " +
				"INNER JOIN _" + instance + "_user u ON u.item_id = h.user_item_id " +
				"WHERE h.owner_item_id = @ownerItemId AND h.date BETWEEN @start AND @end ORDER BY h.date";


			SetDBParam(SqlDbType.Int, "@ownerItemId", ownerItemId);
			SetDBParam(SqlDbType.DateTime, "@start", start);
			SetDBParam(SqlDbType.DateTime, "@end", end);

			var data = db.GetDatatable(sql, DBParameters);

			//Get Distinct Dates
			var view = new DataView(data);
			var distinctDates = new DataTable();
			distinctDates = view.ToTable(true, "date");

			var distinctTasks = new DataTable();
			distinctTasks = view.ToTable(true, new string[2] {"user_item_id", "task_item_id"});

			var pck = new ExcelPackage();
			var ws = pck.Workbook.Worksheets.Add("Time Analysis");
			ws.View.ShowGridLines = true;

			ws.Cells[1, 1].Value = "";
			ws.Cells[1, 2].Value = "";
			ws.Cells[2, 1].Value = "Name";
			ws.Cells[2, 2].Value = "Task Title";


			var colIndex = 2;
			var rowIndex = 3;
			foreach (DataRow d in distinctDates.Rows) {
				colIndex += 1;
				ws.Cells[1, colIndex].Value = ConvertDate(Conv.ToDateTime(d["date"]));
				ws.Cells[2, colIndex].Value = "Hours";

				colIndex += 1;
				ws.Cells[1, colIndex].Value = ConvertDate(Conv.ToDateTime(d["date"]));
				ws.Cells[2, colIndex].Value = "Add. Hours";
			}

			foreach (DataRow row in distinctTasks.Rows) {
				colIndex = 0;

				var rowData = data.Select("task_item_id = " + row["task_item_id"] + " AND user_item_id = " +
				                          row["user_item_id"]);

				colIndex += 1;
				ws.Cells[rowIndex, colIndex].Value = rowData[0]["user_name"];

				colIndex += 1;
				ws.Cells[rowIndex, colIndex].Value = rowData[0]["task_title"];

				foreach (DataRow d in distinctDates.Rows) {
					var value = rowData.SingleOrDefault(rd => rd["date"].Equals(d["date"]));
					if (value != null) {
						colIndex += 1;
						ws.Cells[rowIndex, colIndex].Value = value["hours"];
						colIndex += 1;
						ws.Cells[rowIndex, colIndex].Value = value["additional_hours"];
					} else {
						colIndex += 1;
						ws.Cells[rowIndex, colIndex].Value = 0;
						colIndex += 1;
						ws.Cells[rowIndex, colIndex].Value = 0;
					}
				}


				rowIndex += 1;
			}

			r.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			r.Headers.Add("content-Disposition", "inline;filename=excel.xlsx");
			var bArr = pck.GetAsByteArray();
			r.ContentLength = bArr.Length;
			r.Body.WriteAsync(bArr, 0, bArr.Length);
		}

		private List<Dictionary<string, object>> GetProcessWtrWeek(string ProcessName, string TaskNameColumn,
			ReportOptions OptionParameters) {
			var weekReport = new List<Dictionary<string, object>>();
			if (OptionParameters.timeframeStart == null || OptionParameters.timeframeEnd == null) {
				return weekReport;
			}

			if (OptionParameters.targetEntityName.Length > 0) {
				SetDBParam(SqlDbType.Date, "@timeframeStartDate", OptionParameters.timeframeStart);
				SetDBParam(SqlDbType.Date, "@timeframeEndDate", OptionParameters.timeframeEnd);
				var filterProcessIds = "";
				var filterUserIds = "";
				if (OptionParameters.reportUsers.Count > 0 &&
				    OptionParameters.reportUsers[0].ContainsValue("all") != true) {
					foreach (var ud in OptionParameters.reportUsers) {
						filterUserIds = filterUserIds + ud.First().Value + ",";
					}

					filterUserIds = filterUserIds.Substring(0, filterUserIds.Length - 1);
				}

				if (OptionParameters.reportProcesses.Count > 0 &&
				    OptionParameters.reportProcesses[0].ContainsValue("all") != true) {
					foreach (var pd in OptionParameters.reportProcesses) {
						filterProcessIds = filterProcessIds + pd.First().Value + ",";
					}

					filterProcessIds = filterProcessIds.Substring(0, filterProcessIds.Length - 1);
				}

				var sql = "";
				if (OptionParameters.grouping.ToLower() == "users") {
					sql = SqlWeeksGroupByUsers(filterUserIds, filterProcessIds, OptionParameters, ProcessName,
						TaskNameColumn);
				} else {
					sql = SqlWeeksGroupByTarget(filterUserIds, filterProcessIds, OptionParameters, ProcessName,
						TaskNameColumn);
				}

				var WeekReportTable = db.GetDatatable(sql, DBParameters);
				foreach (DataRow rowD in WeekReportTable.Rows) {
					//Convert XML days_and_hours to JSON. Conversion must be done with C# to support SQL Server 2008
					var doc = new XmlDocument();
					var xmlData = rowD["weeks_and_hours"].ToString();
					xmlData = "<root>" + xmlData + "</root>";
					doc.LoadXml(xmlData);
					rowD["weeks_and_hours"] = JsonConvert.SerializeXmlNode(doc);

					//Use Linq to convert row into a dictionary
					var r = Enumerable.Range(0, rowD.Table.Columns.Count)
						.ToDictionary(
							i => rowD.Table.Columns[i].ColumnName,
							i => rowD[rowD.Table.Columns[i]]
						);
					weekReport.Add(r);
				}
			}

			return weekReport;
		}

		private string SqlWeeksGroupByTarget(string filterUserIds, string filterProcessIds,
			ReportOptions OptionParameters, string ProcessName, string TaskNameColumn) {
			var sql = "SELECT";
			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) AS year,DATEPART(WEEK,wth2.date) AS week, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      "INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t2" +
			      " ON t2.item_id = wth2.task_item_id INNER JOIN _" + instance + "_" + ProcessName +
			      " p2 ON p2.item_id = t2.owner_item_id WHERE p.item_id = p2.item_id AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND wth2.process = 'task'";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " AND t2.item_id = t.item_id";
			}

			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t2.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date)";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,t2.item_id";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " ORDER BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date) ASC FOR XML RAW ('wth2'))";
			sql = sql + " AS weeks_and_hours";
			sql = sql + ", SUM(wth.hours) as hours_sum";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name, t." + TaskNameColumn +
				      " AS task_name, t.owner_item_id";
			} else {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name";
			}

			sql = sql + " FROM _" + instance + "_" + ProcessName + " p";
			sql = sql + " INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t";
			sql = sql + " ON t.owner_item_id = p.item_id";
			sql = sql + " INNER JOIN " + GetWorktimeTrackingHourTableName() + " wth ON t.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND p.item_id IS NOT NULL AND wth.process = 'task'";
			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY p." + OptionParameters.targetEntityName + ", p.item_id";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,t.owner_item_id,t." + TaskNameColumn + ", t.item_id";
			}

			sql = sql + " ORDER BY p." + OptionParameters.targetEntityName + " ASC";
			return sql;
		}

		private string SqlWeeksGroupByUsers(string filterUserIds, string filterProcessIds,
			ReportOptions OptionParameters, string ProcessName, string TaskNameColumn) {
			var sql = "SELECT u.item_id AS user_item_id, (u.first_name + ' ' + u.last_name) AS responsible_name, ";
			if (OptionParameters.reportScope == "task") {
				sql = sql + "t.item_id AS task_item_id,";
			}

			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) AS year,DATEPART(WEEK,wth2.date) AS week, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      "INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t2" +
			      " ON t2.item_id = wth2.task_item_id INNER JOIN _" +
			      instance + "_" + ProcessName + " p2 ON p2.item_id = t2.owner_item_id" +
			      " INNER JOIN _" + instance + "_user u2 ON u2.item_id = wth2.user_item_id " +
			      " WHERE p.item_id = p2.item_id AND u2.item_id = u.item_id AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND wth2.process = 'task'";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " AND t2.item_id = t.item_id";
			}

			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t2.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date)";
			if (OptionParameters.reportScope == "task") {
				sql = sql + ",t2.owner_item_id ,t2.item_id";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " ORDER BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date) ASC FOR XML RAW ('wth2'))";
			sql = sql + " AS weeks_and_hours";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name ,t." + TaskNameColumn +
				      " AS task_name, t.owner_item_id";
			} else {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM _" + instance + "_" + ProcessName + " p";
			sql = sql + " INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t";
			sql = sql + " ON t.owner_item_id = p.item_id";
			sql = sql + " INNER JOIN " + GetWorktimeTrackingHourTableName() + " wth ON t.item_id = wth.task_item_id";
			sql = sql + " INNER JOIN _" + instance + "_user u ON u.item_id = wth.user_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND p.item_id IS NOT NULL AND wth.process = 'task'";
			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY u.item_id, u.first_name,u.last_name, p." + OptionParameters.targetEntityName +
			      ", p.item_id";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " , t.owner_item_id, t.item_id, t." + TaskNameColumn;
			}

			sql = sql + " ORDER BY (u.first_name + ' ' + u.last_name), u.item_id ASC, p." +
			      OptionParameters.targetEntityName + " ASC";
			return sql;
		}

		private List<Dictionary<string, object>> GetProcessWtrMonth(string ProcessName, string TaskNameColumn,
			ReportOptions OptionParameters) {
			DBParameters.Clear();
			var monthReport = new List<Dictionary<string, object>>();
			if (OptionParameters.targetEntityName.Length > 0) {
				SetDBParam(SqlDbType.Date, "@timeframeStartDate", OptionParameters.timeframeStart);
				SetDBParam(SqlDbType.Date, "@timeframeEndDate", OptionParameters.timeframeEnd);
				var filterProcessIds = "";
				var filterUserIds = "";
				if (OptionParameters.reportUsers.Count > 0 &&
				    OptionParameters.reportUsers[0].ContainsValue("all") != true) {
					foreach (var ud in OptionParameters.reportUsers) {
						filterUserIds = filterUserIds + ud.First().Value + ",";
					}

					filterUserIds = filterUserIds.Substring(0, filterUserIds.Length - 1);
				}

				if (OptionParameters.reportProcesses.Count > 0 &&
				    OptionParameters.reportProcesses[0].ContainsValue("all") != true) {
					foreach (var pd in OptionParameters.reportProcesses) {
						filterProcessIds = filterProcessIds + pd.First().Value + ",";
					}

					filterProcessIds = filterProcessIds.Substring(0, filterProcessIds.Length - 1);
				}

				//int tfDays = (OptionParameters.timeframeEnd - OptionParameters.timeframeStart).Days;
				var sql = "";
				if (OptionParameters.grouping.ToLower() == "users") {
					sql = SqlMonthsGroupByUsers(filterUserIds, filterProcessIds, OptionParameters, ProcessName,
						TaskNameColumn);
				} else {
					sql = SqlMonthsGroupByTarget(filterUserIds, filterProcessIds, OptionParameters, ProcessName,
						TaskNameColumn);
				}

				var MonthReportTable = db.GetDatatable(sql, DBParameters);
				foreach (DataRow rowD in MonthReportTable.Rows) {
					//Convert XML days_and_hours to JSON. Conversion must be done with C# to support SQL Server 2008
					var doc = new XmlDocument();
					var xmlData = rowD["months_and_hours"].ToString();
					xmlData = "<root>" + xmlData + "</root>";
					doc.LoadXml(xmlData);
					rowD["months_and_hours"] = JsonConvert.SerializeXmlNode(doc);

					//Use Linq to convert row into a dictionary
					var r = Enumerable.Range(0, rowD.Table.Columns.Count)
						.ToDictionary(
							i => rowD.Table.Columns[i].ColumnName,
							i => rowD[rowD.Table.Columns[i]]
						);
					monthReport.Add(r);
				}
			}

			return monthReport;
		}

		private string SqlMonthsGroupByTarget(string filterUserIds, string filterProcessIds,
			ReportOptions OptionParameters, string ProcessName, string TaskNameColumn) {
			var sql = "SELECT";
			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) AS year,DATEPART(MONTH,wth2.date) AS month, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      "INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t2" +
			      " ON t2.item_id = wth2.task_item_id INNER JOIN _" + instance + "_" + ProcessName +
			      " p2 ON p2.item_id = t2.owner_item_id WHERE p.item_id = p2.item_id AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND wth2.process = 'task'";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " AND t2.item_id = t.item_id";
			}

			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t2.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY DATEPART(YEAR,wth2.date),DATEPART(MONTH,wth2.date)";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,t2.owner_item_id ,t2.item_id";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " ORDER BY DATEPART(YEAR,wth2.date),DATEPART(MONTH,wth2.date) ASC FOR XML RAW ('wth2'))";
			sql = sql + " AS months_and_hours";
			sql = sql + ", SUM(wth.hours) as hours_sum";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name, t." + TaskNameColumn +
				      " AS task_name, t.owner_item_id";
			} else {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name";
			}

			sql = sql + " FROM _" + instance + "_" + ProcessName + " p";
			sql = sql + " INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t";
			sql = sql + " ON t.owner_item_id = p.item_id";
			sql = sql + " INNER JOIN " + GetWorktimeTrackingHourTableName() + " wth ON t.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND p.item_id IS NOT NULL AND wth.process = 'task'";
			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY p." + OptionParameters.targetEntityName + ", p.item_id";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,t.owner_item_id,t." + TaskNameColumn + ", t.item_id";
			}

			sql = sql + " ORDER BY p." + OptionParameters.targetEntityName + " ASC";
			return sql;
		}

		private string SqlMonthsGroupByUsers(string filterUserIds, string filterProcessIds,
			ReportOptions OptionParameters, string ProcessName, string TaskNameColumn) {
			var sql = "SELECT u.item_id AS user_item_id, (u.first_name + ' ' + u.last_name) AS responsible_name,";
			if (OptionParameters.reportScope == "task") {
				sql = sql + "t.item_id AS task_item_id,";
			}

			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) AS year,DATEPART(MONTH,wth2.date) AS month, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      " INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t2" +
			      " ON t2.item_id = wth2.task_item_id INNER JOIN _" +
			      instance + "_" + ProcessName + " p2 ON p2.item_id = t2.owner_item_id INNER JOIN _" + instance +
			      "_user u2 ON u2.item_id = wth2.user_item_id" +
			      " WHERE p.item_id = p2.item_id AND u2.item_id = u.item_id AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND wth2.process = 'task'";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " AND t2.item_id = t.item_id";
			}

			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t2.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY DATEPART(YEAR,wth2.date),DATEPART(MONTH,wth2.date)";
			if (OptionParameters.reportScope == "task") {
				sql = sql + ", t2.owner_item_id,t2.item_id";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " ORDER BY DATEPART(YEAR,wth2.date),DATEPART(MONTH,wth2.date) ASC FOR XML RAW ('wth2'))";
			sql = sql + " AS months_and_hours";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name, t." + TaskNameColumn +
				      " AS task_name, t.owner_item_id";
			} else {
				sql = sql + " ,p." + OptionParameters.targetEntityName + " AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM _" + instance + "_" + ProcessName + " p";
			sql = sql + " INNER JOIN (SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) + " FROM _" + instance +
			      "_task UNION ALL SELECT " + GetTaskProcessColumnsToQuery(TaskNameColumn) +
			      " FROM archive__" + instance + "_task WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_task) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" + instance +
			      "_task GROUP BY item_id )) t";
			sql = sql + " ON t.owner_item_id = p.item_id";
			sql = sql + " INNER JOIN " + GetWorktimeTrackingHourTableName() + " wth ON t.item_id = wth.task_item_id";
			sql = sql + " INNER JOIN _" + instance + "_user u ON u.item_id = wth.user_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND p.item_id IS NOT NULL AND wth.process = 'task'";
			if (OptionParameters.lmApprovedHours == true) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND t.owner_item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY u.item_id, u.first_name,u.last_name, p." + OptionParameters.targetEntityName +
			      ", p.item_id";
			if (OptionParameters.reportScope == "task") {
				sql = sql + " ,t.owner_item_id, t.item_id, t." + TaskNameColumn;
			}

			sql = sql + " ORDER BY (u.first_name + ' ' + u.last_name), u.item_id ASC, p." +
			      OptionParameters.targetEntityName + " ASC";
			return sql;
		}

		private List<Dictionary<string, object>> GetBasicTasksDay(string basicTaskTranslation,
			ReportOptions OptionParameters) {
			DBParameters.Clear();
			var dayReport = new List<Dictionary<string, object>>();
			SetDBParam(SqlDbType.Date, "@timeframeStartDate", OptionParameters.timeframeStart);
			SetDBParam(SqlDbType.Date, "@timeframeEndDate", OptionParameters.timeframeEnd);
			SetDBParam(SqlDbType.NVarChar, "@basicTaskTranslation", basicTaskTranslation);
			var filterProcessIds = "";
			var filterUserIds = "";
			var scopeIsTask = false;
			var lmApprovedHours = false;
			if (OptionParameters.reportScope == "task") {
				scopeIsTask = true;
			}

			if (OptionParameters.lmApprovedHours == true) {
				lmApprovedHours = true;
			}

			if (OptionParameters.reportUsers.Count > 0 &&
			    OptionParameters.reportUsers[0].ContainsValue("all") != true) {
				foreach (var ud in OptionParameters.reportUsers) {
					filterUserIds = filterUserIds + ud.First().Value + ",";
				}

				filterUserIds = filterUserIds.Substring(0, filterUserIds.Length - 1);
			}

			if (OptionParameters.reportProcesses.Count > 0 &&
			    OptionParameters.reportProcesses[0].ContainsValue("all") != true) {
				foreach (var pd in OptionParameters.reportProcesses) {
					filterProcessIds = filterProcessIds + pd.First().Value + ",";
				}

				filterProcessIds = filterProcessIds.Substring(0, filterProcessIds.Length - 1);
			}

			var sql = "";
			if (OptionParameters.grouping.ToLower() == "users") {
				sql = SqlBtDaysGroupByUsers(filterUserIds, filterProcessIds, scopeIsTask, lmApprovedHours);
			} else {
				sql = SqlBtDaysGroupByTarget(filterUserIds, filterProcessIds, scopeIsTask, lmApprovedHours);
			}

			if (sql.Length > 0) {
				var DayReportTable = db.GetDatatable(sql, DBParameters);
				if (DayReportTable.Rows.Count == 1) {
					//Additional data validity check.
					//If table has only owner_name data and 1 row then clear it because basic task owner_name is just a posted value
					var row = DayReportTable.Rows[0];
					if (row["days_and_hours"] == DBNull.Value && row["hours_sum"] == DBNull.Value) {
						DayReportTable.Clear();
					}
				}

				foreach (DataRow rowD in DayReportTable.Rows) {
					//Convert XML days_and_hours to JSON. Conversion must be done with C# to support SQL Server 2008
					var doc = new XmlDocument();
					var xmlData = rowD["days_and_hours"].ToString();
					xmlData = "<root>" + xmlData + "</root>";
					doc.LoadXml(xmlData);
					rowD["days_and_hours"] = JsonConvert.SerializeXmlNode(doc);

					//Use Linq to convert row into a dictionary
					var r = Enumerable.Range(0, rowD.Table.Columns.Count)
						.ToDictionary(
							i => rowD.Table.Columns[i].ColumnName,
							i => rowD[rowD.Table.Columns[i]]
						);
					dayReport.Add(r);
				}
			}

			return dayReport;
		}

		private string SqlBtDaysGroupByTarget(string filterUserIds, string filterProcessIds, bool scopeIsTask,
			bool lmApprovedHours) {
			var sql = "SELECT ";
			sql = sql + " (SELECT wth2.date, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() +
			      " wth2" +
			      " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt2" +
			      " ON bt2.item_id = wth2.task_item_id " +
			      " WHERE wth2.process = 'worktime_tracking_basic_tasks' AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate)";
			if (scopeIsTask) {
				sql = sql + " AND bt2.item_id = bt.item_id";
			}

			if (lmApprovedHours) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt2.item_id IN(" + filterProcessIds + ")";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " GROUP BY wth2.date ORDER BY wth2.date ASC FOR XML RAW('wth2')) AS days_and_hours";
			if (scopeIsTask) {
				sql =
					sql +
					" ,@basicTaskTranslation AS owner_name, bt.list_item AS task_name"; //, bt.item_id AS owner_item_id
			} else {
				sql = sql + " ,@basicTaskTranslation AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM " + GetWorktimeTrackingHourTableName() + " wth";
			sql = sql + " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt" +
			      " ON bt.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND wth.process = 'worktime_tracking_basic_tasks'";
			if (lmApprovedHours) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt.item_id IN(" + filterProcessIds + ")";
			}

			if (scopeIsTask) {
				sql = sql + " GROUP BY bt.list_item, bt.item_id ORDER BY bt.list_item ASC";
			}

			return sql;
		}

		private string SqlBtDaysGroupByUsers(string filterUserIds, string filterProcessIds, bool scopeIsTask,
			bool lmApprovedHours) {
			var sql = "SELECT u.item_id AS user_item_id, (u.first_name + ' ' + u.last_name) AS responsible_name,";
			sql = sql + " (SELECT wth2.date, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() +
			      " wth2 " +
			      " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt2" +
			      " ON bt2.item_id = wth2.task_item_id " +
			      " INNER JOIN _" + instance +
			      "_user u2 ON u2.item_id = wth2.user_item_id WHERE wth2.process = 'worktime_tracking_basic_tasks' AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND u2.item_id = u.item_id ";
			if (scopeIsTask) {
				sql = sql + " AND bt2.item_id = bt.item_id";
			}

			if (lmApprovedHours) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt2.item_id IN(" + filterProcessIds + ")";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql + " GROUP BY wth2.date ORDER BY wth2.date ASC FOR XML RAW('wth2')) AS days_and_hours";
			if (scopeIsTask) {
				sql =
					sql +
					" ,@basicTaskTranslation AS owner_name, bt.list_item AS task_name"; //, bt.item_id AS owner_item_id
			} else {
				sql = sql + " ,@basicTaskTranslation AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM " + GetWorktimeTrackingHourTableName() + " wth";
			sql = sql + " INNER JOIN _" + instance + "_user u ON u.item_id = wth.user_item_id";
			sql = sql + " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt" +
			      " ON bt.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND wth.process = 'worktime_tracking_basic_tasks'";
			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt.item_id IN(" + filterProcessIds + ")";
			}

			if (lmApprovedHours) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			sql = sql + " GROUP BY u.item_id, u.first_name,u.last_name";
			if (scopeIsTask) {
				sql = sql +
				      ", bt.list_item, bt.item_id ORDER BY (u.first_name + ' ' + u.last_name), u.item_id ASC, bt.list_item ASC";
			} else {
				sql = sql + " ORDER BY (u.first_name + ' ' + u.last_name) ASC, u.item_id ASC";
			}

			return sql;
		}

		private List<Dictionary<string, object>> GetBasicTaskWeek(string basicTaskTranslation,
			ReportOptions OptionParameters) {
			DBParameters.Clear();
			var weekReport = new List<Dictionary<string, object>>();
			SetDBParam(SqlDbType.Date, "@timeframeStartDate", OptionParameters.timeframeStart);
			SetDBParam(SqlDbType.Date, "@timeframeEndDate", OptionParameters.timeframeEnd);
			SetDBParam(SqlDbType.NVarChar, "@basicTaskTranslation", basicTaskTranslation);
			var filterProcessIds = "";
			var filterUserIds = "";
			var scopeIsTask = false;
			var lmApprovedHours = false;
			if (OptionParameters.reportScope == "task") {
				scopeIsTask = true;
			}

			if (OptionParameters.lmApprovedHours == true) {
				lmApprovedHours = true;
			}

			if (OptionParameters.reportUsers.Count > 0 &&
			    OptionParameters.reportUsers[0].ContainsValue("all") != true) {
				foreach (var ud in OptionParameters.reportUsers) {
					filterUserIds = filterUserIds + ud.First().Value + ",";
				}

				filterUserIds = filterUserIds.Substring(0, filterUserIds.Length - 1);
			}

			if (OptionParameters.reportProcesses.Count > 0 &&
			    OptionParameters.reportProcesses[0].ContainsValue("all") != true) {
				foreach (var pd in OptionParameters.reportProcesses) {
					filterProcessIds = filterProcessIds + pd.First().Value + ",";
				}

				filterProcessIds = filterProcessIds.Substring(0, filterProcessIds.Length - 1);
			}

			var sql = "";
			if (OptionParameters.grouping.ToLower() == "users") {
				sql = SqlBtWeeksGroupByUsers(filterUserIds, filterProcessIds, scopeIsTask, lmApprovedHours);
			} else {
				sql = SqlBtWeeksGroupByTarget(filterUserIds, filterProcessIds, scopeIsTask, lmApprovedHours);
			}

			if (sql.Length > 0) {
				var WeekReportTable = db.GetDatatable(sql, DBParameters);
				if (WeekReportTable.Rows.Count == 1) {
					//Additional data validity check.
					//If table has only owner_name data and 1 row then clear it because basic task owner_name is just a posted value
					var row = WeekReportTable.Rows[0];
					if (row["weeks_and_hours"] == DBNull.Value && row["hours_sum"] == DBNull.Value) {
						WeekReportTable.Clear();
					}
				}

				foreach (DataRow rowD in WeekReportTable.Rows) {
					//Convert XML days_and_hours to JSON. Conversion must be done with C# to support SQL Server 2008
					var doc = new XmlDocument();
					var xmlData = rowD["weeks_and_hours"].ToString();
					xmlData = "<root>" + xmlData + "</root>";
					doc.LoadXml(xmlData);
					rowD["weeks_and_hours"] = JsonConvert.SerializeXmlNode(doc);

					//Use Linq to convert row into a dictionary
					var r = Enumerable.Range(0, rowD.Table.Columns.Count)
						.ToDictionary(
							i => rowD.Table.Columns[i].ColumnName,
							i => rowD[rowD.Table.Columns[i]]
						);
					weekReport.Add(r);
				}
			}

			return weekReport;
		}

		private string SqlBtWeeksGroupByTarget(string filterUserIds, string filterProcessIds, bool scopeIsTask,
			bool lmApprovedHours) {
			var sql = "SELECT ";
			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) as year, DATEPART(WEEK,wth2.date) as week, ISNULL(SUM(wth2.hours),0) as hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt2" +
			      " ON bt2.item_id = wth2.task_item_id " +
			      " WHERE wth2.process = 'worktime_tracking_basic_tasks' AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) ";
			if (scopeIsTask) {
				sql = sql + " AND bt2.item_id = bt.item_id";
			}

			if (lmApprovedHours) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt2.item_id IN(" + filterProcessIds + ")";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql +
			      " GROUP BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date) ORDER BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date) ASC FOR XML RAW('wth2')) AS weeks_and_hours";
			if (scopeIsTask) {
				sql =
					sql +
					" ,@basicTaskTranslation AS owner_name, bt.list_item AS task_name"; //, bt.item_id AS owner_item_id
			} else {
				sql = sql + " ,@basicTaskTranslation AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM " + GetWorktimeTrackingHourTableName() + " wth";
			sql = sql + " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt" +
			      " ON bt.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND wth.process = 'worktime_tracking_basic_tasks'";
			if (lmApprovedHours) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt.item_id IN(" + filterProcessIds + ")";
			}

			if (scopeIsTask) {
				sql = sql + " GROUP BY bt.list_item, bt.item_id ORDER BY bt.list_item ASC";
			}

			return sql;
		}

		private string SqlBtWeeksGroupByUsers(string filterUserIds, string filterProcessIds, bool scopeIsTask,
			bool lmApprovedHours) {
			var sql = "SELECT u.item_id AS user_item_id, (u.first_name + ' ' + u.last_name) AS responsible_name,";
			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) AS year, DATEPART(WEEK,wth2.date) AS week, ISNULL(SUM(wth2.hours),0) AS hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt2" +
			      " ON bt2.item_id = wth2.task_item_id " +
			      " INNER JOIN _" + instance +
			      "_user u2 ON u2.item_id = wth2.user_item_id WHERE wth2.process = 'worktime_tracking_basic_tasks' AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) AND u2.item_id = u.item_id ";
			if (scopeIsTask) {
				sql = sql + " AND bt2.item_id = bt.item_id";
			}

			if (lmApprovedHours) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt2.item_id IN(" + filterProcessIds + ")";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql +
			      " GROUP BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date) ORDER BY DATEPART(YEAR,wth2.date),DATEPART(WEEK,wth2.date) ASC FOR XML RAW('wth2')) AS weeks_and_hours";
			if (scopeIsTask) {
				sql =
					sql +
					" ,@basicTaskTranslation AS owner_name, bt.list_item AS task_name"; //, bt.item_id AS owner_item_id
			} else {
				sql = sql + " ,@basicTaskTranslation AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM " + GetWorktimeTrackingHourTableName() + " wth";
			sql = sql + " INNER JOIN _" + instance + "_user u ON u.item_id = wth.user_item_id";
			sql = sql + " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt" +
			      " ON bt.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND wth.process = 'worktime_tracking_basic_tasks'";
			if (lmApprovedHours) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt.item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY u.item_id, u.first_name,u.last_name";
			if (scopeIsTask) {
				sql = sql +
				      ", bt.list_item, bt.item_id ORDER BY (u.first_name + ' ' + u.last_name), u.item_id ASC, bt.list_item ASC";
			} else {
				sql = sql + " ORDER BY (u.first_name + ' ' + u.last_name) ASC, u.item_id ASC";
			}

			return sql;
		}

		private List<Dictionary<string, object>> GetBasicTasksMonth(string basicTaskTranslation,
			ReportOptions OptionParameters) {
			DBParameters.Clear();
			var monthReport = new List<Dictionary<string, object>>();
			SetDBParam(SqlDbType.Date, "@timeframeStartDate", OptionParameters.timeframeStart);
			SetDBParam(SqlDbType.Date, "@timeframeEndDate", OptionParameters.timeframeEnd);
			SetDBParam(SqlDbType.NVarChar, "@basicTaskTranslation", basicTaskTranslation);
			var filterProcessIds = "";
			var filterUserIds = "";
			var scopeIsTask = false;
			var lmApprovedHours = false;
			if (OptionParameters.reportScope == "task") {
				scopeIsTask = true;
			}

			if (OptionParameters.lmApprovedHours == true) {
				lmApprovedHours = true;
			}

			if (OptionParameters.reportUsers.Count > 0 &&
			    OptionParameters.reportUsers[0].ContainsValue("all") != true) {
				foreach (var ud in OptionParameters.reportUsers) {
					filterUserIds = filterUserIds + ud.First().Value + ",";
				}

				filterUserIds = filterUserIds.Substring(0, filterUserIds.Length - 1);
			}

			if (OptionParameters.reportProcesses.Count > 0 &&
			    OptionParameters.reportProcesses[0].ContainsValue("all") != true) {
				foreach (var pd in OptionParameters.reportProcesses) {
					filterProcessIds = filterProcessIds + pd.First().Value + ",";
				}

				filterProcessIds = filterProcessIds.Substring(0, filterProcessIds.Length - 1);
			}

			var sql = "";
			if (OptionParameters.grouping.ToLower() == "users") {
				sql = SqlBtMonthsGroupByUsers(filterUserIds, filterProcessIds, scopeIsTask, lmApprovedHours);
			} else {
				sql = SqlBtMonthsGroupByTarget(filterUserIds, filterProcessIds, scopeIsTask, lmApprovedHours);
			}

			if (sql.Length > 0) {
				var MonthReportTable = db.GetDatatable(sql, DBParameters);
				if (MonthReportTable.Rows.Count == 1) {
					//Additional data validity check.
					//If table has only owner_name data and 1 row then clear it because basic task owner_name is just a posted value
					var row = MonthReportTable.Rows[0];
					if (row["months_and_hours"] == DBNull.Value && row["hours_sum"] == DBNull.Value) {
						MonthReportTable.Clear();
					}
				}

				foreach (DataRow rowD in MonthReportTable.Rows) {
					//Convert XML days_and_hours to JSON. Conversion must be done with C# to support SQL Server 2008
					var doc = new XmlDocument();
					var xmlData = rowD["months_and_hours"].ToString();
					xmlData = "<root>" + xmlData + "</root>";
					doc.LoadXml(xmlData);
					rowD["months_and_hours"] = JsonConvert.SerializeXmlNode(doc);

					//Use Linq to convert row into a dictionary
					var r = Enumerable.Range(0, rowD.Table.Columns.Count)
						.ToDictionary(
							i => rowD.Table.Columns[i].ColumnName,
							i => rowD[rowD.Table.Columns[i]]
						);
					monthReport.Add(r);
				}
			}

			return monthReport;
		}

		private string SqlBtMonthsGroupByTarget(string filterUserIds, string filterProcessIds, bool scopeIsTask,
			bool lmApprovedHours) {
			var sql = "SELECT ";
			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) as year, DATEPART(month,wth2.date) as month, ISNULL(SUM(wth2.hours),0) as hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      " INNER JOIN(SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt2" +
			      " ON bt2.item_id = wth2.task_item_id " +
			      " WHERE wth2.process = 'worktime_tracking_basic_tasks' AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) ";
			if (scopeIsTask) {
				sql = sql + " AND bt2.item_id = bt.item_id";
			}

			if (lmApprovedHours) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt2.item_id IN(" + filterProcessIds + ")";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql +
			      " GROUP BY DATEPART(YEAR,wth2.date), DATEPART(MONTH,wth2.date) ORDER BY DATEPART(YEAR,wth2.date), DATEPART(MONTH,wth2.date) ASC FOR XML RAW('wth2')) AS months_and_hours";
			if (scopeIsTask) {
				sql =
					sql +
					" ,@basicTaskTranslation AS owner_name, bt.list_item AS task_name"; //, bt.item_id AS owner_item_id
			} else {
				sql = sql + " ,@basicTaskTranslation AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM " + GetWorktimeTrackingHourTableName() + " wth";
			sql = sql + " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt" +
			      " ON bt.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND wth.process = 'worktime_tracking_basic_tasks'";
			if (lmApprovedHours) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt.item_id IN(" + filterProcessIds + ")";
			}

			if (scopeIsTask) {
				sql = sql + " GROUP BY bt.list_item, bt.item_id ORDER BY bt.list_item ASC";
			}

			return sql;
		}

		private string SqlBtMonthsGroupByUsers(string filterUserIds, string filterProcessIds, bool scopeIsTask,
			bool lmApprovedHours) {
			var sql = "SELECT u.item_id AS user_item_id, (u.first_name + ' ' + u.last_name) AS responsible_name,";
			sql = sql +
			      " (SELECT DATEPART(YEAR,wth2.date) as year, DATEPART(month,wth2.date) as month, ISNULL(SUM(wth2.hours),0) as hours FROM " +
			      GetWorktimeTrackingHourTableName() + " wth2 " +
			      " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt2" +
			      " ON bt2.item_id = wth2.task_item_id " +
			      " INNER JOIN _" + instance +
			      "_user u2 ON u2.item_id = wth2.user_item_id WHERE wth2.process = 'worktime_tracking_basic_tasks' AND wth2.user_item_id = u.item_id AND wth2.date > DATEADD(DAY, -1, @timeframeStartDate) AND wth2.date < DATEADD(DAY, 1,@timeframeEndDate) ";
			if (scopeIsTask) {
				sql = sql + " AND bt2.item_id = bt.item_id";
			}

			if (lmApprovedHours) {
				sql = sql + " AND wth2.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth2.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt2.item_id IN(" + filterProcessIds + ")";
			}

			//XML AUTO in next row replaced with RAW because xml node attribute naming is out of control with UNION archive SQL logic -ehe
			sql = sql +
			      " GROUP BY DATEPART(YEAR,wth2.date), DATEPART(MONTH,wth2.date) ORDER BY DATEPART(YEAR,wth2.date), DATEPART(MONTH,wth2.date) ASC FOR XML RAW('wth2')) AS months_and_hours";
			if (scopeIsTask) {
				sql =
					sql +
					" ,@basicTaskTranslation AS owner_name, bt.list_item AS task_name"; //, bt.item_id AS owner_item_id
			} else {
				sql = sql + " ,@basicTaskTranslation AS owner_name";
			}

			sql = sql + ", SUM(wth.hours) as hours_sum";
			sql = sql + " FROM " + GetWorktimeTrackingHourTableName() + " wth";
			sql = sql + " INNER JOIN _" + instance + "_user u ON u.item_id = wth.user_item_id";
			sql = sql + " INNER JOIN (SELECT " + GetBasicTaskColumnsToQuery() + " FROM _" + instance +
			      "_worktime_tracking_basic_tasks UNION ALL SELECT " + GetBasicTaskColumnsToQuery() +
			      " FROM archive__" + instance + "_worktime_tracking_basic_tasks" +
			      " WHERE item_id NOT IN(SELECT item_id FROM _" + instance +
			      "_worktime_tracking_basic_tasks) AND archive_id IN (SELECT MAX(archive_id) FROM archive__" +
			      instance + "_worktime_tracking_basic_tasks GROUP BY item_id )) bt" +
			      " ON bt.item_id = wth.task_item_id";
			sql = sql +
			      " WHERE wth.date > DATEADD(DAY, -1, @timeframeStartDate) AND date < DATEADD(DAY, 1,@timeframeEndDate)";
			sql = sql + " AND wth.process = 'worktime_tracking_basic_tasks'";
			if (lmApprovedHours) {
				sql = sql + " AND wth.line_manager_status = 2";
			}

			if (filterUserIds.Length > 0) {
				sql = sql + " AND wth.user_item_id IN(" + filterUserIds + ")";
			}

			if (filterProcessIds.Length > 0) {
				sql = sql + " AND bt.item_id IN(" + filterProcessIds + ")";
			}

			sql = sql + " GROUP BY u.item_id, u.first_name,u.last_name";
			if (scopeIsTask) {
				sql = sql +
				      ", bt.list_item, bt.item_id ORDER BY (u.first_name + ' ' + u.last_name) ASC, u.item_id ASC, bt.list_item ASC";
			} else {
				sql = sql + " ORDER BY (u.first_name + ' ' + u.last_name) ASC, u.item_id ASC";
			}

			return sql;
		}
	} //Class ends
} //Namespace ends