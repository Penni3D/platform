﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.instanceUnions {
	[Route("v1/settings/InstanceUnions")]
	public class InstanceUnionsRest : PageBase {
		public InstanceUnionsRest() : base("InstanceUnions", true) { }

		[HttpGet("")]
		public List<InstanceUnion> GetUnions() {
			var iu = new InstanceUnions(instance, currentSession);
			return iu.GetUnions();
		}

		[HttpGet("{instanceUnionId}")]
		public InstanceUnion GetUnion(int instanceUnionId) {
			CheckRight("read");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.GetUnion(instanceUnionId);
		}

		[HttpPost("")]
		public InstanceUnion AddUnion([FromBody] InstanceUnion u) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.AddUnion(u);
		}

		[HttpPut("")]
		public List<InstanceUnion> SaveUnions([FromBody] List<InstanceUnion> unions) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.SaveUnions(unions);
		}


		[HttpPost("addProcess")]
		public InstanceUnionProcess AddProcess([FromBody] InstanceUnionProcess up) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.AddProcess(up);
		}

		[HttpPut("saveProcesses")]
		public bool SaveProcesses([FromBody] List<InstanceUnionProcess> processes) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.SaveProcesses(processes);
		}
		
		
		[HttpGet("getUnionProcesses/{instanceUnionId}")]
		public List<InstanceUnionProcess> GetUnionProcessess(int instanceUnionId) {
			CheckRight("read");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.GetUnionProcesses(instanceUnionId);
		}

		[HttpPost("addUnionRow")]
		public InstanceUnionRow AddUnionRow([FromBody] InstanceUnionRow ur) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.AddUnionRow(ur);
		}

		[HttpGet("getUnionRows/{instanceUnionId}")]
		public List<InstanceUnionRow> GetUnionRows(int instanceUnionId) {
			CheckRight("read");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.GetUnionRows(instanceUnionId);
		}

		[HttpPut("saveUnionColumnsAndRows")]
		public Dictionary<int, Dictionary<int, int?>> saveUnionColumnsAndRows(
			[FromBody] Tuple<Dictionary<int, Dictionary<int, int?>>, List<InstanceUnionRow>> data) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			
			//Update Rows
			iu.SaveRows(data.Item2);
			
			//Update Columns
			var result = iu.SaveUnionColumns(data.Item1);

			//Update Data Types
			if (data.Item2.Count > 0) iu.UpdateDataTypes(data.Item2[0].InstanceUnionId);

			return result;
		}

		[HttpGet("getUnionColumns/{instanceUnionId}")]
		public Dictionary<int, Dictionary<int, int?>> GetUnionColumns(int instanceUnionId) {
			CheckRight("read");
			var iu = new InstanceUnions(instance, currentSession);
			return iu.GetUnionColumns(instanceUnionId);
		}

		[HttpDelete("deleteUnionProcess/{instanceUnionProcessId}")]
		public void DeleteUnionProcess(int instanceUnionProcessId) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			iu.DeleteUnionProcess(instanceUnionProcessId);
		}

		[HttpDelete("deleteUnionRow/{instanceUnionRowId}")]
		public void DeleteUnionRow(int instanceUnionRowId) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			iu.DeleteUnionRow(instanceUnionRowId);
		}

		[HttpDelete("deleteUnion/{instanceUnionId}")]
		public void DeleteUnion(int instanceUnionId) {
			CheckRight("write");
			var iu = new InstanceUnions(instance, currentSession);
			iu.DeleteUnion(instanceUnionId);
		}
	}
}