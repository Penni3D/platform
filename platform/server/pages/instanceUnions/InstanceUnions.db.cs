﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages.instanceUnions {
	public class InstanceUnion {
		public InstanceUnion() { }

		public InstanceUnion(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			InstanceUnionId = (int) dr[alias + "instance_union_id"];
			Variable = Conv.ToStr(dr[alias + "variable"]);
			Process = Conv.ToStr(dr[alias + "process"]);

			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
		}

		public int InstanceUnionId { get; set; }
		public string Variable { get; set; }
		public string Process { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
	}

	public class InstanceUnionProcess {
		public InstanceUnionProcess() { }

		public InstanceUnionProcess(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			InstanceUnionProcessId = (int) dr[alias + "instance_union_process_id"];
			InstanceUnionId = (int) dr[alias + "instance_union_id"];
			PortfolioId = Conv.ToInt(dr[alias + "portfolio_id"]);
			Process = (string) dr[alias + "process"];
		}

		public int InstanceUnionProcessId { get; set; }
		public int InstanceUnionId { get; set; }
		public int PortfolioId { get; set; }
		public string Process { get; set; }
	}

	public class InstanceUnionRow {
		public InstanceUnionRow() { }

		public InstanceUnionRow(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			InstanceUnionRowId = (int) dr[alias + "instance_union_row_id"];
			InstanceUnionId = (int) dr[alias + "instance_union_id"];
			ItemColumnId = (int) dr[alias + "item_column_id"];
			Name = new Translations(session.instance, session).GetTranslations(Conv.ToStr(dr[alias + "name"]));
			Variable = Conv.ToStr(dr[alias + "variable"]);
			Process = Conv.ToStr(dr[alias + "process"]);
		}

		public int InstanceUnionRowId { get; set; }
		public Dictionary<string, string> Name { get; set; }
		public string Variable { get; set; }
		public int ItemColumnId { get; set; }
		public string Process { get; set; }
		public int InstanceUnionId { get; set; }
	}

	public class InstanceUnionColumn {
		public InstanceUnionColumn() { }

		public InstanceUnionColumn(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			InstanceUnionRowId = (int) dr[alias + "instance_union_row_id"];
			InstanceUnionProcessId = (int) dr[alias + "instance_union_process_id"];
			InstanceUnionRowId = (int) dr[alias + "instance_union_row_id"];
			ItemColumnId = (int) dr[alias + "item_column_id"];
		}

		public int InstanceUnionColumnId { get; set; }
		public int InstanceUnionProcessId { get; set; }
		public int InstanceUnionRowId { get; set; }
		public int ItemColumnId { get; set; }
	}

	public class InstanceUnions : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;
		private readonly Translations translations;

		public InstanceUnions(string instance, AuthenticatedSession a) : base(instance,
			a) {
			authenticatedSession = a;
			translations = new Translations(instance, authenticatedSession);
		}

		public DataTable GetPortfolioUnionProcessTable(int portfolioId) {
			SetDBParam(SqlDbType.Int, "@processPortfolioId", portfolioId);
			const string sql = "SELECT process, portfolio_id FROM instance_union_processes WHERE instance_union_id = (SELECT instance_union_id FROM instance_unions WHERE process = (SELECT process FROM process_portfolios WHERE process_portfolio_id = @processPortfolioId)) ORDER BY instance_union_process_id ASC";
			return db.GetDatatable(sql, DBParameters);
		}

		public DataTable GetPortfolioUnionDataTable(int portfolioId) {
			SetDBParam(SqlDbType.Int, "@processPortfolioId", portfolioId);
			const string sql = " SELECT ic2.name AS destination_name, ic2.item_column_id AS destination_id, ic2.process AS destination_process, ic1.process AS source_process, iup.portfolio_id AS source_portfolio_id, ic1.name AS source_name, ic1.item_column_id AS source_id " +
			                   " FROM instance_union_rows iur  " +
			                   " LEFT JOIN instance_union_columns iuc ON iuc.instance_union_row_id = iur.instance_union_row_id " +
			                   " LEFT JOIN item_columns ic1 ON ic1.item_column_id = iuc.item_column_id " +
			                   " LEFT JOIN item_columns ic2 ON ic2.item_column_id = iur.item_column_id " +
                               " LEFT JOIN instance_union_processes iup ON iup.instance_union_process_id = iuc.instance_union_process_id " +
			                   " WHERE iur.instance_union_id = (SELECT instance_union_id FROM instance_unions WHERE process = (SELECT process FROM process_portfolios WHERE process_portfolio_id = @processPortfolioId)) " +
			                   " ORDER BY destination_id ASC ";
            return db.GetDatatable(sql, DBParameters);
		}

		public List<InstanceUnion> GetUnions() {
			var retval = new List<InstanceUnion>();
			foreach (DataRow dr in db
				.GetDatatable("SELECT * FROM instance_unions WHERE instance = @instance ", DBParameters).Rows) {
				retval.Add(new InstanceUnion(dr, authenticatedSession));
			}

			return retval;
		}

		public InstanceUnion AddUnion(InstanceUnion u) {
			u.InstanceUnionId =
				db.ExecuteInsertQuery("INSERT INTO instance_unions (instance) VALUES(@instance)",
					DBParameters);

			if (u.LanguageTranslation[authenticatedSession.locale_id] == null) return u;


			SetDBParam(SqlDbType.NVarChar, "@union_process", "union_process_" + u.InstanceUnionId);
			db.ExecuteStoredProcedure("EXEC app_ensure_process @instance, @union_process, @union_process, 6",
				DBParameters);


			u.Process = "union_process_" + u.InstanceUnionId;
			var newVar = translations.GetTranslationVariable("",
				u.LanguageTranslation[authenticatedSession.locale_id], "UNION");


			SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
			SetDBParam(SqlDbType.Int, "@instanceUnionId", u.InstanceUnionId);


			db.ExecuteUpdateQuery(
				"UPDATE instance_unions SET variable = @variable, process = @union_process WHERE instance = @instance AND instance_union_id = @instanceUnionId",
				DBParameters);

			foreach (var translation in u.LanguageTranslation) {
				translations.insertOrUpdateProcessTranslation(null, newVar, translation.Value, translation.Key);
			}

			u.Variable = newVar;

			return u;
		}

		public InstanceUnion SaveUnion(InstanceUnion u) {
			if (u.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVar = translations.GetTranslationVariable("",
					u.LanguageTranslation[authenticatedSession.locale_id], "UNION");

				SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
				SetDBParam(SqlDbType.Int, "@instanceUnionId", u.InstanceUnionId);
				db.ExecuteUpdateQuery(
					"UPDATE instance_unions SET variable=@variable WHERE instance = @instance AND process = @process AND instance_union_id = @instanceUnionId",
					DBParameters);

				foreach (var translation in u.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(null, newVar, translation.Value, translation.Key);
				}
			}

			return u;
		}

		public List<InstanceUnion> SaveUnions(List<InstanceUnion> uu) {
			foreach (var u in uu) {
				SaveUnion(u);
			}

			return uu;
		}

		public InstanceUnionProcess AddProcess(InstanceUnionProcess up) {
			SetDBParam(SqlDbType.Int, "@instanceUnionId", up.InstanceUnionId);
			SetDBParam(SqlDbType.NVarChar, "@process", up.Process);

			up.InstanceUnionProcessId =
				db.ExecuteInsertQuery(
					"INSERT INTO instance_union_processes (instance_union_id, instance, process) VALUES (@instanceUnionId, @instance, @process) ",
					DBParameters);
			return up;
		}

		public bool SaveProcesses(List<InstanceUnionProcess> processes) {
			foreach (var p in processes) {
				SetDBParam(SqlDbType.Int, "@instanceUnionProcessId", p.InstanceUnionProcessId);
				SetDBParam(SqlDbType.Int, "@portfolio_id", p.PortfolioId);
				db.ExecuteUpdateQuery(
					"UPDATE instance_union_processes SET portfolio_id = @portfolio_id WHERE instance_union_process_id = @instanceUnionProcessId",
					DBParameters);
			}

			return true;
		}

		public List<InstanceUnionProcess> GetUnionProcesses(int instanceUnionId) {
			var retval = new List<InstanceUnionProcess>();
			SetDBParam(SqlDbType.Int, "@instanceUnionId", instanceUnionId);

			foreach (DataRow dr in db
				.GetDatatable(
					"SELECT * FROM instance_union_processes WHERE instance = @instance AND instance_union_id = @instanceUnionId",
					DBParameters).Rows) {
				retval.Add(new InstanceUnionProcess(dr, authenticatedSession));
			}

			return retval;
		}

		public InstanceUnion GetUnion(int instanceUnionId) {
			SetDBParam(SqlDbType.Int, "@instanceUnionId", instanceUnionId);
			var dr =
				db.GetDataRow(
					"SELECT * FROM instance_unions WHERE instance = @instance AND instance_union_id = @instanceUnionId",
					DBParameters);
			if (dr != null) {
				return new InstanceUnion(dr, authenticatedSession);
			}

			return null;
		}

		public InstanceUnionRow AddUnionRow(InstanceUnionRow ur) {
			SetDBParam(SqlDbType.Int, "@instanceUnionId", ur.InstanceUnionId);

			ur.InstanceUnionRowId =
				db.ExecuteInsertQuery(
					"INSERT INTO instance_union_rows (instance_union_id) VALUES (@instanceUnionId) ",
					DBParameters);

			var process = Conv.ToStr(
				db.ExecuteScalar("SELECT process FROM instance_unions WHERE instance_union_id = @instanceUnionId",
					DBParameters));

			var ic = new ItemColumns(authenticatedSession.instance, process, authenticatedSession);

			ur.ItemColumnId = ic.AddNewItemColumn("union_row_" + ur.InstanceUnionRowId, false,
				"union_row_" + ur.InstanceUnionRowId, 0,
				"", 0);


			SetDBParam(SqlDbType.Int, "@itemColumnId", ur.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@instanceUnionRowId", ur.InstanceUnionRowId);
			db.ExecuteUpdateQuery(
				"UPDATE instance_union_rows SET item_column_id = @itemColumnId WHERE instance_union_row_id = @instanceUnionRowId",
				DBParameters);

			return ur;
		}

		public bool SaveRows(List<InstanceUnionRow> rows) {
			foreach (var r in rows) {
				SetDBParam(SqlDbType.Int, "@urid", r.InstanceUnionRowId);
				SetDBParam(SqlDbType.NVarChar, "@name", r.Name);
				foreach (var t in r.Name) {
					translations.insertOrUpdateProcessTranslation(r.Process, r.Variable, t.Value, t.Key);
				}
			}

			DBParameters.Clear();

			return true;
		}

		public List<InstanceUnionRow> GetUnionRows(int instanceUnionId) {
			var retval = new List<InstanceUnionRow>();
			SetDBParam(SqlDbType.Int, "@instanceUnionId", instanceUnionId);

			foreach (DataRow dr in db
				.GetDatatable(
					"SELECT r.*, ic.variable AS name, ic.variable AS variable, u.process FROM instance_union_rows r LEFT JOIN instance_unions u ON u.instance_union_id = r.instance_union_id LEFT JOIN item_columns ic ON ic.item_column_id = r.item_column_id WHERE r.instance_union_id = @instanceUnionId",
					DBParameters).Rows) {
				retval.Add(new InstanceUnionRow(dr, authenticatedSession));
			}

			return retval;
		}

		public Dictionary<int, Dictionary<int, int?>> SaveUnionColumns(Dictionary<int, Dictionary<int, int?>> unions) {
			foreach (var unionRowId in unions.Keys) {
				foreach (var unionProcessId in unions[unionRowId].Keys) {
					SetDBParam(SqlDbType.Int, "@instanceUnionRowId", unionRowId);
					SetDBParam(SqlDbType.Int, "@instanceUnionProcessId", unionProcessId);
					if (unions[unionRowId][unionProcessId] != null) {
						SetDBParam(SqlDbType.Int, "@itemColumnId", unions[unionRowId][unionProcessId]);

						if (db.ExecuteUpdateQuery("UPDATE instance_union_columns SET " +
						                          " item_column_id = @itemColumnId WHERE instance_union_row_id = @instanceUnionRowId AND instance_union_process_id = @instanceUnionProcessId  ",
							DBParameters) == 0) {
							db.ExecuteInsertQuery(
								"INSERT INTO instance_union_columns (instance_union_row_id, instance_union_process_id, item_column_id) VALUES (@instanceUnionRowId, @instanceUnionProcessId, @itemColumnId) ",
								DBParameters);
						}
					}
				}
			}

			return unions;
		}

		public void UpdateDataTypes(int instanceUnionId) {
			var sql = " SELECT iq.*, ic1.data_type as source_data_type, ic2.data_type AS target_data_type, ic1.data_additional AS source_da, ic1.data_additional2 AS source_da2, ic1.input_name AS source_input_name FROM ( " +
			          " SELECT  " +
			          "       iur.item_column_id as target_item_column_id, " +
			          "       (SELECT TOP 1 item_column_id FROM instance_union_columns iuc WHERE iuc.instance_union_row_id = iur.instance_union_row_id ORDER BY iuc.instance_union_process_id ASC) AS source_item_column_id " +
			          " FROM instance_union_rows iur WHERE iur.instance_union_id = @instanceUnionId " +
			          " ) iq " +
			          " LEFT JOIN item_columns ic1 ON ic1.item_column_id = iq.source_item_column_id " +
			          " LEFT JOIN item_columns ic2 ON ic2.item_column_id = iq.target_item_column_id " +
			          " WHERE ic1.data_type <> ic2.data_type OR ic1.data_additional <> ic2.data_additional OR ic1.data_additional2 <> ic2.data_additional2 OR ic1.input_name <> ic2.input_name";

			SetDBParam(SqlDbType.Int, "@instanceUnionId", instanceUnionId);

			var updated = false;
			foreach (DataRow updates in db.GetDatatable(sql, DBParameters).Rows) {
				DBParameters.Clear();

				SetDBParam(SqlDbType.Int, "@dt", Conv.ToInt(updates["source_data_type"]));
				SetDBParam(SqlDbType.Int, "@ic", Conv.ToInt(updates["target_item_column_id"]));
				SetDBParam(SqlDbType.NVarChar, "@da", Conv.ToStr(updates["source_da"]));
				SetDBParam(SqlDbType.NVarChar, "@da2", Conv.ToStr(updates["source_da2"]));
				SetDBParam(SqlDbType.NVarChar, "@is", Conv.ToStr(updates["source_input_name"]));

				db.ExecuteUpdateQuery("UPDATE item_columns SET data_type = @dt, data_additional = @da, data_additional2 = @da2, input_name = @is WHERE item_column_id = @ic",
					DBParameters);

				updated = true;
			}

			if (updated) Cache.Initialize();
		}

		public Dictionary<int, Dictionary<int, int?>> GetUnionColumns(int instanceUnionId) {
			SetDBParam(SqlDbType.Int, "@instanceUnionId", instanceUnionId);
			var sql =
				"SELECT iup.*, iur.instance_union_id, iur.instance_union_row_id, iuc.* FROM instance_union_processes iup  " +
				"INNER JOIN instance_union_rows iur on iup.instance_union_id = iur.instance_union_id " +
				"LEFT JOIN instance_union_columns iuc on iur.instance_union_row_id = iuc.instance_union_row_id and iup.instance_union_process_id = iuc.instance_union_process_id " +
				"WHERE iup.instance_union_id = @instanceUnionId ORDER BY iur.instance_union_row_id, iup.instance_union_process_id ";

			var oldUnionRowId = 0;
			var oldUnionProcessId = 0;
			var unionRowId = 0;
			var unionProcessId = 0;

			var unionProcessDict = new Dictionary<int, int?>();
			var retval = new Dictionary<int, Dictionary<int, int?>>();
			foreach (DataRow dr in db.GetDatatable(sql, DBParameters).Rows) {
				unionRowId = Conv.ToInt(dr["instance_union_row_id"]);
				unionProcessId = Conv.ToInt(dr["instance_union_process_id"]);

				if (oldUnionRowId != unionRowId) {
					if (oldUnionRowId > 0) {
						retval.Add(oldUnionRowId, unionProcessDict);
					}

					unionProcessDict = new Dictionary<int, int?>();
				}

				if (DBNull.Value.Equals(dr["item_column_id"])) {
					unionProcessDict.Add(unionProcessId, null);
				} else {
					unionProcessDict.Add(unionProcessId, Conv.ToInt(dr["item_column_id"]));
				}

				oldUnionRowId = unionRowId;
				oldUnionProcessId = unionProcessId;
			}

			retval.Add(unionRowId, unionProcessDict);
			return retval;
		}

		public void DeleteUnionProcess(int instanceUnionProcessId) {
			SetDBParam(SqlDbType.Int, "@instanceUnionProcessId", instanceUnionProcessId);
			db.ExecuteDeleteQuery(
				"DELETE FROM instance_union_processes WHERE instance = @instance AND instance_union_process_id = @instanceUnionProcessId ",
				DBParameters);
		}

		public void DeleteUnionRow(int instanceUnionRowId) {
			SetDBParam(SqlDbType.Int, "@instanceUnionRowId", instanceUnionRowId);

			db.ExecuteDeleteQuery(
				"DELETE FROM item_columns WHERE item_column_id = (SELECT item_column_id FROM instance_union_rows WHERE instance_union_row_id = @instanceUnionRowId)",
				DBParameters);

			db.ExecuteDeleteQuery(
				"DELETE FROM instance_union_rows WHERE instance_union_row_id = @instanceUnionRowId ",
				DBParameters);

			Cache.Initialize();
		}

		public void DeleteUnion(int instanceUnionId) {
			SetDBParam(SqlDbType.Int, "@instanceUnionId", instanceUnionId);

			db.ExecuteDeleteQuery(
				"DELETE FROM processes WHERE process = (SELECT process FROM instance_unions WHERE instance_union_id = @instanceUnionId)",
				DBParameters);

			db.ExecuteDeleteQuery(
				"DELETE FROM instance_unions WHERE instance_union_id = @instanceUnionId ",
				DBParameters);
		}
	}
}