﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.pages.statspage {
	[Route("v1/features/[controller]")]
	public class statspage : RestBase {
		[HttpGet("pageviews/{view}/{processId}/{userId}/{tabId}/{feature}")]
		public List<int> Get(string view, int processId, int userId, int tabId, string feature) {
			var rm = new StatspageDatas(instance, currentSession);
			var retval = rm.GetStatspageData(view, processId, userId, tabId, feature);

			return retval;
		}


		[HttpGet("query/{view}/{timerange}/{uniqueViews}/{startDate}")]
		public List<StatsByPageData> Query(string view, string timerange, string uniqueViews, DateTime startDate) {
			var rm = new StatspageDatas(instance, currentSession);
			return rm.QueryStatsPageData(view, timerange, uniqueViews, startDate);
		}


		[HttpGet("options")]
		public StatsPageOptions getOptions() {
			var rm = new StatspageDatas(instance, currentSession);
			return rm.GetOptions();
		}

		[HttpPost]
		public IActionResult Post([FromBody]StatspageData statspageData) {
			var statspageDatas = new StatspageDatas(instance, currentSession);
			statspageDatas.add(statspageData);

			return new ObjectResult(statspageData);
		}
	}
}
