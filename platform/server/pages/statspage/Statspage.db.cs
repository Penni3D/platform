﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.pages.statspage {

	public class StatsPageOptions {
		public List<string> views { get; set; }
	}

	public class StatsByPageData {
		public int year { get; set; }
		public int month { get; set; }
		public int day { get; set; }
		public int week { get; set; }
		public int viewCount { get; set; }
	}

	public class StatspageDatas : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;

		public StatspageDatas(string instance, AuthenticatedSession authenticatedSession) : base(instance, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		private object valueOrDBNull(object value) {
			return value == null ? DBNull.Value : value;
		}

		public StatspageData add(StatspageData statspageData) {
			var db = new Connections(authenticatedSession);

			SetDBParam(SqlDbType.NVarChar, "@view", statspageData.view);
			SetDBParam(SqlDbType.Int, "@process_item_id", valueOrDBNull(statspageData.process));
			SetDBParam(SqlDbType.Int, "@user_item_id", valueOrDBNull(statspageData.userId));
			SetDBParam(SqlDbType.Int, "@tab", valueOrDBNull(statspageData.tab));
			SetDBParam(SqlDbType.NVarChar, "@feature", valueOrDBNull(statspageData.feature));

			var sql = "INSERT INTO instance_visit_stats ([view], process_item_id, user_item_id, date, tab_id, feature) VALUES (@view, @process_item_id, @user_item_id, CURRENT_TIMESTAMP, @tab, @feature)";
			db.ExecuteInsertQuery(sql, DBParameters);
			return statspageData;
		}

		public List<int> GetStatspageData(string view, int processId = 0, int userId = 0, int tabId = 0, string feature = "") {

			var retval = new List<int>();
			var whereClause = "";

			if (processId > 0) {
				SetDBParam(SqlDbType.Int, "@processId", processId);
				whereClause += " AND process_item_id = @processId";
			}

			if (userId > 0) {
				SetDBParam(SqlDbType.Int, "@userId", userId);
				whereClause += " AND user_item_id = @userId";
			}

			if (tabId > 0) {
				SetDBParam(SqlDbType.Int, "@tabId", tabId);
				whereClause += " AND tab_id = @tabId";
			}

			if (feature != null && feature != "") {
				SetDBParam(SqlDbType.NVarChar, "@feature", feature);
				whereClause += " AND feature = @feature";
			}

			SetDBParam(SqlDbType.NVarChar, "@view", view);
			var uniqueViews = 0;
			var totalViews = 0;
			var viewsByUserId = new Dictionary<int, int>();
			foreach (DataRow row in db.GetDatatable("SELECT user_item_id, count(*) count FROM instance_visit_stats WHERE [view] = @view " + whereClause + " GROUP BY user_item_id", DBParameters).Rows) {
				uniqueViews++;
				totalViews += (int)row["count"];
			}
			retval.Add(uniqueViews);
			retval.Add(totalViews);
			return retval;
		}


		private string getDatePartClause(string qualifier) {
			return "DATEPART(" + qualifier + ", date)";
		}

		public List<StatsByPageData> QueryStatsPageData(string view = "", string timerange = "day", string uniqueViews = "all", DateTime startDate = new DateTime()) {
			var datepart = getDatePartClause("YY") + " year";
			var groupByClause = getDatePartClause("YY");

			if (timerange == "week") {
				datepart += ", " + getDatePartClause("WW") + " week";
				groupByClause += ", " + getDatePartClause("WW");
			} else if (timerange != "year") {
				datepart += ", " + getDatePartClause("MM") + " month";
				groupByClause += ", " + getDatePartClause("MM");
				if (timerange != "month") {
					datepart += ", " + getDatePartClause("DD") + " day";
					groupByClause += ", " + getDatePartClause("DD");
				}
			}
			var retval = new List<StatsByPageData>();

			SetDBParam(SqlDbType.DateTime, "@startDate", startDate);
			var whereClause = "WHERE date >= @startDate ";
			if (view != null && view != "") {
				SetDBParam(SqlDbType.NVarChar, "@view", view);
				whereClause += " AND [view] = @view";
			}
			var countWhat = "user_item_id";
			if (uniqueViews == "unique") {
				countWhat = "distinct " + countWhat;
			}

			var sql = "SELECT " + datepart + ", count(" + countWhat + ") count FROM instance_visit_stats " + whereClause + " GROUP BY " + groupByClause;

			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var r = new StatsByPageData();
				r.viewCount = (int)row["count"];
				r.year = (int)row["year"];
				if (row.Table.Columns.Contains("week")) {
					r.week = (int)row["week"];
				}
				if (row.Table.Columns.Contains("month")) {
					r.month = (int)row["month"];
				}
				if (row.Table.Columns.Contains("day")) {
					r.day = (int)row["day"];
				}
				retval.Add(r);
			}

			return retval;
		}

		public StatsPageOptions GetOptions() {
			var options = new StatsPageOptions();
			options.views = new List<string>();
			foreach (DataRow row in db.GetDatatable("SELECT DISTINCT [view] FROM instance_visit_stats", DBParameters).Rows) {

				options.views.Add((string)row["view"]);
			}
			return options;
		}
	}

	[Serializable]
	public class StatspageData {
		public StatspageData() { }

		public StatspageData(DataRow row) {
			view = (string)row["view"];
			process = (string)nullOrValue(row, "process_item_id");
			userId = (int)nullOrValue(row, "user_item_id");
			date = (DateTime)nullOrValue(row, "date");
			tab = (int?)nullOrValue(row, "tab_id");
			feature = (string)nullOrValue(row, "feature");
		}

		public string view { get; set; }
		public string process { get; set; }
		public int userId { get; set; }
		public DateTime date { get; set; }
		public int? tab { get; set; }
		public string feature { get; set; }

		private object nullOrValue(DataRow row, string fieldName) {
			if (DBNull.Value.Equals(row[fieldName])) {
				return null;
			}

			return row[fieldName];
		}
	}
}
