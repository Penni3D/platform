﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.features.user;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.common {
	public static class Diag {
		public enum LogCategories {
			System = 0,
			Authentication = 1,
			Database = 2,
			Email = 3,
			Angular = 4,
			RegularJs = 5,
			Integration = 6,
			BackgroundTask = 7
		}

		public enum LogSeverities {
			Message = 0,
			Warning = 1,
			Error = 2,
			Dataloss = 3,
			SuccessMessage = 4,
			Performance = 5,
			Sql = 6
		}

		public enum LogTypes {
			ServerMessage = 0,
			ServerException = 1,
			ClientAngular = 2,
			ClientJavaScript = 3
		}

		public static string ServerId = Util.CreateToken(16);
		public static bool LogQueries = false;
		public static bool PerformanceCounters = true;
		public static bool ErrorState = false;
		public static bool Restarting = false;
		public static int ConnectedSignalRUsers = 0;
		private static readonly StringBuilder ConsoleStore = new StringBuilder();
		private static readonly List<string> SqlQueries = new List<string>();
		public static ConfigurationReport LastConfigurationReport = null;

		public static bool WriteToConsole { get; } = true;
		public static bool WriteToDb { get; } = true;

		public static void AddSqlQuery(string cmdText, List<SqlParameter> cmdParams) {
			SqlQueries.Add(ParseSqlParametersIntoString(cmdText, cmdParams));
		}

		private static DateTime _timer = DateTime.Now;
		private static double _counterTotalMS = 0;

		public static void ResetPerformanceTimer() {
			_timer = DateTime.Now;
			_counterTotalMS = 0;
		}

		public static void SupressException(Exception e) { }

		public static double GetPerformanceTotalMs() {
			return _counterTotalMS;
		}

		public static void WriteQueriesToConsole() {
			foreach (var q in GetSqlQueries()) {
				LogToConsole(q, LogSeverities.Sql);
			}

			LogToConsole("Queries: " + GetSqlQueries().Count);
		}

		public static void LogPerformanceCounter(string message) {
			if (!PerformanceCounters) return;
			var t = DateTime.Now.Subtract(_timer).TotalMilliseconds;
			_counterTotalMS += t;
			LogToConsole(message + " -> " + t, LogSeverities.Performance);
			_timer = DateTime.Now;
		}

		public static string ParseSqlParametersIntoString(string cmdText, List<SqlParameter> cmdParams) {
			if (cmdParams != null) {
				cmdText = cmdText + " ";
				foreach (var param in cmdParams) {
					if (param.SqlDbType == SqlDbType.NVarChar || param.SqlDbType == SqlDbType.NText ||
					    param.SqlDbType == SqlDbType.DateTime) {
						cmdText = cmdText.Replace("@" + param.ParameterName.Replace("@", "") + " ",
							"'" + Conv.ToStr(param.SqlValue) + "'");
					} else {
						cmdText = cmdText.Replace("@" + param.ParameterName.Replace("@", "") + " ",
							Conv.ToStr(param.SqlValue));
					}
				}
			}

			return cmdText;
		}

		public static List<string> GetSqlQueries() {
			return SqlQueries;
		}

		public static void ClearSqlQueries() {
			SqlQueries.Clear();
		}

		private static SqlParameter SetDbParam(SqlDbType type, string name, object value) {
			var p = new SqlParameter {
				ParameterName = name,
				SqlDbType = type,
				Value = value
			};
			return p;
		}

		public static List<ApplicationVersion> GetList() {
			var av = new List<ApplicationVersion>();
			var rootDir = Config.GetValue("Deployment", "ApplicationRootDirectory");
			var dir = new DirectoryInfo(rootDir);
			if (dir.Exists) {
				CheckDirectory(dir, av, dir.Name);
			}

			return av;
		}

		private static void CheckDirectory(DirectoryInfo d, List<ApplicationVersion> versions, string rootName) {
			//Check if current directory has release.xml
			var versionName = Convert.ToString(d.Name);
			if (d.Parent != null && d.Parent.Name != rootName) {
				versionName += " - " + d.Parent.Name;
				if (d.Parent != null && d.Parent.Parent != null && d.Parent.Parent.Name != rootName)
					versionName += " - " + d.Parent.Parent.Name;
			}

			var fi = new FileInfo(Path.Combine(d.FullName, "release.xml"));
			if (fi.Exists) {
				var ap = new ApplicationVersion();
				var doc = XDocument.Load(Path.Combine(d.FullName, "release.xml"));
				var version = doc.Root.Element("version").Value;
				var releaseDate = doc.Root.Element("release-date").Value;
				ap.VersionNumber = version;
				ap.ReleaseDate = releaseDate;
				ap.ApplicationName = versionName;
				versions.Add(ap);
			} else {
				foreach (var dd in d.GetDirectories()) {
					CheckDirectory(dd, versions, rootName);
				}
			}
		}

		public static List<ServerDetails> GetAllServers(string listname, string instance, int currentUserId) {
			var result = new List<ServerDetails>();
			var serverSql = "SELECT list_item, token_key, name, instance FROM " +
			                Conv.ToSql("_" + instance + "_" + listname);
			var cb = new ConnectionBase(instance, new AuthenticatedSession(instance, currentUserId, null));
			foreach (DataRow row in cb.db.GetDatatable(serverSql, cb.DBParameters).Rows) {
				var sd = new ServerDetails();
				sd.url = Conv.ToStr(row["list_item"]);
				sd.tokenKey = Conv.ToStr(row["token_key"]);
				sd.name = Conv.ToStr(row["name"]);
				sd.instance = Conv.ToStr(row["instance"]);
				result.Add(sd);
			}

			return result;
		}

		public static string GetConsole() {
			return ConsoleStore.ToString();
		}

		public static void LogToConsole(string message, LogSeverities severity = LogSeverities.Message,
			string instance = "") {
			var c = "#bbbbbb";
			var prefix = "";
			if (WriteToConsole) {
				switch (severity) {
					case LogSeverities.Warning:
						prefix = "WARNING: ";
						Console.ForegroundColor = ConsoleColor.Yellow;
						c = "#a8c200";
						break;
					case LogSeverities.Error:
						prefix = "ERROR: ";
						Console.ForegroundColor = ConsoleColor.Red;
						c = "#ff8683";
						break;
					case LogSeverities.Dataloss:
						Console.ForegroundColor = ConsoleColor.Red;
						c = "#ff8683";
						break;
					case LogSeverities.SuccessMessage:
						Console.ForegroundColor = ConsoleColor.Green;
						c = "#557f19";
						break;
					case LogSeverities.Performance:
						prefix = "PERF: ";
						Console.ForegroundColor = ConsoleColor.DarkBlue;
						break;
					case LogSeverities.Sql:
						prefix = "SQL: ";
						c = "#17b1ff";
						Console.ForegroundColor = ConsoleColor.Blue;
						break;
				}

				Console.Out.WriteLine(prefix + message);
				Console.ResetColor();
			}

			ConsoleStore.Append("<li style='color:" + c + "'>" + message + "</li>");
		}

		private static int LogToDb(
			string instance,
			string message,
			LogCategories category,
			LogSeverities severity = LogSeverities.Message,
			Exception ex = null,
			int userItemId = 0,
			LogTypes logtype = LogTypes.ServerMessage, string additionalMessage = "", int LogId = 0, int FileId = 0) {
			var m = "";
			if (ex == null) return LogToDbFinal(instance, message, m, category, severity, userItemId, logtype, LogId, FileId);
			if (ex is CustomArgumentException || ex is CustomPermissionException || ex is CustomUniqueException) {
				var customServerException = (BaseServerException)ex;
				message = customServerException.GetDescription();
				m = customServerException.StackTrace;
			} else {
				m = ex.StackTrace;
				message = Conv.ToStr(ex);
			}

			message = message + " " + additionalMessage;

			return LogToDbFinal(instance, message, m, category, severity, userItemId, logtype, LogId, FileId);
		}

		public static string CleanStackTrace(string stack) {
			var result = "";
			var lines = stack.Split(" at ");
			foreach (var line in lines) {
				if (!line.Contains(".cs:line") || line.Contains("Exceptions.cs:line") ||
				    line.Contains("Startup.cs:line")) continue;

				try {
					var parts = line.Trim().Split(" in ");
					var signature = parts[0];
					var path = parts[1];
					result += (result == "" ? "" : "\n -> ") + signature.Replace("Keto5.x.", "") + " in " + path.Split("platform/server")[1];
				} catch (Exception e) {
					SupressException(e);
					result += line;
				}
			}

			return result == "" ? stack : result;
		}

		private static int LogToDbFinal(
			string instance,
			string message,
			string stack,
			LogCategories category,
			LogSeverities severity = LogSeverities.Message,
			int userItemId = 0,
			LogTypes logtype = LogTypes.ServerMessage,
			int LogId = 0,
			int FileId = 0
		) {
			var result = 0;
			if (!WriteToDb) return result;
			if (Conv.ToStr(instance) == "") instance = "?";
			var db = new Connections(true);
			var dbParameters = new List<SqlParameter> {
				SetDbParam(SqlDbType.NVarChar, "instance", instance),
				SetDbParam(SqlDbType.NVarChar, "server_id", ServerId),
				SetDbParam(SqlDbType.TinyInt, "log_severity", (int)severity),
				SetDbParam(SqlDbType.TinyInt, "log_category", (int)category),
				SetDbParam(SqlDbType.Int, "user_item_id", userItemId)
			};
			if (stack != null) {
				stack = CleanStackTrace(stack);

				dbParameters.Add(SetDbParam(SqlDbType.NVarChar, "message", Conv.ToStr(message)));
				dbParameters.Add(SetDbParam(SqlDbType.NVarChar, "stack", Conv.ToStr(stack)));
				if (logtype == LogTypes.ClientAngular || logtype == LogTypes.ClientJavaScript) {
					dbParameters.Add(SetDbParam(SqlDbType.TinyInt, "log_type", (int)logtype));
				} else {
					dbParameters.Add(SetDbParam(SqlDbType.TinyInt, "log_type", 1));
				}
			} else {
				dbParameters.Add(SetDbParam(SqlDbType.NVarChar, "message", message));
				dbParameters.Add(SetDbParam(SqlDbType.NVarChar, "stack", ""));
				if (logtype == LogTypes.ClientAngular || logtype == LogTypes.ClientJavaScript) {
					dbParameters.Add(SetDbParam(SqlDbType.TinyInt, "log_type", (int)logtype));
				} else {
					dbParameters.Add(SetDbParam(SqlDbType.TinyInt, "log_type", 0));
				}
			}

			dbParameters.Add(SetDbParam(SqlDbType.Int, "@logId", LogId));
			dbParameters.Add(SetDbParam(SqlDbType.Int, "@fileId", FileId));
			result = db.ExecuteInsertQuery(
				"INSERT INTO instance_logs (instance, server_id, message, stack, log_type, log_severity, log_category, user_item_id, background_log_id, file_id) VALUES (@instance, @server_id, @message, @stack, @log_type, @log_severity, @log_category, @user_item_id, @logId, @fileId)"
				,
				dbParameters, "instance_logs", "instance_log_id");


			WriteToEventLog(Config.EnvironmentIdentifier, message + (stack == "" ? "" : " --> ") + stack, severity);

			return result;
		}

		private static void WriteToEventLog(string id, string message, LogSeverities type) {
			if (Environment.OSVersion.Platform == PlatformID.Unix) return;

			var etype = type switch {
				LogSeverities.Warning => EventLogEntryType.Warning,
				LogSeverities.Error => EventLogEntryType.Error,
				LogSeverities.Dataloss => EventLogEntryType.Error,
				LogSeverities.Performance => EventLogEntryType.Error,
				LogSeverities.Sql => EventLogEntryType.Error,
				_ => EventLogEntryType.Information
			};

			using var eventLog = new EventLog("Application") { Source = ".NET Runtime" };
			eventLog.WriteEntry("Application " + id + ": " + message, etype, 1000);
		}

		private static Dictionary<string, object> ExecuteLogSql(string instance, List<SqlParameter> dbParameters,
			int top,
			int limit,
			string sql) {
			var db = new Connections();
			if (dbParameters == null) {
				dbParameters = new List<SqlParameter>();
			}

			dbParameters.Add(SetDbParam(SqlDbType.NVarChar, "instance", instance));
			var t = db.GetDatatable(sql, "occurence DESC", "instance_log_id", top, limit, dbParameters);
			foreach (DataRow r in t.Rows) {
				r["log_type"] = ((LogTypes)Conv.ToInt(r["log_type"])).ToString();
				r["log_severity"] = ((LogSeverities)Conv.ToInt(r["log_severity"])).ToString();
				r["log_category"] = ((LogCategories)Conv.ToInt(r["log_category"])).ToString();
			}

			// SetDbParam(SqlDbType.NVarChar, "client",
			// 	context.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString()),
			var result = new Dictionary<string, object> { { "rowdata", t }, { "rowcount", db.lastRowTotal } };
			return result;
		}

		public static Dictionary<string, object>
			GetLogs(string instance, LogCategories category, int top = 0, int limit = 25) {
			if (instance == "?") return null;
			var dbParameters = new List<SqlParameter> { SetDbParam(SqlDbType.TinyInt, "log_category", (int)category) };
			return ExecuteLogSql(instance, dbParameters, top, limit,
				"SELECT instance_log_id, message, occurence, stack, CAST(log_type AS nvarchar) AS log_type, CAST(log_severity AS nvarchar) AS log_severity, CAST(log_category AS nvarchar) AS log_category, ISNULL(u.domain,'') + ' ' + ISNULL(u.login,'') AS login FROM instance_logs il LEFT JOIN _"
				+
				instance +
				"_user u ON u.item_id = il.user_item_id WHERE instance = @instance AND log_category = @log_category");
		}

		public static Dictionary<string, object> GetLogs(string instance, int top = 0, int limit = 25) {
			if (instance == "?") return null;
			return ExecuteLogSql(instance, null, top, limit,
				"SELECT instance_log_id, message, occurence, stack, CAST(log_type AS nvarchar) AS log_type, CAST(log_severity AS nvarchar) AS log_severity, CAST(log_category AS nvarchar) AS log_category, ISNULL(u.domain,'') + ' ' + ISNULL(u.login,'') AS login FROM instance_logs il LEFT JOIN _"
				+
				instance + "_user u ON u.item_id = il.user_item_id WHERE instance = @instance");
		}

		public static int ErrorsToday(string instance) {
			var db = new Connections();
			var dbParameters = new List<SqlParameter> { SetDbParam(SqlDbType.NVarChar, "instance", instance) };
			return Conv.ToInt(db.ExecuteScalar(
				"SELECT COUNT(*) FROM instance_logs WHERE log_severity = 2 AND log_type > 0 AND YEAR(occurence) = YEAR(getdate()) AND MONTH(occurence) = MONTH(getdate()) AND DAY(occurence) = DAY(getdate()) AND instance = @instance",
				dbParameters));
		}

		private static ConcurrentDictionary<string, Dictionary<string, object>> _requests = new();

		public static void FlushRequests() {
			var requestsCopy = _requests.ToDictionary(entry => entry.Key,
				entry => entry.Value);
			_requests.Clear();

			var db = new Connections(true);
			var requestCache = db.GetDatatable("SELECT * FROM instance_requests WHERE 1=2");
			requestCache.TableName = "instance_requests";
			foreach (var rows in requestsCopy) {
				var newRow = requestCache.NewRow();
				foreach (var (key, value) in rows.Value) {
					newRow[key] = value;
				}

				requestCache.Rows.Add(newRow);
			}

			db.ExecuteFromDataTable(requestCache);
		}

		public static string LogRestRequest(string instance, ActionExecutingContext context,
			AuthenticatedSession session, int networkDelay, string method, DateTime startTime) {
			var itemId = session?.item_id ?? 0;

			var r = new Dictionary<string, object>();
			var g = Guid.NewGuid().ToString();
			r.Add("instance", instance);
			r.Add("path", Modi.Left(context.HttpContext.Request.Path.Value, 254));
			r.Add("user_item_id", itemId);
			r.Add("occurence", startTime);
			r.Add("networkMS", networkDelay);
			r.Add("method", method);
			_requests.TryAdd(g, r);

			return g;
		}

		public static void LogRestRequestUpdate(string rowGuid, DateTime startTime) {
			if (!_requests.ContainsKey(rowGuid)) return;
			var dr = _requests[rowGuid];
			dr?.Add("durationMS", Conv.ToInt(DateTime.UtcNow.Subtract(startTime).TotalMilliseconds));
			if (_requests.Count >= 1000) FlushRequests();
		}

		private const int CleanThreshold = 1000000;

		public static void CleanLogs() {
			//Compress Instance Requests
			var db = new Connections(true);
			if (Conv.ToInt(db.ExecuteScalar("SELECT COUNT(*) FROM instance_requests WHERE compressed <> 1", null)) >
			    CleanThreshold) {
				const string sql = " SELECT  " +
				                   "       instance, " +
				                   "       path,  " +
				                   "       MAX(occurence) AS occurence,  " +
				                   "       user_item_id, " +
				                   "       AVG(durationMS) AS durationMS, " +
				                   "       AVG(networkMS) AS networkMS, " +
				                   "       method " +
				                   " INTO #temp " +
				                   " FROM ( " +
				                   "       SELECT " +
				                   "               instance, " +
				                   "               path,  " +
				                   "               DATEADD(dd, DATEDIFF(dd, 0, occurence), 0) AS group_occurence, " +
				                   "               occurence,  " +
				                   "               user_item_id, " +
				                   "               cast(durationMS as bigint) AS durationMS, " +
				                   "               cast(networkMS as bigint) AS networkMS, " +
				                   "               method " +
				                   "       FROM instance_requests " +
				                   " ) iq " +
				                   " GROUP BY instance, path, group_occurence, user_item_id, method " +
				                   " TRUNCATE TABLE instance_requests " +
				                   " INSERT INTO instance_requests (instance, path, occurence, user_item_id,durationMS,networkMS,method,compressed)  " +
				                   " SELECT instance, path, occurence, user_item_id,durationMS,networkMS,method,1 FROM #temp";
				Diag.LogToConsole("Compressing Instance Requests");
				db.ExecuteDeleteQuery(sql, null);
			}

			//Clean other tables
			db.ExecuteDeleteQuery("DELETE FROM instance_emails WHERE sent_on < DATEADD(month, -2, GETUTCDATE())", null);
			db.ExecuteDeleteQuery("DELETE FROM instance_logs WHERE occurence < DATEADD(month, -2, GETUTCDATE())", null);
			db.ExecuteDeleteQuery("DELETE FROM archive_allocation_durations WHERE archive_type = 2", null);
		}

		public static int LogPingRequest() {
			var db = new Connections(true);

			var dbParameters = new List<SqlParameter> {
				SetDbParam(SqlDbType.NVarChar, "instance", ""),
				SetDbParam(SqlDbType.NVarChar, "path", "ping"),
				SetDbParam(SqlDbType.Int, "user_item_id", 0),
				SetDbParam(SqlDbType.Int, "networkMS", 0),
				SetDbParam(SqlDbType.NVarChar, "method", "")
			};
			return db.ExecuteInsertQuery(
				"INSERT INTO instance_requests (instance,path,user_item_id,occurence, networkms, method) VALUES (@instance,@path,@user_item_id,getutcdate(),@networkMS,@method)"
				,
				dbParameters);
		}


		public static int Log(string instance, string message, int userItemId,
			LogSeverities severity = LogSeverities.Message, LogCategories category = LogCategories.System,
			int background_log_id = 0, int file_id = 0) {
			LogToConsole(message, severity, instance);
			return LogToDb(instance, message, category, severity, null, userItemId, LogTypes.ServerMessage, "",
				background_log_id, file_id);
		}

		public static int LogClient(string instance, string message, string stack, int userItemId,
			LogCategories category = LogCategories.System, LogTypes logtype = LogTypes.ClientAngular,
			bool internaloop = false) {
			LogToConsole("User " + userItemId + " produced a client error: " + message, LogSeverities.Error, instance);
			return internaloop == false
				? LogToDbFinal(instance, message, stack, category, LogSeverities.Error, userItemId, logtype)
				: 0;
		}

		public static int Log(string instance, Exception ex, string additionalMessage, int userItemId,
			LogCategories category = LogCategories.System,
			LogTypes logtype = LogTypes.ClientAngular,
			bool internaloop = false) {
			LogToConsole(
				"User: " + userItemId + ", Error: " + ex.Message + ", Stack Trace: " + CleanStackTrace(ex.StackTrace),
				LogSeverities.Error,
				instance);

			return internaloop == false
				? LogToDb(instance, "", category, LogSeverities.Error, ex, userItemId, logtype, additionalMessage)
				: 0;
		}

		public static int Log(string instance, string message, string stackTrace, string additionalMessage, int userItemId,
			LogCategories category = LogCategories.System,
			LogTypes logtype = LogTypes.ClientAngular,
			bool internaloop = false) {
			LogToConsole(
				"User " + userItemId + ", Error: " + message + ", Stack Trace: " + CleanStackTrace(stackTrace),
				LogSeverities.Error,
				instance);

			message = message + " " + additionalMessage;
			return LogToDbFinal(instance, message, stackTrace, category, LogSeverities.Error, userItemId, logtype);
		}

		public static int Log(string instance, Exception ex, int userItemId,
			LogCategories category = LogCategories.System,
			LogTypes logtype = LogTypes.ClientAngular,
			bool internaloop = false) {
			LogToConsole(
				"User " + userItemId + ", Error: " + ex.Message + ", Stack Trace: " + CleanStackTrace(ex.StackTrace),
				LogSeverities.Error,
				instance);

			return internaloop == false
				? LogToDb(instance, "", category, LogSeverities.Error, ex, userItemId, logtype)
				: 0;
		}

		public class ApplicationVersion {
			public string ApplicationName;
			public string ReleaseDate;
			public string VersionNumber;
		}

		public class ServerDetails {
			public string instance;
			public string name;
			public string tokenKey;
			public string url;
		}
	}

	public class RuntimeModel {
		public List<RuntimeModelDBRow> DBInfo = new List<RuntimeModelDBRow>();
		public Dictionary<string, int> DBRequests = new Dictionary<string, int>();
		public List<RuntimeModelDBRow> DBRows = new List<RuntimeModelDBRow>();
		public Dictionary<string, RuntimeModelOccurence> Occurence = new Dictionary<string, RuntimeModelOccurence>();
		public RuntimeInformation runtime = new RuntimeInformation();

		public Dictionary<string, object> GetModel(string instance, int CurrentUserItemId, HttpContext context,
			AuthenticatedSession currentSession, int tabNumber) {
			var result = new Dictionary<string, object>();
			var s = DateTime.Now;

			//var allConfigurations = new Configurations(instance, _authenticatedSession);
			//var clientConfigurations = allConfigurations.GetClientConfigurations();
			var userGroup = new UserGroupsData(instance, "user", currentSession);
			var userGroupDict = userGroup.GetUserGroups(CurrentUserItemId);
			var userGroups = userGroupDict.Keys.ToList();


			string runTimeInfoWidgetConfSql =
				"SELECT params FROM instance_menu WHERE default_state='widget.runTimeInfoWidget' AND params like '%availabilityItemId%'";
			var db = new Connections();
			var instanceMenuConfTable = db.GetDatatable(runTimeInfoWidgetConfSql);
			JObject confObj = null;
			bool showAvailability = false;
			bool showUserLicensing = false;
			bool showPerformance = false;
			bool showEvents = false;
			bool showAdmin = false;

			if (instanceMenuConfTable.Rows.Count > 0) {
				confObj = JObject.Parse(Conv.ToStr(instanceMenuConfTable.Rows[0]["params"]));
				var availabilityGroups = Conv.ToIntArray(confObj["availabilityItemId"]);
				var userLicensingGroups = Conv.ToIntArray(confObj["userLicensingItemId"]);
				var performanceGroups = Conv.ToIntArray(confObj["performanceItemId"]);
				var eventLogGroups = Conv.ToIntArray(confObj["eventLogItemId"]);
				var adminGroups = Conv.ToIntArray(confObj["adminItemId"]);

				foreach (int groupId in availabilityGroups) {
					if (userGroups.Contains(groupId)) {
						showAvailability = true;
					}
				}

				foreach (int groupId in userLicensingGroups) {
					if (userGroups.Contains(groupId)) {
						showUserLicensing = true;
					}
				}

				foreach (int groupId in performanceGroups) {
					if (userGroups.Contains(groupId)) {
						showPerformance = true;
					}
				}

				foreach (int groupId in eventLogGroups) {
					if (userGroups.Contains(groupId)) {
						showEvents = true;
					}
				}

				foreach (int groupId in adminGroups) {
					if (userGroups.Contains(groupId)) {
						showAdmin = true;
					}
				}
			}

			if (showAvailability && tabNumber == 0) {
				var Uptime = new interfaces.UptimeMonitor(instance, currentSession);
				DateTime? monitorStartTime = Uptime.GetMonitorStart(context);

				AddOccurence();
				if (monitorStartTime != null) {
					result.Add("UptimeData", Uptime.GetUptimeData(context));
				}

				var instanceChartData = GetInstanceChartData(instance);
				if (instanceChartData != null) {
					result.Add("InstanceChartData", instanceChartData);
					result.Add("InstanceChartTime", DateTime.Now);
				} else {
					result.Add("InstanceChartData", "");
					result.Add("InstanceChartTime", DateTime.Now);
				}

				result.Add("Occurence", Occurence);
				result.Add("MonitorStartTime", monitorStartTime);
			}


			if (showUserLicensing && tabNumber == 1) {
				var licenseUserData = new Dictionary<string, object>();
				//var UserGroupActivityData = new Dictionary<string, object>();

				DateTime nowTime = DateTime.Now;

				var UserGroupActivities = new UserGroupActivities(instance, currentSession);

				for (var i = -12; i < 1; i++) {
					DateTime startTime = nowTime.AddMonths(i);
					//UserGroupActivityData.Add(startTime.Year + "-" + startTime.Month, UserGroupActivities.GetGroupActivity(startTime.Month, startTime.Year));
					licenseUserData.Add(startTime.Year + "-" + startTime.Month,
						UserGroupActivities.GetLicensesUserCounts(startTime.Month, startTime.Year));
				}

				//result.Add("UserGroupActivityData", UserGroupActivityData);
				result.Add("MonthlyLicenseUsers", licenseUserData);
			}

			if (showPerformance && tabNumber == 2) {
				AddDB();
				result.Add("DBDetails", DBInfo);
				result.Add("DBRows", DBRows);
				result.Add("DBRequests", DBRequests);
			}

			if (showEvents && tabNumber == 3) {
				result.Add("Logs", GetTop20Logs(instance));
			}

			if (showAdmin && tabNumber == 4) {
				var domains = new List<string>();
				foreach (string domain in Config.Domains) {
					if (Conv.ToBool(Config.GetDomainValue(domain, "azureadauthentication", "enabled"))) {
						domains.Add(domain);
					}
				}

				result.Add("Domains", domains);
				result.Add("AzureAdLogs", GetAzureAdLogs(domains));
				result.Add("OnlineSessions", ConnectedUsers.GetConnectedUserCount());
			}

			return result;
		}

		private void AddDB() {
			var info = runtime.GetDatabaseInformation();
			foreach (DataRow r in info.Rows) {
				var o = new RuntimeModelDBRow();
				o.Name = Conv.ToStr(r["type"]);
				o.TotalMB = Conv.ToInt(r["TotalSpaceMB"]);
				o.Rows = Conv.ToInt(r["RowCounts"]);
				DBInfo.Add(o);
			}

			var rows = runtime.GetDatabaseRows();
			foreach (DataRow r in rows.Rows) {
				var o = new RuntimeModelDBRow();
				o.Name = Conv.ToStr(r["tablename"]);
				o.TotalMB = Conv.ToInt(r["TotalSpaceMB"]);
				o.Rows = Conv.ToInt(r["RowCounts"]);
				DBRows.Add(o);
			}
		}

		private void AddOccurence() {
			var info = runtime.GetDurationInformation();
			foreach (DataRow row in info.Rows) {
				var start = Util.StartOfWeek(DateTime.UtcNow);
				var occurence_data = new RuntimeModelOccurence(Conv.ToInt(row["occurence"]),
					Conv.ToInt(row["avarageNetwork"]),
					Conv.ToInt(row["avarageDuration"]), Conv.ToDateTime(row["occurenceDate"]));
				Occurence.Add("day_" + Conv.ToInt(row["occurence"]), occurence_data);
			}
		}

		private List<Dictionary<string, object>> GetInstanceChartData(string instance) {
			string sql = "";
			//Today
			sql += "SELECT CAST((SELECT last_name + ' ' + first_name FROM _" + instance +
			       "_user WHERE item_id=user_item_id) AS nvarchar(511))  AS name , COUNT(instance_request_id) AS count, 'today' AS category FROM instance_requests WHERE occurence BETWEEN Convert(date, getdate()) AND GETDATE() GROUP BY user_item_id ";
			//Last 30
			sql += " UNION ALL SELECT CAST((SELECT last_name + ' ' + first_name FROM _" + instance +
			       "_user WHERE item_id=user_item_id) AS nvarchar(511)) AS name, COUNT(instance_request_id) AS count, 'last_30' AS category FROM instance_requests WHERE occurence BETWEEN DATEADD(day, -30, Convert(date, getdate())) AND Convert(date, getdate()) GROUP BY user_item_id ";
			//Last 365
			sql += " UNION ALL SELECT CAST((SELECT last_name + ' ' + first_name FROM _" + instance +
			       "_user WHERE item_id=user_item_id) AS nvarchar(511)) AS name, COUNT(instance_request_id) AS count, 'last_365' AS category FROM instance_requests WHERE occurence BETWEEN DATEADD(day, -365, Convert(date, getdate())) AND Convert(date, getdate()) GROUP BY user_item_id ";

			// 8 weeks including this

			var ci = System.Threading.Thread.CurrentThread.CurrentCulture;
			ci.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
			var fdow = ci.DateTimeFormat.FirstDayOfWeek;

			DateTime today = DateTime.Now;
			DateTime firstDayOfCurrentWeek = today.AddDays(-(DateTime.Today.DayOfWeek - fdow)).Date;

			for (int i = 0; i < 8; i++) {
				sql += " UNION ALL SELECT CAST((SELECT last_name + ' ' + first_name FROM _" + instance +
				       "_user WHERE item_id=user_item_id) AS nvarchar(511)) AS name, COUNT(instance_request_id) AS count, 'week_" +
				       (8 - i) + "' AS category FROM instance_requests WHERE occurence BETWEEN '" +
				       firstDayOfCurrentWeek.AddDays(i * -7).ToString("yyyy-MM-dd HH:mm:ss.fff") + "' AND '" +
				       firstDayOfCurrentWeek.AddDays(i * -7 + 7).ToString("yyyy-MM-dd HH:mm:ss.fff") +
				       "' GROUP BY user_item_id ";
			}

			var db = new Connections();
			return db.GetDatatableDictionary(sql);
		}

		public int GetOnlineSessions() {
			return ConnectedUsers.GetConnectedUserCount();
		}

		private List<Dictionary<string, object>> GetAzureAdLogs(List<string> domains) {
			if (domains.Count > 0) {
				string sql = "";
				if (domains.Count == 1) {
					sql = "SELECT TOP 1 0 AS type,CAST('" + domains[0] +
					      "' AS varchar(max)) AS domain, message,occurence  INTO #temp0 FROM instance_logs WHERE message LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
					      domains[0] + "%' order by instance_log_id DESC";
					sql += " INSERT INTO #temp0 SELECT TOP 1 1 AS type,'" + domains[0] +
					       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync%'  AND message NOT LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
					       domains[0] + "%' order by instance_log_id DESC";
					sql += " SELECT * FROM #temp0";
				} else {
					int count = 0;
					foreach (string domain in domains) {
						if (count == 0) {
							sql = "SELECT TOP 1 0 AS type,CAST('" + domain +
							      "' AS varchar(max)) AS domain, message,occurence  INTO #temp0 FROM instance_logs WHERE message LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							      domain + "%' order by instance_log_id DESC";
							sql += " INSERT INTO #temp0 SELECT TOP 1 1 AS type,'" + domain +
							       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync%'  AND message NOT LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							       domain + "%' order by instance_log_id DESC";
						} else {
							sql += " INSERT INTO #temp0 SELECT TOP 1 0 AS type,'" + domain +
							       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							       domain + "%' order by instance_log_id DESC";
							sql += " INSERT INTO #temp0 SELECT TOP 1 1 AS type,'" + domain +
							       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync%'  AND message NOT LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							       domain + "%' order by instance_log_id DESC";
						}

						count += 1;
					}

					sql += " SELECT * FROM #temp0";
				}

				var db = new Connections();
				return db.GetDatatableDictionary(sql);
			} else {
				return null;
			}
		}

		private List<Dictionary<string, object>> GetTop20Logs(string instance) {
			var db = new Connections();
			return db.GetDatatableDictionary(
				"SELECT TOP (20) instance_log_id,message,stack,occurence,last_name,first_name,login FROM instance_logs LEFT JOIN _" +
				instance + "_user ON _" + instance +
				"_user.item_id=instance_logs.user_item_id ORDER BY instance_log_id DESC");
		}

		public class RuntimeModelLoginPair {
			public string Name;
			public int TotalLogins;
			public int UniqueLogins;

			public RuntimeModelLoginPair(int u, int t, string s) {
				UniqueLogins = u;
				TotalLogins = t;
				Name = s;
			}
		}

		public class RuntimeModelOccurence {
			public int avarageNetwork;
			public int avarageDuration;
			public int occurence;
			public DateTime? occurenceDate;

			public RuntimeModelOccurence(int occurenceV, int avarageNetworkV, int avarageDurationV,
				DateTime? occurenceDateV) {
				occurence = occurenceV;
				avarageNetwork = avarageNetworkV;
				avarageDuration = avarageDurationV;
				occurenceDate = occurenceDateV;
			}
		}

		public class RuntimeModelDBRow {
			public string Name;
			public int Rows;
			public int TotalMB;
		}
	}

	public class RuntimeInformation {
		public DataTable GetRequestInformation(int numberofdays) {
			var sql =
				"SELECT COUNT(*) AS cnt, YEAR(occurence) AS year, MONTH(occurence) AS month, DAY(occurence) AS day FROM instance_requests "
				+
				"WHERE occurence > DATEADD(day,@days,getutcdate()) " +
				"GROUP BY YEAR(occurence), MONTH(occurence), DAY(occurence) " +
				"ORDER BY YEAR(occurence), MONTH(occurence), DAY(occurence) ASC";
			var dbParameters = new List<SqlParameter>();
			dbParameters.Add(new SqlParameter {
				ParameterName = "days",
				SqlDbType = SqlDbType.Int,
				Value = numberofdays * -1
			});
			var db = new Connections();
			return db.GetDatatable(sql, dbParameters);
		}

		public DataTable GetDatabaseInformation() {
			var sql = "SELECT " +
			          "t.NAME AS TableName, " +
			          "SUM(p.rows) AS RowCounts, " +
			          "(SUM(a.total_pages) * 8) / 1024 AS TotalSpaceMB " +
			          "INTO #Temp FROM " +
			          "sys.tables t " +
			          "INNER JOIN sys.indexes i ON t.OBJECT_ID = i.object_id " +
			          "INNER JOIN sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id " +
			          "INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id " +
			          "WHERE " +
			          "t.NAME NOT LIKE 'dt%' AND " +
			          "i.OBJECT_ID > 255 AND " +
			          "i.index_id <= 1 " +
			          "GROUP BY t.NAME, i.object_id, i.index_id, i.name " +
			          "SELECT 'InstanceOnline' AS type, SUM(TotalSpaceMB) AS TotalSpaceMB, SUM(RowCounts) AS RowCounts FROM #Temp WHERE LEFT(TableName,1) = '_' AND TableName NOT LIKE 'archive_%' "
			          +
			          "UNION " +
			          "SELECT 'InstanceHistory' AS type, SUM(TotalSpaceMB) AS TotalSpaceMB, SUM(RowCounts) AS RowCounts FROM #Temp WHERE LEFT(tablename,9) = 'archive__' "
			          +
			          "UNION " +
			          "SELECT 'SystemOnline' AS type, SUM(TotalSpaceMB) AS TotalSpaceMB, SUM(RowCounts) AS RowCounts FROM #Temp WHERE LEFT(TableName,1) <> '_' AND TableName NOT LIKE 'archive_%' "
			          +
			          "UNION " +
			          "SELECT 'SystemHistory' AS type, SUM(TotalSpaceMB) AS TotalSpaceMB, SUM(RowCounts) AS RowCounts FROM #Temp WHERE LEFT(TableName,8) = 'archive_' AND LEFT(tablename,9) <> 'archive__'"
				;
			var db = new Connections();
			return db.GetDatatable(sql);
		}

		public DataTable GetDatabaseRows() {
			var sql = "SELECT " +
			          "t.NAME AS TableName, " +
			          "SUM(p.rows) AS RowCounts, " +
			          "(SUM(a.total_pages) * 8) / 1024 AS TotalSpaceMB " +
			          "INTO #Temp FROM " +
			          "sys.tables t " +
			          "INNER JOIN sys.indexes i ON t.OBJECT_ID = i.object_id " +
			          "INNER JOIN sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id " +
			          "INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id " +
			          "WHERE " +
			          "t.NAME NOT LIKE 'dt%' AND " +
			          "i.OBJECT_ID > 255 AND " +
			          "i.index_id <= 1 " +
			          "GROUP BY t.NAME, i.object_id, i.index_id, i.name " +
			          "SELECT TOP 6 tablename, totalspacemb, rowcounts FROM #temp ORDER BY rowcounts DESC";
			var db = new Connections();
			return db.GetDatatable(sql);
		}

		public DataTable GetLoginInformation(int year, int week) {
			var sql = "SELECT COUNT(user_item_id) AS unique_logins, SUM(logins) AS total_logins, 1 AS success FROM (" +
			          "SELECT user_item_id, COUNT(occurence) AS logins FROM instance_logs " +
			          "WHERE log_type = 1 AND log_category = 1 AND log_severity = 0 AND " +
			          "YEAR(occurence) = @year AND DATEPART(ISO_WEEK,occurence) = @week " +
			          "GROUP BY user_item_id" +
			          ") iq " +
			          "UNION ALL " +
			          "SELECT COUNT(user_item_id) AS unique_logins, SUM(logins) AS total_logins, 0 AS success FROM (" +
			          "SELECT user_item_id, COUNT(occurence) AS logins FROM instance_logs " +
			          "WHERE log_type = 1 AND log_category = 1 AND log_severity = 1 AND " +
			          "YEAR(occurence) = @year AND DATEPART(ISO_WEEK,occurence) = @week " +
			          "GROUP BY user_item_id" +
			          ") iq";
			var db = new Connections();
			var dbParameters = new List<SqlParameter>();
			dbParameters.Add(new SqlParameter {
				ParameterName = "year",
				SqlDbType = SqlDbType.Int,
				Value = year
			});
			dbParameters.Add(new SqlParameter {
				ParameterName = "week",
				SqlDbType = SqlDbType.Int,
				Value = week
			});
			return db.GetDatatable(sql, dbParameters);
		}

		public DataTable GetDurationInformation() {
			var sql =
				" SELECT Dateadd(dd, Datediff(dd, 0, occurence), 0) AS occurenceDate, Avg(Cast(networkms AS FLOAT)) AS avarageNetwork , Avg(Cast(durationms AS FLOAT)) AS avarageDuration INTO #temp FROM instance_requests WHERE  durationms > 0 AND Cast(occurence AS DATE) >= Cast(Dateadd(day, -7, Getdate()) AS DATE) AND path!='/core/ClientInformation' GROUP  BY Day(occurence),Dateadd(dd, Datediff(dd, 0, occurence), 0)  " +
				" select Day(occurence) AS occurence, DATEADD(dd, DATEDIFF(dd, 0, occurence), 0) AS occurenceDate, t.avarageDuration,t.avarageNetwork from instance_requests  " +
				" LEFT JOIN #temp t ON t.occurenceDate=Dateadd(dd, Datediff(dd, 0, instance_requests.occurence), 0) " +
				"where durationms > 0 AND CAST(occurence AS date) >= CAST(DATEADD(day, -7, GETDATE()) AS date) " +
				"group by Day(occurence), DATEADD(dd, DATEDIFF(dd, 0, occurence), 0),t.avarageDuration,t.avarageNetwork";
			var db = new Connections();
			var dbParameters = new List<SqlParameter>();
			return db.GetDatatable(sql, dbParameters);
		}
	}

	public class ConfigurationReport {
		public DateTime GenerationTime;
		public List<ConfigurationReportColumn> SuspiciousColumns;
		public List<ConfigurationReportPortfolio> Portfolios;
		public Dictionary<string, int> Adhoc;

		public void GenerateReport(AuthenticatedSession session) {
			Portfolios = new List<ConfigurationReportPortfolio>();
			SuspiciousColumns = new List<ConfigurationReportColumn>();
			Adhoc = new Dictionary<string, int>();

			var conditionProblems = 0;
			var columnProblems = 0;

			//Get Suspicious Item Columns
			var db = new Connections();
			var dbParameters = new List<SqlParameter>();

			if (Cache.GetSuspiciousItemColumns().Keys.Count > 0) {
				foreach (DataRow sc in db.GetDatatable("SELECT * FROM item_columns WHERE item_column_id IN (" + Modi.JoinListWithComma(Cache.GetSuspiciousItemColumns().Keys.ToList()) + ")",
						         dbParameters)
					         .Rows) {
					SuspiciousColumns.Add(new ConfigurationReportColumn(sc) {
						Exception = new Exception(Cache.GetSuspiciousItemColumns()[Conv.ToInt(sc["item_column_id"])])
					});
				}
			}

			var colsSql = "SELECT pp.process_portfolio_id, pp.instance, pp.process, pp.variable AS portfolio_name, ic.item_column_id, ic.name AS name, ic.variable, ic.data_type," +
			              "STUFF((SELECT ',' + eqc.name FROM item_column_equations eq LEFT JOIN item_columns eqc ON eqc.item_column_id = eq.item_column_id WHERE eq.parent_item_column_id = ic.item_column_id FOR XML PATH('')),1,1,'') AS related_item_columns " +
			              "FROM process_portfolios pp " +
			              "INNER JOIN process_portfolio_columns ppc on ppc.process_portfolio_id = pp.process_portfolio_id " +
			              "INNER JOIN item_columns ic ON ic.item_column_id = ppc.item_column_id " +
			              "WHERE data_type IN (16,20) " +
			              "ORDER BY process, portfolio_name";

			var cols = db.GetDatatable(colsSql);
			foreach (DataRow ppi in db.GetDatatable("SELECT pp.process_portfolio_id, pp.instance, pp.process, pp.variable FROM process_portfolios pp LEFT JOIN instance_unions iu ON iu.process = pp.process WHERE iu.process IS NULL").Rows) {
				var portfolio = new ConfigurationReportPortfolio(Conv.ToStr(ppi["process"]), Conv.ToInt(ppi["process_portfolio_id"]), Conv.ToStr(ppi["variable"]));

				dbParameters = new List<SqlParameter> {
					new SqlParameter {
						ParameterName = "@instance",
						SqlDbType = SqlDbType.VarChar,
						Value = session.instance
					},
					new SqlParameter {
						ParameterName = "@process",
						SqlDbType = SqlDbType.VarChar,
						Value = portfolio.Process
					},
					new SqlParameter {
						ParameterName = "@signedInUserId",
						SqlDbType = SqlDbType.Int,
						Value = session.item_id
					}
				};

				//Check conditions of this portfolio
				try {
					var conditions =
						new EntityRepository(session.instance, portfolio.Process, session).GetWhere(portfolio.PortfolioId);

					var sql = "SELECT TOP 1 item_id FROM _" + session.instance + "_" + portfolio.Process + " pf WHERE 1=1 AND " + conditions;
					db.GetDatatable(sql, dbParameters);
				} catch (Exception e) {
					portfolio.ConditionTestResult = false;
					portfolio.ConditionException = e;
					conditionProblems += 1;

					//Diag.Log(session.instance, "Issue Report: Problem with conditions for portfolio " + portfolio.PortfolioId + ".", 0, Diag.LogSeverities.Warning, Diag.LogCategories.System);
				}

				//Check columns
				foreach (var col in cols.Select("process_portfolio_id = " + portfolio.PortfolioId)) {
					var resultRow = new ConfigurationReportColumn(col);
					portfolio.PortfolioName = Conv.ToStr(col["portfolio_name"]);

					//Get Portfolio Column and it's related equation columns
					var getCols = new List<string>() { resultRow.Name };
					if (Conv.ToStr(col["related_item_columns"]) != "") getCols.AddRange(Conv.ToStr(col["related_item_columns"]).Split(","));

					//Get columns
					foreach (DataRow s in db.GetDatatable(
						         "SELECT ic.name FROM process_portfolio_columns ppc LEFT JOIN item_columns ic ON ic.item_column_id = ppc.item_column_id WHERE process_portfolio_id = " +
						         portfolio.PortfolioId + " AND data_type NOT IN (20,16,60,65)").Rows) {
						var c = Conv.ToStr(s["name"]);
						if (!getCols.Contains(c)) getCols.Add(c);
					}

					//Get Portfolio SQL
					var sql = new ItemColumns(Conv.ToStr(col["instance"]), resultRow.Process, session).GetItemSql(cols: getCols);
					sql = "SELECT TOP 1 " + sql.Substring(6);

					//Check
					try {
						dbParameters = new List<SqlParameter> {
							new SqlParameter {
								ParameterName = "@instance",
								SqlDbType = SqlDbType.VarChar,
								Value = session.instance
							},
							new SqlParameter {
								ParameterName = "@process",
								SqlDbType = SqlDbType.VarChar,
								Value = portfolio.Process
							},
							new SqlParameter {
								ParameterName = "@signedInUserId",
								SqlDbType = SqlDbType.Int,
								Value = session.item_id
							}
						};
						db.GetDatatable(sql, dbParameters);
					} catch (Exception e) {
						resultRow.Exception = e;
						portfolio.PortfolioColumns.Add(resultRow);
						columnProblems += 1;
					}
				}

				if (portfolio.PortfolioColumns.Count > 0 || portfolio.ConditionTestResult == false) Portfolios.Add(portfolio);
			}

			//Adhoc Tests
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_DATEFORMAT", "SELECT COUNT(*) FROM _" + session.instance + "_user WHERE ISNULL(date_format,'') = ''");
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_FIRSTNAME", "SELECT COUNT(*) FROM _" + session.instance + "_user WHERE ISNULL(first_name,'') = ''");
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_LASTNAME", "SELECT COUNT(*) FROM _" + session.instance + "_user WHERE ISNULL(last_name,'') = ''");
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_LOGIN", "SELECT COUNT(*) FROM _" + session.instance + "_user WHERE ISNULL(login,'') = ''");
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_LOCALE", "SELECT COUNT(*) FROM _" + session.instance + "_user WHERE ISNULL(locale_id,'') = ''");
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_CALENDAR",
				"SELECT COUNT(*) FROM _" + session.instance + "_user u LEFT JOIN calendars c ON c.item_id = u.item_id WHERE c.calendar_id IS NULL");
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_COST_CENTER",
				"SELECT COUNT(*) FROM _" + session.instance +
				"_user u WHERE (SELECT COUNT(*) FROM item_data_process_selections idps WHERE idps.item_column_id = (SELECT item_column_id FROM item_columns WHERE process = 'user' AND name = 'cost_center') AND idps.item_id = u.item_id) = 0");
			AdhocTest(Adhoc, "ADMIN_CONFIGURATION_ADHOC_COST_CENTER_OWNER",
				"SELECT COUNT(*) FROM _" + session.instance +
				"_list_cost_center u WHERE (SELECT COUNT(*) FROM item_data_process_selections idps WHERE idps.item_column_id = (SELECT item_column_id FROM item_columns WHERE process = 'list_cost_center' AND name = 'owner') AND idps.item_id = u.item_id) = 0");


			//Finnish
			GenerationTime = DateTime.Now;
			Console.WriteLine("Configuration Report Generated at " + GenerationTime + " in which " + SuspiciousColumns.Count + " suspicious columns where found and " + columnProblems +
			                  " columns in various portfolios didn't work. There were condition problems in " + conditionProblems + " portfolios. There were " + Adhoc.Count +
			                  " problems in various checks.");

			Diag.LastConfigurationReport = this;
		}

		private void AdhocTest(Dictionary<string, int> adHocs, string variable, string sql) {
			var db = new Connections();
			var result = Conv.ToInt(db.ExecuteScalar(sql));
			if (result > 0) adHocs.Add(variable, result);
		}
	}


	public class ConfigurationReportPortfolio {
		public int PortfolioId;
		public string Process;
		public string PortfolioName;
		public bool ConditionTestResult = true;
		public Exception ConditionException;
		public List<ConfigurationReportColumn> PortfolioColumns = new List<ConfigurationReportColumn>();

		public ConfigurationReportPortfolio(string process, int portfolioId, string portfolioName) {
			this.Process = process;
			this.PortfolioId = portfolioId;
			this.PortfolioName = portfolioName;
		}
	}

	public class ConfigurationReportColumn {
		public int ItemColumnId;
		public string Process;
		public string Name;
		public string Variable;
		public Exception Exception;

		public ConfigurationReportColumn(DataRow dr) {
			ItemColumnId = Conv.ToInt(dr["item_column_id"]);
			Process = Conv.ToStr(dr["process"]);
			Name = Conv.ToStr(dr["name"]);
			Variable = Conv.ToStr(dr["variable"]);
		}
	}

	public class InstanceLog {
		public string Message { get; set; }
		public int InstanceLogId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int UserItemId { get; set; }
		public string Stack { get; set; }
		public DateTime? Occurence { get; set; }
		public string Login { get; set; }
		public int LogType { get; set; }
		public int LogCategory { get; set; }
		public int LogSeverity { get; set; }
		public int MaxRows { get; set; }

		public InstanceLog() { }

		public InstanceLog(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0)
				InstanceLogId = (int)row["instance_log_id"];
			Message = (string)row["message"];
			FirstName = (string)row["first_name"];
			LastName = (string)row["last_name"];
			UserItemId = (int)row["user_item_id"];
			Stack = (string)row["stack"];
			Occurence = (DateTime)row["occurence"];
			Login = (string)row["login"];
			LogType = (int)row["log_type"];
			LogCategory = (int)row["log_category"];
			LogSeverity = (int)row["log_severity"];
		}
	}

	public class InstanceLogs {
		public List<InstanceLog> GetInstanceLogs(string instance, InstanceLogFilter ilf) {
			var minRowNumber = ilf.MinRow;
			var maxRowNumber = ilf.MaxRow;

			var logId = Conv.ToInt(ilf.IdSearch);

			var filterSql = "";
			var orderByText = "";

			var user_join = false;

			if (ilf.SearchText.Length > 0 || logId != 0) {
				filterSql = "WHERE (message LIKE '%" + Conv.Escape(ilf.SearchText) + "%' OR login LIKE '%" +
				            Conv.Escape(ilf.SearchText) + "%')";
				user_join = true;
			} else {
				filterSql = "WHERE 69 = 69";
			}

			if (logId != 0) {
				filterSql = filterSql + " AND instance_log_id = " + logId;
			}

			if (ilf.LogType != null) {
				filterSql = filterSql + " AND log_type = " + ilf.LogType;
			}

			if (ilf.UserItemId != null) {
				filterSql = filterSql + " AND user_item_id = " + ilf.UserItemId;
				user_join = true;
			}

			if (ilf.LogCategory != null) {
				filterSql = filterSql + " AND log_category = " + ilf.LogCategory;
			}

			if (ilf.LogSeverity != null) {
				filterSql = filterSql + " AND log_severity = " + ilf.LogSeverity;
			}

			if (ilf.StartOccurence != null && ilf.EndOccurence != null) {
				filterSql = filterSql + " AND occurence BETWEEN '" + ilf.StartOccurence + "' AND '" + ilf.EndOccurence +
				            "'";
			}

			var initialOrder = "DESC";
			var defaultOrderBy = "instance_log_id";

			if (ilf.OrderBy != null) {
				orderByText = "ORDER BY " + Conv.ToSql(ilf.OrderBy[0]) + " " + Conv.ToSql(ilf.OrderBy[1]);
				initialOrder = ilf.OrderBy[1];
				defaultOrderBy = ilf.OrderBy[0];
			}

			var joinType = ilf.UserItemId != null ? "INNER" : "LEFT";

			var result = new List<InstanceLog>();
			var db = new Connections(true);

			Diag.ResetPerformanceTimer();
			var maxNumber = Conv.ToInt(db.ExecuteScalar(
				"SELECT COUNT(*) FROM instance_logs ig " + (user_join
					? joinType + " JOIN _" + instance +
					  "_user u ON u.item_id = ig.user_item_id "
					: "") + filterSql));

			var sql =
				"SELECT row_num, instance_log_id, message, stack, occurence, last_name, first_name, user_item_id, login, log_type, log_category, log_severity " +
				"FROM (" +
				"   SELECT ROW_NUMBER() OVER ( " +
				"	   ORDER BY " + defaultOrderBy + " " + initialOrder +
				"	) row_num, instance_log_id, message, stack, occurence, last_name, first_name, user_item_id, login, log_type, log_category, log_severity " +
				"	FROM instance_logs ig " +
				"   " + joinType + " JOIN _" + instance + "_user u ON u.item_id = ig.user_item_id " + filterSql +
				") iq WHERE row_num > " + minRowNumber + " AND row_num <= " + maxRowNumber + " " + orderByText;

			foreach (DataRow r in db.GetDatatable(sql).Rows) {
				var il = new InstanceLog();
				il.Message = Conv.ToStr(r["message"]);
				il.Stack = Conv.ToStr(r["stack"]);
				il.InstanceLogId = Conv.ToInt(r["instance_log_id"]);
				il.FirstName = Conv.ToStr(r["first_name"]);
				il.LastName = Conv.ToStr(r["last_name"]);
				il.UserItemId = Conv.ToInt(r["user_item_id"]);
				il.Login = Conv.ToStr(r["login"]);
				il.Occurence = Conv.ToDateTime(r["occurence"]);
				il.LogType = Conv.ToInt(r["log_type"]);
				il.MaxRows = maxNumber;
				il.LogCategory = Conv.ToInt(r["log_category"]);
				il.LogSeverity = Conv.ToInt(r["log_severity"]);
				result.Add(il);
			}

			return result;
		}
	}

	public class InstanceLogFilter {
		public string SearchText { get; set; }

		public string IdSearch { get; set; }
		public int? LogType { get; set; }
		public int MinRow { get; set; }
		public int? UserItemId { get; set; }
		public int MaxRow { get; set; }
		public DateTime? StartOccurence { get; set; }
		public DateTime? EndOccurence { get; set; }

		public int? LogCategory { get; set; }
		public int? LogSeverity { get; set; }
		public List<string> OrderBy { get; set; }
		public InstanceLogFilter() { }
	}

	//Used for UserLoginReport
	public class UserGroupActivities : ConnectionBase {
		public UserGroupActivities(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) { }

		//User licensing
		public System.Collections.Generic.List<System.Collections.Generic.Dictionary<string, object>> GetGroupActivity(
			int month, int year) {
			SetDBParam(SqlDbType.Date, "@ed", new DateTime(year, month, 1).AddMonths(1));
			SetDBParam(SqlDbType.Date, "@sd", new DateTime(year, month, 1));

			StringBuilder sql = new StringBuilder();
			sql.Append("DECLARE @licenceGroupListFieldId INT ");
			sql.Append("DECLARE @licenceRequiredtFieldId INT ");

			sql.Append("SET @licenceGroupListFieldId = (SELECT TOP 1 item_column_id \n");
			sql.Append("                                FROM   item_columns \n");
			sql.Append("                                WHERE  variable = 'IC_USER_GROUP_LICENCE_CATEGORIES') ");

			sql.Append("SET @licenceRequiredtFieldId = (SELECT TOP 1 item_column_id \n");
			sql.Append("                                FROM   item_columns \n");
			sql.Append("                                WHERE  variable = 'IC_USER_LICENCE_REQUIRED') ");

			sql.Append("SELECT user_item_id, \n");
			sql.Append("       usergroup_item_id, \n");
			sql.Append("       usergroup_name, \n");
			sql.Append("       licence_group, \n");
			sql.Append("	   licence_category, \n");
			sql.Append("	   licence_cost, \n");
			sql.Append("       userInfo.first_name, \n");
			sql.Append("       userInfo.last_name, \n");
			sql.Append("       licence_required_code \n");
			sql.Append("FROM   (SELECT fq.usergroup_item_id, \n");
			sql.Append("               user_item_id, \n");
			sql.Append("               licence_category, \n");
			sql.Append("			   licence_cost, \n");
			sql.Append("			   licence_group, \n");
			sql.Append("               licence_required_code \n");
			sql.Append("        FROM   (SELECT oq.item_id \n");
			sql.Append("                       AS \n");
			sql.Append("                               usergroup_item_id, \n");
			sql.Append("                       oq.usergroup_name, \n");
			sql.Append("                       licenceInfo.list_item AS licence_category, \n");
			sql.Append("					   licenceInfo.cost AS licence_cost, \n");
			sql.Append("					   licenceInfo.licence_group AS licence_group, \n");
			sql.Append("                       (SELECT lr.code \n");
			sql.Append("                        FROM   item_data_process_selections lr_selection \n");
			sql.Append("                               LEFT JOIN _" + instance + "_list_user_licence_required lr \n");
			sql.Append("                                      ON \n");
			sql.Append("                               lr_selection.selected_item_id = lr.item_id \n");
			sql.Append("                        WHERE  lr_selection.item_id = idps.selected_item_id \n");
			sql.Append("                               AND \n");
			sql.Append("                       lr_selection.item_column_id = @licenceRequiredtFieldId) \n");
			sql.Append("                       AS \n");
			sql.Append("                       licence_required_code, \n");
			sql.Append("                       idps.selected_item_id \n");
			sql.Append("                       AS \n");
			sql.Append("                               user_item_id, \n");
			sql.Append("                       Max(idps.archive_start) \n");
			sql.Append("                       AS \n");
			sql.Append("                               idps_start, \n");
			sql.Append("                       (SELECT TOP 1 archive_type \n");
			sql.Append("                        FROM   (SELECT item_id, \n");
			sql.Append("                                       item_column_id, \n");
			sql.Append("                                       selected_item_id, \n");
			sql.Append("                                       Getutcdate() AS archive_start, \n");
			sql.Append("                                       1            AS archive_type, \n");
			sql.Append("                                       9999999999   AS archive_id \n");
			sql.Append("                                FROM   item_data_process_selections \n");
			sql.Append("                                WHERE  selected_item_id = idps.selected_item_id \n");
			sql.Append("                                       AND item_id = oq.item_id \n");
			sql.Append("                                       AND item_column_id = (SELECT \n");
			sql.Append("                                           item_column_id \n");
			sql.Append("                                                             FROM   item_columns \n");
			sql.Append("                                                             WHERE \n");
			sql.Append("                                           process = 'user_group' \n");
			sql.Append("                                           AND NAME = 'user_id') \n");
			sql.Append("                                UNION \n");
			sql.Append("                                SELECT item_id, \n");
			sql.Append("                                       item_column_id, \n");
			sql.Append("                                       selected_item_id, \n");
			sql.Append("                                       archive_start, \n");
			sql.Append("                                       archive_type, \n");
			sql.Append("                                       archive_id \n");
			sql.Append("                                FROM   archive_item_data_process_selections \n");
			sql.Append("                                WHERE  selected_item_id = idps.selected_item_id \n");
			sql.Append("                                       AND archive_start = \n");
			sql.Append("                                           Max(idps.archive_start) \n");
			sql.Append("                                       AND item_id = oq.item_id \n");
			sql.Append("                                       AND item_column_id = (SELECT \n");
			sql.Append("                                           item_column_id \n");
			sql.Append("                                                             FROM   item_columns \n");
			sql.Append("                                                             WHERE \n");
			sql.Append("                                           process = 'user_group' \n");
			sql.Append("                                           AND NAME = 'user_id')) \n");
			sql.Append("                               idps2 \n");
			sql.Append("                        ORDER  BY archive_id DESC) \n");
			sql.Append("                       AS \n");
			sql.Append("                               deleted \n");
			sql.Append("                FROM   (SELECT item_id, \n");
			sql.Append("                               usergroup_name, \n");
			sql.Append("                               Min(date) AS group_created \n");
			sql.Append("                        FROM   (SELECT item_id, \n");
			sql.Append("                                       usergroup_name, \n");
			sql.Append("                                       Getdate() AS date \n");
			sql.Append("                                FROM   _" + instance + "_user_group \n");
			sql.Append("                                UNION ALL \n");
			sql.Append("                                SELECT item_id, \n");
			sql.Append("                                       usergroup_name, \n");
			sql.Append("                                       archive_start AS date \n");
			sql.Append("                                FROM   archive__" + instance + "_user_group) iq \n");
			sql.Append("                        GROUP  BY item_id, \n");
			sql.Append("                                  usergroup_name) oq \n");
			sql.Append("                       LEFT JOIN (SELECT item_id, \n");
			sql.Append("                                         item_column_id, \n");
			sql.Append("                                         selected_item_id, \n");
			sql.Append("                                         Getutcdate() AS archive_start \n");
			sql.Append("                                  FROM   item_data_process_selections \n");
			sql.Append("                                  UNION \n");
			sql.Append("                                  SELECT item_id, \n");
			sql.Append("                                         item_column_id, \n");
			sql.Append("                                         selected_item_id, \n");
			sql.Append("                                         archive_start \n");
			sql.Append("                                  FROM   archive_item_data_process_selections) \n");
			sql.Append("                                 idps \n");
			sql.Append("                              ON idps.item_id = oq.item_id \n");
			sql.Append("                                 AND idps.item_column_id = (SELECT \n");
			sql.Append("                                     item_column_id \n");
			sql.Append("                                                            FROM   item_columns \n");
			sql.Append("                                                            WHERE \n");
			sql.Append("                                     process = 'user_group' \n");
			sql.Append("                                     AND NAME = 'user_id') \n");
			sql.Append("                -- AND idps.archive_start <= @sd  \n");
			sql.Append(
				"				LEFT JOIN item_data_process_selections li_selection ON li_selection.item_id=oq.item_id AND li_selection.item_column_id = @licenceGroupListFieldId \n");
			sql.Append("				LEFT JOIN _" + instance +
			           "_list_user_licences licenceInfo ON li_selection.selected_item_id = licenceInfo.item_id \n");
			sql.Append("                WHERE  group_created < @ed \n");
			sql.Append("                GROUP  BY oq.item_id, \n");
			sql.Append("                          usergroup_name, \n");
			sql.Append("                          idps.selected_item_id, \n");
			sql.Append("						  licenceInfo.list_item, \n");
			sql.Append("						  licenceInfo.licence_group, \n");
			sql.Append("						  licenceInfo.cost) fq \n");
			sql.Append("        WHERE  NOT ( fq.idps_start < @sd \n");
			sql.Append("                     AND deleted = 2 ) \n");
			sql.Append("        GROUP  BY usergroup_item_id, \n");
			sql.Append("                  user_item_id, \n");
			sql.Append("                 licence_category, \n");
			sql.Append("				 licence_cost, \n");
			sql.Append("				 licence_group, \n");
			sql.Append("                  licence_required_code) uq \n");
			sql.Append("       LEFT JOIN _" + instance + "_user_group \n");
			sql.Append("              ON _" + instance + "_user_group.item_id = uq.usergroup_item_id \n");
			sql.Append("       LEFT JOIN _" + instance + "_user userInfo \n");
			sql.Append("              ON userInfo.item_id = user_item_id \n");
			sql.Append("--where usergroup_name='Personnel'  \n");
			sql.Append("WHERE  licence_category IS NOT NULL \n");
			sql.Append("       AND licence_required_code = 1 \n");
			sql.Append("       AND ( ISNULL(Cast((SELECT active FROM Get__" + instance +
			           "_user(@ed) WHERE item_id= \n");
			sql.Append("             user_item_id \n");
			sql.Append("                 ) \n");
			sql.Append("                 AS INT),0) \n");
			sql.Append("             + ISNULL(Cast((SELECT Max(active) FROM archive__" + instance +
			           "_user WHERE item_id= \n");
			sql.Append("             user_item_id \n");
			sql.Append("                 AND archive_start>=@sd AND archive_end < @ed) AS \n");
			sql.Append("             INT),0) ) \n");
			sql.Append("           > 0");


			return db.GetDatatableDictionary(sql.ToString(), DBParameters);
		}


		public object GetGroupLicensedUsers(int month, int year, bool group_by_user = false) {
			//Get list of users who have license during this years this moth, raw unfiltered data (no licence_group filtering etc)
			//Console.WriteLine("GetGroupLicensedUsers " + month + "." + year + ": " + DateTime.Now.ToString() + " start ");

			string span_end_date = new DateTime(year, month, 1).AddMonths(1).ToString("yyyy-MM-dd");
			string span_start_date = new DateTime(year, month, 1).ToString("yyyy-MM-dd");

			string ic_user_license_required = Conv.ToStr(db.ExecuteScalar(
				"SELECT item_column_id FROM item_columns WHERE instance = '" + instance +
				"' AND process = 'user' AND name = 'licence_required'", null));
			string val_license_required_yes =
				Conv.ToStr(db.ExecuteScalar(
					"SELECT item_id FROM _" + instance + "_list_user_licence_required WHERE code = '1'", null));

			string ic_ug_license_categories = Conv.ToStr(db.ExecuteScalar(
				"SELECT item_column_id FROM item_columns WHERE instance = '" + instance +
				"' AND process = 'user_group' AND name = 'licence_categories'", null));
			string ic_ug_users = Conv.ToStr(db.ExecuteScalar(
				"SELECT item_column_id FROM item_columns WHERE instance = '" + instance +
				"' AND process = 'user_group' AND name = 'user_id'", null));


			// get users that are/were active and have license requirement any moment during period.
			//Console.WriteLine("--users:" + DateTime.Now.ToString());
			StringBuilder sql_str = new StringBuilder();
			sql_str.AppendLine(
				"SELECT user_item_id = active_users_during_span.item_id, user_last_name = MAX(active_users_during_span.last_name), user_first_name = MAX(active_users_during_span.first_name), user_login = MAX(active_users_during_span.login), user_license = MAX(license_yes)");
			sql_str.AppendLine("FROM (");
			sql_str.AppendLine("	SELECT users_during_span.*");
			sql_str.AppendLine("		   ,license_yes = CASE WHEN user_license_required_during_span.selected_item_id = " +
			                   val_license_required_yes + " THEN 1 ELSE 0 END ");
			sql_str.AppendLine("	FROM (");
			sql_str.AppendLine(
				"		SELECT item_id,last_name,first_name,login,active,archive_start,NULL as archive_end FROM _" +
				instance + "_user WHERE archive_start < '" + span_end_date + "'");
			sql_str.AppendLine("		UNION ALL");
			sql_str.AppendLine(
				"		SELECT item_id,last_name,first_name,login,active,archive_start,archive_end FROM archive__" +
				instance + "_user WHERE NOT (archive_end < '" + span_start_date + "' OR archive_start > '" +
				span_end_date + "')");
			sql_str.AppendLine("	) users_during_span ");
			sql_str.AppendLine("	LEFT JOIN (");
			sql_str.AppendLine(
				"		SELECT idps.item_id,idps.selected_item_id,idps.archive_start,NULL as archive_end FROM item_data_process_selections idps WHERE idps.item_column_id = " +
				ic_user_license_required + " AND archive_start < '" + span_end_date + "'");
			sql_str.AppendLine("		UNION ALL");
			sql_str.AppendLine(
				"		SELECT adps.item_id,adps.selected_item_id,adps.archive_start,adps.archive_end FROM archive_item_data_process_selections adps WHERE adps.item_column_id = " +
				ic_user_license_required + " AND NOT (archive_end < '" + span_start_date + "' OR archive_start > '" +
				span_end_date + "')");
			sql_str.AppendLine(
				"	) user_license_required_during_span ON user_license_required_during_span.item_id = users_during_span.item_id");
			sql_str.AppendLine(
				"	WHERE users_during_span.active = 1 AND user_license_required_during_span.selected_item_id = " +
				val_license_required_yes + "");
			sql_str.AppendLine(") active_users_during_span");
			sql_str.AppendLine("GROUP BY active_users_during_span.item_id");
			var licensed_users_during_span = db.GetDatatableDictionary(sql_str.ToString(), null);
			//Console.WriteLine(sql_str.ToString());

			// get groups and group users during period
			//Console.WriteLine("--groups:" + DateTime.Now.ToString());
			sql_str = new StringBuilder();
			sql_str.AppendLine(
				"SELECT group_id = groups_during_span.item_id, group_name = MAX(groups_during_span.usergroup_name), all_group = MAX(groups_during_span.all_group)");
			sql_str.AppendLine("	,groups_users_during_span = ");
			sql_str.AppendLine(
				"		STUFF((SELECT ',' + CAST(selected_item_id as nvarchar) FROM ("); // get any group user that has been in the group during period.
			sql_str.AppendLine(
				"			SELECT idps.item_id,idps.selected_item_id,idps.archive_start,NULL as archive_end FROM item_data_process_selections idps WHERE idps.item_column_id = " +
				ic_ug_users + " AND archive_start < '" + span_end_date + "'");
			sql_str.AppendLine("			UNION ALL");
			sql_str.AppendLine(
				"			SELECT adps.item_id,adps.selected_item_id,adps.archive_start,adps.archive_end FROM archive_item_data_process_selections adps WHERE adps.item_column_id = " +
				ic_ug_users + " AND NOT (archive_end < '" + span_start_date + "' OR archive_start > '" + span_end_date +
				"')");
			sql_str.AppendLine(
				"		) group_users_during_span WHERE group_users_during_span.item_id = groups_during_span.item_id  FOR XML PATH('')),1,1,'')");
			//sql_str.AppendLine("	,groups_licenses_during_span = ");
			//sql_str.AppendLine("		STUFF((SELECT ',' + CAST(selected_item_id as nvarchar) FROM ("); // any license that was linked to group during period, and was active more than 24h or is active.
			//sql_str.AppendLine("			SELECT idps.item_id,idps.selected_item_id,idps.archive_start,NULL as archive_end FROM item_data_process_selections idps WHERE idps.item_column_id = " + ic_ug_license_categories + " AND archive_start < '" + span_end_date + "'");
			//sql_str.AppendLine("			UNION ALL");
			//sql_str.AppendLine("			SELECT adps.item_id,adps.selected_item_id,adps.archive_start,adps.archive_end FROM archive_item_data_process_selections adps WHERE adps.item_column_id = " + ic_ug_license_categories + " AND NOT (archive_end < '" + span_start_date + "' OR archive_start > '" + span_end_date + "') AND DATEDIFF(HOUR, archive_start, archive_end) > 24");
			//sql_str.AppendLine("		) group_license_during_span WHERE group_license_during_span.item_id = groups_during_span.item_id  FOR XML PATH('')),1,1,'')");
			sql_str.AppendLine("	,group_licenses_now = ");
			sql_str.AppendLine("		STUFF((SELECT ',' + CAST(selected_item_id as nvarchar) FROM (");
			sql_str.AppendLine(
				"			SELECT idps.item_id,idps.selected_item_id,idps.archive_start,NULL as archive_end FROM item_data_process_selections idps WHERE idps.item_column_id = " +
				ic_ug_license_categories + "");
			sql_str.AppendLine(
				"		) group_license_during_span WHERE group_license_during_span.item_id = groups_during_span.item_id  FOR XML PATH('')),1,1,'')");
			sql_str.AppendLine("FROM (");
			sql_str.AppendLine(
				"	SELECT item_id,usergroup_name,all_group = CAST(all_group AS INT),archive_start,NULL as archive_end FROM _" +
				instance + "_user_group WHERE archive_start < '" + span_end_date + "'");
			sql_str.AppendLine("	UNION ALL");
			sql_str.AppendLine(
				"	SELECT item_id,usergroup_name,all_group = CAST(all_group AS INT),archive_start,archive_end FROM archive__" +
				instance + "_user_group WHERE NOT (archive_end < '" + span_start_date + "' OR archive_start > '" +
				span_end_date + "')  AND DATEDIFF(HOUR, archive_start, archive_end) > 0");
			sql_str.AppendLine(") groups_during_span ");
			sql_str.AppendLine("GROUP BY groups_during_span.item_id ");
			var active_groups_during_span = db.GetDatatableDictionary(sql_str.ToString(), null);
			//Console.WriteLine(sql_str.ToString());

			// get license data during period
			// specified to not use past licensing, always current
			//Console.WriteLine("--licenses:" + DateTime.Now.ToString());
			//sql_str = new StringBuilder();
			//sql_str.AppendLine("SELECT license_item_id = item_id, license_name = MAX(list_item), license_cost_during = MAX(cost), license_group_during = MAX(licence_group)");
			//sql_str.AppendLine("	FROM (");
			//sql_str.AppendLine("		SELECT item_id,list_item,cost = CAST(cost AS FLOAT),licence_group,archive_start,NULL as archive_end FROM _" + instance + "_list_user_licences WHERE archive_start < '" + span_end_date + "'");
			//sql_str.AppendLine("		UNION ALL");
			//sql_str.AppendLine("		SELECT item_id,list_item,cost = CAST(cost AS FLOAT),licence_group,archive_start,archive_end FROM archive__" + instance + "_list_user_licences WHERE NOT (archive_end < '" + span_start_date + "' OR archive_start > '" + span_end_date + "') AND DATEDIFF(HOUR, archive_start, archive_end) > 0");
			//sql_str.AppendLine("	) licenses_during_span");
			//sql_str.AppendLine("	GROUP BY item_id");
			//var active_licenses_during_span = db.GetDatatableDictionary(sql_str.ToString(), null);
			//Console.WriteLine(sql_str.ToString());

			// get licenses now
			//Console.WriteLine("--licenses:" + DateTime.Now.ToString());
			sql_str = new StringBuilder();
			sql_str.AppendLine(
				"SELECT license_item_id = item_id, license_name = list_item, license_cost_during = cost, license_group_during = licence_group FROM _" +
				instance + "_list_user_licences"); // = CAST(cost AS FLOAT)
			var active_licenses_now = db.GetDatatableDictionary(sql_str.ToString(), null);
			//Console.WriteLine(sql_str.ToString());

			List<Dictionary<string, object>> compile_data = new List<Dictionary<string, object>>();
			Dictionary<int, List<Dictionary<string, object>>> compile_user_data =
				new Dictionary<int, List<Dictionary<string, object>>>();
			foreach (var grp in active_groups_during_span) {
				Dictionary<string, object> groups_license = null;
				//if (grp["groups_licenses_during_span"] == DBNull.Value)
				//	continue;
				//foreach (var license_row in active_licenses_during_span) // specified to not use past licensing, always current
				//	if (Conv.ToStr(grp["groups_licenses_during_span"]).Split(",").Contains(Conv.ToStr(license_row["license_item_id"])))
				//		if (groups_license == null || Conv.ToDouble(groups_license["license_cost_during"]) < Conv.ToDouble(license_row["license_cost_during"]))
				//			groups_license = license_row;
				if (groups_license == null)
					foreach (var license_row in active_licenses_now) // specified to use current licensing
						if (Conv.ToStr(grp["group_licenses_now"]).Split(",")
						    .Contains(Conv.ToStr(license_row["license_item_id"])))
							if (groups_license == null || Conv.ToDouble(groups_license["license_cost_during"]) <
							    Conv.ToDouble(license_row["license_cost_during"]))
								groups_license = license_row;
				if (groups_license == null)
					continue;

				List<Dictionary<string, object>> groups_userlist = new List<Dictionary<string, object>>();
				if (Conv.ToInt(grp["all_group"]) == 1)
					groups_userlist = licensed_users_during_span;
				else
					foreach (var usr in licensed_users_during_span)
						if (Conv.ToStr(grp["groups_users_during_span"]).Split(",")
							    .Contains(Conv.ToStr(usr["user_item_id"])) && groups_userlist.Contains(usr) == false)
							groups_userlist.Add(usr);

				foreach (var usr in groups_userlist) {
					var group_user_data = new Dictionary<string, object>();

					group_user_data.Add("user_item_id", usr["user_item_id"]);
					group_user_data.Add("usergroup_item_id", grp["group_id"]);
					group_user_data.Add("usergroup_name", grp["group_name"]);

					group_user_data.Add("licence_id", groups_license["license_item_id"]);
					group_user_data.Add("licence_category", groups_license["license_name"]);
					group_user_data.Add("licence_group", groups_license["license_group_during"]);
					group_user_data.Add("licence_cost", groups_license["license_cost_during"]);

					group_user_data.Add("last_name", usr["user_last_name"]);
					group_user_data.Add("first_name", usr["user_first_name"]);
					group_user_data.Add("licence_required_code", usr["user_license"]);


					if (group_by_user) {
						if (compile_user_data.ContainsKey(Conv.ToInt(usr["user_item_id"])) == false)
							compile_user_data.Add(Conv.ToInt(usr["user_item_id"]),
								new List<Dictionary<string, object>>());
						compile_user_data[Conv.ToInt(usr["user_item_id"])].Add(group_user_data);
					} else
						compile_data.Add(group_user_data);
				}
			}

			//Console.WriteLine("GetGroupLicensedUsers " + month + "." + year + ": " + DateTime.Now.ToString() + " done ");

			if (group_by_user)
				return compile_user_data;
			return compile_data;
		}

		public List<Dictionary<string, object>> GetLicensesUserCounts(int month, int year) {
			return GetLicensesUserCounts(
				(Dictionary<int, List<Dictionary<string, object>>>)GetGroupLicensedUsers(month, year, true));
		}

		private List<Dictionary<string, object>> GetLicensesUserCounts(
			Dictionary<int, List<Dictionary<string, object>>> userLicenseData) {
			//Get licences user counts in numbers
			// grouped licenses (where licence_group is not empty) select the max by cost where the group is same
			// ungrouped licenses (where licence_group is empty) are always on for the user

			//Console.WriteLine("GetLicensesUserCounts: " + DateTime.Now.ToString() + " start ");
			Dictionary<int, Dictionary<string, object>> license_numbers =
				new Dictionary<int, Dictionary<string, object>>();

			foreach (var usr_id in userLicenseData.Keys) {
				List<Dictionary<string, object>> users_licenses = userLicenseData[usr_id];
				List<Dictionary<string, object>> users_grouped_licenses =
					users_licenses.Where(i => Conv.ToStr(i["licence_group"]).Length > 0)
						.ToList(); // grouped licenses (where licence_group is not empty) select the max by cost where the group is same
				List<Dictionary<string, object>> users_ungrouped_licenses =
					users_licenses.Where(i => Conv.ToStr(i["licence_group"]).Length == 0)
						.ToList(); // ungrouped licenses (where licence_group is empty) are always on for the user

				List<Dictionary<string, object>> user_relevant_licenses = users_ungrouped_licenses;
				if (users_grouped_licenses.Count > 0) {
					List<string> spent_group = new List<string>();
					foreach (var grpd in users_grouped_licenses) {
						if (spent_group.Contains(Conv.ToStr(grpd["licence_group"])) == false) {
							user_relevant_licenses.Add(users_grouped_licenses
								.Where(i => Conv.ToStr(i["licence_group"]) == Conv.ToStr(grpd["licence_group"]))
								.Aggregate((i1, i2) =>
									Conv.ToDouble(i1["licence_cost"]) > Conv.ToDouble(i2["licence_cost"]) ? i1 : i2));
							spent_group.Add(Conv.ToStr(grpd["licence_group"]));
						}
					}
				}


				foreach (var user_relevant_license in user_relevant_licenses) {
					if (license_numbers.ContainsKey(Conv.ToInt(user_relevant_license["licence_id"])) == false) {
						Dictionary<string, object> initial_data = new Dictionary<string, object>();
						initial_data["license_id"] = Conv.ToInt(user_relevant_license["licence_id"]);
						initial_data["license_name"] = Conv.ToStr(user_relevant_license["licence_category"]);
						initial_data["license_price"] = Conv.ToStr(user_relevant_license["licence_cost"]);
						initial_data["user_count"] = 0;
						initial_data["user_ids"] = "";

						license_numbers.Add(Conv.ToInt(user_relevant_license["licence_id"]), initial_data);
					}

					license_numbers[Conv.ToInt(user_relevant_license["licence_id"])]["user_count"] =
						Conv.ToInt(license_numbers[Conv.ToInt(user_relevant_license["licence_id"])]["user_count"]) + 1;
					if (Conv.ToInt(license_numbers[Conv.ToInt(user_relevant_license["licence_id"])]["user_count"]) <
					    500)
						license_numbers[Conv.ToInt(user_relevant_license["licence_id"])]["user_ids"] =
							(Conv.ToStr(license_numbers[Conv.ToInt(user_relevant_license["licence_id"])]["user_ids"]) +
							 "," + usr_id).Trim(',');
					else
						license_numbers[Conv.ToInt(user_relevant_license["licence_id"])]["user_ids"] = "";
				}
			}

			List<Dictionary<string, object>> ret_nums = new List<Dictionary<string, object>>();
			foreach (var k in license_numbers)
				ret_nums.Add(k.Value);

			//Console.WriteLine("GetLicensesUserCounts: " + DateTime.Now.ToString() + " end ");
			return ret_nums;
		}
	}
}