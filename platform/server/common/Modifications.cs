﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Keto5.x.platform.server.common {
	public static class Modi {
		public static string Left(string value, int maxLength) {
			value = Conv.ToStr(value);
			maxLength = Math.Abs(maxLength);
			return value.Length <= maxLength ? value : value.Substring(0, maxLength);
		}

		public static string Right(this string value, int maxLength) {
			value = Conv.ToStr(value);
			if (value == "") {
				return string.Empty;
			}

			maxLength = Math.Abs(maxLength);
			return value.Length <= maxLength ? value : value.Substring(value.Length - maxLength);
		}

		public static string RemoveSpecialCharacters(string str) {
			var sb = new StringBuilder();
			foreach (var c in str) {
				if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_' || c == ' ') {
					sb.Append(c);
				}
			}

			return sb.ToString();
		}

		public static string StripHtml(string find, string replace = "") { 
			return Regex.Replace(find, @"(<[^>]*>|(\s{2,}))", replace);
		}

		public static string JoinListWithComma<T>(List<T> items, bool? parenthesis = false) {
			var joinedItems = string.Join(", ", items);
			if (parenthesis == true) {
				return "(" + joinedItems + ")";
			}

			return joinedItems;
		}

		public static string ReplaceLastOccurrence(string source, string find, string replace) {
			var place = source.LastIndexOf(find, StringComparison.Ordinal);
			return place == -1 ? source : source.Remove(place, find.Length).Insert(place, replace);
		}
	}
}