﻿using Microsoft.AspNetCore.Http;

namespace Keto5.x.platform.server.common {
	public class State {
		private readonly HttpContext Context;
		private readonly string instance;

		public State(string instance, HttpContext Context = null) {
			this.Context = Context;
			this.instance = instance;
		}

		public void toSession(string key, string value) {
			Context.Session.SetString(instance + "_" + key, value);
		}

		public string fromSession(string key) {
			return Context.Session.GetString(instance + "_" + key);
		}
	}
}