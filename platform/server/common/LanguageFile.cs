﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.common {
	
	public class LanguageFile {
		public IFileProvider FileProvider;

		public LanguageFile() {
			FileProvider = new FileProvider();
		}

		//Plain JSON file
		public Dictionary<string, string> GetLanguageKeyCollectionFromJSON(string language_file, string locale_id) {
			var result = new Dictionary<string, string>();
			var filepath = Path.Combine(Config.AppPath, "platform", "resources",
				language_file + "_" + locale_id.Replace("-", "_") + ".json");
			
			//Read file
			var fileContent = FileProvider.GetFileContent(filepath);
			if (string.IsNullOrEmpty(fileContent)) {
				throw new Exception("Translation file is empty");
			}
			var file = JObject.Parse(fileContent);
			//Extract language variables
			foreach (var item in file) {
				var key = item.Key.ToUpper();
				if (result.ContainsKey(key)) throw new Exception("Language file (json) has to identical keys: " + key);
				result.Add(key, item.Value.ToString());
			}
			 
			// Override variables with possible framework-translations
			var filepathForFrameworkTranslations = Path.Combine(Config.AppPath, "customer", "resources",
				language_file + "_" + locale_id.Replace("-", "_") + ".json");
			var frameworkfile = FileProvider.GetFileContent(filepathForFrameworkTranslations, false);
			if (string.IsNullOrEmpty(frameworkfile)) {
				return result;
			}
			var frameworkTranslations = JObject.Parse(frameworkfile);
			
			//Extract language variables
			foreach (var item in frameworkTranslations) {
				var key = item.Key.ToUpper();
				if (result.ContainsKey(key)) {
					result[key] = item.Value.ToString();
				} else {
					result.Add(key, item.Value.ToString());
				}
			}
			
			return result;
		}
	}
}