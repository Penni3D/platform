﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DrawingCore.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace Keto5.x.platform.server.common {
	public static class Conv {
		public static int ToInt(object integer) {
			switch (integer) {
				case int i:
					return i;
				case string s: {
					int.TryParse(s, out var result);
					return result;
				}
				case double d:
					return Convert.ToInt32(d);
				case float f:
					return Convert.ToInt32(f);
				case decimal integer1:
					return Convert.ToInt32(integer1);
			}

			if (integer == null || integer == DBNull.Value) return 0;
			if (!(integer is bool)) return Convert.ToInt32(integer.ToString());
			if ((bool) integer) return 1;
			return 0;
		}

		public static long ToLong(object longInteger) {
			switch (longInteger) {
				case long l:
					return l;
				case int i:
					return i;
				case string s: {
						long.TryParse(s, out var result);
						return result;
					}
				case double d:
					return Convert.ToInt64(d);
				case float f:
					return Convert.ToInt64(f);
				case decimal de:
					return Convert.ToInt64(de);
			}

			if (longInteger == null || longInteger == DBNull.Value) return 0;
			if (!(longInteger is bool)) return Convert.ToInt64(longInteger.ToString());
			if ((bool)longInteger) return 1;
			return 0;
		}

		public static bool ToBool(object boolean) {
			switch (boolean) {
				case bool b:
					return b;
				case string s when boolean.ToString()?.ToLower() == "true":
				case int i when i == 1:
					return true;
				case JValue j: {
					return boolean.ToString()?.ToLower() == "true";
				}
			}

			return false;
		}

		public static byte ToByte(object integer) {
			switch (integer) {
				case byte b:
					return b;
				case string s: {
					byte.TryParse(s, out var result);
					return result;
				}
				case double d:
				case float f:
					return Convert.ToByte((double) integer);
			}

			if (integer == null || integer == DBNull.Value) return 0;
			return Convert.ToByte(integer);
		}

		private const NumberStyles Style = NumberStyles.Number;

		public static double ToDouble(object dbl, string cultureKey = null) {
			switch (dbl) {
				case double d:
					return d;
				case string s: {
					double result = 0;
					if (string.IsNullOrEmpty(cultureKey)) {
						double.TryParse(s, out result);
						return result;
					}

					double.TryParse(s, Style, CultureInfo.CreateSpecificCulture(cultureKey), out result);
					return result;
				}
				case int i:
					return Convert.ToDouble(i);
				case long l:
					return Convert.ToDouble(l);
				case float f:
					return Convert.ToDouble(f);
				case decimal dbl1:
					return Convert.ToDouble(dbl1);
				default:
					return 0;
			}
		}

		public static DateTime? ToDateTime(object datetime) {
			switch (datetime) {
				case DateTime time:
					return time;
				case string s: {
					DateTime.TryParse(s, out var result);
					if (result.Year < 1900) return null;
					return result;
				}
				default:
					return null;
			}
		}

		public static DateTime? ToDate(object datetime) {
			switch (datetime) {
				case DateTime time:
					return time.Date;
				case string s: {
					DateTime.TryParse(s, out var result);
					if (result.Year < 1900) return null;
					return result.Date;
				}
				default:
					return null;
			}
		}

		public static string ToStr(object str) {
			switch (str) {
				case null:
					return "";
				case Exception exception:
					return GetExceptionMessage(exception);
			}

			return DBNull.Value.Equals(str) ? "" : str.ToString();
		}

		private static string GetExceptionMessage(Exception ex) {
			var message = ex.Message;
			if (ex.InnerException != null) message += " -> " + GetExceptionMessage(ex.InnerException);
			return message;
		}

		public static string SerializeToString(object serializableClass, bool noReplace = false) {
			var ms = new MemoryStream();
			var reader = new StreamReader(ms);

			var formatter =
				new BinaryFormatter();
			formatter.Serialize(ms, serializableClass);
			ms.Position = 0;

			if (noReplace) return Convert.ToBase64String(ms.GetBuffer());
			return Convert.ToBase64String(ms.GetBuffer()).Replace("/", "�").Replace("=", "�");
		}

		public static object DeserializeToObject(string str, bool noReplace = false) {
			if (string.IsNullOrEmpty(str)) return null;
			var data = Convert.FromBase64String(noReplace
				? str
				: str.Replace("�", "/")
					.Replace("�", "="));
			var formatter = new BinaryFormatter();
			var ms = new MemoryStream(data);
			return formatter.Deserialize(ms);
		}

		public static string ToDelimitedString(IEnumerable<string> list, string delimiter = ",") {
			return string.Join(delimiter, list);
		}

		public static string ToDelimitedString(IEnumerable<int> list, string delimiter = ",") {
			return string.Join(delimiter, list);
		}
		
		public static string ToDelimitedString(JArray list, string delimiter = ",") {
			return string.Join(delimiter, list);
		}

		public static string ToDelimitedString(System.Collections.Specialized.StringCollection list, string delimiter = ",") {
			var a = new List<string>();
			foreach (var s in list) { 
				a.Add(s);
			}
			return ToDelimitedString(a, delimiter);
		}

		public static object IfNullThenDbNull(object val) {
			return val ?? DBNull.Value;
		}


		public static object IfNullOrEmptyThenDbNull(object val) {
			switch (val) {
				case null:
				case "":
					return DBNull.Value;
				default:
					return val;
			}
		}

		public static object IfDbNullThenNull(object val) {
			return val == DBNull.Value ? null : val;
		}

		public static string[] Split(string text, string delimiter) {
			return text.Split(new[] {delimiter}, StringSplitOptions.None);
		}

		public static Dictionary<string, object> DataRowToDictionary(DataRow row) {
			if (row == null) return new Dictionary<string, object>();
			var result = Enumerable.Range(0, row.Table.Columns.Count)
				.ToDictionary(
					i => row.Table.Columns[i].ColumnName,
					i => row[row.Table.Columns[i]]
				);
			return result;
		}

		public static int[] ToIntArray(object arr) {
			if (arr == null) return new int[0];
			if (arr != null && arr.GetType().IsArray && arr.GetType().GetElementType() == typeof(int)) {
				return (int[]) arr;
			}

			if (arr != null && arr.GetType().ToString().Equals("Newtonsoft.Json.Linq.JArray")) {
				var jsonObject = arr.ToString();
				return JsonConvert.DeserializeObject<List<int>>(jsonObject).ToArray();
			}

			switch (arr) {
				case int _:
				case long _:
				case short _:
					return new[] {ToInt(arr)};
				case string s when s.Length > 0:
					return new[] {ToInt(s)};
				default:
					try {
						var ids = (List<int>) arr;
						return ids.ToArray();
					} catch {
						try {
							var ids = (List<string>) arr;
							return ids.Select(s => Convert.ToInt32(s)).ToList().ToArray();
						} catch {
							return new int[0];
						}
					}
			}
		}

		private static string ContainsIllegalChar(string val, bool BreakOnSecurityException = false) {
			if (val == null) return null;
			if (!val.Contains(";") && !val.Contains("--") && !val.Contains("*") && !val.Contains("'")) return val;
			Diag.Log("",
				"Conversion to SQL failed security check: Illegal character encoutered. This check should only be used for table and column names",
				0, Diag.LogSeverities.Sql);
			
			if (BreakOnSecurityException) throw new SecurityException("Conversion to SQL failed security check");
			
			return val.Replace(";", "").Replace("--", "").Replace("*", "").Replace("'", "");
		}

		public static string ToSql(object val, bool BreakOnSecurityException = false) {
			return Escape(ContainsIllegalChar(ToStr(val), BreakOnSecurityException));
		}

		public static string Escape(string val) {
			return val == null ? "" : val.Replace("'", "''").Replace("\\", "\\\\");
		}

		public static string SelectEscape(string val) {
			return val == null ? "" : Regex.Replace(val.Replace("'", "''"), @"[\[\]\*\%]", "[$&]");
		}

		public static List<Dictionary<string, object>> DataTableToDictionary(DataTable dt) {
			var retval = new List<Dictionary<string, object>>();
			foreach (DataRow row in dt.Rows) {
				//Use Linq to convert row into a dictionary
				var result = Enumerable.Range(0, dt.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => dt.Rows[0].Table.Columns[i].ColumnName,
						i => row[dt.Rows[0].Table.Columns[i]]
					);
				retval.Add(result);
			}

			return retval;
		}

		public static List<Dictionary<string, object>> DataRowsToDictionary(DataRow[] dt) {
			return dt.Select(row => Enumerable.Range(0, dt[0].Table.Columns.Count)
					.ToDictionary(i => dt[0].Table.Columns[i].ColumnName, i =>
						row[dt[0].Table.Columns[i]]))
				.ToList();
		}
	}
}