using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Keto5.x.platform.server.common {
	public abstract class BaseServerException : Exception {
		public enum ExceptionType {
			ARGUMENTS,
			UNIQUE,
			PERMISSIONS
		}

		public enum ExceptionSubType { 
			DELETE,
			INSERT,
			UPDATE
		}

		public Dictionary<string, object> Messages = new Dictionary<string, object>();
		public int Status = 400;
		public string TranslationKey;
		public ExceptionType Type;
		public ExceptionSubType SubType;

		protected BaseServerException() { }

		protected BaseServerException(object description) {
			Messages.Add("description", description.ToString());
			Messages.Add("stacktrace", Environment.StackTrace);
		}

		protected BaseServerException(object description, string translation) {
			TranslationKey = translation;
			Messages.Add("description", description.ToString());
			Messages.Add("stacktrace", Environment.StackTrace);
		}

		public string GetDescription() {
			if (Messages.ContainsKey("description")) return Conv.ToStr(Messages["description"]);
			return "";
		}

		public override string StackTrace => Environment.StackTrace;

		public ObjectResult GetObjectResult(bool DeveloperMode = false, string logMessage = "", string logId = "") {
			var d = new Dictionary<string, object>();
			d.Add("Info", Messages);
			Messages["description"] += logMessage;
			Messages["logId"] = logId;
			
			Messages["stacktrace"] = Diag.CleanStackTrace(Conv.ToStr(Messages["stacktrace"]));
			if (DeveloperMode == false) Messages["stacktrace"] = "Not Available";
			d.Add("Type", Type.ToString());
			d.Add("SubType", SubType.ToString());
			d.Add("Variable", TranslationKey);
			d.Add("Data", Data);
			return new ObjectResult(d);
		}
	}

	public class CustomArgumentException : BaseServerException {
		public CustomArgumentException() {
			TranslationKey = "EXCEPTIONS_CUSTOM_ARGUMENT";
			Type = ExceptionType.ARGUMENTS;
		}

		public CustomArgumentException(object description) : base(description) {
			TranslationKey = "EXCEPTIONS_CUSTOM_ARGUMENT";
			Type = ExceptionType.ARGUMENTS;
		}

		public CustomArgumentException(object description, string translation) : base(description, translation) {
			Type = ExceptionType.ARGUMENTS;
		}

		public CustomArgumentException(ModelStateDictionary modelStateDictionary) {
			var modelStateErrors = new Dictionary<string, object>();
			var errors = modelStateDictionary
				.Where(x => x.Value.Errors.Count > 0)
				.Select(x => new {x.Key, x.Value.Errors})
				.ToArray();
			foreach (var error in errors) {
				var errorMessages = error.Errors.Select(x => x.ErrorMessage).ToList();
				modelStateErrors.Add(error.Key, errorMessages);
			}

			Messages.Add("modelstate", modelStateDictionary);
		}
	}

	public class CustomPermissionException : BaseServerException {
		public CustomPermissionException() {
			Type = ExceptionType.PERMISSIONS;
			TranslationKey = "EXCEPTIONS_CUSTOM_PERMISSIONS";
		}

		public CustomPermissionException(string translation, ExceptionSubType exceptionSubType = ExceptionSubType.UPDATE) : base(translation) {
			Type = ExceptionType.PERMISSIONS;
			TranslationKey = "EXCEPTIONS_CUSTOM_PERMISSIONS";
			SubType = exceptionSubType;
		}

		public CustomPermissionException(string translation, string description, ExceptionSubType exceptionSubType = ExceptionSubType.UPDATE) : base(translation, description) {
			Type = ExceptionType.PERMISSIONS;
			TranslationKey = "EXCEPTIONS_CUSTOM_PERMISSIONS";
			SubType = exceptionSubType;
		}
	}

	public sealed class CustomUniqueException : BaseServerException {
		public CustomUniqueException() {
			Type = ExceptionType.UNIQUE;
			TranslationKey = "EXCEPTIONS_CUSTOM_UNIQUE";
		}

		public CustomUniqueException(string description) : base(description) {
			Type = ExceptionType.UNIQUE;
			TranslationKey = "EXCEPTIONS_CUSTOM_UNIQUE";
		}
		
		public CustomUniqueException(string data, bool column) : base(data) {
			Type = ExceptionType.UNIQUE;
			if (column) {
				Data.Add("col", data);
			}
			TranslationKey = "EXCEPTIONS_CUSTOM_UNIQUE";
		}

		public CustomUniqueException(string description, string translation) : base(description, translation) {
			Type = ExceptionType.UNIQUE;
			TranslationKey = "EXCEPTIONS_CUSTOM_UNIQUE";
		}
	}
}