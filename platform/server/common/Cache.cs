﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.Extensions.Caching.Memory;

namespace Keto5.x.platform.server.common {
	public static class Cache {
		private static MemoryCache cache;
		private static Hashtable ProcessItemCache = new Hashtable(); //Stores in which process an item id belongs to
		private static ConcurrentDictionary<string, List<string>> MemorizedGroups = new ConcurrentDictionary<string, List<string>>();
		private static List<string> Log;
		private static Dictionary<int, int> _delegationTable = new Dictionary<int, int>();
		private static Dictionary<int, string> _suspiciousItemColumns = new Dictionary<int, string>();

		public static void AddSuspiciousItemColumn(int itemColumnId, string reason) { 
			_suspiciousItemColumns.Add(itemColumnId, reason);
		}

		public static void RemoveSuspiciousItemColumn(int itemColumnId) {
			_suspiciousItemColumns.Remove(itemColumnId);
		}

		public static bool IsItemColumnSuspicious(int itemColumnId) {
			return _suspiciousItemColumns.ContainsKey(itemColumnId);
		}

		public static Dictionary<int, string> GetSuspiciousItemColumns() {
			return _suspiciousItemColumns;
		}

		public static int SuspiciousItemColumnCount() {
			return _suspiciousItemColumns.Count;
		}

		public static void AddOrUpdateDelegate(int delegatorUser, int delegatingFor) {
			if (delegatorUser == delegatingFor) {
				RemoveDelegate(delegatorUser);
				return;
			}

			if (_delegationTable.ContainsKey(delegatorUser)) {
				_delegationTable[delegatorUser] = delegatingFor;
			} else {
				_delegationTable.Add(delegatorUser, delegatingFor);
			}
		}

		public static void RemoveDelegate(int delegatorUser) {
			if (_delegationTable.ContainsKey(delegatorUser)) {
				_delegationTable.Remove(delegatorUser);
			}
		}

		public static int GetDelegate(int delegatorUser) {
			return _delegationTable.ContainsKey(delegatorUser) ? _delegationTable[delegatorUser] : 0;
		}

		public static string GetItemProcess(int itemId, string instance) {
			//In cache?
			if (ProcessItemCache.ContainsKey(itemId)) return Conv.ToStr(ProcessItemCache[itemId]);

			//Add to cache
			var con = new ConnectionBase(instance, new AuthenticatedSession("", 0, null));
			con.DBParameters.Clear();
			con.SetDBParam(SqlDbType.Int, "itemId", itemId);
			con.SetDBParam(SqlDbType.VarChar, "instance", instance);
			var itemprocess = Conv.ToStr(con.db.ExecuteScalar(
				"SELECT process FROM items WHERE item_id = @itemId AND instance = @instance", con.DBParameters));
			ProcessItemCache.Add(itemId, itemprocess);
			return itemprocess;
		}

		public static void SetItemProcess(int itemId, string process) {
			ProcessItemCache.Add(itemId, process);
		}

		public static bool Initialize() {
			cache = new MemoryCache(new MemoryCacheOptions());
			return true;
		}

		private static bool _log = false;

		public static bool LogCache {
			get => _log;
			set {
				_log = value;
				Log = _log ? new List<string>() : null;
			}
		}

		public static void WriteCacheToConsole() {
			foreach (var entry in Log) {
				try {
					Console.WriteLine(entry + ": " + cache.Get(entry));
				} catch (Exception e) {
					Diag.SupressException(e);
					//Console.WriteLine(entry + ": " + "DELETED");
				}
			}
		}

		public static object Get(string instance, string key) {
			return cache.Get(instance + "_" + key);
		}

		private static void AddToGroup(string group, string key) {
			if (group == null) return;
			if (!MemorizedGroups.ContainsKey(group)) {
				var ng = new List<string>();
				ng.Add(key);
				MemorizedGroups.TryAdd(group, ng);
			} else {
				var og = MemorizedGroups[group];
				if (!og.Contains(key)) og.Add(key);
				MemorizedGroups[group] = og;
			}
		}

		public static object Get(string instance, string group, string key) {
			key = instance + "_" + group + "_" + key;
			return cache.Get(key);
		}

		public static object Get(string instance, string domain, string group, string key) {
			key = instance + "_" + domain + "_" + group + "_" + key;
			return cache.Get(key);
		}

		public static void Get(string instance, string domain, string group, string name, string key) {
			key = instance + "_" + domain + "_" + group + "_" + name + "_" + key;
			cache.Get(key);
		}

		public static void Set(string instance, string group, string key, object value) {
			key = instance + "_" + group + "_" + key;
			AddToGroup(group, key);
			Set(key, value);
		}

		public static void Set(string instance, string domain, string group, string key, object value) {
			key = instance + "_" + domain + "_" + group + "_" + key;
			AddToGroup(group, key);
			Set(key, value);
		}

		public static void Set(string instance, string domain, string group, string name, string key, object value) {
			key = instance + "_" + domain + "_" + group + "_" + name + "_" + key;
			AddToGroup(group, key);
			Set(key, value);
		}

		public static void Set(string instance, string key, object value) {
			key = instance + "_" + key;
			cache.Set(key, value);
			if (_log && Log.Contains(key) == false) Log.Add(key);
		}

		private static void Set(string key, object value) {
			cache.Set(key, value);
			if (_log && Log.Contains(key) == false) Log.Add(key);
		}

		public static void RemoveGroup(string group) {
			if (MemorizedGroups.ContainsKey(group)) {
				foreach (var key in MemorizedGroups[group]) {
					Remove(key);
				}
				var x = MemorizedGroups[group];
				MemorizedGroups.TryRemove(group, out x);
			}
		}

		public static void Remove(string instance, string domain, string group, string key) {
			key = instance + "_" + domain + "_" + group + "_" + key;
			cache.Remove(key);
		}

		public static void Remove(string instance, string domain, string group, string name, string key) {
			key = instance + "_" + domain + "_" + group + "_" + name + "_" + key;
			cache.Remove(key);
		}

		public static void Remove(string instance, string group, string key) {
			key = instance + "_" + group + "_" + key;
			cache.Remove(key);
		}

		public static void Remove(string instance, string key) {
			key = instance + "_" + key;
			cache.Remove(key);
		}

		private static void Remove(string key) {
			cache.Remove(key);
		}
	}
}