﻿using System.IO;

namespace Keto5.x.platform.server.common {
	public interface IFileProvider {
		string GetFileContent(string path, bool require = true);
	}

	public class FileProvider : IFileProvider {
		public string GetFileContent(string filepath, bool require = true) {
			if (new FileInfo(filepath).Exists) return File.ReadAllText(filepath);
			if (require) {
				throw new FileNotFoundException("File could not be found: " + filepath);
			}
			return "";
		}
	}
}