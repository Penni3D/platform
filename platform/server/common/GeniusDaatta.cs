﻿using System;

namespace Keto5.x.platform.server.common {

	//Interface to replace baseclass
	public interface iTheGenius {
		string GetCreatorGF();
	}

	//This was a baseclass but now implements our interface
	public class TheGenius : iTheGenius {
		public string GetCreatorGF() {
			throw new Exception("READING A FILE BUT NO GF FOUND!");
		}
	}

	//This is our amazing class that doesn't inherit baseclass anymore, but using a constructor, accepts an object that implemements the interface and accesses former baseclass methods from it
	public class GeniusDaatta {
		private readonly iTheGenius myGenious;

		//Constructor
		public GeniusDaatta(iTheGenius myGenius) {
			myGenious = myGenius;
		}

		//Method 1
		public string GetCreatorPotency(int potency) {
			return "Aleksi: " + potency * 100;
		}

		//Method 2
		public bool HasCreatorGF() {
			return myGenious.GetCreatorGF() == "Jani";
		}
	}
}