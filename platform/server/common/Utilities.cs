﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Graph;
using Newtonsoft.Json.Linq;
using DayOfWeek = System.DayOfWeek;

namespace Keto5.x.platform.server.common {
	public static class Util {
		public static string CreateToken(int length,
			string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {
			if (length < 0) throw new ArgumentOutOfRangeException(nameof(length), "length cannot be less than zero.");
			if (string.IsNullOrEmpty(allowedChars)) throw new ArgumentException("allowedChars may not be empty.");

			const int byteSize = 0x100;
			var allowedCharSet = new HashSet<char>(allowedChars).ToArray();
			if (byteSize < allowedCharSet.Length)
				throw new ArgumentException(string.Format("allowedChars may contain no more than {0} characters.",
					byteSize));

			// Guid.NewGuid and System.Random are not particularly random. By using a
			// cryptographically-secure random number generator, the caller is always
			// protected, regardless of use.
			using (var rng = new RNGCryptoServiceProvider()) {
				var result = new StringBuilder();
				var buf = new byte[128];
				while (result.Length < length) {
					rng.GetBytes(buf);
					for (var i = 0; i < buf.Length && result.Length < length; ++i) {
						// Divide the byte into allowedCharSet-sized groups. If the
						// random value falls into the last group and the last group is
						// too small to choose from the entire allowedCharSet, ignore
						// the value in order to avoid biasing the result.
						var outOfRangeStart = byteSize - byteSize % allowedCharSet.Length;
						if (outOfRangeStart <= buf[i]) continue;
						result.Append(allowedCharSet[buf[i] % allowedCharSet.Length]);
					}
				}

				return result.ToString();
			}
		}

		public static DateTime StartOfWeek(DateTime d) {
			var diff = (7 + (d.DayOfWeek - DayOfWeek.Monday)) % 7;
			return d.AddDays(-1 * diff).Date;
		}

		public static DateTime EndOfWeek(DateTime d) {
			var monday = StartOfWeek(d);
			return monday.AddDays(6);
		}

		public static DateTime StartOfWeek(int year, int week_number) {
			DateTime d = new DateTime(year, 1, 1);
			if (GetWeekNumber(d) == 1) {
				d = d.AddDays(7 * (week_number - 1));
			}
			else {
				d = d.AddDays(7 * week_number);
			}
			return StartOfWeek(d);
		}

		public static DateTime EndOfWeek(int year, int week_number) {
			DateTime d = new DateTime(year, 1, 1);
			if (GetWeekNumber(d) == 1) {
				d = d.AddDays(7 * (week_number - 1));
			}
			else {
				d = d.AddDays(7 * week_number);
			}
			return EndOfWeek(d);
		}

		public static int FirstWeekOfMonth(int year, int month) {
			DateTime d = new DateTime(year, month, 1);
			if (((int)d.DayOfWeek).In(5, 6, 0)) {
				return GetWeekNumber(d.AddDays(7));
			}
			else {
				return GetWeekNumber(d);
			}
		}

		public static int LastWeekOfMonth(int year, int month) {
			DateTime d = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
			if (((int)d.DayOfWeek).In(1, 2, 3)) {
				return GetWeekNumber(d.AddDays(-7));
			}
			else {
				return GetWeekNumber(d);
			}
		}

		public static int GetWeekNumber(DateTime dtPassed) {
			var ciCurr = CultureInfo.CurrentCulture;
			var weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
			return weekNum;
		}

		public static string FormatNumber(double value, string locale_id) {
			if (locale_id == "fi-FI" || locale_id == "de-DE") {
				return $"{value:#,0.##}".Replace(","," ").Replace(".", ",");
			}
			return $"{value:#,0.##}"; 
		}
		
		public static string FormatNumber(int value, string locale_id) {
			if (locale_id == "fi-FI" || locale_id == "de-DE") {
				return $"{value:#,0}".Replace(","," ");
			}
			return $"{value:#,0}"; 
		}

		public static string FormatDate(DateTime d, string locale_id) {
			if (locale_id == "fi-FI" || locale_id == "de-DE") { 
				return d.ToString("dd.MM.yyyy");
			} else if (locale_id == "en-US") { 
				return d.ToString("MM/dd/yyyy");
			}
			return d.ToString("dd/MM/yyyy");
		}
		
		public static string FormatDateTime(DateTime d, string locale_id) {
			if (locale_id == "fi-FI" || locale_id == "de-DE") { 
				return d.ToString("dd.MM.yyyy hh.mm");
			} else if (locale_id == "en-US") { 
				return d.ToString("MM/dd/yyyy hh.mm");
			}
			return d.ToString("dd/MM/yyyy hh.mm");
		}

		public static bool isEmailValid(string email) {
			try {
				var addr = new System.Net.Mail.MailAddress(email);
				return addr.Address == email;
			} catch {
				return false;
			}
		}

		public static JToken GetItemForFields(JObject jObject, IEnumerable<string> propertyPath) {
			return GetItemForFields(jObject, false, propertyPath);
		}

		public static JToken GetItemForFields(JObject jObject, bool throwIfNotFound, IEnumerable<string> propertyPath) {
			JToken cursor = jObject;
			foreach (var key in propertyPath) {
				if (cursor == null) {
					if (throwIfNotFound) {
						Diag.LogToConsole(
							"Could not find property [" + string.Join(", ", propertyPath) + "] from JObject",
							Diag.LogSeverities.Error);
						throw new CustomArgumentException();
					}

					Diag.LogToConsole("Could not find property [" + string.Join(", ", propertyPath) + "] from JObject",
						Diag.LogSeverities.Warning);
					return null;
				}

				var jType = cursor.GetType();

				if (jType != typeof(JObject) || cursor[key] == null) {
					if (!throwIfNotFound) return null;
					Diag.LogToConsole(
						"There is a unexpected null value in instance configurations table: [" +
						string.Join(", ", propertyPath) + "] from JObject", Diag.LogSeverities.Error);
					throw new CustomArgumentException();
				}

				cursor = cursor[key];
			}

			if (cursor == null && throwIfNotFound) {
				Diag.LogToConsole("Property [" + string.Join(", ", propertyPath) + "] was null",
					Diag.LogSeverities.Error);
				throw new CustomArgumentException();
			}

			return cursor;
		}

		public static bool In<T>(this T item, params T[] items) {
			return items.Contains(item);
		}

		public static string Left(this string text, int length) {
			return Modi.Left(text, length);
		}

		public static string Right(this string text, int length) {
			return Modi.Right(text, length);
		}

		public static string TrimEnd(this string text, string trimText) {
			if (!text.EndsWith(trimText)) return text;
			return text.Remove(text.LastIndexOf(trimText));
		}

		public static string TrimEnd<T>(this T item, string trimText) where T : IEquatable<string> {
			return TrimEnd(item, trimText);
		}

		public class Filter {
			private List<KeyValuePair<string, string>> filters = new();
			private List<string> strings = new();
			private List<KeyValuePair<string, int>> intFilters = new();

			public Filter Add(string key, string value) {
				filters.Add(new KeyValuePair<string, string>(key, value));
				return this;
			}

			public Filter Add(string key, int value) {
				intFilters.Add(new KeyValuePair<string, int>(key, value));
				return this;
			}

			public Filter Add(string value) {
				strings.Add(value);
				return this;
			}

			public override string ToString() {
				var result = "";
				foreach (var pair in filters) {
					result += (result.Length == 0 ? "" : " AND ") + pair.Key + " = '" + pair.Value + "'";
				}

				foreach (var pair in intFilters) {
					result += (result.Length == 0 ? "" : " AND ") + pair.Key + " = " + pair.Value;
				}

				foreach (var value in strings) {
					result += (result.Length == 0 ? "" : " AND ") + value;
				}

				return result;
			}
		}
	}
}