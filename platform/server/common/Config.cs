﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.common {
	public static class Config {
		private const string DomainProperty = "domain";
		private const string DynamicProperty = "dynamic";

		public static string GetDefaultDomain() {
			var result = "default";
			foreach (var d in Domains) {
				if (d != "default") {
					result = d;
					break;
				}
			}
			return result;
		}
		public static List<string> Domains = new List<string>();
		public static Dictionary<string, string> Instances = new Dictionary<string, string>();

		private static readonly string ConfigGroup = "config";
		private static readonly string ConfigClientGroup = "clientConfig";
		public static List<Dictionary<string, object>> SystemIcons = new List<Dictionary<string, object>>();
		public static Dictionary<string, string> IconCodes = new Dictionary<string, string>();
		public static string AppPath { get; set; }
		public static string WwwPath { get; set; }
		public static string Environment { get; set; }
		public static string EnvironmentIdentifier { get; set; }
		public static string Version { get; set; }
		public static string VersionDate { get; set; }
		public static string Repo { get; set; } = "";
		public static JObject Configuration { get; set; }
		public static string VirtualDirectory { get; set; }

		public static string GetGroupName() {
			return ConfigClientGroup;
		}

		public static void InitializeSystemList() {
			var json = File.ReadAllText(WwwPath + Path.Combine("public", "fonts", "icons.json"));
			var icons = JsonConvert.DeserializeObject<Root>(json);

			foreach (var group in icons.Categories) {
				var menu = new Dictionary<string, object> {
					{
						"Variable", group.Name
					}, {
						"ItemId", group.Name
					}
				};

				foreach (var icon in group.Icons) {
					var m = new Dictionary<string, object> {
						{
							"Variable", icon.Id
						}, {
							"ItemId", icon.Id
						}, {
							"ParentItemId", group.Name
						}, {
							"Icon", icon.Id
						}
					};

					SystemIcons.Add(m);
				}

				SystemIcons.Add(menu);
			}

			var codePoints = File.ReadAllText(WwwPath + Path.Combine("public", "fonts", "codepoints"));
			foreach (var codeLine in codePoints.Split('\n')) {
				var codeArr = codeLine.Split(' ');
				if (codeArr.Length > 1) {
					IconCodes.Add(codeArr[0], codeArr[1]);
				}
			}
		}

		public static Exception Initialize(IConnections db) {
			var instanceConfig = new JObject();
			var domainConfig = new JObject();

			DataTable data;
			try {
				data = db.GetDatatable(
					"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'instance_configurations') BEGIN SELECT instance, domain, category, name, value FROM instance_configurations WHERE server = 1 ORDER BY instance, domain, category END");
			} catch (Exception e) {
				return e;
			}

			foreach (DataRow r in data.Rows) {
				var instance = (string) r["instance"];
				var category = (string) r["category"];
				var name = (string) r["name"];
				var value = (string) r["value"];
				var domain = r[DomainProperty]?.ToString() ?? "";

				if (!string.IsNullOrEmpty(domain)) {
					if (domainConfig[instance] == null) {
						var it = GetRequiredItemFromConfiguration(DomainProperty, domain, DynamicProperty);
						var iT = it.DeepClone();
						iT[category][name]["value"] = value;
						var dD = new Dictionary<string, JToken> {
							{domain, iT}
						};

						JToken dT = JObject.Parse(JsonConvert.SerializeObject(dD, Formatting.Indented));
						domainConfig.Add(instance, dT);
					} else if (domainConfig[instance][domain] == null) {
						var iT = GetRequiredItemFromConfiguration(DomainProperty, domain, DynamicProperty).DeepClone();
						iT[category][name]["value"] = value;
						domainConfig[instance][domain] = iT;
					} else if (domainConfig[instance][domain][category] == null) {
						var iT = GetRequiredItemFromConfiguration(DomainProperty, domain, DynamicProperty, category)
							.DeepClone();
						iT[name]["value"] = value;
						domainConfig[instance][domain][category] = iT;
					} else {
						var iT = GetRequiredItemFromConfiguration(DomainProperty, domain, DynamicProperty, category,
							name).DeepClone();
						iT["value"] = value;
						domainConfig[instance][domain][category][name] = iT;
					}
				} else {
					if (instanceConfig[instance] == null) {
						var iT = GetRequiredItemFromConfiguration(DynamicProperty).DeepClone();
						iT[category][name]["value"] = value;
						instanceConfig.Add(instance, iT);
					} else if (instanceConfig[instance][category] == null) {
                        var iT = GetRequiredItemFromConfiguration(DynamicProperty, category);
                        iT = iT.DeepClone();
                        iT[name]["value"] = value;
                        instanceConfig[instance][category] = iT;
                    } else {
						var iT = GetRequiredItemFromConfiguration(DynamicProperty, category, name).DeepClone();
						iT["value"] = value;
						instanceConfig[instance][category][name] = iT;
					}
				}
			}

			foreach (var instance in instanceConfig) {
				foreach (var jToken in instance.Value) {
					var category = (JProperty) jToken;
					Cache.Set(instance.Key, "Config", category.Name, category.Value);
				}
			}

			foreach (var instance in domainConfig) {
				foreach (var jToken in instance.Value) {
					var domain = (JProperty) jToken;
					foreach (var jToken1 in domain.Value) {
						var category = (JProperty) jToken1;
						Cache.Set(instance.Key, domain.Name, "Config", category.Name, category.Value);
					}
				}
			}

			return null;
		}

		public static void SetJsonFile(string path) {
			var config = JObject.Parse(File.ReadAllText(WwwPath + path));
			SetDomainConfigs(config);
		}

		public static void SetDomainConfigs(JObject config) {
			Configuration = config;
			foreach (var jToken in config[DomainProperty]) {
				var domain = (JProperty) jToken;
				Domains.Add(domain.Name);
			}
		}


		private static void MergeSettings(JContainer r, string type, string category, string domain = "") {
			JToken sT = null;

			if (domain != "") {
				if (Configuration?[DomainProperty]?[domain] != null &&
				    Configuration[DomainProperty][domain][type] != null &&
				    Configuration[DomainProperty][domain][type][category] != null) {
					sT = Configuration[DomainProperty][domain][type][category].DeepClone();
				}
			} else {
				if (Configuration?[type] != null && Configuration[type][category] != null)
					sT = Configuration[type][category].DeepClone();
			}

			if (sT == null) return;

			var sO = sT.Value<JObject>();
			r.Merge(sO, new JsonMergeSettings {MergeArrayHandling = MergeArrayHandling.Concat});
		}

		private static JObject CombineDomainConfigurations(string domain, string category) {
			var r = JObject.Parse("{}");
			MergeSettings(r, "static", category, domain);
			MergeSettings(r, "hidden", category, domain);
			MergeSettings(r, DynamicProperty, category, domain);
			return r;
		}

		private static JObject CombineDomainConfigurations(JObject dynamic, string domain, string category) {
			var r = JObject.Parse("{}");
			MergeSettings(r, "static", category, domain);
			MergeSettings(r, "hidden", category, domain);

			if (dynamic != null) {
				r.Merge(dynamic, new JsonMergeSettings {MergeArrayHandling = MergeArrayHandling.Concat});
			}

			return r;
		}

		private static JObject CombineConfigurations(string category) {
			var r = JObject.Parse("{}");
			MergeSettings(r, "static", category);
			MergeSettings(r, "hidden", category);
			MergeSettings(r, DynamicProperty, category);
			return r;
		}

		private static JObject CombineConfigurations(JObject dynamic, string category) {
			var r = JObject.Parse("{}");
			MergeSettings(r, "static", category);
			MergeSettings(r, "hidden", category);

			if (dynamic != null) {
				r.Merge(dynamic, new JsonMergeSettings {MergeArrayHandling = MergeArrayHandling.Concat});
			}

			return r;
		}

		public static JObject GetDomain(string domain, string category) {
			if (domain == "") domain = "default";
			domain = domain.ToLower();
			category = category.ToLower();
			JObject r;
			var jT = GetOptionalItemFromConfiguration(DomainProperty, domain);
			if (jT != null) {
				jT = GetOptionalItemFromConfiguration(DomainProperty, domain, DynamicProperty, category);
				r = jT != null
					? CombineDomainConfigurations(jT.Value<JObject>(), domain, category)
					: CombineDomainConfigurations(domain, category);
			} else {
				r = CombineDomainConfigurations(domain, category);
			}

			return r;
		}

		public static JObject GetDomain(string instance, string domain, string category) {
			domain = domain.ToLower();
			category = category.ToLower();

			var res = (JObject) Cache.Get(instance, domain, ConfigGroup, category);
			if (res == null) {
				return GetDomain(domain, category);
			}

			var r = CombineDomainConfigurations(res, domain, category);
			return r;
		}


		public static string GetDomainValue(string domain, string category, string name) {
			if (domain == "") domain = "default";
			domain = domain.ToLower();
			category = category.ToLower();
			name = name.ToLower();

			JObject r;
			if (GetOptionalItemFromConfiguration(DomainProperty, domain) != null) {
				var jT = GetOptionalItemFromConfiguration(DomainProperty, domain, DynamicProperty, category);
				r = jT != null
					? CombineDomainConfigurations(jT.Value<JObject>(), domain, category)
					: CombineDomainConfigurations(domain, category);
			} else {
				r = CombineDomainConfigurations(domain, category);
			}

			return GetValueForName(name, r, DomainProperty);
		}

		public static string GetDomainValue(string instance, string domain, string category, string name) {
			if (domain == "") domain = "default";
			domain = domain.ToLower();
			category = category.ToLower();
			name = name.ToLower();

			var res = (JObject) Cache.Get(instance, domain, ConfigGroup, category);
			if (res == null) {
				return GetDomainValue(domain, category, name);
			}

			var r = CombineDomainConfigurations(res, domain, category);

			return GetValueForName(name, r, DynamicProperty);
		}

		public static JObject Get(string category) {
			category = category.ToLower();

			var jT = GetOptionalItemFromConfiguration(DynamicProperty, category);
			// ReSharper disable once ConvertIfStatementToReturnStatement
			if (jT != null) {
				return CombineConfigurations(jT.Value<JObject>(), category);
			}

			return CombineConfigurations(category);
		}

		public static JObject Get(string instance, string category) {
			category = category.ToLower();

			var res = (JObject) Cache.Get(instance, ConfigGroup, category);
			return res == null ? Get(category) : CombineConfigurations(category);
		}

		private static JToken GetOptionalItemFromConfiguration(params string[] parameters) {
			var parameterList = parameters.ToList();
			return Util.GetItemForFields(Configuration, parameterList);
		}

		private static JToken GetRequiredItemFromConfiguration(params string[] parameters) {
			var parameterList = parameters.ToList();
			return Util.GetItemForFields(Configuration, true, parameterList);
		}

		public static string GetValue(string category, string name) {
			category = category.ToLower();
			name = name.ToLower();
			var jT = GetOptionalItemFromConfiguration(DynamicProperty, category);
			JObject r;
			// ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
			if (jT != null) {
				r = CombineConfigurations(jT.Value<JObject>(), category);
			} else {
				r = CombineConfigurations(category);
			}

			return GetValueForName(name, r, DynamicProperty);
		}

		private static string GetValueForName(string name, JObject r, string scopeName) {
			if (r == null) {
				return null;
			}

			if (r[name] == null) {
				throw new NullReferenceException("Key '" + name + "' was not found in '" + scopeName + "' config");
			}

			if (r[name]["value"] == null) {
				throw new NullReferenceException("Key '" + name + "' has no value field in '" + scopeName + "' config");
			}

			return r[name]["value"].ToString();
		}

		public static string GetValue(string instance, string category, string name) {
			category = category.ToLower();
			name = name.ToLower();
			var res = (JObject) Cache.Get(instance, ConfigGroup, category);
			if (res == null) {
				return GetValue(category, name);
			}

			var r = CombineConfigurations(res, category);

			return GetValueForName(name, r, "");
		}

		public static string GetClientConfiguration(AuthenticatedSession session, string process, string category,
			string name) {
			return (string) Cache.Get(session.instance, process, category, name) ??
			       ClientConfiguration(session, process, category, name);
		}


		private static string ClientConfiguration(AuthenticatedSession session, string process, string category,
			string name) {
			var db = new Connections(session);
			var cbp = new ConnectionBaseProcess(session.instance, process, session);

			cbp.SetDBParam(SqlDbType.NVarChar, "@category", category);
			cbp.SetDBParam(SqlDbType.NVarChar, "@name", name);

			var r = db.GetDataRow("SELECT value FROM instance_configurations WHERE " +
			                      "instance = @instance AND " +
			                      "process = @process AND " +
			                      "category = @category AND " +
			                      "name = @name AND " +
			                      "server = 0", cbp.DBParameters);

			Cache.Set(session.instance, ConfigClientGroup, process, category, name, r);
			return (string) r?["value"];
		}

		public static string GetClientConfigurationKey(AuthenticatedSession session, string process, string category,
			string name, string key) {
			var d = ClientConfiguration(session, process, category, name);
			if (Conv.ToStr(d) == "") return "";

			var r = JObject.Parse(d);
			if (r.ContainsKey(key) && r[key] != null) return r[key].ToString();

			return "";
		}

		public static Dictionary<string, string> GetClientConfiguration(AuthenticatedSession session, string category,
			string name) {
			var db = new Connections(session);
			var cbp = new ConnectionBase(session.instance, session);
			var result = new Dictionary<string, string>();

			cbp.SetDBParam(SqlDbType.NVarChar, "@category", category);
			cbp.SetDBParam(SqlDbType.NVarChar, "@name", name);

			foreach (DataRow row in db.GetDatatable("SELECT process, value FROM instance_configurations WHERE " +
			                                        "instance = @instance AND " +
			                                        "category = @category AND " +
			                                        "name = @name AND " +
			                                        "server = 0", cbp.DBParameters).Rows) {
				var process = Conv.ToStr(row["process"]);
				var value = Conv.ToStr(row["value"]);

				result.Add(process, value);
				Cache.Set(session.instance, ConfigClientGroup, process, category, name, value);
			}

			return result;
		}

		public class Root {
			public string BaseUrl { get; set; }
			public Category[] Categories { get; set; }
		}

		public class Category {
			public string Name { get; set; }
			public Icon[] Icons { get; set; }
		}

		public class Icon {
			public string Id { get; set; }
		}
	}
}