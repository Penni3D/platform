﻿using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Authentication = Keto5.x.platform.server.security.Authentication;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Threading.Tasks;

//Rest Base Class
namespace Keto5.x.platform.server.@base {
	public class IntegrationBase : Controller {
		//public Authentication Auth = new Authentication("");
		private AuthenticatedSession _currentSession;

		public string instance {
			get {
				//Get instance by request's hostname
				var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
				return conbase.GetInstance(Request.Host.Host);
			}
		}

		private string authError = null;

		public AuthenticatedSession currentSession {
			get {
				if (_currentSession == null) {
					//Get information from post
					var key = Request.Headers["key"];
					var token = Request.Headers["token"];
					
					var Auth = new Authentication(instance);
				
					//Create session
					var u = Auth.getUserByIntegrationToken(key, token, HttpContext);
					if (u == null) {
						return null;
					}
					else if (u.CurrentUserId == 0) {
						authError = "BasicAuthentication_LoginTriesExceeded";
						return null;
					}

					_currentSession = new AuthenticatedSession(instance, u.ItemId, u.GetMembers());
				}

				//Session established
				return _currentSession;
			}
		}

		public override void OnActionExecuting(ActionExecutingContext context) {
			if (Response.StatusCode != 401) {
				//Rest code is about to be executed. Validate session or cookie
				if (currentSession != null) {
					//Execute Requested Action
					base.OnActionExecuting(context);
				} else {
					//Response is 401: Unauthorized
					Response.StatusCode = 401;

					if (authError != null)
						context.Result = new ErrorResult(authError);
					else 
						context.Result = new EmptyResult();
					base.OnActionExecuting(context);
				}
			}
		}

		private class ErrorResult : ActionResult, IStatusCodeActionResult, IActionResult {
			public ErrorResult(string content) { Content = content; }
			public string Content { get; set; }
			public string ContentType { get; set; }
			public int? StatusCode { get; set; }
			public override Task ExecuteResultAsync(ActionContext context) {
				var buffer = System.Text.Encoding.UTF8.GetBytes(Content);
				return context.HttpContext.Response.Body.WriteAsync(buffer, 0, buffer.Length);
			}
		}
	}
}