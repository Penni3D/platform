﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Keto5.x.customer.server;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.core.resourceManagement;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Lucene.Net.Analysis.Hunspell;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.@base {
	/// <summary>
	/// Work in progress.
	/// Goal is to get rid of this completely.
	/// Good ol' ItemBase. This is currently split into multiple files.
	/// DatabaseItemService handles actions for plain database objects
	/// FavouriteService handles favourites (doh!)
	/// ItemCollectionService handles collection of entities. Used with searches.
	/// </summary>
	public partial class ItemBase : SessionBaseInfo, IDatabaseItemService {
		public Dictionary<string, object> InsertItemRow(Dictionary<string, object> data,
			List<ItemColumn> cachedItemColumns = null, bool executeActions = true) {
			return DatabaseItemService.InsertItemRow(data, cachedItemColumns, executeActions);
		}

		public List<Dictionary<string, object>> InsertItemRows(List<Dictionary<string, object>> data,
			List<ItemColumn> cachedItemColumns = null, bool executeActions = true) {
			return DatabaseItemService.InsertItemRows(data, cachedItemColumns, executeActions);
		}

		public int InsertItemRow(int ownerItemId = 0, int parentItemId = 0) {
			return ProcessBase.InsertItemRow(ownerItemId: ownerItemId, parentItemId: parentItemId);
		}

		public Dictionary<string, object> AddItem(Dictionary<string, object> data, int parentId = 0, bool checkRights = true, bool checkNotifications = true,
			bool executeNewRowActions = true, int ownerItemId = 0) {
			data["item_id"] = ProcessBase.InsertItemRow(parentId, executeNewRowActions, ownerItemId);

			var itemId = Conv.ToInt(data["item_id"]);

			Dictionary<string, object> oldData = null;
			if (checkNotifications) oldData = DatabaseItemService.GetItem(itemId, checkRights: checkRights);

			SaveItem(data, checkRights: checkRights, followsInsert: true);

			//INVALIDATE CACHE
			InvalidateCache(itemId);

			if (checkNotifications) {
				var states = GetStates();
				var newData = DatabaseItemService.GetItem(itemId, false, checkRights);
				states.CheckNotifications(Process, itemId, oldData, newData);
			}

			return data;
		}

		public int AddItemOrUpdate(Dictionary<string, object> data, Dictionary<string, object> uniqueBy,
			int parentId = 0, bool checkRights = true) {
			var uniqueSql = "SELECT item_id FROM _" + ProcessBase.instance + "_" + ProcessBase.process + " WHERE 1=1 ";

			foreach (var (key, value) in uniqueBy) {
				var pkey = key == "process" ? "unique_process" : key;
				uniqueSql += " AND " + key + " = @" + pkey;
				ProcessBase.SetDBParameter(pkey, value);
			}

			var existingItemId = Conv.ToInt(ProcessBase.db.ExecuteScalar(uniqueSql, ProcessBase.DBParameters));
			var itemId = existingItemId;
			if (existingItemId == 0) {
				data["item_id"] = ProcessBase.InsertItemRow(parentId);
				itemId = Conv.ToInt(data["item_id"]);
			} else {
				data["item_id"] = existingItemId;
			}

			SaveItem(data, checkRights: checkRights, returnSubqueries: false);

			//INVALIDATE CACHE
			InvalidateCache(itemId);

			return itemId;
		}

		public APISaveResult AddRow(APISaveResult data, int parentId = 0, string process = "",
			bool checkRights = false, int ownerItemId = 0) {
			var itemId = ProcessBase.InsertItemRow(ownerItemId: ownerItemId);

			var states = new States(ProcessBase.instance, AuthenticatedSession);
			if (checkRights) {
				var fStates =
					states.GetEvaluatedStates(ProcessBase.process, "meta", itemId, new List<string> { "meta.insert" });
				if (!fStates.States.Contains("meta.insert")) {
					//No access to row -- remove completely
					ProcessBase.DeleteItemRow(itemId, false, true);

					var exception = new CustomPermissionException("NOT_ALLOWED_TO_INSERT_THE_PROCESS",
						"Current user can't insert into process " + Process,
						BaseServerException.ExceptionSubType.INSERT);
					exception.Data.Add("process", Process);
					throw exception;
				}

				checkRights = false;
			}

			if (parentId > 0) {
				var p = new Processes(Instance, AuthenticatedSession);
				foreach (var pp in p.GetParentProcesses(false, Process)) {
					ProcessBase.SetDBParam(SqlDbType.Int, "@processParentId", parentId);
					ProcessBase.SetDBParam(SqlDbType.Int, "@processSubId", itemId);
					ProcessBase.db.ExecuteInsertQuery(
						"INSERT INTO item_subitems (parent_item_id,item_id) VALUES(@processParentId, @processSubId)",
						ProcessBase.DBParameters);
				}
			}

			data.Data["item_id"] = itemId;
			if (data.Data.Count > 0) {
				//Copy model event
				if (data.Data.ContainsKey("CopyModelProcess") && data.Data.ContainsKey("CopyModelItemColumn")) {
					var copyProcess = Conv.ToStr(data.Data["CopyModelProcess"]);
					var copyCol = Conv.ToStr(data.Data["CopyModelItemColumn"]);
					if (copyProcess != null && copyProcess.Length > 0 && copyCol != null && copyCol.Length > 0 &&
					    data.Data.ContainsKey(copyCol)) {
						var copyFromColumn = ItemColumns.GetItemColumnByName(copyCol);
						if (copyFromColumn.DataType == 6) {
							var listIcs = new ItemColumns(Instance, copyFromColumn.DataAdditional,
								AuthenticatedSession);
							var copyProcessColFound = false;
							var listCopyColName = "";
							foreach (var c in listIcs.GetItemColumns()) {
								if (c.DataType == 5 && c.DataAdditional == copyProcess) {
									copyProcessColFound = true;
									listCopyColName = c.Name;
									break;
								}
							}

							if (copyProcessColFound) {
								var listIb = new ItemBase(Instance, copyFromColumn.DataAdditional,
									AuthenticatedSession);
								var ids = Conv.ToIntArray(data.Data[copyCol]);
								if (ids.Length > 0) {
									var listData = listIb.GetItem(ids[0]);
									var ids2 = Conv.ToIntArray(listData[listCopyColName]);
									var copyId = ids2[0];
									if (copyId > 0) {
										if (data.Data.ContainsKey("copy_from_id")) {
											data.Data["copy_from_id"] = copyId;
										} else {
											data.Data.Add("copy_from_id", copyId);
										}
									}
								}
							}
						}
					}
				}

				if (data.Data.ContainsKey("copy_from_id") && Conv.ToInt(data.Data["copy_from_id"]) > 0) {
					var copyFromId = Conv.ToInt(data.Data["copy_from_id"]);
					Dictionary<string, object> copiedTasks = null;
					var t = new TaskData(Instance, AuthenticatedSession);
					if (data.Data.ContainsKey("copy_date") && data.Data.ContainsKey("copy_date_2")) {
						copiedTasks = t.CopyTasks(copyFromId, Conv.ToInt(itemId),
							Conv.ToDateTime(data.Data["copy_date"]),
							Conv.ToDateTime(data.Data["copy_date_2"]));
						if (data.Data.ContainsKey("copy_to_start_date") && data.Data.ContainsKey("copy_to_end_date")) {
							ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
							ProcessBase.SetDBParam(SqlDbType.SmallDateTime, "@startdate",
								Conv.ToDateTime(data.Data["copy_date"]));
							ProcessBase.SetDBParam(SqlDbType.SmallDateTime, "@enddate",
								Conv.ToDateTime(data.Data["copy_date_2"]));
							if (Conv.ToStr(data.Data["copy_to_start_date"]).Length > 0 &&
							    Conv.ToStr(data.Data["copy_to_end_date"]).Length > 0) {
								ProcessBase.db.ExecuteUpdateQuery(
									"UPDATE _" + Instance + "_" + Process + " SET " +
									Conv.ToStr(data.Data["copy_to_start_date"]) +
									" = @startdate, " + Conv.ToStr(data.Data["copy_to_end_date"]) +
									" = @enddate WHERE item_id = @item_id",
									ProcessBase.DBParameters);
							}
						}
					} else if (data.Data.ContainsKey("copy_date")) {
						copiedTasks = t.CopyTasks(copyFromId, Conv.ToInt(itemId),
							Conv.ToDateTime(data.Data["copy_date"]));
						if (data.Data.ContainsKey("copy_to_start_date")) {
							ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
							ProcessBase.SetDBParam(SqlDbType.SmallDateTime, "@startdate",
								Conv.ToDateTime(data.Data["copy_date"]));
							if (Conv.ToStr(data.Data["copy_to_start_date"]).Length > 0) {
								ProcessBase.db.ExecuteUpdateQuery(
									"UPDATE _" + Instance + "_" + Process + " SET " +
									Conv.ToStr(data.Data["copy_to_start_date"]) +
									" = @startdate WHERE item_id = @item_id", ProcessBase.DBParameters);
							}
						}
					} else {
						copiedTasks = t.CopyTasks(copyFromId, Conv.ToInt(itemId));
					}

					var pp = new Processes(Instance, AuthenticatedSession);
					if (pp.ProcessExist("budget_row")) {
						var b = new BudgetRowData(Instance, AuthenticatedSession);
						b.CopyBudgetRows(Conv.ToInt(data.Data["copy_from_id"]), Conv.ToInt(itemId));
					}

					if (pp.ProcessExist("allocation")) {
						var rmr = new ResourceManagementRepository(
							new ConnectionBase(Instance, AuthenticatedSession),
							new EntityServiceProvider(Instance, AuthenticatedSession));
						var allocationService = new AllocationService(Instance, AuthenticatedSession, rmr);
						allocationService.CopyAllocations(Conv.ToInt(data.Data["copy_from_id"]), Conv.ToInt(itemId),
							(Dictionary<int, int>)copiedTasks["idPairs"]);
					}

					if (data.Data.ContainsKey("CopyModelInstanceUnionId") &&
					    Conv.ToInt(data.Data["CopyModelInstanceUnionId"]) > 0) {
						var instanceUnionId = Conv.ToInt(data.Data["CopyModelInstanceUnionId"]);
						var p = new Processes(Instance, AuthenticatedSession);
						CopyDataFromProcessToProcess(instanceUnionId, p.GetProcessByItemId(copyFromId), copyFromId,
							Conv.ToInt(itemId));
					}
				}
			}

			//Override User Process with Instance Defaults
			if (process == "user") {
				var u = new User(Instance, AuthenticatedSession.item_id);
				data.Data = u.SetInstanceDefaults(data.Data);
			}

			try {
				SaveItem(data.Data);
			} catch (SqlException e) when (e.Number == 2601 || e.Number == 2627) {
				Delete(itemId, false);
				throw new CustomUniqueException();
			}

			//INVALIDATE CACHE
			InvalidateCache(itemId);

			//Check if custom serial needs to be saved
			var customerConfig = new CustomerConfig();
			var newSerial = customerConfig.OverrideSerial(itemId, AuthenticatedSession, Instance, Process);
			if (newSerial.Length > 0) {
				if (data.Data.ContainsKey("serial")) {
					data.Data["serial"] = newSerial;
				} else {
					data.Data.Add("serial", newSerial);
				}

				SaveItem(data.Data);
			}

			if (!data.Data.ContainsKey("instance")) data.Data.Add("instance", Instance);
			if (!data.Data.ContainsKey("process")) data.Data.Add("process", Process);

			data.Data = DatabaseItemService.GetItem(itemId, false);
			if (Process.Equals("task")) data.Data.Add("has_write_right", true);

			var linkData = new Dictionary<int, Dictionary<int, Dictionary<string, object>>>();
			var idps = new ItemDataProcessSelections(ProcessBase.instance, Process,
				ProcessBase.authenticatedSession);
			ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
			var dt = ProcessBase.db.GetDatatable(
				"SELECT item_column_id, selected_item_id, link_item_id FROM item_data_process_selections WHERE item_id = @item_id",
				ProcessBase.DBParameters);

			foreach (var ic in ItemColumns.GetItemColumns()) {
				if ((ic.DataType == 5 || ic.DataType == 6 || ic.DataType == 12) && ic.LinkProcess != null &&
				    !ic.Name.Equals("linked_tasks")) {
					linkData.Add(ic.ItemColumnId,
						idps.GetItemDataProcessSelectionIds(itemId, ic.ItemColumnId, ic.LinkProcess, null, dt));
				}
			}

			data.LinkData = linkData;

			states.CheckNotificationsForCreate(Process, itemId, data.Data);

			ProcessBase.DoNewRowAction(itemId, true, checkRights);

			return data;
		}
	}
}