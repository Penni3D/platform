using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.backgroundProcesses;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.@base {
	public abstract class SubQueryConfigBase {
		protected AuthenticatedSession authenticatedSession;
		protected SubQueryConfig Config;
		protected Dictionary<string, object> ConfigData;

		protected Dictionary<string, object> FieldConfig;
		protected string instance;
		protected string process;

		private readonly Semaphore sem;
		private bool _cacheEnabled = false;

		public SubQueryConfigBase() {
			Config = new SubQueryConfig();
			ResetFieldConfig();
			sem = new Semaphore(1, 1);
		}

		private string _subprocess = "";

		protected virtual string SubProcess {
			get {
				if (_subprocess == "") return process;
				return _subprocess;
			}
			set { _subprocess = value; }
		}

		protected virtual string ColumnName { get; set; } = "list_item";
		public bool FilterWithResolvedValues { get; set; } = false;

		/*public virtual string GetSql(string instance, int attachedItemColumnId = 0) {
			return GetSql(instance); }*/
		public abstract string GetSql(string instance, int attachedItemColumnId = 0);

		public string GetQueryCachedSql(string instance, string name, int itemColumnId = 0) {
			var originalSql = GetSql(instance, itemColumnId);
			var sql = "";

			if (HasCache()) {
				sql = "(CASE WHEN pf.cache_" + name + " = 'cache_null' THEN NULL ELSE CASE WHEN pf.cache_" + name +
				      " IS NULL THEN (" + originalSql + ") ELSE pf.cache_" + name + " END END)";
				switch (GetConfig().DataType) {
					case ItemColumn.ColumnType.Int:
					case ItemColumn.ColumnType.Float:
						sql = "(CASE WHEN pf.cache_" + name + " = " + int.MinValue.ToString() +
						      " THEN NULL ELSE CASE WHEN pf.cache_" + name + " IS NULL THEN (" + originalSql +
						      ") ELSE pf.cache_" + name + " END END)";
						break;
					case ItemColumn.ColumnType.Date:
					case ItemColumn.ColumnType.Datetime:
						sql = "(CASE WHEN pf.cache_" + name + " = '" + new DateTime(1800, 1, 1).ToString("yyyy-MM-dd") +
						      "' THEN NULL ELSE CASE WHEN pf.cache_" + name + " IS NULL THEN (" + originalSql +
						      ") ELSE pf.cache_" + name + " END END)";
						break;
				}
			} else {
				sql = originalSql;
			}

			return sql;
		}

		public virtual string GetCacheValidationSql(string instance) {
			return "";
		}

		public bool RequireValidation = true;

		public SubQueryConfig GetConfig() {
			return Config;
		}

		protected abstract void InitializeFieldConfig();

		private void ResetFieldConfig() {
			FieldConfig = new Dictionary<string, object>();
		}


		public void SetFieldConfigs(string config) {
			InitializeFieldConfig();
			SetFieldConfig(config);
		}


		private void SetFieldConfig(string config) {
			if (!string.IsNullOrEmpty(config)) {
				var configData =
					JsonConvert.DeserializeObject<Dictionary<string, object>>(config);
				foreach (var key in configData.Keys) {
					if (key == "cacheEnabled") {
						_cacheEnabled = Conv.ToBool(configData[key]);
						continue;
					}

					if (FieldConfig.ContainsKey(key)) {
						if (((SubQueryConfigValue)FieldConfig[key]).ValueType.Equals("int")) {
							((SubQueryConfigValue)FieldConfig[key]).Value = Conv.ToInt(configData[key]);
						} else if (((SubQueryConfigValue)FieldConfig[key]).ValueType.Equals("string")) {
							((SubQueryConfigValue)FieldConfig[key]).Value = Conv.ToStr(configData[key]);
						} else if (((SubQueryConfigValue)FieldConfig[key]).ValueType.Equals("int_array")) {
							((SubQueryConfigValue)FieldConfig[key]).Value = Conv.ToIntArray(configData[key]);
						}
					}
				}

				ConfigData = configData;
			}
		}

		public Dictionary<string, object> GetConfigData() {
			return ConfigData;
		}

		public Dictionary<string, object> GetFieldConfigs() {
			InitializeFieldConfig();
			return FieldConfig;
		}

		public bool HasCache() {
			return _cacheEnabled;
		}

		protected void AddFieldConfig(string key, object value) {
			if (GetSemaphore().WaitOne(1000)) {
				if (FieldConfig.ContainsKey(key)) {
					if (((SubQueryConfigValue)value).Value == null &&
					    ((SubQueryConfigValue)FieldConfig[key]).Value != null) {
						((SubQueryConfigValue)value).Value = ((SubQueryConfigValue)FieldConfig[key]).Value;
					}

					FieldConfig[key] = value;
				} else {
					FieldConfig.Add(key, value);
				}

				GetSemaphore().Release();
			}
		}

		public void SetInstance(string i) {
			instance = i;
		}

		public void SetProcess(string p) {
			process = p;
		}

		public void SetSession(AuthenticatedSession a) {
			authenticatedSession = a;
		}

		public Semaphore GetSemaphore() {
			return sem;
		}

		public virtual List<Dictionary<string, object>> GetValues(string instance, int itemColumnId) {
			return new List<Dictionary<string, object>>();
		}

		public virtual string GetSubProcess() {
			return SubProcess;
		}

		public virtual string GetColumnName() {
			return ColumnName;
		}

		public virtual string GetFilterSql(string instance = "") {
			return "";
		}

		public virtual string GetOrderSql(string instance = "") {
			return "[COLUMNNAME]";
		}

		public virtual string GetArchiveSql(string instance) {
			return GetSql(instance);
		}

		protected string processSelectionsTable(bool isArchive, string whereColId, string whereCol = "item_column_id") {
			if (isArchive) {
				return
					" ( SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start] " +
					" 				FROM archive_item_data_process_selections WHERE archive_id IN ( " +
					" 					SELECT MAX(archive_id) " +
					" 					FROM archive_item_data_process_selections " +
					" 					WHERE archive_start <= @archiveDate AND archive_end > @archiveDate AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN " +
					" 					(SELECT CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) FROM  item_data_process_selections  WHERE archive_start <= @archiveDate AND " +
					whereCol + " =  " + whereColId + "  ) " +
					"					AND " + whereCol + " =  " + whereColId +
					" 					GROUP BY [item_column_id],[item_id],[selected_item_id] " +
					" 					) AND " + whereCol + " =  " + whereColId +
					" 				UNION  " +
					" 				SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start]  " +
					" 				FROM item_data_process_selections  " +
					" 				WHERE archive_start <= @archiveDate   AND " + whereCol + " =  " + whereColId + " ) ";
			}

			return " item_data_process_selections ";
		}
	}

	public class SubQueryConfig {
		public string Name { get; set; }
		public string Value { get; set; }
		public string TranslationKey { get; set; }
		public ItemColumn.ColumnType DataType { get; set; }
	}

	public class SubQueryConfigValue {
		public string Name;
		public string OptionTranslation;
		public SubQueryOptionType OptionType;
		public string OptionValue;
		public bool ReQuery;
		public object Value;
		public object Values;
		public string ValueType;

		public SubQueryConfigValue(string name, string valueType, SubQueryOptionType optionType, string optionValue,
			string optionTranslation,
			object values = null, bool r = false) {
			Name = name;
			ValueType = valueType;
			OptionType = optionType;
			OptionValue = optionValue;
			OptionTranslation = optionTranslation;
			Values = values;
			Value = null;
			ReQuery = r;
		}
	}

	public enum SubQueryOptionType {
		Nvarchar = 0,
		Int = 1,
		Float = 2,
		Date = 3,
		Text = 4,
		Process = 5,
		List = 6,
		MultiSelectList = 7
	}

	public class SubqueryCacheUpdater {
		private class ValidationQuery {
			public string tableName;
			public string validationSql;
			public string valueSql;
			public int itemColumnId;
			public string cacheColumnName;
			public string sourceColumnName;
			public string process;
			public string isNull;

			public string GetValidationClause(int forcedItemId) {
				if (forcedItemId > 0)
					return "(SELECT item_id, " + valueSql + " FROM " + tableName + " pf WHERE item_id = " +
					       forcedItemId + ") source";

				return "(SELECT item_id, " + valueSql + " FROM " + tableName + " pf WHERE item_id IN " +
				       validationSql + ") source";
			}

			public string GetUpdateClause(int forcedItemId) {
				return "UPDATE " + tableName + " SET " + tableName + ".cache_" + cacheColumnName + " = ISNULL(source." +
					   sourceColumnName + ", " + isNull + ")" +
					   " FROM " + GetValidationClause(forcedItemId) +
					   " INNER JOIN " + tableName + " ON source.item_id = " + tableName + ".item_id";
			}

			public string GetInitializeClause(int forcedItemId) {
				return "UPDATE " + tableName + " SET " + tableName + ".cache_" + cacheColumnName + " = " +
				       " ISNULL((SELECT " + valueSql + " FROM " + tableName + " pf WHERE pf.item_id = " + tableName +
				       ".item_id), " + isNull + ") " +
				       " FROM " + tableName + " WHERE " +
				       (forcedItemId > 0
					       ? tableName + ".item_id = " + forcedItemId + " "
					       : tableName + ".cache_" + cacheColumnName + " IS NULL ");
			}
		}

		public bool UpdateCache(string instance, string process, AuthenticatedSession session, int itemId = 0) {
			var cb = new ConnectionBase(instance, session);
			//Console.WriteLine(DateTime.Now.ToString() + " update cache " + itemId);

			//If cache is empty, get all cached columns
			var cachedValidationQueries = (List<ValidationQuery>)Cache.Get(instance, "CachedValidationQueries");

			if (cachedValidationQueries == null) {
				try {
					cachedValidationQueries = new List<ValidationQuery>();

					const string cacheColumns =
						"SELECT * FROM item_columns WHERE data_type = 16 AND data_additional2 LIKE '%\"cacheEnabled\":true%'";

					foreach (DataRow sq in cb.db.GetDatatable(cacheColumns).Rows) {
						var ic = new ItemColumn(sq, session);
						var ics = new ItemColumns(instance, ic.Process, session);
						var subQuery = RegisteredSubQueries.GetType(ic.DataAdditional, ic.ItemColumnId);
						var valueSql = ics.GetSubQuery(ic, noCache: true, omitCacheCaseClause: true);
						var cacheSql = subQuery.GetCacheValidationSql(instance);

						if (!subQuery.HasCache() || cacheSql == "" || valueSql == "") continue;

						var modifier =
							subQuery.GetConfig().DataType == ItemColumn.ColumnType.List ||
							subQuery.GetConfig().DataType == ItemColumn.ColumnType.Process
								? "_str"
								: "";

						var isNull = "'cache_null'";
						switch (subQuery.GetConfig().DataType) {
							case ItemColumn.ColumnType.Int:
							case ItemColumn.ColumnType.Float:
								isNull = int.MinValue.ToString(); // "0"
								break;
							case ItemColumn.ColumnType.Date:
							case ItemColumn.ColumnType.Datetime:
								isNull = "'" + new DateTime(1800, 1, 1).ToString("yyyy-MM-dd") + "'"; //"NULL"
								break;
						}

						cachedValidationQueries.Add(new ValidationQuery() {
							validationSql = cacheSql,
							tableName = "_" + instance + "_" + ic.Process,
							valueSql = valueSql,
							itemColumnId = ic.ItemColumnId,
							cacheColumnName = ic.Name,
							sourceColumnName = ic.Name + modifier,
							process = ic.Process,
							isNull = isNull
						});
					}

					Cache.Set(instance, "CachedValidationQueries", cachedValidationQueries);
				} catch (Exception e) {
					Diag.SupressException(e);
					//	Console.WriteLine("Unable to initialize subquery cache");
				}
			}

			//Get cache dates for item column ids
			var itemColumnIds = cachedValidationQueries.Select(validation => validation.itemColumnId).ToList();
			if (itemColumnIds.Count == 0) return false;

			var dates = cb.db.GetDatatable(
				"SELECT item_column_id, cache_date FROM item_columns WHERE item_column_id IN (" +
				Conv.ToDelimitedString(itemColumnIds) + ")");

			foreach (var validation in cachedValidationQueries) {
				if (process != "" && validation.process != process) continue;

				var update = DateTime.UtcNow;
				var date = Conv.ToDateTime(
					dates.Select("item_column_id = " + validation.itemColumnId)[0]["cache_date"]);
				var cacheDate = date ?? new DateTime(2000, 1, 1);

				cb.SetDBParameter("@cacheDate", cacheDate);
				cb.SetDBParameter("@updatedOn", update);
				cb.SetDBParameter("@itemColumnId", validation.itemColumnId);

				try {
					if (itemId > 0) {
						// a process item was directly updated, (force) update all its caches
                        cb.db.ExecuteUpdateQueryWithoutArchive(validation.GetUpdateClause(itemId),
							cb.DBParameters);
                    } else {
						// interval timed cache validation query for all items
                        cb.db.ExecuteUpdateQueryWithoutArchive(
							validation.GetInitializeClause(itemId) + ";" + validation.GetUpdateClause(itemId),
							cb.DBParameters);

						cb.db.ExecuteUpdateQuery(
							"UPDATE item_columns SET cache_date = @updatedOn WHERE item_column_id = @itemColumnId",
							cb.DBParameters); // update column cache_date only when querying and updating all, because some validation queries might want to update item ids other than what is updated with the single row update (itemid > 0) and then it would move cache_date forward and exclude updating those.
					}
				} catch (Exception e) {
					Diag.Log(instance, e, 0, Diag.LogCategories.BackgroundTask, Diag.LogTypes.ServerException);
				}

				//cb.db.ExecuteUpdateQuery(
				//	"UPDATE item_columns SET cache_date = @updatedOn WHERE item_column_id = @itemColumnId",
				//	cb.DBParameters); // moved to only when full validation update
			}

			return true;
		}
	}
}