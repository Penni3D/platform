using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;

namespace Keto5.x.platform.server.@base {
	public class RegisteredFeatures {
		private static readonly Dictionary<string, FeatureBase> Features = new Dictionary<string, FeatureBase>();
		
		public static void AddType(FeatureBase feature) { 
			if (Features.ContainsKey(feature.Name)) throw new Exception("Feature '" + feature.Name + "' has already been registered once");
			Features.Add(feature.Name, feature);
		}

		public static FeatureBase GetFeature(string featureName) {
			if (ContainsFeature(featureName) == false) { 
				throw new Exception("Feature '" + featureName+ "' has not been registered");
			}
			return Features[featureName];
		}

		public static List<FeatureBase> GetFeatures() {
			var r = new List<FeatureBase>();
			foreach (var f in Features) { 
				r.Add(f.Value);
			}
			return r;
		}

		public static bool ContainsFeature(string featureName) {
			return Features.ContainsKey(featureName);
		}

	}

	public abstract class FeatureBase : RestBase {
		public abstract string Process { get; }
		public abstract string Name { get; }
		
		public abstract bool ImplementsCopy { get; }
		public abstract bool ImplementsProcessRelations { get; }

		public abstract bool CopyFeature(int sourceOwnerItemId, int targetOwnerItemId);

		public bool ProcessExists() { 
			var db = new Connections();
			return Conv.ToInt(db.ExecuteScalar("SELECT COUNT(*) FROM processes WHERE process='" + Conv.ToSql(Process) + "'")) > 0;
		}

		public List<int> GetRelatedChildsByOwnerItemId(string instance, int itemId) { 
			var db = new Connections();
			var sql = "SELECT item_id FROM _" + instance + "_" + Process + " WHERE owner_item_id = " + itemId;
			var result = new List<int>();
			foreach (DataRow r in db.GetDatatable(sql).Rows) { 
				result.Add(Conv.ToInt(r["item_id"]));
			}

			return result;
		}
		public int GetRelatedParentByItemId(string instance, int itemId) { 
			var db = new Connections();
			var sql = "SELECT owner_item_id FROM _" + instance + "_" + Process + " WHERE item_id = " + itemId;
			return Conv.ToInt(db.ExecuteScalar(sql));
		}
		
		public bool Enabled = false;
		public abstract void EnableFeature();

	}
}