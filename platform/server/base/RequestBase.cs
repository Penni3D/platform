﻿using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Authentication = Keto5.x.platform.server.security.Authentication;

//Rest Base Class
namespace Keto5.x.platform.server.@base {
	[Route("v1/model/[controller]")]
	public class Error : Controller {
		public IActionResult ErrorHandler() {
			var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
			if (exceptionFeature != null) {
				var routeWhereExceptionOccurred = exceptionFeature.Path;
				var exceptionThatOccurred = exceptionFeature.Error;

				var ex = "";
				foreach (var line in exceptionThatOccurred.StackTrace.Split(" at")) {
					if (line.Contains("lambda_method")) break;
					if (line.Trim() != "") ex += "-> " + line + "<br>";
				}
				throw new CustomArgumentException(exceptionThatOccurred.Message + " <br> " + ex, "GENERIC_ERROR");
			}

			return new ObjectResult("Error handling failed");
		}
	}


	public class RequestBase : Controller {
		private AuthenticatedSession _currentSession;
		public Authentication Auth = new Authentication("");

		public string instance {
			get {
				if (currentSession != null) {
					return currentSession.instance;
				}

				return "";
			}
		}

		public AuthenticatedSession currentSession {
			get {
				//Restore session to _currentSession variable
				if (_currentSession == null) _currentSession = RestoreSession();

				//Session does not exist -- user must reauthenticate
				if (_currentSession == null) {
					//Session has expired, but user might be authenticated -- Look up from the database if cookie is valid
					if (Auth.IsCookieValid(null, HttpContext)) {
						//Return the new session created in IsCookieValid
						_currentSession = RestoreSession();

						Diag.Log(instance,
							"Cookie (Login) Authentication: Success -- User: '" + _currentSession.GetMember("login") +
							"'.",
							_currentSession.item_id, Diag.LogSeverities.Message, Diag.LogCategories.Authentication);
					} else {
						//Windows Authentication might be able to log user in and create session and cookie
						if (Auth.WindowsAuthentication(HttpContext)) {
							_currentSession = RestoreSession();
						} else {
							//Session doesn't exist and user cannot be authenticated. Response is 401: Unauthorized
							//Diag.Log("", "Session doesn't exist nor can the user be authenticated. Response is 401: Unauthorized 1/2.", 0, Diag.LogSeverities.warning);
							Response.StatusCode = 401;
							return null;
						}
					}
				}

				//Session established
				return _currentSession;
			}
			set => StoreSession(value);
		}

		public void SetTemporarySession(AuthenticatedSession tempSession) {
			_currentSession = tempSession;
		}


		private AuthenticatedSession RestoreSession() {
			var state = new State("", HttpContext);
			return (AuthenticatedSession) Conv.DeserializeToObject(state.fromSession("AuthenticatedUserData"));;
		}

		private void StoreSession(AuthenticatedSession value) {
			var state = new State("", HttpContext);
			_currentSession = value;
			state.toSession("AuthenticatedUserData", Conv.SerializeToString(value));
		}
	}
}