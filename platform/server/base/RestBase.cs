﻿using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.InkML;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Lucene.Net.Documents;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;


//Rest Base Class
namespace Keto5.x.platform.server.@base {
	public class RestBase : RequestBase {
		public enum FailureCodes {
			Success = 200,
			FailureInRequest = 400,
			ForbiddenAction = 403,
			FailureOnServer = 500
		}

		private string _currentRequestId;
		private readonly DateTime _currentRequestStart = DateTime.UtcNow;
		public APIResult APIResult = new APIResult();
		public string process = "";

		public string success(object result) {
			Response.StatusCode = 200;
			return JsonConvert.SerializeObject(result);
		}

		public string success(APIResult result) {
			return success((object) result);
		}

		public string success() {
			return success(null);
		}

		public string failure(APIResult result, FailureCodes code, Exception ex = null) {
			Response.StatusCode = (int) code;

			if (ex != null) {
				Diag.Log(instance, ex, currentSession.item_id, Diag.LogCategories.System);
				result.data = ex.Message;
			}

			return JsonConvert.SerializeObject(result);
		}

		public string failure(FailureCodes code, Exception ex = null) {
			return failure(APIResult, code, ex);
		}

		public override void OnActionExecuting(ActionExecutingContext context) {
			var delay = 0;
			if (context.HttpContext.Request.Headers.ContainsKey("RequestInitiated")) {
				var c = Conv.ToStr(context.HttpContext.Request.Headers["RequestInitiated"]).Split(" ");
				var d = c[0].Split("-");
				var t = c[1].Split(":");

				var dt = new DateTime(Conv.ToInt(d[0]), Conv.ToInt(d[1]), Conv.ToInt(d[2]), Conv.ToInt(t[0]),
					Conv.ToInt(t[1]), Conv.ToInt(t[2]), Conv.ToInt(t[3]));
				delay = Conv.ToInt(DateTime.Now.ToUniversalTime().Subtract(dt).TotalMilliseconds);
			}
			
			if (currentSession != null)
				_currentRequestId = Diag.LogRestRequest(instance, context, currentSession, delay, context.HttpContext.Request.Method, _currentRequestStart);


			if (Response.StatusCode != 401) {
				//Rest code is about to be executed. Validate session or cookie
				if (Auth.IsCookieValid(currentSession, HttpContext)) {
					//Execute Requested Action
					RequestSecurity.RequestPathCheck(context.ActionArguments);
					base.OnActionExecuting(context);
				} else {
					//Response is 401: Unauthorized
					Response.StatusCode = 401;

					context.Result = new EmptyResult();
					//base.OnActionExecuting(context);
				}
			} else {
				throw new CustomPermissionException("Unauthorised");
			}
		}

		public override void OnActionExecuted(ActionExecutedContext context) {
			if (currentSession != null)
				Diag.LogRestRequestUpdate(_currentRequestId, _currentRequestStart);
		}
	}

	[Serializable]
	public class APIResult {
		public object data;
	}

	[Serializable]
	public class APISaveResult {
		public Dictionary<string, object> Data;
		public Dictionary<int, Dictionary<int, Dictionary<string, object>>> LinkData;
		public DateTime? modifyDate;
		public int? nextArchiveId;
		public int? previousArchiveId;
		public bool statesChanged;
	}
}