﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.@base {
	/// <summary>
	/// Work in progress.
	/// Goal is to get rid of this completely.
	/// Good ol' ItemBase. This is currently split into multiple files.
	/// DatabaseItemService handles actions for plain database objects
	/// FavouriteService handles favourites (doh!)
	/// ItemCollectionService handles collection of entities. Used with searches.
	/// </summary>
	public partial class ItemBase : SessionBaseInfo, IDatabaseItemService {
		public Dictionary<string, object> GetItem(int itemId, bool cache = true, bool checkRights = true,
			bool onlySelectorColumns = false, bool onlyConditionColumns = false) {
			return GetEntityService().GetItem(itemId, cache, checkRights, onlySelectorColumns, onlyConditionColumns);
		}

		public DataTable GetAllData() {
			return GetAllData("");
		}

		public DataTable GetAllData(string idStr = "", string order = "", string extraWhere = "") {
			return GetEntityService().GetAllData(idStr, order, extraWhere);
		}

		public Dictionary<string, object> GetItem(int itemId, DataTable data, bool checkRights = true,
			bool onlyConditionColumns = false) {
			throw new NotImplementedException("Deprecated");
		}

		public bool IsUniqueColumn(int columnId, int itemId, string value) {
			throw new NotImplementedException();
		}

		public List<Dictionary<string, object>> GetArchiveReport(int itemId, int itemColumnId) {
			return EntityRepository.GetArchiveReport(itemId, itemColumnId);
		}

		public List<Dictionary<string, object>> GetArchiveReport(int itemId, string itemColumnName) {
			return EntityRepository.GetArchiveReport(itemId, itemColumnName);
		}

		public List<Dictionary<string, int>> GetArchiveMonthly(int itemId) {
			var retval = new List<Dictionary<string, int>>();
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			foreach (DataRow row in ProcessBase.db.GetDatatable(
				         "SELECT YEAR(archive_end) as year, MONTH(archive_end) AS month, MAX(archive_id) as archive_id FROM archive_" +
				         ProcessBase.tableName +
				         "  WHERE item_id = @itemId GROUP BY YEAR(archive_end), MONTH(archive_end) ORDER BY YEAR(archive_end), MONTH(archive_end) ",
				         ProcessBase.DBParameters).Rows) {
				var d = new Dictionary<string, int>();
				d.Add("year", Conv.ToInt(row["year"]));
				d.Add("month", Conv.ToInt(row["month"]));
				retval.Add(d);
			}

			return retval;
		}

		private Dictionary<string, object> GetArchiveItem(int itemId, int archiveId) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@archiveId", archiveId);
			ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate",
				ProcessBase.db.ExecuteScalar(
					"SELECT archive_start FROM archive_" + ProcessBase.tableName + " WHERE archive_id = @archiveId",
					ProcessBase.DBParameters));

			var icsSql = ItemColumns;

			var dt = ProcessBase.db.GetDatatable(
				icsSql.GetItemSql(archiveType: 1) + " WHERE item_id=@itemId AND archive_id=@archiveId",
				ProcessBase.DBParameters);

			if (dt.Rows.Count > 0) {
				var result = Enumerable.Range(0, dt.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => dt.Rows[0].Table.Columns[i].ColumnName,
						i => dt.Rows[0][dt.Rows[0].Table.Columns[i]]
					);

				return result;
			}

			throw new CustomArgumentException("Archived Item cannot be found using item id " + itemId +
			                                  " and archive id " +
			                                  archiveId);
		}

		public Dictionary<string, object> GetArchiveItem(int itemId, DateTime archiveDate,
			bool RaiseExceptionIfNotFound = true) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
			var dbs = new DatabaseItemService(Instance, Process, AuthenticatedSession);
			var userCols = dbs.GetUserCols(itemId, 0, new[] { "meta.read", "meta.write" });
			var icsSql = ItemColumns;
			var sql = icsSql.GetItemSql(archiveType: 2, item_id: itemId, cols: userCols) + " WHERE item_id=@itemId ";
			var dt = ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);

			if (dt.Rows.Count > 0) {
				var result = Enumerable.Range(0, dt.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => dt.Rows[0].Table.Columns[i].ColumnName,
						i => dt.Rows[0][dt.Rows[0].Table.Columns[i]]
					);

				var idps = GetItemDataProcessSelectionsObject();
				foreach (var ic in icsSql.GetItemColumns()) {
					if (!userCols.Contains(ic.Name.TrimEnd("_str"))) continue;
					if (ic.IsProcess() || ic.IsList()) {
						result.Add(ic.Name,
							idps.GetArchiveItemDataProcessSelectionIds(archiveDate, itemId, ic.ItemColumnId));
					} else if (ic.IsSubquery() &&
					           (ic.IsOfSubType(ItemColumn.ColumnType.Process) ||
					            ic.IsOfSubType(ItemColumn.ColumnType.List))) {
						if (result[ic.Name + "_str"] != null && !DBNull.Value.Equals(result[ic.Name + "_str"])) {
							result.Add(ic.Name,
								Array.ConvertAll(Conv.ToStr(result[ic.Name + "_str"]).Split(','), Conv.ToInt)
							);
						} else {
							result.Add(ic.Name, null);
						}

						result.Remove(ic.Name + "_str");
					} else if (ic.IsEquation() &&
					           (Conv.ToInt(ic.DataAdditional2) == 5 || Conv.ToInt(ic.DataAdditional2) == 6)) {
						if (Conv.ToStr(result[ic.Name]) == "")
							result[ic.Name] = new List<int>();
						else
							result[ic.Name] =
								Array.ConvertAll(Conv.ToStr(result[ic.Name]).Split(','), s => int.Parse(s));
					} else if (ic.IsRich()) {
						result[ic.Name] = JsonConvert.DeserializeObject(Conv.ToStr(result[ic.Name]));
					}
				}

				return result;
			}

			if (RaiseExceptionIfNotFound)
				throw new CustomArgumentException("Archived Item cannot be found using item id " + itemId +
				                                  " and archive date " +
				                                  archiveDate.ToShortTimeString());

			return null;
		}

		private ItemDataProcessSelections GetItemDataProcessSelectionsObject(string process = null) {
			if (process == null) {
				process = Process;
			}

			return new ItemDataProcessSelections(Instance, process, AuthenticatedSession);
		}

		public APISaveResult GetRow(int itemId) {
			var data = new APISaveResult();
			if (itemId == 0) return data;

			data.Data = DatabaseItemService.GetItem(itemId);
			if (data.Data.ContainsKey("archive_start")) {
				data.modifyDate = Conv.ToDateTime(data.Data["archive_start"]);
			} else {
				data.modifyDate = DateTime.Now.ToUniversalTime();
			}

			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			var prevArchiveId =
				ProcessBase.db.ExecuteScalar(
					"SELECT MAX(archive_id) FROM archive_" + ProcessBase.tableName + " WHERE item_id = @itemId",
					ProcessBase.DBParameters);
			if (prevArchiveId != DBNull.Value) {
				data.previousArchiveId = Conv.ToInt(prevArchiveId);
			} else {
				data.previousArchiveId = null;
			}


			var linkData = new Dictionary<int, Dictionary<int, Dictionary<string, object>>>();
			var idps = new ItemDataProcessSelections(ProcessBase.instance, Process,
				ProcessBase.authenticatedSession);

			foreach (var ic in ItemColumns.GetItemColumns()) {
				if ((ic.DataType == 5 || ic.DataType == 6 || ic.DataType == 12) && ic.LinkProcess != null &&
				    !ic.Name.Equals("linked_tasks")) {
					linkData.Add(ic.ItemColumnId,
						idps.GetItemDataProcessSelectionIds(itemId, ic.ItemColumnId, ic.LinkProcess));
				}
			}

			data.LinkData = linkData;

			return data;
		}

		public APISaveResult GetArchiveRow(int itemId, int archiveId) {
			var data = new APISaveResult();
			data.Data = GetArchiveItem(itemId, archiveId);
			data.modifyDate = Conv.ToDateTime(data.Data["archive_end"]);

			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@archiveId", archiveId);
			var prevArchiveId =
				ProcessBase.db.ExecuteScalar(
					"SELECT TOP 1 archive_id FROM archive_" + ProcessBase.tableName +
					" WHERE item_id = @itemId AND archive_id < @archiveId ORDER BY archive_id DESC",
					ProcessBase.DBParameters);
			if (prevArchiveId != DBNull.Value) {
				data.previousArchiveId = Conv.ToInt(prevArchiveId);
			} else {
				data.previousArchiveId = null;
			}

			ProcessBase.SetDBParam(SqlDbType.Int, "@archiveId", archiveId);
			var nextArchiveId =
				ProcessBase.db.ExecuteScalar(
					"SELECT TOP 1 archive_id FROM archive_" + ProcessBase.tableName +
					" WHERE item_id = @itemId AND archive_id > @archiveId  ORDER BY archive_id",
					ProcessBase.DBParameters);
			if (nextArchiveId != DBNull.Value) {
				data.nextArchiveId = Conv.ToInt(nextArchiveId);
			} else {
				data.nextArchiveId = null;
			}

			var ic = ItemColumns;
			var idpss = new ItemDataProcessSelections(Instance, Process, AuthenticatedSession);
			foreach (var iCol in ic.GetItemColumns()) {
				if (iCol.IsProcess() || iCol.IsList()) {
					data.Data.Add(iCol.Name,
						idpss.GetArchiveItemDataProcessSelectionIds(data.modifyDate, itemId, iCol.ItemColumnId));
				}
			}

			return data;
		}

		public APISaveResult GetArchiveRow(int itemId, DateTime archiveDate) {
			var data = new APISaveResult();
			data.Data = GetArchiveItem(itemId, archiveDate);
			data.modifyDate = (DateTime)data.Data["archive_start"];

			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveEnd", data.Data["archive_start"]);
			var prevArchiveId =
				ProcessBase.db.ExecuteScalar(
					"SELECT TOP 1 archive_id FROM archive_" + ProcessBase.tableName +
					" WHERE item_id = @itemId AND archive_end < @archiveEnd ORDER BY archive_id DESC",
					ProcessBase.DBParameters);
			if (prevArchiveId != DBNull.Value) {
				data.previousArchiveId = Conv.ToInt(prevArchiveId);
			} else {
				data.previousArchiveId = null;
			}

			var nextArchiveId =
				ProcessBase.db.ExecuteScalar(
					"SELECT TOP 1 archive_id FROM archive_" + ProcessBase.tableName +
					" WHERE item_id = @itemId AND archive_end > @archiveEnd  ORDER BY archive_id",
					ProcessBase.DBParameters);
			if (nextArchiveId != DBNull.Value) {
				data.nextArchiveId = Conv.ToInt(nextArchiveId);
			} else {
				data.nextArchiveId = null;
			}

			return data;
		}

		public List<Dictionary<string, object>> GetItems(Dictionary<string, object> search) {
			var searchSql = EntityHelpers.ConvertSearchDictionaryToSql(search);
			var result = new List<Dictionary<string, object>>();
			var icsSql = ItemColumns;
			var dt =
				ProcessBase.db.GetDatatable(
					icsSql.GetItemSql() + " WHERE " + searchSql + " ORDER BY order_no  ASC ",
					ProcessBase.DBParameters);
			foreach (DataRow row in dt.Rows) {
				var result2 = Enumerable.Range(0, dt.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => dt.Rows[0].Table.Columns[i].ColumnName,
						i => row[dt.Rows[0].Table.Columns[i]]
					);

				var idps = GetItemDataProcessSelectionsObject();
				foreach (var ic in icsSql.GetItemColumns()) {
					if (ic.IsProcess() || ic.IsList()) {
						result2.Add(ic.Name,
							idps.GetItemDataProcessSelectionIds(Conv.ToInt(row["item_id"]), ic.ItemColumnId));
					}
				}

				result.Add(result2);
			}

			return result;
		}

		public List<Dictionary<string, object>> GetItems(Dictionary<string, object> search, string orderBy, List<string> cols = null) {
			var param = new List<SqlParameter>();
			var searchSql = EntityHelpers.ConvertSearchDictionaryToSql(search);

			var result = new List<Dictionary<string, object>>();
			var icsSql = ItemColumns;
			var dt =
				ProcessBase.db.GetDatatable(
					icsSql.GetItemSql(cols: cols) + " WHERE " + searchSql + " ORDER BY " + orderBy,
					param);
			foreach (DataRow row in dt.Rows) {
				var result2 = Enumerable.Range(0, dt.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => dt.Rows[0].Table.Columns[i].ColumnName,
						i => row[dt.Rows[0].Table.Columns[i]]
					);

				var idps = GetItemDataProcessSelectionsObject();
				foreach (var ic in icsSql.GetItemColumns()) {
					if (ic.IsProcess() || ic.IsList()) {
						result2.Add(ic.Name,
							idps.GetItemDataProcessSelectionIds(Conv.ToInt(row["item_id"]), ic.ItemColumnId));
					}
				}

				result.Add(result2);
			}

			return result;
		}

		public List<Dictionary<string, object>>
			GetArchiveItems(Dictionary<string, object> search, DateTime archiveDate) {
			var searchSql = EntityHelpers.ConvertSearchDictionaryToSql(search);

			ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);

			var result = new List<Dictionary<string, object>>();
			var icsSql = ItemColumns;
			var dt =
				ProcessBase.db.GetDatatable(icsSql.GetItemSql(archiveType: 2) + " WHERE " +
				                            searchSql, ProcessBase.DBParameters);
			foreach (DataRow row in dt.Rows) {
				var result2 = Enumerable.Range(0, dt.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => dt.Rows[0].Table.Columns[i].ColumnName,
						i => row[dt.Rows[0].Table.Columns[i]]
					);

				var idps = GetItemDataProcessSelectionsObject();
				foreach (var ic in icsSql.GetItemColumns()) {
					if (ic.IsProcess() || ic.IsList()) {
						result2.Add(ic.Name,
							idps.GetArchiveItemDataProcessSelectionIds(archiveDate, Conv.ToInt(row["item_id"]),
								ic.ItemColumnId));
					}
				}

				result.Add(result2);
			}

			return result;
		}

		public int GetParentItemId(int itemId) {
			return EntityRepository.GetParentItemId(itemId);
		}

		public int GetParentItemId(int itemId, string process) {
			return EntityRepository.GetParentItemId(itemId, process);
		}

		public List<int> GetItemIds() {
			return EntityRepository.GetItemIds();
		}

		public List<int> GetItemIds(string order) {
			return EntityRepository.GetItemIds(order);
		}

		public string getPhaseFilterWhere(List<int> filterPhases) {
			var mStates = GetStates();
			string[] states = { "progressProgress.green", "progressProgress.red", "progressProgress.yellow" };
			var groupStates =
				mStates.GetStatesForGroups(Process, "progressProgress", states.ToList());

			var exps = new Dictionary<int, string>();

			foreach (var groupId in filterPhases) {
				if (!exps.ContainsKey(Conv.ToInt(groupId))) {
					exps.Add(Conv.ToInt(groupId), " ( 1=0 ");
				}

				foreach (var state in groupStates.Keys) {
					if (groupStates[state].ContainsKey(Conv.ToInt(groupId))) {
						foreach (var exp in groupStates[state][Conv.ToInt(groupId)]) {
							exps[Conv.ToInt(groupId)] = exps[Conv.ToInt(groupId)] + " OR " + exp + " ";
						}
					}
				}

				exps[Conv.ToInt(groupId)] = exps[Conv.ToInt(groupId)] + " ) ";
			}

			var filterWhere = " ( 1=0 ";

			foreach (var gId in exps.Keys) {
				filterWhere += " OR " + exps[gId];
			}

			filterWhere += " ) ";


			filterWhere = filterWhere.Replace("true  == true", "1=1").Replace("== true", "").Replace(" == ", "=")
				.Replace("true", "1=1").Replace("false", "0=1");

			return filterWhere;
		}

		public DataTable GetFilteredData(string filters, string textsearch) {
			var columnList = ItemColumns.GetItemColumnListForSql();
			var conds = "(1=0)";
			if (!string.IsNullOrEmpty(textsearch)) {
				var textSearchInLower = textsearch.ToLower();
				var textSearchParam = ProcessBase.SetDBParameter("@textsearch", "%" + textSearchInLower + "%");
				foreach (var colNam in columnList) {
					conds += " OR LOWER(" + colNam + ") LIKE @textsearch ";
				}
			} else {
				conds = "(1=1)";
			}

			conds = " AND (" + conds + ") ";

			var sql = ItemColumns.GetItemSql() + "  WHERE (1=1) " + conds;
			return ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);
		}

		public DataTable GetContainerCols() {
			return ProcessBase.db.GetDatatable(
				"SELECT  process_container_id, ic.item_column_id, name, data_type FROM process_container_columns pcc INNER JOIN item_columns ic ON pcc.item_column_id = ic.item_column_id WHERE instance=@instance AND process=@process",
				ProcessBase.DBParameters);
		}

		private IDatabaseItemService GetEntityService() {
			return DatabaseItemService;
		}

		public Dictionary<string, object> GetItem(Dictionary<string, object> search, bool checkRights = true) {
			throw new NotImplementedException("Deprecated");
		}

		public int GetItemIdByColumn(List<DataVal> dataVals) {
			return EntityRepository.GetItemIdByColumn(dataVals);
		}

		public int GetItemIdByColumn(string col, string val) {
			ProcessBase.SetDBParam(SqlDbType.NVarChar, "@val", val);
			return (int)ProcessBase.db.ExecuteScalar(
				" SELECT TOP 1 COALESCE(item_id, 0) FROM " + ProcessBase.tableName + " WHERE " + col + " = @val",
				ProcessBase.DBParameters);
		}

		public Dictionary<string, object> GetLinkItem(int itemId, int itemColumnId, int selectedItemId,
			DateTime? archiveDate = null) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@selectedItemId", selectedItemId);

			var dt = ProcessBase.db.GetDatatable(
				" SELECT link_item_id FROM item_data_process_selections WHERE item_id=@itemId AND item_column_id = @itemColumnId AND selected_item_id = @selectedItemId ",
				ProcessBase.DBParameters);


			if (dt.Rows.Count > 0) {
				if (DBNull.Value.Equals(dt.Rows[0]["link_item_id"])) {
					var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process,
						ProcessBase.authenticatedSession);
					var linkCol = ics.GetItemColumn(itemColumnId);

					if (linkCol.LinkProcess != null) {
						var linkIb = new ItemBase(ProcessBase.instance, linkCol.LinkProcess,
							ProcessBase.authenticatedSession);
						var linkItemId = linkIb.InsertItemRow();
						ProcessBase.SetDBParam(SqlDbType.Int, "@linkItemId", linkItemId);
						ProcessBase.db.ExecuteUpdateQuery(
							"UPDATE item_data_process_selections SET link_item_id = @linkItemId WHERE item_id=@itemId AND item_column_id = @itemColumnId AND selected_item_id = @selectedItemId ",
							ProcessBase.DBParameters);

						return linkIb.GetItem(linkItemId);
					}

					return null;
				} else {
					var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process,
						ProcessBase.authenticatedSession);
					var linkCol = ics.GetItemColumn(itemColumnId);
					var linkIb = new ItemBase(ProcessBase.instance, linkCol.LinkProcess,
						ProcessBase.authenticatedSession);
					if (archiveDate != null)
						return linkIb.GetArchiveItem(Conv.ToInt(dt.Rows[0]["link_item_id"]), (DateTime)archiveDate,
							false);
					return linkIb.GetItem(Conv.ToInt(dt.Rows[0]["link_item_id"]));
				}
			}

			{
				var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process,
					ProcessBase.authenticatedSession);
				var linkCol = ics.GetItemColumn(itemColumnId);
				var linkIb = new ItemBase(ProcessBase.instance, linkCol.LinkProcess,
					ProcessBase.authenticatedSession);
				var linkItemId = linkIb.InsertItemRow();
				ProcessBase.SetDBParam(SqlDbType.Int, "@linkItemId", linkItemId);
				ProcessBase.db.ExecuteInsertQuery(
					"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id) VALUES(@itemId, @itemColumnId, @selectedItemId, @linkItemId) ",
					ProcessBase.DBParameters);
				return linkIb.GetItem(linkItemId);
			}
		}

		public Dictionary<string, object> GetBaselineData(int itemId, int type = 0) {
			var retval = new Dictionary<int, Dictionary<string, object>>();
			var processBaseline =
				new ProcessBaseline(Instance, Process, AuthenticatedSession);
			var groups = new ProcessGroups(Instance, Process, AuthenticatedSession);

			var baselineDates = processBaseline.GetBaselineDates(itemId, true, type);

			var utcNow = DateTime.UtcNow;
			var translations = new Translations(Instance, AuthenticatedSession);

			var currentBaseLine = new Dictionary<string, object> {
				{ "process_baseline_id", 0 },
				{ "instance", Instance },
				{ "process_item_id", Process },
				{ "baseline_date", utcNow },
				{ "process_group_id", null },
				{ "creator_user_item_id", AuthenticatedSession.item_id },
				{ "description", translations.GetTranslation("CURRENT_DATE") },
				{ "archive_user_id", AuthenticatedSession.item_id },
				{ "archive_start", utcNow }
			};
			baselineDates.Add(currentBaseLine);

			foreach (var baselineRow in baselineDates) {
				if (Conv.ToInt(baselineRow["process_group_id"]) > 0) {
					baselineRow["description"] =
						groups.GetGroup(Conv.ToInt(baselineRow["process_group_id"]))
							.LanguageTranslation[AuthenticatedSession.locale_id];
				}

				var archiveData = GetArchiveItem(itemId, (DateTime)baselineRow["baseline_date"]);
				retval.Add(Conv.ToInt(baselineRow["process_baseline_id"]), archiveData);
			}

			var retval2 = new Dictionary<string, object>();

			retval2.Add("dates", baselineDates);
			retval2.Add("data", retval);
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			var table = Conv.ToSql("_" + Instance + "_" + Process);
			retval2.Add("start_date", ProcessBase.db.ExecuteScalar(" select min(archive_start) from ( " +
			                                                       " select archive_start from " + table +
			                                                       " where item_id = @itemId " +
			                                                       " union all " +
			                                                       " select archive_start from archive_" + table +
			                                                       " where item_id = @itemId) archives ",
				ProcessBase.DBParameters));

			return retval2;
		}

		public DataTable GetProcessSelections(string idStr) {
			return ProcessBase.db.GetDatatable(
				"SELECT item_id, item_column_id, selected_item_id FROM item_data_process_selections WHERE item_id IN (" +
				idStr +
				")", ProcessBase.DBParameters);
		}
	}
}