﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.@base {
	/// <summary>
	/// Work in progress.
	/// Goal is to get rid of this completely.
	/// Good ol' ItemBase. This is currently split into multiple files.
	/// DatabaseItemService handles actions for plain database objects
	/// FavouriteService handles favourites (doh!)
	/// ItemCollectionService handles collection of entities. Used with searches.
	/// </summary>
	public partial class ItemBase : SessionBaseInfo, IDatabaseItemService {
		public Dictionary<string, object> SaveItem(
			Dictionary<string, object> data,
			int? itemId,
			bool checkRights,
			FeatureStates featureStates,
			bool isArchiveSave,
			bool isLinkProcessSave, List<ItemColumn> cachedItemColumns = null, bool followsInsert = false,
			bool returnSubqueries = true,
			bool hadLinks = false,
			bool preventSpecialProcessChecks = false,
			int fileId = 0) {
			return DatabaseItemService.SaveItem(data, itemId, checkRights, featureStates, isArchiveSave,
				isLinkProcessSave, cachedItemColumns, followsInsert, returnSubqueries: returnSubqueries, hadLinks: hadLinks,
				preventSpecialProcessChecks: preventSpecialProcessChecks, fileId);
		}

		public Dictionary<string, object> SaveItem(
			Dictionary<string, object> data) {
			return SaveItem(data, null, false, null, false, false, null, false);
		}

		public Dictionary<string, object> SaveItem(
			Dictionary<string, object> data,
			int? itemId = null,
			bool checkRights = false,
			FeatureStates featureStates = null, List<ItemColumn> cachedItemColumns = null, bool followsInsert = false,
			bool returnSubqueries = true, bool hadLinks = false, int fileId = 0) {
			return SaveItem(data, itemId, checkRights, featureStates, false, false, cachedItemColumns, followsInsert,
				returnSubqueries: returnSubqueries, hadLinks: hadLinks, fileId :fileId);
		}

		private States GetStates() {
			return new States(Instance, AuthenticatedSession);
		}

		public Dictionary<string, object> SaveArchiveItem(Dictionary<string, object> data, int archiveId) {
			return SaveItem(data, archiveId, false, null, true, false);
		}

		public APISaveResult SaveRow(int itemId, APISaveResult data) {
			var isModified = IsRowModified(itemId, data.modifyDate);

			if (data.Data.Count > 0) {
				//Get States
				var states = GetStates();

				//Get Old Data for Notifications
				var hasNotifications = states.HasNotificatioConditions(Instance, Process);
				var oldData = new Dictionary<string, object>();
				if (hasNotifications) oldData = DatabaseItemService.GetItem(itemId, true, false);

				//Check if links need saving
				if (data.LinkData != null) {
					var p = new Processes(Instance, AuthenticatedSession);
					foreach (var ld in data.LinkData.Keys) {
						var linkProcess = p.GetLinkProcessForItemColum(ld);
						var LinkIb = new ItemBase(Instance, linkProcess, AuthenticatedSession);
						foreach (var linkItemId in data.LinkData[ld].Keys) {
							LinkIb.SaveItem(data.LinkData[ld][linkItemId], returnSubqueries: false);
						}
					}
				}

				var oldStates = states.GetEvaluatedStatesForFeature(Process, "meta", itemId);
				
				SaveItem(data.Data, itemId, true, oldStates,
					hadLinks: (data.LinkData != null && data.LinkData.Count > 0), returnSubqueries: true);

				//INVALIDATE CACHE
				InvalidateCache(itemId);
				Cache.Remove(Instance, "list_hierarchy_meta_" + Process, Conv.ToStr(itemId) + AuthenticatedSession.item_id);

				//Get New States after save is complete
				var newStates = states.GetEvaluatedStatesForFeature(Process, "meta", itemId);
				var statesChanged = HaveStatesChanged(oldStates, newStates);

				data.statesChanged = statesChanged;
				if (statesChanged) {
					var hidsToSave = new List<int>();
					foreach (var hid in oldStates.groupStates.Keys) {
						if (!oldStates.groupStates[hid].s.Contains("meta.history")) {
							if (newStates.groupStates.ContainsKey(hid) &&
							    newStates.groupStates[hid].s.Contains("meta.history")) {
								hidsToSave.Add(hid);
							}
						}
					}

					if (hidsToSave.Count > 0) {
						ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
						ProcessBase.SetDBParam(SqlDbType.Int, "@userId", AuthenticatedSession.item_id);
						foreach (var gid in hidsToSave) {
							ProcessBase.SetDBParam(SqlDbType.Int, "@processGroupId", gid);
							ProcessBase.db.ExecuteInsertQuery(
								"INSERT INTO process_baseline (instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id) VALUES (@instance, @process, @itemId, @processGroupId , DATEADD(SECOND, 3, GETUTCDATE()) , @userId)",
								ProcessBase.DBParameters);
						}
					}
				}

				if (hasNotifications) {
					var newData = DatabaseItemService.GetItem(itemId, false, checkRights: false);
					states.CheckNotifications(Process, itemId, oldData, newData);
				}
			}

			if (isModified) data = GetRow(itemId);
			return data;
		}

		public void SaveItemDataProcessSelection(int itemId, Dictionary<int, List<ItemDataProcessSelection>> data,
			bool checkRights = false, FeatureStates featureStates = null) {
			var userCols = new List<int>();
			if (checkRights) {
				FeatureStates fStates;
				if (featureStates == null) {
					var states = GetStates();
					fStates = states.GetEvaluatedStatesForFeature(Process, "meta", itemId);
				} else {
					fStates = featureStates;
				}

				var pcc = new ProcesContainerColumns(Instance, Process, AuthenticatedSession);
				foreach (var cId in fStates.GetContainerIds("write")) {
					foreach (var pcf in pcc.GetFields(cId)) {
						userCols.Add(pcf.ItemColumn.ItemColumnId);
					}
				}
			}

			foreach (var id in data.Keys) {
				if (!checkRights || userCols.Contains(id)) {
					ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
					ProcessBase.SetDBParam(SqlDbType.NVarChar, "@item_column_id", id);
					ProcessBase.db.ExecuteDeleteQuery(
						"DELETE FROM item_data_process_selections WHERE item_id = @item_id AND item_column_id = @item_column_id",
						ProcessBase.DBParameters);
					foreach (var selection in data[id]) {
						ProcessBase.SetDBParam(SqlDbType.NVarChar, "@selected_item_id", selection.SelectedItemId);
						ProcessBase.db.ExecuteInsertQuery(
							"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES(@item_id, @item_column_id, @selected_item_id)",
							ProcessBase.DBParameters);
					}

					Cache.Remove(Instance, "idps_" + itemId + " " + id);
					Cache.Remove(Instance, "idpsl_" + itemId + " " + id);
				}
			}
		}

		public void SaveItemDataProcessSelection(int item_id, Dictionary<int, List<ItemDataProcessSelection>> data) {
			foreach (var id in data.Keys) {
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@item_column_id", id);
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@item_id", item_id);
				ProcessBase.db.ExecuteDeleteQuery(
					"DELETE FROM item_data_process_selections WHERE item_id = @item_id AND item_column_id = @item_column_id",
					ProcessBase.DBParameters);
				foreach (var selection in data[id]) {
					ProcessBase.SetDBParam(SqlDbType.NVarChar, "@selected_item_id", selection.SelectedItemId);
					ProcessBase.db.ExecuteInsertQuery(
						"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES(@item_id, @item_column_id, @selected_item_id)",
						ProcessBase.DBParameters);
				}

				Cache.Remove(Instance, "idps_" + item_id + " " + id);
				Cache.Remove(Instance, "idpsl_" + item_id + " " + id);
			}
		}
	}
}