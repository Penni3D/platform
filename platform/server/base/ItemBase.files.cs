﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using HtmlToOpenXml;
using Keto5.x.platform.server.calendar;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using Newtonsoft.Json;


namespace Keto5.x.platform.server.@base {
	/// <summary>
	/// Work in progress.
	/// Goal is to get rid of this completely.
	/// Good ol' ItemBase. This is currently split into multiple files.
	/// DatabaseItemService handles actions for plain database objects
	/// FavouriteService handles favourites (doh!)
	/// ItemCollectionService handles collection of entities. Used with searches.
	/// </summary>
	public partial class ItemBase : SessionBaseInfo, IDatabaseItemService {
		private bool ColumnShown(ProcessPortfolioColumn pc, List<int> displayedColumns) {
			return (displayedColumns.Count == 0 || (pc.ReportColumn == 1 && displayedColumns.Count == 0) ||
					displayedColumns.Contains(pc.ItemColumn.ItemColumnId)) &&
				   pc.ReportColumn != 2 && pc.ItemColumn.DataType != 60;
		}

		public byte[] PortfolioExcel(int portfolioId, PortfolioOptions q, List<int> displayedColumns = null) {
			return GetExcelPackage(portfolioId, q, displayedColumns).GetAsByteArray();
		}

		public byte[] PortfolioCsv(int portfolioId, PortfolioOptions q = null, List<int> displayedColumns = null
			, bool create_mode = false, bool fromPortfolio = false, bool strictMode = true) {
			var csv = new StringBuilder("");
			if (q == null) q = new PortfolioOptions();

			var col_datatypes = new Dictionary<string, int>();
			var ppcs = new ProcessPortfolioColumns(Instance, Process, AuthenticatedSession);
			var cols = ppcs.GetColumns(portfolioId);
			foreach (var col in cols) {
				string title;
				if (col.ShortNameTranslation != null &&
					col.ShortNameTranslation.ContainsKey(AuthenticatedSession.locale_id) &&
					col.ShortNameTranslation[AuthenticatedSession.locale_id].Length > 0) {
					title = col.ShortNameTranslation[AuthenticatedSession.locale_id];
				} else if (col.ItemColumn.LanguageTranslation.ContainsKey(AuthenticatedSession.locale_id)) {
					title = col.ItemColumn.LanguageTranslation[AuthenticatedSession.locale_id];
				} else {
					title = col.ItemColumn.Name;
				}

				var datatype = col.ItemColumn.DataType;
				if (datatype == 16) {
					// Lookup
					datatype = col.ItemColumn.SubDataType;
				} else if (datatype == 20) {
					// Server Equation
					datatype = Conv.ToInt(col.ItemColumn.DataAdditional2);
				}

				col_datatypes.Add(title, datatype);
			}

			ExcelWorksheet worksheet;
			if (create_mode) {
				worksheet = CreateExcelPackage(displayedColumns, fromPortfolio, portfolioId, q, strictMode).Workbook
					.Worksheets[0];
			} else {
				worksheet = GetExcelPackage(portfolioId, q, displayedColumns).Workbook.Worksheets[0];
			}

			for (int c = 1; c <= worksheet.Dimension.Columns; c++) {
				if (c > 1) csv.Append(";");
				csv.Append(@"""" + Conv.ToStr(worksheet.Cells[1, c].Value).Replace(@"""", @"""""") + @"""");
			}

			for (int r = 2; r <= worksheet.Dimension.Rows; r++) {
				csv.Append(Environment.NewLine);
				for (int c = 1; c <= worksheet.Dimension.Columns; c++) {
					if (c > 1) csv.Append(";");
					if (col_datatypes[Conv.ToStr(worksheet.Cells[1, c].Value)].In(1, 2)) {
						// Integer & Float
						csv.Append(Conv.ToDouble(worksheet.Cells[r, c].Value));
					} else if (col_datatypes[Conv.ToStr(worksheet.Cells[1, c].Value)] == 3) {
						// Date
						if (Conv.ToDate(worksheet.Cells[r, c].Value) != null) {
							csv.Append(((DateTime)Conv.ToDate(worksheet.Cells[r, c].Value)).ToString("yyyy-MM.dd"));
						}
					} else if (col_datatypes[Conv.ToStr(worksheet.Cells[1, c].Value)] == 13) {
						// DateTime
						if (Conv.ToDateTime(worksheet.Cells[r, c].Value) != null) {
							csv.Append(
								((DateTime)Conv.ToDateTime(worksheet.Cells[r, c].Value))
								.ToString("yyyy-MM.dd hh:mm:ss"));
						}
					} else {
						// The Others
						csv.Append(@"""" + Conv.ToStr(worksheet.Cells[r, c].Value).Replace(@"""", @"""""") + @"""");
					}
				}
			}

			return Encoding.UTF8.GetBytes(csv.ToString());
		}

		public List<Dictionary<string, object>> PortfolioJson(int portfolioId, PortfolioOptions q = null,
			List<int> displayedColumns = null) {
			var table = new List<Dictionary<string, object>>();
			if (q == null) q = new PortfolioOptions();
			var worksheet = GetExcelPackage(portfolioId, q, displayedColumns).Workbook.Worksheets[0];
			for (int r = 2; r <= worksheet.Dimension.Rows; r++) {
				var row = new Dictionary<string, object>();
				for (int c = 1; c <= worksheet.Dimension.Columns; c++) {
					if (Conv.ToStr(worksheet.Cells[1, c].Value).Length > 0) {
						row.Add(Conv.ToStr(worksheet.Cells[1, c].Value), worksheet.Cells[r, c].Value);
					}
				}

				table.Add(row);
			}

			return table;
		}

		private ExcelPackage GetExcelPackage(int portfolioId, PortfolioOptions q, List<int> displayedColumns = null) {
			var data = FindItems(portfolioId, q);

			var p = new Processes(Instance, AuthenticatedSession);
			var ppcs = new ProcessPortfolioColumns(Instance, Process, AuthenticatedSession);

			var pck = new ExcelPackage();

			var code = p.GetProcess(Process);
			var sheetName = "Unknown";
			if (code.LanguageTranslation.ContainsKey(AuthenticatedSession.locale_id))
				sheetName = code.LanguageTranslation[AuthenticatedSession.locale_id];

			var ws = pck.Workbook.Worksheets.Add(sheetName);
			ws.View.ShowGridLines = true;
			var i = 1;
			var cols = ppcs.GetColumns(portfolioId);

			if (displayedColumns == null) displayedColumns = new List<int>();
			foreach (var pc in cols) {
				if (!ColumnShown(pc, displayedColumns)) continue;

				var groupname = "";
				if (pc.GroupName != "") {
					var t = new Translations(Instance, AuthenticatedSession);
					groupname = t.GetTranslation(pc.GroupName) + " ";
				}

				if (pc.ShortNameTranslation != null &&
					pc.ShortNameTranslation.ContainsKey(AuthenticatedSession.locale_id) &&
					pc.ShortNameTranslation[AuthenticatedSession.locale_id].Length > 0) {
					ws.Cells[1, i].Value = groupname + pc.ShortNameTranslation[AuthenticatedSession.locale_id];
				} else {
					if (pc.ItemColumn.LanguageTranslation.ContainsKey(AuthenticatedSession.locale_id)) {
						ws.Cells[1, i].Value =
							groupname + pc.ItemColumn.LanguageTranslation[AuthenticatedSession.locale_id];
					} else {
						ws.Cells[1, i].Value =
							groupname + pc.ItemColumn.Name;
					}
				}

				if (pc.Width != null && pc.Width > 0) ws.Column(i).Width = Conv.ToDouble(pc.Width) / 8;

				i++;
			}

			var i2 = 2;

			if (q.recursive) {
				RecursiveExcel(ws, i2, data, cols, "0", displayedColumns);
			} else {
				foreach (DataRow d in ((DataTable)data["rowdata"]).Rows) {
					i = 1;
					foreach (var c in cols) {
						if (!ColumnShown(c, displayedColumns)) continue;
						SetExcelCellVall(ws, c, i, i2, d, data, 0);
						i++;
					}

					i2++;
				}
			}

			return pck;
		}

		public int RecursiveExcel(ExcelWorksheet ws, int i2, Dictionary<string, object> data,
			List<ProcessPortfolioColumn> cols, string parentId, List<int> displayedColumns) {
			foreach (var d in ((DataTable)data["rowdata"]).Select("parent_item_id = '" + Conv.ToSql(parentId) + "'")) {
				var i = 1;
				var recursionLevel = parentId.Split('_').Length - 1;
				foreach (var c in cols) {
					if (!ColumnShown(c, displayedColumns)) continue;
					SetExcelCellVall(ws, c, i, i2, d, data, recursionLevel);
					i++;
				}

				i2++;
				i2 = RecursiveExcel(ws, i2, data, cols, Conv.ToStr(d["item_id"]), displayedColumns);
			}

			return i2;
		}

		private decimal GetExcelDecimalValueForDate(DateTime date) {
			var start = new DateTime(1900, 1, 1);
			var diff = date - start;
			return diff.Days + 2;
		}

		private string GetDateFormat(bool withTime = false) {
			if (AuthenticatedSession.date_format == "dd/mm/yy") {
				return "d/m/yyyy" + (withTime ? " h:mm" : "");
			} else if (AuthenticatedSession.date_format == "mm/dd/yy") {
				return "m/d/yyyy" + (withTime ? " h:mm" : "");
			} else if (AuthenticatedSession.date_format == "d M yyyy") {
				return "d mmm yyyy" + (withTime ? " h:mm" : "");
			} else if (AuthenticatedSession.date_format == "dd.mm.yy") {
				return "d.m.yyyy" + (withTime ? " h:mm" : "");
			} else {
				return "d.m.yyyy" + (withTime ? " h:mm" : "");
			}
		}

		private DateTime? ParseDate(DateTime? date) {
			if (date != null && ((DateTime)date).Year < 1970) {
				return null;
			}

			return date;
		}

		public void SetExcelCellVall(ExcelWorksheet ws, ProcessPortfolioColumn c, int i, int i2, DataRow d,
			Dictionary<string, object> data, int recursionLevel) {
			if (c.ItemColumn.IsDatabasePrimitive() || (c.ItemColumn.IsEquation() &&
													   (Conv.ToInt(c.ItemColumn.DataAdditional2) != 5 &&
														Conv.ToInt(c.ItemColumn.DataAdditional2) != 6)) ||
				c.ItemColumn.IsSubquery() && !c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Process) &&
				!c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.List) || c.ItemColumn.IsFormula()) {
				var val = d[c.ItemColumn.Name];

				if (c.ItemColumn.DataType == 0 || (c.ItemColumn.DataType == 16 && c.ItemColumn.SubDataType == 0))
					val = Conv.ToStr(val).TrimStart();

				if ((c.ItemColumn.IsDate() ||
					 (c.ItemColumn.IsEquation() && Conv.ToInt(c.ItemColumn.DataAdditional2) == 3) ||
					 (c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Date))) &&
					!DBNull.Value.Equals(val)) {
					ws.Cells[i2, i].Style.Numberformat.Format = GetDateFormat();
					val = ParseDate(Conv.ToDate(val));
				}

				if ((c.ItemColumn.IsDateTime() ||
					 (c.ItemColumn.IsEquation() && Conv.ToInt(c.ItemColumn.DataAdditional2) == 13) ||
					 (c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Datetime))) &&
					!DBNull.Value.Equals(val)) {
					ws.Cells[i2, i].Style.Numberformat.Format = GetDateFormat(true);
					val = ParseDate(Conv.ToDateTime(val));
				}

				if (recursionLevel > 0 && i == 1) {
					var recursionOffset = "";
					for (var x = 0; x <= recursionLevel; x++) {
						recursionOffset += "    ";
					}

					ws.Cells[i2, i].Value = recursionOffset + val;
				} else {
					ws.Cells[i2, i].Value = val;
				}
			} else if (c.ItemColumn.IsProcess() ||
					   (c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Process))) {
				var val2 = "";
				if (d[c.ItemColumn.Name] != DBNull.Value) {
					var colP = c.ItemColumn.IsSubquery() ? c.ItemColumn.SubDataAdditional : c.ItemColumn.DataAdditional;
					var ppcs2 =
						new ProcessPortfolioColumns(Instance, colP, AuthenticatedSession);
					var ids = (int[])d[c.ItemColumn.Name];
					foreach (var id in ids) {
						var val = "";
						foreach (var pc2 in ppcs2.GetPrimaryColumns()) {
							var pdata = (Dictionary<int, object>)data["processes"];

							Dictionary<string, object> rowItem;
							if (pdata.ContainsKey(id)) {
								rowItem = (Dictionary<string, object>)pdata[id];
							} else {
								var rowIb = new ItemBase(Instance, colP, AuthenticatedSession);
								rowItem = rowIb.GetItem(id, checkRights: false);
							}

							if (pc2.UseInSelectResult == 1) {
								if (val.Length > 0) {
									val += " " + rowItem[pc2.ItemColumn.Name];
								} else {
									val += rowItem[pc2.ItemColumn.Name];
								}
							}
						}

						if (val2.Length > 0) {
							val2 += ", " + val;
						} else {
							val2 += val;
						}
					}

					ws.Cells[i2, i].Value = val2;
				}
			} else if (c.ItemColumn.IsList() ||
					   (c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.List)) ||
					   (c.ItemColumn.IsEquation() && (Conv.ToInt(c.ItemColumn.DataAdditional2) == 6 ||
													  Conv.ToInt(c.ItemColumn.DataAdditional2) == 5))) {
				var val2 = "";
				if (d[c.ItemColumn.Name] != DBNull.Value) {
					var colP = c.ItemColumn.IsSubquery() ? c.ItemColumn.SubDataAdditional : c.ItemColumn.DataAdditional;
					if (c.ItemColumn.IsEquation()) colP = c.ItemColumn.InputName;

					var ids = (int[])d[c.ItemColumn.Name];
					foreach (var id in ids) {
						if (id == 0) {
							ws.Cells[i2, i].Value = "";
							continue;
						}

						var val = "";
						var pdata = (Dictionary<int, object>)data["processes"];

						if (c.ItemColumn.IsEquation() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Process)) {
							if (pdata.ContainsKey(id)) {
								val = new Processes(Instance, AuthenticatedSession).GetProcessPrimary(
									(Dictionary<string, object>)pdata[id]);
							} else {
								val = new Processes(Instance, AuthenticatedSession).GetProcessPrimary(id);
							}
						} else {
							Dictionary<string, object> rowItem;
							if (pdata.ContainsKey(id)) {
								rowItem = (Dictionary<string, object>)pdata[id];
							} else {
								var rowIb = new ItemBase(Instance, colP, AuthenticatedSession);
								rowItem = rowIb.GetItem(id, checkRights: false);
							}

							var co = "list_item";
							if (c.ItemColumn.IsSubquery()) {
								var subQuery = RegisteredSubQueries.GetType(c.ItemColumn.DataAdditional,
									c.ItemColumn.ItemColumnId);
								co = subQuery.GetColumnName();
							}

							if (val.Length > 0) {
								val += " " + (rowItem.ContainsKey(co) ? rowItem[co] : "?");
							} else {
								val += rowItem.ContainsKey(co) ? rowItem[co] : "?";
							}
						}


						if (val2.Length > 0) {
							val2 += ", " + val;
						} else {
							val2 += val;
						}
					}

					ws.Cells[i2, i].Value = val2;
				}
			} else if (c.ItemColumn.IsTag()) {
				if (d[c.ItemColumn.Name] != DBNull.Value) {
					var val = "";
					foreach (var tag in (string[])d[c.ItemColumn.Name]) {
						if (val.Length == 0) {
							val += tag;
						} else {
							val += ", " + tag;
						}
					}

					ws.Cells[i2, i].Value = val;
				} else {
					ws.Cells[i2, i].Value = "";
				}
			}
		}

		public string ConvertHtmlToBase64(string html) {
			var converter = new HtmlConverter(WordprocessingDocument
				.Create(new MemoryStream(), WordprocessingDocumentType.Document, false)
				.AddMainDocumentPart());
			var parsedhtml = converter.Parse(html);

			var sb = new StringBuilder();
			sb.Append("<w:p xmlns:w=\'http://schemas.openxmlformats.org/wordprocessingml/2006/main\'>");
			foreach (var phtml in parsedhtml) {
				sb.Append(phtml.InnerXml);
				sb.Append("<w:br/>");
			}

			sb.Append("</w:p>");

			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
			return "base64:" + System.Convert.ToBase64String(plainTextBytes);
		}

		public object SetWordCellValue(ProcessPortfolioColumn c, object valIn, Dictionary<string, object> data,
			Dictionary<string, int> colTypes = null, string locale_id = "en-GB") {
			if ((c.ItemColumn.IsRich() ||
				 (c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Text))) &&
				Conv.ToStr(valIn).Contains("html") && Conv.ToStr(valIn).Contains("delta")) {
				if (valIn != DBNull.Value) {
					var richJson = JToken.Parse(Conv.ToStr(valIn));
					var richHtml = richJson.Value<string>("html");
					return ConvertHtmlToBase64(richHtml);
				}
			}

			if (c.ItemColumn.IsNvarChar() || c.ItemColumn.IsText() ||
				(c.ItemColumn.IsSubquery() && (c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Nvarchar) ||
											   c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Text))) ||
				(c.ItemColumn.IsEquation() &&
				 (Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.Nvarchar ||
				  Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.Text))) {
				if (colTypes != null && !colTypes.ContainsKey(c.ItemColumn.Name)) {
					colTypes.Add(c.ItemColumn.Name, 0);
				}

				if (c.Process == "action_history" && c.ItemColumn.Name == "action_json") {
					try {
						var parsedJson = JsonConvert.DeserializeObject<Dictionary<string, string>>((string)valIn);

						if (parsedJson.ContainsKey(locale_id))
							return parsedJson[locale_id];

					} catch (Exception e) {
						Console.WriteLine(e);
					}
				}
				return valIn;
			}

			if (c.ItemColumn.IsInt() ||
				c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Int) ||
				c.ItemColumn.IsEquation() &&
				Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.Int) {
				return Util.FormatNumber(Conv.ToInt(valIn), locale_id);
			}

			if (c.ItemColumn.IsFloat() ||
				c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Float) ||
				c.ItemColumn.IsEquation() &&
				 Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.Float) {
				return Util.FormatNumber(Conv.ToDouble(valIn), locale_id);
			}

			if (c.ItemColumn.IsDate() ||
				(c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Date)) ||
				(c.ItemColumn.IsEquation() &&
				 Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.Date)) {
				if (colTypes != null && !colTypes.ContainsKey(c.ItemColumn.Name)) colTypes.Add(c.ItemColumn.Name, 0);
				var dateVal = Conv.ToDateTime(valIn);
				return dateVal != null ? Util.FormatDate((DateTime)dateVal, locale_id) : "";
			}

			if (c.ItemColumn.IsDateTime() ||
				(c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Datetime)) ||
				(c.ItemColumn.IsEquation() &&
				 Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.Datetime)) {
				if (colTypes != null && !colTypes.ContainsKey(c.ItemColumn.Name)) colTypes.Add(c.ItemColumn.Name, 0);
				var dateVal = Conv.ToDateTime(valIn);
				return dateVal != null ? Util.FormatDateTime((DateTime)dateVal, locale_id) : "";
			}

			if (c.ItemColumn.IsProcess() ||
				(c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Process)) ||
				(c.ItemColumn.IsEquation() &&
				 Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.Process)
			) {
				if (colTypes != null && !colTypes.ContainsKey(c.ItemColumn.Name)) {
					colTypes.Add(c.ItemColumn.Name, 0);
				}

				var val2 = "";
				if (valIn != DBNull.Value) {
					var colP = c.ItemColumn.IsSubquery() ? c.ItemColumn.SubDataAdditional : c.ItemColumn.DataAdditional;

					var ppcs2 =
						new ProcessPortfolioColumns(Instance, colP, AuthenticatedSession);
					var ids = (int[])valIn;
					foreach (var id in ids) {
						var val = "";
						foreach (var pc2 in ppcs2.GetPrimaryColumns()) {
							if (pc2.UseInSelectResult == 1) {
								var pdata =
									(Dictionary<int, object>)data["processes"];

								Dictionary<string, object> rowItem;
								if (pdata.ContainsKey(id)) {
									rowItem = (Dictionary<string, object>)pdata[id];
								} else {
									var rowIb =
										new ItemBase(Instance, colP, AuthenticatedSession);
									rowItem = rowIb.GetItem(id);
								}

								if (val.Length > 0) {
									val += " " + rowItem[pc2.ItemColumn.Name];
								} else {
									val += rowItem[pc2.ItemColumn.Name];
								}
							}
						}

						if (val2.Length > 0) {
							val2 += ", " + val;
						} else {
							val2 += val;
						}
					}

					return val2;
				}
			}

			if (c.ItemColumn.IsList() ||
				(c.ItemColumn.IsSubquery() && c.ItemColumn.IsOfSubType(ItemColumn.ColumnType.List)) ||
				(c.ItemColumn.IsEquation() &&
				 Conv.ToInt(c.ItemColumn.DataAdditional2) == (int)ItemColumn.ColumnType.List)
			) {
				var colP = c.ItemColumn.IsSubquery() || c.ItemColumn.IsEquation()
					? c.ItemColumn.SubDataAdditional
					: c.ItemColumn.DataAdditional;
				if (valIn == DBNull.Value) return valIn;
				var ids = (int[])valIn;
				var pdata =
					(Dictionary<int, object>)data["processes"];

				if ((c.ItemColumn.IsSubquery() || c.ItemColumn.IsEquation()) && c.ShowTypeCombined == 0) {
					var lp = new Processes(ProcessBase.instance, AuthenticatedSession);
					c.ShowTypeCombined = lp.GetListProcess(colP).PortfolioShowType;
				}

				if (valIn == DBNull.Value) return valIn;
				if (c.ShowTypeCombined == 0 || c.ShowTypeCombined == 1) {
					if (colTypes != null && !colTypes.ContainsKey(c.ItemColumn.Name)) {
						colTypes.Add(c.ItemColumn.Name, 0);
					}

					var retval = "";
					foreach (var id in ids) {
						Dictionary<string, object> rowItem;
						if (pdata.ContainsKey(id)) {
							rowItem = (Dictionary<string, object>)pdata[id];
						} else {
							var rowIb =
								new ItemBase(Instance, colP, AuthenticatedSession);
							rowItem = rowIb.GetItem(id);
						}

						var langSplit = locale_id.Split("-");
						var langItem = "list_item_" + langSplit[0] + "_" + langSplit[1].ToLower();
						var ifLangItem = rowItem.ContainsKey(langItem) ? rowItem[langItem] : rowItem["list_item"];

						if (retval.Length > 0) {
							retval += ", " + ifLangItem;
						} else {
							retval += ifLangItem;
						}
					}

					return retval;
				}

				if (c.ShowTypeCombined != 2 && c.ShowTypeCombined != 3) return valIn;
				{
					if (colTypes != null && !colTypes.ContainsKey(c.ItemColumn.Name)) {
						colTypes.Add(c.ItemColumn.Name, 0);
					}

					if (ids.Length <= 0) return "";
					var id = ids[0];
					Dictionary<string, object> rowItem;
					if (pdata.ContainsKey(id)) {
						rowItem = (Dictionary<string, object>)pdata[id];
					} else {
						var rowIb =
							new ItemBase(Instance, colP, AuthenticatedSession);
						rowItem = rowIb.GetItem(id);
					}

					if (c.ShowTypeCombined != 2) {
						return OfficeTemplater.SetColor(Conv.ToStr(rowItem["list_item"]),
							Conv.ToStr(rowItem["list_symbol_fill"]), Conv.ToStr(rowItem["list_symbol"]));
					} else {
						return OfficeTemplater.SetColor("", Conv.ToStr(rowItem["list_symbol_fill"]),
							Conv.ToStr(rowItem["list_symbol"]));
					}
				}
			}

			if (c.ItemColumn.IsTag()) {
				if (data[c.ItemColumn.Name] == DBNull.Value) return "";
				var val = "";
				foreach (var tag in (string[])data[c.ItemColumn.Name]) {
					if (val.Length == 0) {
						val += tag;
					} else {
						val += ", " + tag;
					}
				}

				return val;
			}

			return valIn;
		}

		public void GetExcelExportTemplate(HttpResponse r) {
			var pck = new ExcelPackage();
			var ws = pck.Workbook.Worksheets.Add(ProcessBase.process);

			var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process, ProcessBase.authenticatedSession);

			var cNum = 1;
			var handledProcesses = new List<string>();
			var cutNames = new List<string>();
			var duplicateIterator = 0;
			var processAbbrs = new Dictionary<string, string>();

			foreach (var c in ics.GetItemColumns()) {
				if (c.IsDatabasePrimitive() || c.IsProcess() || c.IsList()) {
					if (c.IsProcess() || c.IsList()) {
						var cProc = c.DataAdditional;
						if (cProc == ProcessBase.process) {
							Console.WriteLine("Skip column " + c.Name + " because same process ");
							continue;
						}
					}

					if (c.LanguageTranslation.ContainsKey(AuthenticatedSession.locale_id)) {
						ws.Cells[1, cNum].Value = c.LanguageTranslation[AuthenticatedSession.locale_id];
					} else {
						ws.Cells[1, cNum].Value = "?";
					}

					ws.Cells[2, cNum].Value = c.ItemColumnId;

					if (c.IsProcess() || c.IsList()) {
						var cProc = c.DataAdditional;
						if (cProc.Length > 29) {
							cProc = cProc.Substring(0, 29);
						}

						if (!handledProcesses.Contains(c.DataAdditional) && cProc != ProcessBase.process) {
							if (cutNames.Contains(cProc)) {
								duplicateIterator++;
								cProc = cProc.Remove(cProc.Length - 1, 1);
								cProc = cProc.Insert(cProc.Length, Conv.ToStr(duplicateIterator));
							}

							processAbbrs[c.DataAdditional] = cProc;

							var processWs = pck.Workbook.Worksheets.Add(cProc);

							var ppcs2 =
								new ProcessPortfolioColumns(Instance, c.DataAdditional, AuthenticatedSession);
							var primaryCols = ppcs2.GetPrimaryColumns();
							var dataIb =
								new ItemBase(Instance, c.DataAdditional, AuthenticatedSession);


							var allData = dataIb.GetAllData();

							if (c.IsProcess()) {
								var rowNum = 1;
								foreach (DataRow rowItem in allData.Rows) {
									var val = "";
									foreach (var pc2 in primaryCols) {
										if (pc2.UseInSelectResult == 1) {
											if (!rowItem.Table.Columns.Contains(pc2.ItemColumn.Name)) {
												val += " ";
											} else {
												if (val.Length > 0) {
													val += " " + rowItem[pc2.ItemColumn.Name];
												} else {
													val += rowItem[pc2.ItemColumn.Name];
												}
											}
										}
									}

									processWs.Cells[rowNum, 1].Value = rowItem["item_id"];
									processWs.Cells[rowNum, 2].Value = val + "#" + rowItem["item_id"] + "#";

									rowNum++;
									//formula.Formula.Values.Add(val);
								}
							} else if (c.IsList()) {
								var rowNum = 1;

								foreach (DataRow rowItem in allData.Rows) {
									var val = "" + rowItem["list_item"];
									processWs.Cells[rowNum, 1].Value = rowItem["item_id"];
									processWs.Cells[rowNum, 2].Value = val + " - #" + rowItem["item_id"] + "#";
									rowNum++;
									//formula.Formula.Values.Add(val);
								}
							}
						}

						handledProcesses.Add(c.DataAdditional);
						cutNames.Add(cProc);

						var range = ExcelCellBase.GetAddress(3, cNum, ExcelPackage.MaxRows, cNum);
						var formula = ws.DataValidations.AddListValidation(range);
						formula.ShowErrorMessage = true;
						formula.Formula.ExcelFormula = processAbbrs[c.DataAdditional] + "!$B:$B";
					}

					cNum++;
				}
			}

			//var ws2 = pck.Workbook.Worksheets.Add("process");
			//ws2.DataValidations.AddListValidation("LIST!A1:LIST!A3");

			r.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			r.Headers.Add("content-Disposition", "inline;filename=excel.xlsx");
			var bArr = pck.GetAsByteArray();
			r.ContentLength = bArr.Length;
			r.Body.WriteAsync(bArr, 0, bArr.Length);
		}

		public ExcelPackage CreateExcelPackage(List<int> columns, bool fromPortfolio, int portfolioId,
			PortfolioOptions q, bool strictMode) {
			var hideColumns = columns.Count > 0;
			var pck = new ExcelPackage();
			try {
				var ws = pck.Workbook.Worksheets.Add(ProcessBase.process);

				var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process, ProcessBase.authenticatedSession);
				var ppcs = new ProcessPortfolioColumns(ProcessBase.instance, ProcessBase.process,
					ProcessBase.authenticatedSession);

				var columnsInExcel = new List<ItemColumn>();

				var itemsByProcess = new Dictionary<string, Dictionary<int, string>>();

				ws.Column(1).Hidden = true;
				ws.Row(2).Hidden = true;
				string sql = "SELECT item_id, ";

				ws.Cells[1, 1].Value = "item_id";
				var cNum = 2;

				ws.View.FreezePanes(3, 3);

				var handledProcesses = new List<string>();
				var cutNames = new List<string>();
				var duplicateIterator = 0;
				var processAbbrs = new Dictionary<string, string>();

				var columnsToLoop = new List<ItemColumn>();

				if (fromPortfolio != true) {
					columnsToLoop = ics.GetItemColumns();
				} else {
					int i = 2;
					foreach (var column in ppcs.GetColumns(portfolioId)) {
						if (column.ReportColumn == 2 && strictMode) continue;

						//Dont add if optional show columns are sent from the client and column is not included
						if (!hideColumns || hideColumns && columns.Contains(column.ItemColumn.ItemColumnId)) {
							columnsToLoop.Add(column.ItemColumn);
						}

						if (column.ItemColumn.IsDatabasePrimitive() || column.ItemColumn.IsProcess() ||
							column.ItemColumn.IsList()) {
							if (column.Width != null && column.Width > 0)
								ws.Column(i).Width = Conv.ToDouble(column.Width) / 8;
							i++;
						}
					}
				}

				foreach (var c in columnsToLoop) {
					if (c.IsDatabasePrimitive() || c.IsProcess() || c.IsList()) {
						sql += c.Name + ",";

						if (c.IsProcess() || c.IsList()) {
							var cProc = c.DataAdditional;
							if (cProc == ProcessBase.process) {
								Console.WriteLine("Skip column " + c.Name + " because same process ");
								continue;
							}
						}

						if (c.LanguageTranslation.ContainsKey(AuthenticatedSession.locale_id)) {
							ws.Cells[1, cNum].Value = c.LanguageTranslation[AuthenticatedSession.locale_id];
							columnsInExcel.Add(c);
						} else {
							ws.Cells[1, cNum].Value = "?";
							columnsInExcel.Add(c);
						}

						ws.Cells[2, cNum].Value = c.ItemColumnId;
						//ws.Cells[3, cNum].Value = FillExcelCell(c.ItemColumnId, c.DataType, itemIds[0]);

						if (c.IsProcess() || c.IsList()) {
							var cProc = c.DataAdditional;
							if (cProc.Length > 29) {
								cProc = cProc.Substring(0, 29);
							}

							if (!handledProcesses.Contains(c.DataAdditional) && cProc != ProcessBase.process) {
								if (cutNames.Contains(cProc)) {
									duplicateIterator++;
									cProc = cProc.Remove(cProc.Length - 1, 1);
									cProc = cProc.Insert(cProc.Length, Conv.ToStr(duplicateIterator));
								}

								processAbbrs[c.DataAdditional] = cProc;

								var processWs = pck.Workbook.Worksheets.Add(cProc);
								processWs.Hidden = eWorkSheetHidden.Hidden;

								var ppcs2 = new ProcessPortfolioColumns(Instance, c.DataAdditional,
									AuthenticatedSession);
								var primaryCols = ppcs2.GetPrimaryColumns();
								var dataIb = new ItemBase(Instance, c.DataAdditional, AuthenticatedSession);

								string order = "";
								if (c.IsProcess()) {
									foreach (var pc2 in primaryCols) {
										if (pc2.UseInSelectResult == 1) {
											if (order.Length > 0) {
												order += ",";
											}

											order += pc2.ItemColumn.Name;
										}
									}
								} else if (c.IsList() && !itemsByProcess.ContainsKey(c.DataAdditional)) {
									ProcessBase.SetDBParam(SqlDbType.NVarChar, "process", c.DataAdditional);
									order = Conv.ToStr(ProcessBase.db.ExecuteScalar(
										"SELECT list_order FROM processes WHERE process = @process",
										ProcessBase.DBParameters));
								}

								var allData = dataIb.GetAllData("", order, "");

								if (c.IsProcess()) {
									var rowNum = 1;

									itemsByProcess.Add(c.DataAdditional, new Dictionary<int, string>());
									foreach (DataRow rowItem in allData.Rows) {
										var val = "";
										foreach (var pc2 in primaryCols) {
											if (pc2.UseInSelectResult == 1) {
												if (val.Length > 0) {
													val += " " + rowItem[pc2.ItemColumn.Name];
												} else {
													val += rowItem[pc2.ItemColumn.Name];
												}
											}
										}

										itemsByProcess[c.DataAdditional].Add(Conv.ToInt(rowItem["item_id"]),
											val + " - #" + rowItem["item_id"] + "#");
										processWs.Cells[rowNum, 1].Value = rowItem["item_id"];
										processWs.Cells[rowNum, 2].Value = val + " - #" + rowItem["item_id"] + "#";

										rowNum++;
										//formula.Formula.Values.Add(val);
									}
								} else if (c.IsList() && !itemsByProcess.ContainsKey(c.DataAdditional)) {
									var rowNum = 1;
									itemsByProcess.Add(c.DataAdditional, new Dictionary<int, string>());
									foreach (DataRow rowItem in allData.Rows) {
										var val = "" + rowItem["list_item"];
										processWs.Cells[rowNum, 1].Value = rowItem["item_id"];
										processWs.Cells[rowNum, 2].Value = val + " - #" + rowItem["item_id"] + "#";
										itemsByProcess[c.DataAdditional].Add(Conv.ToInt(rowItem["item_id"]),
											val + " - #" + rowItem["item_id"] + "#");
										rowNum++;
										//formula.Formula.Values.Add(val);
									}
								}
							}

							handledProcesses.Add(c.DataAdditional);
							cutNames.Add(cProc);
							var range = ExcelCellBase.GetAddress(3, cNum, ExcelPackage.MaxRows, cNum);
							var formula = ws.DataValidations.AddListValidation(range);
							formula.ShowErrorMessage = true;
							formula.Formula.ExcelFormula = processAbbrs[c.DataAdditional] + "!$B:$B";
						}

						cNum++;
					}
				}

				var processString = " FROM _" + ProcessBase.instance + "_" + ProcessBase.process;
				sql = sql.Remove(sql.Length - 1, 1).Insert(sql.Length - 1, processString);

				var rn = 3;
				var idps = new ItemDataProcessSelections(ProcessBase.instance, ProcessBase.process,
					ProcessBase.authenticatedSession);

				if (fromPortfolio) {
					var data = FindItems(portfolioId, q);
					var data2 = (DataTable)data["rowdata"];

					foreach (DataRow row in data2.Rows) {
						var colpos = 1;

						ws.Cells[rn, colpos].Value = Conv.ToInt(row["item_id"]);

						foreach (var itemcolumn in columnsInExcel) {
							if (itemcolumn.DataType == 5 || itemcolumn.DataType == 6) {
								var stringolingo = "";
								foreach (var selectedId in idps.GetItemDataProcessSelection(Conv.ToInt(row["item_id"]),
									itemcolumn.ItemColumnId)) {
									if (stringolingo.Length > 0) {
										stringolingo += ", ";
									}

									stringolingo +=
										itemsByProcess[itemcolumn.DataAdditional][selectedId.SelectedItemId];
								}

								ws.Cells[rn, colpos + 1].Value = stringolingo;
							} else if (itemcolumn.DataType == 3 || itemcolumn.DataType == 13) {
								if (Conv.ToStr(row[itemcolumn.Name]).Length > 1) {
									//	var dt = (DateTime)row[itemcolumn.Name];

									//ws.Cells[rn, colpos + 1].Value = dt.ToString(("dd/MM/yyyy"));

									var index = Conv.ToStr(row[itemcolumn.Name]).IndexOf(" ", 1);

									if (itemcolumn.DataType == 3) {
										ws.Cells[rn, colpos + 1].Style.Numberformat.Format = GetDateFormat();
										ws.Cells[rn, colpos + 1].Value = ParseDate(Conv.ToDate(row[itemcolumn.Name]));
									} else if (itemcolumn.DataType == 13) {
										ws.Cells[rn, colpos + 1].Style.Numberformat.Format = GetDateFormat(true);
										ws.Cells[rn, colpos + 1].Value =
											ParseDate(Conv.ToDateTime(row[itemcolumn.Name]));
									} else {
										ws.Cells[rn, colpos + 1].Value =
											Conv.ToStr(row[itemcolumn.Name]).Substring(0, index);
									}

									//SetExcelCellVall(ws, itemcolumn, colpos + 1, rn, row, null, 0);

									//These might help in the future...
									/*ws.Cells[rn, colpos + 1].Value = (DateTime)Conv.ToDateTime(row[itemcolumn.Name]);
									ws.Cells[rn, colpos + 1].Style.Numberformat.Format = GetDateFormat();*/
								} else {
									ws.Cells[rn, colpos + 1].Value = Conv.ToStr(row[itemcolumn.Name]);
								}
							} else {
								ws.Cells[rn, colpos + 1].Value = row[itemcolumn.Name];
							}

							colpos++;
						}

						rn++;
					}
				} else {
					foreach (DataRow row in ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters).Rows) {
						var colpos = 1;

						ws.Cells[rn, colpos].Value = Conv.ToInt(row["item_id"]);

						foreach (var itemcolumn in columnsInExcel) {
							if (itemcolumn.DataType == 5 || itemcolumn.DataType == 6) {
								var stringolingo = "";
								foreach (var selectedId in idps.GetItemDataProcessSelection(Conv.ToInt(row["item_id"]),
									itemcolumn.ItemColumnId)) {
									if (stringolingo.Length > 0) {
										stringolingo += ", ";
									}

									stringolingo +=
										itemsByProcess[itemcolumn.DataAdditional][selectedId.SelectedItemId];
								}

								ws.Cells[rn, colpos + 1].Value = stringolingo;
							} else if (itemcolumn.DataType == 3 || itemcolumn.DataType == 13) {
								if (Conv.ToStr(row[itemcolumn.Name]).Length > 1) {
									var index = Conv.ToStr(row[itemcolumn.Name]).IndexOf(" ", 1);

									if (itemcolumn.DataType == 3) {
										ws.Cells[rn, colpos + 1].Style.Numberformat.Format = GetDateFormat();
										ws.Cells[rn, colpos + 1].Value = ParseDate(Conv.ToDate(row[itemcolumn.Name]));
									} else if (itemcolumn.DataType == 13) {
										ws.Cells[rn, colpos + 1].Style.Numberformat.Format = GetDateFormat(true);
										ws.Cells[rn, colpos + 1].Value =
											ParseDate(Conv.ToDateTime(row[itemcolumn.Name]));
									} else {
										ws.Cells[rn, colpos + 1].Value =
											Conv.ToStr(row[itemcolumn.Name]).Substring(0, index);
									}

									//SetExcelCellVall(ws, itemcolumn, colpos + 1, rn, row, null, 0);

									//These might help in the future...
									/*ws.Cells[rn, colpos + 1].Value = (DateTime)Conv.ToDateTime(row[itemcolumn.Name]);
									ws.Cells[rn, colpos + 1].Style.Numberformat.Format = GetDateFormat();*/
								} else {
									ws.Cells[rn, colpos + 1].Value = Conv.ToStr(row[itemcolumn.Name]);
								}
							} else {
								ws.Cells[rn, colpos + 1].Value = row[itemcolumn.Name];
							}

							colpos++;
						}

						rn++;
					}
				}
			} catch (Exception e) {
				Console.WriteLine(e);
			}

			return pck;
		}

		//This overload may be obsolete
		public void GetExcelExportTemplateWithData(HttpResponse r, bool fromPortfolio = false, int portfolioId = 0,
			PortfolioOptions q = null) {
			r.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			r.Headers.Add("content-Disposition", "inline;filename=excel.xlsx");
			var bArr = CreateExcelPackage(new List<int>(), fromPortfolio, portfolioId, q, true).GetAsByteArray();
			r.ContentLength = bArr.Length;
			r.Body.WriteAsync(bArr, 0, bArr.Length);
		}

		public byte[] GetExcelExportTemplateWithData(List<int> columns, bool fromPortfolio = false, int portfolioId = 0,
			PortfolioOptions q = null, bool strictMode = true) {
			return CreateExcelPackage(columns, fromPortfolio, portfolioId, q, strictMode).GetAsByteArray();
		}

		public byte[] GetCsvExportTemplateWithData(List<int> columns, bool fromPortfolio = false, int portfolioId = 0,
			PortfolioOptions q = null, bool strictMode = true) {
			return PortfolioCsv(portfolioId, q, columns, fromPortfolio, strictMode);
		}

		public void ImportExcelUpdate(int fileId) {
			ImportExcel(fileId, 2);
		}

		public void ImportExcelInsert(int fileId) {
			ImportExcel(fileId, 1);
		}

		public void ImportExcel(int fileId, int mode = 0) {
			try {
				var x = new AttachmentsDownload(ProcessBase.authenticatedSession);
				var x2 = x.DownloadAttachment(fileId, 0);
				var iStream = new MemoryStream(x2.Filedata);
				using (var package = new ExcelPackage(iStream)) {
					var worksheet = package.Workbook.Worksheets[0];

					var rowCount = worksheet.Dimension.Rows;
					var colCount = worksheet.Dimension.Columns;

					var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process,
						ProcessBase.authenticatedSession);
					var collist = ics.GetItemColumns();
					var iCols = ics.GetItemColumnsIntDictionary();
					var ib = new ItemBase(ProcessBase.instance, ProcessBase.process, ProcessBase.authenticatedSession);
					var iColsDict = new Dictionary<int, ItemColumn>();

					for (var col = 1; col <= colCount; col++) {
						var itemColumnId = Conv.ToInt(worksheet.Cells[2, col].Value);
						if (itemColumnId > 0 && iCols.ContainsKey(itemColumnId)) {
							iColsDict.Add(col, iCols[itemColumnId]);
						}
					}

					for (var row = 3; row <= rowCount; row++) {
						var row_mode = mode;
						var itemId = 0;
						var dataToSave = new Dictionary<string, object>();
						for (var col = 1; col <= colCount; col++) {
							if (col == 1) {
								itemId = Conv.ToInt(worksheet.Cells[row, col].Value);
								if (row_mode == 0) {
									if (itemId == 0) {
										row_mode = 1; // Insert
									} else {
										row_mode = 2; // Update
									}
								}
							} else {
								if (!iColsDict.ContainsKey(col)) continue;
								var iCol = iColsDict[col];
								if (iCol.IsDate() || iCol.IsDateTime()) {
									// JKa, ASu - Joskus tulee date, joskus double, 28.1.2021
									if (Conv.ToDouble(worksheet.Cells[row, col].Value) == 0) {
										dataToSave.Add(iCol.Name, Conv.ToDateTime(worksheet.Cells[row, col].Value));
									} else {
										dataToSave.Add(iCol.Name,
											new DateTime(1899, 12, 30).AddDays(
												Conv.ToDouble(worksheet.Cells[row, col].Value)));
									}
								} else if (iCol.IsDatabasePrimitive()) {
									dataToSave.Add(iCol.Name, worksheet.Cells[row, col].Value);
								} else if (iCol.IsProcess() || iCol.IsList()) {
									var ids = new List<int>();
									Regex rx = new Regex(@"#([0-9]*)#");
									foreach (Match match in rx.Matches(Conv.ToStr(worksheet.Cells[row, col].Value))) {
										int id = Conv.ToInt(match.Groups[1].Value);
										if (id > 0 && !ids.Contains(id)) {
											ids.Add(id);
										}
									}

									if (ids.Count > 0) {
										dataToSave.Add(iCol.Name, ids);
									} else {
										dataToSave.Add(iCol.Name, null);
									}
								}
							}
						}

						if (row_mode == 1) {
							// Insert
							itemId = ib.InsertItemRow();
							dataToSave.Add("item_id", itemId);
							if (ProcessBase.process == "user") {
								var calendars = new Calendars(ProcessBase.instance, AuthenticatedSession);
								calendars.CreateUserCalendar(itemId);
							}

							ib.SaveItem(dataToSave, cachedItemColumns: collist, checkRights: false,
								returnSubqueries: false, fileId: fileId);
							ProcessBase.DoNewRowAction(itemId, true);
							ib.InvalidateCache(itemId);
						} else if (row_mode == 2 && itemId > 0) {
							// Update
							dataToSave.Add("item_id", itemId);
							ib.SaveItem(dataToSave, cachedItemColumns: collist, checkRights: false,
								returnSubqueries: false, fileId: fileId);
							ib.InvalidateCache(itemId);
						}
					}
				}
			} catch (Exception e) {
				Console.WriteLine(e);
			}
		}

		private Translations translations = null;

		public byte[] GetResourceUserExcel(int portfolioId, string dateStart, string dateEnd, int precision, PortfolioOptions filter, List<int> additionalFields, string resolution, List<int> ids) {
			if (translations == null) translations = new Translations(ProcessBase.instance, ProcessBase.authenticatedSession, ProcessBase.authenticatedSession.locale_id);

			var pck = new ExcelPackage();
			try {
				var ws = pck.Workbook.Worksheets.Add("user");

				var ppc = new ProcessPortfolioColumns(ProcessBase.instance, "user", ProcessBase.authenticatedSession);
				var primary_columns = ppc.GetPrimaryColumns();
				var user_name = "''";
				foreach (var primary_column in primary_columns) {
					if (primary_column.ItemColumn.DataType == 0) {
						if (user_name.Length > 0) user_name += " + ' '";
						user_name += " + u." + primary_column.ItemColumn.Name;
					}
				}

				var cppc = new ProcessPortfolioColumns(ProcessBase.instance, "competency", ProcessBase.authenticatedSession);
				primary_columns = cppc.GetPrimaryColumns();
				var competency_name = "''";
				foreach (var primary_column in primary_columns) {
					if (primary_column.ItemColumn.DataType == 0) {
						if (competency_name.Length > 0) competency_name += " + ' '";
						competency_name += " + c." + primary_column.ItemColumn.Name;
					}
				}

				var additional_columns = new List<ProcessPortfolioColumn>();
				var users = new Dictionary<string, object>();
				var user_item_ids = new List<int>();
				var additional_data = new Dictionary<int, DataRow>();
				if (ids.Count == 0) {
					if (additionalFields.Count > 0) {
						ProcessBase.SetDBParam(SqlDbType.Int, "@process_portfolio_id", portfolioId);
						foreach (DataRow col_row in ProcessBase.db.GetDatatable(
							"SELECT process_portfolio_column_id FROM process_portfolio_columns WHERE process_portfolio_id = @process_portfolio_id " +
							"AND item_column_id IN (" + string.Join(",", additionalFields.ToArray()) + ")",
							ProcessBase.DBParameters).Rows) {
							additional_columns.Add(ppc.GetColumn(portfolioId, Conv.ToInt(col_row["process_portfolio_column_id"])));
						}
					}

					var ib = new ItemBase(ProcessBase.instance, "user", ProcessBase.authenticatedSession);
					users = ib.FindItems(portfolioId, filter);
					foreach (DataRow user_row in ((DataTable)users["rowdata"]).Rows) {
						user_item_ids.Add(Conv.ToInt(user_row["item_id"]));
						additional_data.Add(Conv.ToInt(user_row["item_id"]), user_row);
					}
				}
				else {
					user_item_ids = ids;
				}

				var user_cost_center_column_id = Conv.ToInt(ProcessBase.db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'user' AND name = 'cost_center'", ProcessBase.DBParameters));
				if (filter.filters.ContainsKey(user_cost_center_column_id)) {
					var competency_cost_center_column_id = Conv.ToInt(ProcessBase.db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'competency' AND name = 'cost_center'", ProcessBase.DBParameters));
					ProcessBase.SetDBParameter("@competency_cost_center_column_id", competency_cost_center_column_id);
					var cost_centers = JsonConvert.DeserializeObject<List<int>>(Conv.ToStr(filter.filters[user_cost_center_column_id]));
					foreach (var cost_center_id in cost_centers) {
						ProcessBase.SetDBParameter("@cost_center_id", cost_center_id);
						foreach (DataRow competency in ProcessBase.db.GetDatatable("SELECT item_id " +
																				   "FROM item_data_process_selections " +
																				   "WHERE selected_item_id = @cost_center_id " +
																				   "AND item_column_id = @competency_cost_center_column_id",
																				   ProcessBase.DBParameters).Rows) {
							user_item_ids.Add(Conv.ToInt(competency["item_id"]));
						}
					}
				}

				var start_date = (DateTime)Conv.ToDate(dateStart);
				var end_date = (DateTime)Conv.ToDate(dateEnd);

				ProcessBase.SetDBParam(SqlDbType.DateTime, "dateStart", start_date);
				ProcessBase.SetDBParam(SqlDbType.DateTime, "dateEnd", end_date);

				ws.Cells[1, 1].Value = translations.GetTranslation("OWNER_NAME");
				ws.Cells[1, 2].Value = "Secondary";
				ws.Cells[1, 3].Value = translations.GetTranslation("USER_NAME");
				ws.Cells[1, 4].Value = translations.GetTranslation("ALLOCATION_TITLE");
				ws.Cells[1, 5].Value = translations.GetTranslation("COST_CENTER_EXCEL");
				ws.Cells[1, 6].Value = translations.GetTranslation("STATUS_EXCEL");
				var c = 0;
				foreach (var additional_column in additional_columns) {
					var title = "";
					if (additional_column.ShortNameTranslationRaw != null) title = translations.GetTranslation(additional_column.ShortNameTranslationRaw);
					if (title.Length == 0) title = translations.GetTranslation(additional_column.ItemColumn.Variable);
					ws.Cells[1, 7 + c].Value = title;
					c++;
				}

				ws.Cells[1, 7 + additional_columns.Count].Value = translations.GetTranslation("TOTAL");

				c = 8 + additional_columns.Count;
				if (precision == 1) {
					// day
					var loop_date = start_date;
					while (loop_date <= end_date) {
						ws.Cells[1, c].Style.Numberformat.Format = GetDateFormat();
						ws.Cells[1, c].Value = ParseDate(loop_date);
						loop_date = loop_date.AddDays(1);
						c++;
					}
				} else if (precision == 2) {
					// week
					var loop_date = MiddleDateOfWeek(start_date, FirstWeekOfYear.FirstFourDays);
					while (loop_date <= MiddleDateOfWeek(end_date, FirstWeekOfYear.FirstFourDays)) {
						ws.Cells[1, c].Value = translations.GetTranslation("WEEK_ABBR") + " " + WeekNumber(loop_date, (FirstDayOfWeek)ProcessBase.authenticatedSession.first_day_of_week, FirstWeekOfYear.FirstFourDays) + "/" + loop_date.Year;
						loop_date = loop_date.AddDays(7);
						c++;
					}
				} else {
					// month
					var loop_date = new DateTime(start_date.Year, start_date.Month, 1);
					while (loop_date <= end_date) {
						ws.Cells[1, c].Value = loop_date.ToString(GetMonthCsFormat());
						loop_date = loop_date.AddMonths(1);
						c++;
					}
				}

				ws.Cells[1, 1, 1, c].Style.Font.Bold = true;

				var sql =
					"SELECT pi.process, a.process_item_id, '' AS process_primary, '' AS process_secondary, a.item_id AS allo_item_id, a.title, a.status, a.resource_type, " +
					"CASE WHEN a.resource_type = 'user' THEN u.item_id ELSE c.item_id END AS user_item_id, " +
					"CASE WHEN a.resource_type = 'user' THEN " + user_name + " ELSE " + competency_name + " END AS user_name, " +
					"ad.date, ad.hours, cc.list_item AS cost_center_name, " +
					"CASE WHEN a.resource_type = 'user' THEN cal.parent_calendar_id ELSE 0 END AS base_calendar_id " +
					"FROM items i " +
					"LEFT JOIN _" + ProcessBase.instance + "_allocation a ON a.resource_item_id = i.item_id " +
					"LEFT JOIN _" + ProcessBase.instance + "_user u ON u.item_id = a.resource_item_id AND a.resource_type = 'user' " +
					"LEFT JOIN _" + ProcessBase.instance + "_competency c ON c.item_id = a.resource_item_id AND a.resource_type = 'competency' " +
					"LEFT JOIN allocation_durations ad ON ad.allocation_item_id = a.item_id " +
					"LEFT JOIN _" + ProcessBase.instance + "_list_cost_center cc ON cc.item_id = a.cost_center " +
					"LEFT JOIN items pi ON pi.item_id = a.process_item_id " +
					"LEFT JOIN calendars cal ON cal.item_id = u.item_id " +
					"WHERE i.item_id IN (" + string.Join(",", user_item_ids.ToArray()) + ") " +
					"AND NOT a.status IN (10, 70) " +
					"AND ad.date BETWEEN @dateStart AND @dateEnd " +
					"AND (u.item_id IS NOT NULL OR c.item_id IS NOT NULL)";

				var allos = ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);
				foreach (DataRow allo in allos.Rows) {
					allo["process_primary"] = GetProcessPrimary(Conv.ToStr(allo["process"]), Conv.ToInt(allo["process_item_id"]));
					allo["process_secondary"] = GetProcessSecondary(Conv.ToStr(allo["process"]), Conv.ToInt(allo["process_item_id"]));
				}

				var default_work_hours = new Dictionary<int, double>();
				default_work_days = new Dictionary<int, Dictionary<int, int>>();
				foreach (DataRow base_calendar in ProcessBase.db.GetDatatable("SELECT calendar_id, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7 FROM calendars WHERE base_calendar = 1 AND instance = @instance", ProcessBase.DBParameters).Rows) {
					default_work_hours.Add(Conv.ToInt(base_calendar["calendar_id"]), Conv.ToDouble(base_calendar["work_hours"]));

					var dwd = new Dictionary<int, int>();
					var current_date = start_date;
					while (current_date <= end_date) {
						var key = GetCell(precision, start_date, current_date);
						if (!dwd.ContainsKey(key)) dwd.Add(key, 0);
						var day_of_week = (int)current_date.DayOfWeek;
						if (day_of_week == 0) day_of_week = 7;
						if (Conv.ToInt(base_calendar["def_" + day_of_week]) == 0) dwd[key] += 1;
						current_date = current_date.AddDays(1);
					}
					default_work_days.Add(Conv.ToInt(base_calendar["calendar_id"]), dwd);
				}

				var default_base_calendar_id = Conv.ToInt(ProcessBase.db.ExecuteScalar("SELECT TOP 1 calendar_id FROM calendars WHERE base_calendar = 1 AND is_default = 1 AND instance = @instance", ProcessBase.DBParameters));

				var row = 1;
				var col = 0;
				var allo_item_id = 0;
				var sum = 0.0;
				foreach (DataRow allo in allos.Select("",
					"user_name ASC, process_primary ASC, process_item_id ASC, title ASC, allo_item_id ASC")) {
					if (Conv.ToInt(allo["allo_item_id"]) != allo_item_id) {
						if (allo_item_id > 0) {
							ws.Cells[row, 7 + additional_columns.Count].Style.Numberformat.Format = "#,##0.00";
							ws.Cells[row, 7 + additional_columns.Count].Value = sum;
							sum = 0;
						}

						allo_item_id = Conv.ToInt(allo["allo_item_id"]);
						row++;
						ws.Cells[row, 1].Value = Conv.ToStr(allo["process_primary"]);
						ws.Cells[row, 2].Value = Conv.ToStr(allo["process_secondary"]);
						ws.Cells[row, 3].Value = Conv.ToStr(allo["user_name"]);
						ws.Cells[row, 4].Value = Conv.ToStr(allo["title"]);
						ws.Cells[row, 5].Value = Conv.ToStr(allo["cost_center_name"]);
						ws.Cells[row, 6].Value = GetStatusTranslation(Conv.ToInt(allo["status"]));

						var co = 0;
						foreach (var additional_column in additional_columns) {
							SetExcelCellVall(ws, additional_column, 7 + co, row, additional_data[Conv.ToInt(allo["user_item_id"])], users, 0);
							co++;
						}
					}

					var offset = GetCell(precision, start_date, (DateTime)Conv.ToDate(allo["date"]));
					col = 8 + additional_columns.Count + offset;
					ws.Cells[row, col].Style.Numberformat.Format = "#,##0.00";

					var hours = Conv.ToDouble(allo["hours"]);
					var work_hours = default_work_hours[default_base_calendar_id];
					var work_days = GetWorkDays(offset, default_base_calendar_id);
					if (Conv.ToStr(allo["resource_type"]) == "user") {
						work_hours = default_work_hours[Conv.ToInt(allo["base_calendar_id"])];
						work_days = GetWorkDays(offset, Conv.ToInt(allo["base_calendar_id"]));
					}
					if (resolution == "resource_allocation") {
						hours = hours / work_hours;
					}
					else if (resolution == "resource_allocation_fte") {
						hours = hours / (work_days * work_hours);
					}
					ws.Cells[row, col].Value = Conv.ToDouble(ws.Cells[row, col].Value) + hours;
					sum += hours;
				}

				if (allo_item_id > 0) {
					ws.Cells[row, 7 + additional_columns.Count].Style.Numberformat.Format = "#,##0.00";
					ws.Cells[row, 7 + additional_columns.Count].Value = sum;
				}
			} catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.WriteLine(e.StackTrace);
			}

			return pck.GetAsByteArray();
		}

		public byte[] GetResourceProcessExcel(string process, int portfolioId, string dateStart, string dateEnd, int precision, PortfolioOptions filter, List<int> additionalFields, string resolution, List<int> ids) {
			if (translations == null) translations = new Translations(ProcessBase.instance, ProcessBase.authenticatedSession, ProcessBase.authenticatedSession.locale_id);

			var pck = new ExcelPackage();
			try {
				var ws = pck.Workbook.Worksheets.Add(process);

				var uppc = new ProcessPortfolioColumns(ProcessBase.instance, "user", ProcessBase.authenticatedSession);
				var primary_columns = uppc.GetPrimaryColumns();
				var user_name = "''";
				foreach (var primary_column in primary_columns) {
					if (primary_column.ItemColumn.DataType == 0) {
						if (user_name.Length > 0) user_name += " + ' '";
						user_name += " + u." + primary_column.ItemColumn.Name;
					}
				}

				var cppc = new ProcessPortfolioColumns(ProcessBase.instance, "competency", ProcessBase.authenticatedSession);
				primary_columns = cppc.GetPrimaryColumns();
				var competency_name = "''";
				foreach (var primary_column in primary_columns) {
					if (primary_column.ItemColumn.DataType == 0) {
						if (competency_name.Length > 0) competency_name += " + ' '";
						competency_name += " + c." + primary_column.ItemColumn.Name;
					}
				}

				var additional_columns = new List<ProcessPortfolioColumn>();
				var processes = new Dictionary<string, object>();
				var process_item_ids = new List<int>();
				var additional_data = new Dictionary<int, DataRow>();
				if (ids.Count == 0) {
					if (additionalFields.Count > 0) {
						var ppc = new ProcessPortfolioColumns(ProcessBase.instance, process, ProcessBase.authenticatedSession);
						ProcessBase.SetDBParam(SqlDbType.Int, "@process_portfolio_id", portfolioId);
						foreach (DataRow col_row in ProcessBase.db.GetDatatable(
							"SELECT process_portfolio_column_id FROM process_portfolio_columns WHERE process_portfolio_id = @process_portfolio_id " +
							"AND item_column_id IN (" + string.Join(",", additionalFields.ToArray()) + ")",
							ProcessBase.DBParameters).Rows) {
							additional_columns.Add(ppc.GetColumn(portfolioId, Conv.ToInt(col_row["process_portfolio_column_id"])));
						}
					}

					var ib = new ItemBase(ProcessBase.instance, process, ProcessBase.authenticatedSession);
					processes = ib.FindItems(portfolioId, filter);
					foreach (DataRow process_row in ((DataTable)processes["rowdata"]).Rows) {
						process_item_ids.Add(Conv.ToInt(process_row["item_id"]));
						additional_data.Add(Conv.ToInt(process_row["item_id"]), process_row);
					}
				}
				else {
					process_item_ids = ids;
				}

				var start_date = (DateTime)Conv.ToDate(dateStart);
				var end_date = (DateTime)Conv.ToDate(dateEnd);

				ProcessBase.SetDBParam(SqlDbType.DateTime, "dateStart", start_date);
				ProcessBase.SetDBParam(SqlDbType.DateTime, "dateEnd", end_date);

				ws.Cells[1, 1].Value = translations.GetTranslation("ALLOCATION_EXCEL_PROCESS_PRIMARY");
				ws.Cells[1, 2].Value = translations.GetTranslation("ALLOCATION_EXCEL_PROCESS_SECONDARY");
				ws.Cells[1, 3].Value = translations.GetTranslation("USER_NAME");
				ws.Cells[1, 4].Value = translations.GetTranslation("ALLOCATION_TITLE");
				ws.Cells[1, 5].Value = translations.GetTranslation("COST_CENTER_EXCEL");
				ws.Cells[1, 6].Value = translations.GetTranslation("STATUS_EXCEL");
				var c = 0;
				foreach (var additional_column in additional_columns) {
					var title = "";
					if (additional_column.ShortNameTranslationRaw != null) title = translations.GetTranslation(additional_column.ShortNameTranslationRaw);
					if (title.Length == 0) title = translations.GetTranslation(additional_column.ItemColumn.Variable);
					ws.Cells[1, 7 + c].Value = title;
					c++;
				}

				ws.Cells[1, 7 + additional_columns.Count].Value = translations.GetTranslation("TOTAL");

				c = 8 + additional_columns.Count;
				if (precision == 1) {
					// day
					var loop_date = start_date;
					while (loop_date <= end_date) {
						ws.Cells[1, c].Style.Numberformat.Format = GetDateFormat();
						ws.Cells[1, c].Value = ParseDate(loop_date);
						loop_date = loop_date.AddDays(1);
						c++;
					}
				} else if (precision == 2) {
					// week
					var loop_date = MiddleDateOfWeek(start_date, FirstWeekOfYear.FirstFourDays);
					while (loop_date <= MiddleDateOfWeek(end_date, FirstWeekOfYear.FirstFourDays)) {
						ws.Cells[1, c].Value = translations.GetTranslation("WEEK_ABBR") + " " + WeekNumber(loop_date, (FirstDayOfWeek)ProcessBase.authenticatedSession.first_day_of_week, FirstWeekOfYear.FirstFourDays) + "/" + loop_date.Year;
						loop_date = loop_date.AddDays(7);
						c++;
					}
				} else {
					// month
					var loop_date = new DateTime(start_date.Year, start_date.Month, 1);
					while (loop_date <= end_date) {
						ws.Cells[1, c].Value = loop_date.ToString(GetMonthCsFormat());
						loop_date = loop_date.AddMonths(1);
						c++;
					}
				}

				ws.Cells[1, 1, 1, c].Style.Font.Bold = true;

				var sql =
					"SELECT i.process, a.process_item_id, '' AS process_primary, '' AS process_secondary, a.item_id AS allo_item_id, a.title, a.status, a.resource_type, " +
					"CASE WHEN  a.resource_type = 'user' THEN " + user_name + " ELSE " + competency_name + " END AS user_name, " + 
					"ad.date, ad.hours, cc.list_item AS cost_center_name, " +
					"CASE WHEN a.resource_type = 'user' THEN u.item_id ELSE c.item_id END AS user_item_id, " +
					"CASE WHEN a.resource_type = 'user' THEN cal.parent_calendar_id ELSE 0 END AS base_calendar_id " +
					"FROM items i " +
					"LEFT JOIN _" + ProcessBase.instance + "_allocation a ON i.item_id = a.process_item_id " +
					"LEFT JOIN _" + ProcessBase.instance + "_user u ON u.item_id = a.resource_item_id AND a.resource_type = 'user' " +
					"LEFT JOIN _" + ProcessBase.instance + "_competency c ON c.item_id = a.resource_item_id AND a.resource_type = 'competency' " +
					"LEFT JOIN allocation_durations ad ON ad.allocation_item_id = a.item_id " +
					"LEFT JOIN _" + ProcessBase.instance + "_list_cost_center cc ON cc.item_id = a.cost_center " +
					"LEFT JOIN calendars cal ON cal.item_id = u.item_id " +
					"WHERE i.item_id IN (" + string.Join(",", process_item_ids.ToArray()) + ") " +
					"AND NOT a.status IN (10, 70) " +
					"AND ad.date BETWEEN @dateStart AND @dateEnd " +
					"AND (u.item_id IS NOT NULL OR c.item_id IS NOT NULL)";

				var allos = ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);
				foreach (DataRow allo in allos.Rows) {
					allo["process_primary"] = GetProcessPrimary(Conv.ToStr(allo["process"]), Conv.ToInt(allo["process_item_id"]));
					allo["process_secondary"] = GetProcessSecondary(Conv.ToStr(allo["process"]), Conv.ToInt(allo["process_item_id"]));
				}

				var default_work_hours = new Dictionary<int, double>();
				default_work_days = new Dictionary<int, Dictionary<int, int>>();
				foreach (DataRow base_calendar in ProcessBase.db.GetDatatable("SELECT calendar_id, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7 FROM calendars WHERE base_calendar = 1 AND instance = @instance", ProcessBase.DBParameters).Rows) {
					default_work_hours.Add(Conv.ToInt(base_calendar["calendar_id"]), Conv.ToDouble(base_calendar["work_hours"]));

					var dwd = new Dictionary<int, int>();
					var current_date = start_date;
					while (current_date <= end_date) {
						var key = GetCell(precision, start_date, current_date);
						if (!dwd.ContainsKey(key)) dwd.Add(key, 0);
						var day_of_week = (int)current_date.DayOfWeek;
						if (day_of_week == 0) day_of_week = 7;
						if (Conv.ToInt(base_calendar["def_" + day_of_week]) == 0) dwd[key] += 1;
						current_date = current_date.AddDays(1);
					}
					default_work_days.Add(Conv.ToInt(base_calendar["calendar_id"]), dwd);
				}

				var default_base_calendar_id = Conv.ToInt(ProcessBase.db.ExecuteScalar("SELECT TOP 1 calendar_id FROM calendars WHERE base_calendar = 1 AND is_default = 1 AND instance = @instance", ProcessBase.DBParameters));

				var row = 1;
				var col = 0;
				var allo_item_id = 0;
				var sum = 0.0;
				foreach (DataRow allo in allos.Select("",
					"process_primary ASC, process_item_id ASC, user_name ASC, title ASC, allo_item_id ASC")) {
					if (Conv.ToInt(allo["allo_item_id"]) != allo_item_id) {
						if (allo_item_id > 0) {
							ws.Cells[row, 7 + additional_columns.Count].Style.Numberformat.Format = "#,##0.00";
							ws.Cells[row, 7 + additional_columns.Count].Value = sum;
							sum = 0;
						}

						allo_item_id = Conv.ToInt(allo["allo_item_id"]);
						row++;
						ws.Cells[row, 1].Value = Conv.ToStr(allo["process_primary"]);
						ws.Cells[row, 2].Value = Conv.ToStr(allo["process_secondary"]);
						ws.Cells[row, 3].Value = Conv.ToStr(allo["user_name"]);
						ws.Cells[row, 4].Value = Conv.ToStr(allo["title"]);
						ws.Cells[row, 5].Value = Conv.ToStr(allo["cost_center_name"]);
						ws.Cells[row, 6].Value = GetStatusTranslation(Conv.ToInt(allo["status"]));

						var co = 0;
						foreach (var additional_column in additional_columns) {
							SetExcelCellVall(ws, additional_column, 7 + co, row, additional_data[Conv.ToInt(allo["process_item_id"])], processes, 0);
							co++;
						}
					}

					var offset = GetCell(precision, start_date, (DateTime)Conv.ToDate(allo["date"]));
					col = 8 + additional_columns.Count + offset;
					ws.Cells[row, col].Style.Numberformat.Format = "#,##0.00";

					var hours = Conv.ToDouble(allo["hours"]);
					var work_hours = default_work_hours[default_base_calendar_id];
					var work_days = GetWorkDays(offset, default_base_calendar_id);
					if (Conv.ToStr(allo["resource_type"]) == "user") {
						work_hours = default_work_hours[Conv.ToInt(allo["base_calendar_id"])];
						work_days = GetWorkDays(offset, Conv.ToInt(allo["base_calendar_id"]));
					}
					if (resolution == "project_allocation") {
						hours = hours / work_hours;
					}
					else if (resolution == "project_allocation_fte") {
						hours = hours / (work_days * work_hours);
					}
					ws.Cells[row, col].Value = Conv.ToDouble(ws.Cells[row, col].Value) + hours;
					sum += hours;
				}

				if (allo_item_id > 0) {
					ws.Cells[row, 7 + additional_columns.Count].Style.Numberformat.Format = "#,##0.00";
					ws.Cells[row, 7 + additional_columns.Count].Value = sum;
				}
			} catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.WriteLine(e.StackTrace);
			}

			return pck.GetAsByteArray();
		}

		private Dictionary<int, Dictionary<int, int>> default_work_days;

		private int GetWorkDays(int index, int base_calendar_id) {
			if (default_work_days[base_calendar_id][index] == 0) {
				return 1;
			}
			else {
				return default_work_days[base_calendar_id][index];
			}
		}

		private string GetMonthCsFormat() {
			if (AuthenticatedSession.date_format == "dd/mm/yy") {
				return "MMMM/yyyy";
			} else if (AuthenticatedSession.date_format == "mm/dd/yy") {
				return "MMMM/yyyy";
			} else if (AuthenticatedSession.date_format == "d M yyyy") {
				return "MMMM yyyy";
			} else if (AuthenticatedSession.date_format == "dd.mm.yy") {
				return "MMMM/yyyy";
			} else {
				return "MMMM/yyyy";
			}
		}

		private Dictionary<string, string> primary_columns_cache = new Dictionary<string, string>();
		private Dictionary<int, string> primary_value_cache = new Dictionary<int, string>();

		private string GetProcessPrimary(string process, int item_id) {
			string process_primary;
			if (primary_columns_cache.ContainsKey(process)) {
				process_primary = primary_columns_cache[process];
			} else {
				var ppc = new ProcessPortfolioColumns(ProcessBase.instance, process, ProcessBase.authenticatedSession);
				var primary_columns = ppc.GetPrimaryColumns();
				process_primary = "''";
				foreach (var col in primary_columns) {
					if (col.ItemColumn.DataType == 0) {
						if (process_primary != "''") process_primary += " + ' '";
						process_primary += " + " + col.ItemColumn.Name;
					} else if (col.ItemColumn.DataType == 5) {
						var sub_ppc = new ProcessPortfolioColumns(ProcessBase.instance, col.ItemColumn.DataAdditional,
							ProcessBase.authenticatedSession);
						var sub_primary_columns = sub_ppc.GetPrimaryColumns();
						var sub_process_primary = "''";
						foreach (var sub_col in sub_primary_columns) {
							if (sub_col.ItemColumn.DataType == 0) {
								if (sub_process_primary != "''") sub_process_primary += " + ' '";
								sub_process_primary += " + _" + ProcessBase.instance + "_" +
								                       col.ItemColumn.DataAdditional + "." + sub_col.ItemColumn.Name;
							}
						}

						if (process_primary != "''") process_primary += " + ' '";
						process_primary += " + (SELECT TOP 1 " + sub_process_primary + " FROM _" +
						                   ProcessBase.instance + "_" + col.ItemColumn.DataAdditional + " WHERE _" +
						                   ProcessBase.instance + "_" + col.ItemColumn.DataAdditional +
						                   ".item_id IN (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = _" +
						                   ProcessBase.instance + "_" + process +
						                   ".item_id AND idps.item_column_id = " + col.ItemColumn.ItemColumnId + ")) ";
					} else if (col.ItemColumn.DataType == 6) {
						if (process_primary != "''") process_primary += " + ' '";
						process_primary += " + (SELECT TOP 1 list_item FROM _" + ProcessBase.instance + "_" +
						                   col.ItemColumn.DataAdditional + " WHERE _" + ProcessBase.instance + "_" +
						                   col.ItemColumn.DataAdditional +
						                   ".item_id IN (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = _" +
						                   ProcessBase.instance + "_" + process +
						                   ".item_id AND idps.item_column_id = " + col.ItemColumn.ItemColumnId + ")) ";
					}
				}

				primary_columns_cache.Add(process, process_primary);
			}

			string primary_value;
			if (primary_value_cache.ContainsKey(item_id)) {
				primary_value = primary_value_cache[item_id];
			} else {
				ProcessBase.SetDBParam(SqlDbType.Int, "item_id", item_id);
				primary_value = Conv.ToStr(ProcessBase.db.ExecuteScalar(
					"SELECT " + process_primary + " FROM _" + ProcessBase.instance + "_" + process +
					" WHERE item_id = @item_id", ProcessBase.DBParameters));
				primary_value_cache.Add(item_id, primary_value);
			}

			return primary_value;
		}

		private Dictionary<string, string> secondary_columns_cache = new Dictionary<string, string>();
		private Dictionary<int, string> secondary_value_cache = new Dictionary<int, string>();

		private string GetProcessSecondary(string process, int item_id) {
			string process_secondary;
			if (secondary_columns_cache.ContainsKey(process)) {
				process_secondary = secondary_columns_cache[process];
			} else {
				var ppc = new ProcessPortfolioColumns(ProcessBase.instance, process, ProcessBase.authenticatedSession);
				var secondary_columns = ppc.GetSecondaryColumns();
				process_secondary = "''";
				foreach (var col in secondary_columns) {
					if (col.ItemColumn.DataType == 0) {
						if (process_secondary != "''") process_secondary += " + ' '";
						process_secondary += " + _" + ProcessBase.instance + "_" + process + "." + col.ItemColumn.Name;
					} else if (col.ItemColumn.DataType == 5) {
						var sub_ppc = new ProcessPortfolioColumns(ProcessBase.instance, col.ItemColumn.DataAdditional,
							ProcessBase.authenticatedSession);
						var sub_secondary_columns = sub_ppc.GetPrimaryColumns();
						var sub_process_secondary = "''";
						foreach (var sub_col in sub_secondary_columns) {
							if (sub_col.ItemColumn.DataType == 0) {
								if (sub_process_secondary != "''") sub_process_secondary += " + ' '";
								sub_process_secondary += " + _" + ProcessBase.instance + "_" +
								                         col.ItemColumn.DataAdditional + "." + sub_col.ItemColumn.Name;
							}
						}

						if (process_secondary != "''") process_secondary += " + ' '";
						process_secondary += " + (SELECT TOP 1 " + sub_process_secondary + " FROM _" +
						                     ProcessBase.instance + "_" + col.ItemColumn.DataAdditional + " WHERE _" +
						                     ProcessBase.instance + "_" + col.ItemColumn.DataAdditional +
						                     ".item_id IN (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = _" +
						                     ProcessBase.instance + "_" + process +
						                     ".item_id AND idps.item_column_id = " + col.ItemColumn.ItemColumnId +
						                     ")) ";
					} else if (col.ItemColumn.DataType == 6) {
						if (process_secondary != "''") process_secondary += " + ' '";
						process_secondary += " + (SELECT TOP 1 list_item FROM _" + ProcessBase.instance + "_" +
						                     col.ItemColumn.DataAdditional + " WHERE _" + ProcessBase.instance + "_" +
						                     col.ItemColumn.DataAdditional +
						                     ".item_id IN (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = _" +
						                     ProcessBase.instance + "_" + process +
						                     ".item_id AND idps.item_column_id = " + col.ItemColumn.ItemColumnId +
						                     ")) ";
					}
				}

				secondary_columns_cache.Add(process, process_secondary);
			}

			string secondary_value;
			if (secondary_value_cache.ContainsKey(item_id)) {
				secondary_value = secondary_value_cache[item_id];
			} else {
				ProcessBase.SetDBParam(SqlDbType.Int, "item_id", item_id);
				secondary_value = Conv.ToStr(ProcessBase.db.ExecuteScalar(
					"SELECT " + process_secondary + " FROM _" + ProcessBase.instance + "_" + process + " WHERE _" +
					ProcessBase.instance + "_" + process + ".item_id = @item_id", ProcessBase.DBParameters));
				secondary_value_cache.Add(item_id, secondary_value);
			}

			return secondary_value;
		}

		private int GetCell(int precision, DateTime start_date, DateTime current_date) {
			if (precision == 1) {
				// day
				return (current_date - start_date).Days;
			} else if (precision == 2) {
				// week
				return (MiddleDateOfWeek(current_date, FirstWeekOfYear.FirstFourDays) - MiddleDateOfWeek(start_date, FirstWeekOfYear.FirstFourDays)).Days / 7;
			} else {
				// month
				return ((current_date.Year - start_date.Year) * 12) + (current_date.Month - start_date.Month);
			}
		}

		private string GetStatusTranslation(int status) {
			switch (status) {
				case 10:
					return translations.GetTranslation("ALLOCATION_DRAFT_STATUS");
				case 13:
					return translations.GetTranslation("ALLOCATION_PREVIOUS_DRAFT_STATUS");
				case 20:
					return translations.GetTranslation("ALLOCATION_REQUEST_STATUS");
				case 25:
					return translations.GetTranslation("ALLOCATION_REQUEST_MODIFIED_STATUS");
				case 30:
					return translations.GetTranslation("ALLOCATION_REJECT_STATUS");
				case 40:
					return translations.GetTranslation("ALLOCATION_APPROVE_MODIFIED_STATUS");
				case 50:
				case 55:
					return translations.GetTranslation("ALLOCATION_APPROVE_STATUS");
				case 70:
					return translations.GetTranslation("ALLOCATION_RELEASED_STATUS");
			}

			return "";
		}

		private DateTime MiddleDateOfWeek(DateTime d, FirstWeekOfYear FirstWeekOfYear) {
			if (FirstWeekOfYear == FirstWeekOfYear.FirstFourDays) {
				if (d.DayOfWeek == DayOfWeek.Sunday) {
					return d.AddDays(-3);
				} else {
					return d.AddDays(4 - (int)d.DayOfWeek);
				}
			} else {
				return d;
			}
		}

		private int WeekNumber(DateTime target, FirstDayOfWeek FirstDayOfWeek, FirstWeekOfYear FirstWeekOfYear) {
			return DateAndTime.DatePart(DateInterval.WeekOfYear, MiddleDateOfWeek(target, FirstWeekOfYear),
				FirstDayOfWeek, FirstWeekOfYear);
		}
	}
}