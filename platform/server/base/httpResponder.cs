﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Lucene.Net.Support;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Authentication = Keto5.x.platform.server.security.Authentication;

namespace Keto5.x.platform.server.@base {
	[Route("")]
	public class Webroot : RequestBase {
		[HttpGet("")]
		public ActionResult GetHtml() {
			string path;
			if (Diag.ErrorState) {
				//Server cannot start, so display the error log
				var s = new StringBuilder();
				s.Append("<html><head>");
				s.Append("<base href='" + Config.VirtualDirectory + "/'>");
				s.Append("<link rel='stylesheet' href='css/keto.css'>");

				s.Append(
					"</head><body style='overflow:scroll;'><div style='width:80%; margin: 0px auto; padding: 40px;0px;0px;0px;'>");
				s.Append(
					"<h2 class='h6' style='padding-bottom:20px;'>Initialization Error Occured <span style='font-size:10px'>(and the solution cannot start...)</span></h2>");

				s.Append("<h2 class='h6' style='margin-bottom:10px;'>Console Output</h2>");
				s.Append(
					"<ul style='background-color: #1e1e1e; padding:10px; border-radius:8px;list-style-type: none; line-height:1.24'>");
				s.Append(Diag.GetConsole());
				s.Append("</ul>");
				s.Append("</div></body></html>");
				Response.ContentType = "text/html";
				HttpContext.Response.WriteAsync(s.ToString());
				return new OkObjectResult("");
			}

			var errored = false;
			if (HttpContext.Request.Query.ContainsKey("e")) {
				if (HttpContext.Request.Query["e"] == "401") {
					errored = true;
				}
			}

			if (currentSession != null && !errored) {
				//Authentication is ok as there is a session
				path = Config.WwwPath + "private/index.html";
			} else {
				//No session, so return login page
				path = Config.WwwPath + "public/login/login.html";

				//If Keto Authentication code is present
				if (HttpContext.Request.Query.ContainsKey("ka")) {
					var ass = new Keto5.x.platform.server.security.AuthenticatedSession("", 0, null);
					var conbase = new ConnectionBase("", ass);
					var instance = conbase.GetInstance(HttpContext.Request.Host.Host);
					var a = new Authentication(instance);
					if (a.KetoAuthResponse(HttpContext.Request.Query["ka"], instance, HttpContext) == Authentication
						.AuthenticationResult
						.BasicAuthentication_SecondKetoAuthSuccess) {
						//Keto authentication was success.
						Response.StatusCode = 200;
						Response.ContentType = "text/html";
						return new ObjectResult(
							"<script type='text/javascript'>document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('?'));</script>Redirecting...");
					}
				}

				//If AzureAD Authentication is enabled, check for code returned from the server
				if (HttpContext.Request.Query.ContainsKey("code")) {
					//Try authenticating with the code
					var ass = new Keto5.x.platform.server.security.AuthenticatedSession("", 0, null);
					var conbase = new ConnectionBase("", ass);
					var instance = conbase.GetInstance(HttpContext.Request.Host.Host);
					var a = new Authentication(instance);

					if (a.AzureADAuthentication(HttpContext, HttpContext.Request.Query["code"]) ==
					    Authentication.AuthenticationResult.AzureADAuthentication_Success) {
						//Azure authentication was success.
						Response.StatusCode = 200;
						Response.ContentType = "text/html";
						return new ObjectResult(
							"<script type='text/javascript'>var view = undefined; if (localStorage) {view = localStorage.getItem('ssoView');localStorage.removeItem('ssoView'); } document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('?')) + (view ? '?view=' + view : '');</script>Redirecting...");
					}
				}

				//If anonymous authentication is enabled and user is not changing account, redirect straight to index
				if (Conv.ToBool(Config.GetValue("AnonymousAuthentication", "Enabled")) &&
				    HttpContext.Request.Query.ContainsKey("al") == false) {
					var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
					var instance = conbase.GetInstance(HttpContext.Request.Host.Host);
					var a = new Authentication(instance);

					if (a.AnonymousAuthentication() ==
					    Authentication.AuthenticationResult.AnonymousAuthentication_Success) {
						a.GrantAccessInHttpContext(HttpContext, "", true);
						path = Config.WwwPath + "private/index.html";
					}
				}
			}

			//Return html to client and replace Virtual Directory
			Response.StatusCode = 200;
			Response.ContentType = "text/html";

			return new ObjectResult(ReadAndReplace(path));
		}

		private string ReadAndReplace(string path) {
			var fileText = System.IO.File.ReadAllText(path);
			if (Config.VirtualDirectory == "/") {
				fileText = fileText.Replace("--VIRTUALDIRECTORY--", Config.VirtualDirectory);
			} else {
				fileText = fileText.Replace("--VIRTUALDIRECTORY--", Config.VirtualDirectory + "/");
			}

			if (Conv.ToBool(Config.GetDomainValue("default", "GoogleAuthentication", "Enabled"))) {
				var cid = Conv.ToStr(Config.GetDomainValue("default", "GoogleAuthentication", "ClientID"));
				var r = "<script src='https://apis.google.com/js/platform.js' async defer></script>" +
				        "<script type='text/javascript'>" +
				        "var googleUser = {};" +
				        "var enableGoogle = function() {" +
				        "gapi.load('auth2', function() {" +
				        "auth2 = gapi.auth2.init({" +
				        "client_id: '" + cid + "'," +
				        "cookiepolicy: 'single_host_origin'" +
				        "});" +
				        "auth2.attachClickHandler(document.getElementById('googleButton'), {}," +
				        "function(googleUser) {" +
				        "var id_token = googleUser.getAuthResponse().id_token;" +
				        "document.location.href = location.origin + location.pathname + 'gsso?code=' + id_token;" +
				        "}, function(error) {" +
				        "console.log(JSON.stringify(error, undefined, 2));" +
				        "}" +
				        ");" +
				        "});" +
				        "};" +
				        "</script>";
				fileText = fileText.Replace("--GOOGLEAUTHENTICATION1--", r);
				var s =
					"<div id='name'></div><div id='gSignInWrapper'><div id='googleButton'><span class='btn btn-primary background-accent'>{{data.LOGIN_SSO}}</span></div></div><script>setTimeout('enableGoogle()',400);</script>";
				fileText = fileText.Replace("--GOOGLEAUTHENTICATION2--", s);
			} else {
				fileText = fileText.Replace("--GOOGLEAUTHENTICATION1--", "");
				fileText = fileText.Replace("--GOOGLEAUTHENTICATION2--", "");
			}

			return fileText;
		}

		private ActionResult GetPrivateFile(string name, string content = "application/javascript") {
			if (currentSession != null) {
				//Get File
				var path = Config.WwwPath + "private/" + name;
				var fileBytes = new FileInfo(path);

				//Create cache controls
				DateTimeOffset last = fileBytes.CreationTime;
				var lastModified =
					new DateTimeOffset(last.Year, last.Month, last.Day, last.Hour, last.Minute, last.Second,
						last.Offset).ToUniversalTime();
				var etagHash = lastModified.ToFileTime() ^ fileBytes.Length;
				var etag = new EntityTagHeaderValue('\"' + Convert.ToString(etagHash, 16) + '\"');

				//Return
				return File(System.IO.File.ReadAllBytes(path), content, name, lastModified, etag);
			}

			Response.StatusCode = 404;
			return Content("404", "text/plain");
		}

		private ActionResult GetPublicFile(string name, string content = "application/javascript") {
			//Get File
			var path = Config.WwwPath + "public/" + name;
			var fileBytes = new FileInfo(path);

			//Create cache controls
			DateTimeOffset last = fileBytes.CreationTime;
			var lastModified =
				new DateTimeOffset(last.Year, last.Month, last.Day, last.Hour, last.Minute, last.Second,
					last.Offset).ToUniversalTime();
			var etagHash = lastModified.ToFileTime() ^ fileBytes.Length;
			var etag = new EntityTagHeaderValue('\"' + Convert.ToString(etagHash, 16) + '\"');

			//Remove cache directives to fix IE11 HTTPS protocol cache problem
			Response.Headers.Add("cache-control", "public");
			Response.Headers.Add("pragma", "");

			//Return
			return File(System.IO.File.ReadAllBytes(path), content, name, lastModified, etag);
		}

		[HttpGet("lsso/{token}")]
		public ActionResult<string> LegacyAuthenticate(string token) {
			token = Conv.ToStr(token);
			var domain = "default";
			var result = "";

			var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
			var instance = conbase.GetInstance(HttpContext.Request.Host.Host);
			var a = new Authentication(instance);

			switch (a.LegacyAuthentication(domain, token)) {
				case Authentication.AuthenticationResult.LegacyAuthentication_NoTokenProvided:
					result = "No authentication token was found from the response.";
					break;
				case Authentication.AuthenticationResult.LegacyAuthentication_LoginNonExistant:
				case Authentication.AuthenticationResult.LegacyAuthentication_TokenCheckFailed:
					result =
						"<script type='text/javascript'>document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('/')).replace('lsso','') + '?e=403';</script>Redirecting...";
					break;
				case Authentication.AuthenticationResult.LegacyAuthentication_Success:
					a.GrantAccessInHttpContext(HttpContext);
					result =
						"<script type='text/javascript'>document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('/')).replace('lsso','');</script>Redirecting...";
					break;
			}

			Response.StatusCode = 200;
			Response.ContentType = "text/html";
			return new ObjectResult(result);
		}

		[HttpGet("gsso")]
		public ActionResult GoogleAuthenticate() {
			var result = "";
			var domain = "default";
			var resp = "";
			var token = "";
			if (HttpContext.Request.Query.ContainsKey("code"))
				token = HttpContext.Request.Query["code"];

			if (Conv.ToBool(Config.GetDomainValue(domain, "GoogleAuthentication", "Enabled"))) {
				var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
				var instance = conbase.GetInstance(HttpContext.Request.Host.Host);
				var a = new Authentication(instance);
				switch (a.GoogleAuthentication(domain, token)) {
					case Authentication.AuthenticationResult.GoogleAuthentication_NoTokenProvided:
						result = "No authentication token was found from the response. <br><br>" + resp;
						break;
					case Authentication.AuthenticationResult.GoogleAuthentication_LoginNonExistant:
					case Authentication.AuthenticationResult.GoogleAuthentication_TokenCheckFailed:
						result =
							"<script type='text/javascript'>document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('/')).replace('gsso','') + '?e=403';</script>Redirecting...";
						break;
					case Authentication.AuthenticationResult.GoogleAuthentication_Success:
						a.GrantAccessInHttpContext(HttpContext);
						result =
							"<script type='text/javascript'>document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('/')).replace('gsso','');</script>Redirecting...";
						break;
				}
			}

			Response.StatusCode = 200;
			Response.ContentType = "text/html";
			return new ObjectResult(result);
		}

		[HttpGet("sso")]
		public ActionResult SamlAuthenticate() {
			var result = "";
			var domain = "default";
			var resp = "";
			if (Conv.ToBool(Config.GetDomainValue(domain, "SAMLAuthentication", "Enabled"))) {
				var claims = new Dictionary<string, string>();

				//Claims
				var upn = Config.GetDomainValue(domain, "SAMLAuthentication", "ClaimsUPN").ToLower();
				var firstname = Config.GetDomainValue(domain, "SAMLAuthentication", "ClaimsFirstName").ToLower();
				var lastname = Config.GetDomainValue(domain, "SAMLAuthentication", "ClaimsLastName").ToLower();
				var email = Config.GetDomainValue(domain, "SAMLAuthentication", "ClaimsEmail").ToLower();

				foreach (var claim in Request.Headers.Keys) {
					resp += claim + ": " + Request.Headers[claim] + "<br>";
					if (claim.ToLower() == upn) claims.Add("login", Request.Headers[claim]);
					if (claim.ToLower() == firstname) claims.Add("first_name", Request.Headers[claim]);
					if (claim.ToLower() == lastname) claims.Add("last_name", Request.Headers[claim]);
					if (claim.ToLower() == email) claims.Add("email", Request.Headers[claim]);
				}

				var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
				var instance = conbase.GetInstance(HttpContext.Request.Host.Host);
				var a = new Authentication(instance);

				switch (a.AdfsAuthentication(domain, claims)) {
					case Authentication.AuthenticationResult.ADFSNoUPNClaimFound:
						result = "No UPN claim was found from the response. <br><br>" + resp;
						break;
					case Authentication.AuthenticationResult.ADFSAuthentication_LoginNonExistant:
						result =
							"<script type='text/javascript'>document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('/')) + '?e=403';</script>Redirecting...";
						break;
					case Authentication.AuthenticationResult.ADFSAuthentication_Success:
						a.GrantAccessInHttpContext(HttpContext);
						result =
							"<script type='text/javascript'>document.location.href=document.location.href.toString().substring(0, document.location.href.toString().lastIndexOf('/'));</script>Redirecting...";
						break;
				}
			}

			Response.StatusCode = 200;
			Response.ContentType = "text/html";
			return new ObjectResult(result);
		}

		[HttpGet("app.js")]
		public ActionResult GetAppJs() {
			return GetPrivateFile("app.js");
		}

		[HttpGet("app.templates.js")]
		public ActionResult GetAppTemplates() {
			return GetPrivateFile("app.templates.js");
		}

		[HttpGet("customer.templates.js")]
		public ActionResult GetCustomerTemplates() {
			return GetPrivateFile("customer.templates.js");
		}

		[HttpGet("customer.js")]
		public ActionResult GetCustomerJs() {
			return GetPrivateFile("customer.js");
		}

		[HttpGet("fonts/{filename}")]
		public ActionResult GetFontFile(string filename) {
			return GetPublicFile("fonts/" + filename);
		}
	}
}