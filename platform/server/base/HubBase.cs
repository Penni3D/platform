﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Lucene.Net.QueryParsers.Flexible.Messages;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.@base {
	public partial class ApiPush : Hub {
		public enum FailureCodes {
			Success = 200,
			FailureInRequest = 400,
			FailureOnServer = 500
		}

		private readonly IHubContext<ApiPush> _myHubContext;

		public APIResult APIResult = new APIResult();

		public ApiPush(IHubContext<ApiPush> myHubContext) {
			_myHubContext = myHubContext;
		}

		public void SendToAll(string command, List<object> arguments) {
			_myHubContext.Clients.All.SendCoreAsync(command, arguments.ToArray());
		}

		private string Instance {
			get {
				var con = ConnectedUsers.GetConnectedUserByConnectionId(Context.ConnectionId);
				return con != null ? con.instance : "?";
			}
		}


		private AuthenticatedSession CurrentSession =>
			ConnectedUsers.GetConnectedUserByConnectionId(Context.ConnectionId);

		public override Task OnConnectedAsync() {
			Diag.ConnectedSignalRUsers += 1;
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception) {
			Diag.ConnectedSignalRUsers -= 1;
			ConnectedUsers.RemoveConnectedUserWithConnectionId(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}

		public void Connect(string token) {
			//Update Bind
			if (ConnectedUsers.IsConnectedWithToken(token))
				ConnectedUsers.BindConnectionID(token, Context.ConnectionId);
		}

		private string Success(APIResult result) {
			return JsonConvert.SerializeObject(result);
		}

		private string Failure(APIResult result, FailureCodes code, Exception ex = null) {
			if (ex != null) Diag.Log(Instance, ex, CurrentSession.item_id);
			return JsonConvert.SerializeObject(result);
		}
	}

	public partial class ApiPush : Hub {
		public string DiagnosticsGetEvents(int category, int page, int limit) {
			try {
				APIResult.data = category == 0
					? Diag.GetLogs(Instance, page, limit)
					: Diag.GetLogs(Instance, (Diag.LogCategories) category, page, limit);
				return Success(APIResult);
			} catch (Exception ex) {
				return Failure(APIResult, FailureCodes.FailureOnServer, ex);
			}
		}

		public void DiagnosticsPutEvent(string title, string stack, string sender) {
			Diag.LogClient(Instance, string.IsNullOrEmpty(title) ? "?" : title, stack, CurrentSession?.item_id ?? 0,
				sender == "RegularJs" ? Diag.LogCategories.RegularJs : Diag.LogCategories.Angular);
		}
	}
}