﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceUnions;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.@base {
	/// <summary>
	/// Work in progress.
	/// Goal is to get rid of this completely.
	/// Good ol' ItemBase. This is currently split into multiple files.
	/// DatabaseItemService handles actions for plain database objects
	/// FavouriteService handles favourites (doh!)
	/// ItemCollectionService handles collection of entities. Used with searches.
	/// </summary>
	public partial class ItemBase : SessionBaseInfo, IDatabaseItemService {
		private readonly IPortfolioSqlQueryService _portfolioSqlQueryService;

		private readonly Dictionary<string, bool>
			_privateCache; // This dies when instance of this class dies (after request is done)

		public IDatabaseItemService DatabaseItemService;
		public IEntityRepository EntityRepository;
		public IItemColumns ItemColumns;
		public ProcessBase ProcessBase;

		public ItemBase(string instance, string process, AuthenticatedSession session) : base(instance, process,
			session) {
			ProcessBase = new ProcessBase(instance, process, session);
			ItemColumns = new ItemColumns(instance, process, session);
			DatabaseItemService = new DatabaseItemService(instance, process, session);
			EntityRepository = new EntityRepository(instance, process, session);

			_portfolioSqlQueryService = new PortfolioSqlService(
				instance,
				session,
				ItemColumns,
				ProcessBase.tableName,
				ProcessBase.process);

			_privateCache = new Dictionary<string, bool>();
		}

		public void Delete(int itemId, bool checkRights = true, bool removeArchive = false) {
			//INVALIDATE CACHE
			InvalidateCache(itemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			foreach (DataRow r in ProcessBase.db
				.GetDatatable(
					"SELECT item_column_id, selected_item_id FROM item_data_process_selections WHERE item_id = @itemId",
					ProcessBase.DBParameters).Rows) {
				InvalidateCache(Conv.ToInt(r["selected_item_id"]));

				Cache.Remove(Instance,
					"idps_" + Conv.ToInt(r["selected_item_id"]) + " " + Conv.ToInt(r["item_column_id"]));
				Cache.Remove(Instance,
					"idpsl_" + Conv.ToInt(r["selected_item_id"]) + " " + Conv.ToInt(r["item_column_id"]));
			}

			foreach (DataRow r in ProcessBase.db
				.GetDatatable(
					"SELECT item_column_id, item_id FROM item_data_process_selections WHERE selected_item_id = @itemId",
					ProcessBase.DBParameters).Rows) {
				InvalidateCache(Conv.ToInt(r["item_id"]));

				Cache.Remove(Instance, "idps_" + Conv.ToInt(r["item_id"]) + " " + Conv.ToInt(r["item_column_id"]));
				Cache.Remove(Instance, "idpsl_" + Conv.ToInt(r["item_id"]) + " " + Conv.ToInt(r["item_column_id"]));
			}

			GetEntityService().Delete(itemId, checkRights, removeArchive);
		}

		public void InvalidateCache(int itemId) {
			Cache.RemoveGroup(Conv.ToStr(itemId));
		}

		public void InvalidateCache() {
			Cache.Initialize();
		}

		private void CopyDataFromProcessToProcess(int instanceUnionId, string copyfromProcess, int copyFromId,
			int copyToId) {
			var unions = new InstanceUnions(Instance, AuthenticatedSession);

			var unionModel = unions.GetUnionColumns(instanceUnionId);
			var unionProcesses = unions.GetUnionProcesses(instanceUnionId);


			if (unionProcesses.Count > 1) {
				var unionProcessFrom = unionProcesses[0].Process;
				var unionProcessTo = unionProcesses[1].Process;

				var unionProcessFromId = unionProcesses[0].InstanceUnionProcessId;
				var unionProcessToId = unionProcesses[1].InstanceUnionProcessId;

				if (unionProcessFrom == copyfromProcess && unionProcessTo == Process) {
					var fromIb = new ItemBase(Instance, copyfromProcess, AuthenticatedSession);
					var fromData = fromIb.GetItem(copyFromId, false, false);
					var toData = GetItem(copyToId, false, false);

					var fromIc = new ItemColumns(Instance, copyfromProcess, AuthenticatedSession);

					foreach (var uRowId in unionModel.Keys) {
						if (unionModel[uRowId][unionProcessFromId] != null &&
						    unionModel[uRowId][unionProcessToId] != null) { }

						var copyFromCol = fromIc.GetItemColumn(Conv.ToInt(unionModel[uRowId][unionProcessFromId]));
						var copyToCol = ItemColumns.GetItemColumn(Conv.ToInt(unionModel[uRowId][unionProcessToId]));

						if (copyFromCol.IsAttachment() || copyFromCol.IsLink()) {
							var a = new Attachments(Instance, AuthenticatedSession);
							a.CopyAttachments(copyFromId, copyToId, copyFromCol.ItemColumnId, copyToCol.ItemColumnId);
						}

						if (copyFromCol.IsComment()) {
							var comments = new Comments(Instance, unionProcessTo, AuthenticatedSession);
							comments.CopyComments(copyFromCol.ItemColumnId.ToString(), copyFromId,
								copyToCol.ItemColumnId.ToString(), copyToId, unionProcessTo);
						}

						if (copyFromCol.IsPortfolio()) {
							var sql = "SELECT item_id FROM _" + Instance + "_" + copyFromCol.DataAdditional +
							          " WHERE owner_item_id = @SourceItemId";
							ProcessBase.SetDBParam(SqlDbType.NVarChar, "@SourceItemId", copyFromId);

							var sib = new ItemBase(Instance, copyFromCol.DataAdditional, AuthenticatedSession);
							var tib = new ItemBase(Instance, copyToCol.DataAdditional, AuthenticatedSession);
							foreach (DataRow r in ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters).Rows) {
								var oldData = new APISaveResult
									{Data = sib.GetItem(Conv.ToInt(r["item_id"]), false, false)};
								if (oldData.Data.ContainsKey("owner_item_id")) oldData.Data["owner_item_id"] = copyToId;
								tib.AddRow(oldData);
							}
						}

						if (fromData.ContainsKey(copyFromCol.Name)) {
							if (!toData.ContainsKey(copyToCol.Name)) {
								toData.Add(copyToCol.Name, copyFromCol.Name);
							} else {
								toData[copyToCol.Name] = fromData[copyFromCol.Name];
							}
						}
					}

					SaveItem(toData);
				} else {
					Console.WriteLine("Processes do not match !!! ");
				}
			}
		}

		public bool HaveStatesChanged(FeatureStates oldStates, FeatureStates newStates) {
			foreach (var state in new[] {"meta.read", "meta.write"}) {
				if (oldStates.States.Contains(state) && !newStates.States.Contains(state)) return true;
				if (!oldStates.States.Contains(state) && newStates.States.Contains(state)) return true;

				var gids = newStates.GetGroupIds(state);
				foreach (var id in oldStates.GetGroupIds(state)) {
					if (!gids.Contains(id)) return true;
				}

				var tids = newStates.GetTabIds(state);
				foreach (var id in oldStates.GetTabIds(state)) {
					if (!tids.Contains(id)) return true;
				}

				var cids = newStates.GetContainerIds(state);
				foreach (var id in oldStates.GetContainerIds(state)) {
					if (!cids.Contains(id)) return true;
				}

				gids = oldStates.GetGroupIds(state);
				foreach (var id in newStates.GetGroupIds(state)) {
					if (!gids.Contains(id)) return true;
				}

				tids = oldStates.GetTabIds(state);
				foreach (var id in newStates.GetTabIds(state)) {
					if (!tids.Contains(id)) return true;
				}

				cids = oldStates.GetContainerIds(state);
				foreach (var id in newStates.GetContainerIds(state)) {
					if (!cids.Contains(id)) return true;
				}
			}

			return false;
		}

		private bool IsRowModified(int itemId, DateTime? modifyDate) {
			if (modifyDate != null) {
				ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
				ProcessBase.SetDBParam(SqlDbType.DateTime, "@modify_date", modifyDate);
				ProcessBase.SetDBParam(SqlDbType.Int, "@user_id", AuthenticatedSession.item_id);

				if ((int) ProcessBase.db.ExecuteScalar(" SELECT COUNT(item_id) FROM( " +
				                                       " select item_id FROM " + ProcessBase.tableName +
				                                       " where item_id = @item_id AND archive_start > @modify_date and archive_userid <> @user_id " +
				                                       " union all select item_id FROM archive_" +
				                                       ProcessBase.tableName +
				                                       " where item_id = @item_id AND archive_start >  @modify_date and archive_userid <> @user_id ) AS tmp",
					ProcessBase.DBParameters) > 0) {
					return true;
				}

				return false;
			}

			return false;
		}

		public object ReplaceIds(ItemColumn c, object valIn) {
			if (c.IsDatabasePrimitive()) {
				return valIn;
			}

			if (c.IsProcess()) {
				var val2 = "";
				if (valIn != DBNull.Value) {
					var ppcs2 =
						new ProcessPortfolioColumns(Instance, c.DataAdditional, AuthenticatedSession);
					var ids = Conv.ToIntArray(valIn);
					foreach (var id in ids) {
						var val = "";
						foreach (var pc2 in ppcs2.GetPrimaryColumns()) {
							if (pc2.UseInSelectResult == 1) {
								var ib = new ItemBase(Instance, c.DataAdditional, AuthenticatedSession);
								var item = ib.GetItem(id, checkRights: false);
								if (val.Length > 0) {
									val += " " + item[pc2.ItemColumn.Name];
								} else {
									val += item[pc2.ItemColumn.Name];
								}
							}
						}

						if (val2.Length > 0) {
							val2 += ", " + val;
						} else {
							val2 += val;
						}
					}

					return val2;
				}
			} else if (c.IsList()) {
				var val2 = "";
				if (valIn != DBNull.Value) {
					var ppcs2 =
						new ProcessPortfolioColumns(Instance, c.DataAdditional, AuthenticatedSession);
					var ids = Conv.ToIntArray(valIn);
					foreach (var id in ids) {
						var val = "";
						var ib = new ItemBase(Instance, c.DataAdditional, AuthenticatedSession);
						var item = ib.GetItem(id, checkRights: false);
						var itemVal = item["list_item"];
						if (Conv.ToStr(itemVal).Length == 0) {
							itemVal = item["list_symbol_fill"];
						}

						if (val.Length > 0) {
							val += " " + itemVal;
						} else {
							val += itemVal;
						}

						if (val2.Length > 0) {
							val2 += ", " + val;
						} else {
							val2 += val;
						}
					}

					return val2;
				}
			}

			return valIn;
		}


		public void UpdateRecursion(string new_parent_id, string old_parent_id, string old_id, List<int> ids) {
			var sql = " update  pp SET " + new_parent_id + " = ( SELECT top 1 item_id FROM " +
			          ProcessBase.tableName + " WHERE " + old_id + " = pp." + old_parent_id + ") FROM " +
			          ProcessBase.tableName +
			          " pp WHERE item_id IN ( " +
			          string.Join(",", ids.ToArray()) + " ) ";
			ProcessBase.db.ExecuteUpdateQuery(sql, ProcessBase.DBParameters);
		}

		public bool ignoreRights(int portfolioId) {
			if (_privateCache.ContainsKey("ignoreRights_" + portfolioId)) {
				return _privateCache["ignoreRights_" + portfolioId];
			}

			var pps = new ProcessPortfolios(Instance, Process, AuthenticatedSession);
			var pp = pps.GetPortfolio(portfolioId);

			var pagesDb = new PagesDb(Instance, AuthenticatedSession);
			var pageParams = new Dictionary<string, object>();
			pageParams.Add("portfolioId", (long) portfolioId);
			pageParams.Add("process", Process);
			var pageStates = pagesDb.GetStates("portfolio", pageParams);
			if (pageStates.Contains("ignoreRights") || Process.Equals("task") || pp.IgnoreRights) {
				_privateCache["ignoreRights_" + portfolioId] = true;
				return true;
			}

			_privateCache["ignoreRights_" + portfolioId] = false;
			return false;
		}

		public void DeleteListUserGroups(int ugId, string instance) {
			foreach (DataRow r in ProcessBase.db
				.GetDatatable(
					"SELECT p.process FROM item_columns ic" +
					" INNER JOIN processes p ON ic.process = p.process " +
					"where name = 'user_group' AND p.list_process = 1",
					ProcessBase.DBParameters).Rows) {
				foreach (DataRow r2 in ProcessBase.db.GetDatatable("SELECT user_group, item_id FROM _" +
				                                                   Conv.ToSql(instance) + "_" +
				                                                   Conv.ToStr(r["process"])).Rows) {
					var usergroupIds = new[] {r2["user_group"]};

					foreach (var usergroupId in usergroupIds) {
						foreach (var id2 in Conv.ToStr(usergroupId).Split(',')) {
							if (id2 == Conv.ToStr(ugId)) {
								var p = Conv.ToStr(usergroupId).IndexOf(id2);
								var substr = Conv.ToStr(usergroupId).Substring(p, id2.Length);

								var removeIdString = Conv.ToStr(usergroupId).Replace(substr, "");
								ProcessBase.SetDBParam(SqlDbType.NVarChar, "@usergroups",
									RemoveExtraCommas(removeIdString));
								ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(r2["item_id"]));
								ProcessBase.db.ExecuteUpdateQuery(
									"UPDATE _" + Conv.ToSql(instance) + "_" + Conv.ToStr(r["process"]) +
									" SET user_group = @usergroups WHERE item_id = @itemId", ProcessBase.DBParameters);
							}
						}
					}
				}
			}
		}

		private string RemoveExtraCommas(string toBeParsed) {
			return Regex.Replace(toBeParsed, ",+", ",").Trim(',');
		}

		public enum RestoreMode {
			Restore = 1,
			GetAndTest = 2,
			FastGet = 3
		}

		public class RestoreResult {
			public int ItemId;
			public int ParentItemId;
			public string Instance;
			public string Process;
			public int LastModifiedBy;
			public DateTime? LastModifiedOn;
			public bool Deleted;
			public bool Error;
			public string ErrorMessage;
			public string Identifier;

			public RestoreResult(DataRow d) {
				ItemId = Conv.ToInt(d["item_id"]);
				ParentItemId = Conv.ToInt(d["parent_item_id"]);
				Instance = Conv.ToStr(d["instance"]);
				Process = Conv.ToStr(d["process"]);
				LastModifiedBy = Conv.ToInt(d["last_modified_by"]);
				LastModifiedOn = Conv.ToDateTime(d["last_modified_on"]);
				Deleted = Conv.ToBool(d["deleted"]);
				Error = Conv.ToBool(d["error"]);
				ErrorMessage = Conv.ToStr(d["error_msg"]);
				Identifier = Conv.ToStr(d["identifier"]);
			}
		}

		public List<RestoreResult> Restore(int itemId, RestoreMode restoreMode = RestoreMode.Restore,
			int? archiveId = null,
			DateTime? archiveDate = null) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@restoreMode", (int) restoreMode);
			ProcessBase.SetDBParam(SqlDbType.Int, "@archiveId", Conv.IfNullThenDbNull(archiveId));
			ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", Conv.IfNullThenDbNull(archiveDate));

			var sql = "DECLARE @restored_items NVARCHAR(MAX) = NULL " +
			          "EXEC app_restore_item @itemId, @archiveId, @archiveDate, @restoreMode, @restored_items OUTPUT " +
			          "IF @restored_items IS NOT NULL BEGIN " +
					  "		CREATE TABLE #result_table (" +
					  "			item_id INT, " +
					  "			parent_item_id INT, " +
					  "			instance NVARCHAR(10), " +
					  "			process NVARCHAR(50), " +
					  "			last_modified_by INT, " +
					  "			last_modified_on DATETIME, " +
					  "			deleted INT, " +
					  "			error INT, " +
					  "			error_msg NVARCHAR(MAX)" +
					  "		)" +
					  "		INSERT INTO #result_table " +
					  "		SELECT item_id, parent_item_id, instance, process, last_modified_by, last_modified_on, deleted, error, error_msg " +
			          "		FROM OPENJSON (@restored_items) WITH ( " +
			          "			item_id INT '$.item_id', " +
			          "			parent_item_id INT '$.parent_item_id', " +
			          "			instance NVARCHAR(10) '$.instance', " +
			          "			process NVARCHAR(50) '$.process', " +
			          "			last_modified_by INT '$.restored_by', " +
			          "			last_modified_on DATETIME '$.restored_at', " +
			          "			deleted INT '$.deleted', " +
			          "			error INT '$.error', " +
			          "			error_msg NVARCHAR(MAX) '$.error_msg' " +
			          "		) " +
					  "		ALTER TABLE #result_table ADD identifier NVARCHAR(MAX) NOT NULL DEFAULT '?' " +
					  "		CREATE TABLE #columns_table (process NVARCHAR(50), [name] NVARCHAR(255)) " +
					  "		INSERT INTO #columns_table SELECT oq.process, " +
					  "		ISNULL(CASE WHEN p.list_process = 1 THEN ISNULL(oq.colname, 'list_item') ELSE oq.colname END, " +
					  "			CASE WHEN (SELECT item_column_id FROM item_columns WHERE instance = oq.instance AND process = oq.process AND [name] = 'title') IS NULL THEN '?' ELSE 'title' END " +
					  "		) AS [name] " +
					  "		FROM (" +
					  "			SELECT DISTINCT process, cols.colname, cols.colprocess, cols.colorder, id.instance " +
					  "			FROM #result_table id " +
					  "			LEFT JOIN (" +
					  "				SELECT ic.[name] AS colname, ic.process AS colprocess, ppc.order_no AS colorder, ic.instance " +
					  "				FROM process_portfolio_columns ppc " +
					  "					INNER JOIN (" +
					  "						SELECT process, process_portfolio_id, variable, process_portfolio_type " +
					  "						FROM process_portfolios " +
					  "						WHERE  process_portfolio_type IN (1, 10) " +
					  "					) pf " +
					  "					ON pf.process_portfolio_id = ppc.process_portfolio_id " +
					  "				INNER JOIN item_columns ic " +
					  "				ON ic.item_column_id = ppc.item_column_id " +
					  "				AND ic.data_type = 0 " +
					  "				WHERE use_in_select_result = 1 " +
					  "			) cols ON cols.colprocess = id.process " +
					  "			AND cols.instance = id.instance " +
					  "		) oq " +
					  "		LEFT JOIN processes p ON p.process = oq.process " +
					  "		ORDER BY oq.process ASC, colorder  ASC " +
					  "		DECLARE @result_cursor CURSOR " +
					  "		DECLARE @item_id INT " +
					  "		DECLARE @_instance NVARCHAR(10) " +
					  "		DECLARE @_process NVARCHAR(50) " +
					  "		DECLARE @columns NVARCHAR(MAX) " +
					  "		DECLARE @sql NVARCHAR(MAX) " +
					  "		DECLARE @last_modified_on DATETIME " +
					  "		SET @result_cursor = CURSOR FAST_FORWARD LOCAL FOR SELECT item_id, instance, process, last_modified_on FROM #result_table " +
					  "		OPEN @result_cursor " +
					  "		FETCH NEXT FROM @result_cursor INTO @item_id, @_instance, @_process, @last_modified_on " +
					  "			WHILE @@fetch_status = 0 BEGIN " +
					  "			SELECT @columns = STUFF((SELECT '+'' ''+' + [name] FROM #columns_table WHERE process = @_process AND [name] <> '?' FOR XML PATH ('')), 1, 5, '') " +
					  "			IF @columns IS NOT NULL BEGIN" +
					  "				SET @sql = 'UPDATE #result_table SET identifier = ISNULL((SELECT ' + @columns + ' FROM get__' + @_instance + '_' + @_process + '_with_id(@item_id, @last_modified_on)), ''?'') WHERE item_id = @item_id' " +
					  "				EXEC sp_executesql @sql, N'@item_id INT, @last_modified_on DATETIME', @item_id, @last_modified_on " +
					  "			END" +
					  "			FETCH NEXT FROM @result_cursor INTO @item_id, @_instance, @_process, @last_modified_on " +
					  "		END " +
					  "		SELECT * FROM #result_table " +
			          "END";

			var restoredItems = ProcessBase.db.GetDatatable(ProcessBase.db.addContextInfo(sql), ProcessBase.DBParameters);

			if (restoreMode == RestoreMode.Restore && Conv.ToInt(restoredItems.Compute("MAX(error)", "")) == 0)
				InvalidateCache();

			return (from DataRow r in restoredItems.Rows select new RestoreResult(r)).ToList();
		}
	}
}