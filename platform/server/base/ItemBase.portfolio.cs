﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceUnions;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.@base {
	/// <summary>
	/// Work in progress.
	/// Goal is to get rid of this completely.
	/// Good ol' ItemBase. This is currently split into multiple files.
	/// DatabaseItemService handles actions for plain database objects
	/// FavouriteService handles favourites (doh!)
	/// ItemCollectionService handles collection of entities. Used with searches.
	/// </summary>
	public partial class ItemBase : SessionBaseInfo, IDatabaseItemService {
		public List<Dictionary<string, object>> GetRelatedParentItems(int itemId, int columnId) {
			var result = new List<Dictionary<string, object>>();
			var s = GetItemDataProcessSelectionsObject();

			foreach (var se in s.GetParentItemDataProcessSelection(itemId, columnId)) {
				result.Add(DatabaseItemService.GetItem(se.ItemId));
			}

			return result;
		}

		public List<Dictionary<string, object>> FindItemsWithSearch(int portfolioId, string search, string textSearch,
			string filters, string ordercol, string orderdir, DateTime? archiveDate = null) {
			orderdir = Conv.ToSql(orderdir.Replace("'", ""));

			var cols = ItemColumns;
			var result = new List<Dictionary<string, object>>();
			var pSqls = _portfolioSqlQueryService.GetSql(portfolioId, textSearch);
			var iCols = ItemColumns;

			var w = " 1=1 ";
			foreach (var c in search.Split(',')) {
				var cArr = c.Split(':');
				if (iCols.HasColumnName(cArr[0])) {
					w += " AND " + cArr[0] + " = @" + cArr[0];
					ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + cArr[0], cArr[1]);
				}
			}

			if (ordercol.Length > 0 && ordercol != "item_id") {
				var ordCol = cols.GetItemColumnByName(ordercol);
				if (ordCol.IsProcess()) {
					ProcessBase.SetDBParam(SqlDbType.Int, "@orderColId", ordCol.ItemColumnId);
					var ppc = new ProcessPortfolioColumns(Instance, ordCol.DataAdditional, AuthenticatedSession);
					var pCols = ppc.GetPrimaryColumns();
					var concatStr = "";
					foreach (var c in pCols) {
						concatStr += " + " + c.ItemColumn.Name;
					}

					ordercol = " STUFF((SELECT ', ' " + concatStr +
					           " FROM item_data_process_selections im INNER JOIN _" +
					           Instance + "_" + ordCol.DataAdditional + " spf" + ordCol.ItemColumnId +
					           " ON im.selected_item_id = spf" + ordCol.ItemColumnId +
					           ".item_id WHERE im.item_id = pf.item_id AND item_column_id = " +
					           ordCol.ItemColumnId +
					           " FOR XML PATH('')),1,1,'') ";
				} else if (ordCol.IsList()) {
					ProcessBase.SetDBParam(SqlDbType.Int, "@orderColId", ordCol.ItemColumnId);
					ordercol = " STUFF((SELECT ', ' + list_item FROM item_data_process_selections im INNER JOIN _" +
					           Instance + "_" +
					           ordCol.DataAdditional + " spf" + ordCol.ItemColumnId + " ON im.selected_item_id = spf" +
					           ordCol.ItemColumnId +
					           ".item_id WHERE im.item_id = pf.item_id AND item_column_id = " +
					           ordCol.ItemColumnId + " FOR XML PATH('')),1,1,'') ";
				} else {
					ordercol = ordCol.Name;
				}
			}

			var eSql1 = "";
			var esql2 = "";
			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					if (Conv.ToInt(col.DataAdditional2) == 5 || Conv.ToInt(col.DataAdditional2) == 6) {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name + "_str";
					} else {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name;
					}
				}

				eSql1 = " SELECT  * " + equationSql + " FROM ( ";
				esql2 = " ) pf ";
			}

			var sql = eSql1 + " SELECT pf.item_id,  '" + Instance + " ' as instance, '" + Process +
			          "' as process, pf.archive_userid, pf.archive_start, " +
			          string.Join(",", pSqls.sqlcols.ToArray()) + " FROM  ";

			if (archiveDate != null) {
				ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
				sql += " get_" + ProcessBase.tableName + "(@archiveDate)  pf ";
			} else {
				sql += ProcessBase.tableName + " pf ";
			}

			var f = new ItemFilterParser(filters, iCols, AuthenticatedSession.instance, "user",
				AuthenticatedSession);

			sql += esql2 + " " + string.Join(" ", pSqls.archiveJoins.ToArray()) + " " +
			       string.Join(" ", pSqls.joins.ToArray()) +
			       " WHERE 1 = 1 " + pSqls.GetSearchSql() + " AND " +
			       EntityRepository.GetWhere(portfolioId) + " AND " + w + f.Get() +
			       " ";
			sql += " ORDER BY " + ordercol + " " + orderdir;

			var pData = ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);

			var itemIds = new List<int>();
			foreach (DataRow row in pData.Rows) {
				itemIds.Add((int)row["item_id"]);
			}

			var idStr = string.Join(",", itemIds.ToArray());
			if (idStr.Length == 0) {
				idStr = "0";
			}
			
			DataTable idpsTable = null;
			DataTable containerColsTable = null;
			if (!ignoreRights(portfolioId) || hasProgressProgress(portfolioId)) {
				idpsTable = GetProcessSelections(idStr);
				containerColsTable = GetContainerCols();
			}

			pData = processPortfolioTable(portfolioId, pData, pSqls, null, containerColsTable, idpsTable,
				idStr: idStr);
			foreach (DataRow row in pData.Rows) {
				var t = Enumerable.Range(0, pData.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => pData.Rows[0].Table.Columns[i].ColumnName,
						i => row[pData.Rows[0].Table.Columns[i].ColumnName]
					);

				result.Add(t);
			}

			return result;
		}

		protected DataTable processPortfolioTable(int portfolioId, DataTable pData, PortfolioSqls pSqls,
			DataTable allData = null,
			DataTable containerCols = null, DataTable processSelections = null,
			Dictionary<int, object> statesRetval = null,
			int recursive = 0, string idStr = "") {
			if (allData == null && idStr != "") {
				var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process, ProcessBase.authenticatedSession);
				allData = ProcessBase.db.GetDatatable(
					ics.GetItemSql(onlyConditionColumns: true) + " WHERE item_id IN (" + idStr + ")", null);
			}

			if (!Process.Equals("task")) {
				pData.Columns.Add("bar_icons", typeof(object));
			}

			foreach (var col in pSqls.processColumns) {
				if (pData.Columns.Contains(col.Name) == false)
					pData.Columns.Add(col.Name, typeof(object));
			}

			foreach (var col in pSqls.transColumns) {
				if (pData.Columns.Contains(col.Name) == false)
					pData.Columns.Add(col.Name, typeof(object));
			}

			foreach (var col in pSqls.equationColumns) {
				if (pData.Columns.Contains(col.Name) == false && Conv.ToInt(col.DataAdditional2) == 5 ||
				    Conv.ToInt(col.DataAdditional2) == 6)
					pData.Columns.Add(col.Name, typeof(object));
			}

			var pp =
				new ProcessPortfolios(Instance, Process, AuthenticatedSession).GetPortfolio(portfolioId);

			var barIconColId = 0;
			var endDateCol = "end_date";
			var descCol = "";
			var listProcess = "list_bar_icon";
			var milestoneProcess = "task";
			var milestoneRelativeCol = "owner_item_id";

			var barIconSql = "";
			ItemColumn barIconCol = null;
			if (!Process.Equals("task")) {
				try {
					if (pSqls.milestoneColumn != null && pSqls.milestoneColumn.DataAdditional != null) {
						var barIconParams =
							JsonConvert.DeserializeObject<Dictionary<string, int>>(pSqls.milestoneColumn
								.DataAdditional2);
						var barIconIcs = new ItemColumns(Instance, pSqls.milestoneColumn.DataAdditional,
							AuthenticatedSession);
						milestoneProcess = pSqls.milestoneColumn.DataAdditional;

						var relcol = barIconIcs.GetItemColumn((int)pSqls.milestoneColumn.RelativeColumnId);
						milestoneRelativeCol = relcol.Name;

						barIconSql = "select t.item_id as item_id, list_symbol, list_symbol_fill";
						if (barIconParams.ContainsKey("milestoneColumnId")) {
							barIconColId = barIconParams["milestoneColumnId"];
							barIconCol = barIconIcs.GetItemColumn(barIconParams["milestoneColumnId"]);
							listProcess = barIconCol.DataAdditional;
						}

						if (barIconParams.ContainsKey("endDateColumnId")) {
							endDateCol = barIconIcs.GetItemColumn(barIconParams["endDateColumnId"]).Name;
							barIconSql += ", " + endDateCol + " end_date ";
						} else {
							barIconSql += ", getdate() end_date ";
						}

						if (barIconParams.ContainsKey("descriptionColumnId")) {
							descCol = barIconIcs.GetItemColumn(barIconParams["descriptionColumnId"]).Name;
							barIconSql += ", t." + descCol + " description ";
						} else {
							barIconSql += ", '' description ";
						}

						if (barIconParams.ContainsKey("dependencyColumnId")) {
							var dependencyCol = barIconIcs.GetItemColumn(barIconParams["dependencyColumnId"]);

							var dsql = " (STUFF((SELECT ',' + CAST((SELECT " + milestoneRelativeCol + " FROM _" +
							           Instance + "_" + milestoneProcess +
							           " WHERE item_id = d.selected_item_id) AS nvarchar) + ':' + CAST(d.selected_item_id AS NVARCHAR) FROM item_data_process_selections d " +
							           " WHERE d.item_id = t.item_id AND d.item_column_id = " +
							           dependencyCol.ItemColumnId +
							           " FOR XML PATH('')),1,1,'') " +
							           " ) AS dependencies ";

							barIconSql += ", " + dsql;
						} else {
							barIconSql += ", '' dependencies ";
						}

						if (relcol.IsNumber()) {
							barIconSql += " FROM _" + Instance + "_" + milestoneProcess + " t " +
							              "LEFT JOIN item_data_process_selections i ON t.item_id = i.item_id " +
							              "LEFT JOIN _" + Instance + "_" + listProcess +
							              " l ON i.selected_item_id = l.item_id " +
							              "WHERE " + milestoneRelativeCol +
							              " = @owner_item_id AND item_column_id = @barIconItemColId AND " +
							              "t." + endDateCol + " IS NOT null AND list_symbol IS NOT null " +
							              "ORDER BY t." + endDateCol + " ASC";
						} else {
							barIconSql += " FROM _" + Instance + "_" + milestoneProcess + " t " +
							              "INNER JOIN item_data_process_selections o " +
							              "ON o.item_id = t.item_id " +
							              "AND o.selected_item_id = @owner_item_id " +
							              "AND o.item_column_id = (select item_column_id FROM item_columns WHERE process='" +
							              relcol.Process + "' AND name='" + Conv.ToSql(milestoneRelativeCol) + "')" +
							              "LEFT JOIN item_data_process_selections i ON t.item_id = i.item_id LEFT JOIN _" +
							              Instance + "_" + listProcess + " l on i.selected_item_id = l.item_id " +
							              "WHERE t." + endDateCol + " is not null AND list_symbol IS NOT null " +
							              "ORDER BY t." + endDateCol + " ASC";
						}
					}
				} catch (Exception e) {
					Console.WriteLine(e.Message);
				}
			} else {
				var barIconIcs = new ItemColumns(Instance, "task", AuthenticatedSession);
				var col = barIconIcs.GetItemColumnByName("bar_icon");
				barIconColId = col.ItemColumnId;

				barIconSql =
					"select t.item_id as item_id, list_symbol, list_symbol_fill, end_date, t.title AS description, '' dependencies from _" +
					Instance +
					"_task t  left join item_data_process_selections i on t.item_id = i.item_id   left join _" +
					Instance +
					"_list_bar_icons l on i.selected_item_id = l.item_id  where owner_item_id  = @owner_item_id and item_column_id = @barIconItemColId and end_date is not null and list_symbol is not null ORDER BY end_date ASC";
			}

			foreach (DataRow row in pData.Rows) {
				try {
					string columnName;
					if (recursive > 0) {
						columnName = "actual_item_id";
					} else {
						columnName = "item_id";
					}

					if (!Process.Equals("task")) {
						if (barIconColId > 0) {
							ProcessBase.SetDBParam(SqlDbType.Int, "@owner_item_id", Conv.ToInt(row[columnName]));
							ProcessBase.SetDBParam(SqlDbType.Int, "@barIconItemColId", barIconColId);

							var barIconDta = new List<Dictionary<string, object>>();

							var dt = ProcessBase.db
								.GetDatatable(barIconSql, ProcessBase.DBParameters);

							foreach (DataRow barIconRow in dt.Rows) {
								var barIconDict = new Dictionary<string, object>();

								barIconDict.Add("item_id", barIconRow["item_id"]);
								barIconDict.Add("list_symbol", barIconRow["list_symbol"]);
								barIconDict.Add("list_symbol_fill", barIconRow["list_symbol_fill"]);
								barIconDict.Add("end_date", barIconRow["end_date"]);
								barIconDict.Add("description", barIconRow["description"]);
								barIconDict.Add("dependencies", barIconRow["dependencies"]);
								barIconDta.Add(barIconDict);
								row["bar_icons"] = barIconDta;
							}
						}
					}
				} catch (Exception e) {
					Console.WriteLine(e.Message);
				}

				var translations = new Translations(Instance, AuthenticatedSession);
				foreach (var col in pSqls.transColumns) {
					if (Conv.ToStr(row[col.Name + "_str"]).Length > 0) {
						row[col.Name] = translations.GetTranslations((string)row[col.Name + "_str"]);
					}
				}

				foreach (var col in pSqls.processColumns) {
					var v = Conv.ToStr(row[col.Name + "_str"]);
					if (v.Length > 0) {
						if (!col.IsTag()) {
							if (col.LinkProcess != null && col.Name.Equals("linked_tasks")) {
								var idps = GetItemDataProcessSelectionsObject(col.LinkProcess);
								row[col.Name] =
									idps.GetItemDataProcessSelectionIds(Conv.ToInt(row["item_id"]), col.ItemColumnId,
										col.LinkProcess, allData);
							} else {
								row[col.Name] = Array.ConvertAll(v.Split(','), s => int.Parse(s));
							}
						} else {
							row[col.Name] = v.Split(',');
						}
					} else {
						if (!col.IsTag()) {
							row[col.Name] = col.LinkProcess != null && col.Name.Equals("linked_tasks")
								? new object()
								: new int[0];
						} else {
							row[col.Name] = new string[0];
						}
					}
				}

				foreach (var col in pSqls.equationColumns) {
					if (Conv.ToInt(col.DataAdditional2) != 5 && Conv.ToInt(col.DataAdditional2) != 6) continue;
					var v = row.Table.Columns.Contains(col.Name + "_str")
						? Conv.ToStr(row[col.Name + "_str"])
						: Conv.ToStr(row[col.Name]);
					row[col.Name] = v.Length > 0
						? Array.ConvertAll(v.Split(','), int.Parse)
						: new List<int>().ToArray();
				}
			}

			foreach (var col in pSqls.processColumns) {
				pData.Columns.Remove(col.Name + "_str");
			}

			foreach (var col in pSqls.transColumns) {
				pData.Columns.Remove(col.Name + "_str");
			}

			var mStates = GetStates();

			var pagesDb = new PagesDb(Instance, AuthenticatedSession);
			var pageParams = new Dictionary<string, object>();
			pageParams.Add("portfolioId", (long)portfolioId);
			pageParams.Add("process", Process);
			var pageStates = pagesDb.GetStates("portfolio", pageParams);

			foreach (DataRow row in pData.Rows) {
				FeatureStates states = null;
				string columnName;
				if (recursive > 0) {
					columnName = "actual_item_id";
				} else {
					columnName = "item_id";
				}


				if (hasProgressProgress(portfolioId)) {
					var states2 = mStates.GetEvaluatedStates(Process, "progressProgress", (int)row[columnName],
						new List<string> {
							"progressProgress.read", "progressProgress.write", "progressProgress.red",
							"progressProgress.yellow", "progressProgress.green", "progressProgress.grey",
							"progressProgress.black", "progressProgress.accent-1", "progressProgress.accent-2"
						},
						allData, processSelections);

					var r = JsonConvert.SerializeObject(states2.groupStates).Replace("progressProgress.", "");
					states2.groupStates = JsonConvert.DeserializeObject<Dictionary<int, EvaluatedGroupState>>(r);
					foreach (var tab in states2.groupStates) {
						tab.Value.tabStates = null;
					}

					if (statesRetval != null) statesRetval[(int)row[columnName]] = states2;
				} else {
					if (statesRetval != null) statesRetval[(int)row[columnName]] = new FeatureStates();
				}

				//TODO: Remove process task whem widgets has proper rights
				if (ignoreRights(portfolioId)) {
					continue;
				}

				states = mStates.GetEvaluatedStates(Process, "meta", (int)row[columnName],
					new List<string> { "meta.read", "meta.write" },
					allData, processSelections);


				var pcc = new ProcesContainerColumns(Instance, Process, AuthenticatedSession);
				var userCols = new List<string>();

				userCols.Add("instance");
				userCols.Add("process");
				userCols.Add("item_id");
				userCols.Add("actual_item_id");
				userCols.Add("parent_item_id");
				userCols.Add("archive_userid");
				userCols.Add("archive_start");
				userCols.Add("states");
				userCols.Add("order_no");
				userCols.Add("RowNum");
				userCols.Add("bar_icons");


				foreach (DataRow row2 in containerCols.Rows) {
					if (states.HasContainerState((int)row2["process_container_id"],
						    new[] { "meta.write", "meta.read" })) {
						if (row2["name"] != null && row2["name"] != DBNull.Value) {
							userCols.Add((string)row2["name"]);
						}
					}
				}

				foreach (var s in pSqls.statusColumns) {
					if (!userCols.Contains(s)) {
						userCols.Add(s);
					}
				}

				foreach (DataColumn col in row.Table.Columns) {
					if (!userCols.Contains(col.ColumnName)) {
						row[col.ColumnName] = DBNull.Value;
					}
				}
			}

			return pData;
		}

		public string GetFindItemsWhere(int portfolioId, PortfolioOptions q) {
			var filters = q.filters;
			var textSearch = q.text;
			var pSqls = _portfolioSqlQueryService.GetSql(portfolioId, textSearch, q.archiveDate);
			var searchSql = pSqls.GetSearchSql();

			var itemFilters = new ItemFilterParser(filters, ItemColumns, Instance, Process, AuthenticatedSession);
			var filterSql = itemFilters.Get();

			var restrictionFilters = new ItemFilterParser(q.restrictions, ItemColumns, Instance, Process,
				AuthenticatedSession, true);
			var restrictionSql = restrictionFilters.Get();

			if (filterSql.Length > 0 && restrictionSql.Length > 0 &&
			    !restrictionSql.ToLower().TrimStart().StartsWith("and")) {
				filterSql += " AND " + restrictionSql;
			} else if (restrictionSql.Length > 0) {
				filterSql += " " + restrictionSql;
			}

			var sql = filterSql + searchSql + " AND " + EntityRepository.GetWhere(portfolioId);

			//Add Context Restrictions
			if (q.context != null) {
				var ics = new ItemCollectionService(Instance, Process, AuthenticatedSession);
				q.itemsRestriction = ics.AddContextualItemIds(q.context.process, q.context.itemId,
					q.context.itemColumnId, q.itemsRestriction);
			}

			if (q.itemsRestriction != null && q.itemsRestriction.Count > 0) {
				var idLimitStr = string.Join(",", q.itemsRestriction.ToArray());
				sql += " AND pf.item_id IN ( " + idLimitStr + " ) ";
			}

			return sql;
		}

		public Dictionary<string, object> FindItems(int offset, int limit, int portfolioId, int parentItemId = 0,
			int itemColumnId = 0, string textSearch = "", string filters = "", string ordercol = "item_id",
			string orderdir = "DESC", string filterPhases = "", bool isFlat = false, List<int> idsLimit = null) {
			var q = new PortfolioOptions();
			q.filters = new Dictionary<int, object>();
			q.offset = offset;
			q.limit = limit;
			q.text = textSearch;
			q.sortBy = ordercol;
			q.order = orderdir;
			q.recursive = !isFlat;
			q.itemsRestriction = idsLimit;

			return FindItems(portfolioId, q);
		}

		public Dictionary<string, object> FindItems(int portfolioId, PortfolioOptions q, bool includeItemId = true,
			bool onlySelectorColumns = true) {
			//Set offset and limit
			var offset = q.offset;
			var limit = q.limit;
			if (offset > 0) {
				limit--;
				offset++;
			}

			//Get Portfolio SQLs
			var textSearch = q.text;
			var filterPhases = q.phases;
			var ordercol = q.sortBy;
			var orderdir = Conv.ToSql(q.order);
			var pp = new ProcessPortfolios(Instance, Process, AuthenticatedSession).GetPortfolio(portfolioId);
			var processes = new Processes(Instance, AuthenticatedSession);
			var table = processes.GetProcess(Process).ProcessType == 5;


			var pSqls = _portfolioSqlQueryService.GetSql(portfolioId, textSearch, q.archiveDate, table);
			if (includeItemId) pSqls.cols.Add("item_id");

			//If getting only count -- remove columns
			if (q.onlyCount) {
				pSqls.cols.Clear();
				pSqls.sqlcols.Clear();
				pSqls.sumcols.Clear();
				pSqls.equationColumns.Clear();
				pSqls.sqCols.Clear();
				pSqls.sqlcols.Add("'count' AS count");
			}

			//Add Item Filters
			var itemIdField = "item_id";
			var itemFilters = new ItemFilterParser(q.filters, ItemColumns, Instance, Process, AuthenticatedSession);
			var filterSql = itemFilters.Get();

			//Add Restriction Filters
			var restrictionFilters = new ItemFilterParser(q.restrictions, ItemColumns, Instance, Process,
				AuthenticatedSession, true);
			var restrictionSql = restrictionFilters.Get();
			if (filterSql.Length > 0 && restrictionSql.Length > 0 &&
			    !restrictionSql.ToLower().TrimStart().StartsWith("and")) {
				filterSql += " AND " + restrictionSql;
			} else if (restrictionSql.Length > 0) {
				filterSql += " " + restrictionSql;
			}

			//Create Portfolio Data
			var result = new Dictionary<string, object>();
			DataTable pData = null;
			var sql = "";
			var searchSql = pSqls.GetSearchSql();

			if (q.recursive == false) pp.Recursive = 0;

			if (pp.IsUnionPortfolio) {
				ProcessUnion(portfolioId, ref pSqls, ref q, ref sql, ref filterSql, ref searchSql, ref filterPhases,
					ref pp, ref limit, ref offset, ref ordercol, ref orderdir, ref pData, ref result, ref itemIdField);
			} else if (Conv.ToInt(pp.Recursive) > 0)
				ProcessRecursive(portfolioId, ref pSqls, ref q, ref sql, ref filterSql, ref searchSql, ref filterPhases,
					ref pp, ref limit, ref offset, ref ordercol, ref orderdir, ref pData, ref result, ref itemIdField);
			else
				ProcessFlat(portfolioId, ref pSqls, ref q, ref sql, ref filterSql, ref searchSql, ref filterPhases,
					ref pp, ref limit, ref offset, ref ordercol, ref orderdir, ref pData, ref result, ref itemIdField);

			//Add Row Total from last ran query
			result.Add("rowcount", ProcessBase.db.lastRowTotal);
			result.Add("actual_row_count", ProcessBase.db.lastRowTotal2);

			//If getting count -- return before additional processing
			if (q.onlyCount) {
				result.Add("rowdata", new List<string>());
				return result;
			}


			//Resolve data for limited filter subquery lists
			var filterListResolve = new List<string>();
			foreach (var col in pSqls.processColumns) {
				if (col.IsSubquery() && col.IsOfSubType(ItemColumn.ColumnType.List)) {
					if (RegisteredSubQueries.GetType(col.DataAdditional, col.ItemColumnId).FilterWithResolvedValues)
						filterListResolve.Add(ItemColumns.GetSubQuery(col));
				}
			}

			//Resolve Filter list values
			var filterListResolvedValues = new Dictionary<string, List<int>>();
			if (filterListResolve.Count > 0) {
				var baseSql = sql.Substring(sql.IndexOf("WHERE 11 = 11"));
				var resolveTotalSql = "SELECT DISTINCT ";
				foreach (var resolveSql in filterListResolve) {
					resolveTotalSql += resolveSql;
				}

				resolveTotalSql += " FROM " + ProcessBase.tableName + " pf " + baseSql;
				var resolveResultTable = ProcessBase.db.GetDatatable(resolveTotalSql, ProcessBase.DBParameters);

				foreach (DataRow resolveRow in resolveResultTable.Rows) {
					foreach (DataColumn resolveCol in resolveResultTable.Columns) {
						List<int> colList;
						if (!filterListResolvedValues.ContainsKey(resolveCol.ColumnName))
							filterListResolvedValues.Add(resolveCol.ColumnName, new List<int>());
						colList = filterListResolvedValues[resolveCol.ColumnName];
						colList.Add(Conv.ToInt(resolveRow[resolveCol.ColumnName]));
					}
				}
			}

			var itemIds = new List<int>();
			foreach (DataRow row in pData.Rows) {
				itemIds.Add((int)row[itemIdField]);
			}

			var idStr = string.Join(",", itemIds.ToArray());
			if (idStr.Length == 0) idStr = "0";

			var statesRetval = new Dictionary<int, object>();
			
			DataTable idpsTable = null;
			DataTable containerColsTable = null;
			if (!ignoreRights(portfolioId) || hasProgressProgress(portfolioId)) {
				idpsTable = GetProcessSelections(idStr);
				containerColsTable = GetContainerCols();
			}

			var rowData = processPortfolioTable(portfolioId, pData, pSqls, null, containerColsTable, idpsTable,
				statesRetval, pp.Recursive, idStr: idStr);

			result.Add("rowdata", rowData);
			result.Add("rowstates", statesRetval);
			result.Add("filterList", filterListResolvedValues);
			result.Add("processes", HandlePortfolioProcessData(pp, pSqls, pData, onlySelectorColumns));

			return result;
		}

		private void ProcessRecursive(int portfolioId, ref PortfolioSqls pSqls, ref PortfolioOptions q, ref string sql,
			ref string filterSql,
			ref string searchSql, ref List<int> filterPhases, ref ProcessPortfolio pp, ref int limit, ref int offset,
			ref string ordercol,
			ref string orderdir, ref DataTable pData, ref Dictionary<string, object> result, ref string itemIdField) {
			itemIdField = "actual_item_id";
			var recursivecol = ItemColumns.GetItemColumn(pp.Recursive);
			if (recursivecol.InputMethod == 3) {
				ProcessBase.SetDBParam(SqlDbType.Int, "@itemColumnId", Conv.ToInt(recursivecol.RelativeColumnId));
			} else {
				ProcessBase.SetDBParam(SqlDbType.Int, "@itemColumnId", Conv.ToInt(pp.Recursive));
			}

			if (limit > 0) {
				var s = new SqlParameter();
				s.ParameterName = "internal_limit_start";
				s.SqlDbType = SqlDbType.Int;
				s.Value = offset;

				var e = new SqlParameter();
				e.ParameterName = "internal_limit_end";
				e.SqlDbType = SqlDbType.Int;
				e.Value = offset + limit;

				ProcessBase.DBParameters.Add(s);
				ProcessBase.DBParameters.Add(e);
			}

			var sql2 =
				" select 0 as actual_parent_item_id, item_id as actual_item_id,  0 as level , cast('0' as nvarchar) as parent_item_id, cast('0_' + cast(item_id as nvarchar) as nvarchar) as item_id ";

			var orderCol2 = "item_id";
			if (ordercol == null) ordercol = "item_id";
			if (ordercol == "order_no") orderCol2 = "order_no ";

			if (ordercol.Length > 0 && ordercol != "item_id" && ordercol != "order_no") {
				orderCol2 = ResolveOrderColumn(ordercol, "pf");
			}

			if (orderCol2.Length > 0 && orderCol2.Equals("item_id")) orderCol2 = "pf.item_id";

			//Use single sort if no multisort columns are provided
			var finalorder = new List<string>();
			foreach (var s in q.sort) {
				if (Conv.ToStr(s.column) != "" && Conv.ToStr(s.column) != "item_id" &&
				    Conv.ToStr(s.column) != "order_no")
					finalorder.Add(ResolveOrderColumn(s.column, "pf") + " " + s.order);
				if (Conv.ToStr(s.column) != "" &&
				    (Conv.ToStr(s.column) == "order_no" || Conv.ToStr(s.column) == "item_id")) {
					orderdir = s.order;
					ordercol = s.column;
				}
			}

			if (finalorder.Count == 0) {
				if (Conv.ToStr(ordercol) != "" && ordercol != "item_id" && ordercol != "order_no") {
					finalorder.Add(orderCol2.Replace("result.actual_item_id", "item_id") + " " + orderdir);
					orderCol2 = Modi.JoinListWithComma(finalorder);
				}
			} else {
				orderCol2 = Modi.JoinListWithComma(finalorder);
			}

			orderCol2 = orderCol2.Replace("pf.item_id", "actual_item_id");
			pSqls.sqlcols.Remove(("pf.parent_item_id"));
			sql2 += ", " + Conv.ToDelimitedString(pSqls.sqlcols);
			sql2 += " FROM ";
			if (q.archiveDate != null) {
				ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", q.archiveDate);
				sql2 += " get_" + ProcessBase.tableName + "(@archiveDate)  pf ";
			} else {
				sql2 += ProcessBase.tableName + " pf ";
			}

			sql2 +=
				string.Join(" ", pSqls.archiveJoins.ToArray()) +
				string.Join(" ", pSqls.joins.ToArray()) +
				"  WHERE 1 = 1 " + filterSql + searchSql + " AND " + EntityRepository.GetWhere(portfolioId);

			if (filterPhases.Count > 0) sql2 += " AND " + getPhaseFilterWhere(filterPhases);

			//We need this string to know amount of all rows of portfolio (not just parent row amount)
			var strB = String.Copy(sql2);

			if (recursivecol.InputMethod == 3) {
				sql2 +=
					" AND item_id not in (select selected_item_id from item_data_process_selections where item_column_id = @itemColumnId ) ";
			} else {
				sql2 +=
					" AND item_id not in (select item_id from item_data_process_selections where item_column_id = @itemColumnId ) ";
			}

			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					if (Conv.ToInt(col.DataAdditional2) == 5 || Conv.ToInt(col.DataAdditional2) == 6) {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name + "_str";
					} else {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name;
					}
				}

				sql2 = " SELECT * " + equationSql + " FROM ( " + sql2 + ") pf ";
			}

			var sql3 = "SELECT COUNT(*) FROM( " + sql2 + " ) as tmp; ";
			//We need this string to know amount of all rows of portfolio (not just parent row amount)
			var actualAmountOfRows = "SELECT COUNT(*) FROM( " + strB + " ) as tmp; ";

			sql +=
				" DECLARE @level0 TABLE (internal_order INT, actual_parent_item_id int, actual_item_id int, level int, parent_item_id nvarchar(max), item_id nvarchar(max)); " +
				" WITH query AS ( SELECT ROW_NUMBER() OVER (ORDER BY " + orderCol2 +
				" ) AS ROWID, * FROM ( " +
				sql2 +
				" ) AS result ) " +
				" insert into @level0 (internal_order, actual_parent_item_id, actual_item_id, level, parent_item_id, item_id)  " +
				" SELECT rowid, actual_parent_item_id, actual_item_id, level, parent_item_id, item_id FROM query ";


			var limitSql = "";
			sql += " ##limitSql## ";
			if (limit > 0) limitSql = " WHERE ROWID BETWEEN @internal_limit_start AND @internal_limit_end ";
			sql += ";";

			var recif =
				" join rec on rec.actual_item_id = idps.selected_item_id where item_column_id = @itemColumnId ";
			var reccol1 = " idps.selected_item_id ";
			var reccol2 = " idps.item_id ";
			if (recursivecol.InputMethod == 3) {
				recif = " join rec on rec.actual_item_id = idps.item_id where item_column_id = @itemColumnId ";
				reccol1 = " idps.item_id ";
				reccol2 = " idps.selected_item_id ";
			}

			recif += " AND level < 4 ";
			sql += " WITH rec AS ( " +
			       " select internal_order, actual_parent_item_id, actual_item_id,  level , cast(parent_item_id as nvarchar) as parent_item_id,  cast(item_id as nvarchar) as item_id " +
			       " from @level0 " +
			       " UNION ALL " +
			       " select 0 AS internal_order, " + reccol1 + " as actual_parent_item_id, " + reccol2 +
			       " as actual_item_id, rec.level + 1 as level  , cast(rec.item_id as nvarchar) as parent_item_id, cast(rec.item_id + '_' + cast(" +
			       reccol2 + " as nvarchar) as nvarchar) as item_id " +
			       " from item_data_process_selections idps inner " +
			       recif +
			       " ) ";

			if (pSqls.equationColumns.Count > 0) {
				var eSql1 = "";
				var esql2 = "";
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					if (Conv.ToInt(col.DataAdditional2) == 5 || Conv.ToInt(col.DataAdditional2) == 6) {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name + "_str";
					} else {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name;
					}
				}

				eSql1 = " SELECT  pf.item_id as actual_item_id, pf.rec_item_id as item_id, * " + equationSql +
				        " FROM ( ";
				esql2 = " ) pf ";
				if (pSqls.sqlcols.Contains("pf.parent_item_id")) pSqls.sqlcols.Remove("pf.parent_item_id");
				sql += eSql1 +
				       " SELECT rec.internal_order, rec.item_id as rec_item_id, rec.parent_item_id as parent_item_id, rec.actual_item_id as item_id , '" +
				       Instance + " ' as instance, '" + Process +
				       "' as process, pf.archive_userid, pf.archive_start, "
				       + string.Join(",", pSqls.sqlcols.ToArray());
				sql += " FROM rec  INNER JOIN ";

				if (q.archiveDate != null) {
					ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", q.archiveDate);
					sql += " get_" + ProcessBase.tableName + "(@archiveDate)  pf ";
				} else {
					sql += ProcessBase.tableName + " pf ";
				}

				sql += " on rec.actual_item_id = pf.item_id " + esql2;
			} else {
				sql +=
					" SELECT rec.internal_order, rec.item_id as item_id, rec.parent_item_id as parent_item_id, rec.actual_item_id as actual_item_id , '" +
					Instance + " ' as instance, '" + Process + "' as process, pf.archive_userid, pf.archive_start, "
					+ string.Join(",", pSqls.sqlcols.ToArray());
				sql += " FROM rec  INNER JOIN ";
				if (q.archiveDate != null) {
					ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", q.archiveDate);
					sql += " get_" + ProcessBase.tableName + "(@archiveDate)  pf ";
				} else {
					sql += ProcessBase.tableName + " pf ";
				}

				sql += " on rec.actual_item_id = pf.item_id ";
			}

			sql += string.Join(" ", pSqls.archiveJoins.ToArray())
			       + string.Join(" ", pSqls.joins.ToArray()) + "  WHERE 1 = 1 ";

			if (pp.RecursiveFilterType != 2) {
				sql += filterSql + searchSql
				                 + " AND " + EntityRepository.GetWhere(portfolioId);
				if (filterPhases.Count > 0) {
					sql += " AND " + getPhaseFilterWhere(filterPhases);
				}
			}

			if (q.excludeItemIds != null && q.excludeItemIds.Count > 0) {
				var idLimitStr69 = string.Join(",", q.excludeItemIds.ToArray());
				sql += " AND pf.item_id NOT IN ( " + idLimitStr69 + " ) ";
			}

			sql += "ORDER BY internal_order ASC";

			foreach (var s in q.sort) {
				if (s.column == "item_id")
					sql += " ,pf.item_id " + s.order;
				else
					sql += " ," + "(" + ResolveOrderColumn(s.column, "pf") + ")" + " " + s.order;
			}

			var pagingSql = sql;
			if (limitSql.Length > 0) pagingSql = sql.Replace("##limitSql##", limitSql);

			//Add Context Restrictions
			if (q.context != null) {
				var ics = new ItemCollectionService(Instance, Process, AuthenticatedSession);
				q.itemsRestriction = ics.AddContextualItemIds(q.context.process, q.context.itemId,
					q.context.itemColumnId, q.itemsRestriction);
			}

			if (q.itemsRestriction != null && q.itemsRestriction.Count > 0) {
				var idLimitStr = string.Join(",", q.itemsRestriction.ToArray());
				sql += " pf.item_id IN ( " + idLimitStr + " ) ";
			}

			pData = ProcessBase.db.GetDatatable(pagingSql, ProcessBase.DBParameters);
			var originalPData = (DataTable)Conv.DeserializeToObject(Conv.SerializeToString(pData));
			if (pSqls.sumcols.Count > 0) {
				CalcRecursivePrepare();
				foreach (var parentRow in pData.Select("parent_item_id = '0'")) {
					CalcRecursiveSum(pData, pSqls, parentRow);
				}

				CalcRecursiveComplete(pData, pSqls.sumcols);

				var allData =
					ProcessBase.db.GetDatatable(sql.Replace("##limitSql##", ""), ProcessBase.DBParameters);

				CalcRecursivePrepare();
				foreach (var parentRow in allData.Select("parent_item_id = '0'")) {
					CalcRecursiveSum(allData, pSqls, parentRow);
				}

				CalcRecursiveComplete(allData, pSqls.sumcols);

				foreach (var col in pSqls.sumcols) {
					double sum = 0;
					foreach (var col2 in allData.Select("parent_item_id = '0'")) {
						sum += Conv.ToDouble(col2[col]);
					}

					result.Add(col, sum);
				}
			}

			// Recursive calculation was modifying original values, now fixed -BSa MPe 02/06/20
			pData = originalPData;

			// ...
			ProcessBase.db.lastRowTotal = 0;
			if (pData.Rows.Count > 0) {
				ProcessBase.db.lastRowTotal =
					Conv.ToInt(ProcessBase.db.ExecuteScalar(sql3, ProcessBase.DBParameters));

				ProcessBase.db.lastRowTotal2 =
					Conv.ToInt(ProcessBase.db.ExecuteScalar(actualAmountOfRows, ProcessBase.DBParameters));
			}
		}

		private string GetCustomJoins(ProcessPortfolio p) {
			if (p.Variable == "port_allocation_allocation_durations")
				return "INNER JOIN allocation_durations system_ad ON pf.item_id = system_ad.allocation_item_id ";
			return "";
		}

		private void ProcessFlat(int portfolioId, ref PortfolioSqls pSqls, ref PortfolioOptions q, ref string sql,
			ref string filterSql,
			ref string searchSql, ref List<int> filterPhases, ref ProcessPortfolio pp, ref int limit, ref int offset,
			ref string ordercol,
			ref string orderdir, ref DataTable pData, ref Dictionary<string, object> result, ref string itemIdField) {
			itemIdField = "item_id";

			//If equations are in use, add cached subquery columns so that they can be used in them
			if (pSqls.equationColumns.Count > 0) {
				foreach (var col in pSqls.sqCols) {
					var subQuery = RegisteredSubQueries.GetType(col.DataAdditional, col.ItemColumnId);
					if (subQuery != null && subQuery.HasCache()) pSqls.sqlcols.Add("pf.cache_" + col.Name);
				}
			}

			//Add Columns to SELECT
			var sql2 = "SELECT pf.item_id, '" + Instance + " ' as instance, '" +
			           Process +
			           "' as process, pf.archive_userid, pf.archive_start, "
			           + string.Join(",", pSqls.sqlcols.ToArray());
			sql2 += " FROM ";
			var countSql = "";


			foreach (var sc in pSqls.sumcols) {
				var sqlco = pSqls.sqCols.FirstOrDefault(pc => pc.Name == sc);
				if (sqlco != null) {
					countSql += (countSql == "" ? "" : ",") + sqlco.GetColumnQuery(Instance) + " " + sqlco.Name;
				} else {
					if (!pSqls.equationColumns.Exists(cc => cc.Name == sc))
						countSql += (countSql == "" ? "" : ",") + sc;
				}
			}

			countSql += " FROM ";

			//Add Table to FROM (and archive date if history)
			if (q.archiveDate != null) {
				ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", q.archiveDate);
				sql2 += " get_" + ProcessBase.tableName + "(@archiveDate)  pf ";
				countSql += " get_" + ProcessBase.tableName + "(@archiveDate)  pf ";
			} else {
				sql2 += ProcessBase.tableName + " pf ";
				countSql += ProcessBase.tableName + " pf ";
			}

			//Custom Joins
			sql2 += GetCustomJoins(pp);

			//Add Equation columns
			var equationSql = "";
			var countEquationSql = "";
			if (pSqls.equationColumns.Count > 0) {
				foreach (var col in pSqls.equationColumns) {
					if (Conv.ToInt(col.DataAdditional2) == 5 || Conv.ToInt(col.DataAdditional2) == 6) {
						var s = "," + ItemColumns.GetEquationQuery(col,
							Conv.ToInt(col.DataAdditional2) == 5) + " " + col.Name + "_str";
						equationSql += s;
						if (pSqls.sumcols.Contains(col.Name)) countEquationSql += s;
					} else {
						var s = "," + ItemColumns.GetEquationQuery(col) + " " + col.Name;
						equationSql += s;
						if (pSqls.sumcols.Contains(col.Name)) countEquationSql += s;
					}
				}

				sql2 = " SELECT * " + equationSql + " FROM ( " + sql2 + ") pf ";
			}

			sql += sql2;

			//Add JOINS
			var initialWhere = " WHERE 11 = 11 " + filterSql + searchSql;
			sql += string.Join(" ", pSqls.archiveJoins.ToArray())
			       + string.Join(" ", pSqls.joins.ToArray()) + initialWhere;
			countSql += initialWhere;

			//Add WHERE
			var where = " AND " + EntityRepository.GetWhere(portfolioId);
			if (filterPhases != null && filterPhases.Count > 0) where += " AND " + getPhaseFilterWhere(filterPhases);
			sql += where;
			countSql += where;

			//Add Columns that are summed together
			var sumCols = "";
			var i = 1;
			var sumParams = new Dictionary<int, SqlParameter>();
			var countCols = "item_id";
			foreach (var sumCol in pSqls.sumcols) {
				sumCols += ", @sum" + i + " = SUM(COALESCE(" + sumCol + ", 0 ))";
				countCols += ", " + sumCol;
				sumParams.Add(i,
					ProcessBase.GetDBParam(SqlDbType.Float, "@sum" + i, 0.0, ParameterDirection.Output));
				ProcessBase.DBParameters.Add(sumParams[i]);
				i++;
			}


			if (countEquationSql != "") {
				countSql = " SELECT " + countCols + " FROM (SELECT item_id " + countEquationSql +
				           (countSql.TrimStart().StartsWith("FROM") ? "" : ",") + countSql + ") pf ";
			} else {
				if (countSql.TrimStart().StartsWith("FROM")) countSql = "item_id " + countSql;
				countSql = "SELECT " + countSql;
			}

			//Resolve order
			var aliasForOrder = "result";
			if (limit == 0) aliasForOrder = "pf";

			if (string.IsNullOrEmpty(ordercol)) {
				if (pp.OrderItemColumnId != null && pp.OrderItemColumnId > 0) {
					orderdir = "asc";
					if (pp.OrderDirection != 0) orderdir = "desc";
					ordercol = ItemColumns.GetItemColumn((int)pp.OrderItemColumnId).Name;
				} else {
					ordercol = "item_id";
					orderdir = "desc";
				}
			}

			if (!string.IsNullOrEmpty(ordercol) && ordercol != "item_id" && ordercol != "order_no")
				ordercol = ResolveOrderColumn(ordercol, aliasForOrder);

			//Use single sort if no multisort columns are provided
			var finalorder = new List<string>();
			foreach (var s in q.sort) {
				if (Conv.ToStr(s.column) != "" && Conv.ToStr(s.column) != "item_id" &&
				    Conv.ToStr(s.column) != "order_no")
					finalorder.Add(ResolveOrderColumn(s.column, aliasForOrder, true) + " " + s.order);
				if (Conv.ToStr(s.column) != "" &&
				    (Conv.ToStr(s.column) == "order_no" || Conv.ToStr(s.column) == "item_id")) {
					orderdir = s.order;
					ordercol = s.column;
				}
			}

			if (finalorder.Count == 0) {
				if (Conv.ToStr(ordercol) != "") {
					if (ordercol == "order_no") ordercol += "  ";
					finalorder.Add(ordercol + " " + orderdir);
				}
			}

			//Add Context Restrictions
			if (q.context != null) {
				var ics = new ItemCollectionService(Instance, Process, AuthenticatedSession);
				q.itemsRestriction = ics.AddContextualItemIds(q.context.process, q.context.itemId,
					q.context.itemColumnId, q.itemsRestriction);
			}

			if (q.itemsRestriction != null && q.itemsRestriction.Count > 0) {
				var idLimitStr = string.Join(",", q.itemsRestriction.ToArray());
				sql += " AND pf.item_id IN ( " + idLimitStr + " ) ";
				countSql += " AND pf.item_id IN ( " + idLimitStr + " ) ";
			}

			//Exclude ids
			if (q.excludeItemIds != null && q.excludeItemIds.Count > 0) {
				var idLimitStr = string.Join(",", q.excludeItemIds.ToArray());
				sql += " AND pf.item_id NOT IN ( " + idLimitStr + " ) ";
				countSql += " AND pf.item_id IN ( " + idLimitStr + " ) ";
			}

			//Get Data with generated SQL and ORDER
			pData = ProcessBase.db.GetDatatable(sql, Modi.JoinListWithComma(finalorder), "item_id", offset, limit,
				ProcessBase.DBParameters, sumCols, optionalCountWrapSql: countSql);

			//Add Final Sums
			i = 1;
			foreach (var sumCol in pSqls.sumcols) {
				if (pData.Rows.Count == 0) {
					sumParams[i].Value = 0;
				}

				result.Add(sumCol, sumParams[i].Value);
				i++;
			}
		}

		private void ProcessUnion(int portfolioId, ref PortfolioSqls pSqls, ref PortfolioOptions q, ref string sql,
			ref string filterSql,
			ref string searchSql, ref List<int> filterPhases, ref ProcessPortfolio pp, ref int limit, ref int offset,
			ref string ordercol,
			ref string orderdir, ref DataTable pData, ref Dictionary<string, object> result, ref string itemIdField) {
			itemIdField = "item_id";

			//Get Union Information
			var unions = new InstanceUnions(Instance, AuthenticatedSession);
			var unionProcessData = unions.GetPortfolioUnionProcessTable(portfolioId);
			var unionData = unions.GetPortfolioUnionDataTable(portfolioId);
			var unionResult = new Dictionary<string, Dictionary<string, ItemColumn>>();

			var outerWhereColIdReplace = new Dictionary<int, List<int>>();
			var outerWhereStrReplace = new List<KeyValuePair<string, string>>();
			var unionSql = new List<string>();

			//Loop through union data
			foreach (DataRow unionRow in unionProcessData.Rows) {
				var unionProcess = Conv.ToStr(unionRow["process"]);
				var unionPortfolioId = Conv.ToInt(unionRow["portfolio_id"]);
				var unionPortfolioSqls = _portfolioSqlQueryService.GetSql(unionPortfolioId, q.text);
				var unionPortfolio =
					new ProcessPortfolios(Instance, unionProcess, AuthenticatedSession).GetPortfolio(unionPortfolioId);

				var colSql = new List<string>();
				var union = new Dictionary<string, ItemColumn>();

				//Add Union Column Sql and collect information fot Where clause
				foreach (var u in unionData.Select("source_process = '" + unionProcess +
				                                   "' AND source_portfolio_id = " + unionPortfolioId)) {
					var sourceName = Conv.ToStr(u["source_name"]);
					var destinationName = Conv.ToStr(u["destination_name"]);
					var destinationId = Conv.ToInt(u["destination_id"]);
					var sourceId = Conv.ToInt(u["source_id"]);
					var sourcePortfolioId = Conv.ToInt(u["source_portfolio_id"]);

					var ics = new ItemColumns(Instance, unionProcess, AuthenticatedSession);
					var ic = ics.GetItemColumn(sourceId);

					//If process or list, item data process selections where clause needs to replace values
					//|| ic.SubDataType is 5 or 6)
					if (ic.IsProcess() || ic.IsList()) {
						if (outerWhereColIdReplace.ContainsKey(destinationId))
							outerWhereColIdReplace[destinationId].Add(sourceId);
						else
							outerWhereColIdReplace.Add(destinationId, new List<int>() { sourceId });
					}

					union.Add(destinationName + "_" + sourcePortfolioId, ic);

					var index = 0;
					foreach (var col in unionPortfolioSqls.cols) {
						if (col == sourceName) {
							var unioncol = unionPortfolioSqls.sqlcols[index];

							if (ic.IsSubquery() && ic.SubDataType == 6) {
								//Console.WriteLine(unioncol);
							} else {
								var asis = unioncol.ToLower().LastIndexOf(" as ", StringComparison.Ordinal);
								if (asis > 0) {
									if (unioncol.LastIndexOf("_str", StringComparison.Ordinal) > -1)
										destinationName += "_str";
									unioncol = unioncol[..asis];
								}
							}

							var colName = Modi.ReplaceLastOccurrence(unioncol, ic.Name, "");
							if (colName == "pf.") colName = unioncol;

							if (ic.IsSubquery()) {
								outerWhereStrReplace.Add(new KeyValuePair<string, string>(colName, destinationName));
								colSql.Add(Modi.ReplaceLastOccurrence(unioncol, ic.Name, " AS " + destinationName));
							} else {
								outerWhereStrReplace.Add(new KeyValuePair<string, string>(
									colName,
									destinationName));
								colSql.Add(unioncol + " AS " + destinationName);
							}

							break;
						}

						index++;
					}

					foreach (var col in unionPortfolioSqls.equationColumns.Where(col => sourceName == col.Name)) {
						if (Conv.ToInt(col.DataAdditional2) == 5 || Conv.ToInt(col.DataAdditional2) == 6) {
							colSql.Add(ItemColumns.GetEquationQuery(col) + " AS " + destinationName + "_str");
							outerWhereStrReplace.Add(new KeyValuePair<string, string>(ItemColumns.GetEquationQuery(ic),
								destinationName + "_str"));
						} else {
							colSql.Add(ItemColumns.GetEquationQuery(col) + " AS " + destinationName);
							outerWhereStrReplace.Add(new KeyValuePair<string, string>(ItemColumns.GetEquationQuery(ic),
								destinationName));
						}

						break;
					}
				}

				unionResult.Add(unionProcess + unionPortfolioId, union);

				//Add Where clause to inner query
				var where = unionPortfolioSqls.GetSearchSql();
				var conditions =
					new EntityRepository(Instance, unionProcess, AuthenticatedSession).GetWhere(unionPortfolioId);

				//Custom Joins
				unionSql.Add(
					"SELECT '" + Instance + "' AS instance, '" +
					unionProcess +
					"' AS process, pf.item_id AS item_id,pf.archive_start AS archive_start,pf.archive_userid,pf.order_no," +
					unionPortfolioId + " AS union_portfolio_id, " +
					Modi.JoinListWithComma(colSql) + " FROM _" + Instance + "_" + unionProcess + " pf " +
					GetCustomJoins(unionPortfolio) + " WHERE 9 = 9 " +
					where + " AND " + conditions);
			}

			result.Add("unionData", unionResult);


			var sql2 = "";
			//Combine inner queries
			foreach (var sqlstr in unionSql) {
				sql2 += (sql2 == "" ? "" : " UNION ALL ") + sql + sqlstr;
			}

			var outerWhere = GetFindItemsWhere(portfolioId, q);

			foreach (var (key, value) in outerWhereColIdReplace) {
				outerWhere = outerWhere.Replace("item_column_id = " + key,
					"item_column_id IN (" + Modi.JoinListWithComma(value) + ")");
			}

			foreach (var (key, value) in outerWhereStrReplace) {
				outerWhere = outerWhere.Replace(key, value);
				//  Console.WriteLine(key + " -> " +  value + " = " + outerWhere);
			}


			sql2 = "SELECT * FROM (" + sql2 + ") pf WHERE 999 = 999 " + outerWhere;
			sql += sql2;

			//Add Columns that are summed together
			var sumCols = "";
			var i = 1;
			var sumParams = new Dictionary<int, SqlParameter>();
			foreach (var sumCol in pSqls.sumcols) {
				sumCols += ", @sum" + i + " = SUM(COALESCE(" + sumCol + ", 0 ))";
				sumParams.Add(i,
					ProcessBase.GetDBParam(SqlDbType.Float, "@sum" + i, 0.0, ParameterDirection.Output));
				ProcessBase.DBParameters.Add(sumParams[i]);
				i++;
			}

			//Resolve order
			var aliasForOrder = "result";
			if (limit == 0) aliasForOrder = "pf";

			if (string.IsNullOrEmpty(ordercol)) {
				if (pp.OrderItemColumnId != null && pp.OrderItemColumnId > 0) {
					orderdir = "asc";
					if (pp.OrderDirection != 0) orderdir = "desc";
					ordercol = ItemColumns.GetItemColumn((int)pp.OrderItemColumnId).Name;
				} else {
					ordercol = "item_id";
					orderdir = "desc";
				}
			}

			if (!string.IsNullOrEmpty(ordercol) && ordercol != "item_id" && ordercol != "order_no")
				ordercol = ResolveOrderColumn(ordercol, aliasForOrder);

			//Use single sort if no multisort columns are provided
			var finalorder = new List<string>();
			foreach (var s in q.sort) {
				if (Conv.ToStr(s.column) != "" && Conv.ToStr(s.column) != "item_id" &&
				    Conv.ToStr(s.column) != "order_no")
					finalorder.Add(ResolveOrderColumn(s.column, aliasForOrder, true) + " " + s.order);
				if (Conv.ToStr(s.column) == "" ||
				    (Conv.ToStr(s.column) != "order_no" && Conv.ToStr(s.column) != "item_id")) continue;
				orderdir = s.order;
				ordercol = s.column;
			}

			if (finalorder.Count == 0) {
				if (Conv.ToStr(ordercol) != "") {
					if (ordercol == "order_no") ordercol += "  ";
					finalorder.Add(ordercol + " " + orderdir);
				}
			}

			//Add Context Restrictions
			if (q.context != null) {
				var ics = new ItemCollectionService(Instance, Process, AuthenticatedSession);
				q.itemsRestriction = ics.AddContextualItemIds(q.context.process, q.context.itemId,
					q.context.itemColumnId, q.itemsRestriction);
			}

			if (q.itemsRestriction is { Count: > 0 })
				sql += " AND pf.item_id IN ( " + string.Join(",", q.itemsRestriction.ToArray()) + " ) ";


			//Exclude ids
			if (q.excludeItemIds is { Count: > 0 })
				sql += " AND pf.item_id NOT IN ( " + string.Join(",", q.excludeItemIds.ToArray()) + " ) ";


			//Get Data with generated SQL and ORDER
			pData = ProcessBase.db.GetDatatable(sql, Modi.JoinListWithComma(finalorder), "item_id", offset, limit,
				ProcessBase.DBParameters, sumCols);

			//Add Final Sums
			i = 1;
			foreach (var sumCol in pSqls.sumcols) {
				if (pData.Rows.Count == 0) {
					sumParams[i].Value = 0;
				}

				result.Add(sumCol, sumParams[i].Value);
				i++;
			}
		}

		private string ResolveOrderColumn(string ordercol, string aliasForOrder, bool useQuery = false) {
			if (Conv.ToStr(ordercol) == "") return "";
			var ordCol = ItemColumns.GetItemColumnByName(ordercol);
			if (ordCol.IsProcess()) {
				ProcessBase.SetDBParam(SqlDbType.Int, "@orderColId", ordCol.ItemColumnId);
				var ppc = new ProcessPortfolioColumns(Instance, ordCol.DataAdditional, AuthenticatedSession);
				var pCols = ppc.GetPrimaryColumns();
				var concatStr = "";
				foreach (var c in pCols) {
					concatStr += " + ISNULL(CAST(" + c.ItemColumn.Name + " AS NVARCHAR),'')";
				}

				var whereCol = "item_id";
				var colId = ordCol.ItemColumnId;
				var innerCol = "selected_item_id";

				//Column is flipped
				if (Conv.ToInt(ordCol.RelativeColumnId) > 0) {
					whereCol = "selected_item_id";
					innerCol = "item_id";
					colId = Conv.ToInt(ordCol.RelativeColumnId);
				}

				ordercol = " STUFF((SELECT ', ' " + concatStr +
				           " FROM item_data_process_selections im INNER JOIN _" +
				           Instance + "_" + ordCol.DataAdditional + " spf" + colId +
				           " ON im." + innerCol + " = spf" + colId +
				           ".item_id WHERE im." + whereCol + " = " + aliasForOrder + ".item_id AND item_column_id = " +
				           colId +
				           " FOR XML PATH('')),1,1,'') COLLATE Finnish_Swedish_CI_AS ";
			} else if (ordCol.IsList()) {
				var processes = new Processes(Instance, AuthenticatedSession);
				var listProcess = processes.GetProcess(ordCol.DataAdditional);
				var orderListColumn = "list_item";
				//var orderCollate = "Finnish_Swedish_CI_AS";
				if (Conv.ToStr(listProcess.ListOrder) != "") orderListColumn = listProcess.ListOrder;

				//List Item Is Used -- Check Language Support
				if (orderListColumn == "list_item") {
					var x = "list_item_" + AuthenticatedSession.locale_id.Replace("-", "_");
					var pe = "IF COL_LENGTH('_" + Instance + "_" + ordCol.DataAdditional + "', '" + x +
					         "') IS NOT NULL " +
					         "SELECT 1 as result " +
					         "ELSE " +
					         "SELECT 0 result";

					var cb = new ConnectionBase(Instance, AuthenticatedSession);
					var tt = Conv.ToInt(cb.db.GetDataRow(pe)[0]);
					if (tt == 1) orderListColumn = x;
				} else if (orderListColumn == "order_no") {
					//orderCollate = "Latin1_General_bin";
				}

				ProcessBase.SetDBParam(SqlDbType.Int, "@orderColId", ordCol.ItemColumnId);
				ordercol = " STUFF((SELECT ', ' + CAST(" + orderListColumn +
				           " AS NVARCHAR) FROM item_data_process_selections im INNER JOIN _" +
				           Instance + "_" +
				           ordCol.DataAdditional + " spf" + ordCol.ItemColumnId +
				           " ON im.selected_item_id = spf" +
				           ordCol.ItemColumnId + ".item_id WHERE im.item_id = " + aliasForOrder +
				           ".item_id AND item_column_id = " +
				           ordCol.ItemColumnId + " FOR XML PATH('')),1,1,'') "; //"COLLATE " + orderCollate;
			} else if (ordCol.IsSubquery() && (ordCol.IsOfSubType(ItemColumn.ColumnType.Process) ||
			                                   ordCol.IsOfSubType(ItemColumn.ColumnType.List))) {
				var subQuery = RegisteredSubQueries.GetType(ordCol.DataAdditional, ordCol.ItemColumnId);
				var osql = subQuery.GetOrderSql();
				if (osql == "[COLUMNNAME]" || osql == "")
					ordercol += "_str ";
				else
					ordercol = osql;
			} else if (ordCol.IsEquation() &&
			           (Conv.ToInt(ordCol.DataAdditional2) == 5 || Conv.ToInt(ordCol.DataAdditional2) == 6)) {
				ordercol += "_str";
			} else if (ordCol.IsText() || ordCol.IsNvarChar()) {
				ordercol = ordCol.Name + " COLLATE Finnish_Swedish_CI_AS ";
			} else if (ordCol.IsSubquery() && ordCol.IsOfSubType(ItemColumn.ColumnType.Nvarchar)) {
				var subQuery = RegisteredSubQueries.GetType(ordCol.DataAdditional, ordCol.ItemColumnId);
				var osql = subQuery.GetOrderSql();
				if (osql == "[COLUMNNAME]" && !useQuery) {
					var cosql = ordCol.GetColumnQuery(Instance);
					if (cosql.Contains("processTable"))
						cosql = cosql.Replace("pf.", "processTable.");

					ordercol = "ISNULL(" + cosql + ",'') COLLATE Finnish_Swedish_CI_AS ";
				} else {
					ordercol = ordCol.Name;
				}
			} else {
				ordercol = ordCol.Name;
			}

			return ordercol;
		}

		private Dictionary<string, Dictionary<string, double>> CalcRecursiveCache;

		private void CalcRecursivePrepare() {
			CalcRecursiveCache = new Dictionary<string, Dictionary<string, double>>();
		}

		private void CalcRecursiveAdd(string item_id, string col, Dictionary<string, double> sum) {
			Dictionary<string, double> o;
			if (!CalcRecursiveCache.ContainsKey(item_id)) {
				o = new Dictionary<string, double>();

				var s = 0.0;
				if (sum.ContainsKey(col)) s = sum[col];
				o.Add(col, s);

				CalcRecursiveCache.Add(item_id, o);
			} else {
				var s = 0.0;
				if (sum.ContainsKey(col)) s = sum[col];
				CalcRecursiveCache[item_id].Add(col, s);
			}
		}

		private void CalcRecursiveSum(DataTable data, PortfolioSqls pSqls, DataRow parentRow,
			Dictionary<string, double> sum = null, int level = 0) {
			var cols = pSqls.sumcols;
			var sums = pSqls.UseInSelectSumcols;

			if (sum == null) sum = new Dictionary<string, double>();

			if (level == 0) {
				foreach (var col in cols) {
					if (!sum.ContainsKey(col)) sum.Add(col, 0);
					sum[col] += Conv.ToDouble(parentRow[col]);
				}
			}

			var item_id = Conv.ToStr(parentRow["item_id"]);
			foreach (var r in data.Select("parent_item_id = '" + Conv.ToSql(item_id) + "'")) {
				foreach (var col in cols) {
					if (sums.Contains(col)) {
						if (!sum.ContainsKey(col)) sum.Add(col, 0);
						sum[col] += Conv.ToDouble(r[col]);
					}
				}

				CalcRecursiveSum(data, pSqls, r, sum, level + 1);
			}

			if (level == 0) {
				foreach (var col in cols) {
					CalcRecursiveAdd(item_id, col, sum);
				}
			}
		}

		public bool hasProgressProgress(int portfolioId) {
			if (_privateCache.ContainsKey("hasProgressProgress")) return _privateCache["hasProgressProgress"];

			var sql = "SELECT count(cf.condition_id) FROM condition_features cf " +
			          "INNER JOIN condition_states cs ON cf.condition_id = cs.condition_id " +
			          "WHERE cf.condition_id IN (SELECT condition_id FROM conditions " +
			          "WHERE disabled = 0 AND instance = @instance AND process = @process AND feature = 'progressProgress')";

			if (Conv.ToInt(ProcessBase.db.ExecuteScalar(sql, ProcessBase.DBParameters)) > 0) {
				ProcessBase.SetDBParam(SqlDbType.Int, "PortfolioId", portfolioId);
				sql =
					"SELECT COUNT(*) FROM process_portfolio_columns p " +
					"LEFT JOIN item_columns c ON p.item_column_id = c.item_column_id " +
					"WHERE c.data_type = 60 AND p.process_portfolio_id = @PortfolioId";
				if (Conv.ToInt(ProcessBase.db.ExecuteScalar(sql, ProcessBase.DBParameters)) > 0) {
					_privateCache["hasProgressProgress"] = true;
					return true;
				}
			}

			_privateCache["hasProgressProgress"] = false;
			return false;
		}


		public Dictionary<int, object>
			HandlePortfolioProcessData(int portfolioId, PortfolioSqls pSqls, DataTable pData) {
			return HandlePortfolioProcessData(
				new ProcessPortfolios(Instance, Process, AuthenticatedSession).GetPortfolio(portfolioId), pSqls, pData);
		}

		private string GetUnionColumnName(string name, string process, DataTable unionData) {
			if (unionData == null) return name;
			var r = unionData.Select("source_name = '" + name + "' AND source_process = '" + process + "'");
			return r.Length > 0 ? Conv.ToStr(r[0]["destination_name"]) : name;
		}

		public Dictionary<int, object>
			HandlePortfolioProcessData(ProcessPortfolio portfolio, PortfolioSqls pSqls, DataTable pData,
				bool onlySelectorColumns = true) {
			var processCols = new Dictionary<string, List<int>>();
			var colProcesses = new Dictionary<int, string>();
			var allColumns = pSqls.processColumns;

			DataTable unionData = null;
			if (portfolio.IsUnionPortfolio) {
				var unions = new InstanceUnions(Instance, AuthenticatedSession);
				unionData = unions.GetPortfolioUnionDataTable(portfolio.ProcessPortfolioId);

				allColumns.Clear();
				foreach (DataRow unionRow in unionData.Rows) {
					var itemColumnId = Conv.ToInt(unionRow["source_id"]);
					var itemProcess = Conv.ToStr(unionRow["source_process"]);
					var ics = new ItemColumns(Instance, itemProcess, AuthenticatedSession);

					if (itemColumnId > 0) {
						var ic = ics.GetItemColumn(itemColumnId);

						if (!ic.IsEquation() && !ic.IsSubquery())
							allColumns.Add(ic);

						if (ic.IsSubquery() && ic.SubDataType is 5 or 6)
							allColumns.Add(ic);

						if (ic.IsEquation() && ic.SubDataType is 5 or 6)
							allColumns.Add(ic);
					}
				}
			}

			foreach (var col in allColumns) {
				var colProcess = col.DataAdditional;
				if (col.IsSubquery() && (col.IsOfSubType(ItemColumn.ColumnType.List) ||
				                         col.IsOfSubType(ItemColumn.ColumnType.Process))) {
					var query = RegisteredSubQueries.GetType(col.DataAdditional, col.ItemColumnId);
					colProcess = query.GetSubProcess();
					colProcesses.Add(col.ItemColumnId, colProcess);
				}

				if (col.IsEquation() &&
				    (col.IsOfSubType(ItemColumn.ColumnType.List) || col.IsOfSubType(ItemColumn.ColumnType.Process))) {
					colProcess = col.InputName;
				}

				if (colProcess != null && !processCols.ContainsKey(colProcess))
					processCols.Add(colProcess, new List<int>());
			}

			foreach (DataRow d in pData.Rows) {
				foreach (var col in allColumns) {
					var colProcess = col.DataAdditional;
					if (col.IsSubquery() && col.IsOfSubType(ItemColumn.ColumnType.List))
						colProcess = colProcesses[col.ItemColumnId];

					if (col.IsEquation() &&
					    (col.IsOfType(ItemColumn.ColumnType.List) || col.IsOfSubType(ItemColumn.ColumnType.Process)))
						colProcess = col.InputName;

					if (!processCols.ContainsKey(Conv.ToStr(colProcess))) continue;
					var colName = GetUnionColumnName(col.Name, col.Process, unionData);
					if (DBNull.Value.Equals(d[colName]) || colName.Equals("linked_tasks") || col.IsTag()) continue;
					foreach (var id in (int[])d[colName]) {
						if (!processCols[colProcess].Contains(id)) processCols[colProcess].Add(id);
					}
				}
			}

			var processData = new Dictionary<int, object>();
			foreach (var process in processCols.Keys) {
				var es = new DatabaseItemService(Instance, process, AuthenticatedSession);
				foreach (var id in processCols[process]) {
					processData.Add(id,
						es.GetItem(id, onlySelectorColumns: onlySelectorColumns,
							checkRights: !ignoreRights(portfolio.ProcessPortfolioId)));
				}
			}

			return processData;
		}

		private void CalcRecursiveComplete(DataTable data, List<string> cols) {
			foreach (DataRow r in data.Rows) {
				var item_id = Conv.ToStr(r["item_id"]);
				if (CalcRecursiveCache.ContainsKey(item_id)) {
					var o = CalcRecursiveCache[item_id];
					foreach (var col in cols) {
						if (o.ContainsKey(col)) r[col] = o[col];
					}
				}
			}
		}
	}
}