﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;


//Rest Base Class
namespace Keto5.x.platform.server.@base {
	public class PageBase : RestBase {
		private readonly bool _skipRightsChecking;
		private List<string> states;
		private PagesDb access;
		private string _page;

		public PageBase(string page, bool skipRightsChecking = false) {
			_skipRightsChecking = skipRightsChecking;
			_page = page;
		}

		public override void OnActionExecuting(ActionExecutingContext context) {
			if (access == null) access = new PagesDb(instance, currentSession);
			if (states == null) states = access.GetStates(_page);

			if (Response.StatusCode != 401) {
				//Rest code is about to be executed. Validate session or cookie
				if (Auth.IsCookieValid(currentSession, HttpContext) && (_skipRightsChecking || states.Count > 0)) {
					//Execute Requested Action
					base.OnActionExecuting(context);
				} else {
					//Response is 401: Unauthorized
					Response.StatusCode = 403;

					context.Result = new EmptyResult();
					base.OnActionExecuting(context);
					//Diag.Log(instance, "Session doesn't exist nor can the user be authenticated at " + Request.Path.Value + ". Response is 401: Unauthorized 2/2.",0, Diag.LogSeverities.warning);
				}
			}
		}

		public void CheckRight(string state) {
			access ??= new PagesDb(instance, currentSession);
			access.CheckRight(state, _page, states);
		}
	}

	[Route("v1/navigation/[controller]")]
	public class Pages : RestBase {
		[HttpGet("{state}")]
		public List<string> Get(string state) {
			return GetRights(state);
		}

		[HttpGet("{state}/{instanceMenuId}")]
		public List<string> Get(string state, int instanceMenuId) {
			return GetRights(state, instanceMenuId);
		}

		private List<string> GetRights(string state, int? instanceMenuId = null) {
			var retval = new List<string>();
			var db = new Connections(currentSession);
			var cdb = new ConnectionBase(instance, currentSession);
			cdb.SetDBParam(SqlDbType.NVarChar, "@state", state);
			var ids = (List<int>)currentSession.GetMember("groups");

			var idStr = string.Join(",", ids.ToArray());
			if (idStr.Length == 0) {
				idStr = "0";
			}

			var selectSql =
				"SELECT DISTINCT state " +
				"FROM instance_menu_states " +
				"WHERE item_id IN (" + idStr + ") AND " +
				"instance_menu_id ";

			if (instanceMenuId == null) {
				selectSql += "IN (SELECT instance_menu_id FROM instance_menu WHERE default_state = @state)";
			} else {
				cdb.SetDBParam(SqlDbType.Int, "@instanceMenuId", instanceMenuId);
				selectSql += "= @instanceMenuId";
			}

			foreach (DataRow dr in db.GetDatatable(selectSql, cdb.DBParameters).Rows) {
				retval.Add(Conv.ToStr(dr["state"]));
			}

			return retval;
		}
	}

	public class PagesDb : ConnectionBase {
		private readonly AuthenticatedSession _authenticatedSession;

		public PagesDb(string instance, AuthenticatedSession session) : base(instance, session) {
			this.instance = instance;
			_authenticatedSession = session;
		}

		private DataTable _instanceMenuRightCache;

		private DataTable InstanceMenuRightCache {
			get {
				_instanceMenuRightCache = (DataTable)Cache.Get("", "pagebase_menu_cache");
				if (_instanceMenuRightCache != null) return _instanceMenuRightCache;

				_instanceMenuRightCache = db.GetDatatable(
					"SELECT DISTINCT state, im.params, item_id, default_state FROM instance_menu_states imc INNER JOIN instance_menu im ON im.instance_menu_id = imc.instance_menu_id");
				Cache.Set("", "pagebase_menu_cache", _instanceMenuRightCache);
				return _instanceMenuRightCache;
			}
		}

		public void CheckRight(string state, string page, List<string> states = null) {
			if (states != null) this._states = states;
			if (!HasRight(state, page))
				throw new CustomPermissionException("Page '" + page + "' has no rights to state '" + state + "'");
		}

		private List<string> _states;

		public bool HasRight(string state, string page) {
			_states ??= GetStates(page);
			return _states.Contains(state);
		}

		public List<string> GetStates(string page, Dictionary<string, object> pageParams = null) {
			var retval = new List<string>();
			if (pageParams == null) pageParams = new Dictionary<string, object>();

			var ids = (List<int>)_authenticatedSession.GetMember("groups");

			var idStr = string.Join(",", ids.ToArray());
			if (idStr.Length == 0) {
				idStr = "0";
			}

			var f = new common.Util.Filter();
			f.Add("default_state", page);
			f.Add("item_id IN (" + idStr + ")");

			var rows = InstanceMenuRightCache.Select(f.ToString());
			foreach (var dr in rows) {
				try {
					var p = JsonConvert.DeserializeObject<Dictionary<string, object>>(Conv.ToStr(dr["params"]));
					var matches = true;
					
					foreach (var key in pageParams.Keys) {
						if (!p.ContainsKey(key) || !p[key].Equals(pageParams[key])) matches = false;
					}

					if (matches && !retval.Contains(Conv.ToStr(dr["state"]))) retval.Add(Conv.ToStr(dr["state"]));
				} catch (Exception e) {
					Diag.SupressException(e);
				}
			}
			return retval;
		}
	}
}