using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Keto5.x.customer.server;
using Keto5.x.platform.db;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.features;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.dashboards;
using Keto5.x.platform.server.processes.dashboards.chartConfigs;
using Keto5.x.platform.server.processes.httpResponseParsers;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.processCustomActions;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.processes.backgroundProcesses;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server {
	public static class InitializeServer {
		public static readonly Dictionary<string, CsMigration> RegisteredCsMigrations =
			new Dictionary<string, CsMigration>();

		public static class Register {
			public static bool DashboardCharts() {
				RegisteredCharts.AddType(new PieChartConfig());
				RegisteredCharts.AddType(new StackedLineChartConfig());
				RegisteredCharts.AddType(new BarChartConfig());
				RegisteredCharts.AddType(new BubbleChartConfig());
				RegisteredCharts.AddType(new PhaseProgressChartConfig());
				RegisteredCharts.AddType(new MixedChartConfig());
				RegisteredCharts.AddType(new MultiListChartConfig());
				RegisteredCharts.AddType(new PivotTableConfig());
				RegisteredCharts.AddType(new ResourceLoadChartConfig());
				RegisteredCharts.AddType(new CircularVisualizationConfig());
				return true;
			}

			public static bool BackgroundProcesses() {
				RegisteredBackgroundProcesses.AddType(new AzureAdBackgroundProcess());
				RegisteredBackgroundProcesses.AddType(new RemoveDelegationsBackgroundProcess());
				RegisteredBackgroundProcesses.AddType(new ExecuteActionsPortfolio());
				RegisteredBackgroundProcesses.AddType(new GetDataAsExcel());
				RegisteredBackgroundProcesses.AddType(new DoActionForSelectedItemIds());
				RegisteredBackgroundProcesses.AddType(new PdfDownloadBackgroundProcess());
				RegisteredBackgroundProcesses.AddType(new AttachmentsReduceImageDimensionsBackgroundProcess());
				RegisteredBackgroundProcesses.AddType(new SetDataWithExcel());
				RegisteredBackgroundProcesses.AddType(new WorktimeExcelBg());
				RegisteredBackgroundProcesses.AddType(new DeactivateExpiredUsersBackgroundProcess());
				RegisteredBackgroundProcesses.AddType(new SendDigestEmails());
				RegisteredBackgroundProcesses.AddType(new SubqueryCacheBackgroundProcess());
				RegisteredBackgroundProcesses.AddType(new GetExcelWithClientData());
				return true;
			}

			public static bool HttpResponseParsers() {
				RegisteredHttpResponseParsers.AddType(new HttpResponseParserBitBucket());
				RegisteredHttpResponseParsers.AddType(new HttpResponseParserAzureDevops());
				return true;
			}

			public static bool CustomActions() {
				RegisteredCustomActions.AddType(new CustomActionExecutableLauncher());
				RegisteredCustomActions.AddType(new CustomActionAllocationActions());
				RegisteredCustomActions.AddType(new CustomActionKanbanActions());
				RegisteredCustomActions.AddType(new CustomActionWorkTimeActions());
				RegisteredCustomActions.AddType(new CustomActionDeleteActions());
				RegisteredCustomActions.AddType(new CustomActionInviteUsers());
				return true;
			}

			public static bool Features() {
				//Register features
				RegisteredFeatures.AddType(new Keto5.x.platform.server.processes.specialProcesses.TasksRest());

				//Enable features
				foreach (var f in RegisteredFeatures.GetFeatures()) {
					f.EnableFeature();
					if (f.Enabled == false)
						Diag.LogToConsole("Feature '" + f.Name + "' could not be enabled.", Diag.LogSeverities.Warning);
				}

				return true;
			}

			public static bool Subqueries() {
				RegisteredSubQueries.AddType(new SubqueryTaskCostConfig());
				RegisteredSubQueries.AddType(new SubqueryTaskEffortConfig());
				RegisteredSubQueries.AddType(new SubqueryTaskActualEffortConfig());
				RegisteredSubQueries.AddType(new SubqueryTaskAllocationStatusConfig());
				RegisteredSubQueries.AddType(new SubqueryTaskOwnerPrimaryConfig());
				RegisteredSubQueries.AddType(new SubqueryTasksRecursive());
				RegisteredSubQueries.AddType(new SubqueryTaskKanbanStatusConfig());

				RegisteredSubQueries.AddType(new SubqueryListRisksConfig());
				RegisteredSubQueries.AddType(new SubqueryListBenefitsConfig());

				RegisteredSubQueries.AddType(new SubqueryAggregateFunction(0));
				RegisteredSubQueries.AddType(new SubqueryAggregateFunction(1));
				RegisteredSubQueries.AddType(new SubqueryAggregateFunction(2));
				RegisteredSubQueries.AddType(new SubqueryAggregateFunction(3));
				RegisteredSubQueries.AddType(new SubqueryAggregateFunction(4));
				RegisteredSubQueries.AddType(new SubqueryAggregateFunction(5));


				RegisteredSubQueries.AddType(new SubQueryYearlyNPV());
				RegisteredSubQueries.AddType(new SubQueryYearlyPayback());
				RegisteredSubQueries.AddType(new SubqueryProcessActualEffortConfig());
				RegisteredSubQueries.AddType(new SubqueryProcessAllocationConfig());
				RegisteredSubQueries.AddType(new SubqueryAllocationDurationHoursConfig());
				RegisteredSubQueries.AddType(new SubqueryAllocationDurationDatesConfig());
				

				RegisteredSubQueries.AddType(new LastModifiedByConfig());
				RegisteredSubQueries.AddType(new LastModifiedBy_MoreExtensive());
				RegisteredSubQueries.AddType(new LastModifiedOnConfig());
				RegisteredSubQueries.AddType(new LastModifiedOn_MoreExtensive());
				RegisteredSubQueries.AddType(new CreatedByConfig());
				RegisteredSubQueries.AddType(new CreatedOnConfig());
				RegisteredSubQueries.AddType(new ProcessItemIdConfig());
				RegisteredSubQueries.AddType(new ProcessItemIdProcessConfig());
				RegisteredSubQueries.AddType(new ProcessItemIdListConfig());
				RegisteredSubQueries.AddType(new SQ_ProcessesListUsersOfGroup());
				RegisteredSubQueries.AddType(new IntegerAsProcessConfig());
				

				RegisteredSubQueries.AddType(new SubqueryUserActiveConfig());
				RegisteredSubQueries.AddType(new SubQueryUserCalendarField());
				RegisteredSubQueries.AddType(new SubquerySprintResponsiblesConfig());
				RegisteredSubQueries.AddType(new SubqueryTasksSprintStartConfig());
				RegisteredSubQueries.AddType(new SubqueryTasksSprintEndConfig());
				RegisteredSubQueries.AddType(new AttachmentCountConfig());
				RegisteredSubQueries.AddType(new SubqueryCurrentUserConfig());

				RegisteredSubQueries.AddType(new CurrencyExchange());

				return true;
			}

			public static bool CsMigrations() {
				RegisteredCsMigrations.Add("V5.1.258__portfolio_column_flip.cs",
					new ServerMigrationPortfolioColumnFlip());
				RegisteredCsMigrations.Add("V5.1.329__notification_remake.cs",
					new ServerMigrationNotificationRemake());
				RegisteredCsMigrations.Add("V5.1.378__GetPaybackYears.cs",
					new GetPaybackYears());
				RegisteredCsMigrations.Add("V5.1.411__instance_helpers_rich_text.cs",
					new ServerMigrationInstanceHelpers());
				return true;
			}
		}

		public static bool CacheItemProcesses() {
			var db = new Connections();
			var dt = db.GetDatatable("SELECT item_id, process FROM items");
			foreach (DataRow r in dt.Rows) {
				Cache.SetItemProcess((int) r["item_id"], (string) r["process"]);
			}

			return true;
		}

		public static bool CacheSubQueries() {
			var db = new Connections();

			var sqs = db
				.GetDatatable(
					"SELECT ic.*, i.default_locale_id  FROM item_columns ic INNER JOIN instances i ON ic.instance = i.instance WHERE data_type = 16 AND data_additional IS NOT null");

			foreach (DataRow r in
				sqs.Rows) {
				RegisteredSubQueries.GetType(Conv.ToStr(r["data_additional"]), Conv.ToInt(r["item_column_id"]));


				var members = new Dictionary<string, object> {
					{
						"item_id",
						Conv.ToInt(db.ExecuteScalar("SELECT MIN(item_id) FROM _" + Conv.ToStr(r["instance"]) + "_user"))
					},
					{"locale_id", Conv.ToStr(r["default_locale_id"])}
				};

				var session = new AuthenticatedSession(Conv.ToStr(r["instance"]), 1, members);
				var ppp = new ProcessBase(session, Conv.ToStr(r["process"]));

				var ics = new ItemColumns(ppp, Conv.ToStr(r["process"]));
				var subQuery =
					RegisteredSubQueries.GetType(Conv.ToStr(r["data_additional"]), Conv.ToInt(r["item_column_id"]));


				if (subQuery != null) {
					try {
						subQuery.SetInstance(Conv.ToStr(r["instance"]));
						subQuery.SetProcess(Conv.ToStr(r["process"]));
						subQuery.SetSession(session);
						subQuery.SetFieldConfigs(Conv.ToStr(r["data_additional2"]));
						var ic = new ItemColumn(r, session);

						var e = new ItemColumnEquations(Conv.ToStr(r["instance"]), Conv.ToStr(r["process"]), session);
						
						if (!e.TestSubqueryAndEnsureCache(Conv.ToInt(r["item_column_id"]), ic.Name, subQuery,
							subQuery.GetFieldConfigs()))
							Console.WriteLine("Subquery " + Conv.ToStr(r["name"]) + " (" +
							                  Conv.ToStr(r["item_column_id"]) + ") in process " +
							                  Conv.ToStr(r["process"]) + " was marked suspicious");
					} catch (Exception ex) {
						Console.WriteLine("Subquery error: " + ex.Message + ex.StackTrace);
					}
				}
			}

			return true;
		}

		public static bool InitializeCustomerConfiguration() {
			var customerConfig = new CustomerConfig();
			try {
				customerConfig.InitCustomerConfigurations();
			} catch (Exception) {
				Diag.LogToConsole("Customer configuration failed (in framework)", Diag.LogSeverities.Error);
				return false;
			}

			return true;
		}


		public static bool Migrate(string[] args) {
			//Run Migration Scripts
			try {
				if (Program.RunPreMigrationsScripts() == false) return false;
				// Run database migrations
				// This runs on every startup, but depending on parameters it may not execute anything (only check that version numbering is correct)
				var databaseMigration = new DatabaseMigration(args);
				var migrationResult = databaseMigration.RunDatabaseMigrations();
				Diag.LogToConsole("Previously migrated scripts before termination where no action was taken: " +
				                  databaseMigration.PreviousScripts + " pcs.");
				if (migrationResult == false) return false;
				Diag.LogToConsole("Migration Completed", Diag.LogSeverities.SuccessMessage);
				if (Program.RunPostMigrationsScripts() == false) return false;
			} catch (Exception) {
				Diag.LogToConsole("Database Migrating Scripts Caused Error");
				return false;
			}

			return true;
		}

		public static bool IndexProcesses() {
			var db = new Connections();
			var sql =
				"SELECT * FROM instances i WHERE (SELECT COUNT(*) FROM item_columns WHERE use_lucene = 1 AND instance = i.instance) > 0";
			foreach (DataRow r in db.GetDatatable(sql).Rows) {
				var members = new Dictionary<string, object> {
					{
						"item_id",
						Conv.ToInt(db.ExecuteScalar("SELECT MIN(item_id) FROM _" + Conv.ToStr(r["instance"]) + "_user"))
					},
					{"locale_id", Conv.ToStr(r["default_locale_id"])}
				};
				var session = new AuthenticatedSession(Conv.ToStr(r["instance"]), 1, members);
				var l = new Keto5.x.platform.server.processes.Lucene(Conv.ToStr(r["instance"]), session);
				Task.Factory.StartNew(() => l.BuildIndex());
			}

			return true;
		}
	}
}