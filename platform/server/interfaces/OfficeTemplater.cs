﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.interfaces {
	public class OfficeTemplater {
		public static byte[] CreateReport(string dataName, object data, int attachmentId, string urlAction, AuthenticatedSession session) {
			var boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");

			var url = Conv.ToStr(Config.GetValue("Report", "TemplaterUrl"));

			var request = (HttpWebRequest)WebRequest.Create(url + urlAction);
			request.ContentType = "multipart/form-data; boundary=" + boundary;
			request.Method = "POST";
			request.KeepAlive = true;

			Stream memStream = new MemoryStream();

			var boundarybytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
			var endBoundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--");

			var formdataTemplate = "\r\n--" + boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

			var formitem = string.Format(formdataTemplate, dataName,
				JsonConvert.SerializeObject(data));
			var formitembytes = Encoding.UTF8.GetBytes(formitem);

			memStream.Write(formitembytes, 0, formitembytes.Length);

			var x = new AttachmentsDownload(session);
			var x2 = x.DownloadAttachment(attachmentId, 0);

			var headerTemplate =
				"Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
				"Content-Type: application/octet-stream\r\n\r\n";

			memStream.Write(boundarybytes, 0, boundarybytes.Length);
			var header = string.Format(headerTemplate, "uplTheFile", x2.Filename);
			var headerbytes = Encoding.UTF8.GetBytes(header);

			memStream.Write(headerbytes, 0, headerbytes.Length);
			using (var iStream = new MemoryStream(x2.Filedata)) {
				var buffer = new byte[1024];
				var bytesRead = 0;
				while ((bytesRead = iStream.Read(buffer, 0, buffer.Length)) != 0) {
					memStream.Write(buffer, 0, bytesRead);
				}
			}
			memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
			request.ContentLength = memStream.Length;

			using (var requestStream = request.GetRequestStream()) {
				memStream.Position = 0;
				var tempBuffer = new byte[memStream.Length];
				memStream.Read(tempBuffer, 0, tempBuffer.Length);
				memStream.Close();
				requestStream.Write(tempBuffer, 0, tempBuffer.Length);
			}

			using (var response = request.GetResponse()) {
				var stream2 = response.GetResponseStream();

				var bytesBuffer = 1024;
				var buffer = new byte[bytesBuffer];
				using (var ms = new MemoryStream()) {
					int readBytes;
					while ((readBytes = stream2.Read(buffer, 0, buffer.Length)) > 0) {
						ms.Write(buffer, 0, readBytes);
					}

					return ms.ToArray();
				}
			}
		}

		public static List<Dictionary<string, object>> ProcessList(List<Dictionary<string, object>> data,
			ItemBase dataIb, List<ItemColumn> cols) {
			var retval = new List<Dictionary<string, object>>();
			foreach (var x in data) {
				var dict = new Dictionary<string, object>();
				foreach (var col in cols) {
					if (x.ContainsKey(col.Name)) {
						dict.Add(col.Name, dataIb.ReplaceIds(col, x[col.Name]));
					}
				}

				foreach (var key in x.Keys) {
					if (!dict.ContainsKey(key)) {
						dict.Add(key, x[key]);
					}
				}

				retval.Add(dict);
			}

			return retval;
		}

		public static Dictionary<string, object> ProcessDict(Dictionary<string, object> data, ItemBase dataIb,
			List<ItemColumn> cols) {
			var dict = new Dictionary<string, object>();
			foreach (var col in cols) {
				if (data.ContainsKey(col.Name)) {
					dict.Add(col.Name, dataIb.ReplaceIds(col, data[col.Name]));
				}
			}

			foreach (var key in data.Keys) {
				if (!dict.ContainsKey(key)) {
					dict.Add(key, data[key]);
				}
			}

			return dict;
		}

		public static object SetColor(object value, string color) {
			var settings = new XmlReaderSettings();
			settings.ConformanceLevel = ConformanceLevel.Fragment;

			var str = "<w:r xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\">";

			str += "<w:rPr><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"" + GetColorHex(color) + "\" /></w:rPr> ";

			str += " <w:t> " + ParseChars((string)value) + " </w:t> </w:r>";

			var xe = XElement.Parse(str);
			return xe;
		}

		public static string GetWorstColor(Dictionary<string, object> data) {
			var colorVals = new Dictionary<string, int>();
			colorVals.Add("", 0);
			colorVals.Add("green", 0);
			colorVals.Add("yellow", 1);
			colorVals.Add("amber", 2);
			colorVals.Add("red", 3);

			var colors = new Dictionary<int, string>();
			colors.Add(0, "green");
			colors.Add(1, "yellow");
			colors.Add(2, "amber");
			colors.Add(3, "red");

			var colorVal = 0;
			foreach (var col in new[] { "time", "budget", "resource", "benefits", "riskissues" }) {
				if (colorVals.ContainsKey(Conv.ToStr(data[col])) && colorVals[Conv.ToStr(data[col])] > colorVal) {
					colorVal = colorVals[Conv.ToStr(data[col])];
				}
			}

			return colors[colorVal];
		}

		public static object SetColor(string value, string color, string codeStr) {
			var str = "<w:r xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"> ";

			if (Config.IconCodes.ContainsKey(codeStr)) {
				var codePoint = Config.IconCodes[codeStr];

				var code = int.Parse(codePoint, NumberStyles.HexNumber);
				var unicodeString = char.ConvertFromUtf32(code);

				if (value.Length > 0) {
					str += "<w:r xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"> ";

					str += "<w:t> " + ParseChars(value) + " <w:br/> </w:t> </w:r> ";

					str += "<w:r xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"> ";
				}

				str += "<w:rPr><w:color w:val=\"" + GetColorHex(color) + "\" /> ";

				str += "<w:rFonts w:ascii=\"Material Icons\" w:hAnsi=\"Material Icons\" w:cs=\"Material Icons\"/> </w:rPr> ";

				str += "<w:t> " + unicodeString + " </w:t> </w:r> ";

				if (value.Length > 0) {
					str += "</w:r>";
				}
			}
			else {
				str += "<w:t> " + ParseChars(value) + " </w:t> </w:r> ";
			}

			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(str);
			return "base64:" + System.Convert.ToBase64String(plainTextBytes);
		}

		private static string ParseChars(string text) {
			return text.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
		}

		private static string GetColorHex(string color) {
			switch (color) {
				case "red":
					return "F44335";
				case "pink":
					return "E91E63";
				case "purple":
					return "9F27B0";
				case "indigo":
					return "3F51B5";
				case "blue":
					return "2196F3";
				case "cyan":
					return "00BCD4";
				case "teal":
					return "4DB6AC";
				case "green":
					return "4CAF50";
				case "lime":
					return "CDDC39";
				case "yellow":
					return "FFEB3B";
				case "amber":
					return "FFC107";
				case "orange":
					return "FF9800";
				case "brown":
					return "795548";
				case "white":
					return "FFFFFF";
				case "black":
					return "212121";
				case "accent-2":
					return "AC70AC";
				case "accent":
					return "FFE97C";
				case "alert":
					return "F48A60";
				case "warn":
					return "F3BD40";
				case "fuchsia":
					return "FF77FF";
				case "deepPurple":
				case "deep purple":
					return "673AB7";
				case "lightBlue":
				case "light blue":
					return "03A9F4";
				case "lightGreen":
				case "light green":
					return "8BC34A";
				case "lightGrey":
				case "light grey":
					return "EEEEEE";
				case "grey":
					return "BDBDBD";
				case "darkGrey":
				case "dark grey":
					return "9E9E9E";
			}
			return "000000";
		}
	}
}