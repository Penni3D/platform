﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Threading;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.interfaces {
	public class Email : ConnectionBase {
		public enum AddressTypes {
			ToAdress = 0,
			CcAddress = 1,
			BccAddress = 2
		}

		private readonly AuthenticatedSession _authenticatedSession;
		private readonly List<int> _bccRecipientIds = new List<int>();
		private readonly List<string> _bccRecipients = new List<string>();

		public string _body;
		private readonly List<int> _ccRecipientIds = new List<int>();
		private readonly List<string> _ccRecipients = new List<string>();

		private readonly SmtpClient _client;

		private string _error = "";
		private readonly string _from = "";
		private readonly string _host = "";
		private readonly string _password = "";
		private readonly int _port = 25;
		private readonly bool _sending;
		public string _subject;

		private readonly List<int> _toRecipientIds = new List<int>();

		private readonly List<string> _toRecipients = new List<string>();
		private readonly string _username = "";

		public Email(string instance, int item_id, string body = null, string subject = null) :
			base(instance, new AuthenticatedSession(instance, item_id, null)) {
			_authenticatedSession = null;

			//Read settings
			_sending = Conv.ToBool(Config.GetValue("Notifications", "Emails"));
			_from = Conv.ToStr(Config.GetValue("SmtpServer", "FromAddress"));
			_username = Conv.ToStr(Config.GetValue("SmtpServer", "Username"));
			_password = Conv.ToStr(Config.GetValue("SmtpServer", "Password"));
			_from = Conv.ToStr(Config.GetValue("SmtpServer", "FromAddress"));
			_host = Conv.ToStr(Config.GetValue("SmtpServer", "Host"));
			_port = Conv.ToInt(Config.GetValue("SmtpServer", "Port"));

			//Instanciate client with credentials (if required)
			_client = CreateClient();

			_body = body;
			_subject = subject;
		}

		private SmtpClient CreateClient() {
			var result = new SmtpClient(_host, _port);
			if (_username == "") return result;
			var smtpCredentials = new NetworkCredential(_username, _password);
			result.Credentials = smtpCredentials;
			return result;
		}

		public Email(string instance, AuthenticatedSession authenticatedSession, string body = null,
			string subject = null) :
			base(instance, authenticatedSession) {
			_authenticatedSession = authenticatedSession;

			//Read settings
			_sending = Conv.ToBool(Config.GetValue("Notifications", "Emails"));
			_from = Conv.ToStr(Config.GetValue("SmtpServer", "FromAddress"));
			_username = Conv.ToStr(Config.GetValue("SmtpServer", "Username"));
			_password = Conv.ToStr(Config.GetValue("SmtpServer", "Password"));
			_from = Conv.ToStr(Config.GetValue("SmtpServer", "FromAddress"));
			_host = Conv.ToStr(Config.GetValue("SmtpServer", "Host"));
			_port = Conv.ToInt(Config.GetValue("SmtpServer", "Port"));

			//Instanciate client with credentials (if required)
			_client = new SmtpClient(_host, _port);
			if (_username != "") {
				var smtpCredentials = new NetworkCredential(_username, _password);
				_client.Credentials = smtpCredentials;
			}

			_body = body;
			_subject = subject;
		}

		public string GetSystemLink(AuthenticatedSession authenticatedSession, string uuid = "") {
			if (uuid != "")
				uuid = "/?view=" + uuid;
			return authenticatedSession.GetMember("protocol") + "://" +
			       (authenticatedSession.GetMember("wwwpath") + uuid).Replace("///", "/").Replace("//", "/");
		}

		public void Add(string address, AddressTypes type) {
			switch (type) {
				case AddressTypes.ToAdress:
					_toRecipients.Add(address);
					break;
				case AddressTypes.CcAddress:
					_ccRecipients.Add(address);
					break;
				case AddressTypes.BccAddress:
					_bccRecipients.Add(address);
					break;
			}
		}

		public void Add(int itemId, AddressTypes type) {
			switch (type) {
				case AddressTypes.ToAdress:
					_toRecipientIds.Add(itemId);
					break;
				case AddressTypes.CcAddress:
					_ccRecipientIds.Add(itemId);
					break;
				case AddressTypes.BccAddress:
					_bccRecipientIds.Add(itemId);
					break;
			}
		}

		public void Remove(string address, AddressTypes type) {
			switch (type) {
				case AddressTypes.ToAdress:
					_toRecipients.Remove(address);
					break;
				case AddressTypes.CcAddress:
					_ccRecipients.Remove(address);
					break;
				case AddressTypes.BccAddress:
					_bccRecipients.Remove(address);
					break;
			}
		}

		public void Remove(int itemId, AddressTypes type) {
			switch (type) {
				case AddressTypes.ToAdress:
					_toRecipientIds.Remove(itemId);
					break;
				case AddressTypes.CcAddress:
					_ccRecipientIds.Remove(itemId);
					break;
				case AddressTypes.BccAddress:
					_bccRecipientIds.Remove(itemId);
					break;
			}
		}

		public void ClearRecipients() {
			_toRecipientIds.Clear();
			_ccRecipientIds.Clear();
			_bccRecipientIds.Clear();
			_toRecipients.Clear();
			_ccRecipients.Clear();
			_bccRecipients.Clear();
		}

		private void ResolveAddresses() {
			var cols = "";
			User u;
			u = _authenticatedSession != null
				? new User(instance, _authenticatedSession.item_id)
				: new User(instance, 0);

			cols = Conv.ToDelimitedString(_toRecipientIds);
			foreach (var address in u.GetEmailAddresses(cols)) {
				Add(address, AddressTypes.ToAdress);
			}

			cols = Conv.ToDelimitedString(_ccRecipientIds);
			foreach (var address in u.GetEmailAddresses(cols)) {
				Add(address, AddressTypes.CcAddress);
			}

			cols = Conv.ToDelimitedString(_bccRecipientIds);
			foreach (var address in u.GetEmailAddresses(cols)) {
				Add(address, AddressTypes.BccAddress);
			}
		}

		private string ApplyTemplate(string subject, string body) {
			body = body.Replace("<p>", "<div>");
			body = body.Replace("</p>", "</div>");

			var template =
				"<!DOCTYPE html><html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"><head>\t<meta charset=\"utf-8\">\t<meta name=\"viewport\" content=\"width=device-width\">\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\t<meta name=\"x-apple-disable-message-reformatting\">\t<title>Keto Software</title>\t<link href=\"https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap\" rel=\"stylesheet\">\t<style>        .isMobile {            display: none;        }        @media only screen and (max-width: 415px) {            .isMobile {                display: table !important;            }            .isDesktop {                display: none !important;            }        }\t</style>\t<style>        center {            padding-top:64px;background: #F5F5F5;        }        p {            margin:0!important;            padding:0!important;        }        html,        body {            margin: 0 auto !important;            padding: 0 !important;            height: 100% !important;            width: 100% !important;        }        * {            -ms-text-size-adjust: 100%;            -webkit-text-size-adjust: 100%;        }        div[style*=\"margin: 16px 0\"] {            margin: 0 !important;        }        table,        td {            mso-table-lspace: 0pt !important;            mso-table-rspace: 0pt !important;        }        table {            border-spacing: 0 !important;            border-collapse: collapse !important;            table-layout: fixed !important;            margin: 0 auto !important;        }        table table table {            table-layout: auto;        }        img {            -ms-interpolation-mode: bicubic;        }        *[x-apple-data-detectors], /* iOS */        .x-gmail-data-detectors, /* Gmail */        .x-gmail-data-detectors *,        .aBn {            border-bottom: 0 !important;            cursor: default !important;            color: inherit !important;            text-decoration: none !important;            font-size: inherit !important;            font-family: inherit !important;            font-weight: inherit !important;            line-height: inherit !important;        }        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */        .a6S {            display: none !important;            opacity: 0.01 !important;        }        /* If the above doesn't work, add a .g-img class to any image in question. */        img.g-img + div {            display: none !important;        }        /* What it does: Prevents underlining the button text in Windows 10 */        .button-link {            text-decoration: none !important;        }        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {            /* iPhone 6 and 6+ */            .email-container {                min-width: 375px !important;            }        }\t</style>\t<style>        /* What it does: Hover styles for buttons */        .button-td,        .button-a {            transition: all 100ms ease-in;        }        .button-td:hover,        .button-a:hover {            background: #555555 !important;            border-color: #555555 !important;        }        /* Media Queries */        @media screen and (max-width: 480px) {            center {                padding-top:24px;background: #FFF;            }            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */            .fluid {                width: 100% !important;                max-width: 100% !important;                height: auto !important;                margin-left: auto !important;                margin-right: auto !important;            }            /* What it does: Forces table cells into full-width rows. */            .stack-column,            .stack-column-center {                display: block !important;                width: 100% !important;                max-width: 100% !important;                direction: ltr !important;            }            /* And center justify these ones. */            .stack-column-center {                text-align: center !important;            }            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */            .center-on-narrow {                text-align: center !important;                display: block !important;                margin-left: auto !important;                margin-right: auto !important;                float: none !important;            }            table.center-on-narrow {                display: inline-block !important;            }            /* What it does: Adjust typography on small screens to improve readability */            .email-container p {                font-size: 15px !important;                line-height: 20px !important;            }        }\t</style>\t<!--[if gte mso 9]>\t<xml>\t\t<o:OfficeDocumentSettings>\t\t\t<o:AllowPNG/>\t\t\t<o:PixelsPerInch>96</o:PixelsPerInch>\t\t</o:OfficeDocumentSettings>\t</xml>\t<![endif]--></head><body width=\"100%\" bgcolor=\"#F5F5F5\" style=\"margin: 0; mso-line-height-rule: exactly;\"><center style=\"width: 100%; text-align: left;\">\t<div style=\"max-width: 600px; padding:20px 20px 0px 40px;margin: auto; background-color: #fff\" class=\"email-container\">\t\t<!--[if mso]>\t\t<table role=\"presentation\" style=\"background-color: #fff;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"600\" align=\"center\">\t\t\t<tr>\t\t\t\t<td style=\"padding: 40px 40px 0px 40px; mso-line-height-rule:exactly;line-height: 24px\">\t\t<![endif]-->\t\t<div style=\"font-family: 'Roboto', sans-serif; font-size:30px; padding-top: 10px;line-height:36px;\">#messagesubject</div>\t\t<div style=\"font-family: 'Roboto', sans-serif; line-height:24px; font-size:14px; text-align: justify;margin-bottom: 46px; width:95%\"><br>#messagebody</div>\t\t<!--[if mso]>\t\t<div style=\"text-align: right;height: 115px;\">\t\t\t<table width=\"50%\"><tr><td><img width=\"287\" src=\"#imglink\" alt=\"img\" style=\"text-align: right; width: 287px; border: 0; text-decoration:none; vertical-align: baseline;\"></td></tr></table>\t\t</div>\t\t<div style=\"display:none\">\t\t<![endif]-->\t\t<div style=\"text-align:right;height: 115px; max-width: 600px;\">\t\t\t<img src=\"#imglink\" style=\"width:287px;height:115px;\" />\t\t</div>\t\t<!--[if mso]>\t\t</div>\t\t<![endif]-->\t\t<!-- Email Body : END -->\t\t<!--[if mso]>\t\t</td>\t\t</tr>\t\t</table>\t\t<![endif]-->\t</div></center></body></html>";
			
			if (_authenticatedSession != null && _authenticatedSession.GetMember("wwwpath").ToString() != "false") {
				var www = _authenticatedSession.GetMember("wwwpath").ToString();
				if (www != null && !www.EndsWith("/")) www += "/";
				var ilink = _authenticatedSession.GetMember("protocol") + "://" +
				            (www + "images/logos/email-logo.gif").Replace("///", "/").Replace("//", "/");
				template = template.Replace("#imglink", ilink);
			} else {
				template = template.Replace("#imglink", "");
			}

			return template.Replace("#messagesubject", subject).Replace("#messagebody", body)
				.Replace("#messagepreview", Modi.Left(body, 50));
		}



		public bool Send(string subject = null, string body = null, bool isHtml = true, bool digest = false,
			int notificationItemId = 0, bool async = true, string instanceOpt = "") {
			var returnValue = true;

			if (instanceOpt.Length > 0)
				instance = instanceOpt;

			var _originalbody = _body;
			ResolveAddresses();

			var sender = _authenticatedSession?.item_id ?? 0;
			var threads = new List<Thread>();
			foreach (var a in _toRecipients) {

				var newRecipientsStyle = new List<string>();
				newRecipientsStyle.Add(a);

				//Create message
				var mail = new MailMessage {
					From = new MailAddress(_from), Sender = new MailAddress(_from), IsBodyHtml = isHtml
				};

				_subject ??= subject;
				_body ??= body;

				if (_subject == null)
					LogEmailError("Email does not have a subject.");

				if (_body == null)
					LogEmailError("Email does not have a body.");

				if (_body != null && sender > 0) {
					_body = ReplaceUserTags(a, _originalbody, sender);
				}

				if (_subject != null && sender > 0) {
					_subject = ReplaceUserTags(a, _subject, sender);
				}

				var translatedBody = _body;
				var translatedSubject = _subject;

				mail.Subject = Conv.ToStr(_subject);
				mail.Body = isHtml ? ApplyTemplate(Conv.ToStr(_subject), Conv.ToStr(_originalbody)) : Conv.ToStr(_originalbody);
				mail.Body = sender > 0 ? ReplaceUserTags(a, mail.Body, sender) : mail.Body;
				mail.Subject = sender > 0 ? ReplaceUserTags(a, mail.Subject, sender) : mail.Subject;

				if (Util.isEmailValid(a))
					mail.To.Add(a);
				else
					LogEmailError("Email (to) address is invalid: " + a);

				//Send email
				if (_sending) {
					if (async) {
						//If asynchroneous save thread for later
						var asyncThread = new Thread(delegate() {
							try {
								if (digest == false)
									CreateClient().Send(mail);
								StoreEmail(mail.Subject, mail.Body, newRecipientsStyle, true, digest,
									translatedBody, translatedSubject, notificationItemId);
							} catch (Exception ex) {
								LogEmailError(ex);
								StoreEmail(mail.Subject, mail.Body, newRecipientsStyle, false, digest,
									translatedBody, translatedSubject, notificationItemId);
							}
						});
						threads.Add(asyncThread);
					} else {
						//If synchroneous send immediately
						try {
							if (digest == false)
								_client.Send(mail);
							StoreEmail(mail.Subject, mail.Body, newRecipientsStyle, true, digest,
								translatedBody, translatedSubject, notificationItemId);
						} catch (Exception ex) {
							LogEmailError(ex);
							StoreEmail(mail.Subject, mail.Body, newRecipientsStyle, false, digest,
								translatedBody, translatedSubject, notificationItemId);
							returnValue = false;
						}
					}
				} else {
					LogEmailError("Email is disabled");
					StoreEmail(mail.Subject, mail.Body, newRecipientsStyle, false, digest,
						translatedBody, translatedSubject, notificationItemId);
				}
			}

			//Send Async Emails
			foreach (var t in threads) {
				t.Start();
			}

			return returnValue;
		}

		private string ReplaceUserTags(string receiverAddress, string content, int senderItemId) {
			SetDBParam(SqlDbType.NVarChar, "@address", receiverAddress);
			var tags = content.Split(new[] {'{', '}'}, StringSplitOptions.RemoveEmptyEntries);
			var userDatas = db.GetDatatable("SELECT * FROM _" + instance + "_user WHERE email = '" +
			                                Conv.ToSql(receiverAddress) + "' OR item_id = " + Conv.ToSql(senderItemId));
			var userData = userDatas.Select("email = '" + Conv.ToSql(receiverAddress) + "'");
			var senderData = userDatas.Select("item_id = '" + Conv.ToSql(senderItemId) + "'");

			foreach (var t in tags) {
				if (Conv.ToStr(Conv.ToDateTime(t)).Length > 0) {
					SetDBParam(SqlDbType.Int, "@item_id", Conv.ToInt(userData[0]["item_id"]));
					var sq = "SELECT date_format FROM _" + instance + "_user WHERE item_id = @item_id";
					var row = db.GetDataRow(sq, DBParameters);
					var userDateFormat = Conv.ToStr(row["date_format"]);
					var format ="MM/dd/yyyy";

					if (userDateFormat is "dd.mm.yy" or "dd/MM/yyyy") {
						format = "dd/MM/yyyy";
					}

					var dateString = Convert.ToDateTime(t).ToString(format);

					if (userDateFormat == "dd.mm.yy") {
						dateString = dateString.Replace("/", ".");
					}
					content = content.Replace("{" + t + "}",
						dateString);
				}
			}

			if (userData.Length > 0) {
				var userItemBase = new ItemBase(instance, "user", _authenticatedSession);
				var uD = userItemBase.GetItem(Conv.ToInt(userData[0]["item_id"]));
				var sD = userItemBase.GetItem(Conv.ToInt(senderData[0]["item_id"]));
				foreach (DataRow r in db.GetDatatable(
					"SELECT tag_type, json_tag_id, ntl.item_column_id, name, ic.data_type, ic.data_additional, ic.item_column_id, tg.list_item FROM _" +
					instance +
					"_notification_tag_links ntl INNER JOIN _" + instance +
					"_notification_tags tg ON tg.item_id = ntl.json_tag_id INNER JOIN item_columns ic ON ntl.item_column_id = ic.item_column_id WHERE process_name = 'user' AND tag_type IN (5,6)",
					DBParameters).Rows) {
					var data = new Dictionary<string, object>();

					if (Conv.ToInt(r["tag_type"]) == 5) {
						data = uD;
					} else {
						data = sD;
					}

					if (data.ContainsKey(Conv.ToStr(r["name"]))) {
						if (Conv.ToInt(r["data_type"]) == 5 || Conv.ToInt(r["data_type"]) == 6 ||
						    Conv.ToInt(r["data_type"]) == 16) {
							var _itemColumns = new ItemColumns(instance, "user", _authenticatedSession);

							var itemList = new List<int>();
							string processNameHere = Conv.ToStr(r["data_additional"]);

							var icSubDataType = 1000;

							if (Conv.ToInt(r["data_type"]) == 16) {
								var ic = _itemColumns.GetItemColumn(Conv.ToInt(r["item_column_id"]));
								icSubDataType = ic.SubDataType;
								if (icSubDataType < 5) {
									content = content.Replace("[" + r["list_item"] + "]",
										Conv.ToStr(data[Conv.ToStr(r["name"])]));
								} else if (icSubDataType == 5 || icSubDataType == 6) {
									processNameHere = ic.SubDataAdditional;
									Lucene.Net.Support.C5.IList<int> collectionOfSelectedIds =
										(Lucene.Net.Support.C5.IList<int>) data[Conv.ToStr(r["name"])];
									foreach (var item_id in collectionOfSelectedIds) {
										itemList.Add(Conv.ToInt(item_id));
									}
								}
							} else {
								itemList = data[Conv.ToStr(r["name"])] as List<int>;
							}

							var ib = new ItemBase(instance, processNameHere, _authenticatedSession);

							var list_data = "";
							foreach (var id in itemList) {
								if (Conv.ToInt(r["data_type"]) == 5 ||
								    Conv.ToInt(r["data_type"]) == 16 && icSubDataType == 5) {
									var item = ib.GetItem(id, checkRights: false);
									var ppcs2 =
										new ProcessPortfolioColumns(instance, processNameHere,
											_authenticatedSession);

									foreach (var pc2 in ppcs2.GetPrimaryColumns()) {
										if (list_data.Length > 0) {
											list_data += " " + item[pc2.ItemColumn.Name];
										} else {
											list_data += item[pc2.ItemColumn.Name];
										}
									}
								} else {
									var item = ib.GetItem(id, checkRights: false);
									if (list_data == "")
										list_data += item["list_item"];
									else
										list_data += ", " + item["list_item"];
								}
							}
							content = content.Replace("[" + r["list_item"] + "]", Conv.ToStr(list_data));
						} else {
							content = content.Replace("[" + r["list_item"] + "]",
									Conv.ToStr(data[Conv.ToStr(r["name"])]));
						}
					}
				}
			}
			return content;
		}

		private void StoreEmail(string subject, string body, List<string> to, bool success, bool digest,
			string transBody, string transSubject, int notificationId = 0) {
			//This needs to be thread safe, so SQL parameters are created into another collection
			var pl = new List<SqlParameter>();

			SetDBParamToList(SqlDbType.NVarChar, "instance", instance, pl);
			SetDBParamToList(SqlDbType.NVarChar, "subject", subject, pl);
			SetDBParamToList(SqlDbType.NVarChar, "body", body, pl);
			SetDBParamToList(SqlDbType.NVarChar, "error", _error, pl);
			SetDBParamToList(SqlDbType.NVarChar, "to", Modi.JoinListWithComma(to), pl);
			SetDBParamToList(SqlDbType.Int, "status", success ? 1 : 0, pl);
			SetDBParamToList(SqlDbType.Int, "digest_status", digest ? 1 : 0, pl);
			SetDBParamToList(SqlDbType.NVarChar, "transSubject", transSubject, pl);
			SetDBParamToList(SqlDbType.NVarChar, "transBody", transBody, pl);
			SetDBParamToList(SqlDbType.Int, "notificationId", notificationId, pl);

			db.ExecuteInsertQuery(
				"INSERT INTO instance_emails (instance, subject, body, to_recipients, status, digest_status, error_description , notification_item_id, translated_body, translated_subject) VALUES (@instance, @subject, @body, @to, @status, @digest_status, @error, @notificationId, @transBody, @transSubject)",
				pl);
		}

		private void LogEmailError(string message) {
			if (_error != message) {
				_error += message;
			}

			if (_authenticatedSession != null) {
				Diag.Log(instance, message, _authenticatedSession.item_id, Diag.LogSeverities.Warning,
					Diag.LogCategories.Email);
			} else {
				Diag.Log(instance, message, 0, Diag.LogSeverities.Warning, Diag.LogCategories.Email);
			}
		}

		public List<InstanceEmailLog> GetEmailLogs(string instance, InstanceEmailFilter ief) {
			var minRowNumber = ief.MinRow;
			var maxRowNumber = ief.MaxRow;

			var filterSql = "";
			var orderByText = "";
			//var filterSql2 = "";

			if (ief.SearchText.Length > 0) {
				filterSql = "WHERE (subject LIKE '%" + Conv.Escape(ief.SearchText) + "%' OR body LIKE '%" +
				            Conv.Escape(ief.SearchText) + "%' OR to_recipients LIKE '%" +
				            Conv.Escape(ief.SearchText) + "%')";
			}

			var initialOrder = "DESC";
			var defaultOrderBy = "instance_email_id";

			if (ief.OrderBy != null) {
				orderByText = "ORDER BY " + Conv.ToSql(ief.OrderBy[0]) + " " + Conv.ToSql(ief.OrderBy[1]);
				initialOrder = ief.OrderBy[1];
				defaultOrderBy = ief.OrderBy[0];
			}

			var result = new List<InstanceEmailLog>();
			var db = new Connections(true);

			var maxNumber = Conv.ToInt(db.ExecuteScalar(
				"WITH logs AS ( SELECT instance_email_id FROM instance_emails ig " + filterSql +
				") SELECT COUNT(*) as allrows FROM logs "));

			var sql =
				"SELECT row_num,instance_email_id, subject, body,to_recipients,status, error_description, translated_body, translated_subject, sent_on  FROM ( " +
				"SELECT " +
				"ROW_NUMBER() OVER ( " +
				"	ORDER BY " + defaultOrderBy + " " + initialOrder +
				"	) row_num,instance_email_id, subject, body,to_recipients,status, error_description, translated_body, translated_subject, sent_on " +
				"	FROM " +
				"instance_emails ig  " + filterSql +
				"	) iq WHERE row_num > " + minRowNumber + " AND row_num <= " + maxRowNumber + " " +
				orderByText;

			foreach (DataRow r in db.GetDatatable(sql).Rows) {
				var il = new InstanceEmailLog();

				var escapedB = "";
				var escapedS = "";

				if (Conv.ToStr(r["translated_body"]).Length > 0) {
					escapedB = Conv.ToStr(r["translated_body"]).Replace("<p>", "");
					escapedB = escapedB.Replace("</p>", "");
				}

				if (Conv.ToStr(r["translated_subject"]).Length > 0) {
					escapedS = Conv.ToStr(r["translated_subject"]).Replace("<p>", "");
					escapedS = escapedS.Replace("</p>", "");
				}

				il.InstanceEmailId = Conv.ToInt(r["instance_email_id"]);
				il.Subject = Conv.ToStr(r["subject"]);
				il.MailBody = Conv.ToStr(r["body"]);
				il.Recipients = Conv.ToStr(r["to_recipients"]);
				il.ErrorMessage = Conv.ToStr(r["error_description"]);
				il.Status = Conv.ToInt(r["status"]);
				il.TranslatedBody = escapedB;
				il.TranslatedSubject = escapedS;
				il.MaxRows = maxNumber;
				il.SentOn = Conv.ToDateTime(r["sent_on"]);
				result.Add(il);
			}

			return result;
		}

		private void LogEmailError(Exception ex) {
			_error += Conv.ToStr(ex);
			if (_authenticatedSession != null) {
				Diag.Log(instance, ex, _authenticatedSession.item_id, Diag.LogCategories.Email);
			} else {
				Diag.Log(instance, ex, 0, Diag.LogCategories.Email);
			}
		}

		public class InstanceEmailLog {
			public int InstanceEmailId { get; set; }
			public string Subject { get; set; }
			public string MailBody { get; set; }
			public string Recipients { get; set; }
			public int Status { get; set; }

			public int MaxRows { get; set; }
			public string ErrorMessage { get; set; }

			public DateTime? SentOn { get; set; }
			public string TranslatedBody { get; set; }
			public string TranslatedSubject { get; set; }

			public InstanceEmailLog() { }

			public InstanceEmailLog(DataRow row, AuthenticatedSession session, string alias = "") {
				if (alias.Length > 0)
					InstanceEmailId = (int) row["instance_email_id"];
				Subject = (string) row["subject"];
				MailBody = (string) row["body"];
				Recipients = (string) row["to_recipients"];
				Status = (int) row["status"];
				ErrorMessage = (string) row["error_description"];
				TranslatedBody = (string) row["translated_body"];
				TranslatedSubject = (string) row["translated_subject"];
				SentOn = (DateTime) row["sent_on"];
			}
		}

		public class InstanceEmailFilter {
			public string SearchText { get; set; }
			public int? Status { get; set; }
			public int MinRow { get; set; }
			public int MaxRow { get; set; }

			public List<string> OrderBy { get; set; }
			public InstanceEmailFilter() { }
		}
	}
}