using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.calendar;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.features.user;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.SignalR;
using System.Dynamic;
using System.Linq;
using Keto5.x.platform.server.processes.backgroundProcesses;

namespace Keto5.x.platform.server.interfaces {
	public static class AzureRunInfo {
		public static Dictionary<string, ExpandoObject> runInfo;
	}

	public class AzureAd : ConnectionBase {
		private AuthenticatedSession session;
		private BackgroundProcessBase bgBase;

		public AzureAd(string instance, AuthenticatedSession session, BackgroundProcessBase bgBase) : base(instance,
			session) {
			this.session = session;
			this.bgBase = bgBase;
		}

		public BackgroundProcessResult.Outcomes
			Sync(string allowedDomains = "all", int runOption = -1, int index = -1) {
			var tempAllowed = new List<string>();

			foreach (string domain in allowedDomains.Split(",")) {
				if (tempAllowed.Contains(domain) == false) tempAllowed.Add(domain);
			}

			return SyncRun(string.Join(",", tempAllowed.ToArray()), runOption, index);
		}

		private BackgroundProcessResult.Outcomes SyncRun(string allowedDomains, int runOption, int index) {
			//runOption -1 = as defined in confuguration, 0 = whole process,1 = get only users,2 = get only groups(list) Not working if useADSyncedGroups=false,3 = test authentication
			var domains = Config.Domains;
			int failed = 0;
			bool affected = false;

			if (allowedDomains != "all") {
				List<string> toBeRemoved = new List<string>();
				foreach (string domain in domains) {
					if (allowedDomains.Contains(",")) {
						var allowedArr = allowedDomains.Split(",");
						if (allowedDomains.Contains(domain) == false) {
							toBeRemoved.Add(domain);
						}
					}
					else {
						if (allowedDomains != domain) {
							toBeRemoved.Add(domain);
						}
					}
				}

				foreach (string del in toBeRemoved) {
					domains.RemoveAt(domains.IndexOf(del));
				}
			}

			DataRow instanceRow = db.GetDataRow("SELECT TOP 1 * FROM instances WHERE instance='" + instance + "'");

			var users = new ItemBase(instance, "user", session);
			var userGroups = new ItemBase(instance, "user_group", session);
			var UserGroupsHandler = new UserGroupsData(instance, "user", session);
			var azureAdGroups = new ItemBase(instance, "list_azure_ad_groups", session);

			var startTime = DateTime.Now;
			string errorMsg = "";

			DataTable groupData = userGroups.GetAllData();
			DataTable azureAdGroupListData = azureAdGroups.GetAllData();
			Dictionary<string, DataRow> azureGroupListCompare = new Dictionary<string, DataRow>();

			foreach (DataRow row in azureAdGroupListData.Rows) {
				azureGroupListCompare.Add(Conv.ToStr(row["list_domain"]) + "__" + Conv.ToStr(row["list_code"]), row);
			}

			foreach (string domain in domains) {
				//Log related variables
				int addedUsers = 0;
				int usersDisabled = 0;
				int userInfoChanged = 0;
				int adGroupsAdded = 0;
				int adGroupInfoChanged = 0;
				var adGroupsNotInSync = new List<string>();
				int usersAddedTogroup = 0;
				int usersRemovedFromGroup = 0;
				int usersReActivated = 0;
				bool usersRead = false;
				bool groupsRead = false;
				bool accessTokenFail = false;
				var userReadError = "";
				var groupReadError = "";
				List<string> groupUsersNotFoundFromDatabase = new List<string>();

				bool canSaveUsers = true;

				var mappingSettings = Config.GetDomain(domain, "AzureADMappings");
				var clientid = Config.GetDomainValue(domain, "AzureADAuthentication", "ClientID");
				var clientkey =
					Uri.EscapeDataString((Config.GetDomainValue(domain, "AzureADAuthentication", "ClientKey")));
				var tenant = Config.GetDomainValue(domain, "AzureADAuthentication", "Tenant");
				var authUrl = "https://login.microsoftonline.com/" + tenant + "/oauth2/v2.0/token";
				var groupFilter =
					Conv.ToStr(Config.GetDomainValue(domain, "AzureADGroupSettings", "GroupNameStartsWith"));
				var groupFilterLike =
					Conv.ToStr(Config.GetDomainValue(domain, "AzureADGroupSettings", "GroupNameLike"));
				var accessToken = "";

				var modeSettings = Config.GetDomain(domain, "AzureADMappings");

				bool GetAllUsersModeInUse = false;
				bool GroupsModeInUse = false;
				bool TestAuth = false;

				if (runOption == -1) {
					//As defined in configuration
					var GetAllUsersStr =
						Convert.ToString(Config.GetDomainValue(domain, "AzureADUserSettings", "GetAllUsers"));
					if (GetAllUsersStr != "true" && GetAllUsersStr != "false") {
						GetAllUsersModeInUse = true;
					}
					else {
						GetAllUsersModeInUse =
							Conv.ToBool(Config.GetDomainValue(domain, "AzureADUserSettings", "GetAllUsers"));
					}

					bool IgnoreUserChanges = Convert.ToBoolean(Config.GetDomainValue(domain, "AzureADUserSettings", "IgnoreUserChanges"));
					if(IgnoreUserChanges) {
						canSaveUsers = false;
					}

					GroupsModeInUse =
						Conv.ToBool(Config.GetDomainValue(domain, "AzureADGroupSettings", "GroupSyncInUse"));
				}
				else if (runOption == 0) {
					//All users and groups
					GetAllUsersModeInUse = true;
					GroupsModeInUse = true;
				}
				else if (runOption == 1) {
					//All users
					GetAllUsersModeInUse = true;
				}
				else if (runOption == 2) {
					//All groups
					GroupsModeInUse = true;
				}
				else if (runOption == 3) {
					//All
					TestAuth = true;
				}

				//Get accessToken
				accessToken = GetAzureToken(clientid, clientkey, authUrl);
				if (accessToken == "error") {
					accessTokenFail = true;
				}
				else if (GetAllUsersModeInUse || GroupsModeInUse) {
					try {
						List<string> requestColumns = new List<string>();
						var mappings = new Dictionary<string, List<string>>();
						var listStuff = new Dictionary<string, Dictionary<string, int>>();
						var listHelper = new Dictionary<string, Dictionary<int, string>>();
						var listNameByColumn = new Dictionary<string, string>();
						var insertableList = new Dictionary<string, bool>();
						string loginRemoteAlias = "";
						List<string> fields = new List<string>();

						List<string> extensionAttributes = new List<string>();

						foreach (var item in mappingSettings) {
							if (Conv.ToStr(item.Value["value"]).Contains("|") == false) {
								string value = ((string)item.Value["value"]).Split("|")[0];
								if (value.Contains("extensionAttribute") && extensionAttributes.Contains(value) == false) extensionAttributes.Add(value);

								if (mappings.ContainsKey(value) == false) {
									mappings.Add(value, new List<string>() { item.Key });
								}
								else {
									mappings[value].Add(item.Key);
								}

								if (requestColumns.Contains(value) == false) requestColumns.Add(value);
								if (fields.Contains(value) == false) fields.Add(value);
							}
							else {
								string valuePart = ((string)item.Value["value"]).Split("|")[0];
								if (valuePart.Contains("extensionAttribute") && extensionAttributes.Contains(valuePart)==false) extensionAttributes.Add(valuePart);
								bool listPart = Conv.ToBool(((string)item.Value["value"]).Split("|")[1]);
								if (mappings.ContainsKey(valuePart) == false) {
									mappings.Add(valuePart, new List<string>() { item.Key });
								}
								else {
									mappings[valuePart].Add(item.Key);
								}

								if (requestColumns.Contains(valuePart) == false) requestColumns.Add(valuePart);
								if (fields.Contains(valuePart) == false) fields.Add(valuePart);
								if (insertableList.ContainsKey(item.Key) == false)
									insertableList.Add(item.Key, listPart);

								listStuff.Add(item.Key, new Dictionary<string, int>());
								listHelper.Add(item.Key, new Dictionary<int, string>());
							}

							if (item.Key == "login") {
								loginRemoteAlias = (string)item.Value["value"];
							}
						}

						DataTable userData = users.GetAllData();
						for (int i = 0; i < userData.Rows.Count; i++) {
							if (userData.Rows[i]["domain"] == System.DBNull.Value)
								userData.Rows[i]["domain"] = "default";
						}

						if (listStuff.Keys.Count > 0) {
							string listDataSql =
								"select idps.item_id,name,STRING_AGG(idps.selected_item_id,',') AS list_item_id from item_columns ic " +
								"LEFT JOIN item_data_process_selections idps ON idps.item_column_id=ic.item_column_id " +
								"where process='user' AND name IN('" +
								String.Join("','", listStuff.Keys.ToList<string>().ToArray()) +
								"') group by idps.item_id,name";
							DataTable lisData = db.GetDatatable(listDataSql);

							List<string> keyList = new List<string>(listStuff.Keys);


							//SetDBParam(SqlDbType.Int, "@columnId", columnId);

							string sql = "SELECT name,data_additional FROM item_columns WHERE name IN('" +
										 String.Join("','", keyList.ToArray()) + "') AND process='user'";
							DataTable listColumns = db.GetDatatable(sql, DBParameters);

							foreach (DataRow row in listColumns.Rows) {
								string listItemSql = "SELECT item_id,list_item FROM _" + instance + "_" +
													 row["data_Additional"];
								DataTable listItems = db.GetDatatable(listItemSql, DBParameters);
								foreach (DataRow row2 in listItems.Rows) {
									listStuff[Conv.ToStr(row["name"])].Add(Conv.ToStr(row2["list_item"]).ToUpper(),
										Conv.ToInt(row2["item_id"]));
									listHelper[Conv.ToStr(row["name"])].Add(Conv.ToInt(row2["item_id"]),
										Conv.ToStr(row2["list_item"]));
								}

								if (listNameByColumn.ContainsKey(Conv.ToStr(row["name"])) == false)
									listNameByColumn.Add(Conv.ToStr(row["name"]), Conv.ToStr(row["data_Additional"]));
							}

							foreach (string fieldName in listStuff.Keys.ToList<string>()) {
								userData.Columns.Add(fieldName, typeof(string));

								var fieldSpecific = listHelper[fieldName];

								for (int i = 0; i < userData.Rows.Count; i++) {
									//userData.Rows[i][fieldName] == System.DBNull.Value) userData.Rows[i]["domain"] = "default";
									var li = new List<string>();
									foreach (DataRow row in lisData.Select("item_id=" +
																		   Conv.ToInt(userData.Rows[i]["item_id"]) +
																		   " AND name='" + fieldName + "'")) {
										//fieldSpecific[row["list_item_id"]]
										if (Conv.ToInt(row["list_item_id"]) != 0)
											li.Add(fieldSpecific[Conv.ToInt(row["list_item_id"])]);
									}

									userData.Rows[i][fieldName] = String.Join(";", li.ToArray());
								}
							}
						}

						if (fields.Contains("userPrincipalName") == false) fields.Add("userPrincipalName");
						if (fields.Contains("id") == false) fields.Add("id");
						if (fields.Contains("accountEnabled") == false) fields.Add("accountEnabled");

						if (GetAllUsersModeInUse && canSaveUsers) {
							//Get users from Azure AD
							List<string> requestColumnsForQuary = new List<string>();
							foreach(string str in requestColumns) {						
								if (str.Contains("extensionAttribute") && requestColumnsForQuary.Contains("onPremisesExtensionAttributes")==false) {
									requestColumnsForQuary.Add("onPremisesExtensionAttributes");
								}
                                else {
									requestColumnsForQuary.Add(str);
								}
							}

							JObject userD = GetAzureUsers(accessToken, string.Join(",", requestColumnsForQuary.ToArray()));
							List<string> importedLogins = new List<string>();
							if (userD["error"] == null) {
								foreach (var item in userD["value"]) {
									if (((JObject)item).ContainsKey("onPremisesExtensionAttributes")) {
										foreach (string attr in extensionAttributes) {
											if (((JObject)item["onPremisesExtensionAttributes"]).ContainsKey(attr)) {
												((JObject)item).Add(attr, ((JObject)item["onPremisesExtensionAttributes"])[attr]);
											}
										}
									}

									importedLogins.Add(item[loginRemoteAlias].ToString().Replace("'", "''"));
									usersRead = true;
									var dict = new Dictionary<string, object>();
									if (userData.Select(
										"login='" + item[loginRemoteAlias].ToString().Replace("'", "''") +
										"' AND domain='" + domain + "'").Length > 0) {
										DataRow[] userRow = userData.Select(
											"login='" + item[loginRemoteAlias].ToString().Replace("'", "''") +
											"' AND domain='" + domain + "' AND integration_update=1");
										if (userRow.Length > 0) {
											foreach (string columnName in requestColumns) {
												foreach (string fieldName in mappings[columnName]) {
													if (Conv.ToStr(userRow[0][fieldName]).Split(";").ToList<string>()
															.ConvertAll(s => s.ToUpper())
															.Contains(Conv.ToStr(item[columnName]).Trim().ToUpper()) ==
														false) {
														if (insertableList.ContainsKey(fieldName) == false) {
															dict.Add(fieldName, (string)item[columnName]);
														}
														else {
															var listItems = listStuff[fieldName];
															foreach (string val in Conv.ToStr(item[columnName])
																.Split(";")) {
																if (listItems.ContainsKey(val.Trim().ToUpper())) {
																	if (dict.ContainsKey(fieldName) == false) {
																		dict.Add(fieldName,
																			new List<int>()
																				{listItems[val.Trim().ToUpper()]});
																	}
																	else {
																		((List<int>)dict[fieldName]).Add(
																			Conv.ToInt(listItems[
																				val.Trim().ToUpper()]));
																	}
																}
																else if (insertableList[fieldName]) {
																	var currentList = new ItemBase(instance,
																		listNameByColumn[fieldName], session);
																	var listDict = new Dictionary<string, object>();
																	listDict.Add("list_item",
																		Conv.ToStr(item[columnName]).Trim());
																	listDict.Add("in_use", 1);
																	APISaveResult saveListData = new APISaveResult();
																	saveListData.Data = listDict;
																	var resultNew =
																		currentList.AddRow(data: saveListData,
																			process: listNameByColumn[fieldName]);
																	listStuff[fieldName]
																		.Add(
																			Conv.ToStr(resultNew.Data["list_item"])
																				.ToUpper(),
																			Conv.ToInt(resultNew.Data["item_id"]));
																	listHelper[fieldName]
																		.Add(Conv.ToInt(resultNew.Data["item_id"]),
																			Conv.ToStr(resultNew.Data["list_item"]));

																	if (dict.ContainsKey(fieldName) == false) {
																		dict.Add(fieldName,
																			new List<int>() {
																				Conv.ToInt(resultNew.Data["item_id"])
																			});
																	}
																	else {
																		((List<int>)dict[fieldName]).Add(
																			Conv.ToInt(resultNew.Data["item_id"]));
																	}
																}
															}
														}
													}
												}

												if (userRow[0]["active"].ToString() == "0" &&
													dict.ContainsKey("active") == false) {
													dict.Add("active", 1);
													usersReActivated += 1;
												}
											}

											if (dict.Count > 0) {
												APISaveResult saveData = new APISaveResult();
												saveData.Data = dict;
												users.SaveItem(dict, (int)userRow[0]["item_id"], false);
												userInfoChanged += 1;
											}
										}
									}
									else {
										foreach (string columnName in requestColumns) {
											foreach (string fieldName in mappings[columnName]) {
												if (insertableList.ContainsKey(fieldName) == false) {
													dict.Add(fieldName, (string)item[columnName]);
												}
												else {
													var listItems = listStuff[fieldName];
													foreach (string val in Conv.ToStr(item[columnName]).Split(";")) {
														if (listItems.ContainsKey(val.Trim().ToUpper())) {
															if (dict.ContainsKey(fieldName) == false) {
																dict.Add(fieldName,
																	new List<int>() { listItems[val.Trim().ToUpper()] });
															}
															else {
																((List<int>)dict[fieldName]).Add(
																	Conv.ToInt(listItems[val.Trim().ToUpper()]));
															}
														}
														else if (insertableList[fieldName]) {
															var currentList = new ItemBase(instance,
																listNameByColumn[fieldName], session);
															var listDict = new Dictionary<string, object>();
															listDict.Add("list_item",
																Conv.ToStr(item[columnName]).Trim());
															listDict.Add("in_use", 1);
															APISaveResult saveListData = new APISaveResult();
															saveListData.Data = listDict;
															var resultNew = currentList.AddRow(data: saveListData,
																process: listNameByColumn[fieldName]);
															listStuff[fieldName]
																.Add(Conv.ToStr(resultNew.Data["list_item"]).ToUpper(),
																	Conv.ToInt(resultNew.Data["item_id"]));
															listHelper[fieldName]
																.Add(Conv.ToInt(resultNew.Data["item_id"]),
																	Conv.ToStr(resultNew.Data["list_item"]));

															if (dict.ContainsKey(fieldName) == false) {
																dict.Add(fieldName,
																	new List<int>()
																		{Conv.ToInt(resultNew.Data["item_id"])});
															}
															else {
																((List<int>)dict[fieldName]).Add(
																	Conv.ToInt(resultNew.Data["item_id"]));
															}
														}
													}
												}
											}
										}

										dict.Add("active", 1);
										dict.Add("domain", domain);
										dict.Add("integration_update", 1);

										if (dict.ContainsKey("timezone") == false) {
											dict.Add("timezone", Conv.ToStr(instanceRow["default_timezone"]));
										}

										APISaveResult saveData = new APISaveResult();
										saveData.Data = dict;
										var result = users.AddRow(data: saveData, process: "user");

										var calendars = new Calendars(instance, session);
										calendars.CreateUserCalendar((int)result.Data["item_id"]);

										addedUsers += 1;
									}
								}
							}
							else {
								var err = userD["error"];
								userReadError = err["code"] + " - " + err["message"];
								usersRead = false;
							}

							// Disable not imported user within this domain - only integration_update=1
							foreach (DataRow toBeDisabled in userData.Select(
								"login NOT IN('" + String.Join("','", importedLogins.ToArray()) +
								"') AND active=1 AND domain='" +
								domain + "' AND integration_update=1")) {
								var disableDict = new Dictionary<string, object>();
								disableDict.Add("active", 0);
								users.SaveItem(disableDict, (int)toBeDisabled["item_id"], false);
								usersDisabled += 1;
							}
						}

						if (GroupsModeInUse) {
							var diffInSeconds = (DateTime.Now - startTime).TotalSeconds;
							if (diffInSeconds > 3000) {
								// if less than 10 minutes left
								accessToken = GetAzureToken(clientid, clientkey, authUrl);
								if (accessToken == "error") {
									accessTokenFail = true;
								}
							}

							//Getting Azure ad groups (list)
							var groupsFromAzure = new List<string>();
							JObject groups = GetAzureGroups(accessToken, groupFilter, groupFilterLike);
							if (groups["error"] == null) {
								foreach (var group in groups["value"]) {
									groupsFromAzure.Add((string)group["id"]);
									groupsRead = true;
									int group_id = 0;
									var dict = new Dictionary<string, object>();
									if (azureGroupListCompare.ContainsKey(domain + "__" + (string)group["id"])) {
										DataRow groupRow = azureGroupListCompare[domain + "__" + (string)group["id"]];
										group_id = (int)groupRow["item_id"];
										if (Conv.ToStr(groupRow["list_item"]) != Conv.ToStr(group["displayName"])) {
											dict.Add("list_item", group["displayName"]);
										}

										if (Conv.ToStr(groupRow["list_description"]) !=
											Conv.ToStr(group["description"])) {
											dict.Add("list_description", group["description"]);
										}

										if (dict.Count > 0) {
											APISaveResult saveData = new APISaveResult();
											saveData.Data = dict;
											azureAdGroups.SaveItem(dict, (int)groupRow["item_id"], false);
											adGroupInfoChanged += 1;
										}
									}
									else {
										dict.Add("list_item", group["displayName"]);
										dict.Add("list_description", group["description"]);
										dict.Add("list_code", group["id"]);
										dict.Add("list_domain", domain);

										var result = azureAdGroups.InsertItemRow(dict);
										group_id = (int)result["item_id"];
										adGroupsAdded += 1;
									}
								}
							}
							else {
								var err = groups["error"];
								groupReadError = err["code"] + " - " + err["message"];
								groupsRead = false;
							}

							if (groupsFromAzure.Count > 0) {
								foreach (DataRow row in azureAdGroupListData.Select(
									"list_domain='" + domain + "' AND list_code NOT IN('" +
									string.Join("','", groupsFromAzure.ToArray()) + "')")) {
									if (adGroupsNotInSync.Contains((string)row["list_item"]) == false) {
										adGroupsNotInSync.Add((string)row["list_item"]);
									}
								}
							}
						}

						//----------------
						//
						if (GroupsModeInUse) {
							//getting azure users to groups
							string columnSql =
								"SELECT TOP 1 item_column_id FROM item_columns WHERE variable='USER_GROUP_USER_ID'";

							DataRow columnRow = db.GetDataRow(columnSql);
							int columnId = 0;
							if (columnRow != null) {
								columnId = (int)columnRow["item_column_id"];
							}

							SetDBParam(SqlDbType.Int, "@columnId", columnId);

							string sql =
								"SELECT  gr.item_id AS list_item_id,gr.list_code,selection.item_column_id,groups.item_id AS group_id,groups.usergroup_name,users.item_id AS user_id, users.login, users.domain,users.integration_update " +
								"FROM  _" + instance + "_list_azure_ad_groups  gr " +
								"INNER JOIN item_data_process_selections selection ON selection.selected_item_id = gr.item_id " +
								"INNER JOIN _" + instance + "_user_group groups ON groups.item_id=selection.item_id " +
								"INNER JOIN item_data_process_selections userSelection ON userSelection.item_id = selection.item_id " +
								"LEFT JOIN _" + instance +
								"_user users ON users.item_id = userSelection.selected_item_id " +
								"WHERE gr.list_domain='" + domain + "'";

							DataTable localAdGroupData = db.GetDatatable(sql, DBParameters);

							Dictionary<string, string> localAdGroupDataDic = new Dictionary<string, string>();

							foreach (DataRow row in localAdGroupData.Rows) {
								if (Conv.ToInt(row["user_id"]) > 0 &&
									localAdGroupDataDic.ContainsKey(row["group_id"] + "|" + row["user_id"]) == false) {
									localAdGroupDataDic.Add(row["group_id"] + "|" + row["user_id"], "");
								}
							}

							List<string> AdList = new List<string>();
							Dictionary<int, List<string>> groupUserDictionary = new Dictionary<int, List<string>>();
							Dictionary<string, List<string>> usersPerADCode = new Dictionary<string, List<string>>();

							foreach (DataRow row in localAdGroupData.Select("group_id is not NULL")) {
								if (AdList.Contains(Conv.ToStr(row["list_code"])) == false) {
									AdList.Add(Conv.ToStr(row["list_code"]));
									usersPerADCode.Add(Conv.ToStr(row["list_code"]), new List<string>());
								}

								if (groupUserDictionary.ContainsKey((int)row["group_id"]) == false) {
									groupUserDictionary.Add((int)row["group_id"], new List<string>());
								}
							}

							List<string> importedLogins = new List<string>();
							var loginsAdded = new List<string>();
							foreach (string code in AdList) {
								JObject resultInitial = new JObject();
								List<string> groupUsed = new List<string>();

								List<string> requestColumnsForQuary = new List<string>();
								foreach (string str in fields) {
									if (str.Contains("extensionAttribute") && requestColumnsForQuary.Contains("onPremisesExtensionAttributes") == false) {
										requestColumnsForQuary.Add("onPremisesExtensionAttributes");
									}
									else {
										requestColumnsForQuary.Add(str);
									}
								}

								JObject groupUsers = GetAzureGroupsUsers(accessToken, code, groupUsed,
									requestColumnsForQuary);
								var allowedGroupUsers = new List<JToken>();
								//--------------------------------------

								if (groupUsers["error"] == null) {
									foreach (var item in groupUsers["value"]) {
										if (Conv.ToStr(item["@odata.type"]) == "#microsoft.graph.user" &&
											Conv.ToBool(item["accountEnabled"])) {
                                            if (((JObject)item).ContainsKey("onPremisesExtensionAttributes")) {
												foreach(string attr in extensionAttributes) {
                                                    if (((JObject)item["onPremisesExtensionAttributes"]).ContainsKey(attr)) {
														((JObject)item).Add(attr, ((JObject)item["onPremisesExtensionAttributes"])[attr]);
													}
                                                }
                                            }
											allowedGroupUsers.Add(item);
										}
									}

									//allowedGroupUsers.RemoveRange(0, 40);
									if (GetAllUsersModeInUse == false && canSaveUsers) {
										foreach (var item in allowedGroupUsers) {
											importedLogins.Add(item[loginRemoteAlias].ToString().Replace("'", "''"));
											usersRead = true;
											var dict = new Dictionary<string, object>();
											if (userData.Select(
												"login='" + item[loginRemoteAlias].ToString().Replace("'", "''") +
												"' AND domain='" + domain + "'").Length > 0) {
												DataRow[] userRow = userData.Select(
													"login='" + item[loginRemoteAlias].ToString().Replace("'", "''") +
													"' AND domain='" + domain + "' AND integration_update=1");
												if (userRow.Length > 0) {
													foreach (string columnName in requestColumns) {
														foreach (string fieldName in mappings[columnName]) {
															if (Conv.ToStr(userRow[0][fieldName]).Split(";")
																.ToList<string>().ConvertAll(s => s.ToUpper())
																.Contains(Conv.ToStr(item[columnName]).Trim()
																	.ToUpper()) == false) {
																if (insertableList.ContainsKey(fieldName) == false) {
																	dict.Add(fieldName, (string)item[columnName]);
																}
																else {
																	var listItems = listStuff[fieldName];
																	foreach (string val in Conv.ToStr(item[columnName])
																		.Split(";")) {
																		if (listItems.ContainsKey(val.Trim().ToUpper())
																		) {
																			if (dict.ContainsKey(fieldName) == false) {
																				dict.Add(fieldName,
																					new List<int>() {
																						listItems[val.Trim().ToUpper()]
																					});
																			}
																			else {
																				((List<int>)dict[fieldName]).Add(
																					Conv.ToInt(listItems[
																						val.Trim().ToUpper()]));
																			}
																		}
																		else if (insertableList[fieldName]) {
																			var currentList = new ItemBase(instance,
																				listNameByColumn[fieldName], session);
																			var listDict =
																				new Dictionary<string, object>();
																			listDict.Add("list_item",
																				Conv.ToStr(item[columnName]).Trim());
																			listDict.Add("in_use", 1);
																			APISaveResult saveListData =
																				new APISaveResult();
																			saveListData.Data = listDict;
																			var resultNew =
																				currentList.AddRow(data: saveListData,
																					process: listNameByColumn[
																						fieldName]);
																			listStuff[fieldName]
																				.Add(
																					Conv.ToStr(resultNew.Data[
																						"list_item"]).ToUpper(),
																					Conv.ToInt(
																						resultNew.Data["item_id"]));
																			listHelper[fieldName]
																				.Add(
																					Conv.ToInt(
																						resultNew.Data["item_id"]),
																					Conv.ToStr(resultNew.Data[
																						"list_item"]));

																			if (dict.ContainsKey(fieldName) == false) {
																				dict.Add(fieldName,
																					new List<int>() {
																						Conv.ToInt(resultNew.Data[
																							"item_id"])
																					});
																			}
																			else {
																				((List<int>)dict[fieldName]).Add(
																					Conv.ToInt(
																						resultNew.Data["item_id"]));
																			}
																		}
																	}
																}
															}
														}

														if (userRow[0]["active"].ToString() == "0" &&
															dict.ContainsKey("active") == false) {
															dict.Add("active", 1);
															usersReActivated += 1;
														}
													}

													if (dict.Count > 0) {
														APISaveResult saveData = new APISaveResult();
														saveData.Data = dict;
														users.SaveItem(dict, (int)userRow[0]["item_id"], false);
														userInfoChanged += 1;
													}
												}
											}
											else {
												if (loginsAdded.Contains(item[loginRemoteAlias].ToString()) == false) {
													foreach (string columnName in requestColumns) {
														foreach (string fieldName in mappings[columnName]) {
															if (mappings.ContainsKey(columnName) &&
																dict.ContainsKey(fieldName) == false) {
																if (insertableList.ContainsKey(fieldName) == false) {
																	dict.Add(fieldName, (string)item[columnName]);
																}
																else {
																	var listItems = listStuff[fieldName];
																	foreach (string val in Conv.ToStr(item[columnName])
																		.Split(";")) {
																		if (listItems.ContainsKey(val.Trim().ToUpper())) {
																			if (dict.ContainsKey(fieldName) == false) {
																				dict.Add(fieldName,
																					new List<int>()
																						{listItems[val.Trim().ToUpper()]});
																			}
																			else {
																				((List<int>)dict[fieldName]).Add(
																					Conv.ToInt(listItems[
																						val.Trim().ToUpper()]));
																			}
																		}
																		else if (insertableList[fieldName]) {
																			var currentList = new ItemBase(instance,
																				listNameByColumn[fieldName], session);
																			var listDict = new Dictionary<string, object>();
																			listDict.Add("list_item",
																				Conv.ToStr(item[columnName]).Trim());
																			listDict.Add("in_use", 1);
																			APISaveResult saveListData =
																				new APISaveResult();
																			saveListData.Data = listDict;
																			var resultNew =
																				currentList.AddRow(data: saveListData,
																					process: listNameByColumn[fieldName]);
																			listStuff[fieldName]
																				.Add(
																					Conv.ToStr(resultNew.Data["list_item"])
																						.ToUpper(),
																					Conv.ToInt(resultNew.Data["item_id"]));
																			listHelper[fieldName]
																				.Add(Conv.ToInt(resultNew.Data["item_id"]),
																					Conv.ToStr(resultNew.Data[
																						"list_item"]));

																			if (dict.ContainsKey(fieldName) == false) {
																				dict.Add(fieldName,
																					new List<int>() {
																					Conv.ToInt(
																						resultNew.Data["item_id"])
																					});
																			}
																			else {
																				((List<int>)dict[fieldName]).Add(
																					Conv.ToInt(resultNew.Data["item_id"]));
																			}
																		}
																	}
																}
															}
														}
													}

													dict.Add("active", 1);
													dict.Add("domain", domain);
													dict.Add("integration_update", 1);

													if (dict.ContainsKey("timezone") == false) {
														dict.Add("timezone", Conv.ToStr(instanceRow["default_timezone"]));
													}

													APISaveResult saveData = new APISaveResult();
													saveData.Data = dict;
													var result = users.AddRow(data: saveData, process: "user");
													loginsAdded.Add(item[loginRemoteAlias].ToString());

													var calendars = new Calendars(instance, session);
													calendars.CreateUserCalendar((int)result.Data["item_id"]);

													addedUsers += 1;
												}
											}
										}
									}
								}
								else {
									var err = groupUsers["error"];
									userReadError = err["code"] + " - " + err["message"];
									usersRead = false;
								}
								//--------------------------------------

								userData = users.GetAllData();
								for (int i = 0; i < userData.Rows.Count; i++) {
									if (userData.Rows[i]["domain"] == System.DBNull.Value)
										userData.Rows[i]["domain"] = "default";
								}

								if (listStuff.Keys.Count > 0) {
									string listDataSql =
										"select idps.item_id,name,STRING_AGG(idps.selected_item_id,',') AS list_item_id from item_columns ic " +
										"LEFT JOIN item_data_process_selections idps ON idps.item_column_id=ic.item_column_id " +
										"where process='user' AND name IN('" +
										String.Join("','", listStuff.Keys.ToList<string>().ToArray()) +
										"') group by idps.item_id,name";
									DataTable lisData = db.GetDatatable(listDataSql);


									foreach (string fieldName in listStuff.Keys.ToList<string>()) {
										userData.Columns.Add(fieldName, typeof(string));

										var fieldSpecific = listHelper[fieldName];

										for (int i = 0; i < userData.Rows.Count; i++) {
											//userData.Rows[i][fieldName] == System.DBNull.Value) userData.Rows[i]["domain"] = "default";
											var li = new List<string>();
											foreach (DataRow row in lisData.Select("item_id=" +
												Conv.ToInt(userData.Rows[i]["item_id"]) + " AND name='" + fieldName +
												"'")) {
												//fieldSpecific[row["list_item_id"]]
												if (Conv.ToInt(row["list_item_id"]) != 0)
													li.Add(fieldSpecific[Conv.ToInt(row["list_item_id"])]);
											}

											userData.Rows[i][fieldName] = String.Join(";", li.ToArray());
										}
									}
								}

								List<string> allowed = usersPerADCode[code];
								foreach (var item in allowedGroupUsers) {
									if (item["userPrincipalName"] != null) {
										DataRow[] user = userData.Select(
											"login='" + Conv.ToStr(item["userPrincipalName"]).Replace("'", "''") +
											"' AND domain='" + domain + "'");
										if (user.Length > 0) {
											if (allowed.Contains(Conv.ToStr(user[0]["item_id"])) == false) {
												allowed.Add(Conv.ToStr(user[0]["item_id"]));
											}
										}
										else {
											if (groupUsersNotFoundFromDatabase.Contains(
												Conv.ToStr(item["userPrincipalName"])) == false) {
												groupUsersNotFoundFromDatabase.Add(
													Conv.ToStr(item["userPrincipalName"]));
											}
										}
									}
								}

								usersPerADCode[code] = allowed;
							}

							// Disable not imported user within this domain - only integration_update=1
							if (GetAllUsersModeInUse == false && canSaveUsers) {
								foreach (DataRow toBeDisabled in userData.Select(
									"login NOT IN('" + String.Join("','", importedLogins.ToArray()) +
									"') AND active=1 AND domain='" +
									domain + "' AND integration_update=1")) {
									var disableDict = new Dictionary<string, object>();
									disableDict.Add("active", 0);
									//APISaveResult disableData = new APISaveResult();
									//disableData.Data = disableDict;
									//users.SaveRow((int)toBeDisabled["item_id"], disableData);
									users.SaveItem(disableDict, (int)toBeDisabled["item_id"], false);
									usersDisabled += 1;
								}
							}

							foreach (int group_id in groupUserDictionary.Keys) {
								List<string> temp = new List<string>();
								List<string> usersInGroup = new List<string>();
								List<string> userToBeAdded = new List<string>();
								foreach (DataRow row in localAdGroupData.Select("group_id=" + group_id)) {
									// add list domain to main localAdGroupData quary
									if (temp.Contains(Conv.ToStr(row["list_code"])) == false) {
										temp.Add(Conv.ToStr(row["list_code"]));
									}
								}

								foreach (string code in temp) {
									foreach (string user_id in usersPerADCode[code]) {
										if (usersInGroup.Contains(user_id) == false) {
											usersInGroup.Add(user_id);
										}

										if (localAdGroupDataDic.ContainsKey(group_id + "|" + user_id) == false) {
											usersAddedTogroup += 1;
											SetDBParam(SqlDbType.Int, "@user_id", user_id);
											SetDBParam(SqlDbType.Int, "@group_id", group_id);
											string insertSql =
												"INSERT INTO item_data_process_selections (item_id,item_column_id,selected_item_id,archive_userid,archive_start) VALUES (@group_id,@columnId,@user_id,0,GETDATE())";
											db.ExecuteInsertQuery(insertSql, DBParameters);
										}
									}

									//Remove not allowed
									string allowedPart = "";
									if (usersInGroup.Count > 0) {
										allowedPart = "AND user_id NOT IN(" + string.Join(",", usersInGroup.ToArray()) +
													  ")";
									}

									var foundLocalGroups = localAdGroupData.Select("group_id=" + group_id +
										" AND domain='" + domain + "' AND integration_update=1 " + allowedPart);
									foreach (DataRow row2 in foundLocalGroups) {
										UserGroupsHandler.DeleteUserGroups((int)row2["user_id"],
											(int)row2["group_id"]);
										usersRemovedFromGroup += 1;
									}
								}
							}
						}
					}
					catch (Exception ex) {
						errorMsg = ex.ToString();
					}
				}

				var endTime = DateTime.Now;
				//Create log for Azure ad sync for this domain
				string logText = "AzureAdSync - domain:" + domain;

				logText += " || Start time: " + startTime.ToString() + " - End time: " + endTime.ToString();

				if (accessTokenFail) {
					logText += " || Failed to receive access token for " + tenant;
				}

				if (TestAuth == false) {
					if (usersRead == false && userReadError != "") {
						logText += " || Reading users from Azure AD failed: " + userReadError;
					}

					if (groupsRead == false && GroupsModeInUse && groupReadError != "") {
						logText += " || Reading groups from Azure AD failed: " + groupReadError;
					}

					if (addedUsers > 0) {
						affected = true;
						logText += " || Users added: " + addedUsers;
					}

					if (usersDisabled > 0) {
						affected = true;
						logText += " || Users disabled: " + usersDisabled;
					}

					if (userInfoChanged > 0) {
						affected = true;
						logText += " || User info changed: " + userInfoChanged;
					}

					if (usersReActivated > 0) {
						affected = true;
						logText += " || Users reactivated: " + usersReActivated;
					}

					if (adGroupsAdded > 0) {
						affected = true;
						logText += " || Ad groups added: " + adGroupsAdded;
					}

					if (adGroupInfoChanged > 0) {
						affected = true;
						logText += " || Ad groups info changed: " + adGroupInfoChanged;
					}

					if (usersAddedTogroup > 0) {
						logText += " || Users added to AD groups: " + usersAddedTogroup;
					}

					if (usersRemovedFromGroup > 0) {
						affected = true;
						logText += " || Users removed from AD groups: " + usersRemovedFromGroup;
					}

					if (groupUsersNotFoundFromDatabase.Count > 0) {
						logText += " || AD groups users not found in user table: " +
								   string.Join(",", groupUsersNotFoundFromDatabase.ToArray());
					}

					if (adGroupsNotInSync.Count > 0) {
						var shortenedList = new List<string>();
						if (adGroupsNotInSync.Count > 50) {
							for (int i = 0; i < 50; i++) {
								shortenedList.Add(adGroupsNotInSync[i]);
							}
							logText += " || AD groups not in sync: " + string.Join(",", shortenedList.ToArray()) + "...(" + (adGroupsNotInSync.Count - shortenedList.Count) + " more)";

						}
						else {
							logText += " || AD groups not in sync: " + string.Join(",", adGroupsNotInSync.ToArray());
						}
					}
				}

				if (usersRead == false && runOption < 2 && runOption != -1 || errorMsg.Length > 0 || accessTokenFail) {
					logText += " || Azure AD sync failed";
					if (errorMsg.Length > 0) {
						bgBase.WriteToLog("Domain:" + domain + " - AzureAdSync crash message:" + errorMsg);
						failed = 1;
					}
				}
				else {
					if (TestAuth) {
						logText += " || Azure AD authentication key has been received successfully.";
					}

					logText += " || Azure AD sync completed";
				}

				bgBase.WriteToLog(Conv.Escape(logText));
			}

			if (domains.Count == 0) {
				bgBase.WriteToLog("AzureAdSync - No valid domains selected");
				failed = 1;
			}

			if (failed == 1) {
				return BackgroundProcessResult.Outcomes.Failed;
			}
			else if (affected) {
				return BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
			}
			else {
				return BackgroundProcessResult.Outcomes.SuccessfulNotAffected;
			}
		}

		private string GetAzureToken(string clientid, string clientkey, string authUrl) {
			var request = (HttpWebRequest) WebRequest.Create(authUrl);
			request.ContentType = "application/x-www-form-urlencoded";
			request.Method = "POST";
			request.KeepAlive = false;

			string stringData = "client_id=" + clientid +
			                    "&scope=https%3A%2F%2Fgraph.microsoft.com%2F.default&client_secret=" + clientkey +
			                    "&grant_type=client_credentials";
			var data = Encoding.ASCII.GetBytes(stringData);
			request.ContentLength = data.Length;

			using (var stream = request.GetRequestStream()) {
				stream.Write(data, 0, data.Length);
			}

			System.Net.HttpWebResponse response = null;
			try {
				response = (HttpWebResponse) request.GetResponse();
			} catch {
				return "error";
			}

			var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

			JObject responsoStr = JObject.Parse(responseString);

			if (responsoStr["access_token"] != null) {
				return (string) responsoStr["access_token"];
			} else {
				return "error";
			}
		}


		private JObject GetAzureUsers(string accessToken, string searchTerms) {
			var request = (HttpWebRequest) WebRequest.Create("https://graph.microsoft.com/v1.0/users?$select=" +
			                                                 searchTerms + "&$filter=accountEnabled eq true&$top=999");
			request.Method = "GET";
			request.KeepAlive = false;
			request.ContentType = "application/json";
			request.Headers["Authorization"] = accessToken;
			//request.aut

			var response = (HttpWebResponse) request.GetResponse();
			var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

			JObject result = JObject.Parse(responseString);
			if (result["@odata.nextLink"] != null) {
				result = AddNextPagingToData(result, (string) result["@odata.nextLink"], accessToken);
			}


			return result;
		}

		private JObject GetAzureGroups(string accessToken, string groupFilter, string groupFilterLike) {
			string filterPart = "";
			if (groupFilter != "") {
				filterPart += "&$filter=";
				Boolean firstRun = true;
				foreach (string filterN in groupFilter.Split(",", StringSplitOptions.RemoveEmptyEntries)) {
					if (firstRun) {
						filterPart += " startswith(displayName,'" + filterN + "') ";
						firstRun = false;
					} else {
						filterPart += " or startswith(displayName,'" + filterN + "') ";
					}
				}
			}

			var request = (HttpWebRequest) WebRequest.Create(
				"https://graph.microsoft.com/v1.0/groups?$select=id,displayName,description&$top=500" + filterPart);
			request.Method = "GET";
			request.KeepAlive = false;
			request.ContentType = "application/json";
			request.Headers["Authorization"] = accessToken;
			//request.aut

			var response = (HttpWebResponse) request.GetResponse();
			var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

			JObject result = JObject.Parse(responseString);

			if (groupFilterLike != "") {
				result = filterGroupResult(result, groupFilterLike);
			}

			if (result["@odata.nextLink"] != null) {
				result = AddNextPagingToData(result, (string) result["@odata.nextLink"], accessToken, groupFilterLike);
			}

			return result;
		}


		private JObject GetAzureGroupsUsers(string accessToken, string group_code, List<string> groupUsed, List<string> requestColumns) {
			JObject result = new JObject();
			if (groupUsed.Contains(group_code) == false) {
				groupUsed.Add(group_code);

				var request = (HttpWebRequest)WebRequest.Create("https://graph.microsoft.com/v1.0/groups/" +
																 group_code +
																 "/members?$select=" +
																 string.Join(",", requestColumns.ToArray()) +
																 "&$top=999");
				request.Method = "GET";
				request.KeepAlive = false;
				request.ContentType = "application/json";
				request.Headers["Authorization"] = accessToken;
				//request.aut

				var response = (HttpWebResponse)request.GetResponse();
				var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

				result = JObject.Parse(responseString);
				if (result["@odata.nextLink"] != null) {
					result = AddNextPagingToData(result, (string)result["@odata.nextLink"], accessToken);
				}

				JObject loopObject = new JObject(result);
				foreach (var item in loopObject["value"]) {
					if (Conv.ToStr(item["@odata.type"]) == "#microsoft.graph.group") {
						var result2 = GetAzureGroupsUsers(accessToken, Conv.ToStr(item["id"]), groupUsed,
							requestColumns);
						JArray arr = (JArray)result.SelectToken("value");
						if (result2.HasValues) {
							JArray arr2 = (JArray)result2.SelectToken("value");
							foreach (JObject innerData in arr2) {
								arr.Add(innerData);
							}
						}
						result["value"] = arr;
					}
				}
			}
			return result;
		}

		private JObject AddNextPagingToData(JObject data, string nextLink, string accessToken,
			string groupFilterLike = "") {
			JArray arr = (JArray) data.SelectToken("value");
			var request = (HttpWebRequest) WebRequest.Create(nextLink);
			request.Method = "GET";
			request.KeepAlive = false;
			request.ContentType = "application/json";
			request.Headers["Authorization"] = accessToken;

			var response = (HttpWebResponse) request.GetResponse();
			var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
			JObject result = JObject.Parse(responseString);

			if (groupFilterLike != "") {
				result = filterGroupResult(result, groupFilterLike);
			}

			if (result["value"] != null) {
				JArray arr2 = (JArray) result.SelectToken("value");

				foreach (JObject innerData in arr2) {
					arr.Add(innerData);
				}

				result["value"] = arr;

				if (result["@odata.nextLink"] != null) {
					return AddNextPagingToData(result, (string) result["@odata.nextLink"], accessToken,
						groupFilterLike);
				} else {
					return result;
				}
			} else {
				return result;
			}
		}

		private List<Dictionary<string, object>> GetAzureAdLogs(List<string> domains) {
			if (domains.Count > 0) {
				string sql = "";
				if (domains.Count == 1) {
					sql = "SELECT TOP 1 0 AS type,'" + domains[0] +
					      "' AS domain, message,occurence  INTO #temp0 FROM instance_logs WHERE message LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
					      domains[0] + "%' order by instance_log_id DESC";
					sql += " INSERT INTO #temp0 SELECT TOP 1 1 AS type,'" + domains[0] +
					       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync%'  AND message NOT LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
					       domains[0] + "%' order by instance_log_id DESC";
					sql += " SELECT * FROM #temp0";
				} else {
					int count = 0;
					foreach (string domain in domains) {
						if (count == 0) {
							sql = "SELECT TOP 1 0 AS type,'" + domains[0] +
							      "' AS domain, message,occurence  INTO #temp0 FROM instance_logs WHERE message LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							      domains[0] + "%' order by instance_log_id DESC";
							sql += " INSERT INTO #temp0 SELECT TOP 1 1 AS type,'" + domains[0] +
							       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync%'  AND message NOT LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							       domains[0] + "%' order by instance_log_id DESC";
						} else {
							sql += " INSERT INTO #temp0 SELECT TOP 1 0 AS type,'" + domains[0] +
							       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							       domains[0] + "%' order by instance_log_id DESC";
							sql += " INSERT INTO #temp0 SELECT TOP 1 1 AS type,'" + domains[0] +
							       "' AS domain, message,occurence FROM instance_logs WHERE message LIKE '%AzureAdSync%'  AND message NOT LIKE '%AzureAdSync crash message%' AND message LIKE '%domain:" +
							       domains[0] + "%' order by instance_log_id DESC";
						}

						count += 1;
					}

					sql += " SELECT * FROM #temp0";
				}

				var db = new Connections();
				return db.GetDatatableDictionary(sql);
			} else {
				return null;
			}
		}

		private JObject filterGroupResult(JObject result, string groupFilterLike) {
			var jArr = ((JArray) result["value"]);
			for (int i = jArr.Count - 1; i > -1; i--) {
				int hitCount = 0;
				foreach (string filterN in groupFilterLike.Split(",", StringSplitOptions.RemoveEmptyEntries)) {
					if (Conv.ToStr(jArr[i]["displayName"]).ToUpper().Contains(filterN.ToUpper())) {
						hitCount += 1;
					}
				}

				if (hitCount == 0) {
					jArr.RemoveAt(i);
				}
			}

			result["value"] = jArr;
			return result;
		}

		// private void WriteToLog(BackgroundProcessResult.Outcomes op, string message) {
		// 	Diag.Log("?", message, 0, Diag.LogSeverities.SuccessMessage,
		// 		Diag.LogCategories.BackgroundTask, LogId);
		// }
		// private string LogId { get; set; }
		// public enum Outcomes {
		// 	SuccessfulAndAffected = 1,
		// 	SuccessfulNotAffected = 2,
		// 	Failed = 3
		// }
	}
}