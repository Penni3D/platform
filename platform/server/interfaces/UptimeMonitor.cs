using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.calendar;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.features.user;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace Keto5.x.platform.server.interfaces {
	public class UptimeMonitor : ConnectionBase {
		private AuthenticatedSession session;

		public UptimeMonitor(string instance, AuthenticatedSession session) : base(instance, session) {
			this.session = session;
		}

        public dynamic GetUptimeData(Microsoft.AspNetCore.Http.HttpContext context) {
            try {
                string instanceHostName = context.Request.Host.Host;
                SetDBParam(SqlDbType.NVarChar, "@hostName", instanceHostName);
                //DateTime monitorStartTemp = monitorStart ?? DateTime.Now;
                string apiKey = Conv.ToStr(Config.GetValue("Report", "UptimeRobotAPIKey"));

                if (apiKey == "") {
                    Diag.Log(instance, "Uptime monitor - Api key not found", 1, category: Diag.LogCategories.System, severity: Diag.LogSeverities.Error);
                }

                string sql = "SELECT * FROM instance_hosts WHERE host=@hostName";
                DataTable instanceData = db.GetDatatable(sql, DBParameters);
                if (instanceData.Rows.Count == 0) { return null; }
                var monitorId = Conv.ToInt(instanceData.Rows[0]["uptime_monitor_id"]);
                var monitorStart = Conv.ToDateTime(instanceData.Rows[0]["uptime_monitor_start"]);
                DateTime monitorStartH = monitorStart ?? DateTime.Now;

                var monitorStartFixed = new DateTime(monitorStartH.Year, monitorStartH.Month, monitorStartH.Day, monitorStartH.Hour, 0, 0).ToUniversalTime();
                var customUptimeRangeList = new List<string>();

                DateTime temp = DateTime.Now;
                //DateTime monitorStartTime = new DateTime(monitorStartTemp.Year, monitorStartTemp.Month, monitorStartTemp.Day, monitorStartTemp.Hour, 0, 0).ToUniversalTime();
                DateTime startTime = new DateTime(temp.AddDays(-7).Year, temp.AddDays(-7).Month, temp.AddDays(-7).Day).ToUniversalTime();
                //if(startTime < monitorStartTime) { startTime = monitorStartTime; }
                DateTime endTime = new DateTime(temp.Year, temp.Month, temp.Day, temp.Hour, 0, 0).AddHours(1).ToUniversalTime();
                var noDataValues = new List<string>();
                while (true) {
                    var endT = startTime.AddHours(1).AddSeconds(-1);
                    if (monitorStartFixed > startTime) {
                        noDataValues.Add("-100");
                    }
                    else {
                        customUptimeRangeList.Add(ConvertToUnixTimestamp(startTime) + "_" + ConvertToUnixTimestamp(endT));
                    }

                    startTime = startTime.AddHours(1);
                    if (startTime == endTime) { break; }
                }

                DateTime todayStart = new DateTime(temp.Year, temp.Month, temp.Day).ToUniversalTime();
                DateTime todayEnds = todayStart.AddDays(1).AddMilliseconds(-1).ToUniversalTime();

                customUptimeRangeList.Add(ConvertToUnixTimestamp(todayStart) + "_" + ConvertToUnixTimestamp(todayEnds));
                customUptimeRangeList.Add(ConvertToUnixTimestamp(todayStart.AddDays(-30)) + "_" + ConvertToUnixTimestamp(todayStart.AddMilliseconds(-1)));
                customUptimeRangeList.Add(ConvertToUnixTimestamp(todayStart.AddDays(-365)) + "_" + ConvertToUnixTimestamp(todayStart.AddMilliseconds(-1)));

                List<string> monthLabels = new List<string>();

                for (int i = 1; i < 13; i++) {
                    DateTime temp2 = DateTime.Now.AddMonths(-i);
                    int daysInMonth = DateTime.DaysInMonth(temp2.Year, temp2.Month);
                    customUptimeRangeList.Add(ConvertToUnixTimestamp(new DateTime(temp2.Year, temp2.Month,1)) + "_" + ConvertToUnixTimestamp(new DateTime(temp2.Year, temp2.Month, 1).AddDays(daysInMonth).AddMilliseconds(-1)));
                    monthLabels.Add(temp2.Month + "/" + temp2.Year);
                }

                // customUptimeRangeList.Add(ConvertToUnixTimestamp(todayStart.AddDays(-365)) + "_" + ConvertToUnixTimestamp(todayStart.AddMilliseconds(-1)));

                var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://api.uptimerobot.com/v2/getMonitors");

                request.Method = "POST";
                request.KeepAlive = false;
                request.ContentType = "application/x-www-form-urlencoded";
                System.Net.Cache.HttpRequestCachePolicy noCachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                request.CachePolicy = noCachePolicy;
                string stringData = "api_key=" + apiKey + "&format=json&monitors=" + monitorId + "&custom_uptime_ranges=" + String.Join("-", customUptimeRangeList.ToArray());
                var data = Encoding.ASCII.GetBytes(stringData);
                request.ContentLength = data.Length;
                using (var stream = request.GetRequestStream()) {
                    stream.Write(data, 0, data.Length);
                }

                var response = (System.Net.HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var responseObj = JObject.Parse(responseString);

                var monitorToken = responseObj["monitors"];
                string custom_uptime_ranges_str = Conv.ToStr(monitorToken[0]["custom_uptime_ranges"]);

                var valArr = custom_uptime_ranges_str.Split("-");
                string todayPercentage = "0";
                string percentage30 = "0";
                string percentage365 = "0";
                List<string> months = new List<string>();
                dynamic obj = new System.Dynamic.ExpandoObject();

                if (valArr.Length > 2) {
                    // 
                    for (int i = 1; i < 13; i++) {
                        months.Add(valArr[valArr.Length - i]);
                    }
                    todayPercentage = valArr[valArr.Length - 15];
                    percentage30 = valArr[valArr.Length - 14];
                    percentage365 = valArr[valArr.Length - 13];
                    Array.Resize(ref valArr, valArr.Length - 15);
                    obj.failed = false;
                }
                else {
                    obj.failed = true;
                }
                List<string> valList = valArr.ToList<string>();
           
                if (noDataValues.Count > 0) {
                    foreach (var val in noDataValues)
                    {
                        valList.Insert(0, "-100");
                    }
                }

                if (valList.Count < 192) {
                    var remaining = 192 - valList.Count;
                    for (int i = 0; i < remaining; i++)
                    {
                        valList.Add("-100");
                    }
                }

                obj.values = valList;
                obj.today = todayPercentage;
                obj.last_30 = percentage30;
                obj.last_365 = percentage365;
                obj.months = months;
                obj.monthLabels = monthLabels;
                return obj;
            }
            catch(Exception e) {
                dynamic obj = new System.Dynamic.ExpandoObject();
                obj.failed = true;
                Diag.Log(instance, "Uptime monitor - Failed to get data: " + e.ToString(), 1, category: Diag.LogCategories.System, severity: Diag.LogSeverities.Error);
                return obj;
            }
        }
        public int CreateUptimeMonitor(Microsoft.AspNetCore.Http.HttpContext context) {
            string hostName = context.Request.Host.ToString();
            string virtualDirectory = Config.VirtualDirectory;
            string virtualDirectoryPart = "";
            string instanceHostName = context.Request.Host.Host;
            if (virtualDirectory != "/") {
                virtualDirectoryPart = "/" + virtualDirectory;
            }
            //hostName

           string apiKey = Conv.ToStr(Config.GetValue("Report", "UptimeRobotAPIKey"));

            if (apiKey == "") {
                Diag.Log(instance, "Uptime monitor - Api key not found", 1, category: Diag.LogCategories.System, severity: Diag.LogSeverities.Error);
            }

            string fullPingURL = "https://" + hostName + virtualDirectoryPart + "/diagnostics/ping";
            string friendlyName = hostName + virtualDirectoryPart;

            if (hostName.Length > 0) {
                try {
                    var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://api.uptimerobot.com/v2/newMonitor");

                    request.Method = "POST";
                    request.KeepAlive = false;
                    request.ContentType = "application/x-www-form-urlencoded";
                    System.Net.Cache.HttpRequestCachePolicy noCachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                    request.CachePolicy = noCachePolicy;
                    string stringData = "api_key=" + apiKey + "&format=json&type=1&url=" + System.Web.HttpUtility.UrlEncode(fullPingURL) + "&friendly_name=" + System.Web.HttpUtility.UrlEncode(friendlyName) + "&interval=60";
                    var data = Encoding.ASCII.GetBytes(stringData);
                    request.ContentLength = data.Length;
                    using (var stream = request.GetRequestStream()) {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (System.Net.HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    var responseObj = JObject.Parse(responseString);


                    if (Conv.ToStr(responseObj.Value<JToken>("stat")) == "ok") {
                        int monitorId = Conv.ToInt(responseObj.Value<JToken>("monitor")["id"]);
                        SetDBParam(SqlDbType.Int, "@monitorId", monitorId);
                        SetDBParam(SqlDbType.NVarChar, "@hostName", instanceHostName);
                        string sql = "UPDATE instance_hosts set uptime_monitor_id=@monitorId,uptime_monitor_start=getdate() WHERE host=@hostName";
                        var sqlResult = db.ExecuteUpdateQuery(sql, DBParameters);
                        if (sqlResult == 1) {
                            Diag.Log(instance, "Uptime monitor creation - Successfull", 1, category: Diag.LogCategories.System, severity: Diag.LogSeverities.Error);
                        }
                        else {
                            Diag.Log(instance, "Uptime monitor creation failed - Failed to save monitor info", 1, category: Diag.LogCategories.System, severity: Diag.LogSeverities.Error);
                        }

                        return 1;
                    }
                    else {
                        Diag.Log(instance, "Uptime monitor creation failed: " + Conv.ToStr(responseObj.Value<JToken>("error")["message"]), 1, category: Diag.LogCategories.System, severity: Diag.LogSeverities.Error);
                        return 0;
                    }
                }
                catch(Exception e){
                    Diag.Log(instance, "Uptime monitor creation failed:" + e.ToString(), 1, category: Diag.LogCategories.System, severity: Diag.LogSeverities.Error);
                }

            }
            return 0;
        }
        public DateTime? GetMonitorStart(HttpContext context)
        {
            var dict = new Dictionary<string, object>();
            var db = new Connections();
            string instanceHostName = context.Request.Host.Host;
            string sql = "SELECT * FROM instance_hosts WHERE host='" + instanceHostName + "'";
            DataTable dt = db.GetDatatable(sql);
            DateTime? monitorStartTime = null;
            if (dt.Rows.Count > 0)
            {
                monitorStartTime = Conv.ToDateTime(dt.Rows[0]["uptime_monitor_start"]);
            }

            return monitorStartTime;
        }

        public List<Dictionary<string, object>> GetMonitorPings(DateTime? monitorStartTime)
        {
            var startTime365 = DateTime.Now.AddDays(-365);
            var startTime30 = DateTime.Now.AddDays(-30);
            if (startTime365 < monitorStartTime)
            {
                startTime365 = monitorStartTime ?? DateTime.Now;
            }
            if (startTime30 < monitorStartTime)
            {
                startTime30 = monitorStartTime ?? DateTime.Now;
            }

            var db = new Connections();

            //string sql = "SELECT occurence FROM instance_requests WHERE instance='' AND path='ping' AND occurence >= '" + startTime.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";

            string sql = "SELECT * INTO #requestTemp FROM instance_requests WHERE instance = '' AND path = 'ping' AND occurence >= '" + DateTime.Now.AddDays(-7).ToUniversalTime().ToString("yyyy-MM-dd") + "' AND occurence < '" + DateTime.Now.AddDays(1).ToUniversalTime().ToString("yyyy-MM-dd") + "'";
            sql += "SELECT ";
            sql += " (SELECT CAST(COUNT(occurence) AS float) / CAST(datediff(MILLISECOND, Convert(date, GETUTCDATE ()), GETUTCDATE ())/1000/60 AS float) FROM #requestTemp WHERE occurence >=  Convert(date, GETUTCDATE ())) AS today,";
            sql += " (SELECT CAST(COUNT(occurence) AS float) / CAST(datediff(MILLISECOND, '" + startTime30.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff") + "', '" + DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "')/1000/60 AS float) FROM instance_requests WHERE instance = '' AND path = 'ping' AND occurence >= '" + startTime30.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff") + "' AND occurence < '" + DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "') AS last_30,";
            sql += " (SELECT CAST(COUNT(occurence) AS float) / CAST(datediff(MILLISECOND, '" + startTime365.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff") + "', '" + DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "')/1000/60 AS float) FROM instance_requests WHERE instance = '' AND path = 'ping' AND occurence >= '" + startTime365.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff") + "' AND occurence < '" + DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "') AS last_365,";

            var firstDateWithTime = DateTime.Now.AddDays(-7);
            var FirstDate = firstDateWithTime.Date;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 24; j++)
                {

                    //var endDate = FirstDate;
                    //endDate.AddHours(1);
                    if (i == 7 && FirstDate <= DateTime.Now && FirstDate.AddHours(1) > DateTime.Now)
                    {
                        sql += " -1 AS 'value_" + i + "_" + j + "',";
                    }
                    else if (i == 7 && FirstDate.AddHours(1) > DateTime.Now)
                    {
                        sql += " -1 AS 'value_" + i + "_" + j + "',";
                    }
                    else
                    {
                        if (monitorStartTime <= FirstDate)
                        {
                            sql += " (SELECT CAST(COUNT(occurence) AS float) / 60 FROM #requestTemp WHERE occurence BETWEEN '" + FirstDate.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff") + "' AND '" + FirstDate.AddHours(1).ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff") + "') AS 'value_" + i + "_" + j + "',";
                        }
                        else
                        {
                            sql += " -1 AS 'value_" + i + "_" + j + "',";
                        }
                    }

                    //if(j==23) FirstDate = FirstDate.AddHours(1);
                    FirstDate = FirstDate.AddHours(1);
                }
            }
            sql = sql.Remove(sql.Length - 1);
            return db.GetDatatableDictionary(sql);
        }
        private double ConvertToUnixTimestamp(DateTime date) {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }
    }
}