﻿using System;
using System.Data;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.calendar;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.actions;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.core.resourceManagement;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.interfaces {
	[Route("integration")]
	public class Integration : IntegrationBase {
		// GET: model/values
		[HttpGet("{type}")]
		public Dictionary<string, object> Get(int type) {
			var p = new Processes(instance, currentSession);

			var retval = new Dictionary<string, object>();

			var columns = new Dictionary<string, List<ItemColumn>>();

			var processes = new Dictionary<string, object>();
			foreach (var pp in p.GetProcesses()) {
				processes.Add(pp.ProcessName, pp);
				var ic = new ItemColumns(instance, pp.ProcessName, currentSession);
				columns.Add(pp.ProcessName, ic.GetItemColumns(onlyIntegrationRestColumns: true));
			}
			foreach (var pp in p.GetListProcesses()) {
				processes.Add(pp.ProcessName, pp);
				var ic = new ItemColumns(instance, pp.ProcessName, currentSession);
				columns.Add(pp.ProcessName, ic.GetItemColumns(onlyIntegrationRestColumns: true));
			}

			retval.Add("processes", processes);

			retval.Add("columns", columns);

			if (type == 1) {
				var data = new Dictionary<string, List<Dictionary<string, object>>>();
				foreach (var pp in processes.Keys) {
					var ib = new ItemCollectionService(instance, pp, currentSession);
					data.Add(pp, ib.GetItems("0=0"));
				}

				retval.Add("data", data);
				retval.Add("item_subitems", p.GetParentItems());
			}

			return retval;
		}

		[HttpGet("remoteVersions")]
		public List<KeyValuePair<string, string>> GetAllRemoteApplications() {
			var mies = new List<KeyValuePair<string, string>>();
			mies.Add(new KeyValuePair<string, string>("j", "version"));
			return mies;
		}

		[HttpPost("{type}")]
		public Dictionary<string, object> Post(int type, [FromBody] Dictionary<string, object> specs) {
			if (specs == null) {
				throw new Exception("Invalid JSON.");
			}

			var p = new Processes(instance, currentSession);
			var retval = new Dictionary<string, object>();
			var columns = new Dictionary<string, List<ItemColumn>>();
			var processes = new Dictionary<string, object>();

			if (specs.ContainsKey("processes")) {
				foreach (var pp in JsonConvert.DeserializeObject<List<string>>(specs["processes"].ToString())) {
					processes.Add(pp, p.GetProcess(pp));
					var ic = new ItemColumns(instance, pp, currentSession);
					columns.Add(pp, ic.GetItemColumns(onlyIntegrationRestColumns: true));
				}
			}
			else {
				foreach (var pp in p.GetProcesses()) {
					processes.Add(pp.ProcessName, pp);
					var ic = new ItemColumns(instance, pp.ProcessName, currentSession);
					columns.Add(pp.ProcessName, ic.GetItemColumns(onlyIntegrationRestColumns: true));
				}
				foreach (var pp in p.GetListProcesses()) {
					processes.Add(pp.ProcessName, pp);
					var ic = new ItemColumns(instance, pp.ProcessName, currentSession);
					columns.Add(pp.ProcessName, ic.GetItemColumns(onlyIntegrationRestColumns: true));
				}
			}

			retval.Add("processes", processes);

			retval.Add("columns", columns);

			if (type == 1) {
				var idList = new List<int>();
				var data = new Dictionary<string, List<Dictionary<string, object>>>();
				foreach (var pp in processes.Keys) {
					var ib = new ItemCollectionService(instance, pp, currentSession);
					List<Dictionary<string, object>> items;
					items = ib.GetItems("0=0");

					foreach (var i in items) {
						idList.Add(Conv.ToInt(i["item_id"]));
					}

					data.Add(pp, items);
				}

				var idStr = string.Join(',', idList.ToArray());
				retval.Add("data", data);
				retval.Add("item_subitems", p.GetParentItems(idStr));
			}

			return retval;
		}

		public string HandeWhereArr(List<List<object>> arr, Dictionary<string, ItemColumn> cols) {
			var sql = "";
			var parenthesisOpen = false;

			foreach (var whereArr in arr) {
				if (whereArr.Count == 5 && Conv.ToStr(whereArr[0]) != "PORTFOLIO" && (cols.ContainsKey(Conv.ToStr(whereArr[1])) ||
											Conv.ToStr(whereArr[1]).Equals("item_id"))) {
					var command = Conv.ToStr(whereArr[0]).ToUpper().Trim();
					var oper = VerifyOperator(Conv.ToStr(whereArr[2]).ToUpper().Trim());

					if (command == "GROUP") {
						parenthesisOpen = true;
						sql += " ( ";
					}

					if (command == "GROUP NOT") {
						parenthesisOpen = true;
						sql += " NOT ( ";
					}

					ItemColumn col = null;
					var colName = "";
					if (Conv.ToStr(whereArr[1]).Equals("item_id")) {
						colName = "item_id";
					}
					else {
						col = cols[Conv.ToStr(whereArr[1])];
						colName = col.Name;
					}

					switch (command) {
						case "AND":
							sql += " AND ";
							break;
						case "OR":
							sql += " OR ";
							break;
						case "GROUP AND":
							if (parenthesisOpen) {
								sql += " ) ";
							}

							parenthesisOpen = true;
							sql += " AND ( ";
							break;
						case "GROUP OR":
							if (parenthesisOpen) {
								sql += " ) ";
							}

							parenthesisOpen = true;
							sql += " OR ( ";
							break;
						case "GROUP AND NOT":
							if (parenthesisOpen) {
								sql += " ) ";
							}

							parenthesisOpen = true;
							sql += " AND NOT ( ";
							break;
						case "GROUP OR NOT":
							if (parenthesisOpen) {
								sql += " ) ";
							}

							parenthesisOpen = true;
							sql += " OR NOT ( ";
							break;
					}

					var datatype = GetDataType(colName, col);

					object[] vals = null;
					var idsStr = "";
					if (oper == "IN" || oper == "NOT IN") {
						vals = JsonConvert.DeserializeObject<object[]>(whereArr[4].ToString());
						if (vals.Length > 0) {
							idsStr = "";
							var first = true;
							foreach (var o in vals) {
								if (!first) {
									idsStr += ",";
								}
								else {
									first = false;
								}

								idsStr += VerifyField(o, datatype);
							}
						}
					}

					if (Conv.ToStr(whereArr[3]) == "col" && !cols.ContainsKey(whereArr[4].ToString())) {
						throw new Exception("Column '" + whereArr[4].ToString() + "' not found in process/list '" +
											col.Process + "'.");
					}

					if (colName == "item_id" || col.IsNvarChar() || col.IsText() || col.IsDate() || col.IsDateTime() ||
						col.IsInt() || col.IsFloat()) {
						if ((oper == "IN" || oper == "NOT IN") && vals != null && vals.Length > 0) {
							sql += colName + " " + oper + "(" + idsStr + ") ";
						}
						else if (Conv.ToStr(whereArr[3]) == "col") {
							sql += colName + " " + oper + " " + whereArr[4] + " ";
						}
						else if (whereArr[4] == null) {
							sql += colName + " " + oper + " NULL ";
						}
						else {
							sql += colName + " " + oper + " " + VerifyField(whereArr[4], datatype) + " ";
						}
					}
					else if (colName != "item_id" && (col.IsProcess() || col.IsList())) {
						if ((oper == "IN" || oper == "NOT IN") && vals != null && vals.Length > 0) {
							var opr = "=";
							if (oper == "IN") opr = ">";
							sql +=
								" (SELECT COUNT(idps.item_id) FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " +
								col.ItemColumnId + " AND idps.selected_item_id IN (" + idsStr + ")) " + opr + " 0 ";
						}
						else if ((oper == "IS" || oper == "IS NOT") && whereArr[4] == null) {
							sql +=
								" (SELECT MAX(idps.item_id) FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " +
								col.ItemColumnId + " ) " + oper + " NULL ";
						}
						else if ((oper == "=" || oper == "<>") &&
								 Conv.ToInt(whereArr[4]) == Conv.ToDouble(whereArr[4])) {
							var opr = "=";
							if (oper == "=") opr = ">";
							sql +=
								" (SELECT COUNT(idps.item_id) FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " +
								col.ItemColumnId + " AND idps.selected_item_id = " + VerifyNumber(whereArr[4]) + ") " +
								opr + " 0 ";
						}
						else {
							throw new Exception("Invalid parameters for process/list.");
						}
					}
				}
			}

			if (parenthesisOpen) sql += " ) ";
			return sql;
		}

		private string VerifyOperator(string oper) {
			switch (oper) {
				case "=":
				case "<":
				case ">":
				case "<=":
				case ">=":
				case "<>":
				case "LIKE":
				case "NOT LIKE":
				case "IN":
				case "NOT IN":
				case "IS":
				case "IS NOT":
					oper = oper.ToUpper().Trim();
					break;
				default:
					throw new Exception("Invalid operator: '" + oper.Trim() +
										"'. Valid operators are (case insensitive) =, <, >, <=, >=, LIKE, NOT LIKE, IN, NOT IN, IS and IS NOT.");
			}

			return oper;
		}

		private ItemColumn.ColumnType GetDataType(string name, ItemColumn col) {
			if (name == "item_id") {
				return ItemColumn.ColumnType.Int;
			}
			else {
				return (ItemColumn.ColumnType)col.DataType;
			}
		}

		private string VerifyField(object value, ItemColumn.ColumnType type) {
			string result = null;

			if (value != null) {
				switch (type) {
					case ItemColumn.ColumnType.Text:
					case ItemColumn.ColumnType.Nvarchar:
						result = "'" + VerifyString(value) + "'";
						break;

					case ItemColumn.ColumnType.Int:
					case ItemColumn.ColumnType.Float:
					case ItemColumn.ColumnType.Process:
					case ItemColumn.ColumnType.List:
						result = VerifyNumber(value);
						break;

					case ItemColumn.ColumnType.Date:
						result = "'" + VerifyDate(value) + "'";
						break;

					case ItemColumn.ColumnType.Datetime:
						result = "'" + VerifyDateTime(value) + "'";
						break;

					default:
						throw new Exception("Invalid value: '" + value.ToString() + "', " + type.ToString() + ".");
				}
			}

			return result;
		}

		private string VerifyString(object value) {
			return Conv.ToStr(value).Replace("'", "''");
		}

		private string VerifyNumber(object value) {
			double number;
			if (double.TryParse(Conv.ToStr(value), out number)) {
				return Conv.ToStr(value);
			}
			else {
				throw new Exception("Invalid number: '" + value + "'.");
			}
		}

		private string VerifyDate(object value) {
			DateTime d = (DateTime)Conv.ToDateTime(value);
			if (d.Year > 1900) {
				return DateTimeToSQL(d, true);
			}
			else {
				throw new Exception("Invalid date: '" + value + "'.");
			}
		}

		private string VerifyDateTime(object value) {
			DateTime d = (DateTime)Conv.ToDateTime(value);
			if (d.Year > 1900) {
				return DateTimeToSQL(d);
			}
			else {
				throw new Exception("Invalid datetime: '" + value + "'.");
			}
		}

		private string DateTimeToSQL(DateTime value, bool StripTime = false) {
			string Year = Conv.ToStr(value.Year);
			string Month = Conv.ToStr(value.Month);
			string Day = Conv.ToStr(value.Day);
			string hour = Conv.ToStr(value.Hour);
			string minutes = Conv.ToStr(value.Minute);
			string seconds = Conv.ToStr(value.Second);
			string mseconds = Conv.ToStr(value.Millisecond);

			if (Month.Length == 1)
				Month = "0" + Month;
			if (Day.Length == 1)
				Day = "0" + Day;
			if (hour.Length == 1)
				hour = "0" + hour;
			if (minutes.Length == 1)
				minutes = "0" + minutes;
			if (seconds.Length == 1)
				seconds = "0" + seconds;

			if (Year.Length == 3)
				Year = "0" + Year;
			if (Year.Length == 2) {
				if (Conv.ToInt(Year) > 40)
					Year = "19" + Year;
				else
					Year = "20" + Year;
			}

			if (StripTime)
				return Year + "-" + Month + "-" + Day;
			else
				return Year + "-" + Month + "-" + Day + " " + hour + ":" + minutes + ":" + seconds + "." + mseconds;
		}

		private int ParsePortfolioId(string process, List<List<object>> arr) {
			foreach (var whereArr in arr) {
				if (whereArr.Count >= 2 && Conv.ToStr(whereArr[0]) == "PORTFOLIO") {
					var cb = new ConnectionBase(instance, currentSession);
					cb.SetDBParam(SqlDbType.Int, "@process_portfolio_id", Conv.ToInt(whereArr[1]));
					cb.SetDBParam(SqlDbType.NVarChar, "@process", process);
					if (Conv.ToInt(cb.db.ExecuteScalar("SELECT process_portfolio_id FROM process_portfolios WHERE process = @process AND process_portfolio_id = @process_portfolio_id", cb.DBParameters)) > 0)
						return Conv.ToInt(whereArr[1]);
				}
			}
			return 0;
		}

		[HttpPost("WHERE")]
		public Dictionary<string, object> PostWithWhere([FromBody] Dictionary<string, object> specs) {
			if (specs == null) {
				throw new Exception("Invalid JSON.");
			}

			var p = new Processes(instance, currentSession);
			var retval = new Dictionary<string, object>();
			var columns = new Dictionary<string, List<ItemColumn>>();
			var processes = new Dictionary<string, object>();
			var sqlWheres = new Dictionary<string, string>();
			var portfolio_ids = new Dictionary<string, int>();

			if (specs.ContainsKey("processes")) {
				var processesJson =
					JsonConvert.DeserializeObject<Dictionary<string, List<List<object>>>>(specs["processes"]
						.ToString());
				foreach (var pp in processesJson.Keys) {
					var ic = new ItemColumns(instance, pp, currentSession);

					sqlWheres.Add(pp, HandeWhereArr(processesJson[pp], ic.GetItemColumnsDictionary()));

					portfolio_ids.Add(pp, ParsePortfolioId(pp, processesJson[pp]));

					processes.Add(pp, p.GetProcess(pp));
					columns.Add(pp, ic.GetItemColumns(onlyIntegrationRestColumns: true));
				}
			}
			else {
				foreach (var pp in p.GetProcesses()) {
					processes.Add(pp.ProcessName, pp);
					var ic = new ItemColumns(instance, pp.ProcessName, currentSession);
					columns.Add(pp.ProcessName, ic.GetItemColumns(onlyIntegrationRestColumns: true));
				}
				foreach (var pp in p.GetListProcesses()) {
					processes.Add(pp.ProcessName, pp);
					var ic = new ItemColumns(instance, pp.ProcessName, currentSession);
					columns.Add(pp.ProcessName, ic.GetItemColumns(onlyIntegrationRestColumns: true));
				}
			}

			retval.Add("processes", processes);

			retval.Add("columns", columns);

			var idList = new List<int>();
			var data = new Dictionary<string, List<Dictionary<string, object>>>();
			foreach (var pp in processes.Keys) {
				var ib = new ItemCollectionService(instance, pp, currentSession);
				List<Dictionary<string, object>> items;
				if (sqlWheres.ContainsKey(pp) && sqlWheres[pp] != "") {
					if (portfolio_ids.ContainsKey(pp) && portfolio_ids[pp] > 0) {
						ib.FillItems(true, false, portfolio_ids[pp], sqlWheres[pp]);
						items = ib.GetItems();
					}
					else
						items = ib.GetItems(sqlWheres[pp]);
				}
				else {
					if (portfolio_ids.ContainsKey(pp) && portfolio_ids[pp] > 0) {
						ib.FillItems(true, false, portfolio_ids[pp]);
						items = ib.GetItems();
					}
					else
						items = ib.GetItems("0=0");
				}

				foreach (var i in items) {
					idList.Add(Conv.ToInt(i["item_id"]));
				}

				data.Add(pp, items);
			}

			var idStr = string.Join(',', idList.ToArray());
			retval.Add("data", data);
			retval.Add("item_subitems", p.GetParentItems(idStr));

			return retval;
		}

		[HttpPost("")]
		public Dictionary<string, List<Dictionary<string, object>>> InsertOrUpdate([FromBody] Dictionary<string, object> da) {
			return DoInsertOrUpdate(da, true);
		}
		[HttpPost("insert_or_update")]
		public Dictionary<string, List<Dictionary<string, object>>> InsertOrUpdateWithActions([FromBody] Dictionary<string, object> da) {
			return DoInsertOrUpdate(da, true);
		}
		[HttpPost("insert_or_update_no_actions")]
		public Dictionary<string, List<Dictionary<string, object>>> InsertOrUpdateNoActions([FromBody] Dictionary<string, object> da) {
			return DoInsertOrUpdate(da, false);
		}
		private Dictionary<string, List<Dictionary<string, object>>> DoInsertOrUpdate(Dictionary<string, object> da, bool default_do_insert_actions) {
			if (da == null) throw new Exception("Invalid JSON.");

			var data = JsonConvert.DeserializeObject<Dictionary<string, List<Dictionary<string, object>>>>(da["data"].ToString().Replace((char)11, ' '));

			Dictionary<string, string> migration_block = null;
			if (da.ContainsKey("migrations")) migration_block = JsonConvert.DeserializeObject<Dictionary<string, string>>(da["migrations"].ToString());

			Dictionary<string, object> parents = null;
			if (da.ContainsKey("parents")) parents = JsonConvert.DeserializeObject<Dictionary<string, object>>(da["parents"].ToString());

			var default_do_insert_before_action = default_do_insert_actions;
			var default_do_insert_after_action = default_do_insert_actions;

			if (da.ContainsKey("options")) {
				var options = (Newtonsoft.Json.Linq.JObject)da["options"];
				if (options.ContainsKey("do_insert_actions")) {
					default_do_insert_before_action = ((bool)(options["do_insert_actions"]) || (int)(options["do_insert_actions"]) == 1);
					default_do_insert_after_action = default_do_insert_before_action;
				}
				else {
					if (options.ContainsKey("do_insert_before_action"))
						default_do_insert_before_action = ((bool)(options["do_insert_before_action"]) || (int)(options["do_insert_before_action"]) == 1);
					if (options.ContainsKey("do_insert_after_action"))
						default_do_insert_after_action = ((bool)(options["do_insert_after_action"]) || (int)(options["do_insert_after_action"]) == 1);
				}
			}

			if (migration_block != null && migration_block.Keys.Count > 0) {
				// Migration manipulates things already supplied with da["data"] by using GetItemIdByColumn to get another process' item_ids by arbitrary value/field. 
				//   Supply the value to be converted to id in da["data"] field and use migrations to convert.
				// example: { "data" { "project":[  "item_id":30 , "responsible":["admin","jka"]  ] }, "migrations":{"user":"login"}

				foreach (var process_name in data.Keys) {
					var ics = new ItemColumns(instance, process_name, currentSession);
					var iCols = ics.GetItemColumns();
					foreach (var data_item in data[process_name]) {
						foreach (var processName in migration_block.Keys) {
							foreach (var col in iCols) {
								if (!col.IsList() && !col.IsProcess() || col.DataAdditional == null ||
									col.DataAdditional != processName) continue;
								if (!data_item.ContainsKey(col.Name) || data_item[col.Name] == null) continue;

								var ib = new ItemBase(instance, processName, currentSession);
								var ids = new List<int>();
								try {
									var jsonObject = data_item[col.Name].ToString();
									var idsJ = JsonConvert.DeserializeObject<List<string>>(jsonObject);
									foreach (var val in idsJ) {
										var id = ib.GetItemIdByColumn(migration_block[processName], val);
										if (id > 0) {
											ids.Add(id);
										}
									}

									data_item[col.Name] = ids;
								}
								catch (Exception) {
									// ignored
								}
							}
						}
					}
				}
			}

			foreach (var process_name in data.Keys) {
				var newIds = new List<int>();

				foreach (var data_item in data[process_name]) {
					Newtonsoft.Json.Linq.JObject item_options = null;
					if (data_item.ContainsKey("integration_item_options")) {
						item_options = (Newtonsoft.Json.Linq.JObject)data_item["integration_item_options"];
						data_item.Remove("integration_item_options");
					}

					var ib = new ItemBase(instance, process_name, currentSession);
					var processBase = new ProcessBase(instance, process_name, currentSession);
					if (data_item.ContainsKey("item_id")) {
						ib.SaveItem(data_item, checkRights: true, returnSubqueries: false);
					}
					else {
						try {
							bool item_insert_with_before_action = default_do_insert_before_action;
							bool item_insert_with_after_action = default_do_insert_after_action;
							if (item_options != null && item_options.ContainsKey("do_insert_actions")) {
								item_insert_with_before_action = ((bool)(item_options["do_insert_actions"]) == true || (int)(item_options["do_insert_actions"]) == 1);
								item_insert_with_after_action = item_insert_with_before_action;
							}

							int newId = 0;
							if (item_insert_with_before_action)
								newId = processBase.InsertItemRow();
							else
								newId = processBase.InsertItemRowNoAction();

							data_item.Add("item_id", newId);
							newIds.Add(newId);
							ib.SaveItem(data_item, checkRights: true, returnSubqueries: false, followsInsert: true);
							if (process_name == "user") {
								var calendars = new Calendars(instance, currentSession);
								calendars.CreateUserCalendar(newId);
							}

							if (item_insert_with_after_action) processBase.DoNewRowAction(newId, true);
						}
						catch (Exception) {
							//ib = new ItemBase(instance, p, currentSession);	
						}
					}
				}

				if (parents != null && parents.ContainsKey(process_name)) {
					dynamic pp = parents[process_name];
					var ib = new ItemBase(instance, process_name, currentSession);
					ib.UpdateRecursion((string)pp.new_parent_id, (string)pp.old_parent_id, (string)pp.old_id,
						newIds);
				}
			}

			Cache.Initialize();

			return data;
		}

		[HttpGet("attachment/{attachmentId}")]
		public void GetAttachment(int attachmentId) {
			AttachmentsDownload attachments = new AttachmentsDownload(currentSession);
			AttachmentData a = attachments.DownloadAttachment(attachmentId, 0);
			Response.ContentType = a.ContentType;
			Response.Headers.Add("content-Disposition", "inline;filename=\"" + System.Uri.EscapeDataString(a.Filename) + "\";name=\"" + System.Uri.EscapeDataString(a.Name) + "\"");
			Response.ContentLength = a.Filedata.Length;
			Response.Body.WriteAsync(a.Filedata, 0, a.Filedata.Length);
			Response.Body.Close();
		}

		[HttpPost("attachment/{attachmentId}")]
		public int SetAttachment(int attachmentId, [FromBody] Dictionary<string, object> attachment_params) {
			Attachments attachments = new Attachments(instance, currentSession);
			if (attachmentId == 0) {
				if (attachment_params.ContainsKey("item_id") && attachment_params.ContainsKey("item_column_id") && Convert.ToInt32(attachment_params["item_id"]) > 0 && Convert.ToInt32(attachment_params["item_column_id"]) > 0) {
					attachmentId = attachments.SaveAttachment(Convert.FromBase64String(Convert.ToString(attachment_params["filedata"])), Convert.ToString(attachment_params["filename"]), Convert.ToString(attachment_params["content_type"]), Convert.ToInt32(attachment_params["item_id"]), Convert.ToInt32(attachment_params["item_column_id"]), Convert.ToString(attachment_params["name"]));
				}
				else if (attachment_params.ContainsKey("parent_id") && attachment_params.ContainsKey("control_name") && Convert.ToInt32(attachment_params["parent_id"]) > 0 && Convert.ToString(attachment_params["control_name"]).Length > 0) {
					attachmentId = attachments.SaveAttachment(Convert.FromBase64String(Convert.ToString(attachment_params["filedata"])), Convert.ToString(attachment_params["filename"]), Convert.ToString(attachment_params["content_type"]), Convert.ToInt32(attachment_params["parent_id"]), Convert.ToString(attachment_params["control_name"]), Convert.ToString(attachment_params["name"]));
				}
			}
			else {
				attachments.SaveAttachment(Convert.FromBase64String(Convert.ToString(attachment_params["filedata"])), Convert.ToString(attachment_params["filename"]), Convert.ToString(attachment_params["content_type"]), attachmentId, Convert.ToString(attachment_params["name"]));
			}
			return attachmentId;
		}

		[HttpPost("attachment/delete/{attachmentId}")]
		public void DeleteAttachment(int attachmentId) {
			Attachments attachments = new Attachments(instance, currentSession);
			attachments.DeleteAttachment(attachmentId);
		}

		[HttpGet("link/{attachmentId}")]
		public Dictionary<string, string> GetLink(int attachmentId) {
			Attachments attachments = new Attachments(instance, currentSession);
			Attachment a = attachments.GetLink(attachmentId);
			Dictionary<string, string> result = new Dictionary<string, string>();
			result.Add("url", a.Url);
			result.Add("name", a.Name);
			return result;
		}

		[HttpPost("link/{attachmentId}")]
		public int SetLink(int attachmentId, [FromBody] Dictionary<string, object> link_params) {
			Attachments attachments = new Attachments(instance, currentSession);
			if (attachmentId == 0) {
				if (link_params.ContainsKey("item_id") && link_params.ContainsKey("item_column_id") && Convert.ToInt32(link_params["item_id"]) > 0 && Convert.ToInt32(link_params["item_column_id"]) > 0) {
					attachmentId = attachments.NewLink(Convert.ToInt32(link_params["item_id"]), Convert.ToInt32(link_params["item_column_id"]), link_params);
				}
				else if (link_params.ContainsKey("parent_id") && link_params.ContainsKey("control_name") && Convert.ToInt32(link_params["parent_id"]) > 0 && Convert.ToString(link_params["control_name"]).Length > 0) {
					attachmentId = attachments.NewLink(Convert.ToInt32(link_params["parent_id"]), Convert.ToString(link_params["control_name"]), link_params);
				}
			}
			else {
				attachments.UpdateLink(attachmentId, link_params);
			}
			return attachmentId;
		}

		[HttpPost("link/delete/{attachmentId}")]
		public void DeleteLink(int attachmentId) {
			Attachments attachments = new Attachments(instance, currentSession);
			attachments.DeleteLink(attachmentId);
		}

		[HttpPost("delete")]
		public void Delete([FromBody] Dictionary<string, List<int>> processes) {
			foreach (var p in processes.Keys) {
				var ib = new ItemBase(instance, p, currentSession);
				foreach (var id in processes[p]) {
					ib.Delete(id);
				}
			}

			Cache.Initialize();
		}

		[HttpPost("doAction/{process}/{processActionId}/{itemId}")]
		public void DoAction(string process, int processActionId, int itemId) {
			var actions = new ProcessActions(instance, process, currentSession);
			actions.DoAction(processActionId, itemId);
		}

		[HttpPost("doActionForPortfolio/{portfolioid}/{process}/{processActionId}")]
		public void DoActionForPortfolio(int portfolioid, string process, int processActionId) {
			var p = new ItemBase(instance, process, currentSession);
			var o = new PortfolioOptions();
			foreach (var collection in p.FindItems(portfolioid, o)) {
				if (collection.Key == "rowdata") {
					var dt = (System.Data.DataTable)collection.Value;
					foreach (System.Data.DataRow row in dt.Rows) {
						var itemId = Conv.ToInt(row["item_id"]);
						var actions = new ProcessActions(instance, process, currentSession);
						actions.DoAction(processActionId, itemId);
					}
				}
			}
		}

		[HttpPost("log/{severity}")]
		public bool Log(Diag.LogSeverities severity, [FromBody] List<string> messages) {
			if (messages != null) {
				foreach (var message in messages) {
					Diag.Log(instance, message, currentSession.item_id, severity, Diag.LogCategories.Integration);
				}

				return true;
			}

			return false;
		}

		[HttpGet("getVersionList")]
		public List<Diag.ApplicationVersion> GetVersionList() {
			return Diag.GetList();
		}

		[HttpPost("item_subitems")]
		public void PostSubItems([FromBody] List<Dictionary<string, object>> subitems) {
			Processes p = new Processes(instance, currentSession);
			foreach (var subitem in subitems) {
				var process = p.GetProcessByItemId(Conv.ToInt(subitem["parent_item_id"]));
				SubItems si = new SubItems(instance, process, currentSession);
				si.AddSubItem(Conv.ToInt(subitem["parent_item_id"]), Conv.ToInt(subitem["item_id"]), Conv.ToByte(subitem["favourite"]));
			}

			Cache.Initialize();
		}

		[HttpPost("item_subitems/delete")]
		public void DeleteSubItems([FromBody] List<Dictionary<string, object>> subitems) {
			Processes p = new Processes(instance, currentSession);
			foreach (var subitem in subitems) {
				var process = p.GetProcessByItemId(Conv.ToInt(subitem["parent_item_id"]));
				SubItems si = new SubItems(instance, process, currentSession);
				si.DeleteSubItem(Conv.ToInt(subitem["parent_item_id"]), Conv.ToInt(subitem["item_id"]), Conv.ToByte(subitem["favourite"]));
			}

			Cache.Initialize();
		}

		[HttpPost("moveAllocations")]
		public void MoveAllocations([FromBody] List<AllocationShift> allocation_shifts) {
			AllocationService allocation_service = new AllocationService(instance, currentSession, new ResourceManagementRepository(new ConnectionBase(instance, currentSession), new EntityServiceProvider(instance, currentSession)));
			foreach (var allocation_shift in allocation_shifts) {
				allocation_service.MoveAllocation(allocation_shift);
			}

			Cache.Initialize();
		}

		[HttpGet("exportPortfolio/{portfolioId}")]
		public List<Dictionary<string, object>> ExportPortfolio(int portfolioId) {
			var cb = new ConnectionBase(instance, currentSession);
			cb.SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);
			var process = Conv.ToStr(cb.db.ExecuteScalar(
				"SELECT process FROM process_portfolios WHERE process_portfolio_id = @portfolioId", cb.DBParameters));

			if (process.Length <= 0) return new List<Dictionary<string, object>>();

			var ib = new ItemBase(instance, process, currentSession);
			return ib.PortfolioJson(portfolioId);
		}

		[HttpGet("exportPortfolio/{portfolioId}/{perpage}/{page}")]
		public List<Dictionary<string, object>> ExportPortfolio(int portfolioId, int perPage, int page) {
			var cb = new ConnectionBase(instance, currentSession);
			cb.SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);
			var process = Conv.ToStr(cb.db.ExecuteScalar(
				"SELECT process FROM process_portfolios WHERE process_portfolio_id = @portfolioId", cb.DBParameters));

			if (process.Length <= 0) return new List<Dictionary<string, object>>();

			var ib = new ItemBase(instance, process, currentSession);
			return ib.PortfolioJson(portfolioId, new PortfolioOptions { limit = perPage, offset = (page - 1) * perPage });
		}
	}
}