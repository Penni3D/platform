﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.calendar {
	public class Calendar {
		public Calendar() { }

		public Calendar(DataRow row) {
			CalendarId = (int) row["calendar_id"];
			if (row["parent_calendar_id"] != DBNull.Value) {
				ParentCalendarId = (int) row["parent_calendar_id"];
			}

			ItemId = Conv.ToInt(row["item_id"]);
			CalendarName = Conv.ToStr(row["calendar_name"]);
			WorkHours = Convert.ToDouble(row["work_hours"]);
			TimesheetHours = Convert.ToDouble(row["timesheet_hours"]);
			WeeklyHours = Convert.ToDouble(row["weekly_hours"]);
			Def1 = Convert.ToByte(row["def_1"]);
			Def2 = Convert.ToByte(row["def_2"]);
			Def3 = Convert.ToByte(row["def_3"]);
			Def4 = Convert.ToByte(row["def_4"]);
			Def5 = Convert.ToByte(row["def_5"]);
			Def6 = Convert.ToByte(row["def_6"]);
			Def7 = Convert.ToByte(row["def_7"]);
			IsDefault = Convert.ToBoolean(row["is_default"]);
			BaseCalendar = Convert.ToBoolean(row["base_calendar"]);
			UseBaseCalendar = Convert.ToBoolean(row["use_base_calendar"]);
			Description = Conv.ToStr(row["description"]);
		}

		public int CalendarId { get; set; }
		public int? ParentCalendarId { get; set; }
		public int? ItemId { get; set; }
		public string CalendarName { get; set; }
		public double WorkHours { get; set; }
		public double WeeklyHours { get; set; }
		public double TimesheetHours { get; set; }
		public byte Def1 { get; set; }
		public byte Def2 { get; set; }
		public byte Def3 { get; set; }
		public byte Def4 { get; set; }
		public byte Def5 { get; set; }
		public byte Def6 { get; set; }
		public byte Def7 { get; set; }
		public bool IsDefault { get; set; }
		public bool BaseCalendar { get; set; }
		public bool UseBaseCalendar { get; set; }
		public string Description { get; set; }
		public int UserCount { get; set; }
	}

	public class Calendars : ConnectionBase {
		public Calendars(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) { }

		public List<Calendar> GetCalendars() {
			var result = new List<Calendar>();
			foreach (DataRow row in db.GetDatatable(
				"SELECT calendar_id, parent_calendar_id, item_id, work_hours, timesheet_hours, weekly_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, calendar_name, description, use_base_calendar, base_calendar FROM calendars c WHERE instance = @instance",
				DBParameters).Rows) {
				var calendar = new Calendar(row);

				SetDBParam(SqlDbType.Int, "@calendarId", row["calendar_id"]);

				calendar.UserCount =
					(int) db.ExecuteScalar(
						"SELECT COUNT(item_id) FROM calendars WHERE parent_calendar_id = @calendarId", DBParameters);
				result.Add(calendar);
			}

			return result;
		}

		public Calendar GetCalendar(int calendarId) {
			SetDBParam(SqlDbType.Int, "@calendarId", calendarId);
			var dt = db.GetDataRow(
				"SELECT calendar_id, parent_calendar_id, calendar_name, item_id, work_hours, timesheet_hours, weekly_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, description, use_base_calendar, base_calendar FROM calendars c WHERE instance = @instance and calendar_id = @calendarId",
				DBParameters);
			return new Calendar(dt);
		}

		public List<Calendar> GetBaseCalendars() {
			var result = new List<Calendar>();
			foreach (DataRow row in db.GetDatatable(
				"SELECT calendar_id, parent_calendar_id, item_id, work_hours, timesheet_hours, weekly_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, calendar_name, description, use_base_calendar, base_calendar FROM calendars c WHERE instance = @instance AND base_calendar = 1",
				DBParameters).Rows) {
				result.Add(new Calendar(row));
			}

			return result;
		}

		public Calendar GetUsersCalendar(int userItemId) {
			SetDBParam(SqlDbType.Int, "@userItemId", userItemId);
			var dt = db.GetDataRow(
				"SELECT TOP 1 calendar_id, parent_calendar_id, calendar_name, item_id, work_hours, timesheet_hours, weekly_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, description, use_base_calendar, base_calendar FROM calendars c WHERE instance = @instance AND item_id = @userItemId",
				DBParameters);
			return new Calendar(dt);
		}

		public Calendar GetUsersBaseCalendar(int userItemId) {
			SetDBParam(SqlDbType.Int, "@userItemId", userItemId);
			var dt = db.GetDataRow(
				"SELECT TOP 1 calendar_id, parent_calendar_id, calendar_name, item_id, work_hours, timesheet_hours, weekly_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default, description, use_base_calendar, base_calendar FROM calendars c WHERE instance = @instance AND calendar_id = (SELECT parent_calendar_id FROM calendars WHERE item_id = @userItemId)",
				DBParameters);
			return new Calendar(dt);
		}
		
		public Dictionary<string, string> GetTimezones() {
			var timezones = new Dictionary<string, string>();
			foreach (var i in TimeZoneInfo.GetSystemTimeZones()) {
				if (OperatingSystem.IsWindows())
					timezones.Add(i.Id, TimeZoneConverter.TZConvert.WindowsToIana(i.Id));
				else
					timezones.Add(i.Id, i.Id);
			}
			return timezones;
		}

		public void CreateUserCalendar(int userId) {
			SetDBParam(SqlDbType.Int, "@userId", userId);
			db.ExecuteInsertQuery(
				"INSERT INTO calendars (instance, item_id, calendar_name, parent_calendar_id) VALUES(@instance, @userId, 'My calendar', (SELECT calendar_id FROM calendars WHERE is_default = 1 AND base_calendar = 1))",
				DBParameters);
		}

		public Calendar AddCalendar(Calendar calendar) {
			calendar.CalendarId =
				db.ExecuteInsertQuery("INSERT INTO calendars(instance) VALUES(@instance)", DBParameters);
			SaveCalendar(calendar);

			return calendar;
		}

		public void DeleteCalendar(int calendarId) {
			SetDBParam(SqlDbType.NVarChar, "@calendarId", calendarId);
			db.ExecuteDeleteQuery("DELETE FROM calendars WHERE instance = @instance AND calendar_id = @calendarId",
				DBParameters);
		}

		public Calendar SaveCalendar(Calendar calendar) {
			SetDBParam(SqlDbType.Int, "@calendarId", calendar.CalendarId);
			SetDBParam(SqlDbType.Int, "@parentCalendarId", Conv.IfNullThenDbNull(calendar.ParentCalendarId));
			SetDBParam(SqlDbType.Int, "@itemId", Conv.IfNullThenDbNull(calendar.ItemId));
			SetDBParam(SqlDbType.NVarChar, "@calendarName", calendar.CalendarName);
			SetDBParam(SqlDbType.Decimal, "@workHours", calendar.WorkHours);
			SetDBParam(SqlDbType.Decimal, "@weeklyHours", calendar.WeeklyHours);
			SetDBParam(SqlDbType.Decimal, "@timesheetHours", calendar.TimesheetHours);
			SetDBParam(SqlDbType.TinyInt, "@def1", calendar.Def1);
			SetDBParam(SqlDbType.TinyInt, "@def2", calendar.Def2);
			SetDBParam(SqlDbType.TinyInt, "@def3", calendar.Def3);
			SetDBParam(SqlDbType.TinyInt, "@def4", calendar.Def4);
			SetDBParam(SqlDbType.TinyInt, "@def5", calendar.Def5);
			SetDBParam(SqlDbType.TinyInt, "@def6", calendar.Def6);
			SetDBParam(SqlDbType.TinyInt, "@def7", calendar.Def7);
			SetDBParam(SqlDbType.TinyInt, "@isDefault", Convert.ToBoolean(calendar.IsDefault));
			SetDBParam(SqlDbType.TinyInt, "@baseCalendar", Convert.ToBoolean(calendar.BaseCalendar));
			SetDBParam(SqlDbType.TinyInt, "@useBaseCalendar", Convert.ToBoolean(calendar.UseBaseCalendar));
			SetDBParam(SqlDbType.NVarChar, "@description", Conv.IfNullThenDbNull(calendar.Description));
			db.ExecuteUpdateQuery(
				"UPDATE calendars SET work_hours = @workHours, timesheet_hours = @timesheetHours, weekly_hours = @weeklyHours, def_1 = @def1, def_2 = @def2, def_3 = @def3, def_4 = @def4, def_5 = @def5, def_6 = @def6, def_7 = @def7, is_default = @isDefault, calendar_name = @calendarName, base_calendar = @baseCalendar, use_base_calendar = @useBaseCalendar, description = @description, parent_calendar_id = @parentCalendarId WHERE instance = @instance AND calendar_id = @calendarId",
				DBParameters);

			return calendar;
		}

		/// <summary>
		/// Apply calendar changes from the beginning of time until 5 years from today
		/// </summary>
		/// <param name="calendarId">Calendar's indentifier</param>
		public void ApplyCalendar(int calendarId) {
			SetDBParam(SqlDbType.Int, "@calendarId", calendarId);
			SetDBParam(SqlDbType.DateTime, "@s", DBNull.Value);
			db.ExecuteStoredProcedure("EXEC Calendar_ApplyToPastWeekends @calendarId, @s", DBParameters);
			ApplyFutureCalendar(calendarId);
		}

		/// <summary>
		/// Apply calendar changes 5 years from selected date
		/// </summary>
		/// <param name="calendarId">Calendar's indentifier</param>
		/// <param name="startDate">Start Date</param>
		public void ApplyCalendar(int calendarId, DateTime startDate) {
			SetDBParam(SqlDbType.Int, "@calendarId", calendarId);
			SetDBParam(SqlDbType.DateTime, "@s", startDate);
			SetDBParam(SqlDbType.DateTime, "@e", startDate.AddYears(5));
			db.ExecuteStoredProcedure("EXEC Calendar_ApplyToPastWeekends @calendarId, @s", DBParameters);
			db.ExecuteStoredProcedure("EXEC Calendar_ApplyToFutureWeekends @calendarId", DBParameters);
			db.ExecuteStoredProcedure("EXEC dbo.Calendar_CheckWeekends @calendarId, @s, @e", DBParameters);
		}

		/// <summary>
		/// Apply calendar changes 5 years from today
		/// </summary>
		/// <param name="calendarId">Calendar's indentifier</param>
		public void ApplyFutureCalendar(int calendarId) {
			SetDBParam(SqlDbType.Int, "@calendarId", calendarId);
			SetDBParam(SqlDbType.DateTime, "@s", DateTime.Now);
			SetDBParam(SqlDbType.DateTime, "@e", DateTime.Now.AddYears(5));
			db.ExecuteStoredProcedure("EXEC Calendar_ApplyToFutureWeekends @calendarId", DBParameters);
			db.ExecuteStoredProcedure("EXEC dbo.Calendar_CheckWeekends @calendarId, @s, @e", DBParameters);
		}
	}
}