﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.calendar {
	public class CalendarStaticHoliday {
		public CalendarStaticHoliday() { }

		public CalendarStaticHoliday(DataRow row) {
			CalendarStaticHolidayId = (int) row["calendar_static_holiday_id"];
			CalendarId = (int) row["calendar_id"];
			Name = (string) row["Name"];
			Month = (byte) row["month"];
			Day = (byte) row["day"];
		}

		public int CalendarStaticHolidayId { get; set; }
		public int CalendarId { get; set; }
		public string Name { get; set; }
		public byte Month { get; set; }
		public byte Day { get; set; }
	}

	public class CalendarStaticHolidays : ConnectionBase {
		public CalendarStaticHolidays(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) { }

		public List<CalendarStaticHoliday> GetStaticHolidays(int calendarId) {
			var result = new List<CalendarStaticHoliday>();
			SetDBParam(SqlDbType.Int, "@calendarId", calendarId);

			var sql = " SELECT calendar_static_holiday_id, calendar_id, name, month, day  " +
			          " FROM calendar_static_holidays  " +
			          " WHERE calendar_id = @calendarId  " +
			          " UNION  " +
			          " SELECT calendar_static_holiday_id, calendar_id, name, month, day  " +
			          " FROM calendar_static_holidays  " +
			          " WHERE calendar_id = (SELECT parent_calendar_id FROM calendars WHERE calendar_id = @calendarId) " +
			          " ORDER BY month, day ";

			foreach (DataRow row in db.GetDatatable(sql,DBParameters).Rows) { 
				result.Add(new CalendarStaticHoliday(row));
			}

			return result;
		}

		internal void DeleteStaticHoliday(int calendarStaticHolidayId) {
			SetDBParam(SqlDbType.Int, "@calendarStaticHolidayId", calendarStaticHolidayId);
			db.ExecuteDeleteQuery(
				"DELETE FROM calendar_static_holidays WHERE calendar_static_holiday_id = @calendarStaticHolidayId; " +
				"DELETE FROM calendar_data WHERE calendar_static_holiday_id = @calendarStaticHolidayId", DBParameters);
		}

		public CalendarStaticHoliday NewStaticHoliday(CalendarStaticHoliday staticHoliday) {
			SetDBParam(SqlDbType.Int, "@calendarId", staticHoliday.CalendarId);
			staticHoliday.CalendarStaticHolidayId = db.ExecuteInsertQuery(
				"INSERT INTO calendar_static_holidays (calendar_id) VALUES(@calendarId)", DBParameters);
			return SaveStaticHoliday(staticHoliday, true);
		}

		public CalendarStaticHoliday
			SaveStaticHoliday(CalendarStaticHoliday staticHoliday, bool executeEnsure = false) {
			SetDBParam(SqlDbType.Int, "@calendarId", staticHoliday.CalendarId);
			SetDBParam(SqlDbType.NVarChar, "@name", staticHoliday.Name);
			SetDBParam(SqlDbType.TinyInt, "@month", staticHoliday.Month);
			SetDBParam(SqlDbType.TinyInt, "@day", staticHoliday.Day);
			SetDBParam(SqlDbType.Int, "@calendarStaticHolidayId", staticHoliday.CalendarStaticHolidayId);

			db.ExecuteInsertQuery(
				"UPDATE calendar_static_holidays SET name = @name, month = @month, day = @day WHERE calendar_static_holiday_id = @calendarStaticHolidayId ",
				DBParameters);


			if (executeEnsure) {
				var row = db.GetDataRow("SELECT MIN(event_date) AS start_date, MAX(event_date) AS end_date " +
				                        "FROM calendar_data WHERE calendar_id = @calendarId", DBParameters);
				if (!row["start_date"].Equals(DBNull.Value)) {
					SetDBParam(SqlDbType.DateTime, "@startDate", row["start_date"]);
					SetDBParam(SqlDbType.DateTime, "@endDate", row["end_date"]);

					db.ExecuteStoredProcedure(
						"EXEC Calendar_ModifyStaticHoliday @calendarId, @calendarStaticHolidayId, @startDate, @endDate",
						DBParameters);
				}
			}

			return staticHoliday;
		}
	}
}