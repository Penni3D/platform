﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.calendar {
	public class CalendarEvent {
		public CalendarEvent() { }

		public CalendarEvent(DataRow row) {
			CalendarEventId = (int) row["calendar_event_id"];
			CalendarId = (int) row["calendar_id"];
			Name = (string) row["name"];

			EventStart = Conv.ToDateTime(row["event_start"]);
			EventEnd = Conv.ToDateTime(row["event_end"]);
			if (row["event_type"] != DBNull.Value) {
				EventType = Conv.ToInt(row["event_type"]);
			}
		}

		public int? CalendarEventId { get; set; }
		public int CalendarId { get; set; }
		public string Name { get; set; }
		public int? EventType { get; set; }
		public DateTime? EventStart { get; set; }
		public DateTime? EventEnd { get; set; }
	}

	public class CalendarEvents : ConnectionBase {
		private readonly AuthenticatedSession _authenticatedSession;

		public CalendarEvents(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			_authenticatedSession = authenticatedSession;
		}

		public CalendarEvent NewEvent(CalendarEvent calendarEvent) {
			SetDBParam(SqlDbType.Int, "@calendarId", calendarEvent.CalendarId);

			calendarEvent.CalendarEventId = db.ExecuteInsertQuery(
				"INSERT INTO calendar_events(calendar_id) VALUES(@calendarId)", DBParameters);

			return SaveEvent(calendarEvent, true);
		}

		private void NotifyResourceOwners(CalendarEvent calendarEvent) {
			SetDBParam(SqlDbType.Int, "@calendarId", calendarEvent.CalendarId);
			SetDBParam(SqlDbType.DateTime, "@startDate", Conv.IfNullThenDbNull(calendarEvent.EventStart));
			SetDBParam(SqlDbType.DateTime, "@endDate", Conv.IfNullThenDbNull(calendarEvent.EventEnd));

			//Check if resource owners need notifications
			var sql1 = " SELECT email, locale_id FROM _" + instance + "_user WHERE item_id IN ( " +
			           " SELECT DISTINCT idps.selected_item_id FROM _" + instance + "_allocation a " +
			           " INNER JOIN allocation_durations ad ON a.item_id = ad.allocation_item_id AND ad.date BETWEEN @startDate AND @endDate " +
			           " INNER JOIN item_data_process_selections idps ON idps.item_id = a.cost_center AND idps.item_column_id = (SELECT item_column_id FROM item_columns WHERE process = 'list_cost_center' AND name = 'owner') " +
			           " WHERE a.status IN (13, 20, 25, 40, 50, 55) AND a.resource_item_id = (SELECT item_id FROM calendars WHERE calendar_id = @calendarId)) ";

			var user = Conv.ToStr(db.ExecuteScalar(
				"SELECT first_name + ' ' + last_name AS name FROM _" + instance +
				"_user WHERE item_id IN (SELECT item_id FROM calendars WHERE calendar_id = @calendarId)",
				DBParameters));

			//Set allocations to draft -- alpiq didn't want this after all bsa
			// var sql2 = " UPDATE _" + instance + "_allocation SET status = 10 FROM _" + instance + "_allocation a " +
			//            " INNER JOIN allocation_durations ad ON a.item_id = ad.allocation_item_id AND ad.date BETWEEN @startDate AND @endDate " +
			//            " WHERE a.status IN (13, 20, 25, 40, 50, 55) AND a.resource_item_id = (SELECT item_id FROM calendars WHERE calendar_id = @calendarId) ";
			// db.ExecuteUpdateQuery(sql2, DBParameters);

			foreach (DataRow owner in db.GetDatatable(sql1, DBParameters).Rows) {
				var t = new Translations(instance, Conv.ToStr(owner["locale_id"]));
				var body = t.GetTranslation("VACATION_NOTIFICATION_BODY");

				var notifyEmail = new Email(instance, _authenticatedSession,
					body.Replace("#USER#", user), t.GetTranslation("VACATION_NOTIFICATION_SUBJECT"));
				notifyEmail.Add(Conv.ToStr(owner["email"]), Email.AddressTypes.ToAdress);
				notifyEmail.Send();
			}
		}

		public CalendarEvent SaveEvent(CalendarEvent calendarEvent, bool executeEnsure = false) {
			SetDBParam(SqlDbType.Int, "@calendarEventId", calendarEvent.CalendarEventId);
			SetDBParam(SqlDbType.NVarChar, "@name", Conv.IfNullThenDbNull(calendarEvent.Name));
			SetDBParam(SqlDbType.DateTime, "@eventStart", Conv.IfNullThenDbNull(calendarEvent.EventStart));
			SetDBParam(SqlDbType.DateTime, "@eventEnd", Conv.IfNullThenDbNull(calendarEvent.EventEnd));
			SetDBParam(SqlDbType.Int, "@eventType", Conv.IfNullThenDbNull(calendarEvent.EventType));

			SetDBParam(SqlDbType.Int, "@dayOff", 0);
			SetDBParam(SqlDbType.Int, "@holiday", 0);
			SetDBParam(SqlDbType.Int, "@notWork", 0);

			switch (calendarEvent.EventType) {
				case 0:
					SetDBParam(SqlDbType.Int, "@holiday", 1);
					SetDBParam(SqlDbType.Int, "@notWork", 1);
					break;
				case 1:
					SetDBParam(SqlDbType.DateTime, "@eventEnd", Conv.IfNullThenDbNull(calendarEvent.EventStart));
					break;
				case 2:
					SetDBParam(SqlDbType.Int, "@dayOff", 1);
					SetDBParam(SqlDbType.Int, "@holiday", 1);
					SetDBParam(SqlDbType.Int, "@notWork", 1);
					SetDBParam(SqlDbType.DateTime, "@eventEnd", Conv.IfNullThenDbNull(calendarEvent.EventStart));
					break;
			}

			var notifyOwnersNeeded = false;
			if (calendarEvent.CalendarEventId != null) {
				var oldEvent = GetCalendarEvent((int) calendarEvent.CalendarEventId);
				if (calendarEvent.EventStart != null && calendarEvent.EventEnd != null) {
					if ((oldEvent.EventStart == null && oldEvent.EventEnd == null) ||
					    (oldEvent.EventStart != calendarEvent.EventStart ||
					     oldEvent.EventEnd != calendarEvent.EventEnd))
						notifyOwnersNeeded = true;
				}
			}

			db.ExecuteUpdateQuery(
				"UPDATE calendar_events SET event_start = @eventStart, event_end = @eventEnd, name = @name, event_type = @eventType WHERE calendar_event_id = @calendarEventId",
				DBParameters);

			if (executeEnsure) {
				db.ExecuteStoredProcedure(
					"EXEC Calendar_ModifyEvent @calendarId, @calendarEventId, @dayOff, @holiday, @notWork",
					DBParameters);
			}

			if (notifyOwnersNeeded) NotifyResourceOwners(calendarEvent);

			return calendarEvent;
		}

		public List<CalendarEvent> GetCalendarEvents(int calendarId) {
			var result = new List<CalendarEvent>();
			SetDBParam(SqlDbType.Int, "@calendarId", calendarId);

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT calendar_event_id, calendar_id, name, event_start, event_end, event_type FROM calendar_events WHERE calendar_id = @calendarId ORDER BY event_start",
					DBParameters).Rows) {
				result.Add(new CalendarEvent(row));
			}

			return result;
		}

		public List<CalendarEvent> GetCalendarEvents(int calendarId, int year) {
			var result = new List<CalendarEvent>();
			SetDBParam(SqlDbType.Int, "@calendarId", calendarId);
			SetDBParam(SqlDbType.Int, "@year", year);

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT calendar_event_id, calendar_id, name, event_start, event_end, event_type FROM calendar_events WHERE calendar_id = @calendarId AND YEAR(event_start) = @year ORDER BY event_start",
					DBParameters).Rows) {
				result.Add(new CalendarEvent(row));
			}

			return result;
		}

		public CalendarEvent GetCalendarEvent(int calendarEventId) {
			SetDBParam(SqlDbType.Int, "@calendarEventId", calendarEventId);
			return new CalendarEvent(db.GetDataRow(
				"SELECT calendar_event_id, calendar_id, name, event_start, event_end, event_type FROM calendar_events WHERE calendar_event_id = @calendarEventId",
				DBParameters));
		}

		public List<CalendarEvent> GetCalendarEvents(string calendarIds) {
			var result = new List<CalendarEvent>();

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT calendar_event_id, calendar_id, name, event_start, event_end, event_type FROM calendar_events WHERE calendar_event_id IN (" +
					Conv.ToSql(calendarIds) + ") ORDER BY event_start",
					DBParameters).Rows) {
				result.Add(new CalendarEvent(row));
			}

			return result;
		}

		public void DeleteEvent(int calendarEventId) {
			SetDBParam(SqlDbType.Int, "@calendarEventId", calendarEventId);
			db.ExecuteDeleteQuery("DELETE FROM calendar_events WHERE calendar_event_id = @calendarEventId; " +
			                      "DELETE FROM calendar_data WHERE calendar_event_id = @calendarEventId", DBParameters);
		}

		public void UpdateFlexTime(List<CalendarEvent> calendarEvents, AuthenticatedSession session) {
			var calendarId = 0;
			var months = new List<DateTime>();
			foreach (var calendarEvent in calendarEvents) {
				calendarId = calendarEvent.CalendarId;
				DateTime? eventStart = calendarEvent.EventStart != null
					? new DateTime(((DateTime) calendarEvent.EventStart).Year,
						((DateTime) calendarEvent.EventStart).Month, 1)
					: null;
				DateTime? eventEnd = calendarEvent.EventEnd != null
					? new DateTime(((DateTime) calendarEvent.EventEnd).Year, ((DateTime) calendarEvent.EventEnd).Month,
						1)
					: null;
				if (eventStart != null && eventEnd != null) {
					while (eventStart <= eventEnd) {
						if (!months.Contains((DateTime) eventStart)) {
							months.Add((DateTime) eventStart);
						}

						eventStart = ((DateTime) eventStart).AddMonths(1);
					}
				} else if (eventStart != null) {
					if (!months.Contains((DateTime) eventStart)) {
						months.Add((DateTime) eventStart);
					}
				} else if (eventEnd != null) {
					if (!months.Contains((DateTime) eventEnd)) {
						months.Add((DateTime) eventEnd);
					}
				}
			}

			var calendars = new Calendars(instance, session);
			var calendar = calendars.GetCalendar(calendarId);
			if (calendar.BaseCalendar) return;

			SetDBParam(SqlDbType.Int, "user_item_id", calendar.ItemId);
			foreach (var month in months) {
				SetDBParam(SqlDbType.DateTime, "date", month);
				db.ExecuteStoredProcedure(
					"EXEC dbo.flex_time_CalculateMonthBalance @instance, @user_item_id, @date", DBParameters);
			}
		}
	}
}