﻿using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.accessControl;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.calendar {
	[Route("v1/calendars")]
	public class CalendarsRest : PageBase {
		public CalendarsRest() : base("calendars", true) { }

		[HttpPost("{startDate}/{endDate}")]
		public Dictionary<int, UserCalendarData> GetUserCalendar(string startDate, string endDate, [FromBody] List<int> ids) {
			var result = new Dictionary<int, UserCalendarData>();
			var cd = new CalendarDatas(instance, currentSession);
			foreach (var id in ids) {
				var c = cd.GetUserCalendarData(id, startDate, endDate);
				if (c != null) result.Add(id, c);
			}
			return result;
		}

		[HttpGet("base/{baseCalenadarIds}/{startDate}/{endDate}")]
		public Dictionary<int, UserCalendarData> GetBaseCalendar(string baseCalenadarIds, string startDate, string endDate) {
			var ids = new List<int>();
			foreach (var id in baseCalenadarIds.Split(',')) {
				ids.Add(Conv.ToInt(id));
			}

			var result = new Dictionary<int, UserCalendarData>();
			var cd = new CalendarDatas(instance, currentSession);

			foreach (var id in ids) {
				var c = cd.GetBaseCalendarData(id, startDate, endDate);
				result.Add(id, c);
			}

			return result;
		}

		[HttpGet]
		public List<Calendar> GetCalendars() {
			var calendars = new Calendars(instance, currentSession);
			return calendars.GetCalendars();
		}

		[HttpGet("Base")]
		public List<Calendar> GetBaseCalendars() {
			var calendars = new Calendars(instance, currentSession);
			return calendars.GetBaseCalendars();
		}

		[HttpGet("{calendarId}")]
		public Calendar GetCalendar(int calendarId) {
			var calendars = new Calendars(instance, currentSession);
			return calendars.GetCalendar(calendarId);
		}

		[HttpGet("User/FeatureStates/{itemId}")]
		public List<string> GetUsersFeatureStates(int itemId) {
			var states =
				new States(instance, currentSession).GetEvaluatedStatesForFeature("user", "calendar_user", itemId);
			return states.States;
		}

		[HttpGet("User/{userId}")]
		public Calendar GetUsersCalendar(int userId) {
			var calendars = new Calendars(instance, currentSession);
			return calendars.GetUsersCalendar(userId);
		}

		[HttpGet("User/Base/{userId}")]
		public Calendar GetUsersBaseCalendar(int userId) {
			var calendars = new Calendars(instance, currentSession);
			return calendars.GetUsersBaseCalendar(userId);
		}

		[HttpGet("Timezones")]
		public Dictionary<string, string> GetTimezones() {
			var calendars = new Calendars(instance, currentSession);
			return calendars.GetTimezones();
		}

		[HttpPost]
		public IActionResult NewCalendar([FromBody] Calendar calendar) {
			CheckRight("write");
			var calendars = new Calendars(instance, currentSession);
			calendars.AddCalendar(calendar);
			return new ObjectResult(calendar);
		}

		[HttpPut]
		public IActionResult SaveCalendar([FromBody] Calendar calendar) {
			var calendars = new Calendars(instance, currentSession);
			calendars.SaveCalendar(calendar);
			return new ObjectResult(calendar);
		}

		[HttpDelete("{calendarId}")]
		public IActionResult DeleteCalendar(int calendarId) {
			CheckRight("write");
			var calendars = new Calendars(instance, currentSession);
			calendars.DeleteCalendar(calendarId);
			return new StatusCodeResult(200);
		}

		[HttpPost("user/{userId}")]
		public IActionResult NewUserCalendar(int userId) {
			CheckRight("write");
			var calendars = new Calendars(instance, currentSession);
			calendars.CreateUserCalendar(userId);
			return new StatusCodeResult(200);
		}

		[HttpPut("apply/{calendarId}")]
		public void ApplyCalendar(int calendarId) {
			var calendars = new Calendars(instance, currentSession);
			calendars.ApplyCalendar(calendarId);
		}

		[HttpPut("apply/{calendarId}/{startDate}")]
		public void ApplyCalendar(int calendarId, DateTime startDate) {
			var calendars = new Calendars(instance, currentSession);
			calendars.ApplyCalendar(calendarId, startDate);
		}

		[HttpPut("apply/future/{calendarId}")]
		public void ApplyFutureCalendar(int calendarId) {
			var calendars = new Calendars(instance, currentSession);
			calendars.ApplyFutureCalendar(calendarId);
		}
	}

	[Route("v1/calendars/Events")]
	public class CalendarEventsRest : RestBase {
		[HttpGet("{calendarId}")]
		public List<CalendarEvent> GetEvents(int calendarId) {
			var calendarEvents = new CalendarEvents(instance, currentSession);
			return calendarEvents.GetCalendarEvents(calendarId);
		}

		[HttpGet("{calendarId}/{year}")]
		public List<CalendarEvent> GetEvents(int calendarId, int year) {
			var calendarEvents = new CalendarEvents(instance, currentSession);
			return calendarEvents.GetCalendarEvents(calendarId, year);
		}

		[HttpPost]
		public CalendarEvent AddEvent([FromBody] CalendarEvent calendarEvent) {
			var calendarEvents = new CalendarEvents(instance, currentSession);
			var ret = calendarEvents.NewEvent(calendarEvent);

			calendarEvents.UpdateFlexTime(new List<CalendarEvent>(){ calendarEvent }, currentSession);

			return ret;
		}

		[HttpPut]
		public List<CalendarEvent> SaveEvents([FromBody] List<CalendarEvent> calendarEvent) {
			var calendarEvents = new CalendarEvents(instance, currentSession);
			var ret = new List<CalendarEvent>();

			foreach (var e in calendarEvent) {
				ret.Add(calendarEvents.SaveEvent(e));
			}
			
			calendarEvents.UpdateFlexTime(calendarEvent, currentSession);

			return ret;
		}

		[HttpDelete("{events}")]
		public void DeleteEvents(string events) {
			var calendarEvents = new CalendarEvents(instance, currentSession);

			var calendarEvent = calendarEvents.GetCalendarEvents(events);

			var ids = events.Split(",");
			foreach (var id in ids) {
				calendarEvents.DeleteEvent(Conv.ToInt(id));
			}
			
			calendarEvents.UpdateFlexTime(calendarEvent, currentSession);
		}
	}

	[Route("v1/calendars/StaticHolidays")]
	public class CalendarStaticHolidaysRest : RestBase {
		[HttpGet("{calendarId}")]
		public List<CalendarStaticHoliday> GetHolidays(int calendarId) {
			var staticHolidays = new CalendarStaticHolidays(instance, currentSession);
			return staticHolidays.GetStaticHolidays(calendarId);
		}

		[HttpPost]
		public CalendarStaticHoliday AddHoliday([FromBody] CalendarStaticHoliday staticHoliday) {
			var staticHolidays = new CalendarStaticHolidays(instance, currentSession);
			return staticHolidays.NewStaticHoliday(staticHoliday);
		}

		[HttpPut]
		public List<CalendarStaticHoliday> SaveHolidays([FromBody] List<CalendarStaticHoliday> staticHoliday) {
			var staticHolidays = new CalendarStaticHolidays(instance, currentSession);
			var ret = new List<CalendarStaticHoliday>();

			foreach (var s in staticHoliday) {
				ret.Add(staticHolidays.SaveStaticHoliday(s));
			}

			return ret;
		}

		[HttpDelete("{holidays}")]
		public void DeleteHoliday(string holidays) {
			var staticHolidays = new CalendarStaticHolidays(instance, currentSession);
			var ids = holidays.Split(",");
			foreach (var id in ids) {
				staticHolidays.DeleteStaticHoliday(Conv.ToInt(id));
			}
		}
	}
}