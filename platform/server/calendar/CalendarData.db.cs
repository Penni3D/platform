﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.calendar {
	[Serializable]
	public class CalendarData {
		public CalendarData() { }

		public CalendarData(DataRow row) {
			CalendarDataId = (int) row["calendar_data_id"];
			CalendarId = Conv.ToInt(row["calendar_id"]);
			if (!DBNull.Value.Equals(row["calendar_event_id"])) {
				CalendarEventId = (int) row["calendar_event_id"];
			}

			if (!DBNull.Value.Equals(row["calendar_static_holiday_id"])) {
				CalendarStaticHolidayId = (int) row["calendar_static_holiday_id"];
			}

			EventDate = (DateTime) row["event_date"];
			DayOff = Convert.ToBoolean(row["dayoff"]);
			Holiday = Convert.ToBoolean(row["holiday"]);
			Notwork = Convert.ToBoolean(row["notwork"]);
			if (!DBNull.Value.Equals(row["event_type"])) {
				EventType = (int)(row["event_type"]);
			}
		}

		public int CalendarDataId { get; set; }
		public int CalendarId { get; set; }
		public int? CalendarEventId { get; set; }
		public int? CalendarStaticHolidayId { get; set; }
		public DateTime EventDate { get; set; }
		public bool DayOff { get; set; }
		public bool Holiday { get; set; }
		public bool Notwork { get; set; }

		public int EventType { get; set; }
	}

	public class UserCalendarData {
		public UserCalendarData() {
			Exceptions = new List<CalendarData>();
		}

		public double WorkHours { get; set; }
		public double TimesheetHours { get; set; }
		public double WeeklyHours { get; set; }
		public List<CalendarData> Exceptions { get; set; }
		public int? BaseCalendarId { get; set; }
		public int? CalendarId { get; set; }
	}

	public class CalendarDatas : ConnectionBase {
		public CalendarDatas(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) { }

		public UserCalendarData GetUserCalendarDataArchivedPatterns(int userId, string startDate, string endDate, DateTime archiveDate) { 
			SetDBParam(SqlDbType.Int, "@userId", userId);
			SetDBParam(SqlDbType.Date, "@archiveDate", archiveDate);
			
			var userData = new UserCalendarData();
			var dr = db.GetDataRow("SELECT * FROM GetCalendarForUserArchive(@userId,@archiveDate)", DBParameters);
			if (dr == null) return GetUserCalendarData(userId, startDate, endDate);

			userData.WorkHours = Conv.ToDouble(dr["work_hours"]);
			userData.TimesheetHours = Conv.ToDouble(dr["timesheet_hours"]);
			userData.WeeklyHours = Conv.ToDouble(dr["weekly_hours"]);

			return userData;
		}

		public UserCalendarData GetUserCalendarData(int userId, string startDate, string endDate) {
			SetDBParam(SqlDbType.Int, "@userId", userId);
			var calendar = db.GetDataRow("SELECT c.parent_calendar_id, c.calendar_id, c.ensure_date, " +
			                             "p.ensure_date AS parent_ensure_date " +
			                             "FROM calendars c " +
			                             "LEFT JOIN calendars p ON c.parent_calendar_id = p.calendar_id " +
			                             "WHERE c.item_id = @userId AND c.instance = @instance", DBParameters);

			if (calendar is null) return null;
			
			SetDBParam(SqlDbType.Int, "@parentCalendarId", Conv.ToInt(calendar["parent_calendar_id"]));
			SetDBParam(SqlDbType.Int, "@userCalendarId", Conv.ToInt(calendar["calendar_id"]));
			SetDBParam(SqlDbType.DateTime, "@startDate", startDate);
			SetDBParam(SqlDbType.DateTime, "@endDate", endDate);
			
			var userCalendarEnsure = Conv.ToDateTime(calendar["ensure_date"]);
			var baseCalendarEnsure = Conv.ToDateTime(calendar["parent_ensure_date"]);

			if (baseCalendarEnsure == null) {
				db.ExecuteStoredProcedure(
					"EXEC Calendar_CheckCalendar @parentCalendarId, NULL, @endDate", DBParameters);
			} else if (baseCalendarEnsure < Conv.ToDateTime(endDate)) {
				SetDBParam(SqlDbType.DateTime, "@ensureEndDate", baseCalendarEnsure);
				db.ExecuteStoredProcedure(
					"EXEC Calendar_CheckCalendar @parentCalendarId, @ensureEndDate, @endDate", DBParameters);
			}

			if (userCalendarEnsure == null) {
				db.ExecuteStoredProcedure(
					"EXEC Calendar_CheckCalendar @userCalendarId, NULL, @endDate", DBParameters);
			} else if (userCalendarEnsure < Conv.ToDateTime(endDate)) {
				SetDBParam(SqlDbType.DateTime, "@ensureEndDate", userCalendarEnsure);
				db.ExecuteStoredProcedure(
					"EXEC Calendar_CheckCalendar @userCalendarId, @ensureEndDate, @endDate", DBParameters);
			}

			var userData = new UserCalendarData();
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT * FROM GetCombinedCalendar(@parentCalendarId, @userCalendarId, @startDate, @endDate)",
					DBParameters)
				.Rows
			) {
				userData.Exceptions.Add(new CalendarData(row));
			}

			var dr = db.GetDataRow("SELECT * FROM GetCalendarForUser(@userId)", DBParameters);
			
			userData.WorkHours = Conv.ToDouble(dr["work_hours"]);
			userData.TimesheetHours = Conv.ToDouble(dr["timesheet_hours"]);
			userData.WeeklyHours = Conv.ToDouble(dr["weekly_hours"]);
			userData.BaseCalendarId = Conv.ToInt(calendar["parent_calendar_id"]);
			userData.CalendarId = Conv.ToInt(calendar["calendar_id"]);

			return userData;
		}

		public UserCalendarData GetBaseCalendarData(int baseCalendarId, string startDate, string endDate) {
			SetDBParam(SqlDbType.Int, "@baseCalendarId", baseCalendarId);
			var calendar = db.GetDataRow("SELECT calendar_id, ensure_date" +
										 ", work_hours, timesheet_hours, weekly_hours " +
										 "FROM calendars " +
										 "WHERE calendar_id = @baseCalendarId AND instance = @instance", DBParameters);

			SetDBParam(SqlDbType.DateTime, "@startDate", startDate);
			SetDBParam(SqlDbType.DateTime, "@endDate", endDate);

			var baseCalendarEnsure = Conv.ToDateTime(calendar["ensure_date"]);

			if (baseCalendarEnsure == null) {
				db.ExecuteStoredProcedure(
					"EXEC Calendar_CheckCalendar @baseCalendarId, NULL, @endDate", DBParameters);
			}
			else if (baseCalendarEnsure < Conv.ToDateTime(endDate)) {
				SetDBParam(SqlDbType.DateTime, "@ensureEndDate", baseCalendarEnsure);
				db.ExecuteStoredProcedure(
					"EXEC Calendar_CheckCalendar @baseCalendarId, @ensureEndDate, @endDate", DBParameters);
			}
		
			var userData = new UserCalendarData();
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT * FROM GetCombinedCalendar(@baseCalendarId, @baseCalendarId, @startDate, @endDate)",
					DBParameters)
				.Rows
			) {
				userData.Exceptions.Add(new CalendarData(row));
			}

			userData.WorkHours = Conv.ToDouble(calendar["work_hours"]);
			userData.TimesheetHours = Conv.ToDouble(calendar["timesheet_hours"]);
			userData.WeeklyHours = Conv.ToDouble(calendar["weekly_hours"]);
			userData.BaseCalendarId = Conv.ToInt(calendar["calendar_id"]);
			userData.CalendarId = Conv.ToInt(calendar["calendar_id"]);

			return userData;
		}
	}
}