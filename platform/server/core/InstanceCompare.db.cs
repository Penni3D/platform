﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.core {
    public static class InstanceCompareProgress {
        private static string Progress = "Not started";

        public static void Initialize() {
            CompareProgress = 0;
            DeployProgress = 0;
            DeployCount = 0;
            PhaseProgress = "";
            Progress = "Not started";
            PhaseCount = 1;
        }

        public static readonly int CompareCount = 64 * 2;
        public static int DeployCount = 0;
        public static int CompareProgress = 0;
        public static int DeployProgress = 0;
        public static int PhaseCount = 1;
        public static string PhaseProgress = "";

        public static void SetCompareProgress(string value) {
            Progress = value;
            CompareProgress++;
            if (CompareProgress > CompareCount) CompareProgress = CompareCount;
        }

        public static void SetDeployProgress(string value) {
            Progress = value;
            DeployProgress++;
        }

        public static void SetPhaseProgress(string value) {
            if (!(PhaseProgress == "Deploying" && value == "Comparing") && PhaseProgress != "Done") PhaseProgress = value;
        }

        public static InstanceCompareProgressReport GetProgress() {
            return new InstanceCompareProgressReport() {
                CompareCount = CompareCount,
                DeployCount = DeployCount,
                CompareProgress = CompareProgress,
                DeployProgress = DeployProgress,
                PhaseCount = PhaseCount,
                PhaseProgress = PhaseProgress,
                Progress = Progress
            };
        }
    }

    public class InstanceCompareProgressReport {
        public int CompareCount;
        public int CompareProgress;
        public int DeployCount;
        public int DeployProgress;
        public int PhaseCount;
        public string PhaseProgress;
        public string Progress;
    }

    public class InstanceCompare : ConnectionBase {
        private AuthenticatedSession authenticateSession;
        private Translations t;
        private List<string> inserted_processes;

        public InstanceCompare(string instance, AuthenticatedSession authenticateSession) : base(instance,
            authenticateSession) {
            this.authenticateSession = authenticateSession;

            t = new Translations(instance, authenticateSession, authenticateSession.locale_id);
        }

        public string GetInstances() {
            return db.GetDatatableJson("SELECT instance, instance AS name FROM instances", DBParameters);
        }

        public string GetInstances(string targetDb) {
            targetDb = Conv.ToSql(targetDb);
            return db.GetDatatableJson(
                "SELECT instance, instance AS name FROM " + Conv.ToSql(targetDb) + ".dbo.instances", DBParameters);
        }

        public List<Dictionary<string, string>> GetDb() {
            string release_date_string = Config.VersionDate;
            DataRow release = db.GetDataRow("SELECT TOP 1 * FROM release_history ORDER BY installation_date DESC",
                new List<SqlParameter>());
            if (release != null) {
                if (Conv.ToDateTime(release["release_date"]) != null &&
                    ((DateTime)Conv.ToDateTime(release["release_date"])).Year > 1900) {
                    release_date_string = ((DateTime)Conv.ToDateTime(release["release_date"])).ToString("d.M.yyyy");
                }
            }

            string migration_version =
                Conv.ToStr(db.ExecuteScalar("SELECT TOP 1 version FROM database_migrations ORDER BY id DESC",
                    DBParameters));
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            Dictionary<string, string> source_db = new Dictionary<string, string>();
            foreach (string param in Config.GetValue("HostingEnvironment", "ConnectionString").Split(";")) {
                if (param.Trim().Left(16) == "Initial Catalog=") {
                    source_db.Add("database", param.Split("=")[1]);
                    source_db.Add("name",
                        param.Split("=")[1] + " --- Rel. " + Config.Version + " @ " + release_date_string +
                        " --- Repo: " + Config.Repo + " --- Migration: " + migration_version);
                    result.Add(source_db);
                    return result;
                }
            }

            source_db.Add("database", "unknown");
            source_db.Add("name",
                "unknown --- Rel. " + Config.Version + " @ " + release_date_string + " --- Repo: " + Config.Repo +
                " --- Migration: " + migration_version);
            result.Add(source_db);
            return result;
        }

        public List<Dictionary<string, object>> GetDbList() {
            SetDBParam(SqlDbType.NVarChar, "@current_db", GetDb()[0]["database"]);
            List<Dictionary<string, object>> databases = db.GetDatatableDictionary(
                "SELECT s.name AS [database], s.name FROM master.dbo.sysdatabases s WHERE s.name NOT IN ('master', 'msdb', 'tempdb', 'model') AND (SELECT d.state FROM sys.databases d where d.name = s.name) = 0 AND (SELECT HAS_PERMS_BY_NAME(s.name, 'DATABASE', 'ANY')) = 1 AND s.name <> @current_db ORDER BY s.name ASC",
                DBParameters);
            List<Dictionary<string, object>> drops = new List<Dictionary<string, object>>();
            foreach (Dictionary<string, object> database in databases) {
                if (Conv.ToInt(db.ExecuteScalar(
                        "USE " + Conv.ToStr(database["name"]) +
                        " SELECT COUNT(*) FROM fn_my_permissions (null, 'DATABASE') WHERE permission_name IN ('SELECT', 'INSERT', 'UPDATE', 'DELETE', 'EXECUTE')",
                        new List<SqlParameter>())) == 5) {
                    if (Conv.ToInt(db.ExecuteScalar(
                            "SELECT COUNT(*) FROM " + Conv.ToStr(database["name"]) +
                            ".INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'database_migrations'", DBParameters)) > 0) {
                        string version = "unknown";
                        string repo = "unknown";
                        if (Conv.ToInt(db.ExecuteScalar(
                                "SELECT COUNT(*) FROM " + Conv.ToStr(database["name"]) +
                                ".INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'release_history'", DBParameters)) > 0) {
                            DataRow release =
                                db.GetDataRow(
                                    "SELECT TOP 1 * FROM " + Conv.ToStr(database["name"]) +
                                    ".dbo.release_history ORDER BY installation_date DESC", new List<SqlParameter>());
                            if (release != null) {
                                string release_date_string = "unknown";
                                if (Conv.ToDateTime(release["release_date"]) != null &&
                                    ((DateTime)Conv.ToDateTime(release["release_date"])).Year > 1900) {
                                    release_date_string =
                                        ((DateTime)Conv.ToDateTime(release["release_date"])).ToString("d.M.yyyy");
                                }

                                version = Conv.ToStr(release["release_version"]) + " @ " + release_date_string;
                                repo = Conv.ToStr(release["repository"]);
                            }
                        }

                        database["name"] += " --- Rel. " + version + " --- Repo: " + repo + " --- Migration: " +
                                            Conv.ToStr(db.ExecuteScalar(
                                                "SELECT TOP 1 version FROM " + Conv.ToStr(database["name"]) +
                                                ".dbo.database_migrations ORDER BY id DESC", DBParameters));
                    } else {
                        drops.Add(database);
                    }
                } else {
                    drops.Add(database);
                }
            }

            foreach (Dictionary<string, object> drop in drops) {
                databases.Remove(drop);
            }

            return databases;
        }

        public string GetProcesses(string sourceInstance) {
            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            string sql = "SELECT p.process, ISNULL(t.translation, '') + ' (' + p.process + ')' AS translation " +
                         "FROM processes p " +
                         "LEFT JOIN process_translations t ON t.instance = p.instance " +
                         "AND t.process = p.process " +
                         "AND t.variable = p.variable " +
                         "AND t.code = " + toSqlVal(authenticateSession.locale_id) +
                         "WHERE p.instance = @sourceInstance " +
                         "ORDER BY t.translation ASC";
            return db.GetDatatableJson(sql, DBParameters);
        }

        public List<Dictionary<string, object>> GetDuplicates(string sourceInstance) {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);

            Dictionary<string, string> tables = new Dictionary<string, string>();
            tables.Add("conditions", "condition_id");
            //tables.Add("conditions_groups", "condition_group_id");
            tables.Add("instance_menu", "instance_menu_id");
            //tables.Add("instance_unions", "instance_union_id");
            //tables.Add("item_columns", "item_column_id");
            //tables.Add("notification_conditions", "notification_condition_id");
            //tables.Add("process_actions", "process_action_id");
            //tables.Add("process_actions_dialog", "process_action_dialog_id");
            tables.Add("process_containers", "process_container_id");
            //tables.Add("process_dashboard_charts", "process_dashboard_chart_id");
            //tables.Add("process_dashboards", "process_dashboard_id");
            //tables.Add("process_group_navigation", "process_navigation_id");
            tables.Add("process_groups", "process_group_id");
            tables.Add("process_portfolio_columns_groups", "portfolio_column_group_id");
            //tables.Add("process_portfolio_dialogs", "process_portfolio_dialog_id");
            tables.Add("process_portfolios", "process_portfolio_id");
            tables.Add("process_tabs", "process_tab_id");

            string last_table = "";
            string last_variable = "";
            int mod = 0;
            foreach (KeyValuePair<string, string> table in tables) {
                string t_instance = "t.instance = @sourceInstance";
                string tt_instance = "tt.instance = @sourceInstance";
                string extra_fields = "";
                //if (table.Key == "process_group_navigation") {
                //	t_instance = "t.process_group_id = (SELECT TOP 1 g.process_group_id FROM process_groups g WHERE g.process_group_id = t.process_group_id AND g.instance = @sourceInstance)";
                //	tt_instance = "tt.process_group_id = (SELECT TOP 1 gg.process_group_id FROM process_groups gg WHERE gg.process_group_id = tt.process_group_id AND gg.instance = @sourceInstance)";
                //}
                //else 
                if (table.Key == "conditions" || table.Key == "process_groups" || table.Key == "process_portfolios" ||
                    table.Key == "process_tabs" || table.Key == "process_portfolio_columns_groups" ||
                    table.Key == "process_containers") {
                    extra_fields = ", t.process";
                } else if (table.Key == "instance_menu") {
                    extra_fields = ", NULL AS process";
                }

                string sql = "SELECT t." + Conv.ToSql(table.Value) + " AS id, t.variable" + extra_fields +
                             " FROM " + Conv.ToSql(table.Key) + " t" +
                             " WHERE " + t_instance +
                             " AND (SELECT COUNT(*) FROM " + Conv.ToSql(table.Key) + " tt WHERE " + tt_instance +
                             " AND LOWER(tt.variable) = LOWER(t.variable) GROUP BY tt.variable) > 1" +
                             " ORDER BY t.variable ASC, t." + Conv.ToSql(table.Value) + " ASC";

                foreach (DataRow duplicate in db.GetDatatable(sql, DBParameters).Rows) {
                    if (last_table != table.Key || last_variable != Conv.ToStr(duplicate["variable"]).ToLower()) {
                        mod = 1 - mod;
                    }

                    Dictionary<string, object> result_row = new Dictionary<string, object>();
                    result_row.Add("id", Conv.ToInt(duplicate["id"]));
                    result_row.Add("table", table.Key);
                    result_row.Add("variable", Conv.ToStr(duplicate["variable"]));
                    result_row.Add("class", mod == 0 ? "background-lightGrey" : "");
                    if (table.Key == "conditions" || table.Key == "process_groups" ||
                        table.Key == "process_portfolios" || table.Key == "process_tabs" ||
                        table.Key == "process_portfolio_columns_groups" || table.Key == "process_containers") {
                        result_row.Add("process", Conv.ToStr(duplicate["process"]));
                        result_row.Add("id_name", Conv.ToSql(table.Value));
                    } else if (table.Key == "instance_menu") {
                        result_row.Add("process", null);
                        result_row.Add("id_name", Conv.ToSql(table.Value));
                    }

                    result.Add(result_row);

                    last_table = table.Key.ToLower();
                    last_variable = Conv.ToStr(duplicate["variable"]).ToLower();
                }
            }

            DataTable instance_cache = null;
            last_table = "";
            last_variable = "";
            foreach (Dictionary<string, object> result_row in result) {
                if (Conv.ToStr(result_row["table"]) == "conditions" ||
                    Conv.ToStr(result_row["table"]) == "process_groups" ||
                    Conv.ToStr(result_row["table"]) == "process_portfolios" ||
                    Conv.ToStr(result_row["table"]) == "process_tabs" ||
                    Conv.ToStr(result_row["table"]) == "process_portfolio_columns_groups" ||
                    Conv.ToStr(result_row["table"]) == "process_containers" ||
                    Conv.ToStr(result_row["table"]) == "instance_menu") {
                    if (last_table != Conv.ToStr(result_row["table"])) {
                        instance_cache =
                            db.GetDatatable(
                                "SELECT variable FROM " + Conv.ToStr(result_row["table"]) +
                                " WHERE instance = @sourceInstance", DBParameters);
                    }

                    if (last_table == Conv.ToStr(result_row["table"]).ToLower() &&
                        last_variable == Conv.ToStr(result_row["variable"]).ToLower()) {
                        SetDBParam(SqlDbType.NVarChar, "@id", Conv.ToInt(result_row["id"]));
                        SetDBParam(SqlDbType.NVarChar, "@process", Conv.ToStr(result_row["process"]));
                        SetDBParam(SqlDbType.NVarChar, "@old_variable", Conv.ToStr(result_row["variable"]));

                        string variable = "";
                        mod = 0;
                        do {
                            mod++;
                            variable = Conv.ToStr(result_row["variable"]) + "_" + mod;
                        } while (instance_cache.Select("variable = '" + Conv.Escape(variable) + "'").Length > 0);

                        SetDBParam(SqlDbType.NVarChar, "@new_variable", variable);
                        DataRow row = instance_cache.NewRow();
                        row["variable"] = variable;
                        instance_cache.Rows.Add(row);

                        db.ExecuteUpdateQuery(
                            "UPDATE " + Conv.ToSql(result_row["table"]) + " SET variable = @new_variable WHERE " +
                            Conv.ToSql(result_row["id_name"]) + " = @id", DBParameters);

                        foreach (DataRow lang in db
                                     .GetDatatable(
                                         "SELECT * FROM process_translations WHERE instance = @sourceInstance AND process = @process AND LOWER(variable) = LOWER(@old_variable)",
                                         DBParameters).Rows) {
                            SetDBParam(SqlDbType.NVarChar, "@code", Conv.ToStr(lang["code"]));
                            SetDBParam(SqlDbType.NVarChar, "@translation", Conv.ToStr(lang["translation"]));
                            db.ExecuteInsertQuery(
                                "INSERT INTO process_translations (instance, process, code, variable, translation) VALUES (@sourceInstance, @process, @code, @new_variable, @translation)",
                                DBParameters);
                        }
                    }

                    last_table = Conv.ToStr(result_row["table"]).ToLower();
                    last_variable = Conv.ToStr(result_row["variable"]).ToLower();
                }
            }

            result = new List<Dictionary<string, object>>();

            return result;
        }

        // \/ Selects \/

        public List<Dictionary<string, object>> InstanceLanguagesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Languages");

            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                targetDb = Conv.ToSql(targetDb);
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "SELECT  c1.code id1, c2.code id2 " +
                    " , c1.name name1, c2.name name2 " +
                    " , c1.abbr abbr1, c2.abbr abbr2 " +
                    " , c1.icon_url icon_url1, c2.icon_url icon_url2 " +
                    " FROM instance_languages c1 left join " + Conv.ToSql(targetDb) +
                    ".dbo.instance_languages c2 on c2.instance = @targetInstance and c1.code = c2.code where c1.instance = @sourceInstance " +
                    " and (c2.code is null or c1.name <> c2.name or c1.abbr <> c2.abbr or c1.icon_url <> c2.icon_url) ",
                    DBParameters);
            }
        }

        public List<Dictionary<string, object>> ProcessTranslationsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Translations");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            targetDb = Conv.ToSql(targetDb);
            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);

            return db.GetDatatableDictionary(
                "SELECT c1.process_translation_id, c2.process_translation_id process_translation_id2, " +
                " c1.process AS process1, c2.process AS process2, " +
                " dbo.notification_VariableIdReplace(c1.variable) AS id1, " +
                Conv.ToSql(targetDb) + ".dbo.notification_VariableIdReplace(c2.variable) AS id2, " +
                " c1.code AS code1, c2.code AS code2, " +
                " c1.translation AS translation1, c2.translation AS translation2 " +
                " FROM process_translations c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_translations c2 ON c2.instance = @targetInstance " +
                " AND c1.code = c2.code " +
                " AND dbo.notification_VariableIdReplace(c1.variable) = " + Conv.ToSql(targetDb) +
                ".dbo.notification_VariableIdReplace(c2.variable) " +
                " WHERE c1.instance = @sourceInstance " + processes_sql +
                " AND ( " +
                " c2.variable IS NULL OR c1.translation <> c2.translation" +
                " OR COALESCE(c1.process, '') <> COALESCE(c2.process, '')" +
                " ) AND c1.variable IS NOT NULL" +
                " AND NOT " + Conv.ToSql(targetDb) +
                ".dbo.notification_VariableVarReplace(@targetInstance, dbo.notification_VariableIdReplace(c1.variable)) LIKE '%_NULL%' " +
                " ORDER BY c1.process_translation_id ASC ", DBParameters);
        }

        //public List<Dictionary<string, object>> CalendarsCompare(string sourceInstance, string targetInstance, string targetDb = "", string processes = "") {
        //	targetDb = Conv.escape(targetDb);
        //	SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
        //	SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
        //	return db.GetDatatableDictionary(
        //		"SELECT  c1.calendar_id id1, c2.calendar_id id2, c1.variable " +
        //		" , c1.work_hours work_hours1, c2.work_hours work_hours2 " +
        //		" , c1.def_1 def_1_1, c2.def_1 def_1_2 " +
        //		" , c1.def_2 def_2_1, c2.def_2 def_2_2 " +
        //		" , c1.def_3 def_3_1, c2.def_3 def_3_2 " +
        //		" , c1.def_4 def_4_1, c2.def_4 def_4_2 " +
        //		" , c1.def_5 def_5_1, c2.def_5 def_5_2 " +
        //		" , c1.def_6 def_6_1, c2.def_6 def_6_2 " +
        //		" , c1.def_7 def_7_1, c2.def_7 def_7_2 " +
        //		" , c1.is_default is_default1, c2.is_default is_default2 " +
        //		" FROM calendars c1 left join " + Conv.toSql(targetDb) +
        //		".dbo.calendars c2 on c2.instance = @targetInstance and c1.variable = c2.variable where c1.instance = @sourceInstance " +
        //		" and (c2.variable is null or c1.work_hours <> c2.work_hours or c1.def_1 <> c2.def_1 or c1.def_2 <> c2.def_2 or c1.def_3 <> c2.def_3 or c1.def_4 <> c2.def_4 or c1.def_5 <> c2.def_5 or c1.def_6 <> c2.def_6 or c1.def_7 <> c2.def_7 or c1.is_default <> c2.is_default) ",
        //		DBParameters);
        //}

        //public List<Dictionary<string, object>> CalendarEventsCompare(string sourceInstance, string targetInstance, string targetDb = "", string processes = "") {
        //	if (processes.Length > 0) {
        //		return new List<Dictionary<string, object>>();
        //	}
        //	else {
        //		targetDb = Conv.escape(targetDb);
        //		SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
        //		SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
        //		return db.GetDatatableDictionary(
        //			"WITH c1 AS ( " +
        //			" SELECT dbo.calendar_GetVariable(c1.calendar_id) AS calendar_id " +
        //			" , name, event_start, event_end, event_type " +
        //			" FROM calendar_events c1 " +
        //			" WHERE c1.calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @sourceInstance) " +
        //			" ), c2 AS ( " +
        //			" SELECT " + Conv.toSql(targetDb) + ".dbo.calendar_GetVariable(c1.calendar_id) AS calendar_id " +
        //			" , name, event_start, event_end, event_type " +
        //			" FROM " + Conv.toSql(targetDb) + ".dbo.calendar_events c1 " +
        //			" WHERE c1.calendar_id IN (SELECT calendar_id FROM " + Conv.toSql(targetDb) + ".dbo.calendars WHERE instance = @targetInstance) " +
        //			" ) " +
        //			" SELECT c1.calendar_id AS calendar_id, c1.name, c1.event_start, c1.event_end, c1.event_type " +
        //			" FROM c1 " +
        //			" LEFT JOIN c2 ON c1.calendar_id = c2.calendar_id AND c1.name = c2.name " +
        //			" AND c1.event_start = c2.event_start AND c1.event_end = c2.event_end AND c1.event_end = c2.event_end " +
        //			" WHERE c1.calendar_id IS NOT NULL AND c2.calendar_id IS NULL ", DBParameters);
        //	}
        //}

        //public List<Dictionary<string, object>> CalendarStaticHolidaysCompare(string sourceInstance, string targetInstance, string targetDb = "", string processes = "") {
        //	if (processes.Length > 0) {
        //		return new List<Dictionary<string, object>>();
        //	}
        //	else {
        //		targetDb = Conv.escape(targetDb);
        //		SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
        //		SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
        //		return db.GetDatatableDictionary(
        //			"WITH c1 AS ( " +
        //			" SELECT dbo.calendar_GetVariable(c1.calendar_id) AS calendar_id " +
        //			" , name, month, day " +
        //			" FROM calendar_static_holidays c1 " +
        //			" WHERE c1.calendar_id IN (SELECT calendar_id FROM calendars WHERE instance = @sourceInstance) " +
        //			" ), c2 AS ( " +
        //			" SELECT " + Conv.toSql(targetDb) + ".dbo.calendar_GetVariable(c1.calendar_id) AS calendar_id " +
        //			" , name, month, day " +
        //			" FROM " + Conv.toSql(targetDb) + ".dbo.calendar_static_holidays c1 " +
        //			" WHERE c1.calendar_id IN (SELECT calendar_id FROM " + Conv.toSql(targetDb) + ".dbo.calendars WHERE instance = @targetInstance ) " +
        //			" ) " +
        //			" SELECT c1.calendar_id AS calendar_id, c1.name, c1.month, c1.day " +
        //			" FROM c1 " +
        //			" LEFT JOIN c2 ON c1.calendar_id = c2.calendar_id AND c1.name = c2.name " +
        //			" AND c1.month = c2.month AND c1.day = c2.day " +
        //			" WHERE c1.calendar_id is not null AND c2.calendar_id IS NULL ", DBParameters);
        //	}
        //}

        public List<Dictionary<string, object>> ProcessCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Processes");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            targetDb = Conv.ToSql(targetDb);
            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.variable, " +
                " c1.process AS id1, c2.process AS id2, " +
                " c1.list_process AS list_process1, c2.list_process AS list_process2, " +
                " c1.process_type AS process_type1, c2.process_type AS process_type2, " +
                " dbo.processAction_GetVariable(c1.new_row_action_id) AS new_row_action_id1, " +
                Conv.ToSql(targetDb) + ".dbo.processAction_GetVariable(c2.new_row_action_id) AS new_row_action_id2, " +
                " COALESCE(c1.colour, '') AS colour1, COALESCE(c2.colour, '') AS colour2, " +
                " COALESCE(c1.list_order, '') AS list_order1, COALESCE(c2.list_order, '') AS list_order2, " +
                " c1.system_process AS system_process1, c2.system_process AS system_process2, " +
                " c1.portfolio_show_Type AS portfolio_show_Type1, c2.portfolio_show_Type AS portfolio_show_Type2, " +
                " dbo.processAction_GetVariable(c1.after_new_row_action_id) AS after_new_row_action_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.processAction_GetVariable(c2.after_new_row_action_id) AS after_new_row_action_id2, " +
                " dbo.item_GetGuid(c1.list_type) AS list_type1, " +
                Conv.ToSql(targetDb) + ".dbo.item_GetGuid(c2.list_type) AS list_type2, " +
                " c1.compare AS compare1, c2.compare AS compare2, " +
                " c1.require_insert AS require_insert1, c2.require_insert AS require_insert2, " +
                " it1.ordering AS ordering1, it2.ordering AS ordering2, " +
                " it1.archive AS archive1, it2.archive AS archive2 " +
                " FROM processes c1 " +
                " LEFT JOIN item_tables it1 ON it1.instance = @sourceInstance AND it1.process = c1.process " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.processes c2 ON c2.instance = @targetInstance AND c1.process = c2.process " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.item_tables it2 ON it2.instance = @targetInstance AND it2.process = c2.process " +
                " WHERE c1.instance = @sourceInstance " + processes_sql +
                " AND (c2.process IS NULL " +
                " OR c1.list_process <> c2.list_process " +
                " OR c1.process_type <> c2.process_type " +
                " OR COALESCE(dbo.processAction_GetVariable(c1.new_row_action_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processAction_GetVariable(c2.new_row_action_id), '')" +
                " OR COALESCE(c1.colour, '') <> COALESCE(c2.colour, '') " +
                " OR COALESCE(c1.list_order, '') <> COALESCE(c2.list_order, '') " +
                " OR COALESCE(c1.system_process, '') <> COALESCE(c2.system_process, '') " +
                " OR COALESCE(c1.portfolio_show_Type, '') <> COALESCE(c2.portfolio_show_Type, '') " +
                " OR COALESCE(dbo.processAction_GetVariable(c1.after_new_row_action_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processAction_GetVariable(c2.after_new_row_action_id), '')" +
                " OR COALESCE(CAST(dbo.item_GetGuid(c1.list_type) AS NVARCHAR(36)), '') <> COALESCE(CAST(" +
                Conv.ToSql(targetDb) + ".dbo.item_GetGuid(c2.list_type) AS NVARCHAR(36)), '') " +
                " OR COALESCE(c1.compare, '') <> COALESCE(c2.compare, '') " +
                " OR COALESCE(c1.require_insert, '') <> COALESCE(c2.require_insert, '') " +
                " OR it1.ordering <> it2.ordering " +
                " OR it1.archive <> it2.archive " +
                " ) ORDER BY c1.process ASC ",
                DBParameters);
        }

        public List<Dictionary<string, object>> ProcessSubprocessesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Sub Processes");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND (c1.parent_process IN (" + processes + ") OR c1.process IN (" + processes + ")) ";
            }

            targetDb = Conv.ToSql(targetDb);
            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.parent_process AS parent_process1, " +
                " c1.process AS process1, c2.instance AS id2 " +
                " FROM process_subprocesses c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) + ".dbo.process_subprocesses c2 " +
                " ON c2.instance = @targetInstance " +
                " AND COALESCE(c1.parent_process, '') = COALESCE(c2.parent_process, '') " +
                " AND COALESCE(c1.process, '') = COALESCE(c2.process, '') " +
                " WHERE c1.instance = @sourceInstance AND c2.instance IS NULL " + processes_sql, DBParameters);
        }

        public List<Dictionary<string, object>> ItemColumnsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "", bool skipConfigs = false) {
            InstanceCompareProgress.SetCompareProgress("Item Columns");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                " SELECT c1.item_column_id AS id1, c2.item_column_id AS id2, " +
                " c1.process, c1.name, " +
                " c1.variable AS variable1, c2.variable AS variable2, " +
                " c1.data_type AS data_type1, c2.data_type AS data_type2, " +
                " c1.system_column AS system_column1, c2.system_column AS system_column2, " +
                " dbo.itemColumns_DaIdReplace(c1.data_type, c1.data_additional) AS da1_1, " +
                Conv.ToSql(targetDb) + ".dbo.itemColumns_DaIdReplace(c2.data_type, c2.data_additional) AS da1_2, " +
                " dbo.itemColumns_Da2IdReplace(c1.data_type, c1.data_additional2) AS da2_1, " +
                Conv.ToSql(targetDb) + ".dbo.itemColumns_Da2IdReplace(c2.data_type, c2.data_additional2) AS da2_2, " +
                " c1.input_method AS input_method1, c2.input_method AS input_method2, " +
                " c1.parent_select_type AS parent_select_type1, c2.parent_select_type AS parent_select_type2, " +
                " c1.input_name AS input_name1, c2.input_name AS input_name2, " +
                " c1.check_conditions AS check_conditions1, c2.check_conditions AS check_conditions2, " +
                " dbo.itemColumn_GetVariable(c1.relative_column_id)AS relative_column_id1,  " +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.relative_column_id) AS relative_column_id2, " +
                " dbo.processActionDialog_GetVariable(c1.process_dialog_id) AS process_dialog_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.processActionDialog_GetVariable(c2.process_dialog_id) AS process_dialog_id2,  " +
                " dbo.processAction_GetVariable(c1.process_action_id) AS process_action_id1, " +
                Conv.ToSql(targetDb) + ".dbo.processAction_GetVariable(c2.process_action_id) AS process_action_id2, " +
                " c1.status_column AS status_column1, c2.status_column AS status_column2, " +
                " c1.link_process AS link_process1, c2.link_process AS link_process2, " +
                " c1.use_fk AS use_fk1, c2.use_fk AS use_fk2, " +
                " c1.unique_col AS unique_col1, c2.unique_col AS unique_col2, " +
                " c1.use_lucene AS use_lucene1, c2.use_lucene AS use_lucene2, " +
                " dbo.processColumns_ValidationIdReplace(c1.ic_validation) AS ic_validation1, " +
                Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationIdReplace(c2.ic_validation) AS ic_validation2, " +
                " c1.in_conditions AS in_conditions1, c2.in_conditions AS in_conditions2, " +
                " dbo.itemColumns_Da2SubIdReplace(c1.data_type, c1.data_additional2, c1.sub_data_additional2) AS da2sub_1, " +
                Conv.ToSql(targetDb) +
                ".dbo.itemColumns_Da2SubIdReplace(c2.data_type, c2.data_additional2, c2.sub_data_additional2) AS da2sub_2 " +
                " FROM item_columns c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.item_columns c2 ON c2.instance = @targetInstance and c1.process = c2.process AND c1.name = c2.name " +
                " WHERE c1.instance = @sourceInstance " +
                " AND (c2.item_column_id IS NULL " +
                " OR COALESCE(c1.variable, '') <> COALESCE(c2.variable, '') " +
                " OR c1.data_type <> c2.data_type " +
                " OR c1.system_column <> c2.system_column " +
                " OR COALESCE(dbo.itemColumns_DaIdReplace(c1.data_type, c1.data_additional), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumns_DaIdReplace(c2.data_type, c2.data_additional), '') " +
                " OR (" + (skipConfigs ? "c1.data_type <> 16 AND " : "") +
                "COALESCE(dbo.itemColumns_Da2IdReplace(c1.data_type, c1.data_additional2), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumns_Da2IdReplace(c2.data_type, c2.data_additional2), '')) " +
                " OR COALESCE(c1.input_method, '') <> COALESCE(c2.input_method, '') " +
                " OR COALESCE(c1.input_name, '') <> COALESCE(c2.input_name, '') " +
                " OR COALESCE(c1.status_column, '') <> COALESCE(c2.status_column, '') " +
                " OR COALESCE(c1.parent_select_type, '') <> COALESCE(c2.parent_select_type, '') " +
                " OR COALESCE(c1.check_conditions, '') <> COALESCE(c2.check_conditions, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.relative_column_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.relative_column_id), '') " +
                " OR COALESCE(dbo.processActionDialog_GetVariable(c1.process_dialog_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processActionDialog_GetVariable(c2.process_dialog_id), '') " +
                " OR COALESCE(dbo.processAction_GetVariable(c1.process_action_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processAction_GetVariable(c2.process_action_id), '') " +
                " OR COALESCE(c1.link_process, '') <> COALESCE(c2.link_process, '') " +
                " OR COALESCE(c1.use_fk, '') <> COALESCE(c2.use_fk, '') " +
                " OR COALESCE(c1.unique_col, '') <> COALESCE(c2.unique_col, '') " +
                " OR COALESCE(c1.use_lucene, '') <> COALESCE(c2.use_lucene, '') " +
                " OR COALESCE(dbo.processColumns_ValidationIdReplace(c1.ic_validation), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationIdReplace(c2.ic_validation), '') " +
                " OR COALESCE(c1.in_conditions, '') <> COALESCE(c2.in_conditions, '') " +
                " OR COALESCE(dbo.itemColumns_Da2SubIdReplace(c1.data_type, c1.data_additional2, c1.sub_data_additional2), '') <> COALESCE(" +
                Conv.ToSql(targetDb) +
                ".dbo.itemColumns_Da2SubIdReplace(c2.data_type, c2.data_additional2, c2.sub_data_additional2), '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.item_column_id ASC ", DBParameters);
            // no sync: cache_date 
        }

        public List<Dictionary<string, object>> ListItemsCompare(string sourceInstance, string targetInstance,
            string targetDb, string processes, string list, int compare
            , List<Dictionary<string, object>> source_cols, List<Dictionary<string, object>> target_cols) {

            //This is done outside of the lists loop
            //InstanceCompareProgress.SetCompareProgress("List Items");
            
            targetDb = Conv.ToSql(targetDb);
            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);

            string sql = "WITH c1 AS (" +
                         " SELECT (SELECT i.guid FROM items i WHERE i.item_id = c1.item_id) AS guid1 " +
                         ", (SELECT i.guid FROM items i WHERE i.item_id = " +
                         "  (SELECT si.parent_item_id FROM item_subitems si WHERE si.item_id = c1.item_id)) AS parent_guid1 " +
                         ", c1.order_no AS order_no1 ";

            foreach (var col in source_cols) {
                if (Conv.ToInt(col["data_type"]).In(5, 6)) {
                    sql += ", STUFF((SELECT ', ' + CONVERT(NVARCHAR(36), i.guid) FROM items i " +
                           " WHERE i.item_id IN " +
                           " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                           " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                           " (SELECT ic.item_column_id FROM item_columns ic " +
                           " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) + "' AND ic.process = '" +
                           Conv.ToSql(list) + "' AND name = '" + Conv.ToSql(col["name"]) + "' " +
                           " ) ) ORDER BY i.guid ASC " +
                           " FOR XML PATH('')), 1, 2, '') AS " + Conv.ToSql(col["name"]) + "1 ";
                } else if (Conv.ToInt(col["data_type"]).In(0, 4) && Conv.ToStr(col["name"]) == "user_group" &&
                           Conv.ToStr(col["data_additional"]) == "user_group") {
                    sql += ", dbo.item_GetGuids(c1." + Conv.ToSql(col["name"]) + ") AS " + Conv.ToSql(col["name"]) +
                           "1 ";
                } else {
                    sql += ", c1." + Conv.ToSql(col["name"]) + " AS " + Conv.ToSql(col["name"]) + "1 ";
                }
            }

            sql += " FROM _" + Conv.ToSql(sourceInstance) + "_" + Conv.ToSql(list) + " c1 )";

            if (compare > 0) {
                sql += ", c2 AS ( " +
                       " SELECT (SELECT i.guid FROM " + Conv.ToSql(targetDb) +
                       ".dbo.items i WHERE i.item_id = c2.item_id) AS guid2 ";

                if (compare >= 2) {
                    sql += ", (SELECT i.guid FROM " + Conv.ToSql(targetDb) + ".dbo.items i WHERE i.item_id = " +
                           "  (SELECT si.parent_item_id FROM " + Conv.ToSql(targetDb) +
                           ".dbo.item_subitems si WHERE si.item_id = c2.item_id)) AS parent_guid2 " +
                           ", c2.order_no AS order_no2 ";
                    foreach (var col in source_cols) {
                        if (HasCol(target_cols, col)) {
                            if (Conv.ToInt(col["data_type"]).In(5, 6)) {
                                sql += ", STUFF((SELECT ', ' + CONVERT(NVARCHAR(36), i.guid) FROM " +
                                       Conv.ToSql(targetDb) + ".dbo.items i " +
                                       " WHERE i.item_id IN " +
                                       " (SELECT idps.selected_item_id FROM " + Conv.ToSql(targetDb) +
                                       ".dbo.item_data_process_selections idps " +
                                       " WHERE idps.item_id = c2.item_id AND idps.item_column_id = " +
                                       " (SELECT ic.item_column_id FROM " + Conv.ToSql(targetDb) +
                                       ".dbo.item_columns ic " +
                                       " WHERE ic.instance = '" + Conv.ToSql(targetInstance) + "' AND ic.process = '" +
                                       Conv.ToSql(list) + "' AND name = '" + Conv.ToSql(col["name"]) + "' " +
                                       " ) ) ORDER BY i.guid ASC " +
                                       " FOR XML PATH('')), 1, 2, '') AS " + Conv.ToSql(col["name"]) + "2 ";
                            } else if (Conv.ToInt(col["data_type"]).In(0, 4) && Conv.ToStr(col["name"]) == "user_group" &&
                                       Conv.ToStr(col["data_additional"]) == "user_group") {
                                sql += ", " + Conv.ToSql(targetDb) + ".dbo.item_GetGuids(c2." +
                                       Conv.ToSql(col["name"]) + ") AS " + Conv.ToSql(col["name"]) + "2 ";
                            } else {
                                sql += ", c2." + Conv.ToSql(col["name"]) + " AS " + Conv.ToSql(col["name"]) + "2 ";
                            }
                        } else {
                            sql += ", '' AS " + Conv.ToSql(col["name"]) + "2 ";
                        }
                    }
                }

                sql += " FROM " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_" +
                       Conv.ToSql(list) + " c2 ) ";
            }

            sql += " SELECT c1.guid1, c1.parent_guid1, c1.order_no1 ";

            if (compare > 0) {
                sql += ", c2.guid2 ";
            } else {
                sql += ", NULL AS guid2 ";
            }

            if (compare >= 2) {
                sql += ", c2.parent_guid2, c2.order_no2 ";
            }

            foreach (var col in source_cols) {
                sql += ", c1." + Conv.ToSql(col["name"]) + "1 ";

                if (HasCol(target_cols, col)) {
                    if (compare >= 2) {
                        sql += ", c2." + Conv.ToSql(col["name"]) + "2 ";
                    }
                }
            }

            sql += " FROM c1 ";

            if (compare > 0) {
                sql += " LEFT JOIN c2 ON c1.guid1 = c2.guid2 ";
            }

            if (compare >= 2) {
                sql += " WHERE c1.guid1 IS NOT NULL " +
                       " AND (c2.guid2 IS NULL " +
                       " OR c1.parent_guid1 <> c2.parent_guid2 " +
                       " OR c1.order_no1 <> c2.order_no2 ";

                foreach (var col in source_cols) {
                    if (HasCol(target_cols, col)) {
                        sql += " OR COALESCE(c1." + Conv.ToSql(col["name"]) + "1, '') <> COALESCE(c2." +
                               Conv.ToSql(col["name"]) + "2, '') ";
                    }
                }

                sql += ")";
            } else if (compare > 0) {
                sql += " WHERE c2.guid2 IS NULL ";
            }

            return db.GetDatatableDictionary(sql, DBParameters);
        }

        private bool HasCol(List<Dictionary<string, object>> list, Dictionary<string, object> dict) {
            foreach (Dictionary<string, object> item in list) {
                if (Conv.ToStr(item["name"]) == Conv.ToStr(dict["name"])) {
                    return true;
                }
            }

            return false;
        }

        public List<Dictionary<string, object>> MatrixCompare(string sourceInstance, string targetInstance,
            string targetDb, string processes, string process, int compare) {
            
            //This is done outside the loop
            //InstanceCompareProgress.SetCompareProgress("Matrices");
            
            
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                targetDb = Conv.ToSql(targetDb);
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);

                string sql = " WITH c1 AS (SELECT c1.item_id " +
                             " , dbo.item_GetGuid(c1.item_id) AS guid " +
                             " , c1.order_no " +
                             " , c1.type " +
                             " , dbo.item_GetGuid(c1.column_item_id) AS column_item_id_guid " +
                             " , dbo.item_GetGuid(c1.question_item_id) AS question_item_id_guid " +
                             " , c1.weighting " +
                             " , c1.name " +
                             " , c1.score " +
                             " , c1.tooltip " +
                             " FROM _" + Conv.ToSql(sourceInstance) + "_" + Conv.ToSql(process) + " c1 ) ";
                if (compare > 0) {
                    sql += " , c2 AS (SELECT c2.item_id " +
                           " , " + Conv.ToSql(targetDb) + ".dbo.item_GetGuid(c2.item_id) AS guid ";
                    if (compare >= 2) {
                        sql += " , c2.order_no " +
                               " , c2.type " +
                               " , " + Conv.ToSql(targetDb) +
                               ".dbo.item_GetGuid(c2.column_item_id) AS column_item_id_guid " +
                               " , " + Conv.ToSql(targetDb) +
                               ".dbo.item_GetGuid(c2.question_item_id) AS question_item_id_guid " +
                               " , c2.weighting " +
                               " , c2.name " +
                               " , c2.score " +
                               " , c2.tooltip ";
                    }

                    sql += " FROM " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_" +
                           Conv.ToSql(process) + " c2 ) ";
                }

                sql += " SELECT c1.item_id AS item_id1 " +
                       " , c1.guid AS guid1 " +
                       " , c1.order_no AS order_no1 " +
                       " , c1.type AS type1 " +
                       " , c1.column_item_id_guid AS column_item_id_guid1 " +
                       " , c1.question_item_id_guid AS question_item_id_guid1 " +
                       " , c1.weighting AS weighting1 " +
                       " , c1.name AS name1 " +
                       " , c1.score AS score1 " +
                       " , c1.tooltip AS tooltip1 ";
                if (compare > 0) {
                    sql += " , c2.item_id AS item_id2 " +
                           " , c2.guid AS guid2 ";
                } else {
                    sql += " , NULL AS item_id2 " +
                           " , NULL AS guid2 ";
                }

                if (compare >= 2) {
                    sql += " , c2.item_id AS item_id2 " +
                           " , c2.guid AS guid2 " +
                           " , c2.order_no AS order_no2 " +
                           " , c2.type AS type2 " +
                           " , c2.column_item_id_guid AS column_item_id_guid2 " +
                           " , c2.question_item_id_guid AS question_item_id_guid2 " +
                           " , c2.weighting AS weighting2 " +
                           " , c2.name AS name2 " +
                           " , c2.score AS score2 " +
                           " , c2.tooltip AS tooltip2 ";
                }

                sql += " FROM c1 ";
                if (compare > 0) {
                    sql += " LEFT JOIN c2 ON c1.guid = c2.guid ";
                }

                if (compare >= 2) {
                    sql += " WHERE c1.guid IS NOT NULL " +
                           " AND (c2.guid IS NULL " +
                           " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                           " OR COALESCE(c1.type, '') <> COALESCE(c2.type, '') " +
                           " OR ((c1.column_item_id_guid IS NULL AND c2.column_item_id_guid IS NOT NULL) OR (c1.column_item_id_guid IS NOT NULL AND c2.column_item_id_guid IS NULL) OR c1.column_item_id_guid <> c2.column_item_id_guid) " +
                           " OR ((c1.question_item_id_guid IS NULL AND c2.question_item_id_guid IS NOT NULL) OR (c1.question_item_id_guid IS NOT NULL AND c2.question_item_id_guid IS NULL) OR c1.question_item_id_guid <> c2.question_item_id_guid) " +
                           " OR COALESCE(c1.weighting, '') <> COALESCE(c2.weighting, '') " +
                           " OR COALESCE(c1.name, '') <> COALESCE(c2.name, '') " +
                           " OR COALESCE(c1.score, '') <> COALESCE(c2.score, '') " +
                           " OR COALESCE(c1.tooltip, '') <> COALESCE(c2.tooltip, '') " +
                           ")";
                } else if (compare > 0) {
                    sql += " AND c2.guid IS NULL ";
                }

                sql += " ORDER BY c1.order_no  ASC ";

                return db.GetDatatableDictionary(sql, DBParameters);
            }
        }

        public List<Dictionary<string, object>> UserGroupCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("User Groups");
            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT (SELECT guid FROM items WHERE item_id = c1.item_id) AS guid, c1.item_id " +
                " , c1.usergroup_name " +
                " , c1.order_no " +
                " , c1.all_group " +
                " , (SELECT ugt.list_item FROM _" + Conv.ToSql(sourceInstance) + "_user_group_type ugt " +
                " WHERE ugt.item_id = " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'group_type' " +
                " ) ) ) AS group_type " +
                " , (SELECT i.guid FROM items i " +
                " WHERE i.item_id = " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'group_type' " +
                " ) ) ) AS group_type_guid " +
                " , STUFF((SELECT ', ' + laag.list_item FROM _" + Conv.ToSql(sourceInstance) +
                "_list_azure_ad_groups laag " +
                " WHERE laag.item_id IN " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'azure_ad_groups' " +
                " ) ) FOR XML PATH('') ), 1, 2, '') AS azure_ad_groups " +
                " , STUFF((SELECT ',''' + CONVERT(NVARCHAR(36), i.guid) + '''' FROM items i " +
                " WHERE i.item_id IN " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'azure_ad_groups' " +
                " ) ) FOR XML PATH('') ), 1, 1, '') AS azure_ad_groups_guid " +
                " , STUFF((SELECT ', ' + lul.list_item FROM _" + Conv.ToSql(sourceInstance) +
                "_list_user_licences lul " +
                " WHERE lul.item_id IN " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'licence_categories' " +
                " ) ) FOR XML PATH('') ), 1, 2, '') AS licence_categories " +
                " , STUFF((SELECT ',''' + CONVERT(NVARCHAR(36), i.guid) + '''' FROM items i " +
                " WHERE i.item_id IN " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'licence_categories' " +
                " ) ) FOR XML PATH('') ), 1, 1, '') AS licence_categories_guid " +
                " , (SELECT lgyn.list_item FROM _" + Conv.ToSql(sourceInstance) + "_list_generic_yes_no lgyn " +
                " WHERE lgyn.item_id = " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'compare' " +
                " ) ) ) AS compare " +
                " , (SELECT i.guid FROM items i " +
                " WHERE i.item_id = " +
                " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                " (SELECT ic.item_column_id FROM item_columns ic " +
                " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                "' AND ic.process = 'user_group' AND name = 'compare' " +
                " ) ) ) AS compare_guid " +
                " FROM _" + Conv.ToSql(sourceInstance) + "_user_group c1 " +
                " ), c2 AS ( " +
                " SELECT (SELECT guid FROM " + Conv.ToSql(targetDb) +
                ".dbo.items WHERE item_id = c2.item_id) AS guid " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_user_group c2 " +
                ") SELECT c1.guid AS guid1, c2.guid AS guid2 " +
                ", c1.usergroup_name AS usergroup_name1 " +
                ", c1.order_no AS order_no1 " +
                ", c1.all_group AS all_group1 " +
                ", c1.group_type AS group_type1 " +
                ", c1.azure_ad_groups AS azure_ad_groups1 " +
                ", c1.group_type_guid AS group_type_guid1" +
                ", c1.azure_ad_groups_guid AS azure_ad_groups_guid1 " +
                ", c1.licence_categories AS licence_categories1" +
                ", c1.licence_categories_guid AS licence_categories_guid1 " +
                ", c1.compare AS compare1 " +
                ", c1.compare_guid AS compare_guid1 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.guid = c2.guid " +
                " WHERE c1.guid IS NOT NULL AND c2.guid IS NULL" +
                " AND ((c1.compare_guid = (SELECT TOP 1 i.guid FROM _" + Conv.ToSql(sourceInstance) +
                "_list_generic_yes_no lgyn INNER JOIN items i ON i.item_id = lgyn.item_id WHERE lgyn.value = 1 ORDER BY lgyn.item_id ASC)" +
                (processes.Length > 0 && !processes.Contains("'user_group'") ? " AND 0=1 " : "") + ") " +
                " OR (SELECT TOP 1 cug.item_id FROM condition_user_groups cug WHERE cug.item_id = c1.item_id" +
                (processes.Length > 0
                    ? " AND cug.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" + processes +
                      "))"
                    : "") + ") = c1.item_id" +
                (processes.Length > 0
                    ? ""
                    : " OR (SELECT TOP 1 imr.item_id FROM instance_menu_rights imr WHERE imr.item_id = c1.item_id) = c1.item_id") +
                ")" +
                " ORDER BY c1.item_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessContainersCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Containers");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.process, c1.variable, " +
                " c1.process_container_id AS id1, c2.process_container_id AS id2, " +
                " c1.rows AS rows1, c2.rows AS rows2, " +
                " c1.cols AS cols1, c2.cols AS cols2, " +
                " COALESCE(c1.maintenance_name, '') AS maintenance_name1, COALESCE(c2.maintenance_name, '') AS maintenance_name2, " +
                " c1.right_container AS right_container1, c2.right_container AS right_container2, " +
                " c1.hide_title AS hide_title1, c2.hide_title AS hide_title2 " +
                " FROM process_containers c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_containers c2 ON c2.instance = @targetInstance AND c1.process = c2.process and c1.variable = c2.variable " +
                " WHERE c1.instance = @sourceInstance " +
                " AND ( " +
                " c2.process_container_id IS NULL " +
                " OR coalesce(c1.rows, '') <> coalesce(c2.rows, '') " +
                " OR c1.cols <> c2.cols " +
                " OR COALESCE(c1.maintenance_name, '') <> COALESCE(c2.maintenance_name, '') " +
                " OR COALESCE(c1.right_container, '') <> COALESCE(c2.right_container, '') " +
                " OR COALESCE(c1.hide_title, '') <> COALESCE(c2.hide_title, '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.process_container_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessGroupsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Phases");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.process_group_id AS id1, c2.process_group_id AS id2, " +
                " c1.process, c1.variable, " +
                " c1.short_variable AS short_variable1, c2.short_variable AS short_variable2, " +
                " c1.order_no AS order_no1, c2.order_no AS order_no2, " +
                " c1.type AS type1, c2.type AS type2, " +
                " c1.maintenance_name maintenance_name1, c2.maintenance_name maintenance_name2 " +
                " FROM process_groups c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_groups c2 ON c2.instance = @targetInstance AND c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.instance = @sourceInstance " +
                " AND (c2.process_group_id IS NULL " +
                " OR COALESCE(c1.short_variable, '') <> COALESCE(c2.short_variable, '') " +
                " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                " OR c1.type <> c2.type " +
                " OR COALESCE(c1.maintenance_name, '') <> COALESCE(c2.maintenance_name, '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.process_group_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfoliosCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolios");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.process_portfolio_id AS id1, c2.process_portfolio_id AS id2, " +
                " c1.process, c1.variable, " +
                " c1.process_portfolio_type AS process_portfolio_type1, c2.process_portfolio_type AS process_portfolio_type2, " +
                " c1.calendar_type AS calendar_type1, c2.calendar_type AS calendar_type2, " +
                " dbo.itemColumn_GetVariable(c1.calendar_start_id) AS calendar_start_id1, " +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.calendar_start_id) AS calendar_start_id2, " +
                " dbo.itemColumn_GetVariable(c1.calendar_end_id) AS calendar_end_id1, " +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.calendar_end_id) AS calendar_end_id2, " +
                " c1.show_progress_arrows AS show_progress_arrows1, c2.show_progress_arrows AS show_progress_arrows2, " +
                " c1.open_split AS open_split1, c2.open_split AS open_split2, " +
                " c1.default_state AS default_state1, c2.default_state AS default_state2, " +
                " dbo.itemColumn_GetVariable(c1.recursive) AS recursive1, " +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.recursive) AS recursive2, " +
                " c1.recursive_filter_type AS recursive_filter_type1, c2.recursive_filter_type AS recursive_filter_type2, " +
                " c1.ignore_rights AS ignore_rights1, c2.ignore_rights AS ignore_rights2, " +
                " dbo.processGroup_GetVariables(c1.hidden_progress_arrows) AS hidden_progress_arrows1, " +
                Conv.ToSql(targetDb) +
                ".dbo.processGroup_GetVariables(c2.hidden_progress_arrows) AS hidden_progress_arrows2, " +
                " c1.use_default_state AS use_default_state1, c2.use_default_state AS use_default_state2, " +
                " dbo.itemColumn_GetVariable(c1.order_item_column_id) AS order_item_column_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.order_item_column_id) AS order_item_column_id2, " +
                " c1.order_direction AS order_direction1, c2.order_direction AS order_direction2, " +
                " dbo.itemColumn_GetVariable(c1.open_row_item_column_id) AS open_row_item_column_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.open_row_item_column_id) AS open_row_item_column_id2, " +
                " c1.open_row_process AS open_row_process1, c2.open_row_process AS open_row_process2, " +
                " c1.timemachine AS timemachine1, c2.timemachine AS timemachine2, " +
                " dbo.processPortfolio_DownloadOptionsIdReplace(c1.download_options) AS download_options1, " +
                Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_DownloadOptionsIdReplace(c2.download_options) AS download_options2, " +
                " c1.also_open_current AS also_open_current1, c2.also_open_current AS also_open_current2, " +
                " c1.sub_default_state AS sub_default_state1, c2.sub_default_state AS sub_default_state2, " +
                " c1.openwidth AS openwidth1, c2.openwidth AS openwidth2, " +
                " c1.portfolio_default_view AS portfolio_default_view1, c2.portfolio_default_view AS portfolio_default_view2 " +
                " FROM process_portfolios c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_portfolios c2 ON c2.instance = @targetInstance AND c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.instance = @sourceInstance " +
                " AND ( " +
                " c2.process_portfolio_id IS NULL " +
                " OR COALESCE(c1.process_portfolio_type, '') <> COALESCE(c2.process_portfolio_type, '') " +
                " OR COALESCE(c1.calendar_type, '') <> COALESCE(c2.calendar_type, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.calendar_start_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.calendar_start_id), '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.calendar_end_id), '') <> COALESCE(" + Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.calendar_end_id), '') " +
                " OR COALESCE(c1.show_progress_arrows, '') <> COALESCE(c2.show_progress_arrows, '') " +
                " OR COALESCE(c1.open_split, '') <> COALESCE(c2.open_split, '') " +
                " OR COALESCE(c1.default_state, '') <> COALESCE(c2.default_state, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.recursive), '') <> COALESCE(" + Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.recursive), '') " +
                " OR COALESCE(c1.recursive_filter_type, '') <> COALESCE(c2.recursive_filter_type, '') " +
                " OR COALESCE(c1.ignore_rights, '') <> COALESCE(c2.ignore_rights, '') " +
                " OR COALESCE(dbo.processGroup_GetVariables(c1.hidden_progress_arrows), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processGroup_GetVariables(c2.hidden_progress_arrows), '') " +
                " OR COALESCE(c1.use_default_state, '') <> COALESCE(c2.use_default_state, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.order_item_column_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.order_item_column_id), '') " +
                " OR COALESCE(c1.order_direction, '') <> COALESCE(c2.order_direction, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.open_row_item_column_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.open_row_item_column_id), '') " +
                " OR COALESCE(c1.open_row_process, '') <> COALESCE(c2.open_row_process, '') " +
                " OR COALESCE(c1.timemachine, '') <> COALESCE(c2.timemachine, '') " +
                " OR COALESCE(dbo.processPortfolio_DownloadOptionsIdReplace(c1.download_options), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processPortfolio_DownloadOptionsIdReplace(c2.download_options), '') " +
                " OR COALESCE(c1.also_open_current, '') <> COALESCE(c2.also_open_current, '') " +
                " OR COALESCE(c1.sub_default_state, '') <> COALESCE(c2.sub_default_state, '') " +
                " OR COALESCE(c1.openwidth, '') <> COALESCE(c2.openwidth, '') " +
                " OR COALESCE(c1.portfolio_default_view, '') <> COALESCE(c2.portfolio_default_view, '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.process_portfolio_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfoliosDownloadTemplatesCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolio Download Templates");
            string processes_join_sql = "";
            string processes_where_sql = "";
            if (processes.Length > 0) {
                processes_join_sql = " LEFT JOIN process_portfolios p1 ON p1.process_portfolio_id = c1.parent_id ";
                processes_where_sql = " AND p1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.attachment_id AS id1, c2.attachment_id AS id2, " +
                " c1.guid AS guid1, c2.guid AS guid2, " +
                " dbo.processPortfolio_GetVariable(c1.parent_id) AS parent_id1, " +
                Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetVariable(c2.parent_id) AS parent_id2, " +
                " c1.name AS name1, c2.name AS name2 " +
                " FROM attachments c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) + ".dbo.attachments c2 " +
                " ON c1.guid = c2.guid " +
                " AND c2.instance = @targetInstance AND c2.control_name = 'portfolioTemplates' AND c2.type = 0 " +
                processes_join_sql +
                " WHERE c1.instance = @sourceInstance AND c1.control_name = 'portfolioTemplates' AND c1.type = 0 " +
                " AND c2.attachment_id IS NULL AND c1.attachment_id IS NOT NULL " + processes_where_sql, DBParameters);
        }

        public List<Dictionary<string, object>> ProcessTabsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Tabs");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.process_tab_id AS id1, c2.process_tab_id AS id2, " +
                " c1.process, c1.variable, " +
                " c1.order_no AS order_no1, c2.order_no AS order_no2, " +
                " c1.use_in_new_item AS use_in_new_item1, c2.use_in_new_item AS use_in_new_item2, " +
                " c1.archive_enabled AS archive_enabled1, c2.archive_enabled AS archive_enabled2, " +
                " dbo.itemColumn_GetVariable(c1.archive_item_column_id) AS archive_item_column_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.archive_item_column_id) AS archive_item_column_id2, " +
                " c1.maintenance_name maintenance_name1, c2.maintenance_name maintenance_name2, " +
                " c1.background background1, c2.background background2 " +
                " FROM process_tabs c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_tabs c2 ON c2.instance = @targetInstance AND c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.instance = @sourceInstance " +
                " AND ( " +
                " c2.process_tab_id IS NULL " +
                " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                " OR COALESCE(c1.use_in_new_item, '') <> COALESCE(c2.use_in_new_item, '') " +
                " OR COALESCE(c1.archive_enabled, '') <> COALESCE(c2.archive_enabled, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.archive_item_column_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.archive_item_column_id), '') " +
                " OR COALESCE(c1.maintenance_name, '') <> COALESCE(c2.maintenance_name, '') " +
                " OR COALESCE(c1.background, '') <> COALESCE(c2.background, '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.process_tab_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessActionsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Actions");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.process_action_id AS id1, c2.process_action_id AS id2, " +
                " c1.process, c1.variable, " +
                " dbo.condition_GetVariable(c1.condition_id) AS condition_id1, " +
                Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c2.condition_id) AS condition_id2, " +
                " c1.use_action_history AS use_action_history1, c2.use_action_history AS use_action_history2, " +
                " dbo.itemColumn_GetVariable(c1.action_history_item_column_id) AS action_history_item_column_id1, " +
                " " + Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.action_history_item_column_id) AS action_history_item_column_id2, " +
                " c1.action_history_text AS action_history_text1, c2.action_history_text AS action_history_text2 " +
                " FROM process_actions c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_actions c2 ON c2.instance = @targetInstance AND c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.instance = @sourceInstance " +
                " AND ( " +
                " c2.process_action_id IS NULL " +
                " OR COALESCE(dbo.condition_GetVariable(c1.condition_id), '') <> COALESCE(" + Conv.ToSql(targetDb) +
                ".dbo.condition_GetVariable(c2.condition_id), '') " +
                " OR COALESCE(c1.use_action_history, '') <> COALESCE(c2.use_action_history, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.action_history_item_column_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.action_history_item_column_id), '') " +
                " OR COALESCE(c1.action_history_text, '') <> COALESCE(c2.action_history_text, '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.process_action_id ASC", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessActionsDialogCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Dialogs");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.process_action_dialog_id AS id1, c2.process_action_dialog_id AS id2, " +
                " c1.process, c1.variable, " +
                " c1.name name1, c2.name name2, " +
                " c1.type type1, c2.type type2, " +
                " c1.dialog_title_variable dialog_title_variable1, c2.dialog_title_variable dialog_title_variable2, " +
                " c1.dialog_text_variable dialog_text_variable1, c2.dialog_text_variable dialog_text_variable2, " +
                " c1.cancel_button_variable cancel_button_variable1, c2.cancel_button_variable cancel_button_variable2, " +
                " c1.submit_button_variable submit_button_variable1, c2.submit_button_variable submit_button_variable2, " +
                " dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.processContainer_GetVariable(c2.process_container_id) AS process_container_id2 " +
                " FROM process_actions_dialog c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_actions_dialog c2 ON c2.instance = @targetInstance AND c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.instance = @sourceInstance " +
                " AND ( " +
                " c2.process_action_dialog_id IS NULL " +
                " OR COALESCE(c1.name, '') <> COALESCE(c2.name, '') " +
                " OR COALESCE(c1.type, '') <> COALESCE(c2.type, '') " +
                " OR COALESCE(c1.dialog_title_variable, '') <> COALESCE(c2.dialog_title_variable, '') " +
                " OR COALESCE(c1.dialog_text_variable, '') <> COALESCE(c2.dialog_text_variable, '') " +
                " OR COALESCE(c1.cancel_button_variable, '') <> COALESCE(c2.cancel_button_variable, '') " +
                " OR COALESCE(c1.submit_button_variable, '') <> COALESCE(c2.submit_button_variable, '') " +
                " OR COALESCE(dbo.processContainer_GetVariable(c1.process_container_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processContainer_GetVariable(c2.process_container_id), '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.process_action_dialog_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessContainerColumnsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "", bool skipHelps = false) {
            InstanceCompareProgress.SetCompareProgress("Container Columns");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_container_id IN (SELECT process_container_id FROM process_containers WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_container_id IN (SELECT process_container_id FROM " +
                                 Conv.ToSql(targetDb) + ".dbo.process_containers WHERE process IN (" + processes +
                                 ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id " +
                " , dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id " +
                " , COALESCE(c1.placeholder, '') AS placeholder " +
                " , c1.validation AS validation " +
                " , COALESCE(c1.order_no, '') AS order_no " +
                " , COALESCE(c1.help_text, '') AS help_text " +
                " , c1.guid " +
                " , c1.process_container_column_id " +
                " FROM process_container_columns c1 " +
                " WHERE (c1.process_container_id IN (SELECT process_container_id FROM process_containers WHERE instance = @sourceInstance) " +
                " OR c1.item_column_id IN (SELECT item_column_id FROM item_columns WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id " +
                " , " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id " +
                " , COALESCE(c1.placeholder, '') AS placeholder " +
                " , c1.validation AS validation " +
                " , COALESCE(c1.order_no, '') AS order_no " +
                " , COALESCE(c1.help_text, '') AS help_text " +
                " , c1.guid " +
                " , c1.process_container_column_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_container_columns c1 " +
                " WHERE (c1.process_container_id IN (SELECT process_container_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_containers WHERE instance = @targetInstance) " +
                " OR c1.item_column_id IN (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.item_columns WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_container_id AS process_container_id1, c2.process_container_id AS process_container_id2, " +
                " c1.item_column_id AS item_column_id1, c2.item_column_id AS item_column_id2, " +
                " c1.placeholder AS placeholder1, c2.placeholder AS placeholder2, " +
                " dbo.processColumns_ValidationIdReplace(c1.validation) AS validation1, " + Conv.ToSql(targetDb) +
                ".dbo.processColumns_ValidationIdReplace(c2.validation) AS validation2, " +
                " c1.order_no AS order_no1, c2.order_no AS order_no2, " +
                " c1.help_text AS help_text1, c2.help_text AS help_text2, " +
                " c1.guid AS guid1, c2.guid AS guid2, " +
                " c1.process_container_column_id AS process_container_column_id1, c2.process_container_column_id AS process_container_column_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.guid = c2.guid " +
                " WHERE (c2.guid IS NULL OR c2.process_container_id IS NULL OR c2.item_column_id IS NULL " +
                " OR COALESCE(c1.process_container_id, '') <> coalesce(c2.process_container_id, '') " +
                " OR COALESCE(c1.item_column_id, '') <> coalesce(c2.item_column_id, '') " +
                " OR COALESCE(c1.placeholder, '') <> coalesce(c2.placeholder, '') " +
                " OR COALESCE(dbo.processColumns_ValidationIdReplace(c1.validation), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationIdReplace(c2.validation), '') " +
                " OR COALESCE(c1.order_no, '') <> coalesce(c2.order_no, '') " +
                (skipHelps ? "" : " OR COALESCE(c1.help_text, '') <> COALESCE(c2.help_text, '') ") +
                " ) AND c1.guid IS NOT NULL AND c1.process_container_id IS NOT NULL AND c1.item_column_id IS NOT NULL" +
                " ORDER BY c1.process_container_column_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfolioColumnGroupsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolio Groups");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT  " +
                " c1.portfolio_column_group_id AS id1, c2.portfolio_column_group_id AS id2, " +
                " c1.process, c1.variable, " +
                " dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c2.process_portfolio_id) AS process_portfolio_id2, " +
                " c1.order_no order_no1, c2.order_no order_no2, " +
                " c1.show_type show_type1, c2.show_type show_type2 " +
                " FROM process_portfolio_columns_groups c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.process_portfolio_columns_groups c2 ON c2.instance = @targetInstance AND c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.instance = @sourceInstance " +
                " AND ( " +
                " c2.portfolio_column_group_id IS NULL " +
                " OR COALESCE(dbo.processPortfolio_GetVariable(c1.process_portfolio_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetVariable(c2.process_portfolio_id), '') " +
                " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                " OR COALESCE(c1.show_type, '') <> COALESCE(c2.show_type, '') " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.portfolio_column_group_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfolioColumnsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolio Columns");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_portfolio_id IN (SELECT process_portfolio_id FROM " +
                                 Conv.ToSql(targetDb) + ".dbo.process_portfolios WHERE process IN (" + processes +
                                 ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " , dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id " +
                " , c1.process_portfolio_column_id " +
                " , c1.order_no " +
                " , c1.use_in_select_result " +
                " , c1.show_type " +
                " , c1.use_in_filter " +
                " , c1.archive_item_column_id " +
                " , c1.width " +
                " , c1.priority " +
                " , c1.sum_column " +
                " , c1.is_default " +
                " , c1.use_in_search " +
                " , c1.report_column " +
                " , c1.short_name " +
                " , c1.validation " +
                " , c1.portfolio_column_group_id " +
                " , c1.process_portfolio_id AS pc_id " +
                " FROM process_portfolio_columns c1 " +
                " WHERE (c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE instance = @sourceInstance) " +
                " OR c1.item_column_id IN (SELECT item_column_id FROM item_columns WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " ," + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id " +
                " , c1.order_no " +
                " , c1.use_in_select_result " +
                " , c1.show_type " +
                " , c1.use_in_filter " +
                " , c1.archive_item_column_id " +
                " , c1.width " +
                " , c1.priority " +
                " , c1.sum_column " +
                " , c1.is_default " +
                " , c1.use_in_search " +
                " , c1.report_column " +
                " , c1.short_name " +
                " , c1.validation " +
                " , c1.portfolio_column_group_id " +
                " , c1.process_portfolio_id AS pc_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_columns c1 " +
                " WHERE (c1.process_portfolio_id IN (SELECT process_portfolio_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_portfolios WHERE instance = @targetInstance) " +
                " OR c1.item_column_id IN (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.item_columns WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_portfolio_id, c1.item_column_id, c2.process_portfolio_id AS process_portfolio_id2, c2.item_column_id AS item_column_id2,  " +
                " c1.order_no AS order_no1, c2.order_no AS order_no2, " +
                " c1.use_in_select_result AS use_in_select_result1, c2.use_in_select_result AS use_in_select_result2, " +
                " c1.show_type AS show_type1, c2.show_type AS show_type2, " +
                " c1.use_in_filter AS use_in_filter1, c2.use_in_filter AS use_in_filter2, " +
                " dbo.itemColumn_GetVariable(c1.archive_item_column_id) AS archive_item_column_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.archive_item_column_id) AS archive_item_column_id2, " +
                " c1.width AS width1, c2.width AS width2, " +
                " c1.priority AS priority1, c2.priority AS priority2," +
                " c1.sum_column AS sum_column1, c2.sum_column AS sum_column2, " +
                " c1.is_default AS is_default1, c2.is_default AS is_default2, " +
                " c1.use_in_search AS use_in_search1, c2.use_in_search AS use_in_search2, " +
                " c1.report_column AS report_column1, c2.report_column AS report_column2, " +
                " c1.short_name AS short_name1, c2.short_name AS short_name2, " +
                " dbo.processColumns_ValidationIdReplace(c1.validation) AS validation1, " + Conv.ToSql(targetDb) +
                ".dbo.processColumns_ValidationIdReplace(c2.validation) AS validation2, " +
                " dbo.processPortfolioColumnGroup_GetVariable(c1.portfolio_column_group_id) AS portfolio_column_group_id1, " +
                Conv.ToSql(targetDb) +
                ".dbo.processPortfolioColumnGroup_GetVariable(c2.portfolio_column_group_id) AS portfolio_column_group_id2, " +
                " c1.pc_id AS pc_id1, c2.pc_id AS pc_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_portfolio_id = c2.process_portfolio_id AND c1.item_column_id = c2.item_column_id " +
                " WHERE (COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                " OR COALESCE(c1.use_in_select_result, '') <> COALESCE(c2.use_in_select_result, '') " +
                " OR COALESCE(c1.show_type, '') <> COALESCE(c2.show_type, '') " +
                " OR COALESCE(c1.use_in_filter, '') <> COALESCE(c2.use_in_filter, '') " +
                " OR COALESCE(dbo.itemColumn_GetVariable(c1.archive_item_column_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.archive_item_column_id), '') " +
                " OR COALESCE(c1.width, '') <> COALESCE(c2.width, '')" +
                " OR COALESCE(c1.priority, '') <> COALESCE(c2.priority, '')" +
                " OR COALESCE(c1.sum_column, '') <> COALESCE(c2.sum_column, '')" +
                " OR COALESCE(c1.is_default, '') <> COALESCE(c2.is_default, '')" +
                " OR COALESCE(c1.use_in_search, '') <> COALESCE(c2.use_in_search, '')" +
                " OR COALESCE(c1.report_column, '') <> COALESCE(c2.report_column, '')" +
                " OR COALESCE(c1.short_name, '') <> COALESCE(c2.short_name, '')" +
                " OR COALESCE(dbo.processColumns_ValidationIdReplace(c1.validation), '') <> COALESCE(" +
                Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationIdReplace(c2.validation), '') " +
                " OR COALESCE(dbo.processPortfolioColumnGroup_GetVariable(c1.portfolio_column_group_id), '') <> COALESCE(" +
                Conv.ToSql(targetDb) +
                ".dbo.processPortfolioColumnGroup_GetVariable(c2.portfolio_column_group_id), '')" +
                " OR c2.process_portfolio_id IS NULL) AND c1.process_portfolio_id IS NOT NULL" +
                " ORDER BY c1.process_portfolio_column_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfolioDialogsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolio Dialogs");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.process IN (" + processes + ") ";
                processes_sql2 = " AND c2.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.process_portfolio_dialog_id AS id1 " +
                " , c1.instance AS instance1 " +
                " , c1.process AS process1 " +
                " , dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id1 " +
                " , COALESCE(c1.order_no, '') AS order_no1 " +
                " , c1.variable AS variable1 " +
                " FROM process_portfolio_dialogs c1" +
                " WHERE c1.instance = @sourceInstance " + processes_sql1 +
                " ), c2 AS (" +
                " SELECT c2.process_portfolio_dialog_id AS id2 " +
                " , c2.instance AS instance2 " +
                " , c2.process AS process2 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processContainer_GetVariable(c2.process_container_id) AS process_container_id2 " +
                " , COALESCE(c2.order_no, '') AS order_no2 " +
                " , c2.variable AS variable2 " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_dialogs c2 " +
                " WHERE c2.instance = @targetInstance " + processes_sql2 +
                " ) " +
                " SELECT c1.id1, c2.id2, c1.instance1, c2.instance2, c1.process1, c2.process2 " +
                " , c1.process_container_id1, c2.process_container_id2, c1.order_no1, c2.order_no2, c1.variable1, c2.variable2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process1 = c2.process2 AND c1.variable1 = c2.variable2 " +
                " WHERE c1.variable1 IS NOT NULL " +
                " AND (c2.variable2 IS NULL " +
                " OR c1.process_container_id1 <> c2.process_container_id2 " +
                " OR c1.order_no1 <> c2.order_no2 " +
                " ) ORDER BY c1.id1 ASC", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfolioActionsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolio Actions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_action_id IN (SELECT process_action_id FROM process_actions WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c2.process_action_id IN (SELECT process_action_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.process_actions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " , dbo.ProcessAction_GetVariable(c1.process_action_id) AS process_action_id " +
                " , c1.button_text_variable " +
                " , c1.guid " +
                " , c1.process_portfolio_action_id " +
                " FROM process_portfolio_actions c1 " +
                " WHERE (c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE instance = @sourceInstance) " +
                " OR c1.process_action_id IN (SELECT process_action_id FROM process_actions WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c2.process_portfolio_id) AS process_portfolio_id " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.ProcessAction_GetVariable(c2.process_action_id) AS process_action_id " +
                " , c2.button_text_variable " +
                " , c2.guid " +
                " , c2.process_portfolio_action_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_actions c2 " +
                " WHERE (c2.process_portfolio_id IN (SELECT process_portfolio_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_portfolios WHERE instance = @targetInstance) " +
                " OR c2.process_action_id IN (SELECT process_action_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_actions WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_portfolio_id AS process_portfolio_id1, c2.process_portfolio_id AS process_portfolio_id2, " +
                " c1.process_action_id AS process_action_id1, c2.process_action_id AS process_action_id2, " +
                " c1.button_text_variable AS button_text_variable1, c2.button_text_variable AS button_text_variable2, " +
                " c1.guid AS guid1, c2.guid AS guid2, " +
                " c1.process_portfolio_action_id AS id1, c2.process_portfolio_action_id AS id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.guid = c2.guid " +
                " WHERE c1.guid IS NOT NULL AND c1.process_portfolio_id IS NOT NULL AND c1.process_action_id IS NOT NULL " +
                " AND (c2.guid IS NULL OR c2.process_portfolio_id IS NULL OR c2.process_action_id IS NULL" +
                " OR COALESCE(c1.process_portfolio_id, '') <> COALESCE(c2.process_portfolio_id, '') " +
                " OR COALESCE(c1.process_action_id, '') <> COALESCE(c2.process_action_id, '') " +
                " OR COALESCE(c1.button_text_variable, '') <> COALESCE(c2.button_text_variable, '') " +
                " ) ORDER BY c1.process_portfolio_action_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfolioGroupsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolio Groups");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_portfolio_id IN (SELECT process_portfolio_id FROM " +
                                 Conv.ToSql(targetDb) + ".dbo.process_portfolios WHERE process IN (" + processes +
                                 ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " , dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id " +
                " , c1.process_portfolio_id AS process_portfolio_id_id" +
                " , c1.process_group_id AS process_group_id_id" +
                " FROM process_portfolio_groups c1 " +
                " WHERE (c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE instance = @sourceInstance) " +
                " OR c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id " +
                " , c1.process_portfolio_id AS process_portfolio_id_id" +
                " , c1.process_group_id AS process_group_id_id" +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_groups c1 " +
                " WHERE (c1.process_portfolio_id IN (SELECT process_portfolio_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_portfolios WHERE instance = @targetInstance) " +
                " OR c1.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_groups WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_portfolio_id AS process_portfolio_id1, c2.process_portfolio_id AS process_portfolio_id1" +
                " , c1.process_group_id AS process_group_id1, c2.process_group_id AS process_group_id2 " +
                " , c1.process_portfolio_id_id AS process_portfolio_id_id1, c2.process_portfolio_id_id AS process_portfolio_id_id1" +
                " , c1.process_group_id_id AS process_group_id_id1, c2.process_group_id_id AS process_group_id_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_portfolio_id = c2.process_portfolio_id AND c1.process_group_id = c2.process_group_id " +
                " WHERE c2.process_portfolio_id IS NULL AND c2.process_group_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfolioUserGroupsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Portfolio User Groups");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_portfolio_id IN (SELECT process_portfolio_id FROM " +
                                 Conv.ToSql(targetDb) + ".dbo.process_portfolios WHERE process IN (" + processes +
                                 ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " , (SELECT guid FROM items WHERE item_id = c1.item_id) AS item_id " +
                " , c1.process_portfolio_id AS process_portfolio_id_id" +
                " , c1.item_id AS item_id_id" +
                " FROM process_portfolio_user_groups c1 " +
                " WHERE (c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE instance = @sourceInstance) " +
                " OR c1.item_id IN (SELECT item_id FROM items WHERE instance = @sourceInstance AND process = 'user_group')) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " , (SELECT guid FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE item_id = c1.item_id) AS item_id " +
                " , c1.process_portfolio_id AS process_portfolio_id_id" +
                " , c1.item_id AS item_id_id" +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_user_groups c1 " +
                " WHERE (c1.process_portfolio_id IN (SELECT process_portfolio_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_portfolios WHERE instance = @targetInstance) " +
                " OR c1.item_id IN (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.items WHERE instance = @targetInstance AND process = 'user_group')) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_portfolio_id AS process_portfolio_id1, c2.process_portfolio_id AS process_portfolio_id1" +
                " , c1.item_id AS item_id1, c2.item_id AS item_id2 " +
                " , c1.process_portfolio_id_id AS process_portfolio_id_id1, c2.process_portfolio_id_id AS process_portfolio_id_id1" +
                " , c1.item_id_id AS item_id_id1, c2.item_id AS item_id_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_portfolio_id = c2.process_portfolio_id AND c1.item_id = c2.item_id " +
                " WHERE c2.process_portfolio_id IS NULL AND c2.item_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessPortfolioTimemachineSavedCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Timemachine");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.process IN (" + processes + ") ";
                processes_sql2 = " AND c2.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.save_id, c1.process, c1.name, c1.date " +
                " FROM process_portfolio_timemachine_saved c1 " +
                " WHERE c1.instance = @sourceInstance " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.save_id, c2.process, c2.name, c2.date " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_timemachine_saved c2 " +
                " WHERE c2.instance = @targetInstance " +
                processes_sql2 +
                " ) " +
                " SELECT c1.save_id AS save_id1, c2.save_id AS save_id2, " +
                " c1.process AS process1, c2.process AS process2, " +
                " COALESCE(c1.name, '') AS name1, COALESCE(c2.name, '') AS name2, " +
                " c1.date AS date1, c2.date AS date2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process = c2.process AND c1.name = c2.name AND c1.date = c2.date " +
                " WHERE c2.save_id IS NULL " +
                " ORDER BY c1.save_id ASC ",
                DBParameters);
        }

        public List<Dictionary<string, object>> ProcessTabContainersCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Containers");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_tab_id IN (SELECT process_tab_id FROM process_tabs WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_tab_id IN (SELECT process_tab_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.process_tabs WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id  " +
                " , dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id " +
                " , c1.container_side, c1.order_no, c1.process_tab_container_id " +
                " FROM process_tab_containers c1 " +
                " WHERE (c1.process_tab_id IN (SELECT process_tab_id FROM process_tabs WHERE instance = @sourceInstance) " +
                " OR c1.process_container_id IN (SELECT process_container_id FROM process_containers WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id " +
                " ," + Conv.ToSql(targetDb) +
                ".dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id " +
                " , c1.container_side, c1.order_no " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_tab_containers c1 " +
                " WHERE (c1.process_tab_id in (SELECT process_tab_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_tabs WHERE instance = @targetInstance)  " +
                " OR c1.process_container_id in (SELECT process_container_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_containers WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_tab_id, c1.process_container_id, c2.process_tab_id AS process_tab_id2, c2.process_container_id AS process_container_id2, " +
                " c1.container_side AS container_side1, c2.container_side AS container_side2, c1.order_no AS order_no1, c2.order_no AS order_no2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_tab_id = c2.process_tab_id AND c1.process_container_id = c2.process_container_id " +
                " WHERE (" +
                " COALESCE(c1.container_side, '') <> COALESCE(c2.container_side, '') OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                " ) AND c1.process_tab_id IS NOT NULL AND c1.process_container_id IS NOT NULL " +
                " ORDER BY c1.process_tab_container_id ASC ",
                DBParameters);
        }

        public List<Dictionary<string, object>> ProcessActionRowsGroupsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Action Row Groups");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.process IN (" + processes + ") ";
                processes_sql2 = " AND c2.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.process_action_group_id " +
                " , c1.type " +
                " , c1.process " +
                " , c1.variable " +
                " , c1.process_in_group " +
                " , c1.in_use " +
                " , c1.order_no " +
                " FROM process_action_rows_groups c1 " +
                " WHERE c1.instance = @sourceInstance " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.process_action_group_id " +
                " , c2.type " +
                " , c2.process " +
                " , c2.variable " +
                " , c2.process_in_group " +
                " , c2.in_use " +
                " , c2.order_no " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_action_rows_groups c2 " +
                " WHERE c2.instance = @targetInstance " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_action_group_id AS id1, c2.process_action_group_id AS id2 " +
                " , c1.type AS type1, c2.type AS type2 " +
                " , c1.process AS process1, c2.process AS process2 " +
                " , c1.variable AS variable1, c2.variable AS variable2 " +
                " , c1.process_in_group AS process_in_group1, c2.process_in_group AS process_in_group2 " +
                " , c1.in_use AS in_use1, c2.in_use AS in_use2 " +
                " , c1.order_no AS order_no1, c2.order_no AS order_no2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.process_action_group_id IS NOT NULL " +
                " AND (c2.process_action_group_id IS NULL " +
                "  OR COALESCE(c1.type, '') <> COALESCE(c2.type, '') " +
                "  OR COALESCE(c1.process_in_group, '') <> COALESCE(c2.process_in_group, '') " +
                "  OR COALESCE(c1.in_use, '') <> COALESCE(c2.in_use, '') " +
                "  OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                " ) ",
                DBParameters);
        }

        public List<Dictionary<string, object>> ProcessActionRowsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Action Rows");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_action_id IN (SELECT process_action_id FROM process_actions WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c2.process_action_id IN (SELECT process_action_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.process_actions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.process_action_row_id " +
                " , dbo.processAction_GetVariable(c1.process_action_id) AS process_action_id " +
                " , c1.process_action_type " +
                " , dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id " +
                " , c1.value_type " +
                " , dbo.processActionRows_ValueIdReplace(c1.item_column_id, c1.process_action_type, c1.value_type, 1, c1.value) AS value " +
                " , dbo.processActionRows_ValueIdReplace(c1.item_column_id, c1.process_action_type, c1.value_type, 2, c1.value2) AS value2 " +
                " , dbo.processActionRows_ValueIdReplace(c1.item_column_id, c1.process_action_type, c1.value_type, 3, c1.value3) AS value3 " +
                " , dbo.itemColumn_GetVariable(c1.user_item_column_id) AS user_item_column_id " +
                " , dbo.itemColumn_GetVariable(c1.link_item_column_id) AS link_item_column_id " +
                " , dbo.condition_GetVariable(c1.condition_id) AS condition_id  " +
                " , c1.guid " +
                " , dbo.processActionRows_ValueIdReplace(c1.item_column_id, c1.process_action_type, c1.value_type, 0, c1.split_options) AS split_options " +
                " , dbo.processActionRowsGroups_GetVariable(c1.action_group_id) AS action_group_id " +
                " , c1.order_no " +
                " , c1.notification_type " +
                " , c1.exclude_sender " +
                " FROM process_action_rows c1 " +
                " WHERE c1.process_action_id IN (SELECT process_action_id FROM process_actions WHERE instance = @sourceInstance)" +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.process_action_row_id " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processAction_GetVariable(c2.process_action_id) AS process_action_id " +
                " , c2.process_action_type " +
                " , " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.item_column_id) AS item_column_id " +
                " , c2.value_type " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processActionRows_ValueIdReplace(c2.item_column_id, c2.process_action_type, c2.value_type, 1, c2.value) AS value " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processActionRows_ValueIdReplace(c2.item_column_id, c2.process_action_type, c2.value_type, 2, c2.value2) AS value2 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processActionRows_ValueIdReplace(c2.item_column_id, c2.process_action_type, c2.value_type, 3, c2.value3) AS value3 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.user_item_column_id) AS user_item_column_id " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.link_item_column_id) AS link_item_column_id " +
                " , " + Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c2.condition_id) AS condition_id " +
                " , c2.guid " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processActionRows_ValueIdReplace(c2.item_column_id, c2.process_action_type, c2.value_type, 0, c2.split_options) AS split_options " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processActionRowsGroups_GetVariable(c2.action_group_id) AS action_group_id  " +
                " , c2.order_no " +
                " , c2.notification_type " +
                " , c2.exclude_sender " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_action_rows c2 " +
                " WHERE c2.process_action_id IN (SELECT process_action_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_actions WHERE instance = @targetInstance) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_action_row_id, c2.process_action_row_id AS process_action_row_id2, c1.process_action_id AS process_action_id1, c1.process_action_type AS process_action_type1, c1.item_column_id AS item_column_id1, c1.value_type AS value_type1, c1.value AS value1, c1.value2 AS value2, c1.value3 AS value3, " +
                " c2.process_action_id AS process_action_id2, c2.process_action_type AS process_action_type2, c2.item_column_id AS item_column_id2 , c2.value_type AS value_type2, c2.value AS value1_2, c2.value2 AS value2_2, c2.value3 AS value3_2, " +
                " c1.user_item_column_id AS user_item_column_id1, c2.user_item_column_id AS user_item_column_id2, " +
                " c1.link_item_column_id AS link_item_column_id1, c2.link_item_column_id AS link_item_column_id2, " +
                " c1.condition_id AS condition_id1, c2.condition_id AS condition_id2, " +
                " c1.guid AS guid1, c2.guid AS guid2, " +
                " c1.split_options AS split_options1, c2.split_options AS split_options2, " +
                " ISNULL(c1.action_group_id, 0) AS action_group_id1, ISNULL(c2.action_group_id, 0) AS action_group_id2, " +
                " c1.order_no AS order_no1, c2.order_no AS order_no2, " +
                " c1.notification_type AS notification_type1, c2.notification_type AS notification_type2, " +
                " c1.exclude_sender AS exclude_sender1, c2.exclude_sender AS exclude_sender2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.guid = c2.guid " +
                " WHERE c1.guid IS NOT NULL AND c1.process_action_row_id IS NOT NULL AND (c2.guid IS NULL OR c2.process_action_row_id IS NULL " +
                " OR c1.process_action_id <> c2.process_action_id " +
                " OR c1.process_action_type <> c2.process_action_type " +
                " OR COALESCE(c1.item_column_id, '') <> COALESCE(c2.item_column_id, '') " +
                " OR COALESCE(c1.value_type, '') <> COALESCE(c2.value_type, '') " +
                " OR COALESCE(c1.value, '') <> COALESCE(c2.value, '') " +
                " OR COALESCE(c1.value2, '') <> COALESCE(c2.value2, '') " +
                " OR COALESCE(c1.value3, '') <> COALESCE(c2.value3, '') " +
                " OR COALESCE(c1.user_item_column_id, '') <> COALESCE(c2.user_item_column_id, '') " +
                " OR COALESCE(c1.link_item_column_id, '') <> COALESCE(c2.link_item_column_id, '') " +
                " OR COALESCE(c1.condition_id, '') <> COALESCE(c2.condition_id, '') " +
                " OR COALESCE(c1.split_options, '') <> COALESCE(c2.split_options, '') " +
                " OR COALESCE(c1.action_group_id, '') <> COALESCE(c2.action_group_id, '') " +
                " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                " OR COALESCE(c1.notification_type, '') <> COALESCE(c2.notification_type, '') " +
                " OR COALESCE(c1.exclude_sender, '') <> COALESCE(c2.exclude_sender, '') " +
                " ) ORDER BY c1.process_action_row_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ItemColumnJoinsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Joins");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.item_column_id IN (SELECT item_column_id FROM item_columns WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.item_column_id IN (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.item_columns WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT item_column_join_id, " +
                " dbo.itemcolumn_GetVariable(c1.item_column_id) AS item_column_id, " +
                " parent_column_type, " +
                " dbo.itemcolumn_GetVariable(c1.parent_item_column_id) AS parent_item_column_id, " +
                " parent_additional_join, " +
                " sub_column_type, " +
                " dbo.itemcolumn_GetVariable(c1.sub_item_column_id) AS sub_item_column_id, " +
                " sub_additional_join " +
                " FROM item_column_joins c1 " +
                " WHERE c1.item_column_id in (SELECT item_column_id FROM item_columns WHERE instance = @sourceInstance) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT item_column_join_id, " +
                Conv.ToSql(targetDb) + ".dbo.itemcolumn_GetVariable(c1.item_column_id) AS item_column_id, " +
                " parent_column_type, " +
                Conv.ToSql(targetDb) +
                ".dbo.itemcolumn_GetVariable(c1.parent_item_column_id) AS parent_item_column_id, " +
                " parent_additional_join, " +
                " sub_column_type, " +
                Conv.ToSql(targetDb) + ".dbo.itemcolumn_GetVariable(c1.sub_item_column_id) AS sub_item_column_id, " +
                " sub_additional_join " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.item_column_joins c1 " +
                " WHERE c1.item_column_id IN (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.item_columns WHERE instance = @targetInstance) " +
                processes_sql2 +
                " ) " +
                " SELECT  c1.item_column_join_id, " +
                " c1.item_column_id, " +
                " c1.parent_column_type, " +
                " c1.parent_item_column_id, " +
                " c1.parent_additional_join, " +
                " c1.sub_column_type, " +
                " c1.sub_item_column_id, " +
                " c1.sub_additional_join, " +
                " c2.item_column_id AS item_column_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON " +
                " COALESCE(c1.item_column_id, '') = COALESCE(c2.item_column_id, '') AND " +
                " c1.parent_column_type = c2.parent_column_type AND " +
                " COALESCE(c1.parent_item_column_id, '') = COALESCE(c2.parent_item_column_id, '') AND " +
                " c1.parent_additional_join = c2.parent_additional_join AND " +
                " c1.sub_column_type = c2.sub_column_type AND  " +
                " COALESCE(c1.sub_item_column_id, '') = COALESCE(c2.sub_item_column_id, '') AND " +
                " c1.sub_additional_join = c2.sub_additional_join " +
                " WHERE c2.item_column_id IS NULL " +
                " ORDER BY c1.item_column_join_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ItemColumnEquationsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Equations");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.item_column_id IN (SELECT item_column_id FROM item_columns WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c2.item_column_id IN (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.item_columns WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.item_column_equation_id AS id1 " +
                " , c1.guid AS guid1 " +
                " , dbo.itemColumn_GetVariable(c1.parent_item_column_id) AS parent_item_column_id1 " +
                " , c1.column_type AS column_type1 " +
                " , dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id1 " +
                " , c1.sql_alias AS sql_alias1 " +
                " , c1.sql_function AS sql_function1 " +
                " , dbo.itemColumnEquations_ParamIdReplace(c1.column_type, c1.sql_function, 1, c1.sql_param1) AS sql_param1_1 " +
                " , dbo.itemColumnEquations_ParamIdReplace(c1.column_type, c1.sql_function, 2, c1.sql_param2) AS sql_param2_1 " +
                " , c1.sql_param3 AS sql_param3_1 " +
                " FROM item_column_equations c1 " +
                " WHERE c1.parent_item_column_id IN (SELECT parent_item_column_id FROM item_columns WHERE instance = @sourceInstance) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.item_column_equation_id AS id2 " +
                " , c2.guid AS guid2 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.itemColumn_GetVariable(c2.parent_item_column_id) AS parent_item_column_id2 " +
                " , c2.column_type AS column_type2 " +
                " , " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c2.item_column_id) AS item_column_id2 " +
                " , c2.sql_alias AS sql_alias2 " +
                " , c2.sql_function AS sql_function2 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.itemColumnEquations_ParamIdReplace(c2.column_type, c2.sql_function, 1, c2.sql_param1) AS sql_param1_2 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.itemColumnEquations_ParamIdReplace(c2.column_type, c2.sql_function, 2, c2.sql_param2) AS sql_param2_2 " +
                " , c2.sql_param3 AS sql_param3_2 " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.item_column_equations c2 " +
                " WHERE c2.parent_item_column_id IN (SELECT parent_item_column_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.item_columns WHERE instance = @targetInstance) " +
                processes_sql2 +
                "  ) " +
                " SELECT c1.id1, c2.id2, c1.guid1, c2.guid2, c1.parent_item_column_id1, c2.parent_item_column_id2, c1.column_type1, c2.column_type2 " +
                " , c1.item_column_id1, c2.item_column_id2, c1.sql_alias1, c2.sql_alias2, c1.sql_function1, c2.sql_function2 " +
                " , c1.sql_param1_1, c2.sql_param1_2, c1.sql_param2_1, c2.sql_param2_2, c1.sql_param3_1, c2.sql_param3_2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.guid1 = c2.guid2 " +
                " WHERE c1.id1 IS NOT NULL AND (c2.id2 IS NULL " +
                " OR COALESCE(c1.parent_item_column_id1,'') <> COALESCE(c2.parent_item_column_id2, '') " +
                " OR c1.column_type1 <> c2.column_type2 " +
                " OR COALESCE(c1.item_column_id1, '') <> COALESCE(c2.item_column_id2, '') " +
                " OR c1.sql_alias1 <> c2.sql_alias2 " +
                " OR COALESCE(c1.sql_param1_1, '') <> COALESCE(c2.sql_param1_2, '') " +
                " OR COALESCE(c1.sql_param2_1, '') <> COALESCE(c2.sql_param2_2, '') " +
                " OR COALESCE(c1.sql_param3_1, '') <> COALESCE(c2.sql_param3_2, '') " +
                " ) ORDER BY c1.id1 ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessGroupFeaturesCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Features");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.process_groups WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id   " +
                " , feature, state, custom_name " +
                " FROM process_group_features c1 " +
                " WHERE c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE instance = @sourceInstance) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id " +
                " , feature, state, custom_name " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_group_features c1 " +
                " WHERE c1.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_groups WHERE instance = @targetInstance) " +
                processes_sql2 +
                "  )   " +
                " SELECT c1.process_group_id process_group_id1, c2.process_group_id process_group_id2 " +
                " , c1.feature AS feature1, c1.state AS state1, c1.custom_name AS custom_name1 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_group_id = c2.process_group_id AND c1.feature = c2.feature AND c1.state = c2.state AND COALESCE(c1.custom_name, '') = COALESCE(c2.custom_name, '') " +
                " WHERE c2.process_group_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessGroupTabsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Tabs");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.process_groups WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id, " +
                " dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id, order_no, COALESCE(tag, '') AS tag, " +
                " process_group_tab_id " +
                " FROM process_group_tabs c1 " +
                " WHERE (c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE instance = @sourceInstance) " +
                " OR c1.process_tab_id IN (SELECT process_tab_id FROM process_tabs WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id, " +
                Conv.ToSql(targetDb) +
                ".dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id, order_no, COALESCE(tag, '') AS tag, " +
                " process_group_tab_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_group_tabs c1 " +
                " WHERE (c1.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_groups WHERE instance = @targetInstance)   " +
                " OR c1.process_tab_id IN (SELECT process_tab_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_tabs WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_group_id AS process_group_id1, c2.process_group_id AS process_group_id2, c1.process_tab_id AS process_tab_id1, c2.process_tab_id AS process_tab_id2" +
                " , c1.order_no AS order_no1, c2.order_no AS order_no2, c1.tag AS tag1, c2.tag AS tag2, c1.process_group_tab_id " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_group_id = c2.process_group_id AND c1.process_tab_id = c2.process_tab_id " +
                " AND c1.order_no = c2.order_no AND c1.tag = c2.tag " +
                " WHERE c2.process_group_id IS NULL AND c1.process_tab_id IS NOT NULL " +
                " ORDER BY c1.process_group_tab_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessGroupNavigationCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Navigation");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c2.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.process_groups WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS (" +
                " SELECT dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id " +
                " , dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id " +
                " , c1.variable, c1.default_state, c1.order_no AS order_no, c1.type_id, c1.icon " +
                " , c1.process_navigation_id " +
                " , dbo.processPortfolio_GetVariable(c1.template_portfolio_id) AS template_portfolio_id " +
                " FROM process_group_navigation c1 " +
                " WHERE c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE instance = @sourceInstance) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processGroup_GetVariable(c2.process_group_id) AS process_group_id " +
                " , " + Conv.ToSql(targetDb) + ".dbo.processTab_GetVariable(c2.process_tab_id) AS process_tab_id " +
                " , c2.variable, c2.default_state, c2.order_no AS order_no, c2.type_id, c2.icon " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c2.template_portfolio_id) AS template_portfolio_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_group_navigation c2 " +
                " WHERE c2.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_groups WHERE instance = @targetInstance) " +
                processes_sql2 +
                "  ) " +
                " SELECT c1.process_group_id AS process_group_id1, c2.process_group_id AS process_group_id2, c1.variable AS variable1, c2.variable AS variable2 " +
                " , c1.process_tab_id AS process_tab_id1, c2.process_tab_id AS process_tab_id2, c1.default_state AS default_state1, c2.default_state AS default_state2 " +
                " , c1.order_no AS order_no1, c2.order_no AS order_no2, c1.type_id AS type_id1, c2.type_id AS type_id2, c1.icon AS icon1, c2.icon AS icon2 " +
                " , c1.process_navigation_id, c1.template_portfolio_id AS template_portfolio_id1, c2.template_portfolio_id AS template_portfolio_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_group_id = c2.process_group_id AND c1.variable = c2.variable " +
                " WHERE (c2.process_group_id IS NULL " +
                " OR COALESCE(c1.icon, '') <> COALESCE(c2.icon, '') " +
                " OR COALESCE(c1.template_portfolio_id, '') <> COALESCE(c2.template_portfolio_id, '') " +
                " ) AND c1.process_tab_id IS NOT NULL " +
                " ORDER BY c1.process_navigation_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessTabColumnsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Layout Columns");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.item_column_id IN (SELECT item_column_id FROM item_columns WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.item_column_id IN (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.item_columns WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id,  " +
                " dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id, " +
                " c1.process_tab_column_id, " +
                " COALESCE(c1.order_no, '') AS order_no " +
                " FROM process_tab_columns c1 " +
                " WHERE (c1.item_column_id IN (SELECT item_column_id FROM item_columns WHERE instance = @sourceInstance) " +
                " OR c1.process_tab_id IN (SELECT process_tab_id FROM process_tabs WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id, " +
                Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id, " +
                " c1.process_tab_column_id, " +
                " COALESCE(c1.order_no, '') AS order_no " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_tab_columns c1 " +
                " WHERE (c1.item_column_id IN (SELECT item_column_id FROM item_columns WHERE instance = @targetInstance)" +
                " OR c1.process_tab_id IN (SELECT process_tab_id FROM process_tabs WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_tab_column_id, " +
                " c1.process_tab_id process_tab_id1, c2.process_tab_id process_tab_id2, c1.item_column_id item_column_id1, " +
                " c1.order_no AS order_no1, c2.order_no AS order_no2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_tab_id = c2.process_tab_id AND c1.item_column_id = c2.item_column_id " +
                " AND c1.order_no = c2.order_no " +
                " WHERE c2.process_tab_id IS NULL " +
                " ORDER BY c1.process_tab_column_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessTabsSubprocessesCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Tab Sub Processes");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_tab_id IN (SELECT process_tab_id FROM process_tabs WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c1.process_tab_id IN (SELECT process_tab_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.process_tabs WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id, c1.process_tab_subprocess_id, " +
                " c1.process, c1.instance, COALESCE(c1.order_no, '') AS order_no, c1.process_side, c1.guid " +
                " FROM process_tabs_subprocesses c1 " +
                " WHERE c1.instance = @sourceInstance " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id, c1.process_tab_subprocess_id, " +
                " c1.process, c1.instance, COALESCE(c1.order_no, '') AS order_no, c1.process_side, c1.guid " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_tabs_subprocesses c1 " +
                " WHERE c1.instance = @targetInstance " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_tab_subprocess_id AS id1, c2.process_tab_subprocess_id AS id2, " +
                " c1.process_tab_id AS process_tab_id1, c2.process_tab_id AS process_tab_id2, " +
                " c1.process AS process1, c2.process AS process2, " +
                " c1.instance AS instance1, c2.instance AS instance2, " +
                " c1.order_no AS order_no1, c2.order_no AS order_no2, " +
                " c1.process_side AS process_side1, c2.process_side AS process_side2, " +
                " c1.guid AS guid1, c2.guid AS guid2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_tab_id = c2.process_tab_id AND c1.process = c2.process AND c1.instance = c2.instance " +
                " AND c1.order_no = c2.order_no AND c1.process_side = c2.process_side AND c1.guid = c2.guid " +
                " WHERE c2.process_tab_subprocess_id IS NULL " +
                " ORDER BY c1.process_tab_subprocess_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessTabsSubprocessDataCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Subprocess Data");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.parent_process_item_id IN (SELECT item_id FROM items WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.parent_process_item_id IN (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.items WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT (SELECT guid FROM process_tabs_subprocesses WHERE process_tab_subprocess_id = c1.process_tab_subprocess_id) AS process_tab_subprocess_id " +
                " , (SELECT guid FROM items WHERE item_id = c1.parent_process_item_id) AS parent_process_item_id " +
                " , (SELECT guid FROM items WHERE item_id = c1.item_id) AS item_id " +
                " , c1.process_tab_subprocess_id AS process_tab_subprocess_id_id " +
                " , c1.parent_process_item_id AS parent_process_item_id_id " +
                " , c1.item_id AS item_id_id " +
                " FROM process_tabs_subprocess_data c1 " +
                " WHERE (c1.parent_process_item_id IN (SELECT item_id FROM items WHERE instance = @sourceInstance) " +
                " OR c1.item_id IN (SELECT item_id FROM items WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT (SELECT guid FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_tabs_subprocesses WHERE process_tab_subprocess_id = c1.process_tab_subprocess_id) AS process_tab_subprocess_id " +
                " , (SELECT guid FROM " + Conv.ToSql(targetDb) +
                ".dbo.items WHERE item_id = c1.parent_process_item_id) AS parent_process_item_id " +
                " , (SELECT guid FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE item_id = c1.item_id) AS item_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_tabs_subprocess_data c1 " +
                " WHERE (c1.parent_process_item_id IN (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.items WHERE instance = @targetInstance) " +
                " OR c1.item_id IN (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.items WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_tab_subprocess_id AS process_tab_subprocess_id1, c2.process_tab_subprocess_id AS process_tab_subprocess_id2, " +
                " c1.parent_process_item_id AS parent_process_item_id1, c2.parent_process_item_id AS parent_process_item_id2, " +
                " c1.item_id AS item_id1, c2.item_id AS item_id2, " +
                " c1.process_tab_subprocess_id_id AS process_tab_subprocess_id_id1, " +
                " c1.parent_process_item_id_id AS parent_process_item_id_id1, " +
                " c1.item_id_id AS item_id_id1 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_tab_subprocess_id = c2.process_tab_subprocess_id " +
                " AND c1.parent_process_item_id = c2.parent_process_item_id AND c1.item_id = c2.item_id " +
                " WHERE c2.process_tab_subprocess_id IS NULL " +
                " ORDER BY c1.process_tab_subprocess_id_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessDashboardsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Dashboards");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " WHERE c1.process IN (" + processes + ") ";
                processes_sql2 = " WHERE c2.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.process_dashboard_id AS id1 " +
                " , c1.instance AS instance1 " +
                " , c1.process AS process1 " +
                " , c1.variable AS variable1 " +
                " , c1.name AS name1 " +
                " , dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id1 " +
                " FROM process_dashboards c1 " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.process_dashboard_id AS id2 " +
                " , c2.instance AS instance2 " +
                " , c2.process AS process2 " +
                " , c2.variable AS variable2 " +
                " , c2.name AS name2 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c2.process_portfolio_id) AS process_portfolio_id2 " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_dashboards c2 " +
                processes_sql2 +
                " ) " +
                " SELECT c1.id1, c2.id2, c1.instance1, c2.instance2, c1.process1, c2.process2, c1.variable1, c2.variable2 " +
                " , c1.name1, c2.name2, c1.process_portfolio_id1, c2.process_portfolio_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process1 = c2.process2 AND c1.variable1 = c2.variable2 " +
                " WHERE c1.id1 IS NOT NULL " +
                " AND (c2.id2 IS NULL " +
                " OR c1.name1 <> c2.name2 " +
                " OR c1.process_portfolio_id1 <> c2.process_portfolio_id2 " +
                " ) ORDER BY c1.id1 ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessDashboardChartsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Charts");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " WHERE c1.process IN (" + processes + ") ";
                processes_sql2 = " WHERE c2.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.process_dashboard_chart_id AS id1 " +
                " , c1.instance AS instance1 " +
                " , c1.process AS process1 " +
                " , c1.variable AS variable1 " +
                " , c1.name AS name1 " +
                " , dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id1 " +
                " , c1.json_data AS json_data1 " +
                " FROM process_dashboard_charts c1" +
                processes_sql1 +
                " ), c2 AS (" +
                " SELECT c2.process_dashboard_chart_id AS id2 " +
                " , c2.instance AS instance2 " +
                " , c2.process AS process2 " +
                " , c2.variable AS variable2 " +
                " , c2.name AS name2 " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c2.process_portfolio_id) AS process_portfolio_id2 " +
                " , c2.json_data AS json_data2 " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_dashboard_charts c2 " +
                processes_sql2 +
                " ) " +
                " SELECT c1.id1, c2.id2, c1.instance1, c2.instance2, c1.process1, c2.process2, c1.variable1, c2.variable2 " +
                " , c1.name1, c2.name2, c1.process_portfolio_id1, c2.process_portfolio_id2, c1.json_data1, c2.json_data2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process1 = c2.process2 AND c1.variable1 = c2.variable2 " +
                " WHERE c1.id1 IS NOT NULL " +
                " AND (c2.id2 IS NULL " +
                " OR c1.name1 <> c2.name2 " +
                " OR c1.process_portfolio_id1 <> c2.process_portfolio_id2 " +
                " OR c1.json_data1 <> c2.json_data2 " +
                " ) ORDER BY c1.id1 ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessDashboardChartMapCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Chart Map");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_dashboard_id IN (SELECT process_dashboard_id FROM process_dashboards WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c2.process_dashboard_id IN (SELECT process_dashboard_id FROM " +
                                 Conv.ToSql(targetDb) + ".dbo.process_dashboards WHERE process IN (" + processes +
                                 ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processDashboardChart_GetVariable(c1.process_dashboard_chart_id) AS process_dashboard_chart1 " +
                " , dbo.processDashboard_GetVariable(c1.process_dashboard_id) AS process_dashboard1 " +
                " , c1.order_no AS order_no1 " +
                " , c1.process_dashboard_chart_id AS process_dashboard_chart_id1 " +
                " , c1.process_dashboard_id AS process_dashboard_id1 " +
                " FROM process_dashboard_chart_map c1 " +
                " WHERE (c1.process_dashboard_chart_id IN (SELECT process_dashboard_chart_id FROM process_dashboard_charts WHERE instance = @sourceInstance) " +
                " OR c1.process_dashboard_id IN (SELECT process_dashboard_id FROM process_dashboards WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processDashboardChart_GetVariable(c2.process_dashboard_chart_id) AS process_dashboard_chart2 " +
                " ," + Conv.ToSql(targetDb) +
                ".dbo.processDashboard_GetVariable(c2.process_dashboard_id) AS process_dashboard2 " +
                " , c2.order_no AS order_no2 " +
                " , c2.process_dashboard_chart_id AS process_dashboard_chart_id2 " +
                " , c2.process_dashboard_id AS process_dashboard_id2 " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_dashboard_chart_map c2 " +
                " WHERE (c2.process_dashboard_chart_id IN (SELECT process_dashboard_chart_id FROM " +
                Conv.ToSql(targetDb) + ".dbo.process_dashboard_charts WHERE instance = @targetInstance) " +
                " OR c2.process_dashboard_id IN (SELECT process_dashboard_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_dashboards WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process_dashboard_chart1, c1.process_dashboard1, c1.order_no1 " +
                " , c1.process_dashboard_chart_id1, c1.process_dashboard_id1 " +
                " , c2.process_dashboard_chart2, c2.process_dashboard2, c2.order_no2 " +
                " , c2.process_dashboard_chart_id2, c2.process_dashboard_id2 " +
                " , c1.process_dashboard_chart1 + '_' + c1.process_dashboard1 AS id1 " +
                " , NULL AS id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process_dashboard_chart1 = c2.process_dashboard_chart2 " +
                " AND c1.process_dashboard1 = c2.process_dashboard2 " +
                " AND c1.order_no1 = c2.order_no2 " +
                " WHERE c2.process_dashboard_chart2 IS NULL AND c2.process_dashboard2 IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessDashboardChartLinksCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Chart Links");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.process IN (" + processes + ") ";
                processes_sql2 = " AND c2.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.process_dashboard_chart_link_id AS id1 " +
                " , c1.instance AS instance1 " +
                " , c1.process AS process1 " +
                " , dbo.processDashboard_GetVariable(c1.dashboard_id) AS dashboard1 " +
                " , c1.dashboard_id AS dashboard_id1 " +
                " , c1.source_field AS source_field1 " +
                " , c1.link_process AS link_process1 " +
                " , c1.link_process_field AS link_process_field1 " +
                " , dbo.processDashboardChart_GetVariables(c1.chart_ids) AS chart_ids1 " +
                " , c1.hide AS hide1 " +
                " FROM process_dashboard_chart_links c1 " +
                " WHERE c1.instance = @sourceInstance " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.process_dashboard_chart_link_id AS id2 " +
                " , c2.instance AS instance2 " +
                " , c2.process AS process2 " +
                " , " + Conv.ToSql(targetDb) + ".dbo.processDashboard_GetVariable(c2.dashboard_id) AS dashboard2 " +
                " , c2.dashboard_id AS dashboard_id2 " +
                " , c2.source_field AS source_field2 " +
                " , c2.link_process AS link_process2 " +
                " , c2.link_process_field AS link_process_field2 " +
                " , " + Conv.ToSql(targetDb) + ".dbo.processDashboardChart_GetVariables(c2.chart_ids) AS chart_ids2 " +
                " , c2.hide AS hide2 " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_dashboard_chart_links c2 " +
                " WHERE c2.instance = @targetInstance " +
                processes_sql2 +
                " ) " +
                " SELECT c1.id1, c2.id2, c1.instance1, c2.instance2, c1.process1, c2.process2, c1.dashboard1, c2.dashboard2, c1.dashboard_id1, c2.dashboard_id2 " +
                " , c1.source_field1, c2.source_field2, c1.link_process1, c2.link_process2, c1.link_process_field1, c2.link_process_field2" +
                " , c1.chart_ids1, c2.chart_ids2, c1.hide1, c2.hide2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process1 = c2.process2 AND c1.dashboard1 = c2.dashboard2 " +
                " AND COALESCE(c1.source_field1, '') = COALESCE(c2.source_field2, '') AND COALESCE(c1.link_process1, '') = COALESCE(c2.link_process2, '') " +
                " AND COALESCE(c1.link_process_field1, '') = COALESCE(c2.link_process_field2, '') AND COALESCE(c1.chart_ids1, '') = COALESCE(c2.chart_ids2, '') " +
                " AND COALESCE(c1.hide1, '') = COALESCE(c2.hide2, '') " +
                " WHERE c2.id2 IS NULL " +
                " ORDER BY c1.id1 ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ProcessDashboardUserGroupsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Dashboard User Groups");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.process_dashboard_id IN (SELECT process_dashboard_id FROM process_dashboards WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 = " AND c2.process_dashboard_id IN (SELECT process_dashboard_id FROM " +
                                 Conv.ToSql(targetDb) + ".dbo.process_dashboards WHERE process IN (" + processes +
                                 ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.processDashboard_GetVariable(c1.process_dashboard_id) AS dashboard1 " +
                " , (SELECT guid FROM items WHERE item_id = c1.user_group_id) AS user_group1 " +
                " , c1.process_dashboard_id AS dashboard_id1 " +
                " , c1.user_group_id AS user_group_id1 " +
                " FROM process_dashboard_user_groups c1 " +
                " WHERE (c1.process_dashboard_id IN (SELECT process_dashboard_id FROM process_dashboards WHERE instance = @sourceInstance) " +
                " OR c1.user_group_id IN (SELECT user_group_id FROM _" + Conv.ToSql(sourceInstance) + "_user_group)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.processDashboard_GetVariable(c2.process_dashboard_id) AS dashboard2 " +
                " , (SELECT guid FROM " + Conv.ToSql(targetDb) +
                ".dbo.items WHERE item_id = c2.user_group_id) AS user_group2 " +
                " , c2.process_dashboard_id AS dashboard_id2 " +
                " , c2.user_group_id AS user_group_id2 " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.process_dashboard_user_groups c2 " +
                " WHERE (c2.process_dashboard_id IN (SELECT process_dashboard_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_dashboards WHERE instance = @targetInstance) " +
                " OR c2.user_group_id IN (SELECT user_group_id FROM " + Conv.ToSql(targetDb) + ".dbo._" +
                Conv.ToSql(targetInstance) + "_user_group)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.dashboard1, c2.dashboard2, c1.user_group1, c2.user_group2 " +
                " , c1.dashboard_id1, c2.dashboard_id2, c1.user_group_id1, c2.user_group_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.dashboard1 = c2.dashboard2 AND c1.user_group1 = c2.user_group2 " +
                " WHERE c2.dashboard2 IS NULL AND c2.user_group2 IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "IF OBJECT_ID('tempdb..#conditions') IS NOT NULL DROP TABLE #conditions " +
                " CREATE TABLE #conditions (condition_id INT, instance NVARCHAR(10), process NVARCHAR(50), variable NVARCHAR(255), condition_json NVARCHAR(MAX), order_no NVARCHAR(MAX), [disabled] TINYINT) " +
                " EXEC dbo.conditions_replace @sourceInstance, 'source' ; " +
                " EXEC " + Conv.ToSql(targetDb) + ".dbo.conditions_replace @targetInstance, 'target' ; " +
                " SELECT c1.process, c1.variable, " +
                " c1.condition_id AS id1, c2.condition_id AS id2, " +
                " c1.condition_json AS condition_json1, c2.condition_json AS condition_json2, " +
                " COALESCE(c1.order_no, '') AS order_no1, COALESCE(c2.order_no, '') AS order_no2, " +
                " c1.disabled AS disabled1, c2.disabled AS disabled2 " +
                " FROM #conditions c1 " +
                " LEFT JOIN #conditions c2 ON c2.instance = 'target' AND c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.instance = 'source' " +
                " AND (c2.condition_id IS NULL " +
                " OR COALESCE(c1.condition_json, '') <> COALESCE(c2.condition_json, '') " +
                " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') OR c1.disabled <> c2.disabled " +
                " ) AND c1.variable IS NOT NULL " + processes_sql +
                " ORDER BY c1.condition_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionContainersCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id " +
                " FROM condition_containers c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                " OR c1.process_container_id IN (SELECT process_container_id FROM process_containers WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " ," + Conv.ToSql(targetDb) +
                ".dbo.processContainer_GetVariable(c1.process_container_id) AS process_container_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.condition_containers c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance)   " +
                " OR c1.process_container_id IN (SELECT process_container_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_containers WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id, c1.process_container_id, c2.condition_id AS condition_id2, c2.process_container_id AS process_container_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.condition_id = c2.condition_id AND c1.process_container_id = c2.process_container_id " +
                " WHERE c2.condition_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionFeaturesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id, feature " +
                " FROM condition_features c1 " +
                " WHERE c1.condition_id in (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.condition_GetVariable(c1.condition_id) AS condition_id, feature " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.condition_features c1 " +
                " WHERE c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id, c1.feature, c2.condition_id AS condition_id2, c2.feature AS feature2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.condition_id = c2.condition_id AND c1.feature = c2.feature " +
                " WHERE c2.condition_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionGroupsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id  " +
                " , dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id " +
                " FROM condition_groups c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                " OR c1.process_group_id IN (SELECT process_group_id FROM process_groups WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , " + Conv.ToSql(targetDb) +
                ".dbo.processGroup_GetVariable(c1.process_group_id) AS process_group_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.condition_groups c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance) " +
                " OR c1.process_group_id IN (SELECT process_group_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_groups WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id, c1.process_group_id, c2.condition_id AS condition_id2, c2.process_group_id AS process_group_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.condition_id = c2.condition_id AND c1.process_group_id = c2.process_group_id " +
                " WHERE c2.condition_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionPortfoliosCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " FROM condition_portfolios c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                " OR c1.process_portfolio_id IN (SELECT process_portfolio_id FROM process_portfolios WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " ," + Conv.ToSql(targetDb) +
                ".dbo.processPortfolio_GetVariable(c1.process_portfolio_id) AS process_portfolio_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.condition_portfolios c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance) " +
                " OR c1.process_portfolio_id IN (SELECT process_portfolio_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_portfolios WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id, c1.process_portfolio_id, c2.condition_id AS condition_id2, c2.process_portfolio_id AS process_portfolio_id2 " +
                " FROM c1 left join c2 on c1.condition_id = c2.condition_id and c1.process_portfolio_id = c2.process_portfolio_id " +
                " WHERE c2.condition_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionStatesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , state " +
                " FROM condition_states c1 " +
                " WHERE c1.condition_id IN (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , state " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.condition_states c1 " +
                " WHERE c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id, c1.state, c2.condition_id AS condition_id2, c2.state AS feature2 " +
                " FROM c1 left join c2 on c1.condition_id = c2.condition_id and c1.state = c2.state " +
                " WHERE c2.condition_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionTabsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id " +
                " FROM condition_tabs c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                " OR c1.process_tab_id IN (SELECT process_tab_id FROM process_tabs WHERE instance = @sourceInstance)) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " ," + Conv.ToSql(targetDb) + ".dbo.processTab_GetVariable(c1.process_tab_id) AS process_tab_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.condition_tabs c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance) " +
                " OR c1.process_tab_id IN (SELECT process_tab_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.process_tabs WHERE instance = @targetInstance)) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id, c1.process_tab_id, c2.condition_id AS condition_id2, c2.process_tab_id AS process_tab_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.condition_id = c2.condition_id AND c1.process_tab_id = c2.process_tab_id " +
                " WHERE c2.condition_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionUserGroupsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id   " +
                " , (SELECT guid FROM items WHERE item_id = c1.item_id) AS item_id " +
                " FROM condition_user_groups c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                " OR c1.item_id IN (SELECT item_id FROM items WHERE instance = @sourceInstance AND process = 'user_group')) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) +
                ".dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , (SELECT guid FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE item_id = c1.item_id) AS item_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.condition_user_groups c1 " +
                " WHERE (c1.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance) " +
                " OR c1.item_id IN (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.items WHERE instance = @targetInstance AND process = 'user_group')) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id, c1.item_id, c2.condition_id AS condition_id2, c2.item_id AS item_id2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.condition_id = c2.condition_id AND c1.item_id = c2.item_id " +
                " WHERE c2.condition_id IS NULL ", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionsGroupsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.process IN (" + processes + ") ";
                processes_sql2 = " AND c2.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT process, variable, COALESCE(order_no, '') AS order_no, condition_group_id " +
                " FROM conditions_groups c1 " +
                " WHERE c1.instance = @sourceInstance " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT process, variable, COALESCE(order_no, '') AS order_no " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.conditions_groups c2 " +
                " WHERE c2.instance = @targetInstance " +
                processes_sql2 +
                " ) " +
                " SELECT c1.process AS process1, c2.process AS process2, c1.variable AS variable1, c2.variable AS variable2, c1.order_no AS order_no1, c2.order_no AS order_no2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.process = c2.process AND c1.variable = c2.variable " +
                " WHERE c1.variable IS NOT NULL AND (c2.variable IS NULL OR c1.order_no <> c2.order_no) " +
                " ORDER BY c1.condition_group_id ASC", DBParameters);
        }

        public List<Dictionary<string, object>> ConditionsGroupsConditionCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Conditions");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 = " AND c1.condition_id IN (SELECT condition_id FROM conditions WHERE process IN (" +
                                 processes + ")) ";
                processes_sql2 = " AND c2.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.conditions WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT dbo.condition_GetVariable(c1.condition_id) AS condition_id " +
                " , CASE WHEN c1.condition_group_id = 0 THEN '<no condition group>' ELSE dbo.conditionsGroups_GetVariable(c1.condition_group_id) END AS condition_group_id " +
                " FROM conditions_groups_condition c1 " +
                " WHERE c1.condition_id IN (SELECT condition_id FROM conditions WHERE instance = @sourceInstance) " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT " + Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c2.condition_id) AS condition_id " +
                " , CASE WHEN c2.condition_group_id = 0 THEN '<no condition group>' ELSE " + Conv.ToSql(targetDb) +
                ".dbo.conditionsGroups_GetVariable(c2.condition_group_id) END AS condition_group_id " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.conditions_groups_condition c2 " +
                " WHERE c2.condition_id IN (SELECT condition_id FROM " + Conv.ToSql(targetDb) +
                ".dbo.conditions WHERE instance = @targetInstance) " +
                processes_sql2 +
                " ) " +
                " SELECT c1.condition_id AS condition_id1, c2.condition_id AS condition_id2, c1.condition_group_id AS condition_group_id1, c2.condition_group_id AS condition_group_id2 " +
                " FROM c1 LEFT JOIN c2 ON c1.condition_id = c2.condition_id " +
                " WHERE c1.condition_id IS NOT NULL AND (c2.condition_id IS NULL OR COALESCE(c1.condition_group_id, '') <> COALESCE(c2.condition_group_id, '')) ",
                DBParameters);
        }

        public List<Dictionary<string, object>> InstanceMenuCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "", bool skipConfigs = false) {
            InstanceCompareProgress.SetCompareProgress("Menu");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                targetDb = Conv.ToSql(targetDb);
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "SELECT c1.variable, c1.instance_menu_id AS instance_menu_id1, c2.instance_menu_id AS instance_menu_id2, " +
                    " dbo.menu_GetVariable(c1.instance_menu_id) AS id1, " +
                    Conv.ToSql(targetDb) + ".dbo.menu_GetVariable(c2.instance_menu_id) AS id2, " +
                    " dbo.menu_GetVariable(c1.parent_id) AS parent_id1, " +
                    Conv.ToSql(targetDb) + ".dbo.menu_GetVariable(c2.parent_id) AS parent_id2, " +
                    " c1.order_no AS order_no1, c2.order_no AS order_no2, " +
                    " c1.default_state AS default_state1, c2.default_state AS default_state2, " +
                    " dbo.menu_idReplace(c1.params) AS params1, " +
                    Conv.ToSql(targetDb) + ".dbo.menu_idReplace(c2.params) AS params2, " +
                    " c1.icon AS icon1, c2.icon AS icon2, " +
                    " c1.link_type AS link_type1, c2.link_type AS link_type2 " +
                    " FROM instance_menu c1 " +
                    " LEFT JOIN " + Conv.ToSql(targetDb) +
                    ".dbo.instance_menu c2 ON c2.instance = @targetInstance AND c1.variable = c2.variable " +
                    " WHERE c1.instance = @sourceInstance " +
                    " AND (c2.instance_menu_id IS NULL " +
                    " OR COALESCE(dbo.menu_GetVariable(c1.parent_id), '') <> COALESCE(" + Conv.ToSql(targetDb) +
                    ".dbo.menu_GetVariable(c2.parent_id), '') " +
                    " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                    " OR COALESCE(c1.default_state, '') <> COALESCE(c2.default_state, '') " +
                    (skipConfigs
                        ? ""
                        : " OR COALESCE(dbo.menu_idReplace(c1.params), '') <> COALESCE(" + Conv.ToSql(targetDb) +
                          ".dbo.menu_idReplace(c2.params), '') ") +
                    " OR COALESCE(c1.icon, '') <> COALESCE(c2.icon, '') " +
                    " OR COALESCE(c1.link_type, '') <> COALESCE(c2.link_type, '') " +
                    " ) AND c1.variable IS NOT NULL " +
                    " ORDER BY c1.instance_menu_id ASC ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceMenuRightsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Menu Rights");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                targetDb = Conv.ToSql(targetDb);
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT dbo.menu_GetVariable(c1.instance_menu_id) AS instance_menu_id " +
                    " , (SELECT guid FROM items WHERE item_id = c1.item_id) AS item_id " +
                    " FROM instance_menu_rights c1 " +
                    " WHERE c1.instance_menu_id IN (SELECT instance_menu_id FROM instance_menu WHERE instance = @sourceInstance) " +
                    " OR c1.item_id IN (SELECT item_id FROM items WHERE instance = @sourceInstance AND process = 'user_group') " +
                    " ), c2 AS ( " +
                    " SELECT " + Conv.ToSql(targetDb) +
                    ".dbo.menu_GetVariable(c1.instance_menu_id) AS instance_menu_id " +
                    " , (SELECT guid FROM " + Conv.ToSql(targetDb) +
                    ".dbo.items WHERE item_id = c1.item_id) AS item_id " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_menu_rights c1 " +
                    " WHERE c1.instance_menu_id IN (SELECT instance_menu_id FROM " + Conv.ToSql(targetDb) +
                    ".dbo.instance_menu WHERE instance = @targetInstance) " +
                    " OR c1.item_id IN (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                    ".dbo.items WHERE instance = @targetInstance AND process = 'user_group') " +
                    " ) " +
                    " SELECT c1.instance_menu_id, c1.item_id, c2.instance_menu_id AS instance_menu_id2, c2.item_id AS item_id2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.instance_menu_id = c2.instance_menu_id AND c1.item_id = c2.item_id " +
                    " WHERE c1.instance_menu_id IS NOT NULL AND c2.instance_menu_id IS NULL ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceMenuStatesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Menu States");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                targetDb = Conv.ToSql(targetDb);
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                var sql =
                    "WITH c1 AS ( " +
                    " SELECT dbo.menu_GetVariable(c1.instance_menu_id) AS instance_menu_id " +
                    " , (SELECT guid FROM items WHERE item_id = c1.item_id) AS item_id " +
                    " , c1.state " +
                    " FROM instance_menu_states c1 " +
                    " WHERE c1.instance_menu_id IN (SELECT instance_menu_id FROM instance_menu WHERE instance = @sourceInstance) " +
                    " OR c1.item_id IN (SELECT item_id FROM items WHERE instance = @sourceInstance AND process = 'user_group') " +
                    " ), c2 AS ( " +
                    " SELECT " + Conv.ToSql(targetDb) +
                    ".dbo.menu_GetVariable(c2.instance_menu_id) AS instance_menu_id " +
                    " , (SELECT guid FROM " + Conv.ToSql(targetDb) +
                    ".dbo.items WHERE item_id = c2.item_id) AS item_id " +
                    " , c2.state " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_menu_states c2 " +
                    " WHERE c2.instance_menu_id IN (SELECT instance_menu_id FROM " + Conv.ToSql(targetDb) +
                    ".dbo.instance_menu WHERE instance = @targetInstance) " +
                    " OR c2.item_id IN (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                    ".dbo.items WHERE instance = @targetInstance AND process = 'user_group') " +
                    " ) " +
                    " SELECT c1.instance_menu_id, c1.item_id, c2.instance_menu_id AS instance_menu_id2, c2.item_id AS item_id2, c1.state AS state1, c2.state AS state2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.instance_menu_id = c2.instance_menu_id AND c1.item_id = c2.item_id AND c1.state = c2.state " +
                    " WHERE c1.instance_menu_id IS NOT NULL AND c2.instance_menu_id IS NULL ";
                return db.GetDatatableDictionary(sql, DBParameters);
            }
        }

        public List<Dictionary<string, object>> NotificationCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "", bool skipRequests = false) {
            InstanceCompareProgress.SetCompareProgress("Notifications");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "SELECT c1.item_id, c2.item_id item_id2 " +
                    " , (SELECT i.guid FROM items i WHERE i.item_id = c1.item_id) AS guid1 " +
                    " , (SELECT i.guid FROM " + Conv.ToSql(targetDb) +
                    ".dbo.items i WHERE i.item_id = c2.item_id) AS guid2 " +
                    " , c1.order_no order_no1, c2.order_no order_no2 " +
                    " , c1.type type1, c2.type type2 " +
                    " , c1.name_variable name_variable_translation1, c2.name_variable name_variable_translation2 " +
                    " , dbo.notification_VariableIdReplace(c1.name_variable) name_variable1 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.notification_VariableIdReplace(c2.name_variable) name_variable2 " +
                    " , dbo.notification_VariableIdReplace(c1.title_variable) title_variable1 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.notification_VariableIdReplace(c2.title_variable) title_variable2 " +
                    " , dbo.notification_VariableIdReplace(c1.body_variable) body_variable1 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.notification_VariableIdReplace(c2.body_variable) body_variable2 " +
                    " , (SELECT u.login FROM _" + Conv.ToSql(sourceInstance) + "_user u " +
                    " WHERE u.item_id = " +
                    " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                    " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                    " (SELECT ic.item_column_id FROM item_columns ic " +
                    " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                    "' AND ic.process = 'notification' AND name = 'sender' " +
                    " ) ) ) AS sender1 " +
                    " , (SELECT i.guid FROM items i " +
                    " WHERE i.item_id = " +
                    " (SELECT idps.selected_item_id FROM item_data_process_selections idps " +
                    " WHERE idps.item_id = c1.item_id AND idps.item_column_id = " +
                    " (SELECT ic.item_column_id FROM item_columns ic " +
                    " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                    "' AND ic.process = 'notification' AND name = 'sender' " +
                    " ) ) ) AS sender_guid1 " +
                    " , (SELECT u.login FROM " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                    "_user u " +
                    " WHERE u.item_id = " +
                    " (SELECT idps.selected_item_id FROM " + Conv.ToSql(targetDb) +
                    ".dbo.item_data_process_selections idps " +
                    " WHERE idps.item_id = c2.item_id AND idps.item_column_id = " +
                    " (SELECT ic.item_column_id FROM " + Conv.ToSql(targetDb) + ".dbo.item_columns ic " +
                    " WHERE ic.instance = '" + Conv.ToSql(sourceInstance) +
                    "' AND ic.process = 'notification' AND name = 'sender' " +
                    " ) ) ) AS sender2 " +
                    " , COALESCE(c1.language, '') AS language1, COALESCE(c2.language, '') AS language2 " +
                    " , c1.notify notify1, c2.notify notify2 " +
                    " , c1.created created1, c2.created created2 " +
                    " , c1.modified modified1, c2.modified modified2 " +
                    " , c1.expiration expiration1, c2.expiration expiration2 " +
                    " , c1.status status1, c2.status status2 " +
                    " , COALESCE(c1.delta, '') AS delta1, COALESCE(c2.delta, '') AS delta2 " +
                    " , c1.digest digest1, c2.digest digest2 " +
                    " , c1.httprequest_type httprequest_type1, c2.httprequest_type httprequest_type2 " +
                    " , COALESCE(c1.httprequest_url, '') AS httprequest_url1, COALESCE(c2.httprequest_url, '') AS httprequest_url2 " +
                    " , COALESCE(c1.httprequest_header, '') AS httprequest_header1, COALESCE(c2.httprequest_header, '') AS httprequest_header2 " +
                    " , COALESCE(c1.httprequest_body, '') AS httprequest_body1, COALESCE(c2.httprequest_body, '') AS httprequest_body2 " +
                    " , dbo.item_GetGuid(c1.user_item_id) AS user_item_id1" +
                    " , " + Conv.ToSql(targetDb) + ".dbo.item_GetGuid(c2.user_item_id) AS user_item_id2" +
                    " , dbo.item_GetGuids(c1.user_groups) AS user_groups1" +
                    " , " + Conv.ToSql(targetDb) + ".dbo.item_GetGuids(c2.user_groups) AS user_groups2" +
                    " , c1.start start1, c2.start start2 " +
                    " , c1.compare compare1, c2.compare compare2 " +
                    " FROM _" + Conv.ToSql(sourceInstance) + "_notification c1 " +
                    " LEFT JOIN " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_notification c2 " +
                    " ON (SELECT i.guid FROM items i WHERE i.item_id = c1.item_id) = (SELECT i.guid FROM " +
                    Conv.ToSql(targetDb) + ".dbo.items i WHERE i.item_id = c2.item_id) AND c1.type = c2.type " +
                    " WHERE (c1.type = 0 OR (c1.type = 3 AND c1.compare > 0)) AND (SELECT i.guid FROM items i WHERE i.item_id = c1.item_id) IS NOT NULL AND " +
                    " ((SELECT i.guid FROM " + Conv.ToSql(targetDb) +
                    ".dbo.items i WHERE i.item_id = c2.item_id) IS NULL " +
                    " OR ((c1.type = 0 OR (c1.type = 3 AND c1.compare = 2)) " +
                    " AND (c1.order_no <> c2.order_no " +
                    " OR dbo.notification_VariableIdReplace(c1.name_variable) <> " + Conv.ToSql(targetDb) +
                    ".dbo.notification_VariableIdReplace(c2.name_variable) " +
                    " OR dbo.notification_VariableIdReplace(c1.title_variable) <> " + Conv.ToSql(targetDb) +
                    ".dbo.notification_VariableIdReplace(c2.title_variable) " +
                    " OR dbo.notification_VariableIdReplace(c1.body_variable) <> " + Conv.ToSql(targetDb) +
                    ".dbo.notification_VariableIdReplace(c2.body_variable) " +
                    " OR c1.sender <> c2.sender " +
                    " OR COALESCE(c1.language, '') <> COALESCE(c2.language, '') " +
                    " OR c1.notify <> c2.notify " +
                    " OR COALESCE(c1.created, '') <> COALESCE(c2.created, '') " +
                    " OR COALESCE(c1.modified, '') <> COALESCE(c2.modified, '') " +
                    " OR COALESCE(c1.expiration, '') <> COALESCE(c2.expiration, '') " +
                    " OR c1.status <> c2.status " +
                    " OR COALESCE(c1.delta, '') <> COALESCE(c2.delta, '') " +
                    " OR c1.digest <> c2.digest " +
                    " OR c1.httprequest_type <> c2.httprequest_type " +
                    " OR COALESCE(c1.httprequest_url, '') <> COALESCE(c2.httprequest_url, '') " +
                    " OR COALESCE(c1.httprequest_header, '') <> COALESCE(c2.httprequest_header, '') " +
                    " OR COALESCE(c1.httprequest_body, '') <> COALESCE(c2.httprequest_body, '') " +
                    " OR COALESCE((SELECT i.guid FROM items i WHERE i.item_id = c1.item_id), '') <> COALESCE((SELECT i.guid FROM " +
                    Conv.ToSql(targetDb) + ".dbo.items i WHERE i.item_id = c2.item_id), '') " +
                    " OR (dbo.item_GetGuid(c1.user_item_id) IS NULL AND " + Conv.ToSql(targetDb) +
                    ".dbo.item_GetGuid(c2.user_item_id) IS NOT NULL) " +
                    " OR (dbo.item_GetGuid(c1.user_item_id) IS NOT NULL AND " + Conv.ToSql(targetDb) +
                    ".dbo.item_GetGuid(c2.user_item_id) IS NULL) " +
                    " OR dbo.item_GetGuid(c1.user_item_id) <> " + Conv.ToSql(targetDb) +
                    ".dbo.item_GetGuid(c2.user_item_id) " +
                    " OR (dbo.item_GetGuids(c1.user_groups) IS NULL AND " + Conv.ToSql(targetDb) +
                    ".dbo.item_GetGuids(c2.user_groups) IS NOT NULL) " +
                    " OR (dbo.item_GetGuids(c1.user_groups) IS NOT NULL AND " + Conv.ToSql(targetDb) +
                    ".dbo.item_GetGuids(c2.user_groups) IS NULL) " +
                    " OR dbo.item_GetGuids(c1.user_groups) <> " + Conv.ToSql(targetDb) +
                    ".dbo.item_GetGuids(c2.user_groups) " +
                    " OR COALESCE(c1.start, '') <> COALESCE(c2.start, '') " +
                    " OR COALESCE(c1.compare, '') <> COALESCE(c2.compare, '') " +
                    " ))) ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> NotificationTagLinksCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Notification Tags");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT dbo.item_GetGuid(c1.item_id) AS guid1 " +
                    " , COALESCE(c1.order_no, '') AS order_no1 " +
                    " , dbo.item_GetGuid(c1.json_tag_id) AS json_tag_guid1 " +
                    " , dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id1 " +
                    " , c1.tag_type AS tag_type1 " +
                    " , c1.process_name AS process_name1 " +
                    " , c1.view_state AS view_state1 " +
                    " , c1.link_param AS link_param1 " +
                    " , c1.item_id " +
                    " FROM _" + Conv.ToSql(sourceInstance) + "_notification_tag_links c1" +
                    " ), c2 AS (" +
                    " SELECT " + Conv.ToSql(targetDb) + ".dbo.item_GetGuid(c2.item_id) AS guid2 " +
                    " , COALESCE(c2.order_no, '') AS order_no2" +
                    " , " + Conv.ToSql(targetDb) + ".dbo.item_GetGuid(c2.json_tag_id) AS json_tag_guid2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.itemColumn_GetVariable(c2.item_column_id) AS item_column_id2 " +
                    " , c2.tag_type AS tag_type2 " +
                    " , c2.process_name AS process_name2 " +
                    " , c2.view_state AS view_state2 " +
                    " , c2.link_param AS link_param2 " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                    "_notification_tag_links c2 " +
                    " ) " +
                    " SELECT c1.guid1, c2.guid2, c1.order_no1, c2.order_no2, c1.json_tag_guid1, c2.json_tag_guid2 " +
                    " , c1.item_column_id1, c2.item_column_id2, c1.tag_type1, c2.tag_type2, c1.process_name1, c2.process_name2 " +
                    " , c1.view_state1, c2.view_state2, c1.link_param1, c2.link_param2, c1.item_id " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.guid1 = c2.guid2 " +
                    " WHERE c1.guid1 IS NOT NULL " +
                    " AND (c2.guid2 IS NULL " +
                    " OR c1.order_no1 <> c2.order_no2 " +
                    " OR c1.json_tag_guid1 <> c2.json_tag_guid2 " +
                    " OR c1.item_column_id1 <> c2.item_column_id2 " +
                    " OR c1.process_name1 <> c2.process_name2 " +
                    " OR c1.view_state1 <> c2.view_state2 " +
                    " OR c1.link_param1 <> c2.link_param2 " +
                    " ) ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> NotificationConditionsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Notification Conditions");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "SELECT c1.variable id1, c2.variable id2, c1.process " +
                    " , (SELECT name_variable FROM _" + Conv.ToSql(sourceInstance) +
                    "_notification WHERE item_id = c1.item_id) item_id1 " +
                    " , (SELECT name_variable FROM " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                    "_notification WHERE item_id = c2.item_id) item_id2 " +
                    " , dbo.condition_GetVariable(c1.before_condition_id) before_condition_id1, " +
                    Conv.ToSql(targetDb) + ".dbo.condition_GetVariable(c2.before_condition_id) before_condition_id2 " +
                    " , dbo.condition_GetVariable(c1.after_condition_id) after_condition_id1, " + Conv.ToSql(targetDb) +
                    ".dbo.condition_GetVariable(c2.after_condition_id) after_condition_id2 " +
                    " , c1.view_id AS view_id1, c2.view_id AS view_id2 " +
                    " , c1.in_use AS in_use1, c2.in_use AS in_use2 " +
                    " , c1.condition_type AS condition_type1, c2.condition_type AS condition_type2 " +
                    " , dbo.processAction_GetVariable(c1.process_action_id) AS process_action_id1, " +
                    Conv.ToSql(targetDb) +
                    ".dbo.processAction_GetVariable(c2.process_action_id) AS process_action_id2 " +
                    " FROM notification_conditions c1 " +
                    " LEFT JOIN " + Conv.ToSql(targetDb) +
                    ".dbo.notification_conditions c2 ON c2.instance = @targetInstance AND c1.process = c2.process AND c1.variable = c2.variable " +
                    " WHERE c1.instance = @sourceInstance " +
                    " AND c1.variable IS NOT NULL AND " +
                    " (c2.variable IS NULL " +
                    " OR (SELECT name_variable FROM _" + Conv.ToSql(sourceInstance) +
                    "_notification WHERE item_id = c1.item_id) <> (SELECT name_variable FROM " + Conv.ToSql(targetDb) +
                    ".dbo._" + Conv.ToSql(targetInstance) + "_notification WHERE item_id = c2.item_id) " +
                    " OR dbo.condition_GetVariable(c1.before_condition_id) <> " + Conv.ToSql(targetDb) +
                    ".dbo.condition_GetVariable(c2.before_condition_id) " +
                    " OR dbo.condition_GetVariable(c1.after_condition_id) <> " + Conv.ToSql(targetDb) +
                    ".dbo.condition_GetVariable(c2.after_condition_id) " +
                    " OR c1.view_id <> c2.view_id " +
                    " OR c1.in_use <> c2.in_use " +
                    " OR c1.condition_type <> c2.condition_type " +
                    " OR COALESCE(dbo.processAction_GetVariable(c1.process_action_id), '') <> COALESCE(" +
                    Conv.ToSql(targetDb) + ".dbo.processAction_GetVariable(c2.process_action_id), '') ) " +
                    " ORDER BY c1.notification_condition_id ASC ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> NotificationReceiversCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Notification Receivers");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT dbo.notificationCondition_GetVariable(c1.notification_condition_id) AS notification_condition_id, " +
                    " dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id, " +
                    " dbo.itemColumn_GetVariable(c1.list_process) AS list_process, " +
                    " dbo.itemColumn_GetVariable(c1.list_item_column_id) AS list_item_column_id, " +
                    " dbo.processActionRows_GetGuid(c1.process_action_row_id) AS process_action_row_id, " +
                    " (SELECT [guid] FROM items WHERE item_id = c1.user_group_item_id) AS user_group_item_id " +
                    " FROM notification_receivers c1 " +
                    " WHERE c1.process_action_row_id IN (SELECT process_action_rows.process_action_row_id FROM process_action_rows INNER JOIN process_actions ON process_actions.process_action_id = process_action_rows.process_action_id WHERE process_actions.instance = @sourceInstance) " +
                    " ), c2 AS ( " +
                    " SELECT " + Conv.ToSql(targetDb) +
                    ".dbo.notificationCondition_GetVariable(c1.notification_condition_id) AS notification_condition_id, " +
                    Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id, " +
                    Conv.ToSql(targetDb) + ".dbo.itemColumn_GetVariable(c1.list_process) AS list_process, " +
                    Conv.ToSql(targetDb) +
                    ".dbo.itemColumn_GetVariable(c1.list_item_column_id) AS list_item_column_id, " +
                    Conv.ToSql(targetDb) +
                    ".dbo.processActionRows_GetGuid(c1.process_action_row_id) AS process_action_row_id, " +
                    " (SELECT [guid] FROM " + Conv.ToSql(targetDb) +
                    ".dbo.items WHERE item_id = c1.user_group_item_id) AS user_group_item_id " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.notification_receivers c1 " +
                    " WHERE c1.process_action_row_id IN (SELECT process_action_rows.process_action_row_id FROM " +
                    Conv.ToSql(targetDb) + ".dbo.process_action_rows INNER JOIN " + Conv.ToSql(targetDb) +
                    ".dbo.process_actions ON process_actions.process_action_id = process_action_rows.process_action_id WHERE process_actions.instance = @targetInstance) " +
                    " ) " +
                    " SELECT c1.notification_condition_id AS notification_condition_id1, c2.notification_condition_id AS notification_condition_id2, " +
                    " c1.item_column_id AS item_column_id1, " +
                    " c1.list_process AS list_process1, " +
                    " c1.list_item_column_id AS list_item_column_id1, " +
                    " c1.process_action_row_id AS process_action_row_id1, " +
                    " c1.user_group_item_id AS user_group_item_id1 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON COALESCE(c1.notification_condition_id, '') = COALESCE(c2.notification_condition_id, '') " +
                    " AND COALESCE(c1.item_column_id, '') = COALESCE(c2.item_column_id, '') " +
                    " AND COALESCE(c1.list_process, '') = COALESCE(c2.list_process, '') " +
                    " AND COALESCE(c1.list_item_column_id, '') = COALESCE(c2.list_item_column_id, '') " +
                    " AND ((c1.process_action_row_id IS NULL AND c2.process_action_row_id IS NULL) OR c1.process_action_row_id = c2.process_action_row_id) " +
                    " AND ((c1.user_group_item_id IS NULL AND c2.user_group_item_id IS NULL) OR c1.user_group_item_id = c2.user_group_item_id) " +
                    " WHERE c2.process_action_row_id IS NULL ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceConfigurationsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Instance Configuration");
            string processes_sql = "";
            if (processes.Length > 0) {
                processes_sql = " AND c1.process IN (" + processes + ") ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT ISNULL(c1.process, 'null') + ' ' + c1.name AS id1, " +
                " CASE WHEN c2.value IS NULL THEN NULL ELSE ISNULL(c2.process, 'null') + ' ' + c2.name END AS id2, " +
                " c1.instance_configuration_id AS ic_id1, " +
                " c2.instance_configuration_id AS ic_id2, " +
                " c1.process AS process1, " +
                " c2.process AS process2, " +
                " c1.name AS name1, " +
                " c2.name AS name2, " +
                " dbo.instanceConfiguration_ValueIdReplace(c1.value) AS value1, " +
                Conv.ToSql(targetDb) + ".dbo.instanceConfiguration_ValueIdReplace(c2.value) AS value2 " +
                " FROM instance_configurations c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) +
                ".dbo.instance_configurations c2 ON c2.instance = @targetInstance " +
                " AND c2.category = 'View Configurator' " +
                " AND c2.[server] = 0 " +
                " AND (c1.process = c2.process OR (c1.process IS NULL AND c2.process IS NULL)) " +
                " AND c1.name = c2.name " +
                " WHERE c1.instance = @sourceInstance " +
                " AND c1.category = 'View Configurator' " +
                " AND c1.[server] = 0 " +
                " AND (c2.value IS NULL OR dbo.instanceConfiguration_ValueIdReplace(c1.value) <> " +
                Conv.ToSql(targetDb) + ".dbo.instanceConfiguration_ValueIdReplace(c2.value)) " + processes_sql +
                " ORDER BY c1.instance_configuration_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> InstanceUnionsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Unions");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT c1.instance_union_id AS id1, c1.variable AS variable1, c1.process AS process1 " +
                    " FROM instance_unions c1 " +
                    " WHERE c1.instance = @sourceInstance " +
                    " ), c2 AS ( " +
                    " SELECT c2.instance_union_id AS id2, c2.variable AS variable2, c2.process AS process2 " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_unions c2 " +
                    " WHERE c2.instance = @targetInstance " +
                    "  ) " +
                    " SELECT c1.id1, c2.id2, c1.variable1, c2.variable2, c1.process1, c2.process2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.variable1 = c2.variable2 " +
                    " WHERE c2.id2 IS NULL " +
                    " ORDER BY c1.id1 ASC ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceUnionRowsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Unions");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT c1.instance_union_row_id AS id1, c1.guid AS guid1 " +
                    " , dbo.instanceUnion_GetVariable(c1.instance_union_id) AS instance_union_id1 " +
                    " , dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id1 " +
                    " FROM instance_union_rows c1 " +
                    " WHERE c1.instance_union_id IN (SELECT instance_union_id FROM instance_unions WHERE instance = @sourceInstance) " +
                    " ), c2 AS ( " +
                    " SELECT c2.instance_union_row_id AS id2, c2.guid AS guid2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.instanceUnion_GetVariable(c2.instance_union_id) AS instance_union_id2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.itemColumn_GetVariable(c2.item_column_id) AS item_column_id2 " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_union_rows c2 " +
                    " WHERE c2.instance_union_id IN (SELECT instance_union_id FROM " + Conv.ToSql(targetDb) +
                    ".dbo.instance_unions WHERE instance = @targetInstance) " +
                    "  ) " +
                    " SELECT c1.id1, c2.id2, c1.guid1, c2.guid2, c1.instance_union_id1, c2.instance_union_id2, c1.item_column_id1, c2.item_column_id2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.guid1 = c2.guid2 AND c1.instance_union_id1 = c2.instance_union_id2 " +
                    " WHERE c1.id1 IS NOT NULL " +
                    " AND (c2.id2 IS NULL " +
                    " OR COALESCE(c1.item_column_id1, '') <> COALESCE(c2.item_column_id2, ''))" +
                    " ORDER BY c1.id1 ASC ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceUnionProcessesCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Unions");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT c1.instance_union_process_id AS id1, c1.guid AS guid1, c1.process AS process1 " +
                    " , dbo.instanceUnion_GetVariable(c1.instance_union_id) AS instance_union_id1 " +
                    " , dbo.processPortfolio_GetVariable(c1.portfolio_id) AS portfolio_id1 " +
                    " FROM instance_union_processes c1 " +
                    " WHERE c1.instance = @sourceInstance " +
                    " ), c2 AS ( " +
                    " SELECT c2.instance_union_process_id AS id2, c2.guid AS guid2, c2.process AS process2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.instanceUnion_GetVariable(c2.instance_union_id) AS instance_union_id2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.processPortfolio_GetVariable(c2.portfolio_id) AS portfolio_id2 " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_union_processes c2 " +
                    " WHERE c2.instance = @targetInstance " +
                    "  ) " +
                    " SELECT c1.id1, c2.id2, c1.guid1, c2.guid2, c1.instance_union_id1, c2.instance_union_id2, c1.process1, c2.process2, c1.portfolio_id1, c2.portfolio_id2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.guid1 = c2.guid2 AND c1.instance_union_id1 = c2.instance_union_id2 AND c1.process1 = c2.process2 " +
                    " WHERE c1.id1 IS NOT NULL " +
                    " AND (c2.id2 IS NULL " +
                    " OR COALESCE(c1.portfolio_id1, '') <> COALESCE(c2.portfolio_id2, ''))" +
                    " ORDER BY c1.id1 ASC ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceUnionColumnsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Unions");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT c1.instance_union_column_id AS id1 " +
                    " , dbo.instanceUnionProcess_GetGuid(c1.instance_union_process_id) AS instance_union_process_id1 " +
                    " , dbo.instanceUnionRow_GetGuid(c1.instance_union_row_id) AS instance_union_row_id1 " +
                    " , dbo.itemColumn_GetVariable(c1.item_column_id) AS item_column_id1 " +
                    " FROM instance_union_columns c1 " +
                    " WHERE c1.instance_union_process_id IN (SELECT instance_union_process_id FROM instance_union_processes WHERE instance = @sourceInstance) " +
                    " ), c2 AS ( " +
                    " SELECT c2.instance_union_column_id AS id2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.instanceUnionProcess_GetGuid(c2.instance_union_process_id) AS instance_union_process_id2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.instanceUnionRow_GetGuid(c2.instance_union_row_id) AS instance_union_row_id2 " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.itemColumn_GetVariable(c2.item_column_id) AS item_column_id2 " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_union_columns c2 " +
                    " WHERE c2.instance_union_process_id IN (SELECT instance_union_process_id FROM " +
                    Conv.ToSql(targetDb) + ".dbo.instance_union_processes WHERE instance = @targetInstance) " +
                    "  ) " +
                    " SELECT c1.id1, c2.id2, c1.instance_union_process_id1, c2.instance_union_process_id2" +
                    " , c1.instance_union_row_id1, c2.instance_union_row_id2, c1.item_column_id1, c2.item_column_id2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.instance_union_process_id1 = c2.instance_union_process_id2 AND c1.instance_union_row_id1 = c2.instance_union_row_id2 " +
                    " WHERE c1.id1 IS NOT NULL AND (c2.id2 IS NULL" +
                    " OR COALESCE(c1.item_column_id1, '') <> COALESCE(c2.item_column_id2, '')) " +
                    " ORDER BY c1.id1 ASC ", DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceSchedulesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "", bool delete = false) {
            InstanceCompareProgress.SetCompareProgress("Background Processes");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            } else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT c1.guid " +
                    " , c1.schedule_id " +
                    " , c1.name " +
                    " , c1.schedule_json " +
                    " , c1.last_modified " +
                    " , dbo.instanceSchedule_OptionsIdReplace(c1.config_options) AS config_options " +
                    " , c1.nickname " +
                    " , c1.disabled " +
                    " , c1.compare " +
                    " , c1.order_no " +
                    " , dbo.instanceScheduleGroup_GetVariable(c1.group_id) AS group_id " +
                    " FROM instance_schedules c1" +
                    " WHERE c1.instance = @sourceInstance " +
                    (delete ? " AND c1.compare = 3 " : " AND c1.compare > 0 ") +
                    " ), c2 AS ( " +
                    " SELECT c2.guid " +
                    " , c2.schedule_id " +
                    " , c2.name " +
                    " , c2.schedule_json " +
                    " , c2.last_modified " +
                    " , " + Conv.ToSql(targetDb) +
                    ".dbo.instanceSchedule_OptionsIdReplace(c2.config_options) AS config_options " +
                    " , c2.nickname " +
                    " , c2.disabled " +
                    " , c2.compare " +
                    " , c2.order_no " +
                    " , " + Conv.ToSql(targetDb) + ".dbo.instanceScheduleGroup_GetVariable(c2.group_id) AS group_id " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_schedules c2 " +
                    " WHERE c2.instance = @targetInstance " +
                    " ) " +
                    " SELECT c1.guid AS guid1, c2.guid AS guid2 " +
                    " , c1.schedule_id AS schedule_id1, c2.schedule_id AS schedule_id2 " +
                    " , c1.name AS name1, c2.name AS name2 " +
                    " , c1.schedule_json AS schedule_json1, c2.schedule_json AS schedule_json2 " +
                    " , c1.last_modified AS last_modified1, c2.last_modified AS last_modified2 " +
                    " , c1.config_options AS config_options1, c2.config_options AS config_options2 " +
                    " , c1.nickname AS nickname1, c2.nickname AS nickname2 " +
                    " , c1.disabled AS disabled1, c2.disabled AS disabled2 " +
                    " , c1.compare AS compare1, c2.compare AS compare2 " +
                    " , c1.order_no AS order_no1, c2.order_no AS order_no2 " +
                    " , c1.group_id AS group_id1, c2.group_id AS group_id2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.guid = c2.guid " +
                    " WHERE c1.guid IS NOT NULL " +
                    " AND (c2.guid IS NULL " +
                    " OR (c1.compare = 2 " +
                    " AND (COALESCE(c1.name, '') <> COALESCE(c2.name, '') " +
                    " OR COALESCE(c1.schedule_json, '') <> COALESCE(c2.schedule_json, '') " +
                    " OR COALESCE(c1.config_options, '') <> COALESCE(c2.config_options, '') " +
                    " OR COALESCE(c1.nickname, '') <> COALESCE(c2.nickname, '') " +
                    " OR COALESCE(c1.disabled, '') <> COALESCE(c2.disabled, '') " +
                    " OR COALESCE(c1.compare, '') <> COALESCE(c2.compare, '') " +
                    " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                    " OR COALESCE(c1.group_id, '') <> COALESCE(c2.group_id, '') " +
                    " ))) "
                    , DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceScheduleGroupsCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Background Processes");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            }
            else {
                SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
                SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
                return db.GetDatatableDictionary(
                    "WITH c1 AS ( " +
                    " SELECT c1.variable " +
                    " , c1.group_id " +
                    " , c1.schedule_json " +
                    " , c1.disabled " +
                    " , c1.order_no " +
                    " FROM instance_schedule_groups c1" +
                    " WHERE c1.instance = @sourceInstance " +
                    " ), c2 AS ( " +
                    " SELECT c2.variable " +
                    " , c2.group_id " +
                    " , c2.schedule_json " +
                    " , c2.disabled " +
                    " , c2.order_no " +
                    " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_schedule_groups c2 " +
                    " WHERE c2.instance = @targetInstance " +
                    " ) " +
                    " SELECT c1.variable AS variable1, c2.variable AS variable2 " +
                    " , c1.group_id AS group_id1, c2.group_id AS group_id2 " +
                    " , c1.schedule_json AS schedule_json1, c2.schedule_json AS schedule_json2 " +
                    " , c1.disabled AS disabled1, c2.disabled AS disabled2 " +
                    " , c1.order_no AS order_no1, c2.order_no AS order_no2 " +
                    " FROM c1 " +
                    " LEFT JOIN c2 ON c1.variable = c2.variable " +
                    " WHERE c1.group_id IS NOT NULL " +
                    " AND (c2.group_id IS NULL " +
                    " OR COALESCE(c1.schedule_json, '') <> COALESCE(c2.schedule_json, '') " +
                    " OR COALESCE(c1.disabled, '') <> COALESCE(c2.disabled, '') " +
                    " OR COALESCE(c1.order_no, '') <> COALESCE(c2.order_no, '') " +
                    " ) "
                    , DBParameters);
            }
        }

        public List<Dictionary<string, object>> InstanceHelpersCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Help");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND CAST(SUBSTRING(c1.identifier, 13, LEN(c1.identifier) - 12) AS INT) IN (SELECT process_tab_id FROM process_tabs WHERE process IN (" +
                    processes + ")) ";
                processes_sql2 =
                    " AND CAST(SUBSTRING(c2.identifier, 13, LEN(c2.identifier) - 12) AS INT) IN (SELECT process_tab_id FROM " +
                    Conv.ToSql(targetDb) + ".dbo.process_tabs WHERE process IN (" + processes + ")) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.instance_helper_id  " +
                " , dbo.processTab_GetVariable(CAST(SUBSTRING(c1.identifier, 13, LEN(c1.identifier) - 12) AS INT)) AS identifier " +
                " , c1.name_variable " +
                " , c1.desc_variable " +
                " , c1.platform " +
                " , c1.media_type " +
                " , COALESCE(c1.video_link, '') AS video_link " +
                " , COALESCE(c1.order_no, '') AS order_no " +
                " FROM instance_helpers c1 " +
                " WHERE c1.instance = @sourceInstance AND c1.identifier LIKE 'process.tab.%' " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.instance_helper_id " +
                " ," + Conv.ToSql(targetDb) +
                ".dbo.processTab_GetVariable(CAST(SUBSTRING(c2.identifier, 13, LEN(c2.identifier) - 12) AS INT)) AS identifier " +
                " , c2.name_variable " +
                " , c2.desc_variable " +
                " , c2.platform " +
                " , c2.media_type " +
                " , COALESCE(c2.video_link, '') AS video_link " +
                " , COALESCE(c2.order_no, '') AS order_no " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_helpers c2 " +
                " WHERE c2.instance = @targetInstance AND c2.identifier LIKE 'process.tab.%' " +
                processes_sql2 +
                " ) " +
                " SELECT c1.instance_helper_id AS id1, c2.instance_helper_id AS id2 " +
                " , c1.identifier AS identifier1, c2.identifier AS identifier2 " +
                " , c1.name_variable AS name_variable1, c2.name_variable AS name_variable2 " +
                " , c1.desc_variable AS desc_variable1, c2.desc_variable AS desc_variable2 " +
                " , c1.platform AS platform1, c2.platform AS platform2 " +
                " , c1.media_type AS media_type1, c2.media_type AS media_type2 " +
                " , c1.video_link AS video_link1, c2.video_link AS video_link2 " +
                " , c1.order_no AS order_no1, c2.order_no AS order_no2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.identifier = c2.identifier AND c1.name_variable = c2.name_variable " +
                " WHERE c1.instance_helper_id IS NOT NULL " +
                " AND (c2.instance_helper_id IS NULL " +
                " OR c1.desc_variable <> c2.desc_variable " +
                " OR c1.platform <> c2.platform " +
                " OR c1.media_type <> c2.media_type " +
                " OR c1.video_link <> c2.video_link " +
                " OR c1.order_no <> c2.order_no " +
                " ) ",
                DBParameters);
        }

        public List<Dictionary<string, object>> InstanceTranslationsCompare(string sourceInstance,
            string targetInstance, string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Translations");
            string processes_sql1 = "";
            string processes_sql2 = "";
            if (processes.Length > 0) {
                processes_sql1 =
                    " AND c1.variable IN (SELECT ih.name_variable FROM instance_helpers ih WHERE c1.instance = ih.instance AND CAST(SUBSTRING(ih.identifier, 13, LEN(ih.identifier) - 12) AS INT) IN (SELECT process_tab_id FROM process_tabs WHERE process IN (" +
                    processes + "))) ";
                processes_sql2 = " AND c2.variable IN (SELECT ih.name_variable FROM " + Conv.ToSql(targetDb) +
                                 ".dbo.instance_helpers ih WHERE c2.instance = ih.instance AND CAST(SUBSTRING(ih.identifier, 13, LEN(ih.identifier) - 12) AS INT) IN (SELECT process_tab_id FROM " +
                                 Conv.ToSql(targetDb) + ".dbo.process_tabs WHERE process IN (" + processes + "))) ";
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.code  " +
                " , c1.variable " +
                " , c1.translation " +
                " FROM instance_translations c1 " +
                " WHERE c1.instance = @sourceInstance " +
                processes_sql1 +
                " ), c2 AS ( " +
                " SELECT c2.code " +
                " , c2.variable " +
                " , c2.translation " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_translations c2 " +
                " WHERE c2.instance = @targetInstance " +
                processes_sql2 +
                " ) " +
                " SELECT c1.code AS code1, c2.code AS code2 " +
                " , c1.variable AS variable1, c2.variable AS variable2 " +
                " , c1.translation AS translation1, c2.translation AS translation2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.code = c2.code AND c1.variable = c2.variable " +
                " WHERE c1.variable IS NOT NULL " +
                " AND (c2.variable IS NULL " +
                " OR c1.translation <> c2.translation " +
                " ) ",
                DBParameters);
        }

        public List<Dictionary<string, object>> FrontpageImagesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Frontpage");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "SELECT c1.attachment_id AS id1, c2.attachment_id AS id2, " +
                " c1.guid AS guid1, c2.guid AS guid2, " +
                " c1.control_name AS control_name1, c2.control_name AS control_name2, " +
                " c1.name AS name1, c2.name AS name2 " +
                " FROM attachments c1 " +
                " LEFT JOIN " + Conv.ToSql(targetDb) + ".dbo.attachments c2 " +
                " ON c1.guid = c2.guid " +
                " AND c2.instance = @targetInstance AND c2.parent_id = 'frontpage' AND c2.type = 0 AND c2.control_name IN ('banner', 'textbanner') " +
                " WHERE c1.instance = @sourceInstance AND c1.parent_id = 'frontpage' AND c1.type = 0 AND c1.control_name IN ('banner', 'textbanner') " +
                " AND c2.attachment_id IS NULL AND c1.attachment_id IS NOT NULL " +
                " ORDER BY c1.attachment_id ASC ", DBParameters);
        }

        public List<Dictionary<string, object>> InstanceCurrenciesCompare(string sourceInstance, string targetInstance,
            string targetDb = "", string processes = "") {
            InstanceCompareProgress.SetCompareProgress("Currencies");
            if (processes.Length > 0) {
                return new List<Dictionary<string, object>>();
            }

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);
            return db.GetDatatableDictionary(
                "WITH c1 AS ( " +
                " SELECT c1.currency_id  " +
                " , c1.name " +
                " , c1.abbr " +
                " , c1.baseline_value " +
                " FROM instance_currencies c1 " +
                " WHERE c1.instance = @sourceInstance " +
                " ), c2 AS ( " +
                " SELECT c2.currency_id " +
                " , c2.name " +
                " , c2.abbr " +
                " , c2.baseline_value " +
                " FROM " + Conv.ToSql(targetDb) + ".dbo.instance_currencies c2 " +
                " WHERE c2.instance = @targetInstance " +
                " ) " +
                " SELECT c1.currency_id AS id1, c2.currency_id AS id2 " +
                " , c1.name AS name1, c2.name AS name2 " +
                " , c1.abbr AS abbr1, c2.abbr AS abbr2 " +
                " , c1.baseline_value AS baseline_value1, c2.baseline_value AS baseline_value2 " +
                " FROM c1 " +
                " LEFT JOIN c2 ON c1.name = c2.name " +
                " WHERE c1.name IS NOT NULL " +
                " AND (c2.name IS NULL " +
                " OR c1.abbr <> c2.abbr " +
                " OR c1.baseline_value <> c2.baseline_value " +
                " ) ",
                DBParameters);
        }

        // /\ Selects /\

        public List<string> GetUpdateSql(Dictionary<string, object> retval, string targetInstance, string targetDb = "",
            string sourceDb = "", bool skipProcessPortfoliosDownloadTemplates = false, bool skipHelps = false,
            bool skipConfigs = false, bool skipRequests = false) {

            inserted_processes = new List<string>();

            var updates = new List<string>();

            string sourceDb_att;
            DataRow release =
                db.GetDataRow(
                    "SELECT TOP 1 attachments_data_db FROM " + Conv.ToSql(sourceDb) +
                    ".dbo.release_history ORDER BY installation_date DESC", new List<SqlParameter>());
            if (release != null && Conv.ToStr(release["attachments_data_db"]).Length > 0) {
                sourceDb_att = Conv.ToStr(release["attachments_data_db"]);
            } else {
                sourceDb_att = sourceDb;
            }

            string targetDb_att;
            release = db.GetDataRow(
                "SELECT TOP 1 attachments_data_db FROM " + Conv.ToSql(targetDb) +
                ".dbo.release_history ORDER BY installation_date DESC", new List<SqlParameter>());
            if (release != null && Conv.ToStr(release["attachments_data_db"]).Length > 0) {
                targetDb_att = Conv.ToStr(release["attachments_data_db"]);
            } else {
                targetDb_att = targetDb;
            }

            // \/ Deletes \/

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceCurrenciesDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.instance_currencies WHERE currency_id = " + toSqlVal(r["id1"]) + "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["frontpageImagesDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb_att) +
                                ".dbo.attachments_data WHERE attachment_id = " + r["id1"]);
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.attachments WHERE attachment_id = " +
                                r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceTranslationsDeletes"]) {
                if (r["variable2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.instance_translations WHERE instance = " +
                                toSqlVal(targetInstance) + " AND code = " + toSqlVal(r["code1"]) + " AND variable = " +
                                toSqlVal(r["variable1"]) + "; ");
                }
            }

            if (!skipHelps) {
                foreach (var r in (List<Dictionary<string, object>>)retval["instanceHelpersDeletes"]) {
                    if (r["id2"] == DBNull.Value) {
                        updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                    ".dbo.instance_helpers WHERE instance_helper_id = " + toSqlVal(r["id1"]) + "; ");
                    }
                }
            }

            //foreach (var r in (List<Dictionary<string, object>>)retval["instanceSchedulesDeletes"]) {
            //	if (r["guid2"] == DBNull.Value) {
            //		updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.instance_schedules WHERE guid = " + toSqlVal(r["guid1"]) + "; ");
            //	}
            //}

            //foreach (var r in (List<Dictionary<string, object>>)retval["instanceScheduleGroupsDeletes"]) {
            //	if (r["group_id2"] == DBNull.Value) {
            //		updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.instance_schedule_groups WHERE group_id = " + toSqlVal(r["group_id1"]) + "; ");
            //	}
            //}

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnionColumnsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.instance_union_columns WHERE instance_union_column_id = " + toSqlVal(r["id1"]) +
                                "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnionProcessesDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.instance_union_processes WHERE instance_union_process_id = " +
                                toSqlVal(r["id1"]) + "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnionRowsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.instance_union_rows WHERE instance_union_row_id = " + toSqlVal(r["id1"]) + "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnionsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.instance_unions WHERE instance_union_id = " + toSqlVal(r["id1"]) + "; ");
                }
            }

            if (!skipConfigs) {
                foreach (var r in (List<Dictionary<string, object>>)retval["instanceConfigurationsDeletes"]) {
                    if (r["id2"] == DBNull.Value) {
                        updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.instance_configurations " +
                                    " WHERE instance = " + toSqlVal(targetInstance) +
                                    " AND process " +
                                    (DBNull.Value.Equals(r["process1"])
                                        ? " IS NULL "
                                        : " = " + toSqlVal(r["process1"])) +
                                    " AND category = 'View Configurator' AND name = " + toSqlVal(r["name1"]) +
                                    " AND [server] = 0; ");
                    }
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notificationReceiversDeletes"]) {
                if (r["notification_condition_id2"] == DBNull.Value) {
                    var sql = "DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.notification_receivers ";
                    sql += " WHERE COALESCE(notification_condition_id, '') = COALESCE(" + Conv.ToSql(targetDb) +
                           ".dbo.notificationCondition_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["notification_condition_id1"]) + "), '') ";
                    sql += " AND COALESCE(item_column_id, '') = COALESCE(" + Conv.ToSql(targetDb) +
                           ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) +
                           "), '') ";
                    sql += " AND COALESCE(list_process, '') = COALESCE(" + Conv.ToSql(targetDb) +
                           ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["list_process1"]) +
                           "), '') ";
                    sql += " AND COALESCE(process_action_row_id, '') = COALESCE(" + Conv.ToSql(targetDb) +
                           ".dbo.processActionRows_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_action_row_id1"]) + "), '') ";
                    sql += " AND COALESCE(list_item_column_id, '') = COALESCE(" + Conv.ToSql(targetDb) +
                           ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["list_item_column_id1"]) + "), '') ";
                    sql += " AND COALESCE(user_group_item_id, '') = COALESCE((SELECT item_id FROM " +
                           Conv.ToSql(targetDb) + ".dbo.items WHERE [guid] = " + toSqlVal(r["user_group_item_id1"]) +
                           "), '') ; ";
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notificationConditionsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.notification_conditions WHERE notification_condition_id = " +
                                Conv.ToSql(targetDb) +
                                ".dbo.notificationCondition_GetId(" + toSqlVal(targetInstance) + ", " +
                                toSqlVal(r["id1"]) + ") ; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notificationTagLinksDeletes"]) {
                if (r["guid2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE item_id = " +
                                toSqlVal(r["item_id"]) + "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notificationDeletes"]) {
                if (r["guid2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE item_id = " +
                                toSqlVal(r["item_id"]) + " ; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["menuStatesDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                            ".dbo.instance_menu_states WHERE instance_menu_id =  " +
                            targetDb + ".dbo.menu_GetId(" + toSqlVal(targetInstance) + ", " +
                            toSqlVal(r["instance_menu_id"]) + ") AND item_id = (SELECT item_id FROM " +
                            Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " + toSqlVal(r["item_id"]) +
                            ") AND state = " + toSqlVal(r["state1"]) + " ; ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["menuRightsDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                            ".dbo.instance_menu_rights WHERE instance_menu_id =  " +
                            targetDb + ".dbo.menu_GetId(" + toSqlVal(targetInstance) + ", " +
                            toSqlVal(r["instance_menu_id"]) + ") AND item_id = (SELECT item_id FROM " +
                            Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " + toSqlVal(r["item_id"]) + ") ; ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["menuDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.instance_menu WHERE instance = " +
                                toSqlVal(targetInstance) + " AND instance_menu_id = " + Conv.ToSql(targetDb) +
                                ".dbo.menu_GetId(" +
                                toSqlVal(targetInstance) + ", " + toSqlVal(r["id1"]) + "); ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionsGroupsConditionDeletes"]) {
                if (r["condition_id2"] == DBNull.Value) {
                    string sql = "DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.conditions_groups_condition ";
                    sql += " WHERE condition_id = " + Conv.ToSql(targetDb) + ".dbo.condition_GetId(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id1"]) + ") ; ";
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionsGroupsDeletes"]) {
                if (r["variable2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.conditions_groups " +
                                " WHERE instance = " + toSqlVal(targetInstance) +
                                " AND process = " + toSqlVal(r["process1"]) +
                                " AND variable = " + toSqlVal(r["variable1"]) + "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionUserGroupsDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.condition_user_groups WHERE condition_id = " +
                            Conv.ToSql(targetDb) +
                            ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id"]) +
                            ") AND item_id = (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                            ".dbo.items WHERE guid = " + toSqlVal(r["item_id"]) + ")  ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionTabsDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.condition_tabs WHERE condition_id = " +
                            Conv.ToSql(targetDb) +
                            ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id"]) +
                            ") and process_tab_id = " + Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" +
                            toSqlVal(targetInstance) +
                            ", " + toSqlVal(r["process_tab_id"]) + "); ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionStatesDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.condition_states WHERE condition_id = " +
                            Conv.ToSql(targetDb) +
                            ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id"]) +
                            ") and state = " + toSqlVal(r["state"]) + " ; ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionPortfoliosDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.condition_portfolios WHERE condition_id = " +
                            Conv.ToSql(targetDb) +
                            ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id"]) +
                            ") and process_portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                            toSqlVal(targetInstance) + ", " + toSqlVal(r["process_portfolio_id"]) + "); ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionGroupsDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.condition_groups WHERE condition_id = " +
                            Conv.ToSql(targetDb) +
                            ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id"]) +
                            ") and process_group_id = " + Conv.ToSql(targetDb) + ".dbo.processGroup_GetId(" +
                            toSqlVal(targetInstance) + ", " + toSqlVal(r["process_group_id"]) + "); ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionFeaturesDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.condition_features WHERE condition_id = " +
                            Conv.ToSql(targetDb) +
                            ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id"]) +
                            ") and feature = " + toSqlVal(r["feature"]) + " ; ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionContainersDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.condition_containers WHERE condition_id = " +
                            Conv.ToSql(targetDb) +
                            ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id"]) +
                            ") and process_container_id = " + Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" +
                            toSqlVal(targetInstance) + ", " + toSqlVal(r["process_container_id"]) + "); ");
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardUserGroupsDeletes"]) {
                if (r["dashboard2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.process_dashboard_user_groups " +
                                "WHERE process_dashboard_id = " + Conv.ToSql(targetDb) +
                                ".dbo.processDashboard_GetId(" + toSqlVal(targetInstance) + ", " +
                                toSqlVal(r["dashboard1"]) + ") " +
                                "AND user_group_id = (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                                ".dbo.items WHERE guid = " + toSqlVal(r["user_group1"]) + "); ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardChartLinksDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_dashboard_chart_links WHERE process_dashboard_chart_link_id = " +
                                r["id1"] + "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardChartMapDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.process_dashboard_chart_map " +
                                "WHERE process_dashboard_chart_id = " + r["process_dashboard_chart_id1"] +
                                "AND process_dashboard_id = " + r["process_dashboard_id1"] +
                                "AND order_no = " + toSqlVal(r["order_no1"]));
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardChartsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_dashboard_charts WHERE process_dashboard_chart_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_dashboards WHERE process_dashboard_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processTabsSubprocessDataDeletes"]) {
                if (r["process_tab_subprocess_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.process_tabs_subprocess_data" +
                                " WHERE process_tab_subprocess_id = " + r["process_tab_subprocess_id_id1"] +
                                " AND parent_process_item_id = " + r["parent_process_item_id_id1"] +
                                " AND item_id = " + r["item_id_id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processTabsSubprocessesDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_tabs_subprocesses WHERE process_tab_subprocess_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["tabColumnsDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                            ".dbo.process_tab_columns WHERE process_tab_column_id = " +
                            toSqlVal(r["process_tab_column_id"]));
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groupNavigationDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                            ".dbo.process_group_navigation WHERE process_navigation_id = " +
                            toSqlVal(r["process_navigation_id"]));
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groupTabsDeletes"]) {
                updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                            ".dbo.process_group_tabs WHERE process_group_tab_id = " +
                            toSqlVal(r["process_group_tab_id"]));
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groupFeaturesDeletes"]) {
                var sql = "DELETE FROM " + Conv.ToSql(targetDb) +
                          ".dbo.process_group_features WHERE process_group_id = " +
                          targetDb + ".dbo.processGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                          toSqlVal(r["process_group_id1"]) + ") ";
                sql += " AND feature = " + toSqlVal(r["feature1"]) + " ";
                sql += " AND state = " + toSqlVal(r["state1"]) + " ";
                sql += " AND COALESCE(custom_name, '') = COALESCE(" + toSqlVal(r["custom_name1"]) + ", '') ";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["itemColumnEquationsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.item_column_equations WHERE item_column_equation_id = " + toSqlVal(r["id1"]));
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["itemColumnJoinsDeletes"]) {
                if (r["item_column_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.item_column_joins WHERE item_column_join_id = " +
                                toSqlVal(r["item_column_join_id"]));
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actionRowsDeletes"]) {
                if (r["process_action_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_action_rows WHERE process_action_row_id = " +
                                toSqlVal(r["process_action_row_id"]));
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actionRowsGroupsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_action_rows_groups WHERE process_action_group_id = " +
                                toSqlVal(r["id1"]));
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["tabContainersDeletes"]) {
                if (r["process_tab_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_tab_containers WHERE process_container_id = " + Conv.ToSql(targetDb) +
                                ".dbo.processContainer_GetId(" + toSqlVal(targetInstance) + "," +
                                toSqlVal(r["process_container_id"]) + ") AND process_tab_id = " + Conv.ToSql(targetDb) +
                                ".dbo.processTab_GetId(" + toSqlVal(targetInstance) + ", " +
                                toSqlVal(r["process_tab_id"]) + ")");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioUserGroupsDeletes"]) {
                if (r["process_portfolio_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolio_user_groups WHERE process_portfolio_id = " +
                                r["process_portfolio_id_id1"] + " AND item_id = " + r["item_id_id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioTimemachineSavedDeletes"]) {
                if (r["save_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolio_timemachine_saved WHERE save_id = " + r["save_id1"] +
                                " AND item_id = " + r["item_id_id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioGroupsDeletes"]) {
                if (r["process_portfolio_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolio_groups WHERE process_portfolio_id = " +
                                r["process_portfolio_id_id1"] + " AND process_group_id = " + r["process_group_id_id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioActionsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolio_actions WHERE process_portfolio_action_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioDialogsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolio_dialogs WHERE process_portfolio_dialog_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["portfolioColumnsDeletes"]) {
                if (r["process_portfolio_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolio_columns where process_portfolio_id = " + Conv.ToSql(targetDb) +
                                ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + "," +
                                toSqlVal(r["process_portfolio_id"]) + ") and item_column_id = " + Conv.ToSql(targetDb) +
                                ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                                toSqlVal(r["item_column_id"]) + ")");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["portfolioColumnGroupsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolio_columns_groups WHERE portfolio_column_group_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["containerColumnsDeletes"]) {
                if (r["process_container_column_id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_container_columns WHERE process_container_column_id = " +
                                toSqlVal(r["process_container_column_id1"]));
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actionsDialogDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("UPDATE " + Conv.ToSql(targetDb) +
                                ".dbo.item_columns SET process_dialog_id = NULL WHERE process_dialog_id = " + r["id1"]);
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_actions_dialog WHERE process_action_dialog_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actionsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("UPDATE " + Conv.ToSql(targetDb) +
                                ".dbo.processes SET new_row_action_id = NULL WHERE new_row_action_id = " + r["id1"]);
                    updates.Add("UPDATE " + Conv.ToSql(targetDb) +
                                ".dbo.processes SET after_new_row_action_id = NULL WHERE after_new_row_action_id = " +
                                r["id1"]);
                    updates.Add("UPDATE " + Conv.ToSql(targetDb) +
                                ".dbo.item_columns SET process_action_id = NULL WHERE process_action_id = " + r["id1"]);
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_actions WHERE process_action_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.conditions WHERE condition_id = " +
                                r["id1"] + "; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["tabsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.process_tabs WHERE process_tab_id = " +
                                r["id1"]);
                }
            }

            if (!skipProcessPortfoliosDownloadTemplates) {
                foreach (var r in (List<Dictionary<string, object>>)retval["portfoliosDownloadTemplatesDeletes"]) {
                    if (r["id2"] == DBNull.Value) {
                        updates.Add("DELETE FROM " + Conv.ToSql(targetDb_att) +
                                    ".dbo.attachments_data WHERE attachment_id = " + r["id1"]);
                        updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.attachments WHERE attachment_id = " +
                                    r["id1"]);
                    }
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["portfoliosDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_portfolios WHERE process_portfolio_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groupsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_groups WHERE process_group_id = " + r["id1"]);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["containersDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) +
                                ".dbo.process_containers WHERE process_container_id = " + r["id1"]);
                }
            }

            foreach (var process in (List<Dictionary<string, object>>)retval["___matrixes"]) {
                foreach (var r in (List<Dictionary<string, object>>)retval[
                             "___matrix_" + Conv.ToStr(process["process"] + "Deletes")]) {
                    if (Conv.ToInt(process["compare"]) == 3 && r["guid2"] == DBNull.Value) {
                        updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.items " +
                                    "WHERE instance = " + toSqlVal(targetInstance) + " AND guid = " +
                                    toSqlVal(r["guid1"]) + " ; ");
                    }
                }
            }

            foreach (var list in (List<Dictionary<string, object>>)retval["___listItems"]) {
                foreach (var r in (List<Dictionary<string, object>>)retval[Conv.ToStr(list["list"] + "Deletes")]) {
                    if (Conv.ToInt(list["compare"]) == 3 && r["guid2"] == DBNull.Value) {
                        updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.items " +
                                    "WHERE instance = " + toSqlVal(targetInstance) + " AND guid = " +
                                    toSqlVal(r["guid1"]) + " ; ");
                    }
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["itemColumnsDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.item_columns WHERE item_column_id = " +
                                r["id1"] + " ; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processSubprocessesDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.process_subprocesses WHERE instance = " +
                                toSqlVal(targetInstance) +
                                " AND parent_process = " + toSqlVal(r["parent_process1"]) + " AND process = " +
                                toSqlVal(r["process1"]) + " ; ");
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processesDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.processes WHERE instance = " +
                                toSqlVal(targetInstance) + " AND process = " + toSqlVal(r["id1"]) + " ; ");
                }
            }

            //foreach (var r in (List<Dictionary<string, object>>)retval["calendarStaticHolidaysDeletes"]) {
            //	var sql = "DELETE FROM " + Conv.toSql(targetDb) + ".dbo.calendar_static_holidays WHERE calendar_id = " +
            //				targetDb + ".dbo.calendar_GetId(" + toSqlVal(targetInstance) + ", " +
            //				toSqlVal(r["calendar_id"]) + ") ";
            //	sql += " AND name = " + toSqlVal(r["name"]) + " ";
            //	sql += " AND month = " + toSqlVal(r["month"]) + " ";
            //	sql += " AND day = " + toSqlVal(r["day"]) + " ; ";
            //	updates.Add(sql);
            //}

            //foreach (var r in (List<Dictionary<string, object>>)retval["calendarEventsDeletes"]) {
            //	var sql = "DELETE FROM " + Conv.toSql(targetDb) + ".dbo.calendar_events WHERE calendar_id = " + Conv.toSql(targetDb) +
            //				".dbo.calendar_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["calendar_id"]) +
            //				") ";
            //	sql += " AND name = " + toSqlVal(r["name"]) + " ";
            //	sql += " AND event_start = " + toSqlVal(r["event_start"]) + " ";
            //	sql += " AND event_end = " + toSqlVal(r["event_end"]) + " ";
            //	sql += " AND event_type = " + toSqlVal(r["event_type"]) + " ; ";
            //	updates.Add(sql);
            //}

            //foreach (Dictionary<string, object> r in (List<Dictionary<string, object>>)retval["calendarsDeletes"]) {
            //	if (r["id2"] == DBNull.Value) {
            //		updates.Add("DELETE FROM " + Conv.toSql(targetDb) + ".dbo.calendars WHERE instance = " + toSqlVal(targetInstance) + " AND calendar_id = " + toSqlVal(r["id1"]));
            //	}
            //}

            //foreach (var r in (List<Dictionary<string, object>>)retval["translationsDeletes"]) {
            //	if (r["id2"] == DBNull.Value) {
            //		updates.Add("DELETE FROM " + Conv.toSql(targetDb) +
            //					".dbo.process_translations WHERE process_translation_id = " +
            //					toSqlVal(r["process_translation_id"]) + " ; ");
            //	}
            //}

            foreach (var r in (List<Dictionary<string, object>>)retval["languagesDeletes"]) {
                if (r["id2"] == DBNull.Value) {
                    updates.Add("DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.instance_languages WHERE instance = " +
                                toSqlVal(targetInstance) + " AND code = " + toSqlVal(r["id1"]) + " ; ");
                }
            }

            // /\ Deletes /\

            // \/ Inserts & Updates \/

            foreach (var r in (List<Dictionary<string, object>>)retval["languages"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_languages (instance, code, name, abbr, icon_url) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["id1"]) + ", ";
                    sql += toSqlVal(r["name1"]) + ", ";
                    sql += toSqlVal(r["abbr1"]) + ", ";
                    sql += toSqlVal(r["icon_url1"]) + " ); ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_languages SET ";

                    if (!r["name1"].Equals(r["name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " name = " + toSqlVal(r["name1"]);
                    }

                    if (!r["abbr1"].Equals(r["abbr2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " abbr = " + toSqlVal(r["abbr1"]);
                    }

                    if (!r["icon_url1"].Equals(r["icon_url2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " icon_url = " + toSqlVal(r["icon_url1"]);
                    }

                    sql += " WHERE instance = " + toSqlVal(targetInstance) + " AND code = " + toSqlVal(r["id2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            //foreach (Dictionary<string, object> r in (List<Dictionary<string, object>>)retval["calendars"]) {
            //	string sql = "";
            //	if (r["id2"] == DBNull.Value) {
            //		sql += "insert into " + Conv.toSql(targetDb) + ".dbo.calendars (instance, variable, work_hours, def_1, def_2, def_3, def_4, def_5, def_6, def_7, is_default) values (";
            //		sql += toSqlVal(targetInstance) + ", ";
            //		sql += toSqlVal(r["variable"]) + ", ";
            //		sql += toSqlVal(r["work_hours1"]) + ", ";
            //		sql += toSqlVal(r["def_1_1"]) + ", ";
            //		sql += toSqlVal(r["def_2_1"]) + ", ";
            //		sql += toSqlVal(r["def_3_1"]) + ", ";
            //		sql += toSqlVal(r["def_4_1"]) + ", ";
            //		sql += toSqlVal(r["def_5_1"]) + ", ";
            //		sql += toSqlVal(r["def_6_1"]) + ", ";
            //		sql += toSqlVal(r["def_7_1"]) + ", ";
            //		sql += toSqlVal(r["is_default1"]) + ") ";
            //	}
            //	else {
            //		bool u = false;
            //		sql = "update " + Conv.toSql(targetDb) + ".dbo.calendars set ";
            //		if (!r["work_hours1"].Equals(r["work_hours2"])) { if (u) { sql += ", "; } u = true; sql += " work_hours = " + toSqlVal(r["work_hours1"]); }
            //		if (!r["def_1_1"].Equals(r["def_1_2"])) { if (u) { sql += ", "; } u = true; sql += " def_1 = " + toSqlVal(r["def_1_1"]); }
            //		if (!r["def_2_1"].Equals(r["def_2_2"])) { if (u) { sql += ", "; } u = true; sql += " def_2 = " + toSqlVal(r["def_2_1"]); }
            //		if (!r["def_3_1"].Equals(r["def_3_2"])) { if (u) { sql += ", "; } u = true; sql += " def_3 = " + toSqlVal(r["def_3_1"]); }
            //		if (!r["def_4_1"].Equals(r["def_4_2"])) { if (u) { sql += ", "; } u = true; sql += " def_4 = " + toSqlVal(r["def_4_1"]); }
            //		if (!r["def_5_1"].Equals(r["def_5_2"])) { if (u) { sql += ", "; } u = true; sql += " def_5 = " + toSqlVal(r["def_5_1"]); }
            //		if (!r["def_6_1"].Equals(r["def_6_2"])) { if (u) { sql += ", "; } u = true; sql += " def_6 = " + toSqlVal(r["def_6_1"]); }
            //		if (!r["def_7_1"].Equals(r["def_7_2"])) { if (u) { sql += ", "; } u = true; sql += " def_7 = " + toSqlVal(r["def_7_1"]); }
            //		if (!r["is_default1"].Equals(r["is_default2"])) { if (u) { sql += ", "; } u = true; sql += " is_default = " + toSqlVal(r["is_default1"]); }
            //		sql += " where instance = " + toSqlVal(targetInstance) + " and calendar_id = " + toSqlVal(r["id2"]) + " ; ";
            //	}
            //	sql = sql.Replace("'null'", "null");
            //	updates.Add(sql);
            //}

            //foreach (var r in (List<Dictionary<string, object>>)retval["calendarEvents"]) {
            //	var sql = "";
            //	sql += "INSERT INTO " + Conv.toSql(targetDb) + ".dbo.calendar_events(calendar_id, name, event_start, event_end, event_type) VALUES ( ";
            //	sql += Conv.toSql(targetDb) + ".dbo.calendar_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["calendar_id"]) + "), ";
            //	sql += toSqlVal(r["name"]) + ", ";
            //	sql += toSqlVal(r["event_start"]) + ", ";
            //	sql += toSqlVal(r["event_end"]) + ", ";
            //	sql += toSqlVal(r["event_type"]) + ") ";
            //	updates.Add(sql);
            //}

            //foreach (var r in (List<Dictionary<string, object>>)retval["calendarStaticHolidays"]
            //) {
            //	var sql = "";
            //	sql += "INSERT INTO " + Conv.toSql(targetDb) + ".dbo.calendar_static_holidays(calendar_id, name, month, day) VALUES ( ";
            //	sql += Conv.toSql(targetDb) + ".dbo.calendar_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["calendar_id"]) + "), ";
            //	sql += toSqlVal(r["name"]) + ", ";
            //	sql += toSqlVal(r["month"]) + ", ";
            //	sql += toSqlVal(r["day"]) + ") ";
            //	updates.Add(sql);
            //}

            foreach (var r in (List<Dictionary<string, object>>)retval["processes"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    inserted_processes.Add(Conv.ToStr(r["id1"]));

                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.processes (instance, process, variable, list_process, process_type, new_row_action_id";
                    sql +=
                        ", colour, list_order, system_process, portfolio_show_Type, after_new_row_action_id, list_type, compare, require_insert) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["id1"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += toSqlVal(r["list_process1"]) + ", ";
                    sql += toSqlVal(r["process_type1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["new_row_action_id1"]) + "), ";
                    sql += toSqlVal(r["colour1"]) + ", ";
                    sql += toSqlVal(r["list_order1"]) + ", ";
                    sql += toSqlVal(r["system_process1"]) + ", ";
                    sql += toSqlVal(r["portfolio_show_Type1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["after_new_row_action_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.item_GetId(" + toSqlVal(r["list_type1"]) + "), ";
                    sql += toSqlVal(r["compare1"]) + ", ";
                    sql += toSqlVal(r["require_insert1"]) + " ";
                    sql += "); ";
                    if (!r["ordering1"].Equals("1") || !r["archive1"].Equals("1")) {
                        sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.item_tables SET ";
                        sql += "ordering = " + toSqlVal(r["ordering1"]) + ", ";
                        sql += "archive = " + toSqlVal(r["archive1"]) + " ";
                        sql += "WHERE instance = " + toSqlVal(targetInstance) + " AND process = " + toSqlVal(r["id2"]) +
                               " ; ";
                    }
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.processes SET ";

                    if (!r["list_process1"].Equals(r["list_process2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " list_process = " + toSqlVal(r["list_process1"]);
                    }

                    if (!r["process_type1"].Equals(r["process_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_type = " + toSqlVal(r["process_type1"]);
                    }

                    if (!r["new_row_action_id1"].Equals(r["new_row_action_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " new_row_action_id = " + Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["new_row_action_id1"]) + ") ";
                    }

                    if (!r["colour1"].Equals(r["colour2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " colour = " + toSqlVal(r["colour1"]);
                    }

                    if (!r["list_order1"].Equals(r["list_order2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " list_order = " + toSqlVal(r["list_order1"]);
                    }

                    if (!r["system_process1"].Equals(r["system_process2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " system_process = " + toSqlVal(r["system_process1"]);
                    }

                    if (!r["portfolio_show_Type1"].Equals(r["portfolio_show_Type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " portfolio_show_Type = " + toSqlVal(r["portfolio_show_Type1"]);
                    }

                    if (!r["after_new_row_action_id1"].Equals(r["after_new_row_action_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " after_new_row_action_id = " + Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["after_new_row_action_id1"]) + ") ";
                    }

                    if (!r["list_type1"].Equals(r["list_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " list_type = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" + toSqlVal(r["list_type1"]) +
                               ") ";
                    }

                    if (!r["compare1"].Equals(r["compare2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " compare = " + toSqlVal(r["compare1"]);
                    }

                    if (!r["require_insert1"].Equals(r["require_insert2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " require_insert = " + toSqlVal(r["require_insert1"]);
                    }

                    sql += " WHERE instance = " + toSqlVal(targetInstance) + " AND process = " + toSqlVal(r["id2"]) +
                           " ;";

                    if (!u) sql = "";

                    if (!r["ordering1"].Equals(r["ordering2"]) || !r["archive1"].Equals(r["archive2"])) {
                        sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.item_tables SET ";
                        sql += "ordering = " + toSqlVal(r["ordering1"]) + ", ";
                        sql += "archive = " + toSqlVal(r["archive1"]) + " ";
                        sql += "WHERE instance = " + toSqlVal(targetInstance) + " AND process = " + toSqlVal(r["id2"]) +
                               " ; ";
                    }
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processSubprocesses"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_subprocesses (instance, parent_process, process) ";
                    sql += "VALUES (" + toSqlVal(targetInstance) + ", " + toSqlVal(r["parent_process1"]) + ", " +
                           toSqlVal(r["process1"]) + "); ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["tabs"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_tabs (instance, process, variable, order_no, use_in_new_item, archive_enabled, archive_item_column_id, maintenance_name, background) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += toSqlVal(r["order_no1"]) + ", ";
                    sql += toSqlVal(r["use_in_new_item1"]) + ", ";
                    sql += toSqlVal(r["archive_enabled1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["archive_item_column_id1"]) + "), ";
                    sql += toSqlVal(r["maintenance_name1"]) + ", ";
                    sql += toSqlVal(r["background1"]) + " ";
                    sql += ") ; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_tabs SET ";

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["use_in_new_item1"].Equals(r["use_in_new_item2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_in_new_item = " + toSqlVal(r["use_in_new_item1"]);
                    }

                    if (!r["archive_enabled1"].Equals(r["archive_enabled2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " archive_enabled = " + toSqlVal(r["archive_enabled1"]);
                    }

                    if (!r["archive_item_column_id1"].Equals(r["archive_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " archive_item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["archive_item_column_id1"]) + ")";
                    }

                    if (!r["maintenance_name1"].Equals(r["maintenance_name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " maintenance_name = " + toSqlVal(r["maintenance_name1"]);
                    }

                    if (!r["background1"].Equals(r["background2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " background = " + toSqlVal(r["background1"]);
                    }

                    sql += " WHERE process_tab_id = " + r["id2"] + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["portfolios"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_portfolios (instance, process, variable, process_portfolio_type, calendar_type, calendar_start_id, calendar_end_id" +
                           ", show_progress_arrows, open_split, default_state, recursive, recursive_filter_type, ignore_rights, hidden_progress_arrows, use_default_state" +
                           ", order_item_column_id, order_direction, open_row_item_column_id, open_row_process, timemachine, download_options, also_open_current" +
                           ", sub_default_state, openwidth, portfolio_default_view) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += toSqlVal(r["process_portfolio_type1"]) + ", ";
                    sql += toSqlVal(r["calendar_type1"]) + ", ";
                    sql += "ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["calendar_start_id1"]) + "), 0), ";
                    sql += "ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["calendar_end_id1"]) + "), 0), ";
                    sql += toSqlVal(r["show_progress_arrows1"]) + ", ";
                    sql += toSqlVal(r["open_split1"]) + ", ";
                    sql += toSqlVal(r["default_state1"]) + ", ";
                    sql += "ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["recursive1"]) + "), 0), ";
                    sql += toSqlVal(r["recursive_filter_type1"]) + ", ";
                    sql += toSqlVal(r["ignore_rights1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processGroup_GetIds(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["hidden_progress_arrows1"]) + "), ";
                    sql += toSqlVal(r["use_default_state1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["order_item_column_id1"]) + "), ";
                    sql += toSqlVal(r["order_direction1"]) + ", ";
                    sql += " ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["open_row_item_column_id1"]) + "), 0), ";
                    sql += toSqlVal(r["open_row_process1"]) + ", ";
                    sql += toSqlVal(r["timemachine1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_DownloadOptionsVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["download_options1"]) + "), ";
                    sql += toSqlVal(r["also_open_current1"]) + ", ";
                    sql += toSqlVal(r["sub_default_state1"]) + ", ";
                    sql += toSqlVal(r["openwidth1"]) + ", ";
                    sql += toSqlVal(r["portfolio_default_view1"]) + " ";
                    sql += "); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_portfolios SET ";

                    if (!r["process_portfolio_type1"].Equals(r["process_portfolio_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_portfolio_type = " + toSqlVal(r["process_portfolio_type1"]);
                    }

                    if (!r["calendar_type1"].Equals(r["calendar_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " calendar_type = " + toSqlVal(r["calendar_type1"]);
                    }

                    if (!r["calendar_start_id1"].Equals(r["calendar_start_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " calendar_start_id = ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["calendar_start_id1"]) + "), 0)";
                    }

                    if (!r["calendar_end_id1"].Equals(r["calendar_end_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " calendar_end_id = ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["calendar_end_id1"]) + "), 0)";
                    }

                    if (!r["show_progress_arrows1"].Equals(r["show_progress_arrows2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " show_progress_arrows = " + toSqlVal(r["show_progress_arrows1"]);
                    }

                    if (!r["open_split1"].Equals(r["open_split2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " open_split = " + toSqlVal(r["open_split1"]);
                    }

                    if (!r["default_state1"].Equals(r["default_state2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " default_state = " + toSqlVal(r["default_state1"]);
                    }

                    if (!r["recursive1"].Equals(r["recursive2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " recursive = ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["recursive1"]) + "), 0)";
                    }

                    if (!r["recursive_filter_type1"].Equals(r["recursive_filter_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " recursive_filter_type = " + toSqlVal(r["recursive_filter_type1"]);
                    }

                    if (!r["ignore_rights1"].Equals(r["ignore_rights2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " ignore_rights = " + toSqlVal(r["ignore_rights1"]);
                    }

                    if (!r["hidden_progress_arrows1"].Equals(r["hidden_progress_arrows2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " hidden_progress_arrows = " + Conv.ToSql(targetDb) + ".dbo.processGroup_GetIds(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["hidden_progress_arrows1"]) + ")";
                    }

                    if (!r["use_default_state1"].Equals(r["use_default_state2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_default_state = " + toSqlVal(r["use_default_state1"]);
                    }

                    if (!r["order_item_column_id1"].Equals(r["order_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["order_item_column_id1"]) + ")";
                    }

                    if (!r["order_direction1"].Equals(r["order_direction2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_direction = " + toSqlVal(r["order_direction1"]);
                    }

                    if (!r["open_row_item_column_id1"].Equals(r["open_row_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " open_row_item_column_id = ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["open_row_item_column_id1"]) + "), 0)";
                    }

                    if (!r["open_row_process1"].Equals(r["open_row_process2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " open_row_process = " + toSqlVal(r["open_row_process1"]);
                    }

                    if (!r["timemachine1"].Equals(r["timemachine2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " timemachine = " + toSqlVal(r["timemachine1"]);
                    }

                    if (!r["download_options1"].Equals(r["download_options2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " download_options = " + Conv.ToSql(targetDb) +
                               ".dbo.processPortfolio_DownloadOptionsVarReplace(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["download_options1"]) + ")";
                    }

                    if (!r["also_open_current1"].Equals(r["also_open_current2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " also_open_current = " + toSqlVal(r["also_open_current1"]);
                    }

                    if (!r["sub_default_state1"].Equals(r["sub_default_state2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sub_default_state = " + toSqlVal(r["sub_default_state1"]);
                    }

                    if (!r["openwidth1"].Equals(r["openwidth2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " openwidth = " + toSqlVal(r["openwidth1"]);
                    }

                    if (!r["portfolio_default_view1"].Equals(r["portfolio_default_view2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " portfolio_default_view = " + toSqlVal(r["portfolio_default_view1"]);
                    }

                    sql += " WHERE process_portfolio_id = " + r["id2"] + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            if (!skipProcessPortfoliosDownloadTemplates) {
                foreach (var r in (List<Dictionary<string, object>>)retval["portfoliosDownloadTemplates"]) {
                    var sql = "";
                    if (r["id2"] == DBNull.Value) {
                        sql += "DECLARE @new_id INT; ";
                        sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.attachments (instance, parent_id, control_name, type, name) VALUES ( ";
                        sql += toSqlVal(targetInstance) + ", ";
                        sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["parent_id1"]) + "), ";
                        sql += "'portfolioTemplates', ";
                        sql += "0, ";
                        sql += toSqlVal(r["name1"]) + "); ";
                        sql += "SET @new_id = @@identity; ";
                        sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo.attachments SET guid = " + toSqlVal(r["guid1"]) +
                               " WHERE attachment_id = @new_id ; ";
                        sql += "INSERT INTO " + Conv.ToSql(targetDb_att) +
                               ".dbo.attachments_data (attachment_id, filename, content_type, filesize, filedata) ";
                        sql += "SELECT @new_id, filename, content_type, filesize, filedata FROM " +
                               Conv.ToSql(sourceDb_att) + ".dbo.attachments_data WHERE attachment_id = " +
                               Conv.ToInt(r["id1"]) + "; ";
                    }

                    sql = sql.Replace("'null'", "null");
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actions"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_actions (instance, process, variable, condition_id, use_action_history, action_history_item_column_id, action_history_text) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["condition_id1"]) + "), ";
                    sql += toSqlVal(r["use_action_history1"]) + ", ";
                    sql += "ISNULL(" + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["action_history_item_column_id1"]) + "), 0), ";
                    sql += toSqlVal(r["action_history_text1"]) + " ";
                    sql += "); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_actions SET ";

                    if (!r["condition_id1"].Equals(r["condition_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " condition_id = " + Conv.ToSql(targetDb) + ".dbo.condition_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id1"]) + ")";
                    }

                    if (!r["use_action_history1"].Equals(r["use_action_history2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_action_history = " + toSqlVal(r["use_action_history1"]);
                    }

                    if (!r["action_history_item_column_id1"].Equals(r["action_history_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " action_history_item_column_id = ISNULL(" + Conv.ToSql(targetDb) +
                               ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["action_history_item_column_id1"]) + "), 0)";
                    }

                    if (!r["action_history_text1"].Equals(r["action_history_text2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " action_history_text = " + toSqlVal(r["action_history_text1"]);
                    }

                    sql += " WHERE process_action_id = " + r["id2"] + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["itemColumns"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.item_columns (instance, process, name, variable, data_type, system_column";
                    sql +=
                        ", data_additional, data_additional2, input_method, input_name, check_conditions, relative_column_id, process_dialog_id";
                    sql +=
                        ", process_action_id, status_column, link_process, use_fk, unique_col, parent_select_type, use_lucene, ic_validation";
                    sql += ", in_conditions, sub_data_additional2) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["name"]) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["data_type1"]) + ", ";
                    sql += toSqlVal(r["system_column1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumns_DaVarReplace(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["data_type1"]) + ", " + toSqlVal(r["da1_1"]) + "), ";
                    if (!skipConfigs || Conv.ToInt(r["data_type1"]) != 16) {
                        sql += Conv.ToSql(targetDb) + ".dbo.itemColumns_Da2VarReplace(" + toSqlVal(targetInstance) +
                               ", " + toSqlVal(r["data_type1"]) + ", " + toSqlVal(r["da2_1"]) + "), ";
                    } else {
                        sql += "NULL, ";
                    }

                    sql += toSqlVal(r["input_method1"]) + ", ";
                    sql += toSqlVal(r["input_name1"]) + ", ";
                    sql += toSqlVal(r["check_conditions1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["relative_column_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processActionDialog_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_dialog_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_action_id1"]) + "),  ";
                    sql += toSqlVal(r["status_column1"]) + " , ";
                    sql += toSqlVal(r["link_process1"]) + " , ";
                    sql += toSqlVal(r["use_fk1"]) + " , ";
                    sql += toSqlVal(r["unique_col1"]) + " , ";
                    sql += toSqlVal(r["parent_select_type1"]) + ", ";
                    sql += toSqlVal(r["use_lucene1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["ic_validation1"]) + "), ";
                    sql += toSqlVal(r["in_conditions1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumns_Da2SubVarReplace(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["data_type1"]) + ", " + toSqlVal(r["da2_1"]) + ", " +
                           toSqlVal(r["da2sub_1"]) + ") ";
                    sql += " ); ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.item_columns SET ";

                    if (!r["variable1"].Equals(r["variable2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " variable = " + toSqlVal(r["variable1"]);
                    }

                    if (!r["system_column1"].Equals(r["system_column2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " system_column = " + toSqlVal(r["system_column1"]);
                    }

                    if (!r["da1_1"].Equals(r["da1_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " data_additional = " + Conv.ToSql(targetDb) + ".dbo.itemColumns_DaVarReplace(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["data_type1"]) + ", " +
                               toSqlVal(r["da1_1"]) + " )";
                    }

                    if (!skipConfigs || Conv.ToInt(r["data_type1"]) != 16) {
                        if (!r["da2_1"].Equals(r["da2_2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " data_additional2 = " + Conv.ToSql(targetDb) + ".dbo.itemColumns_Da2VarReplace(" +
                                   toSqlVal(targetInstance) + ", " + toSqlVal(r["data_type1"]) + ", " +
                                   toSqlVal(r["da2_1"]) + " )";
                        }
                    }

                    if (!r["input_method1"].Equals(r["input_method2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " input_method = " + toSqlVal(r["input_method1"]);
                    }

                    if (!r["input_name1"].Equals(r["input_name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " input_name = " + toSqlVal(r["input_name1"]);
                    }

                    if (!r["check_conditions1"].Equals(r["check_conditions2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " check_conditions = " + toSqlVal(r["check_conditions1"]);
                    }

                    if (!r["relative_column_id1"].Equals(r["relative_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " relative_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["relative_column_id1"]) + ")";
                    }

                    if (!r["process_dialog_id1"].Equals(r["process_dialog_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_dialog_id = " + Conv.ToSql(targetDb) + ".dbo.processActionDialog_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_dialog_id1"]) + ")";
                    }

                    if (!r["process_action_id1"].Equals(r["process_action_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_action_id = " + Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_action_id1"]) + ")";
                    }

                    if (!r["status_column1"].Equals(r["status_column2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " status_column = " + toSqlVal(r["status_column1"]);
                    }

                    if (!r["link_process1"].Equals(r["link_process2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " link_process = " + toSqlVal(r["link_process1"]);
                    }

                    if (!r["use_fk1"].Equals(r["use_fk2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_fk = " + toSqlVal(r["use_fk1"]);
                    }

                    if (!r["unique_col1"].Equals(r["unique_col2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " unique_col = " + toSqlVal(r["unique_col1"]);
                    }

                    if (!r["parent_select_type1"].Equals(r["parent_select_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " parent_select_type = " + toSqlVal(r["parent_select_type1"]);
                    }

                    if (!r["use_lucene1"].Equals(r["use_lucene2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_lucene = " + toSqlVal(r["use_lucene1"]);
                    }

                    if (!r["ic_validation1"].Equals(r["ic_validation2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " ic_validation = " + Conv.ToSql(targetDb) +
                               ".dbo.processColumns_ValidationVarReplace(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["ic_validation1"]) + ")";
                    }

                    if (!r["in_conditions1"].Equals(r["in_conditions2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " in_conditions = " + toSqlVal(r["in_conditions1"]);
                    }

                    if (!r["da2sub_1"].Equals(r["da2sub_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sub_data_additional2 = " + Conv.ToSql(targetDb) +
                               ".dbo.itemColumns_Da2SubVarReplace(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["data_type1"]) + ", " + toSqlVal(r["da2_1"]) + ", " +
                               toSqlVal(r["da2sub_1"]) + ")";
                    }

                    sql += " WHERE item_column_id = " + r["id2"] + " ; ";

                    if (!u) {
                        sql = "";
                    }

                    if (!r["data_type1"].Equals(r["data_type2"])) {
                        sql += "EXEC " + Conv.ToSql(targetDb) + ".dbo.items_ChangeColumnType " + Conv.ToInt(r["id2"]) +
                               ", -1, " + Conv.ToInt(r["data_type1"]) + "; ";
                    }
                }

                if (sql.Length > 0) {
                    sql = sql.Replace("'null'", "null");
                    updates.Add(sql);
                }
            }

            foreach (var list in (List<Dictionary<string, object>>)retval["___listItems"]) {
                if (!inserted_processes.Contains(Conv.ToSql(list["list"]))) {
                    foreach (var r in (List<Dictionary<string, object>>)retval[Conv.ToStr(list["list"])]) {
                        bool first_list_item_guid2 = true;
                        var sql = "";
                        if (r["guid2"] == DBNull.Value) {
                            sql += "DECLARE @new_id INT; INSERT INTO " + Conv.ToSql(targetDb) +
                                   ".dbo.items (instance, process) ";
                            sql += " VALUES (" + toSqlVal(targetInstance) + ", " + toSqlVal(list["list"]) +
                                   "); SET @new_id = @@identity; ";

                            sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo.items SET guid = " + toSqlVal(r["guid1"]) +
                                   " WHERE item_id = @new_id ; ";

                            sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_" +
                                   Conv.ToSql(list["list"]) + " SET ";
                            sql += " order_no = " + toSqlVal(r["order_no1"]);
                            foreach (var col in (List<Dictionary<string, object>>)retval[
                                         Conv.ToStr("___source_cols_" + list["list"])]) {
                                if (HasCol(
                                        (List<Dictionary<string, object>>)retval[
                                            Conv.ToStr("___target_cols_" + list["list"])], col)) {
                                    if (Conv.ToInt(col["data_type"]).In(0, 4) && Conv.ToStr(col["name"]) == "user_group" &&
                                        Conv.ToStr(col["data_additional"]) == "user_group") {
                                        sql += ", " + Conv.ToStr(col["name"]) + " = " + Conv.ToSql(targetDb) +
                                               ".dbo.item_GetIds(" + toSqlVal(r[Conv.ToStr(col["name"] + "1")]) + ")";
                                    }
                                    else if (!Conv.ToInt(col["data_type"]).In(5, 6)) {
                                        sql += ", " + Conv.ToStr(col["name"]) + " = " +
                                               toSqlVal(r[Conv.ToStr(col["name"] + "1")]);
                                    }
                                }
                            }

                            sql += " WHERE item_id = @new_id ; ";

                            foreach (var col in (List<Dictionary<string, object>>)retval[
                                         Conv.ToStr("___source_cols_" + list["list"])]) {
                                if (HasCol(
                                        (List<Dictionary<string, object>>)retval[
                                            Conv.ToStr("___target_cols_" + list["list"])], col)) {
                                    if (Conv.ToInt(col["data_type"]).In(5, 6)) {
                                        if (r[Conv.ToSql(col["name"]) + "1"] != DBNull.Value) {
                                            foreach (string guid1 in Conv.ToStr(r[Conv.ToSql(col["name"]) + "1"])
                                                         .Split(",")) {
                                                if (first_list_item_guid2) {
                                                    sql += "DECLARE @guid2 UNIQUEIDENTIFIER;";
                                                    first_list_item_guid2 = false;
                                                }

                                                sql += " SET @guid2 = NULL ";
                                                sql += " SELECT @guid2 = guid FROM " + Conv.ToSql(targetDb) +
                                                       ".dbo.items WHERE guid = " + toSqlVal(guid1.Trim()) + " ";
                                                sql += " IF @guid2 IS NOT NULL BEGIN ";
                                                sql += "    INSERT INTO " + Conv.ToSql(targetDb) +
                                                       ".dbo.item_data_process_selections (item_id, item_column_id, selected_item_id) ";
                                                sql += "    VALUES (@new_id, ";
                                                sql += "    (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                                                       ".dbo.item_columns WHERE instance = " + toSqlVal(targetInstance) +
                                                       " AND process = '" + Conv.ToSql(list["list"]) + "' AND name = '" +
                                                       Conv.ToSql(col["name"]) + "'),";
                                                sql += "    (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                                                       ".dbo.items WHERE guid = " + toSqlVal(guid1.Trim()) + ") ";
                                                sql += "    ) ; ";
                                                sql += " END ";
                                            }
                                        }
                                    }
                                }
                            }

                            if (!r["parent_guid1"].Equals(DBNull.Value)) {
                                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                                       ".dbo.item_subitems (parent_item_id, item_id, favourite) VALUES ( " +
                                       "  (SELECT TOP 1 i.item_id FROM " + Conv.ToSql(targetDb) +
                                       ".dbo.items i WHERE i.guid = " + toSqlVal(r["parent_guid1"]) + ")" +
                                       ", @new_id, 0) ; ";
                            }
                        }
                        else if (Conv.ToInt(list["compare"]) >= 2) {
                            sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_" +
                                   Conv.ToSql(list["list"]) + " SET ";
                            sql += " order_no = " + toSqlVal(r["order_no1"]);
                            foreach (var col in (List<Dictionary<string, object>>)retval[
                                         Conv.ToStr("___source_cols_" + list["list"])]) {
                                if (HasCol(
                                        (List<Dictionary<string, object>>)retval[
                                            Conv.ToStr("___target_cols_" + list["list"])], col)) {
                                    if (!r[Conv.ToStr(col["name"] + "1")].Equals(r[Conv.ToStr(col["name"] + "2")])) {
                                        if (Conv.ToInt(col["data_type"]).In(0, 4) && Conv.ToStr(col["name"]) == "user_group" &&
                                            Conv.ToStr(col["data_additional"]) == "user_group") {
                                            sql += ", " + Conv.ToStr(col["name"]) + " = " + Conv.ToSql(targetDb) +
                                                   ".dbo.item_GetIds(" + toSqlVal(r[Conv.ToStr(col["name"] + "1")]) + ")";
                                        }
                                        else if (!Conv.ToInt(col["data_type"]).In(5, 6)) {
                                            sql += ", " + Conv.ToStr(col["name"]) + " = " +
                                                   toSqlVal(r[Conv.ToStr(col["name"] + "1")]);
                                        }
                                    }
                                }
                            }

                            sql += " WHERE item_id = (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                                   ".dbo.items WHERE guid = " + toSqlVal(r["guid1"]) + ") ; ";

                            if (r["parent_guid1"] != DBNull.Value && !r["parent_guid1"].Equals(r["parent_guid2"])) {
                                if (r["parent_guid2"] != DBNull.Value) {
                                    sql += "DELETE FROM " + Conv.ToSql(targetDb) + ".dbo.item_subitems " +
                                           " WHERE item_id = (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                                           ".dbo.items WHERE guid = " + toSqlVal(r["guid1"]) + ") " +
                                           " AND parent_item_id = (SELECT i.item_id FROM " + Conv.ToSql(targetDb) +
                                           ".dbo.items i WHERE i.guid = " + toSqlVal(r["parent_guid2"]) + ") ; ";
                                }

                                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                                       ".dbo.item_subitems (parent_item_id, item_id, favourite) VALUES ( " +
                                       "  (SELECT TOP 1 pi.item_id FROM " + Conv.ToSql(targetDb) +
                                       ".dbo.items pi WHERE pi.guid = " + toSqlVal(r["parent_guid1"]) + ")" +
                                       ", (SELECT TOP 1 i.item_id FROM " + Conv.ToSql(targetDb) +
                                       ".dbo.items i WHERE i.guid = " + toSqlVal(r["guid1"]) + ") " +
                                       ", 0) ; ";
                            }

                            foreach (var col in (List<Dictionary<string, object>>)retval[
                                         Conv.ToStr("___source_cols_" + list["list"])]) {
                                if (HasCol(
                                        (List<Dictionary<string, object>>)retval[
                                            Conv.ToStr("___target_cols_" + list["list"])], col)) {
                                    if (Conv.ToInt(col["data_type"]).In(5, 6)) {
                                        if (!r[Conv.ToSql(col["name"]) + "1"].Equals(r[Conv.ToSql(col["name"]) + "2"])) {
                                            if (r[Conv.ToSql(col["name"]) + "2"] != DBNull.Value) {
                                                foreach (string guid2 in Conv.ToStr(r[Conv.ToSql(col["name"]) + "2"])
                                                             .Split(",")) {
                                                    if (!DBNull.Value.Equals(guid2) && guid2 != "") {
                                                        sql += "DELETE FROM " + Conv.ToSql(targetDb) +
                                                               ".dbo.item_data_process_selections " +
                                                               " WHERE item_id = (SELECT i.item_id FROM " +
                                                               Conv.ToSql(targetDb) + ".dbo.items i WHERE i.guid = " +
                                                               toSqlVal(r["guid1"]) + ") " +
                                                               " AND item_column_id = (SELECT item_column_id FROM " +
                                                               Conv.ToSql(targetDb) +
                                                               ".dbo.item_columns WHERE instance = " +
                                                               toSqlVal(targetInstance) + " AND process = '" +
                                                               Conv.ToSql(list["list"]) + "' AND name = '" +
                                                               Conv.ToSql(col["name"]) + "') " +
                                                               " AND selected_item_id = (SELECT ci.item_id FROM " +
                                                               Conv.ToSql(targetDb) + ".dbo.items ci WHERE ci.guid = " +
                                                               toSqlVal(guid2.Trim()) + ") ; ";
                                                    }
                                                }
                                            }

                                            if (r[Conv.ToSql(col["name"]) + "1"] != DBNull.Value) {
                                                foreach (string guid1 in Conv.ToStr(r[Conv.ToSql(col["name"]) + "1"])
                                                             .Split(",")) {
                                                    if (first_list_item_guid2) {
                                                        sql += "DECLARE @guid2 UNIQUEIDENTIFIER;";
                                                        first_list_item_guid2 = false;
                                                    }

                                                    sql += " SET @guid2 = NULL ";
                                                    sql += " SELECT @guid2 = guid FROM " + Conv.ToSql(targetDb) +
                                                           ".dbo.items WHERE guid = " + toSqlVal(guid1.Trim()) + " ";
                                                    sql += " IF @guid2 IS NOT NULL BEGIN ";
                                                    sql += "    INSERT INTO " + Conv.ToSql(targetDb) +
                                                           ".dbo.item_data_process_selections (item_id, item_column_id, selected_item_id) ";
                                                    sql += "    VALUES (";
                                                    sql += "	(SELECT i.item_id FROM " + Conv.ToSql(targetDb) +
                                                           ".dbo.items i WHERE i.guid = " + toSqlVal(r["guid1"]) + "), ";
                                                    sql += "    (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                                                           ".dbo.item_columns WHERE instance = " +
                                                           toSqlVal(targetInstance) + " AND process = '" +
                                                           Conv.ToSql(list["list"]) + "' AND name = '" +
                                                           Conv.ToSql(col["name"]) + "'),";
                                                    sql += "    (SELECT ci.item_id FROM " + Conv.ToSql(targetDb) +
                                                           ".dbo.items ci WHERE ci.guid = " + toSqlVal(guid1.Trim()) + ") ";
                                                    sql += "    ) ; ";
                                                    sql += " END ";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        sql = sql.Replace("'null'", "null");
                        updates.Add(sql);
                    }
                }
            }

            foreach (var process in (List<Dictionary<string, object>>)retval["___matrixes"]) {
                foreach (var r in (List<Dictionary<string, object>>)retval[
                             "___matrix_" + Conv.ToStr(process["process"])]) {
                    var sql = "";
                    if (r["guid2"] == DBNull.Value) {
                        sql += " DECLARE @new_id INT;";
                        sql += " INSERT INTO " + Conv.ToSql(targetDb) + ".dbo.items (instance, process) ";
                        sql += " VALUES (" + toSqlVal(targetInstance) + ", " + toSqlVal(process["process"]) + ");";
                        sql += " SET @new_id = @@identity; ";
                        sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo.items SET guid = " + toSqlVal(r["guid1"]) +
                               " WHERE item_id = @new_id ; ";
                        sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_" +
                               Conv.ToSql(process["process"]);
                        sql += " SET order_no = " + toSqlVal(r["order_no1"]);
                        sql += " , type = " + toSqlVal(r["type1"]);
                        sql += " , column_item_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" +
                               toSqlVal(r["column_item_id_guid1"]) + ")";
                        sql += " , question_item_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" +
                               toSqlVal(r["question_item_id_guid1"]) + ")";
                        sql += " , weighting = " + toSqlVal(r["weighting1"]);
                        sql += " , name = " + toSqlVal(r["name1"]);
                        sql += " , score = " + toSqlVal(r["score1"]);
                        sql += " , tooltip = " + toSqlVal(r["tooltip1"]);
                        sql += " WHERE item_id = @new_id ; ";
                    } else if (Conv.ToInt(process["compare"]) >= 2) {
                        sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) + "_" +
                               Conv.ToSql(process["process"]);
                        sql += " SET order_no = " + toSqlVal(r["order_no1"]);
                        sql += " , type = " + toSqlVal(r["type1"]);
                        sql += " , column_item_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" +
                               toSqlVal(r["column_item_id_guid1"]) + ")";
                        sql += " , question_item_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" +
                               toSqlVal(r["question_item_id_guid1"]) + ")";
                        sql += " , weighting = " + toSqlVal(r["weighting1"]);
                        sql += " , name = " + toSqlVal(r["name1"]);
                        sql += " , score = " + toSqlVal(r["score1"]);
                        sql += " , tooltip = " + toSqlVal(r["tooltip1"]);
                        sql += " WHERE item_id = (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.items WHERE guid = " + toSqlVal(r["guid1"]) + ") ; ";
                    }

                    sql = sql.Replace("'null'", "null");
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["userGroup"]) {
                var sql = "";
                if (r["guid2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.items (instance, process) ";
                    sql += " VALUES ( " + toSqlVal(targetInstance) + ", 'user_group'); SET @new_id = @@identity; ";

                    sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo.items SET guid = " + toSqlVal(r["guid1"]) +
                           " WHERE item_id = @new_id ; ";

                    sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                           "_user_group SET ";
                    sql += " usergroup_name = " + toSqlVal(r["usergroup_name1"]) + ", ";
                    sql += " order_no = " + toSqlVal(r["order_no1"]) + ", ";
                    sql += " all_group = " + toSqlVal(r["all_group1"]) + " ";
                    sql += " WHERE item_id = @new_id ; ";

                    if (r["group_type_guid1"] != DBNull.Value) {
                        sql += " DECLARE @group_type_guid2 UNIQUEIDENTIFIER; ";
                        sql += " SELECT @group_type_guid2 = guid FROM " + Conv.ToSql(targetDb) +
                               ".dbo.items WHERE guid = " + toSqlVal(r["group_type_guid1"]) + " ";
                        sql += " IF @group_type_guid2 IS NOT NULL BEGIN ";
                        sql += "    INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.item_data_process_selections (item_id, item_column_id, selected_item_id) ";
                        sql += "    VALUES (@new_id, ";
                        sql += "    (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.item_columns WHERE instance = " + toSqlVal(targetInstance) +
                               " AND process = 'user_group' AND name = 'group_type'),";
                        sql += "    (SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                               toSqlVal(r["group_type_guid1"]) + ") ";
                        sql += "    ) ; ";
                        sql += " END ";
                    }

                    if (r["azure_ad_groups_guid1"] != DBNull.Value) {
                        sql += " DECLARE @azure_ad_groups_guid2 UNIQUEIDENTIFIER; ";
                        sql += " DECLARE @cur CURSOR";
                        sql += " SET @cur = CURSOR FAST_FORWARD LOCAL FOR";
                        sql += " SELECT guid FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid IN (" +
                               Conv.ToStr(r["azure_ad_groups_guid1"]) + ") ";
                        sql += " OPEN @cur";
                        sql += " FETCH NEXT FROM @cur INTO @azure_ad_groups_guid2";
                        sql += " WHILE (@@FETCH_STATUS = 0) BEGIN";
                        sql += "    IF @azure_ad_groups_guid2 IS NOT NULL BEGIN ";
                        sql += "       INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.item_data_process_selections (item_id, item_column_id, selected_item_id) ";
                        sql += "       VALUES (@new_id, ";
                        sql += "       (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.item_columns WHERE instance = " + toSqlVal(targetInstance) +
                               " AND process = 'user_group' AND name = 'azure_ad_groups'),";
                        sql += "       (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.items WHERE guid = @azure_ad_groups_guid2) ";
                        sql += "       ) ; ";
                        sql += "    END ";
                        sql += " FETCH NEXT FROM @cur INTO @azure_ad_groups_guid2";
                        sql += " END ";
                        sql += " CLOSE @cur";
                    }

                    if (r["licence_categories_guid1"] != DBNull.Value) {
                        sql += " DECLARE @licence_categories_guid2 UNIQUEIDENTIFIER; ";
                        sql += " DECLARE @cur2 CURSOR";
                        sql += " SET @cur2 = CURSOR FAST_FORWARD LOCAL FOR";
                        sql += " SELECT guid FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid IN (" +
                               Conv.ToStr(r["licence_categories_guid1"]) + ") ";
                        sql += " OPEN @cur2";
                        sql += " FETCH NEXT FROM @cur2 INTO @licence_categories_guid2";
                        sql += " WHILE (@@FETCH_STATUS = 0) BEGIN";
                        sql += "    IF @licence_categories_guid2 IS NOT NULL BEGIN ";
                        sql += "       INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.item_data_process_selections (item_id, item_column_id, selected_item_id) ";
                        sql += "       VALUES (@new_id, ";
                        sql += "       (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.item_columns WHERE instance = " + toSqlVal(targetInstance) +
                               " AND process = 'user_group' AND name = 'licence_categories'),";
                        sql += "       (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.items WHERE guid = @licence_categories_guid2) ";
                        sql += "       ) ; ";
                        sql += "    END ";
                        sql += " FETCH NEXT FROM @cur2 INTO @licence_categories_guid2";
                        sql += " END ";
                        sql += " CLOSE @cur2";
                    }

                    if (r["compare_guid1"] != DBNull.Value) {
                        sql += " DECLARE @compare_guid2 UNIQUEIDENTIFIER; ";
                        sql += " SELECT @compare_guid2 = guid FROM " + Conv.ToSql(targetDb) +
                               ".dbo.items WHERE guid = " + toSqlVal(r["compare_guid1"]) + " ";
                        sql += " IF @compare_guid2 IS NOT NULL BEGIN ";
                        sql += "    INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.item_data_process_selections (item_id, item_column_id, selected_item_id) ";
                        sql += "    VALUES (@new_id, ";
                        sql += "    (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.item_columns WHERE instance = " + toSqlVal(targetInstance) +
                               " AND process = 'user_group' AND name = 'compare'),";
                        sql += "    (SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                               toSqlVal(r["compare_guid1"]) + ") ";
                        sql += "    ) ; ";
                        sql += " END ";
                    }
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["containers"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_containers (instance, process, variable, rows, cols, maintenance_name, right_container, hide_title) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += toSqlVal(r["rows1"]) + ", ";
                    sql += toSqlVal(r["cols1"]) + ", ";
                    sql += toSqlVal(r["maintenance_name1"]) + ", ";
                    sql += toSqlVal(r["right_container1"]) + ", ";
                    sql += toSqlVal(r["hide_title1"]) + "); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_containers SET ";

                    if (!r["rows1"].Equals(r["rows2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " rows = " + toSqlVal(r["rows1"]);
                    }

                    if (!r["cols1"].Equals(r["cols2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " cols = " + toSqlVal(r["cols1"]);
                    }

                    if (!r["maintenance_name1"].Equals(r["maintenance_name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " maintenance_name = " + toSqlVal(r["maintenance_name1"]);
                    }

                    if (!r["right_container1"].Equals(r["right_container2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " right_container = " + toSqlVal(r["right_container1"]);
                    }

                    if (!r["hide_title1"].Equals(r["hide_title2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " hide_title = " + toSqlVal(r["hide_title1"]);
                    }

                    sql += " WHERE process_container_id = " + r["id2"] + " ; ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groups"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_groups (instance, process, variable, short_variable, order_no, type, maintenance_name) ";
                    sql += " VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += toSqlVal(r["short_variable1"]) + ", ";
                    sql += toSqlVal(r["order_no1"]) + ", ";
                    sql += toSqlVal(r["type1"]) + ", ";
                    sql += toSqlVal(r["maintenance_name1"]) + " ); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_groups SET ";

                    if (!r["short_variable1"].Equals(r["short_variable2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " short_variable = " + toSqlVal(r["short_variable1"]);
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["type1"].Equals(r["type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " type = " + toSqlVal(r["type1"]);
                    }

                    if (!r["maintenance_name1"].Equals(r["maintenance_name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " maintenance_name = " + toSqlVal(r["maintenance_name1"]);
                    }

                    sql += " WHERE process_group_id = " + r["id2"] + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actionsDialog"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_actions_dialog (instance, process, variable, type, name, dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable, process_container_id) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + " , ";
                    sql += toSqlVal(r["type1"]) + " , ";
                    sql += toSqlVal(r["name1"]) + " , ";
                    sql += toSqlVal(r["dialog_title_variable1"]) + " , ";
                    sql += toSqlVal(r["dialog_text_variable1"]) + " , ";
                    sql += toSqlVal(r["cancel_button_variable1"]) + " , ";
                    sql += toSqlVal(r["submit_button_variable1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_container_id1"]) + ") ); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_actions_dialog SET ";

                    if (!r["type1"].Equals(r["type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " type = " + toSqlVal(r["type1"]);
                    }

                    if (!r["name1"].Equals(r["name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " name = " + toSqlVal(r["name1"]);
                    }

                    if (!r["dialog_title_variable1"].Equals(r["dialog_title_variable2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " dialog_title_variable = " + toSqlVal(r["dialog_title_variable1"]);
                    }

                    if (!r["dialog_text_variable1"].Equals(r["dialog_text_variable2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " dialog_text_variable = " + toSqlVal(r["dialog_text_variable1"]);
                    }

                    if (!r["cancel_button_variable1"].Equals(r["cancel_button_variable2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " cancel_button_variable = " + toSqlVal(r["cancel_button_variable1"]);
                    }

                    if (!r["submit_button_variable1"].Equals(r["submit_button_variable2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " submit_button_variable = " + toSqlVal(r["submit_button_variable1"]);
                    }

                    if (!r["process_container_id1"].Equals(r["process_container_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_container_id = " + Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_container_id1"]) + ")";
                    }

                    sql += " WHERE process_action_dialog_id = " + r["id2"] + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["containerColumns"]) {
                var sql = "";
                var u = false;
                if (r["process_container_column_id2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_container_columns (process_container_id, item_column_id, placeholder, validation, order_no";
                    if (!skipHelps) {
                        sql += ", help_text";
                    }

                    sql += ") VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_container_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + "), ";
                    sql += toSqlVal(r["placeholder1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["validation1"]) + "), ";
                    sql += toSqlVal(r["order_no1"]) + " ";
                    if (!skipHelps) {
                        sql += ", " + toSqlVal(r["help_text1"]) + " ";
                    }

                    sql += "); ";
                    sql += "SET @new_id = @@identity; ";
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_container_columns SET guid = " +
                           toSqlVal(r["guid1"]) + " WHERE process_container_column_id = @new_id; ";
                    u = true;
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_container_columns SET ";

                    if (!r["process_container_id1"].Equals(r["process_container_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_container_id = " + Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_container_id1"]) + ")";
                    }

                    if (!r["item_column_id1"].Equals(r["item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + ")";
                    }

                    if (!r["placeholder1"].Equals(r["placeholder2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " placeholder = " + toSqlVal(r["placeholder1"]);
                    }

                    if (!r["validation1"].Equals(r["validation2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " validation = " + Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationVarReplace(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["validation1"]) + ")";
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!skipHelps) {
                        if (!r["help_text1"].Equals(r["help_text2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " help_text = " + toSqlVal(r["help_text1"]);
                        }
                    }

                    sql += " WHERE process_container_column_id = " + toSqlVal(r["process_container_column_id2"]) + " ;";
                }

                if (u) {
                    sql = sql.Replace("'null'", "null");
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["portfolioColumnGroups"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_portfolio_columns_groups (instance, process, variable, process_portfolio_id, order_no, show_type) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_portfolio_id1"]) + "), ";
                    sql += toSqlVal(r["order_no1"]) + ", ";
                    sql += toSqlVal(r["show_type1"]) + " ";
                    sql += ") ; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_columns_groups SET ";

                    if (!r["process_portfolio_id1"].Equals(r["process_portfolio_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_portfolio_id1"]) + ")";
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["show_type1"].Equals(r["show_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " show_type = " + toSqlVal(r["show_type1"]);
                    }

                    sql += " WHERE portfolio_column_group_id = " + toSqlVal(r["id2"]) + ";";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["portfolioColumns"]) {
                var sql = "";
                var u = false;
                if (r["process_portfolio_id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_portfolio_columns (process_portfolio_id, item_column_id, order_no, use_in_select_result, show_type, use_in_filter" +
                           ", archive_item_column_id, width, sum_column, priority, is_default, use_in_search, report_column, short_name, validation, portfolio_column_group_id) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_portfolio_id"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id"]) + "), ";
                    sql += toSqlVal(r["order_no1"]) + ", ";
                    sql += toSqlVal(r["use_in_select_result1"]) + ", ";
                    sql += toSqlVal(r["show_type1"]) + ", ";
                    sql += toSqlVal(r["use_in_filter1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["archive_item_column_id1"]) + "), ";
                    sql += toSqlVal(r["width1"]) + ", ";
                    sql += toSqlVal(r["sum_column1"]) + ", ";
                    sql += toSqlVal(r["priority1"]) + ", ";
                    sql += toSqlVal(r["is_default1"]) + ", ";
                    sql += toSqlVal(r["use_in_search1"]) + ", ";
                    sql += toSqlVal(r["report_column1"]) + ", ";
                    sql += toSqlVal(r["short_name1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["validation1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolioColumnGroup_GetId(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["portfolio_column_group_id1"]) + ") ";
                    sql += ") ; ";

                    sql += "DECLARE @pid INT = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["process_portfolio_id"]) + ") ; ";
                    sql += "DECLARE @cid INT = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id"]) + ") ; ";
                    sql += "EXEC " + Conv.ToSql(targetDb) + ".dbo.ExcludeNewColumnInSavedFilters @pid, @cid ; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_columns SET ";

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["use_in_select_result1"].Equals(r["use_in_select_result2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_in_select_result = " + toSqlVal(r["use_in_select_result1"]);
                    }

                    if (!r["show_type1"].Equals(r["show_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " show_type = " + toSqlVal(r["show_type1"]);
                    }

                    if (!r["use_in_filter1"].Equals(r["use_in_filter2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_in_filter = " + toSqlVal(r["use_in_filter1"]);
                    }

                    if (!r["archive_item_column_id1"].Equals(r["archive_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " archive_item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["archive_item_column_id1"]) + ")";
                    }

                    if (!r["width1"].Equals(r["width2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " width = " + toSqlVal(r["width1"]);
                    }

                    if (!r["sum_column1"].Equals(r["sum_column2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sum_column = " + toSqlVal(r["sum_column1"]);
                    }

                    if (!r["priority1"].Equals(r["priority2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " priority = " + toSqlVal(r["priority1"]);
                    }

                    if (!r["is_default1"].Equals(r["is_default2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " is_default = " + toSqlVal(r["is_default1"]);
                    }

                    if (!r["use_in_search1"].Equals(r["use_in_search2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " use_in_search = " + toSqlVal(r["use_in_search1"]);
                    }

                    if (!r["report_column1"].Equals(r["report_column2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " report_column = " + toSqlVal(r["report_column1"]);
                    }

                    if (!r["short_name1"].Equals(r["short_name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " short_name = " + toSqlVal(r["short_name1"]);
                    }

                    if (!r["validation1"].Equals(r["validation2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " validation = " + Conv.ToSql(targetDb) + ".dbo.processColumns_ValidationVarReplace(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["validation1"]) + ")";
                    }

                    if (!r["portfolio_column_group_id1"].Equals(r["portfolio_column_group_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " portfolio_column_group_id = " + Conv.ToSql(targetDb) +
                               ".dbo.processPortfolioColumnGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["portfolio_column_group_id1"]) + ")";
                    }

                    sql += " WHERE process_portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["process_portfolio_id"]) +
                           ") AND item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                           toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["item_column_id"]) + ") ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioDialogs"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    if (r["process_container_id1"] != DBNull.Value) {
                        sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.process_portfolio_dialogs (instance, process, process_container_id, order_no, variable) VALUES ( ";
                        sql += toSqlVal(targetInstance) + ", ";
                        sql += toSqlVal(r["process1"]) + ", ";
                        sql += Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["process_container_id1"]) + "), ";
                        sql += toSqlVal(r["order_no1"]) + ", ";
                        sql += toSqlVal(r["variable1"]) + " ";
                        sql += ") ; ";
                    }
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_dialogs SET ";

                    if (!r["process_container_id1"].Equals(r["process_container_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_container_id = " + Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_container_id1"]) + ")";
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    sql += " WHERE process_portfolio_dialog_id = " + toSqlVal(r["id2"]) + ";";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioActions"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_portfolio_actions (process_portfolio_id, process_action_id, button_text_variable) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_portfolio_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_action_id1"]) + "), ";
                    sql += toSqlVal(r["button_text_variable1"]) + " ";
                    sql += ") ; ";
                    sql += "SET @new_id = @@identity; ";
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_actions SET guid = " +
                           toSqlVal(r["guid1"]) + " WHERE process_portfolio_action_id = @new_id; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_portfolio_actions SET ";

                    if (!r["process_portfolio_id1"].Equals(r["process_portfolio_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_portfolio_id1"]) + ")";
                    }

                    if (!r["process_action_id1"].Equals(r["process_action_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_action_id = " + Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_action_id1"]) + ")";
                    }

                    if (!r["button_text_variable1"].Equals(r["button_text_variable2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " button_text_variable = " + toSqlVal(r["button_text_variable1"]);
                    }

                    sql += " WHERE process_portfolio_action_id = " + toSqlVal(r["id2"]) + ";";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioGroups"]) {
                if (r["process_portfolio_id2"] == DBNull.Value) {
                    var sql = "";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_portfolio_groups (process_portfolio_id, process_group_id) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_portfolio_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_group_id1"]) + ") ";
                    sql += "); ";
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioUserGroups"]) {
                if (r["process_portfolio_id2"] == DBNull.Value) {
                    var sql = "";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_portfolio_user_groups (process_portfolio_id, item_id) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_portfolio_id1"]) + "), ";
                    sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                           toSqlVal(r["item_id1"]) + ") ";
                    sql += "); ";
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processPortfolioTimemachineSaved"]) {
                if (r["save_id2"] == DBNull.Value) {
                    var sql = "";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_portfolio_timemachine_saved (instance, process, name, date) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += toSqlVal(r["name1"]) + ", ";
                    sql += toSqlVal(r["date1"]) + "); ";
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["tabContainers"]) {
                var sql = "";
                var u = false;
                if (r["process_tab_id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_tab_id"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_container_id"]) + "), ";
                    sql += toSqlVal(r["container_side1"]) + ", ";
                    sql += toSqlVal(r["order_no1"]) + "); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_tab_containers SET ";

                    if (!r["container_side1"].Equals(r["container_side2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " container_side = " + toSqlVal(r["container_side1"]);
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    sql += " WHERE process_tab_id = " + Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" +
                           toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["process_tab_id"]) + ") AND process_container_id = " +
                           Conv.ToSql(targetDb) +
                           ".dbo.processContainer_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_container_id"]) + ") ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actionRowsGroups"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_action_rows_groups (type, instance, process, variable, process_in_group, in_use, order_no) VALUES ( ";
                    sql += toSqlVal(r["type1"]) + ", ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["process_in_group1"]) + ", ";
                    sql += toSqlVal(r["in_use1"]) + ", ";
                    sql += toSqlVal(r["order_no1"]) + " ";
                    sql += ") ; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_action_rows_groups SET ";

                    if (!r["type1"].Equals(r["type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " type = " + toSqlVal(r["type1"]);
                    }

                    if (!r["process_in_group1"].Equals(r["process_in_group2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_in_group = " + toSqlVal(r["process_in_group1"]);
                    }

                    if (!r["in_use1"].Equals(r["in_use2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " in_use = " + toSqlVal(r["in_use1"]);
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    sql += " WHERE process_action_group_id = " + toSqlVal(r["id2"]) + " ;";
                }

                //sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["actionRows"]) {
                var sql = "";
                var u = false;
                if (r["process_action_row_id2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_action_rows (process_action_id, process_action_type, item_column_id, value_type ";
                    sql +=
                        ", value, value2, value3, user_item_column_id, link_item_column_id, condition_id, split_options, action_group_id, order_no ";
                    sql += ", notification_type, exclude_sender) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_action_id1"]) + "), ";
                    sql += toSqlVal(r["process_action_type1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + "), ";
                    sql += toSqlVal(r["value_type1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" + toSqlVal(targetInstance) +
                           ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + "), " + toSqlVal(r["process_action_type1"]) + ", " +
                           toSqlVal(r["value_type1"]) + ", 1, " + toSqlVal(r["value1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" + toSqlVal(targetInstance) +
                           ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + "), " + toSqlVal(r["process_action_type1"]) + ", " +
                           toSqlVal(r["value_type1"]) + ", 2, " + toSqlVal(r["value2"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" + toSqlVal(targetInstance) +
                           ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + "), " + toSqlVal(r["process_action_type1"]) + ", " +
                           toSqlVal(r["value_type1"]) + ", 3, " + toSqlVal(r["value3"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["user_item_column_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["link_item_column_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["condition_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" + toSqlVal(targetInstance) +
                           ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + "), " + toSqlVal(r["process_action_type1"]) + ", " +
                           toSqlVal(r["value_type1"]) + ", 0, " + toSqlVal(r["split_options1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processActionRowsGroups_GetId(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["action_group_id1"]) + "), ";
                    sql += toSqlVal(r["order_no1"]) + ", ";
                    sql += toSqlVal(r["notification_type1"]) + ", ";
                    sql += toSqlVal(r["exclude_sender1"]) + " ";
                    sql += "); ";
                    sql += "SET @new_id = @@identity; ";
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_action_rows SET guid = " +
                           toSqlVal(r["guid1"]) + " WHERE process_action_row_id = @new_id; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_action_rows SET ";

                    if (!r["process_action_id1"].Equals(r["process_action_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_action_id = " + Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_action_id1"]) + ")";
                    }

                    if (!r["process_action_type1"].Equals(r["process_action_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_action_type = " + toSqlVal(r["process_action_type1"]);
                    }

                    if (!r["item_column_id1"].Equals(r["item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + ")";
                    }

                    if (!r["value_type1"].Equals(r["value_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " value_type = " + toSqlVal(r["value_type1"]);
                    }

                    if (!r["value1"].Equals(r["value1_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " value = " + Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" +
                               toSqlVal(targetInstance) + ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + "), " +
                               toSqlVal(r["process_action_type1"]) + ", " + toSqlVal(r["value_type1"]) + ", 1, " +
                               toSqlVal(r["value1"]) + ")";
                    }

                    if (!r["value2"].Equals(r["value2_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " value2 = " + Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" +
                               toSqlVal(targetInstance) + ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + "), " +
                               toSqlVal(r["process_action_type1"]) + ", " + toSqlVal(r["value_type1"]) + ", 2, " +
                               toSqlVal(r["value2"]) + ")";
                    }

                    if (!r["value3"].Equals(r["value3_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " value3 = " + Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" +
                               toSqlVal(targetInstance) + ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + "), " +
                               toSqlVal(r["process_action_type1"]) + ", " + toSqlVal(r["value_type1"]) + ", 3, " +
                               toSqlVal(r["value3"]) + ")";
                    }

                    if (!r["user_item_column_id1"].Equals(r["user_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " user_item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["user_item_column_id1"]) + ")";
                    }

                    if (!r["link_item_column_id1"].Equals(r["link_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " link_item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["link_item_column_id1"]) + ")";
                    }

                    if (!r["condition_id1"].Equals(r["condition_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " condition_id = " + Conv.ToSql(targetDb) + ".dbo.condition_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id1"]) + ")";
                    }

                    if (!r["split_options1"].Equals(r["split_options2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " split_options = " + Conv.ToSql(targetDb) + ".dbo.processActionRows_ValueVarReplace(" +
                               toSqlVal(targetInstance) + ", " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + "), " +
                               toSqlVal(r["process_action_type1"]) + ", " + toSqlVal(r["value_type1"]) + ", 0, " +
                               toSqlVal(r["split_options1"]) + ")";
                    }

                    if (!r["action_group_id1"].Equals(r["action_group_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " action_group_id = " + Conv.ToSql(targetDb) + ".dbo.processActionRowsGroups_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["action_group_id1"]) + ")";
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["notification_type1"].Equals(r["notification_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " notification_type = " + toSqlVal(r["notification_type1"]);
                    }

                    if (!r["exclude_sender1"].Equals(r["exclude_sender2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " exclude_sender = " + toSqlVal(r["exclude_sender1"]);
                    }

                    sql += " WHERE guid = " + toSqlVal(r["guid1"]) + " ;";
                }

                //sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["itemColumnJoins"]) {
                var sql = "";
                if (r["item_column_id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) + ".dbo.item_column_joins ";
                    sql +=
                        "(item_column_id, parent_column_type, parent_item_column_id, parent_additional_join, sub_column_type, sub_item_column_id, sub_additional_join) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id"]) + "), ";
                    sql += toSqlVal(r["parent_column_type"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["parent_item_column_id"]) + "), ";
                    sql += toSqlVal(r["parent_additional_join"]) + ", ";
                    sql += toSqlVal(r["sub_column_type"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["sub_item_column_id"]) + "), ";
                    sql += toSqlVal(r["sub_additional_join"]) + "); ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["itemColumnEquations"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.item_column_equations (parent_item_column_id, column_type, item_column_id, sql_alias ";
                    sql += ", sql_function, sql_param1, sql_param2, sql_param3) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["parent_item_column_id1"]) + "), ";
                    sql += toSqlVal(r["column_type1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + "), ";
                    sql += toSqlVal(r["sql_alias1"]) + ", ";
                    sql += toSqlVal(r["sql_function1"]) + ",  ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumnEquations_ParamVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["column_type1"]) + ", " +
                           toSqlVal(r["sql_function1"]) + ", 1, " + toSqlVal(r["sql_param1_1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumnEquations_ParamVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["column_type1"]) + ", " +
                           toSqlVal(r["sql_function1"]) + ", 2, " + toSqlVal(r["sql_param2_1"]) + "), ";
                    sql += toSqlVal(r["sql_param3_1"]) + " ";
                    sql += "); ";
                    sql += "SET @new_id = @@identity; ";
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.item_column_equations SET guid = " +
                           toSqlVal(r["guid1"]) + " WHERE item_column_equation_id = @new_id; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.item_column_equations SET ";

                    if (!r["parent_item_column_id1"].Equals(r["parent_item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " parent_item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["parent_item_column_id1"]) + ")";
                    }

                    if (!r["column_type1"].Equals(r["column_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " column_type = " + toSqlVal(r["column_type1"]);
                    }

                    if (!r["item_column_id1"].Equals(r["item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + ")";
                    }

                    if (!r["sql_alias1"].Equals(r["sql_alias2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sql_alias = " + toSqlVal(r["sql_alias1"]);
                    }

                    if (!r["sql_function1"].Equals(r["sql_function2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sql_function = " + toSqlVal(r["sql_function1"]);
                    }

                    if (!r["sql_param1_1"].Equals(r["sql_param1_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sql_param1 = " + Conv.ToSql(targetDb) + ".dbo.itemColumnEquations_ParamVarReplace(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["column_type1"]) + ", " +
                               toSqlVal(r["sql_function1"]) + ", 1, " + toSqlVal(r["sql_param1_1"]) + ")";
                    }

                    if (!r["sql_param2_1"].Equals(r["sql_param2_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sql_param2 = " + Conv.ToSql(targetDb) + ".dbo.itemColumnEquations_ParamVarReplace(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["column_type1"]) + ", " +
                               toSqlVal(r["sql_function1"]) + ", 2, " + toSqlVal(r["sql_param2_1"]) + ")";
                    }

                    if (!r["sql_param3_1"].Equals(r["sql_param3_2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " sql_param3 = " + toSqlVal(r["sql_param3_1"]);
                    }

                    sql += " WHERE guid = " + toSqlVal(r["guid1"]) + " ;";
                }

                //sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groupFeatures"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.process_group_features (process_group_id, feature, state, custom_name) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.processGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_group_id1"]) + "), ";
                sql += toSqlVal(r["feature1"]) + ", ";
                sql += toSqlVal(r["state1"]) + ", ";
                sql += toSqlVal(r["custom_name1"]) + " ); ";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groupTabs"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.process_group_tabs (process_group_id, process_tab_id, order_no, tag) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.processGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_group_id1"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_tab_id1"]) + "), " + toSqlVal(r["order_no1"]) + ", " + toSqlVal(r["tag1"]) +
                       " ) ; ";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["groupNavigation"]) {
                var sql = "";
                var u = false;
                if (r["variable2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) + ".dbo.process_group_navigation ";
                    sql +=
                        " (process_group_id, variable, process_tab_id, default_state, order_no, type_id, icon, template_portfolio_id) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_group_id1"]) + "), ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_tab_id1"]) + "), ";
                    sql += toSqlVal(r["default_state1"]) + ", " + toSqlVal(r["order_no1"]) + ", " +
                           toSqlVal(r["type_id1"]) + ", " + toSqlVal(r["icon1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["template_portfolio_id1"]) + ")); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_group_navigation SET ";

                    if (!r["icon1"].Equals(r["icon2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " icon = " + toSqlVal(r["icon1"]);
                    }

                    if (!r["template_portfolio_id1"].Equals(r["template_portfolio_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " template_portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["template_portfolio_id1"]) + ")";
                    }

                    sql += " WHERE process_navigation_id = " + toSqlVal(r["process_navigation_id"]) + " ; ";
                }

                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["tabColumns"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.process_tab_columns (process_tab_id, item_column_id, order_no) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_tab_id1"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["item_column_id1"]) + "), ";
                sql += toSqlVal(r["order_no1"]) + "); ";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processTabsSubprocesses"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.process_tabs_subprocesses (instance, process, process_tab_id, order_no, process_side) VALUES ( ";
                sql += toSqlVal(targetInstance) + ", ";
                sql += toSqlVal(r["process1"]) + ", ";
                sql += Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_tab_id1"]) + "), ";
                sql += toSqlVal(r["order_no1"]) + ", ";
                sql += toSqlVal(r["process_side1"]) + "); ";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processTabsSubprocessData"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.process_tabs_subprocess_data (process_tab_subprocess_id, parent_process_item_id, item_id) VALUES ( ";
                sql += "(SELECT process_tab_subprocess_id FROM " + Conv.ToSql(targetDb) +
                       ".dbo.process_tabs_subprocesses WHERE guid = " + toSqlVal(r["process_tab_subprocess_id1"]) +
                       ", ";
                sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE instance = " +
                       toSqlVal(targetInstance) + " AND guid = " + toSqlVal(r["parent_process_item_id1"]) + ", ";
                sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE instance = " +
                       toSqlVal(targetInstance) + " AND guid = " + toSqlVal(r["item_id1"]) + " ); ";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboards"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_dashboards (instance, process, variable, name, process_portfolio_id) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["name1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + "," +
                           toSqlVal(r["process_portfolio_id1"]) + ") ";
                    sql += "); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_dashboards SET ";

                    if (!r["name1"].Equals(r["name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " name = " + toSqlVal(r["name1"]);
                    }

                    if (!r["process_portfolio_id1"].Equals(r["process_portfolio_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_portfolio_id1"]) + ")";
                    }

                    sql += " WHERE process_dashboard_id = " + r["id2"] + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardCharts"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_dashboard_charts (instance, process, variable, name, process_portfolio_id, json_data) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["name1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + "," +
                           toSqlVal(r["process_portfolio_id1"]) + "), ";
                    sql += toSqlVal(r["json_data1"]) + " ";
                    sql += "); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_dashboard_charts SET ";

                    if (!r["name1"].Equals(r["name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " name = " + toSqlVal(r["name1"]);
                    }

                    if (!r["process_portfolio_id1"].Equals(r["process_portfolio_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_portfolio_id1"]) + ")";
                    }

                    if (!r["json_data1"].Equals(r["json_data2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " json_data = " + toSqlVal(r["json_data1"]);
                    }

                    sql += " WHERE process_dashboard_chart_id = " + r["id2"] + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardChartMap"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_dashboard_chart_map (process_dashboard_chart_id, process_dashboard_id, order_no) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processDashboardChart_GetId(" + toSqlVal(targetInstance) + "," +
                           toSqlVal(r["process_dashboard_chart1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processDashboard_GetId(" + toSqlVal(targetInstance) + "," +
                           toSqlVal(r["process_dashboard1"]) + "), ";
                    sql += toSqlVal(r["order_no1"]) + " ";
                    sql += "); ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardChartLinks"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_dashboard_chart_links (instance, process, dashboard_id, source_field, link_process, link_process_field, chart_ids, hide) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processDashboard_GetId(" + toSqlVal(targetInstance) + "," +
                           toSqlVal(r["dashboard1"]) + "), ";
                    sql += toSqlVal(r["source_field1"]) + ", ";
                    sql += toSqlVal(r["link_process1"]) + ", ";
                    sql += toSqlVal(r["link_process_field1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processDashboardChart_GetIds(" + toSqlVal(targetInstance) +
                           "," + toSqlVal(r["chart_ids1"]) + "), ";
                    sql += toSqlVal(r["hide1"]) + " ";
                    sql += "); ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["processDashboardUserGroups"]) {
                var sql = "";
                if (r["dashboard2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_dashboard_user_groups (process_dashboard_id, user_group_id) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processDashboard_GetId(" + toSqlVal(targetInstance) + "," +
                           toSqlVal(r["dashboard1"]) + "), ";
                    sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                           toSqlVal(r["user_group1"]) + ") ";
                    sql += "); ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditions"]) {
                var sql = "DECLARE @json NVARCHAR(MAX) = " + toSqlVal(r["condition_json1"]) +
                          "; DECLARE @json2 NVARCHAR(MAX) = ''; " +
                          "EXEC " + Conv.ToSql(targetDb) + ".dbo.condition_VarReplaceProc " + toSqlVal(targetInstance) +
                          ", @json,  @replaced_json = @json2 OUTPUT ; ";
                bool u = false;

                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.conditions (instance, process, variable, condition_json, order_no, [disabled]) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["variable"]) + ", ";
                    sql += "@json2, ";
                    sql += toSqlVal(r["order_no1"]) + ", ";
                    sql += toSqlVal(r["disabled1"]) + "); ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.conditions SET ";
                    sql += " condition_json = @json2 ";
                    u = true;

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["disabled1"].Equals(r["disabled2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " disabled = " + toSqlVal(r["disabled1"]);
                    }

                    sql += " WHERE condition_id = " + r["id2"] + " ;";
                }

                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionContainers"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.condition_containers (condition_id, process_container_id) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["condition_id"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.processContainer_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_container_id"]) + "));";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionFeatures"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.condition_features (condition_id, feature) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["condition_id"]) + "), ";
                sql += toSqlVal(r["feature"]) + ");";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionGroups"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.condition_groups (condition_id, process_group_id) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["condition_id"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.processGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_group_id"]) + "));";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionPortfolios"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.condition_portfolios (condition_id, process_portfolio_id) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["condition_id"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_portfolio_id"]) + "));";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionStates"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) + ".dbo.condition_states (condition_id, state) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["condition_id"]) + "), ";
                sql += toSqlVal(r["state"]) + ");";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionTabs"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.condition_tabs (condition_id, process_tab_id) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["condition_id"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_tab_id"]) + "));";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionUserGroups"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.condition_user_groups (condition_id, item_id) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["condition_id"]) + "), ";
                sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                       toSqlVal(r["item_id"]) + ") );";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionsGroups"]) {
                var sql = "";
                if (r["variable2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.conditions_groups (instance, process, variable, order_no) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["order_no1"]) + "); ";
                } else {
                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.conditions_groups SET ";
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                        sql += " WHERE instance = " + toSqlVal(targetInstance) + " ";
                        sql += " AND process = " + toSqlVal(r["process1"]) + " ";
                        sql += " AND variable = " + toSqlVal(r["variable1"]) + "; ";
                    }
                }

                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["conditionsGroupsCondition"]) {
                var sql = "";
                if (r["condition_id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.conditions_groups_condition (condition_group_id, condition_id) VALUES ( ";
                    if (Conv.ToStr(r["condition_group_id1"]) == "<no condition group>") {
                        sql += " 0, ";
                    } else {
                        sql += Conv.ToSql(targetDb) + ".dbo.conditionsGroups_GetId(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["condition_group_id1"]) + "), ";
                    }

                    sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["condition_id1"]) + ") ); ";
                } else {
                    if (!r["condition_group_id1"].Equals(r["condition_group_id2"])) {
                        sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.conditions_groups_condition SET ";

                        if (Conv.ToStr(r["condition_group_id1"]) == "<no condition group>") {
                            sql += " condition_group_id = 0 ";
                        } else {
                            sql += " condition_group_id = " + Conv.ToSql(targetDb) + ".dbo.conditionsGroups_GetId(" +
                                   toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_group_id1"]) + ") ";
                        }

                        sql += " WHERE condition_id = " + Conv.ToSql(targetDb) + ".dbo.condition_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["condition_id2"]) + ") ; ";
                    }
                }

                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["menu"]) {
                var sql = "";
                var sql2 = "";
                if (r["id2"] == DBNull.Value) {
                    sql2 += "INSERT INTO " + Conv.ToSql(targetDb) +
                            ".dbo.instance_menu (instance, parent_id, variable, order_no, default_state, icon, link_type, params) VALUES (";
                    sql2 += toSqlVal(targetInstance) + ", ";
                    sql2 += Conv.ToSql(targetDb) + ".dbo.menu_GetId(" + toSqlVal(targetInstance) + ", " +
                            toSqlVal(r["parent_id1"]) + "), ";
                    sql2 += toSqlVal(r["variable"]) + ", ";
                    sql2 += toSqlVal(r["order_no1"]) + ", ";
                    sql2 += toSqlVal(r["default_state1"]) + ", ";
                    sql2 += toSqlVal(r["icon1"]) + ", ";
                    sql2 += toSqlVal(r["link_type1"]) + ", ";
                    if (!skipConfigs) {
                        sql2 += Conv.ToSql(targetDb) + ".dbo.menu_varReplace(" + toSqlVal(targetInstance) + ", " +
                                toSqlVal(r["params1"]) + ") ); ";
                    } else {
                        sql2 += "NULL ); ";
                    }
                } else {
                    var u = false;

                    if (!r["parent_id1"].Equals(r["parent_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " parent_id = " + Conv.ToSql(targetDb) + ".dbo.menu_GetId(" + toSqlVal(targetInstance) +
                               ", " + toSqlVal(r["parent_id1"]) + ")";
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["default_state1"].Equals(r["default_state2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " default_state = " + toSqlVal(r["default_state1"]);
                    }

                    if (!r["icon1"].Equals(r["icon2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " icon = " + toSqlVal(r["icon1"]);
                    }

                    if (!r["link_type1"].Equals(r["link_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " link_type = " + toSqlVal(r["link_type1"]);
                    }

                    if (!skipConfigs) {
                        if (!r["params1"].Equals(r["params2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " params = " + Conv.ToSql(targetDb) + ".dbo.menu_VarReplace(" +
                                   toSqlVal(targetInstance) + ", " + toSqlVal(r["params1"]) + ")";
                        }
                    }

                    if (u) {
                        sql2 = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_menu SET ";
                        sql2 += sql;
                        sql2 += " WHERE instance = " + toSqlVal(targetInstance) + " AND instance_menu_id = " +
                                Conv.ToSql(targetDb) +
                                ".dbo.menu_GetId(" + toSqlVal(targetInstance) + ", " + toSqlVal(r["id2"]) + ") ; ";
                    }
                }

                //sql = sql.Replace("'null'", "null");
                updates.Add(sql2);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["menuRights"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.instance_menu_rights (instance_menu_id, item_id) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.menu_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["instance_menu_id"]) + "), ";
                sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                       toSqlVal(r["item_id"]) + " ));";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["menuStates"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.instance_menu_states (instance_menu_id, item_id, state) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.menu_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["instance_menu_id"]) + "), ";
                sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                       toSqlVal(r["item_id"]) + "), " + toSqlVal(r["state1"]) + " ) ;";
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notification"]) {
                var sql = "";
                var sql2 = "";
                var u = false;
                if (r["guid2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.items (instance, process) VALUES ( " +
                           toSqlVal(targetInstance) + ", 'notification'); SET @new_id = @@identity; ";

                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.items SET guid = " + toSqlVal(r["guid1"]) +
                           " WHERE item_id = @new_id ; ";

                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                           "_notification SET ";
                    sql += " order_no = " + toSqlVal(r["order_no1"]) + ", ";
                    sql += " type = " + toSqlVal(r["type1"]) + ", ";
                    sql += " name_variable = " + Conv.ToSql(targetDb) + ".dbo.notification_VariableVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["name_variable1"]) + "), ";
                    sql += " title_variable = " + Conv.ToSql(targetDb) + ".dbo.notification_VariableVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["title_variable1"]) + "), ";
                    sql += " body_variable = " + Conv.ToSql(targetDb) + ".dbo.notification_VariableVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["body_variable1"]) + "), ";
                    sql += " language = " + toSqlVal(r["language1"]) + ", ";
                    sql += " notify = " + toSqlVal(r["notify1"]) + ", ";
                    sql += " created = " + toSqlVal(r["created1"]) + ", ";
                    sql += " modified = " + toSqlVal(r["modified1"]) + ", ";
                    sql += " expiration = " + toSqlVal(r["expiration1"]) + ", ";
                    sql += " status = " + toSqlVal(r["status1"]) + ", ";
                    sql += " delta = " + toSqlVal(r["delta1"]) + ", ";
                    sql += " digest = " + toSqlVal(r["digest1"]) + ", ";
                    sql += " httprequest_type = " + toSqlVal(r["httprequest_type1"]) + ", ";
                    sql += " httprequest_url = " + toSqlVal(r["httprequest_url1"]) + ", ";
                    sql += " httprequest_header = " + toSqlVal(r["httprequest_header1"]) + ", ";
                    sql += " httprequest_body = " + toSqlVal(r["httprequest_body1"]) + ", ";
                    sql += " user_item_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetIds(" +
                           toSqlVal(r["user_item_id1"]) + "), ";
                    sql += " user_groups = " + Conv.ToSql(targetDb) + ".dbo.item_GetIds(" +
                           toSqlVal(r["user_groups1"]) + "), ";
                    sql += " start = " + toSqlVal(r["start1"]) + ", ";
                    sql += " compare = " + toSqlVal(r["compare1"]) + " ";
                    sql += " WHERE item_id = @new_id ; ";

                    if (r["sender_guid1"] != DBNull.Value) {
                        sql += " DECLARE @sender_guid2 UNIQUEIDENTIFIER; ";
                        sql += " SELECT @sender_guid2 = guid FROM " + Conv.ToSql(targetDb) +
                               ".dbo.items WHERE guid = " + toSqlVal(r["sender_guid1"]) + " ";
                        sql += " IF @sender_guid2 IS NOT NULL BEGIN ";
                        sql += "    INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.item_data_process_selections (item_id, item_column_id, selected_item_id) ";
                        sql += "    VALUES (@new_id, ";
                        sql += "    (SELECT item_column_id FROM " + Conv.ToSql(targetDb) +
                               ".dbo.item_columns WHERE instance = " + toSqlVal(targetInstance) +
                               " AND process = 'notification' AND name = 'sender'),";
                        sql += "    (SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                               toSqlVal(r["sender_guid1"]) + ") ";
                        sql += "    ) ; ";
                        sql += " END ";
                    }
                } else {
                    if (!r["guid1"].Equals(r["guid2"])) {
                        sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo.items SET guid = " + toSqlVal(r["guid1"]) +
                               " WHERE guid = " + toSqlVal(r["guid2"]) + " ; ";
                    }

                    sql2 += "UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                            "_notification SET ";

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["type1"].Equals(r["type2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " type = " + toSqlVal(r["type1"]);
                    }

                    if (!r["name_variable1"].Equals(r["name_variable2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " name_variable = " + Conv.ToSql(targetDb) + ".dbo.notification_VariableVarReplace(" +
                                toSqlVal(targetInstance) + ", " + toSqlVal(r["name_variable1"]) + ") ";
                    }

                    if (!r["title_variable1"].Equals(r["title_variable2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " title_variable = " + Conv.ToSql(targetDb) + ".dbo.notification_VariableVarReplace(" +
                                toSqlVal(targetInstance) + ", " + toSqlVal(r["title_variable1"]) + ") ";
                    }

                    if (!r["body_variable1"].Equals(r["body_variable2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " body_variable = " + Conv.ToSql(targetDb) + ".dbo.notification_VariableVarReplace(" +
                                toSqlVal(targetInstance) + ", " + toSqlVal(r["body_variable1"]) + ") ";
                    }

                    if (!r["language1"].Equals(r["language2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " language = " + toSqlVal(r["language1"]);
                    }

                    if (!r["notify1"].Equals(r["notify2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " notify = " + toSqlVal(r["notify1"]);
                    }

                    if (!r["created1"].Equals(r["created2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " created = " + toSqlVal(r["created1"]);
                    }

                    if (!r["modified1"].Equals(r["modified2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " modified = " + toSqlVal(r["modified1"]);
                    }

                    if (!r["expiration1"].Equals(r["expiration2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " expiration = " + toSqlVal(r["expiration1"]);
                    }

                    if (!r["status1"].Equals(r["status2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " status = " + toSqlVal(r["status1"]);
                    }

                    if (!r["delta1"].Equals(r["delta2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " delta = " + toSqlVal(r["delta1"]);
                    }

                    if (!r["digest1"].Equals(r["digest2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " digest = " + toSqlVal(r["digest1"]);
                    }

                    if (!r["httprequest_type1"].Equals(r["httprequest_type2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " httprequest_type = " + toSqlVal(r["httprequest_type1"]);
                    }

                    if (!r["httprequest_url1"].Equals(r["httprequest_url2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " httprequest_url = " + toSqlVal(r["httprequest_url1"]);
                    }

                    if (!r["httprequest_header1"].Equals(r["httprequest_header2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " httprequest_header = " + toSqlVal(r["httprequest_header2"]);
                    }

                    if (!r["httprequest_body1"].Equals(r["httprequest_body2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " httprequest_body = " + toSqlVal(r["httprequest_body1"]);
                    }

                    if (!r["user_item_id1"].Equals(r["user_item_id2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " user_item_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" +
                                toSqlVal(r["user_item_id1"]) + ")";
                    }

                    if (!r["user_groups1"].Equals(r["user_groups2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " user_groups = " + Conv.ToSql(targetDb) + ".dbo.item_GetIds(" +
                                toSqlVal(r["user_groups1"]) + ")";
                        ;
                    }

                    if (!r["start1"].Equals(r["start2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " start = " + toSqlVal(r["start1"]);
                    }

                    if (!r["compare1"].Equals(r["compare2"])) {
                        if (u) {
                            sql2 += ", ";
                        }

                        u = true;
                        sql2 += " compare = " + toSqlVal(r["compare1"]);
                    }

                    sql2 += " WHERE item_id = " + toSqlVal(r["item_id2"]) + " ; ";
                }

                if (u) {
                    sql += sql2;
                }

                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notificationTagLinks"]) {
                var sql = "";
                var u = false;
                if (r["guid2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.items (instance, process) VALUES ( " +
                           toSqlVal(targetInstance) + ", 'notification_tag_links'); SET @new_id = @@identity; ";

                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.items SET guid = " + toSqlVal(r["guid1"]) +
                           " WHERE item_id = @new_id ; ";

                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                           "_notification_tag_links SET ";
                    sql += " order_no = " + toSqlVal(r["order_no1"]) + " , ";
                    sql += " json_tag_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" +
                           toSqlVal(r["json_tag_guid1"]) + "), ";
                    sql += " item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + "), ";
                    sql += " tag_type = " + toSqlVal(r["tag_type1"]) + " , ";
                    sql += " process_name = " + toSqlVal(r["process_name1"]) + " , ";
                    sql += " view_state = " + toSqlVal(r["view_state1"]) + " , ";
                    sql += " link_param = " + toSqlVal(r["link_param1"]) + " ";
                    sql += " WHERE item_id = @new_id ; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                           "_notification_tag_links SET ";

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]);
                    }

                    if (!r["json_tag_guid1"].Equals(r["json_tag_guid2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " json_tag_id = " + Conv.ToSql(targetDb) + ".dbo.item_GetId(" +
                               toSqlVal(r["json_tag_guid1"]) + ")";
                    }

                    if (!r["item_column_id1"].Equals(r["item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + ")";
                    }

                    if (!r["tag_type1"].Equals(r["tag_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " tag_type = " + toSqlVal(r["tag_type1"]);
                    }

                    if (!r["process_name1"].Equals(r["process_name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_name = " + toSqlVal(r["process_name1"]);
                    }

                    if (!r["view_state1"].Equals(r["view_state2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " view_state = " + toSqlVal(r["view_state1"]);
                    }

                    if (!r["link_param1"].Equals(r["link_param2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " link_param = " + toSqlVal(r["link_param1"]);
                    }

                    sql += " WHERE item_id = (SELECT item_id FROM " + Conv.ToSql(targetDb) +
                           ".dbo.items WHERE [guid] = " + toSqlVal(r["guid1"]) + " ); ";
                }

                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notificationConditions"]) {
                var sql = "";
                var u = false;
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.notification_conditions (instance, process, variable, item_id, before_condition_id, after_condition_id, view_id, in_use, condition_type, process_action_id) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process"]) + ", ";
                    sql += toSqlVal(r["id1"]) + ", ";
                    sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo._" + Conv.ToSql(targetInstance) +
                           "_notification WHERE name_variable = " + toSqlVal(r["item_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["before_condition_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.condition_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["after_condition_id1"]) + "), ";
                    sql += toSqlVal(r["view_id1"]) + ", ";
                    sql += toSqlVal(r["in_use1"]) + ", ";
                    sql += toSqlVal(r["condition_type1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["process_action_id1"]) + ") ) ; ";
                } else {
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.notification_conditions SET ";

                    if (!r["item_id1"].Equals(r["item_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " item_id = (SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo._" +
                               Conv.ToSql(targetInstance) + "_notification WHERE name_variable = " +
                               toSqlVal(r["item_id1"]) + ") ";
                    }

                    if (!r["before_condition_id1"].Equals(r["before_condition_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " before_condition_id = " + Conv.ToSql(targetDb) + ".dbo.condition_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["before_condition_id1"]) + ") ";
                    }

                    if (!r["after_condition_id1"].Equals(r["after_condition_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " after_condition_id = " + Conv.ToSql(targetDb) + ".dbo.condition_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["after_condition_id1"]) + ") ";
                    }

                    if (!r["view_id1"].Equals(r["view_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " view_id = " + toSqlVal(r["view_id1"]);
                    }

                    if (!r["in_use1"].Equals(r["in_use2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " in_use = " + toSqlVal(r["in_use1"]);
                    }

                    if (!r["condition_type1"].Equals(r["condition_type2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " condition_type = " + toSqlVal(r["condition_type1"]);
                    }

                    if (!r["process_action_id1"].Equals(r["process_action_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process_action_id = " + Conv.ToSql(targetDb) + ".dbo.processAction_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["process_action_id1"]) + ") ";
                    }

                    sql += " WHERE notification_condition_id = " + Conv.ToSql(targetDb) +
                           ".dbo.notificationCondition_GetId(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["id2"]) + ") ;";
                }

                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["notificationReceivers"]) {
                var sql = "";
                sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                       ".dbo.notification_receivers (notification_condition_id, item_column_id, list_process, list_item_column_id, process_action_row_id, user_group_item_id) VALUES ( ";
                sql += Conv.ToSql(targetDb) + ".dbo.notificationCondition_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["notification_condition_id1"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["item_column_id1"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["list_process1"]) + "), ";
                sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["list_item_column_id1"]) + ") , ";
                sql += Conv.ToSql(targetDb) + ".dbo.processActionRows_GetId(" + toSqlVal(targetInstance) + ", " +
                       toSqlVal(r["process_action_row_id1"]) + ") , ";
                sql += "(SELECT item_id FROM " + Conv.ToSql(targetDb) + ".dbo.items WHERE guid = " +
                       toSqlVal(r["user_group_item_id1"]) + " ) );";
                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["translations"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.process_translations (instance, process, code, variable, translation) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += toSqlVal(r["code1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.notification_VariableVarReplace(" + toSqlVal(targetInstance) +
                           ", " + toSqlVal(r["id1"]) + "), ";
                    sql += toSqlVal(r["translation1"]) + " ); ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.process_translations SET ";

                    if (!r["process1"].Equals(r["process2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " process = " + toSqlVal(r["process1"]) + " ";
                    }

                    if (!r["translation1"].Equals(r["translation2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " translation = " + toSqlVal(r["translation1"]) + " ";
                    }

                    sql += "WHERE process_translation_id = " + toSqlVal(r["process_translation_id2"]);
                }

                //sql = sql.Replace("'null'", "null");
                //sql = sql.Replace("''", "null");
                updates.Add(sql);
            }

            if (!skipConfigs) {
                foreach (var r in (List<Dictionary<string, object>>)retval["instanceConfigurations"]) {
                    var sql = "";
                    if (r["id2"] == DBNull.Value) {
                        sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.instance_configurations (instance, process, category, name, value, [server]) VALUES (";
                        sql += toSqlVal(targetInstance) + ", ";
                        sql += toSqlVal(r["process1"]) + ", ";
                        sql += "'View Configurator', ";
                        sql += toSqlVal(r["name1"]) + ", ";
                        sql += Conv.ToSql(targetDb) + ".dbo.instanceConfiguration_ValueVarReplace(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["value1"]) + "), ";
                        sql += "0); ";
                    } else {
                        if (!r["value1"].Equals(r["value2"])) {
                            sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_configurations SET ";
                            sql += " value = " + Conv.ToSql(targetDb) + ".dbo.instanceConfiguration_ValueVarReplace(" +
                                   toSqlVal(targetInstance) + ", " + toSqlVal(r["value1"]) + ") ";
                            sql += " WHERE instance = " + toSqlVal(targetInstance);
                            sql += " AND process " + (DBNull.Value.Equals(r["process2"])
                                ? " IS NULL "
                                : " = " + toSqlVal(r["process2"]));
                            sql += " AND category = 'View Configurator' AND name = " + toSqlVal(r["name2"]) +
                                   " AND [server] = 0; ";
                        }
                    }

                    sql = sql.Replace("'null'", "null");
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnions"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_unions (instance, variable, process) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["process1"]) + " ); ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnionRows"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_union_rows (instance_union_id, item_column_id) VALUES ( ";
                    sql += Conv.ToSql(targetDb) + ".dbo.instanceUnion_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["instance_union_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + ") ); ";
                    sql += "SET @new_id = @@identity; ";
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_union_rows SET guid = " +
                           toSqlVal(r["guid1"]) + " WHERE instance_union_row_id = @new_id; ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_union_rows SET ";

                    if (!r["item_column_id1"].Equals(r["item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + ") ";
                    }

                    sql += " WHERE instance_union_row_id = " + toSqlVal(r["id2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnionProcesses"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_union_processes (instance, instance_union_id, process, portfolio_id) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.instanceUnion_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["instance_union_id1"]) + "), ";
                    sql += toSqlVal(r["process1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["portfolio_id1"]) + ") ); ";
                    sql += "SET @new_id = @@identity; ";
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_union_processes SET guid = " +
                           toSqlVal(r["guid1"]) + " WHERE instance_union_process_id = @new_id; ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_union_processes SET ";

                    if (!r["portfolio_id1"].Equals(r["portfolio_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " portfolio_id = " + Conv.ToSql(targetDb) + ".dbo.processPortfolio_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["portfolio_id1"]) + ") ";
                    }

                    sql += " WHERE instance_union_process_id = " + toSqlVal(r["id2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceUnionColumns"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_union_columns (instance_union_process_id, instance_union_row_id, item_column_id) VALUES (";
                    sql += Conv.ToSql(targetDb) + ".dbo.instanceUnionProcess_GetId(" +
                           toSqlVal(r["instance_union_process_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.instanceUnionRow_GetId(" +
                           toSqlVal(r["instance_union_row_id1"]) + "), ";
                    sql += Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" + toSqlVal(targetInstance) + ", " +
                           toSqlVal(r["item_column_id1"]) + ") ); ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_union_columns SET ";

                    if (!r["instance_union_process_id1"].Equals(r["instance_union_process_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " instance_union_process_id = " + Conv.ToSql(targetDb) +
                               ".dbo.instanceUnionProcess_GetId(" + toSqlVal(r["instance_union_process_id1"]) + ") ";
                    }

                    if (!r["instance_union_row_id1"].Equals(r["instance_union_row_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " instance_union_row_id = " + Conv.ToSql(targetDb) + ".dbo.instanceUnionRow_GetId(" +
                               toSqlVal(r["instance_union_row_id1"]) + ") ";
                    }

                    if (!r["item_column_id1"].Equals(r["item_column_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " item_column_id = " + Conv.ToSql(targetDb) + ".dbo.itemColumn_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["item_column_id1"]) + ") ";
                    }

                    sql += " WHERE instance_union_column_id = " + toSqlVal(r["id2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceScheduleGroups"]) {
                var sql = "";
                if (r["group_id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_schedule_groups (instance, variable, schedule_json, [disabled], order_no) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["schedule_json1"]) + ", ";
                    sql += toSqlVal(r["disabled1"]) + ", ";
                    sql += toSqlVal(r["order_no1"]) + "); ";
                }
                else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_schedule_groups SET ";

                    if (!r["schedule_json1"].Equals(r["schedule_json2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " schedule_json = " + toSqlVal(r["schedule_json1"]) + " ";
                    }

                    if (!r["disabled1"].Equals(r["disabled2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " [disabled] = " + toSqlVal(r["disabled1"]) + " ";
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]) + " ";
                    }

                    sql += " WHERE group_id = " + toSqlVal(r["group_id2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceSchedules"]) {
                var sql = "";
                if (r["guid2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_schedules (instance, [name], schedule_json, last_modified, config_options, nickname, [disabled], compare, order_no, group_id) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["name1"]) + ", ";
                    sql += toSqlVal(r["schedule_json1"]) + ", ";
                    sql += toSqlVal(r["last_modified1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.instanceSchedule_OptionsVarReplace(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["config_options1"]) + "), ";
                    sql += toSqlVal(r["nickname1"]) + ", ";
                    sql += toSqlVal(r["disabled1"]) + ", ";
                    sql += toSqlVal(r["compare1"]) + ", ";
                    sql += toSqlVal(r["order_no1"]) + ", ";
                    sql += Conv.ToSql(targetDb) + ".dbo.instanceScheduleGroup_GetId(" +
                           toSqlVal(targetInstance) + ", " + toSqlVal(r["group_id1"]) + ") ); ";
                    sql += "SET @new_id = @@identity; ";
                    sql += "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_schedules SET [guid] = " +
                           toSqlVal(r["guid1"]) + " WHERE schedule_id = @new_id ; ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_schedules SET ";

                    if (!r["name1"].Equals(r["name2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " [name] = " + toSqlVal(r["name1"]) + " ";
                    }

                    if (!r["schedule_json1"].Equals(r["schedule_json2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " schedule_json = " + toSqlVal(r["schedule_json1"]) + " ";
                    }

                    if (!r["last_modified1"].Equals(r["last_modified2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " last_modified = " + toSqlVal(r["last_modified1"]) + " ";
                    }

                    if (!r["config_options1"].Equals(r["config_options2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " config_options = " + Conv.ToSql(targetDb) +
                               ".dbo.instanceSchedule_OptionsVarReplace(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["config_options1"]) + ")";
                    }

                    if (!r["nickname1"].Equals(r["nickname2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " nickname = " + toSqlVal(r["nickname1"]) + " ";
                    }

                    if (!r["disabled1"].Equals(r["disabled2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " [disabled] = " + toSqlVal(r["disabled1"]) + " ";
                    }

                    if (!r["compare1"].Equals(r["compare2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " compare = " + toSqlVal(r["compare1"]) + " ";
                    }

                    if (!r["order_no1"].Equals(r["order_no2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " order_no = " + toSqlVal(r["order_no1"]) + " ";
                    }

                    if (!r["group_id1"].Equals(r["group_id2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " group_id = " + Conv.ToSql(targetDb) +
                               ".dbo.instanceScheduleGroup_GetId(" + toSqlVal(targetInstance) + ", " +
                               toSqlVal(r["group_id1"]) + ")";
                    }

                    sql += " WHERE [guid] = " + toSqlVal(r["guid2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            if (!skipHelps) {
                foreach (var r in (List<Dictionary<string, object>>)retval["instanceHelpers"]) {
                    var sql = "";
                    if (r["id2"] == DBNull.Value) {
                        sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                               ".dbo.instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link, order_no) VALUES (";
                        sql += toSqlVal(targetInstance) + ", ";
                        sql += "'process.tab.' + CAST(" + Conv.ToSql(targetDb) + ".dbo.processTab_GetId(" +
                               toSqlVal(targetInstance) + ", " + toSqlVal(r["identifier1"]) + ") AS NVARCHAR), ";
                        sql += toSqlVal(r["name_variable1"]) + ", ";
                        sql += toSqlVal(r["desc_variable1"]) + ", ";
                        sql += toSqlVal(r["platform1"]) + ", ";
                        sql += toSqlVal(r["media_type1"]) + ", ";
                        sql += toSqlVal(r["video_link1"]) + ", ";
                        sql += toSqlVal(r["order_no1"]) + "); ";
                    } else {
                        var u = false;
                        sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_helpers SET ";

                        if (!r["desc_variable1"].Equals(r["desc_variable2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " desc_variable = " + toSqlVal(r["desc_variable1"]) + " ";
                        }

                        if (!r["platform1"].Equals(r["platform2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " platform = " + toSqlVal(r["platform1"]) + " ";
                        }

                        if (!r["media_type1"].Equals(r["media_type2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " media_type = " + toSqlVal(r["media_type1"]) + " ";
                        }

                        if (!r["video_link1"].Equals(r["video_link2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " video_link = " + toSqlVal(r["video_link1"]) + " ";
                        }

                        if (!r["order_no1"].Equals(r["order_no2"])) {
                            if (u) {
                                sql += ", ";
                            }

                            u = true;
                            sql += " order_no = " + toSqlVal(r["order_no1"]) + " ";
                        }

                        sql += " WHERE instance_helper_id = " + toSqlVal(r["id2"]) + " ;";
                    }

                    sql = sql.Replace("'null'", "null");
                    updates.Add(sql);
                }
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceTranslations"]) {
                var sql = "";
                if (r["variable2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_translations (instance, code, variable, translation) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["code1"]) + ", ";
                    sql += toSqlVal(r["variable1"]) + ", ";
                    sql += toSqlVal(r["translation1"]) + "); ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_translations SET ";

                    if (!r["translation1"].Equals(r["translation2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " translation = " + toSqlVal(r["translation1"]) + " ";
                    }

                    sql += " WHERE instance = " + toSqlVal(targetInstance) + " AND code = " + toSqlVal(r["code2"]) +
                           " AND variable = " + toSqlVal(r["variable2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["frontpageImages"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "DECLARE @new_id INT; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.attachments (instance, parent_id, control_name, type, name) VALUES ( ";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += "'frontpage', ";
                    sql += toSqlVal(r["control_name1"]) + ", ";
                    sql += "0, ";
                    sql += toSqlVal(r["name1"]) + "); ";
                    sql += "SET @new_id = @@identity; ";
                    sql += " UPDATE " + Conv.ToSql(targetDb) + ".dbo.attachments SET guid = " + toSqlVal(r["guid1"]) +
                           " WHERE attachment_id = @new_id ; ";
                    sql += "INSERT INTO " + Conv.ToSql(targetDb_att) +
                           ".dbo.attachments_data (attachment_id, filename, content_type, filesize, filedata) ";
                    sql += "SELECT @new_id, filename, content_type, filesize, filedata FROM " +
                           Conv.ToSql(sourceDb_att) + ".dbo.attachments_data WHERE attachment_id = " +
                           Conv.ToInt(r["id1"]) + "; ";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            foreach (var r in (List<Dictionary<string, object>>)retval["instanceCurrencies"]) {
                var sql = "";
                if (r["id2"] == DBNull.Value) {
                    sql += "INSERT INTO " + Conv.ToSql(targetDb) +
                           ".dbo.instance_currencies (instance, name, abbr, baseline_value) VALUES (";
                    sql += toSqlVal(targetInstance) + ", ";
                    sql += toSqlVal(r["name1"]) + ", ";
                    sql += toSqlVal(r["abbr1"]) + ", ";
                    sql += toSqlVal(r["baseline_value1"]) + "); ";
                } else {
                    var u = false;
                    sql = "UPDATE " + Conv.ToSql(targetDb) + ".dbo.instance_currencies SET ";

                    if (!r["abbr1"].Equals(r["abbr2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " abbr = " + toSqlVal(r["abbr1"]) + " ";
                    }

                    if (!r["baseline_value1"].Equals(r["baseline_value2"])) {
                        if (u) {
                            sql += ", ";
                        }

                        u = true;
                        sql += " baseline_value = " + toSqlVal(r["baseline_value1"]) + " ";
                    }

                    sql += " WHERE currency_id = " + toSqlVal(r["id2"]) + " ;";
                }

                sql = sql.Replace("'null'", "null");
                updates.Add(sql);
            }

            // /\ Inserts & Updates /\

            return updates;
        }

        public string toSqlVal(object o) {
            if (DBNull.Value.Equals(o)) {
                return " NULL ";
            }

            if (o.GetType() == typeof(float) || o.GetType() == typeof(double) ||
                o.GetType() == typeof(decimal)) {
                return "N'" + Conv.ToStr(o).Replace("'", "''").Replace(",", ".") + "'";
            }

            if (o.GetType() == typeof(DateTime)) {
                return "N'" + Conv.ToStr(o).Replace("T", " ") + "'";
            }

            return "N'" + Conv.ToStr(o).Replace("'", "''") + "'";
        }

        public Dictionary<string, object> CompareAll(string sourceInstance, string targetInstance, string targetDb = "",
            string processes = "", bool skipProcessPortfoliosDownloadTemplates = false, bool skipHelps = false,
            bool skipConfigs = false, bool skipRequests = false) {

            InstanceCompareProgress.SetPhaseProgress("Comparing");
            InstanceCompareProgress.CompareProgress = 0;
            
            sourceInstance = Conv.ToSql(sourceInstance);
            targetInstance = Conv.ToSql(targetInstance);
            targetDb = Conv.ToSql(targetDb);
            processes = ParseProcesses(processes);

            SetDBParam(SqlDbType.NVarChar, "@sourceInstance", sourceInstance);
            SetDBParam(SqlDbType.NVarChar, "@targetInstance", targetInstance);

            var currentDb = Conv.ToStr(db.ExecuteScalar("SELECT DB_NAME()"));

            var retval = new Dictionary<string, object>();

            retval.Add("___duplicates", GetDuplicates(sourceInstance));

            var connStr = GetConnectionString(targetDb);
            db = new Connections(connStr, 0);

            GetDuplicates(targetInstance);

            connStr = GetConnectionString(currentDb);
            db = new Connections(connStr, 0);

            retval.Add("languages", InstanceLanguagesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("translations", ProcessTranslationsCompare(sourceInstance, targetInstance, targetDb, processes));

            //retval.Add("calendars", new List<Dictionary<string, object>>());
            //retval.Add("calendarEvents", CalendarEventsCompare(sourceInstance, targetInstance, targetDb, processes));
            //retval.Add("calendarStaticHolidays", CalendarStaticHolidaysCompare(sourceInstance, targetInstance, targetDb, processes));

            retval.Add("processes", ProcessCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processSubprocesses",
                ProcessSubprocessesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("itemColumns",
                ItemColumnsCompare(sourceInstance, targetInstance, targetDb, processes, skipConfigs));

            string lists_sql = " SELECT p.compare, p.process AS list " +
                               ", (SELECT sp.parent_process FROM process_subprocesses sp WHERE sp.process = p.process) AS parent_list " +
                               ", (SELECT tp.process FROM " + Conv.ToSql(targetDb) +
                               ".dbo.processes tp WHERE tp.instance = @targetInstance " +
                               "  AND tp.process = p.process) AS target_list " +
                               ", dbo.processSubprocesses_GetParentsForOrder(p.process) AS sort " +
                               " FROM processes p " +
                               " WHERE p.instance = @sourceInstance AND p.list_process = 1 AND p.compare > 0 " +
                               (processes.Length > 0 ? " AND p.process IN (" + processes + ") " : "") +
                               " ORDER BY sort ASC ";
            List<Dictionary<string, object>> lists = db.GetDatatableDictionary(lists_sql, DBParameters);
            retval.Add("___listItems", lists);
            
            InstanceCompareProgress.SetCompareProgress("List Items");
            foreach (var list in lists) {
                List<Dictionary<string, object>> source_cols = db.GetDatatableDictionary(
                    "SELECT name, variable, data_type, data_additional " +
                    "FROM item_columns " +
                    "WHERE instance = @sourceInstance AND process = " + toSqlVal(list["list"]) + " " +
                    "AND data_type IN (0, 1, 2, 3, 4, 5, 6, 7, 10, 13, 14, 23, 24)"
                    , DBParameters);
                retval.Add("___source_cols_" + Conv.ToStr(list["list"]), source_cols);

                List<Dictionary<string, object>> target_cols = db.GetDatatableDictionary(
                    "SELECT name, variable, data_type, data_additional " +
                    "FROM " + Conv.ToSql(targetDb) + ".dbo.item_columns " +
                    "WHERE instance = @targetInstance AND process = " + toSqlVal(list["list"]) + " " +
                    "AND data_type IN (0, 1, 2, 3, 4, 5, 6, 7, 10, 13, 14, 23, 24)"
                    , DBParameters);
                retval.Add("___target_cols_" + Conv.ToStr(list["list"]), target_cols);

                int compare = Conv.ToInt(list["compare"]);
                if (list["target_list"] == DBNull.Value) {
                    compare = 0;
                }

                retval.Add(Conv.ToStr(list["list"]),
                    ListItemsCompare(sourceInstance, targetInstance, targetDb, processes, Conv.ToStr(list["list"]),
                        compare, source_cols, target_cols));
            }

            string matrixes_sql = " SELECT p.compare, p.process " +
                                  ", (SELECT tp.process FROM " + Conv.ToSql(targetDb) +
                                  ".dbo.processes tp WHERE tp.instance = @targetInstance " +
                                  "  AND tp.process = p.process) AS target_process " +
                                  " FROM processes p " +
                                  " WHERE p.instance = @sourceInstance AND p.list_process = 0 AND p.process_type = 4 AND p.compare > 0 ";
            List<Dictionary<string, object>> matrixes = db.GetDatatableDictionary(matrixes_sql, DBParameters);
            retval.Add("___matrixes", matrixes);
            
            InstanceCompareProgress.SetCompareProgress("Matrices");
            foreach (var matrix in matrixes) {
                int compare = Conv.ToInt(matrix["compare"]);
                if (matrix["target_process"] == DBNull.Value) {
                    compare = 0;
                }

                retval.Add("___matrix_" + Conv.ToStr(matrix["process"]),
                    MatrixCompare(sourceInstance, targetInstance, targetDb, processes, Conv.ToStr(matrix["process"]),
                        compare));
            }

            retval.Add("userGroup", UserGroupCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("containers", ProcessContainersCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("groups", ProcessGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("portfolios", ProcessPortfoliosCompare(sourceInstance, targetInstance, targetDb, processes));
            if (skipProcessPortfoliosDownloadTemplates) {
                retval.Add("portfoliosDownloadTemplates", new List<Dictionary<string, object>>());
            } else {
                retval.Add("portfoliosDownloadTemplates",
                    ProcessPortfoliosDownloadTemplatesCompare(sourceInstance, targetInstance, targetDb, processes));
            }

            retval.Add("tabs", ProcessTabsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("actions", ProcessActionsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("actionsDialog",
                ProcessActionsDialogCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("containerColumns",
                ProcessContainerColumnsCompare(sourceInstance, targetInstance, targetDb, processes, skipHelps));
            retval.Add("portfolioColumnGroups",
                ProcessPortfolioColumnGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("portfolioColumns",
                ProcessPortfolioColumnsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processPortfolioDialogs",
                ProcessPortfolioDialogsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processPortfolioActions",
                ProcessPortfolioActionsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processPortfolioGroups",
                ProcessPortfolioGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processPortfolioUserGroups",
                ProcessPortfolioUserGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processPortfolioTimemachineSaved",
                ProcessPortfolioTimemachineSavedCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("tabContainers",
                ProcessTabContainersCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("actionRowsGroups",
                ProcessActionRowsGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("actionRows", ProcessActionRowsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("itemColumnJoins", ItemColumnJoinsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("itemColumnEquations",
                ItemColumnEquationsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("groupFeatures",
                ProcessGroupFeaturesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("groupTabs", ProcessGroupTabsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("groupNavigation",
                ProcessGroupNavigationCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("tabColumns", ProcessTabColumnsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processTabsSubprocesses",
                ProcessTabsSubprocessesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processTabsSubprocessData",
                ProcessTabsSubprocessDataCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processDashboards",
                ProcessDashboardsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processDashboardCharts",
                ProcessDashboardChartsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processDashboardChartMap",
                ProcessDashboardChartMapCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processDashboardChartLinks",
                ProcessDashboardChartLinksCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("processDashboardUserGroups",
                ProcessDashboardUserGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditions", ConditionsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionContainers",
                ConditionContainersCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionFeatures",
                ConditionFeaturesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionGroups", ConditionGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionPortfolios",
                ConditionPortfoliosCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionStates", ConditionStatesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionTabs", ConditionTabsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionUserGroups",
                ConditionUserGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionsGroups",
                ConditionsGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("conditionsGroupsCondition",
                ConditionsGroupsConditionCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("menu", InstanceMenuCompare(sourceInstance, targetInstance, targetDb, processes, skipConfigs));
            retval.Add("menuRights", InstanceMenuRightsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("menuStates", InstanceMenuStatesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("notification",
                NotificationCompare(sourceInstance, targetInstance, targetDb, processes, skipRequests));
            retval.Add("notificationTagLinks",
                NotificationTagLinksCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("notificationConditions",
                NotificationConditionsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("notificationReceivers",
                NotificationReceiversCompare(sourceInstance, targetInstance, targetDb, processes));
            if (skipConfigs) {
                retval.Add("instanceConfigurations", new List<Dictionary<string, object>>());
            } else {
                retval.Add("instanceConfigurations",
                    InstanceConfigurationsCompare(sourceInstance, targetInstance, targetDb, processes));
            }

            retval.Add("instanceUnions", InstanceUnionsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("instanceUnionRows",
                InstanceUnionRowsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("instanceUnionProcesses",
                InstanceUnionProcessesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("instanceUnionColumns",
                InstanceUnionColumnsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("instanceSchedules",
                InstanceSchedulesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("instanceScheduleGroups",
                InstanceScheduleGroupsCompare(sourceInstance, targetInstance, targetDb, processes));
            if (skipHelps) {
                retval.Add("instanceHelpers", new List<Dictionary<string, object>>());
            } else {
                retval.Add("instanceHelpers",
                    InstanceHelpersCompare(sourceInstance, targetInstance, targetDb, processes));
            }

            retval.Add("instanceTranslations",
                InstanceTranslationsCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("frontpageImages", FrontpageImagesCompare(sourceInstance, targetInstance, targetDb, processes));
            retval.Add("instanceCurrencies",
                InstanceCurrenciesCompare(sourceInstance, targetInstance, targetDb, processes));

            connStr = GetConnectionString(targetDb);
            db = new Connections(connStr, 0);

            targetDb = currentDb;

            GetDuplicates(targetInstance);

            retval.Add("languagesDeletes",
                InstanceLanguagesCompare(targetInstance, sourceInstance, targetDb, processes));
            //retval.Add("translationsDeletes", ProcessTranslationsCompare(targetInstance, sourceInstance, targetDb, processes));
            //retval.Add("calendarsDeletes", new List<Dictionary<string, object>>());
            //retval.Add("calendarEventsDeletes", CalendarEventsCompare(targetInstance, sourceInstance, targetDb, processes));
            //retval.Add("calendarStaticHolidaysDeletes", CalendarStaticHolidaysCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processesDeletes", ProcessCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processSubprocessesDeletes",
                ProcessSubprocessesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("itemColumnsDeletes",
                ItemColumnsCompare(targetInstance, sourceInstance, targetDb, processes, skipConfigs));

            InstanceCompareProgress.SetCompareProgress("Lists");
            foreach (var list in lists) {
                if (list["target_list"] != DBNull.Value) {
                    retval.Add(Conv.ToStr(list["list"] + "Deletes"),
                        ListItemsCompare(targetInstance, sourceInstance, targetDb, processes, Conv.ToStr(list["list"]),
                            Conv.ToInt(list["compare"]),
                            (List<Dictionary<string, object>>)retval["___target_cols_" + Conv.ToStr(list["list"])],
                            (List<Dictionary<string, object>>)retval["___source_cols_" + Conv.ToStr(list["list"])]));
                } else {
                    retval.Add(Conv.ToStr(list["list"] + "Deletes"), new List<Dictionary<string, object>>());
                }
            }
            
            InstanceCompareProgress.SetCompareProgress("Matrices");
            foreach (var matrix in matrixes) {
                if (matrix["target_process"] != DBNull.Value) {
                    retval.Add("___matrix_" + Conv.ToStr(matrix["process"] + "Deletes"),
                        MatrixCompare(targetInstance, sourceInstance, targetDb, processes,
                            Conv.ToStr(matrix["process"]), Conv.ToInt(matrix["compare"])));
                } else {
                    retval.Add("___matrix_" + Conv.ToStr(matrix["process"] + "Deletes"),
                        new List<Dictionary<string, object>>());
                }
            }
            

            retval.Add("containersDeletes",
                ProcessContainersCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("groupsDeletes", ProcessGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("portfoliosDeletes",
                ProcessPortfoliosCompare(targetInstance, sourceInstance, targetDb, processes));
            if (skipProcessPortfoliosDownloadTemplates) {
                retval.Add("portfoliosDownloadTemplatesDeletes", new List<Dictionary<string, object>>());
            } else {
                retval.Add("portfoliosDownloadTemplatesDeletes",
                    ProcessPortfoliosDownloadTemplatesCompare(targetInstance, sourceInstance, targetDb, processes));
            }

            retval.Add("tabsDeletes", ProcessTabsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("actionsDeletes", ProcessActionsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("actionsDialogDeletes",
                ProcessActionsDialogCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("containerColumnsDeletes",
                ProcessContainerColumnsCompare(targetInstance, sourceInstance, targetDb, processes, skipHelps));
            retval.Add("portfolioColumnGroupsDeletes",
                ProcessPortfolioColumnGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("portfolioColumnsDeletes",
                ProcessPortfolioColumnsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processPortfolioDialogsDeletes",
                ProcessPortfolioDialogsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processPortfolioActionsDeletes",
                ProcessPortfolioActionsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processPortfolioGroupsDeletes",
                ProcessPortfolioGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processPortfolioUserGroupsDeletes",
                ProcessPortfolioUserGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processPortfolioTimemachineSavedDeletes",
                ProcessPortfolioTimemachineSavedCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("tabContainersDeletes",
                ProcessTabContainersCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("actionRowsGroupsDeletes",
                ProcessActionRowsGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("actionRowsDeletes",
                ProcessActionRowsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("itemColumnJoinsDeletes",
                ItemColumnJoinsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("itemColumnEquationsDeletes",
                ItemColumnEquationsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("groupFeaturesDeletes",
                ProcessGroupFeaturesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("groupTabsDeletes",
                ProcessGroupTabsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("groupNavigationDeletes",
                ProcessGroupNavigationCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("tabColumnsDeletes",
                ProcessTabColumnsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processTabsSubprocessesDeletes",
                ProcessTabsSubprocessesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processTabsSubprocessDataDeletes",
                ProcessTabsSubprocessDataCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processDashboardsDeletes",
                ProcessDashboardsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processDashboardChartsDeletes",
                ProcessDashboardChartsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processDashboardChartMapDeletes",
                ProcessDashboardChartMapCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processDashboardChartLinksDeletes",
                ProcessDashboardChartLinksCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("processDashboardUserGroupsDeletes",
                ProcessDashboardUserGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionsDeletes", ConditionsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionContainersDeletes",
                ConditionContainersCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionFeaturesDeletes",
                ConditionFeaturesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionGroupsDeletes",
                ConditionGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionPortfoliosDeletes",
                ConditionPortfoliosCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionStatesDeletes",
                ConditionStatesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionTabsDeletes",
                ConditionTabsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionUserGroupsDeletes",
                ConditionUserGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionsGroupsDeletes",
                ConditionsGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("conditionsGroupsConditionDeletes",
                ConditionsGroupsConditionCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("menuDeletes",
                InstanceMenuCompare(targetInstance, sourceInstance, targetDb, processes, skipConfigs));
            retval.Add("menuRightsDeletes",
                InstanceMenuRightsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("menuStatesDeletes",
                InstanceMenuStatesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("notificationDeletes",
                NotificationCompare(targetInstance, sourceInstance, targetDb, processes, skipRequests));
            retval.Add("notificationTagLinksDeletes",
                NotificationTagLinksCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("notificationConditionsDeletes",
                NotificationConditionsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("notificationReceiversDeletes",
                NotificationReceiversCompare(targetInstance, sourceInstance, targetDb, processes));
            if (skipConfigs) {
                retval.Add("instanceConfigurationsDeletes", new List<Dictionary<string, object>>());
            } else {
                retval.Add("instanceConfigurationsDeletes",
                    InstanceConfigurationsCompare(targetInstance, sourceInstance, targetDb, processes));
            }

            retval.Add("instanceUnionsDeletes",
                InstanceUnionsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("instanceUnionRowsDeletes",
                InstanceUnionRowsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("instanceUnionProcessesDeletes",
                InstanceUnionProcessesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("instanceUnionColumnsDeletes",
                InstanceUnionColumnsCompare(targetInstance, sourceInstance, targetDb, processes));
            //retval.Add("instanceSchedulesDeletes", InstanceSchedulesCompare(targetInstance, sourceInstance, targetDb, processes, true));
            //retval.Add("instanceScheduleGroupsDeletes", InstanceScheduleGroupsCompare(targetInstance, sourceInstance, targetDb, processes));
            if (skipHelps) {
                retval.Add("instanceHelpersDeletes", new List<Dictionary<string, object>>());
            } else {
                retval.Add("instanceHelpersDeletes",
                    InstanceHelpersCompare(targetInstance, sourceInstance, targetDb, processes));
            }

            retval.Add("instanceTranslationsDeletes",
                InstanceTranslationsCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("frontpageImagesDeletes",
                FrontpageImagesCompare(targetInstance, sourceInstance, targetDb, processes));
            retval.Add("instanceCurrenciesDeletes",
                InstanceCurrenciesCompare(targetInstance, sourceInstance, targetDb, processes));

            
            InstanceCompareProgress.SetCompareProgress("Done");
            return retval;
        }

        public string GetsourceDb() {
            return Conv.ToStr(db.ExecuteScalar("SELECT DB_NAME()"));
        }

        public List<string> DeployChanges(string sourceInstance, string targetInstance, string targetDb = "",
            string processes = "", string sourceDb = "", bool skipProcessPortfoliosDownloadTemplates = false,
            bool skipHelps = false, bool skipConfigs = false, bool skipRequests = false) {
            InstanceCompareProgress.PhaseCount += 2;

            var retv = new List<string>();
            InstanceCompareProgress.SetPhaseProgress("Deploying");
            
            var retval = CompareAll(sourceInstance, targetInstance, targetDb, processes, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests);
               
            var upds = GetUpdateSql(retval, targetInstance, targetDb, sourceDb, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests);

            var connStr = GetConnectionString(targetDb);
            var dbDeploy = new Connections(connStr, 0);

            InstanceCompareProgress.DeployProgress = 0;
            InstanceCompareProgress.DeployCount = upds.Count + 1;

            dbDeploy.BeginTransaction();

            var lastSql = "";
            try {
                foreach (var sql in upds) {
                    lastSql = sql;

                    if (sql.ToLower().Contains("update")) {
                        InstanceCompareProgress.SetDeployProgress("Update");
                        dbDeploy.ExecuteUpdateQuery(sql, DBParameters);
                    } else if (sql.ToLower().Contains("insert")) {
                        InstanceCompareProgress.SetDeployProgress("Insert");
                        dbDeploy.ExecuteInsertQuery(sql, DBParameters);
                    } else if (sql.ToLower().Contains("delete")) {
                        InstanceCompareProgress.SetDeployProgress("Delete");
                        dbDeploy.ExecuteDeleteQuery(sql, DBParameters);
                    } else if (sql.ToLower().Contains("exec")) {
                        InstanceCompareProgress.SetDeployProgress("Execute");
                        dbDeploy.ExecuteStoredProcedure(sql, DBParameters);
                    }
                }

                InstanceCompareProgress.SetDeployProgress("Commit");
                dbDeploy.CommitTransaction();

                retv.Add("OK");
                retv.Add(Conv.ToStr(upds.Count));
            }
            catch (Exception ex) {
                retv.Add("NOT OK");
                retv.Add(lastSql);
                retv.Add(ex.Message);
                dbDeploy.RollbackTransaction();
            }

            return retv;
        }

        private string GetConnectionString(string targetDb) {
            var connStr = Conv.ToStr(Config.GetValue("HostingEnvironment", "ConnectionString")).ToLower();
            var origConnStr = Conv.ToStr(Config.GetValue("HostingEnvironment", "ConnectionString"));
            var start = connStr.IndexOf("initial catalog=");
            var end = connStr.IndexOf(";", start);

            var dbStr = origConnStr.Substring(start, end - start);

            connStr = origConnStr.Replace(dbStr, "initial catalog=" + Conv.ToSql(targetDb) + "");

            return connStr;
        }

        private string ParseProcesses(string processes) {
            string result = "";
            if (processes != "___none___" && processes != "") {
                foreach (string process in processes.Split(",")) {
                    if (result != "") {
                        result += ", ";
                    }

                    result += "'" + Conv.ToSql(process) + "'";
                }
            }

            return result;
        }

        public List<Dictionary<string, object>> DiscoverGUID(Guid guid, string target_db = "") {
            string database = target_db;
            string database_sql = "";
            if (target_db.Length > 0) {
                database_sql = Conv.ToSql(target_db) + ".dbo.";
            } else {
                database = GetsourceDb();
            }

            List<Dictionary<string, object>> guid_infos = new List<Dictionary<string, object>>();
            SetDBParam(SqlDbType.UniqueIdentifier, "guid", guid);
            string sql =
                "SELECT i.item_id, i.instance, i.process, p.variable, p.list_process, p.process_type, p.list_type, p.compare " +
                "FROM " + database_sql + "items i " +
                "LEFT JOIN " + database_sql + "processes p ON p.instance = i.instance AND p.process = i.process " +
                "WHERE i.guid = @guid ";
            List<Dictionary<string, object>> items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 2);
                items[0].Add("database", database);

                SetDBParam(SqlDbType.Int, "item_id", Conv.ToInt(items[0]["item_id"]));
                sql = "SELECT * " +
                      "FROM " + database_sql + "_" + items[0]["instance"] + "_" + items[0]["process"] + " " +
                      "WHERE item_id = @item_id";
                DataRow item = db.GetDataRow(sql, DBParameters);

                if (Conv.ToInt(items[0]["list_process"]) == 1) {
                    if (item.Table.Columns.Contains("list_item_" + authenticateSession.locale_id)) {
                        items[0].Add("item_name", Conv.ToStr(item["list_item_" + authenticateSession.locale_id]));
                    } else {
                        items[0].Add("item_name", Conv.ToStr(item["list_item"]));
                    }
                } else if (Conv.ToStr(items[0]["process"]) == "user") {
                    items[0].Add("item_name",
                        Conv.ToStr(item["last_name"]) + " " + Conv.ToStr(item["first_name"]) + " (" +
                        Conv.ToStr(item["login"]) + ")");
                } else if (Conv.ToStr(items[0]["process"]) == "user_group") {
                    items[0].Add("item_name", Conv.ToStr(item["usergroup_name"]));
                } else if (Conv.ToStr(items[0]["process"]) == "notification") {
                    items[0].Add("item_name", t.GetTranslation(Conv.ToStr(item["name_variable"])));
                } else if (Conv.ToInt(items[0]["process_type"]) == 4) {
                    items[0].Add("item_name", t.GetTranslation(Conv.ToStr(item["name"])));
                } else if (item.Table.Columns.Contains("title")) {
                    items[0].Add("item_name", Conv.ToStr(item["title"]));
                } else {
                    items[0].Add("item_name", "");
                }

                guid_infos.Add(items[0]);
            }

            sql =
                "SELECT pcc.process_container_column_id AS id, pc.variable AS container, ic.variable AS [column], p.variable AS process, p.instance " +
                "FROM " + database_sql + "process_container_columns pcc " +
                "LEFT JOIN " + database_sql +
                "process_containers pc ON pc.process_container_id = pcc.process_container_id " +
                "LEFT JOIN " + database_sql + "item_columns ic ON ic.item_column_id = pcc.item_column_id " +
                "LEFT JOIN " + database_sql + "processes p ON p.instance = pc.instance AND p.process = pc.process " +
                "WHERE pcc.guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 3);
                items[0].Add("database", database);
                guid_infos.Add(items[0]);
            }

            sql =
                "SELECT ppa.process_portfolio_action_id AS id, ppa.button_text_variable AS name, pp.variable AS portfolio, p.variable AS process, p.instance " +
                "FROM " + database_sql + "process_portfolio_actions ppa " +
                "LEFT JOIN " + database_sql +
                "process_portfolios pp ON pp.process_portfolio_id = ppa.process_portfolio_id " +
                "LEFT JOIN " + database_sql + "processes p ON p.instance = pp.instance AND p.process = pp.process " +
                "WHERE ppa.guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 4);
                items[0].Add("database", database);

                var jsons = JObject.Parse(Conv.ToStr(items[0]["name"]));
                items[0]["name"] = "";
                foreach (var json in jsons) {
                    if (json.Key == authenticateSession.locale_id || Conv.ToStr(items[0]["name"]) == "") {
                        items[0]["name"] = json.Value;
                    }
                }

                guid_infos.Add(items[0]);
            }

            sql = "SELECT par.process_action_row_id AS id, pa.variable AS action, p.variable AS process, p.instance " +
                  "FROM " + database_sql + "process_action_rows par " +
                  "LEFT JOIN " + database_sql + "process_actions pa ON pa.process_action_id = par.process_action_id " +
                  "LEFT JOIN " + database_sql + "processes p ON p.instance = pa.instance AND p.process = pa.process " +
                  "WHERE par.guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 5);
                items[0].Add("database", database);
                guid_infos.Add(items[0]);
            }

            sql =
                "SELECT ice.item_column_equation_id AS id, ic.variable AS [column], p.variable AS process, p.instance " +
                "FROM " + database_sql + "item_column_equations ice " +
                "LEFT JOIN " + database_sql + "item_columns ic ON ic.item_column_id = ice.parent_item_column_id " +
                "LEFT JOIN " + database_sql + "processes p ON p.instance = ic.instance AND p.process = ic.process " +
                "WHERE ice.guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 6);
                items[0].Add("database", database);
                guid_infos.Add(items[0]);
            }

            sql = "SELECT pts.process_tab_subprocess_id AS id, pt.variable AS tab, p.variable AS process, p.instance " +
                  "FROM " + database_sql + "process_tabs_subprocesses pts " +
                  "LEFT JOIN " + database_sql + "process_tabs pt ON pt.process_tab_id = pts.process_tab_id " +
                  "LEFT JOIN " + database_sql +
                  "processes p ON p.instance = pts.instance AND p.process = pts.process " +
                  "WHERE pts.guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 7);
                items[0].Add("database", database);
                guid_infos.Add(items[0]);
            }

            sql = "SELECT iur.instance_union_row_id AS id, iu.variable AS [union], iu.instance " +
                  "FROM " + database_sql + "instance_union_rows iur " +
                  "LEFT JOIN " + database_sql + "instance_unions iu ON iu.instance_union_id = iur.instance_union_id " +
                  "WHERE iur.guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 8);
                items[0].Add("database", database);
                guid_infos.Add(items[0]);
            }

            sql =
                "SELECT iup.instance_union_process_id AS id, iu.variable AS [union], p.variable AS process, p.instance " +
                "FROM " + database_sql + "instance_union_processes iup " +
                "LEFT JOIN " + database_sql + "instance_unions iu ON iu.instance_union_id = iup.instance_union_id " +
                "LEFT JOIN " + database_sql + "processes p ON p.instance = iup.instance AND p.process = iup.process " +
                "WHERE iup.guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 9);
                items[0].Add("database", database);
                guid_infos.Add(items[0]);
            }

            sql = "SELECT schedule_id AS id, name, instance " +
                  "FROM " + database_sql + "instance_schedules " +
                  "WHERE guid = @guid ";
            items = db.GetDatatableDictionary(sql, DBParameters);
            if (items != null && items.Count > 0) {
                items[0].Add("status", 10);
                items[0].Add("database", database);
                guid_infos.Add(items[0]);
            }

            return guid_infos;
        }
    }
}