﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core {
	[Route("v1/diagnostics/instancelogs")]
	public class InstanceLogRest : PageBase {
		public InstanceLogRest() : base("eventLog") { }

		[HttpPost("")]
		public List<InstanceLog> GetInstanceLogs([FromBody] InstanceLogFilter filters) {
			CheckRight("read");
			var i = new InstanceLogs();
			return i.GetInstanceLogs(instance, filters);
		}
	}

	[Route("v1/diagnostics/runtimeinfo/{num}")]
	public class RuntimeInfo : RestBase {
		[HttpGet("")]
		public Dictionary<string, object> Get(int num) {
			var r = new RuntimeModel();
			return r.GetModel(instance, currentSession.item_id, HttpContext, currentSession, num);
		}
	}

	[Route("v1/diagnostics/getOnlineStatus/")]
	public class getOnlineStatus : RestBase {
		[HttpGet("")]
		public OnlineStatusResult Get() {
			return new OnlineStatusResult() {
				sessions = Diag.ConnectedSignalRUsers,
				issues = Diag.LastConfigurationReport,
				errors = Diag.ErrorsToday(currentSession.instance)
			};
		}
	}

	public class OnlineStatusResult {
		public int sessions;
		public ConfigurationReport issues;
		public int errors;
	}

	[Route("v1/diagnostics/ping")]
	public class Ping : Controller {
		[HttpGet("")]
		public string Get() {
			return "pong";
		}
	}

	[Route("v1/diagnostics/version")]
	public class Version : Controller {
		[HttpGet("")]
		public Dictionary<string, string> Get() {
			var x = new Dictionary<string, string>();
			x.Add("version", Config.Version);
			x.Add("versionDate", Config.VersionDate);
			x.Add("repo", Config.Repo);
			return x;
		}
	}

	[Route("v1/diagnostics")]
	public class Diagnostics : PageBase {
		public Diagnostics() : base("diagnostics") { }

		[HttpGet("")]
		public List<string> Get() {
			if (Diag.LogQueries) {
				return Diag.GetSqlQueries();
			}

			return null;
		}

		[HttpGet("restart")]
		public void RestartServer() {
			Task.Factory.StartNew(() => { Environment.Exit(0); });
		}

		[HttpGet("createMonitor")]
		public int CreateUptimeMonitor() {
			var uptime = new UptimeMonitor(instance, currentSession);
			return uptime.CreateUptimeMonitor(HttpContext);
		}

		[HttpGet("getMonitorData")]
		public dynamic GetMonitorData() {
			var uptime = new UptimeMonitor(instance, currentSession);
			return uptime.GetUptimeData(HttpContext);
		}

		[HttpGet("localVersions/{listname}")]
		public Dictionary<string, List<Diag.ApplicationVersion>> GetAllApplications(string listname) {
			var result =
				new Dictionary<string, List<Diag.ApplicationVersion>>();
			var thisEnvironmentVersions = Diag.GetList();
			result.Add(Config.GetValue("Deployment", "ServerName"), thisEnvironmentVersions);
			var user = Config.GetValue("Deployment", "DiagnosticsUserName");
			var password = Config.GetValue("Deployment", "DiagnosticsPassword");
			var serverDetails = Diag.GetAllServers(listname, instance, currentSession.item_id);
			foreach (var serverDetail in serverDetails) {
				var requestToken =
					(HttpWebRequest) WebRequest.Create(serverDetail.url + "core/Login/IntegrationToken/" +
					                                   serverDetail.tokenKey);
				var postData = "{'Username':'" + user + "','Password':'" + password + "','Instance':'" +
				               serverDetail.instance + "'}";
				var data = Encoding.ASCII.GetBytes(postData);

				requestToken.ContentType = "application/JSON";
				requestToken.Method = "POST";
				requestToken.KeepAlive = true;
				requestToken.ContentLength = data.Length;

				using (var stream = requestToken.GetRequestStream()) {
					stream.Write(data, 0, data.Length);
				}

				var tokenValue = "";

				using (var tokenResponse = requestToken.GetResponse() as HttpWebResponse) {
					if (tokenResponse.StatusCode == HttpStatusCode.OK) {
						using (var reader = new StreamReader(tokenResponse.GetResponseStream())) {
							tokenValue = reader.ReadToEnd();
						}
					}

					//requestToken.Credentials = new NetworkCredential(user, password);

					var requestVersionList =
						(HttpWebRequest) WebRequest.Create(
							serverDetail.url + "integration/getVersionList");

					requestVersionList.ContentType = "application/JSON";
					requestVersionList.Method = "GET";
					requestVersionList.KeepAlive = true;
					requestVersionList.Headers.Add("key", serverDetail.tokenKey);
					//Console.WriteLine(tokenValue);
					requestVersionList.Headers.Add("token", tokenValue);
					requestVersionList.Credentials = new NetworkCredential(user, password);
					requestVersionList.ServerCertificateValidationCallback = delegate { return true; };
					using (var versionListResponse = requestVersionList.GetResponse() as HttpWebResponse) {
						if (versionListResponse.StatusCode == HttpStatusCode.OK) {
							using (var stream = versionListResponse.GetResponseStream())
							using (var reader = new StreamReader(stream)) {
								var jsonResponse = reader.ReadToEnd();
								var integrationVersions =
									JsonConvert.DeserializeObject<List<Diag.ApplicationVersion>>(
										jsonResponse);
								result.Add(serverDetail.name, integrationVersions);
							}
						}
					}
				}
			}

			return result;
		}

		[HttpGet("testemail/{address}")]
		public string TestEmail(string address) {
			var test = new Email(instance, currentSession, "Test mail", "Test mail");
			test.Add(address, Email.AddressTypes.ToAdress);
			return "Result: " + test.Send(async: false);
		}

		[HttpGet("formattedQueries/{empty}")]
		public string Get(bool empty) {
			var queries = new Dictionary<string, int>();
			foreach (var query in Diag.GetSqlQueries()) {
				if (!queries.ContainsKey(query)) {
					queries.Add(query, 1);
				} else {
					queries[query] = queries[query] + 1;
				}
			}

			var retval = "";
			var total = 0;

			foreach (var key in queries.Keys) {
				retval += "<br><b>" + queries[key] + "</b>:<br> " + key;
				total += queries[key];
			}

			retval += "<br><b>Total: " + total + "</b>";

			Response.ContentType = "text/html";

			//Empty queries
			if (empty) {
				Diag.ClearSqlQueries();
			}

			return "<html><body> " + retval + "</body></html>";
		}

		[HttpGet("event/{category}/{page}/{limit}")]
		public string GetEvents(int category, int page, int limit) {
			try {
				if (category == 0) {
					APIResult.data = Diag.GetLogs(instance, page, limit);
				} else {
					APIResult.data = Diag.GetLogs(instance, (Diag.LogCategories) category, page, limit);
				}

				return success(APIResult);
			} catch (Exception ex) {
				return failure(APIResult, FailureCodes.FailureOnServer, ex);
			}
		}

		[HttpGet("{id}")]
		public string Get(int id) {
			if (Diag.LogQueries) {
				switch (id) {
					case 0:
						Diag.ClearSqlQueries();
						return "cleared queries";
					case 1:
						Cache.Initialize();
						return "cleared cache";
				}
			}

			return "";
		}

		[HttpGet("rests")]
		public Dictionary<string, List<string>> GetRests() {
			return (Dictionary<string, List<string>>) Cache.Get("", "rests");
		}
	}
}