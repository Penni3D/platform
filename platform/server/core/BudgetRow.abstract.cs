﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	[Serializable]
	public class BudgetRowData : ItemBase {
		private static readonly string _process = "budget_row";
		private AuthenticatedSession authenticatedSession;
		private string instance;

		public BudgetRowData(string instance, AuthenticatedSession authenticatedSession) : base(instance, _process,
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
			this.instance = instance;
		}

		public List<Dictionary<string, object>> CopyBudgetRows(int sourceId, int targetId) {
			var s = new Dictionary<string, object>();
			s.Add("project_item_id", sourceId);
			var budgetRows = GetItems(s);
			foreach (var bRow in budgetRows) {				
				bRow["project_item_id"] = targetId;
				var oldId = Conv.ToInt(bRow["item_id"]);
				bRow.Remove("item_id");
				var newId = InsertItemRow();
				bRow.Add("item_id", newId);
				SaveItem(bRow);
			}			
			return budgetRows;
		}
	}
}