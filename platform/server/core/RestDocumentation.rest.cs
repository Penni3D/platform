﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/meta/RestDocs/")]
	public class RestDocumentationRest : RestBase {
		[HttpGet("")]
		public RestDocs Get() {
			var processes = new Dictionary<string, string>();
			var pros = new Processes(instance, currentSession);
			foreach (var pro in pros.GetProcesses()) {
				processes.Add(pro.ProcessName, pro.Variable);
			}
			var rd = new RestDocumentation(instance, currentSession);
			var vd = Config.VirtualDirectory;
			if (!vd.EndsWith("/")) vd += "/";
			return rd.GetRestDocs(processes, HttpContext.Request.Scheme + "://" + HttpContext.Request.Host + vd);
		}

		[HttpGet("{process}")]
		public RestDocs Get(string process) {
			var processes = new Dictionary<string, string>();
			var pros = new Processes(instance, currentSession);
			var pro = pros.GetListProcess(process);
			processes.Add(pro.ProcessName, pro.Variable);
			var rd = new RestDocumentation(instance, currentSession);
			var vd = Config.VirtualDirectory;
			if (!vd.EndsWith("/")) vd += "/";
			return rd.GetRestDocs(processes, HttpContext.Request.Scheme + "://" + HttpContext.Request.Host + vd);
		}
	}
}