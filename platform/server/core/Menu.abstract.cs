﻿using System.Collections.Generic;

namespace Keto5.x.platform.server.core {
	public class MenuItem {
		public int? InstanceMenuId { get; set; }
		public Dictionary<string, object> Params { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }

		public string Icon { get; set; }
		public List<Dictionary<string, object>> Links { get; set; }
	}

	public class MenuGroup {
		public List<Dictionary<string, object>> tabs;
		public int processGroupId { get; set; }
		public string icon { get; set; }
		public Dictionary<string, string> translation { get; set; }
		public string iconClass { get; set; }
	}
}
