﻿using System.Collections.Generic;
using System.Linq;

namespace Keto5.x.platform.server.core {
	public class ConditionJSONItem {
		public int ItemColumnId { get; set; }
		public int ComparisonTypeId { get; set; }
		public string Value { get; set; }
	}

	public class ExistingConditionJSONItem {
		public int ConditionId { get; set; }
		public int ComparisonTypeId { get; set; }
		public string Value { get; set; }
	}

	public class UserConditionJSONItem {
		public int ItemColumnId { get; set; }
		public int ComparisonTypeId { get; set; }
		public int UserColumnTypeId { get; set; }
		public int UserColumnId { get; set; }
	}

	public enum ConditionType {
		CONDITION = 1,
		EXISTING_CONDITION = 2,
		SIGNED_IN_USER_CONDITION = 3,
		PARENT_PROCESS_CONDITION = 4,
		PARENT_USER_CONDITION = 5,
		DATE_DIFF_CONDITION = 7,
		PARENT_PROCESS_EXISTING_CONDITION = 8
	}

	public class Operation {
		public int ConditionTypeId { get; set; }
		public object Condition { get; set; }
		public int? OperatorId { get; set; }

		public bool IsOfType(ConditionType conditionType) {
			return ConditionTypeId == (int) conditionType;
		}

		public bool IsOneOfType(params ConditionType[] conditionTypes) {
			return conditionTypes.Any(conditionType => IsOfType(conditionType));
		}
	}

	public class OperationGroup {
		public List<Operation> Operations { get; set; }
	}

	public class ConditionContent {
		public OperationGroup OperationGroup { get; set; }
		public int? OperatorId { get; set; }

		public bool Disabled { get; set; }
	}
}
