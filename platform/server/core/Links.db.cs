﻿using System;
using System.Data;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	public class Link {
		public Link(DataRow row, string alias = "") {
			if (alias.Length > 0) alias = alias + "_";

			Url = (string)row["url"];
			Params = (string)row["params"];
			Created = (DateTime)row["created"];
			Expiration = (int)row["expiration"];
		}

		public string Url { get; set; }
		public string Params { get; set; }
		public DateTime Created { get; set; }
		public int Expiration { get; set; }
	}

	public class Links : ConnectionBase {
		private AuthenticatedSession _authS;

		public Links(string instance, AuthenticatedSession aS) : base(instance, aS) {
			_authS = aS;
		}

		public void SetUrl(string url, string Params, int expiration) {
			SetDBParam(SqlDbType.NVarChar, "url", url);
			SetDBParam(SqlDbType.NVarChar, "params", Params);
			SetDBParam(SqlDbType.DateTime, "created", DateTime.Now.ToUniversalTime());
			SetDBParam(SqlDbType.Int, "expiration", expiration);

			db.ExecuteInsertQuery("INSERT INTO links(url, params, created, expiration) VALUES(@url, @params, @created, @expiration)", DBParameters);
		}

		public string GetParams(string uuid) {
			SetDBParam(SqlDbType.NVarChar, "uuid", uuid);

			var r = db.GetDataRow("SELECT * FROM links WHERE url = @uuid", DBParameters);

			if (r != null) {
				var l = new Link(r);
				if ((DateTime.Now.ToUniversalTime() - l.Created).TotalDays < l.Expiration) {
					return l.Params;
				}
				db.ExecuteDeleteQuery("DELETE FROM links WHERE url = @uuid", DBParameters);
				return null;
			}
			return null;
		}
	}
}
