﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	public class Helpers : ConnectionBase {
		private AuthenticatedSession session;
		private string instance_;

		public Helpers(string instance, AuthenticatedSession session) : base(instance, session) {
			this.session = session;
			this.instance_ = instance;
		}

		public List<HelperModel> GetHelpers() {
			const string sql = "SELECT * FROM instance_helpers WHERE instance = @instance ORDER BY order_no  ASC";
			return (from DataRow r in db.GetDatatable(sql, DBParameters).Rows
				select new HelperModel(r, session, instance_)).ToList();
		}

		public bool SaveHelper(List<HelperModel> hms) {
			foreach (var h in hms) {
				if (h.VideoLink != null) {

					if (h.Platform) {
						h.VideoLink = h.VideoLink.Remove(0,37);
					}

					SetDBParameter("@videolink", h.VideoLink);
				} else {
					SetDBParam(SqlDbType.NVarChar, "@videolink", DBNull.Value);
				}
				SetDBParameter("@instance", instance);
				SetDBParameter("@platform", Conv.ToInt(h.Platform));
				SetDBParameter("@mediatype", h.MediaType);
				SetDBParameter("@id", h.InstanceHelperId);
				SetDBParameter("@namevariable", h.NameVariable);
				SetDBParameter("@descvariable", h.DescVariable);
				SetDBParameter("@orderNo", h.OrderNo == null ? DBNull.Value : h.OrderNo);
				db.ExecuteUpdateQuery(
					"UPDATE instance_helpers SET order_no = @orderNo, platform = @platform, media_type = @mediatype, video_link = @videolink, desc_variable = @descvariable WHERE instance_helper_id = @id",
					DBParameters);

				foreach (var langTransPair in h.LanguageTranslationName) {
					SetDBParameter("@translation", langTransPair.Value);
					SetDBParameter("@code", langTransPair.Key);

					var existsAlready = Conv.ToInt(db.ExecuteScalar(
						"SELECT COUNT(*) FROM instance_translations WHERE code = @code AND variable = @namevariable",
						DBParameters));
					if (existsAlready != 0) {
						db.ExecuteUpdateQuery(
							"UPDATE instance_translations SET translation = @translation WHERE code = @code AND variable = @namevariable",
							DBParameters);
					} else {
						db.ExecuteInsertQuery(
							"INSERT INTO instance_translations (instance, code, variable, translation) VALUES(@instance, @code, @namevariable, @translation)",
							DBParameters);
					}
					Cache.Set(instance, "instance_translations", null);
				}

			}
			return true;
		}

		public void DeleteHelper(int helperId) {
			SetDBParameter("@helperId", helperId);
			db.ExecuteDeleteQuery("DELETE FROM instance_helpers WHERE instance_helper_id = @helperId", DBParameters);
		}

		public HelperModel AddHelper(HelperModel h) {

			if (h.VideoLink != null) {
				SetDBParameter("@videolink", h.VideoLink);
			} else {
				SetDBParam(SqlDbType.NVarChar, "@videolink", DBNull.Value);
			}

			SetDBParameter("@instance", instance);
			SetDBParameter("@identifier", h.Identifier);
			SetDBParameter("@platform", Conv.ToInt(h.Platform));
			SetDBParameter("@mediatype", h.MediaType = h.MediaType == null ? 1 : h.MediaType);
			var translations = new Translations(instance, session);

			var nameVariable = translations.GetHelpTextVariable(h.LanguageTranslationName[session.locale_id], "name");

			SetDBParameter("@langVariable", nameVariable);
			SetDBParameter("@descJson", h.DescVariable);

			foreach (var langTransPair in h.LanguageTranslationName) {
				SetDBParameter("@translation", langTransPair.Value);
				SetDBParameter("@code", langTransPair.Key);

				db.ExecuteInsertQuery(
					"INSERT INTO instance_translations (instance, code, variable, translation) VALUES(@instance, @code, @langVariable, @translation)",
					DBParameters);
				Cache.Set(instance, "instance_translations", null);
			}

			h.InstanceHelperId = db.ExecuteInsertQuery(
				"INSERT INTO instance_helpers (instance, identifier, name_variable, desc_variable, platform, media_type, video_link, order_no) VALUES(@instance,@identifier, @langVariable, @descJson, @platform, @mediatype, @videolink, ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM instance_helpers WHERE identifier = @identifier AND order_no <> '' ORDER BY order_no  DESC),null)),'U'))",
				DBParameters);

			h.NameVariable = nameVariable;

			return h;
		}
	}

	public class HelperModel {
		public HelperModel() { }

		public HelperModel(DataRow row, AuthenticatedSession session, string instance) {
			InstanceHelperId = Conv.ToInt(row["instance_helper_id"]);
			Identifier = Conv.ToStr(row["identifier"]);
			NameVariable = Conv.ToStr(row["name_variable"]);
			DescVariable = Conv.ToStr(row["desc_variable"]);

			MediaType = Conv.ToInt(row["media_type"]);
			Platform = Conv.ToBool(Conv.ToInt(row["platform"]));

			if (row["name_variable"] != null) {
				LanguageTranslationName =
					new Translations(instance, session).GetInstanceTranslations(Conv.ToStr(row["name_variable"]));
			}
			if (Platform) {
				VideoLink = "https://static.ketoapps.com/platform/" + Conv.ToStr(row["video_link"]);
			} else {
				VideoLink = Conv.ToStr(row["video_link"]);
			}
			OrderNo = Conv.ToStr(row["order_no"]);
		}

		public int InstanceHelperId { get; set; }
		public string Identifier { get; set; }
		public string NameVariable { get; set; }
		public string DescVariable { get; set; }
		public bool Platform { get; set; }
		public int? MediaType { get; set; }
		public string VideoLink { get; set; }

		public string OrderNo { get; set; }

		public Dictionary<string, string> LanguageTranslationName { get; set; }
		public Dictionary<string, string> LanguageTranslationDescription { get; set; }
	}
}