﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	public class Translations : ConnectionBase {
		private readonly string locale_id;
		private readonly AuthenticatedSession authenticatedSession;

		public Translations(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			locale_id = authenticatedSession.locale_id;
			this.authenticatedSession = authenticatedSession;
		}

		public Translations(string instance, AuthenticatedSession authenticatedSession, string localeid) : base(
			instance,
			authenticatedSession) {
			locale_id = localeid;
			this.authenticatedSession = authenticatedSession;
		}

		public Translations(string instance, string localeid) : base(instance, new AuthenticatedSession("", 0, null)) {
			locale_id = localeid;
		}

		public string GetTranslation(Dictionary<string, string> Translation, string localeId = "") {
			if (localeId == "") localeId = locale_id;
			if (Translation.ContainsKey(localeId) && Translation[localeId] != "") return Translation[localeId];

			if (authenticatedSession != null) {
				var ins = new Instances(instance, authenticatedSession);
				var i = ins.GetInstance();
				if (Translation.ContainsKey(i.DefaultLocaleId) && Translation[i.DefaultLocaleId] != "")
					return Translation[i.DefaultLocaleId];
			}

			foreach (var t in Translation) {
				if (t.Value != "") return t.Value;
			}

			return "";
		}

		public Dictionary<string, string> GetTranslations() {
			if (Cache.Get(instance, "languages_" + locale_id) == null) {
				DBParameters.Clear();

				//Get Data from Database
				SetDBParam(SqlDbType.NVarChar, "instance", instance);
				SetDBParam(SqlDbType.NVarChar, "code", locale_id);

				var sql = "SELECT variable, translation FROM (" +
				          "SELECT variable, translation FROM process_translations WHERE variable <> '' AND @instance = instance AND code = @code " +
				          "UNION ALL " +
				          "SELECT variable, translation FROM instance_translations WHERE variable <> '' AND @instance = instance AND code = @code" +
				          ") iq GROUP BY variable, translation";

				var data = db.GetDatatable(sql, DBParameters);

				//Get Data from languagefile
				var l = new LanguageFile();
				var result = l.GetLanguageKeyCollectionFromJSON("keto", locale_id);

				//Override Data in language file with data from database by variable
				foreach (DataRow row in data.Rows) {
					var variable = (string) row["variable"];

					if (result.ContainsKey(variable)) {
						result[variable] = Conv.ToStr(row["translation"]);
					} else {
						result.Add(variable, Conv.ToStr(row["translation"]));
					}
				}

				//Save to cache
				Cache.Set(instance, "languages_" + locale_id, result);
				return result;
			}

			//Cache contains data
			return (Dictionary<string, string>) Cache.Get(instance, "languages_" + locale_id);
		}

		public void insertOrUpdateProcessTranslation(string process, string oldVariable, string variable) {
			DBParameters.Clear();
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@process", Conv.IfNullThenDbNull(process));
			SetDBParam(SqlDbType.NVarChar, "@code", locale_id);
			SetDBParam(SqlDbType.NVarChar, "@langVariable", variable);
			SetDBParam(SqlDbType.NVarChar, "@oldLangVariable", oldVariable);

			if ((int) db.ExecuteScalar(
				"SELECT count(variable) from process_translations WHERE instance = @instance AND code = @code AND variable = @oldLangVariable",
				DBParameters) == 0) {
				db.ExecuteInsertQuery(
					"INSERT INTO process_translations (instance, process, variable, code, translation) VALUES(@instance, @process, @oldLangVariable, @code, @langVariable)",
					DBParameters);
			} else {
				db.ExecuteUpdateQuery(
					string.IsNullOrEmpty(process)
						? "UPDATE process_translations SET translation = @langVariable WHERE instance = @instance AND code = @code AND variable = @oldLangVariable"
						: "UPDATE process_translations SET translation = @langVariable WHERE instance = @instance AND process = @process AND code = @code AND variable = @oldLangVariable",
					DBParameters);
			}

			//Invalidate cache
			Cache.Remove(instance, "translation_" + oldVariable);
			Cache.Remove(instance, "languages_" + locale_id);
			Cache.Remove(instance, "all_languages");
		}

		public void insertOrUpdateProcessTranslation(string process, string oldVariable, string variable,
			string locale) {
			DBParameters.Clear();
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@process", Conv.IfNullThenDbNull(process));
			SetDBParam(SqlDbType.NVarChar, "@code", locale);
			SetDBParam(SqlDbType.NVarChar, "@langVariable", variable);
			SetDBParam(SqlDbType.NVarChar, "@oldLangVariable", oldVariable);

			if ((int) db.ExecuteScalar(
				"SELECT count(variable) from process_translations WHERE instance = @instance AND code = @code AND variable = @oldLangVariable",
				DBParameters) == 0) {
				db.ExecuteInsertQuery(
					"INSERT INTO process_translations (instance, process, variable, code, translation) VALUES(@instance, @process, @oldLangVariable, @code, @langVariable)",
					DBParameters);
			} else {
				db.ExecuteUpdateQuery(
					string.IsNullOrEmpty(process)
						? "UPDATE process_translations SET translation = @langVariable WHERE instance = @instance AND code = @code AND variable = @oldLangVariable"
						: "UPDATE process_translations SET translation = @langVariable WHERE instance = @instance AND process = @process AND code = @code AND variable = @oldLangVariable",
					DBParameters);
			}

			//Invalidate cache
			Cache.Remove(instance, "translation_" + oldVariable);
			Cache.Remove(instance, "languages_" + locale);

			Cache.Remove(instance, "all_languages");
		}

		public void insertOrUpdateInstanceTranslation(string process, string oldVariable, string variable) {
			insertOrUpdateProcessTranslation(process, oldVariable, variable);
		}

		public void InsertInstanceTranslation(Dictionary<string, string> translations, string languageVariable) {
			var cb = new ConnectionBase(instance, authenticatedSession);
			cb.SetDBParam(SqlDbType.NVarChar, "@langVariable", languageVariable);
			cb.SetDBParam(SqlDbType.NVarChar, "@instance", instance);

			foreach(KeyValuePair<string, string> pair in translations) {
				cb.SetDBParam(SqlDbType.NVarChar, "@code", pair.Key);
				cb.SetDBParam(SqlDbType.NVarChar, "@translation", pair.Value);
				cb.db.ExecuteInsertQuery(
					"INSERT INTO instance_translations (instance, code,variable,translation) VALUES(@instance, @code, @langVariable, @translation)",
					cb.DBParameters);
			}
			Cache.Remove(instance, "instance_translations");
		}

		public void SaveInstanceTranslation(Dictionary<string, string> translations, string languageVariable) {
			var cb = new ConnectionBase(instance, authenticatedSession);
			cb.SetDBParam(SqlDbType.NVarChar, "@langVariable", languageVariable);
			cb.SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			foreach(KeyValuePair<string, string> pair in translations) {
				cb.SetDBParam(SqlDbType.NVarChar, "@code", pair.Key);
				cb.SetDBParam(SqlDbType.NVarChar, "@translation", pair.Value);
				cb.db.ExecuteInsertQuery(
					"UPDATE instance_translations SET translation = @translation WHERE code = @code AND variable = @langVariable",
					cb.DBParameters);
			}
			Cache.Remove(instance, "instance_translations");
		//	Cache.Remove(instance, "instance_translation_" + languageVariable);

		}

		public Dictionary<string, string> GetInstanceTranslationDictionary(string variable) {
		//	if(Cache.Get(instance, "instance_translation_" + variable) == null)
			//{
				var result = new Dictionary<string, string>();
				var cb = new ConnectionBase(instance, authenticatedSession);
				cb.SetDBParam(SqlDbType.NVarChar, "@variable", variable);
				foreach (DataRow r in cb.db.GetDatatable("SELECT * FROM instance_translations WHERE [variable] = @variable", cb.DBParameters).Rows) {
					result.Add(Conv.ToStr(r["code"]), Conv.ToStr(r["translation"]));
				}
				//Cache.Set(instance, "instance_translation_" + variable, result);
				return result;
		//	}
		//	return (Dictionary<string,string>)Cache.Get(instance, "instance_translation_" + variable);
		}

		public string GetItemColumnTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("IC_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetGroupTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("GROUP_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetTabTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("TAB_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetContainerTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("CONT_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetPortfolioTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("PORT_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetListTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("LIST_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetInstanceMenuTranslationVariable(string variable) {
			var var = Modi.RemoveSpecialCharacters("INSTANCEMENU_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetConditionTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("CONDITION_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetInstanceNotificationTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("NOTIFICATION_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetInstanceNotificationTitleTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("N_TITLE_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetInstanceNotificationBodyTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("N_BODY_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetNotificationConditionTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("N_CONDITION_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetActionTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("ACTION_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetActionDialogTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("ACTIONDG_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetCalendarTranslationVariable(string variable) {
			var var = Modi.RemoveSpecialCharacters("CALENDAR_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			return var;
		}

		public string GetDashboardTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("DASHBOAD_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetHelpTextVariable(string variable, string indentifier) {
			var var = Modi.RemoveSpecialCharacters("HELPER" + "_" + indentifier + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');
			SetDBParameter("@variable", var);
			var varExists =
				Conv.ToInt((db.ExecuteScalar(
					"SELECT COUNT (variable) FROM instance_translations WHERE variable = @variable",
					DBParameters)));
			Random rnd = new Random();
			if (varExists > 0)
				var = var + Conv.ToStr(rnd.Next(1, 99)); //allu et voi olla tosissas, tää pitää kyllä muuttaa

			return var;
		}

		public string GetPortfolioDialogTranslationVariable(string process, string variable) {
			var var = Modi.RemoveSpecialCharacters("PORTDG_" + process + "_" + variable);
			var = Modi.Left(var, 50).ToLower().Replace(' ', '_');

			return var;
		}

		public string GetTranslationVariable(string process, string variable, string type) {
			string var;

			if (process.Length > 0) {
				var = Modi.RemoveSpecialCharacters(type + "_" + process + "_" + variable);
			} else {
				var = Modi.RemoveSpecialCharacters(type + "_" + variable);
			}

			var = Modi.Left(var, 75).ToLower().Replace(' ', '_');

			var counter = 1;

			var t = GetTranslations();
			while (t.ContainsKey(var)) {
				if (counter > 100) {
					throw new StackOverflowException("Max. variable recursion reached.");
				}

				var cStr = "_" + counter;
				if ((var + cStr).Length > 50) {
					var = Modi.Left(var, 50 - cStr.Length) + cStr;
				} else {
					var = var + cStr;
				}

				counter++;
			}

			return var;
		}

		public string GetTranslation(string variable, bool replaceTags = false) {
			var t = GetTranslations();
			if (t.ContainsKey(variable)) {
				if (replaceTags) {
					return ReplaceTags(t[variable]);
				}

				return t[variable];
			}

			return variable;
		}

		public Dictionary<string, string> GetTranslations(string variable, bool replaceTags = false) {
			var result = new Dictionary<string, string>();
			if (variable != null) {
				DataTable data;
				if (Cache.Get(instance, "all_languages") != null) {
					data = (DataTable) Cache.Get(instance, "all_languages");
				} else {
					SetDBParam(SqlDbType.NVarChar, "variable", variable);
					data = db.GetDatatable(
						"SELECT variable, translation, code FROM process_translations WHERE variable <> '' AND @instance = instance  ",
						DBParameters);

					Cache.Set(instance, "all_languages", data);
				}

				var d2 = data.Select("variable = '" + Conv.Escape(variable) + "'");
				if (d2.Length > 0) {
					foreach (var row in d2) {
						if (replaceTags) {
							result.Add((string) row["code"], ReplaceTags(Conv.ToStr(row["translation"])));
						} else {
							result.Add((string) row["code"], Conv.ToStr(row["translation"]));
						}
					}
				}
			}

			return result;
		}


		public Dictionary<string, string> GetInstanceTranslations(string variable, bool replaceTags = false) {
			var result = new Dictionary<string, string>();
			
			var data = (DataTable) Cache.Get(instance, "instance_translations");
			if (data == null) {
				SetDBParam(SqlDbType.NVarChar, "variable", variable);
				data = db.GetDatatable(
					"SELECT variable, translation, code FROM instance_translations WHERE variable <> '' AND @instance = instance",
					DBParameters);
				Cache.Set(instance, "instance_translations", data);
			}

			var d2 = data.Select("variable = '" + Conv.Escape(variable) + "'");
			if (d2.Length <= 0) return result;
			
			foreach (var row in d2) {
				result.Add((string) row["code"],
					replaceTags ? ReplaceTags(Conv.ToStr(row["translation"])) : Conv.ToStr(row["translation"]));
			}
			
			return result;
		}

		private string ReplaceTags(string translation) {
			if (translation.Contains("[") && translation.Contains("[")) {
				var tags = translation.Split('[', ']');
				foreach (var tag in tags) {
					if (tag.ToUpper().StartsWith("YEAR")) {
						translation = translation.Replace("[" + tag + "]",
							"" + (DateTime.Now.Year + parseEndOfTag(tag.Substring(4))));
					} else if (tag.ToUpper().StartsWith("QUARTER")) {
						translation = translation.Replace("[" + tag + "]",
							"" + ((DateTime.Now.Month + 2) / 3 + parseEndOfTag(tag.Substring(7))));
					} else if (tag.ToUpper().StartsWith("MONTH")) {
						translation = translation.Replace("[" + tag + "]",
							"" + (DateTime.Now.Month + parseEndOfTag(tag.Substring(5))));
					} else if (tag.ToUpper().StartsWith("WEEK")) {
						var dfi = DateTimeFormatInfo.CurrentInfo;
						var cal = dfi.Calendar;

						translation = translation.Replace("[" + tag + "]",
							"" + (cal.GetWeekOfYear(DateTime.Now, dfi.CalendarWeekRule, dfi.FirstDayOfWeek) +
							      parseEndOfTag(tag.Substring(4))));
					}
				}
			}

			return translation;
		}

		private int parseEndOfTag(string tagEnd) {
			if (tagEnd.Length > 0) {
				var tagVal = Conv.ToInt(tagEnd.Substring(1));
				if (tagEnd.Substring(0, 1) == "-") {
					return tagVal * -1;
				}

				if (tagEnd.Substring(0, 1) == "+") {
					return tagVal;
				}
			}

			return 0;
		}
	}

	public class Translation : ConnectionBase {
		public Translation(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) { }

		public List<TranslationModel> GetTranslation(string variable) {
			var result = new List<TranslationModel>();

			//Get Data from Database
			SetDBParam(SqlDbType.NVarChar, "variable", variable);
			var data = db.GetDatatable(
				"SELECT il.code, pt.variable, pt.translation, il.icon_url FROM instance_languages il LEFT OUTER JOIN process_translations pt ON il.instance = pt.instance AND il.code = pt.code AND variable = @variable WHERE il.instance = @instance ",
				DBParameters);

			//Override Data in language file with data from database by variable
			foreach (DataRow row in data.Rows) {
				result.Add(new TranslationModel(row));
			}

			return result;
		}

		public void Save(string process, string variable, TranslationModel[] translationsModel) {
			var cacheKey = "translation_" + variable;
			Cache.Remove(instance, cacheKey);

			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.NVarChar, "@variable", variable);

			foreach (var t in translationsModel) {
				if (t.TranslatedText != null) {
					SetDBParam(SqlDbType.NVarChar, "@code", t.Code);
					SetDBParam(SqlDbType.NVarChar, "@translation", t.TranslatedText);

					if (t.Variable != null) {
						db.ExecuteUpdateQuery(
							"UPDATE process_translations SET translation = @translation WHERE instance = @instance AND process = @process AND code = @code AND variable = @variable",
							DBParameters);
					} else {
						db.ExecuteInsertQuery(
							"INSERT INTO process_translations (instance, process, code, variable, translation) VALUES(@instance, @process, @code, @variable, @translation)",
							DBParameters);
					}

					Cache.Set(instance, "languages_" + t.Code, null);
				}
			}
		}
	}
}