﻿using System;
using System.Data;
using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.@base;

namespace Keto5.x.platform.server.core {
	public class FlexTimeTrackingModel : ConnectionBase {
		private readonly AuthenticatedSession session;
		private Translations t;
		private bool admin_rights = false;

		public enum GetDataModes {
			Normal = 0,
			Report = 1,
			Corrections = 2
		}

		public FlexTimeTrackingModel(string instance, AuthenticatedSession session) : base(instance, session) {
			this.session = session;
			t = new Translations(instance, session, session.locale_id);

			var pagesDb = new PagesDb(instance, session);
			var pageParams = new Dictionary<string, object>();
			var pageStates = pagesDb.GetStates("FlexTimeTrackingReport", pageParams);
			if (pageStates.Contains("admin")) {
				admin_rights = true;
			}
		}

		public FlexDay GetData(int? user_item_id, DateTime? selected_date, GetDataModes mode = GetDataModes.Normal, int flex_row_item_id = 0) {
			FlexDay flex_day = new FlexDay();

			if (user_item_id == null) {
				flex_day.user_item_id = session.item_id;
			}
			else {
				flex_day.user_item_id = (int)user_item_id;
			}

			if (admin_rights || flex_day.user_item_id == session.item_id) {
				if (selected_date == null) {
					flex_day.selected_date = DateTime.UtcNow.Date;
				}
				else {
					flex_day.selected_date = ((DateTime)selected_date).Date;
				}

				flex_day.formated_date = t.GetTranslation("CAL_DAYS_SHORT_" + (int)flex_day.selected_date.DayOfWeek) + " " +  flex_day.selected_date.ToString("d.M.yyy");

				DateTime? reporting_start_date = Conv.ToDateTime(db.ExecuteScalar("SELECT MIN(start_date) FROM _" + instance + "_flex_time_tracking WHERE type IN (0, 1) AND special IN (0, 1, 2) AND draft = 0", DBParameters));

				DateTime week_start;
				if ((int)flex_day.selected_date.DayOfWeek < session.first_day_of_week) {
					week_start = flex_day.selected_date.AddDays(session.first_day_of_week - (int)flex_day.selected_date.DayOfWeek - 7);
				}
				else if ((int)flex_day.selected_date.DayOfWeek > session.first_day_of_week) {
					week_start = flex_day.selected_date.AddDays(session.first_day_of_week - (int)flex_day.selected_date.DayOfWeek);
				}
				else {
					week_start = flex_day.selected_date;
				}

				double lunch_break_sum = 0;
				double balance = 0;

				SetDBParam(SqlDbType.Int, "user_item_id", flex_day.user_item_id);
				SetDBParam(SqlDbType.DateTime, "start_date", week_start);
				SetDBParam(SqlDbType.DateTime, "end_date", week_start.AddDays(6));
				string sql = "SELECT event_date, notwork, holiday FROM GetCombinedCalendarForUser(@user_item_id, @start_date, @end_date)";
				DataTable not_works = db.GetDatatable(sql, DBParameters);

				if (mode.In(GetDataModes.Normal, GetDataModes.Report)) {
					sql = "SELECT calendar_id, parent_calendar_id FROM calendars WHERE item_id = @user_item_id";
					DataRow user_calendar = db.GetDataRow(sql, DBParameters);
					SetDBParam(SqlDbType.Int, "base_calendar_id", Conv.ToInt(user_calendar["parent_calendar_id"]));
					SetDBParam(SqlDbType.Int, "user_calendar_id", Conv.ToInt(user_calendar["calendar_id"]));
					if (mode == GetDataModes.Report) {
						flex_day.calendar_events = GetCalendarEvents(flex_day.selected_date);
					}
				}

				SetDBParam(SqlDbType.DateTime, "selected_date_start", flex_day.selected_date);
				SetDBParam(SqlDbType.DateTime, "selected_date_end", flex_day.selected_date.AddDays(1));
				sql = "SELECT * " +
					  "FROM _" + instance + "_flex_time_tracking ";
				if (mode == GetDataModes.Corrections) {
					SetDBParam(SqlDbType.Int, "flex_row_item_id", flex_row_item_id);
					sql += "WHERE item_id = @flex_row_item_id ";
				}
				else {
					sql += "WHERE type IN (0, 1) ";
				}
				sql += "AND user_item_id = @user_item_id " +
					   "AND start_date < @selected_date_end " +
					   "AND start_date >= @selected_date_start " +
					   "ORDER BY type DESC, start_date ASC";
				foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
					FlexRow flex_row = new FlexRow();
					flex_row.type = (int)row["type"];
					flex_row.flex_row_item_id = (int)row["item_id"];
					if (mode == GetDataModes.Normal || Conv.ToInt(row["type"]) != 1) {
						flex_row.start.hours = ((DateTime)row["start_date"]).Hour.ToString();
						flex_row.start.minutes = LeadingZero(((DateTime)row["start_date"]).Minute);
						if (Conv.ToDateTime(row["end_date"]) != null) {
							if (((DateTime)row["start_date"]).Date == ((DateTime)row["end_date"]).Date) {
								flex_row.end.hours = ((DateTime)row["end_date"]).Hour.ToString();
							}
							else {
								flex_row.end.hours = "24";
							}
							flex_row.end.minutes = LeadingZero(((DateTime)row["end_date"]).Minute);
						}
						flex_row.lunch.hours = Math.Floor((double)row["lunch_break"]).ToString();
						flex_row.lunch.minutes = Conv.ToInt(((double)row["lunch_break"] - Math.Floor((double)row["lunch_break"])) * 60).ToString();
					}

					double work_hours = Conv.ToDouble(row["work_hours"]);
					bool work_hours_negarive_h = false;
					bool work_hours_negative_m = false;
					if (work_hours <= -1) {
						work_hours_negarive_h = true;
						work_hours = -work_hours;
					}
					else if (work_hours < 0) {
						work_hours_negative_m = true;
						work_hours = -work_hours;
					}
					flex_row.work.hours = Math.Floor(work_hours).ToString();
					flex_row.work.minutes = Conv.ToInt((work_hours - Math.Floor(work_hours)) * 60).ToString();
					if (work_hours_negarive_h) {
						flex_row.work.hours = "-" + flex_row.work.hours;
					}
					else if (work_hours_negative_m) {
						flex_row.work.minutes = "-" + flex_row.work.minutes;
					}

					flex_row.comment = "";
					if (mode == GetDataModes.Report && Conv.ToInt(row["type"]) == 1) {
						flex_row.comment = t.GetTranslation("FLEX_TIME_TRACKING_CORRECTION_ROW");
					}
					if (Conv.ToStr(row["comment"]).Length > 0) {
						if (flex_row.comment.Length > 0) {
							flex_row.comment += Environment.NewLine;
						}
						flex_row.comment += Conv.ToStr(row["comment"]);
					}

					if (flex_row.comment.Length == 0) {
						flex_row.comment_icon = "chat_bubble";
					}
					else {
						flex_row.comment_icon = "chat";
					}

					flex_row.special = Conv.ToInt(row["special"]);

					flex_day.flex_rows.Add(flex_row);

					lunch_break_sum += (double)row["lunch_break"];

					if (mode == GetDataModes.Normal && Conv.ToInt(row["type"]) == 1 && ((DateTime)row["start_date"]).Date == flex_day.selected_date) {
						balance += Conv.ToDouble(row["work_hours"]);
					}
				}

				if (mode == GetDataModes.Normal) {
					flex_day.special_list = GetSpecialList();
				}

				double daily_hours = 0;
				int not_work = Conv.ToInt(not_works.Compute("SUM(notwork)", "event_date = #" + flex_day.selected_date.ToString("yyyy-MM-dd") + "#"));
				if (not_work > 0) {
					flex_day.daily_hours.hours = "0";
					flex_day.daily_hours.minutes = "0";
					flex_day.not_work = true;
				}
				else {
					SetDBParam(SqlDbType.Int, "user_id", flex_day.user_item_id);
					SetDBParam(SqlDbType.DateTime, "date", flex_day.selected_date.AddDays(1).AddMilliseconds(-1));
					DataRow calendar_row = db.GetDataRow("SELECT * FROM GetCalendarForUserByDate(@user_id, @date)", DBParameters);
					if (calendar_row != null) {
						daily_hours = Conv.ToDouble(calendar_row["timesheet_hours"]);
					}
					flex_day.daily_hours.hours = Math.Floor(daily_hours).ToString();
					flex_day.daily_hours.minutes = Conv.ToInt((daily_hours - Math.Floor(daily_hours)) * 60).ToString();
				}

				if (mode == GetDataModes.Normal || (mode == GetDataModes.Report && flex_day.flex_rows.Count == 0)) {
					FlexRow new_row = new FlexRow();
					if (mode == GetDataModes.Normal) {
						if (lunch_break_sum < 0.5 && not_work == 0) {
							new_row.lunch.hours = Math.Floor(0.5 - lunch_break_sum).ToString();
							new_row.lunch.minutes = Conv.ToInt((0.5 - lunch_break_sum - Math.Floor(0.5 - lunch_break_sum)) * 60).ToString();
						}
						else {
							new_row.lunch.hours = "0";
							new_row.lunch.minutes = "0";
						}
					}
					new_row.comment = "";
					new_row.comment_icon = "chat_bubble";
					new_row.row_ok = false;
					if (mode == GetDataModes.Report) {
						new_row.special = 0;
					}
					else {
						new_row.special = 1;
					}
					new_row.new_row = true;
					flex_day.flex_rows.Add(new_row);
				}

				if (mode == GetDataModes.Normal) {
					SetDBParam(SqlDbType.DateTime, "start_date", week_start);
					SetDBParam(SqlDbType.DateTime, "end_date", week_start.AddDays(7));
					sql = "SELECT CAST(YEAR(start_date) AS nvarchar) + '-' + CAST(MONTH(start_date) AS nvarchar) + '-' + CAST(DAY(start_date) AS nvarchar) AS date" +
						  ", SUM(CASE WHEN (type = 0 AND special = 1) OR type = 1 THEN work_hours ELSE 0 END) AS work_hours " +
						  ", MAX(CASE WHEN special = 2 THEN 1 ELSE 0 END) AS sick_leave " +
						  ", MAX(CASE WHEN special = 3 THEN 1 ELSE 0 END) AS balance_free " +
						  ", MAX(CASE WHEN type = 1 THEN 1 ELSE 0 END) AS correction_row " +
						  "FROM _" + instance + "_flex_time_tracking " +
						  "WHERE type IN (0, 1) " +
						  "AND user_item_id = @user_item_id " +
						  "AND start_date >= @start_date " +
						  "AND start_date < @end_date " +
						  "AND draft = 0 " +
						  "GROUP BY start_date";
					DataTable week_day_hours = db.GetDatatable(sql, DBParameters);

					for (int d = 0; d < 7; d++) {
						DateTime current_day = week_start.AddDays(d);
						Day day = new Day();
						day.date = current_day;
						day.day = Conv.ToStr(current_day.Day + "." + current_day.Month + ".");
						day.week_day = t.GetTranslation("CAL_DAYS_SHORT_" + (int)current_day.DayOfWeek);
						double hours = Conv.ToDouble(week_day_hours.Compute("SUM(work_hours)", "date = '" + current_day.Year + "-" + current_day.Month + "-" + current_day.Day + "'"));
						day.day_hours.hours = Math.Floor(hours).ToString();
						day.day_hours.minutes = Conv.ToInt((hours - Math.Floor(hours)) * 60).ToString();

						if (current_day == flex_day.selected_date) {
							day.selected = true;
							flex_day.selected_index = d;
						}
						if (current_day == DateTime.UtcNow.Date) {
							day.today = true;
						}
						not_work = Conv.ToInt(not_works.Compute("SUM(notwork)", "event_date = #" + current_day.ToString("yyyy-MM-dd") + "#"));
						if (not_work > 0) {
							day.not_work = true;
						}

						int sick_leave = Conv.ToInt(week_day_hours.Compute("SUM(sick_leave)", "date = '" + current_day.Year + "-" + current_day.Month + "-" + current_day.Day + "'"));
						if (sick_leave == 1) {
							day.sick_leave = true;
						}

						int balance_free = Conv.ToInt(week_day_hours.Compute("SUM(balance_free)", "date = '" + current_day.Year + "-" + current_day.Month + "-" + current_day.Day + "'"));
						if (balance_free == 1) {
							day.balance_free = true;
						}

						int correction_row = Conv.ToInt(week_day_hours.Compute("SUM(correction_row)", "date = '" + current_day.Year + "-" + current_day.Month + "-" + current_day.Day + "'"));
						if (correction_row == 1) {
							day.correction_row = true;
						}

						day.calendar_events = GetCalendarEvents(current_day);

						if (Conv.ToInt(not_works.Compute("SUM(holiday)", "event_date = #" + current_day.ToString("yyyy-MM-dd") + "#")) > 0) {
							day.holiday = true;
						}

						if ((day.sick_leave || day.balance_free || day.correction_row) && (day.day_hours.hours != "0" || day.day_hours.minutes != "0")) flex_day.has_double_info = true;

						flex_day.week.Add(day);
					}
				}

				if (mode.In(GetDataModes.Normal, GetDataModes.Report)) {
					if (mode == GetDataModes.Report) {
						SetDBParam(SqlDbType.DateTime, "selected_date", flex_day.selected_date.AddDays(1));
					}
					else {
						SetDBParam(SqlDbType.DateTime, "selected_date", flex_day.selected_date);
					}
					SetDBParam(SqlDbType.DateTime, "month_start_date", new DateTime(flex_day.selected_date.Year, flex_day.selected_date.Month, 1));
					sql = "SELECT MIN(start_date) AS start_date" +
						  ", SUM(work_hours) AS work_hours " +
						  "FROM _" + instance + "_flex_time_tracking " +
						  "WHERE type IN (0, 1) " +
						  "AND special IN (0, 1, 2) " +
						  "AND user_item_id = @user_item_id " +
						  "AND start_date < @selected_date " +
						  "AND start_date >= @month_start_date " +
						  "AND draft = 0";
					DataRow balance_row = db.GetDataRow(sql, DBParameters);
					DateTime? start_date = new DateTime(flex_day.selected_date.Year, flex_day.selected_date.Month, 1);
					double work_hours = 0;
					if (Conv.ToDateTime(balance_row["start_date"]) != null) {
						if (Conv.ToDateTime(balance_row["start_date"]) == reporting_start_date) {
							start_date = Conv.ToDateTime(balance_row["start_date"]);
						}
						work_hours = Conv.ToDouble(balance_row["work_hours"]);
					}

					int adder = -1;
					if (mode == GetDataModes.Report) {
						adder = 0;
					}

					SetDBParam(SqlDbType.DateTime, "start_date", ((DateTime)start_date).Date);
					SetDBParam(SqlDbType.DateTime, "end_date", flex_day.selected_date.AddDays(adder).Date);
					sql = "SELECT event_date, notwork FROM GetCombinedCalendarForUser(@user_item_id, @start_date, @end_date)";
					not_works = db.GetDatatable(sql, DBParameters);

					double work_load = 0;
					DateTime current_date = ((DateTime)start_date).Date;
					if (reporting_start_date == null) {
						current_date = DateTime.UtcNow.Date;
					}
					else if (current_date.Date < ((DateTime)reporting_start_date).Date) {
						current_date = (DateTime)reporting_start_date;
					}
					while (current_date <= flex_day.selected_date.AddDays(adder).Date) {
						not_work = Conv.ToInt(not_works.Compute("SUM(notwork)", "event_date = #" + current_date.ToString("yyyy-MM-dd") + "#"));
						if (not_work == 0) {
							SetDBParam(SqlDbType.DateTime, "current_date", current_date);
							sql = "SELECT timesheet_hours FROM GetCalendarForUserByDate(@user_item_id, @current_date)";
							work_load += Conv.ToDouble(db.ExecuteScalar(sql, DBParameters));
						}
						current_date = current_date.AddDays(1);
					}

					balance += work_hours - work_load;

					sql = "SELECT SUM(work_hours) " +
						  "FROM _" + instance + "_flex_time_tracking " +
						  "WHERE type = 2 " +
						  "AND user_item_id = @user_item_id " +
						  "AND start_date < @month_start_date " +
						  "AND draft = 0";
					balance += Conv.ToDouble(db.ExecuteScalar(sql, DBParameters));
					bool balance_negative_h = false;
					bool balance_negative_m = false;
					if (balance <= -1) {
						balance_negative_h = true;
						balance = -balance;
					}
					else if (balance < 0) {
						balance_negative_m = true;
						balance = -balance;
					}
					flex_day.balance.hours = Math.Floor(balance).ToString();
					flex_day.balance.minutes = Conv.ToInt((balance - Math.Floor(balance)) * 60).ToString();
					if (flex_day.balance.minutes == "60") {
						flex_day.balance.hours = Conv.ToStr(Conv.ToInt(flex_day.balance.hours) + 1);
						flex_day.balance.minutes = "0";
					}
					if (balance_negative_h) {
						flex_day.balance.hours = "-" + flex_day.balance.hours;
					}
					else if (balance_negative_m) {
						flex_day.balance.minutes = "-" + flex_day.balance.minutes;
					}
				}
			}

			return flex_day;
		}

		private string GetCalendarEvents(DateTime event_date) {
			string result = "";
			SetDBParam(SqlDbType.DateTime, "event_date", event_date);
			string sql = "SELECT [name] FROM (" +
						 " SELECT [name] FROM calendar_events WHERE calendar_id IN (@base_calendar_id, @user_calendar_id) AND event_start <= @event_date AND event_end >= @event_date" +
						 " UNION ALL" +
						 " SELECT [name] FROM calendar_static_holidays WHERE calendar_id = @base_calendar_id AND [month] = month(@event_date) AND[day] = day(@event_date)" +
						 ") AS tmp ORDER BY [name] ASC";
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				if (result.Length > 0) {
					result += Environment.NewLine;
				}
				result += Conv.ToStr(row["name"]);
			}
			return result;
		}

		private string LeadingZero(int num) {
			if (num < 10) {
				return "0" + num;
			}
			return num.ToString();
		}

		public FlexDay AddRow(FlexDay flex_day, GetDataModes mode) {
			if (admin_rights || flex_day.user_item_id == session.item_id) {
				FlexRow new_row = flex_day.flex_rows[flex_day.flex_rows.Count - 1];
				if (new_row.new_row) {
					int draft = 0;
					if (!new_row.row_ok || new_row.start_conflict || new_row.end_conflict || new_row.order_conflict || new_row.lunch_conflict) {
						draft = 1;
					}

					ItemBase ib = new ItemBase(instance, "flex_time_tracking", session);
					Dictionary<string, object> values = new Dictionary<string, object>();
					values.Add("type", new_row.type);
					values.Add("user_item_id", flex_day.user_item_id);
					values.Add("flex_user", new List<int> { flex_day.user_item_id });
					values.Add("start_date", new DateTime(flex_day.selected_date.Year, flex_day.selected_date.Month, flex_day.selected_date.Day, Conv.ToInt(new_row.start.hours), Conv.ToInt(new_row.start.minutes), 0));
					if (draft == 1 && (new_row.end.hours == null || new_row.end.minutes == null || new_row.end.hours == "" || new_row.end.minutes == "")) {
						values.Add("end_date", null);
					}
					else {
						if (Conv.ToInt(new_row.end.hours) == 24) {
							values.Add("end_date", new DateTime(flex_day.selected_date.AddDays(1).Year, flex_day.selected_date.AddDays(1).Month, flex_day.selected_date.AddDays(1).Day, 0, 0, 0));
						}
						else {
							values.Add("end_date", new DateTime(flex_day.selected_date.Year, flex_day.selected_date.Month, flex_day.selected_date.Day, Conv.ToInt(new_row.end.hours), Conv.ToInt(new_row.end.minutes), 0));
						}
					}
					values.Add("lunch_break", Conv.ToDouble(new_row.lunch.hours) + (Conv.ToDouble(new_row.lunch.minutes) / 60));
					values.Add("work_hours", Conv.ToDouble(new_row.work.hours) + (Conv.ToDouble(new_row.work.minutes) / 60));
					values.Add("special", Conv.ToInt(new_row.special));
					values.Add("comment", new_row.comment);
					values.Add("draft", draft);
					ib.AddItem(values);

					SetDBParam(SqlDbType.Int, "user_item_id", flex_day.user_item_id);
					SetDBParam(SqlDbType.DateTime, "date", flex_day.selected_date);
					db.ExecuteStoredProcedure("EXEC dbo.flex_time_CalculateMonthBalance @instance, @user_item_id, @date", DBParameters);
				}
			}
			return GetData(flex_day.user_item_id, flex_day.selected_date, mode);
		}

		public void Save(FlexDay flex_day) {
			if (admin_rights || flex_day.user_item_id == session.item_id) {
				ItemBase ib = new ItemBase(instance, "flex_time_tracking", session);
				foreach (FlexRow flex_row in flex_day.flex_rows) {
					int draft = 0;
					if (!flex_row.row_ok || flex_row.start_conflict || flex_row.end_conflict || flex_row.order_conflict || flex_row.lunch_conflict) {
						draft = 1;
					}

					//if (!flex_row.new_row && flex_row.type == 0 && flex_row.row_ok && !flex_row.start_conflict && !flex_row.end_conflict && !flex_row.order_conflict && !flex_row.lunch_conflict) {
					if (!flex_row.new_row && flex_row.type == 0) {
							Dictionary<string, object> values = new Dictionary<string, object>();
						values.Add("start_date", new DateTime(flex_day.selected_date.Year, flex_day.selected_date.Month, flex_day.selected_date.Day, Conv.ToInt(flex_row.start.hours), Conv.ToInt(flex_row.start.minutes), 0));
						if (draft == 1 && (flex_row.end.hours == null || flex_row.end.minutes == null || flex_row.end.hours == "" || flex_row.end.minutes == "")) {
							values.Add("end_date", null);
						}
						else {
							if (Conv.ToInt(flex_row.end.hours) == 24) {
								values.Add("end_date", new DateTime(flex_day.selected_date.AddDays(1).Year, flex_day.selected_date.AddDays(1).Month, flex_day.selected_date.AddDays(1).Day, 0, 0, 0));
							}
							else {
								values.Add("end_date", new DateTime(flex_day.selected_date.Year, flex_day.selected_date.Month, flex_day.selected_date.Day, Conv.ToInt(flex_row.end.hours), Conv.ToInt(flex_row.end.minutes), 0));
							}
						}
						values.Add("lunch_break", Conv.ToDouble(flex_row.lunch.hours) + (Conv.ToDouble(flex_row.lunch.minutes) / 60));
						values.Add("work_hours", Conv.ToDouble(flex_row.work.hours) + (Conv.ToDouble(flex_row.work.minutes) / 60));
						values.Add("special", Conv.ToInt(flex_row.special));
						values.Add("comment", flex_row.comment);
						values.Add("draft", draft);
						ib.SaveItem(values, flex_row.flex_row_item_id, true);

						SetDBParam(SqlDbType.Int, "user_item_id", flex_day.user_item_id);
						SetDBParam(SqlDbType.DateTime, "date", flex_day.selected_date);
						db.ExecuteStoredProcedure("EXEC dbo.flex_time_CalculateMonthBalance @instance, @user_item_id, @date", DBParameters);
					}
				}
			}
		}

		public void DeleteRow(int flex_row_item_id) {
			ItemBase ib = new ItemBase(instance, "flex_time_tracking", session);
			Dictionary<string, object> row = ib.GetItem(flex_row_item_id);
			if (admin_rights || Conv.ToInt(row["user_item_id"]) == session.item_id) {
				ib.Delete(flex_row_item_id);

				SetDBParam(SqlDbType.Int, "user_item_id", Conv.ToInt(row["user_item_id"]));
				SetDBParam(SqlDbType.DateTime, "date", (DateTime)row["start_date"]);
				db.ExecuteStoredProcedure("EXEC dbo.flex_time_CalculateMonthBalance @instance, @user_item_id, @date", DBParameters);
			}
		}

		public FlexReport GetReport(int? user_item_id, int? year, int? month) {
			if (user_item_id == null) {
				user_item_id = session.item_id;
			}
			if (year == null) {
				year = DateTime.UtcNow.Date.Year;
			}
			if (month == null) {
				month = DateTime.UtcNow.Date.Month;
			}

			if (!admin_rights && user_item_id != session.item_id) {
				user_item_id = session.item_id;
			}

			FlexReport flex_report = new FlexReport();
			flex_report.user_item_id = new List<int>() { (int)user_item_id };
			flex_report.year = (int)year;
			flex_report.month = (int)month;

			int min_year = Conv.ToInt(db.ExecuteScalar("SELECT MIN(YEAR(start_date)) FROM _" + instance + "_flex_time_tracking WHERE type IN (0, 1) AND special IN (0, 1, 2) AND draft = 0", DBParameters));
			if (min_year == 0) {
				min_year = DateTime.UtcNow.Date.Year;
			}
			if (flex_report.year < min_year) {
				min_year = flex_report.year;
			}

			int max_year = DateTime.UtcNow.Date.Year;
			if (flex_report.year > max_year) {
				max_year = flex_report.year;
			}

			for (int y = max_year; y >= min_year; y--) {
				Dictionary<string, object> year_item = new Dictionary<string, object>();
				year_item.Add("value", y);
				year_item.Add("name", y);
				flex_report.year_list.Add(year_item);
			}

			for (int m = 1; m <= 12; m++) {
				Dictionary<string, object> month_item = new Dictionary<string, object>();
				month_item.Add("value", m);
				month_item.Add("name", t.GetTranslation("CAL_MONTHS_LONG_" + (m - 1)));
				flex_report.month_list.Add(month_item);
			}

			DateTime date_start = new DateTime(flex_report.year, flex_report.month, 1);
			DateTime date_end = date_start.AddMonths(1).AddDays(-1);

			DateTime date = date_start;
			while (date <= date_end) {
				flex_report.flex_days.Add(GetData(flex_report.user_item_id[0], date, GetDataModes.Report));
				date = date.AddDays(1);
			}

			flex_report.special_list = GetSpecialList(true);

			return flex_report;
		}

		private List<Dictionary<string, object>> GetSpecialList(bool report_mode = false) {
			List<Dictionary<string, object>> special_list = new List<Dictionary<string, object>>();
			Dictionary<string, object> special_list_item = new Dictionary<string, object>();
			if (report_mode) {
				special_list_item.Add("name", "");
				special_list_item.Add("value", 0);
				special_list_item.Add("icon", "");
				special_list.Add(special_list_item);
				special_list_item = new Dictionary<string, object>();
			}
			special_list_item.Add("name", t.GetTranslation("FLEX_TIME_TRACKING_SPECIAL_AT_WORK"));
			special_list_item.Add("value", 1);
			special_list_item.Add("icon", "work");
			special_list.Add(special_list_item);
			special_list_item = new Dictionary<string, object>();
			special_list_item.Add("name", t.GetTranslation("FLEX_TIME_TRACKING_SPECIAL_SICK_LEAVE"));
			special_list_item.Add("value", 2);
			special_list_item.Add("icon", "local_hospital");
			special_list.Add(special_list_item);
			special_list_item = new Dictionary<string, object>();
			special_list_item.Add("name", t.GetTranslation("FLEX_TIME_TRACKING_SPECIAL_BALANCE_FREE"));
			special_list_item.Add("value", 3);
			special_list_item.Add("icon", "restore");
			special_list.Add(special_list_item);
			return special_list;
		}

		public FlexCorrections GetCorrections(int? user_item_id) {
			if (user_item_id == null) {
				user_item_id = session.item_id;
			}

			if (!admin_rights && user_item_id != session.item_id) {
				user_item_id = session.item_id;
			}

			FlexCorrections flex_corrections = new FlexCorrections();
			flex_corrections.user_item_id = new List<int>() { (int)user_item_id };

			SetDBParam(SqlDbType.Int, "user_item_id", user_item_id);
			foreach (DataRow row in db.GetDatatable("SELECT item_id, start_date FROM _" + instance + "_flex_time_tracking WHERE type = 1 AND user_item_id = @user_item_id AND draft = 0 GROUP BY item_id, start_date ORDER BY start_date DESC, item_id DESC", DBParameters).Rows) {
				flex_corrections.flex_days.Add(GetData(user_item_id, Conv.ToDateTime(row["start_date"]), GetDataModes.Corrections, Conv.ToInt(row["item_id"])));
			}

			flex_corrections.new_correction.user_item_id = (int)user_item_id;
			FlexRow flex_row = new FlexRow();
			flex_row.new_row = true;
			flex_row.type = 1;
			flex_row.start.hours = "0";
			flex_row.start.minutes = "0";
			flex_row.end.hours = "24";
			flex_row.end.minutes = "0";
			flex_row.lunch.hours = "0";
			flex_row.lunch.minutes = "0";
			flex_row.special = 0;
			flex_corrections.new_correction.flex_rows.Add(flex_row);

			return flex_corrections;
		}

		[Serializable]
		public class FlexReport {
			public List<int> user_item_id = new List<int>();
			public int year;
			public int month;
			public List<Dictionary<string, object>> year_list = new List<Dictionary<string, object>>();
			public List<Dictionary<string, object>> month_list = new List<Dictionary<string, object>>();
			public List<FlexDay> flex_days = new List<FlexDay>();
			public List<Dictionary<string, object>> special_list;
		}

		[Serializable]
		public class FlexCorrections {
			public List<int> user_item_id = new List<int>();
			public List<FlexDay> flex_days = new List<FlexDay>();
			public FlexDay new_correction = new FlexDay();
		}

		[Serializable]
		public class FlexDay {
			public int user_item_id;
			public DateTime selected_date;
			public string formated_date;
			public List<FlexRow> flex_rows = new List<FlexRow>();
			public Time day_hours = new Time();
			public Time daily_hours = new Time();
			public Time day_sick_leave = new Time();
			public Time day_balance_free = new Time();
			public Time balance = new Time();
			public List<Day> week = new List<Day>();
			public int selected_index;
			public List<Dictionary<string, object>> special_list;
			public bool not_work = false;
			public string calendar_events;
			public bool has_double_info = false;
		}

		[Serializable]
		public class FlexRow {
			public int type;
			public int? flex_row_item_id;
			public Time start = new Time();
			public Time end = new Time();
			public Time lunch = new Time();
			public Time work = new Time();
			public bool row_ok = true;
			public bool start_conflict = false;
			public bool end_conflict = false;
			public bool order_conflict = false;
			public bool lunch_conflict = false;
			public string comment;
			public string comment_icon;
			public bool show_comment = false;
			public int special;
			public bool new_row = false;
		}

		[Serializable]
		public class Time {
			public string hours;
			public string minutes;
		}

		[Serializable]
		public class Day {
			public DateTime date;
			public string week_day;
			public string day;
			public Time day_hours = new Time();
			public bool today = false;
			public bool not_work = false;
			public bool selected = false;
			public bool sick_leave = false;
			public bool balance_free = false;
			public bool correction_row = false;
			public string calendar_events;
			public bool holiday = false;
		}
	}
}