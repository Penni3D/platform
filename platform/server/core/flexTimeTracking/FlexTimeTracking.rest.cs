﻿using System;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/time/flextimetracking")]

	public class FlextimetrackingRest : RestBase {
		[HttpGet("{user_item_id}/{selected_date}")]
		public FlexTimeTrackingModel.FlexDay GetData(int? user_item_id, DateTime? selected_date) {
			var model = new FlexTimeTrackingModel(instance, currentSession);
			return model.GetData(user_item_id, selected_date);
		}

		[HttpPost("{mode}")]
		public FlexTimeTrackingModel.FlexDay AddRow(FlexTimeTrackingModel.GetDataModes mode, [FromBody]FlexTimeTrackingModel.FlexDay flex_day) {
			var model = new FlexTimeTrackingModel(instance, currentSession);
			return model.AddRow(flex_day, mode);
		}

		[HttpPut("")]
		public void Save([FromBody]FlexTimeTrackingModel.FlexDay flex_day) {
			var model = new FlexTimeTrackingModel(instance, currentSession);
			model.Save(flex_day);
		}

		[HttpDelete("{flex_row_item_id}")]
		public void DeleteRow(int flex_row_item_id) {
			var model = new FlexTimeTrackingModel(instance, currentSession);
			model.DeleteRow(flex_row_item_id);
		}

		[HttpGet("report/{user_item_id}/{year}/{month}")]
		public FlexTimeTrackingModel.FlexReport GetReport(int? user_item_id, int? year, int? month) {
			var model = new FlexTimeTrackingModel(instance, currentSession);
			return model.GetReport(user_item_id, year, month);
		}

		[HttpGet("corrections/{user_item_id}")]
		public FlexTimeTrackingModel.FlexCorrections GetCorrections(int? user_item_id) {
			var model = new FlexTimeTrackingModel(instance, currentSession);
			return model.GetCorrections(user_item_id);
		}
	}
}