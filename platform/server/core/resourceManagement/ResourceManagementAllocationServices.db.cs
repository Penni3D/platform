using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core.resourceManagement {
	public class AllocationService : ResourceManagementCommonService {
		public AllocationService(string instance, AuthenticatedSession session,
			IResourceManagementRepository repository) :
			base(instance, session,
				repository) { }

		public Dictionary<string, object> GetAllocation(int id) {
			var item = _resourceManagementRepository.GetItem(id);
			SetDBParam(SqlDbType.Int, "@allocationId", id);
			var sqlForDurations = "SELECT * FROM " + AllocationDurationTableName + " " +
			                      "WHERE allocation_item_id = @allocationId";
			var listOfDurations = new List<object>();
			foreach (DataRow row in db.GetDatatable(sqlForDurations, DBParameters).Rows) {
				listOfDurations.Add(Conv.DataRowToDictionary(row));
			}

			item.Add("durations", listOfDurations);

			var sqlForComments = "SELECT * FROM allocation_comments " +
			                     "WHERE allocation_item_id = @allocationId";
			var listOfComments = new List<object>();
			foreach (DataRow row in db.GetDatatable(sqlForComments, DBParameters).Rows) {
				listOfComments.Add(Conv.DataRowToDictionary(row));
			}

			item.Add("comments", listOfComments);
			return item;
		}

		public void InsertAllocationDurations(int allocationItemId, DateTime startDate, DateTime endDate,
			double dailyEffort, int userId, bool updateTask = true) {
			var durations = GetListOfAllocationDurations(allocationItemId, startDate, endDate, dailyEffort, userId);
			UpdateAllocationDurations(durations, updateTask);
		}

		public AllocationBase CreateAllocationForSpecs(
			EditAllocationModel allocationModel,
			DateTime fromDate,
			DateTime toDate,
			double dailyEffort,
			int userId) {
			var durations = GetListOfDateAndDurations(fromDate, toDate, dailyEffort, allocationModel.resource_item_id);
			allocationModel.Durations = durations;
			return CreateAllocationRequest(allocationModel, userId);
		}

		private string GetCalendarSql(int resourceOrCompentencyId) {
			var p = new Processes(instance, AuthenticatedSession);
			var process = p.GetProcessByItemId(resourceOrCompentencyId);
			return process == "user"
				? "SELECT * FROM GetCombinedCalendarForUser(@userId, @startDate, @endDate) WHERE dayoff = 1 "
				: "SELECT * FROM GetCombinedCalendar((SELECT TOP 1 calendar_id FROM calendars WHERE is_default = 1),0,@startdate, @enddate) WHERE dayoff = 1 ";
		}

		private List<AllocationDuration> GetListOfAllocationDurations(int allocationItemId, DateTime fromDate,
			DateTime to,
			double dailyEffort, int userId) {
			var dateCursor = fromDate;
			var durations = new List<AllocationDuration>();

			SetDBParam(SqlDbType.Int, "@userId", userId);
			SetDBParam(SqlDbType.DateTime, "@startDate", fromDate);
			SetDBParam(SqlDbType.DateTime, "@endDate", to);

			var totalDays = (to - fromDate).TotalDays + 1;
			var totalEffort = totalDays * dailyEffort;
			var userCal = db.GetDatatable(GetCalendarSql(userId), DBParameters);

			totalDays -= userCal.Rows.Count;

			var newDailyEffort = totalEffort / totalDays;

			while (dateCursor.Date <= to.Date) {
				var rows = userCal.Select("event_date = '" + dateCursor.ToLongDateString() + "'");
				if (rows.Length == 0) {
					var duration = GetAllocationDuration(allocationItemId, newDailyEffort, dateCursor);
					durations.Add(duration);
				}

				dateCursor = dateCursor.AddDays(1);
			}

			return durations;
		}

		private List<DateAndDuration> GetListOfDateAndDurations(DateTime fromDate, DateTime to, double dailyEffort,
			int userId) {
			var dateCursor = fromDate;
			var durations = new List<DateAndDuration>();

			SetDBParam(SqlDbType.Int, "@userId", userId);
			SetDBParam(SqlDbType.DateTime, "@startDate", fromDate);
			SetDBParam(SqlDbType.DateTime, "@endDate", to);

			var totalDays = (to - fromDate).TotalDays + 1;
			var totalEffort = totalDays * dailyEffort;

			var userCal = db.GetDatatable(GetCalendarSql(userId), DBParameters);

			totalDays -= userCal.Rows.Count;

			var newDailyEffort = totalEffort / totalDays;

			while (dateCursor.Date <= to.Date) {
				var rows = userCal.Select("event_date = '" + dateCursor.ToLongDateString() + "'");
				if (rows.Length == 0) {
					var dur = (DateAndDuration)GetDateAndDuration(newDailyEffort, dateCursor);
					durations.Add(dur);
				}

				dateCursor = dateCursor.AddDays(1);
			}

			return durations;
		}

		private AllocationDuration
			GetAllocationDuration(int allocationItemId, double dailyEffort, DateTime dateCursor) {
			var dur = GetDateAndDuration(dailyEffort, dateCursor);
			dur.allocation_item_id = allocationItemId;
			return dur;
		}

		private AllocationDuration GetDateAndDuration(double dailyEffort, DateTime dateCursor) {
			var dur = new AllocationDuration();
			dur.date = dateCursor.Year + "-" + dateCursor.Month.ToString("00") + "-" + dateCursor.Day.ToString("00");
			dur.hours = dailyEffort;
			return dur;
		}

		public AllocationInformation GetInformationAboutAllocation(int allocationItemId) {
			SetDBParameter("@allocationItemId", allocationItemId);

			var sql =
				"select cast(value as NVARCHAR) value, 1 AS o from (select top 1 date value from allocation_durations where allocation_item_id = @allocationItemId order by date asc) q " +
				"UNION " +
				"select cast(value as NVARCHAR) value, 2 AS o from (select top 1 date value from allocation_durations where allocation_item_id = @allocationItemId order by date desc) q2 " +
				"UNION " +
				"select cast(value as NVARCHAR) value, 3 AS o from (select sum(hours) value from allocation_durations where allocation_item_id = @allocationItemId) q3 ORDER BY o";
			var allocationInformation = new AllocationInformation(db.GetDatatableDictionary(sql, DBParameters));
			return allocationInformation;
		}

		public AllocationBase CreateAllocationRequest(EditAllocationModel allocationModel, int userId) {
			var process = getProcessNameForProcessItemId(allocationModel.process_item_id);
			_resourceManagementAccessControl.IAmGoingToWriteToProcess(
				allocationModel.process_item_id,
				process);
			_resourceManagementAccessControl.EnsureItemType(allocationModel.resource_item_id,
				new List<object> { "'user'", "'competency'" });
			_resourceManagementAccessControl.EnsureItemType(allocationModel.process_item_id);

			var insertedItemId = _resourceManagementRepository.InsertItemRow();
			allocationModel.item_id = insertedItemId;
			var item = _resourceManagementRepository.GetItem(insertedItemId);
			if (!allocationModel.status.Equals(AllocationStatus.StatusForTasks) &&
			    !(allocationModel.status.Equals(AllocationStatus.Draft) ||
			      allocationModel.status.Equals(AllocationStatus.Requested))) {
				allocationModel.status = AllocationStatus.Draft;
			}

			item[CreatorItemIdColumnName] = userId;
			item[StatusColumnName] = (int)allocationModel.status;
			item[TitleColumnName] = allocationModel.title;
			item[ResourceItemIdColumnName] = allocationModel.resource_item_id;
			item[TaskItemIdColumnName] = allocationModel.task_item_id;
			item[ProcessItemIdColumnName] = allocationModel.process_item_id;


			SetCostCenterAndResourceType(allocationModel);
			item[CostCenterColumnName] = allocationModel.cost_center;
			item[ResourceTypeColumnName] = allocationModel.resource_type;

			if (!allocationModel.status.Equals(AllocationStatus.Draft) &&
			    allocationModel.cost_center == 0) {
				// cost_center and resource_type is very must, if status
				// is not draft
				throw new CustomArgumentException("COST_CENTER_REQUIRED");
			}

			_resourceManagementRepository.SaveItem(item);

			_resourceManagementRepository.InsertAllocationHours(insertedItemId, allocationModel.Durations);

			if (!string.IsNullOrEmpty(allocationModel.NewComment)) {
				InsertAllocationComment(allocationModel);
			}

			var storedItem = _resourceManagementRepository.GetItem(allocationModel.item_id);
			return new AllocationBase(storedItem);
		}

		/// <summary>
		/// Updates items for given days (removes first the dates found in durationsByDate-list and then inserts them again)
		/// </summary>
		/// <param name="durationsByDate"></param>
		public void UpdateAllocationDurations(List<AllocationDuration> durationsByDate, bool updateTask = true) {
			// TODO: check if user is allowed for this
			// TODO: creator can edit when in status 10 and resource owner can edit when in status 20 or 25

			var sqlForDeletes = "";
			var i = 0;

			var uniqueItems = new Dictionary<int, Dictionary<string, double>>();
			foreach (var duration in durationsByDate) {
				var allocationItemId = duration.allocation_item_id;
				if (!uniqueItems.ContainsKey(allocationItemId)) {
					uniqueItems.Add(allocationItemId, new Dictionary<string, double>());
				}

				uniqueItems[allocationItemId].Remove(duration.date);
				uniqueItems[allocationItemId].Add(duration.date, duration.hours);
			}

			var hourParametersByHourAmount = new Dictionary<double, IDbDataParameter>();
			var insertqueries = new List<InsertQuery>();
			foreach (var allocId in uniqueItems.Keys) {
				var idParameter = SetDBParameter("@allocationId" + i, allocId);
				foreach (var date in uniqueItems[allocId].Keys) {
					var dateParamName = "@date" + i;
					var allocationIdCondition = new SqlCondition("allocation_item_id", SqlComp.EQUALS, idParameter);
					var dateCondition = new SqlCondition("date", SqlComp.EQUALS, "'" + date + "'");
					var deleteQuery = new DeleteQuery()
						.From(AllocationDurationTableName)
						.Where(
							dateCondition.And(allocationIdCondition))
						.Get();
					sqlForDeletes += deleteQuery;
					i++;
					var hours = uniqueItems[allocId][date];
					if (!(hours > 0)) continue;
					
					insertqueries.Add(new InsertQuery()
						.To(AllocationDurationTableName)
						.Value("allocation_item_id", idParameter.ParameterName)
						.Value("date", "'" + date + "'")
						.Value("hours", hours));
				}
			}

			if (!string.IsNullOrEmpty(sqlForDeletes))  db.ExecuteDeleteQuery(sqlForDeletes, DBParameters);
			if (insertqueries.Count > 0) db.ExecuteInsertQuery(insertqueries, DBParameters);
			
			foreach (var allocId in uniqueItems.Keys) {
				if (updateTask) {
					//Update Task From Allocation 1 - After UpdateAllocationDurations
					var allocation = GetAllocation(allocId);
					if (!DBNull.Value.Equals(allocation["task_item_id"])) {
						UpdateTaskFromAllocation(Conv.ToInt(allocation["task_item_id"]), allocId,
							Conv.ToInt(allocation["resource_item_id"]));
					}
				}
			}
		}

		public Dictionary<string, object> UpdateAllocationRequestStatus(int id, int status) {
			var item = _resourceManagementRepository.GetItem(id);
			var editModel = new EditAllocationModel(item) { status = (AllocationStatus)status };
			UpdateAllocation(editModel);
			return _resourceManagementRepository.GetItem(id);
		}

		public void DeleteAllocation(int itemId) {
			var itemToDelete = _resourceManagementRepository.GetItem(itemId);
			var processItemId = Conv.ToInt(itemToDelete["process_item_id"]);
			var process = getProcessNameForProcessItemId(processItemId);

			_resourceManagementAccessControl.IAmGoingToWriteToProcess(processItemId, process);
			_resourceManagementRepository.DeleteAllocationHours(itemId);
			_resourceManagementRepository.Delete(itemId);
		}

		public void SetTaskAllocationsToDraft(int taskId) {
			var allocations = _resourceManagementRepository.GetAllocationsForTaskId(taskId);
			var processItemId = allocations.Select(x => Conv.ToInt(x["process_item_id"])).First();
			var processName = _resourceManagementRepository.GetProcessNameForProcessItemId(processItemId);
			_resourceManagementAccessControl.IAmGoingToWriteToProcess(processItemId, processName);
			var entityService = _resourceManagementRepository.GetEntityService();
			allocations.ForEach(x => {
				var alloc = entityService
					.GetItem(Conv.ToInt(x["item_id"]));
				alloc["status"] = (int)AllocationStatus.Draft;
				entityService.SaveItem(alloc);
			});
		}

		public AllocationBase UpdateAllocation(EditAllocationModel updatedAllocationModel,
			bool internalUpdate = false) {
			if (updatedAllocationModel == null ||
			    updatedAllocationModel.item_id == 0) {
				throw new CustomArgumentException("ITEM_ID_MISSING");
			}

			if (updatedAllocationModel.process_item_id == 0) {
				throw new CustomArgumentException("PROCESS_ITEM_ID_MISSING");
			}

			// Fetch stored record from database
			var item = _resourceManagementRepository.GetItem(updatedAllocationModel.item_id);

			var states = new States(instance, AuthenticatedSession);
			var hasNotifications = states.HasNotificatioConditions(instance, "allocation");
			var oldData = new Dictionary<string, object>();
			if (hasNotifications) oldData = _resourceManagementRepository.GetItem(updatedAllocationModel.item_id);

			var currentDbItem = new EditAllocationModel(item);
			if (item == null || currentDbItem == null || currentDbItem.item_id == 0) {
				throw new CustomArgumentException("ITEM_NOT_FOUND_IN_DB");
			}

			updatedAllocationModel.resource_type = GetResourceType(currentDbItem.resource_item_id);


			var allowedToWriteToProcess = true;
			try {
				_resourceManagementAccessControl
					.IAmGoingToWriteToProcess(
						updatedAllocationModel.process_item_id,
						getProcessNameForProcessItemId(currentDbItem.process_item_id));
			} catch (UnauthorizedAccessException) {
				allowedToWriteToProcess = false;
			}

			// Check if current user is allowed to edit allocation item
			var amITheOwnerOfResourcesCostCenter =
				AmITheOwnerOfResourcesCostCenter(updatedAllocationModel.resource_item_id,
					updatedAllocationModel.resource_type);
			if (!allowedToWriteToProcess && !amITheOwnerOfResourcesCostCenter) {
				throw new CustomPermissionException("NOT_ALLOWED");
			}

			if (updatedAllocationModel.status.Equals(AllocationStatus.Undefined)) {
				updatedAllocationModel.status = currentDbItem.status;
			}

			// If new status is 'draft' and previous status is either 'Approved', then we switch it to 'PreviouslyApproved'

			if (
				updatedAllocationModel.status.Equals(AllocationStatus.Draft) &&
				(currentDbItem.status.Equals(AllocationStatus.PreviouslyApproved) ||
				 currentDbItem.status.Equals(AllocationStatus.Approved) ||
				 currentDbItem.status.Equals(AllocationStatus.ApprovedWithModifications))
			) {
				updatedAllocationModel.status = AllocationStatus.PreviouslyApproved;
			}

			//Prevent update from StatusForTasks to Draft
			if (currentDbItem.status == AllocationStatus.StatusForTasks &&
			    updatedAllocationModel.status == AllocationStatus.Draft)
				updatedAllocationModel.status = AllocationStatus.StatusForTasks;

			if (currentDbItem.status == AllocationStatus.Released) {
				return new AllocationBase(_resourceManagementRepository.GetItem(updatedAllocationModel.item_id));
			}

			// Check if status change is allowed
			var isStatusChangeAllowed = IsStatucChangeAllowed(
				currentDbItem.status,
				updatedAllocationModel.status,
				allowedToWriteToProcess,
				amITheOwnerOfResourcesCostCenter,
				currentDbItem.process_item_id);

			if (!isStatusChangeAllowed) {
				throw new CustomArgumentException("STATUS_CHANGE_IS_NOT_ALLOWED");
			}

			var updateTitle = false;
			var updateResourceItemId = false;

			/* Check if resource_item_id should be updated */
			if (allowedToWriteToProcess && currentDbItem.status.Equals(AllocationStatus.Draft)
			) {
				updateResourceItemId = true;
			} else if (amITheOwnerOfResourcesCostCenter &&
			           currentDbItem.status.Equals(AllocationStatus.Requested) &&
			           currentDbItem.status.Equals(AllocationStatus.RequestedModified)
			) {
				updateResourceItemId = true;
			}

			// Check if title should be updated
			if (allowedToWriteToProcess) {
				updateTitle = true;
			}

			if (updatedAllocationModel.status.Equals(AllocationStatus.Approved)) {
				/* APPROVED */
				updateTitle = allowedToWriteToProcess;
			}

			if (updateTitle && !string.IsNullOrEmpty(updatedAllocationModel.title)) {
				item[TitleColumnName] = updatedAllocationModel.title;
			}


			var oldResourceId = Conv.ToInt(item[ResourceItemIdColumnName]);
			if (updateResourceItemId && updatedAllocationModel.resource_item_id > 0) {
				item[ResourceItemIdColumnName] = updatedAllocationModel.resource_item_id;
				item[ResourceTypeColumnName] = GetResourceType(updatedAllocationModel.resource_item_id);

				//Update Task from Allocation
				if (updatedAllocationModel.task_item_id != null) {
					//Update Task From Allocation 2 - After UpdateAllocation
					UpdateTaskFromAllocation((int)updatedAllocationModel.task_item_id,
						updatedAllocationModel.item_id, updatedAllocationModel.resource_item_id, oldResourceId);
				}
			}

			if (updatedAllocationModel.status.Equals(AllocationStatus.Approved)) {
				if (updatedAllocationModel.task_item_id != null && Conv.ToInt(item[StatusColumnName]) ==
					(int)AllocationStatus.ApprovedWithModifications) {
					//Update Task From Allocation 6 - After UpdateAllocation -- WHEN APPROVED BY PM
					UpdateTaskFromAllocation((int)updatedAllocationModel.task_item_id,
						updatedAllocationModel.item_id, updatedAllocationModel.resource_item_id, oldResourceId, true);
				}
			}

			if (updatedAllocationModel.resource_item_id == 0) {
				updatedAllocationModel.resource_item_id = Conv.ToInt(item[ResourceItemIdColumnName]);
				updatedAllocationModel.resource_type = GetResourceType(updatedAllocationModel.resource_item_id);
			}

			var requestedCostCenter = updatedAllocationModel.cost_center;
			SetCostCenterAndResourceType(updatedAllocationModel);
			if (requestedCostCenter != updatedAllocationModel.cost_center && requestedCostCenter != 0) {
				throw new CustomArgumentException("COST_CENTER_UNAVAILABLE");
			}

			item[ResourceTypeColumnName] = updatedAllocationModel.resource_type;
			item[CostCenterColumnName] = updatedAllocationModel.cost_center;

			if (!updatedAllocationModel.status.Equals(AllocationStatus.Draft) &&
			    updatedAllocationModel.cost_center == 0) {
				// cost_center and resource_type is very must if the status is not 'draft'
				throw new CustomArgumentException("COST_CENTER_IS_NOT_SET");
			}

			item[StatusColumnName] = (int)updatedAllocationModel.status;

			_resourceManagementRepository.SaveItem(item, internalUpdate);
			if (hasNotifications) {
				var newData = _resourceManagementRepository.GetItem(updatedAllocationModel.item_id);
				states.CheckNotifications("allocation", updatedAllocationModel.item_id, oldData, newData);
			}

			return new AllocationBase(_resourceManagementRepository.GetItem(updatedAllocationModel.item_id));
		}

		public void UpdateTaskFromAllocation(int taskItemId, int allocationItemId, int newResourceItemId,
			int oldResourceItemId, bool fromPmApproval = false) {
			SetDBParam(SqlDbType.Int, "@allocId", allocationItemId);
			SetDBParam(SqlDbType.Int, "@taskId", taskItemId);
			SetDBParam(SqlDbType.Int, "@newUserId", newResourceItemId);
			SetDBParam(SqlDbType.Int, "@oldUserId", oldResourceItemId);

			var p = new Processes(instance, AuthenticatedSession);
			var oldProcess = p.GetProcessByItemId(oldResourceItemId);
			var newProcess = p.GetProcessByItemId(newResourceItemId);

			SetDBParam(SqlDbType.NVarChar, "@OldColumnName",
				oldProcess == "user" ? "responsibles" : "competencies");
			SetDBParam(SqlDbType.NVarChar, "@NewColumnName",
				newProcess == "user" ? "responsibles" : "competencies");

			var t = "task_" + (newProcess == "competencies" ? "competency" : "user") + "_effort";

			const string oldColumnId =
				"(SELECT TOP 1 item_column_id FROM item_columns WHERE instance = @instance AND process = 'task' AND name = @OldColumnName)";
			const string newColumnId =
				"(SELECT TOP 1 item_column_id FROM item_columns WHERE instance = @instance AND process = 'task' AND name = @NewColumnName)";

			//Update Resources / Competencies
			if (newResourceItemId != oldResourceItemId) {
				//Remove old selection
				var delsql =
					"DELETE FROM items WHERE item_id = (SELECT link_item_id FROM item_data_process_selections WHERE item_id = @taskId AND selected_item_id = @oldUserId AND item_column_id = " +
					oldColumnId + ")";
				db.ExecuteDeleteQuery(delsql, DBParameters);

				//Add new selection
				var newLinkId =
					db.ExecuteInsertQuery("INSERT INTO items (instance, process) VALUES (@instance, '" + t + "')",
						DBParameters);
				SetDBParam(SqlDbType.Int, "@newLinkId", newLinkId);

				//Recreate durations for the new resource so that holidays etc are accounted for
				var oldSpec = db.GetDataRow("SELECT start_date, end_date, (SELECT SUM(hours) FROM allocation_durations WHERE allocation_item_id = @allocId) AS effort FROM _" + instance + "_task" + " WHERE item_id = @taskId", DBParameters);
				var totalDays = ((DateTime)oldSpec["end_date"] - (DateTime)oldSpec["start_date"]).TotalDays + 1;
				var dailyEffort = totalDays > 0 ? Conv.ToDouble(oldSpec["effort"]) / totalDays : 0;

				_resourceManagementRepository.DeleteAllocationHours(allocationItemId);
				InsertAllocationDurations(allocationItemId, (DateTime)oldSpec["start_date"], (DateTime)oldSpec["end_date"], dailyEffort, newResourceItemId, false);
				
				//Update task to have the correct effort
				db.ExecuteUpdateQuery("UPDATE _" + instance + "_" + t +
				                      " SET effort = (SELECT SUM(hours) FROM allocation_durations WHERE allocation_item_id = @allocId) WHERE item_id = " +
				                      newLinkId, DBParameters);

				//Update Item Data Process Selections -- process has changed
				db.ExecuteUpdateQuery(
					"UPDATE item_data_process_selections SET selected_item_id = @newUserId, link_item_id = @newLinkId, item_column_id = " +
					newColumnId + " WHERE item_id = @taskId AND item_column_id = " + oldColumnId +
					" AND selected_item_id = @oldUserId ",
					DBParameters);
				Cache.Initialize();
			} else {
				//Update Item Data Process Selections -- process is the same
				db.ExecuteUpdateQuery(
					"UPDATE item_data_process_selections SET selected_item_id = @newUserId WHERE item_id = @taskId AND item_column_id = " +
					oldColumnId + " AND selected_item_id = @oldUserId ",
					DBParameters);
			}

			//Update Dates
			if (Conv.ToBool(Config.GetValue("Gantt", "AutoUpdateTaskFromAllocation"))) {
				//Update Effort
				if (fromPmApproval) {
					db.ExecuteUpdateQuery(
						"UPDATE " + "_" + instance + "_" + t +
						" SET effort = (SELECT SUM(hours) FROM allocation_durations WHERE allocation_item_id = @allocId) WHERE item_id = (SELECT TOP 1 link_item_id FROM item_data_process_selections WHERE item_id = @taskId AND item_column_id = " +
						newColumnId + " AND selected_item_id = @newUserId)",
						DBParameters);
				}

				db.ExecuteUpdateQuery(
					"UPDATE _" + instance +
					"_task SET start_date = (SELECT MIN(date) FROM allocation_durations WHERE allocation_item_id = @allocId AND hours > 0),  " +
					" end_date = (SELECT MAX(date) FROM allocation_durations WHERE allocation_item_id = @allocId AND hours > 0) " +
					" WHERE item_id = @taskId ",
					DBParameters);
			}
		}

		public void UpdateTaskFromAllocation(int taskItemId, int allocationItemId, int userItemId) {
			SetDBParam(SqlDbType.Int, "@allocId", allocationItemId);
			SetDBParam(SqlDbType.Int, "@taskId", taskItemId);
			SetDBParam(SqlDbType.Int, "@userId", userItemId);

			if (Conv.ToBool(Config.GetValue("Gantt", "AutoUpdateTaskFromAllocation"))) {
				//Update Effort
				db.ExecuteUpdateQuery(
					"UPDATE _" + instance +
					"_task_user_effort SET effort = (SELECT SUM(hours) FROM allocation_durations WHERE allocation_item_id = @allocId) WHERE item_id = (SELECT TOP 1 link_item_id FROM item_data_process_selections WHERE item_id = @taskId AND item_column_id = (SELECT TOP 1 item_column_id FROM item_columns WHERE instance = @instance AND process='task' AND name='responsibles') AND selected_item_id = @userId)",
					DBParameters);

				//Update Dates
				db.ExecuteUpdateQuery(
					"UPDATE _" + instance +
					"_task SET start_date =(select min(date) from allocation_durations WHERE allocation_item_id =@allocId AND hours > 0),  " +
					" end_date =(select max(date) from allocation_durations WHERE allocation_item_id =@allocId AND hours > 0) " +
					" WHERE item_id = @taskId ",
					DBParameters);
			}
		}

		public Dictionary<string, object> AddSimpleAllocation(EditAllocationModel allocationModel) {
			var insertedItemId = _resourceManagementRepository.InsertItemRow();
			allocationModel.item_id = insertedItemId;
			var item = _resourceManagementRepository.GetItem(insertedItemId);
			item[CreatorItemIdColumnName] = AuthenticatedSession.item_id;
			item[StatusColumnName] = (int)AllocationStatus.Approved;
			item[TitleColumnName] = allocationModel.title;
			item[ResourceItemIdColumnName] = allocationModel.resource_item_id;
			if (allocationModel.task_item_id > 0) {
				item[TaskItemIdColumnName] = allocationModel.task_item_id;
			}

			item[ProcessItemIdColumnName] = allocationModel.process_item_id;
			item[CostCenterColumnName] = allocationModel.cost_center;
			item[ResourceTypeColumnName] = allocationModel.resource_type;
			item["work_category_item_id"] = allocationModel.work_category_item_id;
			item["user_item_id"] = allocationModel.user_item_id;
			_resourceManagementRepository.SaveItem(item);
			Dictionary<string, object> new_allocation = _resourceManagementRepository.GetItem(insertedItemId);
			if (allocationModel.resource_type == "user") {
				new_allocation["user_item_id"] = allocationModel.resource_item_id;
			} else {
				new_allocation["user_item_id"] = 0;
			}

			new_allocation["edit"] = true;
			return new_allocation;
		}

		public void UpdateSimpleHours(int mode, List<SimpleAllocationHours> items) {
			items.ForEach(item => {
				var month = item.month;
				var year = item.year;
				var allocationItemId = item.allocation_item_id;

				if (mode == 1) {
					DateTime date_start;
					DateTime date_end;
					int week = Util.FirstWeekOfMonth(year, month);
					if (week > 50) {
						date_start = Util.StartOfWeek(year - 1, week);
					} else {
						date_start = Util.StartOfWeek(year, week);
					}

					week = Util.LastWeekOfMonth(year, month);
					if (week == 1) {
						date_end = Util.EndOfWeek(year + 1, week);
					} else {
						date_end = Util.EndOfWeek(year, week);
					}

					_resourceManagementRepository.DeleteAllocationHours(allocationItemId, date_start, date_end);
				} else {
					_resourceManagementRepository.DeleteAllocationHours(allocationItemId, year, month);
				}
			});
			items.ForEach(item => {
				var month = item.month;
				var year = item.year;
				var allocationItemId = item.allocation_item_id;

				if (item.dates != null && item.dates.Count > 0) {
					var dates = item.dates;
					var datesAndDurations = new List<DateAndDuration>();
					foreach (var dateItem in dates) {
						if (dateItem.hours != 0) {
							var dateAndDuration = new DateAndDurationWithDay();
							dateAndDuration.date = dateItem.year.ToString("0000") + "-" + month.ToString("00") + "-" +
							                       dateItem.day.ToString("00");
							dateAndDuration.hours = dateItem.hours;
							datesAndDurations.Add(dateAndDuration);
						}
					}

					_resourceManagementRepository.InsertAllocationHours(allocationItemId, datesAndDurations);
				}


				//Update Task From Allocation 3 - After UpdateSimpleHours
				var allocation = GetAllocation(allocationItemId);
				if (!DBNull.Value.Equals(allocation["task_item_id"])) {
					UpdateTaskFromAllocation(Conv.ToInt(allocation["task_item_id"]), allocationItemId,
						Conv.ToInt(allocation["resource_item_id"]));
				}
			});
		}

		public void UpdateSimpleAllocation(EditAllocationModel allocationModel) {
			var item = _resourceManagementRepository.GetItem(allocationModel.item_id);
			item[StatusColumnName] = (int)AllocationStatus.Approved;
			item[TitleColumnName] = allocationModel.title;

			var oldResourceId = Conv.ToInt(item[ResourceItemIdColumnName]);

			item[ResourceItemIdColumnName] = allocationModel.resource_item_id;

			item[TaskItemIdColumnName] = allocationModel.task_item_id;
			item[ProcessItemIdColumnName] = allocationModel.process_item_id;
			item[CostCenterColumnName] = allocationModel.cost_center;
			item[ResourceTypeColumnName] = allocationModel.resource_type;
			item["work_category_item_id"] = allocationModel.work_category_item_id;
			_resourceManagementRepository.SaveItem(item);

			if (oldResourceId != allocationModel.resource_item_id) {
				//Update Task From Allocation 4 - After UpdateSimpleAllocation
				if (allocationModel.task_item_id != null) {
					UpdateTaskFromAllocation((int)allocationModel.task_item_id, allocationModel.item_id,
						allocationModel.resource_item_id, oldResourceId);
				}
			}
		}

		private InsertQuery GetSqlForInsert(int index, DateAndDuration duration, DurationType type) {
			var dateParameterName = "@durationDate" + index;
			var hoursParameterName = "@durationHours" + index;
			SetDBParameter(dateParameterName, duration.date);
			SetDBParam(SqlDbType.Float, hoursParameterName, duration.hours);
			var insertQuery = new InsertQuery();

			if (type.Equals(DurationType.Task)) {
				insertQuery
					.To(TaskDurationTableName)
					.Value("date", dateParameterName)
					.Value("hours", hoursParameterName)
					.Value("task_item_id", "@taskId")
					.Value("resource_item_id", "@processItemId");
			}

			if (type.Equals(DurationType.Allocation)) {
				insertQuery
					.To(AllocationDurationTableName)
					.Value("date", dateParameterName)
					.Value("hours", hoursParameterName)
					.Value("allocation_item_id", "@allocationId");
			}

			return insertQuery;
		}

		private int ReturnCostCenterIdIfCompetencyIsAvailable(int competencyId, int costCenterId) {
			var listOfCostCenters = ListOfCostCentersForCompetency(competencyId);
			return listOfCostCenters.Contains(costCenterId) ? costCenterId : 0;
		}

		private List<int> ListOfCostCentersForCompetency(int competencyId) {
			var competencyParam = SetDBParameter("@competencyId", competencyId);
			var costCenterMatch = SetDBParameter("@costCenterField", "cost_center");
			var competencyMatch = SetDBParameter("@competencyField", "competency");

			var queryForCostCentersForCompetency = BuildSelectQuery()
				.Columns("idps.selected_item_id")
				.From("item_data_process_selections", "idps")
				.Join(new Join("item_columns", "ic").On("idps.item_column_id", "ic.item_column_id"))
				.Where(
					_instanceCondition
						.And(new SqlCondition("process", SqlComp.EQUALS, competencyMatch))
						.And(new SqlCondition("name", SqlComp.EQUALS, costCenterMatch))
						.And(new SqlCondition("item_id", SqlComp.EQUALS, competencyParam)))
				.Fetch();

			var listOfCostCenters = queryForCostCentersForCompetency
				.Select(row => Conv.ToInt(row["selected_item_id"])).ToList();
			return listOfCostCenters;
		}

		private int GetCostCenterForUser(int resourceItemId) {
			var itemIdParam = SetDBParameter("@resourceItemId", resourceItemId);
			var costCenterMatch = SetDBParameter("@costCenterField", "cost_center");
			var queryForUsersCostCenter = BuildSelectQuery()
				.Columns("idps.selected_item_id")
				.From("item_data_process_selections", "idps")
				.Join(new Join("item_columns", "ic").On("idps.item_column_id", "ic.item_column_id"))
				.Where(
					_instanceCondition
						.And(_userProcessCondition)
						.And(new SqlCondition("name", SqlComp.EQUALS, costCenterMatch))
						.And(new SqlCondition("item_id", SqlComp.EQUALS, itemIdParam)))
				.Fetch();
			return queryForUsersCostCenter.Count > 0
				? Conv.ToInt(queryForUsersCostCenter.First()["selected_item_id"])
				: 0;
		}

		private string GetResourceType(int resourceItemId) {
			var itemIdParam = SetDBParameter("@itemId", resourceItemId);
			var queryForResourceType = new SelectQuery()
				.Columns("process")
				.From("items")
				.Where("item_id", SqlComp.EQUALS, itemIdParam);
			return Conv.ToStr(db.ExecuteScalar(queryForResourceType.Get(), DBParameters));
		}

		private void SetCostCenterAndResourceType(EditAllocationModel allocationModel) {
			var processName = GetResourceType(allocationModel.resource_item_id);
			if (!string.IsNullOrEmpty(processName)) {
				allocationModel.resource_type = processName;
				var costCenter = 0;
				if (processName.Equals("user")) {
					costCenter = GetCostCenterForUser(allocationModel.resource_item_id);
				} else if (processName.Equals("competency")) {
					if (allocationModel.cost_center > 0) {
						// for competencies we check that given cost center has competency available
						costCenter = ReturnCostCenterIdIfCompetencyIsAvailable(
							allocationModel.resource_item_id,
							allocationModel.cost_center);
					} else {
						var costCentersForCompetency = ListOfCostCentersForCompetency(allocationModel.resource_item_id);
						if (costCentersForCompetency.Count == 1) {
							costCenter = costCentersForCompetency.First();
						}
					}
				}

				allocationModel.cost_center = costCenter;
			}
		}

		private void InsertAllocationComment(EditAllocationModel allocationModel) {
			SetDBParameters(
				"@comment", allocationModel.NewComment,
				"@instance", instance,
				"@authorId", AuthenticatedSession.item_id);
			var insertComment = new InsertQuery()
				.To("allocation_comments")
				.Value("allocation_item_id", "@allocationId")
				.Value("comment", "@comment")
				.Value("created_at", "CURRENT_TIMESTAMP")
				.Value("author_item_id", "@authorId");
			db.ExecuteInsertQuery(insertComment, DBParameters);
		}

		private string getProcessNameForProcessItemId(int processItemId) {
			return _resourceManagementRepository.GetProcessNameForProcessItemId(processItemId);
		}

		private bool IsStatucChangeAllowed(AllocationStatus current,
			AllocationStatus newStatus,
			bool allocationAuthor,
			bool ownerOfResource,
			int processItemId) {
			if (current == newStatus) return true;
			return _resourceManagementAccessControl.IsStatucChangeAllowed(current, newStatus, allocationAuthor,
				ownerOfResource,
				processItemId);
		}

		private bool AmITheOwnerOfResourcesCostCenter(int resourceId, string type) {
			var processName = GetProcessTableName(type);
			var sql = "SELECT COUNT(*) FROM " + processName + " u " +
			          "JOIN item_data_process_selections idps ON idps.item_id = u.item_id " +
			          "JOIN item_columns ic ON ic.item_column_id = idps.item_column_id " +
			          "WHERE instance = '" + Conv.ToSql(instance) + "' " +
			          "AND process = '" + Conv.ToSql(type) + "' " +
			          "AND ic.name = 'cost_center' " +
			          "AND idps.selected_item_id IN( " +
			          "    SELECT idps.item_id FROM item_data_process_selections idps " +
			          "JOIN item_columns ic ON ic.item_column_id = idps.item_column_id " +
			          "where instance = '" + Conv.ToSql(instance) + "' " +
			          "AND process = 'list_cost_center' " +
			          "AND ic.name = 'owner' " +
			          "AND selected_item_id = @userId " +
			          ") " +
			          "AND u.item_id = @resourceId";
			SetDBParameters(
				"@resourceId", resourceId,
				"@userId", AuthenticatedSession.item_id);
			var result = db.ExecuteScalar(sql, DBParameters);
			return (int)result > 0;
		}

		public int GetAllocationCostCenter(int allocationItemId) {
			SetDBParam(SqlDbType.Int, "@allocationItemId", allocationItemId);
			return Conv.ToInt(db.ExecuteScalar(
				" SELECT cost_center FROM _" + instance +
				"_allocation WHERE item_id = @allocationItemId",
				DBParameters));
		}

		public int GetAlloctionItemId(int processItemId, int taskItemId, int userId) {
			SetDBParam(SqlDbType.Int, "@processItemId", processItemId);
			SetDBParam(SqlDbType.Int, "@taskItemId", taskItemId);
			SetDBParam(SqlDbType.Int, "@userId", userId);
			return Conv.ToInt(db.ExecuteScalar(
				" SELECT COALESCE(MIN(item_id), 0) FROM _" + instance +
				"_allocation WHERE process_item_id = @processItemId and task_item_id = @taskItemId and resource_item_id = @userID",
				DBParameters));
		}

		public int GetAlloctionItemId(int processItemId, int taskItemId, int competencyId, int costCenterId) {
			SetDBParam(SqlDbType.Int, "@processItemId", processItemId);
			SetDBParam(SqlDbType.Int, "@taskItemId", taskItemId);
			SetDBParam(SqlDbType.Int, "@competencyId", competencyId);
			SetDBParam(SqlDbType.Int, "@costCenterId", costCenterId);
			return Conv.ToInt(db.ExecuteScalar(
				" SELECT COALESCE(MIN(item_id), 0) FROM _" + instance +
				"_allocation WHERE process_item_id = @processItemId AND task_item_id = @taskItemId AND resource_item_id = @competencyId AND cost_center = @costCenterId",
				DBParameters));
		}

		public void DeleteAllocationsForNotAllocatedUsers(int processItemId, int taskItemId, List<int> users) {
			SetDBParam(SqlDbType.Int, "@processItemId", processItemId);
			SetDBParam(SqlDbType.Int, "@taskItemId", taskItemId);

			var sql = "DELETE FROM items WHERE item_id IN (SELECT item_id FROM _" + instance +
			          "_allocation WHERE " + GetProcessCheckSql(true) +
			          " AND process_item_id = @processItemId AND task_item_id = @taskItemId ";

			if (users.Count > 0) {
				var idStr = string.Join(",", users.ToArray());
				sql += " AND resource_item_id NOT IN (" + idStr + ") ) ";
			} else {
				sql += " ) ";
			}

			db.ExecuteDeleteQuery(sql, DBParameters);
		}

		private string GetProcessCheckSql(bool user) {
			var e = user ? "=" : "<>";
			return "(SELECT resource_type FROM _" + instance + "_allocation a WHERE a.item_id = items.item_id) " + e +
			       " 'user'";
		}

		public void DeleteAllocationsForNotAllocatedCompetencies(int processItemId, int taskItemId,
			List<KeyValuePair<int, int>> competencies) {
			SetDBParam(SqlDbType.Int, "@processItemId", processItemId);
			SetDBParam(SqlDbType.Int, "@taskItemId", taskItemId);
			var sql = "DELETE FROM items WHERE item_id IN (SELECT item_id FROM _" + instance +
			          "_allocation WHERE " + GetProcessCheckSql(false) +
			          " AND process_item_id = @processItemId AND task_item_id = @taskItemId ";
			sql = competencies.Aggregate(sql,
				(current, c) =>
					current + (" AND NOT (resource_item_id = " + c.Key + " AND cost_center = " + c.Value + ")"));
			sql += " )";

			db.ExecuteDeleteQuery(sql, DBParameters);
		}

		public void CopyAllocations(int sourceItemId, int targetItemId, Dictionary<int, int> taskIdPairs) {
			SetDBParam(SqlDbType.Int, "@sourceItemId", sourceItemId);
			SetDBParam(SqlDbType.Int, "@targetItemId", targetItemId);

			foreach (DataRow dr in db
				.GetDatatable("SELECT * FROM _" + instance + "_allocation WHERE process_item_id = @sourceItemId",
					DBParameters).Rows) {
				var insertedItemId = _resourceManagementRepository.InsertItemRow();

				SetDBParam(SqlDbType.Int, "@sourceAllocationItemId", dr["item_id"]);


				SetDBParam(SqlDbType.Int, "@insertedItemId", insertedItemId);

				SetDBParam(SqlDbType.NVarChar, "@orderNo", dr["order_no"]);
				SetDBParam(SqlDbType.Int, "@status", dr["status"]);
				SetDBParam(SqlDbType.Int, "@creatorId", dr["creator_item_id"]);
				SetDBParam(SqlDbType.NVarChar, "@title", dr["title"]);
				SetDBParam(SqlDbType.Int, "@resourceItemId", dr["resource_item_id"]);
				SetDBParam(SqlDbType.Int, "@costCenter", dr["cost_center"]);
				SetDBParam(SqlDbType.NVarChar, "@resourceType", dr["resource_type"]);
				SetDBParam(SqlDbType.Int, "@workCategoryItemId", dr["work_category_item_id"]);


				if (!DBNull.Value.Equals(dr["task_item_id"]) &&
				    taskIdPairs.ContainsKey(Conv.ToInt(dr["task_item_id"]))) {
					SetDBParam(SqlDbType.Int, "@taskItemId", taskIdPairs[Conv.ToInt(dr["task_item_id"])]);
				} else {
					SetDBParam(SqlDbType.Int, "@taskItemId", DBNull.Value);
				}

				db.ExecuteUpdateQuery(
					"UPDATE _" + instance +
					"_allocation SET " +
					" process_item_id = @targetItemId, " +
					" order_no = @orderNo, " +
					" status = @status, " +
					" creator_item_id = @creatorId, " +
					" title = @title, " +
					" resource_item_id = @resourceItemId, " +
					" cost_center = @costCenter, " +
					" resource_type = @resourceType, " +
					" task_item_id = @taskItemId, " +
					" work_category_item_id = @workCategoryItemId " +
					" WHERE item_id = @insertedItemId ",
					DBParameters);

				var sourceHours =
					db.GetDatatable(
						"SELECT allocation_item_id, date, hours, archive_userid, archive_start FROM allocation_durations WHERE allocation_item_id = @sourceAllocationItemId ",
						DBParameters);

				var targetHours = db.GetDatatable(
					"SELECT allocation_item_id, date, hours, archive_userid, archive_start FROM allocation_durations WHERE allocation_item_id = @insertedItemId ",
					DBParameters);


				foreach (DataRow hourRow in sourceHours.Rows) {
					var n = targetHours.NewRow();
					n["allocation_item_id"] = insertedItemId;
					n["date"] = hourRow["date"];
					n["hours"] = 0;
					n["archive_userid"] = AuthenticatedSession.item_id;
					n["archive_start"] = DateTime.Now;
					targetHours.Rows.Add(n);
				}

				targetHours.TableName = "allocation_durations";

				db.ExecuteFromDataTable(targetHours);
			}
		}

		public Dictionary<string, object> GetAllocationSums(int itemId, int year, string process,
			string filter_item_ids) {
			var d = new Dictionary<string, object>();

			if (itemId > 0) {
				filter_item_ids = Conv.ToStr(itemId);
			}

			foreach (string resource_item_id in filter_item_ids.Split(",")) {
				if (resource_item_id != "0") {
					var retval1 = new Dictionary<int, double>();
					var retval2 = new Dictionary<int, double>();
					var retval3 = new Dictionary<int, double>();
					var retval4 = new Dictionary<int, double>();

					var totalSum = 0.0;
					var totalSum2 = 0.0;
					var yearSum = 0.0;
					var yearSum2 = 0.0;

					string where;
					if (itemId > 0) {
						where = "AND a.process_item_id = @itemId ";
					} else {
						where = "AND a.resource_item_id IN (" + resource_item_id + ") AND a.resource_item_id > 0 ";
					}

					SetDBParam(SqlDbType.Int, "@itemId", itemId);
					SetDBParam(SqlDbType.NVarChar, "@process", process);
					foreach (DataRow r in db.GetDatatable("SELECT a.item_id, a.work_category_item_id, " +
					                                      "SUM(ad.hours) AS hour_sum, " +
					                                      "SUM(CASE WHEN YEAR(ad.date) = " + year +
					                                      " THEN ad.hours ELSE 0 END) AS year_sum " +
					                                      "FROM _" + instance + "_allocation a " +
					                                      "LEFT JOIN allocation_durations ad ON a.item_id = ad.allocation_item_id " +
					                                      "LEFT JOIN items i ON i.item_id = a.process_item_id " +
					                                      "WHERE (i.process = @process OR i.process IS NULL) " +
					                                      where +
					                                      "GROUP BY a.item_id, a.work_category_item_id ", DBParameters)
						.Rows) {
						totalSum += Conv.ToDouble(r["hour_sum"]);
						yearSum += Conv.ToDouble(r["year_sum"]);
						retval1.Add(Conv.ToInt(r["item_id"]), Conv.ToDouble(r["hour_sum"]));
						retval3.Add(Conv.ToInt(r["item_id"]), Conv.ToDouble(r["year_sum"]));
						if (Conv.ToInt(r["work_category_item_id"]) > 0) {
							if (!retval2.ContainsKey(Conv.ToInt(r["work_category_item_id"]))) {
								retval2.Add(Conv.ToInt(r["work_category_item_id"]), 0);
								retval4.Add(Conv.ToInt(r["work_category_item_id"]), 0);
							}

							totalSum2 += Conv.ToDouble(r["hour_sum"]);
							yearSum2 += Conv.ToDouble(r["year_sum"]);

							retval2[Conv.ToInt(r["work_category_item_id"])] += Conv.ToDouble(r["hour_sum"]);
							retval4[Conv.ToInt(r["work_category_item_id"])] += Conv.ToDouble(r["year_sum"]);
						}
					}

					retval1.Add(0, totalSum);
					retval2.Add(0, totalSum2);
					retval3.Add(0, yearSum);
					retval4.Add(0, yearSum2);

					if (itemId > 0) {
						d.Add("itemTotals", retval1);
						d.Add("categoryTotals", retval2);
						d.Add("itemYearTotals", retval3);
						d.Add("categoryYearTotals", retval4);
					} else {
						d.Add("itemTotals_" + resource_item_id, retval1);
						d.Add("categoryTotals_" + resource_item_id, retval2);
						d.Add("itemYearTotals_" + resource_item_id, retval3);
						d.Add("categoryYearTotals_" + resource_item_id, retval4);
					}
				}
			}

			return d;
		}

		public class QuickLookResult {
			public int ItemId;
			public double WorkingHours;
			public double Hours;
			public DateTime? Date;
			public bool IsHoliday;

			public QuickLookResult(DataRow r) {
				ItemId = Conv.ToInt(r["resource_item_id"]);
				WorkingHours = Conv.ToDouble(r["workinghours"]);
				Hours = Conv.ToDouble(r["hours"]);
				Date = Conv.ToDateTime(r["date"]);
				IsHoliday = Conv.ToBool(r["holiday"]);
			}
		}

		public List<QuickLookResult> GetAllocationQuickLook(List<int> userIds) {
			SetDBParam(SqlDbType.DateTime, "@startDate", DateTime.Now.AddDays(-3));
			SetDBParam(SqlDbType.DateTime, "@endDate", DateTime.Now.AddDays(14));
			var ids = Modi.JoinListWithComma(userIds);
			var sql = " WITH HolidayQuery AS " +
			          " ( " +
			          "   SELECT TOP (DATEDIFF(DAY, @startDate, @endDate) + 1)  " +
			          "     n = ROW_NUMBER() OVER (ORDER BY [object_id]) " +
			          "   FROM sys.all_objects " +
			          " ) " +
			          " SELECT resource_item_id, SUM(workinghours) AS workinghours, SUM(hours) AS hours, date, SUM(holiday) AS holiday FROM ( " +
			          "       SELECT  " +
			          "               i.item_id AS resource_item_id,  " +
			          "               0 AS workinghours, " +
			          "               CAST (DATEADD(DAY, n - 1, @startDate) AS DATE) AS date, " +
			          "               ISNULL((SELECT holiday FROM dbo.GetCombinedCalendarForUser(i.item_id, DATEADD(DAY, n - 1, CAST(DATEADD(DAY, n - 1, @startDate) AS DATE)), DATEADD(DAY, n - 1, CAST (DATEADD(DAY, n - 1, @startDate) AS DATE)))),0) AS holiday, " +
			          "               0 AS hours " +
			          "       FROM HolidayQuery " +
			          "       LEFT JOIN items i ON item_id IN (" + ids + ") " +
			          "        " +
			          "       UNION ALL " +
			          "        " +
			          "       SELECT  " +
			          "               iq.resource_item_id, " +
			          "               CASE WHEN ca.work_hours = -1 THEN ISNULL(pc.work_hours,0) ELSE ISNULL(ca.work_hours,0) END AS workinghours, " +
			          "               iq.date, " +
			          "               0 AS holiday, " +
			          "               hours " +
			          "       FROM ( " +
			          "               SELECT  " +
			          "                       a.resource_item_id,  " +
			          "                       ad.date,  " +
			          "                       SUM(ad.hours) AS hours  " +
			          "               FROM _" + instance + "_allocation a " +
			          "               LEFT JOIN allocation_durations ad ON ad.allocation_item_id = a.item_id " +
			          "               WHERE a.resource_item_id IN (" + ids +
			          ") AND a.status > 0 AND a.status NOT IN (30,70) AND ad.date BETWEEN @startDate AND @endDate " +
			          "               GROUP BY a.resource_item_id, ad.date " +
			          "       ) iq " +
			          "       LEFT JOIN calendars pc ON pc.item_id = iq.resource_item_id " +
			          "       LEFT JOIN calendars ca ON ca.calendar_id = pc.parent_calendar_id " +
			          " ) oq " +
			          " GROUP BY resource_item_id, date " +
			          " ORDER BY resource_item_id, date ASC ";

			return (from DataRow r in db.GetDatatable(sql, DBParameters).Rows select new QuickLookResult(r)).ToList();
		}

		public void MoveAllocation(AllocationShift allocation_shift) {
			MoveAllocation(allocation_shift.process_item_id, allocation_shift.date_mode, allocation_shift.shift,
				allocation_shift.move_history, allocation_shift.allocation_item_id);
		}

		public void MoveAllocation(int process_item_id, AllocationShiftDateModes date_mode, int shift,
			bool move_history = false, int? allocation_item_id = null) {
			if (date_mode == AllocationShiftDateModes.Weeks) {
				throw new Exception("ERROR: Moving allocation is not implemented for weeks.");
			} // Weeks coming later, maybe ???

			//Diag.LogToConsole("MoveAllocation");
			//Diag.LogToConsole("process_item_id " + process_item_id);
			//Diag.LogToConsole("date_mode " + date_mode);
			//Diag.LogToConsole("shift " + shift);
			//Diag.LogToConsole("move_history " + move_history);
			//Diag.LogToConsole("allocation_item_id " + allocation_item_id);

			int adder = 0;

			SetDBParam(SqlDbType.Int, "@process_item_id", process_item_id);
			string month_sql = "MONTH(ad.date)";
			if (date_mode == AllocationShiftDateModes.Weeks) {
				month_sql = "DATEPART(ISO_WEEK, ad.date)";
			}

			string date_sql = "";
			if (!move_history) {
				SetDBParam(SqlDbType.DateTime, "@start_date",
					new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1));
				date_sql = "AND date >= @start_date ";
			}

			string allocation_item_id_sql = "";
			if (allocation_item_id > 0) {
				SetDBParam(SqlDbType.Int, "@allocation_item_id", allocation_item_id);
				allocation_item_id_sql = "AND ad.allocation_item_id = @allocation_item_id ";
			}

			string sql =
				"SELECT allocation_item_id, SUM(hours) AS hours, year, month, MIN(date) AS min_date, MAX(date) AS max_date " +
				"FROM (" +
				"SELECT ad.allocation_item_id, ad.hours, YEAR(ad.date) AS year, " + month_sql + " AS month, ad.date " +
				"FROM _" + instance + "_allocation a " +
				"LEFT JOIN allocation_durations ad ON ad.allocation_item_id = a.item_id " +
				"WHERE a.process_item_id = @process_item_id " + date_sql + allocation_item_id_sql +
				") tmp " +
				"GROUP BY year, month, allocation_item_id " +
				"ORDER BY allocation_item_id ASC, year ASC, month ASC";
			DataTable allocations = db.GetDatatable(sql, DBParameters);
			if (allocations.Rows.Count > 0) {
				SetDBParam(SqlDbType.Int, "@user_item_id", AuthenticatedSession.item_id);
				DateTime min_date = (DateTime)allocations.Compute("MIN(min_date)", "");
				if (shift < 0) {
					min_date = min_date.AddMonths(shift);
					adder = -1;
				}

				DateTime max_date = (DateTime)allocations.Compute("MAX(max_date)", "");
				if (shift > 0) {
					max_date = max_date.AddMonths(shift);
					adder = 1;
				}

				SetDBParam(SqlDbType.DateTime, "@start_date", min_date);
				SetDBParam(SqlDbType.DateTime, "@end_date", max_date);
				DataTable not_works =
					db.GetDatatable(
						"SELECT event_date, notwork FROM GetCombinedCalendarForUser(@user_item_id, @start_date, @end_date)",
						DBParameters);

				//Diag.LogToConsole("AuthenticatedSession.item_id = " + AuthenticatedSession.item_id + ", min_date = " + min_date + ", max_date = " + max_date + ", not_works = " + not_works.Rows.Count);

				Dictionary<string, SimpleAllocationHours> changes = new Dictionary<string, SimpleAllocationHours>();

				foreach (DataRow row in allocations.Rows) {
					allocation_item_id = Conv.ToInt(row["allocation_item_id"]);
					int old_year = Conv.ToInt(row["year"]);
					int old_month = Conv.ToInt(row["month"]);
					List<DateAndDurationWithDay> old_values = new List<DateAndDurationWithDay>();
					int new_year = old_year;
					int new_month = old_month;
					List<DateAndDurationWithDay> new_values = new List<DateAndDurationWithDay>();

					if (date_mode == AllocationShiftDateModes.Months) {
						if (Conv.ToInt(not_works.Compute("SUM(notwork)",
							    "event_date >= #" + new DateTime(old_year, old_month, 1).ToString("yyyy-MM-dd") +
							    "# AND event_date < #" +
							    new DateTime(old_year, old_month, 1).AddMonths(1).ToString("yyyy-MM-dd") + "#")) !=
						    DateTime.DaysInMonth(old_year, old_month)) {
							int shift_count = 0;
							while (shift_count < Math.Abs(shift)) {
								new_month += adder;
								if (new_month < 1) {
									new_year--;
									new_month += 12;
								} else if (new_month > 12) {
									new_year++;
									new_month -= 12;
								}

								if (Conv.ToInt(not_works.Compute("SUM(notwork)",
									"event_date >= #" + new DateTime(new_year, new_month, 1).ToString("yyyy-MM-dd") +
									"# AND event_date < #" + new DateTime(new_year, new_month, 1).AddMonths(1)
										.ToString("yyyy-MM-dd") + "#")) != DateTime.DaysInMonth(new_year, new_month)) {
									shift_count++;
								}
							}

							DateAndDurationWithDay value = new DateAndDurationWithDay();
							value.hours = 0;
							value.day = 1;
							value.year = old_year;
							old_values.Add(value);

							int work_days_in_month = DateTime.DaysInMonth(new_year, new_month) - Conv.ToInt(
								not_works.Compute("SUM(notwork)",
									"event_date >= #" + new DateTime(new_year, new_month, 1).ToString("yyyy-MM-dd") +
									"# AND event_date < #" + new DateTime(new_year, new_month, 1).AddMonths(1)
										.ToString("yyyy-MM-dd") + "#"));

							//Diag.LogToConsole("allocation_item_id = " + row["allocation_item_id"] + ", hours = " + row["hours"] + ", old_year = " + old_year + ", old_month = " + old_month + ", new_year = " + new_year + ", new_month = " + new_month + ", work_days_in_month = " + work_days_in_month);

							DateTime start_date = new DateTime(new_year, new_month, 1);
							DateTime end_date = new DateTime(new_year, new_month, 1).AddMonths(1);
							while (start_date < end_date) {
								if (Conv.ToInt(not_works.Compute("SUM(notwork)",
									"event_date = #" + start_date.ToString("yyyy-MM-dd") + "#")) == 0) {
									value = new DateAndDurationWithDay();
									value.hours = Conv.ToDouble(row["hours"]) / work_days_in_month;
									value.day = start_date.Day;
									value.year = start_date.Year;
									new_values.Add(value);
								}

								start_date = start_date.AddDays(1);
							}

							if (!changes.ContainsKey(allocation_item_id + "_" + old_year + "_" + old_month)) {
								SimpleAllocationHours old_hours = new SimpleAllocationHours();
								old_hours.allocation_item_id = (int)allocation_item_id;
								old_hours.year = old_year;
								old_hours.month = old_month;
								old_hours.dates = old_values;
								changes.Add(allocation_item_id + "_" + old_year + "_" + old_month, old_hours);
							}

							if (move_history || new_year > DateTime.Today.Year || (new_year == DateTime.Today.Year &&
								new_month >= DateTime.Today.Month)) {
								SimpleAllocationHours new_hours = new SimpleAllocationHours();
								new_hours.allocation_item_id = (int)allocation_item_id;
								new_hours.year = new_year;
								new_hours.month = new_month;
								new_hours.dates = new_values;
								if (changes.ContainsKey(allocation_item_id + "_" + new_year + "_" + new_month)) {
									changes[allocation_item_id + "_" + new_year + "_" + new_month] = new_hours;
								} else {
									changes.Add(allocation_item_id + "_" + new_year + "_" + new_month, new_hours);
								}
							}
						}
					} else {
						// Weeks ???
					}
				}

				UpdateSimpleHours((int)date_mode, changes.Values.ToList());
			}
		}
	}

	public enum AllocationShiftDateModes {
		Months = 0,
		Weeks = 1
	}

	[Serializable]
	public class AllocationShift {
		public int process_item_id;
		public AllocationShiftDateModes date_mode;
		public int shift;
		public bool move_history;
		public int? allocation_item_id;
	}
}