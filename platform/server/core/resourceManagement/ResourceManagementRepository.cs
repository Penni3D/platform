﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DocumentFormat.OpenXml.Drawing.Charts;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.@base;
using DataTable = System.Data.DataTable;

namespace Keto5.x.platform.server.core.resourceManagement {
	public class ResourceManagementRepository : IResourceManagementRepository {
		private readonly IConnectionBase _connectionBase;

		private readonly IEntityServiceProvider _entityServiceProvider;

		public ResourceManagementRepository(IConnectionBase cb, IEntityServiceProvider entityServiceProvider) {
			_connectionBase = cb;
			_entityServiceProvider = entityServiceProvider;
		}

		public Dictionary<string, object> GetItem(int id) {
			return GetAllocationEntityService().GetItem(id, checkRights: false);
		}

		public void Delete(int itemId) {
			GetAllocationEntityService().Delete(itemId);
		}

		/// <summary>
		/// Deletes all durations for given allocationItemId and returns amount of hours that existed before
		/// </summary>
		/// <param name="allocationItemId"></param>
		/// <returns>Amount of hours that was removed</returns>
		public void DeleteAllocationHours(int allocationItemId) {
			var allocationIdParam = _connectionBase.SetDBParameter("@allocationId", allocationItemId);
			var allocationCondition = new SqlCondition("allocation_item_id", SqlComp.EQUALS, allocationIdParam);

			var deleteQuery = _connectionBase.BuildDeleteQuery()
				.From("allocation_durations")
				.Where(allocationCondition);

			_connectionBase.db.ExecuteDeleteQuery(deleteQuery, _connectionBase.DBParameters);
		}

		public void DeleteAllocationHours(int allocationItemId, int year, int month) {
			var allocationIdParam = _connectionBase.SetDBParameter("@allocationId", allocationItemId);
			var yearParam = _connectionBase.SetDBParameter("@year", year);
			var monthParam = _connectionBase.SetDBParameter("@month", month);
			var deleteQuery = _connectionBase.BuildDeleteQuery()
				.From("allocation_durations")
				.Where(
					new SqlCondition("allocation_item_id", SqlComp.EQUALS, allocationIdParam)
						.And(new SqlCondition("month(date)", SqlComp.EQUALS, monthParam)
							.And(new SqlCondition("year(date)", SqlComp.EQUALS, yearParam))));
			_connectionBase.db.ExecuteDeleteQuery(deleteQuery, _connectionBase.DBParameters);
		}

		public void DeleteAllocationHours(int allocationItemId, DateTime date_start, DateTime date_end) {
			var allocationIdParam = _connectionBase.SetDBParameter("@allocationId", allocationItemId);
			var date_start_param = _connectionBase.SetDBParameter("@date_start", date_start);
			var date_end_param = _connectionBase.SetDBParameter("@date_end", date_end);
			var deleteQuery = _connectionBase.BuildDeleteQuery()
				.From("allocation_durations")
				.Where(
					new SqlCondition("allocation_item_id", SqlComp.EQUALS, allocationIdParam)
						.And(new SqlCondition("date", SqlComp.GREATER_OR_EQUAL, date_start_param)
							.And(new SqlCondition("date", SqlComp.LESS_THAN_OR_EQUAL, date_end_param))));
			_connectionBase.db.ExecuteDeleteQuery(deleteQuery, _connectionBase.DBParameters);
		}

		public string GetProcessNameForProcessItemId(int processItemId) {
			return Cache.GetItemProcess(processItemId, _connectionBase.instance);
		}

		public int InsertItemRow() {
			return GetAllocationEntityService().InsertItemRow();
		}

		public void SaveItem(Dictionary<string, object> item, bool internalUpdate = false) {
			var service = GetAllocationEntityService();
			service.SaveItem(item, returnSubqueries: false, checkRights: !internalUpdate,
				preventSpecialProcessChecks: internalUpdate);
		}


		public void InsertAllocationHours(int allocationItemId, List<DateAndDuration> dateAndDurations) {
			_connectionBase.SetDBParameter("@allocationId", allocationItemId);
			var insertSqls = GetSqlsForInserts(dateAndDurations);
			_connectionBase.db.ExecuteInsertQuery(insertSqls, _connectionBase.DBParameters);
		}

		public IDatabaseItemService GetEntityService() {
			return _entityServiceProvider.GetEntityService("allocation");
		}

		public Dictionary<string, DateTime?> GetAllocationStartAndEndDate(int itemId) {
			_connectionBase.SetDBParameter("@itemId", itemId);
			var r = _connectionBase.db.GetDataRow(
				"SELECT MIN(date) AS start_date, MAX(date) AS end_date FROM allocation_durations WHERE allocation_item_id = @itemId",
				_connectionBase.DBParameters);

			var result = new Dictionary<string, DateTime?>();
			if (r == null)
				return result;

			result.Add("start_date", Conv.ToDateTime(r["start_date"]));
			result.Add("end_date", Conv.ToDateTime(r["end_date"]));

			return result;
		}

		public List<Dictionary<string, object>> GetResourcesOfCostCenter(int costCenterId) {
			var instanceCondition = new SqlCondition("instance", SqlComp.EQUALS, _connectionBase.instance);
			var userProcessCondition = new SqlCondition("process", SqlComp.EQUALS, "'user'");

			var costCenterParam = _connectionBase.SetDBParameter("@costCenterId", costCenterId);
			var query = new SelectQuery()
				.Columns("u.*")
				.From(_connectionBase.GetProcessTableName("user"))
				.Join(new Join("item_data_process_selections", "idps").On("idps.item_id", "u.item_id"))
				.Join(new Join("item_columns", "ic").On("ic.item_column_id", "idps.item_column_id"))
				.Where(
					instanceCondition
						.And(userProcessCondition)
						.And(new SqlCondition("name", SqlComp.EQUALS, "'cost_center'"))
						.And(new SqlCondition("idps.selected_item_id", SqlComp.EQUALS, costCenterParam)));

			return _connectionBase.db.GetDatatableDictionary(query, _connectionBase.DBParameters);
		}

		public List<Dictionary<string, object>> GetAllocationsForUserOrWithStatusOverDraft(List<int> resourceIds,
			int userId, string portfolioFilters = "") {
			var allocationTableName = _connectionBase.GetProcessTableName("allocation");
			var userIdCondition = _connectionBase.SetDBParameter("@orderNo", userId);
			var userIdParam = Modi.JoinListWithComma(resourceIds, true);

			//Change to show drafts to everyone
			// var sqlForAllocations = "SELECT *, " + GetAllocationDateSQ() + " FROM " + allocationTableName + " alloc " +
			//                         "WHERE resource_item_id IN " + userIdParam + " " +
			//                         "AND (alloc.creator_item_id = " + userIdCondition.ParameterName +
			//                         " OR alloc.status >= 13) " + portfolioFilters.Replace("pf.", "alloc.");
			var sqlForAllocations = "SELECT *, " + GetAllocationDateSQ() + " FROM " + allocationTableName + " alloc " +
			                        "WHERE resource_item_id IN " + userIdParam + " " +
			                        "AND alloc.status >= 0 " + portfolioFilters.Replace("pf.", "alloc.");
			return _connectionBase.db.GetDatatableDictionary(sqlForAllocations, _connectionBase.DBParameters);
		}

		private string GetAllocationDateSQ() {
			return
				"(SELECT MIN(date) FROM allocation_durations WHERE allocation_durations.allocation_item_id = alloc.item_id) AS allocation_start_date,(SELECT MAX(date) FROM allocation_durations WHERE allocation_durations.allocation_item_id = alloc.item_id) AS allocation_end_date";
		}

		public List<Dictionary<string, object>> GetAllocationsForUser(int userId, DateTime? fromDate,
			List<int> wantedStatuses = null, string portfolioFilters = "") {
			var allocationTableName = _connectionBase.GetProcessTableName("allocation");
			var userIdCondition = _connectionBase.SetDBParameter("@orderNo", userId);
			var statusCond = "alloc.status>1";
			if (wantedStatuses != null) {
				var statuses = string.Join(",", wantedStatuses);
				statusCond = "alloc.status IN (" + statuses + ")";
			}

			if (fromDate != null) {
				statusCond += " AND alloc." + ResourceManagementCommonService.ItemIdColumnName +
				              " IN (SELECT " + ResourceManagementCommonService.AllocationItemIdColumnName +
				              " FROM " + ResourceManagementCommonService.AllocationDurationTableName +
				              " WHERE " + ResourceManagementCommonService.DateColumnName +
				              " >= '" + ((DateTime) fromDate).ToString("yyyy-MM-dd") + "')";
			}

			var sqlForAllocations = "SELECT alloc.*, " + GetAllocationDateSQ() + " , i.process AS process_name FROM " +
			                        allocationTableName +
			                        " alloc " +
			                        "INNER JOIN items i ON i.item_id = alloc.process_item_id " +
			                        "WHERE resource_item_id = " + userIdCondition.ParameterName +
			                        " AND " + statusCond + portfolioFilters.Replace("pf.", "alloc.")
				                        .Replace(" pf ", " alloc ");

			return _connectionBase.db.GetDatatableDictionary(sqlForAllocations, _connectionBase.DBParameters);
		}

		public List<Dictionary<string, object>> GetAllocationsForTaskId(int taskId) {
			var taskIdParam = _connectionBase.SetDBParameter("@taskId", taskId);
			return _connectionBase.BuildSelectQuery()
				.From(_connectionBase.GetProcessTableName("allocation"))
				.Where(new SqlCondition("task_item_id", SqlComp.EQUALS, taskIdParam))
				.Fetch();
		}

		private Dictionary<double, List<DateTime>> ChainDurationDates(List<DateAndDuration> durations) {
			var durationsByHours = new Dictionary<double, List<DateTime>>();

			durations.ForEach(x => {
				if (!durationsByHours.ContainsKey(x.hours)) {
					durationsByHours.Add(x.hours, new List<DateTime>());
				}

				var date = DateTime.Parse(x.date);
				durationsByHours[x.hours].Add(date);
			});
			return durationsByHours;
		}

		private List<InsertQuery> GetSqlsForInserts(List<DateAndDuration> durations) {
			var insertQueries = new List<InsertQuery>();
			var groupedDates = ChainDurationDates(durations); // group dates by hour to spare sql parameters
			var keyIndex = 0;
			var dateIndex = 0;
			foreach (var groupedDatesKey in groupedDates.Keys) {
				groupedDates[groupedDatesKey].ForEach(x => {
					insertQueries.Add(new InsertQuery()
						.To("allocation_durations")
						.Value("date", "'" + x + "'")
						.Value("hours", groupedDatesKey)
						.Value("allocation_item_id", "@allocationId"));
					dateIndex++;
				});
				keyIndex++;
			}

			return insertQueries;
		}

		private IDatabaseItemService GetAllocationEntityService() {
			return _entityServiceProvider.GetEntityService("allocation");
		}

		public DataTable GetAllocationTotalsById(int processItemId, DateTime startDate = new DateTime(),
			DateTime endDate = new DateTime(), int grouping = 0) {
			_connectionBase.SetDBParam(SqlDbType.Int, "@processItemId", processItemId);
			_connectionBase.SetDBParam(SqlDbType.Date, "@startDate", startDate);
			_connectionBase.SetDBParam(SqlDbType.Date, "@endDate", endDate);

			var mode = "";
			switch (grouping) {
				case 1:
					mode = ", resource_item_id";
					break;
				case 2:
					mode = ", task_item_id";
					break;
				case 3:
					mode = ", task_item_id, resource_item_id";
					break;
			}

			
			//DOES NOT RETURN COMPETENCIES -- BSa 22.10.2021
			var sql =
				"SELECT SUM(hours) AS hours, date" + mode + " FROM _" + _connectionBase.instance + "_allocation a " +
				"LEFT JOIN allocation_durations ad ON a.item_id = ad.allocation_item_id " +
				"WHERE a.resource_type = 'user' AND a.status IN (13, 20, 25, 40, 50, 55) AND process_item_id = @processItemId AND date BETWEEN @startDate AND @endDate " +
				"GROUP BY date" + mode + " ORDER BY date";

			return _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters);
		}

		public Dictionary<int, TimeAnalysisRow> GetAnalysisRows(int processItemId) {
			_connectionBase.SetDBParam(SqlDbType.Int, "@processItemId", processItemId);
			var result = new Dictionary<int, TimeAnalysisRow>();
			
			var sql =
				"SELECT *, ISNULL((select CAST(idps.selected_item_id AS nvarchar) + ',' FROM item_data_process_selections idps " +
				"where idps.item_column_id = (select item_column_id from item_columns where name = 'responsibles' AND process = 'task') and idps.item_id = t.item_id " +
				"for xml path('')),'') AS users, (SELECT task_type FROM _" + _connectionBase.instance +
				"_task WHERE item_id = t.parent_item_id) as parent_type FROM _" + _connectionBase.instance +
				"_task t WHERE owner_item_id = @processItemId";

			foreach (DataRow r in _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters).Rows) {
				var row = new TimeAnalysisRow(r);

				if (!result.ContainsKey(Conv.ToInt(r["item_id"]))) {
					result.Add(Conv.ToInt(r["item_id"]), row);
				}
			}

			return result;
		}

		// simple allocation stuff
		public Dictionary<string, object> GetSimpleAllocations(int processItemId, int year, int mode, string process,
			string filter_item_ids, int portfolio_id, AuthenticatedSession session) {
			if (mode == 1) {
				_connectionBase.SetDBParameter("@from", Util.StartOfWeek(year, 1));
				_connectionBase.SetDBParameter("@to", Util.EndOfWeek(year + 1, 1).AddDays(-7));
			} else {
				_connectionBase.SetDBParameter("@year", year);
			}

			_connectionBase.SetDBParameter("@process", process);
			string allocationTable = _connectionBase.GetProcessTableName("allocation");
			string allocationItemsQuery = "SELECT a.* " +
			                              "FROM " + allocationTable + " a " +
			                              "LEFT JOIN items i ON i.item_id = a.process_item_id " +
			                              "WHERE i.process = @process";
			if (processItemId > 0) {
				_connectionBase.SetDBParameter("@processItemId", processItemId);
				allocationItemsQuery += " AND a.process_item_id = @processItemId";
			} else {
				allocationItemsQuery +=
					" AND a.resource_item_id IN (" + filter_item_ids + ") AND a.resource_item_id > 0";
			}

			List<Dictionary<string, object>> allocationItems =
				_connectionBase.db.GetDatatableDictionary(allocationItemsQuery, _connectionBase.DBParameters);

			List<Dictionary<string, object>> portfolio = null;
			if (processItemId == 0) {
				ItemBase ib = new ItemBase(_connectionBase.instance, process, session);
				string sql = "SELECT pf.item_id FROM " + _connectionBase.GetProcessTableName(process) + " pf WHERE " +
				             ib.EntityRepository.GetWhere(portfolio_id);
				portfolio = _connectionBase.db.GetDatatableDictionary(sql, _connectionBase.DBParameters);
			}

			if (allocationItems.Count > 0) {
				Dictionary<int, Dictionary<string, object>> allocationsInDict =
					new Dictionary<int, Dictionary<string, object>>();
				allocationItems.ForEach(x => {
					int id = Conv.ToInt(x["item_id"]);
					x["process"] = "allocation";
					x["task_type"] = "50";

					if (Conv.ToStr(x["resource_type"]) == "user") {
						x["user_item_id"] = x["resource_item_id"];
					} else {
						x["user_item_id"] = 0;
					}

					if (processItemId == 0 && Conv.ToInt(x["process_item_id"]) > 0) {
						x["edit"] = portfolio.Exists(y => (int) y["item_id"] == Conv.ToInt(x["process_item_id"]));
					} else {
						x["edit"] = true;
					}

					allocationsInDict.Add(id, x);
				});

				string itemIds = string.Join(",", allocationsInDict.Keys);

				string hourQuery = "";
				if (mode == 1) {
					hourQuery = "SELECT allocation_item_id, month, SUM(hours) AS hours FROM " +
					            "( " +
					            "	SELECT allocation_item_id, DATEPART(ISO_WEEK, date) AS month, hours " +
					            "	FROM allocation_durations " +
					            "	WHERE date BETWEEN @from AND @to AND allocation_item_id IN (" + itemIds + ")" +
					            ") q " +
					            "GROUP BY allocation_item_id, month " +
					            "ORDER BY allocation_item_id, month";
				} else {
					hourQuery = "SELECT allocation_item_id, month, SUM(hours) AS hours FROM " +
					            "( " +
					            "	SELECT allocation_item_id, MONTH(date) AS month, hours " +
					            "	FROM allocation_durations " +
					            "	WHERE YEAR(date) = @year AND allocation_item_id IN (" + itemIds + ")" +
					            ") q " +
					            "GROUP BY allocation_item_id, month " +
					            "ORDER BY allocation_item_id, month";
				}

				List<Dictionary<string, object>> hours =
					_connectionBase.db.GetDatatableDictionary(hourQuery, _connectionBase.DBParameters);

				foreach (var hourItem in hours) {
					int allocId = (int) hourItem["allocation_item_id"];
					if (!allocationsInDict.ContainsKey(allocId))
						continue;
					Dictionary<string, object> allocationForHour = allocationsInDict[allocId];

					if (!allocationForHour.ContainsKey("month")) {
						allocationForHour.Add("month", new Dictionary<int, double>());
					}

					Dictionary<int, double> monthDictionary = (Dictionary<int, double>) allocationForHour["month"];
					int month = (int) hourItem["month"];
					if (!monthDictionary.ContainsKey(month) && (double) hourItem["hours"] > 0) {
						monthDictionary.Add(month, Math.Round((double) hourItem["hours"], 2));
					}
				}
			}

			Dictionary<string, object> resultDictionary = new Dictionary<string, object> {
				{"allocations", allocationItems}
			};

			return resultDictionary;
		}

		public void DeleteSimpleAllocation(int allocationItemId) {
			Delete(allocationItemId);
			DeleteAllocationHours(allocationItemId);
		}

		public class TimeAnalysisRow {
			public TimeAnalysisRow(DataRow row, string alias = "") {
				if (alias.Length > 0) {
					alias = alias + "_";
				}

				ItemId = Conv.ToInt(row["item_id"]);
				TaskType = Conv.ToInt(row["task_type"]);
				ParentItemId = Conv.ToInt(row["parent_item_id"]);
				Title = Conv.ToStr(row["title"]);
				ParentType = Conv.ToInt(row["parent_type"]);
				OrderNo = Conv.ToStr(row["order_no"]);
				var parsedUsers = Conv.ToStr(row["users"]).TrimEnd(',');
				TaskResponsibles = parsedUsers.Length > 0
					? parsedUsers.Split(',').Select(int.Parse).ToList()
					: new List<int>();
			}

			public TimeAnalysisRow() { }

			public int ItemId { get; set; }
			public int TaskType { get; set; }
			public int ParentItemId { get; set; }
			public string OrderNo { get; set; }
			public string Title { get; set; }

			public int ParentType { get; set; }

			public List<int> TaskResponsibles { get; set; }
		}
	}
}