﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;

namespace Keto5.x.platform.server.core.resourceManagement {
	public class ResourceManagementQueryService : ResourceManagementCommonService {
		private readonly string _allocationDurationsTableName;
		private readonly string _allocationTableName;
		private readonly string _itemDataProcessSelectionsTableName;

		public ResourceManagementQueryService(string instance, AuthenticatedSession session,
			IResourceManagementRepository repository,
			DateTime? archiveDate = null)
			: base(instance, session, repository, archiveDate) {
			_allocationTableName = GetProcessTableName("allocation");
			if (archiveDate == null) {
				_allocationDurationsTableName = "allocation_durations";
				_itemDataProcessSelectionsTableName = "item_data_process_selections";
			} else {
				var d = (DateTime)archiveDate;
				SetDBParameter("@archiveDate", d.Date);
				_allocationDurationsTableName = "get_allocation_durations(@archiveDate)";
				_itemDataProcessSelectionsTableName = "get_item_data_process_selections(@archiveDate)";
			}
		}

		public List<Dictionary<string, object>> GetCompetencies(IEnumerable<int> competencyIds) {
			var itemBase = new ItemBase(instance, "competency", AuthenticatedSession);
			List<Dictionary<string, object>> items;
			if (competencyIds == null || !competencyIds.Any()) {
				items = db.DataTableToDictionary(itemBase.GetAllData());
			} else {
				items = db.DataTableToDictionary(itemBase.GetAllData(string.Join(",", competencyIds)));
			}

			var itemsInDictionary = new Dictionary<int, Dictionary<string, object>>();
			foreach (var item in items) {
				var id = Conv.ToInt(item["item_id"]);
				if (id != 0) {
					itemsInDictionary.Add(id, item);
				}
			}

			SetDBParameter("@costCenter", "list_cost_center");
			var costCenterQuery =
				"SELECT idps.item_id competency_id, idps.selected_item_id cost_center_id FROM " +
				_itemDataProcessSelectionsTableName + " idps " +
				"JOIN items_all i ON i.item_id = idps.selected_item_id " +
				"WHERE idps.item_id IN (" + string.Join(",", itemsInDictionary.Keys) +
				") AND i.process='list_cost_center' ";
			if (itemsInDictionary.Keys.Count > 0) {
				var costCenters = db.GetDatatableDictionary(costCenterQuery, DBParameters);
				foreach (var competencyCostCenterPair in costCenters) {
					var compId = Conv.ToInt(competencyCostCenterPair["competency_id"]);
					if (itemsInDictionary.ContainsKey(compId)) {
						var compRow = itemsInDictionary[compId];
						List<int> costCenterList;
						if (!compRow.ContainsKey("cost_center")) {
							costCenterList = new List<int>();
							compRow.Add("cost_center", costCenterList);
						} else {
							costCenterList = (List<int>)compRow["cost_center"];
						}

						costCenterList.Add(Conv.ToInt(competencyCostCenterPair["cost_center_id"]));
					}
				}
			}

			return items;
		}

		public List<Dictionary<string, object>> GetCostCenterCompetencies() {
			var itemBase = new ItemBase(instance, "list_cost_center", AuthenticatedSession);
			return db.DataTableToDictionary(itemBase.GetAllData());
		}

		private Dictionary<int, List<Dictionary<string, object>>> GetParentListForAllocations(
			List<Dictionary<string, object>> allocations) {
			var processes = allocations.GroupBy(dict => dict["resource_item_id"],
				dict => dict["process_item_id"], (key, items) => new {
					resource_item_id = key,
					allocations = items
				});

			var parentList = new Dictionary<int, List<Dictionary<string, object>>>();
			foreach (var p in processes) {
				var uniqueAllocations = p.allocations.GroupBy(x => x, (key) => new { process_item_id = key });
				var allocationProcesses = new List<Dictionary<string, object>>();
				foreach (var processItemId in uniqueAllocations) {
					var i = Conv.ToInt(processItemId.Key);
					var e = new ItemBase(instance, Cache.GetItemProcess(i, instance), AuthenticatedSession);
					allocationProcesses.Add(e.GetItem(i, true, false, true));
				}

				parentList.Add(Conv.ToInt(p.resource_item_id), allocationProcesses);
			}

			return parentList;
		}

		public Dictionary<string, object> GetDurationsForUser(int userId, DateTime? dateParam = null,
			string portfolioFilters = "") {
			var excludeStatuses = new List<AllocationStatus> { AllocationStatus.Draft };
			var allocations = GetAllocationsForUser(userId, dateParam, excludeStatuses, portfolioFilters);
			var entityRepository = new ItemBase(instance, "user", AuthenticatedSession);

			var resources = entityRepository.GetItem(userId, true, false, true);
			var resourceList = new List<Dictionary<string, object>> { resources };
			return GetDurationsForAllocations(allocations, resourceList,
				parents: GetParentListForAllocations(allocations));
		}

		public Dictionary<string, object> GetDurationsForUserLimited(int userId, string startDate, string endDate, int mode,
			string portfolioFilters = "") {
			var excludeStatuses = new List<AllocationStatus> { AllocationStatus.Draft };
			var allocations = GetAllocationsForUser(userId, startDate, excludeStatuses, portfolioFilters);
			var entityRepository = new ItemBase(instance, mode == 1 ? "user" : "competency", AuthenticatedSession);

			var resources = entityRepository.GetItem(userId, true, false, true);
			var resourceList = new List<Dictionary<string, object>> { resources };
			return GetDurationsForAllocations(allocations, resourceList,
				parents: GetParentListForAllocations(allocations), filterStart: startDate, filterEnd: endDate);
		}


		public Dictionary<string, object> GetDurationsForUsers(string userIds, DateTime? dateParam = null) {
			/*var userIdList = userIds.Split("-");
			
			var allocations = GetAllocationsForUser(userId, GetDateForCutter(dateParam));
			var resources = GetResourceForUserId(userId);
			return GetDurationsForAllocations(allocations, resources);*/
			return null;
		}

		public Dictionary<string, object> GetDurationsForCostCenter(DateTime? dateParam = null, string filterStart = "",
			string filterEnd = "", List<int> filterIds = null, List<int> competencyFilters = null) {
			var resources = GetResourcesByCostCenter(filterIds, competencyFilters);
			var allocations = GetAllocationsFromCostCenters(dateParam, resources);
			return GetDurationsForAllocations(allocations, resources, filterStart: filterStart, filterEnd: filterEnd,
				parents: GetParentListForAllocations(allocations));
		}

		private Dictionary<string, object> GetDurationsForAllocations(
			List<Dictionary<string, object>> allocations,
			List<Dictionary<string, object>> resources, string filterStart = "", string filterEnd = "",
			Dictionary<int, List<Dictionary<string, object>>> parents = null) {
			var allocationIds = new List<int>();
			var allocationStatus = new Dictionary<int, int>();
			var competencyIds = new HashSet<int>();
			var calendarDataObject = new CalendarDataObject();
			List<Dictionary<string, object>> comments = null;

			foreach (var allocation in allocations) {
				if (allocation.ContainsKey("process")) allocation["process"] = "allocation";
				else allocation.Add("process", "allocation");

				var allocationItemId = Conv.ToInt(allocation[ItemIdColumnName]);
				var resourceType = Conv.ToStr(allocation[ResourceTypeColumnName]);
				var resourceItemId = allocation[ResourceItemIdColumnName];
				var processItemId = Conv.ToInt(allocation["process_item_id"]);


				if (resourceType.Equals("competency")) competencyIds.Add(Conv.ToInt(resourceItemId));

				//Set Parent Id
				SetParentItemIdForObject(resourceItemId, allocation);
				if (parents != null) allocation["parent_item_id"] = resourceItemId + "_" + processItemId;


				allocationIds.Add(allocationItemId);
				var allocStatus = Conv.ToInt(allocation[StatusColumnName]);
				if (allocStatus > 0) allocationStatus.Add(allocationItemId, allocStatus);
			}

			if (allocationIds.Count > 0) {
				var idsInString = Modi.JoinListWithComma(allocationIds, false);

				var limitWhere = (filterStart != "" && filterEnd != "")
					? " allocation_durations.date BETWEEN '" + Conv.ToSql(filterStart) + "' AND '" +
					  Conv.ToSql(filterEnd) + "'"
					: "";

				var conditions = new ConditionGroup()
					.Operation(SqlOperator.AND)
					.Add(new SqlCondition().Column(AllocationItemIdColumnName).In(idsInString));
				if (limitWhere != "") conditions.Add(new SqlCondition().RawSql(limitWhere));

				var durationsForAllocations = BuildSelectQuery()
					.From(AllocationDurationTableName)
					.Where(conditions)
					.Fetch();

				foreach (var row in durationsForAllocations) {
					var date = (DateTime)Conv.ToDateTime(row[DateColumnName]);
					var allocationId = Conv.ToInt(row[AllocationItemIdColumnName]);
					var status = allocationStatus.ContainsKey(allocationId)
						? allocationStatus[allocationId]
						: (int)AllocationStatus.Draft;
					calendarDataObject.SetHoursForItem(
						date,
						Conv.ToStr(row[AllocationItemIdColumnName]),
						(double)row[HoursColumnName],
						status.ToString());
				}

				var userTableName = GetProcessTableName("user");
				comments = BuildSelectQuery()
					.Columns("ac.*", "u.first_name + ' ' + u.last_name AS author_name ")
					.From("allocation_comments", "ac")
					.Join(new Join(userTableName, "u").On("u.item_id", "ac.author_item_id"))
					.Where(new SqlCondition().Column("ac.allocation_item_id").In(idsInString))
					.Fetch();
			}

			var commentsByAllocationId = new Dictionary<int, List<object>>();
			if (comments != null) {
				foreach (var comment in comments) {
					var allocationId = Conv.ToInt(comment[AllocationItemIdColumnName]);
					if (!commentsByAllocationId.ContainsKey(allocationId)) {
						commentsByAllocationId.Add(allocationId, new List<object>());
					}

					commentsByAllocationId[allocationId].Add(comment);
				}
			}


			calendarDataObject.AddRowData(resources);
			foreach (var resource in resources) {
				var resourceId = Conv.ToInt(resource["item_id"]);
				if (parents != null) {
					if (parents.ContainsKey(resourceId)) {
						var resourceParents = parents[resourceId];
						foreach (var parent in resourceParents) {
							parent["item_id"] = resourceId + "_" + parent["item_id"];
							parent["actual_item_id"] = parent["item_id"];
							parent["parent_item_id"] = resourceId;
							parent["cost_center"] = resource["cost_center"];

							foreach (var a in allocations) {
								if (Conv.ToStr(a["parent_item_id"]) == Conv.ToStr(parent["item_id"]))
									parent["cost_center"] = a["cost_center"];
							}
						}

						calendarDataObject.AddRowData(resourceParents);
					}
				}

				resource["process"] = "user"; // todo: fix to match resource type
				resource["parent_item_id"] = 0;
				resource.Remove("password");
				resource.Remove("token");
			}

			if (competencyIds.Count > 0) {
				var competencies = GetCompetencies(competencyIds);
				var competenciesById = new Dictionary<int, object>();

				foreach (var competency in competencies) {
					competenciesById.Add(Conv.ToInt(competency[ItemIdColumnName]), competency);
					competency[ProcessColumnName] = "competency";
					competency[ParentItemIdColumnName] = 0;
				}

				calendarDataObject.AddRowData(competencies);
			}

			calendarDataObject.AddRowData(allocations);
			calendarDataObject.AddAdditionalProperty("comments", commentsByAllocationId);

			return calendarDataObject.GetObject();
		}

		private void ConvertCostCenterToArray(Dictionary<string, object> allocation) {
			var costCenterId = Conv.ToInt(allocation[CostCenterColumnName]);
			allocation[CostCenterColumnName] = new List<int> { costCenterId };
		}

		public List<int> GetMyCostCenterIds() {
			var items = GetMyCostCenters();
			var ids = (from item in items select Conv.ToInt(item["item_id"])).ToList();
			return ids;
		}

		private List<Dictionary<string, object>> GetAllocationsForUser(
			int userId,
			DateTime? fromDate = null,
			List<AllocationStatus> excludedStatuses = null, string portfolioFilters = "") {
			var statusesToShow = new List<int> {
				(int)AllocationStatus.PreviouslyApproved,
				(int)AllocationStatus.Requested,
				(int)AllocationStatus.RequestedModified,
				(int)AllocationStatus.Approved,
				(int)AllocationStatus.ApprovedWithModifications,
				(int)AllocationStatus.Denied,
				(int)AllocationStatus.StatusForTasks
			};

			if (excludedStatuses != null) {
				statusesToShow = statusesToShow.Where(x => !excludedStatuses.Contains((AllocationStatus)x)).ToList();
			}

			var processes = new Processes(instance, AuthenticatedSession);

			var allocationForUser =
				_resourceManagementRepository.GetAllocationsForUser(userId, fromDate, statusesToShow, portfolioFilters);

			foreach (var d in allocationForUser) {
				d.Add("process_translation", processes.GetProcessPrimary((int)d["process_item_id"]));
			}

			return allocationForUser;
		}

		private List<Dictionary<string, object>> GetAllocationsForUser(
			int userId,
			string fromDate = null,
			List<AllocationStatus> excludedStatuses = null, string portfolioFilters = "") {
			return GetAllocationsForUser(userId, Conv.ToDate(fromDate), excludedStatuses, portfolioFilters);
		}

		private List<Dictionary<string, object>> GetAllocationsFromCostCenters(DateTime? fromDate,
			List<Dictionary<string, object>> resources, List<int> competencyFilters = null) {
			var statusesToShowToOwner = new List<int> {
				(int)AllocationStatus.Requested,
				(int)AllocationStatus.RequestedModified,
				(int)AllocationStatus.Approved,
				(int)AllocationStatus.ApprovedWithModifications,
				(int)AllocationStatus.Denied,
				(int)AllocationStatus.StatusForTasks,
				(int)AllocationStatus.PreviouslyApproved
			};
			var statusesToShowForCostCenterOwner = Modi.JoinListWithComma(statusesToShowToOwner, false);

			var myCostCenterIds = GetMyCostCenterIds();
			if (myCostCenterIds == null || myCostCenterIds.Count == 0) {
				return new List<Dictionary<string, object>>();
			}

			//User IDs
			var userIDs = resources.Select(x => Conv.ToInt(x["item_id"])).ToList();
			var processes = new Processes(instance, AuthenticatedSession);
			var whereClause = new SqlCondition().Column(StatusColumnName).In(statusesToShowForCostCenterOwner).And(
				new SqlCondition().Column(ResourceItemIdColumnName).In(userIDs));

			if (fromDate != null) {
				var dateString = "'" + ((DateTime)fromDate).ToString("yyyy-MM-dd") + "'";
				whereClause = whereClause.And(
					new SqlCondition()
						.Column(ItemIdColumnName)
						.In(BuildSelectQuery()
							.Columns(AllocationItemIdColumnName)
							.From(AllocationDurationTableName)
							.Where(new SqlCondition(DateColumnName, SqlComp.GREATER_OR_EQUAL, dateString))
						)
				);
			}

			var allocationsForMyCostCenters = BuildSelectQuery()
				.Columns(" alloc.*",
					"'allocation' AS process")
				.From(GetProcessTableName(AllocationProcessName), "alloc")
				.Where(whereClause);

			var data = allocationsForMyCostCenters.Fetch();
			foreach (var d in data) {
				d.Add("process_name", processes.GetProcessByItemId((int)d["process_item_id"]));
				d.Add("process_translation", processes.GetProcessPrimary((int)d["process_item_id"]));
			}

			return data;
		}

		private List<Dictionary<string, object>> GroupCostCentersForCompetencies(
			List<Dictionary<string, object>> competencies) {
			var newList = new List<Dictionary<string, object>>();
			var cursorItemId = 0;
			var costCenterArray = new List<int>();
			for (var i = 0; i < competencies.Count; i++) {
				var curr = competencies[i];
				var currItemId = Conv.ToInt(curr[ItemIdColumnName]);
				var costCenterOfCurrent = Conv.ToInt(curr["_cost_center"]);
				if (i > 0 && cursorItemId.Equals(currItemId)) {
					costCenterArray.Add(costCenterOfCurrent);
				} else {
					newList.Add(curr);
					cursorItemId = currItemId;
					costCenterArray = new List<int> { costCenterOfCurrent };
					curr["cost_center"] = costCenterArray;
				}
			}

			return newList;
		}

		protected void SetParentItemIdForObject(object parentId, Dictionary<string, object> dict) {
			dict[ParentItemIdColumnName] = Conv.ToInt(parentId);
		}

		public Dictionary<string, object> GetDurationsByProcessesAndUsers(List<int> ids, string portfolioFilters = "",
			string startDate = "", string endDate = "") {
			var calendarDataObject = new CalendarDataObject();
			ids.ForEach(x => {
				GetDurationsByProcessAndUsers(x, calendarDataObject, true, portfolioFilters: portfolioFilters,
					limitStart: startDate, limitEnd: endDate);
			});
			return calendarDataObject.GetObject();
		}

		private Join GetItemDataProcessSelectionJoin() {
			return new Join(_itemDataProcessSelectionsTableName, "idps").On("idps.item_id", "r.item_id");
		}

		private Join GetListCostCenterJoin() {
			return new Join(GetProcessTableName("list_cost_center"), "lcc").On("lcc.item_id", "idps.selected_item_id");
		}

		private SqlCondition GetProcessItemComparisonCondition(int processId) {
			var processIdSqlParameter = SetDBParameter("@processId", processId);
			return new SqlCondition(ProcessItemIdColumnName, SqlComp.EQUALS, processIdSqlParameter);
		}

		private List<Dictionary<string, object>>
			GetUsersWithAllocationForProcess(int processId, int? resourceId = null) {
			var whereClauses = GetProcessItemComparisonCondition(processId);

			if (resourceId != null) {
				var resourceParam = SetDBParameter("@resourceLimiter", resourceId);
				whereClauses =
					whereClauses.And(new SqlCondition("alloc.resource_item_id", SqlComp.EQUALS, resourceParam));
			}

			//Get Columns
			var pp = new ProcessPortfolios(instance, "user", AuthenticatedSession);
			var ucols = "";
			foreach (var col in pp.GetDefaultSelectorColumnNames()) {
				ucols += ",r." + col;
			}

			// find users that have allocation for process
			var users = BuildSelectQuery().Distinct()
				.Columns("r.item_id" + ucols, "lcc.item_id as _cost_center")
				.From(_allocationTableName, "alloc")
				.Join(new Join(GetProcessTableName("user"), "r").On("r.item_id", "alloc.resource_item_id"))
				.Join(GetItemDataProcessSelectionJoin())
				.Join(GetListCostCenterJoin())
				.Where(whereClauses)
				.Fetch();
			return users;
		}

		public CalendarDataObject GetOtherDurationsByUser(int processId, int userId) {
			var calendarDataObject = new CalendarDataObject();
			var allocations =
				_resourceManagementRepository.GetAllocationsForUserOrWithStatusOverDraft(new List<int> { userId },
					AuthenticatedSession.item_id);
			var allocationIds = new List<int>();
			var dictionaryForAllocations = new Dictionary<string, Dictionary<string, object>>();

			foreach (var alloc in allocations) {
				var itemId = Conv.ToInt(alloc["item_id"]);
				var resourceId = Conv.ToStr(alloc[ResourceItemIdColumnName]) + "_00";
				var currItemId = Conv.ToInt(alloc[ProcessItemIdColumnName]);

				if (currItemId == processId)
					continue;

				allocationIds.Add(itemId);

				if (dictionaryForAllocations.ContainsKey(resourceId)) {
					continue;
				}

				// we group allocations of other processes into one allocation item
				var allocationFromOtherProcess = new Dictionary<string, object>();
				allocationFromOtherProcess[ItemIdColumnName] = resourceId;
				allocationFromOtherProcess[ResourceItemIdColumnName] = Conv.ToStr(alloc[ResourceItemIdColumnName]);
				allocationFromOtherProcess[ProcessColumnName] = "allocation";
				allocationFromOtherProcess[StatusColumnName] = (int)AllocationStatus.StatusForExternalAllocationLoad;
				allocationFromOtherProcess[ParentItemIdColumnName] = Conv.ToStr(alloc[ResourceItemIdColumnName]);

				dictionaryForAllocations.Add(resourceId, allocationFromOtherProcess);
			}

			if (dictionaryForAllocations.Count == 1) {
				calendarDataObject.AddRowData(dictionaryForAllocations.First().Value);
			}

			SetDBParam(SqlDbType.Int, "@minimumStatus", AllocationStatus.Draft);
			// find all durations for allocations
			var allocationIdsString = string.Join(",", allocationIds);
			var queryForAllDurations =
				"SELECT alloc.process_item_id, alloc.resource_item_id, alloc.status, ad.date, ad.hours, ad.allocation_item_id " +
				"FROM " + _allocationDurationsTableName + " ad " +
				"JOIN _" + instance + "_allocation alloc ON alloc.item_id = ad.allocation_item_id " +
				"WHERE allocation_item_id IN (" + allocationIdsString + ")";

			var durations = string.IsNullOrEmpty(allocationIdsString)
				? new List<Dictionary<string, object>>()
				: db.GetDatatableDictionary(queryForAllDurations, DBParameters);

			foreach (var durationRow in durations) {
				var date = (DateTime)Conv.ToDateTime(durationRow[DateColumnName]);
				var hours = (double)durationRow[HoursColumnName];
				var status = Conv.ToInt(durationRow[StatusColumnName]).ToString();
				var itemId = Conv.ToStr(durationRow[ResourceItemIdColumnName]) + "_00";

				calendarDataObject.SetHoursForItem(date, itemId, hours, status);
			}

			return calendarDataObject;
		}

		public CalendarDataObject GetDurationsByProcessAndUsers(
			int processId,
			CalendarDataObject calendarDataObject = null,
			bool multipleProcesses = false,
			int? resourceId = null, string limitStart = "", string limitEnd = "", string portfolioFilters = "") {
			calendarDataObject ??= new CalendarDataObject();

			var processIdSqlParameter = SetDBParameter("@processId", processId);

			// Find process name by its item_id
			var processName = GetProcessName(processIdSqlParameter);
			var process = GetProcessForRowdataObject(processName, processIdSqlParameter);
			process.Add(ParentItemIdColumnName, 0);
			process.Add(ProcessColumnName, processName);
			process["task_type"] = TaskType.ProjectProcess;
			calendarDataObject.AddRowData(process);

			var users = GetUsersWithAllocationForProcess(processId, resourceId);
			var competencies = GetCompetencies(processId);

			if (users.Count == 0 && competencies.Count == 0) return calendarDataObject;

			var resourceIds = new List<int>();
			FetchAndSetOrganizationInformation(users);
			foreach (var user in users) {
				ConvertCostCenterToArray(user, "_cost_center");
				SetParentItemIdForObject(processId, user);
				user[ProcessColumnName] = "user";
				user["task_type"] = TaskType.UserWithAllocation;
				var userId = user["item_id"];
				if (multipleProcesses) {
					user["actual_item_id"] = user["item_id"];
				}

				var itemId = multipleProcesses ? processId + "_" + user["item_id"] : user["item_id"];
				user["item_id"] = itemId;
				user.Remove("password");
				calendarDataObject.AddRowData(user);
				resourceIds.Add((int)userId);
			}

			foreach (var competency in competencies) {
				ConvertCostCenterToArray(competency, "_cost_center");
				SetParentItemIdForObject(processId, competency);
				competency[ProcessColumnName] = "competency";
				competency["task_type"] = TaskType.UserWithAllocation;
				var competencyId = competency["item_id"];
				if (multipleProcesses) {
					competency["actual_item_id"] = competency["item_id"];
					competency["item_id"] = processId + "_" + competency["item_id"];
				}

				calendarDataObject.AddRowData(competency);
				resourceIds.Add((int)competencyId);
			}

			// find all allocations for users that we previously found
			var allocations =
				_resourceManagementRepository.GetAllocationsForUserOrWithStatusOverDraft(resourceIds,
					AuthenticatedSession.item_id, portfolioFilters);

			var dictionaryForAllocations = new Dictionary<string, Dictionary<string, object>>();
			var allocationIds = new List<int>();
			var allowedOtherAllocationsStatus = new[] {
				(int)AllocationStatus.PreviouslyApproved,
				(int)AllocationStatus.Requested,
				(int)AllocationStatus.RequestedModified,
				(int)AllocationStatus.ApprovedWithModifications,
				(int)AllocationStatus.Approved
			};

			foreach (var alloc in allocations) {
				SetParentItemIdForObject(alloc[ResourceItemIdColumnName], alloc);
				alloc[ProcessColumnName] = "allocation";
				alloc["task_type"] = TaskType.AllocationOrTask;
				var currItemId = Conv.ToInt(alloc[ProcessItemIdColumnName]);

				if (!processId.Equals(currItemId) && !multipleProcesses) {
					// we group allocations of other processes into one allocation item
					var itemId = Conv.ToStr(alloc[ResourceItemIdColumnName]) + "_00";
					var status = (int)alloc[StatusColumnName];

					if (!allowedOtherAllocationsStatus.Contains(status)) {
						continue;
					}

					var allocationFromOtherProcess = new Dictionary<string, object>();
					allocationFromOtherProcess[ItemIdColumnName] = itemId;
					allocationFromOtherProcess[ResourceItemIdColumnName] =
						Conv.ToStr(alloc[ResourceItemIdColumnName]);
					allocationFromOtherProcess[ProcessColumnName] = "allocation";
					allocationFromOtherProcess[StatusColumnName] =
						(int)AllocationStatus.StatusForExternalAllocationLoad;
					allocationFromOtherProcess[ParentItemIdColumnName] =
						Conv.ToStr(alloc[ParentItemIdColumnName]);

					if (!dictionaryForAllocations.ContainsKey(itemId)) {
						dictionaryForAllocations.Add(itemId, allocationFromOtherProcess);
					}
				} else if (processId.Equals(currItemId)) {
					if (multipleProcesses) {
						alloc["parent_item_id"] = processId + "_" + alloc["resource_item_id"];
					}

					calendarDataObject.AddRowData(alloc);
				}

				allocationIds.Add(Conv.ToInt(alloc["item_id"]));
			}

			foreach (var alloc in dictionaryForAllocations.Values) {
				calendarDataObject.AddRowData(alloc);
			}

			SetDBParam(SqlDbType.Int, "@minimumStatus", AllocationStatus.Draft);

			// find all durations for allocations
			var allocationIdsString = string.Join(",", allocationIds);
			var limitWhere = (limitStart != "" && limitEnd != "")
				? " AND ad.date BETWEEN '" + Conv.ToSql(limitStart) + "' AND '" + Conv.ToSql(limitEnd) + "'"
				: "";

			var queryForAllDurations =
				"SELECT alloc.process_item_id, alloc.resource_item_id, alloc.status, ad.hours,ad.date,ad.allocation_item_id " +
				"FROM " + _allocationDurationsTableName + " ad " +
				"JOIN _" + instance + "_allocation alloc ON alloc.item_id = ad.allocation_item_id " +
				"WHERE allocation_item_id IN (" + allocationIdsString + ")" + limitWhere;

			var durations = string.IsNullOrEmpty(allocationIdsString)
				? new List<Dictionary<string, object>>()
				: db.GetDatatableDictionary(queryForAllDurations, DBParameters);

			foreach (var durationRow in durations) {
				var processIdOfCurrent = (int)durationRow[ProcessItemIdColumnName];
				var date = (DateTime)Conv.ToDateTime(durationRow[DateColumnName]);
				var hours = (double)durationRow[HoursColumnName];
				var status = Conv.ToInt(durationRow[StatusColumnName]).ToString();
				string itemId = null;
				if (processId.Equals(processIdOfCurrent)) {
					itemId = Conv.ToStr(durationRow[AllocationItemIdColumnName]);
				} else if (!multipleProcesses) {
					itemId = Conv.ToStr(
						         durationRow[ResourceItemIdColumnName]) +
					         "_00";
				}

				if (!string.IsNullOrEmpty(itemId)) {
					calendarDataObject.SetHoursForItem(date, itemId, hours, status);
				}
			}

			return calendarDataObject;
		}

		private Dictionary<string, object> GetProcessForRowdataObject(string processName,
			IDbDataParameter processIdSqlParameter) {
			var processTable = GetProcessTableName(processName);

			var pp = new ProcessPortfolios(instance, processName, AuthenticatedSession);
			var cols = pp.GetDefaultSelectorColumnNames();
			cols.Add("item_id");
			// find process item
			var queryForFindingProcess = new SelectQuery()
				.Columns(cols.ToArray())
				.From(processTable)
				.Where("item_id", SqlComp.EQUALS, processIdSqlParameter);

			var processRow = db.GetDataRow(queryForFindingProcess, DBParameters);
			var process = Conv.DataRowToDictionary(processRow);
			return process;
		}

		private string GetProcessName(IDbDataParameter processIdSqlParameter) {
			var queryForItems = BuildSelectQuery()
				.Columns("process")
				.From("items_all")
				.Where("item_id", SqlComp.EQUALS, processIdSqlParameter);
			var processName = Conv.ToStr(db.ExecuteScalar(queryForItems, DBParameters));
			return processName;
		}

		private List<Dictionary<string, object>> GetCompetencies(int processId) {
			var competencies = BuildSelectQuery().Distinct()
				.Columns("r.*", "lcc.item_id as _cost_center")
				.From(_allocationTableName, "alloc")
				.Join(new Join(GetProcessTableName("competency"), "r").On("r.item_id", "alloc.resource_item_id"))
				.Join(GetItemDataProcessSelectionJoin())
				.Join(GetListCostCenterJoin())
				.Where(GetProcessItemComparisonCondition(processId))
				.OrderBy("item_id")
				.Fetch();

			competencies = GroupCostCentersForCompetencies(competencies);
			return competencies;
		}

		public Dictionary<string, object> GetDurationsByTaskAndUsers(int taskId) {
			var itemBaseForTask = new ItemBase(instance, TaskProcessName, AuthenticatedSession);
			// Fetch task-object
			var task = itemBaseForTask.GetItem(taskId, true, false);
			return GetDurationsByTaskAndUsers(task);
		}

		private Dictionary<string, object> getTask(int taskId) {
			var itemBaseForTask = new ItemBase(instance, TaskProcessName, AuthenticatedSession);
			return itemBaseForTask.GetItem(taskId, true, false);
		}

		private List<Dictionary<string, object>> AllocatedResourceIdsForDateRange(DateTime? from, DateTime? to,
			IDbDataParameter processIdParameter) {
			var startDate = (DateTime)from;
			var endDate = (DateTime)to;
			var dateDiff = (endDate - startDate).TotalDays;
			var allocationTable = _allocationTableName;

			var noZeroHours = SetDBParameter("@noZeroHours", 0);
			var statusAccepted = SetDBParameter("@statusAccepted", (int)AllocationStatus.Approved);

			var queryForHoursInDateSpan = new SelectQuery()
				.Distinct()
				.Columns("alloc.resource_item_id")
				.From(allocationTable, "alloc")
				.Join(new Join(_allocationDurationsTableName, "ad").On("alloc.item_id", "ad.allocation_item_id"))
				.Where(new SqlCondition(ProcessItemIdColumnName, SqlComp.EQUALS, processIdParameter)
					.And(new SqlCondition().RawSql("ad.date BETWEEN @fromDate AND @toDate"))
					.And(new SqlCondition("hours", SqlComp.GREATER_THAN, noZeroHours))
					.And(new SqlCondition("alloc.status", SqlComp.EQUALS, statusAccepted)));

			return db.GetDatatableDictionary(queryForHoursInDateSpan, DBParameters);
		}

		private List<Dictionary<string, object>> HoursForResources(DateTime? from, DateTime? to, double effort,
			IDbDataParameter processIdParameter) {
			var startDate = (DateTime)from;
			var endDate = (DateTime)to;
			var dateDiff = (endDate - startDate).TotalDays;
			var allocationTable = _allocationTableName;
			double hoursPerDay = 0;
			if (effort > 0 && dateDiff > 0) {
				hoursPerDay = effort / dateDiff;
			} else if (effort > 0) {
				hoursPerDay = effort;
			}


			var queryForHoursInDateSpan = new SelectQuery()
				.Columns("alloc.resource_item_id, iif(ad.hours > @hoursPerDay, ad.hours, @hoursPerDay) hours")
				.From(_allocationDurationsTableName, "ad")
				.Join(new Join(allocationTable, "alloc").On("alloc.item_id", "ad.allocation_item_id"))
				.Where(new SqlCondition(ProcessItemIdColumnName, SqlComp.EQUALS, processIdParameter)
					.And(new SqlCondition().RawSql("ad.date BETWEEN @fromDate AND @toDate")));
			var queryForResourceValues = new SelectQuery()
				.Columns("resource_item_id, sum(hours) value")
				.From("(" + queryForHoursInDateSpan.Get(false) + ")", "users")
				.Where(new SqlCondition("hours", SqlComp.GREATER_THAN, "0"));
			var sqlForResourceHourValues = queryForResourceValues.Get(false) + " GROUP BY resource_item_id";
			SetDBParameters("@hoursPerDay", hoursPerDay);
			var hoursForResource = db.GetDatatableDictionary(sqlForResourceHourValues, DBParameters);
			return hoursForResource;
		}


		// this is a remake of GetOrderedListOfUsers
		// This returns only users with approved allocations (between tasks start and end dates)
		public Dictionary<string, object>
			GetOrderedListOfUsersWhoHaveAllocationWhileTaskIsActive(int taskId, int processId) {
			var calendarDataObject = new CalendarDataObject();
			var task = getTask(taskId);
			var from = Conv.ToDateTime(task[StartDateColumnName]);
			var to = Conv.ToDateTime(task[EndDateColumnName]);
			if (from != null && to != null) {
				SetDBParameters("@fromDate", from, "@toDate", to);
			} else {
				return calendarDataObject.GetObject();
			}

			var processIdParameter = SetDBParameter("@processItemId", processId);

			var itemBaseForUsers = new ItemBase(instance, "user", AuthenticatedSession);


			var resourceIds = new List<object>();
			var resourcesWithApprovedAllocation = AllocatedResourceIdsForDateRange(from, to, processIdParameter);
			if (resourcesWithApprovedAllocation.Count == 0) {
				return calendarDataObject.GetObject();
			}

			foreach (var item in resourcesWithApprovedAllocation) {
				resourceIds.Add(Conv.ToInt(item["resource_item_id"]));
			}

			var users = db.DataTableToDictionary(itemBaseForUsers.GetAllData(string.Join(",", resourceIds)));

			if (users.Count == 0) {
				return calendarDataObject.GetObject();
			}

			foreach (var usr in users) {
				var usrId = Conv.ToInt(usr[ItemIdColumnName]);
				resourceIds.Add(usrId);
				usr["process"] = "user";
				usr["task_type"] = TaskType.UserWithAllocation;
			}

			users = users
				.OrderByDescending(item => Conv.ToStr(item["last_name"]))
				.ToList();


			var noZeroHours = new SqlCondition(HoursColumnName, SqlComp.GREATER_THAN, "0");
			// Find all allocations for current process for each users in resourceIds
			var statusApprovedParameter = SetDBParameter("@statusApproved", (int)AllocationStatus.Approved);

			var conditionsForAllocations = new SqlCondition(ProcessItemIdColumnName, SqlComp.EQUALS, processIdParameter)
				.And(new SqlCondition(StatusColumnName, SqlComp.EQUALS, statusApprovedParameter))
				.And(noZeroHours);
			var resourceItemIdInCondition = new SqlCondition().Column(ResourceItemIdColumnName).In(resourceIds);
			if (resourceIds.Count > 0) {
				conditionsForAllocations = conditionsForAllocations.And(resourceItemIdInCondition);
			}

			var queryForFindingAllocationsForThisProcess = new SelectQuery()
				.From(GetProcessTableName(AllocationProcessName), "alloc")
				.Join(new Join(AllocationDurationTableName, "ad").On("ad.allocation_item_id", "alloc.item_id"))
				.Where(conditionsForAllocations);
			var query = queryForFindingAllocationsForThisProcess.Get();
			var allocations = db.GetDatatableDictionary(query, DBParameters);
			foreach (var allocation in allocations) {
				calendarDataObject.SetHoursForItem(
					(DateTime)Conv.ToDateTime(allocation[DateColumnName]),
					Conv.ToStr(allocation[ResourceItemIdColumnName]),
					Conv.ToDouble(allocation[HoursColumnName]),
					"ta" // Maybe a constant
				);
			}


			// Find all stuff from task_durations for each users in resourceIds
			var conditionForTaskDurations = noZeroHours.And(resourceItemIdInCondition);
			var queryForFindingTaskDurations = new SelectQuery()
				.From(TaskDurationTableName)
				.Where(conditionForTaskDurations);
			var taskDurations = db.GetDatatableDictionary(queryForFindingTaskDurations, DBParameters);
			foreach (var taskDuration in taskDurations) {
				var taskIdOfItem = Conv.ToInt(taskDuration[TaskItemIdColumnName]);
				if (!taskIdOfItem.Equals(taskId)) {
					calendarDataObject.SetHoursForItem(
						(DateTime)Conv.ToDateTime(taskDuration[DateColumnName]),
						Conv.ToStr(taskDuration[ResourceItemIdColumnName]),
						Conv.ToDouble(taskDuration[HoursColumnName]),
						"oa" // Maybe a constant
					);
				}
			}

			calendarDataObject.AddRowData(users);

			return calendarDataObject.GetObject();
		}

		public Dictionary<string, object> GetOrderedListOfUsers(int taskId, int processId, string textsearch,
			string filters,
			int limit = 50) {
			SqlCondition resourceIdIn;
			var calendarDataObject = new CalendarDataObject();
			var task = getTask(taskId);
			var from = Conv.ToDateTime(task[StartDateColumnName]);
			var to = Conv.ToDateTime(task[EndDateColumnName]);
			double effort = 0;
			if (from != null && to != null) {
				effort = Conv.ToDouble(task[EffortColumnName]);
				SetDBParameters("@fromDate", from, "@toDate", to);
			}

			var processIdParameter = SetDBParameter("@processItemId", processId);

			var itemBaseForUsers = new ItemBase(instance, "user", AuthenticatedSession);

			var users = db.DataTableToDictionary(itemBaseForUsers.GetFilteredData(filters, textsearch));

			if (users.Count == 0) {
				return calendarDataObject.GetObject();
			}

			var resourceIds = new List<object>();
			var valueByItemId = new Dictionary<int, int>();

			if (effort > 0) {
				var hoursForResource = HoursForResources(from, to, effort, processIdParameter);
				foreach (var item in hoursForResource) {
					valueByItemId[Conv.ToInt(item[ResourceItemIdColumnName])] = Conv.ToInt(item["value"]);
				}
			}

			foreach (var usr in users) {
				var usrId = Conv.ToInt(usr[ItemIdColumnName]);
				resourceIds.Add(usrId);
				if (valueByItemId.ContainsKey(usrId)) {
					usr["_value"] = valueByItemId[usrId];
				} else {
					usr["_value"] = 0;
				}

				usr["process"] = "user";
				usr["task_type"] = TaskType.UserWithAllocation;
			}

			var userQuery = users
				.OrderByDescending(item => Conv.ToDouble(item["_value"]))
				.ThenBy(item => Conv.ToStr(item["last_name"]));
			if (limit > 0) {
				users = userQuery.Take(limit).ToList();
			} else {
				users = userQuery.ToList();
			}

			var noZeroHours = new SqlCondition(HoursColumnName, SqlComp.GREATER_THAN, "0");
			// Find all allocations for current process for each users in resourceIds
			var statusApprovedParameter = SetDBParameter("@statusApproved", (int)AllocationStatus.Approved);
			if (!string.IsNullOrEmpty(filters) || !string.IsNullOrEmpty(textsearch)) {
				resourceIdIn = new SqlCondition().Column(ResourceItemIdColumnName).In(resourceIds);
			} else {
				resourceIdIn = new SqlCondition("1", SqlComp.EQUALS, "1");
			}

			var conditionsForAllocations = new SqlCondition(ProcessItemIdColumnName, SqlComp.EQUALS, processIdParameter)
				.And(new SqlCondition(StatusColumnName, SqlComp.EQUALS, statusApprovedParameter))
				.And(noZeroHours);
			var resourceItemIdInCondition = new SqlCondition().Column(ResourceItemIdColumnName).In(resourceIds);
			if (resourceIds.Count > 0) {
				conditionsForAllocations = conditionsForAllocations.And(resourceItemIdInCondition);
			}

			var queryForFindingAllocationsForThisProcess = new SelectQuery()
				.From(GetProcessTableName(AllocationProcessName), "alloc")
				.Join(new Join(AllocationDurationTableName, "ad").On("ad.allocation_item_id", "alloc.item_id"))
				.Where(conditionsForAllocations);
			var query = queryForFindingAllocationsForThisProcess.Get();
			var allocations = db.GetDatatableDictionary(query, DBParameters);
			foreach (var allocation in allocations) {
				calendarDataObject.SetHoursForItem(
					(DateTime)Conv.ToDateTime(allocation[DateColumnName]),
					Conv.ToStr(allocation[ResourceItemIdColumnName]),
					Conv.ToDouble(allocation[HoursColumnName]),
					"ta" // Maybe a constant
				);
			}


			// Find all stuff from task_durations for each users in resourceIds
			var conditionForTaskDurations = noZeroHours;
			if (effort > 0) {
				conditionForTaskDurations.And(resourceItemIdInCondition);
			}

			var queryForFindingTaskDurations = new SelectQuery()
				.From(TaskDurationTableName)
				.Where(conditionForTaskDurations);
			var taskDurations = db.GetDatatableDictionary(queryForFindingTaskDurations, DBParameters);
			foreach (var taskDuration in taskDurations) {
				var taskIdOfItem = Conv.ToInt(taskDuration[TaskItemIdColumnName]);
				if (!taskIdOfItem.Equals(taskId)) {
					calendarDataObject.SetHoursForItem(
						(DateTime)Conv.ToDateTime(taskDuration[DateColumnName]),
						Conv.ToStr(taskDuration[ResourceItemIdColumnName]),
						Conv.ToDouble(taskDuration[HoursColumnName]),
						"oa" // Maybe a constant
					);
				}
			}

			calendarDataObject.AddRowData(users);

			return calendarDataObject.GetObject();
		}

		private SelectQuery GetQueryForCostCenters() {
			var userIdParam = SetDBParameter("@userId", AuthenticatedSession.item_id);
			return new SelectQuery()
				.Columns("idps.item_id")
				.From(_itemDataProcessSelectionsTableName, "idps")
				.Join(new Join("item_columns", "ic").On("ic.item_column_id", "idps.item_column_id"))
				.Where(_instanceCondition
					.And(new SqlCondition("process", SqlComp.EQUALS, "'list_cost_center'"))
					.And(new SqlCondition("name", SqlComp.EQUALS, "'owner'"))
					.And(new SqlCondition("selected_item_id", SqlComp.EQUALS, userIdParam)));
		}


		private List<Dictionary<string, object>> GetResourceForUserId(int userId) {
			var userTable = GetProcessTableName("user");
			var listCostCenterTable = GetProcessTableName("list_cost_center");
			var instanceCondition = _instanceCondition.Get();
			SetDBParameter("@userIdForResource", userId);
			GetQueryForCostCenters().Get(false);
			var sql = "SELECT u.*, lcc.item_id  as _cost_center FROM " + userTable + " u " +
			          "JOIN " + _itemDataProcessSelectionsTableName + " idps ON idps.item_id = u.item_id " +
			          "JOIN " + listCostCenterTable + " lcc ON lcc.item_id = idps.selected_item_id " +
			          "JOIN item_columns ic ON ic.item_column_id = idps.item_column_id " +
			          "WHERE (" + instanceCondition + " AND process = 'user' AND name = 'cost_center')  " +
			          "  AND u.item_id = @userIdForResource";

			var items = db.GetDatatableDictionary(sql, DBParameters);
			FetchAndSetOrganizationInformation(items);
			foreach (var resource in items) {
				ConvertCostCenterToArray(resource, "_cost_center");
			}

			return items;
		}

		private List<Dictionary<string, object>> GetResourcesByCostCenter(List<int> filterIds = null,
			List<int> competencyFilters = null) {
			var listCostCenterQuery = GetQueryForCostCenters().Get(false);

			var instanceCondition = _instanceCondition.Get();
			var listCostCenterTable = GetProcessTableName("list_cost_center");
			var userTable = GetProcessTableName("user");

			var inactiveuserssql =
				" AND (CASE WHEN u.active = 1 THEN 1 ELSE (SELECT CASE WHEN MAX(date) > DATEADD(day,-90,getutcdate()) THEN 1 ELSE 0 END FROM _" +
				instance + "_allocation a  " +
				" LEFT JOIN allocation_durations d ON a.item_id = d.allocation_item_id " +
				" WHERE a.resource_item_id = u.item_id AND a.status IN (13,20,25,40,50)) END) = 1 ";

			var pp = new ProcessPortfolios(instance, "user", AuthenticatedSession);
			var ucols = "";
			foreach (var col in pp.GetDefaultSelectorColumnNames()) {
				ucols += ",u." + col;
			}

			var filterIdString = "";
			if (filterIds != null && filterIds.Count > 0) {
				filterIdString = " u.item_id IN (" + Conv.ToDelimitedString(filterIds) + ") AND ";
			} else if (filterIds != null && filterIds.Count == 0) {
				filterIdString = " u.item_id IN (0) AND ";
			}

			var sql = "SELECT u.item_id, u.cost_center" + ucols + ", lcc.item_id  as _cost_center FROM " + userTable +
			          " u " +
			          "JOIN " + _itemDataProcessSelectionsTableName + " idps ON idps.item_id = u.item_id " +
			          "JOIN " + listCostCenterTable + " lcc ON lcc.item_id = idps.selected_item_id " +
			          "JOIN item_columns ic ON ic.item_column_id = idps.item_column_id " +
			          "WHERE " + filterIdString + " (" + instanceCondition + inactiveuserssql +
			          " AND process = 'user' AND name = 'cost_center')  " +
			          "  AND (idps.selected_item_id IN ( " + listCostCenterQuery + "))";


			var items = db.GetDatatableDictionary(sql, DBParameters);

			FetchAndSetOrganizationInformation(items);

			foreach (var resource in items) {
				ConvertCostCenterToArray(resource, "_cost_center");
			}

			var filterIdString2 = "";
			if (competencyFilters != null && competencyFilters.Count > 0) {
				filterIdString2 = " WHERE r.item_id IN (" + Conv.ToDelimitedString(competencyFilters) + ")";
			} else if (competencyFilters != null && competencyFilters.Count == 0) {
				filterIdString2 = " WHERE r.item_id IN (0)";
			}

			var competencyTable = GetProcessTableName("competency");
			var c = "select r.item_id, r.name, avg_hour_rate, lcc.item_id as cost_center from " + competencyTable +
			        " r " +
			        "left join item_data_process_selections lcc ON lcc.item_id = r.item_id AND lcc.item_column_id = (select item_column_id from item_columns where process = 'competency' and name = 'cost_center') " +
			        filterIdString2 +
			        "GROUP BY r.item_id, r.name,avg_hour_rate, lcc.item_id";

			var citems = db.GetDatatableDictionary(c, DBParameters);
			foreach (var r in citems) {
				items.Add(r);
			}

			return items;
		}

		private List<Dictionary<string, object>> GetDurationsForTasks(DateTime fromDate) {
			var tableName = GetProcessTableName("task");
			var dateParam = SetDBParameter("@datecutter", fromDate);
			var queryForTaskDurations = new SelectQuery()
				.Columns("task_item_id")
				.From("task_durations")
				.Where("date", SqlComp.GREATER_OR_EQUAL, dateParam);
			var query = new SelectQuery()
				.From("task_durations", "td")
				.Join(new Join(tableName, "t").On("t.item_id", "td.task_item_id"))
				.Where(new SqlCondition().Column("td." + TaskItemIdColumnName).In(queryForTaskDurations));

			var taskDurations = db.GetDatatableDictionary(query, DBParameters);

			return taskDurations;
		}

		// Activity Widget Data Service
		public DataTable GetDataForActivitiesWidget() {
			var myCostCenterIds = GetMyCostCenterIds();
			if (myCostCenterIds.Count == 0) {
				return new DataTable();
			}

			var wantedStatuses = (int)AllocationStatus.Requested + ", " + (int)AllocationStatus.RequestedModified;

			var myCostCenterIdsInString = string.Join(",", myCostCenterIds);
			var userIdParam = SetDBParameter("@userId", AuthenticatedSession.item_id);

			// Subcondition for finding allocation requests with statuses Requested or RequestedModified and 
			// that are in one of my cost centers
			var conditionForApprovable = new SqlCondition().Column("alloc.status").In(wantedStatuses)
				.And(new SqlCondition().Column("alloc.cost_center").In(myCostCenterIdsInString));

			// Condition for finding allocation requests with status ApprovedWithModifications
			var conditionForApprovedWithChanges =
				new SqlCondition().Column("alloc.status").In("" + (int)AllocationStatus.ApprovedWithModifications)
					.And(new SqlCondition("alloc.creator_item_id", SqlComp.EQUALS, userIdParam));


			// Group subconditions with OR
			var conditions = new ConditionGroup()
				.Operation(SqlOperator.OR)
				.Add(conditionForApprovable)
				.Add(conditionForApprovedWithChanges)
				.Get();

			var conditionForTime = new SqlCondition().RawSql(
				conditions.Get() +
				" AND DATEADD(DAY, -90, getutcdate()) < (SELECT MAX(date) FROM allocation_durations WHERE allocation_item_id = alloc.item_id)");

			var query = BuildSelectQuery()
				.Columns(
					"alloc.*, u.first_name, u.last_name, c.name, '' as owner_name, items.process as owner_process, task.title as task_name")
				.From(GetProcessTableName("Allocation"), "alloc")
				.Join(new Join(GetProcessTableName("user"), "u").Type("LEFT JOIN")
					.On("u.item_id", "alloc.resource_item_id"))
				.Join(new Join(GetProcessTableName("competency"), "c").Type("LEFT JOIN")
					.On("c.item_id", "alloc.resource_item_id"))
				.Join(new Join(GetProcessTableName("task"), "task").Type("LEFT JOIN")
					.On("task.item_id", "alloc.task_item_id"))
				.Join(new Join("items_all", "items").Type("LEFT JOIN").On("alloc.process_item_id", "items.item_id"))
				.Where(conditions).Where(conditionForTime);

			var datatable = query.FetchDataTable();
			new Processes(instance, AuthenticatedSession)
				.AddPrimaryNameToDatatableColumn(datatable, "owner_name", "process_item_id", "owner_process");
			return datatable;
		}
	}
}