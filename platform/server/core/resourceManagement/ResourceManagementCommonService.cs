﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core.resourceManagement{

	// DATABASE SERVICES
	public class ResourceManagementCommonService : ConnectionBase{
		// CONSTANTS
		// Process names
		public const string TaskProcessName = "task";

		public const string AllocationProcessName = "allocation";

		// Table names
		public const string TaskDurationTableName = "task_durations";

		public const string AllocationDurationTableName = "allocation_durations";

		// Field names
		public const string ResourceItemIdColumnName = "resource_item_id";

		public const string ItemIdColumnName = "item_id";
		public const string CreatorItemIdColumnName = "creator_item_id";
		public const string StatusColumnName = "status";
		public const string TaskItemIdColumnName = "task_item_id";
		public const string ProcessItemIdColumnName = "process_item_id";
		public const string TitleColumnName = "title";
		public const string DateColumnName = "date";
		public const string AllocationItemIdColumnName = "allocation_item_id";
		public const string HoursColumnName = "hours";
		public const string ParentItemIdColumnName = "parent_item_id";
		public const string StartDateColumnName = "start_date";
		public const string EndDateColumnName = "end_date";
		public const string EffortColumnName = "effort";
		public const string ProcessColumnName = "process";
		public const string CostCenterColumnName = "cost_center";
		public const string ResourceTypeColumnName = "resource_type";
		public const string UserColumnName = "user_item_id";

		// Object properties
		public const string DatesProperty = "dates";

		public const string RowDataProperty = "rowdata";

		public const string Task = TaskProcessName;
		public const string Allocation = AllocationProcessName;

		// Commonly used sql where conditions
		protected readonly SqlCondition _instanceCondition;
		protected readonly SqlCondition _listCostCenterCondition;

		protected readonly SqlCondition _userProcessCondition;

		protected readonly AuthenticatedSession AuthenticatedSession;
		protected IResourceManagementRepository _resourceManagementRepository;

		public ResourceManagementCommonService(string instance, AuthenticatedSession session, IResourceManagementRepository repository, DateTime? archiveDate = null) :
			base(instance, session, archiveDate){
			_resourceManagementRepository = repository;
			AuthenticatedSession = session;
			_resourceManagementAccessControl = new ResourceManagementAccessControl(instance, session, repository);
			
			// SqlConditions for kids to use
			var instanceParam = SetDBParameter("@instance", instance);
			_instanceCondition = new SqlCondition("instance", SqlComp.EQUALS, instanceParam);
			_userProcessCondition = new SqlCondition("process", SqlComp.EQUALS, "'user'");
			_listCostCenterCondition = new SqlCondition("process", SqlComp.EQUALS, "'list_cost_center'");
		}

		public IResourceManagementAccessControl _resourceManagementAccessControl{ get; set; }

		public List<Dictionary<string, object>> GetMyCostCenters(){
			var userIdParam = SetDBParameter("@userId", AuthenticatedSession.item_id);
			var query = new SelectQuery()
				.Columns("item_id")
				.From("item_data_process_selections", "idps")
				.Join(new Join("item_columns", "ic").On("ic.item_column_id", "idps.item_column_id"))
				.Where(
					_instanceCondition
						.And(_listCostCenterCondition)
						.And(new SqlCondition("name", SqlComp.EQUALS, "'owner'"))
						.And(new SqlCondition("selected_item_id", SqlComp.EQUALS, userIdParam)));

			var items = db.GetDatatableDictionary(query, DBParameters);
			return items;
		}

		protected void FetchAndSetOrganizationInformation(List<Dictionary<string, object>> resources){
			var ids = resources
				.Select(resource => Conv.ToInt(resource["item_id"]))
				.ToList();

			var orgParam = SetDBParameter("@organization", "organisation");
			var query = new SelectQuery()
				.Columns("idps.item_id", "idps.selected_item_id")
				.From("item_data_process_selections", "idps")
				.JoinWith("item_columns ic", "ic.item_column_id", "idps.item_column_id")
				.Where(
					_instanceCondition
						.And(new SqlCondition("name", SqlComp.EQUALS, orgParam))
						.And(new SqlCondition().Column(ItemIdColumnName).In(ids))
				);

			var items = db.GetDatatableDictionary(query, DBParameters);
			var organizationsForResourceId = new Dictionary<int, int>();
			foreach (var row in items){
				var orgId = Conv.ToInt(row["selected_item_id"]);
				if (orgId > 0 && !organizationsForResourceId.ContainsKey(Conv.ToInt(row["item_id"]))){
					organizationsForResourceId.Add(Conv.ToInt(row["item_id"]), orgId);
				}
			}
			foreach (var resource in resources){
				var resourceId = Conv.ToInt(resource["item_id"]);
				if (organizationsForResourceId.ContainsKey(resourceId)){
					resource["organisation"] = new List<int>{organizationsForResourceId[resourceId]};
				}
			}
		}

		protected static void ConvertCostCenterToArray(Dictionary<string, object> dictionary, string fromProperty){
			if (dictionary["cost_center"] is List<int>) return;
			var costCenter = Conv.ToInt(dictionary[fromProperty]);
			if (costCenter > 0){
				var ccInList = new List<int>{costCenter};
				dictionary["cost_center"] = ccInList;
			}
			dictionary.Remove(fromProperty);
		}

		protected Dictionary<string, object> GetDurationsByTaskAndUsers(Dictionary<string, object> task, int? userId = null){
			var calendarDataObject = new CalendarDataObject();
			var allocationTableName = GetProcessTableName(AllocationProcessName);
			var itemBaseForTask = new ItemBase(instance, TaskProcessName, AuthenticatedSession);
			//var itemBaseForUser = new ItemBase(instance, "user", AuthenticatedSession);
			// Fetch task-object
			var taskId = task["item_id"];
			var parentItemIdOfTask = task[ParentItemIdColumnName];
			task["parent_item_id"] = taskId;
			task["item_id"] = taskId + "_" + taskId;

			// Fetch users we want to play with (by allocations and task_durations)
			SetDBParameters(
				"@taskId", taskId,
				"@processItemId", parentItemIdOfTask);
			List<int> userIds;
			if (userId == null){
				calendarDataObject.AddRowData(task);
				var queryForAllUserIds = "SELECT DISTINCT resource_item_id " +
				                         "FROM ( " +
				                         "SELECT DISTINCT resource_item_id FROM task_durations WHERE task_item_id = @taskId " +
				                         "UNION " +
				                         "SELECT DISTINCT resource_item_id FROM " + allocationTableName + " alloc JOIN allocation_durations ad ON (ad.allocation_item_id = alloc.item_id AND ad.hours > 0) WHERE task_item_id = @taskId " +
				                         ") q";

				var userIdsInDictList = db.GetDatatableDictionary(queryForAllUserIds, DBParameters);
				userIds = (from x in userIdsInDictList select (int) x["resource_item_id"]).ToList();
			}
			else{
				userIds = new List<int>{(int) userId};
			}

			// if no users found, then return blank object
			if (userIds.Count <= 0) return calendarDataObject.GetObject();

			var userIdsInString = Modi.JoinListWithComma(userIds, true);

			var queryForAllocationHoursByResourceAndDate =
				"SELECT sum(hours) hours, date, resource_item_id FROM allocation_durations ad " +
				"JOIN " + allocationTableName + " alloc ON alloc.item_id = ad.allocation_item_id " +
				"WHERE alloc.status = " + (int) AllocationStatus.Approved + " AND resource_item_id in " + userIdsInString +
				"GROUP BY date, resource_item_id";
			var allocationHoursForUsers =
				db.GetDatatableDictionary(queryForAllocationHoursByResourceAndDate);
			foreach (var allocationHoursForUser in allocationHoursForUsers){
				var hours = Conv.ToDouble(allocationHoursForUser[HoursColumnName]);
				var date = Conv.ToDateTime(allocationHoursForUser[DateColumnName]);
				var resourceItemId =
					Conv.ToStr(allocationHoursForUser[ResourceItemIdColumnName]);
				calendarDataObject.SetHoursForItem(
					(DateTime) date,
					resourceItemId,
					hours,
					"ta");
			}
			var userTableName = GetProcessTableName("user");

			var queryForUsers = "SELECT u.*, q.selected_item_id AS _cost_center " +
			                    "FROM " + userTableName + " u " +
			                    "left join ( " +
			                    "select selected_item_id, idps.item_id " +
			                    "from item_data_process_selections idps " +
			                    "join item_columns ic on idps.item_column_id =ic.item_column_id " +
			                    "where ic.process='user' and ic.data_additional = 'list_cost_center' " +
			                    ") q on q.item_id = u.item_id " +
			                    "where u.item_id in " + userIdsInString;

			var users = db.GetDatatableDictionary(queryForUsers);
			FetchAndSetOrganizationInformation(users);
			foreach (var usr in users){
				usr["task_type"] = TaskType.UserWithAllocation;
				usr["process"] = "user";
				usr["parent_item_id"] = taskId;
				usr.Remove("password");
				usr.Remove("token");
				ConvertCostCenterToArray(usr, "_cost_center");
			}

			var queryForAllTaskIds = "select distinct task.item_id from task_durations td " +
			                         "join _" + instance + "_task task on task.item_id = td.task_item_id " +
			                         "where task.parent_item_id = @processItemId " +
			                         "and resource_item_id in " + userIdsInString;

			var taskIds = db.GetDatatableDictionary(queryForAllTaskIds, DBParameters);
			if (taskIds.Count == 0){
				return calendarDataObject.GetObject();
			}
			var taskIdList = from x in taskIds select x[ItemIdColumnName];
			var taskIdsInString = Modi.JoinListWithComma(taskIdList.ToList());
			var tasks = itemBaseForTask.GetAllData(taskIdsInString);
			var tasksByItemId = new Dictionary<int, Dictionary<string, object>>();
			foreach (DataRow taskItem in tasks.Rows){
				var itemId = Conv.ToInt(taskItem[ItemIdColumnName]);
				if (!tasksByItemId.ContainsKey(itemId)){
					tasksByItemId.Add(itemId, Conv.DataRowToDictionary(taskItem));
				}
			}

			calendarDataObject.AddRowData(users);

			var newQueryForHoursByTaskAndUser = new SelectQuery()
				.From(TaskDurationTableName)
				.Where(
					new SqlCondition(ResourceItemIdColumnName, SqlComp.IN, userIdsInString)
						.And(new SqlCondition(TaskItemIdColumnName, SqlComp.IN, "(" + taskIdsInString + ")")));

			var tasksForUsersSet = new HashSet<string>();
			var taskHours = db.GetDatatableDictionary(newQueryForHoursByTaskAndUser);
			foreach (var taskHour in taskHours){
				var taskItemId = Conv.ToInt(taskHour[TaskItemIdColumnName]);
				var resourceItemId = Conv.ToInt(taskHour[ResourceItemIdColumnName]);
				var artificialItemId = resourceItemId + "_" + taskItemId;
				if (!tasksForUsersSet.Contains(artificialItemId)){
					// Check if task is already added to rowdata list
					var taskObject = tasksByItemId[taskItemId];
					var copyOfObject = taskObject.ToDictionary(
						entry => entry.Key,
						entry => entry.Value);
					copyOfObject[ItemIdColumnName] = artificialItemId;
					copyOfObject["actual_item_id"] = taskItemId;
					copyOfObject["task_type"] = TaskType.AllocationOrTask;
					copyOfObject["process"] = "task";
					copyOfObject[ParentItemIdColumnName] = resourceItemId;
					tasksForUsersSet.Add(artificialItemId);
					calendarDataObject.AddRowData(copyOfObject);
				}

				calendarDataObject.SetHoursForItem(
					(DateTime) Conv.ToDateTime(taskHour[DateColumnName]),
					artificialItemId,
					Conv.ToDouble(taskHour[HoursColumnName]),
					((int) AllocationStatus.StatusForTasks).ToString()
				);
			}
			return calendarDataObject.GetObject();
		}

		protected enum DurationType{
			Task,
			Allocation
		}
	}
}