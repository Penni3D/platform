﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.database;

namespace Keto5.x.platform.server.core.resourceManagement{
	public interface IResourceManagementRepository{
		Dictionary<string, object> GetItem(int itemId);
		void Delete(int itemId);

		void DeleteAllocationHours(int allocationItemId);

		void DeleteAllocationHours(int allocationItemId, int year, int month);

		void DeleteAllocationHours(int allocationItemId, DateTime data_start, DateTime date_end);

		string GetProcessNameForProcessItemId(int processItemId);

		int InsertItemRow();

		void SaveItem(Dictionary<string, object> item, bool internalUpdate = false);

		void InsertAllocationHours(int allocationItemId, List<DateAndDuration> dateAndDurations);

		IDatabaseItemService GetEntityService();

		List<Dictionary<string, object>> GetResourcesOfCostCenter(int costCenterId);

		List<Dictionary<string, object>> GetAllocationsForUserOrWithStatusOverDraft(List<int> resourceIds, int userId, string portfolioFilters = "");

		List<Dictionary<string, object>> GetAllocationsForTaskId(int taskId);

		List<Dictionary<string, object>> GetAllocationsForUser(int userId, DateTime? fromDate, List<int> wantedStatuses = null, string portfolioFilters = "");
	}
}