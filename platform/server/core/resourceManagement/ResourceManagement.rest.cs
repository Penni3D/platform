using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core.resourceManagement {
	[Route("v1/resources/")]
	public class TaskAndAllocationDurationsApi : RestBase {
		// DATA QUERIES
		[HttpGet("bydate/{endAfterDate}")]
		public Dictionary<string, object> GetDurationsForCostCenter(DateTime endAfterDate) {
			return getQueryService().GetDurationsForCostCenter(endAfterDate);
		}

		[HttpGet("bydate/{endAfterDate}/{startDate}/{endDate}")]
		public Dictionary<string, object> GetDurationsForCostCenter(DateTime endAfterDate, string startDate,
			string endDate) {
			return getQueryService().GetDurationsForCostCenter(endAfterDate, startDate, endDate);
		}

		[HttpGet("bydate")]
		public Dictionary<string, object> GetDurationsForCostCenter() {
			return getQueryService().GetDurationsForCostCenter();
		}

		[HttpGet("archive/{date}/bydate")]
		public Dictionary<string, object> GetDurationsForCostCenter(string date) {
			return getQueryService(date).GetDurationsForCostCenter();
		}

		[HttpPost("{userPortfolioId}/{competencyPortfolioId}/{endAfterDate}/filtered")]
		public Dictionary<string, object> GetDurationsForCostCenterFiltered(int userPortfolioId,
			int competencyPortfolioId, DateTime endAfterDate, [FromBody] List<PortfolioOptions> por) {
			List<int> filterIds = null;
			List<int> competencyFilters = null;

			var p1 = por[0];
			if (p1 != null) {
				filterIds = new List<int>();
				var p = new ItemBase(instance, "user", currentSession);
				var items = p.FindItems(userPortfolioId, p1);
				var data2 = (DataTable)items["rowdata"];

				foreach (DataRow row in data2.Rows) {
					filterIds.Add(Conv.ToInt(row["item_id"]));
				}
			}

			var p2 = por[1];
			if (p2 != null) {
				competencyFilters = new List<int>();
				var p = new ItemBase(instance, "competency", currentSession);
				var items2 = p.FindItems(competencyPortfolioId, por[1]);
				var data = (DataTable)items2["rowdata"];
				foreach (DataRow row in data.Rows) {
					competencyFilters.Add(Conv.ToInt(row["item_id"]));
				}
			}

			return getQueryService().GetDurationsForCostCenter(dateParam: endAfterDate, filterIds: filterIds,
				competencyFilters: competencyFilters);
		}

		[HttpGet("bydate/user/{userId}")]
		public Dictionary<string, object> GetDurationsForUser(int userId) {
			return getQueryService().GetDurationsForUser(userId);
		}

		[HttpGet("bydate/{date}/user/{userId}")]
		public Dictionary<string, object> GetDurationsForUser(int userId, DateTime d) {
			return getQueryService().GetDurationsForUser(userId, d);
		}

		[HttpGet("bydate/{date}/users/{userIds}")]
		public Dictionary<int, object> GetDurationsForUsers(string userIds, DateTime d) {
			var result = new Dictionary<int, object>();
			foreach (var userId in userIds.Split(",").Select(Conv.ToInt)) {
				if (!result.ContainsKey(userId))
					result.Add(userId, getQueryService().GetDurationsForUser(userId, d, ""));
			}

			return result;
		}
		
		[HttpPost("bydates/{sdate}/{edate}/users")]
		public Dictionary<int, object> GetDurationsForUsers(string sdate, string edate, [FromBody] List<ResourceAllocationValues> userIds) {
			var result = new Dictionary<int, object>();
			foreach (var userId in userIds) {
				if (userId.Process == "user" && !result.ContainsKey(userId.ItemId)) {
					result.Add(userId.ItemId, getQueryService().GetDurationsForUserLimited(userId.ItemId, sdate, edate, 1));
				} else if (userId.Process == "list_cost_center") {
					try {
						var cb = new ConnectionBase(instance, currentSession);
						cb.SetDBParameter("@itemId", userId.ItemId);
						foreach (DataRow row in cb.db.GetDatatable("SELECT item_id FROM item_data_process_selections " +
						                                           "where selected_item_id = @itemId " +
						                                           "AND item_column_id = (SELECT item_column_id FROM item_columns WHERE process = 'competency' AND name = 'cost_center')",
							cb.DBParameters).Rows) {
							result.Add(Conv.ToInt(row["item_id"]),
								getQueryService()
									.GetDurationsForUserLimited(Conv.ToInt(row["item_id"]), sdate, edate, 2));
						}
					} catch (Exception e) {
						Console.WriteLine(e);
					}
				}
			}
			return result;
		}


		[HttpPost("bydate/{date}/users/{portfolioId}/{userIds}")]
		public Dictionary<int, object> GetDurationsForUsers(string userIds, DateTime d, int portfolioId,
			[FromBody] PortfolioOptions filters = null) {
			var where = "";
			if (filters != null) {
				var ib = new ItemBase(instance, "allocation", currentSession);
				where = ib.GetFindItemsWhere(portfolioId, filters);
			}

			return userIds.Split(",").Select(Conv.ToInt).ToDictionary<int, int, object>(
				userId => userId,
				userId => getQueryService().GetDurationsForUser(userId, d, where)
			);
		}

		[HttpGet("archive/{date}/bydate/user/{userId}")]
		public Dictionary<string, object> GetDurationsForUser(string date, int userId) {
			return getQueryService(date).GetDurationsForUser(userId);
		}

		[HttpGet("bydate/users/{userIds}")]
		public Dictionary<string, object> GetDurationsForUser(string userIds) {
			return getQueryService().GetDurationsForUsers(userIds);
		}

		[HttpGet("bydate/byprocess/{processId}")]
		public Dictionary<string, object> GetDurationsByUsersInProcess(int processId) {
			return getQueryService().GetDurationsByProcessAndUsers(processId).GetObject();
		}

		[HttpGet("bydate/byprocess/{processId}/{startDate}/{endDate}")]
		public Dictionary<string, object>
			GetDurationsByUsersInProcess(int processId, string startDate, string endDate) {
			return getQueryService().GetDurationsByProcessAndUsers(processId, limitStart: startDate, limitEnd: endDate)
				.GetObject();
		}

		[HttpGet("archive/{date}/bydate/byprocess/{processId}")]
		public Dictionary<string, object> GetDurationsByUsersInProcess(string date, int processId) {
			return getQueryService(date).GetDurationsByProcessAndUsers(processId).GetObject();
		}

		[HttpGet("bydate/byprocess/{processId}/{resourceId}")]
		public Dictionary<string, object> GetDurationsByUsersInProcess(int processId, int resourceId) {
			return getQueryService().GetDurationsByProcessAndUsers(processId, null, false, resourceId).GetObject();
		}

		[HttpGet("archive/{date}/bydate/byprocess/{processId}/{resourceId}")]
		public Dictionary<string, object> GetDurationsByUsersInProcess(string date, int processId, int resourceId) {
			return getQueryService(date).GetDurationsByProcessAndUsers(processId, null, false, resourceId).GetObject();
		}

		[HttpGet("bydate/byprocess/{processId}/resources/{resourceId}/other")]
		public Dictionary<string, object> GetOtherUserAllocations(int processId, int resourceId) {
			return getQueryService().GetOtherDurationsByUser(processId, resourceId).GetObject();
		}

		[HttpGet("arhive/{date}/bydate/byprocess/{processId}/resources/{resourceId}/other")]
		public Dictionary<string, object> GetOtherUserAllocations(string date, int processId, int resourceId) {
			return getQueryService(date).GetOtherDurationsByUser(processId, resourceId).GetObject();
		}

		[HttpPost("bydate/byprocesses")]
		public Dictionary<string, object> GetDurationsByUsersInProcesses([FromBody] List<int> ids) {
			return ids.Count == 0 ? new Dictionary<string, object>() : getQueryService().GetDurationsByProcessesAndUsers(ids);
		}
		
		[HttpPost("bydate/byprocesses/date/{startDate}/{endDate}")]
		public Dictionary<string, object> GetDurationsByUsersInProcesses(string startDate, string endDate, [FromBody] List<int> ids) {
			return ids.Count == 0 ? new Dictionary<string, object>() : getQueryService().GetDurationsByProcessesAndUsers(ids, startDate: startDate, endDate: endDate);
		}

		[HttpPost("bydate/byprocesses/{portfolioId}/{processIds}")]
		public Dictionary<string, object> GetDurationsByUsersInProcesses(string processIds, int portfolioId,
			[FromBody] PortfolioOptions filters = null) {
			var where = "";
			if (filters != null) {
				var ib = new ItemBase(instance, "allocation", currentSession);
				where = ib.GetFindItemsWhere(portfolioId, filters);
			}

			if (string.IsNullOrEmpty(processIds)) return new Dictionary<string, object>();

			var idsSplitted = processIds.Split(',');
			var ids = idsSplitted.Select(s => Convert.ToInt32(s)).ToList();

			return getQueryService().GetDurationsByProcessesAndUsers(ids, where);
		}

		[HttpGet("archive/{date}/bydate/byprocesses/{processIds}")]
		public Dictionary<string, object> GetDurationsByUsersInProcesses(string date, string processIds) {
			if (string.IsNullOrEmpty(processIds)) {
				return new Dictionary<string, object>();
			}

			var idsSplitted = processIds.Split('-');
			var ids = idsSplitted.Select(s => Convert.ToInt32(s)).ToList();

			return getQueryService(date).GetDurationsByProcessesAndUsers(ids);
		}


		[HttpGet("bydate/bytask/{taskId}")]
		public Dictionary<string, object> GetDurationsByUsersInTask(int taskId) {
			return getQueryService().GetDurationsByTaskAndUsers(taskId);
		}

		[HttpGet("archive/{date}/bydate/bytask/{taskId}")]
		public Dictionary<string, object> GetDurationsByUsersInTask(string date, int taskId) {
			return getQueryService(date).GetDurationsByTaskAndUsers(taskId);
		}

		[HttpGet("competencies")]
		public List<Dictionary<string, object>> GetCompetencies() {
			return getQueryService().GetCostCenterCompetencies();
		}

		[HttpGet("users/allocated/{taskId}/{processId}")]
		public IActionResult GetAllocatedUsers(int taskId, int processId) {
			// TODO: SPEC 'Pitäisi hakea taskin ajalle osuvat allokaatiot (edes yhden päivän osalta) käyttäjineen. Ei palauta muita käyttäjiä'
			return new ObjectResult(getQueryService()
				.GetOrderedListOfUsersWhoHaveAllocationWhileTaskIsActive(taskId, processId));
		}

		[HttpGet("users/{taskId}/{processId}")]
		public IActionResult GetOrderedListOfUsers(int taskId, int processId) {
			return GetFilteredAndOrderedListOfUsers(taskId, processId, "", "");
		}

		[HttpGet("users/{taskId}/{processId}/{textsearch}")]
		public IActionResult GetOrderedListOfUsers(int taskId, int processId, string textsearch) {
			return GetFilteredAndOrderedListOfUsers(taskId, processId, textsearch, "");
		}

		[HttpGet("users/{taskId}/{processId}/{textsearch}/{filters}")]
		public IActionResult GetFilteredAndOrderedListOfUsers(int taskId, int processId, string textsearch,
			string filters) {
			return new ObjectResult(getQueryService().GetOrderedListOfUsers(taskId, processId, textsearch, filters));
		}

		[HttpGet("allocation/costcenters")]
		public IActionResult GetAllUsersInOwnedCostCenters() {
			return new ObjectResult(GetTaskDbService().GetMyCostCenters());
		}

		[HttpGet("allocation/dates/{itemId}")]
		public Dictionary<string, DateTime?> GetAllocationStartAndEndDate(int itemId) {
			var repository = GetRepository();
			return repository.GetAllocationStartAndEndDate(itemId);
		}

		[HttpGet("allocation/costcenter/{costCenterId}")]
		public IActionResult GetCostCenterById(int costCenterId) {
			var repository = GetRepository();
			var costCenters = repository.GetResourcesOfCostCenter(costCenterId);
			return new ObjectResult(costCenters);
		}

		[HttpGet("allocation/totals/{processItemId}/{startDate}/{endDate}/{grouping}")]
		public IActionResult GetAllocationTotalById(int processItemId, DateTime startDate, DateTime endDate,
			int grouping) {
			var repository = GetRepository();
			return new ObjectResult(repository.GetAllocationTotalsById(processItemId, startDate, endDate, grouping));
		}

		[HttpGet("allocation/totals/{processItemId}/new")]
		public IActionResult GetTimeAnalysis(int processItemId) {
			var repository = GetRepository();
			return new ObjectResult(repository.GetAnalysisRows(processItemId));
		}

		// Simple allocation stuff
		[HttpGet("simple/allocations/{processItemId}/{year}/{mode}/{process}/{filter_item_ids}/{portfolio_id}")]
		public IActionResult GetSimpleAllocations(int processItemId, int year, int mode, string process,
			string filter_item_ids, int portfolio_id) {
			var repository = GetRepository();
			return new ObjectResult(repository.GetSimpleAllocations(processItemId, year, mode, process, filter_item_ids,
				portfolio_id, currentSession));
		}

		[HttpDelete("simple/allocations/{allocationItemIds}")]
		public IActionResult DeleteSimpleAllocation(string allocationItemIds) {
			foreach (var id in allocationItemIds.Split(',')) {
				GetRepository().DeleteSimpleAllocation(Conv.ToInt(id));
			}

			return new ObjectResult("");
		}

		[HttpPost("simple/allocations")]
		public IActionResult AddSimpleAllocation([FromBody] EditAllocationModel model) {
			var newItem = GetAllocationService().AddSimpleAllocation(model);
			return new ObjectResult(newItem);
		}

		[HttpPut("simple/allocations")]
		public IActionResult UpdateSimpleAllocation([FromBody] List<EditAllocationModel> model) {
			foreach (var row in model) {
				GetAllocationService().UpdateSimpleAllocation(row);
			}

			return new ObjectResult("");
		}

		[HttpPut("simple/allocations/hours/{mode}")]
		public IActionResult EditSimpleHours(int mode, [FromBody] List<SimpleAllocationHours> items) {
			GetAllocationService().UpdateSimpleHours(mode, items);
			return new ObjectResult("");
		}

		[HttpGet("simple/allocations/sum/{itemId}/{year}/{process}/{filter_item_ids}")]
		public Dictionary<string, object>
			EditSimpleHours(int itemId, int year, string process, string filter_item_ids) {
			return GetAllocationService().GetAllocationSums(itemId, year, process, filter_item_ids);
		}

		[HttpPut("simple/allocations/move/{processItemId}/{year}/{mode}/{process}/{filter_item_ids}/{portfolio_id}")]
		public IActionResult MoveSimpleAllocation(int processItemId, int year, int mode, string process,
			string filter_item_ids, int portfolio_id, [FromBody] List<AllocationShift> shifts) {
			AllocationService allocation_service = GetAllocationService();
			foreach (AllocationShift shift in shifts) {
				allocation_service.MoveAllocation(shift);
			}

			return GetSimpleAllocations(processItemId, year, mode, process, filter_item_ids, portfolio_id);
		}

		// TASK DURATION EDITING

		[HttpPost("task/{taskId}/resource/{resourceId}")]
		public IActionResult Post(int taskId, int resourceId) {
			var retObj = GetTaskDbService().BindPersonToTask(resourceId, taskId);
			return new ObjectResult(retObj);
		}

		[HttpDelete("task/{taskId}/resource/{resourceId}")]
		public void DeleteUserFromTask(int taskId, int resourceId) {
			GetTaskDbService().RemovePersonFromTask(resourceId, taskId);
		}

		[HttpPut("task/dates")]
		public IActionResult Put([FromBody] List<CreateTaskDurations> taskDurations) {
			GetTaskDbService().SaveTaskDurations(taskDurations);
			return new ObjectResult(taskDurations);
		}

		// ALLOCATION DURATION EDITING


		[HttpGet("allocation/{id}")]
		public IActionResult GetAllocation(int id) {
			return new ObjectResult(GetAllocationService().GetAllocation(id));
		}

		[HttpPost("allocation")]
		public string PostAllocationRequest([FromBody] EditAllocationModel allocationModel) {
			var taskDurations = GetAllocationService();
			try {
				return success(taskDurations.CreateAllocationRequest(allocationModel, currentSession.item_id));
			} catch (ArgumentException e) {
				return failure(FailureCodes.FailureInRequest, e);
			}
		}

		[HttpPut("allocation/{id}")]
		public string UpdateAllocationActionResult([FromBody] EditAllocationModel allocationModel, int id) {
			allocationModel.item_id = id;
			return success(GetAllocationService().UpdateAllocation(allocationModel));
		}

		[HttpPut("allocation/dates")]
		public string updateDurations([FromBody] List<AllocationDuration> durationsByDate) {
			GetAllocationService().UpdateAllocationDurations(durationsByDate, false);
			return success();
		}

		[HttpPost("allocation/{id}/status/{status}")]
		public IActionResult DenyRequest(int id, int status) {
			var item = GetAllocationService().UpdateAllocationRequestStatus(id, status);
			return new ObjectResult(item);
		}

		[HttpDelete("allocation/{id}")]
		public void DeleteAllocation(int id) {
			GetAllocationService().DeleteAllocation(id);
		}

		[HttpPost("allocation/task/{taskId}/todraft")]
		public void taskAllocationToDraft(int taskId) {
			GetAllocationService().SetTaskAllocationsToDraft(taskId);
		}

		[HttpPost("allocation/quickLook")]
		public List<AllocationService.QuickLookResult> GetUserQuickLook([FromBody] List<int> ids) {
			return GetAllocationService().GetAllocationQuickLook(ids);
		}

		private ResourceManagementRepository GetRepository(DateTime? date = null) {
			var cb = new ConnectionBase(instance, currentSession, date);
			var singleItemOperationsFactory = new EntityServiceProvider(instance, currentSession);
			return new ResourceManagementRepository(cb, singleItemOperationsFactory);
		}

		private AllocationService GetAllocationService() {
			return new AllocationService(instance, currentSession, GetRepository());
		}

		private TaskService GetTaskDbService() {
			return new TaskService(instance, currentSession, GetRepository());
		}

		private ResourceManagementQueryService getQueryService(string date = null) {
			DateTime? parsedDate = null;
			if (date != null) {
				parsedDate = DateTime.ParseExact(date, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
			}

			return new ResourceManagementQueryService(instance, currentSession, GetRepository(parsedDate), parsedDate);
		}

		public class ResourceAllocationValues
		{
			public string Process { get; set; }
			public int ItemId { get; set; }
		}
	}
}