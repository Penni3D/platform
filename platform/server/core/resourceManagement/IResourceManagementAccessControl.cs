﻿using System.Collections.Generic;

namespace Keto5.x.platform.server.core.resourceManagement{
	public interface IResourceManagementAccessControl{
		void IAmGoingToWriteToProcess(int processItemId, string process);

		//void CanIReadAndWriteToProcess(int processItemId, string process);
		void EnsureItemType(int resourceItemId, List<object> allowedProcessTypes = null);

		bool IsStatucChangeAllowed(AllocationStatus current,
			AllocationStatus newStatus,
			bool allocationAuthor,
			bool ownerOfResource,
			int processItemId);
	}
}