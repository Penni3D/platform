﻿using System.Collections.Generic;

namespace Keto5.x.platform.server.core.resourceManagement {
	public static class ResourceManagementAllowedStatusChanges {
		// Rules for the dude who created request or is allowed to write to process

		public static readonly Dictionary<AllocationStatus, HashSet<AllocationStatus>>
			AllowedAllocationStatusChangesForAuthor =
				new Dictionary<AllocationStatus, HashSet<AllocationStatus>> {
					{
						AllocationStatus.Draft,
						new HashSet<AllocationStatus> { AllocationStatus.Draft, AllocationStatus.Requested }
					}, {
						AllocationStatus.PreviouslyApproved,
						new HashSet<AllocationStatus> {
							AllocationStatus.PreviouslyApproved, AllocationStatus.Requested, AllocationStatus.Draft,
							AllocationStatus.Approved
						}
					}, {
						AllocationStatus.Requested,
						new HashSet<AllocationStatus> { AllocationStatus.Draft, AllocationStatus.Requested }
					}, {
						AllocationStatus.RequestedModified,
						new HashSet<AllocationStatus> { AllocationStatus.Draft }
					}, {
						AllocationStatus.Denied,
						new HashSet<AllocationStatus> { AllocationStatus.Denied, AllocationStatus.Draft }
					}, {
						AllocationStatus.ApprovedWithModifications,
						new HashSet<AllocationStatus> {
							AllocationStatus.Requested,
							AllocationStatus.Denied,
							AllocationStatus.Draft,
							AllocationStatus.Approved,
							AllocationStatus.ApprovedWithModifications,
							AllocationStatus.PreviouslyApproved
						}
					}, {
						AllocationStatus.Approved,
						new HashSet<AllocationStatus>
							{ AllocationStatus.Approved, AllocationStatus.PreviouslyApproved, AllocationStatus.Draft }
					}, {
						AllocationStatus.StatusForTasks,
						new HashSet<AllocationStatus> { AllocationStatus.StatusForTasks }
					}
				};

		// Rules for the users who owns cost center that resource belongs to
		public static readonly Dictionary<AllocationStatus, HashSet<AllocationStatus>>
			AllowedAllocationStatusChangesForResourceOwner =
				new Dictionary<AllocationStatus, HashSet<AllocationStatus>> {
					{
						AllocationStatus.Draft, new HashSet<AllocationStatus> {
							AllocationStatus.Draft
						}
					}, {
						AllocationStatus.PreviouslyApproved, new HashSet<AllocationStatus> {
							AllocationStatus.PreviouslyApproved, AllocationStatus.Draft
						}
					}, {
						AllocationStatus.Requested,
						new HashSet<AllocationStatus> {
							AllocationStatus.Requested,
							AllocationStatus.RequestedModified,
							AllocationStatus.Denied,
							AllocationStatus.Approved
						}
					}, {
						AllocationStatus.RequestedModified,
						new HashSet<AllocationStatus> {
							AllocationStatus.Draft,
							AllocationStatus.Denied,
							AllocationStatus.RequestedModified,
							AllocationStatus.ApprovedWithModifications
						}
					}, {
						AllocationStatus.Denied, new HashSet<AllocationStatus> {
							AllocationStatus.Denied
						}
					}, {
						AllocationStatus.ApprovedWithModifications,
						new HashSet<AllocationStatus> {
							AllocationStatus.ApprovedWithModifications,
							AllocationStatus.PreviouslyApproved,
							AllocationStatus.Requested,
							AllocationStatus.Draft,
							AllocationStatus.Approved,
							AllocationStatus.Released
						}
					}, {
						AllocationStatus.Approved, new HashSet<AllocationStatus> {
							AllocationStatus.Approved, AllocationStatus.PreviouslyApproved, AllocationStatus.ApprovedWithModifications
						}
					}, {
						AllocationStatus.StatusForTasks, new HashSet<AllocationStatus> {
							AllocationStatus.StatusForTasks
						}
					}
				};
	}
}