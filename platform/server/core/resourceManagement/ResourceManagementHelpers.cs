﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;

namespace Keto5.x.platform.server.core.resourceManagement{

	public enum TaskType {
		KanbanTask = 0,
		TaskOfPhaseOrSprint = 1,
		GanttPhase = 2,
		Sprint = 21,
		Project = 3,
		AllocationOrTask = 50,
		UserWithAllocation = 51,
		ProjectProcess = 52
	}

	/* DB MODELS */

	public enum AllocationStatus {
		Undefined = 0,
		Draft = 10, // explicit values are given for each item to avoid conflicts after possible modifications
		PreviouslyApproved = 13, // acts like drafts, but it has been approved and then cancelled
		Requested = 20,
		RequestedModified = 25,
		Denied = 30,
		ApprovedWithModifications = 40,
		Approved = 50,
		
		StatusForExternalAllocationLoad = 53, // used for load from other processes
		StatusForTasks = 55, // used for tasks in dates-dictionary 
		ExternalLoad = 60,
		Released = 70
	}

	public class DateAndDuration {
		// CONSTRUCTORS
		public DateAndDuration() { }

		public DateAndDuration(DataRow dataRow) {
			hours = Conv.ToDouble(dataRow[ResourceManagementCommonService.HoursColumnName]);
			date = Conv.ToStr(dataRow[ResourceManagementCommonService.DateColumnName]);
		}

		public double hours { get; set; }
		public string date { get; set; }
	}

	public class AllocationDuration : DateAndDuration {
		public AllocationDuration() { }

		public AllocationDuration(DataRow dataRow) : base(dataRow) {
			allocation_item_id = Conv.ToInt(dataRow[ResourceManagementCommonService.AllocationItemIdColumnName]);
		}

		public int allocation_item_id { get; set; }
	}

	public class TaskDuration : DateAndDuration {
		// CONSTRUCTORS
		public TaskDuration() { }

		public TaskDuration(DataRow dataRow) : base(dataRow) {
			task_item_id = Conv.ToInt(dataRow[ResourceManagementCommonService.TaskItemIdColumnName]);
			resource_item_id = Conv.ToInt(dataRow[ResourceManagementCommonService.ResourceItemIdColumnName]);
		}

		public int task_item_id { get; set; }

		public int resource_item_id { get; set; }
	}

	public class CreateTaskDurations {
		// CONSTRUCTORS
		public int task_item_id { get; set; }
		public int resource_item_id { get; set; }
		public double hours { get; set; }
		public DateTime date { get; set; }
	}

	public abstract class DurationsBase {
		protected DurationsBase() { }

		protected DurationsBase(DataRow dataRow) { }
		public int item_id { get; set; }
		public List<DateAndDuration> Durations { get; set; } = new List<DateAndDuration>();

		public void SetDurations(DataTable durationsTable) {
			Durations = new List<DateAndDuration>();
			foreach (DataRow durationsTableRow in durationsTable.Rows) {
				Durations.Add(new DateAndDuration(durationsTableRow));
			}
		}
	}

	public class AllocationBase : DurationsBase {
		public AllocationBase() { }

		// CONSTRUCTORS
		public AllocationBase(DataRow dataRow) : base(dataRow) {
			item_id = (int) GetValueIfAvailable(dataRow, ResourceManagementCommonService.ItemIdColumnName);
			title = Conv.ToStr(GetValueIfAvailable(dataRow, ResourceManagementCommonService.TitleColumnName));
			resource_item_id = Conv.ToInt(GetValueIfAvailable(dataRow, ResourceManagementCommonService.ResourceItemIdColumnName));
			creator_item_id = Conv.ToInt(GetValueIfAvailable(dataRow, ResourceManagementCommonService.CreatorItemIdColumnName));
			status = (AllocationStatus) Conv.ToInt(GetValueIfAvailable(dataRow, ResourceManagementCommonService.StatusColumnName));
			task_item_id = Conv.ToInt(GetValueIfAvailable(dataRow, ResourceManagementCommonService.TaskItemIdColumnName));
			process_item_id = Conv.ToInt(GetValueIfAvailable(dataRow, ResourceManagementCommonService.ProcessItemIdColumnName));
			order_no = Conv.ToStr(GetValueIfAvailable(dataRow, "order_no"));
			cost_center = Conv.ToInt(GetValueIfAvailable(dataRow, "cost_center"));
			resource_type = Conv.ToStr(GetValueIfAvailable(dataRow, "resource_type"));
			work_category_item_id = Conv.ToInt(GetValueIfAvailable(dataRow, "work_category_item_id"));
			user_item_id = 0;
		}

		public AllocationBase(Dictionary<string, object> item) {
			item_id = Conv.ToInt(GetValueIfAvailable(item, ResourceManagementCommonService.ItemIdColumnName));
			title = Conv.ToStr(GetValueIfAvailable(item, ResourceManagementCommonService.TitleColumnName));
			resource_item_id = Conv.ToInt(GetValueIfAvailable(item, ResourceManagementCommonService.ResourceItemIdColumnName));
			creator_item_id = Conv.ToInt(GetValueIfAvailable(item, ResourceManagementCommonService.CreatorItemIdColumnName));
			status = (AllocationStatus) Conv.ToInt(GetValueIfAvailable(item, ResourceManagementCommonService.StatusColumnName));
			task_item_id = Conv.ToInt(GetValueIfAvailable(item, ResourceManagementCommonService.TaskItemIdColumnName));
			process_item_id = Conv.ToInt(GetValueIfAvailable(item, ResourceManagementCommonService.ProcessItemIdColumnName));
			order_no = Conv.ToStr(GetValueIfAvailable(item, "order_no"));
			cost_center = Conv.ToInt(GetValueIfAvailable(item, "cost_center"));
			resource_type = Conv.ToStr(GetValueIfAvailable(item, "resource_type"));
			work_category_item_id = Conv.ToInt(GetValueIfAvailable(item, "work_category_item_id"));
			user_item_id = 0;
		}

		public string title { get; set; }
		public int resource_item_id { get; set; }
		public int creator_item_id { get; set; }
		public int? task_item_id { get; set; }
		public int process_item_id { get; set; }
		public string order_no { get; set; }
		public int cost_center{ get; set; }
		public string resource_type{ get; set; }
		public AllocationStatus status { get; set; }
		public int work_category_item_id { get; set; }
		public int user_item_id { get; set; }

		public static object GetValueIfAvailable(Dictionary<string, object> dict, string key) {
			if (dict == null) {
				return null;
			}
			return !dict.ContainsKey(key) ? null : dict[key];
		}

		public static object GetValueIfAvailable(DataRow row, string key) {
			if (row == null) {
				return null;
			}
			return !row.Table.Columns.Contains(key) ? null : row[key];
		}
	}

	/// <summary>
	/// Used when inserting or updating allocations
	/// </summary>
	public class EditAllocationModel : AllocationBase {
		// CONSTRUCTORS
		public EditAllocationModel() { }

		public EditAllocationModel(Dictionary<string, object> item) : base(item) { }
		public string NewComment { get; set; }
	}

	/// <summary>
	/// Comment object. Relates to allocate. Read only.
	/// </summary>
	public class AllocationComment {
		public AllocationComment() { }

		public AllocationComment(DataRow dataRow) {
			Id = Conv.ToInt(dataRow["allocation_comment_id"]);
			AllocationId = Conv.ToInt(dataRow[ResourceManagementCommonService.AllocationItemIdColumnName]);
			Comment = Conv.ToStr(dataRow["comment"]);
			CreatedAt = Conv.ToDateTime(dataRow["created_at"]);
			Author = Conv.ToStr(dataRow["author_name"]);
		}

		public int Id { get; set; }
		public int AllocationId { get; set; }
		public string Comment { get; set; }
		public DateTime? CreatedAt { get; set; }
		public string Author { get; set; }
	}

	public class NewAllocationComment {
		public string comment { get; set; }
	}

	/// <summary>
	/// Allocation object when GETting stuff. Read only.
	/// </summary>
	public class Allocation : AllocationBase {
		// CONSTRUCTORS
		public Allocation(DataRow dataRow) : base(dataRow) { }

		public Allocation(Dictionary<string, object> item) : base(item) { }
		public List<AllocationComment> Comments { get; set; } = new List<AllocationComment>();
	}

	/// <summary>
	/// Object used when getting items split for different dates. Contains data for one day.
	/// </summary>
	public class ItemsByDate {
		public List<Dictionary<string, object>> Allocations { get; set; } = new List<Dictionary<string, object>>();
		public List<Dictionary<string, object>> Tasks { get; set; } = new List<Dictionary<string, object>>();

		public void AddTaskHours(int taskId, double hours, int resourceId) {
			var itemsInDictionary = new Dictionary<string, object>();
			itemsInDictionary.Add("taskId", taskId);
			itemsInDictionary.Add("hours", hours);
			itemsInDictionary.Add("resourceId", resourceId);
			Tasks.Add(itemsInDictionary);
		}

		public void AddAllocationHours(int allocationId, double hours, int resourceId) {
			var itemsInDictionary = new Dictionary<string, object>();
			itemsInDictionary.Add("allocationId", allocationId);
			itemsInDictionary.Add("hours", hours);
			itemsInDictionary.Add("resourceId", resourceId);
			Allocations.Add(itemsInDictionary);
		}

		public ItemsByDate MergeWith(ItemsByDate itemToMerge) {
			if (itemToMerge == null) {
				return this;
			}
			Tasks.AddRange(itemToMerge.Tasks);
			Allocations.AddRange(itemToMerge.Allocations);
			return this;
		}
	}
		// DB MODELS END

	// HELPER MODELS
	public class CalendarDataObject : ItemsByResourceByDate {
		private readonly Dictionary<string, object> _baseObject = new Dictionary<string, object>();
		private readonly List<Dictionary<string, object>> _items;

		public CalendarDataObject() {
			_items = new List<Dictionary<string, object>>();
		}

		public void AddRowData(Dictionary<string, object> item) {
			_items.Add(item);
		}

		public void AddRowData(List<Dictionary<string, object>> items) {
			_items.AddRange(items);
		}

		public void AddRowData(DataTable items) {
			foreach (DataRow itemsRow in items.Rows) {
				_items.Add(Conv.DataRowToDictionary(itemsRow));
			}
		}

		public Dictionary<string, object> GetObject() {
			_baseObject.Add(ResourceManagementCommonService.DatesProperty, GetItems());
			_baseObject.Add(ResourceManagementCommonService.RowDataProperty, _items);
			return _baseObject;
		}

		public void AddAdditionalProperty(string key, object obj) {
			_baseObject.Add(key, obj);
		}
	}

	public class ItemsByResourceByDate {
		private Dictionary<int, object> _itemsByDate = new Dictionary<int, object>();

		private Dictionary<int, object> GetItemsDict() {
			if (_itemsByDate == null) {
				_itemsByDate = new Dictionary<int, object>();
			}
			return _itemsByDate;
		}

		private Dictionary<string, object> GetDictionaryForDate(DateTime date) {
			var dict = GetItemsDict();
			var year = date.Year;
			var month = date.Month;
			var day = date.Day;
			if (!dict.ContainsKey(year)) {
				dict.Add(year, new Dictionary<int, object>());
			}
			var dictForYear = (Dictionary<int, object>) dict[year];

			if (!dictForYear.ContainsKey(month)) {
				dictForYear.Add(month, new Dictionary<int, object>());
			}
			var dictForMonth = (Dictionary<int, object>) dictForYear[month];

			if (!dictForMonth.ContainsKey(day)) {
				dictForMonth.Add(day, new Dictionary<string, object>());
			}

			return (Dictionary<string, object>) dictForMonth[day];
		}

		public void SetHoursForItem(DateTime date, string itemId, double hours, string type) {
			var dictForDate = GetDictionaryForDate(date);

			hours = Math.Round(hours, 3); //This reduces output size as useless decimals are removed --BSa 22.4.2020
			
			Dictionary<string, double> dictForItem;
			if (dictForDate.ContainsKey(itemId)) {
				dictForItem = (Dictionary<string, double>) dictForDate[itemId];
			} else {
				dictForItem = new Dictionary<string, double>();
				dictForDate.Add(itemId, dictForItem);
			}

			if (dictForItem.ContainsKey(type)) {
				dictForItem[type] = dictForItem[type] + hours;
			} else {
				dictForItem.Add(type, hours);
			}
		}

		public Dictionary<int, object> GetItems() {
			return _itemsByDate;
		}
	}

	public class CalendarItems {
		private readonly List<Dictionary<string, object>> _allocations;
		private readonly HashSet<int> _projectIds;
		private readonly HashSet<int> _resourceIds;

		public CalendarItems() {
			_allocations = new List<Dictionary<string, object>>();
			_resourceIds = new HashSet<int>();
			_projectIds = new HashSet<int>();
		}

		public void AddAllocationItem(Dictionary<string, object> allocation) {
			_allocations.Add(allocation);
			var resourceId = Conv.ToInt(allocation[ResourceManagementCommonService.ResourceItemIdColumnName]);
			var projectId = Conv.ToInt(allocation[ResourceManagementCommonService.ProcessItemIdColumnName]);

			_resourceIds.Add(resourceId);
			_projectIds.Add(projectId);
		}
	}

	public class DateAndDurationWithDay : DateAndDuration {
		public int day { get; set; }
		public int year { get; set; }
	}

	public class SimpleAllocationHours {
		public List<DateAndDurationWithDay> dates;

		public int month { get; set; }
		public int year { get; set; }
		public int allocation_item_id { get; set; }
	}

	public class AllocationInformation {
		public AllocationInformation(List<Dictionary<string, object>> items) {
			if (items.Count != 3) {
				return;
			}

			FirstAllocationDate = (DateTime)Conv.ToDateTime(items.First()["value"]);
			LastAllocationDate = (DateTime)Conv.ToDateTime(items[1]["value"]);
			TotalHours = Conv.ToDouble(items[2]["value"]);
		}

		public DateTime FirstAllocationDate { get; set; }
		public DateTime LastAllocationDate { get; set; }
		public double TotalHours { get; set; }
	}
}