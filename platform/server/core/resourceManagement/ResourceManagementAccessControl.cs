﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core.resourceManagement{
	
	public class ResourceManagementAccessControl : IResourceManagementAccessControl {
		private readonly IResourceManagementRepository _repository;

		private readonly AuthenticatedSession authenticatedSession;
		private readonly string instance;

		public ResourceManagementAccessControl(string instance, AuthenticatedSession session, IResourceManagementRepository repository) {
			authenticatedSession = session;
			this.instance = instance;
			_repository = repository;
		}

		public void IAmGoingToWriteToProcess(int processItemId, string process) {
			var accessControl = new ProcessAccessControlService(instance, authenticatedSession);
			accessControl.IAmGoingToWriteToProcessFeature(processItemId, process, "project_allocation");
		}

//		public void CanIReadAndWriteToProcess(int processItemId, string process){
//			var accessControl = new ProcessAccessControlService(instance, authenticatedSession);
//			accessControl.IAmGoingToReadAndWriteToProcessFeature(processItemId, process, "project_allocation");
//		}

		/// <summary>
		/// Ensures that item exists
		/// </summary>
		/// <param name="resourceItemId"></param>
		/// <param name="allowedProcessTypes"></param>
		/// <exception cref="CustomArgumentException"></exception>
		public void EnsureItemType(int resourceItemId, List<object> allowedProcessTypes = null){
			var cb = new ConnectionBase(instance, authenticatedSession);
			var param = cb.SetDBParameter("@itemId", resourceItemId);
			var instanceParam = cb.SetDBParameter("@instance", cb.instance);
			var conditions = new SqlCondition("instance", SqlComp.EQUALS, instanceParam)
				.And(new SqlCondition("item_id", SqlComp.EQUALS, param));
			if (allowedProcessTypes != null && allowedProcessTypes.Count > 0){
				conditions = conditions.And(new SqlCondition().Column("process").In(allowedProcessTypes));
			}
			var query = new SelectQuery()
				.From("items")
				.Where(conditions);

			var dataRow = cb.db.GetDataRow(query, cb.DBParameters);

			if (dataRow == null){
				throw new CustomArgumentException("RESOURCE_NOT_FOUND" + resourceItemId);
			}
		}


		public bool IsStatucChangeAllowed(AllocationStatus current,
			AllocationStatus newStatus,
			bool allocationAuthor,
			bool ownerOfResource,
			int processItemId){

			var isAllowed = false;
			
			// Cost center owner fix
			isAllowed = ownerOfResource && ResourceManagementAllowedStatusChanges.AllowedAllocationStatusChangesForResourceOwner[current].Contains(newStatus);

			if (!isAllowed){
				IAmGoingToWriteToProcess(
					processItemId,
					_repository.GetProcessNameForProcessItemId(processItemId));
				isAllowed = ResourceManagementAllowedStatusChanges.AllowedAllocationStatusChangesForAuthor[current].Contains(newStatus);
			}
			return isAllowed;
		}
	}
}