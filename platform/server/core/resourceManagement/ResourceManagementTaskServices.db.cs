using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core.resourceManagement {


	// DATABASE SERVICES
	public class TaskService : ResourceManagementCommonService {
		public TaskService(string instance, AuthenticatedSession session, IResourceManagementRepository repository) 
			: base(instance, session, repository){
			
		}

		public Dictionary<string, object> BindPersonToTask(int resourceId, int taskId) {
			var itemBaseForTask = new ItemBase(instance, TaskProcessName, AuthenticatedSession);
			// Fetch task-object
			var task = itemBaseForTask.GetItem(taskId);
			var resourceIdParam = SetDBParameter("@resourceId", resourceId);
			var taskIdParam = SetDBParameter("@taskId", taskId);
			var taskDurationsForUser = new SelectQuery()
				.From(TaskDurationTableName)
				.Where(new SqlCondition(TaskItemIdColumnName, SqlComp.EQUALS, taskIdParam)
					.And(new SqlCondition(ResourceItemIdColumnName, SqlComp.EQUALS, resourceIdParam)));
			var hoursForUserAndTask = db.GetDatatableDictionary(taskDurationsForUser, DBParameters);
			if (hoursForUserAndTask.Count == 0) {
				var date = DateTime.Now;
				SetDBParam(SqlDbType.DateTime, "@date", date);
				var ins = "INSERT INTO " + TaskDurationTableName + " " +
				          "(resource_item_id, task_item_id, date, hours) " +
				          "VALUES (@resourceId, @taskId, @date, 0);";
				db.ExecuteInsertQuery(ins, DBParameters);
			}
			
			return GetDurationsByTaskAndUsers(task, resourceId);
		}


		public void RemovePersonFromTask(int resourceId, int taskId) {
			var sql = "DELETE FROM " + TaskDurationTableName + " " +
			          "WHERE resource_item_id=@resourceItemId AND task_item_id=@taskId";
			SetDBParam(SqlDbType.Int, "@resourceItemId", resourceId);
			SetDBParam(SqlDbType.Int, "@taskId", taskId);
			db.ExecuteDeleteQuery(sql, DBParameters);
		}

		public void SaveTaskDurations(List<CreateTaskDurations> taskDurations) {
			var deleteQueries = "";
			var insertQueries = "";
			var i = 0;
			foreach (var taskDuration in taskDurations) {
				_resourceManagementAccessControl.EnsureItemType(taskDuration.resource_item_id);
				_resourceManagementAccessControl.EnsureItemType(taskDuration.task_item_id);
				var dateParamName = "@date" + i;
				var idParamName = "@taskItemId" + i;
				var resourceItemIdParam = "@resourceItemId" + i;
				var hourParameter = "@hours" + i;
				
				SetDBParameters(
					dateParamName, taskDuration.date,
					idParamName, taskDuration.task_item_id,
					resourceItemIdParam, taskDuration.resource_item_id,
					hourParameter, taskDuration.hours);
				
				var deleteQuery = "DELETE FROM task_durations " +
				                  "WHERE date=" + dateParamName + " " +
				                  "AND task_item_id=" + idParamName + " " +
				                  "AND resource_item_id = " + resourceItemIdParam + ";";
				deleteQueries += deleteQuery;
				if (taskDuration.hours > 0) {
					var ins = "INSERT INTO " + TaskDurationTableName +
					          " (resource_item_id, task_item_id, date, hours) " +
					          "VALUES (" + 
					          resourceItemIdParam + ", " + 
					          idParamName + ", " + 
					          dateParamName + ", " +
					          hourParameter + ");";
					insertQueries += ins;
				}
				i++;
			}

			if (!string.IsNullOrEmpty(deleteQueries)) {
				db.ExecuteDeleteQuery(deleteQueries, DBParameters);
			}
			if (!string.IsNullOrEmpty(insertQueries)) {
				db.ExecuteInsertQuery(insertQueries, DBParameters);
			}
		}
	}
}