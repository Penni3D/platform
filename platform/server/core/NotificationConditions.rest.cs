﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/settings/NotificationConditions")]
	public class NotificationConditionsRest : RestBase {
		// GET: model/values
		[HttpGet("{process}")]
		public List<NotificationCondition> Get(string process) {
			var conditions = new NotificationConditions(instance, process, currentSession);
			return conditions.GetNotificationConditions();
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]NotificationCondition condition) {
			var conditions = new NotificationConditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				conditions.Add(condition);
				return new ObjectResult(condition);
			}
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]List<NotificationCondition> conditions) {
			var c = new NotificationConditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var condition in conditions) {
					c.Save(condition);
				}
				return new ObjectResult(conditions);
			}
			return null;
		}

		[HttpDelete("{process}/{notificationConditionIds}")]
		public IActionResult Delete(string process, string notificationConditionIds) {
			var c = new NotificationConditions(instance, process, currentSession);
			foreach (var id in notificationConditionIds.Split(',')) {
				c.Delete(Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}
}