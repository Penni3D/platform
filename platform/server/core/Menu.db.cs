﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core {
	public class InstanceMenu {
		public InstanceMenu() { }

		public InstanceMenu(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) { }

			InstanceMenuId = (int) row["instance_menu_id"];
			if (row["variable"] != DBNull.Value) {
				Variable = (string) row["variable"];
				LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
			}
			
			if (row["parent_id"] != DBNull.Value) ParentId = (int?) row["parent_id"];
			if (row["default_state"] != DBNull.Value) DefaultState = (string) row["default_state"];
			if (row["params"] != DBNull.Value && Conv.ToStr(row["params"]) != "null") {
				try {
					Params = JsonConvert.DeserializeObject<Dictionary<string, object>>((string) row["params"]);
					if (Params.ContainsKey("color")) Color = Conv.ToStr(Params["color"]);
					if (Params.ContainsKey("disableIcon")) DisableIcon = Conv.ToStr(Params["disableIcon"]);
				} catch (Exception e) {
					Diag.SupressException(e);
				}
			}

			OrderNo = (string) row["order_no"];
			if (row["icon"] != DBNull.Value) Icon = (string) row["icon"];

			LinkType = (int) row["link_type"];
		}

		public int InstanceMenuId { get; set; }
		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public int? ParentId { get; set; }

		public string OrderNo { get; set; }
		public string DefaultState { get; set; }
		public string Icon { get; set; }
		public Dictionary<string, object> Params { get; set; }
		public List<int> UserGroups { get; set; }

		public int LinkType { get; set; }
		public Dictionary<int, List<string>> UserGroupStates { get; set; }

		public List<InstanceMenu> Links { get; set; }

		public string Color { get; set; }

		public string DisableIcon { get; set; }
	}

	public class InstanceMenus : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;
		private readonly Translations translations;

		public InstanceMenus(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			translations = new Translations(instance, authenticatedSession);
			this.authenticatedSession = authenticatedSession;
		}

		private DataTable _InstanceMenuCache;

		private DataTable InstanceMenuCache {
			get {
				if (_InstanceMenuCache == null)
					_InstanceMenuCache = (DataTable) Cache.Get(instance, "InstanceMenuCache");

				if (_InstanceMenuCache == null) {
					_InstanceMenuCache = db.GetDatatable(
						"SELECT ROW_NUMBER() OVER (ORDER BY order_no  ASC) AS final_order, instance_menu_id, variable, ISNULL(parent_id,0) AS parent_id, order_no, default_state, params, icon, ISNULL(link_type,0) AS link_type FROM instance_menu WHERE instance = @instance ORDER BY order_no  ASC",
						DBParameters);
					Cache.Set(instance, "InstanceMenuCache", _InstanceMenuCache);
				}

				return _InstanceMenuCache;
			}
		}

		public List<InstanceMenu> GetParentInstanceMenus() {
			var imr = new InstanceMenuRights(instance, authenticatedSession.item_id);
			var result = new List<InstanceMenu>();

			foreach (DataRow row in InstanceMenuCache.Select("parent_id = 0 AND link_type IN (0,1,2)",
				"final_order ASC")) {
				var im = new InstanceMenu(row, authenticatedSession);
				im.UserGroups = imr.GetUserGroupsForMenuId(im.InstanceMenuId);
				im.UserGroupStates = imr.GetUserGroupStatesForMenuId(im.InstanceMenuId);
				result.Add(im);
			}

			return result;
		}

		public List<InstanceMenu> GetChildInstanceMenus(int parentId) {
			var imr = new InstanceMenuRights(instance, authenticatedSession.item_id);
			var result = new List<InstanceMenu>();

			foreach (DataRow row in InstanceMenuCache.Select("parent_id = " + parentId + " AND link_type IN (0,1,2)",
				"final_order ASC")) {
				var im = new InstanceMenu(row, authenticatedSession);
				im.UserGroups = imr.GetUserGroupsForMenuId(im.InstanceMenuId);
				im.UserGroupStates = imr.GetUserGroupStatesForMenuId(im.InstanceMenuId);
				result.Add(im);
			}

			return result;
		}


		public List<InstanceMenu> GetFrontpage() {
			var ug = new UserGroups(instance, authenticatedSession);
			var retval = new List<InstanceMenu>();
			var userGroups = string.Join(",", ug.GetUserGroupsForAuthenticatedUser());

			var db = new Connections(authenticatedSession);
			var sql =
				"SELECT im.instance_menu_id, im.variable, im.parent_id, im.order_no, im.default_state, im.params, im.icon, im.link_type FROM instance_menu im " +
				"LEFT JOIN instance_menu_rights ims ON im.instance_menu_id = ims.instance_menu_id " +
				"WHERE im.instance = @instance AND im.link_type IN (4, 5) AND (item_id IS NULL";

			if (userGroups.Length > 0) {
				sql += " OR item_id IN (" + userGroups + ")";
			}


			sql +=
				") GROUP BY im.instance_menu_id, im.variable, im.parent_id, im.order_no, im.default_state, im.params, im.icon, im.link_type " +
				"ORDER BY order_no  ASC";

			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var im = new InstanceMenu(row, authenticatedSession);
				retval.Add(im);
			}

			return retval;
		}


		public List<InstanceMenu> GetInstanceMenuHierarchy() {
			var imr = new InstanceMenuRights(instance, authenticatedSession.item_id);
			var result = new List<InstanceMenu>();
			foreach (DataRow row in InstanceMenuCache.Select("parent_id = 0 AND link_type IN (0,1,2)",
				"final_order ASC")) {
				var im = new InstanceMenu(row, authenticatedSession);
				im.UserGroups = imr.GetUserGroupsForMenuId(im.InstanceMenuId);
				im.UserGroupStates = imr.GetUserGroupStatesForMenuId(im.InstanceMenuId);
				im.Links = new List<InstanceMenu>();

				foreach (DataRow row2 in InstanceMenuCache.Select("parent_id = " + im.InstanceMenuId, "final_order ASC")
				) {
					var im2 = new InstanceMenu(row2, authenticatedSession);
					im2.UserGroups = imr.GetUserGroupsForMenuId(im2.InstanceMenuId);
					im2.UserGroupStates = imr.GetUserGroupStatesForMenuId(im2.InstanceMenuId);
					im.Links.Add(im2);
				}

				result.Add(im);
			}

			return result;
		}

		public List<InstanceMenu> GetInstanceFrontpage() {
			var imr = new InstanceMenuRights(instance, authenticatedSession.item_id);
			var result = new List<InstanceMenu>();
			foreach (DataRow row in InstanceMenuCache.Select("link_type IN (4,5)", "final_order ASC")) {
				var im = new InstanceMenu(row, authenticatedSession);
				im.UserGroups = imr.GetUserGroupsForMenuId(im.InstanceMenuId);
				im.UserGroupStates = imr.GetUserGroupStatesForMenuId(im.InstanceMenuId);
				result.Add(im);
			}

			return result;
		}

		public List<InstanceMenu> GetInstanceGlobals() {
			var imr = new InstanceMenuRights(instance, authenticatedSession.item_id);
			var result = new List<InstanceMenu>();

			foreach (DataRow row in InstanceMenuCache.Select("link_type = 3", "final_order ASC")) {
				var im = new InstanceMenu(row, authenticatedSession);
				im.UserGroups = imr.GetUserGroupsForMenuId(im.InstanceMenuId);
				im.UserGroupStates = imr.GetUserGroupStatesForMenuId(im.InstanceMenuId);
				result.Add(im);
			}

			return result;
		}

		public InstanceMenu NewInstanceMenu(InstanceMenu menu) {
			SetDBParam(SqlDbType.Int, "@parentId", Conv.IfNullThenDbNull(menu.ParentId));
			SetDBParam(SqlDbType.NVarChar, "@defaultState", Conv.IfNullThenDbNull(menu.DefaultState));
			SetDBParam(SqlDbType.Int, "@linkType", menu.LinkType);

			menu.InstanceMenuId = db.ExecuteInsertQuery(
				"INSERT INTO instance_menu(instance, default_state, link_type) " +
				"VALUES(@instance, @defaultState, @linkType)", DBParameters);

			return SaveInstanceMenu(menu.InstanceMenuId, menu);
		}

		public InstanceMenu SaveInstanceMenu(int instanceMenuId, InstanceMenu menu) {
			string variable = null;
			SetDBParam(SqlDbType.Int, "@instanceMenuId", instanceMenuId);

			if (menu.ParentId == 0) menu.ParentId = null;
			SetDBParam(SqlDbType.Int, "@parentId", Conv.IfNullThenDbNull(menu.ParentId));
			
			SetDBParam(SqlDbType.NVarChar, "@orderNo", menu.OrderNo);
			SetDBParam(SqlDbType.Int, "@linkType", menu.LinkType);
			SetDBParam(SqlDbType.NVarChar, "@params", JsonConvert.SerializeObject(menu.Params));
			SetDBParam(SqlDbType.NVarChar, "@icon", Conv.IfNullThenDbNull(menu.Icon));

			db.ExecuteUpdateQuery(
				"UPDATE instance_menu " +
				"SET parent_id = @parentId, order_no = @orderNo, params = @params, icon = @icon, link_type = @linkType WHERE " +
				"instance = @instance AND instance_menu_id = @instanceMenuId", DBParameters);

			if (menu.Variable != null) {
				variable = menu.Variable;
			} else if (menu.LanguageTranslation != null) {
				if (menu.LanguageTranslation.ContainsKey(authenticatedSession.locale_id) &&
				    menu.LanguageTranslation[authenticatedSession.locale_id].Length > 0
				) {
					variable = translations.GetTranslationVariable("",
						menu.LanguageTranslation[authenticatedSession.locale_id],
						"INSTANCEMENU");
					SetDBParam(SqlDbType.NVarChar, "@variable", variable);
					db.ExecuteUpdateQuery(
						"UPDATE instance_menu SET variable = @variable WHERE instance = @instance AND instance_menu_id = @instanceMenuId",
						DBParameters);
				} else {
					foreach (var l in menu.LanguageTranslation) {
						if (l.Value.Length > 0) {
							variable = translations.GetTranslationVariable("", l.Value, "INSTANCEMENU");
							break;
						}
					}

					if (variable != null) {
						SetDBParam(SqlDbType.NVarChar, "@variable", variable);
						db.ExecuteUpdateQuery(
							"UPDATE instance_menu SET variable = @variable WHERE instance = @instance AND instance_menu_id = @instanceMenuId",
							DBParameters);
					}
				}
			}

			if (variable != null && menu.LanguageTranslation != null) {
				SaveTranslationObject(variable, menu.LanguageTranslation);
			}

			if (menu.UserGroups != null) {
				db.ExecuteDeleteQuery("DELETE FROM instance_menu_rights WHERE instance_menu_id = @instanceMenuId",
					DBParameters);
				foreach (var ugId in menu.UserGroups) {
					SetDBParam(SqlDbType.Int, "@ugId", ugId);
					db.ExecuteInsertQuery(
						"INSERT INTO instance_menu_rights(instance_menu_id, item_id) " +
						"VALUES(@instanceMenuId, @ugId)", DBParameters);
				}
			}

			if (menu.UserGroupStates != null) {
				db.ExecuteDeleteQuery("DELETE FROM instance_menu_states WHERE instance_menu_id = @instanceMenuId",
					DBParameters);
				foreach (var ugId in menu.UserGroupStates.Keys) {
					SetDBParam(SqlDbType.Int, "@ugId", ugId);
					foreach (var state in menu.UserGroupStates[ugId]) {
						SetDBParam(SqlDbType.NVarChar, "@state", state);
						db.ExecuteInsertQuery(
							"INSERT INTO instance_menu_states(instance_menu_id, item_id, state) " +
							"VALUES(@instanceMenuId, @ugId, @state)", DBParameters);
					}
				}
			}

			menu.Variable = variable;
			InvalidateInstanceMenuCache();
			return menu;
		}

		public void DeleteInstaceMenu(int instanceMenuId) {
			SetDBParam(SqlDbType.Int, "@instanceMenuId", instanceMenuId);
			db.ExecuteDeleteQuery(
				"DELETE FROM instance_menu " +
				"WHERE instance = @instance AND instance_menu_id = @instanceMenuId", DBParameters);
			InvalidateInstanceMenuCache();
		}

		private void SaveTranslationObject(string variable, Dictionary<string, string> languages) {
			foreach (var l in languages) {
				translations.insertOrUpdateProcessTranslation(null, variable, l.Value, l.Key);
			}
		}

		private void InvalidateInstanceMenuCache() {
			Cache.Set(instance, "InstanceMenuCache", null);
			Cache.Set(instance, "InstanceMenuRightCache", null);
			Cache.Set(instance, "InstanceMenuStateCache", null);
			Cache.Set(instance, "pagebase_menu_cache", null);
		}
	}

	public class InstanceMenuRight {
		public int ItemId { get; set; }
		public List<int> MenuIds { get; set; }
	}

	public class InstanceMenuRights : ConnectionBase {
		public InstanceMenuRights(string instance, int currentUSerId) : base(instance, new AuthenticatedSession(instance, currentUSerId, null)) { }

		private DataTable _InstanceMenuRightCache;

		private DataTable InstanceMenuRightCache {
			get {
				if (_InstanceMenuRightCache == null)
					_InstanceMenuRightCache = (DataTable) Cache.Get(instance, "InstanceMenuRightCache");

				if (_InstanceMenuRightCache == null) {
					_InstanceMenuRightCache = db.GetDatatable(
						"SELECT * FROM instance_menu_rights WHERE instance_menu_id IN (SELECT instance_menu_id FROM instance_menu WHERE instance = @instance)",
						DBParameters);
					Cache.Set(instance, "InstanceMenuRightCache", _InstanceMenuRightCache);
				}

				return _InstanceMenuRightCache;
			}
		}

		private DataTable _InstanceMenuStateCache;

		private DataTable InstanceMenuStateCache {
			get {
				if (_InstanceMenuStateCache == null)
					_InstanceMenuStateCache = (DataTable) Cache.Get(instance, "InstanceMenuStateCache");

				if (_InstanceMenuStateCache == null) {
					_InstanceMenuStateCache = db.GetDatatable(
						"SELECT * FROM instance_menu_states WHERE instance_menu_id IN (SELECT instance_menu_id FROM instance_menu WHERE instance = @instance)",
						DBParameters);
					Cache.Set(instance, "InstanceMenuStateCache", _InstanceMenuStateCache);
				}

				return _InstanceMenuStateCache;
			}
		}

		public List<int> GetMenuIdsForUserGroups(List<int> itemIds) {
			var ids = new List<int>();
			foreach (var id in itemIds) {
				foreach (var r in InstanceMenuRightCache.Select("item_id = " + id)) {
					if (!ids.Contains(Conv.ToInt(r["instance_menu_id"]))) ids.Add(Conv.ToInt(r["instance_menu_id"]));
				}
			}

			return ids;
		}

		public List<int> GetUserGroupsForMenuId(int instanceMenuId) {
			var ids = new List<int>();
			foreach (var r in InstanceMenuRightCache.Select("instance_menu_id = " + instanceMenuId)) {
				ids.Add(Conv.ToInt(r["item_id"]));
			}

			return ids;
		}

		public Dictionary<int, List<string>> GetUserGroupStatesForMenuId(int instanceMenuId) {
			var retval = new Dictionary<int, List<string>>();
			SetDBParam(SqlDbType.Int, "@instanceMenuId", instanceMenuId);

			var itemIds = new List<int>();
			foreach (DataRow r in InstanceMenuRightCache.Select("instance_menu_id = " + instanceMenuId)) {
				if (!itemIds.Contains(Conv.ToInt(r["item_id"]))) itemIds.Add(Conv.ToInt(r["item_id"]));
			}

			foreach (var item_id in itemIds) {
				var states = new List<string>();
				foreach (DataRow rr in InstanceMenuStateCache.Select("instance_menu_id = " + instanceMenuId +
				                                                     " AND item_id = " + item_id)) {
					var state = Conv.ToStr(rr["state"]);
					if (!states.Contains(state)) states.Add(state);
				}

				retval.Add(item_id, states);
			}

			return retval;
		}
	}
}