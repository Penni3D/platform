﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	public class InstanceTags : ProcessBase {
		//AuthenticatedSession authenticatedSession;

		public InstanceTags(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, "instance_tags",
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public Dictionary<string, object> PostProcessTag(string process, int itemId, int itemColumnId, List<string> tags) {
			foreach (var tag in tags) {
				SetDBParam(SqlDbType.NVarChar, "@tagName", tag);
				if (Conv.ToInt(db.ExecuteScalar("SELECT item_id FROM " + tableName + " WHERE tag_name = @tagName",
					DBParameters)) > 0) {
					
				};
			}

			return null;
		}
	}
}