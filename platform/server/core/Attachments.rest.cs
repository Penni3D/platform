﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.core {
	[Route("v1/meta/Attachments")]
	public class AttachmentsResult : RestBase {
		[HttpPost]
		public IActionResult Post() {
			var attachments = new Attachments(instance, currentSession);
			return new ObjectResult(attachments.UploadFiles(Request.Form));
		}

		[HttpGet("{itemId}/{itemColumnId}")]
		public List<Attachment> Get(int itemId, int itemColumnId) {
			var attachments = new Attachments(instance, currentSession);
			return attachments.GetAttachments(itemId, itemColumnId);
		}

		[HttpGet("{itemId}/{itemColumnId}/{archiveDate}")]
		public List<Attachment> Get(int itemId, int itemColumnId, DateTime archiveDate) {
			var attachments = new Attachments(instance, currentSession);
			return attachments.GetArchiveAttachments(itemId, itemColumnId, archiveDate);
		}


		[HttpDelete("{attachmentId}")]
		public IActionResult Delete(int attachmentId) {
			var attachments = new Attachments(instance, currentSession);
			attachments.DeleteAttachment(attachmentId);
			return new StatusCodeResult(200);
		}
		[HttpGet("{process}")]
		public List<Attachment> GetPortfolioTemplatesByProcess(string process) {
			var attachments = new Attachments(instance, currentSession);
			return attachments.GetPortfolioTemplatesByProcess(process);
		}
	}

	[Route("v1/meta/Links")]
	public class ProcessLinksRest : RestBase {
		[HttpPost("{itemId}/{itemColumnId}")]
		public IActionResult Post(int itemId, int itemColumnId, [FromBody] JObject data) {
			var links = new Attachments(instance, currentSession);
			return new ObjectResult(links.NewLink(itemId, itemColumnId, data));
		}

		[HttpGet("{itemId}/{itemColumnId}")]
		public List<Attachment> Get(int itemId, int itemColumnId) {
			var links = new Attachments(instance, currentSession);
			return links.GetLinks(itemId, itemColumnId);
		}

		[HttpGet("{itemId}/{itemColumnId}/{archiveDate}")]
		public List<Attachment> Get(int itemId, int itemColumnId, DateTime archiveDate) {
			var links = new Attachments(instance, currentSession);
			return links.GetArchiveLinks(itemId, itemColumnId, archiveDate);
		}

		[HttpDelete("{linkId}")]
		public IActionResult Delete(int linkId) {
			var attachments = new Attachments(instance, currentSession);
			attachments.DeleteLink(linkId);
			return new StatusCodeResult(200);
		}
	}

	[Route("v1/meta/[controller]")]
	public class ControlAttachments : RestBase {
		[HttpGet("{parentId}/{controlName}")]
		public List<Attachment> Get(string parentId, string controlName) {
			var attachments = new Attachments(instance, currentSession);
			return attachments.GetAttachments(parentId, controlName);
		}
	}


	[Route("v1/meta/[controller]")]
	public class ControlLinks : RestBase {
		[HttpGet("{parentId}/{controlName}")]
		public List<Attachment> Get(int parentId, string controlName) {
			var links = new Attachments(instance, currentSession);
			return links.GetLinks(parentId, controlName);
		}

		[HttpPost("{parentId}/{controlName}")]
		public IActionResult Post(int parentId, string controlName, [FromBody] JObject data) {
			var links = new Attachments(instance, currentSession);
			return new ObjectResult(links.NewLink(parentId, controlName, data));
		}
	}

	[Route("v1/meta/AttachmentsDownload")]
	public class AttachmentsDownloadRest : RestBase {
		[HttpGet("{attachmentId}/{type}")]
		public FileContentResult Get(int attachmentId, int type) {
			var attachments = new AttachmentsDownload(currentSession);
			var a = attachments.DownloadAttachment(attachmentId, type);
			return File(a.Filedata ?? new byte[0], a.ContentType, a.Filename);
		}
		[HttpGet("{attachmentId}/{type}/{archiveDate}")]
		public FileContentResult Get(int attachmentId, int type, DateTime archiveDate) {
			var attachments = new AttachmentsDownload(currentSession);
			var a = attachments.DownloadAttachment(attachmentId, type, archiveDate);
			return File(a.Filedata ?? new byte[0], a.ContentType, a.Filename);
		}
	}
}