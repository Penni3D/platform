﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;
using DataTable = System.Data.DataTable;

namespace Keto5.x.platform.server.core {

	public class Notification {
		public string html { get; set; }
		public int digest { get; set; }
	}

	public class Notifications {
		private readonly string _instance;
		private readonly ItemBase _itemBase;
		private readonly string _process = "notification";
		private readonly SystemList _systemList;
		private readonly Translations _translations;
		private readonly AuthenticatedSession authenticatedSession;
		private readonly ProcessBase ProcessBase;

		public Notifications(string instance, AuthenticatedSession authenticatedSession) {
			_instance = instance;
			ProcessBase = new ProcessBase(_instance,_process, authenticatedSession);
			_itemBase = new ItemBase(_instance,_process, authenticatedSession);
			_translations = new Translations(instance, authenticatedSession, authenticatedSession.locale_id);
			_systemList = new SystemList(_instance, authenticatedSession);
			this.authenticatedSession = authenticatedSession;
		}

		public Dictionary<string, object> GetNotifications(int offset, int limit, int portfolioId) {
			var retval = _itemBase.FindItems(offset, limit, portfolioId);
			var data = (DataTable)retval["rowdata"];
			data.Columns.Add("name_translations", typeof(object));
			data.Columns.Add("title_translations", typeof(object));
			data.Columns.Add("body_translations", typeof(object));

			foreach (DataRow row in data.Rows) {
				row["name_translations"] = _translations.GetTranslations(Conv.ToStr(row["name_variable"]));
				row["title_translations"] = _translations.GetTranslations(Conv.ToStr(row["title_variable"]));
				row["body_translations"] = _translations.GetTranslations(Conv.ToStr(row["body_variable"]));
			}

			return retval;
		}

		public Dictionary<string, object> GetBulletins(int portfolioId) {

			var p = new ItemBase(_instance, "notification", authenticatedSession);
			var q = new PortfolioOptions();
			var ItemColumns = new ItemColumns(ProcessBase, "notification");
			//var startItemColumnId = ItemColumns.GetItemColumnByName("Start").ItemColumnId;
			//var endItemColumnId = ItemColumns.GetItemColumnByName("Expiration").ItemColumnId;
			var statusId = ItemColumns.GetItemColumnByName("Status").ItemColumnId;
			var parsedDateToday = DateTime.Now.ToString("yyyy/MM/dd").Replace("/", "-");

			/*
			List<string> dateRange = new List<string>(new string[]
			{
				null,
				parsedDateToday
			});

			List<string> dateRange2 = new List<string>(new string[]
			{
				parsedDateToday,
				null
			});
			*/

			List<string> v = new List<string>(new string[]
			{
				"1"
			});

			/*var s = "((start >= '" + parsedDateToday + "' AND expiration <= '" + parsedDateToday + "') " +
			        "OR " +
			        "	(start >= '" + parsedDateToday + "' AND expiration IS NULL) " +
			        "OR " +
			        "	(start IS NULL AND expiration IS NULL) " +
			        "OR " +
			        "	(start IS NULL AND expiration <= '" + parsedDateToday + "'))";*/

			var inDateRange = "start > '" + parsedDateToday + "' OR expiration < '" + parsedDateToday + "'";

			/*var sql = "SELECT * " +
			          "FROM _" + _instance + "_notification " +
			"WHERE type = 2 AND ((start >= '" + parsedDateToday + "' AND expiration <= '"+ parsedDateToday +"') " +
			"OR " +
			"	(start >= '" + parsedDateToday + "' AND expiration IS NULL) " +
			"OR " +
			"	(start IS NULL AND expiration IS NULL) " +
			"OR " +
			"	(start IS NULL AND expiration <= '" + parsedDateToday + "'))";*/


			q.filters = new Dictionary<int, object>();
			q.filters.Add(statusId, v);

			var data = p.FindItems(portfolioId, q);

			var data2 = (DataTable)data["rowdata"];

			var notAllowedIds = new List<int>();

			foreach (DataRow row in data2.Rows) {
				if (Conv.ToInt(row["type"]) == 2) {

					if (Conv.ToIntArray(row["user_item_id"]).Length != 0 || Conv.ToIntArray(row["user_groups"]).Length != 0) {

						if (!EvaluateUserRight(authenticatedSession, row)) {
							notAllowedIds.Add(Conv.ToInt(row["item_id"]));
						}
					}
				}
			}

			var notAlloweds = "item_id IN(" + Conv.ToDelimitedString(notAllowedIds) + ") OR " + inDateRange;

			if (notAllowedIds.Count != 0) {
				data2.Select(notAlloweds).ToList().ForEach(r => r.Delete());
				data2.AcceptChanges();
			} else {
				data2.Select(inDateRange).ToList().ForEach(r => r.Delete());
				data2.AcceptChanges();
			}

			return data;
		}

		private bool EvaluateUserRight(AuthenticatedSession authenticatedSession, DataRow row) {

			if (Conv.ToIntArray(row["user_item_id"]).Contains(authenticatedSession.item_id))
				return true;

			//if (Conv.ToIntArray(row["user_item_id"]).Length != 0) {
				foreach (int usersUserGroup in GetUserGroupsForAuthenticatedUser()) {
					if (Conv.ToIntArray(row["user_groups"]).Contains(usersUserGroup))
						return true;
				}
			//}

			return false;
		}

		//Miksi en saa alustettua haluamaani user groups construktoria tänne
		public List<int> GetUserGroupsForAuthenticatedUser() {
			var connectionBase = new ConnectionBase(_instance, authenticatedSession);
			var retval = new List<int>();
			connectionBase.SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			foreach (DataRow row in connectionBase.db.GetDatatable(
				"SELECT i.item_id FROM items i INNER JOIN item_data_process_selections idps ON i.item_id = idps.item_id WHERE instance = @instance AND process = 'user_group' AND item_column_id = (SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'user_group' AND name = 'user_id') AND selected_item_id = @userId",
				connectionBase.DBParameters).Rows) {
				retval.Add((int) row["item_id"]);
			}

			var ics = new ItemColumns(_instance, "user_group", authenticatedSession);
			if (ics.HasColumnName("all_group")) {
				var ib = new ItemBase(_instance, "user_group", authenticatedSession);
				var search = new Dictionary<string, object>();
				search.Add("all_group", 1);
				foreach (var row in ib.GetItems(search)) {
					if (!retval.Contains(Conv.ToInt(row["item_id"]))) {
						retval.Add(Conv.ToInt(row["item_id"]));
					}
				}
			}

			return retval;
		}



		public Dictionary<string, object> SaveNotifications(Dictionary<string, object> row) {
			var itemId = Conv.ToInt(row["item_id"]);
			var oldRow = _itemBase.GetItem(itemId);

			var hasNameVariable = false;
			var t2Found = false;
			var t3Found = false;

			if (row.ContainsKey("name_translations")) {
				foreach (var translationObject in (Dictionary<string, object>)row["name_translations"]) {
					var translationVariable = translationObject.Key;
					var translation = (string) translationObject.Value;

					var translations = new Translations(authenticatedSession.instance, authenticatedSession, translationVariable);

					if (!hasNameVariable && oldRow["name_variable"] == DBNull.Value && translation != null) {
						translation.Replace("'", "''");
						var newVariable = translations.GetTranslationVariable(_process, translationVariable, "NOTIFICATION");
						translations.insertOrUpdateInstanceTranslation("notification", newVariable, translation);
						row["name_variable"] = newVariable;
						hasNameVariable = true;
					} else if (oldRow["name_variable"] != DBNull.Value) {
						translations.insertOrUpdateInstanceTranslation("notification", (string)oldRow["name_variable"], translation);
					}
				}
			}

			if (row.ContainsKey("title_translations")) {
				foreach (var translationObject in (Dictionary<string, object>)row["title_translations"]) {
					var translationVariable = translationObject.Key;
					var translation = (string) translationObject.Value;

					var translations = new Translations(authenticatedSession.instance, authenticatedSession, translationVariable);

					if (!hasNameVariable && oldRow["name_variable"] == DBNull.Value && translation != null) {
						translation.Replace("'", "''");
						var newVariable = translations.GetTranslationVariable(_process, translationVariable, "NOTIFICATION");
						translations.insertOrUpdateInstanceTranslation("notification", newVariable, translation);
						row["name_variable"] = newVariable;
						hasNameVariable = true;
					} else if (oldRow["name_variable"] != DBNull.Value) {
						translations.insertOrUpdateInstanceTranslation("notification", (string)oldRow["name_variable"], translation);
					}
				}
			}

			if (row.ContainsKey("title_translations") && row["title_translations"] != null) {
				foreach (var (key, value) in ((JArray)row["title_translations"]).ToObject<List<KeyValuePair<string, string>>>()) {
					var translations = new Translations(authenticatedSession.instance, authenticatedSession, key);
					if (!t2Found && oldRow["title_variable"] == DBNull.Value && value != null) {
						value.Replace("'", "''");
						oldRow["title_variable"] = translations.GetTranslationVariable(_process, value, "N_TITLE");
						translations.insertOrUpdateInstanceTranslation("notification", (string)oldRow["title_variable"], value);
						row["title_variable"] = oldRow["title_variable"];
						t2Found = true;
					} else if (oldRow["title_variable"] != DBNull.Value) {
						translations.insertOrUpdateInstanceTranslation("notification", (string)oldRow["title_variable"], value);
					}
				}
			}

			if (row.ContainsKey("body_translations") && row["body_translations"] != null) {
				foreach (var (key, value) in ((JArray)row["body_translations"]).ToObject<List<KeyValuePair<string, string>>>()) {
					var translations = new Translations(authenticatedSession.instance, authenticatedSession, key);
					if (!t3Found && oldRow["body_variable"] == DBNull.Value && value != null) {
						value.Replace("'", "''");
						oldRow["body_variable"] = translations.GetTranslationVariable(_process, value, "N_BODY");
						translations.insertOrUpdateInstanceTranslation("notification", (string)oldRow["body_variable"], value);
						row["body_variable"] = oldRow["body_variable"];
						t3Found = true;
					} else if (oldRow["body_variable"] != DBNull.Value) {
						translations.insertOrUpdateInstanceTranslation("notification", (string)oldRow["body_variable"], value);
					}
				}
			}

			_itemBase.SaveItem(row);

			return row;
		}

		public List<int> GetInUseNotificationIds(string instance) {

			var result = new List<int>();

			var db = new Connections(true);

			var sql = "SELECT DISTINCT kn.item_id FROM _" + instance +
			          "_notification kn INNER JOIN process_action_rows par ON kn.item_id = CAST(par.value as INT) WHERE ISNUMERIC(par.value) = 1 UNION SELECT item_id FROM notification_conditions WHERE item_id IS NOT NULL";

			var sqlBulletins = "SELECT item_id FROM _" + instance + "_notification WHERE type = 2 and status = 1";

			foreach (DataRow r in db.GetDatatable(sql).Rows) {
				result.Add(Conv.ToInt(r["item_id"]));
			}
			foreach (DataRow r in db.GetDatatable(sqlBulletins).Rows) {
				result.Add(Conv.ToInt(r["item_id"]));
			}

			return result;
		}

		public Dictionary<int, List<NotificationActionPair>> ActionNotificationConnections(string instance) {
			var result = new Dictionary<int, List<NotificationActionPair>>();
			var db = new Connections(true);

			var translations = new Translations(instance, authenticatedSession);

			var sql = "SELECT pa.process_action_id, kn.item_id, pa.variable, pa.process FROM process_actions pa " +
			          "INNER JOIN process_action_rows par ON pa.process_action_id = par.process_action_id " +
			          "INNER JOIN _" + instance + "_notification kn ON CAST(par.value AS int) = kn.item_id " +
			          "WHERE ISNUMERIC(par.value) = 1 ";

			foreach (DataRow r in db.GetDatatable(sql).Rows) {
				if (!result.ContainsKey(Conv.ToInt(r["item_id"]))) {
					result.Add(Conv.ToInt(r["item_id"]), new List<NotificationActionPair>());
				}

				var item = new NotificationActionPair();

				item.Translation = translations.GetTranslation(Conv.ToStr(r["variable"]));
				item.ProcessActionId = Conv.ToInt(r["process_action_id"]);
				item.Process = Conv.ToStr((r["process"]));
				result[Conv.ToInt(r["item_id"])].Add(item);

			}
			return result;
		}


		public class NotificationActionPair
		{
			public int ProcessActionId { get; set; }
			public string Translation { get; set; }
			public string Process { get; set; }
		}

		public class NotificationCondPair
		{
			public string Process { get; set; }
			public string Translation { get; set; }
		}

		public Dictionary<int, List<NotificationCondPair>> NotiCondNotificationConnections(string instance) {
			var result = new Dictionary<int, List<NotificationCondPair>>();
			var db = new Connections(true);

			var translations = new Translations(instance, authenticatedSession);

			var sql = "SELECT kn.item_id, nc.variable, nc.process FROM notification_conditions nc " +
			"INNER JOIN _" + instance + "_notification kn ON nc.item_id = kn.item_id  ";

			foreach (DataRow r in db.GetDatatable(sql).Rows) {
				if (!result.ContainsKey(Conv.ToInt(r["item_id"]))) {
					result.Add(Conv.ToInt(r["item_id"]), new List<NotificationCondPair>());
				}

				var item = new NotificationCondPair();
				item.Process = Conv.ToStr(r["process"]);
				item.Translation = translations.GetTranslation(Conv.ToStr(r["variable"]));
				result[Conv.ToInt(r["item_id"])].Add(item);

			}
			return result;
		}

	}
}
