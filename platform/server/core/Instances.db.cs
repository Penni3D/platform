﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	public class Instance {
		public Instance() {

		}

		public Instance(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			DefaultFirstDayOfWeek = (byte)row["default_first_day_of_week"];
			DefaultDateFormat = (string)row["default_date_format"];
			DefaultLocaleId = (string)row["default_locale_id"];
			DefaultTimezone = (string)row["default_timezone"];
			Variable = (string)row["variable"];
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);

		}

		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public string DefaultLocaleId { get; set; }
		public string DefaultDateFormat { get; set; }
		public byte DefaultFirstDayOfWeek { get; set; }
		public string DefaultTimezone { get; set; }
	}

	public class Instances : ConnectionBase {
		private readonly AuthenticatedSession session;

		public Instances(string instance, AuthenticatedSession session) : base(instance, session) {
			this.session = session;
		}

		public Instance GetInstance() {
			var d = db.GetDataRow("SELECT variable, default_first_day_of_week, default_date_format, default_locale_id, default_timezone FROM instances WHERE instance = @instance", DBParameters);
			if (d != null) {
				return new Instance(d, session);
			}

			throw new CustomArgumentException("Instance cannot be found with string '" + instance + "'");
		}
	}
}
