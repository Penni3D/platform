﻿using System;
using System.Collections.Generic;

namespace Keto5.x.platform.server.core.workTimeTracking{
	public interface IWorktimeTrackingRepository{
		Dictionary<string, object> GetMyTasksWithHours(DateTime from, DateTime to, int? userId);

		int InsertTaskToMyList(WorktimeTrackingTaskListItem worktimeTrackingTaskListItem);
		void RemoveItemFromMyList(int userItemId, int taskId);

		void InsertUpdateAndDeleteHours(
			List<WorktimeTrackingTaskHours> toBeInserted,
			List<WorktimeTrackingTaskHours> toBeUpdated,
			List<WorktimeTrackingTaskHours> toBeDeleted,
			int userItemId);

		void UpdateDescription(int taskId, string description);

		void UpdateStatusesOfHoursAsAdmin(
			List<WorktimeHourApprovalItem> items,
			List<double?> fromStatuses,
			double toStatus,
			bool updateEditedByField,
			bool resetEditedByFields);

		void UpdateStatusesOfHoursAsMySelf(
			List<WorktimeHourApprovalItem> items,
			List<double?> fromStatuses,
			double toStatus,
			bool updateEditedByField,
			bool resetEditedByFields);

		void UpdateLineManagerStatusesOfHours(
			List<WorktimeHourApprovalItem> items,
			List<double> fromStatuses,
			double toStatus,
			bool updateEditedByField);

		void UpdateProjectManagerStatusesOfHours(
			List<WorktimeHourApprovalItem> items,
			List<double> fromStatuses,
			double toStatus,
			bool updateEditedByField);

		int GetMyManager(int user_item_id);

		List<Dictionary<string, object>> GetMySubUsers();

		List<Dictionary<string, object>> GetApprovableHoursForUsers(List<int> ids);

		List<Dictionary<string, object>> GetApprovedTasksForUsers(List<int> ids, DateTime from, DateTime to);

		List<Dictionary<string, object>> GetApprovableHoursForProjectManager(int projectId, DateTime from, DateTime to);

		List<Dictionary<string, object>> GetApprovedTasksForProjectManager(int projectId, DateTime from, DateTime to);

		List<Dictionary<string, object>> GetItems(string process, List<int> ids);

		List<int> FindStatusesInTimeSpan(List<WorktimeHourApprovalItem> items);

		List<Dictionary<string, object>> GetMyOpenTasks(int userId);

		List<int> GetBasicTaskIdsThatRequireApproval();

		List<int> GetTaskIdsThatRequireApproval();
	}
}