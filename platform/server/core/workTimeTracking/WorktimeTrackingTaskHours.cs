using System;
using System.Collections.Generic;
using System.Globalization;
using Keto5.x.platform.server.common;

namespace Keto5.x.platform.server.core.workTimeTracking{
	public class WorktimeTrackingTaskHours{
		public long? ItemId { get; set; }
		public int TaskId { get; set; }
		public string Date{ get; set; }
		public double? Hours{ get; set; }
		public double? AdditionalHours{ get; set; }
		public string Process{ get; set; }
		public int? UserItemId { get; set; }
		public List<WorktimeTrackingTaskHourBreakdown> BreakdownHours { get; set; }

		public Dictionary<string, object> ToDictionary(){
			return new Dictionary<string, object> {
				{"item_id", ItemId},
				{"task_item_id", TaskId},
				{"date", DateTime.ParseExact(Date, "ddMMyyyy", CultureInfo.InvariantCulture)},
				{"hours", Hours},
				{"additional_hours", AdditionalHours},
				{"process", Process},
				{"user_item_id", UserItemId}
			};
		}
	}
	
	public class WorktimeTrackingTaskHourBreakdown{
		public WorktimeTrackingTaskHourBreakdown() {	
		}

		public WorktimeTrackingTaskHourBreakdown(Dictionary<string, object> dict) {
			Hours = Conv.ToDouble(dict["hours"]);
			AdditionalHours = Conv.ToDouble(dict["additional_hours"]);
			ListItemId = Conv.ToInt(dict["list_item_id"]);
			Type = Conv.ToInt(dict["type"]);
			Comment = Conv.ToStr(dict["comment"]);
			CommentListItemId = Conv.ToInt(dict["comment_list_item_id"]);
		}

		public long? Id { get; set; }
		public double? Hours{ get; set; }
		public double? AdditionalHours{ get; set; }
		public long ListItemId { get; set; }
		public int Type { get; set; }
		public string Comment { get; set; }
		public int? CommentListItemId { get; set; }
	}
}