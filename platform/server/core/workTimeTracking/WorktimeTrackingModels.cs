﻿using System.Collections.Generic;

namespace Keto5.x.platform.server.core.workTimeTracking {
	public class WorktimeTrackingTaskListItem {
		public int? UserItemId { get; set; }
		public int? TaskItemId { get; set; }
		public int? AddedBy { get; set; }

		public Dictionary<string, object> GetAsDictionary() {
			return new Dictionary<string, object> {
				{"user_item_id", UserItemId},
				{"task_item_id", TaskItemId},
				{"added_by", AddedBy}
			};
		}
	}

	public class WorktimeTrackingTaskCopy {
		public int UserItemId { get; set; }
		public int OffSet { get; set; }
		public string CopyFromStart { get; set; }
		public string CopyFromEnd { get; set; }


	}

	public class WorkTimeTrackingSheetStatus {
		public int UserItemId { get; set; }
		public string Name { get; set; }
		public string LineManager { get; set; }
		public int Week { get; set; }
		public int Year { get; set; }
		public float Hours { get; set; }
		public float AdditionalHours { get; set; }
		public double Status { get; set; }
	}

	public class WorkTimeTrackingWidgetItem {
		public string Title { get; set; }
		public string Process { get; set; }
		public double Hours { get; set; }
		public double AdditionalHours { get; set; }
	}
}