﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.calendar;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core.workTimeTracking {
	public class WorktimeTrackingRepository : IWorktimeTrackingRepository {
		// Constants
		private const string TimetrackerUserTasksTable = "WORKTIME_TRACKING_USERS_TASKS";
		private const string TimetrackerHoursBreakdownTable = "WORKTIME_TRACKING_HOURS_BREAKDOWN";

		private const string UserItemId = "user_item_id";

		// Read only properties
		private readonly AuthenticatedSession _authenticatedSession;
		private readonly IConnectionBase _connectionBase;
		private readonly string TimetrackerHoursTable = "WORKTIME_TRACKING_HOURS";


		public WorktimeTrackingRepository(AuthenticatedSession session, IConnectionBase connectionBase) {
			_authenticatedSession = session;
			_connectionBase = connectionBase;
			TimetrackerHoursTable = "_" + session.instance + "_WORKTIME_TRACKING_HOURS";
		}

		public Dictionary<string, object> GetMyTasksWithHours(DateTime from, DateTime to, int? userId = null) {
			var id = userId != null ? userId.ToString() : _authenticatedSession.item_id.ToString();

			var userIdCondition = new SqlCondition(UserItemId, SqlComp.EQUALS, id);

			var fromParameter = _connectionBase.SetDBParameter("@from", from);
			var toParameter = _connectionBase.SetDBParameter("@to", to);

			var tasksWithProcessAndOwner = TaskIdsWithProcess(userIdCondition, fromParameter, toParameter);

			var parentStuff = AddStartAndEndDatesToTasks(tasksWithProcessAndOwner);

			var taskIdsWhereIAmResponsible = GetTaskIdsWhereIAmResponsible(Conv.ToInt(id))
				.Fetch()
				.Select(x => {
					var itemId = Conv.ToInt(x["item_id"]);
					return itemId;
				});
			var taskIdsThatRequireApproval = GetTaskIdsThatRequireApproval();
			var tasks = _connectionBase.db.DataTableToDictionary(tasksWithProcessAndOwner);
			var taskItemIds = new List<int>();
			var taskIdsByProcesses = new Dictionary<string, List<int>>();


			tasks.ForEach(x => {
				var itemId = Conv.ToInt(x["task_item_id"]);
				taskItemIds.Add(itemId);
				if (taskIdsThatRequireApproval.Contains(itemId)) x["approval_required"] = 1;
				if (taskIdsWhereIAmResponsible.Contains(itemId)) x["myTask"] = 1;

				var process = Conv.ToStr(x["process"]);
				if (!taskIdsByProcesses.ContainsKey(process)) taskIdsByProcesses.Add(process, new List<int>());

				taskIdsByProcesses[process].Add(itemId);
			});

			var itemsThatAreUnLocked = new List<int>();
			if (taskItemIds.Count > 0) {
				var unLockedTasks = GetTaskIdsThatAreNotLocked(taskItemIds);
				var l = _connectionBase.db.GetDatatableDictionary(unLockedTasks);
				foreach (var t in l) {
					itemsThatAreUnLocked.Add(Conv.ToInt(t["item_id"]));
				}
			}

			if (itemsThatAreUnLocked.Count > 0) {
				tasks.ForEach(x => {
					var itemId = Conv.ToInt(x["task_item_id"]);
					x["locked"] = 1;
					if (itemsThatAreUnLocked.Contains(itemId)) x["locked"] = 0;
				});
			}

			// find parent phases for tasks
			var parentPhases = FindParentPhasesForTasks(taskItemIds);


			var calendarData = new Dictionary<int, UserCalendarData>();
			if (userId > 0) {
				var cd = new CalendarDatas(_authenticatedSession.instance, _authenticatedSession);
				var c = cd.GetUserCalendarDataArchivedPatterns(Conv.ToInt(userId), from.ToString("yyyy-MM-dd"),
					to.ToString("yyyy-MM-dd"), from);
				calendarData.Add(Conv.ToInt(userId), c);
			}

			// fetch hours
			var hoursForTasks = GetHoursForTasks(userIdCondition, fromParameter, toParameter);
			var breakdownTypesService = new EntityServiceProvider(_authenticatedSession.instance, _authenticatedSession)
				.GetEntityService("worktime_tracking_breakdown_types");

			var brd = breakdownTypesService.GetAllData("", "list_item");
			//Add an empty row that shouldn't be selected

			var endDate = from.AddDays(-1);
			var startDate = new DateTime(from.Year, 1, 1);

			return new Dictionary<string, object> {
				{"tasks", tasks},
				{"parentPhases", parentPhases},
				{"hours", hoursForTasks},
				{"basicTasks", GetBasicTasks(Conv.ToInt(id), startDate, endDate)},
				{"breakdownTypes", brd},
				{"parentStuff", parentStuff},
				{"calendarData", calendarData}
			};
		}

		public void Rebase(List<int> Users) {
			var sql =
				"UPDATE _" + _connectionBase.instance +
				"_worktime_tracking_hours SET line_manager_status = 2 WHERE (line_manager_status < 2 OR line_manager_status IS NULL) AND date < DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1) AND user_item_id IN (" +
				Conv.ToDelimitedString(Users) + ")";
			_connectionBase.db.ExecuteUpdateQuery(sql, _connectionBase.DBParameters);
		}

		private bool CheckCopyRestrictionForTimeRecord(DataTable restrictionSettings, DataRow hourRow, int offSet,
			bool allowOutSideOfTasks) {
			//Check Owning Process Start / End Restrictions
			var restrictions = restrictionSettings.Select("process = '" + Conv.ToStr(hourRow["owner_process"]) + "'");
			var startRestrictionColumn = "";
			var endRestrictionColumn = "";
			var ownerRestriction = false;
			var date = ((DateTime) (hourRow["date"])).AddDays(offSet);

			foreach (var r in restrictions) {
				if (Conv.ToStr(r["name"]) == "TaskStartDate") startRestrictionColumn = Conv.ToStr(r["value"]);
				if (Conv.ToStr(r["name"]) == "TaskEndDate") endRestrictionColumn = Conv.ToStr(r["value"]);
			}

			if (startRestrictionColumn == "" || endRestrictionColumn == "") ownerRestriction = true;
			if (!ownerRestriction) {
				var restrictionRow = "SELECT " + startRestrictionColumn + " AS rstart, " + endRestrictionColumn +
				                     " AS rend FROM _" + _connectionBase.instance + "_" +
				                     Conv.ToStr(hourRow["owner_process"]) + " WHERE item_id = " +
				                     "(SELECT owner_item_id FROM _" + _connectionBase.instance +
				                     "_task t WHERE t.item_id = " + Conv.ToInt(hourRow["task_item_id"]) + ")";

				var restriction = _connectionBase.db.GetDataRow(restrictionRow, _connectionBase.DBParameters);

				if (restriction["rstart"] != DBNull.Value && restriction["rend"] != DBNull.Value)
					ownerRestriction = date >= (DateTime) restriction["rstart"] &&
					                   date <= (DateTime) restriction["rend"];
				else
					ownerRestriction = true;
			}

			if (!allowOutSideOfTasks)
				allowOutSideOfTasks = Conv.ToStr(hourRow["process"]) != "task" ||
				                      date >= (DateTime) hourRow["task_start"] &&
				                      date <= (DateTime) hourRow["task_end"];

			return ownerRestriction && allowOutSideOfTasks;
		}

		public bool CopyTimesheetBetweenDateRanges(WorktimeTrackingTaskCopy copyInstructions) {
			_connectionBase.SetDBParam(SqlDbType.Int, "@user_item_id", copyInstructions.UserItemId);
			_connectionBase.SetDBParam(SqlDbType.Int, "@offset", copyInstructions.OffSet);
			_connectionBase.SetDBParam(SqlDbType.Date, "@from_start", copyInstructions.CopyFromStart);
			_connectionBase.SetDBParam(SqlDbType.Date, "@from_end", copyInstructions.CopyFromEnd);
			var oldIds = new Dictionary<int, int>();

			//Check that timesheet has not been submitted
			var isSubmitted = Conv.ToInt(_connectionBase.db.ExecuteScalar(
				"SELECT COUNT(*) FROM _" + _connectionBase.instance + "_worktime_tracking_hours " +
				"WHERE date BETWEEN DATEADD(day,@offset,@from_start) AND DATEADD(day,@offset,@from_end) " +
				" AND user_item_id = @user_item_id AND line_manager_status > 0",
				_connectionBase.DBParameters));
			if (isSubmitted > 0) return false;

			//Get selected timesheet
			var sql =
				"SELECT item_id, user_item_id, task_item_id, process, " +
				"CASE WHEN process = 'task' THEN (SELECT process FROM items WHERE item_id = " +
				"(SELECT owner_item_id FROM _" + _connectionBase.instance + "_task WHERE item_id = task_item_id)) " +
				"ELSE '' END AS owner_process," +
				"CASE WHEN process = 'task' THEN " +
				"(SELECT start_date FROM _" + _connectionBase.instance + "_task WHERE item_id = task_item_id) " +
				"ELSE null END AS task_start," +
				"CASE WHEN process = 'task' THEN " +
				"(SELECT end_date FROM _" + _connectionBase.instance + "_task WHERE item_id = task_item_id) " +
				"ELSE null END AS task_end," +
				"hours, additional_hours, [date], line_manager_status, project_manager_status " +
				"FROM _" + _connectionBase.instance + "_worktime_tracking_hours " +
				"WHERE date BETWEEN @from_start AND @from_end AND user_item_id = @user_item_id " +
				"ORDER BY date ASC";
			var timesheetData = _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters);
			var timesheetIds = (from DataRow r in timesheetData.Rows select Conv.ToInt(r["item_id"])).ToList();
			var processNames = (from DataRow r in timesheetData.Rows select "'" + Conv.ToStr(r["owner_process"]) + "'")
				.ToList().Distinct().ToList();

			//Get Owner Start and End Restriction Settings
			var csql = "SELECT iconf.process, iconf.name, iconf.value " +
			           "FROM instance_configurations iconf " +
			           "JOIN item_columns ic on ic.process=iconf.process and ic.name=iconf.value " +
			           "WHERE iconf.process IN (" +
			           (processNames.Count > 0 ? Modi.JoinListWithComma(processNames) : "''") +
			           ") AND iconf.instance = @instance AND (iconf.name = 'TaskStartDate' OR iconf.name = 'TaskEndDate') AND ic.process is not null";
			var restrictionSettings = _connectionBase.db.GetDatatable(csql, _connectionBase.DBParameters);

			//Get Task Start and End Restriction Settings
			var allowOutsideOfTasks = true;
			var confData =
				Config.GetClientConfiguration(_authenticatedSession, "View Configurator", "worktimeTracking");
			if (confData.ContainsKey("")) {
				var confJson = confData[""];
				var confJsonData = JsonConvert.DeserializeObject<Dictionary<string, object>>(confJson);
				if (confJsonData.ContainsKey("AllowOutsideOfTasks"))
					allowOutsideOfTasks = Conv.ToBool(confJsonData["AllowOutsideOfTasks"]);
			}

			//Get Breakdown Data
			var breakdownData =
				_connectionBase.db.GetDatatable(
					"SELECT * FROM worktime_tracking_hours_breakdown WHERE worktime_tracking_hour_id IN (" +
					(timesheetIds.Count > 0 ? Modi.JoinListWithComma(timesheetIds) : "0") + ")");

			//Remove old entries from timesheet
			_connectionBase.db.ExecuteDeleteQuery("DELETE FROM items WHERE item_id IN (SELECT item_id " +
			                                      "FROM _" + _connectionBase.instance + "_worktime_tracking_hours " +
			                                      "WHERE date BETWEEN DATEADD(day,@offset,@from_start) AND DATEADD(day,@offset,@from_end) AND user_item_id = @user_item_id)",
				_connectionBase.DBParameters);

			//Loop existing timesheet
			var itemService = GetHourItemService();
			foreach (DataRow hourRow in timesheetData.Rows) {
				if (CheckCopyRestrictionForTimeRecord(restrictionSettings, hourRow,
					    copyInstructions.OffSet,
					    allowOutsideOfTasks) ==
				    false) continue;

				//If hours = 0, skip
				if (!(Conv.ToDouble(hourRow["hours"]) > 0 || Conv.ToDouble(hourRow["additional_hours"]) > 0)) continue;

				var date = (DateTime) (hourRow["date"]);
				var uniqueData = new Dictionary<string, object> {
					{"task_item_id", Conv.ToInt(hourRow["task_item_id"])},
					{"date", date.AddDays(copyInstructions.OffSet)},
					{"process", Conv.ToStr(hourRow["process"])},
					{"user_item_id", Conv.ToInt(hourRow["user_item_id"])}
				};
				var data = new Dictionary<string, object> {
					{"hours", Conv.ToDouble(hourRow["hours"])},
					{"additional_hours", Conv.ToDouble(hourRow["additional_hours"])},
					{"line_manager_status", 0},
					{"project_manager_status", 0},
					{"task_item_id", Conv.ToInt(hourRow["task_item_id"])},
					{"date", date.AddDays(copyInstructions.OffSet)},
					{"process", Conv.ToStr(hourRow["process"])},
					{"user_item_id", Conv.ToInt(hourRow["user_item_id"])}
				};
				var itemId = itemService.AddItemOrUpdate(data, uniqueData, 0, false);
				oldIds.Add(Conv.ToInt(hourRow["item_id"]), itemId);

				foreach (var breakdown in breakdownData.Select(
					"worktime_tracking_hour_id = " + Conv.ToInt(hourRow["item_id"]))) {
					_connectionBase.SetDBParam(SqlDbType.Int, "@worktime_tracking_hour_id",
						oldIds[Conv.ToInt(breakdown["worktime_tracking_hour_id"])]);
					_connectionBase.SetDBParam(SqlDbType.Decimal, "@hours", Conv.ToDouble(breakdown["hours"]));
					_connectionBase.SetDBParam(SqlDbType.Decimal, "@additional_hours",
						Conv.ToDouble(breakdown["additional_hours"]));
					_connectionBase.SetDBParam(SqlDbType.Int, "@type", Conv.ToInt(breakdown["type"]));
					_connectionBase.SetDBParam(SqlDbType.Int, "@list_item_id",
						Conv.ToInt(breakdown["list_item_id"]));
					_connectionBase.SetDBParam(SqlDbType.Int, "@comment_list_item_id",
						Conv.ToInt(breakdown["comment_list_item_id"]));
					_connectionBase.db
						.ExecuteInsertQuery("INSERT INTO worktime_tracking_hours_breakdown " +
						                    "(worktime_tracking_hour_id, hours, additional_hours, type, list_item_id, comment, comment_list_item_id) " +
						                    "VALUES (@worktime_tracking_hour_id, @hours, @additional_hours, @type, @list_item_id, '', @comment_list_item_id)",
							_connectionBase.DBParameters);
				}
			}

			return true;
		}

		private DataTable GetBasicTasks(int userItemId, DateTime startDate, DateTime endDate) {
			_connectionBase.SetDBParam(SqlDbType.Int, "@userItemId", userItemId);
			_connectionBase.SetDBParam(SqlDbType.Date, "@startDate", startDate);
			_connectionBase.SetDBParam(SqlDbType.Date, "@endDate", endDate);

			var sql = " SELECT  " +
			          "       item_id ,  " +
			          "       approval_required,  " +
			          "       list_item,  " +
			          "       submission_warning,  " +
			          "       ( " +
			          "               SELECT ISNULL(SUM(h.hours),0)  " +
			          "               FROM _" + _connectionBase.instance + "_worktime_tracking_hours h  " +
			          "               WHERE h.user_item_id = @userItemId AND h.task_item_id = pf.item_id AND h.date BETWEEN @startDate AND @endDate " +
			          "       ) AS TotalHours " +
			          " FROM _" + _connectionBase.instance + "_worktime_tracking_basic_tasks pf  " +
			          " ORDER BY list_item ASC ";
			return _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters);
		}


		public int InsertTaskToMyList(WorktimeTrackingTaskListItem worktimeTrackingTaskListItem) {
			return _connectionBase
				.BuildInsertQuery()
				.To(TimetrackerUserTasksTable)
				.Values(worktimeTrackingTaskListItem.GetAsDictionary())
				.Execute();
		}

		public void RemoveItemFromMyList(int userItemId, int taskId) {
			var userIdParam = _connectionBase.SetDBParameter("@userId", userItemId);
			var taskIdParam = _connectionBase.SetDBParameter("@idParam", taskId);
			var deleteQuery = _connectionBase
				.BuildDeleteQuery()
				.From(TimetrackerUserTasksTable)
				.Where(new SqlCondition("user_item_id", SqlComp.EQUALS, userIdParam)
					.And(new SqlCondition("task_item_id", SqlComp.EQUALS, taskIdParam)));
			_connectionBase.db.ExecuteDeleteQuery(deleteQuery, _connectionBase.DBParameters);
		}

		public void InsertUpdateAndDeleteHours(
			List<WorktimeTrackingTaskHours> toBeInserted,
			List<WorktimeTrackingTaskHours> toBeUpdated,
			List<WorktimeTrackingTaskHours> toBeDeleted,
			int userItemId
		) {
			//Clean
			_connectionBase.SetDBParameter("@userId", userItemId);
			_connectionBase.db.ExecuteDeleteQuery(
				"DELETE FROM items WHERE item_id IN (SELECT item_id FROM _" + _connectionBase.instance +
				"_worktime_tracking_hours WHERE user_item_id = @userId AND date IS NULL)",
				_connectionBase.DBParameters);

			//Insert, update and delete
			RunInsertQueries(toBeInserted);
			RunUpdateQueries(toBeUpdated);
			RunDeleteQueries(toBeDeleted);
		}

		public void UpdateDescription(int taskId, string description) {
			_connectionBase.SetDBParameter("@userId", _authenticatedSession.item_id);
			_connectionBase.SetDBParameter("@taskId", taskId);
			_connectionBase.SetDBParameter("@description", description);
			var updateQuery = "UPDATE " + TimetrackerUserTasksTable + " " +
			                  "SET description = @description " +
			                  "WHERE user_item_id = @userId " +
			                  "AND task_item_id = @taskId";
			_connectionBase.db.ExecuteUpdateQuery(updateQuery, _connectionBase.DBParameters);
		}

		public void UpdateStatusesOfHoursAsAdmin(
			List<WorktimeHourApprovalItem> items,
			List<double?> fromStatuses,
			double toStatus,
			bool updateEditedByField,
			bool resetEditedFields) {
			var queriesForLineManagerField = items
				.Select((worktimeHourApprovalItem, i) => BuildHourStatusUpdateQuery(worktimeHourApprovalItem, i,
					updateEditedByField, fromStatuses, toStatus, true, true, true))
				.ToList();

			var queries = new List<string>();
			queries.AddRange(queriesForLineManagerField);
			_connectionBase.db.ExecuteUpdateQuery(string.Join(";", queries), _connectionBase.DBParameters);
		}

		public void UpdateStatusesOfHoursAsMySelf(
			List<WorktimeHourApprovalItem> items,
			List<double?> fromStatuses,
			double toStatus,
			bool updateEditedByField,
			bool resetEditedFields) {
			var queriesForLineManagerField = items
				.Select((worktimeHourApprovalItem, i) => BuildHourStatusUpdateQuery(worktimeHourApprovalItem, i,
					updateEditedByField, fromStatuses, toStatus, true, true))
				.ToList();

			var queries = new List<string>();
			queries.AddRange(queriesForLineManagerField);
			_connectionBase.db.ExecuteUpdateQuery(string.Join(";", queries), _connectionBase.DBParameters);
		}

		public void UpdateLineManagerStatusesOfHours(
			List<WorktimeHourApprovalItem> items,
			List<double> fromStatuses,
			double toStatus,
			bool updateEditedByField) {
			var nullableListOfFromStatuses = fromStatuses.Select(x => (double?) x).ToList();

			var queries = items
				.Select((worktimeHourApprovalItem, i) => BuildHourStatusUpdateQuery(worktimeHourApprovalItem, i,
					updateEditedByField, nullableListOfFromStatuses, toStatus, true, false))
				.ToList();

			_connectionBase.db.ExecuteUpdateQuery(string.Join(";", queries), _connectionBase.DBParameters);
		}

		public void UpdateProjectManagerStatusesOfHours(
			List<WorktimeHourApprovalItem> items,
			List<double> fromStatuses,
			double toStatus,
			bool updateEditedByField) {
			var nullableListOfFromStatuses = fromStatuses.Select(x => (double?) x).ToList();
			var queries = items
				.Select((worktimeHourApprovalItem, i) => BuildHourStatusUpdateQuery(worktimeHourApprovalItem, i,
					updateEditedByField, nullableListOfFromStatuses, toStatus, false, true))
				.ToList();

			_connectionBase.db.ExecuteUpdateQuery(string.Join(";", queries), _connectionBase.DBParameters);
		}

		public int GetMyManager(int user_item_id) {
			_connectionBase.SetDBParam(SqlDbType.Int, "@userid", user_item_id);
			return Conv.ToInt(_connectionBase.db.ExecuteScalar(
				"SELECT TOP 1 item_id FROM item_data_process_selections WHERE item_column_id = (SELECT item_column_id FROM item_columns WHERE process='user' AND name='hour_approval_users') AND selected_item_id = @userid",
				_connectionBase.DBParameters));
		}

		public List<Dictionary<string, object>> GetMySubUsers() {
			var instanceParam = _connectionBase.SetDBParameter("@instance", _connectionBase.instance);
			var processParam = _connectionBase.SetDBParameter("@process", "user");
			var fieldParam = _connectionBase.SetDBParameter("@fieldParam", "hour_approval_users");
			var userParam = _connectionBase.SetDBParameter("@userId", _authenticatedSession.item_id);
			var instanceCondition = new SqlCondition("instance", SqlComp.EQUALS, instanceParam);
			var processCondition = new SqlCondition("process", SqlComp.EQUALS, processParam);
			var fieldCondition = new SqlCondition("name", SqlComp.EQUALS, fieldParam);
			var userCondition = new SqlCondition("item_id", SqlComp.EQUALS, userParam);

			var query = _connectionBase
				.BuildSelectQuery()
				.Columns("idps.selected_item_id")
				.From("ITEM_DATA_PROCESS_SELECTIONS", "idps")
				.Join(new Join("item_columns", "ic").On("idps.item_column_id", "ic.item_column_id"))
				.Where(instanceCondition
					.And(processCondition)
					.And(fieldCondition)
					.And(userCondition));

			return query.Fetch();
		}

		public List<int> GetTaskIdsThatRequireApproval() {
			var checkIfConfigColumnsCanBeFound =
				" SELECT conf.process, conf.name, conf.value, ic.instance, ic.name, ic.item_column_id, is_list = case when charindex('.', conf.value) >0 THEN 1 ELSE 0 END , data_additional " +
				" FROM INSTANCE_CONFIGURATIONS conf  " +
				" LEFT JOIN item_columns ic ON ic.process = conf.process AND ic.name =  " +
				" CASE WHEN charindex('.', conf.value) > 0 THEN substring(conf.value,0, charindex('.', conf.value)) ELSE conf.value END " +
				" WHERE conf.category = 'Page' AND conf.process IS NOT NULL AND conf.name = 'ApprovalRequiredFieldInProcesses' ";

			var foundColumns = _connectionBase.db.GetDatatableDictionary(checkIfConfigColumnsCanBeFound);

			var queryBlocks = new List<string>();
			foundColumns.ForEach(x => {
				if (x["name"] != null && (string) x["name"] != "") {
					var taskTable = "_" + Conv.ToStr(x["instance"]) + "_task";
					var tableName = "_" + Conv.ToStr(x["instance"]) + "_" + Conv.ToStr(x["process"]);
					var confValue = Conv.ToStr(x["value"]);
					var colName = Conv.ToStr(x["name"]);
					var colId = Conv.ToInt(x["item_column_id"]);
					var listTableProcess = "_" + Conv.ToStr(x["instance"]) + "_" + Conv.ToStr(x["data_additional"]);

					var q = "";

					if (Conv.ToInt(x["is_list"]) == 1) {
						var colNames = confValue.Split(".");

						q = "SELECT t.item_id " +
						    "FROM " + taskTable + " t " +
						    "JOIN item_data_process_selections idps ON t.owner_item_id = idps.item_id " +
						    "INNER JOIN " + listTableProcess + " list1 ON idps.selected_item_id = list1.item_id " +
						    "WHERE list1." + colNames[1] + " = 1";
					} else {
						q = "SELECT t.item_id " +
						    "FROM " + taskTable + " t " +
						    "JOIN " + tableName + " taskproc ON t.owner_item_id = taskproc.item_id " +
						    "WHERE taskproc." + colName + " = 1";
					}

					queryBlocks.Add(q);
				}
			});
			if (queryBlocks.Count == 0) {
				return new List<int>();
			}

			var query = "SELECT DISTINCT * FROM ( " + string.Join(" UNION ", queryBlocks) + " ) q";
			var taskIdsDict = _connectionBase.db.GetDatatableDictionary(query);
			return taskIdsDict.Select(x => Conv.ToInt(x["item_id"])).ToList();
		}

		public List<int> GetBasicTaskIdsThatRequireApproval() {
			var tableName = _connectionBase.GetProcessTableName("worktime_tracking_basic_tasks");
			var query = "SELECT item_id FROM " + tableName + " WHERE approval_required=1";
			var ids = _connectionBase.db.GetDatatableDictionary(query);
			if (ids.Count == 0) {
				return new List<int>();
			}

			return ids.Select(x => Conv.ToInt(x["item_id"])).ToList();
		}

		public List<Dictionary<string, object>> GetApprovableHoursForUsers(List<int> ids) {
			if (ids.Count == 0) {
				return new List<Dictionary<string, object>>();
			}

			var statusReadyParameter =
				_connectionBase.SetDBParameter("@statusReady", (int) WorktimeTrackingService.HourStatus.Submitted);

			var conditions = new SqlCondition("user_item_id").In(string.Join(",", ids))
				.And(new SqlCondition("line_manager_status", SqlComp.EQUALS, statusReadyParameter));

			var taskIdCond = new SqlCondition("1", SqlComp.EQUALS, "0");
			var taskIds = GetTaskIdsThatRequireApproval();
			if (taskIds != null) {
				taskIdCond = new SqlCondition("task_item_id").In(taskIds)
					.And(new SqlCondition("process", SqlComp.NOT_EQUALS, "'worktime_tracking_basic_tasks'"));
			}

			var basicTaskIds = GetBasicTaskIdsThatRequireApproval();
			if (basicTaskIds != null) {
				var cond = taskIdCond.Or(new SqlCondition("task_item_id").In(basicTaskIds)
					.And(new SqlCondition("process", SqlComp.EQUALS, "'worktime_tracking_basic_tasks'")));
				taskIdCond = taskIdCond == null ? taskIdCond.Or(cond) : cond;
			}

			if (taskIdCond != null) {
				conditions = conditions.And(taskIdCond);
			}

			var query = _connectionBase
				.BuildSelectQuery()
				.From(TimetrackerHoursTable)
				.Where(conditions);
			return query.Fetch();
		}

		public List<Dictionary<string, object>> GetApprovedTasksForUsers(List<int> ids, DateTime from, DateTime to) {
			if (ids.Count == 0) {
				return new List<Dictionary<string, object>>();
			}

			var statuses = new List<int> {(int) WorktimeTrackingService.HourStatus.Approved};

			var fromParameter = _connectionBase.SetDBParameter("@from", from);
			var toParameter = _connectionBase.SetDBParameter("@to", to);

			var conditions = new SqlCondition("user_item_id").In(string.Join(",", ids))
				.And(new SqlCondition("line_manager_status").In(statuses))
				.And(new SqlCondition("date").Between(fromParameter, toParameter));

			var taskIdCond = new SqlCondition("1", SqlComp.EQUALS, "0");
			var taskIds = GetTaskIdsThatRequireApproval();
			if (taskIds != null) {
				taskIdCond = new SqlCondition("task_item_id").In(taskIds)
					.And(new SqlCondition("process", SqlComp.NOT_EQUALS, "'worktime_tracking_basic_tasks'"));
			}

			var basicTaskIds = GetBasicTaskIdsThatRequireApproval();
			if (basicTaskIds != null) {
				var cond = taskIdCond.Or(new SqlCondition("task_item_id").In(basicTaskIds)
					.And(new SqlCondition("process", SqlComp.EQUALS, "'worktime_tracking_basic_tasks'")));
				taskIdCond = taskIdCond == null ? taskIdCond.Or(cond) : cond;
			}

			if (taskIdCond != null) {
				conditions = conditions.And(taskIdCond);
			}

			var query = _connectionBase
				.BuildSelectQuery()
				.From(TimetrackerHoursTable)
				.Where(conditions);
			return query.Fetch();
		}

		public List<Dictionary<string, object>>
			GetApprovableHoursForProjectManager(int projectId, DateTime from, DateTime to) {
			var fromParameter = _connectionBase.SetDBParameter("@from", from);
			var toParameter = _connectionBase.SetDBParameter("@to", to);
			var taskTableName = _connectionBase.GetProcessTableName("task");
			var projectIdParam = _connectionBase.SetDBParameter("@projectId", projectId);
			var conditions = new SqlCondition("t.owner_item_id", SqlComp.EQUALS, projectIdParam)
				.And(new SqlCondition("wth.project_manager_status").In(new List<int> {
					(int) WorktimeTrackingService.HourStatus.Submitted
				}));
			var query = _connectionBase.BuildSelectQuery()
				.From(TimetrackerHoursTable, "wth")
				.Join(new Join(taskTableName, "t").On("t.item_id", "wth.task_item_id"))
				.Where(
					conditions
						.And(new SqlCondition("date").Between(fromParameter, toParameter)));
			return query.Fetch();
		}

		public List<Dictionary<string, object>> GetApprovedTasksForProjectManager(int projectId, DateTime from,
			DateTime to) {
			var fromParameter = _connectionBase.SetDBParameter("@from", from);
			var toParameter = _connectionBase.SetDBParameter("@to", to);
			var taskTableName = _connectionBase.GetProcessTableName("task");
			var projectIdParam = _connectionBase.SetDBParameter("@projectId", projectId);
			var conditions = new SqlCondition("t.owner_item_id", SqlComp.EQUALS, projectIdParam)
				.And(
					new SqlCondition("wth.project_manager_status")
						.In(new List<int> {(int) WorktimeTrackingService.HourStatus.Approved}))
				.And(new SqlCondition("date").Between(fromParameter, toParameter));
			var query = _connectionBase.BuildSelectQuery()
				.From(TimetrackerHoursTable, "wth")
				.Join(new Join(taskTableName, "t").On("t.item_id", "wth.task_item_id"))
				.Where(conditions);
			return query.Fetch();
		}

		public List<Dictionary<string, object>> GetItems(string process, List<int> ids) {
			if (ids.Count == 0) {
				return new List<Dictionary<string, object>>();
			}

			var itemBase = new ItemBase(_authenticatedSession.instance, process, _authenticatedSession);
			return _connectionBase.db.DataTableToDictionary(itemBase.GetAllData(string.Join(",", ids)));
		}

		public List<int> FindStatusesInTimeSpan(List<WorktimeHourApprovalItem> items) {
			var userIds = new List<int>();
			var taskIds = new List<int>();
			DateTime? from = null;
			DateTime? to = null;
			foreach (var worktimeHourApprovalItem in items) {
				if (from == null || worktimeHourApprovalItem.From < from) {
					from = worktimeHourApprovalItem.From;
				}

				if (to == null || worktimeHourApprovalItem.To < to) {
					to = worktimeHourApprovalItem.To;
				}

				if (!userIds.Contains(worktimeHourApprovalItem.UserId)) {
					userIds.Add(worktimeHourApprovalItem.UserId);
				}

				if (!taskIds.Contains(worktimeHourApprovalItem.TaskId) && worktimeHourApprovalItem.TaskId > 0) {
					taskIds.Add(worktimeHourApprovalItem.TaskId);
				}
			}

			var userIdCondition = new SqlCondition("user_item_id").In(userIds);
			var fromParam = _connectionBase.SetDBParameter("@from", from);
			var toParam = _connectionBase.SetDBParameter("@to", to);
			var dateCondition = new SqlCondition("date").Between(fromParam, toParam);

			var conditions = userIdCondition.And(dateCondition);
			if (taskIds.Count > 0) {
				var taskIdCondition = new SqlCondition("task_item_id").In(taskIds);
				conditions.And(taskIdCondition);
			}

			var lineManagerQuery = _connectionBase.BuildSelectQuery()
				.Columns("line_manager_status status")
				.From(TimetrackerHoursTable)
				.Where(conditions);
			var projectManagerQuery = _connectionBase.BuildSelectQuery()
				.Columns("project_manager_status status")
				.From(TimetrackerHoursTable)
				.Where(conditions);
			var union = lineManagerQuery.Union(projectManagerQuery);

			var retVal = _connectionBase.db.GetDatatableDictionary(union, _connectionBase.DBParameters)
				.Select(dictionary => Conv.ToInt(dictionary["status"])).ToList();

			return retVal;
		}

		public List<Dictionary<string, object>> GetMyOpenTasks(int userId) {
			var query = GetTaskIdsWhereIAmResponsible(userId);

			//Get limits
			var limitSql = "SELECT process, MAX(col) AS name, MAX(val) AS value FROM (" +
			               "SELECT process, value AS col, '' AS val FROM instance_configurations WHERE " +
			               "name = 'AddOnlyTasksFromProcessWithStatusColumn' AND process IS NOT NULL " +
			               "UNION ALL " +
			               "SELECT process, '' AS col, value AS val FROM instance_configurations WHERE " +
			               "name = 'AddOnlyTasksFromProcessWithStatusValueIn' AND process IS NOT NULL" +
			               ") iq GROUP BY iq.process";

			//Apply limits
			foreach (DataRow row in _connectionBase.db.GetDatatable(limitSql).Rows) {
				var process = Conv.ToStr(row["process"]);
				var name = Conv.ToStr(row["name"]);
				var value = Conv.ToStr(row["value"]);

				//If invalid quit
				if (name == "" || value == "" || process == "") continue;

				//Add join and where
				var t = "_" + _connectionBase.instance + "_" + process;
				query.Join(new Join(t).On(t + ".item_id", "pf.owner_item_id").Type("LEFT JOIN"));
				query.Join(new Join("item_data_process_selections pidps").On("pidps.item_id",
						t +
						".item_id AND pidps.item_column_id = (SELECT item_column_id FROM item_columns WHERE name = '" +
						name + "' AND process = '" + process + "')")
					.Type("LEFT JOIN"));

				query.Where("((SELECT data_type FROM item_columns WHERE name = '" + name + "' AND process = '" +
				            process + "') <> 6 AND ((" + t + "." + name + " IS NULL OR " + t + "." + name + " IN (" +
				            value + "))) OR ((SELECT data_type FROM item_columns WHERE name = '" + name +
				            "' AND process = '" + process + "') = 6 AND pidps.selected_item_id IN (" + value +
				            "))) AND " +
				            query.GetWhereClause() + " AND pf.end_date > getutcdate() ");
			}

			return query.Fetch();
		}

		private Dictionary<string, Dictionary<int, Dictionary<string, object>>> AddStartAndEndDatesToTasks(
			DataTable dataTable) {
			var returnObject = new Dictionary<string, Dictionary<int, Dictionary<string, object>>>();
			var processNames = new List<string>();
			var parentIdsByProcess = new Dictionary<string, List<int>>();
			foreach (DataRow dataTableRow in dataTable.Rows) {
				var processName = Conv.ToStr(dataTableRow["owner_type"]);
				var parentItemId = Conv.ToInt(dataTableRow["owner_item_id"]);

				if (!parentIdsByProcess.ContainsKey(processName)) {
					parentIdsByProcess.Add(processName, new List<int>());
				}

				parentIdsByProcess[processName].Add(parentItemId);
				var processWithHipsut = "'" + processName + "'";
				if (!processNames.Contains(processWithHipsut)) {
					processNames.Add(processWithHipsut);
				}
			}

			if (processNames.Count == 0) {
				return returnObject;
			}

			_connectionBase.SetDBParameter("@instance", _authenticatedSession.instance);
			var sql = "SELECT iconf.process, iconf.name, iconf.value " +
			          "FROM instance_configurations iconf " +
			          "JOIN item_columns ic on ic.process=iconf.process and ic.name=iconf.value " +
			          "WHERE iconf.process IN (" + string.Join(",", processNames) +
			          ") AND iconf.instance = @instance AND (iconf.name = 'TaskStartDate' OR iconf.name = 'TaskEndDate') AND ic.process is not null";

			var configFields = _connectionBase.db.GetDatatableDictionary(sql, _connectionBase.DBParameters);

			var startEndDateFieldsByProcess = new Dictionary<string, Dictionary<string, string>>();
			configFields.ForEach(x => {
				var process = Conv.ToStr(x["process"]);
				if (!startEndDateFieldsByProcess.ContainsKey(process)) {
					startEndDateFieldsByProcess.Add(process, new Dictionary<string, string>());
				}

				startEndDateFieldsByProcess[process].Add(Conv.ToStr(x["name"]), Conv.ToStr(x["value"]));
			});
			foreach (var key in startEndDateFieldsByProcess.Keys) {
				var tablename = _connectionBase.GetProcessTableName(key);

				var startField = startEndDateFieldsByProcess[key].ContainsKey("TaskStartDate")
					? startEndDateFieldsByProcess[key]["TaskStartDate"]
					: "''";
				var endField = startEndDateFieldsByProcess[key].ContainsKey("TaskEndDate")
					? startEndDateFieldsByProcess[key]["TaskEndDate"]
					: "''";

				var sqlForStartAndEndDates =
					"SELECT item_id, " + startField + " fromDate ," + endField + " toDate FROM " + tablename +
					" WHERE  item_id IN (" +
					string.Join(", ", parentIdsByProcess[key]) + ")";

				var row = _connectionBase.db.GetDatatableDictionary(sqlForStartAndEndDates);
				var startAndDatesByItemId = new Dictionary<int, Dictionary<string, object>>();
				row.ForEach(x => {
					var parentItemId = Conv.ToInt(x["item_id"]);
					if (!startAndDatesByItemId.ContainsKey(parentItemId)) {
						startAndDatesByItemId.Add(parentItemId, new Dictionary<string, object>());
					}

					if (!string.IsNullOrEmpty(Conv.ToStr(x["fromDate"]))) {
						startAndDatesByItemId[parentItemId].Add("from", Conv.ToDateTime(x["fromDate"]));
					}

					if (!string.IsNullOrEmpty(Conv.ToStr(x["toDate"]))) {
						startAndDatesByItemId[parentItemId].Add("to", Conv.ToDateTime(x["toDate"]));
					}
				});
				returnObject.Add(key, startAndDatesByItemId);
			}

			return returnObject;
		}

		private Dictionary<int, object> FindParentPhasesForTasks(List<int> taskIds) {
			if (taskIds == null || taskIds.Count == 0) {
				return new Dictionary<int, object>();
			}

			var taskIdsToHandle = new List<int>();
			taskIdsToHandle.AddRange(taskIds);

			var idParentDictionary = new Dictionary<int, object>();

			var check_counter = 0;
			while (taskIdsToHandle.Count > 0) {
				check_counter++;
				if (check_counter > 9999) {
					throw new StackOverflowException("Max. task id recursion reached.");
				}

				var items = GetParentPhasesForTaskList(taskIdsToHandle);
				taskIdsToHandle.Clear();
				items.ForEach(x => {
					var parentId = Conv.ToInt(x["t2_id"]);
					var taskId = Conv.ToInt(x["t1_id"]);
					if (parentId > 0) {
						if (!idParentDictionary.ContainsKey(parentId)) {
							taskIdsToHandle.Add(parentId);
						}

						if (!idParentDictionary.ContainsKey(taskId)) {
							idParentDictionary.Add(taskId, new Dictionary<string, object> {
								{"parent_item_id", parentId},
								{"parent_title", x["t2_title"]}
							});
						}
					}
				});
			}

			return idParentDictionary;
		}

		private List<Dictionary<string, object>> GetParentPhasesForTaskList(List<int> taskIds) {
			var idString = string.Join(", ", taskIds);
			var taskTableName = _connectionBase.GetProcessTableName("task");
			var sql =
				"select t1.item_id t1_id, t2.item_id t2_id, t2.title t2_title from " + taskTableName +
				" t1 left join " + taskTableName + " t2 on t1.parent_item_id = t2.item_id where t1.item_id in (" +
				idString + ")";
			return _connectionBase.db.GetDatatableDictionary(sql);
		}

		private List<Dictionary<string, object>> GetHoursForTasks(SqlCondition userIdCondition,
			IDbDataParameter fromParameter,
			IDbDataParameter toParameter) {
			var hoursForTasksQuery = _connectionBase
				.BuildSelectQuery()
				.Columns("wth.*", "line_manager.first_name + ' ' + line_manager.last_name line_manager")
				.From(TimetrackerHoursTable, "wth")
				.Join(new Join(_connectionBase.GetProcessTableName("user"), "line_manager")
					.Type("LEFT JOIN")
					.On("line_manager_user_id", "line_manager.item_id"))
				.Where(userIdCondition.And(
					new SqlCondition().Column("[Date]").Between(fromParameter, toParameter)));
			var hoursForTasks = hoursForTasksQuery.Fetch();

			var itemsById = new Dictionary<int, Dictionary<string, object>>();
			hoursForTasks.ForEach(x => {
				var id = Conv.ToInt(x["item_id"]);
				if (!itemsById.ContainsKey(id)) {
					itemsById.Add(id, x);
				}
			});

			var breakdownItems = hoursForTasksQuery
				.Columns("wthb.*")
				.Join(new Join(TimetrackerHoursBreakdownTable, "wthb").On("wthb.WORKTIME_TRACKING_HOUR_ID",
					"wth.item_id"))
				.Fetch();
			breakdownItems.ForEach(x => {
				var id = Conv.ToInt(x["worktime_tracking_hour_id"]);
				var parent = itemsById[id];
				if (!parent.ContainsKey("breakdownHours")) {
					parent.Add("breakdownHours", new List<WorktimeTrackingTaskHourBreakdown>());
				}

				((List<WorktimeTrackingTaskHourBreakdown>) parent["breakdownHours"]).Add(
					new WorktimeTrackingTaskHourBreakdown(x));
			});

			return hoursForTasks;
		}

		private DataTable TaskIdsWithProcess(SqlCondition userIdCondition, IDbDataParameter fromParameter,
			IDbDataParameter toParameter) {
			const string taskItemId = "task_item_id";
			var taskIdsQuery = TaskIdsQuery(userIdCondition, fromParameter, toParameter, taskItemId);
			var existingTasks = FindExistingTasks(taskIdsQuery);

			var processes = new Processes(_authenticatedSession.instance, _authenticatedSession);
			processes.AddPrimaryNameToDatatableColumn(existingTasks, "owner_name", "owner_item_id", "owner_type",
				compileForClient: true, includeSecondaries: true);

			processes.AddPrimaryNameToDatatableColumn(existingTasks, "title", "task_item_id", "process",
				compileForClient: true, includeSecondaries: true);

			return existingTasks;
		}

		private List<Dictionary<string, object>> FindDeletedTasks(string taskIdsQuery) {
			var taskTable = _connectionBase.GetProcessTableName("task");
			var query = "SELECT * FROM (" + taskIdsQuery + ") q " +
			            "LEFT JOIN " + taskTable + " t ON t.item_id=q.task_item_id WHERE item_id IS NULL";
			return _connectionBase.db.GetDatatableDictionary(query, _connectionBase.DBParameters);
		}

		private DataTable FindExistingTasks(string taskIdsQuery) {
			var taskTable = _connectionBase.GetProcessTableName("task");

			var taskIdQueryWrappedWithInstanceAndProcessJoin =
				"SELECT DISTINCT q.task_item_id, 'task' AS process, tt.start_date, tt.end_date, " +
				" CASE WHEN tt.title IS NULL THEN 1 ELSE 0 END AS deleted,ISNULL(tt.title, at.title) AS title, ISNULL(tt.owner_item_id, at.owner_item_id) AS owner_item_id, ISNULL(i.process,'task') AS process, ISNULL(towner_type.process, atowner_type.process) owner_type, timetracker.description, '' as owner_name FROM ( " +
				taskIdsQuery +
				") q " +
				"LEFT JOIN items i ON i.item_id = q.task_item_id " +
				"LEFT JOIN " + taskTable + " tt ON tt.item_id = q.task_item_id " +
				"LEFT JOIN archive_" + taskTable +
				" at ON at.item_id = q.task_item_id AND at.archive_start = (SELECT MAX(archive_start) FROM archive_" +
				taskTable +
				" WHERE item_id = q.task_item_id) " +
				"LEFT JOIN items atowner_type ON atowner_type.item_id = at.owner_item_id " +
				"LEFT JOIN items towner_type ON towner_type.item_id = tt.owner_item_id " +
				"LEFT JOIN " + TimetrackerUserTasksTable +
				" timetracker ON timetracker.task_item_id = q.task_item_id " +
				"WHERE ISNULL(tt.title, at.title) IS NOT null " +
				"ORDER BY owner_item_id";

			var existingTasks = _connectionBase.db.GetDatatable(
				taskIdQueryWrappedWithInstanceAndProcessJoin,
				_connectionBase.DBParameters);

			return existingTasks;
		}

		private string TaskIdsQuery(SqlCondition userIdCondition, IDbDataParameter fromParameter,
			IDbDataParameter toParameter,
			string taskItemId) {
			// find task ids
			var myListOfTaskIds = _connectionBase
				.BuildSelectQuery()
				.Columns(taskItemId)
				.From(TimetrackerUserTasksTable, "wtus")
				.Where(userIdCondition);
			var myTaskIdsWhereIsHoursInDateRange = _connectionBase
				.BuildSelectQuery()
				.Columns(taskItemId)
				.From(TimetrackerHoursTable, "wth")
				.Where(userIdCondition
					.And(new SqlCondition().Column("[date]").Between(fromParameter, toParameter)));

			var taskIdsQuery = myListOfTaskIds.Union(myTaskIdsWhereIsHoursInDateRange);
			return taskIdsQuery;
		}

		private ItemBase GetHourItemService() {
			return new ItemBase(_authenticatedSession.instance, "worktime_tracking_hours", _authenticatedSession);
		}

		private void RunDeleteQueries(List<WorktimeTrackingTaskHours> toBeDeleted) {
			var index = 0;
			var breakdownRowsToRemove = new List<DeleteQuery>();
			var deleteQueries = new List<DeleteQuery>();
			var itemService = GetHourItemService();
			foreach (var itemToDelete in toBeDeleted) {
				itemService.Delete(Conv.ToInt(itemToDelete.ItemId));
				breakdownRowsToRemove.Add(GetDeleteQueryForBreakdownRows(itemToDelete));
				index++;
			}

			_connectionBase.db.ExecuteDeleteQuery(breakdownRowsToRemove, _connectionBase.DBParameters);
			_connectionBase.db.ExecuteDeleteQuery(deleteQueries, _connectionBase.DBParameters);
		}

		private static DeleteQuery GetDeleteQueryForBreakdownRows(WorktimeTrackingTaskHours itemToDelete) {
			return new DeleteQuery()
				.From(TimetrackerHoursBreakdownTable)
				.Where(new SqlCondition("worktime_tracking_hour_id", SqlComp.EQUALS, itemToDelete.ItemId.ToString()));
		}

		private int RunUpdateQueries(List<WorktimeTrackingTaskHours> toBeUpdated) {
			var updateQueries = new List<string>();
			var breakdownInsertQueries = new List<InsertQuery>();
			var index = 0;
			var breakdownRowsToRemove = new List<DeleteQuery>();
			foreach (var itemToUpdate in toBeUpdated) {
				updateQueries.Add(ModelToUpdateQuery(itemToUpdate, index));
				breakdownRowsToRemove.Add(GetDeleteQueryForBreakdownRows(itemToUpdate));
				breakdownInsertQueries.AddRange(GetInsertQueriesForBreakdownHours(itemToUpdate, index));
				index++;
			}

			_connectionBase.db.ExecuteUpdateQuery(string.Concat(updateQueries), _connectionBase.DBParameters);
			_connectionBase.db.ExecuteDeleteQuery(breakdownRowsToRemove, _connectionBase.DBParameters);
			_connectionBase.db.ExecuteInsertQuery(breakdownInsertQueries, _connectionBase.DBParameters);
			return index;
		}

		private List<InsertQuery> RunInsertQueries(List<WorktimeTrackingTaskHours> toBeInserted) {
			var index = 0;
			var insertQueries = new List<InsertQuery>();
			var breakdownInsertQueries = new List<InsertQuery>();
			foreach (var itemToInsert in toBeInserted) {
				ModelToInsertQuery(itemToInsert, index);
				breakdownInsertQueries.AddRange(GetInsertQueriesForBreakdownHours(itemToInsert, index));
				index++;
			}

			_connectionBase.db.ExecuteInsertQuery(insertQueries, _connectionBase.DBParameters);
			_connectionBase.db.ExecuteInsertQuery(breakdownInsertQueries, _connectionBase.DBParameters);
			return breakdownInsertQueries;
		}


		public List<WorkTimeTrackingSheetStatus> GetMyTimesheetStatuses(int userItemId, DateTime start, DateTime end) {
			_connectionBase.SetDBParameter("@userId", userItemId);
			var startOfWeek = Util.StartOfWeek(start);
			var endOfWeek = Util.EndOfWeek(end);
			_connectionBase.SetDBParameter("@start", startOfWeek);
			_connectionBase.SetDBParameter("@end", endOfWeek);
			var userTable = _connectionBase.GetProcessTableName("user");
			var sql = "SELECT " +
			          "u.item_id AS user_item_id, " +
			          "first_name + ' ' + last_name AS name, " +
			          "(SELECT first_name + ' ' + last_name FROM " + userTable +
			          " WHERE item_id = (SELECT TOP 1 item_id FROM item_data_process_selections WHERE item_column_id = (SELECT item_column_id FROM item_columns WHERE process='user' AND name='hour_approval_users') AND selected_item_id = u.item_id)) AS line_manager," +
			          " ISNULL(DATEPART(iso_week,date),0) AS week, " +
			          "SUM(ISNULL(hours,0)) AS hours, " +
			          "SUM(ISNULL(additional_hours,0)) AS additional_hours, " +
			          "MIN(ISNULL(CASE WHEN line_manager_status = 0.5 THEN -2 ELSE line_manager_status END,0)) AS status " +
			          "FROM " + userTable + " u " +
			          "LEFT JOIN " + TimetrackerHoursTable +
			          " h ON h.user_item_id = u.item_id AND h.date BETWEEN @start AND @end " +
			          " WHERE " +
			          " u.item_id IN (@userId) " +
			          "GROUP BY first_name + ' ' + last_name, u.item_id, DATEPART(iso_week,date) " +
			          "ORDER BY first_name + ' ' + last_name ASC, week ASC ";

			var result = new List<WorkTimeTrackingSheetStatus>();
			foreach (DataRow status in _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters).Rows) {
				var c = new WorkTimeTrackingSheetStatus();
				c.UserItemId = Conv.ToInt(status["user_item_id"]);
				c.Name = Conv.ToStr(status["name"]);
				c.LineManager = Conv.ToStr(status["line_manager"]);
				c.Week = Conv.ToInt(status["week"]);
				c.Hours = Conv.ToInt(status["hours"]);
				c.AdditionalHours = Conv.ToInt(status["additional_hours"]);
				c.Status = Conv.ToInt(status["status"]);
				result.Add(c);
			}

			return result;
		}

		public List<int> MyTeamUserIds() {
			var sql_getmyteam = "SELECT selected_item_id AS item_id FROM item_data_process_selections " +
			                    "WHERE " +
			                    "item_column_id = (SELECT item_column_id FROM item_columns ic WHERE name = 'hour_approval_users') " +
			                    "AND item_id = @user_item_id ";
			_connectionBase.SetDBParameter("@user_item_id", _authenticatedSession.item_id);
			var userIds = new List<int>();
			foreach (DataRow row in _connectionBase.db.GetDatatable(sql_getmyteam, _connectionBase.DBParameters).Rows) {
				userIds.Add(Conv.ToInt(row["item_id"]));
			}

			return userIds;
		}

		public KeyValuePair<double, double> GetHourTotal(int UserItemId, DateTime start, DateTime end) {
			var sql =
				"SELECT ISNULL(SUM(hours),0) AS hours, ISNULL(SUM(additional_hours),0) AS additional_hours FROM " +
				TimetrackerHoursTable + " h " +
				"WHERE user_item_id = @user_item_id AND date BETWEEN @start AND @end";

			_connectionBase.SetDBParameter("@user_item_id", UserItemId);
			_connectionBase.SetDBParameter("@start", start);
			_connectionBase.SetDBParameter("@end", end);

			var r = _connectionBase.db.GetDataRow(sql, _connectionBase.DBParameters);
			return new KeyValuePair<double, double>(Conv.ToDouble(r["hours"]), Conv.ToDouble(r["additional_hours"]));
		}

		public List<WorkTimeTrackingWidgetItem> GetWidgetItems(int UserItemId, DateTime start, DateTime end) {
			var result = new List<WorkTimeTrackingWidgetItem>();
			var sql =
				"SELECT list_item AS title, SUM(ISNULL(hours,0)) AS hours, SUM(ISNULL(additional_hours,0)) AS additional_hours " +
				"FROM _" + _connectionBase.instance + "_worktime_tracking_breakdown_types t " +
				"LEFT JOIN (SELECT hb.hours, hb.additional_hours, hb.list_item_id " +
				"FROM " + TimetrackerHoursTable + " h " +
				"LEFT JOIN " + TimetrackerHoursBreakdownTable + " hb ON h.item_id = hb.worktime_tracking_hour_id " +
				"LEFT JOIN _" + _connectionBase.instance +
				"_worktime_tracking_basic_tasks bt ON h.task_item_id = bt.item_id " +
				"WHERE h.user_item_id = @user_item_id AND h.process = 'task' AND h.date BETWEEN @start AND @end) h ON t.item_id = h.list_item_id " +
				"GROUP BY list_item ORDER BY list_item ASC";

			_connectionBase.SetDBParameter("@user_item_id", UserItemId);
			_connectionBase.SetDBParameter("@start", Util.StartOfWeek(start));
			_connectionBase.SetDBParameter("@end", Util.EndOfWeek(end));

			foreach (DataRow status in _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters).Rows) {
				var c = new WorkTimeTrackingWidgetItem();
				c.Title = Conv.ToStr(status["title"]);
				c.Hours = Conv.ToDouble(status["hours"]);
				c.AdditionalHours = Conv.ToDouble(status["additional_hours"]);
				result.Add(c);
			}

			return result;
		}

		public List<WorkTimeTrackingSheetStatus> GetTimesheetStatusesForFilters(DateTime start, DateTime end,
			int portfolioId,
			string filterText, string filterSelections) {
			var result = new List<WorkTimeTrackingSheetStatus>();
			var userTable = _connectionBase.GetProcessTableName("user");
			var filters = new ItemFilterParser(filterSelections, null, _authenticatedSession.instance, "user",
				_authenticatedSession);
			var filtering = filters.Get();
			var _portfolioSqlQueryService = new PortfolioSqlService(
				_authenticatedSession.instance,
				_authenticatedSession,
				new ItemColumns(_authenticatedSession.instance, "user", _authenticatedSession),
				"user", "user");

			var userPortfolioId = portfolioId;
			_connectionBase.SetDBParameter("@portfolioId", portfolioId);
			var assertPortfolioIdIsValidSql =
				"SELECT count(*) FROM process_portfolios WHERE process = 'user' AND instance = @instance AND process_portfolio_id=@portfolioId";
			var idExists =
				Conv.ToInt(_connectionBase.db.ExecuteScalar(assertPortfolioIdIsValidSql, _connectionBase.DBParameters));
			if (idExists == 0) {
				return result;
			}

			var pSqls = _portfolioSqlQueryService.GetSql(userPortfolioId, filterText);

			var searchSql = pSqls.GetSearchSql();

			var sqlForUsers = "SELECT item_id FROM " + userTable + " pf WHERE 1=1 " +
			                  filtering + searchSql;
			var users = _connectionBase.db.GetDatatableDictionary(sqlForUsers);
			if (users.Count == 0) {
				return result;
			}

			var userIds = new List<int>();
			users.ForEach(x => userIds.Add(Conv.ToInt(x["item_id"])));
			var userIdsInString = string.Join(",", userIds);
			var sql = "SELECT " +
			          "u.item_id AS user_item_id, " +
			          "first_name + ' ' + last_name AS name, " +
			          "(SELECT first_name + ' ' + last_name FROM " + userTable +
			          " WHERE item_id = (SELECT TOP 1 item_id FROM item_data_process_selections WHERE item_column_id = (SELECT item_column_id FROM item_columns WHERE process='user' AND name='hour_approval_users') AND selected_item_id = u.item_id)) AS line_manager," +
			          " ISNULL(DATEPART(iso_week,date),0) AS week, " +
			          "SUM(ISNULL(hours,0)) AS hours, " +
			          "SUM(ISNULL(additional_hours,0)) AS additional_hours, " +
			          "MIN(ISNULL(CASE WHEN line_manager_status = 0.5 THEN -2 ELSE line_manager_status END,0)) AS status " +
			          "FROM " + userTable + " u " +
			          "LEFT JOIN " + TimetrackerHoursTable +
			          " h ON h.user_item_id = u.item_id AND h.date BETWEEN @start AND @end " +
			          " WHERE " +
			          " u.active = 1 AND u.item_id IN (" + userIdsInString;

			sql += ") " +
			       "GROUP BY first_name + ' ' + last_name, u.item_id, DATEPART(iso_week,date) " +
			       "ORDER BY first_name + ' ' + last_name ASC, week ASC ";
			_connectionBase.SetDBParameter("@start", Util.StartOfWeek(start));
			_connectionBase.SetDBParameter("@end", Util.EndOfWeek(end));


			foreach (DataRow status in _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters).Rows) {
				var c = new WorkTimeTrackingSheetStatus();
				c.UserItemId = Conv.ToInt(status["user_item_id"]);
				c.Name = Conv.ToStr(status["name"]);
				c.LineManager = Conv.ToStr(status["line_manager"]);
				c.Week = Conv.ToInt(status["week"]);
				c.Hours = Conv.ToInt(status["hours"]);
				c.AdditionalHours = Conv.ToInt(status["additional_hours"]);
				c.Status = Conv.ToInt(status["status"]);
				result.Add(c);
			}

			return result;
		}

		public List<WorkTimeTrackingSheetStatus> GetTimesheetStatuses(int UserItemId, DateTime start, DateTime end) {
			var result = new List<WorkTimeTrackingSheetStatus>();
			var userTable = _connectionBase.GetProcessTableName("user");
			var sql = "SELECT " +
			          "u.item_id AS user_item_id, " +
			          "first_name + ' ' + last_name AS name, " +
			          "(SELECT first_name + ' ' + last_name FROM " + userTable +
			          " WHERE item_id = (SELECT TOP 1 item_id FROM item_data_process_selections WHERE item_column_id = (SELECT item_column_id FROM item_columns WHERE process='user' AND name='hour_approval_users') AND selected_item_id = u.item_id)) AS line_manager," +
			          "ISNULL(DATEPART(iso_week,date),0) AS week, " +
			          "SUM(ISNULL(hours,0)) AS hours, " +
			          "SUM(ISNULL(additional_hours,0)) AS additional_hours, " +
			          "MIN(ISNULL(CASE WHEN line_manager_status = 0.5 THEN -2 ELSE line_manager_status END,0)) AS status " +
			          "FROM " + userTable + " u " +
			          "LEFT JOIN " + TimetrackerHoursTable +
			          " h ON h.user_item_id = u.item_id AND h.date BETWEEN @start AND @end " +
			          " WHERE " +
			          " u.active = 1 AND u.item_id IN (";

			var sql_getmyteam = "SELECT selected_item_id AS item_id FROM item_data_process_selections " +
			                    "WHERE " +
			                    "item_column_id = (SELECT item_column_id FROM item_columns ic WHERE name = 'hour_approval_users') " +
			                    "AND item_id = @user_item_id ";

			var sql_getall = "SELECT item_id FROM _" + _connectionBase.instance + "_user";

			_connectionBase.SetDBParameter("@start", Util.StartOfWeek(start));
			_connectionBase.SetDBParameter("@end", Util.EndOfWeek(end));

			if (UserItemId == 0) {
				sql += sql_getall;
			} else {
				sql += sql_getmyteam;
				_connectionBase.SetDBParameter("@user_item_id", UserItemId);
			}

			sql += ") " +
			       "GROUP BY first_name + ' ' + last_name, u.item_id, DATEPART(iso_week,date) " +
			       "ORDER BY first_name + ' ' + last_name ASC, week ASC ";

			foreach (DataRow status in _connectionBase.db.GetDatatable(sql, _connectionBase.DBParameters).Rows) {
				var c = new WorkTimeTrackingSheetStatus();
				c.UserItemId = Conv.ToInt(status["user_item_id"]);
				c.Name = Conv.ToStr(status["name"]);
				c.LineManager = Conv.ToStr(status["line_manager"]);
				c.Week = Conv.ToInt(status["week"]);
				c.Hours = Conv.ToInt(status["hours"]);
				c.AdditionalHours = Conv.ToInt(status["additional_hours"]);
				c.Status = Conv.ToDouble(status["status"]);
				result.Add(c);
			}

			return result;
		}

		private SelectQuery GetTaskIdsThatAreNotLocked(List<int> ids) {
			if (ids.Count == 0) return null;
			var taskProcessTableName = _connectionBase.GetProcessTableName("task");
			var instanceParam = _connectionBase.SetDBParameter("@instance", _connectionBase.instance);
			_connectionBase.SetDBParameter("@process", "task");
			_connectionBase.SetDBParameter("@propertyName", "responsibles");
			var query = _connectionBase.BuildSelectQuery()
				.Columns("DISTINCT pf.item_id")
				.From(taskProcessTableName, "pf")
				.Join(new Join("ITEM_DATA_PROCESS_SELECTIONS", "idps").Type("LEFT JOIN")
					.On("idps.item_id", "pf.item_id"))
				.Join(new Join("item_columns", "ic").Type("LEFT JOIN").On("ic.item_column_id", "idps.item_column_id"))
				.Where("pf.item_id IN (" + Conv.ToDelimitedString(ids) + ")");


			var confData =
				Config.GetClientConfiguration(_authenticatedSession, "View Configurator", "worktimeTracking");
			if (confData.ContainsKey("")) {
				var confJson = confData[""];
				var confJsonData = JsonConvert.DeserializeObject<Dictionary<string, object>>(confJson);
				if (confJsonData.ContainsKey("TaskSelectionPortfolioId")) {
					var portfolioId = Conv.ToInt(confJsonData["TaskSelectionPortfolioId"]);
					var EntityRepository =
						new EntityRepository(_connectionBase.instance, "task", _authenticatedSession);
					query.Where(query.GetWhereClause() + " AND " + EntityRepository.GetWhere(portfolioId));
				}
			}

			return query;
		}

		private SelectQuery GetTaskIdsWhereIAmResponsible(int userId) {
			var taskProcessTableName = _connectionBase.GetProcessTableName("task");
			var instanceParam = _connectionBase.SetDBParameter("@instance", _connectionBase.instance);
			_connectionBase.SetDBParameter("@process", "task");
			_connectionBase.SetDBParameter("@propertyName", "responsibles");
			var selectedUser = _connectionBase.SetDBParameter("@userSelected", userId);
			var query = _connectionBase.BuildSelectQuery()
				.Columns("pf.item_id")
				.From(taskProcessTableName, "pf")
				.Join(new Join("ITEM_DATA_PROCESS_SELECTIONS", "idps").On("idps.item_id", "pf.item_id"))
				.Join(new Join("item_columns", "ic").On("ic.item_column_id", "idps.item_column_id"))
				.Where(new SqlCondition("ic.instance", SqlComp.EQUALS, instanceParam)
					.And("idps.selected_item_id", SqlComp.EQUALS, selectedUser));

			var confData =
				Config.GetClientConfiguration(_authenticatedSession, "View Configurator", "worktimeTracking");
			if (confData.ContainsKey("")) {
				var confJson = confData[""];
				var confJsonData = JsonConvert.DeserializeObject<Dictionary<string, object>>(confJson);
				if (confJsonData.ContainsKey("TaskPortfolioId")) {
					var portfolioId = Conv.ToInt(confJsonData["TaskPortfolioId"]);
					var EntityRepository =
						new EntityRepository(_connectionBase.instance, "task", _authenticatedSession);
					query.Where(query.GetWhereClause() + " AND " + EntityRepository.GetWhere(portfolioId));
				}
			}

			return query;
		}

		private string BuildHourStatusUpdateQuery(
			WorktimeHourApprovalItem worktimeHourApprovalItem,
			int index,
			bool updateEditor,
			List<double?> fromStatus,
			double toStatus,
			bool updateLineManagerStatus,
			bool updateProjectManagerStatus,
			bool doNoSetApprover = false
		) {
			if (!updateLineManagerStatus && !updateProjectManagerStatus) {
				throw new CustomArgumentException("Select which status you want to update");
			}

			var toStatusParameter = _connectionBase.SetDBParameter("@toStatus", toStatus);

			var taskParameter = _connectionBase.SetDBParameter("@taskId" + index, worktimeHourApprovalItem.TaskId);
			var userParameter = _connectionBase.SetDBParameter("@userId" + index, worktimeHourApprovalItem.UserId);
			var fromParameter = _connectionBase.SetDBParameter("@from" + index, worktimeHourApprovalItem.From.Date);
			var toParameter = _connectionBase.SetDBParameter("@to" + index, worktimeHourApprovalItem.To.Date);

			var myUserIdParameter =
				_connectionBase.SetDBParameter("@myUserId", _authenticatedSession.DelegatorOrItemId);

			var fieldToUpdate = new List<string>();
			var statusFieldNames = new List<string>();
			var fromStatusContainsNull = fromStatus.Contains(null);
			var fromStatusesWithoutNulls = fromStatus.Where(x => x != null).ToList();
			if (updateLineManagerStatus) {
				fieldToUpdate.Add("line_manager_status = " + toStatusParameter.ParameterName + " ");
				fieldToUpdate.Add(" line_manager_approved_at = CURRENT_TIMESTAMP ");
				if (updateEditor) {
					fieldToUpdate.Add(" line_manager_user_id = " + myUserIdParameter.ParameterName + " ");
				} else {
					fieldToUpdate.Add(" line_manager_user_id = null ");
				}


				var statusField = "ISNULL(line_manager_status,0) IN (" + string.Join(",", fromStatusesWithoutNulls) +
				                  ")";
				//if (fromStatusContainsNull) {
				//	statusField = "(line_manager_status is null or (" + statusField + "))";
				//}

				statusFieldNames.Add(statusField);
			}

			if (updateProjectManagerStatus) {
				fieldToUpdate.Add("project_manager_status = " + toStatusParameter.ParameterName + " ");
				fieldToUpdate.Add(" project_manager_approved_at = CURRENT_TIMESTAMP ");
				if (updateEditor) {
					fieldToUpdate.Add(" project_manager_user_id = " + myUserIdParameter.ParameterName + " ");
				} else {
					fieldToUpdate.Add(" project_manager_user_id = null ");
				}

				var statusField = " ISNULL(project_manager_status,0) IN (" +
				                  string.Join(",", fromStatusesWithoutNulls) + ")";
				//if (fromStatusContainsNull) {
				//	statusField = "(project_manager_status is null or (" + statusField + ")) ";
				//}

				statusFieldNames.Add(statusField);
			}

			var taskCondition = worktimeHourApprovalItem.TaskId > 0
				? "task_item_id = " + taskParameter.ParameterName + " AND "
				: "";
			var updateQuery = "UPDATE " + TimetrackerHoursTable + " " +
			                  "SET " +
			                  string.Join(",", fieldToUpdate) +
			                  "WHERE " +
			                  taskCondition +
			                  "user_item_id = " + userParameter.ParameterName + " AND " +
			                  "date BETWEEN " + fromParameter.ParameterName + " AND " + toParameter.ParameterName +
			                  " AND " +
			                  string.Join(" AND ", statusFieldNames);

			return updateQuery;
		}


		private InsertQuery ModelToInsertQuery(WorktimeTrackingTaskHours item, int index) {
			var itemService = GetHourItemService();
			var data = item.ToDictionary();

			var taskId = Conv.ToInt(data["task_item_id"]);
			data.Add("owner_item_id", taskId > 0
				? Conv.ToInt(_connectionBase.db.ExecuteScalar("SELECT owner_item_id FROM _" + _connectionBase.instance +
				                                              "_task WHERE item_id = " + taskId))
				: 0);
			
			var uniqueData = new Dictionary<string, object> {
				{"task_item_id", data["task_item_id"]},
				{"date", data["date"]},
				{"process", data["process"]},
				{"user_item_id", data["user_item_id"]}
			};

			itemService.AddItemOrUpdate(data, uniqueData, 0, false);

			return null;
		}

		private List<InsertQuery> GetInsertQueriesForBreakdownHours(WorktimeTrackingTaskHours item, int index) {
			var date = DateTime.ParseExact(item.Date, "ddMMyyyy", CultureInfo.InvariantCulture);
			var dateParam = _connectionBase.SetDBParameter("@date" + index, date);
			var taskIdParam = _connectionBase.SetDBParameter("@taskId" + index, item.TaskId);
			var userIdParam =
				_connectionBase.SetDBParameter("@userId", item.UserItemId ?? _authenticatedSession.item_id);
			var processParam = _connectionBase.SetDBParameter("@process" + index, item.Process);

			var whereClauses = new SqlCondition("task_item_id", SqlComp.EQUALS, taskIdParam)
				.And(new SqlCondition("user_item_id", SqlComp.EQUALS, userIdParam))
				.And(new SqlCondition("date", SqlComp.EQUALS, dateParam))
				.And(new SqlCondition("process", SqlComp.EQUALS, processParam));

			var queries = new List<InsertQuery>();
			string insertedItemSelector;
			if (item.ItemId == null) {
				insertedItemSelector = new SelectQuery()
					.Columns("TOP 1 item_id")
					.From(TimetrackerHoursTable)
					.OrderBy("created_at DESC")
					.Where(
						whereClauses).Get(false);
				insertedItemSelector = "(" + insertedItemSelector + ")";
			} else {
				insertedItemSelector = item.ItemId.ToString();
			}

			if (item.BreakdownHours == null) {
				return queries;
			}

			var subindex = 0;

			foreach (var breakdownItem in item.BreakdownHours) {
				subindex++;
				if ((breakdownItem == null || breakdownItem.Hours == 0) &&
				    (breakdownItem.AdditionalHours == null || breakdownItem.AdditionalHours == 0)) continue;
				var values = new Dictionary<string, object>();
				if (breakdownItem.Hours > 0) {
					var breakdownParam =
						_connectionBase.SetDBParameter("@breakdownHours" + index + "_" + subindex, breakdownItem.Hours);
					values.Add("hours", breakdownParam.ParameterName);
				}

				if (breakdownItem.AdditionalHours > 0) {
					var additionParam = _connectionBase.SetDBParameter(
						"@breakdownAdditionalHours" + index + "_" + subindex,
						breakdownItem.AdditionalHours);
					values.Add("additional_hours", additionParam.ParameterName);
				}

				//if (breakdownItem.AdditionalHours > 0) {
				var CommentParam = _connectionBase.SetDBParameter(
					"@Comment" + index + "_" + subindex,
					Conv.ToStr(breakdownItem.Comment));
				values.Add("comment", CommentParam.ParameterName);
				//}

				var commentListItem = _connectionBase.SetDBParameter(
					"@commentListItem" + index + "_" + subindex,
					Conv.ToInt(breakdownItem.CommentListItemId));
				values.Add("comment_list_item_id", commentListItem.ParameterName);

				var typeParam = _connectionBase.SetDBParameter("@type" + index + "_" + subindex, breakdownItem.Type);
				var listitemParam =
					_connectionBase.SetDBParameter("@listItemId" + index + "_" + subindex, breakdownItem.ListItemId);
				values.Add("type", typeParam.ParameterName);
				values.Add("list_item_id", listitemParam.ParameterName);
				values.Add("worktime_tracking_hour_id", insertedItemSelector);

				//values.Add("comment", _connectionBase.SetDBParameter("@Comment",Conv.toStr(breakdownItem.Comment)));

				queries.Add(new InsertQuery()
					.To(TimetrackerHoursBreakdownTable)
					.Values(values));
			}

			return queries;
		}

		private string ModelToUpdateQuery(WorktimeTrackingTaskHours itemToUpdate, int index) {
			var idParam = _connectionBase.SetDBParameter("@id" + index, itemToUpdate.ItemId);
			var processParam = _connectionBase.SetDBParameter("@process" + index, itemToUpdate.Process);

			var setHoursStrings = new List<string>();
			if (itemToUpdate.Hours == null) {
				itemToUpdate.Hours = 0;
			}

			if (itemToUpdate.AdditionalHours == null) {
				itemToUpdate.AdditionalHours = 0;
			}

			var hourParam = _connectionBase.SetDBParameter("@hours" + index, itemToUpdate.Hours);
			setHoursStrings.Add("hours = " + hourParam.ParameterName + " ");
			var additionalHourParam =
				_connectionBase.SetDBParameter("@additionalhours" + index, itemToUpdate.AdditionalHours);
			setHoursStrings.Add("additional_hours = " + additionalHourParam.ParameterName + " ");

			var setHoursStr = string.Join(",", setHoursStrings);

			var sql = "UPDATE " + TimetrackerHoursTable +
			          " SET " +
			          setHoursStr +
			          " WHERE item_id = " + idParam.ParameterName + " AND process = " + processParam.ParameterName +
			          ";";
			return sql;
		}
	}
}