﻿using System;

namespace Keto5.x.platform.server.core.workTimeTracking{
	public class WorktimeHourApprovalItem{
		private DateTime _from;


		private DateTime _to;
		public int TaskId{ get; set; }
		public int UserId{ get; set; }

		public DateTime From{
			get => new DateTime(_from.Year, _from.Month, _from.Day, 0, 0, 0, 0);
			set => _from = value;
		}

		public DateTime To{
			get => new DateTime(_to.Year, _to.Month, _to.Day, 23, 59, 59, 999);
			set => _to = value;
		}
	}
}