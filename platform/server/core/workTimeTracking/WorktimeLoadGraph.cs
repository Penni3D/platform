using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core.workTimeTracking
{
	public class WorktimeGraph
	{
		public WorktimeGraph() { }


		public WorktimeGraph(DataRow dr, AuthenticatedSession session) {


			Hours = (double) dr["hours"];
			AdditionalHours = (double) dr["additional_hours"];
			CommentListItemId = (int) dr["comment_list_item_id"];
		}

		public double Hours { get; set; }
		public double AdditionalHours { get; set; }
		public int CommentListItemId { get; set; }

	}

	public class WorktimeGraphs : ProcessBase
	{
		public WorktimeGraphs(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance,
			process, authenticatedSession) { }


		public Dictionary<string, List<WorktimeGraph>> GetLoadGraphData(string startTime, string endTime) {
			SetDBParameters("@currentUser", authenticatedSession.item_id);
			SetDBParameters("@startTime", startTime);
			SetDBParameters("@endTime", endTime);

			var result = new Dictionary<string, List<WorktimeGraph>>();
			var newList = new List<WorktimeGraph>();
			result.Add(startTime, newList);
			var sql =
				"SELECT ISNULL(SUM(wthb.hours), 0) as hours, ISNULL(SUM(wthb.additional_hours), 0) as additional_hours, wthb.comment_list_item_id FROM worktime_tracking_hours_breakdown wthb  " +
				"INNER JOIN _" + instance + "_worktime_tracking_hours wth ON wthb.worktime_tracking_hour_id = wth.item_id " +
				"WHERE wth.user_item_id = @currentUser AND date between @startTime AND @endTime AND comment_list_item_id IS NOT NULL " +
				"GROUP BY wthb.comment_list_item_id ";

			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				newList.Add(new WorktimeGraph(row, authenticatedSession));
			}

			return result;

		}

	}
}