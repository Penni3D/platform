﻿using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core.workTimeTracking {
	public class WorktimeTrackingService {
		public struct HourStatus {
			public const double Draft = 0;
			public const double SubmittedPartial = 0.5;
			public const double Submitted = 1;
			public const double Approved = 2;
			public const double Rejected = 3;
		}

		private readonly AuthenticatedSession _currentSession;

		private readonly IWorktimeTrackingRepository _worktimeTrackingRepository;

		public WorktimeTrackingService(
			IWorktimeTrackingRepository worktimeTrackingRepository,
			AuthenticatedSession currentSession) {
			_currentSession = currentSession;
			_worktimeTrackingRepository = worktimeTrackingRepository;
		}

		public void AddItem(WorktimeTrackingTaskListItem item) {
			_worktimeTrackingRepository.InsertTaskToMyList(item);
		}

		public void RemoveItem(int userItemId, int taskId) {
			_worktimeTrackingRepository.RemoveItemFromMyList(userItemId, taskId);
		}

		public void SaveHours(int userItemId, List<WorktimeTrackingTaskHours> hours) {
			var itemsToUpdate = new List<WorktimeTrackingTaskHours>();
			var itemsToDelete = new List<WorktimeTrackingTaskHours>();
			var itemsToInsert = new List<WorktimeTrackingTaskHours>();
			foreach (var worktimeHours in hours) {
				worktimeHours.UserItemId = userItemId;
				var isHoursEmpty = worktimeHours.Hours == null || worktimeHours.Hours == 0;
				var isAdditionalHoursEmpty =
					worktimeHours.AdditionalHours == null || worktimeHours.AdditionalHours == 0;
				if (worktimeHours.ItemId != null && worktimeHours.ItemId > 0) {
					if (isAdditionalHoursEmpty && isHoursEmpty) {
						itemsToDelete.Add(worktimeHours);
					} else {
						itemsToUpdate.Add(worktimeHours);
					}
				} else {
					itemsToInsert.Add(worktimeHours);
				}
			}

			_worktimeTrackingRepository.InsertUpdateAndDeleteHours(
				itemsToInsert, itemsToUpdate, itemsToDelete, userItemId);
		}

		public void UpdateDescription(int taskId, Dictionary<string, string> descriptionObject) {
			if (descriptionObject == null || !descriptionObject.ContainsKey("description")) {
				throw new CustomArgumentException("No description property in received object");
			}

			_worktimeTrackingRepository.UpdateDescription(taskId, descriptionObject["description"]);
		}


		// Line manager methods
		public void ApproveHoursAsLineManager(List<WorktimeHourApprovalItem> approvalItems) {
			CheckSupervisorRelation(approvalItems);
			_worktimeTrackingRepository.UpdateLineManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Submitted},
				HourStatus.Approved,
				true);
		}

		public void RejectHoursAsLineManager(List<WorktimeHourApprovalItem> approvalItems) {
			CheckSupervisorRelation(approvalItems);

			_worktimeTrackingRepository.UpdateLineManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Approved, HourStatus.Submitted},
				HourStatus.Draft,
				true);
		}

		public void RevertHoursAsLineManager(List<WorktimeHourApprovalItem> approvalItems) {
			CheckSupervisorRelation(approvalItems);

			_worktimeTrackingRepository.UpdateLineManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Approved},
				HourStatus.Submitted,
				true);
		}

		private void CheckSupervisorRelation(List<WorktimeHourApprovalItem> approvalItems) {
			var usersUnderMyPower = _worktimeTrackingRepository.GetMySubUsers();
			var userIds = usersUnderMyPower.Select(x => (int) x["selected_item_id"]).ToList();
			approvalItems.ForEach(x => {
				if (!userIds.Contains(x.UserId)) {
					throw new CustomPermissionException("You can't reject hours if you are not users supervisor");
				}
			});
		}

		public void RestoreReadyAsLineManager(List<WorktimeHourApprovalItem> approvalItems) {
			var usersUnderMyPower = _worktimeTrackingRepository.GetMySubUsers();
			var userIds = usersUnderMyPower.Select(x => (int) x["selected_item_id"]).ToList();
			approvalItems.ForEach(x => {
				if (!userIds.Contains(x.UserId)) {
					throw new CustomPermissionException(
						"You can't restore hour statuses if you are not users supervisor");
				}
			});

			_worktimeTrackingRepository.UpdateLineManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Approved},
				HourStatus.Submitted,
				true);
		}

		public void MarkMyHoursAsApproved(int user_item_id, List<WorktimeHourApprovalItem> approvalItems) {
			// todo: permission control for approval 
			//CheckHourConfirmPermissions(user_item_id, approvalItems);
			_worktimeTrackingRepository.UpdateStatusesOfHoursAsMySelf(
				approvalItems,
				new List<double?> {HourStatus.Draft, HourStatus.Submitted},
				HourStatus.Approved,
				true,
				false
			);
		}

		public void ResetHoursToSubmitted(int user_item_id, List<WorktimeHourApprovalItem> approvalItems) {
			// todo: permission control for approval 
			//CheckHourConfirmPermissions(user_item_id, approvalItems);
			_worktimeTrackingRepository.UpdateStatusesOfHoursAsMySelf(
				approvalItems,
				new List<double?> {HourStatus.Approved},
				HourStatus.Submitted,
				false,
				true
			);
		}

		public void SubmitHours(int user_item_id, List<WorktimeHourApprovalItem> approvalItems, bool partial = false) {
			//CheckHourConfirmPermissions(user_item_id, approvalItems);
			var taskIdsThatRequireApproval = _worktimeTrackingRepository.GetTaskIdsThatRequireApproval();
			var basicTaskIdsThatRequireApproval = _worktimeTrackingRepository.GetBasicTaskIdsThatRequireApproval();
			var taskIdsToApprove = taskIdsThatRequireApproval.Concat(basicTaskIdsThatRequireApproval);

			var itemsToApproveDirectly = new List<WorktimeHourApprovalItem>();
			var itemsThatRequireApproval = new List<WorktimeHourApprovalItem>();
			approvalItems.ForEach(task => {
				if (taskIdsToApprove.Contains(task.TaskId)) {
					itemsThatRequireApproval.Add(task);
				} else {
					itemsToApproveDirectly.Add(task);
				}
			});

			if (itemsToApproveDirectly.Count > 0) {
				_worktimeTrackingRepository.UpdateStatusesOfHoursAsAdmin(
					itemsToApproveDirectly,
					new List<double?> {HourStatus.Draft, HourStatus.SubmittedPartial, null},
					partial ? HourStatus.SubmittedPartial : HourStatus.Approved,
					false,
					false
				);
			}

			if (itemsThatRequireApproval.Count > 0) {
				SendEmailToApprover(user_item_id);

				_worktimeTrackingRepository.UpdateStatusesOfHoursAsMySelf(
					itemsThatRequireApproval,
					new List<double?> {HourStatus.Draft, HourStatus.SubmittedPartial, null},
					partial ? HourStatus.SubmittedPartial : HourStatus.Submitted,
					false,
					true
				);
			}
		}

		private void SendEmailToApprover(int userItemId) {
			//Send Email to Approver
			var manager = _worktimeTrackingRepository.GetMyManager(userItemId);
			var email = new Email(_currentSession.instance, _currentSession);
			var body =
				"You have a timesheet from one of your team that requires approval. Please go to Keto to approve or reject.<br><br>" +
				email.GetSystemLink(_currentSession, "");
			var subject = "Timesheet Approval";
			email.Add(manager, Email.AddressTypes.ToAdress);
			email.Send(subject, body);
		}

		public void MarkMyHoursAsDraft(int user_item_id, List<WorktimeHourApprovalItem> approvalItems, double status = 0) {
			_worktimeTrackingRepository.UpdateStatusesOfHoursAsMySelf(
				approvalItems,
				new List<double?>
					{HourStatus.Submitted, HourStatus.Approved, HourStatus.Draft, HourStatus.SubmittedPartial, null},
				status,
				false,
				true
			);
		}

		private void CheckHourConfirmPermissions(int user_item_id, List<WorktimeHourApprovalItem> approvalItems) {
			approvalItems.ForEach(x => {
				if (x.UserId != user_item_id) {
					throw new CustomPermissionException("You can't mess with other people drafts " + x.UserId + " <> " +
					                                    user_item_id);
				}
			});

			///TODO: Janne, tarkistettava, että henkilö saa hyväksyä muiden puolesta int user_item_id <> session.item_id 

			var statuses = _worktimeTrackingRepository.FindStatusesInTimeSpan(approvalItems);
			var allowedStatuses = new List<int> {(int) HourStatus.Draft, (int) HourStatus.Submitted};
			if (statuses.Any(status => !allowedStatuses.Contains(status))) {
				throw new CustomArgumentException("Can't act on anything but draft and confirmed hours");
			}
		}

		public Dictionary<string, object> GetItemsToApproveAsLineManager(DateTime from, DateTime to) {
			var allConfigurations = new Configurations(_currentSession.instance, _currentSession);
			var clientConfigurations = allConfigurations.GetClientConfigurations();
			var worktimeTrackingConfigs = clientConfigurations
				.FindAll(x =>
					x.Category == "WorktimeTracking" &&
					x.Name == "ApprovalRequiredFieldInProcesses" &&
					!string.IsNullOrEmpty(x.Process))
				.ToList();

			var usersUnderMyPower = _worktimeTrackingRepository.GetMySubUsers();
			var userIds = usersUnderMyPower.Select(x => (int) x["selected_item_id"]).ToList();
			var returnDict = new Dictionary<string, object>();
			var approvable = _worktimeTrackingRepository.GetApprovableHoursForUsers(userIds);
			var approved = _worktimeTrackingRepository.GetApprovedTasksForUsers(userIds, from, to);
			var taskIds = new HashSet<int>();
			foreach (var dictionary in approvable) {
				taskIds.Add(Conv.ToInt(dictionary["task_item_id"]));
			}

			foreach (var dictionary in approved) {
				taskIds.Add(Conv.ToInt(dictionary["task_item_id"]));
			}

			var basicTasks = _worktimeTrackingRepository.GetItems("worktime_tracking_basic_tasks", taskIds.ToList());
			var tasks = _worktimeTrackingRepository.GetItems("task", taskIds.ToList());
			var users = _worktimeTrackingRepository.GetItems("user", userIds);
			returnDict.Add("approvable", approvable);
			returnDict.Add("approvedForCurrentWeek", approved);
			returnDict.Add("basicTasks", basicTasks);
			returnDict.Add("tasks", tasks);
			returnDict.Add("users", users);
			return returnDict;
		}


		// Project manager methods
		public void ApproveHoursAsProjectManager(List<WorktimeHourApprovalItem> approvalItems) {
			var usersUnderMyPower = _worktimeTrackingRepository.GetMySubUsers();
			var userIds = usersUnderMyPower.Select(x => (int) x["selected_item_id"]).ToList();
			approvalItems.ForEach(x => {
				var userId = x.UserId;
				if (!userIds.Contains(userId)) {
					throw new CustomPermissionException("You can't approve hours if you are not users supervisor");
				}
			});

			_worktimeTrackingRepository.UpdateProjectManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Submitted, HourStatus.Rejected},
				HourStatus.Approved,
				true);
		}

		public void RejectHoursAsProjectManager(List<WorktimeHourApprovalItem> approvalItems) {
			var usersUnderMyPower = _worktimeTrackingRepository.GetMySubUsers();
			var userIds = usersUnderMyPower.Select(x => (int) x["selected_item_id"]).ToList();
			approvalItems.ForEach(x => {
				if (!userIds.Contains(x.UserId)) {
					throw new CustomPermissionException("You can't reject hours if you are not users supervisor");
				}
			});

			_worktimeTrackingRepository.UpdateProjectManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Approved, HourStatus.Submitted, HourStatus.Rejected},
				HourStatus.Draft,
				true);
		}

		public void RevertHoursAsProjectManager(List<WorktimeHourApprovalItem> approvalItems) {
			var usersUnderMyPower = _worktimeTrackingRepository.GetMySubUsers();
			var userIds = usersUnderMyPower.Select(x => (int) x["selected_item_id"]).ToList();
			approvalItems.ForEach(x => {
				if (!userIds.Contains(x.UserId)) {
					throw new CustomPermissionException("You can't reject hours if you are not users supervisor");
				}
			});

			_worktimeTrackingRepository.UpdateProjectManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Approved, HourStatus.Rejected},
				HourStatus.Submitted,
				true);
		}

		public void RestoreReadyAsProjectManager(List<WorktimeHourApprovalItem> approvalItems) {
			var usersUnderMyPower = _worktimeTrackingRepository.GetMySubUsers();
			var userIds = usersUnderMyPower.Select(x => (int) x["selected_item_id"]).ToList();
			approvalItems.ForEach(x => {
				if (!userIds.Contains(x.UserId)) {
					throw new CustomPermissionException(
						"You can't restore hour statuses if you are not users supervisor");
				}
			});

			_worktimeTrackingRepository.UpdateProjectManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Approved, HourStatus.Rejected},
				HourStatus.Submitted,
				true);
		}

		public void MarkHoursAsReadyAsProjectManager(List<WorktimeHourApprovalItem> approvalItems) {
			approvalItems.ForEach(x => {
				if (x.UserId != _currentSession.item_id) {
					throw new CustomPermissionException("You can't mess with other people drafts");
				}
			});

			_worktimeTrackingRepository.UpdateLineManagerStatusesOfHours(
				approvalItems,
				new List<double> {HourStatus.Draft, HourStatus.Rejected},
				HourStatus.Submitted,
				false);
		}

		public Dictionary<string, object> GetItemsToApproveAsProjectManager(int projectId, DateTime from, DateTime to) {
			var returnDict = new Dictionary<string, object>();
			var approvable = _worktimeTrackingRepository.GetApprovableHoursForProjectManager(projectId, from, to);
			var approved = _worktimeTrackingRepository.GetApprovedTasksForProjectManager(projectId, from, to);

			var taskIds = new HashSet<int>();
			var userIds = new List<int>();

			foreach (var dictionary in approvable) {
				taskIds.Add(Conv.ToInt(dictionary["task_item_id"]));
				var userId = Conv.ToInt(dictionary["user_item_id"]);
				if (!userIds.Contains(userId)) {
					userIds.Add(userId);
				}
			}

			foreach (var dictionary in approved) {
				taskIds.Add(Conv.ToInt(dictionary["task_item_id"]));
				var userId = Conv.ToInt(dictionary["user_item_id"]);
				if (!userIds.Contains(userId)) {
					userIds.Add(userId);
				}
			}

			var tasks = _worktimeTrackingRepository.GetItems("task", taskIds.ToList());
			var users = _worktimeTrackingRepository.GetItems("user", userIds);

			returnDict.Add("approvable", approvable);
			returnDict.Add("approvedForCurrentWeek", approved);
			returnDict.Add("tasks", tasks);
			returnDict.Add("users", users);
			return returnDict;
		}
	}
}