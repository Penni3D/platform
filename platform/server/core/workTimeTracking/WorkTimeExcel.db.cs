﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;

namespace Keto5.x.platform.server.core.workTimeTracking {
	public class WorktimeExcelDB : ProcessBase {
		public WorktimeExcelDB(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance,
			process, authenticatedSession) { }

		private string GetWorktimeTrackingHourTableName() {
			return Conv.ToSql("_" + instance + "_WORKTIME_TRACKING_HOURS");
		}

		private string ConvertDate(DateTime? d) {
			if (d is null) return "";
			var delimeter = ".";
			if (authenticatedSession.locale_id == "en-GB") {
				delimeter = "/";
			}

			return d.Value.Day + delimeter + d.Value.Month + delimeter + d.Value.Year;
		}

		private decimal GetExcelDecimalValueForDate(DateTime date) {
			var start = new DateTime(1900, 1, 1);
			var diff = date - start;
			return diff.Days + 2;
		}
		private string GetDateFormat(bool withTime = false) {
			if (authenticatedSession.date_format == "dd/mm/yy") {
				return "dd/mm/yyyy" + (withTime ? "h:mm" : "");
			} else if (authenticatedSession.date_format == "mm/dd/yy") {
				return "m/d/yyyy"+ (withTime ? "h:mm" : "");
			} else if (authenticatedSession.date_format == "d M yyyy") {
				return "d m yyyy"+ (withTime ? "h:mm" : "");
			} else if (authenticatedSession.date_format == "dd.mm.yy") {
				return "d.m.yyyy"+ (withTime ? "h:mm" : "");
			} else {
				return "d.m.yyyy"+ (withTime ? "h:mm" : "");
			}
		}
		public byte[] GetExel(DateTime? start, DateTime? end, int v = 1, string ids = "", int process_item_id = 0) {
			var allConfigurations = new Configurations(instance, authenticatedSession);
			var clientConfigurations = allConfigurations.GetClientConfigurations();

			var pageConfigIndex =
				clientConfigurations.FindIndex(conf =>
					conf.Name == "worktimeHourStatus"); //Finds the index for the configuration of the feature.

			var additionalSql = "";
			var additionalCols2 = new List<ItemColumn>();

			if (pageConfigIndex > -1) {
				//Configuration was found. An attempt to create data will be made if config is good.
				var jsonSettingString = clientConfigurations[pageConfigIndex].Value;

				dynamic settingData = JObject.Parse(jsonSettingString);

				var jsonObject = settingData.additionalColumnId.ToString();
				var additionalCols = JsonConvert.DeserializeObject<List<int>>(jsonObject).ToArray();

				var hourIcs = new ItemColumns(instance, "WORKTIME_TRACKING_HOURS", authenticatedSession);

				foreach (var colId in additionalCols) {
					var col = hourIcs.GetItemColumn(colId);
					if (col.IsDatabasePrimitive()) {
						additionalSql += ", " + col.Name;
					} else if (col.IsSubquery()) {
						var colSql = col.GetColumnQuery(instance);
						additionalSql += ", " + colSql + " " + col.Name;
					}

					additionalCols2.Add(col);
				}
			}

			var sql1 = "SELECT u.item_id, " +
			           " first_name + ' ' + last_name AS name,  " +
			           " 	COALESCE(bt.item_id, t.item_id, th.item_id) as TaskNo,  " +
			           " COALESCE(bt.list_item, t.title, th.title, 'Unknown') as TaskTitle,'' AS owner_name, COALESCE(t.owner_item_id, th.owner_item_id) as owner_item_id, COALESCE(i.process,i2.process) AS owner_type,  pf.date as entryDate, " +
			           " l1.list_item AS location, l2.list_item AS breakdown," +
			           " CASE WHEN b.hours IS NULL THEN pf.hours ELSE b.hours END as hours, round(CASE WHEN b.hours IS NULL THEN pf.hours ELSE b.hours END /	(SELECT timesheet_hours FROM dbo.GetCalendarForUser(u.item_id)) , 2) as days, " +
			           " CASE WHEN b.additional_hours IS NULL THEN pf.additional_hours ELSE b.additional_hours END as additionalHours, round(CASE WHEN b.additional_hours IS NULL THEN pf.additional_hours ELSE b.additional_hours END/	(SELECT timesheet_hours FROM dbo.GetCalendarForUser(u.item_id)) , 2) as additionalDays " +
			           additionalSql +
			           " FROM  " +
			           "	_" + instance + "_user u " +
			           " inner join " + GetWorktimeTrackingHourTableName() + " pf on u.item_id = pf.user_item_id " +
			           "LEFT JOIN worktime_tracking_hours_breakdown b ON pf.item_id = b.worktime_tracking_hour_id " +
			           "left join _" + instance + "_list_wtr_location l1 ON b.comment_list_item_id = l1.item_id " +
			           "left join _" + instance +
			           "_worktime_tracking_breakdown_types l2 ON b.list_item_id = l2.item_id " +
			           " left join _" + instance +
			           "_worktime_tracking_basic_tasks bt on pf.task_item_id = bt.item_id " +
			           " left join _" + instance + "_task t on pf.task_item_id = t.item_id " +
			           " LEFT JOIN archive__" + instance +
			           "_task th on pf.task_item_id = th.item_id AND th.archive_id = (SELECT MAX(archive_id) FROM archive__" +
			           instance + "_task thh WHERE thh.item_id = pf.task_item_id) " +
			           " LEFT JOIN items i ON t.owner_item_id = i.item_id " +
			           " LEFT JOIN items_deleted i2 ON th.owner_item_id = i2.item_id " +
			           " WHERE  " +
			           " u.item_id = @userId AND pf.date BETWEEN @start AND @end ORDER BY name, entrydate";

			var sql0 = "SELECT u.item_id, " +
			           " first_name + ' ' + last_name AS name,  " +
			           " 	COALESCE(bt.item_id, t.item_id, th.item_id) as TaskNo,  " +
			           " COALESCE(bt.list_item, t.title, th.title, 'Unknown') as TaskTitle,'' AS owner_name, COALESCE(t.owner_item_id, th.owner_item_id) as owner_item_id, COALESCE(i.process,i2.process) AS owner_type,  pf.date as entryDate, " +
			           " l1.list_item AS location, l2.list_item AS breakdown," +
			           " CASE WHEN b.hours IS NULL THEN pf.hours ELSE b.hours END as hours, round(CASE WHEN b.hours IS NULL THEN pf.hours ELSE b.hours END /	(SELECT timesheet_hours FROM dbo.GetCalendarForUser(u.item_id)) , 2) as days, " +
			           " CASE WHEN b.additional_hours IS NULL THEN pf.additional_hours ELSE b.additional_hours END as additionalHours, round(CASE WHEN b.additional_hours IS NULL THEN pf.additional_hours ELSE b.additional_hours END/	(SELECT timesheet_hours FROM dbo.GetCalendarForUser(u.item_id)) , 2) as additionalDays " +
			           additionalSql +
			           " FROM  " +
			           " 	item_data_process_selections idps  " +
			           "	inner join _" + instance + "_user u on idps.selected_item_id = u.item_id " +
			           " inner join " + GetWorktimeTrackingHourTableName() + " pf on u.item_id = pf.user_item_id " +
			           "LEFT JOIN worktime_tracking_hours_breakdown b ON pf.item_id = b.worktime_tracking_hour_id " +
			           "left join _" + instance + "_list_wtr_location l1 ON b.comment_list_item_id = l1.item_id " +
			           "left join _" + instance +
			           "_worktime_tracking_breakdown_types l2 ON b.list_item_id = l2.item_id " +
			           " left join _" + instance +
			           "_worktime_tracking_basic_tasks bt on pf.task_item_id = bt.item_id " +
			           " left join _" + instance + "_task t on pf.task_item_id = t.item_id " +
			           " LEFT JOIN archive__" + instance +
			           "_task th on pf.task_item_id = th.item_id AND th.archive_id = (SELECT MAX(archive_id) FROM archive__" +
			           instance + "_task thh WHERE thh.item_id = pf.task_item_id) " +
			           " LEFT JOIN items i ON t.owner_item_id = i.item_id " +
			           " LEFT JOIN items_deleted i2 ON th.owner_item_id = i2.item_id " +
			           " WHERE  " +
			           "	item_column_id = ( " +
			           "		SELECT  " +
			           " item_column_id " +
			           "	FROM " +
			           " item_columns ic " +
			           " WHERE instance = @instance and process='user' and name = 'hour_approval_users')  " +
			           " AND idps.item_id = @userId AND pf.date BETWEEN @start AND @end ORDER BY name, entrydate";

			var sql2 = "SELECT u.item_id, " +
			           " first_name + ' ' + last_name AS name,  " +
			           " 	COALESCE(bt.item_id, t.item_id, th.item_id) as TaskNo,  " +
			           " COALESCE(bt.list_item, t.title, th.title, 'Unknown') as TaskTitle,'' AS owner_name, COALESCE(t.owner_item_id, th.owner_item_id) as owner_item_id, COALESCE(i.process,i2.process) AS owner_type,  pf.date as entryDate, " +
			           " l1.list_item AS location, l2.list_item AS breakdown," +
			           " CASE WHEN b.hours IS NULL THEN pf.hours ELSE b.hours END as hours, round(CASE WHEN b.hours IS NULL THEN pf.hours ELSE b.hours END/	(SELECT timesheet_hours FROM dbo.GetCalendarForUser(u.item_id)) , 2) as days, " +
			           " CASE WHEN b.additional_hours IS NULL THEN pf.additional_hours ELSE b.additional_hours END as additionalHours, round(CASE WHEN b.additional_hours IS NULL THEN pf.additional_hours ELSE b.additional_hours END/	(SELECT timesheet_hours FROM dbo.GetCalendarForUser(u.item_id)) , 2) as additionalDays " +
			           additionalSql +
			           " FROM  " +
			           "	_" + instance + "_user u " +
			           " INNER JOIN " + GetWorktimeTrackingHourTableName() + " pf on u.item_id = pf.user_item_id " +
			           "LEFT JOIN worktime_tracking_hours_breakdown b ON pf.item_id = b.worktime_tracking_hour_id " +
			           "left join _" + instance + "_list_wtr_location l1 ON b.comment_list_item_id = l1.item_id " +
			           "left join _" + instance +
			           "_worktime_tracking_breakdown_types l2 ON b.list_item_id = l2.item_id " +
			           " LEFT JOIN _" + instance +
			           "_worktime_tracking_basic_tasks bt on pf.task_item_id = bt.item_id " +
			           " LEFT JOIN _" + instance + "_task t on pf.task_item_id = t.item_id " +
			           " LEFT JOIN archive__" + instance +
			           "_task th on pf.task_item_id = th.item_id AND th.archive_id = (SELECT MAX(archive_id) FROM archive__" +
			           instance + "_task thh WHERE thh.item_id = pf.task_item_id) " +
			           " LEFT JOIN items i ON t.owner_item_id = i.item_id " +
			           " LEFT JOIN items_deleted i2 ON th.owner_item_id = i2.item_id " +
			           " WHERE  " +
			           " pf.date BETWEEN @start AND @end #ids# ORDER BY name, entrydate";

			var pidlimit = "";
			if (process_item_id > 0) pidlimit = " AND t.owner_item_id = " + process_item_id;

			var sql = sql1;
			if (v == 0) sql = sql0;
			if (v == 1) sql = sql1;
			if (v == 2) sql = sql2.Replace("#ids#", pidlimit);
			if (v == 3) sql = sql2.Replace("#ids#", " AND u.item_id IN (" + ids + ")" + pidlimit);

			SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			SetDBParam(SqlDbType.DateTime, "@start", start);
			SetDBParam(SqlDbType.DateTime, "@end", end);

			var pck = new ExcelPackage();
			var ws = pck.Workbook.Worksheets.Add("Timesheet");
			ws.View.ShowGridLines = true;
			ws.Cells[1, 1].Value = "User name";
			ws.Cells[1, 2].Value = "EntryDate";
			ws.Cells[1, 3].Value = "TaskNo";
			ws.Cells[1, 4].Value = "Task Title";
			ws.Cells[1, 5].Value = "Project number";
			ws.Cells[1, 6].Value = "Project serial";
			ws.Cells[1, 7].Value = "Project Title";
			ws.Cells[1, 8].Value = "Location";
			ws.Cells[1, 9].Value = "Breakdown";
			ws.Cells[1, 10].Value = "Hours";
			ws.Cells[1, 11].Value = "HoursInDays";
			ws.Cells[1, 12].Value = "AdditionalHours";
			ws.Cells[1, 13].Value = "AdditionalHoursInDays";

			var additionalColNum = 14;
			foreach (var additionalCol in additionalCols2) {
				ws.Cells[1, additionalColNum].Value = additionalCol.LanguageTranslation[authenticatedSession.locale_id];
				additionalColNum++;
			}

			var row = 2;

			var data = db.GetDatatable(sql, DBParameters);
			var processes = new Processes(authenticatedSession.instance, authenticatedSession);
			processes.AddPrimaryNameToDatatableColumn(data, "owner_name", "owner_item_id", "owner_type", true);

			var maxRecursion = 0;

			var distinctIds = new Dictionary<string, List<int>>();

			foreach (DataRow dr in data.Rows) {
				if (!DBNull.Value.Equals(dr["owner_type"])) {
					if (!distinctIds.ContainsKey(Conv.ToStr(dr["owner_type"]))) {
						var ics = new ItemColumns(instance, Conv.ToStr(dr["owner_type"]), authenticatedSession);
						if (ics.HasColumnName("serial")) {
							distinctIds.Add(Conv.ToStr(dr["owner_type"]), new List<int>());
						} else {
							distinctIds.Add(Conv.ToStr(dr["owner_type"]), null);
						}
					}

					if (distinctIds[Conv.ToStr(dr["owner_type"])] != null) {
						if (!distinctIds[Conv.ToStr(dr["owner_type"])].Contains(Conv.ToInt(dr["owner_item_id"]))) {
							distinctIds[Conv.ToStr(dr["owner_type"])].Add(Conv.ToInt(dr["owner_item_id"]));
						}
					}
				}
			}

			var distinctSerials = new Dictionary<int, string>();

			foreach (var pp in distinctIds.Keys) {
				var sql3 = " select item_id, COALESCE((select serial from _" + instance + "_" + pp +
				           " where item_id = a1.item_id), (select top 1 serial from archive__" + instance + "_" + pp +
				           " " +
				           " where item_id = a1.item_id order by archive_end desc)) as serial from " +
				           " (select item_id from _" + instance + "_" + pp + " where item_id in (" +
				           string.Join(",", distinctIds[pp].ToArray()) + ") " +
				           " union  " +
				           " select item_id from archive__" + instance + "_" + pp + " where item_id in (" +
				           string.Join(",", distinctIds[pp].ToArray()) + ") ) a1 ";
				foreach (DataRow serRow in db.GetDatatable(sql3).Rows) {
					distinctSerials.Add(Conv.ToInt(serRow["item_id"]), Conv.ToStr(serRow["serial"]));
				}
			}


			foreach (DataRow dr in data.Rows) {
				ws.Cells[row, 1].Value = dr["name"];
				ws.Cells[row, 2].Value = ConvertDate(Conv.ToDateTime(dr["entryDate"]));


				ws.Cells[row, 2].Value = GetExcelDecimalValueForDate((DateTime)Conv.ToDateTime(dr["entryDate"]));
				ws.Cells[row, 2].Style.Numberformat.Format = GetDateFormat();

				ws.Cells[row, 3].Value = dr["TaskNo"];
				ws.Cells[row, 4].Value = dr["TaskTitle"];
				ws.Cells[row, 5].Value = dr["owner_item_id"];

//				if (!DBNull.Value.Equals(dr["owner_item_id"])) {
//					var ibs = new ItemBase(instance, Conv.toStr(dr["owner_type"]), authenticatedSession);
//					var item = ibs.GetItem(Conv.toInt(dr["owner_item_id"]));
//					if (item.ContainsKey("serial")) {
//						ws.Cells[row, 6].Value = item["serial"];
//					} else {
//						ws.Cells[row, 6].Value = "";
//					}
//				} else {
//					ws.Cells[row, 6].Value = "";
//				}

				if (distinctSerials.ContainsKey(Conv.ToInt(dr["owner_item_id"]))) {
					ws.Cells[row, 6].Value = distinctSerials[Conv.ToInt(dr["owner_item_id"])];
				} else {
					ws.Cells[row, 6].Value = "";
				}


				ws.Cells[row, 7].Value = dr["owner_name"];

				ws.Cells[row, 8].Value = dr["location"];
				ws.Cells[row, 9].Value = dr["breakdown"];

				ws.Cells[row, 10].Value = dr["hours"];
				ws.Cells[row, 11].Value = dr["days"];
				ws.Cells[row, 12].Value = dr["additionalHours"];
				ws.Cells[row, 13].Value = dr["additionalDays"];

				additionalColNum = 14;
				foreach (var additionalCol in additionalCols2) {
					ws.Cells[row, additionalColNum].Value = dr[additionalCol.Name];
					additionalColNum++;
				}

				if (!DBNull.Value.Equals(dr["owner_item_id"])) {
					var recursion = 0;
					SetDBParam(SqlDbType.Int, "@taskId", dr["TaskNo"]);
					foreach (DataRow dr2 in db.GetDatatable("WITH rec AS ( " +
					                                        " SELECT t.parent_item_id, t.item_id, t.title " +
					                                        " FROM _" + instance + "_task  t  " +
					                                        " WHERE t.item_id =@taskId " +
					                                        " UNION ALL  " +
					                                        " SELECT  t.parent_item_id, t.item_id, t.title " +
					                                        " FROM _" + instance + "_task  t  " +
					                                        " INNER JOIN rec r ON r.parent_item_id = t.item_id) " +
					                                        " SELECT * FROM rec where item_id <> @taskId", DBParameters)
						.Rows) {
						ws.Cells[row, additionalColNum + recursion].Value = dr2["title"];
						ws.Cells[row, additionalColNum + 2 + recursion].Value = dr2["item_id"];
						recursion++;
					}

					if (recursion > maxRecursion) {
						maxRecursion = recursion;
					}
				}

				//Console.WriteLine("Excel row " + row);			    
				row++;
			}

			additionalColNum--;

			for (var i = 1; i < maxRecursion + 1; i++) {
				ws.Cells[1, additionalColNum + i].Value = "Parent Level " + i;
				ws.Cells[1, additionalColNum + i + 1].Value = "Parent Level Id " + i;
			}

			/*r.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			r.Headers.Add("content-Disposition", "inline;filename=excel.xlsx");*/
			return pck.GetAsByteArray();
			/*r.ContentLength = bArr.Length;
			r.Body.WriteAsync(bArr, 0, bArr.Length);*/
		}
	}
}