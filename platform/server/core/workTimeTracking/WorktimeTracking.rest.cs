using System;
using System.Collections.Generic;
using System.Globalization;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.calendar;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core.workTimeTracking {
	[Route("v1/time/[controller]")]
	public class WorktimeTracking : RestBase {
		// PUBIC METHODS

		// My Task List Handling
		[HttpGet("dashboard/{startTime}/{endTime}")]
		public Dictionary<string, List<WorktimeGraph>> GetLoadGraphs(string startTime, string endTime) {
			var wt = new WorktimeGraphs(instance, process, currentSession);
			return wt.GetLoadGraphData(startTime, endTime);
		}

		[HttpGet("myitems/{from}/{to}")]
		public IActionResult GetTasksWithHours(string from, string to) {
			var repository = GetTimeTrackingRepository();

			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			return new ObjectResult(repository.GetMyTasksWithHours(fromDate, toDate));
		}

		[HttpGet("{userId}/myitems/{from}/{to}")]
		public IActionResult GetTasksWithHours(int userId, string from, string to) {
			var repository = GetTimeTrackingRepository();

			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			return new ObjectResult(repository.GetMyTasksWithHours(fromDate, toDate, userId));
		}

		[HttpPost("myitems/copy")]
		public IActionResult CopyToToday([FromBody] WorktimeTrackingTaskCopy copyInstructions) {
			var repository = GetTimeTrackingRepository();
			return new ObjectResult(repository.CopyTimesheetBetweenDateRanges(copyInstructions));
		}

		[HttpPost("myitems/{userItemId}")]
		public IActionResult Post(int userItemId, [FromBody] WorktimeTrackingTaskListItem item) {
			item.AddedBy = currentSession.item_id;
			if (item.UserItemId == null) {
				item.UserItemId = userItemId;
			}

			if (item.TaskItemId == null) {
				throw new CustomArgumentException(ModelState);
			}

			var service = GetWorktimeTrackingService();
			service.AddItem(item);
			return new ObjectResult("");
		}

		[HttpDelete("myitems/{userItemId}/{taskId}")]
		public IActionResult RemoveTaskFromMyList(int userItemId, int taskId) {
			var service = GetWorktimeTrackingService();
			service.RemoveItem(userItemId, taskId);
			return new ObjectResult("");
		}

		[HttpGet("myopentasks/{userItemId}")]
		public IActionResult GetMyOpenTasks(int userItemId) {
			var tasks = GetTimeTrackingRepository().GetMyOpenTasks(userItemId);
			return new ObjectResult(tasks);
		}

		[HttpPost("myitems/{taskId}/description")]
		public IActionResult
			UpdateTaskDescription(int taskId, [FromBody] Dictionary<string, string> descriptionObject) {
			var service = GetWorktimeTrackingService();
			service.UpdateDescription(taskId, descriptionObject);
			return new ObjectResult("");
		}


		// Hour Updates
		[HttpPost("hours/{userItemId}")]
		public IActionResult PostHours(int userItemId, [FromBody] List<WorktimeTrackingTaskHours> hours) {
			var service = GetWorktimeTrackingService();
			service.SaveHours(userItemId, hours);
			return new ObjectResult("");
		}

		// Hour Approval

		[HttpPost("approve/{userItemId}")]
		// TODO: This should not be in clients hands. Server should decide which tasks can be marked approved without manager actions
		public IActionResult MarkApproved(int userItemId, [FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			if (approvalItems == null || approvalItems.Count == 0) return new ObjectResult("");
			var service = GetWorktimeTrackingService();
			service.MarkMyHoursAsApproved(userItemId, approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("reject/{userItemId}")]
		// TODO: This should not be in clients hands. Server should decide which tasks can be marked approved without manager actions
		public IActionResult MarkRejected(int userItemId, [FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			if (approvalItems == null || approvalItems.Count == 0) return new ObjectResult("");

			//Reject
			var service = GetWorktimeTrackingService();
			service.MarkMyHoursAsDraft(userItemId, approvalItems);

			//Send Email to Submitter
			var email = new Email(currentSession.instance, currentSession);
			var body =
				"Your timesheet submission has been rejected. Please go to Keto to edit and resubmit.<br><br>" +
				email.GetSystemLink(currentSession, "");
			var subject = "Timesheet Rejected";
			email.Add(userItemId, Email.AddressTypes.ToAdress);
			email.Send(subject, body);

			return new ObjectResult("");
		}

		[HttpPost("reset/{userItemId}")]
		// TODO: This should not be in clients hands. Server should decide which tasks can be marked approved without manager actions
		public IActionResult MarkReset(int userItemId, [FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			if (approvalItems == null || approvalItems.Count == 0) {
				return new ObjectResult("");
			}

			var service = GetWorktimeTrackingService();
			service.ResetHoursToSubmitted(userItemId, approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("submit/{userItemId}")]
		public IActionResult Submit(int userItemId, [FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			if (approvalItems == null || approvalItems.Count == 0) {
				return new ObjectResult("");
			}

			var service = GetWorktimeTrackingService();
			service.SubmitHours(userItemId, approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("submitpartial/{userItemId}")]
		public IActionResult SubmitPartial(int userItemId, [FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			if (approvalItems == null || approvalItems.Count == 0) {
				return new ObjectResult("");
			}

			var service = GetWorktimeTrackingService();
			service.SubmitHours(userItemId, approvalItems, true);
			return new ObjectResult("");
		}

		[HttpPost("revert/{userItemId}/{status}")]
		public IActionResult PostMarkDraft(int userItemId, double status,
			[FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			if (approvalItems == null || approvalItems.Count == 0) {
				return new ObjectResult("");
			}

			var service = GetWorktimeTrackingService();
			service.MarkMyHoursAsDraft(userItemId, approvalItems, status);
			return new ObjectResult("");
		}

		// STATUS
		[HttpGet("status/{from}/{to}/{all}")]
		public IActionResult GetUserStatus(string from, string to, int all) {
			var repository = GetTimeTrackingRepository();
			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			var useritemid = 0;
			var returnDict = new Dictionary<string, object>();
			List<WorkTimeTrackingSheetStatus> listOfItems;
			if (all == 0) useritemid = currentSession.item_id;
			if (all == 1) {
				listOfItems = repository.GetMyTimesheetStatuses(currentSession.item_id, fromDate, toDate);
			} else {
				listOfItems = repository.GetTimesheetStatuses(useritemid, fromDate, toDate);
			}

			returnDict.Add("items", listOfItems);
			returnDict.Add("myTeam", repository.MyTeamUserIds());
			return new ObjectResult(returnDict);
		}

		[HttpGet("status/{from}/{to}/{all}/{portfolioId}/{filterText}/{filters}")]
		public IActionResult GetUserStatus(string from, string to, int all, int portfolioId, string filterText,
			string filters) {
			filters = filters?.Replace("@", ";");
			var repository = GetTimeTrackingRepository();
			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			var useritemid = 0;
			var returnDict = new Dictionary<string, object>();
			List<WorkTimeTrackingSheetStatus> listOfItems;
			if (all == 0) useritemid = currentSession.item_id;
			if (all == 1) {
				listOfItems = repository.GetMyTimesheetStatuses(currentSession.item_id, fromDate, toDate);
			} else if (all == 3) {
				listOfItems =
					repository.GetTimesheetStatusesForFilters(fromDate, toDate, portfolioId, filterText, filters);
			} else {
				listOfItems = repository.GetTimesheetStatuses(useritemid, fromDate, toDate);
			}

			returnDict.Add("items", listOfItems);
			returnDict.Add("myTeam", repository.MyTeamUserIds());
			return new ObjectResult(returnDict);
		}

		[HttpGet("widget/{from}/{to}")]
		public IActionResult GetUserWidgetData(string from, string to) {
			var repository = GetTimeTrackingRepository();
			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			return new ObjectResult(repository.GetWidgetItems(currentSession.item_id, fromDate, toDate));
		}

		[HttpGet("total/{from}/{to}")]
		public IActionResult GetUserHourTotal(string from, string to) {
			var repository = GetTimeTrackingRepository();
			var fromDate = DayAndMonthToDateTime(from);
			var toDate = DayAndMonthToDateTime(to);
			var r = repository.GetHourTotal(currentSession.item_id, fromDate, toDate);
			var c = new CalendarDatas(instance, currentSession);
			var y = c.GetUserCalendarData(currentSession.item_id, Conv.ToStr(fromDate), Conv.ToStr(toDate));
			var result = new Dictionary<string, double>();
			result.Add("hours", r.Key);
			result.Add("additional_hours", r.Value);
			result.Add("total", y.WeeklyHours);

			return new ObjectResult(result);
		}

		[HttpGet("rebaseDate")]
		public DateTime RebaseDate() {
			var c = new Configurations(instance, currentSession);
			var v = c.GetClientConfiguration("WorktimeTracking", "rebaseDate");
			if (v != "") return DateTime.ParseExact(v, "dd.MM.yyyy", CultureInfo.CurrentCulture);
			return new DateTime(2000, 1, 1);
		}

		[HttpPost("rebaseDate")]
		public IActionResult Rebase([FromBody] DateTime d) {
			var c = new Configurations(instance, currentSession);
			c.SaveClientConfiguration("WorktimeTracking", "rebaseDate", d.ToString("dd.MM.yyyy"));

			return new StatusCodeResult(200);
		}

		[HttpPost("rebase")]
		public IActionResult Rebase([FromBody] EmailData data) {
			var overallresult = true;
			var repository = GetTimeTrackingRepository();
			if (data.users.Count > 0) {
				repository.Rebase(data.users);
			} else {
				overallresult = false;
			}

			return new ObjectResult(overallresult);
		}

		[HttpPost("sendemail")]
		public IActionResult SendEmail([FromBody] EmailData data) {
			if (data.users.Count == 0) return new ObjectResult("WORKINGTIME_TRACKING_EMAIL_RECIPIENTS");
			if (data.text == "") return new ObjectResult("WORKINGTIME_TRACKING_EMAIL_NO_BODY");

			data.text = data.text.Replace("\n", "<br/>");

			if (data.linemanager) {
				var repository = GetTimeTrackingRepository();
				var mail = new Email(instance, currentSession, data.text, "Timesheet Reminder");
				foreach (var id in data.users) {
					var manager = repository.GetMyManager(id);
					if (manager > 0) mail.Add(manager, Email.AddressTypes.ToAdress);
				}

				var result = mail.Send(async: false);
				if (result == false) return new ObjectResult("WORKINGTIME_TRACKING_EMAIL_FAIL");
			} else {
				foreach (var id in data.users) {
					var mail = new Email(instance, currentSession, data.text, "Timesheet Reminder");
					mail.Add(id, Email.AddressTypes.ToAdress);
					var result = mail.Send(async: false);
					if (result == false) return new ObjectResult("WORKINGTIME_TRACKING_EMAIL_FAIL");
				}
			}

			return new ObjectResult("WORKINGTIME_TRACKING_EMAIL_OK");
		}

		// LINE MANAGER
		[HttpPost("linemanager/approve")]
		public IActionResult PostApprovalAsLineManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.ApproveHoursAsLineManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("linemanager/reject")]
		public IActionResult PostRejectAsLineManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.RejectHoursAsLineManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("linemanager/revert")]
		public IActionResult PostRevertAsLineManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.RevertHoursAsLineManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("linemanager/restoreconfirmed")]
		public IActionResult PostRestoreReadyAsLineManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.RestoreReadyAsLineManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpGet("linemanager/approvable/{from}/{to}")]
		public IActionResult GetItemsToApproveAsLineManager(string from, string to) {
			var fromDate = DateTime.ParseExact(from, "ddMMyyyy", CultureInfo.InvariantCulture);
			var toDate = DateTime.ParseExact(to, "ddMMyyyy", CultureInfo.InvariantCulture);

			var service = GetWorktimeTrackingService();
			return new ObjectResult(service.GetItemsToApproveAsLineManager(fromDate, toDate));
		}


		// PROJECT MANAGER
		[HttpGet("projectmanager/{projectId}/{from}/{to}")]
		public IActionResult GetItemsToApproveAsProjectManager(int projectId, string from, string to) {
			var fromDate = DateTime.ParseExact(from, "ddMMyyyy", CultureInfo.InvariantCulture);
			var toDate = DateTime.ParseExact(to, "ddMMyyyy", CultureInfo.InvariantCulture);

			var service = GetWorktimeTrackingService();
			return new ObjectResult(service.GetItemsToApproveAsProjectManager(projectId, fromDate, toDate));
		}

		[HttpPost("projectmanager/approve")]
		public IActionResult PostApprovalAsProjectManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.ApproveHoursAsProjectManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("projectmanager/reject")]
		public IActionResult PostRejectAsProjectManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.RejectHoursAsProjectManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("projectmanager/revert")]
		public IActionResult PostRevertAsProjectManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.RevertHoursAsProjectManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpPost("projectmanager/confirm")]
		public IActionResult PostRestoreReadyAsProjectManager([FromBody] List<WorktimeHourApprovalItem> approvalItems) {
			var service = GetWorktimeTrackingService();
			service.RestoreReadyAsProjectManager(approvalItems);
			return new ObjectResult("");
		}

		[HttpGet("projectmanager/approvable/{from}/{to}")]
		public IActionResult GetItemsToApproveAsProjectManager(string from, string to) {
			var fromDate = DateTime.ParseExact(from, "ddMMyyyy", CultureInfo.InvariantCulture);
			var toDate = DateTime.ParseExact(to, "ddMMyyyy", CultureInfo.InvariantCulture);

			var service = GetWorktimeTrackingService();
			return new ObjectResult(service.GetItemsToApproveAsProjectManager(0, fromDate, toDate));
		}

		private WorktimeTrackingService GetWorktimeTrackingService() {
			var service = new WorktimeTrackingService(GetTimeTrackingRepository(), currentSession);
			return service;
		}
		// PRIVATE METHODS

		private WorktimeTrackingRepository GetTimeTrackingRepository() {
			var cb = new ConnectionBase(instance, currentSession);
			var repository = new WorktimeTrackingRepository(currentSession, cb);
			return repository;
		}

		private static DateTime DayAndMonthToDateTime(string dateString) {
			if (string.IsNullOrEmpty(dateString)) {
				throw new CustomArgumentException("Date parameter is empty or null " + dateString);
			}

			try {
				return DateTime.ParseExact(dateString, "ddMMyyyy", CultureInfo.InvariantCulture);
			} catch (FormatException) {
				throw new CustomArgumentException("Can't convert " + dateString + " to DateTime");
			}
		}

		public class EmailData {
			public bool linemanager;
			public string text;
			public List<int> users;
		}
	}
}