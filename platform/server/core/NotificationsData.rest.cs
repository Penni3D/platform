﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/settings/[controller]")]
	public class NotificationsData : RestBase {

		[HttpGet("inuse/{instance}")]
		public List<int> GetInUseIds(string instance) {
			var p = new Notifications(instance, currentSession);
			return p.GetInUseNotificationIds(instance);
		}

		[HttpGet("bulletins/{pFolioId}")]
		public Dictionary<string, object> GetBulletins(int pFolioId) {
			var p = new Notifications(instance, currentSession);
			return p.GetBulletins(pFolioId);
		}

		[HttpPost("emaillogs/{instance}")]
		public List<Email.InstanceEmailLog>
			GetEmailLogs(string instance, [FromBody] Email.InstanceEmailFilter filters) {
			var e = new Email(instance, currentSession);
			return e.GetEmailLogs(instance, filters);
		}

		[HttpGet("actionconnections/{instance}")]
		public Dictionary<int, List<Notifications.NotificationActionPair>> GetMailConnections(string instance) {
			var p = new Notifications(instance, currentSession);
			return p.ActionNotificationConnections(instance);
		}

		[HttpGet("nocondconnections/{instance}")]
		public Dictionary<int, List<Notifications.NotificationCondPair>> GetNotiConConnections(string instance) {
			var p = new Notifications(instance, currentSession);
			return p.NotiCondNotificationConnections(instance);
		}

		[HttpGet("{jsonTagId}/{process}")]
		public List<Dictionary<string, object>> Get(int jsonTagId, string process) {
			var p = new ItemBase(instance, "notification_tag_links", currentSession);
			var search = new Dictionary<string, object>();
			search.Add("json_tag_id", jsonTagId);
			search.Add("process_name", process);

			var retval = new Dictionary<string, object>();
			var data = p.GetItems(search);
			return p.GetItems(search);
		}

		[HttpGet("{jsonTagId}")]
		public List<Dictionary<string, object>> Get(int jsonTagId) {
			var p = new ItemBase(instance, "notification_tag_links", currentSession);
			var search = new Dictionary<string, object>();
			search.Add("json_tag_id", jsonTagId);

			var retval = new Dictionary<string, object>();
			var data = p.GetItems(search);
			return p.GetItems(search);
		}

		[HttpPost("{jsonTagId}")]
		public Dictionary<string, object> Post(int jsonTagId) {
			var ib = new ItemBase(instance, "notification_tag_links", currentSession);

			var retval = new Dictionary<string, object>();
			var newId = ib.InsertItemRow();
			var data2 = ib.GetItem(newId);
			data2["json_tag_id"] = jsonTagId;
			return ib.SaveItem(data2);
		}

		[HttpPut("{jsonTagId}")]
		public void Put(int jsonTagId, [FromBody] List<Dictionary<string, object>> data) {
			var ib = new ItemBase(instance, "notification_tag_links", currentSession);
			foreach (var key in data) {
				ib.SaveItem(key);
			}
		}

		[HttpDelete("{jsonTagId}")]
		public void Delete(string jsonTagId) {
			var ib = new ItemBase(instance, "notification_tag_links", currentSession);
			foreach (var id in jsonTagId.Split(',')) {
				ib.Delete(Conv.ToInt(id));
			}
		}

		[HttpPost("httpRequests")]
		public void DeleteHttpRequests([FromBody] List<int> idsToDelete) {
			var ib = new ItemBase(instance, "notification", currentSession);
			foreach (var id in idsToDelete) {
				ib.Delete(id);
			}
		}

		[HttpGet("userNotifications/{portfolioId}")]
		public Dictionary<string, object> getUserNotifications(int portfolioId) {

			var q = new ItemBase(instance, "notification", currentSession);
			var itemColumns = new ItemColumns(instance, "notification", currentSession);
			var filters = new PortfolioOptions();
			filters.filters = new Dictionary<int, object>();

			var userItemColumnId = itemColumns.GetItemColumnByName("user_item_id").ItemColumnId;
			var t = new List<string>();
			t.Add(Conv.ToStr(currentSession.item_id));

			filters.filters.Add(userItemColumnId, t);

			return q.FindItems(portfolioId, filters);

		}
	}
}