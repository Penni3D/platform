﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core {
	public class Condition {
		public Condition() { }

		public Condition(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ConditionId = (int) row["condition_id"];
			Process = (string) row["process"];
			Variable = (string) row["variable"];
			Disabled = Conv.ToBool(Conv.ToInt(row["disabled"]));

			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);

			try {
				ConditionJSON =
					JsonConvert.DeserializeObject<List<ConditionContent>>((string) row["condition_json"]);
			} catch (Exception ex) {
				throw new CustomArgumentException("Condition parse error. (" + ConditionId + ", " + ") - " + ex.Message);
			}

			OrderNo = Conv.ToStr(row["order_no"]);
		}

		public bool Disabled { get; set; }

		public int ConditionId { get; set; }
		public string Process { get; set; }
		public string Variable { get; set; }

		private List<ConditionContent> _conditionJSON;
		private List<ConditionContent> _conditionJSONEnabled;

		/// <summary>
		/// Returns all enabled and disabled Condition Content for Saving on the server
		/// </summary>
		[JsonIgnore]
		public List<ConditionContent> ConditionJSONComplete {
			get {
				if (_conditionJSON == null) return null;
				if (_conditionJSON.Count == 0) return null;
				return _conditionJSON;
			}
		}

		/// <summary>
		/// Sets and Gets conditions to and from the client
		/// </summary>
		public List<ConditionContent> ConditionJSON {
			get {
				if (_conditionJSON == null) return null;
				if (_conditionJSON.Count == 0) return null;
				return _conditionJSON;
			}
			set {
				_conditionJSON = value;
				_conditionJSONEnabled = null;
			}
		}

		/// <summary>
		/// Gets all enabled conditions for server's condition evaluations
		/// </summary>
		[JsonIgnore]
		public List<ConditionContent> ConditionJSONServer {
			get {
				if (_conditionJSON == null) return null;
				if (_conditionJSONEnabled == null) {
					_conditionJSONEnabled = new List<ConditionContent>();
					foreach (var cc in _conditionJSON) {
						if (cc.Disabled == false) _conditionJSONEnabled.Add(cc);
					}
				}

				if (_conditionJSONEnabled.Count == 0) return null;
				return _conditionJSONEnabled;
			}
		}

		public List<string> Features { get; set; }
		public List<string> States { get; set; }
		public List<int> ProcessPortfolioIds { get; set; }
		public List<int> ProcessGroupIds { get; set; }
		public List<int> ProcessTabIds { get; set; }
		public List<int> ProcessContainerIds { get; set; }
		public List<int> ItemIds { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public string OrderNo { get; set; }
		public int ConditionGroupId { get; set; }
	}

	public class ConditionsGroup {
		public ConditionsGroup() { }

		public ConditionsGroup(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) alias = alias + "_";
			ConditionGroupId = (int) row["condition_group_id"];
			Process = (string) row["process"];
			Variable = (string) row["variable"];
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
			OrderNo = Conv.ToStr(row["order_no"]);
		}

		public int ConditionGroupId { get; set; }
		public string Process { get; set; }
		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public string OrderNo { get; set; }
	}

	public class Conditions : ProcessBase {
		private string inc(bool includeDisabled) {
			if (includeDisabled) return "";
			return " AND disabled = 0";
		}

		public Conditions(string instance, string process, AuthenticatedSession authenticatedSession,
			bool includeDisabled = false) : base(instance,
			process, authenticatedSession) {
			if (Cache.Get(instance, "c_dts_" + process) == null) {
				var dts = new Dictionary<string, DataTable>();
				dts.Add("conditions",
					db.GetDatatable(
						"SELECT * FROM conditions WHERE instance = @instance and process = @process " +
						inc(includeDisabled),
						DBParameters));
				dts.Add("condition_features",
					db.GetDatatable(
						"SELECT condition_id, feature FROM condition_features where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));
				dts.Add("condition_states",
					db.GetDatatable(
						"SELECT condition_id, state FROM condition_states where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));
				dts.Add("condition_portfolios",
					db.GetDatatable(
						"SELECT condition_id, process_portfolio_id FROM condition_portfolios where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));
				dts.Add("condition_groups",
					db.GetDatatable(
						"SELECT condition_id, process_group_id FROM condition_groups where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));
				dts.Add("condition_tabs",
					db.GetDatatable(
						"SELECT condition_id, process_tab_id FROM condition_tabs where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));
				dts.Add("condition_containers",
					db.GetDatatable(
						"SELECT condition_id, process_container_id FROM condition_containers where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));
				dts.Add("condition_user_groups",
					db.GetDatatable(
						"SELECT condition_id, item_id FROM condition_user_groups where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));
				dts.Add("conditions_groups_condition",
					db.GetDatatable(
						"SELECT condition_id, condition_group_id FROM conditions_groups_condition where condition_id in (select condition_id from conditions where instance = @instance and process = @process)",
						DBParameters));

				Cache.Set(instance, "c_dts_" + process, dts);
			}
		}


//		public List<Condition> GetConditions(bool includeDisabled = false) {
//			var result = new List<Condition>();
//
//			DataTable rows;
//			if (Cache.Get(instance, "c_dts_" + process) != null) {
//				var dts = (Dictionary<string, DataTable>) Cache.Get(instance, "c_dts_" + process);
//				rows = dts["conditions"];
//			} else {
//				rows = db.GetDatatable(
//					"SELECT condition_id, process, variable, condition_json, order_no FROM conditions WHERE instance = @instance " + inc(includeDisabled) + " ORDER BY order_no  ASC",
//					DBParameters);
//			}
//
//			foreach (DataRow row in rows.Rows) {
//				var condition = new Condition(row, authenticatedSession);
//				result.Add(condition);
//			}
//
//			return result;
//		}

		public List<Condition> GetConditionsForItemColumn(int itemColumnId, bool includeDisabled = false) {
			var result = new List<Condition>();
			DataTable rows;
			rows = db.GetDatatable(
				"SELECT * FROM conditions WHERE instance = @instance AND condition_json IS NOT NULL AND condition_json <> 'null' " +
				inc(includeDisabled) + " ORDER BY order_no  ASC",
				DBParameters);
			foreach (DataRow row in rows.Rows) {
				var c = new Condition(row, authenticatedSession);
				foreach (var c1 in c.ConditionJSONServer) {
					foreach (var o in c1.OperationGroup.Operations) {
						try {
							if (o != null) {
								dynamic condition = o.Condition;
								if (condition.ItemColumnId != null && condition.ItemColumnId.Value == itemColumnId) {
									if (!result.Contains(c)) {
										result.Add(c);
									}
								}

								if (condition.ItemColumnId != null &&
								    condition.UserItemColumnId.Value == itemColumnId) {
									if (!result.Contains(c)) {
										result.Add(c);
									}
								}

								if (condition.ItemColumnId != null &&
								    condition.ParentItemColumnId.Value == itemColumnId) {
									if (!result.Contains(c)) {
										result.Add(c);
									}
								}
							}
						} catch (Exception e) {
							Console.WriteLine(e.Message);
						}
					}
				}
			}

			return result;
		}

		public List<Condition> GetConditions(string process, bool includeDisabled = false) {
			if (Cache.Get(instance, "c_" + process) != null) {
				return (List<Condition>) Cache.Get(instance, "c_" + process);
			}

			var start = DateTime.Now.ToUniversalTime();
			var span = DateTime.Now.ToUniversalTime() - start;


			var result = new List<Condition>();
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT condition_id, process, variable, condition_json, order_no, disabled FROM conditions WHERE instance = @instance AND process = @process " +
					inc(includeDisabled) + " ORDER BY order_no  ASC",
					DBParameters).Rows) {
				var condition = new Condition(row, authenticatedSession);

				condition.Features = GetStringList("condition_features", "feature", (int) row["condition_id"]);
				condition.States = GetStringList("condition_states", "state", (int) row["condition_id"]);
				condition.ProcessPortfolioIds =
					GetIdList("condition_portfolios", "process_portfolio_id", (int) row["condition_id"]);
				condition.ProcessGroupIds =
					GetIdList("condition_groups", "process_group_id", (int) row["condition_id"]);
				condition.ProcessTabIds = GetIdList("condition_tabs", "process_tab_id", (int) row["condition_id"]);
				condition.ProcessContainerIds =
					GetIdList("condition_containers", "process_container_id", (int) row["condition_id"]);
				condition.ItemIds = GetIdList("condition_user_groups", "item_id", (int) row["condition_id"]);
				condition.ConditionGroupId =
					GetIdInt("conditions_groups_condition", "condition_group_id", (int) row["condition_id"]);
				result.Add(condition);
			}

			Cache.Set(instance, "c_" + process, result);
			return result;
		}

		public List<Condition> GetConditionsForUserGroup(string process, int itemId, bool includeDisabled = false) {
			var result = new List<Condition>();
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			foreach (DataRow row in db.GetDatatable(
				"SELECT c.condition_id, process, variable, condition_json, order_no, disabled FROM conditions c INNER JOIN condition_user_groups cug ON c.condition_id = cug.condition_id WHERE instance = @instance AND process = @process AND item_id = @itemId " +
				inc(includeDisabled),
				DBParameters).Rows) {
				var condition = new Condition(row, authenticatedSession);
				condition.Features = GetStringList("condition_features", "feature", (int) row["condition_id"]);
				condition.States = GetStringList("condition_states", "state", (int) row["condition_id"]);
				condition.ProcessPortfolioIds =
					GetIdList("condition_portfolios", "process_portfolio_id", (int) row["condition_id"]);
				condition.ProcessGroupIds =
					GetIdList("condition_groups", "process_group_id", (int) row["condition_id"]);
				condition.ProcessTabIds = GetIdList("condition_tabs", "process_tab_id", (int) row["condition_id"]);
				condition.ProcessContainerIds =
					GetIdList("condition_containers", "process_container_id", (int) row["condition_id"]);
				condition.ItemIds = GetIdList("condition_user_groups", "item_id", (int) row["condition_id"]);
				result.Add(condition);
			}

			return result;
		}

		public Dictionary<string, object> GetIdsForPhases(string process) {
			var returnValue = new Dictionary<string, object>();
			//Dictionary<int, List<int>> phaseIds = new Dictionary<int, List<int>>();
			var tabIds = new Dictionary<int, List<int>>();
			var containerIds = new Dictionary<int, List<int>>();

			SetDBParam(SqlDbType.VarChar, "@process", process);

			var getIdsSql =
				"SELECT ISNULL(pgt.process_tab_id, 0) as process_tab_id, ISNULL(pgc.process_container_id, 0) as process_container_id, ISNULL(process_groups.process_group_id, 0) as process_group_id FROM process_groups " +
				"LEFT JOIN process_group_tabs pgt ON process_groups.process_group_id = pgt.process_group_id " +
				"LEFT JOIN process_tab_containers pgc ON pgt.process_tab_id = pgc.process_tab_id " +
				"WHERE process_groups.process = @process";


			foreach (DataRow row in db.GetDatatable(getIdsSql, DBParameters).Rows) {
				if (tabIds.ContainsKey(Conv.ToInt(row["process_group_id"]))) {
					if (!tabIds[Conv.ToInt(row["process_group_id"])].Contains(Conv.ToInt(row["process_tab_id"]))) {
						tabIds[Conv.ToInt(row["process_group_id"])].Add(Conv.ToInt(row["process_tab_id"]));
					}
				} else {
					tabIds[Conv.ToInt(row["process_group_id"])] = new List<int>();
					tabIds[Conv.ToInt(row["process_group_id"])].Add(Conv.ToInt(row["process_tab_id"]));
				}

				if (containerIds.ContainsKey(Conv.ToInt(row["process_tab_id"]))) {
					if (!containerIds[Conv.ToInt(row["process_tab_id"])]
						.Contains(Conv.ToInt(row["process_container_id"]))) {
						containerIds[Conv.ToInt(row["process_tab_id"])].Add(Conv.ToInt(row["process_container_id"]));
					}
				} else {
					containerIds[Conv.ToInt(row["process_tab_id"])] = new List<int>();
					containerIds[Conv.ToInt(row["process_tab_id"])].Add(Conv.ToInt(row["process_container_id"]));
				}
			}

			returnValue.Add("tabIds", tabIds);
			returnValue.Add("containerIds", containerIds);
			return returnValue;
		}

		public DataTable GetConditionHierarchy(string process) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var sql =
				"SELECT pg.process_group_id, pg.variable as phase_name, pt.process_tab_id as tab_id, pt.variable as tab_name, pc.process_container_id as container_id, pc.variable as container_name FROM process_groups pg " +
				"LEFT JOIN process_group_tabs pgt ON pg.process_group_id = pgt.process_group_id " +
				"LEFT JOIN process_tabs pt ON pt.process_tab_id = pgt.process_tab_id " +
				"LEFT JOIN process_tab_containers ptc ON pt.process_tab_id = ptc.process_tab_id " +
				"LEFT JOIN process_containers pc ON ptc.process_container_id = pc.process_container_id " +
				"WHERE pg.process = @process";
			return db.GetDatatable(sql, DBParameters);
		}

		public List<Condition> GetConditions(string process, string feature, string state,
			bool includeDisabled = false) {
			if (Cache.Get(instance, "s_" + process + "_" + feature + "_" + state) != null) {
				return (List<Condition>) Cache.Get(instance, "s_" + process + "_" + feature + "_" + state);
			}

			var result = new List<Condition>();
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.NVarChar, "@feature", feature);
			SetDBParam(SqlDbType.NVarChar, "@state", state);


			var rows = db.GetDatatable(
				"SELECT c.condition_id, process, variable, condition_json, order_no, disabled FROM conditions c INNER JOIN condition_features cf ON c.condition_id = cf.condition_id INNER JOIN condition_states cs ON c.condition_id = cs.condition_id WHERE instance = @instance AND process = @process AND feature=@feature AND state = @state " +
				inc(includeDisabled) + " ORDER BY state",
				DBParameters);


			foreach (DataRow row in rows.Rows) {
				var condition = new Condition(row, authenticatedSession);
				condition.Features = GetStringList("condition_features", "feature", (int) row["condition_id"]);
				condition.States = GetStringList("condition_states", "state", (int) row["condition_id"]);
				condition.ProcessPortfolioIds =
					GetIdList("condition_portfolios", "process_portfolio_id", (int) row["condition_id"]);
				condition.ProcessGroupIds =
					GetIdList("condition_groups", "process_group_id", (int) row["condition_id"]);
				condition.ProcessTabIds = GetIdList("condition_tabs", "process_tab_id", (int) row["condition_id"]);
				condition.ProcessContainerIds =
					GetIdList("condition_containers", "process_container_id", (int) row["condition_id"]);
				condition.ItemIds = GetIdList("condition_user_groups", "item_id", (int) row["condition_id"]);
				result.Add(condition);
			}

			Cache.Set(instance, "s_" + process + "_" + feature + "_" + state, result);
			return result;
		}

		public List<Condition> GetConditionsForGroup(int processGroupId, bool includeDisabled = false) {
			var result = new List<Condition>();
			SetDBParam(SqlDbType.Int, "@processGroupId", processGroupId);
			foreach (DataRow row in db.GetDatatable(
				"SELECT c.condition_id, process, variable, condition_json, order_no FROM conditions c INNER JOIN condition_groups cg ON c.condition_id = cg.condition_id WHERE instance = @instance AND process_group_id = @processGroupId " +
				inc(includeDisabled),
				DBParameters).Rows) {
				var condition = new Condition(row, authenticatedSession);
				condition.Features = GetStringList("condition_features", "feature", (int) row["condition_id"]);
				condition.States = GetStringList("condition_states", "state", (int) row["condition_id"]);
				condition.ProcessPortfolioIds =
					GetIdList("condition_portfolios", "process_portfolio_id", (int) row["condition_id"]);
				condition.ProcessGroupIds =
					GetIdList("condition_groups", "process_group_id", (int) row["condition_id"]);
				condition.ProcessTabIds = GetIdList("condition_tabs", "process_tab_id", (int) row["condition_id"]);
				condition.ProcessContainerIds =
					GetIdList("condition_containers", "process_container_id", (int) row["condition_id"]);
				condition.ItemIds = GetIdList("condition_user_groups", "item_id", (int) row["condition_id"]);
				result.Add(condition);
			}

			return result;
		}

		public List<Condition> GetConditionsForTab(int processTabId, bool includeDisabled = false) {
			var result = new List<Condition>();
			SetDBParam(SqlDbType.Int, "@processTabId", processTabId);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT c.condition_id, process, variable, condition_json, order_no FROM conditions c INNER JOIN condition_tabs ct ON c.condition_id = ct.condition_id WHERE instance = @instance AND process_tab_id = @processTabId " +
					inc(includeDisabled),
					DBParameters).Rows) {
				var condition = new Condition(row, authenticatedSession);
				condition.Features = GetStringList("condition_features", "feature", (int) row["condition_id"]);
				condition.States = GetStringList("condition_states", "state", (int) row["condition_id"]);
				condition.ProcessPortfolioIds =
					GetIdList("condition_portfolios", "process_portfolio_id", (int) row["condition_id"]);
				condition.ProcessGroupIds =
					GetIdList("condition_groups", "process_group_id", (int) row["condition_id"]);
				condition.ProcessTabIds = GetIdList("condition_tabs", "process_tab_id", (int) row["condition_id"]);
				condition.ProcessContainerIds =
					GetIdList("condition_containers", "process_container_id", (int) row["condition_id"]);
				condition.ItemIds = GetIdList("condition_user_groups", "item_id", (int) row["condition_id"]);
				result.Add(condition);
			}

			return result;
		}

		public List<Condition> GetConditionsForContainer(int processContainerId, bool includeDisabled = false) {
			var result = new List<Condition>();
			SetDBParam(SqlDbType.Int, "@processContainerId", processContainerId);
			foreach (DataRow row in db.GetDatatable(
				"SELECT c.condition_id, process, variable, condition_json, order_no FROM conditions c INNER JOIN condition_containers cc ON c.condition_id = cc.condition_id WHERE instance = @instance AND process_container_id = @processContainerId " +
				inc(includeDisabled),
				DBParameters).Rows) {
				var condition = new Condition(row, authenticatedSession);
				condition.Features = GetStringList("condition_features", "feature", (int) row["condition_id"]);
				condition.States = GetStringList("condition_states", "state", (int) row["condition_id"]);
				condition.ProcessPortfolioIds =
					GetIdList("condition_portfolios", "process_portfolio_id", (int) row["condition_id"]);
				condition.ProcessGroupIds =
					GetIdList("condition_groups", "process_group_id", (int) row["condition_id"]);
				condition.ProcessTabIds = GetIdList("condition_tabs", "process_tab_id", (int) row["condition_id"]);
				condition.ProcessContainerIds =
					GetIdList("condition_containers", "process_container_id", (int) row["condition_id"]);
				condition.ItemIds = GetIdList("condition_user_groups", "item_id", (int) row["condition_id"]);
				result.Add(condition);
			}

			return result;
		}

		public Condition GetCondition(int conditionId, bool includeDisabled = false, bool supressNotFoundError = false) {
			if (Cache.Get(instance, "condition_id_" + conditionId) != null) {
				return (Condition) Cache.Get(instance, "condition_id_" + conditionId);
			}

			DataRow row = null;
			if (!includeDisabled && Cache.Get(instance, "c_dts_" + process) != null) {
				var dts = (Dictionary<string, DataTable>) Cache.Get(instance, "c_dts_" + process);
				var rows = dts["conditions"].Select("condition_id = " + conditionId);

				if (rows.Length > 0) {
					row = rows[0];
				}
			} else {
				SetDBParam(SqlDbType.Int, "@conditionId", conditionId);
				var dt =
					db.GetDatatable(
						"SELECT condition_id, process, variable, condition_json, order_no, disabled FROM conditions WHERE instance = @instance AND condition_id = @conditionId " +
						inc(includeDisabled),
						DBParameters);
				if (dt.Rows.Count > 0) {
					row = dt.Rows[0];
				}
			}


			if (row != null) {
				var c = new Condition(row, authenticatedSession);
				c.Features = GetStringList("condition_features", "feature", (int) row["condition_id"]);
				c.States = GetStringList("condition_states", "state", (int) row["condition_id"]);
				c.ProcessPortfolioIds =
					GetIdList("condition_portfolios", "process_portfolio_id", (int) row["condition_id"]);
				c.ProcessGroupIds = GetIdList("condition_groups", "process_group_id", (int) row["condition_id"]);
				c.ProcessTabIds = GetIdList("condition_tabs", "process_tab_id", (int) row["condition_id"]);
				c.ProcessContainerIds =
					GetIdList("condition_containers", "process_container_id", (int) row["condition_id"]);
				c.ItemIds = GetIdList("condition_user_groups", "item_id", (int) row["condition_id"]);

				Cache.Set(instance, "condition_id_" + conditionId, c);

				return c;
			}

			if (!supressNotFoundError) throw new CustomArgumentException("Condition Cannot be found with condition id " + conditionId);
			return null;
		}

		public Condition Add(Condition condition) {
			SetDBParam(SqlDbType.NVarChar, "@process", condition.Process);
			//SetDBParam(SqlDbType.NVarChar, "@conditionJSON", "null");
			SetDBParam(SqlDbType.NVarChar, "@conditionJSON",
				JsonConvert.SerializeObject(condition.ConditionJSONServer));

			if (condition.OrderNo == null) {
				condition.ConditionId =
					db.ExecuteInsertQuery(
						"INSERT INTO conditions (instance, process, condition_json, order_no) VALUES(@instance, @process, @conditionJSON, ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM conditions WHERE process = @process " +
						"ORDER BY order_no  DESC),null)),'U'))",
						DBParameters);
			} else {
				SetDBParam(SqlDbType.NVarChar, "@orderNo", condition.OrderNo);
				condition.ConditionId =
					db.ExecuteInsertQuery(
						"INSERT INTO conditions (instance, process, condition_json, order_no) VALUES(@instance, @process, @conditionJSON, @orderNo)",
						DBParameters);
			}

			UpdateStringList("condition_features", "feature", condition.ConditionId, condition.Features);
			UpdateStringList("condition_states", "state", condition.ConditionId, condition.States);
			UpdateIdList("condition_portfolios", "process_portfolio_id", condition.ConditionId,
				condition.ProcessPortfolioIds);
			UpdateIdList("condition_groups", "process_group_id", condition.ConditionId, condition.ProcessGroupIds);
			UpdateIdList("condition_tabs", "process_tab_id", condition.ConditionId, condition.ProcessTabIds);
			UpdateIdList("condition_containers", "process_container_id", condition.ConditionId,
				condition.ProcessContainerIds);
			UpdateIdList("condition_user_groups", "item_id", condition.ConditionId, condition.ItemIds);

			SetDBParam(SqlDbType.Int, "@ConId", condition.ConditionId);
			SetDBParam(SqlDbType.Int, "@ConditionGroupId", condition.ConditionGroupId);
			db.ExecuteInsertQuery(
				"INSERT INTO conditions_groups_condition (condition_group_id, condition_id) VALUES(@ConditionGroupId, @ConId)",
				DBParameters);

			if (condition.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var variable = translations.GetTranslationVariable(process,
					condition.LanguageTranslation[authenticatedSession.locale_id], "CONDITION");
				condition.Variable = variable;

				SetDBParam(SqlDbType.NVarChar, "@variable", condition.Variable);
				SetDBParam(SqlDbType.Int, "@Ci", condition.ConditionId);
				db.ExecuteUpdateQuery(
					"UPDATE conditions SET variable = @variable WHERE instance = @instance AND process = @process AND condition_id = @Ci",
					DBParameters);
				foreach (var translation in condition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, variable, translation.Value,
						translation.Key);
				}
			}

			Cache.Remove(instance, "c_" + condition.Process);
			Cache.Remove(instance, "c_dts_" + condition.Process);
			Cache.Remove(instance, "condition_id_" + condition.ConditionId);

			return condition;
		}

		public Condition Copy(Condition condition) {
			SetDBParam(SqlDbType.Int, "@conditionId", condition.ConditionId);
			SetDBParam(SqlDbType.NVarChar, "@conditionJSON",
				JsonConvert.SerializeObject(condition.ConditionJSONServer));
			SetDBParam(SqlDbType.NVarChar, "@orderNo", condition.OrderNo);
			condition.ConditionId =
				db.ExecuteInsertQuery(
					"INSERT INTO conditions (instance, process, condition_json, order_no) VALUES(@instance, @process, @conditionJSON, @orderNo)",
					DBParameters);

			SetDBParam(SqlDbType.Int, "@ConId", condition.ConditionId);
			SetDBParam(SqlDbType.Int, "@ConditionGroupId", condition.ConditionGroupId);

			db.ExecuteInsertQuery(
				"INSERT INTO conditions_groups_condition (condition_group_id, condition_id) VALUES(@ConditionGroupId, @ConId)",
				DBParameters);

			UpdateStringList("condition_features", "feature", condition.ConditionId, condition.Features);
			UpdateStringList("condition_states", "state", condition.ConditionId, condition.States);
			UpdateIdList("condition_portfolios", "process_portfolio_id", condition.ConditionId,
				condition.ProcessPortfolioIds);
			UpdateIdList("condition_groups", "process_group_id", condition.ConditionId, condition.ProcessGroupIds);
			UpdateIdList("condition_tabs", "process_tab_id", condition.ConditionId, condition.ProcessTabIds);
			UpdateIdList("condition_containers", "process_container_id", condition.ConditionId,
				condition.ProcessContainerIds);
			UpdateIdList("condition_user_groups", "item_id", condition.ConditionId, condition.ItemIds);

			var oldVar =
				db.ExecuteScalar(
					"SELECT variable FROM conditions WHERE instance = @instance AND process = @process AND condition_id = @conditionId",
					DBParameters);
			if ((oldVar == null || oldVar == DBNull.Value) &&
			    condition.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVal = translations.GetTranslationVariable(process,
					condition.LanguageTranslation[authenticatedSession.locale_id], "CONDITION");
				condition.Variable = newVal;
				SetDBParam(SqlDbType.NVarChar, "@variable", newVal);
				db.ExecuteUpdateQuery(
					"UPDATE conditions SET variable = @variable WHERE instance = @instance AND process = @process AND condition_id = @conditionId",
					DBParameters);
				foreach (var translation in condition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVal, translation.Value, translation.Key);
				}
			} else if (oldVar != null && oldVar != DBNull.Value &&
			           condition.LanguageTranslation[authenticatedSession.locale_id] != null) {
				foreach (var translation in condition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, condition.Variable, translation.Value,
						translation.Key);
				}
			}

			// INVALIDATE CACHE
			Cache.Remove(instance, "c_" + condition.Process);
			Cache.Remove(instance, "c_dts_" + condition.Process);

			var itemBase = new ItemBase(instance, condition.Process, authenticatedSession);
			foreach (var itemId in itemBase.GetItemIds()) {
				Cache.Remove(instance, "c_" + condition.ConditionId + "_" + itemId);
			}

			Cache.Remove(instance, "condition_id_" + condition.ConditionId);

			return condition;
		}

		public void Delete(int conditionId) {
			var condition = GetCondition(conditionId, true);
			SetDBParam(SqlDbType.Int, "@conditionId", conditionId);

			// INVALIDATE CACHE
			Cache.Remove(instance, "f_" + condition.Process);
			foreach (var feature in condition.Features) {
				Cache.Remove(instance, "f_" + condition.Process + "_" + feature);
				foreach (var state in condition.States) {
					Cache.Remove(instance, "s_" + condition.Process + "_" + feature + "_" + state);
					Cache.Remove(instance, "states_for_" + condition.Process + "_" + feature + "_" + state);
				}
			}

			Cache.Remove(instance, "c_" + condition.Process);
			Cache.Remove(instance, "c_dts_" + condition.Process);

			db.ExecuteDeleteQuery("DELETE FROM conditions WHERE instance=@instance AND condition_id = @conditionId",
				DBParameters);
			db.ExecuteDeleteQuery("DELETE FROM conditions_groups_condition WHERE condition_id = @conditionId",
				DBParameters);
		}

		public Condition Save(Condition condition) {
			SetDBParam(SqlDbType.Int, "@conditionId", condition.ConditionId);
			SetDBParam(SqlDbType.NVarChar, "@conditionJSON",
				JsonConvert.SerializeObject(condition.ConditionJSONComplete));
			SetDBParameter("@order_no", condition.OrderNo);
			SetDBParam(SqlDbType.Int, "@disabled", Conv.ToInt(condition.Disabled));
			db.ExecuteUpdateQuery(
				"UPDATE conditions SET condition_json = @conditionJSON, order_no = @order_no, disabled = @disabled WHERE instance = @instance AND process = @process AND condition_id = @conditionId",
				DBParameters);

			// INVALIDATE CACHE
			Cache.Remove(instance, "f_" + condition.Process);
			foreach (var feature in GetStringList("condition_features", "feature", condition.ConditionId)) {
				Cache.Remove(instance, "f_" + condition.Process + "_" + feature);
				foreach (var state in GetStringList("condition_states", "state", condition.ConditionId)) {
					Cache.Remove(instance, "s_" + condition.Process + "_" + feature + "_" + state);
				}
			}

			// INVALIDATE CACHE
			Cache.Remove(instance, "f_" + condition.Process);
			foreach (var feature in condition.Features) {
				Cache.Remove(instance, "f_" + condition.Process + "_" + feature);
				foreach (var state in condition.States) {
					Cache.Remove(instance, "s_" + condition.Process + "_" + feature + "_" + state);
					Cache.Remove(instance, "states_for_" + condition.Process + "_" + feature + "_" + state);
				}
			}

			UpdateStringList("condition_features", "feature", condition.ConditionId, condition.Features);
			UpdateStringList("condition_states", "state", condition.ConditionId, condition.States);
			UpdateIdList("condition_portfolios", "process_portfolio_id", condition.ConditionId,
				condition.ProcessPortfolioIds);
			UpdateIdList("condition_groups", "process_group_id", condition.ConditionId, condition.ProcessGroupIds);
			UpdateIdList("condition_tabs", "process_tab_id", condition.ConditionId, condition.ProcessTabIds);
			UpdateIdList("condition_containers", "process_container_id", condition.ConditionId,
				condition.ProcessContainerIds);
			UpdateIdList("condition_user_groups", "item_id", condition.ConditionId, condition.ItemIds);

			var oldVar =
				db.ExecuteScalar(
					"SELECT variable FROM conditions WHERE instance = @instance AND process = @process AND condition_id = @conditionId",
					DBParameters);
			if ((oldVar == null || oldVar == DBNull.Value) &&
			    condition.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVal = translations.GetTranslationVariable(process,
					condition.LanguageTranslation[authenticatedSession.locale_id], "CONDITION");
				SetDBParam(SqlDbType.NVarChar, "@variable", newVal);
				db.ExecuteUpdateQuery(
					"UPDATE conditions SET variable = @variable WHERE instance = @instance AND process = @process AND condition_id = @conditionId",
					DBParameters);
				foreach (var translation in condition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVal, translation.Value, translation.Key);
				}
			} else if (oldVar != null && oldVar != DBNull.Value) {
				foreach (var translation in condition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, condition.Variable, translation.Value,
						translation.Key);
				}
			}

			// INVALIDATE CACHE
			Cache.Remove(instance, "c_" + condition.Process);
			Cache.Remove(instance, "c_dts_" + condition.Process);
			var itemBase = new ItemBase(instance, condition.Process, authenticatedSession);
			foreach (var itemId in itemBase.GetItemIds()) {
				Cache.Remove(instance, "c_" + condition.ConditionId + "_" + itemId);
			}

			Cache.Remove(instance, "condition_id_" + condition.ConditionId);

			return condition;
		}

		public ConditionsGroup AddGroup(string process, ConditionsGroup conditionGroup) {
			var db = new Connections(authenticatedSession);

			conditionGroup.ConditionGroupId = db.ExecuteInsertQuery(
				"INSERT INTO conditions_groups (instance, process, order_no) VALUES(@instance, @process, ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM conditions_groups WHERE instance = @instance AND process = @process ORDER BY order_no  DESC),null)),'U'))", DBParameters);

			if (conditionGroup.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var variable = translations.GetTranslationVariable(process,
					conditionGroup.LanguageTranslation[authenticatedSession.locale_id], "CONDITION");
				conditionGroup.Variable = variable;

				SetDBParam(SqlDbType.NVarChar, "@variable", conditionGroup.Variable);
				SetDBParam(SqlDbType.Int, "@ConditionGroupId", conditionGroup.ConditionGroupId);


				conditionGroup.OrderNo =
					Conv.ToStr(db.ExecuteScalar(
						"SELECT order_no FROM conditions_groups WHERE condition_group_id = @ConditionGroupId",
						DBParameters));

				db.ExecuteUpdateQuery(
					"UPDATE conditions_groups SET variable = @variable WHERE instance = @instance AND process = @process AND condition_group_id = @ConditionGroupId",
					DBParameters);
				foreach (var translation in conditionGroup.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, variable, translation.Value,
						translation.Key);
				}
			}

			return conditionGroup;
		}

		public List<Condition> UpdateGroup(string process, List<Condition> conditions) {
			//	List<Condition> result = new List<Condition>();
			var db = new Connections(authenticatedSession);
			foreach (var condition in conditions) {
				SetDBParameter("@condition_id", condition.ConditionId);
				SetDBParameter("@condition_group_id", condition.ConditionGroupId);
				var sql =
					"SELECT COUNT(*) as count FROM conditions_groups_condition WHERE condition_id = @condition_id";
				var ifExists = (int) db.ExecuteScalar(sql, DBParameters);

				if (ifExists == 0) {
					db.ExecuteInsertQuery(
						"INSERT INTO conditions_groups_condition (condition_group_id, condition_id) VALUES(@condition_group_id, @condition_id)",
						DBParameters);
				} else {
					db.ExecuteUpdateQuery(
						"UPDATE conditions_groups_condition SET condition_group_id = @condition_group_id WHERE condition_id = @condition_id",
						DBParameters);
					Cache.Remove(instance, "c_" + condition.Process);
					Cache.Remove(instance, "c_dts_" + condition.Process);
					Cache.Remove(instance, "condition_id_" + condition.ConditionId);
				}
			}

			return conditions;
		}

		public void DeleteConditionGroup(int groupId) {
			var db = new Connections(authenticatedSession);
			SetDBParameter("@groupid", groupId);
			db.ExecuteDeleteQuery("DELETE FROM conditions_groups WHERE condition_group_id = @groupid",
				DBParameters);
			//db.ExecuteDeleteQuery("DELETE FROM conditions_groups_condition WHERE condition_group_id = @groupid",
			//	DBParameters);
		}

		public List<ConditionsGroup> GetConditionsGroups(string process) {
			var result = new List<ConditionsGroup>();
			var db = new Connections(authenticatedSession);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT condition_group_id, process, variable, order_no FROM conditions_groups WHERE instance = @instance ORDER BY order_no  ASC",
					DBParameters).Rows) {
				var conditionGroup = new ConditionsGroup(row, authenticatedSession);
				result.Add(conditionGroup);
			}

			return result;
		}

		public ConditionsGroup SaveGroup(string process, ConditionsGroup conditionGroup) {
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", conditionGroup.OrderNo);
			SetDBParam(SqlDbType.Int, "@ConditionGroupId", conditionGroup.ConditionGroupId);

			if (conditionGroup.OrderNo.Length > 0) {
				db.ExecuteUpdateQuery(
					"UPDATE conditions_groups SET order_no = @orderNo WHERE instance = @instance AND process = @process AND condition_group_id = @ConditionGroupId",
					DBParameters);
			}

			string variable = null;

			if (conditionGroup.Variable != null) {
				variable = conditionGroup.Variable;
			} else if (conditionGroup.LanguageTranslation != null) {
				if (conditionGroup.LanguageTranslation.ContainsKey(authenticatedSession.locale_id) &&
				    conditionGroup.LanguageTranslation[authenticatedSession.locale_id].Length > 0
				) {
					variable = translations.GetTranslationVariable("",
						conditionGroup.LanguageTranslation[authenticatedSession.locale_id],
						"CONDITIONSGROUPS");
					SetDBParam(SqlDbType.NVarChar, "@variable", variable);
					db.ExecuteUpdateQuery(
						"UPDATE conditions_groups SET variable = @variable WHERE instance = @instance AND condition_group_id = @ConditionGroupId",
						DBParameters);
				} else {
					foreach (var l in conditionGroup.LanguageTranslation) {
						if (l.Value.Length > 0) {
							variable = translations.GetTranslationVariable("", l.Value, "CONDITIONSGROUPS");
							break;
						}
					}

					if (variable != null) {
						SetDBParam(SqlDbType.NVarChar, "@variable", variable);
						db.ExecuteUpdateQuery(
							"UPDATE conditions_groups SET variable = @variable WHERE instance = @instance AND condition_group_id = @ConditionGroupId",
							DBParameters);
					}
				}
			}

			if (variable != null && conditionGroup.LanguageTranslation != null) {
				foreach (var translation in conditionGroup.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, variable, translation.Value,
						translation.Key);
				}

				conditionGroup.Variable = variable;
			}

			return conditionGroup;
		}


		public List<string> GetStringList(string table, string column, int conditionId) {
			var retval = new List<string>();


			if (Cache.Get(instance, "c_dts_" + process) != null) {
				var dts = (Dictionary<string, DataTable>) Cache.Get(instance, "c_dts_" + process);
				foreach (var row in dts[table].Select("condition_id = " + conditionId)) {
					retval.Add((string) row[column]);
				}
			} else {
				SetDBParam(SqlDbType.Int, "@conditionId", conditionId);
				foreach (DataRow row in db
					.GetDatatable("SELECT " + column + " FROM " + table + " WHERE condition_id = @conditionId",
						DBParameters).Rows) {
					retval.Add((string) row[column]);
				}
			}

			return retval;
		}

		public List<int> GetIdList(string table, string column, int conditionId) {
			var retval = new List<int>();


			if (Cache.Get(instance, "c_dts_" + process) != null) {
				var dts = (Dictionary<string, DataTable>) Cache.Get(instance, "c_dts_" + process);
				foreach (var row in dts[table].Select("condition_id = " + conditionId)) {
					retval.Add((int) row[column]);
				}
			} else {
				SetDBParam(SqlDbType.Int, "@conditionId", conditionId);
				foreach (DataRow row in db
					.GetDatatable("SELECT " + column + " FROM " + table + " WHERE condition_id = @conditionId",
						DBParameters).Rows) {
					retval.Add((int) row[column]);
				}
			}

			return retval;
		}

		public int GetIdInt(string table, string column, int conditionId) {
			var result = new int();

			if (Cache.Get(instance, "c_dts_" + process) != null) {
				var dts = (Dictionary<string, DataTable>) Cache.Get(instance, "c_dts_" + process);
				var rows = dts[table].Select("condition_id = " + conditionId);
				if (rows.Length > 0) {
					result = (int) rows[0][column];
				}
			} else {
				SetDBParam(SqlDbType.Int, "@conditionId", conditionId);
				var d = db.GetDataRow("SELECT " + column + " FROM " + table + " WHERE condition_id = @conditionId",
					DBParameters);
				if (d != null) {
					result = (int) d[column];
				}
			}


			return result;
		}


		public void UpdateStringList(string table, string column, int conditionId, List<string> list) {
			SetDBParam(SqlDbType.Int, "@conditionId", conditionId);
			db.ExecuteDeleteQuery("DELETE FROM " + table + " WHERE condition_id = @conditionId", DBParameters);
			if (list == null) {
				return;
			}

			foreach (var s in list) {
				SetDBParam(SqlDbType.NVarChar, "@s", s);
				db.ExecuteInsertQuery(
					"INSERT INTO " + table + " (condition_id, " + column + ") VALUES(@conditionId, @s)",
					DBParameters);
			}
		}

		public void UpdateIdList(string table, string column, int conditionId, List<int> list) {
			SetDBParam(SqlDbType.Int, "@conditionId", conditionId);
			db.ExecuteDeleteQuery("DELETE FROM " + table + " WHERE condition_id = @conditionId", DBParameters);
			if (list == null) {
				return;
			}

			foreach (var id in list) {
				SetDBParam(SqlDbType.Int, "@id", id);
				db.ExecuteInsertQuery(
					"INSERT INTO " + table + " (condition_id, " + column + ") VALUES(@conditionId, @id)",
					DBParameters);
			}
		}

			public Dictionary<int,List<UsageResult>> GetConditionUsages(string process, int conditionId) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.Int, "@conditionId", conditionId);
			var dataTable = db.GetDatatable("SELECT * from conditions", DBParameters);
			var result = new Dictionary<int, List<UsageResult>>();

			result.Add(1, new List<UsageResult>());
			result.Add(2, new List<UsageResult>());
			result.Add(3, new List<UsageResult>());
			result.Add(4, new List<UsageResult>());
			result.Add(5, new List<UsageResult>());
			//Existing condition, parent existing condition. Type 1 & 2
			foreach (DataRow r in db.GetDatatable("SELECT condition_json, condition_id as parent_condition_id, process FROM conditions WHERE condition_json LIKE '%ConditionId%'", DBParameters).Rows) {
				var o = SliceConditionOut(Conv.ToStr(r["condition_json"]), "ConditionId\":", ",");
				if(o != Conv.ToStr(conditionId)) continue;
				SetDBParam(SqlDbType.Int, "@condition_id", Conv.ToInt(o));
				var cc = dataTable.Select("condition_id = '" + Conv.ToSql(o) + "'")[0];

				var sameProcessCondition = Conv.ToStr(cc["process"]) == Conv.ToStr(r["process"]);
				var ur = new UsageResult();
				ur.OwnerElementId = sameProcessCondition
					? Conv.ToInt(r["parent_condition_id"])
					: 0;

				ur.OwnerElementType = sameProcessCondition
					? UsageResult.OwnerElementTypes.ExistingCondition
					: UsageResult.OwnerElementTypes.ParentExistingCondition;

				ur.OwnerElementParentId = sameProcessCondition
					? 0
					: Conv.ToInt(r["parent_condition_id"]);

				if (!sameProcessCondition)
					ur.OwnerElementParentProcess = Conv.ToStr(r["process"]);


				result[sameProcessCondition ? 1 : 2].Add(ur);
			}

			//Process Actions, type 3
			foreach (DataRow r in db.GetDatatable("SELECT process_action_id, condition_id FROM process_actions " +
			" where process = @process and condition_id = @conditionId", DBParameters).Rows) {
				var ur = new UsageResult();
				ur.OwnerElementId = Conv.ToInt(r["process_action_id"]);
				ur.OwnerElementType = UsageResult.OwnerElementTypes.ProcessAction;
				result[3].Add(ur);
			}

			//Process Action Rows, type 4
			foreach (DataRow r in db.GetDatatable("SELECT process_action_row_id, process_action_id FROM process_action_rows par " +
			"INNER JOIN conditions c ON c.condition_id = par.condition_id " +
			"WHERE par.condition_id = @conditionId",DBParameters).Rows) {

				var ur = new UsageResult();
				ur.OwnerElementId = Conv.ToInt(r["process_action_row_id"]);
				ur.OwnerElementType = UsageResult.OwnerElementTypes.ProcessActionRow;
				ur.OwnerElementParentId = Conv.ToInt(r["process_action_id"]);
				result[4].Add(ur);
			}

			//Notification conditions, Type 5
			var firstQuery = "SELECT before_condition_id, after_condition_id, notification_condition_id, variable " +
			                 "FROM notification_conditions " +
			                 " WHERE process = @process AND (before_condition_id = @conditionId OR after_condition_id = @conditionId)";

			foreach (DataRow row in db.GetDatatable(firstQuery, DBParameters).Rows) {
				var ur = new UsageResult();
				ur.OwnerElementId = Conv.ToInt(row["notification_condition_id"]);
				ur.OwnerElementType = UsageResult.OwnerElementTypes.NotificationCondition;
				if(!result[5].Contains(ur)) result[5].Add(ur);
			}
			return result;
		}

			private static string SliceConditionOut(string strSource, string strStart, string strEnd)
			{
				if (strSource.Contains(strStart) && strSource.Contains(strEnd))
				{
					int Start, End;
					Start = strSource.IndexOf(strStart, 0) + strStart.Length;
					End = strSource.IndexOf(strEnd, Start);
					return strSource.Substring(Start, End - Start);
				}

				return "";
			}

			public class UsageResult
			{
				public enum OwnerElementTypes {
					ExistingCondition = 1,
					ParentExistingCondition = 2,
					ProcessAction = 3,
					ProcessActionRow = 4,
					NotificationCondition = 5
				}

				public OwnerElementTypes OwnerElementType { get; set; }

				public int OwnerElementId { get; set; }

				public int OwnerElementParentId { get; set; }

				public string OwnerElementParentProcess { get; set; }
			}

		internal IActionResult UpdateGroup(string process, List<ConditionsGroup> conditionGroups33) {
			throw new NotImplementedException();
		}
	}
}