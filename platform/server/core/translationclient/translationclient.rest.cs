﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core.translationclient {
	[Route("v1/language/[controller]")]
	public class TranslationClientRest : RestBase {
		[HttpGet]
		public APIResult GetData() {
			var translationClient = new TranslationClient();
			APIResult.data = translationClient.Get();
			return APIResult;
		}

		[HttpPost("")]
		public APIResult Post([FromBody] Dictionary<string, Dictionary<string, string>> newFiles) {
			var translationClient = new TranslationClient();
			APIResult.data = translationClient.Save(newFiles);
			return APIResult;
		}

		[HttpPut("")]
		public bool Put([FromBody] List<InstanceTranslations.InstanceTranslation> instanceTranslations) {
			var it = new InstanceTranslations(instance, currentSession);
			return it.SaveInstanceTranslations(instanceTranslations);;
		}

		[HttpGet("instance")]
		public APIResult GetInstanceTranslations() {
			var it = new InstanceTranslations(instance, currentSession);
			APIResult.data = it.GetInstanceTranslations();
			return APIResult;
		}

        [HttpGet("{process}")]
        public APIResult GetProcessTranslations(string process) {
            var it = new InstanceTranslations(instance, this.currentSession);
            APIResult.data = it.GetProcessTranslations(process);
            return APIResult;
        }


        [HttpPost("new")]
        public APIResult NewTranslation([FromBody]List<Dictionary <string,string>> translations) {
            var it = new InstanceTranslations(instance, currentSession);
            APIResult.data = it.newTranslations(translations);
            return APIResult;
        }
        [HttpPut("save")]
        public APIResult SaveTranslation([FromBody]List<Dictionary<string, string>> translationUpdate)
        {
            var it = new InstanceTranslations(instance, currentSession);
            APIResult.data = it.updateTranslations(translationUpdate);
            return APIResult;
        }
    }
}