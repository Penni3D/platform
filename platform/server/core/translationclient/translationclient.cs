﻿using System;
using System.Collections.Generic;
using System.IO;
using Keto5.x.platform.server.common;
using Newtonsoft.Json.Linq;
using System.Data;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core.translationclient {

    public class TranslationClient {
        private readonly DirectoryInfo dir = new DirectoryInfo(Path.Combine(Config.AppPath, "platform", "resources"));

        public Dictionary<string, Dictionary<string, string>> Get() {


            var result = new Dictionary<string, Dictionary<string, string>>();

            foreach (var f in dir.GetFiles("keto_*.json")) {
                var localeDictionary = new Dictionary<string, string>();
                result.Add(f.Name.Replace("keto_", "").Replace(".json", ""), localeDictionary);

                var file = JObject.Parse(File.ReadAllText(f.FullName));
                foreach (var item in file) {
                    var key = item.Key.ToUpper();
                    localeDictionary.Add(key, item.Value.ToString());
                }

            }
            return result;
        }

        public Dictionary<string, Dictionary<string, string>> Save(Dictionary<string, Dictionary<string, string>> newFiles) {

            var languageKeys = newFiles.Keys;

            foreach (var key in languageKeys) {
                var wwwPath = Path.Combine(Config.WwwPath, "private", "keto_" + key + ".json");
                var fullPath = Path.Combine(Config.AppPath, "platform", "resources", "keto_" + key + ".json");
                var jObject = JObject.FromObject(newFiles[key]);
                var jsonText = jObject.ToString();
                File.WriteAllText(fullPath, jsonText);
                File.WriteAllText(wwwPath, jsonText);
            }
            return newFiles;
        }
    }

    public class InstanceTranslations : ConnectionBase {
        public InstanceTranslations(string instance, AuthenticatedSession authenticatedSession) : base(instance,
        authenticatedSession) { }

    
        public class ProcessTranslation {
            public int ProcessTranslationId { get; set; }
            public string Variable { get; set; }
            public string Translation { get; set; }
            public string Process { get; set; }
            public string Language { get; set; }
            public ProcessTranslation() { }

            public ProcessTranslation(DataRow row) {
                ProcessTranslationId = Conv.ToInt(row["process_translation_id"]);
                Variable = Conv.ToStr(row["variable"]);
                Translation = Conv.ToStr(row["translation"]);
                Process = Conv.ToStr(row["process"]);
                Language = Conv.ToStr(row["code"]);
                }
        }

        public class InstanceTranslation {
            public string Variable { get; set; }
            public string Translation { get; set; }
            public string Code { get; set; }

            public InstanceTranslation() { }

            public InstanceTranslation(DataRow row) {
                Variable = Conv.ToStr(row["variable"]);
                Translation = Conv.ToStr(row["translation"]);
                Code = Conv.ToStr(row["code"]);
            }
        }


        public bool SaveInstanceTranslations(List<InstanceTranslation> translations) {
            SetDBParam(SqlDbType.NVarChar, "@instance", instance);

            var activeLanguages = new List<string>();

            foreach (InstanceTranslation t in translations) {
                SetDBParam(SqlDbType.NVarChar, "@code", t.Code);
                SetDBParam(SqlDbType.NVarChar, "@translation", t.Translation);
                SetDBParam(SqlDbType.NVarChar, "@variable", t.Variable);

                var langExists = Conv.ToInt(db.ExecuteScalar("SELECT COUNT(*) FROM instance_languages WHERE code = @code",
                    DBParameters));

                var count = Conv.ToInt(db.ExecuteScalar("SELECT COUNT(*) FROM instance_translations WHERE code = @code AND variable = @variable",
                    DBParameters));

                if (langExists != 0) {
                    activeLanguages.Add(t.Code);
                    if (count == 0) {
                        db.ExecuteInsertQuery(
                            "INSERT INTO instance_translations (instance, code, translation, variable) VALUES(@instance, @code, @translation, @variable)",
                            DBParameters);
                    } else {
                        db.ExecuteUpdateQuery(
                            "UPDATE instance_translations SET translation = @translation WHERE code = @code AND variable = @variable AND instance = @instance",
                            DBParameters);
                    }
                    Cache.Set(instance, "instance_translations", null);
                }
            }

            foreach (var cod in activeLanguages) {
                Cache.Remove(instance, "languages_" + cod);
            }

            return true;
        }


        public List<InstanceTranslation> GetInstanceTranslations() {
            SetDBParam(SqlDbType.NVarChar, "@instance", instance);
            var result = new List<InstanceTranslation>();
            foreach (DataRow r in db.GetDatatable("SELECT code, variable, translation from instance_translations WHERE instance = @instance", DBParameters)
                .Rows) {
                var it = new InstanceTranslation(r);
                result.Add(it);
            }
            return result;
        }

        public List<ProcessTranslation> newTranslations(List<Dictionary<string, string>> values) {
            SetDBParam(SqlDbType.NVarChar, "@instance", instance);
            var l = new List<ProcessTranslation>();
            foreach (var newTranslation in values) {
                foreach (KeyValuePair<string, string> translation in newTranslation) {
                    if (translation.Key == "language") {
                        SetDBParam(SqlDbType.NVarChar, "@language", translation.Value);
                    }
                    else if (translation.Key == "langVariable") {
                        SetDBParam(SqlDbType.NVarChar, "@variable", translation.Value);
                    }
                    else if (translation.Key == "translation") {
                        SetDBParam(SqlDbType.NVarChar, "@translation", translation.Value);
                    }
                    else if (translation.Key == "process") {
                        SetDBParam(SqlDbType.NVarChar, "@process", translation.Value);
                    }
                }
                db.ExecuteInsertQuery("INSERT INTO process_translations (instance, process, code, variable, translation)" +
                    " Values (@instance, @process ,@language, @variable, @translation) " ,
                    DBParameters);
                DataRow row = db.GetDataRow(" SELECT process_translation_id, code, variable, translation, process FROM process_translations " +
                              " WHERE code = @language AND process = @process AND" +
                              " variable = @variable AND translation = @translation ", DBParameters);
                l.Add(new ProcessTranslation(row));
            }
            return l;
        }

        public List<ProcessTranslation> updateTranslations(List<Dictionary<string, string>> values) {
            SetDBParam(SqlDbType.NVarChar, "@instance", instance);

            foreach (var translationUpdate in values) {

                foreach (KeyValuePair<string, string> translation in translationUpdate) {
                    if ( translation.Key == "Translation") {
                        SetDBParam(SqlDbType.NVarChar, "@translation", translation.Value);
                    }
                    else if (translation.Key == "Id") {
                        SetDBParam(SqlDbType.NVarChar, "@processId", translation.Value);
                    }
                }
                db.ExecuteUpdateQuery(" exec app_set_archive_user_id 0" +
                                      " UPDATE process_translations" +
                                      " SET translation = @translation" +
                                      " WHERE process_translation_id = @processId" , 
                    DBParameters);

            }

            var l = new List<ProcessTranslation>();
            return l;
        }
            public Dictionary<string, List<ProcessTranslation>> GetProcessTranslations(string process) {
            SetDBParam(SqlDbType.NVarChar, "@process", process);
            var instanceResult = new Dictionary<string, List<ProcessTranslation>>();

            foreach (DataRow r in db.GetDatatable(
                " SELECT code FROM instance_languages " ,
                DBParameters).Rows) {
                var newr = new List<ProcessTranslation>();
                instanceResult.Add(Conv.ToStr(r["code"]), newr);
            }
            foreach (DataRow r in db.GetDatatable(
            " SELECT variable, translation, process_translation_id, code, process FROM process_translations " +
            " WHERE process = @process ",
            DBParameters).Rows) {
                instanceResult[Conv.ToStr(r["code"])].Add(new ProcessTranslation(r));
            }
            return instanceResult;
        }

    }
}



