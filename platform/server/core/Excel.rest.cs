﻿using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/files/Excel")]
	public class ExcelRest : RestBase {
		[HttpPost]
		public string GetExcel([FromBody] Excel data) {
			return data.GetExcelBase64();
		}
	}
}