﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/language/Translations/")]
	public class TranslationsRest : RestBase {
		[HttpGet("{lang}")]
		public Dictionary<string, string> Get(string lang) {
			return new Translations(instance, currentSession, lang).GetTranslations();
		}

		[HttpGet("{lang}/{forced}")]
		public Dictionary<string, string> Get(string lang, bool forced) {
			if (forced) currentSession = currentSession.SetMember("locale_id", lang);
			return new Translations(instance, currentSession, lang).GetTranslations();
		}
	}

	[Route("v1/language/Translation/")]
	public class TranslationRest : RestBase {
		[HttpGet("{lang}")]
		public List<TranslationModel> Get(string lang) {
			return new Translation(instance, currentSession).GetTranslation(lang);
		}

		[HttpPost("{process}/{variable}")]
		public void Post(string process, string variable, [FromBody] TranslationModel[] translationsModel) {
			new Translation(instance, currentSession).Save(process, variable, translationsModel);
		}
	}

	public class TranslationModel {

		public TranslationModel(DataRow row, string alias = "") {
			if (alias.Length > 0) alias = alias + "_";
			Code = (string) row["code"];
			if (!DBNull.Value.Equals(row["variable"])) Variable = (string) row["variable"];
			if (!DBNull.Value.Equals(row["translation"])) TranslatedText = (string) row["translation"];
			if (!DBNull.Value.Equals(row["icon_url"])) IconUrl = (string) row["icon_url"];
		}

		public string Code { get; set; }
		public string Variable { get; set; }
		public string TranslatedText { get; set; }
		public string IconUrl { get; set; }
	}
}