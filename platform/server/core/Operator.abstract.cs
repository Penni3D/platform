﻿using System.Collections.Generic;

namespace Keto5.x.platform.server.core {
	
	
	public class Operator {
		public Operator(int id, string oText) {
			OperatorId = id;
			OperatorName = oText;
		}

		public int OperatorId { get; set; }
		public string OperatorName { get; set; }
	}

	public class Operators {
		private readonly Dictionary<int, Operator> operators = new Dictionary<int, Operator> {
			{1, new Operator(1, "and")},
			{2, new Operator(2, "or")},
			{3, new Operator(3, "xor")},
			{4, new Operator(4, "not")}
		};

		public Dictionary<int, Operator> GetOperators() {
			var operators = new Dictionary<int, Operator>();
			operators.Add(1, new Operator(1, "and"));
			operators.Add(2, new Operator(2, "or"));
			operators.Add(3, new Operator(3, "xor"));
			operators.Add(4, new Operator(4, "not"));
			return operators;
		}

		public Operator GetOperator(int operatorId){
			return operators[operatorId];
		}
	}

}
