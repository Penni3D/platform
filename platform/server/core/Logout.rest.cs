﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;
using Authentication = Keto5.x.platform.server.security.Authentication;

namespace Keto5.x.platform.server.core {
	[Route("v1/core/[controller]")]
	public class ClearCookies : Controller {
		[HttpGet("")]
		public void Clear() {
			var a = new Authentication("");
			a.ClearCookies(HttpContext);
		}
	}

	[Route("v1/core/[controller]")]
	public class Logout : RestBase {
		[HttpPost("")]
		public void PostLogout() {
			var a = new Authentication(instance);
			Cache.Remove(instance, "authentication", "item_id_" + currentSession.item_id);
			a.RevokeAccessInHttpContext(HttpContext, currentSession.item_id, currentSession.token);
		}
	}
}