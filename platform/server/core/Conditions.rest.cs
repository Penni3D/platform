﻿//System references

using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/settings/Conditions")]
	public class ConditionsRest : PageBase {
		public ConditionsRest() : base("settings") { }

		public class ConditionTestResult {
			public string ProcessName { get; set; }
			public string ConditionName { get; set; }
			public bool Error { get; set; }
			public bool Suspicious { get; set; }
			public Double TimeMs { get; set; }
		}

		[HttpGet("testall")]
		public List<ConditionTestResult> TestAll() {
			var result = new List<ConditionTestResult>();
			var p = new Processes(instance, currentSession);

			//Create Mock Session
			var members = new Dictionary<string, object>();
			members.Add("locale_id", currentSession.locale_id);
			var testSession =
				new AuthenticatedSession(instance, p.ChooseItemAtRandom(instance, "user"), members) {
					instance = currentSession.instance
				};
			var ug = new UserGroups(testSession.instance, testSession);
			var groups = ug.GetUserGroupsForAuthenticatedUser();
			testSession.SetMember("groups", groups);

			//Test Conditions
			foreach (var po in p.GetProcesses()) {
				var conditions = new Conditions(instance, po.ProcessName, testSession);
				foreach (var condition in conditions.GetConditions(po.ProcessName)) {
					var r = new ConditionTestResult {ConditionName = condition.Variable, ProcessName = po.Variable};

					Cache.Initialize();
					try {
						var randomItemId = p.ChooseItemAtRandom(instance, po.ProcessName);
						if (randomItemId > 0) {
							var time = DateTime.Now;
							var exp = new States(instance, testSession).GetItemsConditionExpression(po.ProcessName,
								randomItemId, condition);
							var test = exp.GetMain() + " " + States.EvaluateExpression(exp);
							r.TimeMs = DateTime.Now.Subtract(time).TotalMilliseconds;
						} else {
							r.Suspicious = true;
						}
					} catch (Exception e) {
						r.Error = true;
						Console.WriteLine(r.ConditionName + ": " + e.Message);
					}

					result.Add(r);
				}
			}

			//Restore Cache
			Cache.Initialize();
			InitializeServer.CacheSubQueries();
			return result.OrderByDescending(r => r.TimeMs).ToList();
		}

		[HttpGet("{process}")]
		public List<Condition> Get(string process) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			return conditions.GetConditions(process, true);
		}

		[HttpGet("{process}/{feature}/{state}")]
		public List<Condition> Get(string process, string feature, string state) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			return conditions.GetConditions(process, feature, state, true);
		}

		[HttpGet("{process}/{conditionId}")]
		public Condition Get(string process, int conditionId) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			return conditions.GetCondition(conditionId, true);
		}

		[HttpGet("{process}/phaseids")]
		public APIResult GetIdsForPhases(string process) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			APIResult.data = conditions.GetIdsForPhases(process);
			return APIResult;
		}

		[HttpGet("{process}/hierarchy")]
		public APIResult GetConditionHierarchy(string process) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			APIResult.data = conditions.GetConditionHierarchy(process);
			return APIResult;
		}

		[HttpGet("relatedconditions/{itemColumnId}")]
		public APIResult GetConditionsForItemColumn(int itemColumnId) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			APIResult.data = conditions.GetConditionsForItemColumn(itemColumnId, true);
			return APIResult;
		}

		[HttpPut("{process}/conditionGroupTabs")]
		public IActionResult PutGroupTabs(string process, [FromBody] List<Condition> condition) {
			CheckRight("write");
			var conditions = new Conditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				conditions.UpdateGroup(process, condition);
			}

			return new ObjectResult(condition);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] Condition condition) {
			CheckRight("write");
			var conditions = new Conditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				conditions.Add(condition);
				return new ObjectResult(condition);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPost("{process}/copy")]
		public IActionResult Post(string process, string copy, [FromBody] Condition condition) {
			CheckRight("write");
			var conditions = new Conditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				conditions.Copy(condition);
				return new ObjectResult(condition);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<Condition> condition) {
			CheckRight("write");
			var conditions = new Conditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var cd in condition) {
					conditions.Save(cd);
				}

				return new ObjectResult(condition);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{conditionGroupId}")]
		public IActionResult Delete(int conditionGroupId) {
			CheckRight("delete");
			var conditions = new Conditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				conditions.DeleteConditionGroup(conditionGroupId);
			}

			return new ObjectResult(conditionGroupId);
		}

		[HttpDelete("{process}/{conditionId}")]
		public IActionResult Delete(string process, string conditionId) {
			CheckRight("write");
			var conditions = new Conditions(instance, process, currentSession);
			foreach (var id in conditionId.Split(',')) {
				conditions.Delete(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}

		[HttpGet("{process}/{conditionId}/usages")]
		public Dictionary<int, List<Conditions.UsageResult>> GetConditionUsages(string process, int conditionId) {
			var conditions = new Conditions(instance, process, currentSession);
			return conditions.GetConditionUsages(process, conditionId);
		}

	}

	[Route("v1/meta/[controller]")]
	public class UserGroupConditions : PageBase {
		public UserGroupConditions() : base("settings") { }

		[HttpGet("{process}/{itemId}")]
		public List<Condition> Get(string process, int itemId) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			return conditions.GetConditionsForUserGroup(process, itemId, true);
		}


		[HttpDelete("{process}/{conditionId}")]
		public IActionResult Delete(string process, string conditionId) {
			CheckRight("delete");
			var conditions = new Conditions(instance, process, currentSession);
			foreach (var id in conditionId.Split(',')) {
				conditions.Delete(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}
	}


	[Route("v1/settings/[controller]")]
	public class ConditionsGroups : PageBase {
		public ConditionsGroups() : base("settings") { }

		[HttpGet("{process}")]
		public List<ConditionsGroup> Get(string process) {
			CheckRight("read");
			var conditions = new Conditions(instance, process, currentSession);
			return conditions.GetConditionsGroups(process);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] ConditionsGroup conditionGroup) {
			CheckRight("write");
			var conditions = new Conditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				conditions.AddGroup(process, conditionGroup);
				return new ObjectResult(conditionGroup);
			}

			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<ConditionsGroup> conditionGroups) {
			CheckRight("write");
			var conditions = new Conditions(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var cg in conditionGroups) {
					conditions.SaveGroup(process, cg);
				}

				return new ObjectResult(conditionGroups);
			}

			return null;
		}
	}
}