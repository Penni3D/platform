﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.specialProcesses;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core {
	[Route("v1/core")]
	public class Core : RestBase {
		[HttpGet("globals")]
		public List<InstanceMenu> GetGlobals() {
			var im = new InstanceMenus(instance, currentSession);
			return im.GetInstanceGlobals();
		}

		[HttpGet("frontpage")]
		public List<InstanceMenu> GetFrontpage() {
			var im = new InstanceMenus(instance, currentSession);
			return im.GetFrontpage();
		}
	}

	[Route("v1/navigation")]
	public class Navigation : RestBase {
		[HttpGet("")]
		public ActionResult GetNavigations() {
			if (currentSession != null) {
				var retval = new List<MenuItem>();
				var ug = new UserGroups(instance, currentSession);
				var imr = new InstanceMenuRights(instance, currentSession.item_id);
				var menuIds = imr.GetMenuIdsForUserGroups(ug.GetUserGroupsForAuthenticatedUser());
				var ims = new InstanceMenus(instance, currentSession);

				foreach (var parent in ims.GetParentInstanceMenus()) {
					var m = new MenuItem {
						InstanceMenuId = parent.InstanceMenuId,
						LanguageTranslation = parent.LanguageTranslation,
						Links = new List<Dictionary<string, object>>(),
						Params = parent.Params ?? new Dictionary<string, object>()
					};

					foreach (var link in ims.GetChildInstanceMenus(parent.InstanceMenuId)) {
						if (!menuIds.Contains(link.InstanceMenuId)) continue;

						var l = new Dictionary<string, object> {
							{"InstanceMenuId", link.InstanceMenuId},
							{"LanguageTranslation", link.LanguageTranslation},
							{"view", link.DefaultState},
							{"icon", link.Icon},
							{"params", link.Params ?? new Dictionary<string, object>()}
						};

						m.Links.Add(l);
					}

					if (m.Links.Count > 0) retval.Add(m);
				}

				return new ObjectResult(retval);
			}

			Response.StatusCode = 404;

			return Content("404", "text/plain");
		}
	}

	public class ListNameAndCount {
		public string Name;
		public int Count;
	}

	[Route("v1/meta")]
	public class Layout : RestBase {
		[HttpPost("layout/{process}/{itemId}")]
		public ActionResult GetLayout(string process, int itemId, [FromBody] List<ListNameAndCount> cachedLists) {
			if (currentSession == null) {
				Response.StatusCode = 404;
				return Content("404", "text/plain");
			}

			var pg = new ProcessGroups(instance, process, currentSession);
			var pt = new ProcessTabs(instance, process, currentSession);
			var pgt = new ProcessGroupTabs(instance, process, currentSession);
			var st = new States(instance, currentSession);

			var rights = st.GetFeatureStates(process, itemId);
			var ret = new Dictionary<string, object>();
			var groups = new List<MenuGroup>();

			foreach (var g in pg.GetGroups()) {
				if (!rights.ContainsKey("meta") ||
				    !rights["meta"].HasGroupState(g.ProcessGroupId, new[] {"meta.write", "meta.read"})) {
					continue;
				}

				var group = new MenuGroup();
				var groupClass = "process-progress circle circle-small";

				group.processGroupId = g.ProcessGroupId;


				group.translation = new Dictionary<string, string> {
					{
						currentSession.locale_id,
						new Translations(currentSession.instance, currentSession).GetTranslation(g.Variable)
					}
				};

				group.icon = "arrow_downward";

				if (rights["meta"].HasGroupState(g.ProcessGroupId, new[] {"meta.write"})) {
					groupClass += " write";
				}

				if (rights["meta"].HasGroupState(g.ProcessGroupId, new[] {"meta.active"})) {
					groupClass += " active";
				}

				group.iconClass = groupClass;
				group.tabs = new List<Dictionary<string, object>>();

				var processGroups = new ProcessGroups(instance, process, currentSession);
				var processGroupNaviagation = processGroups.GetGroupsNavigationMenu(g.ProcessGroupId);
				if (processGroupNaviagation.Count == 0) {
					foreach (var gt in pgt.GetGroupTabs(g.ProcessGroupId)) {
						if (!rights.ContainsKey("meta") ||
						    !rights["meta"].HasTabState(gt.ProcessTab.ProcessTabId,
							    new[] {"meta.write", "meta.read"})) {
							continue;
						}

						var t = pt.GetTab(gt.ProcessTab.ProcessTabId);
						var tr = new Dictionary<string, string> {
							{
								currentSession.locale_id, new Translations(currentSession.instance, currentSession)
									.GetTranslation(t.Variable)
							}
						};

						var tab = new Dictionary<string, object> {
							{
								"LanguageTranslation", tr
							},
							{"view", "process.tab"}
						};
						var pp = new Dictionary<string, object> {
							{"process", process},
							{"itemId", itemId},
							{"tabId", t.ProcessTabId},
							{"processGroupId", g.ProcessGroupId}
						};

						tab.Add("params", pp);
						group.tabs.Add(tab);
					}

					foreach (var feature in g.SelectedFeatures) {
						if (!rights.ContainsKey(feature.Feature) ||
						    !rights[feature.Feature].groupStates.ContainsKey(g.ProcessGroupId) ||
						    rights[feature.Feature].groupStates[g.ProcessGroupId].s.Count <= 0) {
							continue;
						}

						var f = new Dictionary<string, object> {
							{"variable", feature.Feature.ToUpper()},
							{"view", feature.DefaultState}
						};

						var pp = new Dictionary<string, object> {
							{"process", process},
							{"itemId", itemId},
							{"processGroupId", g.ProcessGroupId}
						};

						f.Add("params", pp);
						group.tabs.Add(f);
					}
				} else {
					foreach (var gt in processGroupNaviagation) {
						if ((int) gt["Type"] == 0) {
							if (!rights.ContainsKey("meta") ||
							    !rights["meta"].HasTabState((int) gt["ProcessTabId"],
								    new[] {"meta.write", "meta.read"})) {
								continue;
							}

							var tr = new Dictionary<string, string> {
								{
									currentSession.locale_id, new Translations(currentSession.instance, currentSession)
										.GetTranslation((string) gt["Variable"])
								}
							};
							var tab = new Dictionary<string, object> {
								{
									"LanguageTranslation", tr
								},
								{"view", (string) gt["DefaultState"]},
								{"icon", Conv.ToStr(gt["Icon"])},
								{"templatePortfolioId", Conv.ToStr(gt["TemplatePortfolioId"])}
							};

							var pp = new Dictionary<string, object> {
								{"process", process},
								{"itemId", itemId},
								{"tabId", (int) gt["ProcessTabId"]},
								{"processGroupId", g.ProcessGroupId}
							};

							tab.Add("params", pp);
							group.tabs.Add(tab);
						} else if ((int) gt["Type"] == 1) {
							if (!rights.ContainsKey((string) gt["Variable"]) ||
							    !rights[(string) gt["Variable"]].groupStates.ContainsKey(g.ProcessGroupId) ||
							    rights[(string) gt["Variable"]].groupStates[g.ProcessGroupId].s.Count <= 0) {
								continue;
							}

							var f = new Dictionary<string, object> {
								{"variable", Conv.ToStr(gt["Variable"]).ToUpper()},
								{"view", (string) gt["DefaultState"]},
								{"icon", Conv.ToStr(gt["Icon"])},
								{"templatePortfolioId", Conv.ToStr(gt["TemplatePortfolioId"])},
								{"customName", Conv.ToStr(gt["CustomName"])}
							};

							var pp = new Dictionary<string, object> {
								{"process", process},
								{"itemId", itemId},
								{"processGroupId", g.ProcessGroupId}
							};

							f.Add("params", pp);
							group.tabs.Add(f);
						}
					}
				}

				if (group.tabs.Count > 0) {
					groups.Add(group);
				}
			}

			ret.Add("Navigation", groups);

			//Clean state strings and unused objects from features
			rights = st.CleanStatesObject(rights);

			ret.Add("States", rights);

			var tabs = new ProcessTabs(instance, process, currentSession);
			ret.Add("Grids",
				rights.ContainsKey("meta")
					? tabs.GetTabHiearchy(itemId, null, rights["meta"], cachedLists)
					: tabs.GetTabHiearchy(itemId, ignoreLists: cachedLists));

			ret.Add("ArchiveDates", pg.GetArchiveDates(itemId));

			return new ObjectResult(ret);
		}
		
		
	}
}