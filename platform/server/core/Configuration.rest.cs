﻿//System references

using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.core {
	[Route("v1/navigation/InstanceMenus")]
	public class InstanceMenusRest : PageBase {
		public InstanceMenusRest() : base("instanceMenu") { }


		[HttpGet]
		public List<InstanceMenu> Get()
		{
			CheckRight("read");
			var im = new InstanceMenus(instance, currentSession);
			return im.GetInstanceMenuHierarchy();
		}

		[HttpGet("frontpage")]
		public List<InstanceMenu> GetFrontpage() {
			CheckRight("read");

			var im = new InstanceMenus(instance, currentSession);
			return im.GetInstanceFrontpage();
		}

		[HttpPut("{instanceMenuId}")]
		public InstanceMenu Put(int instanceMenuId, [FromBody]InstanceMenu menu) {
			CheckRight("write");

			return SaveMenu(instanceMenuId, menu);
		}

		[HttpPut]
		public List<InstanceMenu> Puts([FromBody] List<InstanceMenu> menus) {
			CheckRight("write");
			var ret = new List<InstanceMenu>();
			foreach (var im in menus) {
				ret.Add(SaveMenu(im.InstanceMenuId, im));
			}

			return ret;
		}

		[HttpPost]
		public List<InstanceMenu> Post([FromBody]List<InstanceMenu> menu) {
			CheckRight("write");
			var ret = new List<InstanceMenu>();

			var im = new InstanceMenus(instance, currentSession);
			if (ModelState.IsValid) {
				foreach (var m in menu) {
					ret.Add(im.NewInstanceMenu(m));
				}
			}

			return ret;
		}

		[HttpDelete("{instanceMenuIds}")]
		public void Delete(string instanceMenuIds) {
			CheckRight("write");

			var im = new InstanceMenus(instance, currentSession);
			if (ModelState.IsValid) {
				foreach (var menuId in instanceMenuIds.Split(",")) {
					im.DeleteInstaceMenu(Conv.ToInt(menuId));
				}
			}
		}

		private InstanceMenu SaveMenu(int instanceMenuId, InstanceMenu menu) {
			var im = new InstanceMenus(instance, currentSession);
			return ModelState.IsValid ? im.SaveInstanceMenu(instanceMenuId, menu) : null;
		}
	}

	[Route("v1/navigation/instanceMenus/[controller]")]
	public class Frontpage : PageBase {
		public Frontpage() : base("frontpage") { }

		[HttpPut("{instanceMenuId}")]
		public InstanceMenu Put(int instanceMenuId, [FromBody]InstanceMenu menu) {
			CheckRight("write");

			return SaveMenu(instanceMenuId, menu);
		}

		[HttpPut]
		public List<InstanceMenu> Puts([FromBody] List<InstanceMenu> menus) {
			CheckRight("write");
			var ret = new List<InstanceMenu>();
			foreach (var im in menus) {
				ret.Add(SaveMenu(im.InstanceMenuId, im));
			}

			return ret;
		}

		[HttpPost]
		public List<int> Post([FromBody]List<InstanceMenu> menu) {
			CheckRight("write");
			var ret = new List<int>();

			var im = new InstanceMenus(instance, currentSession);
			if (ModelState.IsValid) {
				foreach (var m in menu) {
					ret.Add(im.NewInstanceMenu(m).InstanceMenuId);
				}
			}

			return ret;
		}

		[HttpDelete("{instanceMenuIds}")]
		public void Delete(string instanceMenuIds) {
			CheckRight("write");

			var im = new InstanceMenus(instance, currentSession);
			if (ModelState.IsValid) {
				foreach (var menuId in instanceMenuIds.Split(",")) {
					im.DeleteInstaceMenu(Conv.ToInt(menuId));
				}
			}
		}

		private InstanceMenu SaveMenu(int instanceMenuId, InstanceMenu menu) {
			var im = new InstanceMenus(instance, currentSession);
			return ModelState.IsValid ? im.SaveInstanceMenu(instanceMenuId, menu) : null;
		}
	}

	[Route("v1/configuration")]
	public class ConfigurationRest : PageBase {
		public ConfigurationRest() : base("configuration.client", true) { }
		
		[HttpGet]
		public List<UserPreferences> GetUserConfigurations() {
			var c = new SavedConfigurations(instance, currentSession);
			return c.GetConfigurations();
		}

		[HttpPut("{group}")]
		public void SetUserConfigurations(string group, [FromBody]JObject value) {
			var c = new SavedConfigurations(instance, currentSession);
			c.SetConfiguration(group, value);
		}

		[HttpPut("{group}/{id}")]
		public void SetUserConfigurations(string group, string id, [FromBody]JObject value) {
			var c = new SavedConfigurations(instance, currentSession);
			c.SetConfiguration(group, id, JsonConvert.SerializeObject(value));
		}

		[HttpGet("saved/{group}")]
		public List<SavedConfiguration> GetSavedUserConfigurations(string group) {
			var c = new SavedConfigurations(instance, currentSession);
			return c.GetSavedConfigurations(group);
		}

		[HttpGet("saved/{group}/{identifier}")]
		public List<SavedConfiguration> GetSavedUserConfigurations(string group, string identifier) {
			var c = new SavedConfigurations(instance, currentSession);
			return c.GetSavedConfigurations(group, identifier);
		}

		[HttpGet("saved/navigation/sticky")]
		public List<SavedConfiguration> GetStickies() {
			var states = new List<string>();
			var db = new Connections(currentSession);
			var ids = (List<int>) currentSession.GetMember("groups");

			var idStr = string.Join(",", ids.ToArray());
			if (idStr.Length == 0) {
				idStr = "0";
			}

			foreach (DataRow dr in db.GetDatatable(
				"SELECT DISTINCT state FROM instance_menu_states WHERE instance_menu_id IN " +
				"(SELECT instance_menu_id FROM instance_menu WHERE default_state = 'sticky') AND item_id in (" + idStr + ")")
				.Rows
			) {
				states.Add(Conv.ToStr(dr["state"]));
			}

			if (states.Contains("read") || states.Contains("write") || states.Contains("save")) {
				var c = new SavedConfigurations(instance, currentSession);
				return c.GetSavedConfigurations("navigation", "sticky");
			}

			return new List<SavedConfiguration>();
		}

		[HttpDelete("saved/{group}/{identifier}/{id}/{userId}")]
		public void DeleteSavedUserConfiguration(string group, string identifier, int id, int userId = 0) {
			var c = new SavedConfigurations(instance, currentSession);
			c.DeleteSavedConfiguration(group, identifier, id, userId);
		}

		[HttpPost("saved/{group}/{identifier}")]
		public SavedConfiguration SetSavedUserConfigurations(string group, string identifier, [FromBody]JObject data) {
			var c = new SavedConfigurations(instance, currentSession);
			return c.CreateSavedConfiguration(group, identifier, data);
		}

		[HttpPut("saved/{group}/{identifier}/{id}")]
		public void UpdateSavedUserConfigurations(string group, string identifier, int id, [FromBody]JObject data) {
			var c = new SavedConfigurations(instance, currentSession);
			c.UpdateSavedConfiguration(group, identifier, id, data);
		}

		// GET: model/values
		[HttpGet("client")]
		public List<Configuration> GetClientConfigurations() {
			//CheckRight("read");
			var c = new Configurations(instance, currentSession);
			return c.GetClientConfigurations();
		}


		[HttpGet("server")]
		public string GetServerConfigurations() {
			//CheckRight("read");
			var c = new Configurations(instance, currentSession);
			return c.GetServerConfigurations().ToString();
		}


		[HttpPost("client")]
		public void SetClientConfigurations([FromBody]JObject conf) {
			CheckRight("write");
			var c = new Configurations(instance, currentSession);
			c.SaveClientConfigurations(conf);
		}

		[HttpPost("server")]
		public void SetServerConfigurations([FromBody]JObject conf) {
			CheckRight("write");
			var c = new Configurations(instance, currentSession);
			c.SaveServerConfigurations(conf);
		}

		[HttpDelete("server")]
		public void DeleteServerConfigurations() {
			CheckRight("write");
			if (currentSession != null) {

			}
		}

		[HttpDelete("server/{domain}/{category}/{name}")]
		public void DeleteServerConfigurations(string domain, string category, string name) {
			CheckRight("write");
			if (currentSession != null) {

			}
		}
	}
}