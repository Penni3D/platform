using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/integration/Config")]
	public class IntegrationConfigRest : PageBase {
		public IntegrationConfigRest() : base("settings") { }

		// GET: model/values
		[HttpGet("keys")]
		public List<Dictionary<string, object>> Get() {
			var ic = new IntegrationConfig(instance, currentSession);
			return ic.GetKeys();
		}

		[HttpGet("tokens/{key}")]
		public List<Dictionary<string, object>> Get(string key) {
			var ic = new IntegrationConfig(instance, currentSession);
			return ic.GetTokens(key);
		}

		[HttpPut("keys")]
		public List<Dictionary<string, object>> Put([FromBody] List<Dictionary<string, object>> data) {
			CheckRight("write");
			var ic = new IntegrationConfig(instance, currentSession);
			return ic.SaveKeys(data);
		}

		[HttpPut("tokens")]
		public List<Dictionary<string, object>> PutTokens([FromBody] List<Dictionary<string, object>> data) {
			CheckRight("write");
			var ic = new IntegrationConfig(instance, currentSession);
			return ic.SaveTokens(data);
		}


		[HttpPost("keys")]
		public List<Dictionary<string, object>> Post([FromBody] List<Dictionary<string, object>> data) {
			CheckRight("write");
			var ic = new IntegrationConfig(instance, currentSession);
			return ic.AddKeys(data);
		}

		[HttpPost("tokens/{key}")]
		public List<Dictionary<string, object>>
			PostTokens(string key, [FromBody] List<Dictionary<string, object>> data) {
			CheckRight("write");
			var ic = new IntegrationConfig(instance, currentSession);
			return ic.AddTokens(key, data);
		}

		[HttpPost("keys/delete")]
		public void Delete([FromBody] List<int> ids) {
			CheckRight("delete");
			var ic = new IntegrationConfig(instance, currentSession);
			ic.DeleteKeys(ids);
		}

		[HttpPost("tokens/delete")]
		public void DeleteTokens([FromBody] List<int> ids) {
			CheckRight("delete");
			var ic = new IntegrationConfig(instance, currentSession);
			ic.DeleteTokens(ids);
		}
	}
}