﻿using System;

namespace Keto5.x.platform.server.core {
	[Serializable]
	public class DataVal {
		public DataVal() {

		}

		public DataVal(string name, object val) {
			ItemColumnName = name;
			Value = val;
		}

		public string ItemColumnName { get; set; }
		public object Value { get; set; }
	}
}