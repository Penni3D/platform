﻿using System.Linq;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/core/ComparisonTypes")]
	public class ComparisonTypesRest: RestBase {
		[HttpGet("")]
		public ComparisonType[] Get() {
			return Enumerable.ToArray<ComparisonType>(new ComparisonTypes().GetComparisonTypes().Values);
		}
	}
}