﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Authentication = Keto5.x.platform.server.security.Authentication;

namespace Keto5.x.platform.server.core {
	public class LoginRequest {
		public string Username { get; set; }
		public string Password { get; set; }
		public string Domain { get; set; }
		public int TwoFactorCode { get; set; }
	}


	[Route("v1/core/[controller]")]
	public class ProtectedLogin : RestBase {
		[HttpGet("{itemid}")]
		public bool LoginAs(int itemid) {
			var a = new Authentication(instance);
			if (a.authenticateAs(currentSession, itemid) == Authentication.AuthenticationResult.ImpersonateSuccess) {
				a.RevokeAccessInHttpContext(HttpContext, currentSession.item_id, currentSession.token);
				a.GrantAccessInHttpContext(HttpContext);
				Cache.AddOrUpdateDelegate(itemid, currentSession.DelegatorOrItemId);
				return true;
			}

			return false;
		}

		[HttpGet("IDelegateFor")]
		public List<int> IDelegateFor() {
			var a = new Authentication(instance);
			return a.GetUserIdsDelegatedToImpersonateAs(currentSession);
		}

		[HttpGet("UserSessionExists/{itemid}")]
		public bool UserSessionExists(int itemid) {
			return ConnectedUsers.IsUserConnected(itemid);
		}
	}

	[Route("v1/core/[controller]")]
	public class Login : Controller {
		private Dictionary<string, string> Translations;

		public string instance {
			get {
				//Get instance by request's hostname
				var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
				return conbase.GetInstance(Request.Host.Host);
			}
		}

		private string Translate(string key) {
			if (Translations.ContainsKey(key)) return Translations[key];
			return "";
		}

		[HttpGet("LoginConfiguration/{domain}")]
		public Dictionary<string, string> LoginConfiguration(string domain) {
			if (domain == "default") domain = Config.GetDefaultDomain();

			Dictionary<string, string> result;
			result = (Dictionary<string, string>) Cache.Get(instance, domain, "LOGIN_CACHE", "TRANSLATIONS");

			if (result == null) {
				result = new Dictionary<string, string>();
				var default_locale_id = Config.Instances.ContainsKey(instance) ? Config.Instances[instance] : "en-GB";


				result.Add("BasicAuthentication", Config.GetDomainValue(domain, "BasicAuthentication", "Enabled"));
				result.Add("BasicAuthenticationHidden", Config.GetDomainValue(domain, "BasicAuthentication", "Hidden"));

				if (Conv.ToBool(Config.GetValue("Notifications", "Emails")) == false) {
					result.Add("BasicAuthenticationPasswordReset", "False");
				} else {
					result.Add("BasicAuthenticationPasswordReset",
						Config.GetDomainValue(domain, "BasicAuthentication", "PasswordReset"));
				}

				result.Add("AzureADAuthentication",
					Config.GetDomainValue(domain, "AzureADAuthentication", "Enabled"));

				result.Add("SamlAuthentication",
					Config.GetDomainValue(domain, "SAMLAuthentication", "Enabled"));

				result.Add("GoogleAuthentication",
					Config.GetDomainValue(domain, "GoogleAuthentication", "Enabled"));


				var allConfigurations = new Configurations(instance, 0);
				var clientConfigurations = allConfigurations.GetClientConfigurations();
				var header = clientConfigurations
					.FindAll(x =>
						x.Category == "Main application" &&
						x.Name == "Login Header")
					.ToList();
				var content = clientConfigurations
					.FindAll(x =>
						x.Category == "Main application" &&
						x.Name == "Login Content (html)")
					.ToList();

				if (header.Count > 0 && content.Count > 0) {
					result.Add("addTitle", header[0].Value);
					result.Add("addText", content[0].Value);
				} else {
					result.Add("addTitle", "");
					result.Add("addText", "");
				}

				var sso = Config.GetDomainValue("default", "SAMLAuthentication", "SSOUrl");
				if (sso == "") sso = "sso";
				result.Add("ssoUrl", sso);

				var t = new Translations(instance, default_locale_id);
				Translations = t.GetTranslations();

				//Login Page Translations
				result.Add("LOGIN_VIEW_SESSION_EXPIRED", Translate("LOGIN_VIEW_SESSION_EXPIRED"));
				result.Add("LOGIN_FIELD_INCOMPLETE", Translate("LOGIN_FIELD_INCOMPLETE"));
				result.Add("LOGIN_LOGIN", Translate("LOGIN_LOGIN"));
				result.Add("LOGIN_LOGIN_SUCCESS", Translate("LOGIN_LOGIN_SUCCESS"));
				result.Add("LOGIN_LOGIN_FAILED_EMPTY_USERNAME", Translate("LOGIN_LOGIN_FAILED_EMPTY_USERNAME"));
				result.Add("LOGIN_LOGIN_FAILED_EMPTY_PASSWORD", Translate("LOGIN_LOGIN_FAILED_EMPTY_PASSWORD"));
				result.Add("LOGIN_LOGIN_FAILED_INCORRECT_LOGIN", Translate("LOGIN_LOGIN_FAILED_INCORRECT_LOGIN"));
				result.Add("LOGIN_LOGIN_FAILED_AZURE", Translate("LOGIN_LOGIN_FAILED_AZURE"));
				result.Add("LOGIN_USERNAME", Translate("LOGIN_USERNAME"));
				result.Add("LOGIN_PASSWORD", Translate("LOGIN_PASSWORD"));
				result.Add("LOGIN_PASSWORD1", Translate("LOGIN_PASSWORD1"));
				result.Add("LOGIN_PASSWORD2", Translate("LOGIN_PASSWORD2"));
				result.Add("LOGIN_PASSWORDS_NOT_SAME", Translate("LOGIN_PASSWORDS_NOT_SAME"));
				result.Add("LOGIN_PASSWORDS_CHANGED", Translate("LOGIN_PASSWORDS_CHANGED"));
				result.Add("LOGIN_PASSWORDS_CHANGE_FAILED", Translate("LOGIN_PASSWORDS_CHANGE_FAILED"));
				result.Add("LOGIN_SIGN_IN", Translate("LOGIN_SIGN_IN"));
				result.Add("LOGIN_SIGN_IN_KETOAUTH", Translate("LOGIN_SIGN_IN_KETOAUTH"));
				result.Add("LOGIN_SSO", Translate("LOGIN_SSO"));
				result.Add("LOGIN_SSO_TITLE", Translate("LOGIN_SSO_TITLE"));
				result.Add("USER_KETOAUTH_ERROR", Translate("USER_KETOAUTH_ERROR"));
				result.Add("LOGIN_PASSWORD_FORGOTTEN", Translate("LOGIN_PASSWORD_FORGOTTEN"));
				result.Add("LOGIN_CANCEL", Translate("LOGIN_CANCEL"));
				result.Add("LOGIN_RESET", Translate("LOGIN_RESET"));
				result.Add("LOGIN_RESET_DONE", Translate("LOGIN_RESET_DONE"));
				result.Add("LOGIN_RESET_INFO", Translate("LOGIN_RESET_INFO"));
				result.Add("LOGIN_COMPLEXITY",
					Translate("LOGIN_COMPLEXITY") + PasswordValidation.GetValidationRequirementString(t) + " " +
					Translate("LOGIN_COMPLEXITY_END"));
				result.Add("LOGIN_COMPLEXITY_NOT_MET", Translate("LOGIN_COMPLEXITY_NOT_MET"));
				result.Add("LOGIN_LOGIN_FAILED_TWO_FACTOR", Translate("LOGIN_LOGIN_FAILED_TWO_FACTOR"));
				result.Add("LOGIN_FACTORCODE", Translate("LOGIN_FACTORCODE"));
				result.Add("LOGIN_SIGN_IN_TWOFACTOR", Translate("LOGIN_SIGN_IN_TWOFACTOR"));
				result.Add("LOGIN_FAILED_TRIES_EXCEEDED", Translate("LOGIN_FAILED_TRIES_EXCEEDED"));
				Cache.Set(instance, domain, "LOGIN_CACHE", "TRANSLATIONS", result);
			}

			return result;
		}

		[HttpPost("")]
		public string Get([FromBody] LoginRequest authentication) {

			if (authentication == null)
				return Authentication.AuthenticationResult.BasicAuthentication_LoginEmpty.ToString();

			var username = authentication.Username;
			var password = authentication.Password;
			var domain = authentication.Domain;


			if (domain == null) domain = Config.GetDefaultDomain();
			if (instance == null) {
				return "Instance not recognised.";
			}

			//Try authenticate
			var a = new Authentication(instance);
			Authentication.AuthenticationResult result;

			if (Conv.ToBool(Config.GetDomainValue(domain, "LDAPAuthentication", "Enabled")) &&
			    a.LdapAuthentication(username, password, domain) ==
			    Authentication.AuthenticationResult.LDAPAuthentication_Success) {
				result = Authentication.AuthenticationResult.LDAPAuthentication_Success;
				a.GrantAccessInHttpContext(instance, username, HttpContext);
			} else {
				result = a.BasicAuthentication(domain, username, password, HttpContext);
				if (result == Authentication.AuthenticationResult.BasicAuthentication_Success) {
					a.GrantAccessInHttpContext(instance, username, HttpContext);
				} else {
					HttpContext.Session.Clear();
				}
			}

			//Do not show the real reason to the client
			if (result == Authentication.AuthenticationResult.BasicAuthentication_LoginNonExistant ||
			    result == Authentication.AuthenticationResult.BasicAuthentication_PasswordNonMatching) {
				result = Authentication.AuthenticationResult.Failed;
			}

			return result.ToString();
		}


		[HttpPost("KetoAuthRequestPhase1")]
		public Authentication.KetoAuthResponsePhase1 KetoAuthRequest(
			[FromBody] Authentication.KetoAuthRequestPhase1 requestContent) {
			var a = new Authentication(instance);
			return a.KetoAuthRequest(requestContent);
		}

		[HttpPost("TwoFactor")]
		public string TwoFactor([FromBody] LoginRequest authentication) {
			if (authentication == null)
				return Authentication.AuthenticationResult.BasicAuthentication_LoginEmpty.ToString();

			var username = authentication.Username;
			var password = authentication.Password;
			var domain = authentication.Domain;
			var code = authentication.TwoFactorCode;

			if (domain == null) domain = Config.GetDefaultDomain();
			if (instance == null) {
				return "Instance not recognised.";
			}

			//Try authenticate
			var a = new Authentication(instance);
			Authentication.AuthenticationResult result;

			result = a.BasicAuthentication(domain, username, password, HttpContext, code);
			if (result == Authentication.AuthenticationResult.BasicAuthentication_Success) {
				a.GrantAccessInHttpContext(instance, username, HttpContext);
			} else {
				HttpContext.Session.Clear();
			}

			return result.ToString();
		}

		[HttpPost("RequestReset")]
		public string RequestReset([FromBody] string login) {
			if (instance == "") return "Instance not recognised.";
			var host = "https://" + (Conv.ToStr(HttpContext.Request.Host) + (Config.VirtualDirectory == "" ? "/" : Config.VirtualDirectory)).Replace("//","/");
			new Authentication(instance).SendResetPassword(Conv.ToStr(login), host);
			return "";
		}

		[HttpPost("CommitReset/{token}")]
		public string CommitReset(string token, [FromBody] string password) {
			if (instance == "") return "LOGIN_PASSWORDS_CHANGE_FAILED";
			password = HttpUtility.HtmlDecode(password);
			var a = new Authentication(instance);
			return a.changePasswordWithResetToken(token, password);
		}

		[HttpPost("AzureURL")]
		public string AzureURL() {
			if (instance == "") {
				return "Instance not recognised.";
			}

			var a = new Authentication(instance);
			return a.AzureADAuthenticationURL("default");
		}
	}

	[Route("core/[controller]")]
	public class IntegrationLogin : Controller {
		public string instance {
			get {
				//Get instance by request's hostname
				var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
				return conbase.GetInstance(Request.Host.Host);
			}
		}

		[HttpPost("IntegrationToken/{key}")]
		public string IntegrationToken(string key, [FromBody] LoginRequest authentication) {
			if (instance == "") {
				return "Instance not recognised.";
			}

			var a = new Authentication(instance);
			return authentication == null
				? Authentication.AuthenticationResult.IntegrationAuthentication_LoginFailed.ToString()
				: a.GetIntegrationToken(key, authentication.Username, authentication.Password);
		}
	}
}