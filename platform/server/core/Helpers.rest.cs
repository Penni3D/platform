﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/meta/helpers/")]
	public class HelpersRest : RestBase {
		[HttpGet("")]
		public List<HelperModel> Get() {
			var h = new Helpers(instance, currentSession);
			return h.GetHelpers();
		}

		[HttpPost("")]
		public HelperModel Add([FromBody] HelperModel helper) {
			var h = new Helpers(instance, currentSession);
			return h.AddHelper(helper);
		}

		[HttpPut("")]
		public bool Save([FromBody] List<HelperModel> helpers) {
			var h = new Helpers(instance, currentSession);
			return h.SaveHelper(helpers);
		}

		[HttpDelete("{helperId}")]
		public void Delete(int helperId) {
			var h = new Helpers(instance, currentSession);
			h.DeleteHelper(helperId);
		}
	}
}