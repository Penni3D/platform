﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/deploy/InstanceCompare")]
	public class InstanceCompareRest : PageBase {
		public InstanceCompareRest() : base("InstanceCompare") { }

		[HttpGet("instances")]
		public string Get() {
			CheckRight("read");
			var ic = new InstanceCompare(instance, currentSession);
			return ic.GetInstances();
		}

		[HttpGet("instances/{targetDb}")]
		public string GetInstances(string targetDb) {
			CheckRight("read");
			var ic = new InstanceCompare(instance, currentSession);
			return ic.GetInstances(targetDb);
		}

		[HttpGet("getDb")]
		public List<Dictionary<string, string>> GetDb() {
			CheckRight("read");
			var ic = new InstanceCompare(instance, currentSession);
			return ic.GetDb();
		}

		[HttpGet("getDbList")]
		public List<Dictionary<string, object>> GetDbList() {
			CheckRight("read");
			var ic = new InstanceCompare(instance, currentSession);
			return ic.GetDbList();
		}

		[HttpGet("processes/{sourceInstance}")]
		public string GetProcesses(string sourceInstance) {
			CheckRight("read");
			var ic = new InstanceCompare(instance, currentSession);
			return ic.GetProcesses(sourceInstance);
		}

        [HttpGet("progress")]
        public InstanceCompareProgressReport GetCompareProgress() {
            return InstanceCompareProgress.GetProgress();
        }
        
        [HttpGet("init")]
        public bool Init() {
            InstanceCompareProgress.Initialize();
            return true;
        }

        [HttpGet("compareAll/{sourceInstance}/{targetInstance}/{targetDb}/{processes}/{skipProcessPortfoliosDownloadTemplates}/{skipHelps}/{skipConfigs}/{skipRequests}")]
		public Dictionary<string, object> compareAll(string sourceInstance, string targetInstance, string targetDb, string processes, bool skipProcessPortfoliosDownloadTemplates, bool skipHelps, bool skipConfigs, bool skipRequests) {
            CheckRight("read");
			var ic = new InstanceCompare(instance, currentSession);
			var sourceDb = ic.GetsourceDb();
			var retval = ic.CompareAll(sourceInstance, targetInstance, targetDb, processes, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests);
			retval.Add("updates", ic.GetUpdateSql(retval, targetInstance, targetDb, sourceDb, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests));
			return retval;
		}

		[HttpPut("deployChanges/{sourceInstance}/{targetInstance}/{targetDb}/{processes}/{skipProcessPortfoliosDownloadTemplates}/{skipHelps}/{skipConfigs}/{skipRequests}")]
		public List<string> deployChanges(string sourceInstance, string targetInstance, string targetDb, string processes, bool skipProcessPortfoliosDownloadTemplates, bool skipHelps, bool skipConfigs, bool skipRequests) {
            CheckRight("write");
			var ic = new InstanceCompare(instance, currentSession);
			var sourceDb = ic.GetsourceDb();
            InstanceCompareProgress.DeployProgress = 0;
			List<string> result = ic.DeployChanges(sourceInstance, targetInstance, targetDb, processes, sourceDb, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests);
			if (result[0] == "OK" && Conv.ToInt(result[1]) > 0) {
				ic = new InstanceCompare(instance, currentSession);
				result = ic.DeployChanges(sourceInstance, targetInstance, targetDb, processes, sourceDb, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests);
				if (result[0] == "OK" && Conv.ToInt(result[1]) > 0) {
					ic = new InstanceCompare(instance, currentSession);
					result = ic.DeployChanges(sourceInstance, targetInstance, targetDb, processes, sourceDb, skipProcessPortfoliosDownloadTemplates, skipHelps, skipConfigs, skipRequests);
				}
            }
            
            InstanceCompareProgress.SetPhaseProgress("Done");
            
			return result;
		}

		[HttpGet("discoverGUID/{guid_text}/{target_db}")]
		public List<Dictionary<string, object>> DiscoverGUID(string guid_text, string target_db) {
			CheckRight("read");
			var ic = new InstanceCompare(instance, currentSession);
			List<Dictionary<string, object>> results;
			Guid guid;
			if (Guid.TryParse(guid_text, out guid)) {
				results = ic.DiscoverGUID(guid);
				if (target_db != "undefined") {
					foreach (var result in ic.DiscoverGUID(guid, target_db)) {
						results.Add(result);
					}
				}
				if (results.Count == 0) {
					results = new List<Dictionary<string, object>>();
					Dictionary<string, object> guid_info = new Dictionary<string, object>();
					guid_info.Add("status", 1);
					results.Add(guid_info);
				}
			}
			else {
				results = new List<Dictionary<string, object>>();
				Dictionary<string, object> guid_info = new Dictionary<string, object>();
				guid_info.Add("status", 0);
				results.Add(guid_info);
			}
			return results;
		}
	}
}
