﻿using System.Linq;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/meta/Operators")]
	public class OperatorsRest : RestBase {
		[HttpGet("")]
		public Operator[] Get() {
			return Enumerable.ToArray<Operator>(new Operators().GetOperators().Values);
		}
	}
}
