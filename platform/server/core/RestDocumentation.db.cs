﻿using System;
using System.Data;
using System.Collections.Generic;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.portfolios;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core {
	public class RestDocumentation : ConnectionBase {
		private string instance;
		private AuthenticatedSession session;
		private Translations t;

		public RestDocumentation(string instance, AuthenticatedSession session) : base(instance, session) {
			this.instance = instance;
			this.session = session;
			t = new Translations(instance, session, session.locale_id);
		}

		public RestDocs GetRestDocs(Dictionary<string, string> processes, string server_url) {
			var restDocs = new RestDocs();

			var token_row = db.GetDataRow("SELECT TOP 1 [key], token, valid_until FROM instance_integration_tokens WHERE DATEADD(DAY, -1, valid_until) > GETUTCDATE() ORDER BY valid_until DESC");
			var key = "<your integration key>";
			var token = "<your integration token>";
			var token_description = "";
			if (token_row != null) {
				key = Conv.ToStr(token_row["key"]);
				token = Conv.ToStr(token_row["token"]);
				token_description = t.GetTranslation("REST_DOCUMENTATION_VALID_UNTIL") + " " + ((DateTime)Conv.ToDateTime(token_row["valid_until"])).ToString("dd.MM.yyyy h.mm") + " UTC";
			}

			foreach (var process in processes) {
				var restDocsProcess = new RestDocsProcess(t.GetTranslation(process.Value));

				var item_row = db.GetDataRow("SELECT TOP 1 item_id FROM _" + instance + "_" + process.Key);
				var item_id = "<item_id of item>";
				if (item_row != null) {
					item_id = Conv.ToStr(item_row["item_id"]);
				}

				SetDBParam(SqlDbType.NVarChar, "@process", process.Key);
				var portfolio_row = db.GetDataRow("SELECT TOP 1 process_portfolio_id, variable FROM process_portfolios WHERE instance = @instance AND process = @process", DBParameters);
				var portfolio_id = "<process_portfolio_id of portfolio>";
				//var portfolio_name = "<name of portfolio>";
				if (portfolio_row != null) {
					portfolio_id = Conv.ToStr(portfolio_row["process_portfolio_id"]);
					//portfolio_name = t.GetTranslation(Conv.ToStr(portfolio_row["variable"]));
				}

				// Get all items
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_GET"), "POST", key, token, token_description, "{\"processes\":{\"" + process.Key + "\":[]}}", server_url + "integration/WHERE", t.GetTranslation("REST_DOCUMENTATION_TAB_GET_ALL")));

				// Get with filter
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_GET_WITH_FILTER"), "POST", key, token, token_description, "{\"processes\":{\"" + process.Key + "\":[[\"GROUP\",\"item_id\",\"=\",\"val\"," + Conv.ToSql(item_id) + "]]}}", server_url + "integration/WHERE", t.GetTranslation("REST_DOCUMENTATION_TAB_GET_FILTER")));

				// Get by portfolio
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_GET_BY_PORTFOLIO"), "POST", key, token, token_description, "{\"processes\":{\"" + process.Key + "\":[[\"PORFOLIO\",\"" + Conv.ToSql(portfolio_id) + "\"]]}}", server_url + "integration/WHERE", t.GetTranslation("REST_DOCUMENTATION_TAB_GET_PORTFOLIO")));

				// Export portfolio
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_EXPORT_PORTFOLIO"), "GET", key, token, token_description, "", server_url + "integration/exportPortfolio/" + portfolio_id, t.GetTranslation("REST_DOCUMENTATION_TAB_GET_EXPORT")));

				// Export page of portfolio
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_EXPORT_PAGE_OF_PORTFOLIO"), "GET", key, token, token_description, "", server_url + "integration/exportPortfolio/" + portfolio_id + "/5/1", t.GetTranslation("REST_DOCUMENTATION_TAB_EXPORT_PAGE_OF_PORTFOLIO_DESC1")));

				// Update
				var desc = t.GetTranslation("REST_DOCUMENTATION_DESCRIPTION_REPLACE") + " \"<item_id of some item>\" " + t.GetTranslation("REST_DOCUMENTATION_TAB_UPDATE_DESC1");
				desc += "\n" + t.GetTranslation("REST_DOCUMENTATION_DESCRIPTION_REPLACE") + " <some column> " + t.GetTranslation("REST_DOCUMENTATION_TAB_UPDATE_DESC2");
				desc += "\n" + t.GetTranslation("REST_DOCUMENTATION_DESCRIPTION_REPLACE") + " \"<value of some column>\" " + t.GetTranslation("REST_DOCUMENTATION_TAB_UPDATE_DESC3");
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_UPDATE"), "POST", key, token, token_description, "{\"data\":{\"" + process.Key + "\":[{\"item_id\":\"<item_id of some item>\",\"<some column>\":\"<value of some column>\"}]}}", server_url + "integration", desc));

				// Insert
				desc = t.GetTranslation("REST_DOCUMENTATION_DESCRIPTION_REPLACE") + " \"<item_id of some item>\" " + t.GetTranslation("REST_DOCUMENTATION_TAB_INSERT_DESC1");
				desc += "\n" + t.GetTranslation("REST_DOCUMENTATION_DESCRIPTION_REPLACE") + " <some column> " + t.GetTranslation("REST_DOCUMENTATION_TAB_INSERT_DESC2");
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_INSERT"), "POST", key, token, token_description, "{\"data\":{\"" + process.Key + "\":[{\"<some column>\":\"<value of some column>\"}]}}", server_url + "integration", desc));

				// Delete
				desc = t.GetTranslation("REST_DOCUMENTATION_DESCRIPTION_REPLACE") + " \"<item_id of some item>\" " + t.GetTranslation("REST_DOCUMENTATION_TAB_DELETE_DESC1");
				restDocsProcess.item.Add(new RestDocsItem(t.GetTranslation("REST_DOCUMENTATION_TAB_DELETE"), "POST", key, token, token_description, "{\"data\":{\"" + process.Key + "\":[\"<item_id of some item>\"]}}", server_url + "integration/delete", desc));

				restDocs.item.Add(restDocsProcess);
			}
			return restDocs;
		}
	}

	[Serializable]
	public class RestDocs {
		public RestDocsInfo info;
		public List<RestDocsProcess> item;

		public RestDocs() {
			info = new RestDocsInfo();
			item = new List<RestDocsProcess>();
		}
	}

	[Serializable]
	public class RestDocsInfo {
		public string _postman_id;
		public string name;
		public string schema;

		public RestDocsInfo() {
			_postman_id = "";
			name = "Keto";
			schema = "https://schema.getpostman.com/json/collection/v2.1.0/collection.json";
		}
	}

	[Serializable]
	public class RestDocsProcess {
		public string name;
		public List<RestDocsItem> item;

		public RestDocsProcess(string process_name) {
			name = process_name;
			item = new List<RestDocsItem>();
		}
	}

	[Serializable]
	public class RestDocsItem {
		public string name;
		public RestDocsRequest request;
		public List<string> response;

		public RestDocsItem(string name, string method, string key, string token, string token_description, string body, string url, string description) {
			this.name = name;
			request = new RestDocsRequest(method, key, token, token_description, body, url, description);
			response = new List<string>();
		}
	}

	[Serializable]
	public class RestDocsRequest {
		public string method;
		public List<RestDocsHeader> header;
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public RestDocsBody body;
		public RestDocsUrl url;
		public string description;

		public RestDocsRequest(string method, string key, string token, string token_description, string body, string url, string description) {
			this.method = method;
			header = header = new List<RestDocsHeader>();
			header.Add(new RestDocsHeader("Content-Type", "application/json", ""));
			header.Add(new RestDocsHeader("key", key, ""));
			header.Add(new RestDocsHeader("token", token, token_description));
			if (body.Length > 0) {
				this.body = new RestDocsBody(body);
			}
			this.url = new RestDocsUrl(url);
			if (description.Length > 0) {
				this.description = description;
			}
		}
	}

	[Serializable]
	public class RestDocsHeader {
		public string key;
		public string value;
		public string type;
		public string description;

		public RestDocsHeader(string key, string value, string description) {
			this.key = key;
			this.value = value;
			type = "text";
			if (description.Length > 0) {
				this.description = description;
			}
		}
	}

	[Serializable]
	public class RestDocsBody {
		public string mode;
		public string raw;

		public RestDocsBody(string body) {
			mode = "raw";
			raw = body;
		}
	}

	[Serializable]
	public class RestDocsUrl {
		public string raw;
		public string protocol;
		public List<string> host;
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string port;
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public List<string> path;

		public RestDocsUrl(string url) {
			raw = url;
			protocol = url.Split(":")[0];
			host = new List<string>();
			host.AddRange(url.Split("/")[2].Split(":")[0].Split("."));
			if (url.Substring(6).Contains(":")) {
				port = url.Split("/")[2].Split(":")[1];
			}
			int start = url.IndexOf("/", 0);
			start = url.IndexOf("/", start + 1);
			start = url.IndexOf("/", start + 1);
			if (start > -1) {
				path = new List<string>();
				path.AddRange(url.Substring(start + 1).Split("/"));
			}
		}
	}
}