using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.backgroundProcesses;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Task = System.Threading.Tasks.Task;

namespace BackgroundTasksSample.Services {
	#region snippet1

	internal class TimedHostedService : IHostedService, IDisposable {
		private readonly ILogger _logger;
		private Timer _timer;
		private Timer _SQCacheTimer;
		private static List<InstanceSchedule> _s = new List<InstanceSchedule>();
		private static ConnectionBase conBase;
		private static DataTable _schedules;
		private static Dictionary<int, bool> PendingExecutes = new Dictionary<int, bool>();

		private static Dictionary<int, List<InstanceSchedule>> _groupSchedules =
			new Dictionary<int, List<InstanceSchedule>>();

		public TimedHostedService(ILogger<TimedHostedService> logger) {
			_logger = logger;
		}

		public static void ClearCache() {
			_schedules =
				conBase.db.GetDatatable("SELECT * FROM instance_schedules WHERE (disabled <> 1 OR disabled IS NULL) AND group_id IS NULL");
			_s.Clear();
			foreach (DataRow r in _schedules.Rows) {
				_s.Add(new InstanceSchedule(r));
			}
		}

		public Task StartAsync(CancellationToken cancellationToken) {
			//_logger.LogInformation("Timed Background Service is starting.");

			conBase = new ConnectionBase("", DummySession.Create("", ""), null);

			//Get Instance List
			var instances = new List<string>();
			foreach (DataRow instance in conBase.db.GetDatatable("SELECT instance FROM instances", null).Rows) {
				instances.Add(Conv.ToStr(instance["instance"]));
			}

			_schedules =
				conBase.db.GetDatatable("SELECT * FROM instance_schedules WHERE (disabled <> 1 OR disabled IS NULL) AND group_id IS NULL");

			foreach (DataRow r in _schedules.Rows) {
				_s.Add(new InstanceSchedule(r));
			}

			var sql2 = "SELECT group_id FROM instance_schedule_groups WHERE disabled <> 1";

			try {
				foreach (DataRow r in conBase.db.GetDatatable(sql2).Rows) {
					var groupSchedules = new List<InstanceSchedule>();
					_groupSchedules.Add(Conv.ToInt(r["group_id"]), groupSchedules);
					foreach (DataRow r2 in conBase.db.GetDatatable(
						"SELECT isc.name, isc.compare, isc.instance, isc.last_modified, isc.nickname, isc.disabled, isc.group_id, isc.order_no, isc.schedule_id, isg.schedule_json,isc.config_options FROM instance_schedule_groups isg " +
						"INNER JOIN instance_schedules isc ON isc.group_id = isg.group_id " +
						"WHERE isc.group_id = " + Conv.ToInt(r["group_id"]) +
						" AND isc.disabled <> 1 AND isc.disabled IS NOT NULL " +
						"ORDER BY isc.order_no ASC").Rows) {
						_groupSchedules[Conv.ToInt(r["group_id"])].Add(new InstanceSchedule(r2));
					}
				}
			} catch (Exception e) {
				Console.WriteLine(e.Message);
			}

			//Update Subquery Caches
			try {
				_SQCacheTimer = new Timer(DoSubqueryCacheWork, instances, TimeSpan.Zero,
					TimeSpan.FromSeconds(60));
			} catch (Exception e) {
				Diag.Log("?", e, "", 0, Diag.LogCategories.BackgroundTask, Diag.LogTypes.ServerException);
			}

			//Run scheduled bg processes
			try {
				_timer = new Timer(DoWork, null, TimeSpan.Zero,
					TimeSpan.FromSeconds(3300)); //<- nyt rullaa 55min välein
			} catch (Exception e) {
				Diag.Log("?", e, "", 0, Diag.LogCategories.BackgroundTask, Diag.LogTypes.ServerException);
			}

			return Task.CompletedTask;
		}

		private void DoSubqueryCacheWork(object state) {
			var instances = (List<string>) state;
			foreach (var instance in instances) {
				var bg = new SubqueryCacheBackgroundProcess();
				var dummySchedule = new InstanceSchedule();
				dummySchedule.Instance = instance;
				bg.Execute(null, DummySession.Create(dummySchedule), null, instance, 0);
			}
		}

		private void DoWork(object state) {
			var currentHour = DateTime.Now.ToString("HH") + ":00";

			//Schedule suoritetaan JOS sen asetettu kellonaika on sama kuin nykyhetki tai jos suoritus on jäänyt väliin ja
			//suoritus tarvitaan

			foreach (var instanceSchedule in _s) {
				DoWorkProtocol(instanceSchedule, currentHour);
				/*try {
					if (instanceSchedule.ScheduleOptions?.Time == null)
						continue;

					if ((instanceSchedule.ScheduleOptions.Time != currentHour && !HasTimerSkipped(currentHour,
						instanceSchedule)) || !NeedsToBeExecuted(instanceSchedule))
						continue;

					if (instanceSchedule.ScheduleOptions.Slot == 1) {
						RunInstanceSchedule(instanceSchedule);
					} else {
						if (instanceSchedule.ScheduleOptions.Weekday != null &&
						    Conv.ToStr(DateTime.Now.DayOfWeek).ToLower() ==
						    instanceSchedule.ScheduleOptions.Weekday.ToLower()) {
							RunInstanceSchedule(instanceSchedule);
						}
					}
				} catch (Exception e) {
					Diag.Log("?", e, "FAIL IN " + instanceSchedule.ScheduleId, 0, Diag.LogCategories.BackgroundTask, Diag.LogTypes.ServerException);
				}*/
			}

			foreach (var groupsSchedules in _groupSchedules) {
				foreach (var grpSchedule in groupsSchedules.Value) {
					DoWorkProtocol(grpSchedule, currentHour);
				}
			}
		}

		private void DoWorkProtocol(InstanceSchedule instanceSchedule, string currentHour) {
			try {
				if (instanceSchedule.ScheduleOptions?.Time == null)
					return;

				if ((instanceSchedule.ScheduleOptions.Time != currentHour && !HasTimerSkipped(currentHour,
					instanceSchedule)) || !NeedsToBeExecuted(instanceSchedule))
					return;

				if (instanceSchedule.ScheduleOptions.Slot == 1) {
					RunInstanceSchedule(instanceSchedule);
				} else {
					if (instanceSchedule.ScheduleOptions.Weekday != null &&
					    Conv.ToStr(DateTime.Now.DayOfWeek).ToLower() ==
					    instanceSchedule.ScheduleOptions.Weekday.ToLower()) {
						RunInstanceSchedule(instanceSchedule);
					}
				}
			} catch (Exception e) {
				Diag.Log("?", e, "FAIL IN " + instanceSchedule.ScheduleId, 0, Diag.LogCategories.BackgroundTask, Diag.LogTypes.ServerException);
			}
		}


		private bool HasTimerSkipped(string currentHour, InstanceSchedule schedule) {
			conBase.SetDBParam(SqlDbType.Int, "@scheduleId", schedule.ScheduleId);
			if (DateTime.Now.Date == schedule.LastModified.Date) {
				return false;
			}
			var split = currentHour.Split(':');
			var split2 = schedule.ScheduleOptions.Time.Split(':');

			var compareFrom = Conv.ToInt(split[0]);
			var compareTo = Conv.ToInt(split2[0]);

			if (schedule.ScheduleOptions.Slot == 1) {
				if (compareFrom > compareTo) {
					//Tapaus, jossa verrattava aika on aikaisemmin kuin nykyhetki, mutta sama päivä
					return CompareSameDayTimes(schedule.ScheduleId);
				}

				if (compareFrom < compareTo && compareFrom - compareTo < 0) {
					return CompareDifferentDayTimes(schedule.ScheduleId);
				}
			} else {
				var thisWeekDay = DateTime.Now.DayOfWeek;
				//sama viikonpäivä mutta menty kellonajan yli

				if (schedule.ScheduleOptions.Weekday != null &&
				    Conv.ToStr(thisWeekDay).ToLower() == schedule.ScheduleOptions.Weekday.ToLower()) {
					if (compareFrom > compareTo) {
						return CompareSameDayTimes(schedule.ScheduleId);
					}
				}
			}

			return false;
		}

		private bool CompareSameDayTimes(int scheduleId) {
			conBase.SetDBParam(SqlDbType.Int, "@scheduleId", scheduleId);

			//Vertaillaan kahden samalle päivälle osuvan kellonajan juttuja
			conBase.SetDBParam(SqlDbType.NVarChar, "@thisDayAsNo", Conv.ToStr(DateTime.Now.Day));
			conBase.SetDBParam(SqlDbType.NVarChar, "@thisMonth", Conv.ToStr(DateTime.Now.Month));
			return Conv.ToInt(conBase.db.ExecuteScalar(
				"SELECT COUNT(*) FROM instance_schedule_dates WHERE (SELECT DATEPART (day, date)) = @thisDayAsNo AND schedule_id = @scheduleId AND (SELECT DATEPART (month, date)) = @thisMonth",
				conBase.DBParameters)) == 0;
		}

		private bool CompareDifferentDayTimes(int scheduleId) {
			//Vertaillaan kahden eri päivälle osuvan kellonajan juttuja
			var d1 = DateTime.Now;

			var prevDay = Conv.ToInt(d1.AddDays(-1).Day);
			var monthCompare = prevDay > Conv.ToInt(d1.Day) ? Conv.ToInt(d1.AddMonths(-1).Month) : d1.Month;

			conBase.SetDBParam(SqlDbType.Int, "@scheduleId", scheduleId);
			conBase.SetDBParam(SqlDbType.NVarChar, "@day", prevDay);
			conBase.SetDBParam(SqlDbType.NVarChar, "@month", monthCompare);
			return Conv.ToInt(conBase.db.ExecuteScalar(
				"SELECT COUNT(*) FROM instance_schedule_dates WHERE (SELECT DATEPART (day, date)) = @day AND schedule_id = @scheduleId AND (SELECT DATEPART (month, date)) = @month",
				conBase.DBParameters)) == 0;
		}

		private bool ModifiedSameDayAfterLaunchTime(InstanceSchedule schedule) {
			if (schedule.ScheduleOptions.Slot != 1) return false;
			var split = schedule.ScheduleOptions.Time.Split(':');

			var launchTime = Conv.ToInt(split[0]);

			return schedule.LastModified.Hour > launchTime && (DateTime.Now.Date == schedule.LastModified.Date ||
			                                                   DateTime.Now.Date.AddDays(-1) ==
			                                                   schedule.LastModified.Date);
		}

		private bool NeedsToBeExecuted(InstanceSchedule schedule) {
			if (ModifiedSameDayAfterLaunchTime(schedule)) {
				return false;
			}

			//Tähän voisi kenties rakentaa jonkun katsastelun perustuen PendingExecutes -dicktionaryyn, jolloin kantaan ei tarvitsi mennä joka ajolla
			var todayAsDate = DateTime.Now.ToString("d");
			conBase.SetDBParam(SqlDbType.Int, "@scheduleId", schedule.ScheduleId);
			conBase.SetDBParam(SqlDbType.NVarChar, "@executeDate", todayAsDate);

			switch (schedule.ScheduleOptions.Slot) {
				//Case: Once a day. Lets check if already executed today
				case 1:
					if (Conv.ToInt(conBase.db.ExecuteScalar(
						"SELECT COUNT(*) FROM instance_schedule_dates WHERE schedule_id = @scheduleId AND CAST(date as date) = @executeDate",
						conBase.DBParameters)) > 0) {
						return false;
					}

					break;
				//Case: Once a week. Lets check if already executed today
				case 2:
					var weekNumber = Util.GetWeekNumber(DateTime.Now);
					conBase.SetDBParam(SqlDbType.Int, "@weekNumber", weekNumber);
					if (Conv.ToInt(conBase.db.ExecuteScalar(
						"SELECT COUNT(*) FROM instance_schedule_dates WHERE schedule_id = @scheduleId AND (SELECT DATEPART (iso_week, date)) = @weekNumber",
						conBase.DBParameters)) > 0) {
						return false;
					}

					break;
				default:
					return false;
			}

			return true;
		}

		public static BackgroundProcessResult RunInstanceSchedule(InstanceSchedule instanceSchedule) {
			if (PendingExecutes.ContainsKey(instanceSchedule.ScheduleId) &&
			    PendingExecutes[instanceSchedule.ScheduleId]) {
				var failedResult = new BackgroundProcessResult();
				failedResult.Outcome = BackgroundProcessResult.Outcomes.Pending;
				return failedResult;
			}
			if (!(PendingExecutes.ContainsKey(instanceSchedule.ScheduleId))) {
				PendingExecutes.Add(instanceSchedule.ScheduleId, true);
			}

			var bg = RegisteredBackgroundProcesses.GetBackgroundProcess(instanceSchedule.Name);
			if (bg == null) {
				var failedResult = new BackgroundProcessResult();
				failedResult.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return failedResult;
			}

			try {
				bg.LogId = WriteToTaskLog(instanceSchedule.ScheduleId, instanceSchedule.ScheduleOptions.Slot,
					(int)BackgroundProcessResult.Outcomes.Pending);
			} catch (Exception e) {
				Console.WriteLine(e.Message);
			}
			bg.NickName = instanceSchedule.Nickname;
			try {
				var result = bg.Execute(instanceSchedule, DummySession.Create(instanceSchedule, conBase), null,
					instanceSchedule.Instance);
				UpdateTaskLog(bg.LogId, (int) result.Outcome);
				PendingExecutes[instanceSchedule.ScheduleId] = false;
				return result;
			} catch (Exception e) {
				bg.WriteToLog(e);
				UpdateTaskLog(bg.LogId, (int) BackgroundProcessResult.Outcomes.Failed);
				PendingExecutes[instanceSchedule.ScheduleId] = false;
				var failedResult = new BackgroundProcessResult();
				failedResult.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return failedResult;
			}

			//PendingExecutes[instanceSchedule.ScheduleId] = false;
		}

		private static int WriteToTaskLog(int scheduleId, int type, int outcome) {
			var todayAsDate = DateTime.Now;
			conBase.SetDBParam(SqlDbType.Int, "@scheduleId", scheduleId);
			conBase.SetDBParam(SqlDbType.NVarChar, "@executeDate", todayAsDate);
			conBase.SetDBParam(SqlDbType.NVarChar, "@type", type);
			conBase.SetDBParam(SqlDbType.Int, "@outcome", outcome);

			return conBase.db.ExecuteInsertQuery(
				"INSERT INTO instance_schedule_dates (schedule_id, date, outcome, type) VALUES (@scheduleId, @executeDate, @outcome, @type)",
				conBase.DBParameters);
		}

		private static void UpdateTaskLog(int instanceScheduleDatesId, int outcome, Exception e = null) {
			conBase.DBParameters.Clear();
			conBase.SetDBParam(SqlDbType.Int, "@s", instanceScheduleDatesId);
			conBase.SetDBParam(SqlDbType.Int, "@o", outcome);

			var id = conBase.db.ExecuteUpdateQuery(
				"UPDATE instance_schedule_dates SET outcome = @o WHERE schedule_date_id = @s",
				conBase.DBParameters);

			if (e != null) {
				//Insert into instance logs, background_process_id = id
			}
		}

		public Task StopAsync(CancellationToken cancellationToken) {
			_logger.LogInformation("Timed Background Service is stopping.");

			_timer?.Change(Timeout.Infinite, 0);

			return Task.CompletedTask;
		}

		public void Dispose() {
			_timer?.Dispose();
		}
	}

	#endregion
}