using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Threading;
using Keto5.x.platform.server;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.backgroundProcesses;
using Keto5.x.platform.server.security;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Task = System.Threading.Tasks.Task;

namespace BackgroundTasksSample.Services {
	public class HostedServiceContext : BackgroundService {
		private IServiceScopeFactory _serviceScopeFactory;
		private static Dictionary<int, APISaveResult> bgApiResult = new Dictionary<int,APISaveResult> ();

		public HostedServiceContext(IServiceScopeFactory serviceScopeFactory) {
			_serviceScopeFactory = serviceScopeFactory;
		}

		protected override async Task ExecuteAsync(CancellationToken stoppingToken) { }

		protected virtual async Task ExecuteAsync(string bgProcessName) { }


		public static string UpdateNotification(BackgroundProcessBase bg, string instance, AuthenticatedSession session,
			BackgroundProcessResult bgResult, int attachmentId = 0, int index = 0) {
			var strResult = "";
			try {
				var ib = new ItemBase(instance, "notification", session);
				var pb = new ProcessBase(instance, "notification", session);
				var translations = new Translations(instance, session);

				bgApiResult[index].Data["name_variable"] = "{}";
				bgApiResult[index].Data["title_variable"] = "{}";
				bgApiResult[index].Data["body_variable"] = "{}";
				bgApiResult[index].Data["type"] = 1;

				ib.SaveRow(Conv.ToInt(bgApiResult[index].Data["item_id"]), bgApiResult[index]);
				pb.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(bgApiResult[index].Data["item_id"]));
				pb.SetDBParam(SqlDbType.Int, "@selected_item_id", session.item_id);
				pb.SetDBParam(SqlDbType.Int, "@item_column_id", Conv.ToInt(pb.db.ExecuteScalar(
					"SELECT item_column_id FROM item_columns where name = 'user_item_id' AND process = 'notification'")));

				pb.db.ExecuteInsertQuery(
					"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES(@itemId, @item_column_id, @selected_item_id)",
					pb.DBParameters);

				var t = pb.db.GetDataRow(
					"SELECT name_variable, title_variable, body_variable FROM _" + instance +
					"_notification WHERE item_id = @itemId", pb.DBParameters);

				var l = new LanguageFile();
				var result = l.GetLanguageKeyCollectionFromJSON("keto", session.locale_id);

				var bodyText = "";
				var nameText = "";
				var titleText = "";
				var messageAsHtml = "";

				var downloadHere = result.ContainsKey("DOWNLOAD_HERE") ? result["DOWNLOAD_HERE"] : "DOWNLOAD_HERE";

				var d = DateTime.Now.ToString("dd/MM/yyyy HH:mm",new CultureInfo(session.locale_id));
				d = result["BG_MESSAGE_RECEIVED"] + " " + d;

				if (bgResult.Outcome != BackgroundProcessResult.Outcomes.Failed) {
					bodyText = bg != null && bg.FinalizeMessageBody != null &&
					           result.ContainsKey(bg.FinalizeMessageBody)
						? result[bg.FinalizeMessageBody] + ". " + bgResult.Description
						: "No translation found";
					nameText = bg != null && bg.FinalizeMessageName != null &&
					           result.ContainsKey(bg.FinalizeMessageBody)
						? result[bg.FinalizeMessageName]
						: "No translation found";
					titleText = bg != null && bg.FinalizeMessageTitle != null &&
					            result.ContainsKey(bg.FinalizeMessageBody)
						? result[bg.FinalizeMessageTitle]
						: "No translation found";

					var link = attachmentId != 0
						? "<a target='_blank' href='" + bg.BaseLink + "v1/meta/AttachmentsDownload/" + attachmentId +
						  "/0'>" +
						  downloadHere + "</a>"
						: "";

					strResult = bg != null ? bg.BaseLink + "v1/meta/AttachmentsDownload/" + attachmentId + "/0" : "";
					messageAsHtml = "{\"html\":\"" + bodyText + " " + link + " <span style='font-size:10px'><br><br>" + d + "</br></br></span>\"}";
				} else {
					bodyText = result["BG_PROCESS_FAILED"];
					nameText = result["BG_PROCESS_FAILED"];
					titleText = result["BG_PROCESS_FAILED"];
					messageAsHtml = "{\"html\":\"" + bodyText + " <span style='font-size:10px'><br><br>" + d + "</br></br></span>\"}";
					strResult = "FAIL";
				}

				translations.insertOrUpdateInstanceTranslation("notification", Conv.ToStr(t["body_variable"]),
					messageAsHtml);
				translations.insertOrUpdateInstanceTranslation("notification", Conv.ToStr(t["title_variable"]),
					titleText);
				translations.insertOrUpdateInstanceTranslation("notification", Conv.ToStr(t["name_variable"]),
					nameText);
			} catch (Exception e) {
				strResult = "FAIL";
				Console.WriteLine(e);
			}
			bgResult.Description = "";
			return strResult;
		}

		public static string UpdateNotification(InstanceSchedule schedule, string instance,
			AuthenticatedSession session, BackgroundProcessResult bgResult, int index = 0) {
			var strResult = "";
			try {
				var ib = new ItemBase(instance, "notification", session);
				var pb = new ProcessBase(instance, "notification", session);
				var translations = new Translations(instance, session);

				bgApiResult[index].Data["name_variable"] = "{}";
				bgApiResult[index].Data["title_variable"] = "{}";
				bgApiResult[index].Data["body_variable"] = "{}";
				bgApiResult[index].Data["type"] = 1;

				ib.SaveRow(Conv.ToInt(bgApiResult[index].Data["item_id"]), bgApiResult[index]);
				pb.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(bgApiResult[index].Data["item_id"]));

				var t = pb.db.GetDataRow(
					"SELECT name_variable, title_variable, body_variable FROM _" + instance +
					"_notification WHERE item_id = @itemId", pb.DBParameters);

				pb.SetDBParam(SqlDbType.Int, "@selected_item_id", session.item_id);
				pb.SetDBParam(SqlDbType.Int, "@item_column_id", Conv.ToInt(pb.db.ExecuteScalar(
					"SELECT item_column_id FROM item_columns where name = 'user_item_id' AND process = 'notification'")));

				pb.db.ExecuteInsertQuery(
					"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES(@itemId, @item_column_id, @selected_item_id)",
					pb.DBParameters);

				var l = new LanguageFile();
				var result = l.GetLanguageKeyCollectionFromJSON("keto", session.locale_id);

				var bodyText = "";
				var nameText = "";
				var titleText = "";
				var messageAsHtml = "";


				if (bgResult.Outcome != BackgroundProcessResult.Outcomes.Failed) {
					bodyText = bgResult.Description != null ? result[bgResult.Description] : Conv.ToStr(pb.db.ExecuteScalar(
						"SELECT TOP 1 message FROM instance_logs WHERE log_category = 7 ORDER BY instance_log_id desc"));
					 nameText = schedule.Name + " " + result["INSTANCE_SCHEDULE_FINISHED"];
					 titleText = schedule.Name + " " + result["INSTANCE_SCHEDULE_FINISHED"];
					 messageAsHtml = "{\"html\":\"" + bodyText + " " + "\"}";
				} else {
					bodyText = result["BG_PROCESS_FAILED"];
					nameText = result["BG_PROCESS_FAILED"];
					titleText = result["BG_PROCESS_FAILED"];
					messageAsHtml = "{\"html\":\"" + bodyText + "\"}";
					strResult = "FAIL";
				}

				translations.insertOrUpdateInstanceTranslation("notification", Conv.ToStr(t["body_variable"]),
					messageAsHtml);
				translations.insertOrUpdateInstanceTranslation("notification", Conv.ToStr(t["title_variable"]),
					titleText);
				translations.insertOrUpdateInstanceTranslation("notification", Conv.ToStr(t["name_variable"]),
					nameText);
			} catch (Exception e) {
				strResult = "FAIL";
				Console.WriteLine(e.Message + " " + e.StackTrace);
			}
			bgResult.Description = "";
			return strResult;
		}

		public static int CreateNotification(string instance, AuthenticatedSession session, int index = 0) {
			try {
				var ib = new ItemBase(instance, "notification", session);
				var o = new APISaveResult();

				o.Data = new Dictionary<string, object>();

				var data = ib.AddRow(o, 0, "notification");
				if (!bgApiResult.ContainsKey(index)) {
					bgApiResult.Add(index, data);
				} else {
					bgApiResult[index] = data;
				}
				return Conv.ToInt(data.Data["item_id"]);
			} catch (Exception e) {
				Console.WriteLine(e.Message  + " " + e.StackTrace);
				return 0;
			}
		}

		public static void Finalize(BackgroundProcessBase bg, string instance, AuthenticatedSession session,
			int notificationId, BackgroundProcessResult result) {
			var SignalR = new ApiPush(Startup.SignalR);
			var parameters = new List<object>();

			parameters.Add(GetSignalRResult(bg, notificationId, instance, session, result));
			SignalR.SendToAll("NotificationsChanged", parameters);
		}

		public static void Finalize(InstanceSchedule schedule, string instance, AuthenticatedSession session,
			int notificationId, BackgroundProcessResult result, int index = 0) {
			var SignalR = new ApiPush(Startup.SignalR);
			var parameters = new List<object>();

			try {
				parameters.Add(GetSignalRResult(schedule, notificationId, instance, session, result, index));
			} catch (Exception e) {
				Console.WriteLine(e.Message + " " + e.StackTrace);
			}

			SignalR.SendToAll("NotificationsChanged", parameters);
		}

		private static object GetSignalRResult(BackgroundProcessBase bg, int notificationId, string instance,
			AuthenticatedSession session, BackgroundProcessResult result) {
			var r = new Dictionary<string, string> {
				["UserItemId"] = Conv.ToStr(session.item_id),
				["success"] = "true",
				["link"] = "",
				["showtoast"] = "true",
				["name"] = bg.Name,
				["onfinish"] = Conv.ToStr(bg.OnFinish)
			};

			if (bg.Download == 1) {
				try {
					var cb = new ConnectionBase(instance, session);
					cb.SetDBParam(SqlDbType.Int, "@notificationId", notificationId);

					var row = cb.db.GetDataRow("SELECT attachment_id FROM attachments WHERE item_id = @notificationId",
						cb.DBParameters);

					var un = UpdateNotification(bg, instance, session, result, Conv.ToInt(row["attachment_id"]));
					if (un == "FAIL")
						r["success"] = "false";
					else
						r["link"] = un;
					return r;
				} catch (Exception e) {
					Console.WriteLine(e);
				}
			}

			r["success"] = UpdateNotification(bg, instance, session, result) == "FAIL" ? "false" : "true";
			return r;
		}

		private static object GetSignalRResult(InstanceSchedule schedule, int notificationId, string instance,
			AuthenticatedSession session, BackgroundProcessResult result, int index = 0) {

			var r = new Dictionary<string, string> {
				["Notificationid"] = Conv.ToStr(notificationId),
				["UserItemId"] = Conv.ToStr(session.item_id),
				["link"] = "",
				["success"] = "true",
				["showtoast"] = "true"
			};

			try {
				var un = UpdateNotification(schedule, instance, session, result, index) == "FAIL" ? "false" : "true";
				if (un == "FAIL")
					r["success"] = "false";
			} catch (Exception e) {
				Console.WriteLine(e);
				r["success"] = "false";
			}

			return r;
		}

		public static BackgroundProcessResult DoWork(BackgroundProcessBase bg, AuthenticatedSession session,
			Dictionary<string, object> settings, string instance, int notificationId) {
			try {
				return bg.Execute(new InstanceSchedule(), session, settings, instance, notificationId);
			} catch (Exception e) {
				bg.WriteToLog(e);
				var r = new BackgroundProcessResult();
				r.Outcome = BackgroundProcessResult.Outcomes.Failed;
				r.Description = Conv.ToStr(e);
				return r;
			}
		}

		public static BackgroundProcessResult DoWork(InstanceSchedule instanceSchedule) {
			return TimedHostedService.RunInstanceSchedule(instanceSchedule);
		}
	}

	public interface IStrategy {
		Task ExecuteAsync();
	}

	public class AsyncTask : IStrategy {
		public AsyncTask() { }

		public async Task ExecuteAsync(BackgroundProcessBase bg, AuthenticatedSession session,
			Dictionary<string, object> settings, string instance) {
			var notificationId = HostedServiceContext.CreateNotification(instance, session);
			var result = await Task.Run(() =>
				HostedServiceContext.DoWork(bg, session, settings, instance, notificationId));
			HostedServiceContext.Finalize(bg, instance, session, notificationId, result);
		}

		public async Task ExecuteAsync(InstanceSchedule instanceSchedule, AuthenticatedSession session, int index = 0) {
			var notificationId = HostedServiceContext.CreateNotification(instanceSchedule.Instance, session, index);
			var result = await Task.Run(() => HostedServiceContext.DoWork(instanceSchedule));
			HostedServiceContext.Finalize(instanceSchedule, instanceSchedule.Instance, session, notificationId, result, index);
		}

		public Task ExecuteAsync() {
			throw new NotImplementedException();
		}
	}
}