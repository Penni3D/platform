using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DrawingCore;
using System.DrawingCore.Drawing2D;
using System.DrawingCore.Imaging;
using System.IO;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.processes.accessControl;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using nClam;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.core {
	public class Attachment {
		public Attachment() { }

		public Attachment(DataRow row, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			AttachmentId = Conv.ToInt(row["attachment_id"]);
			ItemId = Conv.ToInt(row["item_id"]);
			ItemColumnId = Conv.ToInt(row["item_column_id"]);
			ParentId = Conv.ToStr(row["parent_id"]);
			ControlName = Conv.ToStr(row["control_name"]);
			Name = Conv.ToStr(row["name"]);
			Url = Conv.ToStr(row["url"]);
			ArchiveStart = Conv.ToDateTime(row["archive_start"]);
		}

		public int AttachmentId { get; set; }
		public int? ItemId { get; set; }
		public int? ItemColumnId { get; set; }
		public string ParentId { get; set; }
		public string ControlName { get; set; }
		public byte Type { get; set; }
		public string Name { get; set; }
		public string Url { get; set; }
		public DateTime? ArchiveStart { get; set; }
		public AttachmentData AttachmentData { get; set; }
	}

	public class AttachmentData {
		public AttachmentData(DataRow row, string alias = "", bool noData = false) {
			if (alias.Length > 0) alias = alias + "_";

			if (row != null) {
				Filename = Conv.ToStr(row["filename"]);
				Name = Conv.ToStr(row["name"]);
				ContentType = Conv.ToStr(row["content_type"]);
				Filesize = Conv.ToInt(row["filesize"]);
				if (row.Table.Columns.Contains("has_thumbnail")) {
					HasThumbnail = Conv.ToBool(row["has_thumbnail"]);
				}

				if (!noData) Filedata = (byte[]) row["filedata"];
			} else {
				Name = "Not Found";
			}
		}

		public AttachmentData(DataRow row, int type, string alias = "", bool noData = false) {
			if (alias.Length > 0) alias += "_";

			if (row != null) {
				Filename = Conv.ToStr(row["filename"]);
				Name = Conv.ToStr(row["name"]);
				ContentType = Conv.ToStr(row["content_type"]);
				Filesize = Conv.ToInt(row["filesize"]);
				if (row.Table.Columns.Contains("has_thumbnail")) {
					HasThumbnail = Conv.ToBool(row["has_thumbnail"]);
				}

				if (noData)
					return;

				switch (type) {
					case 0:
						if (row["filedata"] == DBNull.Value) return;
						Filedata = (byte[]) row["filedata"];
						break;
					case 1:
						if (row["thumbnail"] == DBNull.Value) return;
						Filedata = (byte[]) row["thumbnail"];
						break;
					case 2:
						if (row["thumbnail_small"] == DBNull.Value) return;
						Filedata = (byte[]) row["thumbnail_small"];
						break;
					case 3:
						if (row["thumbnail_tiny"] == DBNull.Value) return;
						Filedata = (byte[]) row["thumbnail_tiny"];
						break;
					default:
						if (row["filedata"] == DBNull.Value) return;
						Filedata = (byte[]) row["filedata"];
						break;
				}
			} else {
				Name = "Not Found";
			}
		}

		public string Filename { get; set; }
		public string Name { get; set; }
		public string ContentType { get; set; }
		public int Filesize { get; set; }
		public bool HasThumbnail { get; set; }
		public byte[] Filedata { get; set; }
	}

	public class Attachments : ConnectionBase {
		private readonly int _currentUSerId;
		private readonly AuthenticatedSession _session;

		public Attachments(string instance, AuthenticatedSession session) : base(instance, session) {
			_currentUSerId = session.item_id;
			_session = session;
		}

		public class UploadFile {
			public string Name { get; set; }
			public int Length => FileBytes.Length;
			public string ContentType { get; set; }
			public byte[] FileBytes { get; set; }
			public Image img { get; set; }
			public string type { get; set; } = "file";
		}

		public class UploadForm {
			public string itemColumnId { get; set; }
			public string itemId { get; set; }
			public string parentId { get; set; }
			public string controlName { get; set; }
			public string name { get; set; }
			public int crop { get; set; }
		}

		public List<Attachment> UploadFiles(IFormCollection form) {
			var f = new UploadForm {
				name = Conv.ToStr(form["name"]) == "null" ? "" : Conv.ToStr(form["name"]),
				controlName = Conv.ToStr(form["controlName"]) == "null" ? "" : Conv.ToStr(form["controlName"]),
				itemId = Conv.ToStr(form["itemId"]) == "null" ? "" : Conv.ToStr(form["itemId"]),
				parentId = Conv.ToStr(form["parentId"]) == "null" ? "" : Conv.ToStr(form["parentId"]),
				itemColumnId = Conv.ToStr(form["itemColumnId"]) == "null" ? "" : Conv.ToStr(form["itemColumnId"]),
				crop = Conv.ToStr(form["crop"]) == "null" ? 0 : Conv.ToInt(form["crop"])
			};

			var files = new List<UploadFile>();
			foreach (var file in form.Files) {
				var newFile = new UploadFile {Name = file.FileName, ContentType = file.ContentType, type = file.Name};
				var s = file.OpenReadStream();
				var br = new BinaryReader(s);
				if (file.Name == "thumbnail") newFile.img = Image.FromStream(s);
				newFile.FileBytes = br.ReadBytes((int) file.Length);
				files.Add(newFile);

				if ((f.crop == 2 || f.crop == 0) && file.ContentType.In("image/png", "image/x-png", "image/jpeg", "image/gif", "image/bmp", "image/tiff")) {
					var thumbFile = new UploadFile();
					thumbFile.type = "thumbnail";
					thumbFile.Name = file.FileName + " (thumbnail)";
					thumbFile.ContentType = file.ContentType;
					thumbFile.img = Image.FromStream(s);
					thumbFile.FileBytes = br.ReadBytes((int)file.Length);
					files.Add(thumbFile);
				}
			}

			return UploadFiles(f, files);
		}

		public List<Attachment> UploadFiles(UploadForm form, UploadFile file) {
			return UploadFiles(form, new List<UploadFile> {file});
		}

		public List<Attachment> UploadFiles(UploadForm form, List<UploadFile> files) {
			if (form.itemId != "" && form.itemColumnId != "" && Conv.ToInt(form.itemColumnId) != 0)
				EnsureRight(Conv.ToInt(form.itemId), Conv.ToInt(form.itemColumnId), "meta.write");

			var retval = new List<Attachment>();
			var id = 0;
			var c = new Connections(Config.GetValue("HostingEnvironment", "AttachmentConnectionString"),
				_currentUSerId);

			SetDBParam(SqlDbType.Int, "@itemId",
				form.itemId == "" ? Conv.IfNullOrEmptyThenDbNull(form.itemId) : Conv.ToInt(form.itemId));
			SetDBParam(SqlDbType.Int, "@itemColumnId",
				form.itemColumnId == ""
					? Conv.IfNullOrEmptyThenDbNull(form.itemColumnId)
					: Conv.ToInt(form.itemColumnId));
			SetDBParam(SqlDbType.NVarChar, "@parentId",
				form.parentId == "" ? Conv.IfNullOrEmptyThenDbNull(form.parentId) : Conv.ToStr(form.parentId));

			SetDBParam(SqlDbType.NVarChar, "@controlName", Conv.IfNullOrEmptyThenDbNull(Conv.ToStr(form.controlName)));
			SetDBParam(SqlDbType.NVarChar, "@name", Conv.IfNullOrEmptyThenDbNull(Conv.ToStr(form.name)));

			var dc = new Connections();
			dc.SetAttachmentsDb();

			foreach (var file in files) {
				switch (file.type) {
					case "file":
						if (!CheckAttachmentFileType(file.Name)) continue;
						if (!ScanFile(file.Name, file.FileBytes)) continue;

						id = db.ExecuteInsertQuery(
							"INSERT INTO attachments (instance, item_id, item_column_id, parent_id, control_name, type, name) VALUES(@instance, @itemId, @itemColumnId, @parentId, @controlName, 0, @name)",
							DBParameters);

						if (Conv.ToBool(Config.GetValue("AttachmentSettings", "ImageProcessExifRotation")) == true) {
							// exif image rotation handling
							if (new string[] {
								"image/png", "image/x-png", "image/jpeg", "image/pjpeg", "image/gif", "image/bmp",
								"image/tiff"
							}.Contains(file.ContentType.ToLower())) {
								// try to find exif data for image rotation and rotate the image before save. This way any displaying methods for images/thumbnails won't have to do it, however any that have or will slip through are not affected and might need to be parsed later.
								Image uploaded_image = Image.FromStream(new System.IO.MemoryStream(file.FileBytes));

								bool rotation_spent = false;
								foreach (PropertyItem prop in uploaded_image.PropertyItems) {
									bool property_omit = false;
									if (prop.Id == 274 || prop.Id == 5029) {
										if (rotation_spent == false && prop.Value.Length > 0)
											switch (prop.Value[0]) {
												case 2:
													uploaded_image.RotateFlip(RotateFlipType.RotateNoneFlipX);
													rotation_spent = true;
													break;
												case 3:
													uploaded_image.RotateFlip(RotateFlipType.Rotate180FlipNone);
													rotation_spent = true;
													break;
												case 4:
													uploaded_image.RotateFlip(RotateFlipType.Rotate180FlipX);
													rotation_spent = true;
													break;
												case 5:
													uploaded_image.RotateFlip(RotateFlipType.Rotate90FlipX);
													rotation_spent = true;
													break;
												case 6:
													uploaded_image.RotateFlip(RotateFlipType.Rotate90FlipNone);
													rotation_spent = true;
													break;
												case 7:
													uploaded_image.RotateFlip(RotateFlipType.Rotate270FlipX);
													rotation_spent = true;
													break;
												case 8:
													uploaded_image.RotateFlip(RotateFlipType.Rotate270FlipNone);
													rotation_spent = true;
													break;
											}

										if (rotation_spent)
											property_omit = true;
									}

									if (property_omit)
										uploaded_image.RemovePropertyItem(prop.Id);
								}

								if (rotation_spent) {
									ImageFormat image_remade_format = ImageFormat.Png;
									switch (file.ContentType.ToLower()) {
										case "image/png":
										case "image/x-png":
											image_remade_format = ImageFormat.Png;
											break;
										case "image/jpeg":
										case "image/pjpeg":
											image_remade_format = ImageFormat.Jpeg;
											break;
										case "image/gif":
											image_remade_format = ImageFormat.Gif;
											break;
										case "image/bmp":
											image_remade_format = ImageFormat.Bmp;
											break;
										case "image/tiff":
											image_remade_format = ImageFormat.Tiff;
											break;
									}

									using (MemoryStream ms = new MemoryStream()) {
										uploaded_image.Save(ms, image_remade_format);
										file.FileBytes = ms.ToArray();
									}
								}
							} // /exif image rotation handling
						}

						if (Conv.ToInt(Config.GetValue("AttachmentSettings", "ImageMaxDimension")) > 0) {
							if (new string[]
									{"image/png", "image/x-png", "image/jpeg", "image/gif", "image/bmp", "image/tiff"}
								.Contains(file.ContentType.ToLower())) {
								Image uploaded_image = Image.FromStream(new MemoryStream(file.FileBytes));
								double width_overbounds =
									(Conv.ToInt(Config.GetValue("AttachmentSettings", "ImageMaxDimension")) > 0
										? Conv.ToDouble(Config.GetValue("AttachmentSettings", "ImageMaxDimension")) /
										  uploaded_image.Width
										: 1);
								double height_overbounds =
									(Conv.ToInt(Config.GetValue("AttachmentSettings", "ImageMaxDimension")) > 0
										? Conv.ToDouble(Config.GetValue("AttachmentSettings", "ImageMaxDimension")) /
										  uploaded_image.Height
										: 1);
								double smaller_overbounds = (width_overbounds < height_overbounds
									? width_overbounds
									: height_overbounds);
								if (smaller_overbounds > 1.0) smaller_overbounds = 1;
								if (smaller_overbounds < 1.0) {
									int height_remade =
										Conv.ToInt(Math.Round(uploaded_image.Height * smaller_overbounds));
									int width_remade =
										Conv.ToInt(Math.Round(uploaded_image.Width * smaller_overbounds));

									// For the unknown reason first resized jpg will be corrupted - JKa 12.2.2021
									if (file.ContentType.ToLower() == "image/jpeg") ResizeImage(uploaded_image, 1, 1, file.ContentType.ToLower());

									file.FileBytes = ResizeImage(uploaded_image, width_remade, height_remade,
										file.ContentType.ToLower());
								}
							}
						}

						SetDBParam(SqlDbType.Int, "@attachmentId", id);
						SetDBParam(SqlDbType.NVarChar, "@filename", file.Name);
						SetDBParam(SqlDbType.NVarChar, "@contentType", file.ContentType);
						SetDBParam(SqlDbType.Int, "@filesize", file.Length);
						SetDBParam(SqlDbType.Binary, "@filedata", file.FileBytes);

						dc.ExecuteInsertQuery(
							"INSERT INTO attachments_data (attachment_id, filename, content_type, filesize, filedata) VALUES(@attachmentId, @filename, @contentType, @filesize, @filedata) ",
							DBParameters);

						retval.Add(GetAttachment(id));
						break;
					case "thumbnail":
						if (id > 0) {
							if (form.crop == 2) { // automatic crop
								var original_image_info = GetAttachment(id);
								byte[] fileBytes = (byte[])dc.ExecuteScalar("SELECT filedata FROM attachments_data WHERE attachment_id = " + id);
								Image original_image = Image.FromStream(new MemoryStream(fileBytes));

								int source_smaller_dim = (original_image.Width < original_image.Height ? original_image.Width : original_image.Height);
								int source_start_height = (original_image.Height - source_smaller_dim) / 2;
								int source_start_width = (original_image.Width - source_smaller_dim) / 2;

								Bitmap dest_cropped = new Bitmap(160, 160);
								using (Graphics g = Graphics.FromImage(dest_cropped))
									g.DrawImage(original_image, new Rectangle(0, 0, dest_cropped.Width, dest_cropped.Height), new Rectangle(source_start_width, source_start_height, source_smaller_dim, source_smaller_dim), GraphicsUnit.Pixel);
								
								var width = dest_cropped.Width;
								var height = dest_cropped.Height;

								// For the unknown reason first resized jpg will be corrupted - JKa 12.2.2021
								if (original_image_info.AttachmentData.ContentType == "image/jpeg") ResizeImage(dest_cropped, 1, 1, original_image_info.AttachmentData.ContentType);

								SetDBParam(SqlDbType.Int, "@attachmentId", id);
								SetDBParam(SqlDbType.Binary, "@filedata1",
									ResizeImage(dest_cropped, width, height, original_image_info.AttachmentData.ContentType));
								SetDBParam(SqlDbType.Binary, "@filedata2",
									ResizeImage(dest_cropped, width / 2, height / 2, original_image_info.AttachmentData.ContentType));
								SetDBParam(SqlDbType.Binary, "@filedata3",
									ResizeImage(dest_cropped, width / 4, height / 4, original_image_info.AttachmentData.ContentType));

								dc.ExecuteInsertQuery(
									"UPDATE attachments_data SET thumbnail = @filedata1, thumbnail_small = @filedata2, thumbnail_tiny = @filedata3 WHERE attachment_id = @attachmentId",
									DBParameters);

							} else { // user cropped image
								if (!ScanFile(file.Name, file.FileBytes)) continue;

								var width = file.img.Width;
								var height = file.img.Height;

								// For the unknown reason first resized jpg will be corrupted - JKa 12.2.2021
								if (file.ContentType == "image/jpeg") ResizeImage(file.img, 1, 1, file.ContentType);

								SetDBParam(SqlDbType.Int, "@attachmentId", id);
								SetDBParam(SqlDbType.Binary, "@filedata1",
									ResizeImage(file.img, width, height, file.ContentType));
								SetDBParam(SqlDbType.Binary, "@filedata2",
									ResizeImage(file.img, width / 2, height / 2, file.ContentType));
								SetDBParam(SqlDbType.Binary, "@filedata3",
									ResizeImage(file.img, width / 4, height / 4, file.ContentType));

								dc.ExecuteInsertQuery(
									"UPDATE attachments_data SET thumbnail = @filedata1, thumbnail_small = @filedata2, thumbnail_tiny = @filedata3 WHERE attachment_id = @attachmentId",
									DBParameters);

							}

						}

						break;
				}
			}

			return retval;
		}

		private readonly string[] _blackList = {
			"exe", "cgi", "php", "rb", "pl", "sh", "smx", "jsp", "lasso", "msi", "386", "app", "asp", "bat", "chm",
			"com", "cmd", "crt", "hta", "req", "vbs", "php2", "php3", "hlp", "msp", "py", "swf", "html", "htm",
			"js"
		};

		private bool CheckAttachmentFileType(string filename) {
			if (!filename.Contains(".")) return false;
			var extension = filename.Split(".")[^1];
			return !_blackList.Contains(extension);
		}

		private bool ScanFile(string name, byte[] filedata) {
			var clamAvCheckType = Conv.ToInt(Config.GetValue("FileScan", "Type"));

			if (clamAvCheckType == 0) return true;
			var url = Conv.ToStr(Config.GetValue("FileScan", "Url"));
			var port = Conv.ToInt(Config.GetValue("FileScan", "Port"));

			var clam = new ClamClient(url, port) {
				MaxStreamSize = 1073741824
			};


			try {
				var scanResult = clam.SendAndScanFileAsync(filedata).Result;
				switch (scanResult.Result) {
					case ClamScanResults.Clean:
						return true;
					case ClamScanResults.VirusDetected:
						var e = new CustomArgumentException(
							"File did not pass security check (" + scanResult.InfectedFiles.First().VirusName +
							"), file: " + name,
							"SECURITY_CHECK_FAILED");
						Diag.Log(instance, e, _currentUSerId);
						throw e;
					case ClamScanResults.Error:
						Diag.Log(instance, "File failed to check. userId: " + _currentUSerId + "  FileName: " + name,
							_currentUSerId, Diag.LogSeverities.Error);
						break;
				}
			} catch (Exception ex) {
				if (ex.GetType() != typeof(CustomArgumentException)) {
					var e = new CustomArgumentException(
						"File did not pass security check (Security service could not be contacted - " + ex.Message +
						")",
						"SECURITY_CHECK_FAILED");
					Diag.Log(instance, ex, _currentUSerId);
					throw e;
				}

                throw;
            }

			return false;
		}

		public Attachment GetAttachment(int attachmentId) {
			SetDBParam(SqlDbType.Int, "@attachmentId", attachmentId);

			var d = db.GetDataRow(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM attachments WHERE instance = @instance AND attachment_id = @attachmentId AND type = 0",
				DBParameters);

			if (d == null)
				throw new CustomArgumentException("Attachment Cannot be found with attachment_id " + attachmentId);

			var itemId = Conv.ToInt(d["item_id"]);
			var itemColumnId = Conv.ToInt(d["item_column_id"]);
			if (itemId > 0 && itemColumnId > 0) EnsureRight(itemId, itemColumnId, "meta.read");

			var a = new Attachment(d);
			var dc = new Connections();
			dc.SetAttachmentsDb();
			a.AttachmentData =
				new AttachmentData(
					dc.GetDataRow(
						"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, '" +
						Conv.Escape(Conv.ToStr(d["name"])) +
						"' AS name FROM attachments_data WHERE attachment_id = @attachmentId", DBParameters), "", true);

			return a;
		}

		public List<Attachment> GetAttachments(string parentId, string controlName) {
			var retval = new List<Attachment>();

			SetDBParam(SqlDbType.NVarChar, "@parentId", parentId);
			SetDBParam(SqlDbType.NVarChar, "@controlName", controlName);

			foreach (DataRow row in db.GetDatatable(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM attachments WHERE instance = @instance AND parent_id = @parentId AND control_name = @controlName AND type = 0 ORDER BY attachment_id ASC",
				DBParameters).Rows) {
				var a = new Attachment(row);
				var dc = new Connections();
				dc.SetAttachmentsDb();

				SetDBParam(SqlDbType.Int, "@attachmentId", a.AttachmentId);
				a.AttachmentData =
					new AttachmentData(
						dc.GetDataRow(
							"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, '" +
							Conv.Escape(Conv.ToStr(row["name"])) +
							"' AS name FROM attachments_data WHERE attachment_id = @attachmentId", DBParameters), "",
						true);

				retval.Add(a);
			}

			return retval;
		}

		public void DeleteAttachment(int attachmentId) {
			SetDBParam(SqlDbType.Int, "@attachmentId", attachmentId);
			var d = db.GetDataRow(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM attachments WHERE instance = @instance AND attachment_id = @attachmentId",
				DBParameters);

			if (d != null) {
				var itemId = Conv.ToInt(d["item_id"]);
				var itemColumnId = Conv.ToInt(d["item_column_id"]);
				if (itemId > 0 && itemColumnId > 0) EnsureRight(itemId, itemColumnId, "meta.write");
			}

			var dc = new Connections();
			dc.SetAttachmentsDb();

			db.ExecuteDeleteQuery(
				"DELETE FROM attachments WHERE instance = @instance AND attachment_id = @attachmentId", DBParameters);
			SetDBParam(SqlDbType.Int, "@attachmentId", attachmentId);
		}

		public void EnsureRight(int attachmentId, string Right,DateTime? archiveDate = null) {
			SetDBParam(SqlDbType.Int, "@attachmentId", attachmentId);
			DataRow d;
			if (archiveDate == null) {
				d = db.GetDataRow(
					"SELECT item_id, item_column_id, parent_id, control_name FROM attachments WHERE instance = @instance AND attachment_id = @attachmentId",
					DBParameters);
			}
			else {
				SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
				d = db.GetDataRow(
					"SELECT item_id, item_column_id, parent_id, control_name FROM get_attachments(@archiveDate) WHERE instance = @instance AND attachment_id = @attachmentId",
					DBParameters);
			}

			if (d == null)
				throw new CustomArgumentException("Attachment Cannot be found with attachment_id " + attachmentId);

			var itemId = Conv.ToInt(d["item_id"]);
			var itemColumnId = Conv.ToInt(d["item_column_id"]);
			if (itemId > 0) EnsureRight(itemId, itemColumnId, Right);
		}

		private void EnsureRight(int parentId, string ControlName, string Right) {
			//TODO: Implement rights check for control name based attachments and links
		}

		private void EnsureRight(int itemId, int itemColumnId, string Right) {
			var statesToCheck = new List<string> {Right};
			if (Right == "meta.read") statesToCheck.Add("meta.write");

			if (itemColumnId == 0) {
				SetDBParam(SqlDbType.Int, "@itemId", itemId);
				var process = Conv.ToStr(db.ExecuteScalar("SELECT process FROM items WHERE item_id = @itemId", DBParameters));
				var states = new States(_session.instance, _session);
				var fStates = states.GetEvaluatedStates(process, "meta", itemId, statesToCheck);
				if (!fStates.HasStateOnAnyLevel(statesToCheck))
					throw new CustomPermissionException("USER IS NOT AUTHORISED TO '" + Right.ToUpper() + "' ATTACHMENT");
			}
			else {
				SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
				var ic = new ItemColumns(_session.instance, "", _session).GetItemColumn(itemColumnId);
				var dbi = new DatabaseItemService(_session.instance, ic.Process, _session);
				var userCols = dbi.GetUserCols(itemId, 1, statesToCheck.ToArray());
				if (!userCols.Contains(ic.Name))
					throw new CustomPermissionException("USER IS NOT AUTHORISED TO '" + Right.ToUpper() + "' ATTACHMENT");
			}
		}

		public List<Attachment> GetAttachments(int itemId, int itemColumnId) {
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			
			var rows = db.GetDatatable(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM attachments WHERE instance = @instance AND item_id = @itemId AND item_column_id = @itemColumnId AND type = 0 ORDER BY attachment_id ASC",
				DBParameters).Rows;
			
			var retval = new List<Attachment>();
			
			if (rows.Count == 0) return retval;

			//Check rights if there are attachments for performance
			EnsureRight(itemId, itemColumnId, "meta.read");

			foreach (DataRow row in rows) {
				var a = new Attachment(row);
				var dc = new Connections();
				dc.SetAttachmentsDb();

				SetDBParam(SqlDbType.Int, "@attachmentId", a.AttachmentId);
				a.AttachmentData =
					new AttachmentData(
						dc.GetDataRow(
							"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, '" +
							Conv.Escape(Conv.ToStr(row["name"])) +
							"' AS name FROM attachments_data WHERE attachment_id = @attachmentId", DBParameters), "",
						true);

				retval.Add(a);
			}

			return retval;
		}
		
		public List<Attachment> GetArchiveAttachments(int itemId, int itemColumnId, DateTime archiveDate) {
			var retval = new List<Attachment>();

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
			var rows = db.GetDatatable(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM get_attachments(@archiveDate) WHERE instance = @instance AND item_id = @itemId AND item_column_id = @itemColumnId AND type = 0",
				DBParameters).Rows;
			
			if (rows.Count == 0) return retval;
			
			EnsureRight(itemId, itemColumnId, "meta.read");
			
			foreach (DataRow row in rows) {
				var a = new Attachment(row);
				var dc = new Connections();
				dc.SetAttachmentsDb();

				SetDBParam(SqlDbType.Int, "@attachmentId", a.AttachmentId);
				var ad = dc.GetDataRow(
					"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, '" +
					Conv.Escape(Conv.ToStr(row["name"])) +
					"' AS name FROM attachments_data WHERE attachment_id = @attachmentId", DBParameters);
				if (ad != null) {
					a.AttachmentData = new AttachmentData(ad, "", true);
				}

				retval.Add(a);
			}

			return retval;
		}

		public Attachment GetLink(int attachmentId) {
			SetDBParam(SqlDbType.Int, "@attachmentId", attachmentId);

			var d = db.GetDataRow(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM attachments WHERE instance = @instance AND attachment_id = @attachmentId AND type = 1",
				DBParameters);

			if (d == null)
				throw new CustomArgumentException("Attachment Cannot be found with attachment_id " + attachmentId);

			var itemId = Conv.ToInt(d["item_id"]);
			var itemColumnId = Conv.ToInt(d["item_column_id"]);
			if (itemId > 0 && itemColumnId > 0) EnsureRight(itemId, itemColumnId, "meta.read");

			var a = new Attachment(d);

			return a;
		}

		public List<Attachment> GetLinks(int itemId, int itemColumnId) {
			var retval = new List<Attachment>();
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			var rows = db.GetDatatable(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM attachments WHERE instance = @instance AND item_id = @itemId AND item_column_id = @itemColumnId AND type = 1",
				DBParameters).Rows;

			if (rows.Count == 0) return retval;
			
			EnsureRight(itemId, itemColumnId, "meta.read");

			foreach (DataRow row in rows) {
				retval.Add(new Attachment(row));
			}

			return retval;
		}

		public List<Attachment> GetArchiveLinks(int itemId, int itemColumnId, DateTime archiveDate) {
			var retval = new List<Attachment>();
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
			
			var rows = db.GetDatatable(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM get_attachments(@archiveDate)  WHERE instance = @instance AND item_id = @itemId AND item_column_id = @itemColumnId AND type = 1",
				DBParameters).Rows;

			if (rows.Count == 0) return retval;
			
			EnsureRight(itemId, itemColumnId, "meta.read");

			foreach (DataRow row in rows) {
				retval.Add(new Attachment(row));
			}

			return retval;
		}

		public List<Attachment> GetLinks(int parentId, string controlName) {
			EnsureRight(parentId, controlName, "meta.read");

			var retval = new List<Attachment>();
			SetDBParam(SqlDbType.Int, "@parentId", parentId);
			SetDBParam(SqlDbType.NVarChar, "@controlName", controlName);

			foreach (DataRow row in db.GetDatatable(
				"SELECT attachment_id, item_id, item_column_id, parent_id, control_name, name, url, archive_start FROM attachments WHERE instance = @instance AND parent_id = @parentId AND control_name = @controlName AND type = 1",
				DBParameters).Rows) {
				retval.Add(new Attachment(row));
			}

			return retval;
		}

		public int NewLink(int itemId, int itemColumnId, JObject data) {
			EnsureRight(itemId, itemColumnId, "meta.write");
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@name", data.GetValue("name"));
			SetDBParam(SqlDbType.NVarChar, "@url", data.GetValue("url"));

			return db.ExecuteInsertQuery(
				"INSERT INTO attachments (instance, item_id, item_column_id, type, name, url) VALUES(@instance, @itemId, @itemColumnId, 1, @name, @url)",
				DBParameters);
		}

		public int NewLink(int itemId, int itemColumnId, Dictionary<string, object> data) {
			EnsureRight(itemId, itemColumnId, "meta.write");
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@name", data["name"]);
			SetDBParam(SqlDbType.NVarChar, "@url", data["url"]);

			return db.ExecuteInsertQuery(
				"INSERT INTO attachments (instance, item_id, item_column_id, type, name, url) VALUES(@instance, @itemId, @itemColumnId, 1, @name, @url)",
				DBParameters);
		}

		public int UpdateLink(int attachmentId, Dictionary<string, object> data) {
			EnsureRight(attachmentId, "meta.write");

			SetDBParam(SqlDbType.Int, "@attachment_id", attachmentId);
			SetDBParam(SqlDbType.NVarChar, "@name", data["name"]);
			SetDBParam(SqlDbType.NVarChar, "@url", data["url"]);

			return db.ExecuteUpdateQuery(
				"UPDATE attachments SET name = @name, url = @url WHERE attachment_id = @attachment_id", DBParameters);
		}

		public int SetLink(int itemId, int itemColumnId, Dictionary<string, object> data) {
			EnsureRight(itemId, itemColumnId, "meta.write");

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@name", data["name"]);
			SetDBParam(SqlDbType.NVarChar, "@url", data["url"]);
			db.ExecuteDeleteQuery(
				"DELETE FROM attachments WHERE instance = @instance AND item_id = @itemId AND item_column_id = @itemColumnId and type = 1",
				DBParameters);
			return db.ExecuteInsertQuery(
				"INSERT INTO attachments (instance, item_id, item_column_id, type, name, url) VALUES(@instance, @itemId, @itemColumnId, 1, @name, @url)",
				DBParameters);
		}

		public int NewLink(int parentId, string controlName, JObject data) {
			EnsureRight(parentId, controlName, "meta.write");

			SetDBParam(SqlDbType.Int, "@parentId", parentId);
			SetDBParam(SqlDbType.NVarChar, "@controlName", controlName);
			SetDBParam(SqlDbType.NVarChar, "@name", data.GetValue("name"));
			SetDBParam(SqlDbType.NVarChar, "@url", data.GetValue("url"));

			return db.ExecuteInsertQuery(
				"INSERT INTO attachments (instance, parent_id, control_name, type, name, url) VALUES(@instance, @parentId, @controlName, 1, @name, @url)",
				DBParameters);
		}

		public int NewLink(int parentId, string controlName, Dictionary<string, object> data) {
			EnsureRight(parentId, controlName, "meta.read");

			SetDBParam(SqlDbType.Int, "@parentId", parentId);
			SetDBParam(SqlDbType.NVarChar, "@controlName", controlName);
			SetDBParam(SqlDbType.NVarChar, "@name", data["name"]);
			SetDBParam(SqlDbType.NVarChar, "@url", data["url"]);

			return db.ExecuteInsertQuery(
				"INSERT INTO attachments (instance, parent_id, control_name, type, name, url) VALUES(@instance, @parentId, @controlName, 1, @name, @url)",
				DBParameters);
		}

		public void DeleteLink(int linkId) {
			EnsureRight(linkId, "meta.write");
			SetDBParam(SqlDbType.Int, "@linkId", linkId);
			db.ExecuteDeleteQuery("DELETE FROM attachments WHERE instance = @instance AND attachment_id = @linkId",
				DBParameters);
		}

		public static byte[] ResizeImage(Image image, int width, int height, string contentType) {
			var destRect = new Rectangle(0, 0, width, height);
			var destImage = new Bitmap(width, height);

			destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

			using (var graphics = Graphics.FromImage(destImage)) {
				graphics.CompositingMode = CompositingMode.SourceCopy;
				graphics.CompositingQuality = CompositingQuality.HighQuality;
				graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graphics.SmoothingMode = SmoothingMode.HighQuality;
				graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

				using (var wrapMode = new ImageAttributes()) {
					wrapMode.SetWrapMode(WrapMode.TileFlipXY);
					graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
				}
			}

			var stream = new MemoryStream();
			var cType = contentType.Split('/')[1];
			if (cType.Equals("bmp")) {
				destImage.Save(stream, ImageFormat.Bmp);
			} else if (cType.Equals("gif")) {
				destImage.Save(stream, ImageFormat.Gif);
			} else if (cType.Equals("jpeg")) {
				destImage.Save(stream, ImageFormat.Jpeg);
			} else if (cType.Equals("tiff")) {
				destImage.Save(stream, ImageFormat.Tiff);
			} else if (cType.Equals("bmp")) {
				destImage.Save(stream, ImageFormat.Bmp);
			} else if (cType.Equals("png")) {
				destImage.Save(stream, ImageFormat.Png);
			} else {
				destImage.Save(stream, ImageFormat.Png);
			}

			return stream.ToArray();
		}

		public int SaveAttachment(byte[] fileBytes, string fileName, string contentType, int parentId,
			string controlName, string name) {
			EnsureRight(parentId, controlName, "meta.read");

			SetDBParam(SqlDbType.Int, "@parentId", parentId);
			SetDBParam(SqlDbType.NVarChar, "@controlName", controlName);
			SetDBParam(SqlDbType.NVarChar, "@name", name);

			var id = db.ExecuteInsertQuery(
				"INSERT INTO attachments (instance,  parent_id, control_name, type, name) VALUES(@instance, @parentId, @controlName, 0, @name)",
				DBParameters);

			SetDBParam(SqlDbType.Int, "@attachmentId", id);
			SetDBParam(SqlDbType.NVarChar, "@filename", fileName);
			SetDBParam(SqlDbType.NVarChar, "@contentType", contentType);
			SetDBParam(SqlDbType.Int, "@filesize", fileBytes.Length);
			SetDBParam(SqlDbType.Binary, "@filedata", fileBytes);

			var thumbnail_fields = "";
			var thumbnail_values = "";
			if (contentType.Split("/")[0] == "image") {
				var stream = new MemoryStream();
				stream.Write(fileBytes, 0, fileBytes.Length);
				var img = Image.FromStream(stream);
				var size = 160;

				// For the unknown reason first resized jpg will be corrupted - JKa 12.2.2021
				if (contentType == "image/jpeg") ResizeImage(img, 1, 1, contentType);

				SetDBParam(SqlDbType.Binary, "@thumbnail", ResizeImage(img, size, size, contentType));
				SetDBParam(SqlDbType.Binary, "@thumbnail_small", ResizeImage(img, size / 2, size / 2, contentType));
				SetDBParam(SqlDbType.Binary, "@thumbnail_tiny", ResizeImage(img, size / 4, size / 4, contentType));

				thumbnail_fields = ", thumbnail, thumbnail_small, thumbnail_tiny";
				thumbnail_values = ", @thumbnail, @thumbnail_small, @thumbnail_tiny";
			}

			var dc = new Connections();
			dc.SetAttachmentsDb();
			dc.ExecuteInsertQuery(
				"INSERT INTO attachments_data (attachment_id, filename, content_type, filesize, filedata" +
				thumbnail_fields + ") VALUES(@attachmentId, @filename, @contentType, @filesize, @filedata" +
				thumbnail_values + ") ", DBParameters);

			return id;
		}

		public int SaveAttachment(byte[] fileBytes, string fileName, string contentType, int itemId, int itemColumnId,
			string name) {
			EnsureRight(itemId, itemColumnId, "meta.write");

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.NVarChar, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@name", name);
			var id = db.ExecuteInsertQuery(
				"INSERT INTO attachments (instance,  item_id, item_column_id, type, name) VALUES(@instance, @itemId, @itemColumnId, 0, @name)",
				DBParameters);

			SetDBParam(SqlDbType.Int, "@attachmentId", id);
			SetDBParam(SqlDbType.NVarChar, "@filename", fileName);
			SetDBParam(SqlDbType.NVarChar, "@contentType", contentType);
			SetDBParam(SqlDbType.Int, "@filesize", fileBytes.Length);
			SetDBParam(SqlDbType.Binary, "@filedata", fileBytes);

			var thumbnail_fields = "";
			var thumbnail_values = "";
			if (contentType.Split("/")[0] == "image") {
				var stream = new MemoryStream();
				stream.Write(fileBytes, 0, fileBytes.Length);
				var img = Image.FromStream(stream);
				var size = 160;

				// For the unknown reason first resized jpg will be corrupted - JKa 12.2.2021
				if (contentType == "image/jpeg") ResizeImage(img, 1, 1, contentType);

				SetDBParam(SqlDbType.Binary, "@thumbnail", ResizeImage(img, size, size, contentType));
				SetDBParam(SqlDbType.Binary, "@thumbnail_small", ResizeImage(img, size / 2, size / 2, contentType));
				SetDBParam(SqlDbType.Binary, "@thumbnail_tiny", ResizeImage(img, size / 4, size / 4, contentType));

				thumbnail_fields = ", thumbnail, thumbnail_small, thumbnail_tiny";
				thumbnail_values = ", @thumbnail, @thumbnail_small, @thumbnail_tiny";
			}

			var dc = new Connections();
			dc.SetAttachmentsDb();
			dc.ExecuteInsertQuery(
				"INSERT INTO attachments_data (attachment_id, filename, content_type, filesize, filedata" +
				thumbnail_fields + ") VALUES(@attachmentId, @filename, @contentType, @filesize, @filedata" +
				thumbnail_values + ")", DBParameters);

			return id;
		}

		public void SaveAttachment(byte[] fileBytes, string fileName, string contentType, int attachmentId,
			string name) {
			EnsureRight(attachmentId, "meta.write");
			SetDBParam(SqlDbType.Int, "@attachmentId", attachmentId);
			SetDBParam(SqlDbType.NVarChar, "@name", name);

			db.ExecuteUpdateQuery("UPDATE attachments SET name = @name WHERE attachment_id = @attachmentId",
				DBParameters);

			SetDBParam(SqlDbType.Int, "@attachmentId", attachmentId);
			SetDBParam(SqlDbType.NVarChar, "@filename", fileName);
			SetDBParam(SqlDbType.NVarChar, "@contentType", contentType);
			SetDBParam(SqlDbType.Int, "@filesize", fileBytes.Length);
			SetDBParam(SqlDbType.Binary, "@filedata", fileBytes);

			var thumbnails = "";
			if (contentType.Split("/")[0] == "image") {
				var stream = new MemoryStream();
				stream.Write(fileBytes, 0, fileBytes.Length);
				var img = Image.FromStream(stream);
				var size = 160;

				// For the unknown reason first resized jpg will be corrupted - JKa 12.2.2021
				if (contentType == "image/jpeg") ResizeImage(img, 1, 1, contentType);

				SetDBParam(SqlDbType.Binary, "@thumbnail", ResizeImage(img, size, size, contentType));
				SetDBParam(SqlDbType.Binary, "@thumbnail_small", ResizeImage(img, size / 2, size / 2, contentType));
				SetDBParam(SqlDbType.Binary, "@thumbnail_tiny", ResizeImage(img, size / 4, size / 4, contentType));

				thumbnails =
					", thumbnail = @thumbnail, thumbnail_small = @thumbnail_small, thumbnail_tiny = @thumbnail_tiny WHERE attachment_id = @attachmentId";
			}

			var dc = new Connections();
			dc.SetAttachmentsDb();
			dc.ExecuteUpdateQuery(
				"UPDATE attachments_data SET filename = @filename, content_type = @contentType, filesize = @filesize, filedata = @filedata" +
				thumbnails, DBParameters);
		}

		public List<Attachment> GetPortfolioTemplatesByProcess(string process) {
			var result = new List<Attachment>();
			var dc = new Connections();
			SetDBParam(SqlDbType.NVarChar, "@process", process);

			var sql = "SELECT * FROM attachments WHERE control_name = 'portfolioTemplates' AND parent_id IN ( " +
			          "SELECT process_portfolio_id FROM process_portfolios WHERE process = @process)";
			foreach (DataRow r in dc.GetDatatable(sql, DBParameters).Rows) {

				result.Add(new Attachment(r));
			}
			return result;
		}


		public void CopyAttachments(int sourceId, int destinationId, int sourceItemColumnId,
			int destinationItemColumnId, bool checkRights = false) {
			if (checkRights) {
				EnsureRight(sourceId, sourceItemColumnId, "meta.read");
				EnsureRight(destinationId, destinationItemColumnId, "meta.write");
			}

			SetDBParam(SqlDbType.Int, "@sourceId", sourceId);
			SetDBParam(SqlDbType.Int, "@destinationId", destinationId);
			SetDBParam(SqlDbType.NVarChar, "@sourceItemColumnId", sourceItemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@destinationItemColumnId", destinationItemColumnId);
			foreach (DataRow r in db.GetDatatable("SELECT attachment_id FROM attachments WHERE item_id = @sourceId AND item_column_id = @sourceItemColumnId", DBParameters).Rows) {

				SetDBParam(SqlDbType.Int, "@attachmentId", Conv.ToInt(r["attachment_id"]));
				var newAttachmentId = Conv.ToInt(db.ExecuteInsertQuery(
					"INSERT INTO attachments (instance, item_id, item_column_id, type, name)  SELECT @instance, @destinationId, @destinationItemColumnId, type, name FROM attachments WHERE attachment_id = @attachmentId ",
					DBParameters));

				SetDBParam(SqlDbType.Int, "@naid", newAttachmentId);
				var dc = new Connections();
				dc.SetAttachmentsDb();
				dc.ExecuteUpdateQuery(
					"INSERT INTO attachments_data (attachment_id, filename, content_type, filesize, filedata, thumbnail, thumbnail_small, thumbnail_tiny) SELECT @naid, filename, content_type, filesize, filedata, thumbnail, thumbnail_small, thumbnail_tiny FROM attachments_data WHERE attachment_id = @attachmentId",
					DBParameters);
			}

		}
	}

	public class AttachmentsDownload {
		private AuthenticatedSession _session;

		public AttachmentsDownload(AuthenticatedSession session) : base() {
			_session = session;
		}

		public AttachmentData DownloadAttachment(int attachmentId, int type, DateTime? archiveDate = null) {
			var rights = new Attachments(_session.instance, _session);
			rights.EnsureRight(attachmentId, "meta.read", archiveDate);

			var dc = new Connections();
			dc.SetAttachmentsDb();

			var p = new SqlParameter();
			p.SqlDbType = SqlDbType.Int;
			p.ParameterName = "attachmentId";
			p.Value = attachmentId;
			var DBParameters = new List<SqlParameter>();
			DBParameters.Add(p);

			DataRow d;
			if (type == 0) {
				d = dc.GetDataRow(
					"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, filedata, '' AS name FROM attachments_data WHERE attachments_data.attachment_id = @attachmentId",
					DBParameters);
			} else if (type == 1) {
				d = dc.GetDataRow(
					"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, thumbnail, '' AS name FROM attachments_data WHERE attachments_data.attachment_id = @attachmentId",
					DBParameters);
			} else if (type == 2) {
				d = dc.GetDataRow(
					"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, thumbnail_small, '' AS name FROM attachments_data WHERE attachments_data.attachment_id = @attachmentId",
					DBParameters);
			} else if (type == 3) {
				d = dc.GetDataRow(
					"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, thumbnail_tiny, '' AS name FROM attachments_data WHERE attachments_data.attachment_id = @attachmentId",
					DBParameters);
			} else {
				d = dc.GetDataRow(
					"SELECT filename, content_type, filesize, CASE WHEN thumbnail IS NOT NULL THEN 1 ELSE 0 END has_thumbnail, filedata, '' AS name FROM attachments_data WHERE attachments_data.attachment_id = @attachmentId",
					DBParameters);
			}

			if (d != null) {
				var c = new Connections();
				var name = "";
				if (archiveDate == null) {
					name = Conv.ToStr(
						c.ExecuteScalar("SELECT name FROM attachments WHERE attachment_id = " + attachmentId));
				}
				else {
					p = new SqlParameter();
					p.SqlDbType = SqlDbType.DateTime;
					p.ParameterName = "archiveDate";
					p.Value = archiveDate;
					DBParameters.Add(p);
					name = Conv.ToStr(
						c.ExecuteScalar("SELECT name FROM get_attachments(@archiveDate) WHERE attachment_id = " + attachmentId, DBParameters));
				}
				d["name"] = name;
				return new AttachmentData(d, type);
			}

			throw new CustomArgumentException("Attachment Cannot be found with attachment_id " + attachmentId);
		}
	}
}