﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Keto5.x.platform.server.core {
	public class ExcelRow {
		public ExcelRow() {
			Cells = new List<ExcelCell>();
		}

		public ExcelColor BackgroundColor { get; set; }
		public List<ExcelCell> Cells { get; set; }

		public void AddCell(ExcelCell cell) {
			Cells.Add(cell);
		}

		public Color GetColor() {
			return BackgroundColor.GetExcelColor();
		}
	}

	public class ExcelColor {
		public int Red { get; set; }
		public int Green { get; set; }
		public int Blue { get; set; }

		public Color GetExcelColor() {
			return Color.FromArgb(Red, Green, Blue);
		}
	}

	public class ExcelCell {
		public ExcelColor BackgroundColor { get; set; }
		public string Value { get; set; }

		public int DataType { get; set; }

		public double NValue { get; set; }

		public Color GetColor() {
			return BackgroundColor.GetExcelColor();
		}
	}

	public class Excel {
		public Excel() {
			Rows = new List<ExcelRow>();
		}

		public string Name { get; set; }
		public List<ExcelRow> Rows { get; set;}

		public ExcelPackage GetExcel() {
			var pck = new ExcelPackage();
			var ws = pck.Workbook.Worksheets.Add(Name);
			ws.View.ShowGridLines = true;
			var rowsCount = Rows.Count;

			for (var i = 0; i < rowsCount; i++) {
				var excelRow = Rows[i];
				var cellsCount = excelRow.Cells.Count;
				var rowIndex = i + 1;

				for (var j = 0; j < cellsCount; j++) {
					var cell = excelRow.Cells[j];
					var columnIndex = j + 1;

					if (cell.DataType != 2) {
						ws.Cells[rowIndex, columnIndex].Value = cell.Value;
					} else {
						ws.Cells[rowIndex, columnIndex].Value = cell.NValue;
					}

					if (cell.DataType == 2) {
						ws.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
					} else if (cell.DataType == 4) {
						ws.Cells[rowIndex, columnIndex].Style.Font.UnderLine = true;
						ws.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
					}

					if (cell.BackgroundColor != null) {
						ws.Cells[rowIndex, columnIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
						ws
							.Cells[rowIndex, columnIndex]
							.Style
							.Fill
							.BackgroundColor
							.SetColor(cell.GetColor());
					}

				}
			}

			return pck;
		}

		public void AddRow(ExcelRow row) {
			Rows.Add(row);
		}

		public string GetExcelBase64() {
			var bArr = GetExcel().GetAsByteArray();
			return Convert.ToBase64String(bArr);
		}
	}
}