﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.core {
	public class Configuration {
		public Configuration(DataRow row) {
			Category = (string) row["category"];
			Name = (string) row["name"];
			Value = (string) row["value"];

			if (row["group_name"] != DBNull.Value) GroupName = (string) row["group_name"];
			if (row["process"] != DBNull.Value) Process = (string) row["process"];
			if (row["domain"] != DBNull.Value) Domain = (string) row["domain"];
		}

		public string Process { get; set; }
		public string GroupName { get; set; }
		public string Domain { get; set; }
		public string Category { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }
	}

	public class Configurations : ConnectionBase {
		private int itemId = 0;
		//private string instance = "";

		public Configurations(string instance, AuthenticatedSession aS) : base(instance, aS) {
			this.itemId = aS.item_id;
			this.instance = instance;
		}

		public Configurations(string instance, int itemId) : base(instance, new AuthenticatedSession("", itemId, null)) {
			this.itemId = itemId;
			this.instance = instance;
		}

		public JObject GetServerConfigurations() {
			var defaultConfigurations = Config.Configuration.DeepClone();

			var a = new AuthenticatedSession(instance, itemId, null);
			db = new Connections(a);
			foreach (DataRow r in db
				.GetDatatable(
					"SELECT category, name, process, value, group_name, domain FROM instance_configurations WHERE instance = @instance AND server = 1",
					DBParameters).Rows) {
				var c = new Configuration(r);

				if (!string.IsNullOrEmpty(c.Domain)) {
					defaultConfigurations["domain"][c.Domain]["dynamic"][c.Category][c.Name]["value"] = c.Value;
				} else {
					defaultConfigurations["dynamic"][c.Category][c.Name]["value"] = c.Value;
				}
			}

			defaultConfigurations["hidden"].Parent.Remove();
			foreach (var domain in Config.Domains) {
				defaultConfigurations["domain"][domain]["hidden"].Parent.Remove();
			}

			return defaultConfigurations.ToObject<JObject>();
		}

		public string GetClientConfiguration(string category, string name) { 
			SetDBParam(SqlDbType.NVarChar, "category", category.ToLower());
			SetDBParam(SqlDbType.NVarChar, "name", name.ToLower());
			return Conv.ToStr(db.ExecuteScalar(
				"SELECT value FROM instance_configurations WHERE instance = @instance AND category = @category AND name = @name AND server = 0",
				DBParameters));
		}

		public void SaveClientConfiguration(string category, string name, string value) {
			SetDBParam(SqlDbType.NVarChar, "category", category.ToLower());
			SetDBParam(SqlDbType.NVarChar, "name", name.ToLower());
			SetDBParam(SqlDbType.NVarChar, "value", value);

			db.ExecuteDeleteQuery(
				"DELETE FROM instance_configurations WHERE instance = @instance AND category = @category AND name = @name AND server = 0",
				DBParameters);

			db.ExecuteInsertQuery(
				"INSERT INTO instance_configurations(instance, category, name, value, server) VALUES(@instance, @category, @name, @value, 0)",
				DBParameters);
		}

		public void SaveServerConfigurations(JObject conf) {
			var a = new AuthenticatedSession(instance, itemId, null);
			db = new Connections(a);
			foreach (var c in conf) {
				if (c.Key == "domain") {
					foreach (JProperty d in c.Value) {
						foreach (JProperty category in d.Value) {
							foreach (JProperty p in category.Value) {
								if (Config.Configuration["domain"][d.Name]["dynamic"][category.Name][p.Name] != null
								    && Config.Configuration["domain"][d.Name]["dynamic"][category.Name][p.Name]
									    ["value"] != p.Value["value"]) {
									SetDBParam(SqlDbType.NVarChar, "domain", d.Name.ToLower());
									SetDBParam(SqlDbType.NVarChar, "category", category.Name.ToLower());
									SetDBParam(SqlDbType.NVarChar, "name", p.Name.ToLower());
									SetDBParam(SqlDbType.NVarChar, "value", p.Value["value"]);

									db.ExecuteInsertQuery(
										"INSERT INTO instance_configurations(instance, domain, category, name, value, server) VALUES(@instance, @domain, @category, @name, @value, 1)",
										DBParameters);
								}
							}
						}
					}
				} else {
					foreach (JProperty p in c.Value) {
						if (Config.Configuration["dynamic"][c.Key][p.Name] != null &&
						    Config.Configuration["dynamic"][c.Key][p.Name]["value"] != p.Value["value"]) {
							SetDBParam(SqlDbType.NVarChar, "category", c.Key.ToLower());
							SetDBParam(SqlDbType.NVarChar, "name", p.Name.ToLower());
							SetDBParam(SqlDbType.NVarChar, "value", p.Value["value"]);

							db.ExecuteInsertQuery(
								"INSERT INTO instance_configurations(instance, category, name, value, server) VALUES(@instance, @category, @name, @value, 1)",
								DBParameters);
						}
					}
				}
			}
		}

		public List<Configuration> GetClientConfigurations() {
			var lc = new List<Configuration>();
			db = new Connections();
			foreach (DataRow r in db
				.GetDatatable(
					"SELECT group_name, process, category, name, value, domain FROM instance_configurations WHERE instance = @instance AND server = 0",
					DBParameters).Rows) {
				var conf = new Configuration(r);
				Cache.Set(this.instance, Config.GetGroupName(), conf.Process, conf.Category, conf.Name, conf.Value);
				lc.Add(conf);
			}

			return lc;
		}

		public void SaveClientConfigurations(JObject conf) {
			var a = new AuthenticatedSession(instance, itemId, null);
			db = new Connections(a);
			foreach (var c in conf) {
				if (c.Value.HasValues) {
					foreach (JProperty p in c.Value) {
						if (p.Value.HasValues) {
							foreach (JProperty n in p.Value) {
								if (n.Value["value"] != null) {
									SetDBParam(SqlDbType.NVarChar, "category", c.Key);
									SetDBParam(SqlDbType.NVarChar, "name", n.Name);
									SetDBParam(SqlDbType.NVarChar, "value", n.Value["value"].ToString());
									if (n.Value["process"] == null || n.Value["process"].ToString() == "") {
										SetDBParam(SqlDbType.NVarChar, "process", DBNull.Value);
									} else {
										SetDBParam(SqlDbType.NVarChar, "process", n.Value["process"]);
										Cache.Set(this.instance, Config.GetGroupName(), (string) n.Value["process"],
											c.Key, n.Name, n.Value["value"].ToString());
									}

									db.ExecuteInsertQuery(
										"INSERT INTO instance_configurations(instance, process, category, name, value, server) VALUES(@instance, @process, @category, @name, @value, 0)",
										DBParameters);
								}
							}
						}
					}
				}
			}
		}
	}
}