﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	public class UserScores : ConnectionBase {
		private readonly AuthenticatedSession session;

		public UserScores(string instance, AuthenticatedSession session) : base(instance, session) {
			this.session = session;
		}

		public Dictionary<int, Dictionary<int, Dictionary<string, object>>> GetScores() {

			var retval = new Dictionary<int, Dictionary<int, Dictionary<string, object>>>();

			var pgs = new ProcessGroups(instance, "office_olympics", session);
			foreach (var pg in pgs.GetGroups()) {
				var r2 = new Dictionary<int, Dictionary<string, object>>();

				foreach (DataRow row in db.GetDatatable("SELECT item_id, first_name + ' ' + last_name AS full_name,  COALESCE((SELECT SUM(CAST(score AS INT)) FROM _keto_office_olympics_scores WHERE user_id = _keto_user.item_id AND group_id = " + pg.ProcessGroupId + "), 0) as score FROM _keto_user where active = 1", DBParameters).Rows) {
					var r = new Dictionary<string, object>();
					r.Add("item_id", row["item_id"]);
					r.Add("full_name", row["full_name"]);
					r.Add("score", row["score"]);
					r2.Add(Conv.ToInt(row["item_id"]), r);
				}
				retval.Add(pg.ProcessGroupId, r2);
			}



			return retval;
		}

		public Dictionary<int, int> GetTeamSCores(int groupId) {
			var retval = new Dictionary<int, int>();
			foreach (DataRow row in db.GetDatatable("SELECT item_id as team_id, COALESCE((select MAX(score) from item_data_process_selections ips  INNER JOIN _keto_office_olympics_scores s ON ips.selected_item_id = s.user_id where ips.item_id = m.item_id AND group_id = " + groupId + "  ), 0) as score FROM [_keto_office_olympics] m ", DBParameters).Rows) {
				retval.Add(Conv.ToInt(row["team_id"]), Conv.ToInt(row["score"]));
			}
			return retval;
		}
	}
}
