﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core {
	public class IntegrationConfig : ConnectionBase {
		private AuthenticatedSession session;

		public IntegrationConfig(string instance, AuthenticatedSession session) : base(instance, session) {
			this.session = session;
		}

		public List<Dictionary<string, object>> GetKeys()
		{
			return db.GetDatatableDictionary("SELECT * FROM instance_integration_keys WHERE instance = @instance", DBParameters);
		}

		public List<Dictionary<string, object>> GetTokens(string key)
		{
			SetDBParam(SqlDbType.NVarChar, "@keyVal", key);
			return db.GetDatatableDictionary("SELECT * FROM instance_integration_tokens WHERE [key] = @keyVal AND [type] = 0", DBParameters);
		}

		public List<Dictionary<string, object>> SaveKeys(List<Dictionary<string, object>> data)
		{
			foreach (var d in data)
			{
				SetDBParam(SqlDbType.Int, "@id", Conv.ToInt(d["instance_integration_key_id"]));
				SetDBParam(SqlDbType.NVarChar, "@keyVal", d["key"]);
				var ids = JsonConvert.DeserializeObject<int[]>(d["userArr"].ToString());
				if (ids.Length > 0)
				{
					SetDBParam(SqlDbType.Int, "@userId", ids[0]);
				}
				else
				{
					SetDBParam(SqlDbType.NVarChar, "@userId", DBNull.Value);
				}
				var key_sql = "";
				if (Conv.ToInt(db.ExecuteScalar("SELECT TOP 1 instance_integration_key_id FROM instance_integration_keys WHERE [key] = @keyVal AND instance_integration_key_id <> @id", DBParameters)) == 0) {
					key_sql = ", [key] = @keyVal";
					d["not_unique"] = false;
				}
				else {
					d["not_unique"] = true;
				}
				db.ExecuteUpdateQuery("UPDATE instance_integration_keys SET user_item_id = @userId" + key_sql + " WHERE instance_integration_key_id = @id AND instance = @instance", DBParameters);
			}
			return data;
		}

		public List<Dictionary<string, object>> SaveTokens(List<Dictionary<string, object>> data)
		{
			foreach (var d in data)
			{
				SetDBParam(SqlDbType.Int, "@id", Conv.ToInt(d["instance_integration_token_id"]));
				SetDBParam(SqlDbType.DateTime, "@validUntil", d["valid_until"]);
				db.ExecuteUpdateQuery("UPDATE instance_integration_tokens SET valid_until = @validUntil WHERE instance_integration_token_id = @id", DBParameters);
			}
			return data;
		}

		public List<Dictionary<string, object>> AddKeys(List<Dictionary<string, object>> data)
		{
			foreach (var d in data)
			{
				var key = "key_" + (Conv.ToInt(db.ExecuteScalar("SELECT MAX(CAST(SUBSTRING(LTRIM(RTRIM([key])), 5, LEN([key])) AS INT)) FROM instance_integration_keys WHERE SUBSTRING(LTRIM(RTRIM([key])), 1, 4) = 'key_' AND SUBSTRING(LTRIM(RTRIM([key])), 5, LEN([key])) LIKE '[0-9]'", DBParameters)) + 1);
				SetDBParam(SqlDbType.NVarChar, "@key", key);
				var newId = db.ExecuteInsertQuery("INSERT INTO instance_integration_keys (instance, [key]) values(@instance, @key)", DBParameters);
				d.Add("instance_integration_key_id", newId);
				d.Add("key", key);
			}
			return data;
		}

		public List<Dictionary<string, object>> AddTokens(string key, List<Dictionary<string, object>> data)
		{
			foreach (var d in data)
			{
				SetDBParam(SqlDbType.NVarChar, "@keyVal", key);
				SetDBParam(SqlDbType.DateTime, "@validUntil", Conv.ToDateTime(d["valid_until"]));
				var token = Util.CreateToken(32);
				SetDBParam(SqlDbType.NVarChar, "@token", token);
				var newId = db.ExecuteInsertQuery("INSERT INTO instance_integration_tokens ([key], valid_until, token, [type]) values(@keyVal, @validUntil, @token, 0)", DBParameters);
				d.Add("instance_integration_token_id", newId);
				d.Add("token", token);
			}
			return data;
		}

		public void DeleteKeys(List<int> ids)
		{
			var idsStr = "-1 ";
			foreach (var id in ids)
			{
				idsStr += ", " + id;
			}
			db.ExecuteDeleteQuery(
				"DELETE FROM instance_integration_keys WHERE instance = @instance AND instance_integration_key_id IN (" + idsStr + ")",
				DBParameters);
		}

		public void DeleteTokens(List<int> ids)
		{
			var idsStr = "-1 ";
			foreach (var id in ids)
			{
				idsStr += ", " + id;
			}
			db.ExecuteDeleteQuery(
				"DELETE FROM instance_integration_tokens WHERE instance_integration_token_id IN (" + idsStr + ")",
				DBParameters);
		}
	}
}
