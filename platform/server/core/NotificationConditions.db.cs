﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core {
	public class NotificationCondition {
		public NotificationCondition() { }

		public NotificationCondition(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			NotificationConditionId = (int) dr[alias + "notification_condition_id"];
			Variable = Conv.ToStr(dr[alias + "variable"]);
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);

			if (!DBNull.Value.Equals(dr["item_id"])) {
				ItemId = (int?) Conv.IfNullThenDbNull(dr[alias + "item_id"]);
			} else {
				ItemId = null;
			}

			if (!DBNull.Value.Equals(dr["before_condition_id"])) {
				BeforeConditionId = (int?) Conv.IfNullThenDbNull(dr[alias + "before_condition_id"]);
			} else {
				BeforeConditionId = null;
			}

			if (!DBNull.Value.Equals(dr["after_condition_id"])) {
				AfterConditionId = (int?) Conv.IfNullThenDbNull(dr[alias + "after_condition_id"]);
			} else {
				AfterConditionId = null;
			}

			View = Conv.ToStr(dr[alias + "view_id"]);
			InUse = Convert.ToBoolean(dr["in_use"]);
			ConditionType = Conv.ToByte(dr["condition_type"]);

			if (!DBNull.Value.Equals(dr["process_action_id"])) {
				ProcessActionId = (int?) Conv.IfNullThenDbNull(dr[alias + "process_action_id"]);
			} else {
				ProcessActionId = null;
			}

		}

		public int NotificationConditionId { get; set; }
		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public int? ItemId { get; set; }
		public List<string> ItemColumnIds { get; set; }
		public int? BeforeConditionId { get; set; }
		public int? AfterConditionId { get; set; }
		public string View { get; set; }
		public bool InUse { get; set; }
		public byte ConditionType { get; set; }
		public List<int> UserGroupIds { get; set; }
		public int? ProcessActionId { get; set; }
	}

	public class NotificationConditions : ProcessBase {
		public NotificationConditions(string instance, string process, AuthenticatedSession authenticatedSession) :
			base(instance, process, authenticatedSession) {
		}

		private DataTable _notificationReceiversCache = null;
		private DataTable GetNotificationReceivers =>
			_notificationReceiversCache ?? (_notificationReceiversCache =
				db.GetDatatable("SELECT notification_condition_id, item_column_id, list_process, list_item_column_id, user_group_item_id FROM notification_receivers"));

		public bool HasNotificationConditionsInUse(string instance, string process) {
			return Conv.ToInt(db.ExecuteScalar(
				"SELECT COUNT(*) FROM notification_conditions WHERE instance = @instance AND process = @process",
				DBParameters)) > 0;
		}

		public List<NotificationCondition> GetNotificationConditions(int? type = null, int? inUse = null) {

			var result = new List<NotificationCondition>();
			//Get Column Names
			var sql =
				"SELECT notification_condition_id, variable, item_id, before_condition_id, after_condition_id, view_id, in_use, condition_type, process_action_id FROM notification_conditions WHERE instance = @instance AND process = @process";

			if (type != null) {
				sql += " AND condition_type = @type";
				SetDBParam(SqlDbType.Int, "@type", type);
			}

			if (inUse != null) {
				sql += " AND in_use = @inUse";
				SetDBParam(SqlDbType.Int, "@inUse", inUse);
			}

			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var c = new NotificationCondition(row, authenticatedSession);

				c.ItemColumnIds = new List<string>();
				c.UserGroupIds = new List<int>();
				foreach (DataRow row2 in GetNotificationReceivers.Select("notification_condition_id = " + c.NotificationConditionId)) {
					if (!DBNull.Value.Equals(row2["item_column_id"])) {
						if (row2["list_item_column_id"] != DBNull.Value) {
							c.ItemColumnIds.Add(Conv.ToStr(row2["item_column_id"]) + ";" +
							                    Conv.ToStr(row2["list_process"]) + ";" +
							                    Conv.ToStr(row2["list_item_column_id"]));
						} else {
							c.ItemColumnIds.Add(Conv.ToStr(row2["item_column_id"]));
						}
					}

					if (!DBNull.Value.Equals(row2["user_group_item_id"])) {
						c.UserGroupIds.Add(Conv.ToInt(row2["user_group_item_id"]));
					}
				}

				result.Add(c);
			}

			return result;
		}

		public NotificationCondition GetNotificationCondition(int notificationConditionId) {
			SetDBParam(SqlDbType.Int, "@notificationId", notificationConditionId);
			var row = db.GetDataRow(
				"SELECT notification_condition_id, variable, item_id, before_condition_id, after_condition_id, view_id, in_use, condition_type,process_action_id FROM notification_conditions WHERE instance = @instance AND process = @process AND notification_condition_id = @notificationId",
				DBParameters);
			var c = new NotificationCondition(row, authenticatedSession);

			c.ItemColumnIds = new List<string>();
			c.UserGroupIds = new List<int>();
			foreach (DataRow row2 in GetNotificationReceivers.Select("notification_condition_id = " + c.NotificationConditionId)) {
				if (!DBNull.Value.Equals(row2["item_column_id"])) {
					if (row2["list_item_column_id"] != DBNull.Value) {
						c.ItemColumnIds.Add(Conv.ToStr(row2["item_column_id"]) + ";" +
						                    Conv.ToStr(row2["list_process"]) + ";" +
						                    Conv.ToStr(row2["list_item_column_id"]));
					} else {
						c.ItemColumnIds.Add(Conv.ToStr(row2["item_column_id"]));
					}
				}

				if (!DBNull.Value.Equals(row2["user_group_item_id"])) {
					c.UserGroupIds.Add(Conv.ToInt(row2["user_group_item_id"]));
				}
			}

			return c;
		}

		public void Delete(int notificationConditionId) {
			SetDBParam(SqlDbType.Int, "notificationConditionId", notificationConditionId);
			db.ExecuteDeleteQuery(
				"DELETE FROM notification_conditions WHERE notification_condition_id = @notificationConditionId",
				DBParameters);
		}

		public NotificationCondition Add(NotificationCondition notificationCondition) {
			notificationCondition.NotificationConditionId = db.ExecuteInsertQuery(
				"INSERT INTO notification_conditions (instance, process) VALUES(@instance, @process)", DBParameters);
			if (notificationCondition.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVar = translations.GetTranslationVariable(process,
					notificationCondition.LanguageTranslation[authenticatedSession.locale_id], "N_CONDITION");
				SetDBParam(SqlDbType.Int, "@notificationConditionId", notificationCondition.NotificationConditionId);
				SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
				db.ExecuteUpdateQuery(
					"UPDATE notification_conditions SET variable = @variable WHERE instance = @instance AND process = @process AND notification_condition_id = @notificationConditionId",
					DBParameters);

				foreach (var translation in notificationCondition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVar, translation.Value, translation.Key);
				}
			}

			return notificationCondition;
		}

		public NotificationCondition Save(NotificationCondition notificationCondition) {
			SetDBParam(SqlDbType.Int, "@notificationConditionId", notificationCondition.NotificationConditionId);
			SetDBParam(SqlDbType.Int, "@itemId", Conv.IfNullThenDbNull(notificationCondition.ItemId));
			SetDBParam(SqlDbType.Int, "@beforeConditionId",
				Conv.IfNullThenDbNull(notificationCondition.BeforeConditionId));
			SetDBParam(SqlDbType.Int, "@afterConditionId",
				Conv.IfNullThenDbNull(notificationCondition.AfterConditionId));
			SetDBParam(SqlDbType.NVarChar, "@view", Conv.IfNullThenDbNull(notificationCondition.View));
			SetDBParam(SqlDbType.Int, "@inUse", notificationCondition.InUse);
			SetDBParam(SqlDbType.TinyInt, "@conditionType", notificationCondition.ConditionType);
			SetDBParam(SqlDbType.Int, "@processActionid",
				Conv.IfNullThenDbNull(notificationCondition.ProcessActionId));

			db.ExecuteUpdateQuery(
				"UPDATE notification_conditions SET item_id = @itemId, before_condition_id = @beforeConditionId, after_condition_id = @afterConditionId, view_id = @view, in_use = @inUse, condition_type = @conditionType, process_action_id = @processActionid WHERE instance = @instance AND process = @process AND notification_condition_id = @notificationConditionId",
				DBParameters);
			db.ExecuteDeleteQuery(
				"DELETE FROM notification_receivers WHERE notification_condition_id = @notificationConditionId",
				DBParameters);
			foreach (var id in notificationCondition.ItemColumnIds) {
				if (id.Contains(";")) {
					SetDBParam(SqlDbType.Int, "@itemColumnId", id.Split(';')[0]);
					SetDBParam(SqlDbType.NVarChar, "@listProcess", id.Split(';')[1]);
					SetDBParam(SqlDbType.Int, "@listItemColumnId", id.Split(';')[2]);
				} else {
					SetDBParam(SqlDbType.Int, "@itemColumnId", id);
					SetDBParam(SqlDbType.NVarChar, "@listProcess", DBNull.Value);
					SetDBParam(SqlDbType.Int, "@listItemColumnId", DBNull.Value);
				}

				db.ExecuteInsertQuery(
					"INSERT INTO notification_receivers (notification_condition_id, item_column_id, list_process, list_item_column_id) VALUES (@notificationConditionId, @itemColumnId, @listProcess, @listItemColumnId)",
					DBParameters);
			}

			foreach (var id in notificationCondition.UserGroupIds) {
				SetDBParam(SqlDbType.Int, "@userGroupItemId", id);
				db.ExecuteInsertQuery(
					"INSERT INTO notification_receivers (notification_condition_id, user_group_item_id) VALUES (@notificationConditionId, @userGroupItemId)",
					DBParameters);
			}

			var oldVar =
				db.ExecuteScalar(
					"SELECT variable FROM notification_conditions WHERE instance = @instance AND process = @process AND notification_condition_id = @notificationConditionId",
					DBParameters);
			if (oldVar != null && oldVar != DBNull.Value &&
			    notificationCondition.LanguageTranslation[authenticatedSession.locale_id] != null) {
				foreach (var translation in notificationCondition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string) oldVar, translation.Value,
						translation.Key);
				}
			} else if (oldVar == DBNull.Value &&
			           notificationCondition.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVar = translations.GetTranslationVariable(process,
					notificationCondition.LanguageTranslation[authenticatedSession.locale_id], "N_CONDITION");
				SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
				db.ExecuteUpdateQuery(
					"UPDATE notification_conditions SET variable = @variable WHERE instance = @instance AND process = @process AND notification_condition_id = @notificationConditionId",
					DBParameters);

				foreach (var translation in notificationCondition.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVar, translation.Value, translation.Key);
				}
			}
			return notificationCondition;
		}
	}
}