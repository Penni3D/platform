﻿using System.Collections.Generic;

namespace Keto5.x.platform.server.core {
	public class ComparisonType {
		public ComparisonType(int id, string cText) {
			ComparisonTypeId = id;
			ComparisonText = cText;
			Type = (ComparisonTypes.Types) id;
		}

		public int ComparisonTypeId { get; set; }
		public string ComparisonText { get; set; }
		public ComparisonTypes.Types Type { get; set; }
	}

	public class ComparisonTypes {
		public enum Types {
			LESS_THAN = 1,
			LESS_THAN_OR_EQUAL = 2,
			EQUALS = 3,
			GREATER_THAN = 4,
			GREATER_OR_EQUAL = 5,
			NOT_EQUALS = 6,
			CONTAINS = 7,
			STARTS_WITH = 8
		}

		private readonly Dictionary<Types, ComparisonType> _comparisonTypes = new Dictionary<Types, ComparisonType> {
			{Types.LESS_THAN, new ComparisonType(1, "<")},
			{Types.LESS_THAN_OR_EQUAL, new ComparisonType(2, "<=")},
			{Types.EQUALS, new ComparisonType(3, "==")},
			{Types.GREATER_THAN, new ComparisonType(4, ">")},
			{Types.GREATER_OR_EQUAL, new ComparisonType(5, ">=")},
			{Types.NOT_EQUALS, new ComparisonType(6, "!=")},
			{Types.CONTAINS, new ComparisonType(7, "")},
			{Types.STARTS_WITH, new ComparisonType(8, "")}
			
		};

		public Dictionary<int, ComparisonType> GetComparisonTypes() {
			var cTypes = new Dictionary<int, ComparisonType>();
			cTypes.Add(1, new ComparisonType(1, "<"));
			cTypes.Add(2, new ComparisonType(2, "<="));
			cTypes.Add(3, new ComparisonType(3, "=="));
			cTypes.Add(4, new ComparisonType(4, ">"));
			cTypes.Add(5, new ComparisonType(5, ">="));
			cTypes.Add(6, new ComparisonType(6, "!="));
			return cTypes;
		}

		public ComparisonType GetComparisonType(int compId){
			return _comparisonTypes[(Types)compId];
		}
	}

}
