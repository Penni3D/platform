﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.core {
	public class UserPreferences {
		public UserPreferences(DataRow row) {
			Group = Conv.ToStr(row["group"]);
			Value = Conv.ToStr(row["value"]);
			Identifier = Conv.ToStr(row["identifier"]);
		}

		public string Group { get; set; }
		public string Value { get; set; }
		public string Identifier { get; set; }
	}

	public class SavedConfiguration {
		public SavedConfiguration(DataRow row) {
			Group = Conv.ToStr(row["group"]);
			Identifier = Conv.ToStr(row["identifier"]);
			ConfigurationId = Conv.ToInt(row["user_configuration_id"]);
			Value = Conv.ToStr(row["value"]);
			Name = Conv.ToStr(row["name"]);
			Public = Conv.ToByte(row["public"]);
			UserId = Conv.ToInt(row["user_id"]);
			VisibleForFilter = Conv.ToStr(row["visible_for_filter"]);
			VisibleForUserGroup = Conv.ToStr(row["visible_for_usergroup"]);
			PortfolioDefaultView = Conv.ToInt(row["portfolio_default_view"]);
			DashBoardId = Conv.ToInt(row["dashboard_id"]);
		}

		public string Group { get; set; }
		public int UserId { get; set; }
		public string Identifier { get; set; }
		public int ConfigurationId { get; set; }
		public string Value { get; set; }
		public string Name { get; set; }
		public byte Public { get; set; }

		public string VisibleForFilter { get; set; }
		public string VisibleForUserGroup { get; set; }

		public int PortfolioDefaultView { get; set; }

		public int DashBoardId { get; set; }
	}

	public class SavedConfigurations : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;

		public SavedConfigurations(string instance, AuthenticatedSession aS) : base(instance, aS) {
			authenticatedSession = aS;
		}

		public List<SavedConfiguration> GetSavedConfigurations(string group) {
			var ret = new List<SavedConfiguration>();
			db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			SetDBParam(SqlDbType.NVarChar, "@group", group);
			foreach (DataRow r in db.GetDatatable(
				"SELECT user_id, [group], identifier, value, [public], name, user_configuration_id, visible_for_filter, visible_for_usergroup, portfolio_default_view FROM instance_user_configurations_saved WHERE instance = @instance AND [group] = @group AND (user_id = @userId OR [public] = 1) ORDER BY name",
				DBParameters).Rows) {
				var uc = new SavedConfiguration(r);
				ret.Add(uc);
			}

			return ret;
		}

		public List<SavedConfiguration> GetSavedConfigurations(string group, string identifier) {
			var ret = new List<SavedConfiguration>();

			db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.NVarChar, "@group", group);

			var innerSql = "";

			foreach (DataRow r in db.GetDatatable(
				"SELECT user_id, [group], identifier, value, [public], name, user_configuration_id, visible_for_filter, visible_for_usergroup, portfolio_default_view, dashboard_id FROM instance_user_configurations_saved " +
				"WHERE instance = @instance AND [group] = @group AND identifier = @identifier AND ((user_id = @userId AND [public] = 0) OR [public] IN (1,2,3,4)) ORDER BY CASE WHEN [public] = 3 THEN 0 ELSE 1 END ASC, UPPER( (SELECT REPLACE(name, '[', '')))  asc",
				DBParameters).Rows) {
				var add = true;
				if (Conv.ToInt(r["public"]) == 2 && Conv.ToInt(r["user_id"]) != authenticatedSession.item_id) {
					var json = JsonConvert.DeserializeObject<Dictionary<string, object>>(
						Conv.ToStr(r["visible_for_filter"]));
					innerSql = "SELECT COUNT(*) AS result FROM ( ";
					foreach (var j in json) {
						var item_column_id = j.Key;
						innerSql += " SELECT item_id FROM item_data_process_selections idps WHERE item_column_id = " +
						            item_column_id + " AND (";

						var condition = "";
						foreach (var selected_item_id in (Newtonsoft.Json.Linq.JArray) j.Value) {
							if (condition == "") {
								condition += "selected_item_id = " + selected_item_id;
							} else {
								condition += " OR selected_item_id = " + selected_item_id;
							}
						}

						if (condition == "") innerSql += "selected_item_id = 0";
						innerSql += condition + ") AND item_id = @userId UNION";
					}

					innerSql += ") iq ";
					innerSql = innerSql.Replace("UNION)", ")");
					if (Conv.ToInt(db.ExecuteScalar(innerSql, DBParameters)) == 0) add = false;
				}

				if (Conv.ToInt(r["public"]) == 4) {
					var json = JsonConvert.DeserializeObject<List<int>>(
						Conv.ToStr(r["visible_for_usergroup"]));

					var ug = new UserGroups(authenticatedSession.instance, authenticatedSession);
					var groups = ug.GetUserGroupsForAuthenticatedUser();
					foreach (var i in json) {
						if (!groups.Contains(i)) continue;
						add = true;
						break;
					}
				}

				if (add) {
					var uc = new SavedConfiguration(r);
					ret.Add(uc);
				}
			}

			return ret;
		}

		public SavedConfiguration CreateSavedConfiguration(string group, string identifier, JObject data) {
			db = new Connections(authenticatedSession);

			SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			SetDBParam(SqlDbType.NVarChar, "@group", group);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.NVarChar, "@value", JsonConvert.SerializeObject(data.GetValue("value")));
			SetDBParam(SqlDbType.TinyInt, "@public", Conv.ToByte(data.GetValue("public").ToString()));


			//todo; check public right

			if (Conv.ToStr(data.GetValue("visible_for_filter")) == "") {
				SetDBParam(SqlDbType.NVarChar, "@visible_for_filter", DBNull.Value);
			} else {
				SetDBParam(SqlDbType.NVarChar, "@visible_for_filter", Conv.ToStr(data.GetValue("visible_for_filter")));
			}

			if (Conv.ToStr(data.GetValue("visible_for_usergroup")) == "") {
				SetDBParam(SqlDbType.NVarChar, "@visible_for_usergroup", DBNull.Value);
			} else {
				SetDBParam(SqlDbType.NVarChar, "@visible_for_usergroup",
					Conv.ToStr(data.GetValue("visible_for_usergroup")));
			}

			if (data.GetValue("portfolio_default_view") != null) {
				SetDBParam(SqlDbType.Int, "@pdv", Conv.ToInt(data.GetValue("portfolio_default_view")));
			} else {
				SetDBParam(SqlDbType.Int, "@pdv", -1);
			}

			Console.WriteLine(data.GetValue("dashboard_id"));
			if (Conv.ToStr(data.GetValue("dashboard_id")) != "") {
				SetDBParam(SqlDbType.Int, "@dbi", Conv.ToInt(data.GetValue("dashboard_id")));
			} else {
				SetDBParam(SqlDbType.Int, "@dbi", DBNull.Value);
			}


			if (Conv.ToByte(data.GetValue("public").ToString()) == 3) {
				DeleteSavedDefaultConfiguration(group, identifier);

				var t = new List<string>();
				var langs = new List<string>();
				langs.Add("en-GB");
				langs.Add("fi-FI");
				foreach (var l in langs) {
					var x = new Translations(instance, l);
					t.Add(Strings.ChrW(34) + l + Strings.ChrW(34) + ":" + Strings.ChrW(34) +
					      x.GetTranslation("DEFAULT_FILTERING") + Strings.ChrW(34));
				}

				SetDBParam(SqlDbType.NVarChar, "@name", "{" + Conv.ToDelimitedString(t) + "}");
			} else {
				SetDBParam(SqlDbType.NVarChar, "@name", Conv.ToStr(data.GetValue("name").ToString()));
			}


			db.ExecuteInsertQuery(
				"INSERT INTO instance_user_configurations_saved(instance, user_id, [group], identifier, value, [public], name, visible_for_filter, visible_for_usergroup, portfolio_default_view, dashboard_id) VALUES(@instance, @userId, @group, @identifier, @value, @public, @name, @visible_for_filter, @visible_for_usergroup, @pdv, @dbi)",
				DBParameters);

			var configurationId =
				Conv.ToInt(
					db.ExecuteScalar("SELECT TOP 1 user_configuration_id FROM instance_user_configurations_saved ORDER BY user_configuration_id DESC"));

			SetDBParam(SqlDbType.Int, "@confId", configurationId);
			db.ExecuteUpdateQuery(
				"UPDATE instance_user_configurations_saved SET portfolio_default_view = @pdv, dashboard_id = @dbi WHERE user_configuration_id = @confId",
				DBParameters);
			
			return new SavedConfiguration(db.GetDataRow(
				"SELECT * FROM instance_user_configurations_saved WHERE user_configuration_id = " +
				configurationId));
		}

		public void UpdateSavedConfiguration(string group, string identifier, int confId, JObject data) {
			db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@group", group);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.NVarChar, "@value", JsonConvert.SerializeObject(data.GetValue("value")));
			SetDBParam(SqlDbType.TinyInt, "@public", Conv.ToByte(data.GetValue("public").ToString()));
			SetDBParam(SqlDbType.NVarChar, "@name", data.GetValue("name").ToString());
			SetDBParam(SqlDbType.Int, "@confId", confId);
			var vf = "";
			var vf1 = "";
			if (data.GetValue("visible_for_filter") != null && Conv.ToStr(data.GetValue("visible_for_filter")) != "") {
				SetDBParam(SqlDbType.NVarChar, "@visible_for_filter",
					Conv.ToStr(data.GetValue("visible_for_filter").ToString()));
				vf1 = ",visible_for_filter = @visible_for_filter ";
			} else {
				vf1 = ",visible_for_filter = null";
			}

			if (data.GetValue("visible_for_usergroup") != null &&
			    Conv.ToStr(data.GetValue("visible_for_usergroup").ToString()) != "[]") {
				SetDBParam(SqlDbType.NVarChar, "@visible_for_usergroup",
					Conv.ToStr(data.GetValue("visible_for_usergroup").ToString()));
				vf = ",visible_for_usergroup = @visible_for_usergroup ";
			} else {
				vf = ",visible_for_usergroup = null ";
			}

			if (Conv.ToStr(data.GetValue("dashboard_id")) != "") {
				SetDBParam(SqlDbType.Int, "@dashboard_id",
					Conv.ToInt(data.GetValue("dashboard_id").ToString()));
				vf += ",dashboard_id = @dashboard_id ";
			} else {
				vf += ",dashboard_id = null ";
			}

			if (data.GetValue("portfolio_default_view") != null &&
			    Conv.ToInt(data.GetValue("portfolio_default_view")) != 0) {
				SetDBParam(SqlDbType.Int, "@portfolio_default_view",
					Conv.ToInt(data.GetValue("portfolio_default_view").ToString()));
				vf += ",portfolio_default_view = @portfolio_default_view ";
			} else {
				vf += ",portfolio_default_view = -1 ";
			}

			var sql =
				"UPDATE instance_user_configurations_saved SET value = @value, [public] = @public, name = @name " + vf +
				vf1 +
				" WHERE instance = @instance AND [group] = @group AND identifier = @identifier AND user_configuration_id = @confId";

			db.ExecuteUpdateQuery(
				sql,
				DBParameters);
		}

		public void DeleteSavedConfiguration(string group, string identifier, int confId, int userId = 0) {
			db = new Connections(authenticatedSession);

			SetDBParam(SqlDbType.Int, "@userId", userId == 0 ? authenticatedSession.item_id : userId);
			SetDBParam(SqlDbType.NVarChar, "@group", group);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.Int, "@confId", confId);

			db.ExecuteDeleteQuery("DELETE FROM instance_user_configurations_saved " +
			                      "WHERE instance = @instance AND user_id = @userId AND [group] = @group AND identifier = @identifier AND user_configuration_id = @confId",
				DBParameters);
		}

		private void DeleteSavedDefaultConfiguration(string group, string identifier) {
			db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			SetDBParam(SqlDbType.NVarChar, "@group", group);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			db.ExecuteDeleteQuery("DELETE FROM instance_user_configurations_saved " +
			                      "WHERE instance = @instance AND [group] = @group AND identifier = @identifier AND [public] = 3",
				DBParameters);
		}

		public List<UserPreferences> GetConfigurations() {
			var ret = new List<UserPreferences>();

			db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			UserPreferences uc;
			foreach (DataRow r in db
				.GetDatatable(
					"SELECT [group], value, identifier FROM instance_user_configurations WHERE instance = @instance AND user_id = @userId",
					DBParameters).Rows) {
				uc = new UserPreferences(r);
				ret.Add(uc);
			}

			return ret;
		}

		public void SetConfiguration(string group, JObject value) {
			db = new Connections(authenticatedSession);

			foreach (var v in value) {
				SetConfiguration(group, v.Key, v.Value.ToString());
			}
		}

		public void SetConfiguration(string group, string identifier, string value) {
			db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
			SetDBParam(SqlDbType.NVarChar, "@group", group);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.NVarChar, "@value", value);

			db.ExecuteScalar("IF EXISTS(" +
			                 "SELECT * FROM instance_user_configurations " +
			                 "WHERE instance = @instance " +
			                 "AND user_id = @userId " +
			                 "AND [group] = @group " +
			                 "AND identifier = @identifier)" +
			                 "BEGIN " +
			                 "UPDATE instance_user_configurations SET value = @value " +
			                 "WHERE instance = @instance " +
			                 "AND user_id = @userId " +
			                 "AND [group] = @group " +
			                 "AND identifier = @identifier " +
			                 "END " +
			                 "ELSE " +
			                 "BEGIN " +
			                 "INSERT INTO instance_user_configurations(instance, user_id, [group], identifier, value) " +
			                 "VALUES(@instance, @userId, @group, @identifier, @value) " +
			                 "END", DBParameters);
		}
	}
}