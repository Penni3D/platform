﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.core {
	[Route("v1/core/[controller]")]
	public class ClientInformation : RestBase {
		[HttpGet("")]
		public AuthenticatedSession.ClientInformationModel Get() {
			if (currentSession != null) return currentSession.GetClientInformation();
			return null;
		}
	}
}