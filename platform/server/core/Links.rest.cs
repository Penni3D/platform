﻿using System;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.core {
	[Route("v1/core/Links/")]
	public class LinksRest : RestBase {
		[HttpGet("{uuid}")]
		public string GetLink(string uuid) {
			var l = new Links(instance, currentSession);
			var r = l.GetParams(uuid);
			if (r != null) {
				return r;
			}

			Response.StatusCode = 400;
			return null;
		}

		[HttpPost("")]
		public string AddLink([FromBody] dynamic data) {
			string p = JsonConvert.SerializeObject(data.Params);
			int e = Conv.ToInt(JsonConvert.SerializeObject(data.Expiration));
			var l = new Links(instance, currentSession);
			var uuid = Guid.NewGuid().ToString();
			l.SetUrl(uuid, p, e);

			return uuid;
		}
	}
}
