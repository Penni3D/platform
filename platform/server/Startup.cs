﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using BackgroundTasksSample.Services;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Graph;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using ApiPush = Keto5.x.platform.server.@base.ApiPush;
using CompressionLevel = System.IO.Compression.CompressionLevel;
using Directory = System.IO.Directory;
using Process = System.Diagnostics.Process;

namespace Keto5.x.platform.server {
	//The main method is started up first
	public class Program {

		public static IWebHost Host;
		public static void Main(string[] args) {
			//Set up directories
			Config.AppPath = Directory.GetCurrentDirectory();
			Config.WwwPath = Path.Combine(Config.AppPath, "wwwroot/");
			Diag.LogToConsole("Application Path: " + Config.AppPath);
			Diag.LogToConsole("WWW Path: " + Config.WwwPath);

			//Allow diagnostics
			Diag.LogQueries = false;

			//Initialize server cache
			if (Cache.Initialize() == false) {
				Diag.LogToConsole("Cache failed to initialize.", Diag.LogSeverities.Error);
				Exit();
			}

			//Read release information
			var releasefile = new FileInfo(Path.Combine(Config.AppPath, "release.xml"));
			if (releasefile.Exists) {
				var doc = new XmlDocument();
				doc.Load(releasefile.FullName);
				var node = doc.DocumentElement.SelectSingleNode("/release");
				foreach (XmlNode subnode in node.ChildNodes) {
					if (subnode.Name == "version") Config.Version = subnode.InnerText;
					if (subnode.Name == "release-date") Config.VersionDate = subnode.InnerText;
					if (subnode.Name == "repo") Config.Repo = subnode.InnerText;
				}

				if (Config.Repo == "") Config.Repo = "Unknown";
			} else {
				Config.Version = "5.XX.YY";
				Config.Repo = "No Repository";
				var n = DateTime.Now;
				Config.VersionDate = n.ToShortDateString();
			}

			Diag.LogToConsole("Current Version: " + Config.Version + " @ " + Config.VersionDate,
				Diag.LogSeverities.Message);

			//Generate current two-factor authentication secret passcode
			try {
				var authcheck = new TwoFactorAuthentication();
				Diag.LogToConsole("Generic Key: " + authcheck.GenerateHardCodedCode());
			} catch (Exception e) {
				Diag.LogToConsole("Code could not be generated: " + e.Message);
			}

			//Initialize, read and override configuration
			var f = new FileInfo(Path.Combine(Config.WwwPath, "private", "config.json"));
			if (f.Exists) {
				Config.SetJsonFile(Path.Combine("private", "config.json"));
				Config.InitializeSystemList();
				Diag.LogToConsole("Config Basefile: " + Path.Combine(Config.WwwPath, "private", "config.json"));
				var e = Config.Initialize(new Connections(true));
				Config.Environment = Config.GetValue("HostingEnvironment", "Environment");

				var s = Config.GetValue("HostingEnvironment", "ConnectionString");
				Diag.LogToConsole("Server and Database name: " + ExtractKey(s, "Data Source") + " -> " +
				                  ExtractKey(s, "Initial Catalog"), Diag.LogSeverities.Sql);

				Config.EnvironmentIdentifier = ExtractKey(s, "Initial Catalog");

				if (e != null) {
					Diag.LogToConsole(
						"The database cannot be accessed using the connection string from the assembled configuration file.",
						Diag.LogSeverities.Error);
					Diag.LogToConsole(" - Developing locally: check your config.local.json file.",
						Diag.LogSeverities.Error);
					Diag.LogToConsole(" - Configuring: check environment specific config file.",
						Diag.LogSeverities.Error);
					Diag.LogToConsole(" - Then check that the SQL server is accessible.", Diag.LogSeverities.Error);
					Exit();
				}
			} else {
				Diag.LogToConsole("Configuration files could not be accessed: Wrong path: " + f.FullName,
					Diag.LogSeverities.Error);
				Exit();
			}

			//Show Information about Email
			var sending = Conv.ToBool(Config.GetValue("Notifications", "Emails"));
			if (sending) {
				var _host = Conv.ToStr(Config.GetValue("SmtpServer", "Host"));
				var _port = Conv.ToInt(Config.GetValue("SmtpServer", "Port"));
				Diag.LogToConsole("Email Enabled, Host: " + _host + ":" + _port);
			} else {
				Diag.LogToConsole("Email Disabled");
			}

			//Run Migration Scripts
			if (!Diag.ErrorState) {
				if (InitializeServer.Register.CsMigrations() == false) Exit();
				if (InitializeServer.Migrate(args) == false) Exit();
				InitializeServer.RegisteredCsMigrations.Clear();
				Diag.CleanLogs();
			}

			//Store all accessible instances for easy checking when authenticating with a cookie.
			var instance = "keto";
			if (!Diag.ErrorState) {
				try {
					var db = new Connections();
					foreach (DataRow r in db
						.GetDatatable(
							"SELECT ih.instance, i.default_locale_id FROM instance_hosts ih LEFT JOIN instances i ON i.instance = ih.instance GROUP BY ih.instance, i.default_locale_id")
						.Rows) {
						Config.Instances.Add(Conv.ToStr(r["instance"]), Conv.ToStr(r["default_locale_id"]));
						instance = Conv.ToStr(r["instance"]);
					}

					//var releaseFound = false;
					var release = db.GetDataRow("SELECT TOP 1 * FROM release_history ORDER BY installation_date DESC");

					var repository = new SqlParameter("repository", SqlDbType.NVarChar, 50);
					repository.Value = Config.Repo;

					var attachments_data_db = new SqlParameter("attachments_data_db", SqlDbType.NVarChar, 255);
					attachments_data_db.Value =
						ParseDB(Config.GetValue("HostingEnvironment", "AttachmentConnectionString"));

					if (!(release != null && Conv.ToStr(release["release_version"]) == Config.Version
						  && Conv.ToStr(release["repository"]).Equals(Conv.ToStr(repository.Value))
 						  && Conv.ToStr(release["attachments_data_db"]).Equals(Conv.ToStr(attachments_data_db.Value)))) {

						var p = new List<SqlParameter>();

						var release_version = new SqlParameter("release_version", SqlDbType.NVarChar, 20);
						release_version.Value = Config.Version;

						var release_date = new SqlParameter("release_date", SqlDbType.NVarChar, 20);
						release_date.Value = Config.VersionDate;

						p.Add(release_version);
						p.Add(release_date);
						p.Add(repository);
						p.Add(attachments_data_db);

						db.ExecuteInsertQuery(
							"INSERT INTO release_history (release_version, release_date, installation_date, repository, attachments_data_db) VALUES (@release_version, @release_date, getutcdate(), @repository, @attachments_data_db)",
							p);
					}
				} catch (Exception) {
					Diag.LogToConsole("Database Error loading hosts");
					Exit();
				}
			}

			//Set virtual directory
			var wwwpath = Config.GetValue("HostingEnvironment", "VirtualDirectory");
			wwwpath = wwwpath != "" ? "/" + wwwpath : "/";
			Config.VirtualDirectory = wwwpath;

			//Register Platform Capabilities
			if (!Diag.ErrorState) {
				if (InitializeServer.Register.DashboardCharts() == false) Exit();
				if (InitializeServer.Register.Subqueries() == false) Exit();
				if (InitializeServer.Register.HttpResponseParsers() == false) Exit();
				if (InitializeServer.Register.BackgroundProcesses() == false) Exit();
				if (InitializeServer.Register.CustomActions() == false) Exit();
				if (InitializeServer.Register.Features() == false) Exit();
				if (InitializeServer.InitializeCustomerConfiguration() == false) Exit();
				if (InitializeServer.CacheSubQueries() == false) Exit();
				if (InitializeServer.CacheItemProcesses() == false) Exit();
				if (InitializeServer.IndexProcesses() == false) Exit();
				var ConfigurationReport = new ConfigurationReport();
				
				
				ConfigurationReport.GenerateReport(DummySession.Create(instance, "",1));
			}

			Console.WriteLine("Default Domain: " + Config.GetDefaultDomain());

			//Excel licensing
			ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

			//Start application
			Host = WebHost.CreateDefaultBuilder()
				.SuppressStatusMessages(true)
				.UseContentRoot(Config.AppPath)
				.UseUrls("https://*:" + Config.GetValue("HostingEnvironment", "InternalPort"))
				.UseStartup<Startup>()
				.ConfigureKestrel(options => {
					options.AddServerHeader = false;
					options.Limits.MaxRequestBodySize = null;
				})
				.Build();

			try {
				Console.WriteLine("Host started...");
				//CreateWebHostBuilder(args).Build().Run();
				Host.Run();
			} catch (Exception e) {
				//Make sure that the application process is actually dead
				Console.WriteLine(e.Message);
				Process.GetCurrentProcess().Kill();
			}

			
		}

		private static string ParseDB(string connection_string) {
			foreach (string part in connection_string.Split(";")) {
				if (part.Left(16) == "Initial Catalog=") {
					return part.Split("=")[1];
				}
			}

			return "";
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>();

		private static string ExtractKey(string s, string k) {
			if (!s.Contains(";")) return "";
			foreach (var key in s.Split(";")) {
				if (key.Contains("=") && key.Split("=")[0] == k) return key.Split("=")[1];
			}

			return "";
		}

		private static void Exit(int code = -1) {
			if (Config.Environment != null && Config.Environment.ToLower() == "Jenkins") Environment.Exit(code);
			Diag.ErrorState = true;
		}

		public static bool RunPreMigrationsScripts() {
			return true;
		}

		/// <summary>
		/// Scripts that should be run after migrations are passed
		/// </summary>
		/// <returns>Return false if startup should be prevented</returns>
		public static bool RunPostMigrationsScripts() {
			return true;
		}

		private static List<string> CloneList(List<FieldConfig> origList) {
			var copyOfRequired = new List<string>();
			copyOfRequired.AddRange(origList.Select(x => x.Name));
			return copyOfRequired;
		}

		private enum LogCategories {
			Neutral = 0,
			Success = 1,
			Error = 2
		}
	}


	//Set exception to be shown in the console
	public class ExceptionFilter : ActionFilterAttribute, IExceptionFilter {
		public void OnException(ExceptionContext context) {
			var e = context.Exception;

			var developerMode = context.HttpContext.Request.Host.Host == "localhost";
			var logMessage = "";

			try {
				//Get request instance to log an exception
				var conbase = new ConnectionBase("", new AuthenticatedSession("", 0, null));
				var instance = conbase.GetInstance(context.HttpContext.Request.Host.Host);

				//Get user_id
				var itemId = 0;
				var cookiedata =
					Conv.ToStr(context.HttpContext.Request.Cookies[SecurityNaming.AuthorisationCookieName]);
				if (cookiedata != "") {
					//Read cookie
					var cookiearray = cookiedata.Split(new[] { '&' });
					itemId = Conv.ToInt(cookiearray[0].Split(new[] { '=' })[1]);
				}

				var id = 0;

				//Log the exception
				if (e.GetType() == typeof(CustomArgumentException) ||
				    e.GetType() == typeof(CustomPermissionException) ||
				    e is CustomUniqueException) {
					var customServerException = (BaseServerException)e;
					id = Diag.Log(instance, Conv.ToStr(customServerException.Messages["description"]),
						Conv.ToStr(customServerException.Messages["stacktrace"]),
						e.Data.Contains("process") ? Conv.ToStr(e.Data["process"]) : "", itemId);
				} else {
					id = Diag.Log(instance, e, e.Data.Contains("process") ? Conv.ToStr(e.Data["process"]) : "", itemId);
				}

				logMessage = Conv.ToStr(id);
			} catch {
				//Logging failed. Output the exception message
				Diag.LogToConsole(e.Message, Diag.LogSeverities.Error);
			}

			if (e.GetType() == typeof(CustomArgumentException) || e.GetType() == typeof(CustomPermissionException) ||
			    e is CustomUniqueException) {
				var customServerException = (BaseServerException)e;
				context.HttpContext.Response.StatusCode = customServerException.Status;
				context.Result = customServerException.GetObjectResult(developerMode, "", logMessage);
			} else {
				var d = new Dictionary<string, object>();
				var messages = new Dictionary<string, object>();

				if (developerMode) {
					messages.Add("description", e.Message);
					messages.Add("stacktrace", e.StackTrace);
				} else {
					messages.Add("description", "An Error Occured");
					messages.Add("stacktrace", "Not Available");
				}

				messages.Add("logId", logMessage);
				d.Add("Info", messages);
				d.Add("Type", "UNHANDELED_EXCEPTION");
				d.Add("Variable", "SERVER_ERROR");
				d.Add("Data", null);
				context.HttpContext.Response.StatusCode = 500;
				context.Result = new ObjectResult(d);
			}
		}
	}

	//Main method starts the Startup class for server side configration
	public class Startup {
		public void ConfigureServices(IServiceCollection services) {
			services.AddLogging(
				builder => {
					builder.AddFilter("Microsoft", LogLevel.Warning)
						.AddFilter("System", LogLevel.Warning)
						.AddConsole();
				});

			services.AddCors(options => {
				options.AddPolicy("AllowOrigin",
					builder => {
						builder.WithOrigins("https://localhost:5001").AllowAnyMethod().AllowAnyHeader()
							.AllowCredentials();
					});
			});

			//MVC Service & Serialisation
			services.AddMvc(options => {
					options.OutputFormatters.Add(new HtmlOutputFormatter());
					options.Filters.Add(typeof(ExceptionFilter));
					options.EnableEndpointRouting = false;
					options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
					options.Conventions.Add(new ApiExplorerVisibilityEnabledConvention());
				})
				.AddNewtonsoftJson(opt => {
						opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
						opt.SerializerSettings.ContractResolver = new DefaultContractResolver();
					}
				);

			//HTTPS Enforce
			services.AddHttpsRedirection(options => {
				options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
			});

			//HTTP Strict Transport Security Options
			services.AddHsts(options => {
				options.Preload = true;
				options.IncludeSubDomains = true;
				options.MaxAge = TimeSpan.FromDays(365);
				//Use these to exclude domains if need be:
				//options.ExcludedHosts.Add("example.com");
				//options.ExcludedHosts.Add("www.example.com");
			});

			//Session Cookie Settings
			services.AddSession(o => {
				o.IdleTimeout = TimeSpan.FromMinutes(30);
				o.Cookie.Path = Config.VirtualDirectory;
				o.Cookie.HttpOnly = true;
				o.Cookie.SameSite = SameSiteMode.Strict;
				o.Cookie.SecurePolicy = CookieSecurePolicy.Always;
				o.Cookie.Name = SecurityNaming.SessionCookieName;
			});
			services.Configure<IISServerOptions>(options => { options.MaxRequestBodySize = int.MaxValue; });
			services.Configure<FormOptions>(x => {
				x.ValueLengthLimit = int.MaxValue;
				x.MultipartBodyLengthLimit = int.MaxValue;
			});

			//SignalR Service
			services.AddSignalR(options => { options.EnableDetailedErrors = true; });
			services.Configure<BrotliCompressionProviderOptions>(options => {
				options.Level = CompressionLevel.Fastest;
			});
			services.Configure<GzipCompressionProviderOptions>(options => {
				options.Level = CompressionLevel.Fastest;
			});
			services.AddResponseCompression(options => { options.EnableForHttps = true; });

			services.Configure<CookiePolicyOptions>(options => {
				options.CheckConsentNeeded = context => true;
				options.MinimumSameSitePolicy = SameSiteMode.None;
			});

			services.AddHostedService<TimedHostedService>();
			services.AddHostedService<HostedServiceContext>();
		}

		public static IHubContext<ApiPush> SignalR;

		private void OnShutdown() {
			Diag.FlushRequests();
			Cache.Initialize();
			ConnectedUsers.Clear();
			
			try {
				Program.Host.StopAsync().RunSynchronously();
				Environment.Exit(0);
			} catch (Exception e) {
				//Process.GetCurrentProcess().Kill();
			}
		}

		public void Configure(IApplicationBuilder app, IServiceProvider sp, IHubContext<ApiPush> hubContextInjection,
			IHostApplicationLifetime applicationLifetime) {
			var chosenCulture = new CultureInfo("en-US");
			CultureInfo.DefaultThreadCurrentCulture = chosenCulture;
			CultureInfo.DefaultThreadCurrentUICulture = chosenCulture;
			var localizationOptions = new RequestLocalizationOptions {
				SupportedCultures = new List<CultureInfo> {
					chosenCulture
				},
				SupportedUICultures = new List<CultureInfo> {
					chosenCulture
				},
				DefaultRequestCulture = new RequestCulture(chosenCulture),
				FallBackToParentCultures = false,
				FallBackToParentUICultures = false,
				RequestCultureProviders = null
			};

			//Enable Configured Services
			app.UseRequestLocalization(localizationOptions);
			app.UseHsts();
			app.UseHttpsRedirection();
			app.UseSession();
			app.UseExceptionHandler("/model/Error");
			app.UseResponseCompression();

			//Instruct the server to find static JS, CSS etc files from 'public' folder EXCEPT for the icons
			app.UseWhen(
				context => !context.Request.Path.StartsWithSegments("/fonts"),
				appBuilder => appBuilder.UseStaticFiles(new StaticFileOptions {
					FileProvider = new PhysicalFileProvider(
						Path.Combine(Config.WwwPath, @"public")),
					RequestPath = new PathString("")
				}));

			//Define routes
			app.MapWhen(
				c => c.Request.Path.Value.StartsWith("/Shibboleth.sso"),
				delegate { }
			);

			applicationLifetime.ApplicationStopping.Register(OnShutdown);

			app.UseWebSockets();

			SignalR = hubContextInjection;
			app.Use(async (context, next) => {
				context.Response.Headers.Add("X-Frame-Options", "deny");
				context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
				context.Response.Headers.Add("X-XSS-Protection", "1; mode=block");
				context.Response.Headers.Add("X-Permitted-Cross-Domain-Policies", "none");
				context.Response.Headers.Add("Access-Control-Allow-Origin", "<origin>");
				
				await next();
			});
			app.UseCors("AllowOrigin");
			app.UseMvc();
			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseCookiePolicy();
			app.UseRouting();
			app.UseEndpoints(endpoints => { endpoints.MapHub<ApiPush>("/ApiHub"); });
		}
	}

	public class FieldConfig {
		public static bool NULLABLE = true;
		public static bool VALUE_REQUIRED = false;

		public FieldConfig(string name, bool nullable) {
			Name = name;
			Nullable = nullable;
		}

		public string Name { get; set; }
		public bool Nullable { get; set; }
	}

	public class HtmlOutputFormatter : StringOutputFormatter {
		public HtmlOutputFormatter() {
			SupportedMediaTypes.Add("text/html");
		}
	}

	public class ApiExplorerVisibilityEnabledConvention : IApplicationModelConvention {
		public void Apply(ApplicationModel application) {
			// var httpGets = new List<string>();
			// var httpPost = new List<string>();
			// var httpPuts = new List<string>();
			// var httpDeletes = new List<string>();
			// var otherTypes = new List<string>();
			//
			// foreach (var controller in application.Controllers) {
			// 	var route = "";
			// 	foreach (var cont in controller.Attributes) {
			// 		if (cont.GetType().ToString() == "Microsoft.AspNetCore.Mvc.RouteAttribute") {
			// 			route = ((RouteAttribute) cont).Template.Replace("[controller]",
			// 				controller.ControllerName);
			// 		}
			// 	}
			//
			// 	if (route.Length > 0 && route[^1] != '/') route += "/";
			// 	foreach (var action in controller.Actions) {
			// 		foreach (var x in action.Attributes) {
			// 			switch (x.GetType().ToString()) {
			// 				case "Microsoft.AspNetCore.Mvc.HttpGetAttribute":
			// 					httpGets.Add(route + ((HttpMethodAttribute) x).Template);
			// 					break;
			// 				case "Microsoft.AspNetCore.Mvc.HttpPostAttribute":
			// 					httpPost.Add(route + ((HttpMethodAttribute) x).Template);
			// 					break;
			// 				case "Microsoft.AspNetCore.Mvc.HttpPutAttribute":
			// 					httpPuts.Add(route + ((HttpMethodAttribute) x).Template);
			// 					break;
			// 				case "Microsoft.AspNetCore.Mvc.HttpDeleteAttribute":
			// 					httpDeletes.Add(route + ((HttpMethodAttribute) x).Template);
			// 					break;
			// 				default:
			// 					otherTypes.Add(route + ((HttpMethodAttribute) x).Template);
			// 					break;
			// 			}
			// 		}
			// 	}
			// }
			//
			// var restsDicts = new Dictionary<string, List<string>>();
			// restsDicts.Add("GET", httpGets);
			// restsDicts.Add("POST", httpPost);
			// restsDicts.Add("PUT", httpPuts);
			// restsDicts.Add("DELETE", httpDeletes);
			// restsDicts.Add("OTHER", otherTypes);
			//
			// Cache.Set("", "rests", restsDicts);
		}
	}
}