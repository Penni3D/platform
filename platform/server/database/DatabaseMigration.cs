using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Keto5.x.platform.server.common;

namespace Keto5.x.platform.server.database {
	public abstract class CsMigration {
		public abstract bool Execute(IConnections db);
		public string message = "";
	}

	public class DbMigration {
		public DbMigration(FileInfo fileInfo) {
			Name = fileInfo.Name;
			FileInfo = fileInfo;
			Version = fileInfo.Name.Split('_')[0];
			var items = Version.Split('.');
			var mainV = new string(items[0].ToCharArray().Where(c => char.IsDigit(c)).ToArray());

			MainVersion = int.Parse(mainV);
			SecondLevel = int.Parse(items[1]);
			ThirdLevel = int.Parse(items[2]);
			if (items.Length == 4) {
				FrameworkVersion = int.Parse(items[3]);
			} else {
				FrameworkVersion = 0;
			}
		}

		public int MainVersion { get; set; }
		public int SecondLevel { get; set; }
		public int ThirdLevel { get; set; }
		public int FrameworkVersion { get; set; }

		public string Version { get; set; }

		public string Name { get; set; }
		public FileInfo FileInfo { get; set; }
	}

	public class DatabaseMigration {
		private readonly List<string> _existingMigrationVersions = new List<string>();

		private readonly string[] args;
		public IConnections db;

		private List<Dictionary<string, object>> existingMigrations;

		public int PreviousScripts;

		public DatabaseMigration(string[] args) {
			this.args = args;
			db = new Connections(true);
		}

		public bool RunDatabaseMigrations() {
			var buildBaseline = args.Contains("baseline");
			var force = args.Contains("force");
			var autoMigrateDatabase = !buildBaseline &&
			                          (Conv.ToBool(Config.GetValue("Deployment", "AutoDatabaseMigrate")) ||
			                           args.Contains("migrate"));

			// this option ('nomigration') exists to allow jenkins to do version checks without trying to write to database
			var nomigrationSelected = args.Contains("nomigration");
			if (nomigrationSelected) {
				buildBaseline = false;
				autoMigrateDatabase = true;
			}

			CreateDatabaseMigrationsTable();
			if (force || buildBaseline) ClearDatabaseMigrationsTable();

			var start = "Starting Migration tasks";
			start += buildBaseline ? " -> Building Baseline" : " -> Checking files to migrate";
			Diag.LogToConsole(start, Diag.LogSeverities.SuccessMessage);

			var migrationSuccess = Execute(autoMigrateDatabase, buildBaseline, force);
			if (migrationSuccess) {
				if (nomigrationSelected) {
					Environment.Exit(0);
				}

				return true;
			}

			if (nomigrationSelected) {
				Environment.Exit(1);
			}

			Diag.LogToConsole("Migration failed.", Diag.LogSeverities.Error);
			return false;
		}

		private bool Execute(bool executeScripts, bool buildBaseline, bool force) {
			existingMigrations =
				db.GetDatatableDictionary("SELECT * FROM database_migrations ORDER BY id asc");
			try {
				RunMigrations(executeScripts, buildBaseline, force);
				// dump versions in file
				var versionLockFile = Path.Combine(Config.AppPath, "platform", "db", "migration-versions.lock");
				try {
					File.WriteAllLines(versionLockFile, _existingMigrationVersions);
				} catch (Exception) {
					Diag.LogToConsole("Unable to write lock file. Check permissions.");
				}

				return true;
			} catch (Exception e) {
				Diag.LogToConsole(e.Message);
				return false;
			}
		}

		public List<DbMigration> SortMigrationFiles(List<FileInfo> files) {
			var sorted = files.Select(s => new DbMigration(s))
				.OrderBy(s => s.SecondLevel)
				.ThenBy(s => s.ThirdLevel)
				.ThenBy(s => s.FrameworkVersion)
				.ToList();

			return sorted;
		}

		private List<DbMigration> GetFileList() {
			var platformFiles = GetMigrations("platform");
			var frameworkFiles = GetMigrations("customer");
			var files = new List<FileInfo>();
			files.AddRange(platformFiles);
			files.AddRange(frameworkFiles);
			return SortMigrationFiles(files);
		}

		private List<FileInfo> GetMigrations(string repo) {
			var platformDbScripts = new DirectoryInfo(Path.Combine(Config.AppPath, repo, "db"));
			if (!platformDbScripts.Exists) return new List<FileInfo>();
			return platformDbScripts.GetFiles("V*.*").ToList();
		}

		private List<FileInfo> GetPlatformMigrations() {
			return GetMigrations("platform");
		}

		private void ClearDatabaseMigrationsTable() {
			db.ExecuteDeleteQuery("DELETE FROM database_migrations", null);
		}

		private void CreateDatabaseMigrationsTable() {
			var checkIfTableExists =
				"SELECT count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'database_migrations'";

			var data = Conv.ToInt(db.ExecuteScalar(checkIfTableExists));
			if (data == 0) {
				db.CreateSchemaVersionTable();
			}
		}

		private string GetMigrationExecutedScript(DbMigration migration) {
			var insert = new InsertQuery();
			var version = migration.Version;
			var filename = migration.Name;
			var dict = new Dictionary<string, object> {
				{"version", "'" + version + "'"},
				{"filename", "'" + filename + "'"}
			};
			insert.To("database_migrations")
				.Values(dict);
			return insert.Get();
		}

		private void RunMigrations(bool executeScripts, bool buildBaseline, bool force) {
			var migrations = GetFileList();
			var i = 0;
			var previousVersion = "";
			_existingMigrationVersions.Add(
				"# This file is used to cause conflicts in merge when migration versions collide. This is automatically built by migration gadget.");
			foreach (var migration in migrations) {
				if (migration.FrameworkVersion == 0) {
					// Write only platform migrations to lock file
					_existingMigrationVersions.Add(migration.Name);
				}

				if (migration.Version.Equals(previousVersion) || string.IsNullOrEmpty(migration.Version)) {
					Diag.LogToConsole("Check versioning, found doubles or empty: " + migration.Name,
						Diag.LogSeverities.Error);
					throw new Exception();
				}

				previousVersion = migration.Version;

				if (existingMigrations.Count > i && force == false) {
					var version = Conv.ToStr(existingMigrations[i]["version"]);
					var fileName = Conv.ToStr(existingMigrations[i]["filename"]);
					if (version.Equals(migration.Version) && fileName.Equals(migration.Name)) {
						//Diag.LogToConsole("Migration File: " + migration.Name + " -> Already Migrated -> No Action Taken", Diag.LogSeverities.Message);
						PreviousScripts += 1;
					} else {
						Diag.LogToConsole("Migration order is messed up!", Diag.LogSeverities.Error);
						Diag.LogToConsole(" Next file would be: " + migration.Name +
						                  "  but this already executed (in database_migrations -table):" + fileName,
							Diag.LogSeverities.Warning);
						throw new Exception();
					}
				} else {
					var cstr = "Migration File: " + migration.Name;
					var sev = Diag.LogSeverities.Message;
					var db = new Connections(true);

					//C# Migration
					if (migration.FileInfo.Name.ToLower().EndsWith(".cs")) {
						if (InitializeServer.RegisteredCsMigrations.ContainsKey(migration.FileInfo.Name) == false) {
							cstr += " -> was found but no corresponding server side class was registered.";
							sev = Diag.LogSeverities.Error;
							Diag.LogToConsole(cstr, sev);
							if (!force) throw new Exception("No server file registered.");
						}

						var m = InitializeServer.RegisteredCsMigrations[migration.FileInfo.Name];

						if (executeScripts || force) {
							if (m.Execute(db)) {
								cstr += " -> Executed";
								sev = Diag.LogSeverities.SuccessMessage;

								if (force) cstr += " (forced)";
								if (executeScripts || buildBaseline || force) {
									var insertScriptForMigrationTable = GetMigrationExecutedScript(migration);
									db.ExecuteDatabaseMigrationInsert(insertScriptForMigrationTable);
									cstr += buildBaseline ? " -> Marked as migrated" : " -> Migrated";
									sev = Diag.LogSeverities.SuccessMessage;
								}
							} else {
								cstr += " -> Execution resulted in an error: " + m.message;
								sev = Diag.LogSeverities.Error;
								Diag.LogToConsole(cstr, sev);
								if (!force) throw new Exception("Execution failed");
							}
						}

						Diag.LogToConsole(cstr, sev);
						continue;
					}

					//SQL Migration
					var sql = File.ReadAllText(migration.FileInfo.FullName);

					db.BeginTransaction();
					try {
						if (executeScripts || force) {
							db.ExecuteDeploymentScript(sql);
							cstr += " -> Executed";
							sev = Diag.LogSeverities.SuccessMessage;
						}

						if (force) cstr += " (forced)";
						if (executeScripts || buildBaseline || force) {
							var insertScriptForMigrationTable = GetMigrationExecutedScript(migration);
							db.ExecuteDatabaseMigrationInsert(insertScriptForMigrationTable);
							cstr += buildBaseline ? " -> Marked as migrated" : " -> Migrated";
							sev = Diag.LogSeverities.SuccessMessage;
						}
					} catch (Exception e) {
						Diag.SupressException(e);
						db.RollbackTransaction();
						cstr += " -> Failed & Rolled Back";
						sev = Diag.LogSeverities.Error;
						Diag.LogToConsole(cstr, sev);
						if (!force) throw;
						var insertScriptForMigrationTable = GetMigrationExecutedScript(migration);
						db.ExecuteDatabaseMigrationInsert(insertScriptForMigrationTable);
						continue;
					}

					if (sev == Diag.LogSeverities.Message) {
						cstr += " -> No Action taken";
						sev = Diag.LogSeverities.Warning;
					}

					try {
						db.CommitTransaction();
					} catch (Exception) {
						cstr += " -> Failed as the script has an invalid Commit Transaction logic";
						sev = Diag.LogSeverities.Error;
						Diag.LogToConsole(cstr, sev);
						if (force) continue;
						throw;
					}

					Diag.LogToConsole(cstr, sev);
				}

				i++;
			}
		}
	}
}