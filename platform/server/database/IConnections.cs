﻿using System.Collections.Generic;
using System.Data;

using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.database {
	public interface IConnections {
		int lastRowTotal { get; set; }
		int lastRowTotal2 { get; set; }
		string instance { get; set; }

		/// <summary>
		/// Adds CONTEXT_INFO setting to provided SQL Query in order to indicate current user item id for the archive system
		/// </summary>
		string addContextInfo(string cmdText, bool triggerOverride = false);

		/// <summary>
		/// Executes a SQL command to insert data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns>Does not return identity</returns>
		int ExecuteInsertQuery(string cmdText, List<SqlParameter> cmdParams);

		/// <summary>
		///  Executes a SQL command to insert data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <param name="TableName"></param>
		/// <param name="IdentityColumnName"></param>
		/// <returns>Returns MAX(identity column) from TABLE</returns>
		int ExecuteInsertQuery(string cmdText, List<SqlParameter> cmdParams, string TableName,
			string IdentityColumnName);

		int ExecuteInsertQuery(List<InsertQuery> queries, List<SqlParameter> cmdParameters);

		int ExecuteInsertQuery(InsertQuery query, List<SqlParameter> cmdParameters);
		DataTable PrepareDataTable(string tableName);
		void ExecuteFromDataTable(DataTable dt);

		/// <summary>
		/// Executes a SQL command to update data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		int ExecuteUpdateQuery(string cmdText, List<SqlParameter> cmdParams);
		int ExecuteUpdateQueryWithoutArchive(string cmdText, List<SqlParameter> cmdParams);
		
		/// <summary>
		/// Executes a SQL command to delete data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		int ExecuteDeleteQuery(string cmdText, List<SqlParameter> cmdParams);
		int ExecuteDeleteQueryWithoutArchive(string cmdText, List<SqlParameter> cmdParams);
		int ExecuteDeleteQuery(DeleteQuery query, List<SqlParameter> cmdParams);

		int ExecuteDeleteQuery(List<DeleteQuery> queries, List<SqlParameter> cmdParams);

		/// <summary>
		/// Executes a stored procedure
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		int ExecuteStoredProcedure(string cmdText, List<SqlParameter> cmdParams);

		void CreateSchemaVersionTable();

		void ExecuteDeploymentScript(string script);

		void ExecuteDatabaseMigrationInsert(string script);

		void BeginTransaction();

		void CommitTransaction();

		void RollbackTransaction();

		SqlConnection GetConnection();

		/// <summary>
		/// Get's paged data from the database into DataTable object. Total number of rows in the table can be fetched with lastRowTotal method.
		/// </summary>
		/// <param name="cmdText">SQL Query without Order By clause</param>
		/// <param name="OrderBy">Order By clause. Include ASC / DESC</param>
		/// <param name="PrimaryOrderColumn">Helper column to enable row count calculation. Preferably primary key id column name. Must be different to OrderBy clause column names</param>
		/// <param name="StartFromRow">Row number to start from</param>
		/// <param name="ReturnRowCount">Number of rows to return</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>DataTable</returns>
		DataTable GetDatatable(string cmdText, string OrderBy, string PrimaryOrderColumn, int StartFromRow,
			int ReturnRowCount, List<SqlParameter> cmdParams = null, string sumCols = "", bool fetchKeyInfo = false, bool inThread = false, string optionalCountWrapSql = "");

		DataTable GetDatatable(SelectQuery query, List<SqlParameter> cmdParams = null, bool fetchKeyInfo = false, bool inThread = false);


		/// <summary>
		/// Get's non paged data from the database into DataTable object.
		/// </summary>
		/// <param name="cmdText">SQL Query</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>DataTable</returns>
		DataTable GetDatatable(string cmdText, List<SqlParameter> cmdParams = null, bool fetchKeyInfo = false, bool inThread = false);

		/// <summary>
		/// Get's paged data from the database into json string. Total number of rows in the table can be fetched with lastRowTotal method.
		/// </summary>
		/// <param name="cmdText">SQL Query without Order By clause</param>
		/// <param name="OrderBy">Order By clause. Include ASC / DESC</param>
		/// <param name="PrimaryOrderColumn">Helper column to enable row count calculation. Preferably primary key id column name. Must be different to OrderBy clause column names</param>
		/// <param name="StartFromRow">Row number to start from</param>
		/// <param name="ReturnRowCount">Number of rows to return</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>Json String serialised from DataTable object</returns>
		string GetDatatableJson(string cmdText, string OrderBy, string PrimaryOrderColumn, int StartFromRow,
			int ReturnRowCount, List<SqlParameter> cmdParams = null);

		/// <summary>
		/// Get's paged data from the database into json string.
		/// </summary>
		/// <param name="cmdText">SQL Query</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>Json String serialised from DataTable object</returns>
		string GetDatatableJson(string cmdText, List<SqlParameter> cmdParams = null);

		/// <summary>
		/// Serializes DataTable into json string
		/// </summary>
		/// <param name="dt"></param>
		/// <returns></returns>
		string GetDatatableJson(DataTable dt);

		List<Dictionary<string, object>> GetDatatableDictionary(string cmdText, string OrderBy,
			string PrimaryOrderColumn, int StartFromRow, int ReturnRowCount, List<SqlParameter> cmdParams = null);

		/// <summary>
		/// Get's paged data from the database into json string.
		/// </summary>
		/// <param name="cmdText">SQL Query</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>Json String serialised from DataTable object</returns>
		List<Dictionary<string, object>> GetDatatableDictionary(string cmdText, List<SqlParameter> cmdParams = null);

		/// <summary>
		/// Overridden method. You can pass SelectQuery object directly for this
		/// </summary>
		/// <param name="query">SelectQuery object</param>
		/// <param name="cmdParams">Query parameters</param>
		/// <returns></returns>
		List<Dictionary<string, object>>
			GetDatatableDictionary(SelectQuery query, List<SqlParameter> cmdParams = null);

		/// <summary>
		/// Serializes DataTable into json string
		/// </summary>
		/// <param name="dt"></param>
		/// <returns></returns>
		List<Dictionary<string, object>> GetDatatableDictionary(DataTable dt);

		DataRow GetDataRow(SelectQuery query, List<SqlParameter> cmdParameters = null);

		DataRow GetDataRow(string cmdText, List<SqlParameter> cmdParams = null);

		string GetDataRowJson(string cmdText, List<SqlParameter> cmdParams = null);

		string GetDataRowJson(DataRow row);

		object ExecuteScalar(SelectQuery query, List<SqlParameter> cmdParams = null);

		/// <summary>
		/// Executes a SQL command intended to return single value
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		object ExecuteScalar(string cmdText, List<SqlParameter> cmdParams = null);

		List<Dictionary<string, object>> DataTableToDictionary(DataTable dt);
	}
}