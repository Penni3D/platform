﻿using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.database{
	
	public interface IEntityServiceProvider{
		IDatabaseItemService GetEntityService(string process);
	}

	public class EntityServiceProvider : IEntityServiceProvider {
		private readonly string _instance;
		private readonly AuthenticatedSession _session;

		public EntityServiceProvider(string instance, AuthenticatedSession session) {
			_instance = instance;
			_session = session;
		}

		public IDatabaseItemService GetEntityService(string process) {
			return new DatabaseItemService(_instance, process, _session);
		}
	}
}