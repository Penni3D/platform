﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.database {
	public class Connections : IConnections {
		private string _connectionString;
		private readonly int _currUserId;
		private readonly int _insertUserId;

		private readonly bool _preventLogging;
		private SqlTransaction _transaction;
		private SqlConnection _transactionConnection;
		private bool _transactionOpen;
		private readonly int timeout = 120;
		public string instance { get; set; }
		public int lastRowTotal { get; set; }
		public int lastRowTotal2 { get; set; }

		#region Public Methods

		public Connections(AuthenticatedSession currentUserId) {
			_connectionString = Config.GetValue("HostingEnvironment", "ConnectionString");
			_currUserId = currentUserId.item_id;
			_insertUserId = currentUserId.DelegatorOrItemId;
		}

		public Connections() {
			_connectionString = Config.GetValue("HostingEnvironment", "ConnectionString");
		}

		public Connections(bool preventLogging) {
			_connectionString = Config.GetValue("HostingEnvironment", "ConnectionString");
			_preventLogging = preventLogging;
		}

		public Connections(string connString) {
			_connectionString = connString;
		}

		public Connections(string connString, int currentUserId) {
			_connectionString = connString;
			_currUserId = currentUserId;
			_insertUserId = currentUserId;
		}

		public void SetAttachmentsDb() {
			_connectionString = Config.GetValue("HostingEnvironment", "AttachmentConnectionString");
		}
		
		/// <summary>
		/// Adds CONTEXT_INFO setting to provided SQL Query in order to indicate current user item id for the archive system
		/// </summary>
		public string addContextInfo(string cmdText, bool triggerOverride = false) {
			return "DECLARE @BinVar varbinary(128); SET @BinVar = CONVERT(varbinary(128), " +
			       (triggerOverride ? -1 : _insertUserId) +
			       "); SET CONTEXT_INFO @BinVar;" + cmdText +
			       ";SET @BinVar = CONVERT(varbinary(128), 0); SET CONTEXT_INFO @BinVar;";
		}

		/// <summary>
		/// Executes a SQL command to insert data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns>Does not return identity</returns>
		public int ExecuteInsertQuery(string cmdText, List<SqlParameter> cmdParams) {
			return Conv.ToInt(ExecuteScalar(cmdText + ";SELECT @@IDENTITY", cmdParams));
		}

		/// <summary>
		///  Executes a SQL command to insert data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <param name="tableName"></param>
		/// <param name="identityColumnName"></param>
		/// <returns>Returns MAX(identity column) from TABLE</returns>
		public int ExecuteInsertQuery(string cmdText, List<SqlParameter> cmdParams, string tableName,
			string identityColumnName) {
			return Conv.ToInt(ExecuteScalar(
				cmdText + ";SELECT MAX(" + identityColumnName + ") FROM " + tableName +
				               " WHERE archive_userid = convert(int, convert(varbinary(4), CONTEXT_INFO()));",
				cmdParams));
		}

		public int ExecuteInsertQuery(List<InsertQuery> queries, List<SqlParameter> cmdParameters) {
			var queryString = queries.Aggregate("", (current, insertQuery) => current + insertQuery.Get());
			return ExecuteInsertQuery(queryString, cmdParameters);
		}

		public int ExecuteInsertQuery(InsertQuery query, List<SqlParameter> cmdParameters) {
			return ExecuteInsertQuery(query.Get(), cmdParameters);
		}


		/// <summary>
		/// Executes a SQL command to update data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		public int ExecuteUpdateQuery(string cmdText, List<SqlParameter> cmdParams) {
			return ExecuteNonQuery(addContextInfo(cmdText), cmdParams);
		}

		public int ExecuteUpdateQueryWithoutArchive(string cmdText, List<SqlParameter> cmdParams) {
			return ExecuteNonQuery(addContextInfo(cmdText, true), cmdParams);
		}

		/// <summary>
		/// Executes a SQL command to delete data
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		public int ExecuteDeleteQuery(string cmdText, List<SqlParameter> cmdParams) {
			return ExecuteNonQuery(addContextInfo(cmdText), cmdParams);
		}

		public int ExecuteDeleteQueryWithoutArchive(string cmdText, List<SqlParameter> cmdParams) {
			return ExecuteNonQuery(addContextInfo(cmdText, true), cmdParams);
		}

		public int ExecuteDeleteQuery(DeleteQuery query, List<SqlParameter> cmdParams) {
			return ExecuteDeleteQuery(query.Get(), cmdParams);
		}

		public int ExecuteDeleteQuery(List<DeleteQuery> queries, List<SqlParameter> cmdParams) {
			var queryString = queries.Aggregate("", (current, deleteQuery) => current + deleteQuery.Get());
			return ExecuteDeleteQuery(queryString, cmdParams);
		}

		/// <summary>
		/// Executes a stored procedure
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		public int ExecuteStoredProcedure(string cmdText, List<SqlParameter> cmdParams) {
			return ExecuteNonQuery(addContextInfo(cmdText), cmdParams);
		}

		private IEnumerable<string> SplitSqlStatements(string sql, string delimiter) {
			if (string.IsNullOrWhiteSpace(sql)) return new List<string>();
			if (string.IsNullOrWhiteSpace(delimiter)) return new List<string> {sql};

			// Split by delimiter
			var statements = Regex.Split(sql, $@"^[\t ]*{delimiter}(?!\w)[\t ]*\d*[\t ]*(?:--.*)?",
				RegexOptions.Multiline | RegexOptions.IgnoreCase);

			// Remove empties, trim, and return
			return statements.Where(x => !string.IsNullOrWhiteSpace(x))
				.Select(x => x.Trim(' ', '\r', '\n'));
		}

		public void CreateSchemaVersionTable() {
			const string creationSql = "CREATE TABLE database_migrations ( " +
			                           "id int NOT NULL IDENTITY(1,1), " +
			                           "filename nvarchar(255) NOT NULL, " +
			                           "version nvarchar(200) NOT NULL, " +
			                           "execution_date DATETIME NOT NULL DEFAULT (GETDATE()), " +
			                           "CONSTRAINT database_migrations_pk PRIMARY KEY CLUSTERED (version)) ";

			ExecuteNonQuery(creationSql, null);
		}

		public void ExecuteDeploymentScript(string script) {
			var scripts = SplitSqlStatements(script, "GO");

			foreach (var splittedScript in scripts) {
				ExecuteNonQuery(splittedScript, null);
			}
		}

		public void ExecuteDatabaseMigrationInsert(string script) {
			ExecuteNonQuery(script, null);
		}

		private void AddCurrentUser(List<SqlParameter> cmdParams) {
			cmdParams ??= new List<SqlParameter>();
			var sqlP = new SqlParameter("@SystemLoggedInUser", SqlDbType.Int);
			sqlP.Value = Conv.ToInt(_currUserId);
			var f = false;
			foreach (var p in cmdParams) {
				if (p.ParameterName != sqlP.ParameterName) continue;
				f = true;
				break;
			}

			if (!f) cmdParams.Add(sqlP);
		}

		/// <summary>
		/// Executes a SQL command not intended to return any values
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		private int ExecuteNonQuery(string cmdText, List<SqlParameter> cmdParams) {
			var val = 0;

			if (Diag.LogQueries) Diag.AddSqlQuery(cmdText, cmdParams);

			SqlCommand cmd;
			SqlConnection conn = null;
			AddCurrentUser(cmdParams);

			if (_transactionOpen) {
				cmd = _transactionConnection.CreateCommand();
				PrepareCommand(ref cmd, ref _transactionConnection, _transaction, cmdText, cmdParams);
			} else {
				conn = new SqlConnection(_connectionString);
				cmd = conn.CreateCommand();
				PrepareCommand(ref cmd, ref conn, null, cmdText, cmdParams);
			}

			try {
				val = cmd.ExecuteNonQuery();
			} catch (Exception ex) {
				if (ex.Message.Contains(
					"The CREATE UNIQUE INDEX statement terminated because a duplicate key was found")) {
					var cols = ex.Message.Split("The duplicate key value is");
					throw new CustomUniqueException(cols[1].Substring(2, cols[1].Length - 4), true);
				}

				LogDbException(cmdText, cmdParams, ex);
				throw;
			} finally {
				cmd.Parameters.Clear();
			}

			if (!_transactionOpen) conn.Close();
			cmd.Dispose();
			return val;
		}

		public DataTable PrepareDataTable(string tableName) {
			var result = GetDatatable("SELECT * FROM " + tableName + " WHERE 1=0");
			result.TableName = tableName;
			return result;
		}

		public void ExecuteFromDataTable(DataTable dt) {
			if (Diag.LogQueries)
				Diag.AddSqlQuery("Executing BulkCopy for " + dt.TableName + " with " + dt.Rows.Count + " rows", null);

			SqlConnection conn;

			if (_transactionOpen) {
				conn = _transactionConnection;
			} else {
				conn = new SqlConnection(_connectionString);
				conn.Open();
			}

			using (var bc = new SqlBulkCopy(conn, SqlBulkCopyOptions.FireTriggers, null)) {
				var addedRows = new List<DataRow>();
				var deletedRows = new List<DataRow>();

				foreach (DataRow dr in dt.Rows) {
					switch (dr.RowState) {
						case DataRowState.Added:
							addedRows.Add(dr);
							break;
						case DataRowState.Deleted:
							deletedRows.Add(dr);
							break;
					}
				}

				using (var a = new SqlDataAdapter()) {
					if (dt.PrimaryKey.Length > 0) {
						var delWhere = "";
						foreach (var pCol in dt.PrimaryKey) {
							if (delWhere.Length == 0) {
								delWhere += " WHERE " + pCol.ColumnName + " = @" + pCol.ColumnName;
							} else {
								delWhere += " AND " + pCol.ColumnName + " = @" + pCol.ColumnName;
							}
						}

						var command = new SqlCommand(addContextInfo("DELETE FROM " + dt.TableName + " " + delWhere),
							conn);
						foreach (var pCol in dt.PrimaryKey) {
							var p1 = command.Parameters.Add(
								"@" + pCol.ColumnName, SqlDbType.Int, 4);
							p1.SourceColumn = pCol.ColumnName;
							p1.SourceVersion = DataRowVersion.Original;
						}

						a.DeleteCommand = command;
						a.Update(deletedRows.ToArray());
					}
				}

				SqlCommand cmd = conn.CreateCommand();
				PrepareCommand(ref cmd, ref conn, null, "DECLARE @BinVar varbinary(128); SET @BinVar = CONVERT(varbinary(128), " + _insertUserId + "); SET CONTEXT_INFO @BinVar;", new List<SqlParameter>());
				cmd.ExecuteNonQuery();

				bc.DestinationTableName = dt.TableName;
				bc.WriteToServer(addedRows.ToArray());

				PrepareCommand(ref cmd, ref conn, null, "DECLARE @BinVar varbinary(128); SET @BinVar = CONVERT(varbinary(128), 0); SET CONTEXT_INFO @BinVar;", new List<SqlParameter>());
				cmd.ExecuteNonQuery();
			}

			if (!_transactionOpen) conn.Close();
		}

		public void BeginTransaction() {
			if (_transactionOpen) return;
			_transactionConnection = new SqlConnection(_connectionString);
			_transactionConnection.Open();
			_transaction = _transactionConnection.BeginTransaction();
			_transactionOpen = true;
		}

		public void CommitTransaction() {
			if (!_transactionOpen) return;
			_transaction.Commit();
			_transactionConnection.Close();
			_transactionOpen = false;
		}

		public void RollbackTransaction() {
			if (!_transactionOpen) return;
			_transaction.Rollback();
			_transactionConnection.Close();
			_transactionOpen = true;
		}

		public SqlConnection GetConnection() {
			var conn = new SqlConnection(_connectionString);
			return conn;
		}

		/// <summary>
		/// Get's paged data from the database into DataTable object. Total number of rows in the table can be fetched with lastRowTotal method.
		/// </summary>
		/// <param name="cmdText">SQL Query without Order By clause</param>
		/// <param name="orderBy">Order By clause. Include ASC / DESC</param>
		/// <param name="primaryOrderColumn">Helper column to enable row count calculation. Preferably primary key id column name. Must be different to OrderBy clause column names</param>
		/// <param name="startFromRow">Row number to start from</param>
		/// <param name="returnRowCount">Number of rows to return</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>DataTable</returns>
		public DataTable GetDatatable(string cmdText, string orderBy, string primaryOrderColumn, int startFromRow,
			int returnRowCount, List<SqlParameter> cmdParams = null, string sumCols = "", bool fetchKeyInfo = false,
			bool inThread = false, string optionalCountWrapSql = "") {
			var cmd = cmdText;
			cmdText = returnRowCount != 0 ? WrapPagingSql(cmdText, orderBy) : WrapOrderBy(cmdText, orderBy);

			var s = new SqlParameter();
			var e = new SqlParameter();

			cmdParams ??= new List<SqlParameter>();
			if (startFromRow != 0 || returnRowCount != 0) {
				s.ParameterName = "internal_limit_start";
				s.SqlDbType = SqlDbType.Int;
				s.Value = startFromRow;

				e.ParameterName = "internal_limit_end";
				e.SqlDbType = SqlDbType.Int;
				e.Value = startFromRow + returnRowCount;
				cmdParams.Add(s);
				cmdParams.Add(e);
			}

			if (!inThread)
				AddCurrentUser(cmdParams);
			
			var t = GetDatatable(cmdText, cmdParams, fetchKeyInfo, inThread);
			lastRowTotal = 0;

			if (returnRowCount > 0 && t.Rows.Count >= returnRowCount || sumCols != "") {
				var countParam = new SqlParameter {
					ParameterName = "internal_count",
					SqlDbType = SqlDbType.Int,
					Value = 0,
					Direction = ParameterDirection.Output
				};
				cmdParams.Add(countParam);
				
				ExecuteNonQuery(WrapCountSql(optionalCountWrapSql != "" ? optionalCountWrapSql : cmd, sumCols), cmdParams);
				lastRowTotal = Conv.ToInt(countParam.Value);
			} else {
				lastRowTotal = t.Rows.Count + startFromRow;
				if (startFromRow != 0) lastRowTotal -= 1;
			}

			return t;
		}


		public DataTable GetDatatable(SelectQuery query, List<SqlParameter> cmdParams = null,
			bool fetchKeyInfo = false, bool inThread = false) {
			return GetDatatable(query.Get(), cmdParams, fetchKeyInfo, inThread);
		}


		/// <summary>
		/// Get's non paged data from the database into DataTable object.
		/// </summary>
		/// <param name="cmdText">SQL Query</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>DataTable</returns>
		public DataTable GetDatatable(string cmdText, List<SqlParameter> cmdParams = null, bool fetchKeyInfo = false,
			bool inThread = false) {
			var result = new DataTable();

			if (Diag.LogQueries) Diag.AddSqlQuery(cmdText, cmdParams);

			try {
				using var conn = new SqlConnection(_connectionString);
				var cmd = conn.CreateCommand();
				conn.Open();
				cmd.Connection = conn;
				cmd.CommandText = cmdText;
				cmd.CommandTimeout = timeout;

				//attach the command parameters if they are provided
				if (cmdParams != null) {
					foreach (var p in cmdParams) {
						//check for derived output value with no value assigned
						if (p.Direction == ParameterDirection.InputOutput && p.Value == null) {
							p.Value = DBNull.Value;
						}
						cmd.Parameters.Add(p);
					}
				}

				if (!inThread) {
					if (!cmd.Parameters.Contains("@SystemLoggedInUser"))
						cmd.Parameters.AddWithValue("@SystemLoggedInUser", _currUserId);
				}

				using var da = new SqlDataAdapter(cmd);
				if (fetchKeyInfo) da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
				da.Fill(result);
				da.Dispose();
				cmd.Parameters.Clear();
			} catch (Exception ex) {
				LogDbException(cmdText, cmdParams, ex);
				throw;
			}

			lastRowTotal = 0;

			return result;
		}

		/// <summary>
		/// Get's paged data from the database into json string. Total number of rows in the table can be fetched with lastRowTotal method.
		/// </summary>
		/// <param name="cmdText">SQL Query without Order By clause</param>
		/// <param name="orderBy">Order By clause. Include ASC / DESC</param>
		/// <param name="primaryOrderColumn">Helper column to enable row count calculation. Preferably primary key id column name. Must be different to OrderBy clause column names</param>
		/// <param name="startFromRow">Row number to start from</param>
		/// <param name="returnRowCount">Number of rows to return</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>Json String serialised from DataTable object</returns>
		public string GetDatatableJson(string cmdText, string orderBy, string primaryOrderColumn, int startFromRow,
			int returnRowCount, List<SqlParameter> cmdParams = null) {
			return JsonConvert.SerializeObject(GetDatatable(cmdText, orderBy, primaryOrderColumn, startFromRow,
				returnRowCount,
				cmdParams));
		}

		/// <summary>
		/// Get's paged data from the database into json string.
		/// </summary>
		/// <param name="cmdText">SQL Query</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>Json String serialised from DataTable object</returns>
		public string GetDatatableJson(string cmdText, List<SqlParameter> cmdParams = null) {
			return JsonConvert.SerializeObject(GetDatatable(cmdText, cmdParams));
		}

		/// <summary>
		/// Serializes DataTable into json string
		/// </summary>
		/// <param name="dt"></param>
		/// <returns></returns>
		public string GetDatatableJson(DataTable dt) {
			return JsonConvert.SerializeObject(dt);
		}

		public List<Dictionary<string, object>> GetDatatableDictionary(string cmdText, string orderBy,
			string primaryOrderColumn, int startFromRow, int returnRowCount, List<SqlParameter> cmdParams = null) {
			return DataTableToDictionary(GetDatatable(cmdText, orderBy, primaryOrderColumn, startFromRow,
				returnRowCount,
				cmdParams));
		}

		/// <summary>
		/// Get's paged data from the database into json string.
		/// </summary>
		/// <param name="cmdText">SQL Query</param>
		/// <param name="cmdParams">Parameters for cmdText query</param>
		/// <returns>Json String serialised from DataTable object</returns>
		public List<Dictionary<string, object>> GetDatatableDictionary(string cmdText,
			List<SqlParameter> cmdParams = null) {
			return DataTableToDictionary(GetDatatable(cmdText, cmdParams));
		}

		/// <summary>
		/// Overridden method. You can pass SelectQuery object directly for this
		/// </summary>
		/// <param name="query">SelectQuery object</param>
		/// <param name="cmdParams">Query parameters</param>
		/// <returns></returns>
		public List<Dictionary<string, object>>
			GetDatatableDictionary(SelectQuery query, List<SqlParameter> cmdParams = null) {
			return DataTableToDictionary(GetDatatable(query.Get(), cmdParams));
		}

		/// <summary>
		/// Serializes DataTable into json string
		/// </summary>
		/// <param name="dt"></param>
		/// <returns></returns>
		public List<Dictionary<string, object>> GetDatatableDictionary(DataTable dt) {
			return DataTableToDictionary(dt);
		}

		public DataRow GetDataRow(SelectQuery query, List<SqlParameter> cmdParameters = null) {
			return GetDataRow(query.Get(), cmdParameters);
		}

		public DataRow GetDataRow(string cmdText, List<SqlParameter> cmdParams = null) {
			var data = GetDatatable(cmdText, cmdParams);
			return data.Rows.Count > 0 ? data.Rows[0] : null;
		}

		public string GetDataRowJson(string cmdText, List<SqlParameter> cmdParams = null) {
			return JsonConvert.SerializeObject(GetDataRow(cmdText, cmdParams));
		}

		public string GetDataRowJson(DataRow row) {
			return JsonConvert.SerializeObject(row);
		}

		public object ExecuteScalar(SelectQuery query, List<SqlParameter> cmdParams = null) {
			return ExecuteScalar(query.Get(), cmdParams);
		}

		/// <summary>
		/// Executes a SQL command intended to return single value
		/// </summary>
		/// <param name="cmdText"></param>
		/// <param name="cmdParams"></param>
		/// <returns></returns>
		public object ExecuteScalar(string cmdText, List<SqlParameter> cmdParams = null) {
			cmdText = addContextInfo(cmdText);

			if (Diag.LogQueries) Diag.AddSqlQuery(cmdText, cmdParams);
			object val = null;

			SqlCommand cmd;
			SqlConnection conn = null;
			AddCurrentUser(cmdParams);
			if (_transactionOpen) {
				cmd = _transactionConnection.CreateCommand();
				PrepareCommand(ref cmd, ref _transactionConnection, _transaction, cmdText, cmdParams);
			} else {
				conn = new SqlConnection(_connectionString);
				cmd = conn.CreateCommand();
				PrepareCommand(ref cmd, ref conn, null, cmdText, cmdParams);
			}

			try {
				val = cmd.ExecuteScalar();
			} catch (Exception ex) {
				LogDbException(cmdText, cmdParams, ex);
				throw;
			} finally {
				cmd.Parameters.Clear();
			}

			if (!_transactionOpen) conn.Close();

			cmd.Dispose();
			return val;
		}

		public List<Dictionary<string, object>> DataTableToDictionary(DataTable dt) {
			var retval = new List<Dictionary<string, object>>();
			foreach (DataRow row in dt.Rows) {
				//Use Linq to convert row into a dictionary
				var result = Enumerable.Range(0, dt.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => dt.Rows[0].Table.Columns[i].ColumnName,
						i => row[dt.Rows[0].Table.Columns[i]]
					);
				retval.Add(result);
			}

			return retval;
		}

		#endregion

		#region Private methods

		private void PrepareCommand(ref SqlCommand cmd, ref SqlConnection conn, SqlTransaction trans, string cmdText,
			List<SqlParameter> commandParameters) {
			if (conn.State != ConnectionState.Open) {
				conn = new SqlConnection(_connectionString);
				try {
					conn.Open();
				} catch (Exception) {
					Diag.LogToConsole("Database connections could not be opened.");
				}
			}

			cmd.Connection = conn;
			cmd.CommandText = cmdText;
			cmd.CommandTimeout = timeout;
			if (trans != null) cmd.Transaction = trans;

			//attach the command parameters if they are provided
			if (commandParameters != null) AttachParameters(ref cmd, commandParameters.ToArray());
		}

		private void AttachParameters(ref SqlCommand command, SqlParameter[] commandParameters) {
			foreach (var p in commandParameters) {
				//check for derived output value with no value assigned
				if (p.Direction == ParameterDirection.InputOutput && p.Value == null) p.Value = DBNull.Value;
				command.Parameters.Add(p);
			}
		}

		private void LogDbException(string cmdText, List<SqlParameter> cmdParams, Exception ex) {
			if (_preventLogging) return;

			Diag.LogToConsole(
				"Offending SQL Query: \n " + Diag.ParseSqlParametersIntoString(cmdText, cmdParams) + " \n -> " +
				ex.StackTrace,
				Diag.LogSeverities.Sql);

			var instance = this.instance;
			if (instance == "") {
				if (cmdParams != null) {
					foreach (var p in cmdParams) {
						//check for derived output value with no value assigned
						if (p.ParameterName == "instance") {
							instance = Conv.ToStr(p.Value);
							break;
						}
					}
				}
			}

			if (instance == "") instance = "?";
			Diag.Log(instance, ex, _currUserId, Diag.LogCategories.Database, Diag.LogTypes.ServerException);
		}

		private string WrapPagingSql(string sql, string orderBy) {
			return " WITH query AS ( SELECT ROW_NUMBER() OVER (ORDER BY " + orderBy + " ) AS ROWID, * FROM ( " + sql +
			       " ) AS result )  SELECT * FROM query WHERE ROWID BETWEEN @internal_limit_start AND @internal_limit_end ";
		}

		private string WrapOrderBy(string sql, string orderBy) {
			return sql + " ORDER BY " + orderBy;
		}

		private string WrapCountSql(string sql, string sumCols = "") {
			return " SELECT @internal_count = COUNT(*) " + sumCols + " FROM( " + sql + " ) as tmp";
		}

		#endregion
	}
}