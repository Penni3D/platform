﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.database {
	public class ConnectionBase : IConnectionBase {
		private const int AdminUserId = 0;
		private readonly DateTime? _archiveArchiveDate;

		public ConnectionBase(string instance, AuthenticatedSession currentUserId,
			DateTime? historyArchiveDate = null) {
			_archiveArchiveDate = historyArchiveDate;
			db = new Connections(currentUserId);
			this.instance = instance;
			DBParameters = new List<SqlParameter>();
			SetDBParameter("@instance", Conv.ToStr(instance) == "" ? currentUserId.instance : instance);
			SetDBParameter("@signedInUserId", currentUserId.item_id);
		}

		public string instance { get; set; }

		public IConnections db { get; set; }

		public List<SqlParameter> DBParameters { get; set; }

		public SelectQuery BuildSelectQuery() {
			return new SelectQuery(this);
		}

		public DeleteQuery BuildDeleteQuery() {
			return new DeleteQuery();
		}

		public InsertQuery BuildInsertQuery() {
			return new InsertQuery(this);
		}

		public string GetProcessTableName(string processName) {
			if (_archiveArchiveDate == null) {
				return Conv.ToSql("_" + instance + "_" + processName);
			}

			var d = (DateTime) _archiveArchiveDate;
			SetDBParameter("@archiveDate", d);
			return "get__" + Conv.ToSql(instance + "_" + processName) + "(@archiveDate)";
		}


		public SqlParameter SetDBParam(SqlParameter parameter) {
			//if parameter already exists, replace its value and type
			foreach (var p in DBParameters.Where(p => string.Equals(p.ParameterName.Replace("@", ""),
				parameter.ParameterName.Replace("@", ""),
				StringComparison.CurrentCultureIgnoreCase))) {
				p.Value = parameter.Value;
				p.SqlDbType = parameter.SqlDbType;
				p.Direction = parameter.Direction;
				return p;
			}

			DBParameters.Add(parameter);
			return parameter;
		}

		/// <summary>
		/// Sets multiple parameters. Determines SqlType from given object type
		/// </summary>
		/// <param name="items">Name and Value pairs. First name, then object.</param>
		/// <returns>List of added parameters</returns>
		/// <exception cref="InvalidOperationException"></exception>
		public List<IDbDataParameter> SetDBParameters(params object[] items) {
			// Parameter list shoud be like: 
			// ["@userId", (int)userId, "@date", (DateTime)date]
			if (items.Length == 0 || items.Length % 2 != 0) {
				throw new InvalidOperationException("Wrong number of parameters");
			}

			var parameters = new List<IDbDataParameter>();
			for (var i = 0; i < items.Length; i = i + 2) {
				var name = (string) items[i];
				var value = items[i + 1];
				if (value == null) {
					continue;
				}

				parameters.Add(setDbParameter(name, value));
			}

			return parameters;
		}

		public IDbDataParameter SetDBParameter(string name, object value) {
			if (string.IsNullOrEmpty(name) || value == null) {
				throw new ArgumentNullException();
			}

			return setDbParameter(name, value);
		}

		public SqlParameter SetDBParam(SqlDbType type, string name, object value) {
			return SetDBParam(new SqlParameter {
				ParameterName = name,
				SqlDbType = type,
				Value = value
			});
		}

		public void SetDBParamToList(SqlDbType type, string name, object value, List<SqlParameter> targetParamList) {
			targetParamList.Add(new SqlParameter {
				ParameterName = name,
				SqlDbType = type,
				Value = value
			});
		}

		public SqlParameter SetDBParam(SqlDbType type, string name, object value, ParameterDirection direction) {
			var p = new SqlParameter {
				ParameterName = name,
				SqlDbType = type,
				Value = value,
				Direction = direction
			};
			return SetDBParam(p);
		}

		public SqlParameter GetDBParam(SqlDbType type, string name, object value) {
			var p = new SqlParameter {
				ParameterName = name,
				SqlDbType = type,
				Value = value
			};
			return p;
		}

		public SqlParameter GetDBParam(SqlDbType type, string name, object value, ParameterDirection direction) {
			var p = new SqlParameter {
				ParameterName = name,
				SqlDbType = type,
				Value = value,
				Direction = direction
			};
			return p;
		}

		public string GetInstance(string hostname) {
			SetDBParam(SqlDbType.NVarChar, "host", hostname.ToLower());
			return Conv.ToStr(db.ExecuteScalar("SELECT instance FROM instance_hosts WHERE LOWER(host) = @host",
				DBParameters));
		}

		private IDbDataParameter setDbParameter(string name, object value) {
			var objectType = value.GetType();
			IDbDataParameter newParam;
			if (objectType == typeof(long)) {
				newParam = SetDBParam(SqlDbType.BigInt, name, (long) value);
			} else if (objectType == typeof(long) || objectType == typeof(int) || objectType == typeof(short) ||
			           objectType == typeof(int)) {
				newParam = SetDBParam(SqlDbType.Int, name, (int) value);
			} else if (objectType == typeof(double)) {
				newParam = SetDBParam(SqlDbType.Float, name, (double) value);
			} else if (objectType == typeof(string) || objectType == typeof(string)) {
				newParam = SetDBParam(SqlDbType.NVarChar, name, (string) value);
			} else if (objectType == typeof(DateTime)) {
				newParam = SetDBParam(SqlDbType.DateTime, name, Conv.ToDateTime(value));
			} else {
				throw new InvalidOperationException("Unable to determine the type of given object");
			}

			return newParam;
		}
	}
}