﻿using System;
using System.Collections.Generic;
using System.Data;

using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.processes.accessControl;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.database {
	public interface IEntityRepository {
		int GetParentItemId(int itemId);

		int GetParentItemId(int itemId, string process);

		List<int> GetItemIds();

		List<int> GetItemIds(string order);

		int GetItemIdByColumn(List<DataVal> dataVals);


		List<Dictionary<string, object>> GetProcessData(string ordercol);

		int GetCountOfIdenticalItems(ItemColumn i, int itemId, string value);

		List<Dictionary<string, object>> FindItemForParentItemId(int parentItemId, string orderCol);

		List<Dictionary<string, object>> GetSubItems(int parentItemId, int processTabSubProcessId);

		DataTable GetItemsToFill(int portfolioId = 0, string additional_where = "");

		List<Dictionary<string, object>> GetArchiveReport(int itemId, int itemColumnId);

		List<Dictionary<string, object>> GetArchiveReport(int itemId, string itemColumnName);

		string GetWhere(int portfolioId);
	}

	public class EntityRepository : SessionBaseInfo, IEntityRepository {
		public IItemColumns ItemColumns;
		public ProcessBase ProcessBase;

		public EntityRepository(string instance, string process, AuthenticatedSession session) : base(instance, process,
			session) {
			ProcessBase = new ProcessBase(instance, process, session);
			ItemColumns = new ItemColumns(instance, process, session);
		}

		public int GetParentItemId(int itemId) {
			ProcessBase.SetDBParameter("@itemId", itemId);
			return (int) ProcessBase.db.ExecuteScalar(
				"SELECT parent_item_id FROM item_subitems WHERE item_id = @itemId", ProcessBase.DBParameters);
		}

		public int GetParentItemId(int itemId, string process) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.NVarChar, "@parentProcess", process);
			return (int) ProcessBase.db.ExecuteScalar(" WITH MyCTE " +
			                                          " AS(SELECT parent_item_id, s.item_id, i.process FROM item_subitems s INNER JOIN items i ON s.parent_item_id = i.item_id " +
			                                          " WHERE s.item_id = @itemId " +
			                                          " UNION ALL " +
			                                          " SELECT s.parent_item_id, s.item_id, i.process " +
			                                          " FROM item_subitems s " +
			                                          " INNER JOIN MyCTE ON s.item_id = MyCTE.parent_item_id " +
			                                          " INNER JOIN items i ON s.parent_item_id = i.item_id ) " +
			                                          " SELECT CASE (SELECT COUNT(parent_item_id) FROM MyCTE WHERE process = @parentProcess) WHEN 0 THEN 0 ELSE (SELECT TOP 1 COALESCE(parent_item_id, 0) FROM MyCTE WHERE process = @parentProcess) END ",
				ProcessBase.DBParameters);
		}

		public List<int> GetItemIds() {
			var retval = new List<int>();
			foreach (DataRow row in ProcessBase.db
				.GetDatatable("SELECT item_id FROM " + ProcessBase.tableName, ProcessBase.DBParameters).Rows) {
				retval.Add((int) row["item_id"]);
			}

			return retval;
		}

		public List<int> GetItemIds(string order) {
			var retval = new List<int>();
			foreach (DataRow row in ProcessBase.db
				.GetDatatable(
					"SELECT item_id FROM " + ProcessBase.tableName + " ORDER BY " +
					ProcessBase.ValidateOrderColumn(order) + " ASC", ProcessBase.DBParameters).Rows) {
				retval.Add((int) row["item_id"]);
			}

			return retval;
		}

		public int GetItemIdByColumn(List<DataVal> dataVals) {
			var itemColumns = ItemColumns;
			var first = true;
			var sqlWhere = "";
			foreach (var d in dataVals) {
				if (first) {
					first = false;
					sqlWhere += d.ItemColumnName + " = @" + d.ItemColumnName;
				} else {
					sqlWhere += " AND " + d.ItemColumnName + " = @" + d.ItemColumnName;
				}

				var itemCol = itemColumns.GetItemColumnByName(d.ItemColumnName);
				switch (itemCol.DataType) {
					case 0:
						ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + d.ItemColumnName, d.Value);
						break;
					case 1:
						ProcessBase.SetDBParam(SqlDbType.Int, "@" + d.ItemColumnName, d.Value);
						break;
					case 2:
						ProcessBase.SetDBParam(SqlDbType.Float, "@" + d.ItemColumnName, d.Value);
						break;
					case 3:
						ProcessBase.SetDBParam(SqlDbType.DateTime, "@" + d.ItemColumnName, d.Value);
						break;
					case 4:
						ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + d.ItemColumnName, d.Value);
						break;
					case 5:
						ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + d.ItemColumnName, d.Value);
						break;
					case 6:
						ProcessBase.SetDBParam(SqlDbType.Int, "@" + d.ItemColumnName, d.Value);
						break;
				}
			}

			var dr = ProcessBase.db.GetDataRow("SELECT item_id FROM " + ProcessBase.tableName + " WHERE " + sqlWhere,
				ProcessBase.DBParameters);
			if (dr != null) {
				return (int) dr["item_id"];
			}

			return 0;
		}


		public List<Dictionary<string, object>> GetProcessData(string ordercol) {
			var collate = "";
			if (ordercol == "") ordercol = "order_no";
			if (ordercol == "order_no") collate = "  ASC";
			return ProcessBase.db.GetDatatableDictionary(
				ItemColumns.GetItemSql() + " ORDER BY " + ProcessBase.ValidateOrderColumn(ordercol) + collate, ProcessBase.DBParameters);
		}

		public int GetCountOfIdenticalItems(ItemColumn i, int itemId, string value) {
			ProcessBase.SetDBParam(SqlDbType.NVarChar, "@value", value);
			var itemIdwhere = "";
			if (itemId > 0) {
				itemIdwhere = " AND item_id <> @itemId";
				ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			}

			var sql = "SELECT COUNT(*) FROM " + ProcessBase.tableName + " WHERE " + i.Name + " = @value" + itemIdwhere;
			var count = (int) ProcessBase.db.ExecuteScalar(sql, ProcessBase.DBParameters);
			return count;
		}


		private Dictionary<string, DataTable> _subItemConnectionCache = new Dictionary<string, DataTable>();

		private DataTable SubItemConnectionCache(string instance, string process) {
			var id = instance + "_" + process;
			DataTable d;

			if (!_subItemConnectionCache.ContainsKey(id)) {
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@instance", instance);
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@process", process);
				d = ProcessBase.db.GetDatatable("SELECT i.item_id, si.parent_item_id, si.favourite FROM items i " +
				                                "INNER JOIN item_subitems si ON i.item_id = si.item_id " +
				                                "WHERE instance = @instance AND process = @process",
					ProcessBase.DBParameters);
				_subItemConnectionCache.Add(id, d);
			} else {
				d = _subItemConnectionCache[id];
			}

			return d;
		}

		private Dictionary<string, DataTable> _subItemDataCache = new Dictionary<string, DataTable>();

		private DataTable SubItemDataCache(string instance, string process, string orderCol) {
			var id = instance + "_" + process + "_" + orderCol;
			DataTable d;
			if (!_subItemDataCache.ContainsKey(id)) {
				var sql = "SELECT ROW_NUMBER() OVER (ORDER BY " +
				          ProcessBase.ValidateOrderColumn(orderCol) + " ASC) AS roworder, * FROM (" +
				          ItemColumns.GetItemSql() + ") pf ORDER BY roworder ASC";

				d = ProcessBase.db.GetDatatable(sql);
				_subItemDataCache.Add(id, d);
			} else {
				d = _subItemDataCache[id];
			}

			return d;
		}

		public List<Dictionary<string, object>> FindItemForParentItemId(int parentItemId, string orderCol) {
			if (orderCol == "") orderCol = "order_no";

			var connections = SubItemConnectionCache(Instance, Process);
			var ids = new List<int>();
			foreach (DataRow id in connections.Select("parent_item_id = " + parentItemId + " AND (favourite = 0 OR (favourite = 1 AND parent_item_id = " + AuthenticatedSession.item_id + "))")) {
				ids.Add(Conv.ToInt(id["item_id"]));
			}

			if (ids.Count == 0) return new List<Dictionary<string, object>>();
			var pp = new ProcessPortfolios(Instance, Process, AuthenticatedSession);
			var portfolioIdId = pp.GetDefaultSelectorId();
			var ics = new ItemCollectionService(Instance, Process, AuthenticatedSession);
			var data = ics.GetItems("item_id IN (" + Conv.ToDelimitedString(ids) + ")", portfolioIdId);
			data.Sort((x, y) => Conv.ToStr(x[orderCol]).CompareTo(Conv.ToStr(y[orderCol])));
			return data;
		}

		public List<Dictionary<string, object>> GetSubItems(int parentItemId, int processTabSubProcessId) {
			var param = new List<SqlParameter>();
			param.Add(ProcessBase.GetDBParam(SqlDbType.Int, "@parentItemId", parentItemId));
			param.Add(ProcessBase.GetDBParam(SqlDbType.Int, "@processTabSubProcessId", processTabSubProcessId));

			var icsSql = ItemColumns;
			return ProcessBase.db.GetDatatableDictionary(
				icsSql.GetItemSql() +
				" INNER JOIN process_tabs_subprocess_data sd ON pf.item_id = sd.item_id WHERE parent_process_item_id = @parentItemId AND process_tab_subprocess_id = @processTabSubProcessId",
				param);
		}

		public DataTable GetItemsToFill(int portfolioId = 0, string additional_where = "") {
			var portfolioSqlService = new PortfolioSqlService(Instance, AuthenticatedSession, ItemColumns,
				ProcessBase.tableName, ProcessBase.process);
			var pSqls = portfolioSqlService.GetSql(0, "");

			var eSql1 = "";
			var esql2 = "";
			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name;
				}

				eSql1 = " SELECT  * " + equationSql + " FROM ( ";
				esql2 = " ) pf ";
			}

			var sql = eSql1 + " SELECT " + string.Join(",", pSqls.sqlcols.ToArray());
			sql += " FROM " + ProcessBase.tableName + " pf ";

			sql += string.Join(" ", pSqls.archiveJoins.ToArray()) + " " +
			       string.Join(" ", pSqls.joins.ToArray()) + esql2;

			if (portfolioId > 0) {
				sql += "WHERE 1=1 AND " + GetWhere(portfolioId);
			}

			if (additional_where.Length > 0) sql += " AND " + additional_where;

			var pData = ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);
			return pData;
		}

		public List<Dictionary<string, object>> GetArchiveReport(int itemId, int itemColumnId) {
			var itemColumns = ItemColumns;
			var itemColumn = itemColumns.GetItemColumn(itemColumnId);

			var sql = "select min(archive_id) as archive_id, min([archive_end]) as archive_start"
			          + " , COALESCE((select TOP 1 archive_userid from archive_" + ProcessBase.tableName +
			          " where archive_id > min(a.archive_id) ORDER BY archive_id ) " +
			          ", (SELECT archive_userid FROM " + ProcessBase.tableName + " WHERE item_id = @itemId) " +
			          ", 0) as archive_userid  " +
			          " FROM archive_" + ProcessBase.tableName + " a WHERE item_id = @itemId AND " + itemColumn.Name +
			          " IS NOT NULL GROUP BY " + itemColumn.Name;

			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			return ProcessBase.db.GetDatatableDictionary(sql, ProcessBase.DBParameters);
		}

		public List<Dictionary<string, object>> GetArchiveReport(int itemId, string itemColumnName) {
			var sql = "select min(archive_id) as archive_id, min([archive_start]) as archive_start FROM archive_" +
			          ProcessBase.tableName + " WHERE item_id = @itemId AND " + itemColumnName +
			          " IS NOT NULL GROUP BY " +
			          itemColumnName;

			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			return ProcessBase.db.GetDatatableDictionary(sql, ProcessBase.DBParameters);
		}

		public string GetWhere(int portfolioId) {
			var mStates = GetStates();
			var readWhere = mStates.GetExpForPortfolio(Process, portfolioId);
			readWhere = readWhere.Replace("true  == true", "1=1").Replace("== true", "").Replace(" == ", "=")
				.Replace("true", "1=1").Replace("false", "0=1");

			if (readWhere.Length == 0) {
				readWhere = "0=1";
			}

			return " ( " + readWhere + ")  ";
		}

		private States GetStates() {
			return new States(Instance, AuthenticatedSession);
		}
	}
}