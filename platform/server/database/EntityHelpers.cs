﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;

namespace Keto5.x.platform.server.database {
	public class EntityHelpers {
		public static string ConvertSearchDictionaryToSql(Dictionary<string, object> search) {
			var searchSql = "";
			if (search == null) {
				return searchSql;
			}

			foreach (var s in search.Keys) {
				if (searchSql.Length == 0) {
					searchSql += s + "= N'" + Conv.Escape(Conv.ToStr(search[s])) + "'";
				} else {
					searchSql += " AND " + s + "= N'" + Conv.Escape(Conv.ToStr(search[s])) + "'";
				}
			}
			return searchSql;
		}
	}
}