﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.database {
	public interface IFavouriteService {
		void AddFavourite(int itemId);
		List<Dictionary<string, object>> GetFavourites();
		void DeleteFavourite(int itemId);
	}

	public class FavouriteService : SessionBaseInfo, IFavouriteService {
		public IDatabaseItemService DatabaseItemService;
		public SubItems SubItems;

		public FavouriteService(string instance, string process, AuthenticatedSession session) : base(instance, process,
			session) {
			SubItems = new SubItems(Instance, Process, AuthenticatedSession);
			DatabaseItemService = new DatabaseItemService(Instance, Process, AuthenticatedSession);
		}

		public void AddFavourite(int itemId) {
			SubItems.AddSubItem(AuthenticatedSession.item_id, itemId, 1);
		}

		public List<Dictionary<string, object>> GetFavourites() {
			var result = new List<Dictionary<string, object>>();

			//Get process' selector id
			var pid = new Processes(Instance, AuthenticatedSession).GetProcessSelectorPortfolioId(Process);
			if (pid == 0) return result;
			
			//Get Favourite Item Ids
			var pp = new ItemBase(Instance, Process, AuthenticatedSession);
			var q = new PortfolioOptions();
			foreach (var sub in SubItems.GetSubItems(AuthenticatedSession.item_id, 1)) {
				q.itemsRestriction.Add(sub.ItemId);
			}

			//Get Process data for each item -- if any found
			if (q.itemsRestriction.Count > 0) {
				var data = (DataTable) pp.FindItems(pid, q)["rowdata"];			
				foreach (DataRow r in data.Rows) {
					result.Add(DatabaseItemService.GetItem(Conv.ToInt(r["item_id"]), data, false));
				}
			}

			return result;
		}

		public void DeleteFavourite(int itemId) {
			SubItems.DeleteSubItem(AuthenticatedSession.item_id, itemId, 1);
		}
	}
}