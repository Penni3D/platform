﻿using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.database{
	public interface IConnectionBase{
		string instance{ get; set; }
		IConnections db{ get; set; }
		List<SqlParameter> DBParameters{ get; set; }
		SqlParameter SetDBParam(SqlParameter parameter);
		List<IDbDataParameter> SetDBParameters(params object[] items);
		IDbDataParameter SetDBParameter(string name, object value);
		SqlParameter SetDBParam(SqlDbType type, string name, object value);
		SqlParameter SetDBParam(SqlDbType type, string name, object value, ParameterDirection direction);
		SqlParameter GetDBParam(SqlDbType type, string name, object value);
		SqlParameter GetDBParam(SqlDbType type, string name, object value, ParameterDirection direction);
		string GetInstance(string hostname);
		SelectQuery BuildSelectQuery();
		DeleteQuery BuildDeleteQuery();
		InsertQuery BuildInsertQuery();
		string GetProcessTableName(string processName);
	}
}