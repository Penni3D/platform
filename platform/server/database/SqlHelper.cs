﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.database{

	public enum SqlComp{
		EQUALS,
		GREATER_THAN,
		LESS_THAN,
		GREATER_OR_EQUAL,
		LESS_THAN_OR_EQUAL,
		IN,
		NOT_EQUALS
	}

	public enum SqlOperator{
		AND,
		OR
	}
	
	public class ConditionGroup{
		private readonly List<SqlCondition> _conditions = new List<SqlCondition>();
		private string _operation = "AND";

		public ConditionGroup Operation(SqlOperator operation){
			if (operation == SqlOperator.AND){
				_operation = "AND";
			}
			if (operation == SqlOperator.OR){
				_operation = "OR";
			}
			return this;
		}

		public ConditionGroup Add(SqlCondition cond){
			_conditions.Add(cond);
			return this;
		}

		public SqlCondition And(){
			var conds = (from _condition in _conditions
				select _condition.Get()).ToArray();
			return new SqlCondition().RawSql(SqlHelper.JoinWithAnd(conds));
		}

		public SqlCondition Or(){
			var conds = (from _condition in _conditions
				select _condition.Get()).ToArray();
			return new SqlCondition().RawSql(SqlHelper.JoinWithOr(conds));
		}

		public SqlCondition Get(){
			if (_operation.Equals("AND")){
				return And();
			}
			if (_operation.Equals("OR")){
				return Or();
			}
			return null;
		}
	}
	

	public class SqlCondition{
		private string _condition;
		private string _left;
		private string _not;
		private string _operation;
		private string _right;

		public SqlCondition(){
		}

		public SqlCondition(string left){
			_left = left;
		}

		public SqlCondition(string left, SqlComp op, string right){
			_left = left;
			_operation = _getOperation(op);
			_right = right;
		}

		public SqlCondition(string left, SqlComp op, IDbDataParameter right){
			_left = left;
			_operation = _getOperation(op);
			_right = right.ParameterName;
		}

		public SqlCondition(ConditionGroup conditionGroup){
			_condition = conditionGroup.Get().Get();
		}

		private string _getOperation(SqlComp op){
			var operation = "";
			switch (op){
				case SqlComp.EQUALS:
					operation = "=";
					break;
				case SqlComp.GREATER_OR_EQUAL:
					operation =  ">=";
					break;
				case SqlComp.LESS_THAN_OR_EQUAL:
					operation =  "<=";
					break;
				case SqlComp.GREATER_THAN:
					operation =  ">";
					break;
				case SqlComp.LESS_THAN:
					operation =  "<";
					break;
				case SqlComp.IN:
					operation =  " IN ";
					break;
				case SqlComp.NOT_EQUALS :
					operation = "=";
					_not = " not ";
					break;
			}
			if (string.IsNullOrEmpty(operation)){
				throw new ArgumentOutOfRangeException();
			}

			return " " + operation + " ";
		}

		public SqlCondition RawSql(string clause){
			_condition = clause;
			return this;
		}

		public SqlCondition Column(string columnName){
			_left = columnName;
			return this;
		}

		public SqlCondition And(SqlCondition rightSide){
			var valueOfRightSide = rightSide.Get();
			if (string.IsNullOrEmpty(valueOfRightSide)){
				return this;
			}
			return new SqlCondition().RawSql("(" + Get() + " AND " + rightSide.Get() + ")");
		}

		public SqlCondition And(string left, SqlComp op, IDbDataParameter right) {
			var cond = new SqlCondition(left, op, right);
			return And(cond);
		}

		public SqlCondition Or(SqlCondition rightSide){
			return new SqlCondition().RawSql("(" + Get() + " OR " + rightSide.Get() + ")");
		}

		public SqlCondition In(string itemsInString){
			if (string.IsNullOrEmpty(itemsInString)){
				return this;
			}
			_operation = "IN";
			_right = "(" + itemsInString + ") ";
			return this;
		}

		public SqlCondition In<T>(List<T> items) {
			if (items == null || items.Count == 0){
				return this;
			}
			var right = string.Join(",", items);
			return In(right);
		}

		public SqlCondition In(SelectQuery subQuery){
			_operation = "IN";
			_right = "(" + subQuery.Get(false) + ") ";
			return this;
		}

		public SqlCondition Between(IDbDataParameter from, IDbDataParameter to){
			_operation = "BETWEEN";
			_right = from.ParameterName + " AND " + to.ParameterName;
			return this;
		}

		public string Get(){
			if (!string.IsNullOrEmpty(_condition)){
				return _condition;
			}

			if (string.IsNullOrEmpty(_left) || string.IsNullOrEmpty(_operation) || string.IsNullOrEmpty(_right)){
				return null;
			}
			
			return _not + _left + " " + _operation + " " + _right;
		
		}
	}

	public class Join{
		private readonly string _joinedTable;
		private string _condition;
		private string JoinType = "JOIN";

		public Join(string table){
			_joinedTable = table;
		}

		public Join(string table, string alias){
			_joinedTable = table + " " + alias;
		}

		public Join Type(string type){
			JoinType = type;
			return this;
		}

		public Join On(string left, string right){
			_condition = left + "=" + right;
			return this;
		}

		public Join On(SqlCondition condition){
			_condition = condition.Get();
			return this;
		}

		public string Get(){
			return JoinType + " " + _joinedTable + " ON " + _condition + " ";
		}
	}

	public class DeleteQuery{
		private string _conditions;
		private string _fromTable;

		public DeleteQuery From(string table){
			_fromTable = table;
			return this;
		}

		public DeleteQuery Where(string conditions){
			_conditions = conditions;
			return this;
		}

		public DeleteQuery Where(SqlCondition condition){
			_conditions = condition.Get();
			return this;
		}

		public string Get(){
			var sql = "DELETE "+
			          "FROM " + _fromTable + " " +
			          "WHERE " + _conditions + ";";
			return sql;
		}

		public override string ToString(){
			return Get();
		}
	}
	
	public class InsertQuery{
		private readonly IConnectionBase _connectionBase;
		private string _toTable;
		private Dictionary<string, object> _values = new Dictionary<string, object>();

		public InsertQuery(){
		}

		public InsertQuery(IConnectionBase connectionBase){
			_connectionBase = connectionBase;
		}

		public InsertQuery To(string table){
			_toTable = table;
			return this;
		}

		public InsertQuery Values(Dictionary<string, object> values){
			_values = values;
			return this;
		}

		public InsertQuery Value(string fieldName, object value){
			_values.Add(fieldName, value);
			return this;
		}

		public InsertQuery Value(string fieldName, SqlParameter parameter){
			_values.Add(fieldName, parameter.ParameterName);
			return this;
		}


		public string Get(bool semicolon = true){
			if (string.IsNullOrEmpty(_toTable) || _values == null) {
				throw new CustomArgumentException("Table name or values are missing from insert query");
			}
			var fieldNames = new List<string>();
			var values = new List<object>();
			
			foreach (var key in _values.Keys){
				fieldNames.Add(key);
				values.Add(_values[key]);
			}

			var semicolonChar = semicolon ? ";" : ""; 
			var sql = "INSERT INTO "+ _toTable + " " +
			          "(" + string.Join(",", fieldNames) + ") " +
			          "VALUES (" +string.Join(",", values) + ")" + semicolonChar;
			return sql;
		}

		public int Execute(){
			if (_connectionBase != null){
				return _connectionBase.db.ExecuteInsertQuery(this, _connectionBase.DBParameters);
			}
			throw new Exception();
		}

		public override string ToString(){
			return Get();
		}
	}
	
	

	public class SelectQuery{
		private readonly IConnectionBase _connectionBase;

		private readonly List<string> _joins = new List<string>();
		private string[] _columns = {"*"};
		private string _conditions;
		private string _distinct;
		private SelectQuery _fromQuery;
		private string _fromQueryAlias;
		private string _fromTable;
		private string[] _groupByColumns = { };
		private string[] _orderByColumns = { };

		public SelectQuery(){
		}

		public SelectQuery(IConnectionBase connectionBase){
			_connectionBase = connectionBase;
		}


		public SelectQuery Columns(params string[] columns){
			_columns = columns;
			return this;
		}

		public SelectQuery From(string table){
			_fromTable = table;
			return this;
		}

		public SelectQuery Distinct(){
			_distinct = " distinct ";
			return this;
		}

		public SelectQuery From(string table, string alias){
			_fromTable = table + " " + alias;
			return this;
		}

		public SelectQuery From(SelectQuery query, string alias){
			_fromQuery = query;
			_fromQueryAlias = alias;
			return this;
		}

		public SelectQuery JoinWith(string table, string onClauseLeft, string onClauseRight){
			_joins.Add("JOIN " + table + " ON " + onClauseLeft + "=" + onClauseRight + " ");
			return this;
		}


		public SelectQuery Join(Join join){
			_joins.Add(join.Get());
			return this;
		}

		public SelectQuery Where(string conditions){
			_conditions = conditions;
			return this;
		}

		public SelectQuery Where(string left, SqlComp op, string right){
			var cond = new SqlCondition(left, op, right);
			Where(cond);
			return this;
		}

		public SelectQuery Where(string left, SqlComp op, IDbDataParameter right){
			var cond = new SqlCondition(left, op, right.ParameterName);
			Where(cond);
			return this;
		}

		public SelectQuery Where(SqlCondition condition){
			_conditions = condition.Get();
			return this;
		}
		
		public SelectQuery Where(ConditionGroup conditions){
			_conditions = conditions.Get().Get();
			return this;
		}

		public SelectQuery GroupBy(params string[] columns){
			_groupByColumns = columns;
			return this;
		}

		public SelectQuery OrderBy(params string[] columns){
			_orderByColumns = columns;
			return this;
		}

		public string GetWhereClause() {
			return _conditions;
		}

		public string Get(bool? semicolonAtEnd = true){
			var columns = string.Join(",", _columns);
			var joins = string.Join(" ", _joins);
			var whereClause = "";
			if (!string.IsNullOrEmpty(_conditions)){
				whereClause = "WHERE " + _conditions;
			}

			var groupByColumns = string.Join(",", _groupByColumns);
			var groupByClause = "";
			if (!string.IsNullOrEmpty(groupByColumns)){
				groupByClause = " GROUP BY " + groupByColumns + " ";
			}

			var orderByColumns = string.Join(",", _orderByColumns);
			var orderByClause = "";
			if (!string.IsNullOrEmpty(orderByColumns)){
				orderByClause = " ORDER BY " + orderByColumns + " ";
			}

			var from = _fromTable;
			if (_fromQuery != null){
				from = "(" + _fromQuery.Get(false) + ") " + _fromQueryAlias + " ";
			}
			
			var sql = "SELECT " + _distinct + columns + " " +
			          "FROM " + from + " " +
			          joins +
			          whereClause +
			          groupByClause +
			          orderByClause
				;
			if (semicolonAtEnd == true){
				sql += ";";
			}
			return sql;
		}

		public string Union(SelectQuery with){
			return Get(false) + " UNION " + with.Get(false);
		}

		public override string ToString(){
			return Get();
		}

		public List<Dictionary<string, object>> Fetch(){
			if (_connectionBase != null){
				return _connectionBase.db.GetDatatableDictionary(this, _connectionBase.DBParameters);
			}
			throw new Exception();
		}

		public DataTable FetchDataTable(){
			if (_connectionBase != null){
				return _connectionBase.db.GetDatatable(this, _connectionBase.DBParameters);
			}
			throw new Exception();
		}
	}
	
	
	/* HELPERS TO AVOID TYPOS IN SQL */
	public static class SqlHelper{
		public static string Select(bool distinct, params string[] columns) {
			var distinctPhrase = "";
			if (distinct) {
				distinctPhrase = "DISTINCT ";
			}
			return "SELECT " + distinctPhrase + string.Join(", ", columns) + " ";
		}

		public static string From(string fromWhere) {
			return "FROM " + fromWhere + " ";
		}

		public static string JoinWithOperatorAndWrapWithParenthesis(string divider, params string[] items) {
			return "(" + string.Join(" " + divider + " ", items) + ") ";
		}

		public static string JoinWithAnd(params string[] items) {
			return JoinWithOperatorAndWrapWithParenthesis("AND", items);
		}

		public static string JoinWithOr(params string[] items) {
			return JoinWithOperatorAndWrapWithParenthesis("OR", items);
		}

		public static string WrapWithFunction(string function, params string[] parameters) {
			return function + "(" + string.Join(", ", parameters) + ") ";
		}

		public static string ItemsShouldBeEqual(string left, string right) {
			return left + "=" + right + " ";
		}

		public static string In(string left, string right) {
			return left + " IN" + right + " ";
		}

		public static string In(string left, List<object> items) {
			var right = string.Join(",", items);
			return left + " IN (" + right + ") ";
		}

		public static string JoinWithSpace(params string[] items) {
			return string.Join(" ", items);
		}

		public static string Where(string whereClauses) {
			return "WHERE " + whereClauses + " ";
		}
	}
}