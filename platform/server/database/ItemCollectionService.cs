﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.processes.accessControl;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.database {
	public interface IItemCollectionService {
		void FillItems(bool getTranslations = false, bool useDefaultSelectorWithListProcess = true, int portfolioId = 0, string additionalWhere = "");
		void FillItems(string ordercol);
		void FillSubItems(int parentItemId, string orderCol);
		void FillSubItems(int parentItemId, int processTabSubProcessId);
		void AddItem(ProcessItem item);

		List<Dictionary<string, object>> GetItems();

		List<Dictionary<string, object>> GetItems(string sqlWhere, int portfolioIdId = 0);
	}

	public class ItemCollectionService : SessionBaseInfo, IItemCollectionService {
		private ItemCollection _itemCollection;
		private readonly ItemColumns _itemColumns;
		private readonly ProcessBase _processBase;
		public IEntityRepository EntityRepository;

		public ItemCollectionService(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance, process, authenticatedSession) {
			_processBase = new ProcessBase(authenticatedSession, process);
			_itemColumns = new ItemColumns(instance, process, authenticatedSession);
			EntityRepository = new EntityRepository(instance, process, authenticatedSession);
		}


		public List<Dictionary<string, object>> GetItems() {
			return GetItemCollection().GetItems();
		}

		public void AddItem(ProcessItem item) {
			GetItemCollection().AddItem(item);
		}

		public void ClearMembers() { 
			GetItemCollection().ClearMembers();
		}

		public void FillItems(string ordercol) {
			var processItems = EntityRepository.GetProcessData(ordercol);
			GetItemCollection().StoreItems(processItems);
		}

		public void FillItems(bool getTranslations = false, bool useDefaultSelectorWithListProcess = true, int override_portfolio_id = 0, string additional_where = "") {
			var portfolioId = 0;
			if (_processBase.isListProcess && useDefaultSelectorWithListProcess) {
				var portfolios = new ProcessPortfolios(Instance, Process, AuthenticatedSession);
				portfolioId = portfolios.GetDefaultSelectorId();
			}

			if (override_portfolio_id > 0) portfolioId = override_portfolio_id;

			PortfolioSqls pSqls;
			if (portfolioId > 0) {
				pSqls = GetPortfolioSqls(portfolioId);
				pSqls.cols.Add("item_id");
			} else {
				pSqls = GetPortfolioSqls();
			}

			var pData = EntityRepository.GetItemsToFill(portfolioId, additional_where);

			foreach (var col in pSqls.processColumns) {
				if (col.LinkProcess != null) {
					pData.Columns.Add(col.Name, typeof(object));
				} else {
					pData.Columns.Add(col.Name, typeof(int[]));
				}
			}

			var ics = _itemColumns;
			List<ItemColumn> iCols = null;
			if (getTranslations) {
				iCols = ics.GetItemColumns();
				foreach (var col in iCols) {
					if (col.IsTranslation()) {

						pData.Columns.Remove(col.Name);
						pData.Columns.Add(col.Name, typeof(object));
					}
				}
			}

			foreach (DataRow row in pData.Rows) {
				foreach (var col in pSqls.processColumns) {
					var v = Conv.ToStr(row[col.Name + "_str"]);
					if (v.Length > 0) {
						if (col.LinkProcess != null) {
							var idps = GetItemDataProcessSelections(col.LinkProcess);

							row[col.Name] =
								idps.GetItemDataProcessSelectionIds(Conv.ToInt(row["item_id"]), col.ItemColumnId,
									col.LinkProcess);
						} else {
							row[col.Name] = Array.ConvertAll(v.Split(','), s => int.Parse(s));
						}
					}
				}

				if (getTranslations) {
					foreach (var col in iCols) {
						if (col.IsTranslation()) {
							var t = new Dictionary<string, string>();
							_processBase.SetDBParam(SqlDbType.NVarChar, "@itemId", row["item_id"]);
							_processBase.SetDBParam(SqlDbType.NVarChar, "@itemColumnId", col.ItemColumnId);
							foreach (DataRow tr in _processBase.db.GetDatatable(
								"SELECT il.code, translation FROM instance_languages il LEFT JOIN process_translations pt ON il.instance = pt.instance AND il.code = pt.code AND pt.process = @process AND pt.variable = @itemId + '_' + @itemColumnId WHERE il.instance = @instance ",
								_processBase.DBParameters).Rows) {
								t.Add(Conv.ToStr(tr["code"]), Conv.ToStr(tr["translation"]));
							}

							row[col.Name] = t;
						}
					}
				}
			}

			foreach (var col in pSqls.processColumns) {
				pData.Columns.Remove(col.Name + "_str");
			}

			var dis = new DatabaseItemService(Instance, Process, AuthenticatedSession);

			var cols = new List<string>();
			foreach (DataColumn col in pData.Columns) {
				cols.Add(col.ColumnName);
			}

			foreach (DataRow row in pData.Rows) {
				var result = Enumerable.Range(0, pSqls.cols.Count)
					.ToDictionary(
						i => pSqls.cols[i],
						i => row[pSqls.cols[i]]
					);

				foreach (var ecol in pSqls.equationColumns) {
					result.Add(ecol.Name, row[ecol.Name]);
				}

				if (!_processBase.isListProcess) {
					var ucols = dis.GetUserCols((int) row["item_id"], 0, new[] {"meta.read", "meta.write"});
					foreach (var col in cols) {
						if (!ucols.Contains(col)) {
							result.Remove(col);
						}
					}
				}

				GetItemCollection().StoreItem(result);
			}
		}

		public void FillSubItems(int parentItemId, int processTabSubProcessId) {
			var subItems = EntityRepository.GetSubItems(parentItemId, processTabSubProcessId);
			GetItemCollection().StoreItems(subItems);
		}
		
		public void FillSubItems(int parentItemId, string orderCol = "") {
			var itemsForParent = EntityRepository.FindItemForParentItemId(parentItemId, orderCol);
			GetItemCollection().StoreItems(itemsForParent);
		}

		public List<Dictionary<string, object>> GetItems(string sqlWhere, int portfolioIdId = 0) {
			var ib = new ItemBase(Instance, Process, AuthenticatedSession);
			var idps = new ItemDataProcessSelections(Instance, Process, AuthenticatedSession);
			var retval = new List<Dictionary<string, object>>();

			var processTable = Conv.ToSql("_" + Instance + "_" + Process);
			var portfolioSqlQueryService =
				new PortfolioSqlService(Instance, AuthenticatedSession, _itemColumns, processTable, Process);
			var pSqls = portfolioSqlQueryService.GetSql(portfolioIdId);
			if (portfolioIdId > 0) {
				pSqls.cols.Add("item_id");
				pSqls.sqlcols.Add("pf.item_id");
			}

			for (var i = 0; i < pSqls.sqlcols.Count; i++) {
				var colNAme = pSqls.cols[i];
				pSqls.sqlcols[i] = pSqls.sqlcols[i].Replace(colNAme + "_str", colNAme);
			}

			var headers = string.Join(", ", pSqls.sqlcols);

			var eSql1 = "";
			var esql2 = "";
			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					equationSql += "," + _itemColumns.GetEquationQuery(col) + " " + col.Name;
				}

				eSql1 = " SELECT  * " + equationSql + " FROM ( ";
				esql2 = " ) pf ";
			}

			var selectorWhere = "";
			var portfolios = new ProcessPortfolios(Instance, Process, AuthenticatedSession);
			var selectorPortfolioId = portfolios.GetDefaultSelectorId();
			if (selectorPortfolioId > 0) {
				var entityRepository = new EntityRepository(Instance, Process, AuthenticatedSession);
				selectorWhere = " AND " + entityRepository.GetWhere(selectorPortfolioId);
			}

			var sql = eSql1 + " SELECT " + headers + " FROM " + processTable + " pf " + esql2 + "  WHERE " + sqlWhere + selectorWhere;
			foreach (DataRow dr in _processBase.db.GetDatatable(sql, _processBase.DBParameters).Rows) {
				var itemId = (int) dr["item_id"];

				if (!_processBase.isListProcess && selectorPortfolioId == 0) {
					var stateList = new List<string> { "meta.read", "meta.write" };
					var states = new States(Instance, AuthenticatedSession);
					var fStates = states.GetEvaluatedStates(Process, "meta", itemId, stateList);
					if (!fStates.HasStateOnAnyLevel(stateList)) continue;
				}

				var result = Conv.DataRowToDictionary(dr);

				foreach (var ic in _itemColumns.GetItemColumns()) {
					if ((ic.IsProcess() || ic.IsList() || ic.IsComment()) && ic.LinkProcess != null &&
					    ic.Name.Equals("linked_tasks")) {
						result.Remove("linked_tasks");
						result.Add("linked_tasks",
							idps.GetItemDataProcessSelectionIds(itemId, ic.ItemColumnId, ic.LinkProcess, null));
					} else if (result.ContainsKey(ic.Name + "_str") &&
					           (ic.IsProcess() || ic.IsList() || ic.IsComment())) {
						if (DBNull.Value.Equals(result[ic.Name + "_str"])) {
							result.Add(ic.Name, new List<int>());
						} else {
							result.Add(ic.Name,
								Array.ConvertAll(Conv.ToStr(result[ic.Name + "_str"]).Split(','), Conv.ToInt)
							);
						}

						result.Remove(ic.Name + "_str");
					} else if (result.ContainsKey(ic.Name) && (ic.IsProcess() || ic.IsList() || ic.IsComment())) {
						if (DBNull.Value.Equals(result[ic.Name])) {
							result[ic.Name] = new List<int>();
						} else {
							result[ic.Name] =
								Array.ConvertAll(Conv.ToStr(result[ic.Name]).Split(','), Conv.ToInt);
						}
					} else if (result.ContainsKey(ic.Name + "_str") && ic.IsSubquery() &&
					           (ic.IsOfSubType(ItemColumn.ColumnType.List) ||
					            ic.IsOfSubType(ItemColumn.ColumnType.Process))) {
						if (DBNull.Value.Equals(result[ic.Name + "_str"])) {
							result.Add(ic.Name, new List<int>());
						} else {
							result.Add(ic.Name,
								Array.ConvertAll(Conv.ToStr(result[ic.Name + "_str"]).Split(','), Conv.ToInt)
							);
						}

						result.Remove(ic.Name + "_str");
					}
				}

				retval.Add(result);
			}

			return retval;
		}

		private ItemCollection GetItemCollection() {
			if (_itemCollection != null) return _itemCollection;

			_itemCollection = new ItemCollection(Instance, Process, AuthenticatedSession.item_id);
			return _itemCollection;
		}

		public void FillDummySubItems() {
			var param = new List<SqlParameter>();

			var dt = _processBase.db.GetDatatable(
				"select name from item_columns where instance=@instance and process=@process",
				_processBase.DBParameters);

			var result = Enumerable.Range(0, dt.Rows.Count)
				.ToDictionary(
					i => (string) dt.Rows[i]["name"],
					i => (object) "random data"
				);
			result.Add("item_id", 1);
			var newItem = CreateProcessItemFromDictionary(result);
			AddItem(newItem);
			AddItem(newItem);
			AddItem(newItem);
		}

		private ItemDataProcessSelections GetItemDataProcessSelections(string process = null) {
			if (process == null) {
				process = Process;
			}

			return new ItemDataProcessSelections(Instance, process, AuthenticatedSession);
		}

		private ProcessItem CreateProcessItemFromDictionary(Dictionary<string, object> result) {
			var newItem = new ProcessItem(Instance, Process, AuthenticatedSession.item_id);
			newItem.SetMembers(result);
			return newItem;
		}

		public PortfolioSqls GetPortfolioSqls(int portfolioId = 0, string textSearch = "") {
			var instance = Instance;
			var session = AuthenticatedSession;
			var portfolioSqlService = new PortfolioSqlService(instance, session, _itemColumns, _processBase.tableName,
				_processBase.process);
			return portfolioSqlService.GetSql(portfolioId, textSearch);
		}

		public List<int> AddContextualItemIds(string contextProcess, int contextItemId, int contextItemColumnId,
			List<int> result) {
			if (result == null) result = new List<int>();
			
			var originalProcess = contextProcess;
			var originalItemColumnId = contextItemColumnId;
			
			//We need to figure out if process is parent or child
			foreach (var f in RegisteredFeatures.GetFeatures()) {
				if (f.ImplementsProcessRelations == false || f.Enabled == false) continue;
				if (f.Process == contextProcess) {
					//Process turned out to be a child -- so let's find out who the parent is
					_processBase.SetDBParameters("@item_id", contextItemId);
					var ownerItemId = Conv.ToInt(_processBase.db.ExecuteScalar(
						"SELECT owner_item_id FROM _" + Instance + "_" + f.Process + " WHERE item_id = @item_id",
						_processBase.DBParameters));
					_processBase.DBParameters.Clear();
					if (ownerItemId == 0) continue;
					
					//Now we know the real parent -- let's change to that context		
					_processBase.SetDBParameters("@item_id", ownerItemId);
					var ownerItem = _processBase.db.GetDataRow("SELECT * FROM items WHERE item_id = @item_id",
						_processBase.DBParameters);
					_processBase.DBParameters.Clear();
					if (ownerItem == null) continue;

					contextItemId = Conv.ToInt(ownerItem["item_id"]);
					contextProcess = Conv.ToStr(ownerItem["process"]);
					break;
				}
			}

			//Get item column info
			var ics = new ItemColumns(Instance, originalProcess, AuthenticatedSession);
			var ic = ics.GetItemColumn(originalItemColumnId);
			_processBase.SetDBParameters(
				"@item_id", contextItemId,
				"@item_column_id", contextItemColumnId,
				"@data_type", ic.DataType,
				"@data_additional", ic.DataAdditional
			);

			//Add query for main item
			var sql = "SELECT DISTINCT selected_item_id FROM item_data_process_selections idps WHERE " +
			          "idps.item_id = @item_id AND item_column_id IN (" +
			          "SELECT item_column_id FROM item_columns " +
			          "WHERE process = '" + Conv.ToSql(contextProcess) +
			          "' AND data_type = @data_type AND data_additional = @data_additional)";

			//Add possibly related sub processes
			foreach (var f in RegisteredFeatures.GetFeatures()) {
				if (f.ImplementsProcessRelations == false || f.Enabled == false) continue;
				sql += "UNION SELECT DISTINCT idps.selected_item_id FROM item_data_process_selections idps WHERE " +
				       "idps.item_id IN (SELECT sp.item_id FROM _" + Instance + "_" + f.Process +
				       " sp WHERE sp.owner_item_id = @item_id) AND " +
				       "idps.item_column_id IN (" +
				       "SELECT ic.item_column_id FROM item_columns ic " +
				       "WHERE ic.process = '" + Conv.ToSql(f.Process) + "' AND ic.data_type = @data_type AND " +
				       "ic.data_additional = @data_additional)";
			}

			//Add Results
			foreach (DataRow r in _processBase.db.GetDatatable(sql, _processBase.DBParameters).Rows) {
				var id = Conv.ToInt(r["selected_item_id"]);
				if (!result.Contains(id)) result.Add(id);
			}

			return result;
		}
	}
}