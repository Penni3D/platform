﻿using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.processes;

namespace Keto5.x.platform.server.database {
	/// <summary>
	/// Collection of entities (or items). Handled by ItemCollectionService.
	/// </summary>
	public class ItemCollection {
		private readonly string _instance;
		private readonly string _process;
		private readonly int _userId;
		private readonly List<ProcessItem> collection = new List<ProcessItem>();

		public ItemCollection(string instance, string process, int userId) {
			_userId = userId;
			_instance = instance;
			_process = process;
		}

		public List<Dictionary<string, object>> GetItems() {
			return collection.Select(item => item.GetMembers()).ToList();
		}

		public void ClearMembers() {
			collection.Clear();
		}

		public void StoreItem(Dictionary<string, object> result) {
			var newItem = new ProcessItem(_instance, _process, _userId);
			newItem.SetMembers(result);
			AddItem(newItem);
		}

		public void StoreItems(List<Dictionary<string, object>> items) {
			foreach (var result in items) {
				StoreItem(result);
			}
		}

		public void AddItem(ProcessItem item) {
			collection.Add(item);
		}
	}
}