﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.core.resourceManagement;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Microsoft.Extensions.Hosting.Internal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.database {
    public interface IDatabaseItemService {
        int InsertItemRow(int ownerItemId = 0, int parentItemId = 0);
        // int InsertItemRow(int id);

        Dictionary<string, object> InsertItemRow(Dictionary<string, object> data,
            List<ItemColumn> cachedItemColumns = null, bool executeActions = true);

        List<Dictionary<string, object>> InsertItemRows(List<Dictionary<string, object>> data,
            List<ItemColumn> cachedItemColumns = null, bool executeActions = true);

        Dictionary<string, object> SaveItem(Dictionary<string, object> data, int? itemId = null,
            bool checkRights = false,
            FeatureStates featureStates = null, bool isArchiveSave = false, bool isLinkProcessSave = false,
            List<ItemColumn> cachedItemColumns = null, bool followsInsert = false, bool returnSubqueries = true,
            bool hadLinks = false, bool preventSpecialProcessChecks = false, int fileId = 0);

        Dictionary<string, object> GetItem(int itemId, bool cache = true, bool checkRights = true,
            bool onlySelectorColumns = false, bool onlyConditionColumns = false);

        Dictionary<string, object> GetItem(int itemId, DataTable data, bool checkRights = true,
            bool onlyConditionColumns = false);


        DataTable GetAllData();
        DataTable GetAllData(string idStr, string order = "", string extraWhere = "");

        void Delete(int itemId, bool checkRights = true, bool removeArchive = false);

        bool IsUniqueColumn(int columnId, int itemId, string value);
    }

    public class DatabaseItemService : SessionBaseInfo, IDatabaseItemService {
//		private List<string> userColsRead = null;
//		private List<string> userColsWrite = null;

        private readonly Dictionary<int, Dictionary<string, object>>
            _privateCache; // This dies when instance of this class dies (after request is done)

        private readonly Dictionary<string, List<string>>
            _privateCache2; // This dies when instance of this class dies (after request is done)

        //private string _tableName;
        public IEntityRepository EntityRepository;
        public IItemColumns ItemColumns;
        public ProcessBase ProcessBase;
        public ProcessContainers ProcessContainers;

        public DatabaseItemService(string instance, string process, AuthenticatedSession session) :
            base(instance, process, session) {
            ProcessBase = new ProcessBase(instance, process, session);
            ItemColumns = new ItemColumns(instance, process, session);
            ProcessContainers = new ProcessContainers(Instance, ProcessBase.process, ProcessBase.authenticatedSession);
            EntityRepository = new EntityRepository(instance, process, AuthenticatedSession);
            _privateCache = new Dictionary<int, Dictionary<string, object>>();
            _privateCache2 = new Dictionary<string, List<string>>();
        }

        public Dictionary<string, object> GetItem(int itemId, bool cache = true, bool checkRights = true,
            bool onlySelectorColumns = false, bool onlyConditionColumns = false) {
            if (cache && _privateCache.ContainsKey(itemId)) return _privateCache[itemId];

            // Never check rights for lists
            if (ProcessBase.isListProcess) checkRights = false;

            //If checking rights, get string list of columns with access
            List<string> userCols = null;
            if (checkRights) userCols = GetUserCols(itemId, 0, new[] { "meta.read", "meta.write" });

            ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
            ProcessBase.SetDBParameter("@instance", ProcessBase.instance);
            ProcessBase.SetDBParameter("@signedInUserId", AuthenticatedSession.item_id);

            //Check if columns need to limited
            var limitColumn = new List<string>();
            var removeColumn = new List<string>();
            if (onlySelectorColumns) {
                var pp = new ProcessPortfolios(Instance, ProcessBase.process, ProcessBase.authenticatedSession);
                limitColumn = pp.GetDefaultSelectorColumnNames();
            }

            if (Process == "user") removeColumn.AddRange(User.GetRestrictionList());

            if (limitColumn.Count == 0 || (limitColumn.Count > 0 && ProcessBase.isListProcess && !limitColumn.Contains("list_item")))
                onlySelectorColumns = false;

            //Get item from DB
            var sql = ItemColumns.GetItemSql(cols: onlySelectorColumns ? limitColumn : null,
                          onlyConditionColumns: onlyConditionColumns) +
                      " WHERE item_id=@itemId ";

            var dt = ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);

            if (dt.Rows.Count <= 0) throw new CustomArgumentException("Item cannot be found with item_id " + itemId);
            var result = Conv.DataRowToDictionary(dt.Rows[0]);

            //Get data for process fields (with one query)
            var idps = new ItemDataProcessSelections(ProcessBase.instance, Process, ProcessBase.authenticatedSession);
            ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
            var selectionsDt = ProcessBase.db.GetDatatable(
                "SELECT item_column_id, selected_item_id, link_item_id FROM item_data_process_selections WHERE item_id = @item_id",
                ProcessBase.DBParameters);

            //Process all fields and skip fields with no access
            foreach (var ic in ItemColumns.GetItemColumns(onlyConditionColumns: onlyConditionColumns)) {
                //Skip fields with no access
                if ((checkRights && !userCols.Contains(ic.Name)) ||
                    (onlySelectorColumns && !limitColumn.Contains(ic.Name))) {
                    if (result.ContainsKey(ic.Name)) result.Remove(ic.Name);
                    continue;
                }

                if (removeColumn.Contains(ic.Name)) {
                    if (result.ContainsKey(ic.Name)) result.Remove(ic.Name);
                    continue;
                }

                if ((ic.IsProcess() || ic.IsList()) && ic.LinkProcess != null && ic.Name.Equals("linked_tasks")) {
                    result.Add(ic.Name,
                        idps.GetItemDataProcessSelectionIds(itemId, ic.ItemColumnId, ic.LinkProcess, null,
                            selectionsDt));
                } else if (ic.IsProcess() && ic.UseLucene) {
                    var da2Dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(ic.DataAdditional2);
                    var luceneScore = 0.0;
                    var luceneMaxCount = 10;
                    if (da2Dict.ContainsKey("luceneScore")) {
                        luceneScore = Conv.ToDouble(da2Dict["luceneScore"]);
                    }

                    if (da2Dict.ContainsKey("luceneMaxCount")) {
                        luceneMaxCount = Conv.ToInt(da2Dict["luceneMaxCount"]);
                    }

                    var lucene = new processes.Lucene(ProcessBase.instance, ProcessBase.authenticatedSession);
                    result.Add(ic.Name, lucene.Search(result, ProcessBase.process, luceneScore, luceneMaxCount));
                } else if (ic.IsProcess() || ic.IsList()) {
                    result.Add(ic.Name, idps.GetItemDataProcessSelectionIds(itemId, ic.ItemColumnId, selectionsDt));
                } else if (ic.IsSubquery() && (ic.IsOfSubType(ItemColumn.ColumnType.List) ||
                                               ic.IsOfSubType(ItemColumn.ColumnType.Process))) {
                    if (DBNull.Value.Equals(result[ic.Name + "_str"])) {
                        result.Add(ic.Name, new List<int>());
                    } else {
                        result.Add(ic.Name,
                            Array.ConvertAll(Conv.ToStr(result[ic.Name + "_str"]).Split(','), Conv.ToInt)
                        );
                    }

                    result.Remove(ic.Name + "_str");
                } else if (ic.IsTag()) {
                    var tagIb = new DatabaseItemService(Instance, "instance_tags",
                        AuthenticatedSession);

                    var tagIds = idps.GetItemDataProcessSelectionIds(itemId, ic.ItemColumnId, selectionsDt);

                    var tags = new List<string>();
                    foreach (var tagId in tagIds) {
                        var tagData = tagIb.GetItem(tagId);
                        tags.Add(Conv.ToStr(tagData["tag_name"]));
                    }

                    result.Add(ic.Name, tags);
                } else if (ic.IsRating()) {
                    ProcessBase.SetDBParam(SqlDbType.Int, "@userId", ProcessBase.authenticatedSession.item_id);
                    var ratingSql =
                        "SELECT TOP 1 value FROM _" + ProcessBase.instance +
                        "_process_ratings WHERE user_item_id = @userId AND process_item_id = @itemId";

                    var rating = ProcessBase.db.ExecuteScalar(ratingSql, ProcessBase.DBParameters);
                    result.Add(ic.Name, rating);
                } else if (ic.IsEquation() &&
                           (Conv.ToInt(ic.DataAdditional2) == 5 || Conv.ToInt(ic.DataAdditional2) == 6)) {
                    result[ic.Name] = Array.ConvertAll(Conv.ToStr(result[ic.Name]).Split(','), s => Conv.ToInt(s));
                } else if (ic.IsRich()) {
                    result[ic.Name] = JsonConvert.DeserializeObject(Conv.ToStr(result[ic.Name]));
                } else if (ic.IsTranslation()) {
                    var v1 = new Translations(Instance, AuthenticatedSession).GetTranslations(
                        "variable_" + ic.Name + "_" + itemId + "_" + ic.ItemColumnId);
                    result[ic.Name] = v1;
                } else if (ic.IsDescription()) {
                    var states = new States(Instance, AuthenticatedSession);
                    result[ic.Name] = states.ReplaceTags(ic.Process, result, ic.DataAdditional);
                }
            }

            if (!_privateCache.ContainsKey(itemId)) _privateCache.Add(itemId, result);

            return result;
        }

        public Dictionary<string, object> GetItem(int itemId, DataTable data, bool checkRights = true,
            bool onlyConditionColumns = false) {
            var rows = data.Select("item_id = " + itemId);

            if (rows.Length > 0) {
                //Use Linq to convert row into a dictionary
                var result = Enumerable.Range(0, data.Rows[0].Table.Columns.Count)
                    .ToDictionary(
                        i => data.Rows[0].Table.Columns[i].ColumnName,
                        i => rows[0][data.Rows[0].Table.Columns[i]]
                    );

                List<string> userCols = null;
                if (checkRights) {
                    userCols = GetUserCols(itemId, 0, new[] { "meta.read", "meta.write" });
                }

                foreach (var key in new List<string>(result.Keys)) {
                    if (checkRights && !userCols.Contains(key)) {
                        result.Remove(key);
                    }
                }

                return result;
            }

            return GetItem(itemId, checkRights: checkRights, onlyConditionColumns: onlyConditionColumns);
        }

        public void Delete(int itemId, bool checkRights = true, bool removeArchive = false) {
            ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
            ProcessBase.SetDBParameter("@instance", ProcessBase.instance);

            if (checkRights) {
                var states = new States(ProcessBase.instance, AuthenticatedSession);
                var fStates = states.GetEvaluatedStates(ProcessBase.process, "meta", itemId,
                    new List<string> { "meta.delete" });

                if (!fStates.States.Contains("meta.delete")) {
                    var exception = new CustomPermissionException("NOT_ALLOWED_TO_DELETE_THE_PROCESS",
                        "Current user can't delete process " + Process, BaseServerException.ExceptionSubType.DELETE);
                    exception.Data.Add("process", Process);
                    throw exception;
                }
            }

            //Invalidate Client Cache on every delete
            ProcessBase.InvalidateClientItemAndSqCache(itemId, null);

            ProcessBase.db.ExecuteDeleteQuery(
                "DELETE FROM items WHERE instance = @instance AND process = @process AND item_id = @itemId",
                ProcessBase.DBParameters);

            if (removeArchive) {
                ProcessBase.db.ExecuteDeleteQuery(
                    "DELETE FROM archive__" + Conv.ToSql(ProcessBase.instance + "_" + ProcessBase.process) +
                    " WHERE item_id = @itemId",
                    ProcessBase.DBParameters);
            }

            if (_privateCache.ContainsKey(itemId)) _privateCache.Remove(itemId);

            var lucene = new processes.Lucene(ProcessBase.instance, ProcessBase.authenticatedSession);
            lucene.DeleteIndex(ProcessBase.process, itemId);
        }

        public int InsertItemRow(int ownerItemId = 0, int parentItemId = 0) {
            return ProcessBase.InsertItemRow(parentItemId, ownerItemId:ownerItemId);
        }

        public List<Dictionary<string, object>> InsertItemRows(List<Dictionary<string, object>> data,
            List<ItemColumn> cachedItemColumns = null, bool executeActions = true) {
            var results = new List<Dictionary<string, object>>();
            foreach (var d in data) {
                results.Add(InsertItemRow(d, cachedItemColumns, executeActions));
            }

            return results;
        }

        public Dictionary<string, object> InsertItemRow(Dictionary<string, object> data,
            List<ItemColumn> cachedItemColumns = null, bool executeActions = true) {
            var id = 0;
            if (executeActions) {
                id = ProcessBase.InsertItemRow();
            } else {
                id = ProcessBase.InsertItemRowNoAction();
            }

            data["item_id"] = id;
            var result = SaveItem(data, null, false, null, false, false, cachedItemColumns, true);

            return result;
        }

        public int InsertItemRow(int id) {
            return ProcessBase.InsertItemRow(id);
        }

        public Dictionary<string, object> SaveItem(Dictionary<string, object> data, int? itemId = null,
            bool checkRights = false, FeatureStates featureStates = null,
            bool isArchiveSave = false, bool isLinkProcessSave = false, List<ItemColumn> cachedItemColumns = null,
            bool followsInsert = false, bool returnSubqueries = true, bool hadLinks = false,
            bool preventSpecialProcessChecks = false, int fileId = 0) {
            if (itemId == null) itemId = Conv.ToInt(data["item_id"]);

            var sql = "";

            if (isArchiveSave) {
                sql = "UPDATE archive_" + ProcessBase.tableName + " SET ";
            } else {
                sql = "UPDATE " + ProcessBase.tableName + " SET ";
            }

            var first = true;
            var foundUpdateData = false;
            var foundArchiveUserId = false;

            if (data.ContainsKey("order_no")) {
                foundUpdateData = true;
                first = false;
                sql += "order_no = @order_no";
                if (data["order_no"] == null) {
                    ProcessBase.SetDBParam(SqlDbType.NVarChar, "@order_no", DBNull.Value);
                } else {
                    ProcessBase.SetDBParam(SqlDbType.NVarChar, "@order_no", data["order_no"]);
                }
            }

            var colsToRemove = new List<string>();
            var skippedCols = "";
            var subQueryCols = new List<ItemColumn>();
            var eqColumns = new List<ItemColumn>();
            var icols = cachedItemColumns;
            if (icols == null) icols = ItemColumns.GetItemColumns();

            ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
            var idStr = "-1";
            foreach (var ic in icols) {
                if ((ic.DataType == 5 || ic.DataType == 6 || ic.DataType == 12 || ic.IsTag()) &&
                    data.ContainsKey(ic.Name)) {
                    idStr += "," + ic.ItemColumnId;
                }
            }

            var dt = ProcessBase.db.GetDatatable(
                "SELECT * FROM item_data_process_selections WHERE item_id = @item_id AND item_column_id IN (" + idStr +
                ")",
                ProcessBase.DBParameters, true);
            dt.TableName = "item_data_process_selections";

            var luceneUpdate = false;
            foreach (var ic in icols) {
                if (ic.Name != null && data.ContainsKey(ic.Name)) {
                    if (!ic.IsSaveableColumn()) {
//						Console.WriteLine("Oh boy!!! someone is trying to save column that cannot be saved!!");
//						Console.WriteLine("Column is: name: " + ic.Name + " itemColumnId: " + ic.ItemColumnId +
//						                  " itemId: " + itemId);
                    }

                    if (checkRights) {
                        var userCols = GetUserCols((int)itemId, 1, new[] { "meta.write" });
                        if (!userCols.Contains(ic.Name)) {
                            if (ic.IsSaveableColumn()) {
                                skippedCols += "," + ic.Name;
                                continue;
                            }
                        }
                    }

                    if (ic.IsTranslation() && data[ic.Name] != null) {
                        try {
                            var jsonObject = data[ic.Name].ToString();
                            var translationsData =
                                JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonObject);
                            foreach (var code in translationsData.Keys) {
                                var t = new Translations(ProcessBase.instance, AuthenticatedSession, code);
                                t.insertOrUpdateProcessTranslation(Process,
                                    "variable_" + ic.Name + "_" + itemId + "_" + ic.ItemColumnId,
                                    translationsData[code]);
                            }

                            foundUpdateData = true;
                            if (first) {
                                first = false;
                                sql += ic.Name + " = @" + ic.Name;
                            } else {
                                sql += "," + ic.Name + " = @" + ic.Name;
                            }
                        } catch (Exception e) {
                        }
                    } else if (!ic.IsPortfolio() && !ic.IsList() && !ic.IsProcess() && !ic.IsComment() &&
                               !ic.IsJoin() &&
                               !ic.IsAttachment() &&
                               !ic.IsTranslation() &&
                               !ic.IsLink() && !ic.IsSubquery() && !ic.IsDescription() && !ic.IsTag() &&
                               !ic.IsRating() && !ic.IsEquation() && !ic.IsRm()) {
                        foundUpdateData = true;
                        var updateCol = " = @" + ic.Name;


                        if (ic.UniqueCol == 1) {
                            updateCol = " = (SELECT CASE WHEN (SELECT COUNT(" + ic.Name + ") FROM _" + Instance + "_" +
                                        Process + " WHERE item_id <> @item_id AND " + ic.Name + " = @" + ic.Name +
                                        ") = 0 THEN @" + ic.Name + " ELSE " + ic.Name + " END)";


                            if (Conv.ToInt(ProcessBase.db.ExecuteScalar("IF (SELECT COUNT(" + ic.Name + ") FROM _" +
                                                                        Instance + "_" +
                                                                        Process + " WHERE item_id <> @item_id AND " +
                                                                        Conv.ToSql(ic.Name) + " = '" +
                                                                        Conv.ToSql(data[ic.Name]) + "') <> 0 " +
                                                                        "SELECT 1 ELSE SELECT 69",
                                    ProcessBase.DBParameters)) == 1) {
                                var message = AuthenticatedSession.locale_id == "en-GB"
                                    ? "Error: trying to update value " + data[ic.Name] + " to unique column " +
                                      ic.Name + " in item_id " + itemId +
                                      " but the value already exists in some other row "
                                    : "Virhe: ei voitu päivittää arvoa " + data[ic.Name] +
                                      " vain uniikin arvon hyväksyvään kolumniin " + ic.Name + " item_id:llä " +
                                      itemId + " koska sama arvo on jo jollain muulla rivillä";

                                Diag.Log(Instance, message, 0, Diag.LogSeverities.Sql,
                                    Diag.LogCategories.BackgroundTask, file_id: fileId);
                            }
                        }

                        if (first) {
                            first = false;
                            sql += ic.Name + updateCol;
                        } else {
                            sql += "," + ic.Name + updateCol;
                        }
                    }

                    switch (ic.DataType) {
                        case 23:
                            ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name,
                                Conv.IfNullOrEmptyThenDbNull(Conv.ToStr(data[ic.Name])));

                            colsToRemove.Add(ic.Name);
                            break;
                        case 24:
                            ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name,
                                "variable_" + ic.Name + "_" + itemId + "_" + ic.ItemColumnId);
                            colsToRemove.Add(ic.Name);
                            break;
                        case 0:
                            if (data[ic.Name] != null && data[ic.Name].GetType() == typeof(JArray))
                                data[ic.Name] = Conv.ToDelimitedString((JArray)data[ic.Name]);

                            ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name,
                                Conv.IfNullThenDbNull(data[ic.Name]));
                            colsToRemove.Add(ic.Name);
                            break;
                        case 1:
                            ProcessBase.SetDBParam(SqlDbType.Int, "@" + ic.Name,
                                Conv.IfNullOrEmptyThenDbNull(data[ic.Name]));
                            colsToRemove.Add(ic.Name);
                            break;
                        case 2:
                        case 14:
                            if (!DBNull.Value.Equals(data[ic.Name]) && data[ic.Name] != null) {
                                if (data[ic.Name] is string && data[ic.Name].Equals("")) {
                                    ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name, DBNull.Value);
                                } else if (data[ic.Name] is string) {
                                    ProcessBase.SetDBParam(SqlDbType.Float, "@" + ic.Name,
                                        Conv.ToDouble(data[ic.Name]));
                                } else {
                                    ProcessBase.SetDBParam(SqlDbType.Float, "@" + ic.Name,
                                        Conv.IfNullOrEmptyThenDbNull(data[ic.Name]));
                                }
                            } else {
                                ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name, DBNull.Value);
                            }

                            colsToRemove.Add(ic.Name);
                            break;
                        case 3:
                        case 13:
                            ProcessBase.SetDBParam(SqlDbType.DateTime, "@" + ic.Name,
                                Conv.IfNullOrEmptyThenDbNull(data[ic.Name]));
                            colsToRemove.Add(ic.Name);
                            break;
                        case 4:
                            if (data[ic.Name] != null && !DBNull.Value.Equals(data[ic.Name])) {
                                var s = Conv.ToStr(data[ic.Name]);
                                ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name, s);
                            } else {
                                ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name, DBNull.Value);
                            }

                            colsToRemove.Add(ic.Name);
                            break;
                        case 5:
                        case 6:
                            if (ic.UseLucene) continue;

                            ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
                            ProcessBase.SetDBParam(SqlDbType.Int, "@item_column_id", ic.ItemColumnId);
                            if (ic.LinkProcess != null) {
                                var jsonObject = data[ic.Name].ToString();
                                if (jsonObject.Length > 0 && jsonObject != "[]" && jsonObject != "{}" &&
                                    (data[ic.Name] is string || data[ic.Name].GetType().ToString()
                                        .Equals("Newtonsoft.Json.Linq.JObject"))) {
                                    DeleteFromDt(dt, Conv.ToInt(itemId), ic.ItemColumnId, data);

                                    var linkData =
                                        JsonConvert.DeserializeObject<Dictionary<int, object>>(
                                            jsonObject);
                                    var linkData2 = new Dictionary<int, object>();
                                    foreach (var key in linkData.Keys) {
                                        var linkIb = new DatabaseItemService(Instance, ic.LinkProcess,
                                            AuthenticatedSession);
                                        linkIb.ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
                                        linkIb.ProcessBase.SetDBParam(SqlDbType.Int, "@item_column_id",
                                            ic.ItemColumnId);
                                        linkIb.ProcessBase.SetDBParam(SqlDbType.Int, "@selected_item_id", key);
                                        if (linkData[key] != null) {
                                            var jsonObject2 = linkData[key].ToString();
                                            var linkRow =
                                                JsonConvert
                                                    .DeserializeObject<Dictionary<string, object>>(jsonObject2);
                                            if (linkRow.ContainsKey("item_id")) {
                                                AddToDt(dt, Conv.ToInt(itemId), ic, key,
                                                    Conv.ToInt(linkRow["item_id"]));
                                                linkIb.SaveItem(linkRow, null, false, null, false, true);
                                            } else {
                                                var newLinkRowId = linkIb.ProcessBase.InsertItemRow();
                                                AddToDt(dt, Conv.ToInt(itemId), ic, key, newLinkRowId);

                                                linkRow.Add("item_id", newLinkRowId);
                                                linkIb.SaveItem(linkRow, null, false, null, false, true);
                                            }

                                            linkData2.Add(key, linkRow);
                                        } else {
                                            var newLinkRowId = linkIb.ProcessBase.InsertItemRow();
                                            AddToDt(dt, Conv.ToInt(itemId), ic, key, newLinkRowId);
                                            linkData2.Add(key, linkIb.GetItem(newLinkRowId));
                                        }

                                        data[ic.Name] = linkData2;
                                    }
                                } else {
                                    var selectedIds = new List<int>();
                                    if (data[ic.Name] != null && data[ic.Name].GetType().IsArray &&
                                        data[ic.Name].GetType().GetElementType().Equals(typeof(int))) {
                                        foreach (var id in (int[])data[ic.Name]) {
                                            selectedIds.Add(id);

                                            AddToDt(dt, Conv.ToInt(itemId), ic, id);
                                        }
                                    } else if (data[ic.Name] != null) {
                                        try {
                                            var ids =
                                                JsonConvert.DeserializeObject<List<int>>(jsonObject);
                                            foreach (var id in ids) {
                                                selectedIds.Add(id);

                                                AddToDt(dt, Conv.ToInt(itemId), ic, id);
                                            }
                                        } catch {
                                            try {
                                                var ids = (List<int>)data[ic.Name];
                                                foreach (var id in ids) {
                                                    selectedIds.Add(id);

                                                    AddToDt(dt, Conv.ToInt(itemId), ic, id);
                                                }
                                            } catch {
                                            }
                                        }
                                    }

                                    DeleteFromDt(dt, Conv.ToInt(itemId), ic.ItemColumnId, selectedIds);
                                }
                            } else {
                                if (!followsInsert) {
                                    DeleteFromDt(dt, Conv.ToInt(itemId), ic.ItemColumnId, data[ic.Name]);
                                }

                                if (data[ic.Name] != null && data[ic.Name].GetType().IsArray &&
                                    data[ic.Name].GetType().GetElementType() == typeof(int)) {
                                    foreach (var id in (int[])data[ic.Name]) {
                                        AddToDt(dt, Conv.ToInt(itemId), ic, id);
                                    }
                                } else if (data[ic.Name] != null) {
                                    if (data[ic.Name] is JArray) {
                                        var ids = JsonConvert.DeserializeObject<List<int>>(data[ic.Name]
                                            .ToString());
                                        foreach (var id in ids) {
                                            AddToDt(dt, Conv.ToInt(itemId), ic, id);
                                        }
                                    } else if (data[ic.Name] is string) {
                                        if ((string)data[ic.Name] != "") {
                                            var ids = (List<int>)data[ic.Name];
                                            foreach (var id in ids) {
                                                AddToDt(dt, Conv.ToInt(itemId), ic, id);
                                            }
                                        }
                                    } else {
                                        var ids = (List<int>)data[ic.Name];
                                        foreach (var id in ids) {
                                            AddToDt(dt, Conv.ToInt(itemId), ic, id);
                                        }
                                    }
                                }
                            }

                            //Change allocation cost center if cost center is changed
                            if (ic.Name == "cost_center" && ic.Process == "user") {
                                foreach (var row in dt.Select(
                                             "item_id = " + itemId + " AND item_column_id = " + ic.ItemColumnId)) {
                                    ProcessBase.SetDBParam(SqlDbType.Int, "@resource_item_id", itemId);
                                    ProcessBase.SetDBParam(SqlDbType.Int, "@cost_center", row["selected_item_id"]);

                                    var findAllocations = "SELECT iq.item_id FROM ( " +
                                                          "SELECT a.item_id, a.resource_item_id, MIN(ad.date) AS start_date, MAX(ad.date) AS end_date FROM _" +
                                                          ProcessBase.instance + "_allocation a " +
                                                          "LEFT JOIN allocation_durations ad ON a.item_id = ad.allocation_item_id " +
                                                          "WHERE a.resource_item_id = @resource_item_id " +
                                                          "GROUP BY a.item_id, a.resource_item_id " +
                                                          ") iq " +
                                                          "WHERE GETDATE() BETWEEN start_date AND end_date";

                                    ProcessBase.db.ExecuteUpdateQuery(
                                        "UPDATE _" + ProcessBase.instance +
                                        "_allocation SET cost_center = @cost_center WHERE item_id IN (" +
                                        findAllocations + ")",
                                        ProcessBase.DBParameters);
                                }
                            }

                            Cache.Remove(ProcessBase.instance, "idps_" + itemId + " " + ic.ItemColumnId);
                            Cache.Remove(ProcessBase.instance, "idpsl_" + itemId + " " + ic.ItemColumnId);

                            colsToRemove.Add(ic.Name);
                            break;
                        case 7:
                            var password = data[ic.Name];

                            PasswordValidation.IsPasswordValid(Conv.ToStr(password), true, true);

                            ProcessBase.SetDBParam(SqlDbType.NVarChar, "@" + ic.Name,
                                password != null
                                    ? HashPbkdf2.CreateHash(Conv.ToStr(password))
                                    : Conv.IfNullThenDbNull(password));

                            colsToRemove.Add(ic.Name);
                            break;
                        case 9:
                            var rating = data[ic.Name];
                            var userId = ProcessBase.authenticatedSession.item_id;

                            ProcessBase.SetDBParam(SqlDbType.NVarChar, "@rating", Conv.IfNullThenDbNull(rating));
                            ProcessBase.SetDBParam(SqlDbType.NVarChar, "@userId", userId);
                            var ratingId = Conv.ToInt(ProcessBase.db.ExecuteUpdateQuery(
                                "UPDATE _" + ProcessBase.instance +
                                "_process_ratings SET value = @rating WHERE user_item_id = @userId AND process_item_id = @item_id",
                                ProcessBase.DBParameters));

                            if (ratingId == 0 && rating != null) {
                                var ratingIb =
                                    new DatabaseItemService(Instance, "process_ratings", AuthenticatedSession);
                                var newRating = ratingIb.GetItem(ratingIb.InsertItemRow());
                                newRating["value"] = rating;
                                newRating["user_item_id"] = userId;
                                newRating["process_item_id"] = itemId;
                                ratingIb.SaveItem(newRating);
                            }

                            colsToRemove.Add(ic.Name);
                            break;
                        case 16:
                            subQueryCols.Add(ic);
                            break;
                        case 10:
                        case 12:
                        case 15:
                        case 18:
                            foundUpdateData = true;
                            if (!foundArchiveUserId) {
                                foundArchiveUserId = true;
                                if (first) {
                                    first = false;
                                    sql += " archive_userid = -1 ";
                                } else {
                                    sql += ", archive_userid = -1 ";
                                }
                            }

                            break;
                        case 19:
                            if (data[ic.Name] != null) {
                                var tagIds = new List<int>();

                                try {
                                    var tagsList = data[ic.Name].GetType() == new List<string>().GetType()
                                        ? (List<string>)data[ic.Name]
                                        : JsonConvert.DeserializeObject<List<string>>(data[ic.Name].ToString());

                                    foreach (var tag in tagsList) {
                                        ProcessBase.SetDBParam(SqlDbType.NVarChar, "@tagName", tag);
                                        var tagItemId = Conv.ToInt(ProcessBase.db.ExecuteScalar(
                                            "SELECT item_id FROM _" + ProcessBase.instance +
                                            "_instance_tags WHERE tag_name = @tagName",
                                            ProcessBase.DBParameters));
                                        Dictionary<string, object> tagData = null;
                                        var tagIb = new DatabaseItemService(Instance, "instance_tags",
                                            AuthenticatedSession);
                                        if (tagItemId == 0) {
                                            tagItemId = tagIb.InsertItemRow();
                                            tagData = tagIb.GetItem(tagItemId);
                                            tagData["tag_name"] = tag;
                                            tagIb.SaveItem(tagData);
                                        }

                                        ;
                                        tagIds.Add(tagItemId);

                                        var groupName = ProcessBase.process;
                                        ProcessBase.SetDBParam(SqlDbType.NVarChar, "@groupName", groupName);
                                        var groupItemId = Conv.ToInt(ProcessBase.db.ExecuteScalar(
                                            "SELECT item_id FROM _" + ProcessBase.instance +
                                            "_instance_tags_groups WHERE tag_group = @groupName",
                                            ProcessBase.DBParameters));

                                        if (groupItemId == 0) {
                                            var groupIb = new DatabaseItemService(Instance, "instance_tags_groups",
                                                AuthenticatedSession);
                                            groupItemId = groupIb.InsertItemRow();
                                            var groupData = groupIb.GetItem(groupItemId);
                                            groupData["tag_group"] = groupName;
                                            groupIb.SaveItem(groupData);
                                        }

                                        if (tagData == null || !tagData.ContainsKey("tag_group")) {
                                            tagData = tagIb.GetItem(tagItemId);
                                        }

                                        if (!((List<int>)tagData["tag_group"]).Contains(groupItemId)) {
                                            ((List<int>)tagData["tag_group"]).Add(groupItemId);
                                            tagIb.SaveItem(tagData);
                                        }
                                    }

                                    if (!followsInsert) {
                                        DeleteFromDt(dt, Conv.ToInt(itemId), ic.ItemColumnId, tagIds);
                                    }

                                    foreach (var tagItemId in tagIds) {
                                        AddToDt(dt, Conv.ToInt(itemId), ic, tagItemId);
                                    }
                                } catch (Exception e) {
                                    Console.WriteLine("Exception caught: " + e.Message + " " + e.StackTrace);
                                }
                            }

                            Cache.Remove(ProcessBase.instance, "idps_" + itemId + " " + ic.ItemColumnId);
                            Cache.Remove(ProcessBase.instance, "idpsl_" + itemId + " " + ic.ItemColumnId);

                            colsToRemove.Add(ic.Name);
                            break;
                    }

                    if (ic.RelativeColumnId.HasValue && ic.RelativeColumnId.Value > 0
                       ) // item tried to save what is actually another process' field
                    {
                        var foreign_process_ic = ItemColumns.GetItemColumn((int)ic.RelativeColumnId);

                        if (foreign_process_ic.IsProcess() || foreign_process_ic.IsList()) {
                            var foreign_process_pb = new ProcessBase(ProcessBase.instance, foreign_process_ic.Process,
                                ProcessBase.authenticatedSession);
                            var current_where_selected = foreign_process_pb.db.GetDatatable(
                                "SELECT * FROM item_data_process_selections WHERE item_column_id = " +
                                ic.RelativeColumnId + " AND selected_item_id = " + itemId + "", null);
                            List<int> new_where_selected = new List<int>();
                            if (data[ic.Name] != null && data[ic.Name].GetType().ToString()
                                    .Contains("Newtonsoft.Json.Linq.JArray"))
                                new_where_selected =
                                    ((Newtonsoft.Json.Linq.JArray)data[ic.Name]).ToObject<List<int>>(); //HAE TÄTÄ ALLU
                            if (data[ic.Name] != null && data[ic.Name].GetType().IsArray)
                                new_where_selected = ((int[])data[ic.Name]).ToList<int>();

                            foreach (DataRow dr in current_where_selected.Rows)
                                if (new_where_selected.Contains((int)dr["item_id"]) == false) {
                                    foreign_process_pb.db.ExecuteDeleteQuery(
                                        "DELETE FROM item_data_process_selections WHERE item_id = " +
                                        (int)dr["item_id"] + " AND item_column_id = " + ic.RelativeColumnId +
                                        " AND selected_item_id = " + itemId + "", null);
                                    Cache.Remove(foreign_process_pb.instance,
                                        "idps_" + (int)dr["item_id"] + " " + ic.RelativeColumnId);
                                    Cache.Remove(foreign_process_pb.instance,
                                        "idpsl_" + (int)dr["item_id"] + " " + ic.RelativeColumnId);
                                }

                            foreach (int new_id in new_where_selected)
                                if (current_where_selected
                                        .Select("item_id = " + new_id + " AND item_column_id = " + ic.RelativeColumnId +
                                                " AND selected_item_id = " + itemId + "").Length == 0) {
                                    foreign_process_pb.db.ExecuteInsertQuery(
                                        "INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES (" +
                                        new_id + "," + ic.RelativeColumnId + "," + itemId + ")", null);
                                    Cache.Remove(foreign_process_pb.instance,
                                        "idps_" + new_id + " " + ic.RelativeColumnId);
                                    Cache.Remove(foreign_process_pb.instance,
                                        "idpsl_" + new_id + " " + ic.RelativeColumnId);
                                }
                        }
                    }
                }

                if (ic.IsSubquery()) {
                    var cids = ProcessContainers.GetContainerIds(ic.ItemColumnId);
                    if (cids.Count > 0) {
                        if (!subQueryCols.Contains(ic)) subQueryCols.Add(ic);
                    }
                }


                if (ic.IsEquation()) {
                    var cids = ProcessContainers.GetContainerIds(ic.ItemColumnId);
                    if (cids.Count > 0) {
                        if (!eqColumns.Contains(ic)) eqColumns.Add(ic);
                    }
                }

                if (ic.IsProcess() && ic.UseLucene) {
                    luceneUpdate = true;
                    var da2Dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(ic.DataAdditional2);
                    var luceneScore = 0.0;
                    var luceneMaxCount = 10;
                    if (da2Dict.ContainsKey("luceneScore")) luceneScore = Conv.ToDouble(da2Dict["luceneScore"]);
                    if (da2Dict.ContainsKey("luceneMaxCount")) luceneMaxCount = Conv.ToInt(da2Dict["luceneMaxCount"]);

                    var lucene = new processes.Lucene(ProcessBase.instance, ProcessBase.authenticatedSession);
                    if (data.ContainsKey(ic.Name)) {
                        data[ic.Name] = lucene.Search(data, ProcessBase.process, luceneScore, luceneMaxCount);
                    } else {
                        data.Add(ic.Name, lucene.Search(data, ProcessBase.process, luceneScore, luceneMaxCount));
                    }
                }
            }

            if (dt.Rows.Count > 0) ProcessBase.db.ExecuteFromDataTable(dt);

            if (isArchiveSave) {
                ProcessBase.SetDBParam(SqlDbType.Int, "@archive_id", itemId);
                sql += " WHERE archive_id = @archive_id";
            } else {
                ProcessBase.SetDBParam(SqlDbType.Int, "@item_id", itemId);
                sql += " WHERE item_id = @item_id";
            }

            if (!foundUpdateData && skippedCols.Length > 0) {
                var exception = new CustomPermissionException("NOT_ALLOWED_TO_WRITE_TO_PROCESS",
                    "Current user can't write to process " + Process + " " + skippedCols);

                exception.Data.Add("process", Process);
                throw exception;
            }

            if (preventSpecialProcessChecks == false && Process.Equals("allocation") &&
                data.ContainsKey("resource_item_id")) {
                var row = ProcessBase.db.GetDataRow(
                    " SELECT resource_item_id, task_item_id FROM " + ProcessBase.tableName +
                    " WHERE item_id = @item_id",
                    ProcessBase.DBParameters);

                var p = new Processes(ProcessBase.instance, AuthenticatedSession);
                var oldProcess = p.GetProcessByItemId(Conv.ToInt(row["resource_item_id"]));
                var newProcess = p.GetProcessByItemId(Conv.ToInt(data["resource_item_id"]));

                //If process changes, add update for the process_type field
                if (oldProcess != newProcess && !sql.Contains("resource_type"))
                    sql = sql.Replace(" SET ",
                        " SET resource_type = '" + (oldProcess == "user" ? "competency" : "user") + "', ");
                
                var oldResourceId = Conv.ToInt(row["resource_item_id"]);
                var taskItemId = data.ContainsKey("task_item_id")
                    ? Conv.ToInt(data["task_item_id"])
                    : Conv.ToInt(row["task_item_id"]);

                //Update Task From Allocation 5 - After SaveItem
                if (taskItemId > 0) {
                    var a = new AllocationService(ProcessBase.instance, ProcessBase.authenticatedSession,
                        new ResourceManagementRepository(ProcessBase, null));

                    a.UpdateTaskFromAllocation(taskItemId, (int)itemId, Conv.ToInt(data["resource_item_id"]),
                        oldResourceId);
                    var resourceColumnId = ProcessBase.db.ExecuteScalar(
                        "SELECT TOP 1 item_column_id FROM item_columns WHERE instance = @instance AND process='task' AND name='responsibles'",
                        ProcessBase.DBParameters);

                    _privateCache.Remove(taskItemId);
                    Cache.Remove(
                        ProcessBase.instance,
                        "idps_" + taskItemId + " " + resourceColumnId);
                }
            }

            if (foundUpdateData) ProcessBase.db.ExecuteUpdateQuery(sql, ProcessBase.DBParameters);
            if (preventSpecialProcessChecks == false && Process.Equals("task")) {
                var tasks = new TaskData(ProcessBase.instance, ProcessBase.authenticatedSession);
                tasks.SaveTask(data, hadLinks);
            }

            if (preventSpecialProcessChecks == false && Process.Equals("user")) {
                if (data.ContainsKey("active") && Conv.ToInt(data["active"]) == 0) {
                    var user = new User(Instance, (int)itemId);
                    user.DeActivate(ProcessBase.authenticatedSession);
                }
            }

            if (Process.Equals("user")) Cache.Remove(ProcessBase.instance, "authentication", "item_id_" + itemId);

            //Force subquery cache update
            new SubqueryCacheUpdater().UpdateCache(ProcessBase.instance, Process, ProcessBase.authenticatedSession,
                (int)itemId);

            if (returnSubqueries) data = UpdateEquationAndSubQCols((int)itemId, eqColumns, subQueryCols, data);

            foreach (var c in colsToRemove) {
                data.Remove(c);
            }

            if (_privateCache.ContainsKey((int)itemId)) _privateCache.Remove((int)itemId);

            //If any of the columns has indexing enabled, update index
            if (luceneUpdate) {
                var lucene = new processes.Lucene(ProcessBase.instance, ProcessBase.authenticatedSession);
                lucene.UpdateIndex(ProcessBase.process, (int)itemId, GetItem((int)itemId, false, false));
            }

            //Invalidate Client Cache on every save
            if (!followsInsert) ProcessBase.InvalidateClientItemAndSqCache((int)itemId, null);

            return data;
        }

        public DataTable GetAllData() {
            return GetAllData("");
        }

        public DataTable GetAllData(string idStr = "", string order = "", string extraWhere = "") {
            var idParam = string.IsNullOrEmpty(idStr) ? "" : "WHERE item_id IN (" + idStr + ")";

            var whereParam = "";
            if (extraWhere.Length > 0) {
                whereParam = string.IsNullOrEmpty(idStr) ? " WHERE " + extraWhere : " AND " + extraWhere;
            }

            var orderParam = string.IsNullOrEmpty(order) ? "" : " ORDER BY " + ParseOrder(order);

            var db = ProcessBase.db;
            var result = db.GetDatatable(ItemColumns.GetItemSql() + idParam + whereParam + orderParam,
                ProcessBase.DBParameters);
            return result;
        }

        private string ParseOrder(string order) {
            string result = "";
            foreach (string col in order.Split(",")) {
                if (result.Length > 0) {
                    result += ", ";
                }

                result += ProcessBase.ValidateOrderColumn(col.Trim());
                if (col.Trim() == "order_no") {
                    result += " ";
                }

                result += " ASC";
            }

            return result;
        }

        public bool IsUniqueColumn(int columnId, string value) {
            var i = ItemColumns.GetItemColumn(columnId);
            return EntityRepository.GetCountOfIdenticalItems(i, 0, value) <= 0;
        }

        public bool IsUniqueColumn(int columnId, int itemId, string value) {
            var i = ItemColumns.GetItemColumn(columnId);
            return EntityRepository.GetCountOfIdenticalItems(i, itemId, value) <= 0;
        }

        private void DeleteFromDt(DataTable dt, int itemId, int itemColumnId, object data) {
            var idStr = "-1";
            if (data != null) {
                if (data is JArray) {
                    var ids = JsonConvert.DeserializeObject<List<int>>(data
                        .ToString());
                    foreach (var id in ids) {
                        idStr += "," + id;
                    }
                } else if (data is string) {
                    var str = Conv.ToStr(data);
                    if (str != "") {
                        var ids = str.Split(",");
                        foreach (var id in ids) {
                            if (id != "") idStr += "," + id;
                        }
                    }
                } else {
                    var ids = Conv.ToIntArray(data);
                    foreach (var id in ids) {
                        idStr += "," + id;
                    }
                }
            }

            foreach (var dr in dt.Select("item_id = " + itemId + " AND item_column_id = " + itemColumnId +
                                         " AND selected_item_id NOT IN (" + idStr + ")")) {
                dr.Delete();
            }
        }

        private void CheckItemBelongsToItsProcess(int item_id, string process) {
            if (process != null && Cache.GetItemProcess(item_id, ProcessBase.instance) != process)
                throw new CustomArgumentException("Item id " + item_id + " does not belong to process " + process);
        }

        private void AddToDt(DataTable dt, int itemId, ItemColumn itemColumn, int selectedItemId,
            int? linkItemId = null) {
            CheckItemBelongsToItsProcess(selectedItemId, itemColumn.DataAdditional);

            var foundRows = dt.Select("item_id = " + itemId + " AND item_column_id = " + itemColumn.ItemColumnId +
                                      " AND selected_item_id = " +
                                      selectedItemId);

            if (foundRows.Length == 0) {
                var n = dt.NewRow();
                n["item_id"] = itemId;
                n["item_column_id"] = itemColumn.ItemColumnId;
                n["selected_item_id"] = selectedItemId;
                if (linkItemId != null) {
                    n["link_item_id"] = linkItemId;
                }

                n["archive_userid"] = AuthenticatedSession.item_id;

                dt.Rows.Add(n);
            } else {
                dt.Rows.Remove(foundRows[0]);
            }
        }

        private Dictionary<string, object> UpdateEquationAndSubQCols(int itemId, List<ItemColumn> eqCols,
            List<ItemColumn> subQueryCols,
            Dictionary<string, object> data) {
            if (eqCols.Count > 0) {
                var evaluatedData = GetItem(itemId, false, false);
                foreach (var v in eqCols) {
                    data[v.Name] = evaluatedData[v.Name];
                }

                foreach (var v in subQueryCols) {
                    data[v.Name] = evaluatedData[v.Name];
                }
            } else {
                return UpdateSubQueryCols(itemId, subQueryCols, data);
            }

            return data;
        }

        public Dictionary<string, object> UpdateSubQueryCols(int? itemId, List<ItemColumn> cols,
            Dictionary<string, object> data) {
            //Getitem jos on equation kolumneja
            //lisätään tonne tähän tulevaan arrayhyn myös equationit


            var ics = new ItemColumns(ProcessBase.instance, ProcessBase.process,
                ProcessBase.authenticatedSession);
            foreach (var c in cols) {
                ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(itemId));
                var sql = "SELECT " + ics.GetSubQuery(c) + " FROM " + ProcessBase.tableName +
                          " pf WHERE item_id = @itemId";
                if (c.SubDataType == 6) {
                    //Transform integer values to array of integers for list models. 
                    var idS = new List<int>();
                    foreach (DataRow r in ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters).Rows) {
                        idS.Add(Conv.ToInt(r[c.Name + "_str"]));
                    }

                    data[c.Name] = idS;
                } else {
                    data[c.Name] = ProcessBase.db.ExecuteScalar(sql, ProcessBase.DBParameters);
                }
            }

            return data;
        }

        public string GetTableName() {
            return Conv.ToSql("_" + Instance + "_" + Process);
        }

        public List<string> GetUserCols(int itemId, int type, string[] statesList) {
            if (_privateCache2.ContainsKey(itemId + "_" + type)) return _privateCache2[itemId + "_" + type];

            var states = new States(Instance, AuthenticatedSession);
            var fStates = states.GetEvaluatedStates(Process, "meta", itemId, statesList.ToList());

            var userCols = new List<string> {
                "instance",
                "process",
                "item_id",
                "actual_item_id",
                "parent_item_id",
                "archive_userid",
                "archive_start",
                "states",
                "order_no",
                "RowNum",
                "bar_icons"
            };

            var pcc = new ProcesContainerColumns(ProcessBase.instance, Process, AuthenticatedSession);
            foreach (var state in statesList) {
                foreach (var cId in fStates.GetContainerIds(state)) {
                    foreach (var pcf in pcc.GetFields(cId)) {
                        if (!userCols.Contains(pcf.ItemColumn.Name)) userCols.Add(pcf.ItemColumn.Name);
                    }
                }
            }

            foreach (var col in ItemColumns.GetStatusColumns()) {
                if (!userCols.Contains(col.Name)) userCols.Add(col.Name);
            }

            _privateCache2.Add(itemId + "_" + type, userCols);

            return userCols;
        }
    }
}