﻿using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.database {
	public class ConnectionBaseProcess : ConnectionBase {
		private int? _isLinkProcess;

		private int? _isListProcess;
		//private string _tableName = "";

		public ConnectionBaseProcess(string instance, string process, AuthenticatedSession currentUserId) :
			base(instance, currentUserId) {
			this.process = process;

			var p = new SqlParameter();
			p.ParameterName = "@process";
			p.Value = process;

			SetDBParam(p);
			db = new Connections(currentUserId);
		}

		private string _process;

		public string process {
			get => Conv.ToSql(_process);
			protected set => _process = value;
		}

		public string tableName => Conv.ToSql("_" + instance + "_" + process);

		public bool isListProcess {
			get {
				if (_isListProcess == null) {
					if (Cache.Get(instance, "process_is_list_" + process) != null) {
						return Conv.ToBool(Cache.Get(instance, "process_is_list_" + process));
					}
					SetDBParam(SqlDbType.NVarChar, "@instance", instance);

					_isListProcess =
						Conv.ToInt(db.ExecuteScalar(
							"SELECT list_process FROM processes WHERE instance = @instance AND process = @process ",
							DBParameters));
					Cache.Set(instance, "process_is_list_" + process, _isListProcess);
					return Conv.ToBool(_isListProcess);
				}

				return Conv.ToBool(_isListProcess);
			}
		}

		public bool isLinkProcess {
			get {
				if (_isLinkProcess == null) {
					if (Cache.Get(instance, "process_is_link_" + process) != null) {
						return Conv.ToBool(Cache.Get(instance, "process_is_link_" + process));
					}

					_isLinkProcess =
						Conv.ToInt(db.ExecuteScalar(
							"SELECT process_type FROM processes WHERE instance = @instance AND process = @process ",
							DBParameters));
					if (_isLinkProcess == 4) {
						Cache.Set(instance, "process_is_link_" + process, true);
						return true;
					}

					Cache.Set(instance, "process_is_link_" + process, false);
					return false;
				}

				if (_isLinkProcess == 4) {
					Cache.Set(instance, "process_is_link_" + process, true);
					return true;
				}

				Cache.Set(instance, "process_is_link_" + process, false);
				return false;
			}
		}
	}
}