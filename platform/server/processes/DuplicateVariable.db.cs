﻿using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes {
	public class DuplicateVariable {
		public string Process { get; set; }
		public string OrderNo { get; set; }
	}

	public class DuplicateVariableCheck : ProcessBase {
		public DuplicateVariableCheck(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
			this.instance = instance;
		}

		public string CheckVariable(string type, string variable, string process) {
			var dt = new DataTable();
			SetDBParam(SqlDbType.NVarChar, "@variable", variable);
			SetDBParam(SqlDbType.NVarChar, "@process", Conv.ToStr(process));

			if (Conv.ToInt(db.ExecuteScalar("SELECT COUNT(*) FROM instance_keywords WHERE keyword = @variable", DBParameters)) == 1) {
				variable = variable + "_";
				SetDBParam(SqlDbType.NVarChar, "@variable", variable);
			}

			switch (type) {
				case "process":
					dt = db.GetDatatable("SELECT process FROM processes WHERE instance = @instance AND variable = @variable", DBParameters);
					break;
				case "itemColumn":
					dt = db.GetDatatable("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = @process AND name = @variable", DBParameters);
					break;
			}

			if (dt.Rows.Count > 0) return "";

			return variable;
		}
	}
}