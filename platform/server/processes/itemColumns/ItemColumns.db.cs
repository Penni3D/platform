using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Spreadsheet;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Lucene.Net.Util.Fst;
using Newtonsoft.Json;
using Connections = Keto5.x.platform.server.database.Connections;

namespace Keto5.x.platform.server.processes.itemColumns {
	public interface IItemColumns {
		ItemColumn GetItemColumn(int key);

		string GetItemColumnsForSql(string alias = null, bool includeItemId = true, int archiveType = 0,
			List<string> cols = null, bool onlyConditionColumns = false);

		List<ItemColumn> GetItemColumns(bool getSubqueries = true, bool onlyConditionColumns = false,
			bool onlyIntegrationRestColumns = false);

		ItemColumn GetItemColumnByName(string name, bool returnnullifnotfound = false);
		List<ItemColumn> GetStatusColumns();

		List<string> GetItemColumnListForSql(string alias = null, int archiveType = 0,
			bool onlyConditionColumns = false);

		string GetSubQuery(ItemColumn c, bool withAlias = true, bool clearCache = false, bool noCache = false,
			int archiveType = 0, bool omitCacheCaseClause = false);

		bool HasColumnName(string name);

		string GetItemSql(string alias = null, int archiveType = 0, int item_id = 0, List<string> cols = null,
			bool onlyConditionColumns = false);

		string GetEquationQuery(ItemColumn col, bool fromPortfolio = false);
	}

	public class ItemColumns : IItemColumns {
		private readonly string _instance;
		private readonly string _process;
		private readonly ProcessBase _processBase;
		private readonly AuthenticatedSession authenticatedSession;

		private DataTable _itemColumns;

		private DataTable itemColumns {
			get {
				_itemColumns = (DataTable)Cache.Get("", "item_columns_table");
				if (_itemColumns != null) return _itemColumns;

				_itemColumns = _processBase.db.GetDatatable(
					"SELECT * FROM item_columns");
				Cache.Set("", "item_columns_table", _itemColumns);
				return _itemColumns;
			}
		}

		private DataTable _itemColumnsWithContainers;

		private DataTable itemColumnsWithContainers {
			get {
				_itemColumnsWithContainers = (DataTable)Cache.Get("", "item_columns_and_containers_table");
				if (_itemColumnsWithContainers != null) return _itemColumnsWithContainers;

				_itemColumnsWithContainers = _processBase.db.GetDatatable(
					"SELECT * FROM item_columns ic LEFT JOIN process_container_columns pcc ON ic.item_column_id = pcc.item_column_id ORDER BY pcc.order_no  ASC");
				Cache.Set("", "item_columns_and_containers_table", _itemColumnsWithContainers);

				return _itemColumnsWithContainers;
			}
		}

		public ItemColumns(string instance, string process, AuthenticatedSession session) {
			_instance = instance;
			_process = process;
			_processBase = new ProcessBase(session, process);
			authenticatedSession = session;
			SetInstanceParameter();
		}

		public ItemColumns(ProcessBase processBase, string process) {
			_processBase = processBase;
			_process = process;
			_instance = processBase.instance;
			authenticatedSession = processBase.authenticatedSession;
			SetInstanceParameter();
		}

		private void ClearCache() {
			_itemColumns = null;
			_itemColumnsWithContainers = null;
			Cache.Remove("", "item_columns_table");
			Cache.Remove("", "item_columns_and_containers_table");
			Cache.Remove("", "item_containers_and_columns_table");
		}

		public List<ItemColumn> GetItemColumns(bool getSubqueries = true, bool onlyConditionColumns = false,
			bool onlyIntegrationRestColumns = false) {
			var result = new List<ItemColumn>();
			if (Cache.Get(_instance, "ics_" + _process) != null && getSubqueries && onlyConditionColumns == false &&
			    onlyIntegrationRestColumns == false)
				return (List<ItemColumn>)Cache.Get(_instance, "ics_" + _process);
			var f = new common.Util.Filter();
			f.Add("instance", _instance).Add("process", _process);
			if (getSubqueries == false) f.Add("data_type <> 16");
			if (onlyConditionColumns) f.Add("(in_conditions = 1 OR name LIKE '%_item_id%')");
			if (onlyIntegrationRestColumns)
				f.Add("data_type IN (0, 1, 2, 3, 4, 5, 6, 10, 13, 14, 15, 16, 19, 20, 23, 24)");

			foreach (DataRow row in itemColumns.Select(f.ToString())) {
				var itemColumn = new ItemColumn(row, authenticatedSession);
				AddEquations(itemColumn);
				result.Add(itemColumn);
			}

			if (getSubqueries && onlyConditionColumns == false && onlyIntegrationRestColumns == false)
				Cache.Set(_instance, "ics_" + _process, result);
			return result;
		}

		private void AddEquations(ItemColumn itemColumn) {
			if (!itemColumn.IsEquation()) return;
			_processBase.SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumn.ItemColumnId);
			itemColumn.Equations =
				_processBase.db.GetDatatableDictionary(
					"SELECT * FROM item_column_equations WHERE parent_item_column_id = @itemColumnId OR parent_item_column_id = (SELECT TOP 1 item_column_id FROM instance_union_columns WHERE instance_union_row_id = (SELECT instance_union_row_id from instance_union_rows WHERE item_column_id = @itemColumnId))",
					_processBase.DBParameters);
		}

		public List<string> GetItemColumnListForSql(string alias = null, int archiveType = 0,
			bool onlyConditionColumns = false) {
			var iCols = GetItemColumns(onlyConditionColumns: onlyConditionColumns);
			var retval = (
					from itemColumn in iCols
					where (itemColumn.IsDatabasePrimitive() || itemColumn.IsDateTime()) && itemColumn.Name != null &&
					      itemColumn.Name.Length > 0
					select itemColumn.Name)
				.ToList();

			retval.AddRange(
				from itemColumn in iCols
				where itemColumn.IsTranslation() && itemColumn.Name != null && itemColumn.Name.Length > 0
				select
					" ( SELECT translation FROM process_translations WHERE instance = @instance AND process=@process AND code = '" +
					authenticatedSession.locale_id + "' AND variable = " + itemColumn.Name + " ) as  " + itemColumn.Name
			);

			foreach (var c in iCols) {
				if (c.IsSubquery() && c.DataAdditional != null) {
					retval.Add(GetSubQuery(c, archiveType: archiveType));
				}

				if (c.Name != null &&
				    (c.IsAttachment() || c.IsLink() || c.IsPortfolio() || c.IsComment() || c.IsRm())) {
					retval.Add("null as " + c.Name);
				}
			}

			return retval;
		}

		public string GetSubQuery(ItemColumn c, bool withAlias = true, bool clearCache = false, bool noCache = false,
			int archiveType = 0, bool omitCacheCaseClause = false) {
			if (noCache) clearCache = true;

			var sql = "";

			var subQuery = RegisteredSubQueries.GetType(c.DataAdditional, c.ItemColumnId);
			if (subQuery == null || Cache.IsItemColumnSuspicious(c.ItemColumnId)) {
				if (withAlias) {
					if (c.IsOfSubType(ItemColumn.ColumnType.List) || c.IsOfSubType(ItemColumn.ColumnType.Process)) {
						return "'' " + c.Name + "_str";
					}

					return "null " + c.Name;
				}

				return "null ";
			}

			if (archiveType == 0 && Cache.Get(_instance, "subquery_" + c.ItemColumnId) != null && !clearCache) {
				sql = Conv.ToStr(Cache.Get(_instance, "subquery_" + c.ItemColumnId));
			} else if (archiveType == 2 && Cache.Get(_instance, "archive_subquery_" + c.ItemColumnId) != null &&
			           !clearCache) {
				sql = Conv.ToStr(Cache.Get(_instance, "archive_subquery_" + c.ItemColumnId));
			} else {
				try {
					var configs = subQuery.GetFieldConfigs();
					if (archiveType == 0) {
						sql = omitCacheCaseClause
							? subQuery.GetSql(_instance, c.ItemColumnId)
							: subQuery.GetQueryCachedSql(_instance, c.Name, c.ItemColumnId);
					} else if (archiveType == 1 || archiveType == 2) {
						sql = subQuery.GetArchiveSql(_instance);
					}

					if (subQuery.GetSemaphore().WaitOne(1000)) {
						foreach (var key in configs.Keys) {
							var x = (SubQueryConfigValue)configs[key];
							if (x.ValueType == "int_array") {
								var idStr = string.Join(",", Conv.ToIntArray(x.Value));
								sql = sql.Replace(key, idStr);
							} else {
								var val = Conv.ToStr(((SubQueryConfigValue)configs[key]).Value);
								sql = sql.Replace(key, val);
							}
						}

						subQuery.GetSemaphore().Release();
					}

					if (noCache == false && archiveType == 0) {
						Cache.Set(_instance, "subquery_" + c.ItemColumnId, sql);
					} else if (noCache == false && archiveType == 2) {
						Cache.Set(_instance, "archive_subquery_" + c.ItemColumnId, sql);
					}
				} catch (Exception ex) {
					sql = " null ";
					Console.WriteLine("Subquery (" + c.Name + " - " + c.ItemColumnId + ") caused an error: " +
					                  ex.Message);
				}
			}

			if (withAlias) {
				if (c.IsOfSubType(ItemColumn.ColumnType.List) || c.IsOfSubType(ItemColumn.ColumnType.Process)) {
					return sql + c.Name + "_str";
				}

				return sql + c.Name;
			}

			return sql;
		}

		public string GetListOrProcessIDPSSelectedSQL(ItemColumn iColumn) {
			// adapted PortfolioSqlService.cs AddColSql
			if (iColumn.IsProcess() || iColumn.IsList()) {
				var t = "item_data_process_selections";
				//if (archiveDate != null) t = "get_item_data_process_selections(@archiveDate)"; 
				if (iColumn.InputMethod == 3) {
					return
						"STUFF ((SELECT ','+cast(item_id as nvarchar) FROM " + t +
						" WHERE selected_item_id = pf.item_id  AND item_column_id = " +
						iColumn.RelativeColumnId + " FOR XML PATH('')) ,1,1,'') ";
				} else {
					return
						"STUFF ((SELECT ','+cast(selected_item_id as nvarchar) FROM " + t +
						" WHERE item_id = pf.item_id  AND item_column_id = " +
						iColumn.ItemColumnId + " FOR XML PATH('')) ,1,1,'') ";
				}
			}

			return "NULL ";
		}

		private bool CheckColumnExists(string name, List<string> cols) {
			name = name.Replace(")", " ").Split(' ').Last();
			return cols.Contains(name.TrimEnd("_str"));
		}

		public string GetItemColumnsForSql(string alias = null, bool includeItemId = true, int archiveType = 0,
			List<string> cols = null, bool onlyConditionColumns = false) {
			var result = alias != null ? alias + ".order_no" : "order_no";
			if (includeItemId) result += alias != null ? ", " + alias + ".item_id " : ", item_id ";

			foreach (var itemColumn in GetItemColumnListForSql(archiveType: archiveType,
				         onlyConditionColumns: onlyConditionColumns)) {
				if (cols != null && !CheckColumnExists(itemColumn, cols)) continue;
				if (includeItemId == false && itemColumn == "parent_item_id") continue;
				result += alias != null ? alias + "." + itemColumn + "," : ", " + itemColumn;
			}

			return result;
		}

		public string GetItemSql(string alias = null, int archiveType = 0, int item_id = 0, List<string> cols = null,
			bool onlyConditionColumns = false) {
			var hasEquationCols = false;

			var sql = "SELECT [instanceandprocess] " + GetItemColumnsForSql(alias, archiveType: archiveType, cols: cols,
				onlyConditionColumns: onlyConditionColumns);


			var colStr = "";
			if (!sql.ToLower().Contains(", instance")) colStr += "'" + Conv.ToSql(_instance) + "' as instance,";
			if (!sql.ToLower().Contains(", process")) colStr += "'" + Conv.ToSql(_process) + "' as process,";
			sql = sql.Replace("[instanceandprocess]", colStr);

			switch (archiveType) {
				default:
					sql += ", pf.archive_start, pf.archive_userid FROM " + _processBase.tableName;
					break;
				case 1:
					sql += ", archive_start, archive_end, archive_userid, archive_id FROM archive_" +
					       _processBase.tableName;
					break;
				case 2:
					if (item_id > 0 && _processBase.tableName.Left(_instance.Length + 2) == "_" + _instance + '_') {
						sql += ", archive_start, archive_userid FROM get_" + _processBase.tableName +
						       "_with_id(@itemId, @archiveDate) ";
					} else {
						sql += ", archive_start, archive_userid FROM get_" + _processBase.tableName + "(@archiveDate) ";
					}

					break;
			}

			sql += " pf ";


			if (sql.Contains("_" + _instance + "_allocation") && (sql.Contains("system_ad.hours") || sql.Contains("system_ad.date")))
				sql += " LEFT JOIN allocation_durations system_ad ON system_ad.allocation_item_id = pf.item_id ";

			var equationSql = "SELECT * ";

			foreach (var col in GetItemColumns(onlyConditionColumns: onlyConditionColumns)) {
				if (col.IsEquation() && onlyConditionColumns) continue;
				if ((!col.IsEquation() || cols != null) && (!col.IsEquation() || cols == null || !cols.Contains(col.Name))) continue;
				
				//if this operation is wanted to all processes, remove this if
				if (col.Process != "user") {
					equationSql += "," + GetEquationQuery(col) + " as " + col.Name;
					hasEquationCols = true;
				} else {
					try {
						var pp = new ProcessBase(_instance, col.Process, authenticatedSession);
						var asda = "SELECT " + GetEquationQuery(col) + " as " + col.Name + " FROM ( " + sql +
						           " ) pf ";

						pp.db.GetDatatable(asda);
						equationSql += "," + GetEquationQuery(col) + " as " + col.Name;
						hasEquationCols = true;
					} catch (Exception e) {
						Console.WriteLine("Error in equation " + e);
					}
				}
			}

			if (!hasEquationCols) return sql;
			equationSql += " FROM ( " + sql + " ) pf ";
			return equationSql;
		}

		public List<ItemColumn> GetStatusColumns() {
			var result = new List<ItemColumn>();

			var f = new common.Util.Filter();
			f.Add("instance", _instance).Add("process", _process);
			f.Add("status_column", "1");

			foreach (DataRow row in itemColumns.Select(f.ToString())) {
				var itemColumn = new ItemColumn(row, authenticatedSession);
				AddEquations(itemColumn);
				result.Add(itemColumn);
			}

			return result;
		}

		public ItemColumn GetItemColumn(int itemColumnId) {
			if (Cache.Get(_instance, "itemcolumn_" + itemColumnId) != null) {
				return (ItemColumn)Cache.Get(_instance, "itemcolumn_" + itemColumnId);
			}

			var f = new common.Util.Filter();
			f.Add("item_column_id", itemColumnId);

			var dt = itemColumns.Select(f.ToString());

			if (dt.Length > 0) {
				var itemColumn = new ItemColumn(dt[0], authenticatedSession);
				AddEquations(itemColumn);

				Cache.Set(_instance, "itemcolumn_" + itemColumnId, itemColumn);
				return itemColumn;
			}

			throw new CustomArgumentException("Item Column cannot be found using item column id " + itemColumnId);
		}

		public ItemColumn GetItemColumnByName(string name, bool returnnullifnotfound = false) {
			var f = new common.Util.Filter();
			f.Add("instance", _instance).Add("process", _process).Add("name", name);

			var dt = itemColumns.Select(f.ToString());

			if (dt.Length > 0) {
				var itemColumn = new ItemColumn(dt[0], authenticatedSession);
				AddEquations(itemColumn);

				return itemColumn;
			}

			if (returnnullifnotfound) return null;
			throw new CustomArgumentException("Item Column cannot be found using name '" + name + "'");
		}

		public bool HasColumnName(string name) {
			var f = new common.Util.Filter();
			f.Add("instance", _instance).Add("process", _process).Add("name", name);
			return itemColumns.Select(f.ToString()).Length > 0;
		}

		public string GetEquationQuery(ItemColumn c, bool fromPortfolio = false) {
			var sql = " null ";
			var listOfChars = new List<char> { '+', '-', '*', '/', ' ', '(', ')' };
			if (c.IsEquation()) {
				if (c.DataAdditional == null) {
					return sql;
				}

				sql = "";

				var equations = new Dictionary<string, Dictionary<string, object>>();
				foreach (var e in c.Equations) {
					equations.Add(Conv.ToStr(e["sql_alias"]), e);
				}

				var tag = "";
				foreach (var ch in c.DataAdditional) {
					if (ch >= 65 && ch <= 90) {
						tag += ch;
					}

					if (listOfChars.Contains(ch)) {
						if (tag.Length > 0) {
							if (equations.ContainsKey(tag)) {
								sql += AddCastToEquation(c, GetSqlForEquation(equations[tag], equations));
							}

							tag = "";
						}

						sql += ch;
					}
				}

				if (tag.Length > 0) {
					if (equations.ContainsKey(tag)) {
						sql += AddCastToEquation(c, GetSqlForEquation(equations[tag], equations));
					}
				}
			}


			if (!fromPortfolio) return sql;

			//Commented as AimAndritz has a process eq using an int column for value
			//if (!(sql.TrimEnd().EndsWith(")") || sql.TrimEnd().ToLower().EndsWith("end") || sql.TrimEnd().EndsWith("_str"))) return sql += "_str";

			return sql;
		}

		private void SetInstanceParameter() {
			_processBase.SetDBParameter("@instance", _instance);
		}

		public List<ItemColumn> GetAllInstanceColumns() {
			var result = new List<ItemColumn>();

			var f = new common.Util.Filter().Add("instance", _instance);

			foreach (DataRow row in itemColumns.Select(f.ToString())) {
				var itemColumn = new ItemColumn(row, authenticatedSession);
				AddEquations(itemColumn);

				result.Add(itemColumn);
			}

			return result;
		}

		public Dictionary<string, ItemColumn> GetItemColumnsDictionary() {
			var retval = new Dictionary<string, ItemColumn>();
			foreach (var col in GetItemColumns()) {
				retval.Add(col.Name, col);
			}

			return retval;
		}

		public Dictionary<int, ItemColumn> GetItemColumnsIntDictionary() {
			var retval = new Dictionary<int, ItemColumn>();
			foreach (var col in GetItemColumns()) {
				retval.Add(col.ItemColumnId, col);
			}

			return retval;
		}

		private DataTable _containerColumnsCache = null;

		private DataTable GetContainerColumns =>
			_containerColumnsCache ??= _processBase.db.GetDatatable(
				"SELECT process_container_id, item_column_id FROM process_container_columns");

		public List<ItemColumn> GetItemColumns(int containerId) {
			var result = new List<ItemColumn>();
			DataRow[] dt;
			var f = new common.Util.Filter();

			if (containerId > 0) {
				f.Add("instance", _instance).Add("process", _process).Add("process_container_id", containerId);
				dt = itemColumnsWithContainers.Select(f.ToString());
			} else {
				f.Add("instance", _instance).Add("process", _process);
				dt = itemColumnsWithContainers.Select(f + " AND process_container_column_id IS NULL");
			}

			foreach (DataRow row in dt) {
				var itemColumn = new ItemColumn(row, authenticatedSession);
				foreach (DataRow row2 in GetContainerColumns.Select("item_column_id = " + itemColumn.ItemColumnId)) {
					itemColumn.ProcessContainerIds.Add((int)row2["process_container_id"]);
				}

				AddEquations(itemColumn);
				result.Add(itemColumn);
			}

			return result;
		}

		public Dictionary<int, List<ItemColumn>> GetItemColumnsInContainers() {
			var pc = new ProcessContainers(_instance, _process, authenticatedSession);
			var retval = new Dictionary<int, List<ItemColumn>>();
			retval.Add(0, GetItemColumns(0));
			foreach (var containerId in pc.GetContainerIds()) {
				retval.Add(containerId, GetItemColumns((int)containerId));
			}

			return retval;
		}

		private object isNull(object value) {
			return value ?? DBNull.Value;
		}

		public ItemColumn Add(string process, ItemColumn itemColumn, bool insertTranslation = true,
			bool isListColumn = false) {
			_processBase.SetDBParam(SqlDbType.Int, "@dataType", itemColumn.DataType);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@systemColumn", itemColumn.SystemColumn);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@checkConditions", itemColumn.CheckConditions);
			_processBase.SetDBParam(SqlDbType.NVarChar, "@dataAdditional", isNull(itemColumn.DataAdditional));
			_processBase.SetDBParam(SqlDbType.TinyInt, "@useInPortfolio", itemColumn.UseInPortfolio);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@useinSelect", itemColumn.UseInSelect);
			_processBase.SetDBParam(SqlDbType.NVarChar, "@dataAdditional2", isNull(itemColumn.DataAdditional2));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@inputMethod", itemColumn.InputMethod);
			_processBase.SetDBParam(SqlDbType.NVarChar, "@inputName", isNull(itemColumn.InputName));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@relativeColumnId", isNull(itemColumn.RelativeColumnId));
			_processBase.SetDBParam(SqlDbType.Int, "@processDialogId", isNull(itemColumn.ProcessDialogId));
			_processBase.SetDBParam(SqlDbType.Int, "@processActionId", isNull(itemColumn.ProcessActionId));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@linkProcess", isNull(itemColumn.LinkProcess));
			_processBase.SetDBParam(SqlDbType.TinyInt, "@statusColumn", itemColumn.StatusColumn);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@useFk", itemColumn.UseFk);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@uniqueCol", itemColumn.UniqueCol);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@parentSelectType", itemColumn.ParentSelectType);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@useLucene", itemColumn.UseLucene);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@inConditions", itemColumn.InConditions);
			if (itemColumn.Validation != null) {
				var validation = JsonConvert.SerializeObject(itemColumn.Validation);
				_processBase.SetDBParam(SqlDbType.NVarChar, "@validation", validation);
			} else {
				_processBase.SetDBParam(SqlDbType.NVarChar, "@validation", DBNull.Value);
			}

			if (itemColumn.LanguageTranslationRaw[authenticatedSession.locale_id] != null) {
				var itemColumnName = "";

				_processBase.SetDBParam(SqlDbType.NVarChar, "@keyword", Conv.ToStr(itemColumn.Name));
				if (Conv.ToInt(_processBase.db.ExecuteScalar(
					    "SELECT COUNT(*) FROM instance_keywords WHERE keyword = @keyword",
					    _processBase.DBParameters)) ==
				    1) {
					itemColumn.Name = itemColumn.Name + "_";
				}

				_processBase.SetDBParam(SqlDbType.NVarChar, "@keyword", Conv.ToStr(itemColumn.Variable));
				if (Conv.ToInt(_processBase.db.ExecuteScalar(
					    "SELECT COUNT(*) FROM instance_keywords WHERE keyword = @keyword",
					    _processBase.DBParameters)) ==
				    1) {
					itemColumn.Variable = itemColumn.Variable + "_";
				}

				itemColumnName = Modi.RemoveSpecialCharacters(itemColumn.Variable ?? itemColumn.Name);
				if (Regex.IsMatch(itemColumnName, @"^\d+")) itemColumnName = "c" + itemColumnName;
				itemColumnName = Modi.Left(itemColumnName, 50).ToLower().Replace(' ', '_');
				_processBase.SetDBParam(SqlDbType.NVarChar, "@name",
					itemColumnName.Length > 50 ? Modi.Left(itemColumnName, 50) : itemColumnName);
				_processBase.SetDBParam(SqlDbType.NVarChar, "@variable",
					_processBase.translations.GetTranslationVariable(process,
						itemColumn.LanguageTranslationRaw[authenticatedSession.locale_id], "IC"));
				itemColumn.ItemColumnId = _processBase.db.ExecuteInsertQuery(
					"INSERT INTO item_columns (instance, process, name, variable, data_type, system_column,data_additional, data_additional2, input_method, input_name, status_column, check_conditions, relative_column_id, process_dialog_id, process_action_id, link_process, use_lucene, use_fk, unique_col, parent_select_type, in_conditions, ic_validation) VALUES(@instance, @process, @name, @variable, @dataType, @systemColumn,@dataAdditional, @dataAdditional2, @inputMethod, @inputName, @statusColumn, @checkConditions, @relativeColumnId, @processDialogId, @processActionId, @linkProcess, @useLucene, @useFk, @uniqueCol, @parentSelectType, @inConditions, @validation)",
					_processBase.DBParameters);
				_processBase.SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumn.ItemColumnId);
				var oldVariable = _processBase.translations.GetTranslationVariable(process,
					itemColumn.LanguageTranslationRaw[authenticatedSession.locale_id], "IC");
				foreach (var translation in itemColumn.LanguageTranslationRaw) {
					_processBase.translations.insertOrUpdateProcessTranslation(process, oldVariable, translation.Value,
						translation.Key);
				}

				Cache.Remove(_instance, "ics_" + process);
				if (itemColumn.ProcessContainerIds != null) {
					foreach (var containerId in itemColumn.ProcessContainerIds) {
						if (containerId <= 0) continue;
						_processBase.SetDBParam(SqlDbType.Int, "@containerId", containerId);
						_processBase.SetDBParam(SqlDbType.NVarChar, "@order_no", itemColumn.OrderNo);
						_processBase.db.ExecuteInsertQuery(
							"INSERT INTO process_container_columns (process_container_id, item_column_id, placeholder, validation, order_no) VALUES(@containerId, @itemColumnId, '', NULL, @order_no)",
							_processBase.DBParameters);
					}
				}

				itemColumn.Name = itemColumnName;
			}

			if (itemColumn.IsSubquery() && itemColumn.DataAdditional != null) {
				var subQuery = RegisteredSubQueries.GetType(itemColumn.DataAdditional, itemColumn.ItemColumnId);
				if (subQuery != null) {
					subQuery.SetInstance(_instance);
					subQuery.SetProcess(_process);
					subQuery.SetSession(authenticatedSession);
					subQuery.SetFieldConfigs(itemColumn.DataAdditional2);
					GetSubQuery(itemColumn, clearCache: true);
				}
			}

			if (itemColumn.Validation == null) {
				itemColumn.Validation = new FieldValidation();
			} else {
				itemColumn.Validation = itemColumn.Validation;
			}

			ClearCache();
			return itemColumn;
		}

		public int AddNewItemColumn(string name, bool systemColumn, string variable, byte inputMethod,
			string inputName, int dataType) {
			var trans = new Dictionary<string, string>();

			var x = new Translations(_instance, authenticatedSession);

			trans.Add(authenticatedSession.locale_id, x.GetTranslation(variable));
			var c = new ItemColumn();
			c.Variable = variable;
			c.Process = _process;
			c.Name = name;
			c.SystemColumn = true;
			c.LanguageTranslationRaw = trans;
			c.LanguageTranslation = trans;
			c.InputMethod = inputMethod;
			c.InputName = inputName;
			c.DataType = dataType;
			return Add(_process, c, false, true).ItemColumnId;
		}

		public void Delete(string process, int itemColumnId) {
			_processBase.SetDBParam(SqlDbType.Int, "@id", itemColumnId);
			_processBase.db.ExecuteDeleteQuery(
				"DELETE FROM item_columns WHERE instance=@instance AND process=@process AND item_column_id=@id",
				_processBase.DBParameters);
			Cache.Remove(_instance, "itemcolumn_" + itemColumnId);
			Cache.Remove(_instance, "ics_" + process);
			Cache.Remove(_instance, "subquery_" + itemColumnId);
			ClearCache();
		}

		public ItemColumn Save(ItemColumn itemColumn) {
			_processBase.SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumn.ItemColumnId);
			_processBase.SetDBParam(SqlDbType.Int, "@systemColumn", itemColumn.SystemColumn);
			_processBase.SetDBParam(SqlDbType.Int, "@checkConditions", itemColumn.CheckConditions);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@dataType", itemColumn.DataType);
			_processBase.db.ExecuteStoredProcedure("EXEC items_ChangeColumnType @itemColumnId, -1, @dataType ",
				_processBase.DBParameters);
			var oldAdditional = Conv.ToStr(_processBase.db.ExecuteScalar(
				"SELECT data_additional FROM item_columns WHERE item_column_id = @itemColumnId",
				_processBase.DBParameters));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@dataAdditional",
				Conv.IfNullThenDbNull(itemColumn.DataAdditional));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@dataAdditional2",
				Conv.IfNullThenDbNull(itemColumn.DataAdditional2));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@inputMethod", Conv.IfNullThenDbNull(itemColumn.InputMethod));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@inputName", Conv.IfNullThenDbNull(itemColumn.InputName));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@relativeColumnId",
				Conv.IfNullThenDbNull(itemColumn.RelativeColumnId));
			_processBase.SetDBParam(SqlDbType.Int, "@processDialogId",
				Conv.IfNullThenDbNull(itemColumn.ProcessDialogId));
			_processBase.SetDBParam(SqlDbType.Int, "@processActionId",
				Conv.IfNullThenDbNull(itemColumn.ProcessActionId));
			_processBase.SetDBParam(SqlDbType.NVarChar, "@linkProcess", Conv.IfNullThenDbNull(itemColumn.LinkProcess));

			_processBase.SetDBParam(SqlDbType.Int, "@statusColumn", itemColumn.StatusColumn);
			_processBase.SetDBParam(SqlDbType.Int, "@useFk", itemColumn.UseFk);
			_processBase.SetDBParam(SqlDbType.Int, "@uniqueCol", itemColumn.UniqueCol);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@parentSelectType", itemColumn.ParentSelectType);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@useLucene", itemColumn.UseLucene);
			_processBase.SetDBParam(SqlDbType.TinyInt, "@inConditions", itemColumn.InConditions);

			if (itemColumn.Validation != null) {
				var validation = JsonConvert.SerializeObject(itemColumn.Validation);
				_processBase.SetDBParam(SqlDbType.NVarChar, "@validation", validation);
			} else {
				_processBase.SetDBParam(SqlDbType.NVarChar, "@validation", DBNull.Value);
			}

			_processBase.SetDBParam(SqlDbType.NVarChar, "@da2a", Conv.IfNullThenDbNull(itemColumn.SubDataAdditional2));

			_processBase.db.ExecuteUpdateQuery(
				"UPDATE item_columns SET data_type = @dataType, system_column = @systemColumn, data_additional = @dataAdditional, data_additional2 = @dataAdditional2, input_method = @inputMethod, input_name = @inputName, status_column = @statusColumn, check_conditions=@checkConditions, relative_column_id = @relativeColumnId, process_dialog_id = @processDialogId, process_action_id = @processActionId, link_process=@linkProcess, use_lucene = @useLucene, use_fk = @useFk, unique_col = @uniqueCol, parent_select_type = @parentSelectType, ic_validation = @validation, in_conditions = @inConditions, sub_data_additional2 = @da2a WHERE instance = @instance AND process = @process AND item_column_id = @itemColumnId",
				_processBase.DBParameters);
			var oldVar = _processBase.db.ExecuteScalar(
				"SELECT variable FROM item_columns WHERE instance = @instance AND process = @process AND item_column_id = @itemColumnId",
				_processBase.DBParameters);

			if (itemColumn.LanguageTranslationRaw != null &&
			    itemColumn.LanguageTranslationRaw.ContainsKey(authenticatedSession.locale_id) == false) {
				itemColumn.LanguageTranslationRaw.Add(authenticatedSession.locale_id, null);
			}

			if (oldVar != null && oldVar != DBNull.Value &&
			    itemColumn.LanguageTranslationRaw[authenticatedSession.locale_id] != null) {
				foreach (var translation in itemColumn.LanguageTranslationRaw) {
					_processBase.translations.insertOrUpdateProcessTranslation(_process, (string)oldVar,
						translation.Value,
						translation.Key);
				}
			} else if (oldVar == DBNull.Value &&
			           itemColumn.LanguageTranslationRaw[authenticatedSession.locale_id] != null) {
				var itemColumnName =
					Modi.RemoveSpecialCharacters(itemColumn.LanguageTranslationRaw[authenticatedSession.locale_id]);
				if (Regex.IsMatch(itemColumnName, @"^\d+")) {
					itemColumnName = "c" + itemColumnName;
				}

				itemColumnName = Modi.Left(itemColumnName, 50).ToLower().Replace(' ', '_');
				if (itemColumnName.Length > 50) {
					_processBase.SetDBParam(SqlDbType.NVarChar, "@name", Modi.Left(itemColumnName, 50));
				} else {
					_processBase.SetDBParam(SqlDbType.NVarChar, "@name", itemColumnName);
				}

				var oldVariable = _processBase.translations.GetTranslationVariable(_process,
					itemColumn.LanguageTranslationRaw[authenticatedSession.locale_id], "IC");
				_processBase.SetDBParam(SqlDbType.NVarChar, "@variable", oldVariable);
				_processBase.db.ExecuteUpdateQuery(
					"UPDATE item_columns SET name = @name, variable = @variable WHERE instance = @instance AND process = @process AND item_column_id = @itemColumnId",
					_processBase.DBParameters);

				foreach (var translation in itemColumn.LanguageTranslationRaw) {
					_processBase.translations.insertOrUpdateProcessTranslation(_process, oldVariable, translation.Value,
						translation.Key);
				}
			}

			if (itemColumn.DataAdditional != null && oldAdditional != null &&
			    !itemColumn.DataAdditional.Equals(oldAdditional)) {
				_processBase.db.ExecuteDeleteQuery(
					"DELETE FROM item_data_process_selections WHERE item_column_id = @itemColumnId",
					_processBase.DBParameters);
			}

			if (itemColumn.IsSubquery() && itemColumn.DataAdditional != null) {
				RegisteredSubQueries.RemoveType(itemColumn.ItemColumnId);
				var subQuery = RegisteredSubQueries.GetType(itemColumn.DataAdditional, itemColumn.ItemColumnId);
				if (subQuery != null) {
					subQuery.SetInstance(_instance);
					subQuery.SetProcess(_process);
					subQuery.SetSession(authenticatedSession);
					subQuery.SetFieldConfigs(itemColumn.DataAdditional2);
					GetSubQuery(itemColumn, clearCache: true);
				}
			}

			Cache.Remove(_instance, "itemcolumn_" + itemColumn.ItemColumnId);
			Cache.Remove(_instance, "ics_" + _process);
			Cache.Remove(_instance, "subquery_" + itemColumn.ItemColumnId);
			ClearCache();
			return itemColumn;
		}

		private string GetSqlForEquation(Dictionary<string, object> equation,
			Dictionary<string, Dictionary<string, object>> equations) {
			var sql = "";

			if (Conv.ToInt(equation["column_type"]) == 0) {
				var iColName = "";
				var pNum = 1;
				if (!DBNull.Value.Equals(equation["item_column_id"])) {
					var this_item_column = GetItemColumn(Conv.ToInt(equation["item_column_id"]));
					if (this_item_column.DataType == 20) // if equation
						iColName = "(" + GetEquationQuery(this_item_column) + ")";
					else {
						var col = GetItemColumn(Conv.ToInt(equation["item_column_id"]));
						iColName = col.Name;
						if (col.DataType == 16) {
							// subquery type
							var sq = RegisteredSubQueries.GetType(col.DataAdditional, col.ItemColumnId);
							if (sq != null) iColName = sq.HasCache() ? col.Name : GetSubQuery(col, false);
						} else if (col.DataType == 6 || col.DataType == 5) {
							// list or process type, are not found in pf columns so get the sql
							iColName = GetListOrProcessIDPSSelectedSQL(col); // how to get archive support?
						}
					}
				} else {
					var tag = Conv.ToStr(equation["sql_param1"]);
					if (equations.ContainsKey(tag)) {
						iColName = GetSqlForEquation(equations[tag], equations);
						pNum = 2;
					}
				}

				switch (Conv.ToInt(equation["sql_function"])) {
					case 0:
						sql += iColName;
						break;
					case 1:
						sql += "LEFT(" + iColName + ", " + ParamToEquation(equation["sql_param" + pNum], equations) +
						       ")";
						break;
					case 2:
						sql += "RIGHT(" + iColName + ", " + ParamToEquation(equation["sql_param" + pNum], equations) +
						       ")";
						break;
					case 3:
						sql += "SUBSTRING(" + iColName + ", " +
						       ParamToEquation(equation["sql_param" + pNum], equations) + ", " +
						       ParamToEquation(equation["sql_param" + (pNum + 1)], equations) + ")";
						break;
					case 4:
						sql += "LEN(" + iColName + ")";
						break;
					case 5:
						var caseSql = "";
						var tag = "";
						foreach (var ch in Conv.ToStr(equation["sql_param1"])) {
							if (ch >= 65 && ch <= 90) {
								tag += ch;
							}

							var listOfChars = new List<char> {
								'+', '-', '*', '/', ' ', '>', '=', '<', '!', '1', '2', '3', '4', '5', '6', '7', '8',
								'9', '0', '(', ')', '&', '|', '.'
							};

							if (listOfChars.Contains(ch)) {
								caseSql += Conv.ToStr(ch).Replace("&", " AND ").Replace("|", " OR ");
								if (tag.Length > 0) {
									if (equations.ContainsKey(tag)) {
										caseSql += GetSqlForEquation(equations[tag], equations);
									}

									tag = "";
								}
							}
						}

						if (equations.ContainsKey(tag)) {
							caseSql += GetSqlForEquation(equations[tag], equations);
						}

						sql += "CASE WHEN " + caseSql +
						       " THEN " + ParamToEquation(equation["sql_param2"], equations);

						var else_sql = ParamToEquation(equation["sql_param3"], equations);
						if (else_sql.Length >= 9 && else_sql.Substring(0, 9) == "CASE WHEN") {
							sql += else_sql.Substring(4);
						} else {
							sql += " ELSE " + else_sql + " END";
						}

						break;
					case 6:
						sql += "NULLIF(" + iColName + ", " + ParamToEquation(equation["sql_param" + pNum], equations) +
						       ")";
						break;
					case 7: // ISNULL
						sql += "COALESCE(" + iColName + ", " +
						       ParamToEquation(equation["sql_param" + pNum], equations) + ")";
						break;
					case 8:
						string compare_mode_8 = "day";
						if (new string[] {
							    "year", "yyyy", "yy", "quarter", "qq", "q", "month", "mm", "m", "dayofyear", "day", "dy",
							    "y", "week", "ww", "wk", "weekday", "dw", "w", "hour", "hh", "minute", "mi", "n", "second",
							    "ss", "s", "millisecond", "ms"
						    }.Contains(equation["sql_param" + (pNum + 1)].ToString().ToLower()))
							compare_mode_8 = equation["sql_param" + (pNum + 1)].ToString().ToLower();

						string compare_to = "";
						if (Conv.ToStr(Conv.ToInt(equation["sql_param" + pNum])) ==
						    Conv.ToStr(equation["sql_param" + pNum])) // if column id
							compare_to = GetItemColumn(Conv.ToInt(equation["sql_param" + pNum])).Name;
						else
							compare_to = ParamToEquation(equation["sql_param" + pNum], equations); // if alias
						sql += " DATEDIFF(" + compare_mode_8 + ", " + iColName + ", " + compare_to + ") ";
						break;
					case 9:
						string compare_mode_9 = "day";
						if (new string[] {
							    "year", "yyyy", "yy", "quarter", "qq", "q", "month", "mm", "m", "dayofyear", "day", "dy",
							    "y", "week", "ww", "wk", "weekday", "dw", "w", "hour", "hh", "minute", "mi", "n", "second",
							    "ss", "s", "millisecond", "ms"
						    }.Contains(equation["sql_param" + pNum].ToString().ToLower()))
							compare_mode_9 = equation["sql_param" + pNum].ToString().ToLower();
						sql += " DATEDIFF(" + compare_mode_9 + ", " + iColName +
						       ", getdate() ) "; // should this be getutcdate()?
						break;
					case 10: // CASE_WHEN_LIST
						if (true) {
							var idsArr =
								JsonConvert.DeserializeObject<List<int>>(
									Conv.ToStr(equation["sql_param1"]));

							var caseSql2 = "";
							var this_item_column = GetItemColumn(Conv.ToInt(equation["item_column_id"]));

							if (this_item_column.DataType == 16) {
								// subquery, and already restricted to lists

								string columns_subquery = GetSubQuery(this_item_column, false);
								string subqueryval = columns_subquery;
								var sq = RegisteredSubQueries.GetType(this_item_column.DataAdditional, this_item_column.ItemColumnId);
								if (sq != null && sq.HasCache() == true)
									subqueryval = this_item_column.Name + "_str"; // why does it need _str?
								foreach (int arr_id in idsArr)
									if (columns_subquery.Contains("STUFF"))
										caseSql2 += (caseSql2.Length > 0 ? " OR " : "") + "(',' + REPLACE(" + subqueryval + ",' ','') + ',' LIKE '%," + arr_id +
										            ",%')"; // compare to STUFFed result
									else
										caseSql2 += (caseSql2.Length > 0 ? " OR " : "") + "(" + arr_id + " IN (" + subqueryval + "))";
								if (caseSql2.Length == 0)
									caseSql2 = "'a'='b'";
							} else if (this_item_column.DataType == 20) {
								// equation, hopefully list.. otherwise bad config
								foreach (int arr_id in idsArr)
									if (iColName.Contains("STUFF"))
										caseSql2 += (caseSql2.Length > 0 ? " OR " : "") + "(',' + REPLACE(" + iColName + ",' ','') + ',' LIKE '%," + arr_id + ",%')";
									else
										caseSql2 += (caseSql2.Length > 0 ? " OR " : "") + "(" + arr_id + " IN (" + iColName + "))";
								if (caseSql2.Length == 0)
									caseSql2 = "'a'='b'";
							} else {
								var idStr = string.Join(",", idsArr.ToArray());
								if (idStr.Length == 0)
									idStr = "-1";
								caseSql2 =
									" ( SELECT COUNT(selected_item_id) FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id  AND idps.item_column_id = " +
									Conv.ToInt(equation["item_column_id"]) + " AND selected_item_id IN ( " + idStr +
									") ) > 0 "; // how to get/use @archiveDate?
							}

							var caseSql3 = "CASE WHEN (" + caseSql2 + ")" +
							               " THEN " + ParamToEquation(equation["sql_param2"], equations);

							var else_caseSql3 = ParamToEquation(equation["sql_param3"], equations);
							if (else_caseSql3.Length >= 9 && else_caseSql3.Substring(0, 9) == "CASE WHEN") {
								caseSql3 += else_caseSql3.Substring(4);
							} else {
								caseSql3 += " ELSE " + else_caseSql3 + " END";
							}

							sql += caseSql3;
						}

						break;
					case 11: // DATEADD
						var date_add_mode = "day";
						if (new string[] {
							    "year", "yyyy", "yy", "quarter", "qq", "q", "month", "mm", "m", "dayofyear", "day", "dy",
							    "y", "week", "ww", "wk", "weekday", "dw", "w", "hour", "hh", "minute", "mi", "n", "second",
							    "ss", "s", "millisecond", "ms"
						    }.Contains(equation["sql_param" + pNum].ToString().ToLower()))
							date_add_mode = equation["sql_param" + pNum].ToString().ToLower();

						sql +=
							"DATEADD(" +
							date_add_mode +
							"," + ParamToEquation(equation["sql_param" + (pNum + 1)], equations) +
							"," + iColName +
							")";
						break;
					case 12: // ROUND
						sql += "ROUND(" + iColName + "," + ParamToEquation(equation["sql_param" + pNum], equations) +
						       ")";
						break;
					case 13: //SEQ_CONCAT_PROCESS
						sql +=
							"NULLIF(STUFF((SELECT DISTINCT ', ' + RTRIM(LTRIM(Value)) FROM STRING_SPLIT(CONCAT_WS(','," +
							iColName + "," + ParamToEquation(equation["sql_param" + pNum], equations) +
							"),',') FOR XML PATH('')), 1,1,''),'')";
						break;
				}
			} else if (Conv.ToInt(equation["column_type"]) == 1) {
				// SEQ_DIRECT_VALUE
				if (Double.TryParse(equation["sql_param1"].ToString(),
					    System.Globalization.NumberStyles.AllowDecimalPoint,
					    System.Globalization.CultureInfo.InvariantCulture, out _)
				   ) // InvariantCulture uses . as a decimal separator, as does SQL
					sql += Conv.Escape(equation["sql_param1"].ToString());
				else
					sql += "'" + Conv.Escape(equation["sql_param1"].ToString()) + "'";
			} else if (Conv.ToInt(equation["column_type"]) == 2) {
				// for some legacy operations
				switch (Conv.ToInt(equation["sql_function"])) {
					case 7:
						sql += "COALESCE(" + ParamToEquation(equation["sql_param1"], equations) + ", " +
						       ParamToEquation(equation["sql_param2"], equations) +
						       ")";
						break;
				}
			} else if (Conv.ToInt(equation["column_type"]) == 3) {
				// SEQ_LIST_VALUE
				sql += "'" + Conv.ToSql(Conv.ToStr(equation["sql_param2"]).Replace("[", "").Replace("]", "")) + "'";
			} else if (Conv.ToInt(equation["column_type"]) == 4) {
				// SEQ_EVALUATION_VALUE
				sql += ParamToEquation(
					"[ " + Conv.ToStr(equation["sql_param1"]).Trim().Replace("[", "").Replace("]", "") + " ]",
					equations);
			}

			return sql;
		}

		private string AddCastToEquation(ItemColumn col, string sql) {
			switch (col.SubDataType) {
				case 0:
					return "CAST( " + sql + " AS " + " NVARCHAR(MAX)) ";
				case 1:
					return "CAST( " + sql + " AS " + " INT) ";
				case 2:
					return "CAST( " + sql + " AS " + " FLOAT) ";
				case 3:
					return "CAST( " + sql + " AS " + " DATE) ";
				default:
					return sql;
			}
		}

		public string ParamToEquation(object param, Dictionary<string, Dictionary<string, object>> equations) {
			var c = Conv.ToStr(param);

			if (equations.ContainsKey(c)) return GetSqlForEquation(equations[c], equations);

			var result = c;
			if (c.StartsWith("[") && c.EndsWith("]")) {
				result = "";
				foreach (var cha in c) {
					var ch = Conv.ToStr(cha);
					if (ch == "[" || ch == "]") continue;

					if (equations.ContainsKey(ch)) {
						result += GetSqlForEquation(equations[ch], equations);
					} else {
						result += ch;
					}
				}
			}

			return result;
		}
	}
}