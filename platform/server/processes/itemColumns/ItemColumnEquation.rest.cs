﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.itemColumns {
	[Route("v1/settings/ItemColumnEquations")]
	public class ItemColumnEquationsRest : PageBase {
		public ItemColumnEquationsRest() : base("settings") { }

		[HttpGet("{process}/{itemColumnId}")]
		public List<ItemColumnEquation> Get(string process, int itemColumnId) {
			var e = new ItemColumnEquations(instance, process, currentSession);
			return e.GetItemColumnEquations(itemColumnId);
		}

		[HttpPost("testEquation/{process}/{itemColumnId}")]
		public bool TestEquation(string process, int itemColumnId, [FromBody]Dictionary<string, string> equation) {
			var e = new ItemColumnEquations(instance, process, currentSession);
			return e.TestEquation(itemColumnId, equation["e"]);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]ItemColumnEquation equation) {
			CheckRight("write");
			var ee = new ItemColumnEquations(instance, process, currentSession);
			ee.Add(equation);
			return new ObjectResult(equation);
		}


		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]List<ItemColumnEquation> equations) {
			CheckRight("write");
			var ee = new ItemColumnEquations(instance, process, currentSession);

			foreach (var e in equations) {
				ee.Save(e);
			}
			return new ObjectResult(equations);

		}

		[HttpDelete("{process}/{itemColumnEquationsIds}")]
		public IActionResult Delete(string process, string itemColumnEquationsIds) {
			CheckRight("delete");
			var e = new ItemColumnEquations(instance, process, currentSession);
			foreach (var id in itemColumnEquationsIds.Split(',')) {
				e.Delete(Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}
}