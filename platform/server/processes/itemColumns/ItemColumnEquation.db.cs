﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.processes.itemColumns {
	public class ItemColumnEquation {
		public ItemColumnEquation() { }

		public ItemColumnEquation(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ItemColumnEquationId = (int) dr[alias + "item_column_equation_id"];
			ParentItemColumnId = (int) dr[alias + "parent_item_column_id"];
			SqlAlias = (string) Conv.IfDbNullThenNull(dr[alias + "sql_alias"]);
			ColumnType = Conv.ToByte(dr[alias + "column_type"]);
			ItemColumnId = (int?) Conv.IfDbNullThenNull(dr[alias + "item_column_id"]);
			SqlFunction = (int) dr[alias + "sql_function"];
			SqlParam1 = (string) Conv.IfDbNullThenNull(dr[alias + "sql_param1"]);
			SqlParam2 = (string) Conv.IfDbNullThenNull(dr[alias + "sql_param2"]);
			SqlParam3 = (string) Conv.IfDbNullThenNull(dr[alias + "sql_param3"]);
		}

		public int ItemColumnEquationId { get; set; }
		public int ParentItemColumnId { get; set; }
		public byte ColumnType { get; set; }
		public int? ItemColumnId { get; set; }
		public string SqlAlias { get; set; }
		public int SqlFunction { get; set; }
		public string SqlParam1 { get; set; }
		public string SqlParam2 { get; set; }
		public string SqlParam3 { get; set; }
	}

	public class ItemColumnEquations : ProcessBase {
		public ItemColumnEquations(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance, process, authenticatedSession) { }

		public List<ItemColumnEquation> GetItemColumnEquations(int itemColumnId) {
			var result = new List<ItemColumnEquation>();
			//Get Column Names
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT * FROM item_column_equations WHERE parent_item_column_id = @itemColumnId ORDER BY case when LEN(sql_alias) = 1 THEN '0' ELSE '1' END + sql_alias ",
					DBParameters).Rows) {
				var col = new ItemColumnEquation(row, authenticatedSession);
				result.Add(col);
			}

			return result;
		}

		public void Delete(int itemColumnEquationId) {
			SetDBParam(SqlDbType.Int, "@itemColumnEquationId", itemColumnEquationId);
			db.ExecuteDeleteQuery(
				"DELETE FROM item_column_equations WHERE item_column_equation_id = @itemColumnEquationId",
				DBParameters);
		}

		public void Add(ItemColumnEquation itemColumnEquation) {
			SetDBParam(SqlDbType.Int, "@parentItemColumnId",
				Conv.IfNullThenDbNull(itemColumnEquation.ParentItemColumnId));
			var colCount =
				(int) db.ExecuteScalar(
					"SELECT COALESCE(MAX( CASE WHEN LEN(sql_alias) = 1 THEN ascii(sql_alias) -64 " +
					" ELSE ( (ascii(SUBSTRING(sql_alias, 1, 1)) - 64) * 25  ) + ascii(SUBSTRING(sql_alias, 2, 1)) - 65  END ), 0)  " +
					"FROM item_column_equations WHERE parent_item_column_id = @parentItemColumnId ",
					DBParameters) + 1;

			var alias = "";
			if (colCount < 26) {
				alias = "" + (char) (colCount + 64);
			} else {
				alias = "" + (char) (colCount / 25 + 64);
				alias += "" + (char) (colCount % 25 + 65);
			}

			itemColumnEquation.SqlAlias = alias;
			SetDBParam(SqlDbType.NVarChar, "@alias", alias);
			itemColumnEquation.ItemColumnEquationId = db.ExecuteInsertQuery(
				"INSERT INTO item_column_equations (parent_item_column_id, sql_alias) VALUES (@parentItemColumnId, @alias);",
				DBParameters);
		}

		public void Save(ItemColumnEquation itemColumnEquation) {
			SetDBParam(SqlDbType.Int, "@itemColumnEquationId", itemColumnEquation.ItemColumnEquationId);
			SetDBParam(SqlDbType.Int, "@columnType", itemColumnEquation.ColumnType);
			SetDBParam(SqlDbType.Int, "@itemColumnId", Conv.IfNullThenDbNull(itemColumnEquation.ItemColumnId));
			SetDBParam(SqlDbType.Int, "@sqlFunction", itemColumnEquation.SqlFunction);
			SetDBParam(SqlDbType.NVarChar, "@sqlParam1", Conv.IfNullThenDbNull(itemColumnEquation.SqlParam1));
			SetDBParam(SqlDbType.NVarChar, "@sqlParam2", Conv.IfNullThenDbNull(itemColumnEquation.SqlParam2));
			SetDBParam(SqlDbType.NVarChar, "@sqlParam3", Conv.IfNullThenDbNull(itemColumnEquation.SqlParam3));

			db.ExecuteUpdateQuery(
				"UPDATE item_column_equations SET column_type = @columnType, item_column_id = @itemColumnId, " +
				"sql_function = @sqlFunction, sql_param1 = @sqlParam1, sql_param2 = @sqlParam2, sql_param3 = @sqlParam3 " +
				" WHERE item_column_equation_id = @itemColumnEquationId", DBParameters);
		}

		public bool TestEquation(int itemColumnId, string equation) {
			var ics = new ItemColumns(instance, process, authenticatedSession);
			var c = ics.GetItemColumn(itemColumnId);
			c.DataAdditional = equation;

			var sql = "SELECT '" + instance + " ' as instance, '" + process + "' as process, " +
			          ics.GetItemColumnsForSql() + " FROM _" + instance + "_" + process + " pf ";
			var equationSql = "SELECT * ";

			var eSql = ics.GetEquationQuery(c);

			equationSql += "," + eSql + " as " + c.Name;

			equationSql += " FROM ( " + sql + " ) pf ";

			try {
				db.GetDatatable(equationSql, DBParameters);
				return true;
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
				return false;
			}
		}

		public bool TestSubqueryAndEnsureCache(int itemColumnId, string name, SubQueryConfigBase sq,
			Dictionary<string, object> configs,
			bool testFetch = false) {
			var result = "";
			var sql = "";
			var where = "WHERE 1=2";
			if (testFetch) where = "";
			
			try {
				if (sq.RequireValidation == false || process.Contains("union_process_")) return true;
				
				sql = "SELECT TOP 1 " + sq.GetSql(instance, itemColumnId) + " FROM _" + instance + "_" + process +
				      " pf " + where + " ORDER BY newid()";

				foreach (var key in configs.Keys) {
					var x = (SubQueryConfigValue) configs[key];
					if (x.ValueType == "int_array") {
						var idStr = string.Join(",", Conv.ToIntArray(x.Value));
						sql = sql.Replace(key, idStr);
					} else {
						var val = Conv.ToStr(((SubQueryConfigValue) configs[key]).Value);
						sql = sql.Replace(key, val);
					}
				}
				
				var testdb = new Connections(true);
				testdb.GetDatatable(sql, DBParameters);

				//Ensure cache column is created / dropped
				var p = new List<SqlParameter> {
					new("@tableName", SqlDbType.NVarChar) {Value = "_" + instance + "_" + process},
					new("@columnName", SqlDbType.NVarChar) {Value = name},
					new("@enabled", SqlDbType.TinyInt) {Value = sq.HasCache() ? 1 : 0},
					new("@dataType", SqlDbType.Int) {Value = (int) sq.GetConfig().DataType}
				};

				testdb.ExecuteStoredProcedure(
					"EXEC app_ensure_cache_column @tableName, @columnName, @enabled, @dataType", p);
				
				result = "";
			} catch (Exception e) {
				Console.WriteLine(e.Message + " " + e.StackTrace);
				Diag.SupressException(e);
				result = e.Message;
			}

			Cache.Remove(instance, "CachedValidationQueries");
			if (result == "") { 
				Cache.RemoveSuspiciousItemColumn(itemColumnId);
				return true;
			} else { 
				Cache.AddSuspiciousItemColumn(itemColumnId, result);
				return false;
			}
		}
	}
}