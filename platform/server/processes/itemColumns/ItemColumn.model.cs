﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.itemColumns {
	public class ItemColumn {
		public enum ColumnType {
			Nvarchar = 0,
			Int = 1,
			Float = 2,
			Date = 3,
			Text = 4,
			Process = 5,
			List = 6,
			Password = 7,
			Button = 8,
			Rating = 9,
			Attachment = 10,
			Join = 11,
			Comment = 12,
			Datetime = 13,
			Formula = 14,
			Link = 15,
			Subquery = 16,
			Description = 17,
			Portfolio = 18,
			Tag = 19,
			Equation = 20,
			Rm = 21,
			Matrix = 22,
			Rich = 23,
			Translation = 24,
			Chart = 25,
			Progress = 60,
			Milestone = 65
		}

		public ItemColumn() { }

		public ItemColumn(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ItemColumnId = (int)dr["item_column_id"];
			Process = (string)dr["process"];
			DataType = (byte)dr["data_type"];
			SystemColumn = Convert.ToBoolean(dr["system_column"]);
			StatusColumn = Convert.ToBoolean(dr["status_column"]);
			CheckConditions = Conv.ToInt(dr["check_conditions"]);
			ProcessContainerIds = new List<int>();
			ParentSelectType = (byte)dr["parent_select_type"];

			if (!DBNull.Value.Equals(dr["name"])) {
				Name = (string)dr["name"];
			}

			if (!DBNull.Value.Equals(dr["variable"])) {
				Variable = (string)dr["variable"];
				LanguageTranslationRaw = new Translations(session.instance, session).GetTranslations(Variable);
				LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable, true);
			}

			if (IsTag()) {
				DataAdditional = "instance_tags";
			} else if (!DBNull.Value.Equals(dr["data_additional"])) {
				DataAdditional = (string)dr["data_additional"];
			}

			if (!DBNull.Value.Equals(dr["data_additional2"])) {
				DataAdditional2 = (string)dr["data_additional2"];
			}

			if (!DBNull.Value.Equals(dr["input_method"])) {
				InputMethod = (byte)dr["input_method"];
			}

			if (!DBNull.Value.Equals(dr["input_name"])) {
				InputName = (string)dr["input_name"];
			}

			if (!DBNull.Value.Equals(dr["relative_column_id"])) {
				RelativeColumnId = (int)dr["relative_column_id"];
			}

			if (!DBNull.Value.Equals(dr["process_dialog_id"])) {
				ProcessDialogId = (int)dr["process_dialog_id"];
			}

			if (!DBNull.Value.Equals(dr["process_action_id"])) {
				ProcessActionId = (int)dr["process_action_id"];
			}

			if (!DBNull.Value.Equals(dr["link_process"])) {
				LinkProcess = (string)dr["link_process"];
			}

			UseFk = Convert.ToInt32(dr["use_fk"]);
			UniqueCol = Convert.ToInt32(dr["unique_col"]);
			if (DataType == 16 && DataAdditional != null) {
				var subQuery = RegisteredSubQueries.GetType(DataAdditional, ItemColumnId);
				if (subQuery != null) {
					SubDataType = (int)subQuery.GetConfig().DataType;
					SubDataAdditional = subQuery.GetSubProcess();
				} else {
					SubDataType = 0;
				}
			}

			if (DataType == 20 && (Conv.ToInt(DataAdditional2) != 5 || Conv.ToInt(DataAdditional2) != 6)) {
				SubDataType = Conv.ToInt(DataAdditional2);
				SubDataAdditional = InputName;
			}

			if (IsEquation()) {
				SubDataType = Conv.ToInt(DataAdditional2);
			}

			if (!DBNull.Value.Equals(dr["ic_validation"])) {
				Validation =
					JsonConvert.DeserializeObject<FieldValidation>(Conv.ToStr(dr["ic_validation"]));
			} else {
				Validation = new FieldValidation();
			}

			UseLucene = Convert.ToBoolean(dr["use_lucene"]);
			InConditions = Convert.ToBoolean(dr["in_conditions"]);
			SubDataAdditional2 = Conv.ToStr(dr["sub_data_additional2"]);
		}

		public int ItemColumnId { get; set; }
		public string Process { get; set; }
		public string Name { get; set; }
		public string Variable { get; set; }
		public int DataType { get; set; }
		public bool SystemColumn { get; set; }
		public string DataAdditional { get; set; }
		public bool UseInPortfolio { get; set; }
		public bool UseInSelect { get; set; }
		public string DataAdditional2 { get; set; }
		public byte InputMethod { get; set; }
		public string InputName { get; set; }
		public bool StatusColumn { get; set; }
		public int CheckConditions { get; set; }
		public string OrderNo { get; set; }
		public ItemDataProcessSelection ItemDataProcessSelection { get; set; }
		public List<ItemDataProcessSelection> ItemDataProcessSelections { get; set; }
		public List<int> ProcessContainerIds { get; set; }
		public int? RelativeColumnId { get; set; }
		public int? ProcessDialogId { get; set; }
		public int? ProcessActionId { get; set; }
		public string LinkProcess { get; set; }
		public int UseFk { get; set; }
		public int UniqueCol { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public int SubDataType { get; set; }
		public string SubDataAdditional { get; set; }
		public List<Dictionary<string, object>> Equations { get; set; }
		public byte ParentSelectType { get; set; }
		public bool UseLucene { get; set; }
		public bool InConditions { get; set; }
		public FieldValidation Validation { get; set; }

		public string SubDataAdditional2 { get; set; }


		public Dictionary<string, string> LanguageTranslationRaw { get; set; }

		public bool IsOneOfTypes(params ColumnType[] columnTypes) {
			return columnTypes.Any(columnType => IsOfType(columnType));
		}

		public bool IsOfType(ColumnType columnType) {
			return DataType == (int)columnType;
		}

		public bool IsOfSubType(ColumnType columnType) {
			return SubDataType == (int)columnType;
		}

		public bool IsDatabasePrimitive() {
			return IsOneOfTypes(ColumnType.Nvarchar, ColumnType.Int, ColumnType.Float, ColumnType.Date,
				ColumnType.Text, ColumnType.Datetime, ColumnType.Rich);
		}

		public bool IsNumber() {
			return IsOneOfTypes(ColumnType.Int, ColumnType.Float);
		}

		public bool IsNvarChar() {
			return IsOfType(ColumnType.Nvarchar);
		}

		public bool IsInt() {
			return IsOfType(ColumnType.Int);
		}

		public bool IsFloat() {
			return IsOfType(ColumnType.Float);
		}

		public bool IsDate() {
			return IsOfType(ColumnType.Date);
		}

		public bool IsText() {
			return IsOfType(ColumnType.Text);
		}

		public bool IsProcess() {
			return IsOfType(ColumnType.Process);
		}

		public bool IsList() {
			return IsOfType(ColumnType.List);
		}

		public bool IsPassword() {
			return IsOfType(ColumnType.Password);
		}

		public bool IsButton() {
			return IsOfType(ColumnType.Button);
		}

		public bool IsAttachment() {
			return IsOfType(ColumnType.Attachment);
		}

		public bool IsJoin() {
			return IsOfType(ColumnType.Join);
		}

		public bool IsComment() {
			return IsOfType(ColumnType.Comment);
		}

		public bool IsDateTime() {
			return IsOfType(ColumnType.Datetime);
		}

		public bool IsFormula() {
			return IsOfType(ColumnType.Formula);
		}

		public bool IsLink() {
			return IsOfType(ColumnType.Link);
		}

		public bool IsSubquery() {
			return IsOfType(ColumnType.Subquery);
		}

		public bool IsDescription() {
			return IsOfType(ColumnType.Description);
		}

		public bool IsPortfolio() {
			return IsOfType(ColumnType.Portfolio);
		}

		public bool IsTag() {
			return IsOfType(ColumnType.Tag);
		}

		public bool IsRating() {
			return IsOfType(ColumnType.Rating);
		}

		public bool IsEquation() {
			return IsOfType(ColumnType.Equation);
		}

		public bool IsRm() {
			return IsOfType(ColumnType.Rm);
		}

		public bool IsMilestone() {
			return IsOfType(ColumnType.Milestone);
		}

		public bool IsMatrix() {
			return IsOfType(ColumnType.Matrix);
		}

		public bool IsRich() {
			return IsOfType(ColumnType.Rich);
		}

		public bool IsTranslation() {
			return IsOfType(ColumnType.Translation);
		}

		public bool IsChart() {
			return IsOfType(ColumnType.Chart);
		}

		public string GetColumnQuery(string instance) {
			if (IsSubquery()) {
				var t = RegisteredSubQueries.GetType(DataAdditional, ItemColumnId);
				if (t is null)
					throw new CustomArgumentException(
						"Subquery column " + Name + ", Item Column Id " + ItemColumnId + " could not be found.",
						"GENERIC_ERROR");
				return t.GetQueryCachedSql(instance, Name);
			}

			return Name;
		}

		private object ExctractAndFormatFieldFromDictionary(Dictionary<string, object> d1) {
			if (!d1.ContainsKey(Name))
				throw new CustomArgumentException("Column " + Name +
				                                  " does not have 'Use In Conditions' checkbox checked.");

			if (IsNvarChar() || IsText() || IsProcess()) return Conv.ToStr(d1[Name]).Replace("'", "\\'");
			if (IsInt()) return Conv.ToInt(d1[Name]);
			if (IsSubquery()) {
				if (IsOfSubType(ColumnType.Nvarchar) || IsOfSubType(ColumnType.Text)) {
					//This value is trimmed because Subquery returns strings with a space in the beginning
					return Conv.ToStr(d1[Name]).TrimStart();
				}
				if (IsOfSubType(ColumnType.Int) || IsOfSubType(ColumnType.Float)) return Conv.ToInt(d1[Name]);
			}

			if (IsFloat()) return Conv.ToDouble(d1[Name]);
			if (!IsDate()) return Conv.ToStr(d1[Name]);
			if (!DBNull.Value.Equals(d1[Name])) return DateTime.Parse("" + d1[Name]);
			return null;
		}

		public IEvaluator CreateComparison(Dictionary<string, object> d1, object condValue, ComparisonType compType) {
			var newLeft = ExctractAndFormatFieldFromDictionary(d1);

			var ic1 = this;

			if (ic1.IsInt() ||
			    (ic1.IsSubquery() && (ic1.IsOfSubType(ColumnType.Int) || ic1.IsOfSubType(ColumnType.Float)))) {
				var newRight = Conv.ToInt(condValue);
				return new ComparisonEquation(newLeft, newRight, compType.Type);
			}

			if (ic1.IsDate()) {
				if (!DBNull.Value.Equals(d1[ic1.Name]) && condValue != null) {
					var newRight = DateTime.Parse("" + condValue);
					return new ComparisonEquation(newLeft, newRight, compType.Type);
				}

				return new BooleanEquation(false);
			}

			if (ic1.IsFloat()) {
				var commasReplaced = Conv.ToStr(condValue);
				commasReplaced =
					commasReplaced.Replace(",",
						"."); // todo: floats are stored with comma in json. This is a little peey solution but Allu approved
				var newRight = Conv.ToDouble(commasReplaced);
				return new ComparisonEquation(newLeft, newRight, compType.Type);
			}

			if (ic1.IsSubquery() && (ic1.IsOfSubType(ColumnType.List) ||
			                         ic1.IsOfSubType(ColumnType.Process))) {
				int[] idList;
				if (d1.ContainsKey(ic1.Name)) {
					idList = Conv.ToIntArray(d1[ic1.Name]);
				} else {
					idList = Conv.ToIntArray(d1[ic1.Name + "_str"]);
				}

				var exp = new LogicalSolver().AddExpression(
					new BooleanEquation(idList.Contains(Conv.ToInt(condValue))));
				if (compType.Type == ComparisonTypes.Types.NOT_EQUALS) {
					exp.Negate();
				}

				return exp;
			}

			var right = Conv.ToStr(condValue).Replace("'", "\\'");
			return new ComparisonEquation(newLeft, right, compType.Type, false);
		}

		public bool IsSaveableColumn() {
			if (IsDatabasePrimitive() || IsOneOfTypes(ColumnType.Process, ColumnType.List, ColumnType.Password,
				    ColumnType.Comment, ColumnType.Formula)) {
				return true;
			}

			return false;
		}
	}
}