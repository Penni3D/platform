﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.itemColumns {
	[Route("v1/meta/ItemColumns")]
	public class ItemColumnsRest : PageBase {
		public ItemColumnsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ItemColumn> Get(string process) {
			var itemColumns = new ItemColumns(instance, process, currentSession);
			return itemColumns.GetItemColumns();
		}

		[HttpGet("{process}/{itemColumnId}")]
		public ItemColumn Get(string process, int itemColumnId) {
			var itemColumns = new ItemColumns(instance, process, currentSession);
			return itemColumns.GetItemColumn(itemColumnId);
		}

		[HttpGet("{process}/name/{columnName}")]
		public ItemColumn GetByName(string process, string columnName) {
			var itemColumns = new ItemColumns(instance, process, currentSession);
			return itemColumns.GetItemColumnByName(columnName);
		}

		[HttpGet]
		public List<ItemColumn> Get() {
			var itemColumns = new ItemColumns(instance, process, currentSession);
			return itemColumns.GetAllInstanceColumns();
		}

		[HttpGet("{process}/lookup_connectors")]

		public List<LookupConnectingColumn> GetLookupConnectors(string process) {
			var processBase = new ProcessBase(currentSession, process);
			var result = new List<LookupConnectingColumn>();

			var spSql =
				"SELECT item_column_id, coalesce(pt2.translation, ic.process) + ' : ' + coalesce(pt.translation, ic.name) AS result FROM item_columns ic left join process_translations pt2 ON ic.process = pt2.variable and pt2.code = '" + currentSession.locale_id +"' left join process_translations pt ON ic.variable = pt.variable and pt.code = '" +
				currentSession.locale_id + "' WHERE ((ic.process = '" + Conv.ToSql(process) +
				"' OR data_additional = '" + Conv.ToSql(process) +
				"' ) AND (data_type =5 OR data_type = 6)) OR ( data_type IN (0,1) AND name like '%item_id%' )  ";

			foreach (DataRow r in processBase.db.GetDatatable(spSql, processBase.DBParameters).Rows) {
				var d = new LookupConnectingColumn();
				d.Name = Conv.ToStr(r["result"]);
				d.ItemColumnId = Conv.ToInt(r["item_column_id"]);
				result.Add(d);
			}
			return result;

		}

		public class LookupConnectingColumn
		{
			public string Name { get; set; }
			public int ItemColumnId { get; set; }
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]ItemColumn itemColumn) {
			CheckRight("write");
			var itemColumns = new ItemColumns(instance, process, currentSession);
			if (ModelState.IsValid) {
				itemColumns.Add(process, itemColumn);
				return new ObjectResult(itemColumn);
			}
			throw new CustomArgumentException("Parameters are invalid");
		}
		[HttpPost("{process}/many")]
		public IActionResult Post(string process, [FromBody]List<ItemColumn> ics) {
			CheckRight("write");
			var itemColumns = new ItemColumns(instance, process, currentSession);
			if (ModelState.IsValid) {

				foreach (ItemColumn i in ics) {
					itemColumns.Add(process, i);
				}


				return new ObjectResult(ics);
			}
			throw new CustomArgumentException("Parameters are invalid");
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]List<ItemColumn> itemColumn) {
			CheckRight("write");
			var itemColumns = new ItemColumns(instance, process, currentSession);
			foreach (var ic in itemColumn) {
				itemColumns.Save(ic);
			}
			Cache.Remove("", "process_portfolio_columns_table");
			return new ObjectResult(itemColumn);
		}

		[HttpDelete("{process}/{itemColumnId}")]
		public IActionResult Delete(string process, string itemColumnId) {
			CheckRight("delete");
			var itemColumns = new ItemColumns(instance, process, currentSession);
			foreach (var id in itemColumnId.Split(',')) {
				itemColumns.Delete(process, Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}

	[Route("v1/settings/[controller]")]
	public class ItemColumnsInContainers : PageBase {
		public ItemColumnsInContainers() : base("settings") { }

		[HttpGet("{process}")]
		public Dictionary<int, List<ItemColumn>> Get(string process) {
			var itemColumns = new ItemColumns(instance, process, currentSession);
			return itemColumns.GetItemColumnsInContainers();
		}

		[HttpGet("{process}/{containerId}")]
		public List<ItemColumn> Get(string process, int containerId) {
			var itemColumns = new ItemColumns(instance, process, currentSession);
			return itemColumns.GetItemColumns(containerId);
		}

		[HttpDelete("{process}/{itemColumnId}")]
		public IActionResult Delete(string process, int itemColumnId) {
			CheckRight("delete");
			var itemColumns = new ItemColumns(instance, process, currentSession);
			itemColumns.Delete(process, itemColumnId);
			return new StatusCodeResult(200);
		}

		[HttpDelete("{process}/{itemColumnIds}/columns")]
		public IActionResult Delete(string process, string itemColumnIds) {
			CheckRight("delete");
			var itemColumns = new ItemColumns(instance, process, currentSession);
			foreach (var id in itemColumnIds.Split(',')) {
				itemColumns.Delete(process, Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}
}