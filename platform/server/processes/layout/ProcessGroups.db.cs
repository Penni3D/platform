﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessGroup {
		public ProcessGroup() {

		}

		public ProcessGroup(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			ProcessGroupId = (int)dr[alias + "process_group_id"];
			Variable = (string)dr[alias + "variable"];
			MaintenanceName = Conv.ToStr(dr[alias + "maintenance_name"]);
			OrderNo = (string)dr[alias + "order_no"];
			Type = (int)dr[alias + "type"];
			TabCount = (int)dr[alias + "tab_count"];
			
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
			
			if (dr[alias + "short_variable"] != DBNull.Value) { ShortTranslation = new Translations(session.instance, session).GetTranslation((string)dr[alias + "short_variable"]); }
		}

		public int ProcessGroupId { get; set; }
		public string Variable { get; set; }
		public string ShortTranslation { get; set; }
		public string MaintenanceName { get; set; }
		public string OrderNo { get; set; }
		public int Type { get; set; }
		public int TabCount { get; set; }
		public List<ProcessGroupFeature> SelectedFeatures { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
	}
	public class ProcessGroupFeature {
		public ProcessGroupFeature() {
		}

		public ProcessGroupFeature(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			Feature = (string)dr[alias + "feature"];
			DefaultState = (string)dr[alias + "state"];
		//	LanguageTranslation = new Translations(session.instance, session).GetTranslations((string)dr["custom_name"]);

			if (dr["custom_name"] != DBNull.Value) { CustomName = new Translations(session.instance, session).GetTranslation((string)dr["custom_name"]); }
		}

		public string Feature { get; set; }

		public string DefaultState { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }

		public string CustomName { get; set; }

	}

	public class ProcessGroups : ProcessBase {
		public ProcessGroups(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
			//this.instance = instance;
			//this.process = process;
		}

		public List<ProcessGroup> GetGroups() {
			if (Cache.Get(instance, "groups_" + process) != null) {
				return (List<ProcessGroup>)Cache.Get(instance, "groups_" + process);
			}

			var result = new List<ProcessGroup>();
			foreach (DataRow row in db.GetDatatable("SELECT process_group_id, variable, short_variable, order_no, type, 0 AS tab_count, maintenance_name FROM process_groups WHERE instance = @instance AND process = @process ORDER BY order_no  ASC", DBParameters).Rows) {
				var group = new ProcessGroup(row, authenticatedSession);
				SetDBParam(SqlDbType.Int, "@processGroupId", group.ProcessGroupId);
				var dt2 = db.GetDatatable("SELECT feature, state, custom_name FROM process_group_features WHERE process_group_id = @processGroupId", DBParameters);
				group.SelectedFeatures = new List<ProcessGroupFeature>();
				foreach (DataRow dr in dt2.Rows) {
					group.SelectedFeatures.Add(new ProcessGroupFeature(dr, authenticatedSession));
				}
				result.Add(group);
			}
			Cache.Set(instance, "groups_" + process, result);

			return result;
		}

		public List<int> GetGroupIds() {
			if (Cache.Get(instance, "group_ids_" + process) != null) {
				return (List<int>)Cache.Get(instance, "group_ids_" + process);
			}
			var result = new List<int>();
			foreach (DataRow row in db.GetDatatable("SELECT process_group_id FROM process_groups WHERE instance = @instance AND process = @process ORDER BY order_no  ASC", DBParameters).Rows) {
				result.Add((int)row["process_group_id"]);
			}
			Cache.Set(instance, "group_ids_" + process, result);
			return result;
		}

		public List<ProcessGroup> GetGroupsWithCounts() {
			var result = new List<ProcessGroup>();
			//Get Column Names
			foreach (DataRow row in db.GetDatatable("SELECT process_group_id, variable, short_variable, order_no, type, (SELECT COUNT(process_group_tab_id), maintenance_name  FROM process_group_tabs WHERE process_group_id = pg.process_group_id) AS tab_count FROM process_groups pg WHERE instance = @instance AND process = @process ORDER BY order_no  ASC", DBParameters).Rows) {
				var group = new ProcessGroup(row, authenticatedSession);
				result.Add(group);
			}
			var row_ = db.GetDataRow("SELECT 0 AS process_group_id, 'ungrouped' AS variable, '9999' AS order_no, 0 AS type, (SELECT COUNT(process_tab_id) FROM process_tabs pt WHERE instance = @instance AND process = @process AND process_tab_id NOT IN (SELECT process_tab_id FROM process_group_tabs)) AS tab_count ", DBParameters);
			var group_ = new ProcessGroup(row_, authenticatedSession);
			result.Add(group_);

			return result;
		}


		public ProcessGroup GetGroup(int processGroupId) {
			SetDBParam(SqlDbType.Int, "processGroupId", processGroupId);
			var dt = db.GetDatatable("SELECT process_group_id, variable, short_variable, order_no, type, 0 AS tab_count, maintenance_name  FROM process_groups WHERE instance = @instance AND process = @process AND process_group_id = @processGroupId ORDER BY order_no  ASC", DBParameters);
			if (dt.Rows.Count > 0) {
				var group = new ProcessGroup(dt.Rows[0], authenticatedSession);
				var dt2 = db.GetDatatable("SELECT feature, state, custom_name FROM process_group_features WHERE process_group_id = @processGroupId", DBParameters);
				group.SelectedFeatures = new List<ProcessGroupFeature>();
				foreach (DataRow dr in dt2.Rows) {
					group.SelectedFeatures.Add(new ProcessGroupFeature(dr, authenticatedSession));
				}

				return group;
			} else {
				var group = new ProcessGroup();
				return group;
			}
		}

		public void Delete(int processGroupId) {
			SetDBParam(SqlDbType.Int, "processGroupId", processGroupId);
			db.ExecuteDeleteQuery("DELETE FROM process_groups WHERE instance = @instance AND process = @process AND process_group_id = @processGroupId", DBParameters);
			Cache.Remove(instance, "group_ids_" + process);
			Cache.Remove(instance, "groups_" + process);
		}

		public void Save(ProcessGroup processGroup) {
			SetDBParam(SqlDbType.NVarChar, "@orderNo", processGroup.OrderNo);
			if (processGroup.MaintenanceName == null) {
				SetDBParam(SqlDbType.NVarChar, "@maintenanceName", DBNull.Value);
			} else {
				SetDBParam(SqlDbType.NVarChar, "@maintenanceName", processGroup.MaintenanceName);
			}

			SetDBParam(SqlDbType.TinyInt, "@type", processGroup.Type);
			SetDBParam(SqlDbType.Int, "@processGroupId", processGroup.ProcessGroupId);
			db.ExecuteUpdateQuery("UPDATE process_groups SET order_no=@orderNo, type=@type, maintenance_name = @maintenanceName WHERE instance = @instance AND process = @process AND process_group_id = @processGroupId", DBParameters);
			var oldVar = db.ExecuteScalar("SELECT variable FROM process_groups WHERE instance = @instance AND process = @process AND process_group_id = @processGroupId", DBParameters);
			if (oldVar != null && oldVar != DBNull.Value && processGroup.LanguageTranslation.ContainsKey(authenticatedSession.locale_id) && processGroup.LanguageTranslation[authenticatedSession.locale_id] != null) {
				
				foreach (var translation in processGroup.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string)oldVar, translation.Value, translation.Key);
				}		
				
			} else if (oldVar == DBNull.Value && processGroup.LanguageTranslation[authenticatedSession.locale_id] != null) {
				SetDBParam(SqlDbType.NVarChar, "@variable", translations.GetTranslationVariable(process, processGroup.LanguageTranslation[authenticatedSession.locale_id], "GROUP"));
				db.ExecuteUpdateQuery("UPDATE process_groups SET variable = @variable WHERE instance = @instance AND process = @process AND process_group_id = @processGroupId", DBParameters);
				var newVariable = translations.GetTranslationVariable(process, processGroup.LanguageTranslation[authenticatedSession.locale_id], "GROUP");

				foreach (var translation in processGroup.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVariable, translation.Value, translation.Key);
				}
			}
		//	db.ExecuteUpdateQuery("DELETE FROM process_group_features WHERE process_group_id = @processGroupId", DBParameters);

			if (processGroup.SelectedFeatures != null) {
				foreach (var feature in processGroup.SelectedFeatures) {
					SetDBParam(SqlDbType.NVarChar, "@feature", feature.Feature);
					SetDBParam(SqlDbType.NVarChar, "@state", feature.DefaultState);

					int ifExists =
						Conv.ToInt(
							db.ExecuteScalar(
								"SELECT COUNT(*) FROM process_group_features where process_group_id = @processGroupId AND feature = @feature",DBParameters));

					if (ifExists == 0) {
						db.ExecuteInsertQuery("INSERT INTO process_group_features (process_group_id, feature, state) VALUES(@processGroupId, @feature, @state)", DBParameters);
					}

					if (feature.LanguageTranslation != null &&
					    feature.LanguageTranslation.ContainsKey(authenticatedSession.locale_id) &&
					    feature.LanguageTranslation[authenticatedSession.locale_id] != null) {
						var translations = new Translations(authenticatedSession.instance, authenticatedSession);

						var oldVariable =
							db.ExecuteScalar(
								"select custom_name FROM process_group_features WHERE process_group_id = @processGroupId AND feature = @feature",
								DBParameters);

						if (DBNull.Value.Equals(oldVariable)) {
							oldVariable = translations.GetTranslationVariable(process,
								feature.LanguageTranslation[authenticatedSession.locale_id], "PGCN");
							SetDBParam(SqlDbType.NVarChar, "@customName", oldVariable);

							db.ExecuteUpdateQuery(
								"UPDATE process_group_features SET custom_name = @customName WHERE process_group_id = @processGroupId AND feature = @feature ",
								DBParameters);
						}

						foreach (var translation in feature.LanguageTranslation) {
							translations.insertOrUpdateProcessTranslation(process, Conv.ToStr(oldVariable), translation.Value,
								translation.Key);
						}
					}
				}
			}

			Cache.Remove(instance, "group_ids_" + process);
			Cache.Remove(instance, "groups_" + process);
		}

		public void DeleteProcessFeatures(List<ProcessGroupFeature> features, int groupId) {
			SetDBParam(SqlDbType.Int, "@groupId", groupId);
			foreach (var feature in features) {
				SetDBParam(SqlDbType.NVarChar, "@feature", feature.Feature);
				db.ExecuteUpdateQuery("DELETE FROM process_group_features WHERE process_group_id = @groupId AND feature = @feature", DBParameters);
			}
			Cache.Remove(instance, "group_ids_" + process);
			Cache.Remove(instance, "groups_" + process);
		}

		public ProcessGroup Add(ProcessGroup processGroup) {
			SetDBParam(SqlDbType.NVarChar, "@orderNo", processGroup.OrderNo);
			SetDBParam(SqlDbType.TinyInt, "@type", processGroup.Type);
			processGroup.ProcessGroupId = db.ExecuteInsertQuery("INSERT INTO process_groups (instance, process, order_no, type) VALUES(@instance, @process, @orderNo, @type)", DBParameters);
			if (processGroup.LanguageTranslation[authenticatedSession.locale_id] != null) {
				SetDBParam(SqlDbType.NVarChar, "@variable", translations.GetTranslationVariable(process, processGroup.LanguageTranslation[authenticatedSession.locale_id], "GROUP"));
				SetDBParam(SqlDbType.Int, "@processGroupId", processGroup.ProcessGroupId);
				db.ExecuteUpdateQuery("UPDATE process_groups SET variable = @variable WHERE instance = @instance AND process = @process AND process_group_id = @processGroupId", DBParameters);

				var oldVariable = translations.GetTranslationVariable(process, processGroup.LanguageTranslation[authenticatedSession.locale_id], "GROUP");
				foreach (var translation in processGroup.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, oldVariable, translation.Value, translation.Key);
				}	
			}
			if (processGroup.SelectedFeatures != null) {
				foreach (var feature in processGroup.SelectedFeatures) {
					SetDBParam(SqlDbType.NVarChar, "@feature", feature.Feature);
					SetDBParam(SqlDbType.NVarChar, "@state", feature.DefaultState);
					SetDBParam(SqlDbType.Int, "@processGroupId", processGroup.ProcessGroupId);
					db.ExecuteInsertQuery("INSERT INTO process_group_features (process_group_id, feature, state) VALUES(@processGroupId, @feature, @state)", DBParameters);
				}
			}
			
	        Cache.Remove(instance, "group_ids_" + process);
			Cache.Remove(instance, "groups_" + process);
			return processGroup;
		}

		public List<Dictionary<string, object>> GetGroupsNavigationMenu(int processGroupId) {
			var retval = new List<Dictionary<string, object>>();
			SetDBParam(SqlDbType.Int, "@processGroupId", processGroupId);
			foreach (DataRow row in db.GetDatatable("SELECT variable, process_tab_id, default_state, type_id, order_no, icon, template_portfolio_id, custom_name FROM process_group_navigation gn LEFT JOIN process_group_features gf ON gn.process_group_id = gf.process_group_id AND gn.variable = gf.feature WHERE gn.process_group_id = @processGroupId ORDER BY order_no  ASC", DBParameters).Rows) {
				var retrow = new Dictionary<string, object>();
				retrow.Add("Variable", row["variable"]);
				retrow.Add("ProcessTabId", row["process_tab_id"]);
				retrow.Add("DefaultState", row["default_state"]);
				retrow.Add("Type", row["type_id"]);
				retrow.Add("OrderNo", row["order_no"]);
				retrow.Add("Icon", row["icon"]);
				retrow.Add("TemplatePortfolioId", row["template_portfolio_id"]);
				retrow.Add("CustomName", row["custom_name"]);

				var lt = new Translations(instance, authenticatedSession).GetTranslations(Conv.ToInt(row["type_id"]) == 0 ? Conv.ToStr(row["variable"]) : Conv.ToStr(row["custom_name"]));
				retrow.Add("LanguageTranslation", lt);


				retval.Add(retrow);
			}
			return retval;
		}

		public Dictionary<string, object> AddGroupsNavigationMenu(int processGroupId, Dictionary<string, object> MenuItem) {
			SetDBParam(SqlDbType.Int, "@processGroupId", processGroupId);
			SetDBParam(SqlDbType.NVarChar, "@variable", MenuItem["Variable"]);
			SetDBParam(SqlDbType.Int, "@processTabId", MenuItem["ProcessTabId"]);
			SetDBParam(SqlDbType.NVarChar, "@defaultState", MenuItem["DefaultState"]);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", MenuItem["OrderNo"]);
			SetDBParam(SqlDbType.Int, "@type", MenuItem["Type"]);
			SetDBParam(SqlDbType.NVarChar, "@icon", Conv.IfNullThenDbNull(MenuItem["Icon"]));
			SetDBParam(SqlDbType.Int, "@templatePortfolioId", Conv.IfNullThenDbNull(MenuItem["TemplatePortfolioId"]));

			db.ExecuteInsertQuery("INSERT INTO process_group_navigation (process_group_id, variable, process_tab_id, default_state, order_no, type_id, icon, template_portfolio_id) VALUES (@processGroupId, @variable, @processTabId, @defaultState, @orderNo, @type, @icon, @templatePortfolioId)", DBParameters);
			return MenuItem;
		}

		public List<Dictionary<string, object>> SaveGroupsNavigationMenu(int processGroupId, List<Dictionary<string, object>> MenuItem) {
			SetDBParam(SqlDbType.Int, "@processGroupId", processGroupId);
			db.ExecuteDeleteQuery("DELETE FROM process_group_navigation WHERE process_group_id = @processGroupId", DBParameters);
			foreach (var menu in MenuItem) {
				SetDBParam(SqlDbType.NVarChar, "@variable", menu["Variable"]);
				SetDBParam(SqlDbType.Int, "@processTabId", menu["ProcessTabId"]);
				SetDBParam(SqlDbType.NVarChar, "@defaultState", menu["DefaultState"]);
				SetDBParam(SqlDbType.NVarChar, "@orderNo", menu["OrderNo"]);
				SetDBParam(SqlDbType.Int, "@type", menu["Type"]);
				SetDBParam(SqlDbType.NVarChar, "@icon", Conv.IfNullThenDbNull(menu["Icon"]));
				SetDBParam(SqlDbType.Int, "@templatePortfolioId", Conv.IfNullThenDbNull(menu["TemplatePortfolioId"]));

				db.ExecuteInsertQuery("INSERT INTO process_group_navigation (process_group_id, variable, process_tab_id, default_state, order_no, type_id, icon, template_portfolio_id) VALUES (@processGroupId, @variable, @processTabId, @defaultState, @orderNo, @type, @icon, @templatePortfolioId)", DBParameters);

				if (menu["TemplatePortfolioId"] != null) {
					db.ExecuteUpdateQuery(
						"UPDATE process_group_navigation SET template_portfolio_id = @templatePortfolioId WHERE process_tab_id = @processTabId AND process_group_id = @processGroupId",
						DBParameters);
				}
			}
			return MenuItem;
		}

		public void DeleteGroupsNavigationMenu(int processGroupId, int processTabId) {
			SetDBParam(SqlDbType.Int, "@processGroupId", processGroupId);
			SetDBParam(SqlDbType.Int, "@processTabId", processTabId);
			db.ExecuteDeleteQuery("DELETE FROM process_group_navigation WHERE process_group_id = @processGroupId AND process_tab_id = @processTabId", DBParameters);
		}

		public Dictionary<int, Dictionary<int, List<int>>> GetGroupsRecursive(int? groupId = null)
		{
			var pgt = new ProcessGroupTabs(instance, process, authenticatedSession);
			var ptc = new ProcessTabContainers(instance, process, authenticatedSession);

			List<int> gids;
			if (groupId == null) {
				gids = GetGroupIds();
			} else {
				gids = new List<int>();
				gids.Add((int) groupId);
			}

			var retval = new Dictionary<int, Dictionary<int, List<int>>>();
			foreach (var id in gids)
			{
				var tabs = new Dictionary<int, List<int>>();

				foreach (var tId in pgt.GetGroupTabIds(id)) {
					var containers = new List<int>();
					foreach (var cId in ptc.GetTabContainerIds(tId))
					{
						containers.Add(cId);
					}
					tabs.Add(tId, containers);
				}
				retval.Add(id, tabs);
			}

			return retval;
		}

		public Dictionary<int, List<DateTime>> GetArchiveDates(int itemId)
	    {
	        var retval = new Dictionary<int, List<DateTime>>();

	        SetDBParam(SqlDbType.Int, "@itemId", itemId);
	        
	        foreach(DataRow r in db.GetDatatable("select pg.process_group_id, pb.baseline_date from process_groups pg " +
	        " left join process_baseline pb on pg.process_group_id = pb.process_group_id and process_item_id = @itemId " +
	        " where pg.instance = @instance and pg.process = @process " + 
	        " order by pg.process_group_id, pb.baseline_date desc", DBParameters).Rows) {
	            if (!retval.ContainsKey(Conv.ToInt(r["process_group_id"])))
	            {
	                retval.Add(Conv.ToInt(r["process_group_id"]), new List<DateTime>());
	            }
	            if (!DBNull.Value.Equals(r["baseline_date"]))
	            {
	                retval[Conv.ToInt(r["process_group_id"])].Add((DateTime)r["baseline_date"]);	                
	            }
	        }
	        return retval;
	    }
	}

	public class ProcessPortfolioGroups : ProcessBase {
		public ProcessPortfolioGroups(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
		}

		public List<ProcessGroup> GetGroups(int portfolioId) {
			var result = new List<ProcessGroup>();
			SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);
			foreach (DataRow row in db.GetDatatable("SELECT pg.process_group_id, variable, short_variable, order_no,type,  0 AS tab_count, maintenance_name  FROM process_portfolio_groups ppg INNER JOIN process_groups pg ON ppg.process_group_id = pg.process_group_id WHERE instance = @instance AND process = @process AND process_portfolio_id = @portfolioId ORDER BY order_no  ASC", DBParameters).Rows) {
				var group = new ProcessGroup(row, authenticatedSession);
				result.Add(group);
			}
			return result;
		}

		public List<int> GetGroupIds(int portfolioId) {
			var result = new List<int>();
			SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);
			foreach (DataRow row in db.GetDatatable("SELECT process_group_id FROM process_portfolio_groups WHERE process_portfolio_id = @portfolioId", DBParameters).Rows) {
				result.Add((int)row["process_group_id"]);
			}
			return result;
		}

		public void Add(int portfolioId, List<ProcessGroup> groups) {
			SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);
			db.ExecuteDeleteQuery("DELETE FROM process_portfolio_groups WHERE process_portfolio_id = @portfolioId", DBParameters);
			foreach (var group in groups) {
				SetDBParam(SqlDbType.Int, "@groupId", group.ProcessGroupId);
				db.ExecuteInsertQuery("INSERT INTO process_portfolio_groups (process_portfolio_id, process_group_id) VALUES(@portfolioId, @groupId)", DBParameters);
			}
		}

		public void Delete(int portfolioId, int groupId) {
			SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);
			SetDBParam(SqlDbType.Int, "@groupId", groupId);
			db.ExecuteDeleteQuery("DELETE FROM process_portfolio_groups WHERE process_portfolio_id = @portfolioId AND process_group_id = @groupId", DBParameters);
		}
	}
}