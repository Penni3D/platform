﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/ProcessContainers")]
	public class ProcessContainersRest : PageBase {
		public ProcessContainersRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessContainer> Get(string process) {
			var containers = new ProcessContainers(instance, process, currentSession);
			return containers.GetContainers();
		}

		[HttpGet("{process}/{processContainerId}")]
		public ProcessContainer Get(string process, int processContainerId) {
			var tabs = new ProcessContainers(instance, process, currentSession);
			return tabs.GetContainer(processContainerId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] ProcessContainer processContainer) {
			CheckRight("write");
			var processContainers = new ProcessContainers(instance, process, currentSession);
			if (ModelState.IsValid) {
				processContainers.Add(processContainer);
				return new ObjectResult(processContainer);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<ProcessContainer> processContainer) {
			CheckRight("write");
			var processContainers = new ProcessContainers(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pc in processContainer) {
					processContainers.Save(pc);
				}

				return new ObjectResult(processContainer);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}/{processContainerId}")]
		public IActionResult Delete(string process, string processContainerId) {
			CheckRight("delete");
			var processContainers = new ProcessContainers(instance, process, currentSession);
			foreach (var id in processContainerId.Split(',')) {
				processContainers.Delete(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}
	}


	[Route("v1/settings/[controller]")]
	public class ProcessProcessContainer : PageBase {
		public ProcessProcessContainer() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}/{processContainerId}")]
		public Dictionary<string, object> Get(string process, int processContainerId) {
			var containers = new ProcessContainers(instance, process, currentSession);
			return containers.GetProcessContainer(processContainerId);
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessContainersNotInTabs : PageBase {
		public ProcessContainersNotInTabs() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessContainer> Get(string process) {
			var containers = new ProcessContainers(instance, process, currentSession);
			return containers.GetContainersNotInTabs();
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessContainerCopy : PageBase {
		public ProcessContainerCopy() : base("settings") { }

		[HttpPost("{process}")]
		public ProcessContainer Post(string process, [FromBody] Dictionary<string, object> data) {
			var containers = new ProcessContainers(instance, process, currentSession);
			return containers.CopyProcessContainer(data);
		}
	}
}