﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/ProcessSubprocesses")]
	public class ProcessSubprocessesRest : PageBase {
		public ProcessSubprocessesRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessSubProcess> Get(string process) {
			var subprocesses = new ProcessSubprocesses(instance, process, currentSession);
			return subprocesses.GetSubProcesses(process);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] ProcessSubProcess subProcess) {
			CheckRight("write");
			var subprocesses = new ProcessSubprocesses(instance, process, currentSession);
			if (ModelState.IsValid) {
				subprocesses.Add(subProcess);
				return new ObjectResult(subProcess);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] ProcessSubProcess subProcess) {
			CheckRight("write");
			var subprocesses = new ProcessSubprocesses(instance, process, currentSession);
			if (ModelState.IsValid) {
				subprocesses.Save(subProcess);
				return new ObjectResult(subProcess);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}/{parentProcess}")]
		public IActionResult Delete(string process, string parentProcess) {
			CheckRight("delete");
			var subprocesses = new ProcessSubprocesses(instance, process, currentSession);
			subprocesses.Delete(parentProcess);
			return new StatusCodeResult(200);
		}
	}
}