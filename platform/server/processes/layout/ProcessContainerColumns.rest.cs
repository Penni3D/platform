﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.accessControl;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/[controller]")]
	public class ProcessContainerColumns : PageBase {
		public ProcessContainerColumns() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public Dictionary<int, List<ProcessContainerField>> Get(string process) {
			var fields = new ProcesContainerColumns(instance, process, currentSession);
			return fields.GetFields();
		}

		[HttpGet("{process}/{containerId}")]
		public List<ProcessContainerField> Get(string process, int containerId) {
			var fields = new ProcesContainerColumns(instance, process, currentSession);
			return fields.GetFields(containerId);
		}

		[HttpGet("{process}/{containerId}/{containerColumnId}")]
		public ProcessContainerField Get(string process, int containerId, int containerColumnId) {
			var fields = new ProcesContainerColumns(instance, process, currentSession);
			return fields.GetField(containerColumnId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]List<ProcessContainerField> field) {
			CheckRight("write");
			var fields = new ProcesContainerColumns(instance, process, currentSession);
			foreach (var item in field) {
				fields.Add(item);
			}
			return new ObjectResult(field);
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]List<ProcessContainerField> field) {
			CheckRight("write");
			var fields = new ProcesContainerColumns(instance, process, currentSession);
			foreach (var pcf in field) {
				fields.Save(pcf);
			}
			return new ObjectResult(field);

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			//return null;
		}

		[HttpDelete("{process}/{containerId}")]
		public IActionResult Delete(string process, string containerId) {
			CheckRight("delete");
			var fields = new ProcesContainerColumns(instance, process, currentSession);

			foreach (var id in containerId.Split(',')) {
				fields.Delete(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}

		[HttpPut("{process}/{itemId}/{containerColumnId}/{containerId}/{itemColumnId}")]
	    public IActionResult HelpTextSave(string process, int itemId, int containerColumnId, int containerId, int itemColumnId, [FromBody]HelpTextMain helpText) {

		    var MetaStates = new States(instance, currentSession).GetEvaluatedStatesForFeature(process, "meta", itemId);
		    if(!MetaStates.States.Contains("meta.help")) throw new CustomPermissionException("User has no rights to state help");

	        var pcc = new ProcesContainerColumns(instance, process, currentSession);
	        
	        //Send Help Text Change to All Clients
	        var SignalR = new ApiPush(Startup.SignalR);
	        var parameters = new List<object>();
	        parameters.Add(process);
	        SignalR.SendToAll("HelpTextUpdated", parameters);
	        //Done
	        return new ObjectResult(pcc.HelpTextSave(containerColumnId, containerId, itemColumnId, itemId, helpText.HelpText));
	    }

	    public class HelpTextMain
	    {
		    public string HelpText { get; set; }
	    }
	}
}