﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/ProcessTabContainers")]
	public class ProcessTabContainersRest : PageBase {
		public ProcessTabContainersRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessTabContainer> Get(string process) {
			var containers = new ProcessTabContainers(instance, process, currentSession);
			return containers.GetTabContainers();
		}

		[HttpGet("{process}/{tabId}")]
		public List<ProcessTabContainer> Get(string process, int tabId) {
			var containers = new ProcessTabContainers(instance, process, currentSession);
			return containers.GetTabContainers(tabId);
		}

		[HttpGet("{process}/{processTabContainerId}/{ProcessTabId}/{ProcessContainerId}")]
		public ProcessTabContainer Get(string process, int processTabContainerId, int processTabId,
			int processContainerId) {
			var containers = new ProcessTabContainers(instance, process, currentSession);
			return containers.GetContainer(processTabContainerId, processTabId, processContainerId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] ProcessTabContainer tabContainer) {
			CheckRight("write");
			var containers = new ProcessTabContainers(instance, process, currentSession);
			if (ModelState.IsValid) {
				containers.Add(tabContainer);
				return new ObjectResult(tabContainer);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] ProcessTabContainer tabContainer) {
			CheckRight("write");
			var containers = new ProcessTabContainers(instance, process, currentSession);
			if (ModelState.IsValid) {
				containers.Save(tabContainer);
				return new ObjectResult(tabContainer);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}/{processTabContainerId}")]
		public IActionResult Delete(string process, int processTabContainerId) {
			CheckRight("delete");
			var containers = new ProcessTabContainers(instance, process, currentSession);
			containers.Delete(processTabContainerId);
			return new StatusCodeResult(200);
		}
	}
}