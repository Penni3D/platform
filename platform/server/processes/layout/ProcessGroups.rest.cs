﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/ProcessGroups")]
	public class ProcessGroupsRest : PageBase {
		public ProcessGroupsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessGroup> Get(string process) {
			var groups = new ProcessGroups(instance, process, currentSession);
			return groups.GetGroups();
		}

		[HttpGet("{process}/{processGroupId}")]
		public ProcessGroup Get(string process, int processGroupId) {
			var groups = new ProcessGroups(instance, process, currentSession);
			return groups.GetGroup(processGroupId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] ProcessGroup processGroup) {
			CheckRight("write");
			var processGroups = new ProcessGroups(instance, process, currentSession);
			if (ModelState.IsValid) {
				processGroups.Add(processGroup);
				return new ObjectResult(processGroup);
			}

			return null;
		}

		[HttpPost("{groupId}/featuredelete")]
		public void DeleteGroupFeatures(int groupId, [FromBody] List<ProcessGroupFeature> features) {
			CheckRight("write");
			var processGroups = new ProcessGroups(instance, process, currentSession);
			processGroups.DeleteProcessFeatures(features, groupId);
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<ProcessGroup> processGroup) {
			CheckRight("write");
			var processGroups = new ProcessGroups(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pg in processGroup) {
					processGroups.Save(pg);
				}

				return new ObjectResult(processGroup);
			}


			return null;
		}

		[HttpDelete("{process}/{processGroupId}")]
		public IActionResult Delete(string process, string processGroupId) {
			CheckRight("delete");

			var processGroups = new ProcessGroups(instance, process, currentSession);
			foreach (var id in processGroupId.Split(',')) {
				processGroups.Delete(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}

		[HttpGet("groupsRecursive/{process}")]
		public Dictionary<int, Dictionary<int, List<int>>> GetGroupsRecursive(string process) {
			var groups = new ProcessGroups(instance, process, currentSession);
			return groups.GetGroupsRecursive();
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessGroupCounts : PageBase {
		public ProcessGroupCounts() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessGroup> Get(string process) {
			var groups = new ProcessGroups(instance, process, currentSession);
			return groups.GetGroupsWithCounts();
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessGroupNavigation : PageBase {
		public ProcessGroupNavigation() : base("settings") { }

		// GET: model/values
		[HttpGet("{processGroupId}")]
		public List<Dictionary<string, object>> Get(int processGroupId) {
			var groups = new ProcessGroups(instance, "", currentSession);
			return groups.GetGroupsNavigationMenu(processGroupId);
		}

		// GET: model/values
		[HttpPost("{process}/{processGroupId}")]
		public Dictionary<string, object> Post(string process, int processGroupId,
			[FromBody] Dictionary<string, object> navigationItems) {
			CheckRight("write");
			var groups = new ProcessGroups(instance, process, currentSession);
			return groups.AddGroupsNavigationMenu(processGroupId, navigationItems);
		}

		// GET: model/values
		[HttpPut("{process}/{processGroupId}")]
		public List<Dictionary<string, object>> Put(string process, int processGroupId,
			[FromBody] List<Dictionary<string, object>> navigationItems) {
			CheckRight("write");
			var groups = new ProcessGroups(instance, process, currentSession);
			return groups.SaveGroupsNavigationMenu(processGroupId, navigationItems);
		}

		// GET: model/values
		[HttpDelete("{process}/{processGroupId}/{processTabId}")]
		public void HttpDelete(string process, int processGroupId, int processTabId) {
			CheckRight("delete");
			var groups = new ProcessGroups(instance, process, currentSession);
			groups.DeleteGroupsNavigationMenu(processGroupId, processTabId);
		}
	}
}