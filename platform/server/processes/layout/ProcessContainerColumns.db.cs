﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessContainerField {
		public ProcessContainerField() { }

		public ProcessContainerField(DataRow row, AuthenticatedSession authenticatedSession, string alias = "") {
			ProcessContainerColumnId = (int) row["process_container_column_id"];
			ProcessContainerId = (int) row["process_container_id"];

			OrderNo = (string) row["order_no"];

			if (!DBNull.Value.Equals(row["validation"])) {
				Validation =
					JsonConvert.DeserializeObject<FieldValidation>(Conv.ToStr(row["validation"]));
			} else {
				Validation = new FieldValidation();
				Validation.ReadOnly = null;
				Validation.Required = null;
			}

			if (!DBNull.Value.Equals(row["placeholder"])) {
				Placeholder = (string) row["placeholder"];
			}

			if (row["item_column_id"] != DBNull.Value) {
				ItemColumn = new ItemColumn(row, authenticatedSession);
			} else {
				ItemColumn = new ItemColumn();
			}

			if (!DBNull.Value.Equals(row["help_text"])) {
				HelpTextTranslation = Conv.ToStr(row["help_text"]);
			}
		}

		public string Instance { get; set; }
		public string Process { get; set; }
		public int ProcessContainerColumnId { get; set; }
		public int ProcessContainerId { get; set; }
		public string Placeholder { get; set; }
		public FieldValidation Validation { get; set; }
		public string OrderNo { get; set; }
		public ItemColumn ItemColumn { get; set; }
		public string HelpTextTranslation { get; set; }
	}

	public class ProcesContainerColumns : ConnectionBaseProcess {
		private readonly AuthenticatedSession authenticatedSession;

		public ProcesContainerColumns(string instance, string process, AuthenticatedSession authenticatedSession) :
			base(
				instance, process, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		private DataTable _itemContainersWithColumns;
		private DataTable itemContainersWithColumns {
			get {
				_itemContainersWithColumns = (DataTable) Cache.Get("", "item_containers_and_columns_table");
				if (_itemContainersWithColumns != null) return _itemContainersWithColumns;
				
				_itemContainersWithColumns = db.GetDatatable(
					"SELECT *, null AS item_id FROM process_container_columns pcc INNER JOIN item_columns ic ON pcc.item_column_id = ic.item_column_id ORDER BY order_no  ASC");
				Cache.Set("", "item_containers_and_columns_table", _itemContainersWithColumns);

				return _itemContainersWithColumns;
			}
		}

		public DataRow[] GetData(string instance, string process) {
			var f = new common.Util.Filter();
			f.Add("process", process);
			f.Add("instance", instance);
			return itemContainersWithColumns.Select(f.ToString());
		}

		private void ClearCache() {
			_itemContainersWithColumns = null;
			Cache.Remove("", "item_columns_table");
			Cache.Remove("", "item_columns_and_containers_table");
			Cache.Remove("", "item_containers_and_columns_table");
		}

		public List<ProcessContainerField> GetFields(int containerId, int item_id) {
			var result = new List<ProcessContainerField>();

			var f = new common.Util.Filter();
			f.Add("process_container_id", containerId);
			var dt = itemContainersWithColumns.Select(f.ToString());

			foreach (DataRow row in dt) {
				var containerField = new ProcessContainerField(row, authenticatedSession);
				AddEquations(containerField.ItemColumn);
				result.Add(containerField);
			}

			return result;
		}

		private void AddEquations(ItemColumn itemColumn) {
			if (!itemColumn.IsEquation()) return;
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumn.ItemColumnId);
			itemColumn.Equations =
				db.GetDatatableDictionary(
					"SELECT * FROM item_column_equations WHERE parent_item_column_id = @itemColumnId",
					DBParameters);
		}

		public ProcessContainerField GetField(int containerColumnId) {
			var f = new common.Util.Filter();
			f.Add("process_container_column_id", containerColumnId);
			var dt = itemContainersWithColumns.Select(f.ToString());

			if (dt.Length > 0) {
				var containerField = new ProcessContainerField(dt[0], authenticatedSession);
				AddEquations(containerField.ItemColumn);
				return containerField;
			}

			throw new CustomArgumentException("Field cannot be found using process_container_column_id " +
			                                  containerColumnId);
		}

		private Dictionary<int, List<ProcessContainerField>> FieldCache = new ();

		public List<ProcessContainerField> GetFields(int containerId) {
			if (FieldCache.ContainsKey(containerId)) return FieldCache[containerId];

			var result = new List<ProcessContainerField>();
			var f = new common.Util.Filter();
			f.Add("process_container_id", containerId);
			var dt = itemContainersWithColumns.Select(f.ToString());
			
			foreach (DataRow row in dt) {
				var containerField = new ProcessContainerField(row, authenticatedSession);
				AddEquations(containerField.ItemColumn);
				result.Add(containerField);
			}

			FieldCache.Add(containerId, result);
			return result;
		}

		public List<ProcessContainerField> GetFields(int groupId, int processTabId, int containerId,
			DataRow[] containerData, FeatureStates rights) {
			var result = new List<ProcessContainerField>();

			var hasRight = false;
			bool hasWriteRight = rights.groupStates.ContainsKey(groupId) && rights.groupStates[groupId].tabStates
				                                                             .ContainsKey(processTabId)
			                                                             && rights.groupStates[groupId]
				                                                             .tabStates[processTabId]
				                                                             .containerStates
				                                                             .ContainsKey(containerId) &&
			                                                             rights.groupStates[groupId]
				                                                             .tabStates[processTabId]
				                                                             .containerStates[containerId].s
				                                                             .Contains("write");

			if (rights.groupStates.ContainsKey(groupId) && rights.groupStates[groupId].tabStates
				                                            .ContainsKey(processTabId)
			                                            && rights.groupStates[groupId].tabStates[processTabId]
				                                            .containerStates.ContainsKey(containerId) &&
			                                            rights.groupStates[groupId].tabStates[processTabId]
				                                            .containerStates[containerId].s.Contains("read")) {
				hasRight = true;
			}

			if (!hasRight && !hasWriteRight) return result;
			
			foreach (var row in containerData.Where( cd => Conv.ToInt(cd["process_container_id"]) == containerId).ToList()) {
				var containerField = new ProcessContainerField(row, authenticatedSession);

				AddEquations(containerField.ItemColumn);

				if (containerField.Validation.ReadOnly != null) {
					containerField.Validation.ReadOnly =
						Conv.ToBool(containerField.Validation.ReadOnly) || !hasWriteRight ||
						containerField.ItemColumn.IsProcess() && containerField.ItemColumn.UseLucene;
				} else {
					if (containerField.ItemColumn.Validation.ReadOnly != null) {
						containerField.Validation.ReadOnly =
							Conv.ToBool(containerField.ItemColumn.Validation.ReadOnly) || !hasWriteRight ||
							containerField.ItemColumn.IsProcess() && containerField.ItemColumn.UseLucene;
					} else {
						containerField.Validation.ReadOnly = !hasWriteRight;
					}
				}

				result.Add(containerField);
			}

			return result;
		}

		public Dictionary<int, List<ProcessContainerField>> GetFields() {
			var pc = new ProcessContainers(instance, process, authenticatedSession);
			var result = new Dictionary<int, List<ProcessContainerField>>();
			foreach (var id in pc.GetContainerIds()) {
				result.Add(id, GetFields(id));
			}

			return result;
		}

		public void Save(ProcessContainerField field) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@containerColumnId", field.ProcessContainerColumnId));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@placeholder", Conv.IfNullThenDbNull(field.Placeholder)));

			if (field.Validation != null) {
				var validation = JsonConvert.SerializeObject(field.Validation);
				p.Add(GetDBParam(SqlDbType.NVarChar, "@validation", validation));
			} else {
				p.Add(GetDBParam(SqlDbType.NVarChar, "@validation", DBNull.Value));
			}

			p.Add(GetDBParam(SqlDbType.Int, "@processContainerId", Conv.IfNullThenDbNull(field.ProcessContainerId)));
			p.Add(GetDBParam(SqlDbType.Int, "@itemColumnId", field.ItemColumn.ItemColumnId));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@orderNo", field.OrderNo));

			db.ExecuteUpdateQuery(
				"UPDATE process_container_columns SET placeholder = @placeholder, validation = @validation, item_column_id = @itemColumnId, order_no = @orderNo, process_container_id = @processContainerId WHERE process_container_column_id = @containerColumnId",
				p);

			//Clear cache
			Cache.RemoveGroup("list_hierarchy_meta_" + process);

			if (field.HelpTextTranslation != null) {
				p.Add(GetDBParam(SqlDbType.NVarChar, "@helpText", field.HelpTextTranslation));
				db.ExecuteUpdateQuery(
					"UPDATE process_container_columns SET help_text = @helpText WHERE process_container_column_id = @containerColumnId ",
					p);
			}

			ClearCache();
		}

		public ProcessContainerField Add(ProcessContainerField field) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@containerId", field.ProcessContainerId));
			if (field.ItemColumn != null) {
				p.Add(GetDBParam(SqlDbType.Int, "@itemColumnId", Conv.IfNullThenDbNull(field.ItemColumn.ItemColumnId)));
			} else {
				p.Add(GetDBParam(SqlDbType.Int, "@itemColumnId", DBNull.Value));
			}

			p.Add(GetDBParam(SqlDbType.NVarChar, "@orderNo", field.OrderNo));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@placeholder", Conv.IfNullThenDbNull(field.Placeholder)));

			if (Conv.ToStr(field.Validation) != "" && Conv.ToStr(field.Validation) != "0") {
				var validation = JsonConvert.SerializeObject(field.Validation);
				p.Add(GetDBParam(SqlDbType.NVarChar, "@validation", validation));
			} else {
				p.Add(GetDBParam(SqlDbType.NVarChar, "@validation", DBNull.Value));
			}

			var new_id = SetDBParam(GetDBParam(SqlDbType.Int, "@new_id", 0, ParameterDirection.Output));
			p.Add(new_id);

			db.ExecuteInsertQuery(
				"INSERT INTO process_container_columns (process_container_id, placeholder, validation, item_column_id, order_no) VALUES(@containerId, @placeholder, @validation, @itemColumnId, @orderNo) SET @new_id = @@identity;",
				p);
			var newId = (int) new_id.Value;
			field.ProcessContainerColumnId = newId;

			//Clear cache
			Cache.RemoveGroup("list_hierarchy_meta_" + process);
			ClearCache();
			
			if (field.HelpTextTranslation != null) {
				SetDBParam(SqlDbType.Int, "@ccId", field.ProcessContainerColumnId);
				db.ExecuteUpdateQuery(
					"UPDATE process_container_columns SET help_text = @helpText WHERE process_container_column_id = @ccId ",
					p);
			}

			return field;
		}

		public void Delete(int processContainerColumnId) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@processContainerColumnId", processContainerColumnId));
			db.ExecuteDeleteQuery(
				"DELETE FROM process_container_columns WHERE process_container_column_id = @processContainerColumnId",
				p);
			ClearCache();
		}

		public string HelpTextSave(int containerColumnId, int containerId, int itemColumnId,
			int itemId, string helpTextTranslation) {
			var userCols = new List<int>();
			var states = new States(instance, authenticatedSession);
			var fStates = states.GetEvaluatedStatesForFeature(process, "meta", itemId);

			var pcc = new ProcesContainerColumns(instance, process, authenticatedSession);
			var cIds = fStates.GetContainerIds("meta.help");
			if (cIds.Contains(containerId)) {
				foreach (var pcf in pcc.GetFields(containerId)) {
					userCols.Add(pcf.ItemColumn.ItemColumnId);
				}
			}

			if (!userCols.Contains(itemColumnId)) {
				var exception = new CustomPermissionException("NOT_ALLOWED_TO_WRITE_TO_PROCESS",
					"Current user can't write to process " + process);
				exception.Data.Add("process", process);
				throw exception;
			}

			SetDBParam(SqlDbType.NVarChar, "@helpText", helpTextTranslation);
			SetDBParam(SqlDbType.Int, "@containerColumnId", containerColumnId);
			db.ExecuteUpdateQuery(
				"UPDATE process_container_columns SET help_text = @helpText WHERE process_container_column_id = @containerColumnId ",
				DBParameters);
			ClearCache();
			return helpTextTranslation;
		}
	}
}