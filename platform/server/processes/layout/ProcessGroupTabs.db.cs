﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessGroupTab {
		public ProcessGroupTab() {

		}

		public ProcessGroupTab(DataRow dr, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			ProcessGroupTabId = (int)dr[alias + "process_group_tab_id"];
			OrderNo = (string)dr[alias + "order_no"];
		}

		public int ProcessGroupTabId { get; set; }
		public ProcessGroup ProcessGroup { get; set; }
		public ProcessTab ProcessTab { get; set; }
		public string OrderNo { get; set; }
		public string Tag { get; set; }
		public string ProcessTabGroups { get; internal set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
	}

	public class ProcessGroupTabs : ConnectionBaseProcess {
		private readonly AuthenticatedSession authenticatedSession;

		public ProcessGroupTabs(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public List<ProcessGroupTab> GetGroupTabs() {
			var result = new List<ProcessGroupTab>();
			var data = db.GetDatatable("SELECT process_group_tab_id, tag, process_group_id, process_tab_id, order_no FROM process_group_tabs ORDER BY order_no  ASC", DBParameters);

			foreach (DataRow row in data.Rows) {
				var gt = new ProcessGroupTab(row);
				gt.ProcessGroup = new ProcessGroups(instance, process, authenticatedSession).GetGroup((int)row["process_group_id"]);
				gt.ProcessTab = new ProcessTabs(instance, process, authenticatedSession).GetTab((int)row["process_tab_id"]);
				gt.Tag = Conv.ToStr(row["tag"]);
				result.Add(gt);
			}
			return result;
		}

		public void Add(ProcessGroupTab groupTab) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@groupId", groupTab.ProcessGroup.ProcessGroupId));
			p.Add(GetDBParam(SqlDbType.Int, "@tabId", groupTab.ProcessTab.ProcessTabId));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@orderNo", groupTab.OrderNo));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@tag", Conv.IfNullThenDbNull(groupTab.Tag)));
			db.ExecuteInsertQuery("INSERT INTO process_group_tabs (process_group_id, process_tab_id, order_no, tag) VALUES(@groupId, @tabId, @orderNo, @tag)", p);
			
			var groups = new ProcessGroups(instance, process, authenticatedSession);
			var MenuItem = new Dictionary<string, object>();

			if (groupTab.ProcessTab.Variable != null) {
				MenuItem.Add("Variable", groupTab.ProcessTab.Variable);				
			} else {
				var tabs = new ProcessTabs(instance, process, authenticatedSession);
				MenuItem.Add("Variable", tabs.GetTab(groupTab.ProcessTab.ProcessTabId).Variable);				
			}
			
			MenuItem.Add("ProcessTabId", groupTab.ProcessTab.ProcessTabId);
			MenuItem.Add("DefaultState", "process.tab");
			MenuItem.Add("OrderNo", groupTab.OrderNo);
			MenuItem.Add("Type", 0);
			MenuItem.Add("Icon", null);
			MenuItem.Add("TemplatePortfolioId", null);

			groups.AddGroupsNavigationMenu(groupTab.ProcessGroup.ProcessGroupId, MenuItem);

			Cache.Remove(instance, "group_tab_ids_" + groupTab.ProcessGroup.ProcessGroupId);
		}

		public void Save(ProcessGroupTab groupTab) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@groupId", groupTab.ProcessGroup.ProcessGroupId));
			p.Add(GetDBParam(SqlDbType.Int, "@tabId", groupTab.ProcessTab.ProcessTabId));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@orderNo", groupTab.OrderNo));
			p.Add(GetDBParam(SqlDbType.Int, "@groupTabId", groupTab.ProcessGroupTabId));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@tag", Conv.IfNullThenDbNull(groupTab.Tag)));
			db.ExecuteUpdateQuery("UPDATE process_group_tabs SET process_group_id = @groupId, process_tab_id = @tabId, order_no = @orderNo, tag = @tag WHERE process_group_tab_id = @groupTabId", p);
			Cache.Remove(instance, "group_tab_ids_" + groupTab.ProcessGroup.ProcessGroupId);
		}

		public List<ProcessGroupTab> GetTabGroups(int tabId) {
			SetDBParam(SqlDbType.Int, "@tabId", tabId);
			var result = new List<ProcessGroupTab>();
			var data = db.GetDatatable("SELECT process_group_tab_id, process_group_id, process_tab_id, order_no FROM process_group_tabs WHERE process_tab_id = @tabId ORDER BY order_no  ASC", DBParameters);

			foreach (DataRow row in data.Rows) {
				var gt = new ProcessGroupTab(row);
				gt.ProcessGroup = new ProcessGroups(instance, process, authenticatedSession).GetGroup((int)row["process_group_id"]);
				gt.ProcessTab = new ProcessTabs(instance, process, authenticatedSession).GetTab((int)row["process_tab_id"]);
				result.Add(gt);
			}
			return result;
		}

		public void Delete(int processGroupTabId) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@groupTabId", processGroupTabId));
			db.ExecuteDeleteQuery("DELETE FROM process_group_tabs WHERE process_group_tab_id = @groupTabId", p);
			Cache.Remove(instance, "group_tab_ids_" + processGroupTabId);
		}

		public ProcessGroupTab GetGroupTab(int processGroupTabId, int processGroupId, int processTabId) {
			SetDBParam(SqlDbType.Int, "@processGroupTabId", processGroupTabId);
			SetDBParam(SqlDbType.Int, "@processGroupId", processGroupId);
			SetDBParam(SqlDbType.Int, "@processTabId", processTabId);
			var dr = db.GetDataRow("SELECT process_group_tab_id, process_group_id, process_tab_id, order_no FROM process_group_tabs WHERE process_group_tab_id = @processGroupTabId AND process_group_id = @processGroupId AND process_tab_id = @processTabId", DBParameters);

			if (dr != null) {
				var gt = new ProcessGroupTab(dr);
				gt.ProcessGroup = new ProcessGroups(instance, process, authenticatedSession).GetGroup((int)dr["process_group_id"]);
				gt.ProcessTab = new ProcessTabs(instance, process, authenticatedSession).GetTab((int)dr["process_tab_id"]);
				return gt;
			}

			throw new CustomArgumentException("Group Tab cannot be found with process_group_tab_id " + processGroupTabId + " AND process_group_id " + processGroupId + " AND process_tab_id " + processTabId);
		}

		public List<ProcessGroupTab> GetGroupTabs(int groupId) {
			SetDBParam(SqlDbType.Int, "@groupId", groupId);
			var result = new List<ProcessGroupTab>();
			var data = db.GetDatatable("SELECT process_group_tab_id, process_group_id, process_tab_id, order_no, tag FROM process_group_tabs WHERE process_group_id = @groupId ORDER BY order_no  ASC", DBParameters);

			foreach (DataRow row in data.Rows) {
				var gt = new ProcessGroupTab(row);
				gt.ProcessGroup = new ProcessGroups(instance, process, authenticatedSession).GetGroup((int)row["process_group_id"]);
				gt.ProcessTab = new ProcessTabs(instance, process, authenticatedSession).GetTab((int)row["process_tab_id"]);
				gt.ProcessTabGroups = new ProcessTabs(instance, process, authenticatedSession).GetTabGroups((int)row["process_tab_id"]);
				gt.Tag = Conv.ToStr(row["tag"]);
			//	gt.ProcessTab.MaintenanceName = "permutaatio";
				result.Add(gt);
			}
			return result;
		}

		public List<int> GetGroupTabIds(int groupId) {
			if (Cache.Get(instance, "group_tab_ids_" + groupId) != null) {
				return (List<int>)Cache.Get(instance, "group_tab_ids_" + groupId);
			}

			SetDBParam(SqlDbType.Int, "@groupId", groupId);
			var result = new List<int>();
			var data = db.GetDatatable("SELECT process_tab_id FROM process_group_tabs WHERE process_group_id = @groupId ORDER BY order_no  ASC", DBParameters);

			foreach (DataRow row in data.Rows) {
				result.Add((int)row["process_tab_id"]);
			}
			Cache.Set(instance, "group_tab_ids_" + groupId, result);
			return result;
		}

		public void CopyTab(Dictionary<string, object> data) 
		{ 
			SetDBParam(SqlDbType.Int, "@from_tab_id", data["from_tab_id"]); 
			SetDBParam(SqlDbType.Int, "@to_group_id", data["to_group_id"]);
			SetDBParam(SqlDbType.Int, "@new_tab_id", data["new_tab_id"]);
			
			db.ExecuteStoredProcedure("EXEC app_ensure_copy_tab @instance, @process, @from_tab_id, @to_group_id, @new_tab_id", DBParameters); 				
		}
	}
}
