﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/ProcessGroupTabs")]
	public class ProcessGroupTabsRest : PageBase {
		public ProcessGroupTabsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessGroupTab> Get(string process) {
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			return groupTabs.GetGroupTabs();
		}

		[HttpGet("{process}/{processGroupId}")]
		public List<ProcessGroupTab> Get(string process, int processGroupId) {
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			return groupTabs.GetGroupTabs(processGroupId);
		}

		[HttpGet("{process}/{processGroupTabId}/{ProcessGroupId}/{ProcessTabId}")]

		public ProcessGroupTab Get(string process, int processGroupTabId, int ProcessGroupId, int ProcessTabId) {
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			return groupTabs.GetGroupTab(processGroupTabId, ProcessGroupId, ProcessTabId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]ProcessGroupTab groupTab) {
			CheckRight("write");
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				groupTabs.Add(groupTab);
				return new ObjectResult(groupTab);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]ProcessGroupTab groupTab) {
			CheckRight("write");
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				groupTabs.Save(groupTab);
				return new ObjectResult(groupTab);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}/{processGroupTabIds}")]
		public IActionResult Delete(string process, string processGroupTabIds) {
			CheckRight("delete");
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			foreach (var id in processGroupTabIds.Split(',')) {
				groupTabs.Delete(Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessTabGroups : PageBase {
		public ProcessTabGroups() : base("settings") { }

		[HttpGet("{process}/{processGroupTabId}")]
		public List<ProcessGroupTab> Get(string process, int processGroupTabId) {
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			return groupTabs.GetTabGroups(processGroupTabId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]ProcessGroupTab groupTab) {
			CheckRight("write");
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				groupTabs.Add(groupTab);
				return new ObjectResult(groupTab);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]ProcessGroupTab groupTab) {
			CheckRight("write");
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				groupTabs.Save(groupTab);
				return new ObjectResult(groupTab);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}/{processGroupTabId}")]
		public IActionResult Delete(string process, int processGroupTabId) {
			CheckRight("delete");
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			groupTabs.Delete(processGroupTabId);
			return new StatusCodeResult(200);
		}
	}
	
	[Route("v1/setting/[controller]")]
	public class ProcessGroupTabsCopy : PageBase {
		public ProcessGroupTabsCopy() : base("settings") { }

		[HttpPost("{process}")]
		public void Post(string process, [FromBody]Dictionary<string, object> data) {
			CheckRight("write");
			var groupTabs = new ProcessGroupTabs(instance, process, currentSession);
			groupTabs.CopyTab(data);
		}
	}
}