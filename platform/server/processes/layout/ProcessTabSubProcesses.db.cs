﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessTabSubProcess {
		public ProcessTabSubProcess() {

		}

		public ProcessTabSubProcess(DataRow row, AuthenticatedSession authenticatedSession, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ProcessTabSubProcessId = (int)row["process_tab_subprocess_id"];
			ProcessTabId = (int)row["process_tab_id"];
			OrderNo = (string)row["order_no"];
			ProcessSide = (byte)row["process_side"];
			Process = new Process(row, authenticatedSession);
		}

		public int ProcessTabSubProcessId { get; set; }
		public int ProcessTabId { get; set; }
		public string OrderNo { get; set; }
		public byte ProcessSide { get; set; }
		public Process Process { get; set; }
	}

	public class ProcessTabSubprocesses : ConnectionBaseProcess {
		private readonly AuthenticatedSession authenticatedSession;

		public ProcessTabSubprocesses(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public List<ProcessTabSubProcess> GetTabSubProcesses(int tabId) {
			var result = new List<ProcessTabSubProcess>();
			SetDBParam(SqlDbType.Int, "@tabId", tabId);

			foreach (DataRow row in db.GetDatatable("SELECT process_tab_subprocess_id, sp.process AS process, process_tab_id, order_no, variable, process_type, list_process, process_side FROM process_tabs_subprocesses sp INNER JOIN processes p ON sp.instance = p.instance AND sp.process = p.process WHERE process_tab_id = @tabId AND list_process = 0", DBParameters).Rows) {
				var tabSubProcess = new ProcessTabSubProcess(row, authenticatedSession);
				result.Add(tabSubProcess);
			}
			return result;
		}

		internal ProcessTabSubProcess GetSubProcess(int tabId, int processTabSubprocessId) {
			var result = new List<ProcessTabSubProcess>();

			SetDBParam(SqlDbType.Int, "@tabId", tabId);
			SetDBParam(SqlDbType.Int, "@processTabSubprocessId", processTabSubprocessId);
			var dt = db.GetDatatable("SELECT process_tab_subprocess_id, sp.process AS process, process_tab_id, order_no, variable, process_type, list_process, process_side FROM process_tabs_subprocesses sp INNER JOIN processes p ON sp.instance = p.instance AND sp.process = p.process WHERE process_tab_id = @tabId AND process_tab_subprocess_id = @processTabSubprocessId AND list_process = 0", DBParameters);

			if (dt.Rows.Count > 0) {
				var tabSubProcess = new ProcessTabSubProcess(dt.Rows[0], authenticatedSession);
				return tabSubProcess;
			}

			throw new CustomArgumentException("Sub Process cannot be found with process_tab_id " + tabId + " AND process_tab_subprocess_id " + processTabSubprocessId);
		}

		public void Save(ProcessTabSubProcess tabSubProcess) {
			SetDBParam(SqlDbType.NVarChar, "@subprocess", tabSubProcess.Process.ProcessName);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", tabSubProcess.OrderNo);
			SetDBParam(SqlDbType.Int, "@processTabSubprocessId", tabSubProcess.ProcessTabSubProcessId);
			SetDBParam(SqlDbType.TinyInt, "@processSide", tabSubProcess.ProcessSide);
			db.ExecuteUpdateQuery("UPDATE process_tabs_subprocesses SET process = @subprocess, order_no = @orderNo, process_side = @processSide WHERE process_tab_subprocess_id = @processTabSubprocessId", DBParameters);
		}

		public void Delete(int tabId, int processTabSubprocessId) {
			SetDBParam(SqlDbType.Int, "@tabId", tabId);
			SetDBParam(SqlDbType.Int, "@processTabSubProcessId", processTabSubprocessId);
			db.ExecuteDeleteQuery("DELETE FROM process_tabs_subprocesses WHERE process_tab_id = @tabId AND process_tab_subprocess_id = @processTabSubProcessId", DBParameters);
		}

		public void Add(ProcessTabSubProcess tabSubProcess) {
			SetDBParam(SqlDbType.NVarChar, "@subprocess", tabSubProcess.Process.ProcessName);
			SetDBParam(SqlDbType.Int, "@tabId", tabSubProcess.ProcessTabId);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", tabSubProcess.OrderNo);
			SetDBParam(SqlDbType.TinyInt, "@processSide", tabSubProcess.ProcessSide);
			db.ExecuteInsertQuery("INSERT INTO process_tabs_subprocesses (instance, process, process_tab_id, order_no, process_side) VALUES(@instance, @subprocess, @tabId, @orderNo, @processSide)", DBParameters);
		}
	}
}
