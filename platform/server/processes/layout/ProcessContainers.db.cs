﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessContainer {
		public ProcessContainer() { }

		public ProcessContainer(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ProcessContainerId = (int) dr[alias + "process_container_id"];
			Variable = Conv.ToStr(dr[alias + "variable"]);
			MaintenanceName = Conv.ToStr(dr[alias + "maintenance_name"]);
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
			RightContainer = Conv.ToInt(dr[alias + "right_container"]);
			HideTitle = Conv.ToInt(dr[alias + "hide_title"]);
		}

		public int ProcessContainerId { get; set; }
		public string Variable { get; set; }
		public string MaintenanceName { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public int RightContainer { get; set; }
		public int HideTitle { get; set; }
	}

	public class ProcessContainers : ProcessBase {
		public ProcessContainers(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance, process, authenticatedSession) { }

		public List<ProcessContainer> GetContainers() {
			var result = new List<ProcessContainer>();

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process_container_id, variable, maintenance_name, right_container, hide_title FROM process_containers WHERE instance = @instance AND process = @process",
					DBParameters).Rows) {
				var container = new ProcessContainer(row, authenticatedSession);
				result.Add(container);
			}

			return result;
		}

		public List<int> GetAccessContainerIds() {
			if (Cache.Get(instance, "container_access_ids_" + process) != null) 
				return (List<int>) Cache.Get(instance, "container_access_ids_" + process);
			
			var result = new List<int>();
			foreach (DataRow pc in db.GetDatatable(
				"SELECT pc.process_container_id FROM process_containers pc LEFT JOIN process_tab_containers ptc ON ptc.process_container_id = pc.process_container_id WHERE process = @process AND instance = @instance AND ptc.process_tab_id IS NULL and pc.right_container = 1",
				DBParameters).Rows) {
				result.Add(Conv.ToInt(pc["process_container_id"]));
			}

			Cache.Set(instance, "container_access_ids_" + process, result);
			return result;
		}

		public List<int> GetContainerIds() {
			var result = new List<int>();
			//Get Column Names
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process_container_id FROM process_containers WHERE instance = @instance AND process = @process",
					DBParameters).Rows) {
				result.Add((int) row["process_container_id"]);
			}

			return result;
		}


		private DataTable _containerColumnsCache = null;

		private DataTable GetContainerColumns =>
			_containerColumnsCache ?? (_containerColumnsCache =
				db.GetDatatable("SELECT process_container_id, item_column_id FROM process_container_columns"));

		public List<int> GetContainerIds(int itemColumnId) {
			var result = new List<int>();
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			foreach (DataRow row in GetContainerColumns.Select("item_column_id = " + itemColumnId)) {
				result.Add(Conv.ToInt(row["process_container_id"]));
			}

			return result;
		}

		public void Delete(int processContainerId) {
			SetDBParam(SqlDbType.Int, "processContainerId", processContainerId);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_containers WHERE instance = @instance AND process = @process AND process_container_id = @processContainerId",
				DBParameters);
			Cache.Remove(instance, "container_access_ids_" + process);
		}

		public ProcessContainer Add(ProcessContainer processContainer) {
			processContainer.ProcessContainerId = db.ExecuteInsertQuery(
				"INSERT INTO process_containers (instance, process) VALUES(@instance, @process)", DBParameters);
			if (processContainer.LanguageTranslation[authenticatedSession.locale_id] != null) {
				SetDBParam(SqlDbType.Int, "@processContainerId", processContainer.ProcessContainerId);
				SetDBParam(SqlDbType.Int, "@rightContainer", processContainer.RightContainer);
				SetDBParam(SqlDbType.Int, "@hideTitle", processContainer.HideTitle);
				SetDBParam(SqlDbType.NVarChar, "@variable",
					translations.GetTranslationVariable(process,
						processContainer.LanguageTranslation[authenticatedSession.locale_id], "CONT"));
				db.ExecuteUpdateQuery(
					"UPDATE process_containers SET variable = @variable, right_container = @rightContainer, hide_title = @hideTitle WHERE instance = @instance AND process = @process AND process_container_id = @processContainerId",
					DBParameters);
				var oldVal = translations.GetTranslationVariable(process,
					processContainer.LanguageTranslation[authenticatedSession.locale_id], "CONT");
				foreach (var translation in processContainer.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, oldVal, translation.Value, translation.Key);
				}
			}
			Cache.Remove(instance, "container_access_ids_" + process);
			return processContainer;
		}

		public ProcessContainer Save(ProcessContainer processContainer) {
			SetDBParam(SqlDbType.Int, "@processContainerId", processContainer.ProcessContainerId);
			SetDBParam(SqlDbType.NVarChar, "@maintenanceName", Conv.IfNullThenDbNull(processContainer.MaintenanceName));
			SetDBParam(SqlDbType.Int, "@rightContainer", processContainer.RightContainer);
			SetDBParam(SqlDbType.Int, "@hideTitle", processContainer.HideTitle);
			var oldVar =
				db.ExecuteScalar(
					"SELECT variable FROM process_containers WHERE instance = @instance AND process = @process AND process_container_id = @processContainerId",
					DBParameters);
			if (oldVar != null && oldVar != DBNull.Value &&
			    processContainer.LanguageTranslation[authenticatedSession.locale_id] != null) {
				foreach (var translation in processContainer.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string) oldVar, translation.Value,
						translation.Key);
				}
			} else if (oldVar == DBNull.Value &&
			           processContainer.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVar = translations.GetTranslationVariable(process,
					processContainer.LanguageTranslation[authenticatedSession.locale_id], "CONT");
				SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
				db.ExecuteUpdateQuery(
					"UPDATE process_containers SET variable = @variable WHERE instance = @instance AND process = @process AND process_container_id = @processContainerId",
					DBParameters);

				foreach (var translation in processContainer.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, Conv.ToStr(newVar), translation.Value,
						translation.Key);
				}
			}

			db.ExecuteUpdateQuery(
				"UPDATE process_containers SET maintenance_name = @maintenanceName, right_container = @rightContainer, hide_title = @hideTitle WHERE instance = @instance AND process = @process AND process_container_id = @processContainerId",
				DBParameters);
			Cache.Remove(instance, "container_access_ids_" + process);
			return processContainer;
		}

		public ProcessContainer GetContainer(int processContainerId) {
			var result = new List<ProcessContainer>();
			var containerIdParam = new SqlParameter();
			containerIdParam.ParameterName = "@containerId";
			containerIdParam.SqlDbType = SqlDbType.Int;
			containerIdParam.Value = processContainerId;
			DBParameters.Add(containerIdParam);
			var dt = db.GetDatatable(
				"SELECT process_container_id, variable, maintenance_name, right_container, hide_title FROM process_containers WHERE instance = @instance AND process = @process AND process_container_id = @containerId",
				DBParameters);
			if (dt.Rows.Count > 0) {
				var container = new ProcessContainer(dt.Rows[0], authenticatedSession);
				return container;
			}

			throw new CustomArgumentException("Container cannot be found using process_container_id " +
			                                  processContainerId);
		}

		public Dictionary<string, object> GetProcessContainer(int processContainerId) {
			var retval = new Dictionary<string, object>();

			var sublists = new Dictionary<string, List<string>>();
			var listData = new Dictionary<string, object>();
			var subListD = new Dictionary<string, object>();
			var inputs = new List<List<ProcessContainerField>>();
			var texts = new List<string>();

			SetDBParam(SqlDbType.Int, "@processContainerId", processContainerId);

			foreach (DataRow dr in db
				.GetDatatable(
					"SELECT process_container_id, variable FROM process_containers WHERE instance = @instance AND process = @process AND process_container_id = @processContainerId",
					DBParameters).Rows) {
				if ((int) dr["process_container_id"] > 0) {
					var containerColumns = new ProcesContainerColumns(instance, process, authenticatedSession);
					var containerFields = containerColumns.GetFields((int) dr["process_container_id"]);
					var processes = new Processes(instance, authenticatedSession);
					inputs.Add(containerFields);

					if (dr["variable"] != DBNull.Value) {
						texts.Add(
							new Translations(authenticatedSession.instance, authenticatedSession).GetTranslation(
								(string) dr["variable"]));
					} else {
						texts.Add("");
					}

					foreach (var col in containerFields) {
						if (col.ItemColumn.DataType == 6 && col.ItemColumn.DataAdditional != null) {
							var parents = processes.GetParentProcesses(true, col.ItemColumn.DataAdditional);
							if (parents.Count == 0) {
								var ib = new ItemCollectionService(instance, col.ItemColumn.DataAdditional,
									authenticatedSession);
								ib.FillItems();
								if (!listData.ContainsKey(col.ItemColumn.DataAdditional)) {
									listData.Add(col.ItemColumn.DataAdditional, ib.GetItems());
								}
							} else {
								foreach (var p in parents) {
									if (!listData.ContainsKey(col.ItemColumn.DataAdditional)) {
										if (!listData.ContainsKey(p.ProcessName)) {
											var ib = new ItemCollectionService(instance, p.ProcessName,
												authenticatedSession);
											ib.FillItems();
											listData.Add(col.ItemColumn.DataAdditional, ib.GetItems());
										}

										var subListData = new Dictionary<int, object>();
										foreach (var pData in (List<Dictionary<string, object>>) listData[p.ProcessName]
										) {
											var ib2 = new ItemCollectionService(instance, col.ItemColumn.DataAdditional,
												authenticatedSession);
											ib2.FillSubItems((int) pData["item_id"]);
											subListData.Add((int) pData["item_id"], ib2.GetItems());
										}

										listData.Add(col.ItemColumn.DataAdditional,
											new List<Dictionary<string, object>>());
										subListD.Add(col.ItemColumn.DataAdditional, subListData);
									}
								}
							}
						}
					}

					foreach (var l in listData.Keys) {
						if (!sublists.ContainsKey(l)) {
							sublists.Add(l, processes.GetSubProcesses(l));
						}
					}
				}
			}

			retval.Add("inputs", inputs);
			retval.Add("texts", texts);
			retval.Add("lists", listData);
			retval.Add("sublists", sublists);
			retval.Add("sublistsData", subListD);

			return retval;
		}

		public List<ProcessContainer> GetContainersNotInTabs() {
			var result = new List<ProcessContainer>();
			//Get Column Names
			foreach (DataRow row in db.GetDatatable(
				"SELECT process_container_id, variable, maintenance_name, right_container, hide_title FROM process_containers WHERE instance = @instance AND process = @process AND NOT EXISTS(SELECT variable FROM process_tab_containers WHERE process_containers.process_container_id = process_tab_containers.process_container_id)",
				DBParameters).Rows) {
				var container = new ProcessContainer(row, authenticatedSession);
				result.Add(container);
			}

			return result;
		}

		public List<int> GetContainerTabIds(int containerId) {
			if (Cache.Get(instance, "container_tab_ids_" + containerId) != null) {
				return (List<int>) Cache.Get(instance, "container_tab_ids_" + containerId);
			}

			SetDBParam(SqlDbType.Int, "@containerId", containerId);
			var result = new List<int>();

			var data = db.GetDatatable(
				"SELECT process_tab_id FROM process_tab_containers WHERE process_container_id = @containerId",
				DBParameters);

			foreach (DataRow row in data.Rows) {
				result.Add((int) row["process_tab_id"]);
			}

			Cache.Set(instance, "container_tab_ids_" + containerId, result);
			return result;
		}

		public ProcessContainer CopyProcessContainer(Dictionary<string, object> data) {
			var trans = Conv.ToStr(data["translation"]);

			SetDBParam(SqlDbType.Int, "@from_process_container_id", data["from_process_container_id"]);
			SetDBParam(SqlDbType.Int, "@to_process_tab_id", data["to_process_tab_id"]);
			SetDBParam(SqlDbType.Int, "@to_container_side", data["to_container_side"]);
			SetDBParam(SqlDbType.Int, "@from_process_tab_id", data["from_process_tab_id"]);
			SetDBParam(SqlDbType.NVarChar, "@new_container_order_no", data["to_order_no"]);

			var ProcessContainerId = db.ExecuteInsertQuery(
				"INSERT INTO process_containers (instance, process) VALUES(@instance, @process)", DBParameters);
			SetDBParam(SqlDbType.Int, "@processContainerId", ProcessContainerId);
			SetDBParam(SqlDbType.NVarChar, "@variable",
				translations.GetTranslationVariable(process,
					trans, "CONT"));
			SetDBParam(SqlDbType.Int, "@hide_title", Conv.ToInt(db.ExecuteScalar("SELECT hide_title FROM process_containers WHERE process_container_id = @from_process_container_id", DBParameters)));
			db.ExecuteUpdateQuery(
				"UPDATE process_containers SET variable = @variable, hide_title = @hide_title WHERE instance = @instance AND process = @process AND process_container_id = @processContainerId",
				DBParameters);

			var oldVal = translations.GetTranslationVariable(process,
				trans, "CONT");
			translations.insertOrUpdateProcessTranslation(process, oldVal, trans, authenticatedSession.locale_id);

			db.ExecuteInsertQuery(
				"INSERT INTO process_tab_containers (process_tab_id, process_container_id, container_side, order_no) VALUES (@to_process_tab_id, @processContainerId, @to_container_side, @new_container_order_no)",
				DBParameters);

			db.ExecuteStoredProcedure(
				"EXEC app_ensure_copy_container @instance, @process, @from_process_container_id, @processContainerId",
				DBParameters);

			Cache.Remove(instance, "container_access_ids_" + process);
			
			return GetContainer(ProcessContainerId);
		}
	}
}