﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessGridTile {
		public ProcessGridTile() { }

		public ProcessGridTile(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			Type = (int) row["type"];
			Id = (int) row["id"];
			Side = (int) row["side"];
			OrderNo = (string) row["order_no"];
			HideTitle = (int)row["hide_title"];
			tcId = (int) row["tcId"];
			
			if (HideTitle == 2) {
				LanguageTranslation = new Dictionary<string, string>();
			}
			else {
				LanguageTranslation = new Dictionary<string, string> {
					{
						session.locale_id,
						new Translations(session.instance, session).GetTranslation(Conv.ToStr(row["variable"]))
					}
				};
			}
		}

		public int Type { get; set; }
		public int Id { get; set; }
		public int tcId { get; set; }
		public int ContainerSide { get; set; }
		public int Side { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public string OrderNo { get; set; }
		public List<ProcessContainerField> fields { get; set; }
		public int HideTitle { get; set; }
	}

	public class ProcessGridTiles : ProcessBase {
		public ProcessGridTiles(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance, process, authenticatedSession) {
			//this.instance = instance;
			//this.process = process;
		}

		public List<ProcessGridTile> GetGridTiles(int groupId, int processTabId, DataTable gridData,
			FeatureStates rights = null) {
			var result = new List<ProcessGridTile>();
			foreach (var row in gridData.Select("process_tab_id = " + processTabId)) {
				if (rights != null) {
					var hasRight = false;
					var hasWriteRight =
						rights.groupStates.ContainsKey(groupId) && rights.groupStates[groupId].tabStates
							                                        .ContainsKey(processTabId)
						                                        && rights.groupStates[groupId].tabStates[processTabId]
							                                        .containerStates.ContainsKey((int) row["id"]) &&
						                                        rights.groupStates[groupId].tabStates[processTabId]
							                                        .containerStates[(int) row["id"]].s
							                                        .Contains("write");

					if (rights.groupStates.ContainsKey(groupId) && rights.groupStates[groupId].tabStates
						                                            .ContainsKey(processTabId)
					                                            && rights.groupStates[groupId].tabStates[processTabId]
						                                            .containerStates.ContainsKey((int) row["id"]) &&
					                                            rights.groupStates[groupId].tabStates[processTabId]
						                                            .containerStates[(int) row["id"]].s
						                                            .Contains("read")) {
						hasRight = true;
					}

					if (hasRight || hasWriteRight) {
						var gridTile = new ProcessGridTile(row, authenticatedSession);
						result.Add(gridTile);
					}
				} else {
					var gridTile = new ProcessGridTile(row, authenticatedSession);
					result.Add(gridTile);
				}
			}

			return result;
		}
	}
}