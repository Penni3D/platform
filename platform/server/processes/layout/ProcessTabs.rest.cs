﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/ProcessTabs")]
	public class ProcessTabsRest : PageBase {
		public ProcessTabsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessTab> Get(string process) {
			var tabs = new ProcessTabs(instance, process, currentSession);
			return tabs.GetTabs();
		}

		[HttpGet("{process}/{processTabId}")]
		public ProcessTab Get(string process, int processTabId) {
			var tabs = new ProcessTabs(instance, process, currentSession);
			return tabs.GetTab(processTabId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] ProcessTab processTab) {
			CheckRight("write");
			var processTabs = new ProcessTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				processTabs.Add(processTab);
				return new ObjectResult(processTab);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<ProcessTab> processTab) {
			CheckRight("write");
			var processTabs = new ProcessTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pt in processTab) {
					processTabs.Save(pt);
				}

				return new ObjectResult(processTab);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}/{processTabId}")]
		public IActionResult Delete(string process, string processTabId) {
			CheckRight("delete");
			var processTabs = new ProcessTabs(instance, process, currentSession);
			foreach (var id in processTabId.Split(',')) {
				processTabs.Delete(Conv.ToInt(id));
			}


			return new StatusCodeResult(200);
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessTabsButtons : PageBase {
		public ProcessTabsButtons() : base("settings") { }

		[HttpGet("{process}/{processTabId}")]
		public List<Dictionary<string, object>> Get(string process, int processTabId) {
			var tabs = new ProcessTabs(instance, process, currentSession);
			return tabs.GetTabButtons(processTabId);
		}


		[HttpPost("{process}/{processTabId}")]
		public IActionResult Post(string process, int processTabId, [FromBody] Dictionary<string, object> processTab) {
			CheckRight("write");
			var processTabs = new ProcessTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				processTabs.AddButtons(processTabId, processTab);
				return new ObjectResult(processTab);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}/{processTabId}")]
		public IActionResult Put(string process, int processTabId,
			[FromBody] List<Dictionary<string, object>> processTab) {
			CheckRight("write");
			var processTabs = new ProcessTabs(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pt in processTab) {
					processTabs.SaveButtons(processTabId, pt);
				}

				return new ObjectResult(processTab);
			}

			return null;
		}

		[HttpDelete("{process}/{processTabId}/{processTabColumnId}")]
		public IActionResult Delete(string process, string processTabId, string processTabColumnId) {
			CheckRight("delete");
			var processTabs = new ProcessTabs(instance, process, currentSession);
			foreach (var id in processTabColumnId.Split(',')) {
				processTabs.DeleteButtons(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessTabHierarchy : PageBase {
		public ProcessTabHierarchy() : base("settings") { }

		[HttpGet("{process}")]
		public Dictionary<int, ProcessTab> Get(string process) {
			{
				var tabs = new ProcessTabs(instance, process, currentSession);
				return tabs.GetTabHiearchy();
			}
		}

		[HttpGet("{process}/{itemId}")]
		public Dictionary<string, object> Get(string process, int itemId) {
			{
				var tabs = new ProcessTabs(instance, process, currentSession);
				return tabs.GetTabHiearchy(itemId);
			}
		}

		[HttpGet("{process}/{itemId}/{groupId}")]
		public Dictionary<string, object> Get(string process, int itemId, int? groupId) {
			{
				var tabs = new ProcessTabs(instance, process, currentSession);
				return tabs.GetTabHiearchy(itemId, groupId);
			}
		}
	}

	[Route("v1/settings/[controller]")]
	public class ProcessTabHierarchyLists : PageBase {
		public ProcessTabHierarchyLists() : base("settings") { }

		[HttpGet("{process}")]
		public Dictionary<string, object> Get(string process) {
			{
				var tabs = new ProcessTabs(instance, process, currentSession);
				return tabs.GetTabHiearchyLists();
			}
		}
	}
}