﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessSubProcess {
		public ProcessSubProcess() {

		}

		public ProcessSubProcess(DataRow row, AuthenticatedSession authenticatedSession, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			ParentProcess = (string)row["parent_process"];
			Process = new Process(row, authenticatedSession);
		}

		public string ParentProcess { get; set; }
		public Process Process { get; set; }
	}

	public class ProcessSubprocesses : ConnectionBaseProcess {
		private readonly AuthenticatedSession authenticatedSession;

		public ProcessSubprocesses(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public List<ProcessSubProcess> GetSubProcesses(string process) {
			var result = new List<ProcessSubProcess>();
			foreach (DataRow row in db.GetDatatable("WITH MyCTE AS(SELECT parent_process, process FROM process_subprocesses WHERE process = @process UNION ALL SELECT s.parent_process, s.process FROM process_subprocesses s INNER JOIN MyCTE ON s.process = MyCTE.parent_process) SELECT parent_process, p.* FROM MyCTE INNER JOIN processes p ON MyCTE.parent_process = p.process", DBParameters).Rows) {
				var subProcess = new ProcessSubProcess(row, authenticatedSession);
				result.Add(subProcess);
			}
			return result;
		}

		public List<string> GetSubProcessesArray(string process) {
			var result = new List<string>();
			foreach (DataRow row in db.GetDatatable("SELECT process FROM process_subprocesses WHERE parent_process = @process", DBParameters).Rows) {
				result.Add((string)row["process"]);
			}
			return result;
		}

		public void Save(ProcessSubProcess subProcess) {
			SetDBParam(SqlDbType.NVarChar, "@parent_process", subProcess.ParentProcess);
			db.ExecuteUpdateQuery("UPDATE process_subprocesses SET parent_process = @parent_process, WHERE instance = @instance AND process = @process", DBParameters);
		}

		public void Delete(string parentProcess) {
			SetDBParam(SqlDbType.NVarChar, "@parent_process", parentProcess);
			db.ExecuteDeleteQuery("DELETE FROM process_subprocesses WHERE instance = @instance, process = @process, parent_process = @parent_process", DBParameters);
		}

		public void Add(ProcessSubProcess subProcess) {
			SetDBParam(SqlDbType.NVarChar, "@parent_process", subProcess.ParentProcess);
			db.ExecuteInsertQuery("INSERT INTO process_subprocesses (instance, process, parent_process) VALUES(@instance, @process, @parent_process)", DBParameters);
		}
	}
}
