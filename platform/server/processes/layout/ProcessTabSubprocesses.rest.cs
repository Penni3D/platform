﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.layout {
	[Route("v1/settings/ProcessTabSubprocesses")]
	public class ProcessTabSubprocessesRest : PageBase {
		public ProcessTabSubprocessesRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}/{tabId}")]
		public List<ProcessTabSubProcess> Get(string process, int tabId) {
			var subprocesses = new ProcessTabSubprocesses(instance, process, currentSession);
			return subprocesses.GetTabSubProcesses(tabId);
		}

		[HttpGet("{process}/{tabId}/{processTabSubprocessId}")]
		public ProcessTabSubProcess Get(string process, int tabId, int processTabSubprocessId) {
			var subprocesses = new ProcessTabSubprocesses(instance, process, currentSession);
			return subprocesses.GetSubProcess(tabId, processTabSubprocessId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]ProcessTabSubProcess tabSubProcess) {
			CheckRight("write");
			var subprocesses = new ProcessTabSubprocesses(instance, process, currentSession);
			if (ModelState.IsValid) {
				subprocesses.Add(tabSubProcess);
				return new ObjectResult(tabSubProcess);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]ProcessTabSubProcess tabSubProcess) {
			CheckRight("write");
			var subprocesses = new ProcessTabSubprocesses(instance, process, currentSession);
			if (ModelState.IsValid) {
				subprocesses.Save(tabSubProcess);
				return new ObjectResult(tabSubProcess);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}/{tabId}/{processTabSubprocessId}")]
		public IActionResult Delete(string process, int tabId, string processTabSubprocessId) {
			CheckRight("delete");
			var subprocesses = new ProcessTabSubprocesses(instance, process, currentSession);
			foreach (var id in processTabSubprocessId.Split(',')) {
				subprocesses.Delete(tabId, Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}
}