﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessTab {
		public ProcessTab() { }

		public ProcessTab(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ProcessTabId = (int) dr[alias + "process_tab_id"];
			Variable = Conv.ToStr(dr[alias + "variable"]);
			MaintenanceName = Conv.ToStr(dr[alias + "maintenance_name"]);
			OrderNo = (string) dr[alias + "order_no"];
			UseInNewItem = (byte) dr[alias + "use_in_new_item"] > 0;
			ArchiveEnabled = (byte) dr[alias + "archive_enabled"] > 0;
			if (dr[alias + "archive_item_column_id"] != DBNull.Value) {
				ArchiveItemColumnId = Conv.ToInt(dr[alias + "archive_item_column_id"]);
			} else {
				ArchiveItemColumnId = null;
			}

			Background = Conv.ToStr(dr[alias + "background"]);
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
		}

		public int ProcessTabId { get; set; }
		public string Variable { get; set; }
		public string Tag { get; set; }
		public string MaintenanceName { get; set; }
		public string OrderNo { get; set; }
		public List<int> Columns { get; set; }
		public List<ProcessGridTile> grids { get; set; }
		public bool UseInNewItem { get; set; }
		public bool ArchiveEnabled { get; set; }
		public int? ArchiveItemColumnId { get; set; }
		public string Background { get; set; }

		public string ProcessTabGroups { get; internal set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
	}


	public class ProcessTabs : ProcessBase {
		public ProcessTabs(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance,
			process, authenticatedSession) {
			//this.instance = instance;
			//this.process = process;
		}

		public List<ProcessTab> GetTabs() {
			var result = new List<ProcessTab>();
			//Get Column Names
			//foreach (DataRow row in db.GetDatatable("SELECT process_tab_id, variable, order_no FROM process_tabs WHERE instance = @instance AND process = @process ORDER by order_no", DBParameters).Rows)

			foreach (DataRow row in db.GetDatatable(
				"SELECT pt.process_tab_id, pt.variable, pgt.tag, pt.order_no, pt.use_in_new_item, pt.archive_enabled, pt.archive_item_column_id, pt.background, pg.order_no as pg_order_no, pt.maintenance_name FROM process_groups pg " +
				" INNER JOIN process_group_tabs pgt ON pg.process_group_id = pgt.process_group_id " +
				" INNER JOIN process_tabs pt ON pt.process_tab_id = pgt.process_tab_id " +
				" WHERE pt.instance = @instance AND pt.process = @process " +
				" UNION SELECT pt.process_tab_id, pt.variable, NULL, pt.order_no, pt.use_in_new_item, pt.archive_enabled, pt.archive_item_column_id, pt.background, '9999' as pg_order_no, pt.maintenance_name as maintenance_name FROM process_tabs pt WHERE instance = @instance AND process = @process AND process_tab_id NOT IN (SELECT process_tab_id FROM process_group_tabs) " +
				" ORDER by pg_order_no, pt.order_no", DBParameters).Rows) {
				var tab = new ProcessTab(row, authenticatedSession);
				tab.ProcessTabGroups = GetTabGroups((int) row["process_tab_id"]);
				tab.Tag = Conv.ToStr(row["tag"]);

				//Conv.toStr(row["maintenance_name"]);
				result.Add(tab);
			}

			return result;
		}

		public List<int> GetTabIds() {
			var result = new List<int>();
			//Get Column Names
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process_tab_id FROM process_tabs WHERE instance = @instance AND process = @process",
					DBParameters).Rows) {
				result.Add((int) row["process_tab_id"]);
			}

			return result;
		}

		public Dictionary<string, object> GetTabHiearchy(int itemId, int? groupId = null, FeatureStates rights = null,
			List<ListNameAndCount> ignoreLists = null) {
			ignoreLists ??= new List<ListNameAndCount>();

			var retval = new Dictionary<string, object>();
			var result = new Dictionary<int, Dictionary<int, ProcessTab>>();

			//Get Column Names
			var gridTiles = new ProcessGridTiles(instance, process, authenticatedSession);
			var fields = new ProcesContainerColumns(instance, process, authenticatedSession);

			if (rights == null) {
				var st = new States(instance, authenticatedSession);
				rights = st.GetEvaluatedStates(process, "meta", itemId, new List<string> {"meta.read", "meta.write"});
			}

			string sql;
			if (groupId == null) {
				sql =
					"SELECT pg.process_group_id, pt.process_tab_id, pt.variable, pt.order_no, pt.use_in_new_item, pt.archive_enabled, pt.archive_item_column_id, pt.background, pg.order_no as pg_order_no, pt.maintenance_name FROM process_groups pg " +
					" INNER JOIN process_group_tabs pgt ON pg.process_group_id = pgt.process_group_id " +
					" INNER JOIN process_tabs pt ON pt.process_tab_id = pgt.process_tab_id " +
					" WHERE pt.instance = @instance AND pt.process = @process ";
			} else {
				SetDBParam(SqlDbType.Int, "@groupId", groupId);
				sql =
					"SELECT pg.process_group_id, pt.process_tab_id, pt.variable, pt.order_no, pt.use_in_new_item, pt.archive_enabled, pt.archive_item_column_id, pt.background, pg.order_no as pg_order_no, pt.maintenance_name FROM process_groups pg " +
					" INNER JOIN process_group_tabs pgt ON pg.process_group_id = pgt.process_group_id " +
					" INNER JOIN process_tabs pt ON pt.process_tab_id = pgt.process_tab_id " +
					" WHERE pt.instance = @instance AND pt.process = @process AND pg.process_group_id = @groupId ";
			}

			var gridData = db.GetDatatable(
				"SELECT 1 AS type,tc.process_tab_container_id AS tcId, tc.process_container_id AS id, tc.container_side as side, pc.hide_title, pc.variable, order_no  as order_no, tc.process_tab_id as process_tab_id " +
				" FROM process_tab_containers tc  " +
				"	INNER JOIN process_containers pc ON tc.process_container_id = pc.process_container_id  " +
				" WHERE instance = @instance AND process = @process   " +
				" UNION SELECT 2 AS type, 0 AS tcId, process_tab_subprocess_id AS id, process_side as side, 0 AS hide_title,'' as variable, order_no  as order_no , process_tabs_subprocesses.process_tab_id as process_tab_id FROM process_tabs_subprocesses " +
				" WHERE instance = @instance " +
				" ORDER BY order_no  ASC", DBParameters);

			var containerData = fields.GetData(instance, process);


			var dataRows = db.GetDatatable(sql, DBParameters);
			var listCols = new Dictionary<int, ItemColumn>();
			var linkCols = new List<string>();

			foreach (DataRow row in dataRows.Rows) {
				var tab = new ProcessTab(row, authenticatedSession, "");

				tab.grids = gridTiles.GetGridTiles(Conv.ToInt(row["process_group_id"]), tab.ProcessTabId, gridData,
					rights);
				foreach (var grid in tab.grids) {
					grid.fields = fields.GetFields(Conv.ToInt(row["process_group_id"]), tab.ProcessTabId, grid.Id,
						containerData,
						rights);

					foreach (var col in grid.fields) {
						if (col.ItemColumn.IsList() && col.ItemColumn.DataAdditional != null ||
						    col.ItemColumn.InputMethod == 1 && !string.IsNullOrEmpty(col.ItemColumn.InputName)) {
							if (!listCols.ContainsKey(col.ItemColumn.ItemColumnId))
								listCols.Add(col.ItemColumn.ItemColumnId, col.ItemColumn);
						} else if (col.ItemColumn.IsSubquery() &&
						           col.ItemColumn.IsOfSubType(ItemColumn.ColumnType.List)) {
							if (!listCols.ContainsKey(col.ItemColumn.ItemColumnId))
								listCols.Add(col.ItemColumn.ItemColumnId, col.ItemColumn);
						} else if (col.ItemColumn.IsEquation() && Conv.ToInt(col.ItemColumn.DataAdditional2) == 6) {
							if (!listCols.ContainsKey(col.ItemColumn.ItemColumnId))
								listCols.Add(col.ItemColumn.ItemColumnId, col.ItemColumn);
						}

						if (col.ItemColumn.LinkProcess == null) continue;
						if (!linkCols.Contains(col.ItemColumn.LinkProcess)) {
							linkCols.Add(col.ItemColumn.LinkProcess);
						}
					}
				}

				if (!result.ContainsKey(Conv.ToInt(row["process_group_id"])))
					result.Add(Conv.ToInt(row["process_group_id"]), new Dictionary<int, ProcessTab>());


				if (!result[Conv.ToInt(row["process_group_id"])].ContainsKey(tab.ProcessTabId))
					result[Conv.ToInt(row["process_group_id"])].Add(tab.ProcessTabId, tab);
			}

			var processes = new Processes(instance, authenticatedSession);
			var listData = new Dictionary<string, object>();
			var subListD = new Dictionary<string, object>();
			var sublists = new Dictionary<string, List<string>>();
			var c = (Dictionary<string, object>) Cache.Get(instance, "list_hierarchy_meta_" + process,
				Conv.ToStr(itemId) + authenticatedSession.item_id);

			var subprocesses = db.GetDatatable("SELECT parent_process FROM process_subprocesses", null);

			DataTable ignoreTable = null;
			if (ignoreLists.Count > 0) {
				var ignoreProcesses = new List<string>();
				foreach (var l in ignoreLists) {
					ignoreProcesses.Add("'" + Conv.ToSql(l.Name) + "'");
				}
				var countSql = "SELECT process,count(*) AS cnt FROM items WHERE process IN (" +
				               Modi.JoinListWithComma(ignoreProcesses) + ") GROUP BY process";
				
				ignoreTable = db.GetDatatable(countSql, null);
			}

			if (c == null) {
				foreach (var col in listCols.Values) {
					//Define process
					var colProcess = col.DataAdditional;
					if (col.IsSubquery() || col.IsEquation()) colProcess = col.SubDataAdditional;

					//Get parents
					var parents = new List<Process>();
					if (colProcess != null) parents = processes.GetParentProcesses(true, colProcess);

					if (parents.Count == 0) {
						if (IsListIgnored(colProcess, ignoreLists, ignoreTable) && subprocesses.Select("parent_process = '" + colProcess + "'").Length == 0) {
							if (listData.ContainsKey(colProcess))
								listData[colProcess] = null;
							else
								listData.Add(colProcess, null);
						} else {
							if (col.InputMethod == 0 && !listData.ContainsKey(colProcess)) {
								var listProcess = processes.GetProcess(colProcess);
								var ib = new ItemCollectionService(instance, colProcess, authenticatedSession);
								ib.FillItems(listProcess.ListOrder);
								listData.Add(colProcess, ib.GetItems());
							} else if (col.InputMethod == 1 && col.InputName.Length > 0) {
								var sl = new SystemList(instance, authenticatedSession);
								listData.Add(col.InputName, sl.GetListItems(col.InputName));
							}
						}
					} else {
						foreach (var p in parents) {
							if (!listData.ContainsKey(p.ProcessName)) {
								var ib = new ItemCollectionService(instance, p.ProcessName, authenticatedSession);
								var listProcess = processes.GetProcess(p.ProcessName);
								ib.FillItems(listProcess.ListOrder);
								listData.Add(p.ProcessName, ib.GetItems());
							}

							if (!subListD.ContainsKey(colProcess)) {
								var subListData = new Dictionary<int, object>();
								var ib2 = new ItemCollectionService(instance, colProcess, authenticatedSession);
								foreach (var pData in (List<Dictionary<string, object>>) listData[p.ProcessName]) {
									ib2.ClearMembers();

									var listProcess = processes.GetProcess(colProcess);
									ib2.FillSubItems((int) pData["item_id"], listProcess.ListOrder);

									var getItems1 = ib2.GetItems();

									//expensive operation -> designed for lists that are both sublist and recursive
									if (listProcess.ProcessType == 1) {
										foreach (var x in ib2.GetItems()) {
											FillRecursiveItems(getItems1, ib2, (int) x["item_id"], listProcess.ListOrder);
										}
									}

									subListData.Add((int) pData["item_id"], getItems1);
								}

								subListD.Add(colProcess, subListData);
							}
						}
					}
				}

				foreach (var col in listCols.Values) {
					if (!col.IsSubquery() && col.DataAdditional != null) {
						if (!sublists.ContainsKey(col.DataAdditional)) {
							sublists.Add(col.DataAdditional, processes.GetSubProcesses(col.DataAdditional));
						}

						if (!listData.ContainsKey(col.DataAdditional)) {
							listData.Add(col.DataAdditional, new List<Dictionary<string, object>>());
						}
					} else if (col.IsSubquery()) {
						if (!listData.ContainsKey(col.SubDataAdditional)) {
							var listProcess = processes.GetProcess(col.SubDataAdditional);
							var ib = new ItemCollectionService(instance, col.SubDataAdditional, authenticatedSession);
							ib.FillItems(listProcess.ListOrder);
							listData.Add(col.SubDataAdditional, ib.GetItems());
						}
					}
				}
			} else {
				//Ignore list
				listData = (Dictionary<string, object>) c["lists"];
				if (listData != null) {
					foreach (var col in listCols.Values) {
						var colProcess = col.DataAdditional;
						if (col.InputMethod == 1 && col.InputName.Length > 0) colProcess = col.InputName;
						if (col.IsSubquery() || col.IsEquation()) colProcess = col.SubDataAdditional;
						
						if (IsListIgnored(colProcess, ignoreLists, ignoreTable) && listData.ContainsKey(colProcess)) { 
							//Opening record, has cache and ignore list
							listData[colProcess] = null;	
						} else { 
							//Reloading client, has cache but no ignore list -- get lists from DB
							if (listData.ContainsKey(colProcess) && listData[colProcess] == null) {
								if (col.InputMethod == 0) {
									var listProcess = processes.GetProcess(colProcess);
									var ib = new ItemCollectionService(instance, colProcess, authenticatedSession);
									ib.FillItems(listProcess.ListOrder);
									listData[colProcess] = ib.GetItems();
								} else if (col.InputMethod == 1 && col.InputName.Length > 0) {
									var sl = new SystemList(instance, authenticatedSession);
									listData[col.InputName] = sl.GetListItems(col.InputName);
								}
							}
						}
					}
				}

				subListD = (Dictionary<string, object>) c["sublistsData"];
				sublists = (Dictionary<string, List<string>>) c["sublists"];
			}

			var linkColsD = new Dictionary<string, object>();
			foreach (var p in linkCols) {
				var ics = new ItemColumns(instance, p, authenticatedSession);
				linkColsD.Add(p, ics.GetItemColumns());
			}

			retval.Add("hierarchy", result);
			retval.Add("lists", listData);
			retval.Add("sublistsData", subListD);
			retval.Add("sublists", sublists);
			retval.Add("linkCols", linkColsD);

			Cache.Set(instance, "list_hierarchy_meta_" + process, Conv.ToStr(itemId) + authenticatedSession.item_id,
				retval);
			return retval;
		}

		private bool IsListIgnored(string colProcess, List<ListNameAndCount> ignoreLists, DataTable ignoreTable) {
			foreach (var l in ignoreLists) {
				if (l.Name != colProcess) continue;
				var countCol = ignoreTable.Select("process = '" + colProcess + "'");
				if (countCol.Length > 0) return Conv.ToInt(countCol[0]["cnt"]) == l.Count;
				return false;
			}
			return false;
		}

		private void FillRecursiveItems(List<Dictionary<string, object>> getItems1, ItemCollectionService ib2,
			int itemId, string order) {
			ib2.ClearMembers();
			ib2.FillSubItems(itemId, order);

			foreach (var x2 in ib2.GetItems()) {
				if (!getItems1.Contains(x2)) {
					getItems1.Add(x2);
				}

				FillRecursiveItems(getItems1, ib2, (int) x2["item_id"], order);
			}
		}

		public Dictionary<int, ProcessTab> GetTabHiearchy() {
			var result = new Dictionary<int, ProcessTab>();
			var gridTiles = new ProcessGridTiles(instance, process, authenticatedSession);
			var fields = new ProcesContainerColumns(instance, process, authenticatedSession);

			var gridData = db.GetDatatable(
				"SELECT 1 AS type,tc.process_container_id AS id, tc.container_side as side, pc.variable, order_no  as order_no, tc.process_tab_id as process_tab_id " +
				" FROM process_tab_containers tc  " +
				"	INNER JOIN process_containers pc ON tc.process_container_id = pc.process_container_id  " +
				" WHERE instance = @instance AND process = @process   " +
				" UNION SELECT 2 AS type, process_tab_subprocess_id AS id, process_side as side,'' as variable, order_no  as order_no, process_tabs_subprocesses.process_tab_id as process_tab_id FROM process_tabs_subprocesses " +
				" WHERE instance = @instance " +
				" ORDER BY order_no  ASC", DBParameters);

			foreach (DataRow row in db.GetDatatable(
				"SELECT pg.process_group_id, pt.process_tab_id, pt.variable, pt.order_no, pt.use_in_new_item, pt.archive_enabled, pt.archive_item_column_id, pt.background, pg.order_no as pg_order_no FROM process_groups pg " +
				" INNER JOIN process_group_tabs pgt ON pg.process_group_id = pgt.process_group_id " +
				" INNER JOIN process_tabs pt ON pt.process_tab_id = pgt.process_tab_id " +
				" WHERE pt.instance = @instance AND pt.process = @process ", DBParameters).Rows) {
				var tab = new ProcessTab(row, authenticatedSession);
				tab.grids = gridTiles.GetGridTiles((int) row["process_group_id"], tab.ProcessTabId, gridData);
				foreach (var grid in tab.grids) {
					grid.fields = fields.GetFields(grid.Id);
				}

				if (!result.ContainsKey(tab.ProcessTabId)) result.Add(tab.ProcessTabId, tab);
			}

			return result;
		}

		public ProcessTab GetTab(int processTabId) {
			SetDBParam(SqlDbType.Int, "processTabId", processTabId);
			var dt = db.GetDatatable(
				"SELECT process_tab_id, variable, order_no, use_in_new_item, archive_enabled, archive_item_column_id, background, maintenance_name FROM process_tabs WHERE instance = @instance AND process = @process AND process_tab_id = @processTabId ORDER BY order_no  ASC",
				DBParameters);

			if (dt.Rows.Count > 0) {
				var tab = new ProcessTab(dt.Rows[0], authenticatedSession);
				tab.Columns = new List<int>();
				var dt2 =
					db.GetDatatable(
						"SELECT item_column_id FROM process_tab_columns WHERE process_tab_id = @processTabId ORDER BY order_no  ASC",
						DBParameters);
				foreach (DataRow dr in dt2.Rows) {
					tab.Columns.Add((int) dr["item_column_id"]);
				}

				return tab;
			} else {
				var tab = new ProcessTab();
				return tab;
			}
		}

		public string GetTabGroups(int processTabId) {
			var retval = "";

			SetDBParam(SqlDbType.Int, "@processTabId", processTabId);
			SetDBParam(SqlDbType.NVarChar, "@locale_id", authenticatedSession.locale_id);
			foreach (DataRow row in db.GetDatatable(
				"SELECT process_translations.translation FROM process_group_tabs INNER JOIN process_groups ON process_groups.process_group_id = process_group_tabs.process_group_id INNER JOIN process_translations ON process_translations.variable = process_groups.variable WHERE process_groups.instance = @instance AND process_groups.process = @process AND process_tab_id = @processTabId AND process_translations.code = @locale_id ORDER BY process_translations.translation ASC",
				DBParameters).Rows) {
				retval += row["translation"] + ", ";
			}

			return retval;
		}

		public List<int> GetTabIdsForContainer(int containerId) {
			var retval = new List<int>();
			SetDBParam(SqlDbType.Int, "containerId", containerId);
			var dt =
				db.GetDatatable(
					"SELECT process_tab_id FROM process_tab_containers WHERE process_container_id = @containerId",
					DBParameters);
			foreach (DataRow r in dt.Rows) {
				retval.Add((int) r["process_tab_id"]);
			}

			return retval;
		}

		public void Delete(int processTabId) {
			SetDBParam(SqlDbType.Int, "processTabId", processTabId);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_tabs WHERE instance = @instance AND process = @process AND process_tab_id = @processTabId",
				DBParameters);
		}

		public void Save(ProcessTab processTab) {
			SetDBParam(SqlDbType.NVarChar, "@orderNo", processTab.OrderNo);
			SetDBParam(SqlDbType.TinyInt, "@useInNewItem", processTab.UseInNewItem);
			SetDBParam(SqlDbType.TinyInt, "@archiveEnabled", processTab.ArchiveEnabled);
			SetDBParam(SqlDbType.NVarChar, "@maintenanceName", Conv.IfNullThenDbNull(processTab.MaintenanceName));
			SetDBParam(SqlDbType.Int, "@archiveItemColumnId", Conv.IfNullThenDbNull(processTab.ArchiveItemColumnId));
			SetDBParam(SqlDbType.NVarChar, "@background", Conv.IfNullThenDbNull(processTab.Background));
			SetDBParam(SqlDbType.Int, "@processTabId", processTab.ProcessTabId);
			db.ExecuteUpdateQuery(
				"UPDATE process_tabs SET order_no = @orderNo, use_in_new_item=@useInNewItem, archive_enabled=@archiveEnabled, archive_item_column_id=@archiveItemColumnId, background = @background, maintenance_name = @maintenanceName WHERE instance = @instance AND process = @process AND process_tab_id = @processTabId",
				DBParameters);
			var oldVar =
				db.ExecuteScalar(
					"SELECT variable FROM process_tabs WHERE instance = @instance AND process = @process AND process_tab_id = @processTabId",
					DBParameters);
			if (oldVar != DBNull.Value && processTab.LanguageTranslation[authenticatedSession.locale_id] != null) {
				foreach (var translation in processTab.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string) oldVar, translation.Value,
						translation.Key);
				}
			} else if (oldVar == DBNull.Value &&
			           processTab.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVariable = translations.GetTranslationVariable(process,
					processTab.LanguageTranslation[authenticatedSession.locale_id], "TAB");
				SetDBParam(SqlDbType.NVarChar, "@variable", newVariable);
				db.ExecuteUpdateQuery(
					"UPDATE process_tabs SET variable=@variable WHERE instance = @instance AND process = @process AND process_tab_id = @processTabId",
					DBParameters);
				foreach (var translation in processTab.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVariable, translation.Value,
						translation.Key);
				}
			}

			if (processTab.Columns != null) {
				db.ExecuteDeleteQuery("DELETE FROM process_tab_columns WHERE process_tab_id = @processTabId",
					DBParameters);
				foreach (var itemColumnId in processTab.Columns) {
					SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
					db.ExecuteInsertQuery(
						"INSERT INTO process_tab_columns (process_tab_id, item_column_id) VALUES(@processTabId, @itemColumnId)",
						DBParameters);
				}
			}
		}

		public List<Dictionary<string, object>> GetTabButtons(int processTabId) {
			var result = new List<Dictionary<string, object>>();
			SetDBParam(SqlDbType.Int, "@processTabId", processTabId);

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process_tab_column_id, item_column_id, order_no FROM process_tab_columns WHERE process_tab_id = @processTabId ORDER BY order_no  ASC",
					DBParameters).Rows) {
				var retval = new Dictionary<string, object>();
				retval.Add("ProcessTabColumnId", row["process_tab_column_id"]);
				retval.Add("ItemColumnId", row["item_column_id"]);
				retval.Add("OrderNo", row["order_no"]);
				result.Add(retval);
			}

			return result;
		}

		public Dictionary<string, object> AddButtons(int processTabId, Dictionary<string, object> processTab) {
			SetDBParam(SqlDbType.NVarChar, "@orderNo", processTab["OrderNo"]);
			SetDBParam(SqlDbType.Int, "@processTabId", processTabId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", processTab["ItemColumnId"]);
			processTab.Add("ProcessTabColumnId",
				db.ExecuteInsertQuery(
					"INSERT INTO process_tab_columns (process_tab_id, item_column_id, order_no) VALUES(@processTabId, @itemColumnId, @orderNo)",
					DBParameters));
			return processTab;
		}

		public void SaveButtons(int ProcessTabId, Dictionary<string, object> processTab) {
			SetDBParam(SqlDbType.NVarChar, "@orderNo", processTab["OrderNo"]);
			SetDBParam(SqlDbType.Int, "@itemColumnId", processTab["ItemColumnId"]);
			SetDBParam(SqlDbType.Int, "@processTabColumnId", processTab["ProcessTabColumnId"]);
			db.ExecuteUpdateQuery(
				"UPDATE process_tab_columns SET item_column_id = @itemColumnId, order_no = @orderNo WHERE process_tab_column_id = @processTabColumnId",
				DBParameters);
		}

		public void DeleteButtons(int processTabColumnId) {
			SetDBParam(SqlDbType.Int, "@processTabColumnId", processTabColumnId);
			db.ExecuteInsertQuery("DELETE FROM process_tab_columns WHERE process_tab_column_id = @processTabColumnId",
				DBParameters);
		}

		public void Add(ProcessTab processTab) {
			SetDBParam(SqlDbType.NVarChar, "@orderNo", processTab.OrderNo);
			SetDBParam(SqlDbType.NVarChar, "@maintenanceName", Conv.IfNullThenDbNull(processTab.MaintenanceName));
			SetDBParam(SqlDbType.TinyInt, "@useInNewItem", processTab.UseInNewItem);
			SetDBParam(SqlDbType.TinyInt, "@archiveEnabled", processTab.ArchiveEnabled);
			SetDBParam(SqlDbType.Int, "@archiveItemColumnId", Conv.IfNullThenDbNull(processTab.ArchiveItemColumnId));
			SetDBParam(SqlDbType.NVarChar, "@background", Conv.IfNullThenDbNull(processTab.Background));

			processTab.ProcessTabId = db.ExecuteInsertQuery(
				"INSERT INTO process_tabs (instance, process, order_no, use_in_new_item, archive_enabled, archive_item_column_id, maintenance_name, background) VALUES(@instance, @process, @orderNo, @useInNewItem, @archiveEnabled, @archiveItemColumnId, @maintenanceName, @background)",
				DBParameters);

			if (processTab.LanguageTranslation[authenticatedSession.locale_id] != null) {
				SetDBParam(SqlDbType.NVarChar, "@variable",
					translations.GetTranslationVariable(process,
						processTab.LanguageTranslation[authenticatedSession.locale_id],
						"TAB"));
				SetDBParam(SqlDbType.Int, "@processTabId", processTab.ProcessTabId);
				db.ExecuteUpdateQuery(
					"UPDATE process_tabs SET variable=@variable WHERE instance = @instance AND process = @process AND process_tab_id = @processTabId",
					DBParameters);
				var newVariable = translations.GetTranslationVariable(process,
					processTab.LanguageTranslation[authenticatedSession.locale_id], "TAB");
				foreach (var translation in processTab.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVariable, translation.Value,
						translation.Key);
				}
			}

			// db.ExecuteDeleteQuery("DELETE FROM process_tab_columns WHERE process_tab_id = @processTabId", DBParameters);
			if (processTab.Columns != null) {
				foreach (var itemColumnId in processTab.Columns) {
					SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
					db.ExecuteInsertQuery(
						"INSERT INTO process_tab_columns (process_tab_id, item_column_id) VALUES(@processTabId, @itemColumnId)",
						DBParameters);
				}
			}
		}

		public Dictionary<string, object> GetTabHiearchyLists() {
			var retval = new Dictionary<string, object>();

			var listCols = new List<object>();
			var listData = new Dictionary<string, object>();
			var subListD = new Dictionary<string, object>();
			var sublists = new Dictionary<string, List<string>>();

			var processes = new Processes(instance, authenticatedSession);

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT data_type, data_additional, item_column_id FROM item_columns WHERE process = @process AND data_type = 6 AND data_additional != ''",
					DBParameters).Rows) {
				if (Conv.ToInt(row["data_type"]) == 6 && Conv.ToStr(row["data_additional"]) != null) {
					var parents = processes.GetParentProcesses(true, (string) row["data_additional"]);
					if (parents.Count == 0) {
						var ib =
							new ItemCollectionService(instance, (string) row["data_additional"], authenticatedSession);
						ib.FillItems();
						if (!listData.ContainsKey((string) row["data_additional"])) {
							listData.Add((string) row["data_additional"], ib.GetItems());
						}
					} else {
						foreach (var p in parents) {
							if (!listData.ContainsKey((string) row["data_additional"])) {
								if (!listData.ContainsKey(p.ProcessName)) {
									var ib = new ItemCollectionService(instance, p.ProcessName, authenticatedSession);
									ib.FillItems();
									listData.Add((string) row["data_additional"], ib.GetItems());
								}

								var subListData = new Dictionary<int, object>();
								foreach (var pData in (List<Dictionary<string, object>>) listData[p.ProcessName]) {
									var ib2 =
										new ItemCollectionService(instance, (string) row["data_additional"],
											authenticatedSession);
									ib2.FillSubItems((int) pData["item_id"]);
									subListData.Add((int) pData["item_id"], ib2.GetItems());
								}

								listData.Add((string) row["data_additional"], new List<Dictionary<string, object>>());
								subListD.Add((string) row["data_additional"], subListData);
							}
						}
					}
				}
			}

			foreach (var l in listData.Keys) {
				sublists.Add(l, processes.GetSubProcesses(l));
			}

			retval.Add("lists", listData);
			retval.Add("sublistsData", subListD);
			retval.Add("sublists", sublists);

			return retval;
		}
	}
}