﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.processes.layout {
	public class ProcessTabContainer {
		public ProcessTabContainer() {

		}

		public ProcessTabContainer(DataRow dr, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			ProcessTabContainerId = (int)dr[alias + "process_tab_container_id"];
			OrderNo = (string)dr[alias + "order_no"];
			ProcessContainerSide = (byte)dr[alias + "container_side"];
		}

		public int ProcessTabContainerId { get; set; }
		public ProcessTab ProcessTab { get; set; }
		public ProcessContainer ProcessContainer { get; set; }
		public string OrderNo { get; set; }
		public byte ProcessContainerSide { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
	}

	public class ProcessTabContainers : ConnectionBaseProcess {
		private readonly AuthenticatedSession authenticatedSession;

		public ProcessTabContainers(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public List<ProcessTabContainer> GetTabContainers(int tabId = 0) {
			SetDBParam(SqlDbType.Int, "@tabId", tabId);

			var result = new List<ProcessTabContainer>();
			var where = " AND ptc.process_tab_id = @tabId";
			if(tabId == 0) {
				where = "";
			}

			var data = db.GetDatatable("SELECT pc.hide_title AS pc_hide_title, pc.right_container AS pc_right_container, ptc.process_tab_container_id AS ptc_process_tab_container_id, pt.maintenance_name AS pt_maintenance_name, pc.maintenance_name AS pc_maintenance_name, pt.process_tab_id AS pt_process_tab_id, pt.order_no AS pt_order_no, pt.use_in_new_item as pt_use_in_new_item, pt.archive_enabled as pt_archive_enabled, pt.archive_item_column_id as pt_archive_item_column_id, pt.background as pt_background, pc.process_container_id AS pc_process_container_id, pt.variable as pt_variable, pc.variable AS pc_variable, ptc.order_no  AS ptc_order_no, ptc.container_side AS ptc_container_side FROM process_tabs pt " +
				" INNER JOIN process_tab_containers ptc ON pt.process_tab_id = ptc.process_tab_id " +
				" INNER JOIN process_containers pc ON ptc.process_container_id = pc.process_container_id" +
				" WHERE pt.instance = @instance AND pt.process = @process " + where + " ORDER BY ptc_order_no ASC", DBParameters);

			foreach (DataRow row in data.Rows) {
				var tabContainer = new ProcessTabContainer(row, "ptc");
				tabContainer.ProcessTab = new ProcessTab(row, authenticatedSession, "pt");
				tabContainer.ProcessContainer = new ProcessContainer(row, authenticatedSession, "pc");
				tabContainer.ProcessContainer.LanguageTranslation = new Translations(authenticatedSession.instance, authenticatedSession).GetTranslations(tabContainer.ProcessContainer.Variable);

				result.Add(tabContainer);
			}
			return result;
		}

		public void Add(ProcessTabContainer tabContainer) {
			var p = new List<SqlParameter> {
				GetDBParam(SqlDbType.Int, "@tabId", tabContainer.ProcessTab.ProcessTabId),
				GetDBParam(SqlDbType.Int, "@containerId", tabContainer.ProcessContainer.ProcessContainerId),
				GetDBParam(SqlDbType.Int, "@container_side", tabContainer.ProcessContainerSide),
				GetDBParam(SqlDbType.NVarChar, "@orderNo", tabContainer.OrderNo)
			};
			tabContainer.ProcessTabContainerId = db.ExecuteInsertQuery("INSERT INTO process_tab_containers (process_tab_id, process_container_id, order_no, container_side) VALUES(@tabId, @containerId, @orderNo, @container_side)", p);
			
			Cache.Remove(instance, "tab_container_ids_" + tabContainer.ProcessTab.ProcessTabId);
			Cache.Remove(instance, "container_access_ids_" + process);
		}

		public void Save(ProcessTabContainer tabContainer) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@tabId", tabContainer.ProcessTab.ProcessTabId));
			p.Add(GetDBParam(SqlDbType.Int, "@containerId", tabContainer.ProcessContainer.ProcessContainerId));
			p.Add(GetDBParam(SqlDbType.Int, "@container_side", tabContainer.ProcessContainerSide));
			p.Add(GetDBParam(SqlDbType.NVarChar, "@orderNo", tabContainer.OrderNo));
			p.Add(GetDBParam(SqlDbType.Int, "@tabContainerId", tabContainer.ProcessTabContainerId));
			db.ExecuteUpdateQuery("UPDATE process_tab_containers SET process_tab_id = @tabId, process_container_id = @containerId, order_no = @orderNo, container_side = @container_side WHERE process_tab_container_id = @tabContainerId", p);

			var containers = new ProcessContainers(instance, process, authenticatedSession);
			containers.Save(tabContainer.ProcessContainer);
			Cache.Remove(instance, "tab_container_ids_" + tabContainer.ProcessTab.ProcessTabId);
			Cache.Remove(instance, "container_access_ids_" + process);
		}

		public void Delete(int processTabContainerId) {
			var p = new List<SqlParameter>();
			p.Add(GetDBParam(SqlDbType.Int, "@tabContainerId", processTabContainerId));
			db.ExecuteDeleteQuery("DELETE FROM process_tab_containers WHERE process_tab_container_id = @tabContainerId", p);
			Cache.Remove(instance, "tab_container_ids_" + processTabContainerId);
			Cache.Remove(instance, "container_access_ids_" + process);
		}

		public ProcessTabContainer GetContainer(int processTabContainerId, int processTabId, int processContainerId) {
			SetDBParam(SqlDbType.Int, "@tabContainerId", processTabContainerId);
			SetDBParam(SqlDbType.Int, "@processTabId", processTabId);
			SetDBParam(SqlDbType.Int, "@processContainerId", processContainerId);

			var data = db.GetDatatable("SELECT ptc.process_tab_container_id AS ptc_process_tab_container_id, pt.process_tab_id AS pt_process_tab_id, pt.order_no AS pt_order_no, pt.use_in_new_item as pt_use_in_new_item, pt.archive_enabled as pt_archive_enabled, pt.archive_item_column_id as pt_archive_item_column_id, pt.background as pt_background, pc.process_container_id AS pc_process_container_id, pt.variable AS pt_variable, pc.variable AS pc_variable, pc.right_container AS pc_right_container, pc.hide_title AS pc_hide_title, ptc.order_no  AS ptc_order_no, ptc.container_side AS ptc_container_side FROM process_tabs pt " +
				" INNER JOIN process_tab_containers ptc ON pt.process_tab_id = ptc.process_tab_id " +
				" INNER JOIN process_containers pc ON ptc.process_container_id = pc.process_container_id" +
				" WHERE pt.instance = @instance AND pt.process = @process AND ptc.process_tab_container_id = @tabContainerId AND pt.process_tab_id = @processTabId AND ptc.process_container_id = @processContainerId ", DBParameters);

			if (data.Rows.Count > 0) {
				var tabContainer = new ProcessTabContainer(data.Rows[0], "ptc");
				tabContainer.ProcessTab = new ProcessTab(data.Rows[0], authenticatedSession, "pt");
				tabContainer.ProcessContainer = new ProcessContainer(data.Rows[0], authenticatedSession, "pc");
				return tabContainer;
			}

			throw new CustomArgumentException("Container cannot be found using process_tab_container_id " + processTabContainerId + " AND process_tab_id " + processTabId + " process_container_id " + processContainerId);
		}

		public List<int> GetTabContainerIds(int tabId) {
			if (Cache.Get(instance, "tab_container_ids_" + tabId) != null) {
				return (List<int>)Cache.Get(instance, "tab_container_ids_" + tabId);
			}

			SetDBParam(SqlDbType.Int, "@tabId", tabId);
			var result = new List<int>();

			var data = db.GetDatatable("SELECT process_container_id FROM process_tab_containers WHERE process_tab_id = @tabId", DBParameters);

			foreach (DataRow row in data.Rows) {
				result.Add((int)row["process_container_id"]);
			}
			Cache.Set(instance, "tab_container_ids_" + tabId, result);
			return result;
		}

		public List<int> GetTabGroupIds(int tabId) {
			if (Cache.Get(instance, "tab_group_ids_" + tabId) != null) {
				return (List<int>)Cache.Get(instance, "tab_group_ids_" + tabId);
			}

			SetDBParam(SqlDbType.Int, "@tabId", tabId);
			var result = new List<int>();

			var data = db.GetDatatable("SELECT process_group_id FROM process_group_tabs WHERE process_tab_id = @tabId", DBParameters);

			foreach (DataRow row in data.Rows) {
				result.Add((int)row["process_group_id"]);
			}
			Cache.Set(instance, "tab_group_ids_" + tabId, result);
			return result;
		}
	}
}
