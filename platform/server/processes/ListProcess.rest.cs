﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/meta/ListProcesses")]
	public class ListProcessesRest : PageBase {
		public ListProcessesRest() : base("lists", true) { }

		// GET: model/values
		[HttpGet("")]
		public List<Process> Get(int processType) {
			var process = new Processes(instance, currentSession);
			return process.GetListProcesses();
		}

		[HttpGet("hierarchy/{pageType}")]
		public List<Processes.HierarchyResult> GetHierarchy(int pageType) {
			var process = new Processes(instance, currentSession);
			return process.GetListsHierarchy(pageType);
		}

		[HttpGet("recursive/{orderBy}")]
		public Dictionary<string, Dictionary<string, List<string>>> RecursiveListRelations(string orderBy) {
			var process = new Processes(instance, currentSession);
			return process.GetRecursiveListRelations(orderBy);
		}


		[HttpGet("all/{processType}")]
		public List<Process> GetByType(string processType) {
			var process = new Processes(instance, currentSession);
			return process.GetListProcesses(processType);
		}

		[HttpGet("{processType}/{process}")]
		public List<Process> Get(int processType, string process) {
			if (processType == 1) {
				var processes = new Processes(instance, currentSession);
				return processes.GetListProcesses(process);
			}

			if (processType == 2) {
				var processes = new Processes(instance, currentSession);
				return processes.GetParentProcesses(true, process);
			}

			if (processType == 3) {
				var processes = new Processes(instance, currentSession);
				return processes.GetParentProcessesWithSubProcess(true, process);
			} else {
				var processes = new Processes(instance, currentSession);
				return processes.GetListProcesses(process);
			}
		}

		[HttpGet("{processType}/{process}/new")]
		public List<Process> GetNew(int processType, string process) {
			if (processType == 1) {
				var processes = new Processes(instance, currentSession);
				return processes.GetListProcesses(process);
			}

			if (processType == 2) {
				var processes = new Processes(instance, currentSession);
				return processes.GetParentProcesses2(true, process);
			}

			if (processType == 3) {
				var processes = new Processes(instance, currentSession);
				return processes.GetParentProcessesWithSubProcess(true, process);
			} else {
				var processes = new Processes(instance, currentSession);
				return processes.GetListProcesses(process);
			}
		}

		[HttpPost]
		public IActionResult Post([FromBody] Process process) {
			CheckRight("write");
			var processes = new Processes(instance, currentSession);
			if (ModelState.IsValid) {
				processes.Add(0, process);
				return new ObjectResult(process);
			}

			return null;
		}

		[HttpPut]
		public IActionResult Put([FromBody] List<Process> process) {
			CheckRight("write");
			var processes = new Processes(instance, currentSession);
			if (ModelState.IsValid) {
				foreach (var pc in process) {
					processes.Save(pc);
				}

				return new ObjectResult(process);
			}

			return null;
		}

		[HttpGet("{process}")]
		public Process Get(string process) {
			var processes = new Processes(instance, currentSession);
			return processes.GetListProcess(process);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] Dictionary<string, object> data) {
			CheckRight("write");
			var ib = new ItemBase(instance, process, currentSession);
			var item_id = ib.InsertItemRow();
			data.Add("item_id", item_id);
			ib.SaveItem(data);
			return new ObjectResult(data);
		}

		[HttpPost("{process}/{parentItemId}/{isRecursive}")]
		public IActionResult Post(string process, int parentItemId, bool isRecursive,
			[FromBody] Dictionary<string, object> data) {
			CheckRight("write");
			var ib = new ItemBase(instance, process, currentSession);
			var item_id = ib.InsertItemRow(parentItemId:parentItemId);

			if (isRecursive) {
				var pb = new ProcessBase(instance, process, currentSession);
				data["parent_item_id"] = pb.UpdateRecursiveParent(item_id, parentItemId, process);
			}

			data.Add("item_id", item_id);
			return new ObjectResult(data);
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<Dictionary<string, object>> data) {
			CheckRight("write");
			var ip = new ItemBase(instance, process, currentSession);

			var processes = new Processes(instance, currentSession);
			var p = processes.GetListProcess(process);

			var pagesDb = new PagesDb(instance, currentSession);
			var pageParams = new Dictionary<string, object>();
			var pageStates = pagesDb.GetStates("Lists", pageParams);
			if (p.SystemProcess && pageStates.Contains("admin") || !p.SystemProcess) {
				foreach (var row in data) {
					if (row.ContainsKey("user_group")) {
						var stringValue = "";
						var nList = new List<int>();
						string[] numbers = Regex.Split(Conv.ToStr(row["user_group"]), @"\D+");
						foreach (string value in numbers)
						{
							if (!string.IsNullOrEmpty(value))
							{
								int i = int.Parse(value);
								nList.Add(i);
							}
						}
						string idsAsString = string.Join(",", nList);
						row["user_group"] = idsAsString;
					}
					ip.SaveItem(row);
				}
				InvalidateCaches(process);


			} else {
				throw new CustomPermissionException("Page lists no rights to admin");
			}


			return new ObjectResult(data);
		}


		[HttpPut("{listName}/selectorcolumns")]
		public void AddSelectorColumns(string listName,[FromBody] List<int> itemColumnIds) {
			CheckRight("write");
			var cb = new ConnectionBase(instance, currentSession);
			cb.SetDBParam(SqlDbType.NVarChar, "@listName", listName);

			var sql2 =
				"INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, order_no) VALUES(@ppid, @id, ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM process_portfolio_columns WHERE process_portfolio_id = @ppid ORDER BY order_no  DESC),null)),'U'))";

				var sql =
					"SELECT TOP 1 ISNULL(process_portfolio_id, 0) FROM process_portfolios WHERE process = @listName";
				var ppid = Conv.ToInt(cb.db.ExecuteScalar(sql, cb.DBParameters));
				if (ppid > 0) {
					cb.SetDBParam(SqlDbType.Int, "@ppid", ppid);
					foreach (var id in itemColumnIds) {
						cb.SetDBParam(SqlDbType.Int, "@id", id);
						cb.db.ExecuteInsertQuery(sql2, cb.DBParameters);
					}
					Cache.Remove("", "process_portfolio_columns_table");
				}
		}

		[HttpPut("{process}/{parentItemId}")]
		public IActionResult Put(string process, int parentItemId, [FromBody] List<Dictionary<string, object>> data) {
			CheckRight("write");
			var ip = new ItemBase(instance, process, currentSession);

			foreach (var row in data) {
				ip.SaveItem(row);
			}
			InvalidateCaches(process);
			return new ObjectResult(data);
		}

		[HttpDelete("{process}")]
		public IActionResult Delete(string process) {
			CheckRight("delete");
			var processes = new Processes(instance, currentSession);

			foreach (var id in process.Split(',')) {
				processes.Delete(id);
			}

			InvalidateCaches(process);
			return new StatusCodeResult(200);
		}

		private void InvalidateCaches(string process) { 
			// Cache.Remove(instance, "list_hierarchy_" + process);
			// Cache.RemoveGroup("list_hierarchy_meta_" + process);
			Cache.Initialize();
			var signalR = new ApiPush(Startup.SignalR);
			var parameters = new List<object> {process};
			signalR.SendToAll("ListChanged", parameters);
		}
	}
}