﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using Translations = Keto5.x.platform.server.core.Translations;

namespace Keto5.x.platform.server.processes {
	public class Process {
		public Process() { }

		public Process(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ProcessName = (string)row["process"];
			Variable = (string)row["variable"];
			ProcessType = Conv.ToInt(row["process_type"]);
			NewRowActionId = (int?)Conv.IfDbNullThenNull(row["new_row_action_id"]);
			AfterNewRowActionId = (int?)Conv.IfDbNullThenNull(row["after_new_row_action_id"]);
			ListProcess = Convert.ToBoolean(row["list_process"]);
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);

			Colour = Conv.ToStr(row["colour"]);
			ListOrder = Conv.ToStr(row["list_order"]);
			SystemProcess = Convert.ToBoolean(row["system_process"]);
			PortfolioShowType = Convert.ToByte(row["portfolio_show_type"]);
			ListType = (int?)Conv.IfDbNullThenNull(row["list_type"]);
			Compare = (int?)Conv.IfDbNullThenNull(row["compare"]);
		}

		public string ProcessName { get; set; }
		public string Variable { get; set; }
		public int ProcessType { get; set; }
		public int? NewRowActionId { get; set; }
		public int? AfterNewRowActionId { get; set; }
		public bool ListProcess { get; set; }
		public string ParentProcess { get; set; }
		public int PortfolioCount { get; set; }
		public List<string> SubProcesses { get; set; }
		public string Colour { get; set; }
		public string ListOrder { get; set; }
		public string ListProcesses { get; set; }
		public List<ItemColumn> ItemColumns { get; set; }
		public int? ProcessPortfolioId { get; set; }
		public bool Selector { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public bool SystemProcess { get; set; }
		public byte PortfolioShowType { get; set; }

		public int? ListType { get; set; }

		public int? Compare { get; set; }
	}

	public class SearchResult {
		public SearchResult() { }

		public SearchResult(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			Type = (string)row["type"];
			ElementId = Conv.ToInt(row["id"]);
			Translation = (string)row["translation"];
			GroupParent = Conv.ToInt(row["group_parent"]);
			TabParent = Conv.ToInt(row["tab_parent"]);
		}

		public string Type { get; set; }
		public int ElementId { get; set; }
		public string Translation { get; set; }
		public int GroupParent { get; set; }
		public int TabParent { get; set; }
	}


	public class Processes : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;
		private readonly Translations translations;

		public Processes(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			translations = new Translations(instance, authenticatedSession);
			this.authenticatedSession = authenticatedSession;
		}

		public int ChooseItemAtRandom(string instance, string process) {
			SetDBParam(SqlDbType.VarChar, "process", process);
			return Conv.ToInt(db.ExecuteScalar(
				"SELECT TOP 1 item_id FROM items WHERE instance = @instance AND process = @process ORDER BY newid()",
				DBParameters));
		}


		public List<SearchResult> GetSearchResult(string searchWord, List<string> elements, string process) {
			var result = new List<SearchResult>();

			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParameters("@locale", authenticatedSession.locale_id);
			var tryToInject = Conv.Escape(searchWord);

			var sqls = "";
			if (elements.Contains("CONDITION") || elements.Contains("ALL"))
				sqls +=
					"SELECT 'CONDITION' AS type, c.condition_id AS id, pt.translation AS translation, 0 as group_parent, 0 as tab_parent FROM conditions c " +
					"LEFT JOIN process_translations pt ON pt.variable = c.variable AND pt.code = @locale " +
					" WHERE pt.process = @process AND pt.translation LIKE '%" + tryToInject + "%' UNION ALL ";

			if (elements.Contains("ACTION") || elements.Contains("ALL"))
				sqls +=
					"SELECT 'ACTION' AS type, pa.process_action_id AS id, pt.translation AS translation, 0 as group_parent, 0 as tab_parent FROM process_actions pa " +
					"LEFT JOIN process_translations pt ON pt.variable = pa.variable AND pt.code = @locale" +
					" WHERE pt.process = @process AND pt.translation LIKE '%" + tryToInject + "%' UNION ALL ";

			if (elements.Contains("TAB") || elements.Contains("ALL"))
				sqls +=
					"SELECT 'TAB' AS type, pta.process_tab_id AS id, pt.translation AS translation, ISNULL(pgt.process_group_id, 0) as group_parent, 0 as tab_parent FROM process_tabs pta " +
					"LEFT JOIN process_translations pt ON pt.variable = pta.variable AND pt.code =  @locale " +
					"LEFT JOIN process_group_tabs pgt ON pta.process_tab_id = pgt.process_tab_id " +
					" WHERE pt.process = @process AND pt.translation LIKE '%" + tryToInject + "%' UNION ALL ";

			if (elements.Contains("CONTAINER") || elements.Contains("ALL"))
				sqls +=
					"SELECT 'CONTAINER' AS type, pc.process_container_id AS id, pt.translation AS translation, ISNULL(pgt.process_group_id, 0) as group_parent, ISNULL(ptc.process_tab_id,0) as tab_parent FROM process_containers pc " +
					"LEFT JOIN process_translations pt ON pt.variable = pc.variable AND pt.code = @locale " +
					"LEFT JOIN process_tab_containers ptc ON pc.process_container_id = ptc.process_container_id " +
					"LEFT JOIN process_group_tabs pgt ON ptc.process_tab_id = pgt.process_tab_id " +
					" WHERE pt.process = @process AND pt.translation LIKE '%" + tryToInject + "%' UNION ALL ";

			if (elements.Contains("FIELD") || elements.Contains("ALL"))
				sqls +=
					"SELECT 'FIELD' AS type, ic.item_column_id AS id, pt.translation AS translation, ISNULL(pgt.process_group_id, 0) as group_parent, ISNULL(ptc.process_tab_id,0) as tab_parent FROM item_columns ic  " +
					"LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = @locale " +
					"LEFT JOIN process_container_columns pcc ON ic.item_column_id = pcc.item_column_id " +
					"LEFT JOIN process_tab_containers ptc ON pcc.process_container_id = ptc.process_container_id " +
					"LEFT JOIN process_group_tabs pgt ON ptc.process_tab_id = pgt.process_tab_id " +
					"WHERE pt.process = @process AND pt.translation LIKE '%" + tryToInject + "%' UNION ALL ";

			//Remove last UNION ALL
			sqls = Modi.Left(sqls, sqls.Length - 10);

			foreach (DataRow row in db.GetDatatable(sqls, DBParameters).Rows) {
				var newRow = new SearchResult {
					Type = Conv.ToStr(row["type"]),
					ElementId = Conv.ToInt(row["id"]),
					Translation = Conv.ToStr(row["translation"]),
					GroupParent = Conv.ToInt(row["group_parent"]),
					TabParent = Conv.ToInt(row["tab_parent"])
				};
				result.Add(newRow);
			}

			return result;
		}

		public List<Process> GetProcesses() {
			var result = new List<Process>();
			var db = new Connections(authenticatedSession);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process, variable, process_type, list_process, new_row_action_id, after_new_row_action_id, colour, list_order, system_process, portfolio_show_type, list_type, compare FROM processes WHERE instance = @instance AND list_process = 0",
					DBParameters).Rows) {
				var process = new Process(row, authenticatedSession);
				var subprocesses =
					new ProcessSubprocesses(instance, (string)row["process"], authenticatedSession);
				process.SubProcesses = subprocesses.GetSubProcessesArray((string)row["process"]);
				result.Add(process);
			}

			return result;
		}

		public bool IsUnique(string variable) {
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@variable", variable);
			var rowCount = db.GetDatatable(
				"SELECT process FROM processes WHERE instance = @instance AND process = @variable",
				DBParameters).Rows.Count;

			return rowCount == 0;
		}

		public List<Process> GetProcesses(byte processType, bool includeLists = false) {
			var result = new List<Process>();
			var db = new Connections(authenticatedSession);

			SetDBParam(SqlDbType.TinyInt, "@processType", processType);
			var listCondition = !includeLists ? "AND list_process = 0" : "";

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process, variable, process_type, list_process, new_row_action_id, after_new_row_action_id, colour, list_order, system_process, portfolio_show_type, list_type, compare FROM processes WHERE instance = @instance " + listCondition + " AND process_type = @processType",
					DBParameters).Rows) {
				var process = new Process(row, authenticatedSession);
				var subprocesses =
					new ProcessSubprocesses(instance, (string)row["process"], authenticatedSession);
				process.SubProcesses = subprocesses.GetSubProcessesArray((string)row["process"]);
				process.PortfolioCount =
					(int)db.ExecuteScalar(
						"SELECT COUNT(item_id) AS count FROM _" + instance + "_" + (string)row["process"],
						DBParameters);

				result.Add(process);
			}

			return result;
		}

		public DataTable GetProcessFeatures(string process) {
			var r = new DataTable();

			SetDBParam(SqlDbType.NVarChar, "@process", process);
			r = db.GetDatatable(" SELECT DISTINCT(pgf.state) FROM process_groups pg " +
								"INNER JOIN process_group_features pgf ON pgf.process_group_id = pg.process_group_id " +
								"WHERE process = @process", DBParameters);
			return r;
		}

		public ProcessMapObject GetProcessMap(string process) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.NVarChar, "@locale", authenticatedSession.locale_id);

			var db = new Connections(authenticatedSession);
			var sql = "SELECT " +
					  "pg.process_group_id AS group_id, " +
					  "CASE WHEN ISNULL(t1.translation,'') = '' THEN pg.variable ELSE t1.translation END AS group_variable, " +
					  "pg.maintenance_name AS group_tag, " +
					  "pgt.process_group_tab_id AS tab_id, " +
					  "CASE WHEN ISNULL(t2.translation,'') = '' THEN pt.variable ELSE t2.translation END AS tab_variable, " +
					  "pgt.tag AS tab_tag, " +
					  "pc.process_container_id AS container_id, " +
					  "CASE WHEN ISNULL(t3.translation,'') = '' THEN pc.variable ELSE t3.translation END AS container_variable, " +
					  "pc.maintenance_name AS container_tag, " +
					  "pcc.process_container_column_id AS field_id, " +
					  "CASE WHEN ISNULL(t4.translation,'') = '' THEN ic.variable ELSE t4.translation END AS field_variable FROM process_groups pg " +
					  "LEFT JOIN process_group_tabs pgt on pg.process_group_id = pgt.process_group_id " +
					  "LEFT JOIN process_tabs pt ON pgt.process_tab_id = pt.process_tab_id " +
					  "LEFT JOIN process_tab_containers ptc on pt.process_tab_id = ptc.process_tab_id " +
					  "LEFT JOIN process_containers pc ON pc.process_container_id = ptc.process_container_id " +
					  "LEFT JOIN process_container_columns pcc on pc.process_container_id = pcc.process_container_id " +
					  "LEFT JOIN item_columns ic on pcc.item_column_id = ic.item_column_id " +
					  "LEFT JOIN process_translations t1 ON t1.instance = pg.instance and t1.variable = pg.variable AND t1.code = @locale and t1.process = pg.process " +
					  "LEFT JOIN process_translations t2 ON  t2.instance = pg.instance and t2.variable = pt.variable AND t2.code = @locale and t2.process = pg.process " +
					  "LEFT JOIN process_translations t3 ON  t3.instance = pg.instance and t3.variable = pc.variable AND t3.code = @locale and t3.process = pg.process " +
					  "LEFT JOIN process_translations t4 ON  t4.instance = pg.instance and t4.variable = ic.variable AND t4.code = @locale and t4.process = pg.process " +
					  "WHERE pg.process = @process " +
					  "ORDER BY pg.order_no ASC, pgt.order_no ASC, ptc.order_no ASC, pcc.order_no ASC";

			var dt = db.GetDatatable(sql, DBParameters);
			//var prev_process_group_id = 0;

			var root = new ProcessMapObject("root", null);
			foreach (DataRow r in dt.Rows) {
				var g = new ProcessMapObject("group", r);
				var t = new ProcessMapObject("tab", r);
				var c = new ProcessMapObject("container", r);
				var f = new ProcessMapObject("field", r);

				var group = root.ProcessChild(g);
				var tab = group.ProcessChild(t);
				var container = tab.ProcessChild(c);
				container.ProcessChild(f);
			}

			return root;
		}

		public Dictionary<string, Dictionary<string, List<string>>> GetRecursiveListRelations(string ordercol) {
			var db = new Connections(authenticatedSession);
			var result = new Dictionary<string, Dictionary<string, List<string>>>();
			var collate = "";
			if (ordercol == "") ordercol = "order_no";
			if (ordercol == "order_no") collate = "  ASC";

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT * FROM processes WHERE list_process = 1 AND process_type = 1",
					DBParameters).Rows) {
				SetDBParam(SqlDbType.NVarChar, "@process", row["process"]);
				var p = Conv.ToStr(row["process"]);
				var nd = new Dictionary<string, List<string>>();
				result.Add(p, nd);
				var ProcessBase = new ProcessBase(authenticatedSession, p);
				foreach (DataRow row2 in db.GetDatatable(
					"SELECT DISTINCT ISNULL(parent_item_id, 0) as parent_item_id FROM _" + instance + "_" + p,
					DBParameters).Rows) {
					var parent = Conv.ToStr(row2["parent_item_id"]);
					var children = new List<string>();

					var sqls = "";

					SetDBParam(SqlDbType.NVarChar, "@parent", parent);
					if (parent == "0") {
						sqls = "SELECT item_id FROM _" + instance + "_" + p +
							   " WHERE parent_item_id = @parent OR parent_item_id IS NULL ORDER BY " + ProcessBase.ValidateOrderColumn(ordercol) + collate;
					}
					else {
						sqls = "SELECT item_id FROM _" + instance + "_" + p + " WHERE parent_item_id = @parent ORDER BY " + ProcessBase.ValidateOrderColumn(ordercol) + collate;
					}

					if (!result.ContainsKey(parent)) {
						result[p].Add(parent, new List<string>());
					}

					foreach (DataRow row3 in db.GetDatatable(
						sqls,
						DBParameters).Rows) {
						children.Add(Conv.ToStr(row3["item_id"]));
						result[p][parent] = children;
					}
				}
			}

			return result;
		}

		public List<Process> GetListProcesses() {
			var result = new List<Process>();
			var db = new Connections(authenticatedSession);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process, variable, process_type, list_process, new_row_action_id, after_new_row_action_id, colour, list_order, system_process, portfolio_show_type, list_type, compare FROM processes WHERE instance = @instance AND list_process = 1",
					DBParameters).Rows) {
				var process = new Process(row, authenticatedSession);
				var subprocesses =
					new ProcessSubprocesses(instance, (string)row["process"], authenticatedSession);
				process.SubProcesses = subprocesses.GetSubProcessesArray((string)row["process"]);

				SetDBParam(SqlDbType.NVarChar, "@dataAdditional", row["process"]);

				var ListProcesses = "";
				foreach (DataRow row2 in db
					.GetDatatable("SELECT DISTINCT(process) FROM item_columns WHERE data_additional = @dataAdditional",
						DBParameters)
					.Rows) {
					ListProcesses +=
						new Translations(authenticatedSession.instance, authenticatedSession).GetTranslation(
							(string)row2["process"]) +
						", ";
				}

				if (ListProcesses.Length > 0) {
					ListProcesses = ListProcesses.Substring(0, ListProcesses.Length - 2);
				}

				var itemColumns =
					new ItemColumns(authenticatedSession.instance, (string)row["process"],
						authenticatedSession);
				process.ItemColumns = itemColumns.GetItemColumns();

				process.ListProcesses = ListProcesses;

				SetDBParam(SqlDbType.NVarChar, "@process", row["process"]);
				process.ProcessPortfolioId = Conv.ToInt(db.ExecuteScalar(
					"SELECT ISNULL(process_portfolio_id, 0) FROM process_portfolios WHERE process = @process",
					DBParameters));
				if (process.ProcessPortfolioId > 0) {
					process.Selector = true;
				}
				else {
					process.Selector = false;
				}


				result.Add(process);
			}

			return result;
		}

		public Process GetProcess(byte processType, string process) {
			SetDBParam(SqlDbType.TinyInt, "@processType", processType);
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var dt = db.GetDatatable(
				"SELECT process, variable, process_type, list_process, new_row_action_id, after_new_row_action_id, colour, list_order, system_process, portfolio_show_type, list_type, compare FROM processes WHERE instance = @instance AND process_type = @processType AND process = @process AND list_process = 0",
				DBParameters);
			if (dt.Rows.Count > 0) {
				var pr = new Process(dt.Rows[0], authenticatedSession);

				return pr;
			}

			throw new CustomArgumentException("Process cannot be found with instance '" + instance +
											  "' and process_type " +
											  processType + " AND process '" + process + "'");
		}

		private DataTable _PrivateProcessCache = null;

		private DataTable PrivateProcessCache {
			get {
				if (_PrivateProcessCache == null) {
					_PrivateProcessCache =
						db.GetDatatable(
							"SELECT instance, process, variable, process_type, list_process, new_row_action_id, after_new_row_action_id, colour, list_order, system_process, portfolio_show_type, list_type, compare FROM processes");
				}

				return _PrivateProcessCache;
			}
		}

		public Process GetProcess(string process, bool includePortfolioCount = false) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var dt = PrivateProcessCache.Select("instance = '" + instance + "' AND process = '" + process + "'");

			if (dt.Length > 0) {
				var pr = new Process(dt[0], authenticatedSession);
				pr.PortfolioCount = 0;
				if (includePortfolioCount) {
					pr.PortfolioCount =
						(int)db.ExecuteScalar(
							"SELECT COUNT(item_id) AS count FROM _" + instance + "_" + (string)dt[0]["process"],
							DBParameters);
				}

				return pr;
			}

			throw new CustomArgumentException("Process cannot be found with string '" + process + "'");
		}

		public int GetProcessSelectorPortfolioId(string process) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var sql =
				"SELECT p.process_portfolio_id FROM process_portfolios p WHERE p.process = @process AND p.process_portfolio_type = 1";
			return Conv.ToInt(db.ExecuteScalar(sql, DBParameters));
		}

		private string SqlJsonValue(string str) {
			return "REPLACE(CAST(ISNULL(" + str + ",'') AS NVARCHAR(255)),CHAR(34),CHAR(39))";
		}

		public List<string> GetProcessSelectorPrimaryColumnNames(string process, bool compileForClient = false,
			bool includeSecondaries = false) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var where = "1";
			if (includeSecondaries) where = "1,2";

			var sql =
				"SELECT ic.*, CASE WHEN use_in_select_result = 1 THEN 'primary' ELSE 'secondary' END AS 'primary' FROM process_portfolios p LEFT JOIN process_portfolio_columns pc ON pc.process_portfolio_id = p.process_portfolio_id LEFT JOIN item_columns ic ON ic.item_column_id = pc.item_column_id WHERE p.process = @process AND (p.process_portfolio_type = 10 OR p.process_portfolio_type = 1) AND pc.use_in_select_result IN (" +
				where + ") ORDER BY pc.order_no  ASC";
			var result = new List<string>();
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var ic = new ItemColumn(row, authenticatedSession);
				var c = ic.Name;
				if (compileForClient && (ic.IsProcess() || ic.IsList())) {
					c =
						"'{ \"item_id\": \"' + CAST(ISNULL((SELECT TOP 1 selected_item_id FROM item_data_process_selections idps WHERE idps.item_column_id = " +
						ic.ItemColumnId +
						" AND idps.item_id = pf.item_id),'') AS nvarchar(255)) + '\", \"process\": \"" +
						ic.DataAdditional + "\", \"primary\": \"" + row["primary"] + "\"}'";
				}
				else if (compileForClient && ic.IsSubquery()) {
					c = "'{ \"name\": \"' + " + SqlJsonValue(ic.GetColumnQuery(instance)) + " + '\", \"primary\": \"" +
						row["primary"] + "\"}'";
				}
				else if (compileForClient && ic.IsDate()) {
					c = "'{ \"date\": \"' + " + SqlJsonValue(ic.Name) + " + '\", \"primary\": \"" + row["primary"] +
						"\"}'";
				}
				else if (compileForClient) {
					c = "'{ \"name\": \"' + " + SqlJsonValue(ic.Name) + " + '\", \"primary\": \"" + row["primary"] +
						"\"}'";
				}

				result.Add(c);
			}

			return result;
		}

		public void AddPrimaryNameToDatatableColumn(DataTable TargetDataTable, string TargetColumnName,
			string OwnerItemIdColumn,
			string ProcessNameColumn, bool fetchArchive = false, bool compileForClient = false,
			bool includeSecondaries = false) {
			var _sqlStore = new Dictionary<string, string>();
			var _idStore = new Dictionary<string, List<int>>();


			foreach (DataRow r in TargetDataTable.Rows) {
				var source_process = Conv.ToStr(r[ProcessNameColumn]);

				//Collect all process names and create SQL
				if (Conv.ToInt(r[OwnerItemIdColumn]) != 0) {
					//Resolve a query for getting the owner of the comment and store it.
					if (!_sqlStore.ContainsKey(source_process)) {
						var cols = GetProcessSelectorPrimaryColumnNames(source_process, compileForClient,
							includeSecondaries);
						if (cols.Count <= 0) continue;

						var colsstring = "";
						foreach (var col in cols) {
							if (colsstring == "") {
								colsstring += "ISNULL(" + col + ",'')";
							}
							else {
								colsstring += " + ', ' + ISNULL(" + col + ",'')";
							}
						}

						colsstring = colsstring.Trim().TrimEnd(',');
						if (compileForClient) colsstring = "'[' + " + colsstring + " + ']'";
						if (fetchArchive) {
							var asql = " select item_id, COALESCE((select " + colsstring +
									   " from _" + instance +
									   "_" + source_process + " where item_id = a1.item_id), " +
									   " (select top 1 " + colsstring + " from archive__" +
									   instance + "_" + source_process + " " +
									   " where item_id = a1.item_id order by archive_end desc)) as owner_name_fetch from " +
									   "     (select item_id from _" + instance + "_" +
									   source_process + " where item_id in (##idString##) " +
									   " union " +
									   "     select item_id from archive_" + Conv.ToSql("_" + instance + "_" + source_process) + " where item_id in (##idString##) ) a1 ";
							_sqlStore.Add(source_process, asql);
						}
						else {
							_sqlStore.Add(source_process, "SELECT " + colsstring +
														  " AS owner_name_fetch, item_id FROM " + Conv.ToSql("_" + instance + "_" + source_process) +
														  " pf WHERE item_id IN ");
						}
					}
				}
			}

			//Collect all id strings
			foreach (DataRow r in TargetDataTable.Rows) {
				var source_process = Conv.ToStr(r[ProcessNameColumn]);
				if (Conv.ToInt(r[OwnerItemIdColumn]) != 0) {
					var l = _idStore.ContainsKey(source_process) ? _idStore[source_process] : new List<int>();
					l.Add(Conv.ToInt(r[OwnerItemIdColumn]));
					_idStore[source_process] = l;
				}
			}

			//Execute SQL's
			foreach (var p in _sqlStore) {
				var ids = Modi.JoinListWithComma(_idStore[p.Key]);
				if (ids.Length > 0) {
					var sql = p.Value + "(" + ids + ")";
					if (fetchArchive) sql = p.Value.Replace("##idString##", ids);
					var idtable = db.GetDatatable(sql);
					foreach (DataRow r in TargetDataTable.Rows) {
						var vs = idtable.Select("item_id = " + Conv.ToInt(r[OwnerItemIdColumn]));
						if (vs.Length > 0) {
							r[TargetColumnName] = vs[0]["owner_name_fetch"];
						}
						else if (compileForClient) {
							if (Conv.ToStr(r[TargetColumnName]) == "")
								r[TargetColumnName] = "[{ \"name\": \"" + r[TargetColumnName] + "\", \"primary\": \"" +
													  "primary" + "\"}]";
							if (!Conv.ToStr(r[TargetColumnName]).Contains("[{"))
								r[TargetColumnName] = "[{ \"name\": \"" + r[TargetColumnName] + "\", \"primary\": \"" +
													  "primary" + "\"}]";
						}
					}
				}
			}
		}

		private DataTable _ParentProcessCache = null;

		private DataTable GetParentProcessCache() {
			if (_ParentProcessCache == null) {
				_ParentProcessCache = db.GetDatatable("SELECT parent_process, process FROM process_subprocesses");
			}

			return _ParentProcessCache;
		}

		public Process GetListProcess(string process) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);

			var dt =
				db.GetDatatable(
					"SELECT process, variable, process_type, list_process, new_row_action_id, after_new_row_action_id, colour, list_order, system_process, portfolio_show_type, list_type, compare FROM processes WHERE instance = @instance AND process = @process ",
					DBParameters);
			if (dt.Rows.Count > 0) {
				var pr = new Process(dt.Rows[0], authenticatedSession);
				var gpr = GetParentProcessCache().Select("process = '" + Conv.ToSql(dt.Rows[0]) + "'");
				if (gpr.Length > 0) pr.ParentProcess = Conv.ToStr(gpr[0]["parent_process"]);
				return pr;
			}

			throw new CustomArgumentException("List Process cannot be found with string '" + process + "'");
		}

		public List<Process> GetListProcesses(string process) {
			var result = new List<Process>();
			var db = new Connections(authenticatedSession);

			SetDBParam(SqlDbType.NVarChar, "@parentProcess", process);
			foreach (DataRow row in db.GetDatatable(
				"SELECT p.process, p.variable, p.process_type, p.list_process, new_row_action_id, after_new_row_action_id, colour, list_order, system_process, portfolio_show_type, list_type, compare FROM processes p INNER JOIN process_subprocesses ps ON p.instance = p.instance AND p.process = ps.process WHERE p.instance = @instance AND p.list_process = 1 AND ps.parent_process = @parentProcess"
				,
				DBParameters).Rows) {
				var process_ = new Process(row, authenticatedSession);
				result.Add(process_);
			}

			return result;
		}


		//private DataTable _subProcessCache = null;

		private DataTable GetSubProcessCache {
			get {
				if (_subprocesscache != null) return _subprocesscache;
				var db = new Connections(authenticatedSession);
				_subprocesscache =
					db.GetDatatable("SELECT parent_process, process, instance FROM process_subprocesses");

				return _subprocesscache;
			}
		}

		public List<Process> GetParentProcesses(bool listProcess, string process) {
			var result = new List<Process>();
			//var db = new Connections(authenticatedSession.item_id);
			//SetDBParam(SqlDbType.NVarChar, "@process_", process);

			DataRow dr = null;
			var subs = GetSubProcessCache.Select("instance = '" + instance + "' AND process = '" + process + "'");
			if (subs.Length > 0) dr = subs[0];

			var check_counter = 0;
			while (dr != null) {
				check_counter++;
				if (check_counter > 10) throw new StackOverflowException("Max. process recursion reached.");

				Process p;
				p = GetListProcess((string)dr["parent_process"]);
				//SetDBParam(SqlDbType.NVarChar, "@process_", (string) dr["parent_process"]);

				subs = GetSubProcessCache.Select("instance = '" + instance + "' AND process = '" +
												 (string)dr["parent_process"] + "'");
				dr = null;
				if (subs.Length > 0) dr = subs[0];

				//				dr = db.GetDataRow(
				//					"SELECT parent_process, process FROM process_subprocesses WHERE instance = @instance AND process = @process_",
				//					DBParameters);


				if (dr != null) p.ParentProcess = (string)dr["parent_process"];

				result.Add(p);
			}

			return result;
		}

		public List<Process> GetParentProcesses2(bool listProcess, string process) {
			var result = new List<Process>();
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@process_", process);

			var dr =
				db.GetDataRow(
					"SELECT parent_process FROM process_subprocesses WHERE instance = @instance AND process = @process_",
					DBParameters);

			Process p;
			p = GetListProcess((string)dr["parent_process"]);
			SetDBParam(SqlDbType.NVarChar, "@process_", (string)dr["parent_process"]);
			dr = db.GetDataRow(
				"SELECT parent_process, process FROM process_subprocesses WHERE instance = @instance AND process = @process_",
				DBParameters);
			if (dr != null) {
				p.ParentProcess = (string)dr["parent_process"];
			}

			result.Add(p);


			return result;
		}

		public List<Process> GetParentProcessesWithSubProcess(bool listProcess, string process) {
			var result = new List<Process>();
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@process_", process);

			var dr =
				db.GetDataRow(
					"SELECT parent_process FROM process_subprocesses WHERE instance = @instance AND process = @process_",
					DBParameters);
			if (dr == null) {
				var pp = GetListProcess(process);
				result.Add(pp);
				return result;
			}

			var check_counter = 0;
			while (dr != null) {
				check_counter++;
				if (check_counter > 10) {
					throw new StackOverflowException("Max. process recursion reached.");
				}

				if (result.Count == 0) {
					var pp = GetListProcess(process);
					pp.ParentProcess = (string)dr["parent_process"];
					result.Add(pp);
				}

				Process p;
				p = GetListProcess((string)dr["parent_process"]);
				SetDBParam(SqlDbType.NVarChar, "@process_", (string)dr["parent_process"]);
				dr = db.GetDataRow(
					"SELECT parent_process, process FROM process_subprocesses WHERE instance = @instance AND process = @process_",
					DBParameters);
				if (dr != null) {
					p.ParentProcess = (string)dr["parent_process"];
				}

				result.Add(p);
			}

			result.Reverse();
			return result;
		}

		public Process Add(int ifExecuted, Process process) {
			if (process.Variable == null) {
				process.ProcessName =
					Modi.RemoveSpecialCharacters(process.LanguageTranslation[authenticatedSession.locale_id]);
				process.ProcessName = Modi.Left(process.ProcessName, 50).ToLower().Replace(' ', '_');
				SetDBParam(SqlDbType.NVarChar, "@process", process.ProcessName);
			}
			else {
				process.ProcessName = Modi.RemoveSpecialCharacters(process.Variable);
				process.ProcessName = Modi.Left(process.Variable, 50).ToLower().Replace(' ', '_');
				SetDBParam(SqlDbType.NVarChar, "@process", process.ProcessName);
			}

			if (ProcessExist(process.ProcessName) == false) {
				if (process.ProcessName.Length > 50) {
					process.ProcessName = Modi.Left(process.ProcessName, 50);
				}

				SetDBParam(SqlDbType.NVarChar, "@process", process.ProcessName);
				SetDBParam(SqlDbType.NVarChar, "@variable", process.ProcessName);
				SetDBParam(SqlDbType.TinyInt, "@processType", process.ProcessType);
				SetDBParam(SqlDbType.Int, "@newRowActionId", Conv.IfNullThenDbNull(process.NewRowActionId));
				SetDBParam(SqlDbType.Int, "@afterNewRowActionId", Conv.IfNullThenDbNull(process.AfterNewRowActionId));
				SetDBParam(SqlDbType.Int, "@listType", Conv.IfNullThenDbNull(process.ListType));
				if (process.ListProcess) {
					SetDBParam(SqlDbType.TinyInt, "@listProcess", 1);
				}
				else {
					SetDBParam(SqlDbType.TinyInt, "@listProcess", 0);
				}

				SetDBParam(SqlDbType.NVarChar, "@colour", Conv.IfNullThenDbNull(process.Colour));
				SetDBParam(SqlDbType.NVarChar, "@listOrder", Conv.IfNullThenDbNull(process.ListOrder));
				if (process.ListProcess || ifExecuted == 0) {
					db.ExecuteInsertQuery(
						"INSERT INTO processes (instance, process, variable, process_type, new_row_action_id, after_new_row_action_id, list_process, colour, list_order, list_type) VALUES(@instance, @process, @variable, @processType, @newRowActionId, @afterNewRowActionId, @listProcess, @colour, @listOrder, @listType)"
						,
						DBParameters);
				}
				else if (ifExecuted == 1) {
					db.ExecuteStoredProcedure(
						"EXEC app_ensure_process_customer @instance, @process, @processType, @colour, @listProcess, @listOrder, @newRowActionId"
						,
						DBParameters);

					Cache.Set(instance, "InstanceMenuCache", null);
					Cache.Set(instance, "InstanceMenuRightCache", null);
					Cache.Set(instance, "InstanceMenuStateCache", null);
					Cache.Set(instance, "pagebase_menu_cache", null);
				}

				if (process.ParentProcess != null) {
					SetDBParam(SqlDbType.NVarChar, "@parentProcess", process.ParentProcess);
					db.ExecuteInsertQuery(
						"INSERT INTO process_subprocesses (instance, process, parent_process) VALUES(@instance, @process, @parentProcess)",
						DBParameters);
				}

				foreach (var translation in process.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process.ProcessName, process.ProcessName,
						translation.Value,
						translation.Key);
				}

				if (process.ListProcess) {
					var itemCols = new ItemColumns(instance, process.ProcessName, authenticatedSession);
					itemCols.AddNewItemColumn("LIST_ITEM", true, "LIST_ITEM", 0, "", 0);
					itemCols.AddNewItemColumn("LIST_SYMBOL", true, "LIST_SYMBOL", 1, "SYMBOLS", 0);
					itemCols.AddNewItemColumn("LIST_SYMBOL_FILL", true, "LIST_SYMBOL_FILL", 1, "SYMBOL_COLORS", 0);
					itemCols.AddNewItemColumn("IN_USE", true, "IN_USE", 0, "IN_USE", 0);
					itemCols.AddNewItemColumn("SYSTEM_ITEM", true, "SYSTEM_ITEM", 0, "SYSTEM_ITEM", 0);
					itemCols.AddNewItemColumn("USER_GROUP", true, "USER_GROUP", 0, "USER_GROUP", 4);

					process.ItemColumns = itemCols.GetItemColumns();
				}
			}

			if (!process.ListProcess && (process.ProcessType == 0 || process.ProcessType == 1 ||
										 process.ProcessType == 2 || process.ProcessType == 5)) {
				SetDBParam(SqlDbType.NVarChar, "@process", process.ProcessName);
				db.ExecuteStoredProcedure(
					"EXEC app_ensure_create_right_container @instance, @process", DBParameters);
			}

			if (process.ProcessType == 5) {
				SetDBParam(SqlDbType.NVarChar, "@process", process.ProcessName);
				db.ExecuteStoredProcedure(
					"EXEC app_ensure_table_process @instance, @process", DBParameters);
			}

			if (process.ProcessType is 4 or 5) {

				var Conditions = new Conditions(instance, process.ProcessName, authenticatedSession);
				var condition = new Condition();
				condition.Process = process.ProcessName;
				condition.ConditionGroupId = 0;
				condition.ConditionJSON = null;
				condition.States = new List<string> {
					"meta.delete"
				};
				condition.Features = new List<string> {
					"meta"
				};

				condition.LanguageTranslation = new Dictionary<string, string> {
					{"en-GB", "delete"},
					{"fi-FI", "delete"}
				};

				Conditions.Add(condition);

			}

			Cache.Remove("", "item_columns_table");
			Cache.Remove("", "item_columns_and_containers_table");
			Cache.Remove("", "item_containers_and_columns_table");
			process.SubProcesses = new List<string>();

			Cache.Remove("", "process_portfolio_table");
			return process;
		}

		public bool ProcessExist(string process) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			return (int)db.ExecuteScalar(
					   "SELECT COUNT(process) FROM processes WHERE instance = @instance AND process = @process",
					   DBParameters) > 0;
		}

		public bool ColumnExist(string process, string name) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.NVarChar, "@name", name);
			var colCount = Conv.ToInt(db.ExecuteScalar(
				"SELECT COUNT(item_column_id) FROM item_columns WHERE instance = @instance AND process = @process AND name = @name",
				DBParameters));
			if (colCount > 0) {
				return true;
			}

			return false;
		}

		public void Delete(string process) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var pDel = GetProcess(process);

			if (pDel.PortfolioCount > 0 && pDel.ListProcess == false) {
				throw new CustomPermissionException("There are records on process. Can't delete");
			}

			if (pDel.ListProcess) {
				Cache.RemoveGroup("list_hierarchy_meta_" + process);
				Cache.Remove(instance, "list_hierarchy_" + process);
			}

			db.ExecuteDeleteQuery("DELETE FROM processes WHERE instance = @instance AND process = @process",
				DBParameters);
		}

		public Process Save(Process process) {
			if (process.ListProcess) {
				var pagesDb = new PagesDb(instance, authenticatedSession);
				var pageParams = new Dictionary<string, object>();
				var pageStates = pagesDb.GetStates("Lists", pageParams);


				var oldProcess = GetListProcess(process.ProcessName);
				if (!pageStates.Contains("admin") && !pageStates.Contains("write")) {
					throw new CustomPermissionException("Page lists no rights to admin or write");
				}

				if (oldProcess.SystemProcess && !pageStates.Contains("admin")) {
					throw new CustomPermissionException("Page lists no rights to admin");
				}
			}

			SetDBParam(SqlDbType.NVarChar, "@process", process.ProcessName);
			SetDBParam(SqlDbType.NVarChar, "@variable", process.Variable);
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.Int, "@processType", process.ProcessType);
			SetDBParam(SqlDbType.Int, "@newRowActionId", Conv.IfNullThenDbNull(process.NewRowActionId));
			SetDBParam(SqlDbType.Int, "@afterNewRowActionId", Conv.IfNullThenDbNull(process.AfterNewRowActionId));
			SetDBParam(SqlDbType.NVarChar, "@colour", Conv.IfNullThenDbNull(process.Colour));
			SetDBParam(SqlDbType.NVarChar, "@listOrder", Conv.IfNullThenDbNull(process.ListOrder));
			SetDBParam(SqlDbType.TinyInt, "@portfolioShowType", process.PortfolioShowType);
			SetDBParam(SqlDbType.Int, "@listType", Conv.IfNullThenDbNull(process.ListType));
			SetDBParam(SqlDbType.Int, "@compare", Conv.IfNullThenDbNull(process.Compare));

			db.ExecuteUpdateQuery(
				"UPDATE processes SET process_type = @processType, new_row_action_id = @newRowActionId,  after_new_row_action_id = @afterNewRowActionId, colour = @colour, list_order = @listOrder, portfolio_show_type = @portfolioShowType, list_type = @listType, compare = @compare WHERE instance = @instance AND process = @process"
				,
				DBParameters);
			foreach (var translation in process.LanguageTranslation) {
				translations.insertOrUpdateProcessTranslation(process.ProcessName, process.Variable,
					translation.Value,
					translation.Key);
			}


			if (process.SubProcesses.Count != 0) {
				db.ExecuteDeleteQuery(
					"DELETE FROM process_subprocesses WHERE process = @process AND instance = @instance",
					DBParameters);

				foreach (var subprocess in process.SubProcesses) {
					SetDBParam(SqlDbType.NVarChar, "@sub_process", subprocess);
					db.ExecuteInsertQuery(
						"INSERT INTO process_subprocesses (instance, parent_process, process) VALUES(@instance, @process, @sub_process)",
						DBParameters);
				}
			}

			if (process.ListProcess && process.ProcessType == 1) {
				if (Conv.ToInt(db.ExecuteScalar(
					"SELECT COUNT(*) FROM item_columns WHERE name = 'parent_item_id' AND process = @process AND instance = @instance",
					DBParameters)) == 0) {
					db.ExecuteStoredProcedure("EXEC app_ensure_recursive_list @instance, @process", DBParameters);
				//	Cache.Remove(instance, "ics_" + process.ProcessName);
					Cache.Initialize();
				}
			}

			if (process.ListProcess) {
				Cache.Remove(instance, "list_hierarchy_" + process.ProcessName);
				Cache.RemoveGroup("list_hierarchy_meta_" + process.ProcessName);

				var pagesDb = new PagesDb(instance, authenticatedSession);
				var pageParams = new Dictionary<string, object>();
				var pageStates = pagesDb.GetStates("Lists", pageParams);

				if (pageStates.Contains("admin")) {
					SetDBParam(SqlDbType.TinyInt, "@systemProcess", Conv.ToByte(process.SystemProcess));

					db.ExecuteUpdateQuery(
						"UPDATE processes SET system_process = @systemProcess WHERE instance = @instance AND process = @process"
						,
						DBParameters);
				}
			}

			return process;
		}

		public List<string> GetSubProcesses(string process) {
			var retval = new List<string>();
			SetDBParam(SqlDbType.NVarChar, "@parentProcess", process);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process FROM process_subprocesses WHERE instance = @instance AND parent_process = @parentProcess",
					DBParameters).Rows) {
				retval.Add(Conv.ToStr(row["process"]));
			}

			return retval;
		}

		public string GetProcessByItemId(int itemId) {
			return Cache.GetItemProcess(itemId, instance);
		}

		public Dictionary<int, PrimaryAndSecondary> GetPrimariesAndSecondaries(string process, int portfolioId, List<int> itemIds) {
			var data = new Dictionary<int, PrimaryAndSecondary>();

			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);

			var sql = " SELECT item_id, '' ";

			var first = true;
			foreach (DataRow row in db.GetDatatable(
				" SELECT ic.name AS name " +
				" FROM process_portfolio_columns ppc " +
				" INNER JOIN item_columns ic " +
				" ON ic.item_column_id = ppc.item_column_id AND ic.data_type = 0 " +
				" WHERE ppc.use_in_select_result = 1 AND ppc.process_portfolio_id = @portfolioId " +
				" ORDER BY ppc.order_no  ASC", DBParameters).Rows) {
				if (first) {
					sql += " + " + row["name"];
					first = false;
				}
				else {
					sql += " + ' ' + " + row["name"];
				}
			}

			sql += " AS [primary], '' ";

			first = true;
			foreach (DataRow row in db.GetDatatable(
				" SELECT ic.name AS name " +
				" FROM process_portfolio_columns ppc " +
				" INNER JOIN item_columns ic " +
				" ON ic.item_column_id = ppc.item_column_id AND ic.data_type = 0 " +
				" WHERE ppc.use_in_select_result = 2 AND ppc.process_portfolio_id = @portfolioId " +
				" ORDER BY ppc.order_no  ASC", DBParameters).Rows) {
				if (first) {
					sql += " + " + row["name"];
					first = false;
				}
				else {
					sql += " + ' ' + " + row["name"];
				}
			}

			sql += " AS [secondary] FROM _" + instance + "_" + process + " WHERE item_id IN (" + (itemIds.Count == 0 ? "0" : string.Join(",", itemIds.ToArray())) + ")";

			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var primaryAndSecondary = new PrimaryAndSecondary();
				primaryAndSecondary.primary = Conv.ToStr(row["primary"]);
				primaryAndSecondary.secondary = Conv.ToStr(row["secondary"]);
				data.Add(Conv.ToInt(row["item_id"]), primaryAndSecondary);
			}
			return data;
		}

		[Serializable]
		public class PrimaryAndSecondary {
			public string primary = "";
			public string secondary = "";
		}

		public List<Dictionary<string, object>> GetParentItems(string idList = null) {
			if (idList == null) {
				return db.GetDatatableDictionary("SELECT parent_item_id, item_id, favourite FROM item_subitems");
			}

			if (idList.Length == 0) {
				return new List<Dictionary<string, object>>();
			}

			var result = new DataTable();
			var ids = new List<string>(idList.Split(","));
			var i = 0;
			while (i < ids.Count) {
				var l = ids.Count - i;
				if (l > 15000) l = 15000;
				idList = String.Join(",", ids.GetRange(i, l).ToArray());
				result.Merge(db.GetDatatable("SELECT parent_item_id, item_id, favourite FROM item_subitems " +
											 "WHERE parent_item_id IN (" + idList + ") OR item_id IN (" + idList + ")"));
				i += 15000;
			}
			return db.DataTableToDictionary(result);
		}

		public Dictionary<string, object> SearchFromAllSelectors(int offset, int limit, string textSearch) {
			var retval = new Dictionary<string, object>();

			foreach (DataRow row in db
				.GetDatatable(
					" SELECT iq.process, process_portfolio_id FROM ( " +
					" SELECT process,instance, min(process_portfolio_id) as process_portfolio_id  " +
					" FROM process_portfolios  " +
					" WHERE instance = @instance and process_portfolio_type = 1  " +
					" GROUP BY process, instance " +
					" ) iq " +
					" LEFT JOIN processes p ON p.process = iq.process AND p.instance = iq.instance " +
					" ORDER BY process_type ASC",
					DBParameters).Rows) {

				var ib = new ItemBase(instance, (string)row["process"], authenticatedSession);
				var d = ib.FindItems(offset, limit, (int)row["process_portfolio_id"], 0, 0,
					textSearch);
				if (retval.ContainsKey("rowdata")) {
					((List<Dictionary<string, object>>)retval["rowdata"]).AddRange(
						db.DataTableToDictionary((DataTable)d["rowdata"]));
					retval["rowcount"] = (int)retval["rowcount"] + (int)d["rowcount"];

					foreach (var id in ((Dictionary<int, object>)d["rowstates"]).Keys) {
						if (!((Dictionary<int, object>)retval["rowstates"]).ContainsKey(id)) {
							((Dictionary<int, object>)retval["rowstates"]).Add(id,
								((Dictionary<int, object>)d["rowstates"])[id]);
						}
					}

					foreach (var id in ((Dictionary<int, object>)d["processes"]).Keys) {
						if (!((Dictionary<int, object>)retval["processes"]).ContainsKey(id)) {
							((Dictionary<int, object>)retval["processes"]).Add(id,
								((Dictionary<int, object>)d["processes"])[id]);
						}
					}
				}
				else {
					retval.Add("rowdata", db.DataTableToDictionary((DataTable)d["rowdata"]));
					retval.Add("processes", d["processes"]);
					retval.Add("rowstates", d["rowstates"]);
					retval.Add("rowcount", d["rowcount"]);
				}
			}

			return retval;
		}

		public class HierarchyResult {
			public Process Process;
			public int Level;
		}


		private DataTable _subprocesscache;

		/// <summary>
		/// We cache all processes. Pagetype means item_id of
		/// </summary>
		/// <param name="pageType"></param>
		/// <param name="process"></param>
		/// <param name="currentresult"></param>
		/// <param name="level"></param>
		/// <returns></returns>
		public List<HierarchyResult> GetListsHierarchy(int pageType, string process = "",
			List<HierarchyResult> currentresult = null, int level = 0) {
			var result = currentresult;
			SetDBParam(SqlDbType.Int, "@type", pageType);
			_subprocesscache =
				db.GetDatatable(
					"SELECT p.*, sp.parent_process AS parent_process, pt.translation as translation FROM process_subprocesses sp LEFT JOIN processes p ON p.process = sp.process INNER JOIN process_translations pt ON p.variable = pt.variable ORDER BY LOWER (translation)  ASC");
			return GetListsHierarchyWork(pageType, process);
		}

		private List<HierarchyResult> GetListsHierarchyWork(int pageType, string process = "",
			List<HierarchyResult> currentresult = null, int level = 0) {
			var result = currentresult;
			if (result == null) result = new List<HierarchyResult>();

			DataRow[] rows;
			SetDBParam(SqlDbType.Int, "@type", pageType);

			var q = db.GetDataRow("SELECT TOP 1 item_id FROM _" + instance + "_list_admin_types WHERE is_admin = '1'");

			string sql =
				"SELECT * FROM processes s WHERE s.instance = @instance AND list_process = 1 " +
				"AND (SELECT COUNT(*) FROM process_subprocesses sp WHERE s.process = sp.process) = 0 " +
				"ORDER BY LOWER ((SELECT TOP 1 translation FROM process_translations pt WHERE pt.variable = s.variable AND code = 'en-GB'))  ASC";

			if (process != "") {
				rows = _subprocesscache.Select("parent_process = '" + process + "'");
			}
			else {
				rows = db.GetDatatable(sql, DBParameters).Select();
			}

			foreach (DataRow dr in rows) {
				var p = new Process(dr, authenticatedSession);
				SetDBParam(SqlDbType.NVarChar, "@dataAdditional", dr["process"]);
				var ListProcesses = "";
				foreach (DataRow row2 in db
					.GetDatatable("SELECT DISTINCT(process) FROM item_columns WHERE data_additional = @dataAdditional",
						DBParameters)
					.Rows) {
					ListProcesses +=
						new Translations(authenticatedSession.instance, authenticatedSession).GetTranslation(
							(string)row2["process"]) +
						", ";
				}

				if (ListProcesses.Length > 0) {
					ListProcesses = ListProcesses.Substring(0, ListProcesses.Length - 2);
				}

				var itemColumns =
					new ItemColumns(authenticatedSession.instance, (string)dr["process"],
						authenticatedSession);
				p.ItemColumns = itemColumns.GetItemColumns();
				p.ListProcesses = ListProcesses;

				SetDBParameter("@process", p.ProcessName);

				p.ProcessPortfolioId = Conv.ToInt(db.ExecuteScalar(
					"SELECT ISNULL(process_portfolio_id, 0) FROM process_portfolios WHERE process = @process",
					DBParameters));
				if (p.ProcessPortfolioId > 0) {
					p.Selector = true;
				}
				else {
					p.Selector = false;
				}

				var r = new HierarchyResult();
				r.Process = p;
				r.Level = level;

				result.Add(r);

				if (p != null) GetListsHierarchyWork(pageType, p.ProcessName, result, level + 1);
			}

			if (pageType != Conv.ToInt(q["item_id"])) {
				var sortedItems = new List<HierarchyResult>();
				foreach (var r in result) {
					if (r.Process.ListType == pageType) {
						sortedItems.Add(r);
					}
				}

				return sortedItems;
			}

			return result;
		}

		public Dictionary<string, object> SearchFromAllSelectors(int offset, int limit, string textSearch,
			string feature,
			string state) {
			var retval = new Dictionary<string, object>();
			retval.Add("rowdata", new Dictionary<int, object>());
			retval.Add("rowstates", new Dictionary<int, object>());
			retval.Add("processes", new Dictionary<int, object>());
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT process, min(process_portfolio_id) as process_portfolio_id FROM process_portfolios WHERE instance = @instance and process_portfolio_type = 1 GROUP BY process"
					,
					DBParameters).Rows) {
				var ib = new ItemBase(instance, (string)row["process"], authenticatedSession);
				var d = ib.FindItems(offset, limit, (int)row["process_portfolio_id"], 0, 0,
					textSearch);
				var data = db.DataTableToDictionary((DataTable)d["rowdata"]);
				foreach (var dd in data) {
					var mStates = new States(instance, authenticatedSession);
					var s = mStates.GetEvaluatedStatesForFeature((string)row["process"], feature,
						Conv.ToInt(dd["item_id"]));
					if (s.States.Contains(state) || s.GetGroupIds(state).Count > 0 || s.GetTabIds(state).Count > 0 || s
							.GetContainerIds(
								state).Count > 0) {
						((Dictionary<int, object>)retval["rowdata"]).Add(Conv.ToInt(dd["item_id"]), dd);
						((Dictionary<int, object>)retval["rowstates"]).Add(Conv.ToInt(dd["item_id"]),
							((Dictionary<int, object>)d["rowstates"])
							[Conv.ToInt(dd["item_id"])]);
					}
				}

				foreach (var id in ((Dictionary<int, object>)d["processes"]).Keys) {
					if (!((Dictionary<int, object>)retval["processes"]).ContainsKey(id)) {
						((Dictionary<int, object>)retval["processes"]).Add(id,
							((Dictionary<int, object>)d["processes"])[id]);
					}
				}
			}

			return retval;
		}

		public string GetProcessPrimary(Dictionary<string, object> item) {
			var pro = item["process"];
			SetDBParam(SqlDbType.NVarChar, "@process", pro);

			var result = "";

			foreach (DataRow row in db.GetDatatable(
				"	SELECT ic.name as name " +
				" FROM process_portfolio_columns ppc  " +
				"	INNER JOIN(  " +
				"	SELECT process, process_portfolio_id, variable, process_portfolio_type  " +
				"	FROM process_portfolios  " +
				"	WHERE process_portfolio_id IN(  " +
				"	SELECT  min(process_portfolio_id) " +
				"	FROM process_portfolios  " +
				"	WHERE process_portfolio_type IN (1, 10) AND instance = @instance and process = @process ) ) pf  " +
				"	ON pf.process_portfolio_id = ppc.process_portfolio_id  " +
				" INNER JOIN item_columns ic  " +
				" ON ic.item_column_id = ppc.item_column_id  " +
				" where use_in_select_result = 1 " +
				" ORDER BY pf.process_portfolio_id, ppc.order_no  ASC", DBParameters).Rows) {

				var col = Conv.ToStr(row["name"]);
				result += " " + (item.ContainsKey(col) ? Conv.ToStr(item[col]) : "");
			}

			return result.TrimStart();
		}
		public string GetProcessPrimary(int itemId) {
			var pro = GetProcessByItemId(itemId);
			SetDBParam(SqlDbType.NVarChar, "@process", pro);
			var sql = " SELECT '' ";
			var first = true;
			//var portfolios = new ProcessPortfolios(instance, pro, authenticatedSession);
			foreach (DataRow row in db.GetDatatable(
				"	SELECT ic.name as name " +
				" FROM process_portfolio_columns ppc  " +
				"	INNER JOIN(  " +
				"	SELECT process, process_portfolio_id, variable, process_portfolio_type  " +
				"	FROM process_portfolios  " +
				"	WHERE process_portfolio_id IN(  " +
				"	SELECT  min(process_portfolio_id) " +
				"	FROM process_portfolios  " +
				"	WHERE process_portfolio_type IN (1, 10) AND instance = @instance and process = @process ) ) pf  " +
				"	ON pf.process_portfolio_id = ppc.process_portfolio_id  " +
				" INNER JOIN item_columns ic  " +
				" ON ic.item_column_id = ppc.item_column_id  " +
				" where use_in_select_result = 1 " +
				" ORDER BY pf.process_portfolio_id, ppc.order_no  ASC", DBParameters).Rows) {
				if (first) {
					sql += " + " + row["name"];
					first = false;
				}
				else {
					sql += " + ' ' + " + row["name"];
				}
			}

			sql += " FROM _" + instance + "_" + pro + " WHERE item_id = @itemId";
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			return Conv.ToStr(db.ExecuteScalar(sql, DBParameters));
		}

		public string GetLinkProcessForItemColum(int itemColumnId) {
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			return Conv.ToStr(db.ExecuteScalar(
				"SELECT link_process FROM item_columns WHERE item_column_id = @itemColumnId", DBParameters));
		}

		public Dictionary<string, object> GetListHierarchy(string process, Dictionary<int, ItemColumn> listCols, bool saveCache = true) {
			var retval = new Dictionary<string, object>();
			var c = Cache.Get(instance, "list_hierarchy_" + process);

			if (c != null) return (Dictionary<string, object>)c;

			var listData = new Dictionary<string, object>();
			var subListD = new Dictionary<string, object>();
			var sublists = new Dictionary<string, List<string>>();

			var processes = new Processes(instance, authenticatedSession);

			foreach (var col in listCols.Values) {
				var colProcess = col.IsSubquery() ? col.SubDataAdditional : col.DataAdditional;
				var parents = new List<Process>();

				if (col.IsSubquery()) colProcess = col.SubDataAdditional;
				if (colProcess != null) parents = processes.GetParentProcesses(true, colProcess);

				if (col.InputMethod == 0 && !listData.ContainsKey(colProcess)) {
					var ib = new ItemCollectionService(instance, colProcess, authenticatedSession);
					var listProcess = processes.GetProcess(colProcess);
					ib.FillItems(listProcess.ListOrder);
					listData.Add(colProcess, ib.GetItems());
				}
				else if (col.InputMethod == 1 && col.InputName.Length > 0) {
					var sl = new SystemList(instance, authenticatedSession);
					listData.Add(col.InputName, sl.GetListItems(col.InputName));
				}

				if (parents.Count != 0) {
					foreach (var p in parents) {
						if (!listData.ContainsKey(p.ProcessName)) {
							var ib =
								new ItemCollectionService(instance, p.ProcessName, authenticatedSession);
							var listProcess = processes.GetProcess(p.ProcessName);
							ib.FillItems(listProcess.ListOrder);
							listData.Add(p.ProcessName, ib.GetItems());
						}

						if (!subListD.ContainsKey(colProcess)) {
							var subListData = new Dictionary<int, object>();
							var ib2 = new ItemCollectionService(instance, colProcess, authenticatedSession);
							foreach (var pData in (List<Dictionary<string, object>>)listData[p.ProcessName]) {
								ib2.ClearMembers();

								var listProcess = processes.GetProcess(colProcess);
								ib2.FillSubItems((int)pData["item_id"], listProcess.ListOrder);

								var getItems1 = ib2.GetItems();
								//expensive operation -> designed for lists that are both sublist and recursive
								if (listProcess.ProcessType == 1) {
									foreach (var x in ib2.GetItems()) {
										FillRecursiveItems(getItems1, ib2, (int)x["item_id"], listProcess.ListOrder);
									}
								}
								subListData.Add((int)pData["item_id"], getItems1);
							}
							subListD.Add(colProcess, subListData);
						}
					}
				}
			}

			foreach (var col in listCols.Values) {
				if (!col.IsSubquery() && col.DataAdditional != null) {
					if (!sublists.ContainsKey(col.DataAdditional))
						sublists.Add(col.DataAdditional, processes.GetSubProcesses(col.DataAdditional));

					if (!listData.ContainsKey(col.DataAdditional))
						listData.Add(col.DataAdditional, new List<Dictionary<string, object>>());
				}
				else if (col.IsSubquery()) {
					if (!listData.ContainsKey(col.SubDataAdditional)) {
						var ib =
							new ItemCollectionService(instance, col.SubDataAdditional, authenticatedSession);
						var listProcess = processes.GetProcess(col.SubDataAdditional);
						ib.FillItems(listProcess.ListOrder);
						listData.Add(col.SubDataAdditional, ib.GetItems());
					}
					if (!sublists.ContainsKey(col.SubDataAdditional))
						sublists.Add(col.SubDataAdditional, processes.GetSubProcesses(col.SubDataAdditional));

				}
			}

			retval.Add("lists", listData);
			retval.Add("sublistsData", subListD);
			retval.Add("sublists", sublists);

			if (saveCache) {
				Cache.Set(instance, "list_hierarchy_" + process, retval);
			}
			return retval;
		}


		private void FillRecursiveItems(List<Dictionary<string, object>> getItems1, ItemCollectionService ib2, int itemId, string order) {
			ib2.ClearMembers();
			ib2.FillSubItems(itemId, order);

			foreach (var x2 in ib2.GetItems()) {
				if (!getItems1.Contains(x2)) {
					getItems1.Add(x2);
				}
				FillRecursiveItems(getItems1, ib2, (int)x2["item_id"], order);
			}
		}



		public class ProcessMapObject {
			public List<ProcessMapObject> children = new List<ProcessMapObject>();
			public string displayName;
			public int item_id;

			public string name;

			//public string tag;
			public string type;

			public ProcessMapObject(string processType, DataRow row) {
				if (processType == "root") {
					item_id = 0;
					name = "Root";
					//	tag = "";
				}
				else {
					item_id = Conv.ToInt(row[processType + "_id"]);
					displayName = Conv.ToStr(row[processType + "_variable"]);
					//	tag = Conv.toStr(row[processType + "_tag"]);
					name = displayName; //+ " " + tag;
				}

				type = processType;
			}

			public ProcessMapObject ProcessChild(ProcessMapObject child) {
				foreach (var c in children) {
					if (c.item_id == child.item_id) {
						return c;
					}
				}

				children.Add(child);
				return child;
			}
		}
	}
}