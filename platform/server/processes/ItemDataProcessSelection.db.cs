﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.processes {
	public class ItemDataProcessSelection {
		public ItemDataProcessSelection() { }

		public ItemDataProcessSelection(DataRow dr, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ItemId = (int) dr["item_id"];
			ItemColumnId = (int) dr["item_column_id"];
			SelectedItemId = (int) dr["selected_item_id"];
		}

		public int ItemId { get; set; }
		public int ItemColumnId { get; set; }
		public int SelectedItemId { get; set; }
	}

	public class ItemDataProcessSelections : ProcessBase {
		private readonly Dictionary<string, bool>
			_privateCache; // This dies when instance of this class dies (after request is done)

		private readonly Dictionary<string, DataTable> BatchList = new Dictionary<string, DataTable>();

		public ItemDataProcessSelections(string instance, string process, AuthenticatedSession authenticatedSession) :
			base(
				instance, process, authenticatedSession) {
			this.instance = instance;
			this.process = process;
			_privateCache = new Dictionary<string, bool>();
		}


		public List<ItemDataProcessSelection> GetItemDataProcessSelection(int itemId, int itemColumnId) {
			var par = new List<SqlParameter> {
				new("@itemId", SqlDbType.Int) {Value = itemId},
				new("@itemColumnId", SqlDbType.Int) {Value = itemColumnId}
			};
			var retval = new List<ItemDataProcessSelection>();
			foreach (DataRow row_ in db
				.GetDatatable(
					"SELECT item_id, item_column_id, selected_item_id FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId",
					par).Rows) {
				retval.Add(new ItemDataProcessSelection(row_));
			}

			return retval;
		}

		public DataTable GetItemDataProcessSelectionCacheTable(List<int> itemColumnIds) {
			if (itemColumnIds.Count > 0)
				return db.GetDatatable(
					"SELECT item_id, item_column_id, selected_item_id, link_item_id FROM item_data_process_selections WHERE item_column_id IN (" +
					Conv.ToDelimitedString(itemColumnIds) + ")",
					null);

			return new DataTable();
		}

		public List<int> GetItemDataProcessSelectionIds(int itemId, int itemColumnId, DataTable selectionTable = null) {
			if (Cache.Get(instance, "idps_" + itemId + " " + itemColumnId) != null) {
				return (List<int>) Cache.Get(instance, "idps_" + itemId + " " + itemColumnId);
			}

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);

			DataRow[] dt = null;
			if (selectionTable != null) {
				var f = "item_column_id = " + itemColumnId;
				
				if (selectionTable.Columns.Contains("item_id"))
					f += " AND item_id = " + itemId;
				
				dt = selectionTable.Select( f);
			} else {
				dt = db.GetDatatable(
					"SELECT selected_item_id, link_item_id FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId",
					DBParameters).Select();
			}

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			var retval = new List<int>();
			foreach (var row_ in dt) {
				retval.Add(Conv.ToInt(row_["selected_item_id"]));
			}

			Cache.Set(instance, "idps_" + itemId + " " + itemColumnId, retval);

			return retval;
		}

		public Dictionary<int, Dictionary<string, object>> GetItemDataProcessSelectionIds(int itemId, int itemColumnId,
			string linkProcess, DataTable allData = null, DataTable selectionTable = null) {
//			if (Cache.Get(instance, "idpsl_" + itemId + " " + itemColumnId) != null) {
//				return (Dictionary<int, Dictionary<string, object>>) Cache.Get(instance, "idpsl_" + itemId + " " + itemColumnId);
//			}

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			var retval = new Dictionary<int, Dictionary<string, object>>();

			DataRow[] dt = null;
			if (selectionTable != null) {
				dt = selectionTable.Select("item_column_id = " + itemColumnId);
			} else {
				dt = db.GetDatatable(
					"SELECT selected_item_id, link_item_id FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId",
					DBParameters).Select();
			}


			foreach (var row_ in dt) {
				if (!DBNull.Value.Equals(row_["link_item_id"])) {
					var entityService = new DatabaseItemService(instance, linkProcess, authenticatedSession);
					if (allData != null) {
						retval.Add(Conv.ToInt(row_["selected_item_id"]),
							entityService.GetItem(Conv.ToInt(row_["link_item_id"]), allData, false));
					} else {
						retval.Add(Conv.ToInt(row_["selected_item_id"]),
							entityService.GetItem(Conv.ToInt(row_["link_item_id"]), checkRights: false));
					}
				} else {
					var linkIb = new ItemBase(instance, linkProcess, authenticatedSession);
					var linkItemId = linkIb.InsertItemRow();
					SetDBParam(SqlDbType.Int, "@linkItemId", linkItemId);
					SetDBParam(SqlDbType.Int, "@selectedItemId", Conv.ToInt(row_["selected_item_id"]));
					db.ExecuteUpdateQuery(
						"UPDATE item_data_process_selections SET link_item_id = @linkItemId WHERE item_id=@itemId AND item_column_id = @itemColumnId AND selected_item_id = @selectedItemId   ",
						DBParameters);
					retval.Add(Conv.ToInt(row_["selected_item_id"]), linkIb.GetItem(linkItemId, checkRights: false));
				}
			}

			//Cache.Set(instance, "idpsl_" + itemId + " " + itemColumnId, retval);

			return retval;
		}


		private Dictionary<string, List<ItemDataProcessSelection>> _idpsCache =
			new Dictionary<string, List<ItemDataProcessSelection>>();

		public List<ItemDataProcessSelection> GetItemDataProcessSelection(int itemId, int itemColumnId,
			DataTable selections = null) {
			var id = itemId + "_" + itemColumnId;
			if (_idpsCache.ContainsKey(id)) return _idpsCache[id];

			var retval = new List<ItemDataProcessSelection>();
			if (selections != null) {
				foreach (var row_ in selections.Select("item_id = " + itemId + " AND " + " item_column_id = " +
				                                       itemColumnId)) {
					retval.Add(new ItemDataProcessSelection(row_));
				}
			} else {
				SetDBParam(SqlDbType.Int, "@itemId", itemId);
				SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
				foreach (DataRow row_ in db
					.GetDatatable(
						"SELECT item_id, item_column_id, selected_item_id FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId",
						DBParameters).Rows) {
					retval.Add(new ItemDataProcessSelection(row_));
				}
			}

			_idpsCache.Add(id, retval);
			return retval;
		}

		public string GetItemDataProcessSelectionSql(string itemIdColumn, int itemColumnId,
			string selectColumn = "selected_item_id") {
			return
				"SELECT " + Conv.ToSql(selectColumn) + " FROM item_data_process_selections WHERE item_id = " +
				Conv.ToSql(itemIdColumn) + " AND item_column_id = " + itemColumnId;
		}

		public List<ItemDataProcessSelection> GetParentItemDataProcessSelection(int selectedItemId, int itemColumnId,
			DataTable selections = null) {
			var retval = new List<ItemDataProcessSelection>();
			if (selections != null) {
				foreach (var row_ in selections.Select("selected_item_id = " + selectedItemId + " AND " +
				                                       " item_column_id = " +
				                                       itemColumnId)) {
					retval.Add(new ItemDataProcessSelection(row_));
				}
			} else {
				SetDBParam(SqlDbType.Int, "@selectedItemId", selectedItemId);
				SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
				foreach (DataRow row_ in db
					.GetDatatable(
						"SELECT item_id, item_column_id, selected_item_id FROM item_data_process_selections WHERE selected_item_id = @selectedItemId AND item_column_id = @itemColumnId",
						DBParameters).Rows) {
					retval.Add(new ItemDataProcessSelection(row_));
				}
			}

			return retval;
		}

		public string GetParentItemDataProcessSelectionSql(string selectedItemIdColumn, int itemColumnId,
			string selectColumn = "item_id") {
			return
				"SELECT " + Conv.ToSql(selectColumn) + " FROM item_data_process_selections WHERE selected_item_id = " +
				Conv.ToSql(selectedItemIdColumn) + " AND item_column_id = " + itemColumnId;
		}

		public List<ItemDataProcessSelection> GetArchiveItemDataProcessSelection(DateTime? archiveDate, int itemId,
			int itemColumnId) {
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
			var retval = new List<ItemDataProcessSelection>();
			foreach (DataRow row_ in db
				.GetDatatable(
					"SELECT item_id, item_column_id, selected_item_id FROM get_item_data_process_selections(@archiveDate) WHERE item_id = @itemId AND item_column_id = @itemColumnId",
					DBParameters).Rows) {
				retval.Add(new ItemDataProcessSelection(row_));
			}

			return retval;
		}

		public List<int> GetArchiveItemDataProcessSelectionIds(DateTime? archiveDate, int itemId, int itemColumnId) {
			var id = itemId + "_" + Conv.ToStr(archiveDate);
			var retval = new List<int>();

			DataTable tempTable;
			if (BatchList.ContainsKey(id)) {
				tempTable = BatchList[id];
			} else {
				SetDBParam(SqlDbType.Int, "@itemId", itemId);
				SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
				SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);

				var sql =
					" 				SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start] " +
					" 				FROM archive_item_data_process_selections WHERE archive_id IN ( " +
					" 					SELECT MAX(archive_id) " +
					" 					FROM archive_item_data_process_selections " +
					" 					WHERE archive_start <= @archiveDate AND archive_end > @archiveDate AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN " +
					" 					(SELECT CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) FROM  item_data_process_selections  WHERE archive_start <= @archiveDate and item_id = @itemId ) and item_id = @itemId " +
					" 					GROUP BY [item_column_id],[item_id],[selected_item_id] " +
					" 					) " +
					" 				UNION  " +
					" 				SELECT [item_id],[item_column_id],[selected_item_id],[link_item_id],[archive_userid],[archive_start]  " +
					" 				FROM item_data_process_selections  " +
					" 				WHERE archive_start <= @archiveDate and item_id = @itemId ";

				//TODO: Maybe new function ?

				//tempTable = db.GetDatatable("SELECT selected_item_id, item_column_id FROM get_item_data_process_selections_with_ids(@archiveDate, @itemColumnId, @itemId) WHERE item_id = @itemId", DBParameters);
				tempTable = db.GetDatatable(sql, DBParameters);
				BatchList.Add(id, tempTable);
			}

			foreach (var row in tempTable.Select("item_column_id = " + itemColumnId)) {
				retval.Add(Conv.ToInt(row["selected_item_id"]));
			}

			return retval;
		}

		private Dictionary<string, bool> _insCache = new Dictionary<string, bool>();

		public bool IdInSelections(int itemId, int itemColumnId, int id) {
			var key = itemId + "_" + itemColumnId + "_" + id;
			if (_insCache.ContainsKey(key)) return _insCache[key];

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);
			SetDBParam(SqlDbType.Int, "@selectedItemId", id);

			var result = Conv.ToBool(db.ExecuteScalar(
				"SELECT COUNT(selected_item_id) FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId AND selected_item_id = @selectedItemId",
				DBParameters));

			_insCache.Add(key, result);
			return result;
		}
	}
}