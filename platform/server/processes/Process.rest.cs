﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.database;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/settings/Processes")]
	public class ProcessesRest : PageBase {
		public ProcessesRest() : base("settings") { }

		// GET: model/values
		[HttpGet("")]
		public List<Process> Get() {
			var process = new Processes(instance, currentSession);
			return process.GetProcesses();
		}

		[HttpGet("map/{process}")]
		public Processes.ProcessMapObject GetProcessMap(string process) {
			var p = new Processes(instance, currentSession);
			return p.GetProcessMap(process);
		}

		[HttpPost("search/{searchWord}/{process}")]
		public List<SearchResult>
			GetSearchResults(string searchWord, string process, [FromBody] List<string> elements) {
			var p = new Processes(instance, currentSession);
			return p.GetSearchResult(searchWord, elements, process);
		}

		[HttpGet("{variable}/unique")]
		public bool GetUnique(string variable) {
			var process = new Processes(instance, currentSession);
			return process.IsUnique(variable);
		}

		[HttpGet("{processType}")]
		public List<Process> Get(byte processType) {
			var process = new Processes(instance, currentSession);
			return process.GetProcesses(processType);
		}

		[HttpGet("{processType}/{lists}")]
		public List<Process> Get(byte processType, bool lists) {
			var process = new Processes(instance, currentSession);
			return process.GetProcesses(processType, lists);
		}

		[HttpGet("{processType}/{process}")]
		public Process Get(byte processType, string process) {
			var processes = new Processes(instance, currentSession);
			return processes.GetProcess(processType, process);
		}

		[HttpGet("listHierarchy/{process}")]
		public Dictionary<string, object> Get(string process) {
			var iCols = new ItemColumns(instance, process, currentSession);
			var listCols = new Dictionary<int, ItemColumn>();
			foreach (var col in iCols.GetItemColumns()) {
				if (col.DataType == 6 && col.DataAdditional != null) {
					listCols.Add(col.ItemColumnId, col);
				}

				if (col.IsSubquery() && col.IsOfSubType(ItemColumn.ColumnType.List)) {
					listCols.Add(col.ItemColumnId, col);
				}
			}

			return new Processes(instance, currentSession).GetListHierarchy(process, listCols);
		}

		[HttpGet("listHierarchy/{process}/{portfolioId}")]
		public Dictionary<string, object> Get(string process, int portfolioId) {
			return Get(process, portfolioId, false);
		}
		[HttpGet("listHierarchy/{process}/{portfolioId}/{editable}")]
		public Dictionary<string, object> Get(string process, int portfolioId, bool editable) {
			var pc = new ProcessPortfolioColumns(instance, process, currentSession);
			var listCols = new Dictionary<int, ItemColumn>();

			var unionPortfolio =
				new ProcessPortfolios(instance, process, currentSession).GetPortfolio(portfolioId);
			if (unionPortfolio.IsUnionPortfolio) return new Dictionary<string, object>();

			foreach (var col in pc.GetColumns(portfolioId)) {
				if ((col.UseInFilter || (editable && IsSubList(col.ItemColumn))) && (col.ItemColumn.DataType == 6 && col.ItemColumn.DataAdditional != null) ||
					(col.ItemColumn.IsSubquery() && col.ItemColumn.IsOfSubType(ItemColumn.ColumnType.List)))
					listCols.Add(col.ItemColumn.ItemColumnId, col.ItemColumn);
			}

			return new Processes(instance, currentSession).GetListHierarchy(process, listCols, saveCache: false);
		}

		private bool IsSubList(ItemColumn itemColumn) {
			if (itemColumn.DataType == 6 && itemColumn.DataAdditional != null && itemColumn.Validation.ReadOnly != true) {
				var cb = new ConnectionBase(instance, currentSession);
				cb.SetDBParam(SqlDbType.NVarChar, "@process", itemColumn.DataAdditional);
				if (Conv.ToInt(cb.db.ExecuteScalar("SELECT COUNT(*) FROM process_subprocesses WHERE process = @process", cb.DBParameters)) > 0) return true;
			}
			return false;
		}

		[HttpPost("{isExecuted}")]
		public IActionResult Post(int isExecuted, [FromBody] Process process) {
			CheckRight("write");
			var processes = new Processes(instance, currentSession);
			if (ModelState.IsValid) {
				processes.Add(isExecuted, process);
				return new ObjectResult(process);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut]
		public IActionResult Put([FromBody] List<Process> process) {
			CheckRight("write");
			var processes = new Processes(instance, currentSession);
			if (ModelState.IsValid) {
				foreach (var pc in process) {
					processes.Save(pc);
				}

				return new ObjectResult(process);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{process}")]
		public IActionResult Delete(string process) {
			CheckRight("delete");
			var processes = new Processes(instance, currentSession);
			foreach (var id in process.Split(',')) {
				processes.Delete(id);
			}

			return new StatusCodeResult(200);
		}

		[HttpGet("processFeatures/{process}")]
		public DataTable GetFeatures(string process) {
			return new Processes(instance, currentSession).GetProcessFeatures(process);
		}
	}
}