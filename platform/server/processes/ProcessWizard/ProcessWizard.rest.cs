using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.layout;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/settings/ProcessWizard")]
	public class ProcessWizardRest : PageBase {
		public ProcessWizardRest() : base("settings") { }

		[HttpPost("init/{isExecuted}")]
		public Process InitializeWizard(int isExecuted, [FromBody] Process process) {
			CheckRight("write");
			var wizard = new ProcessWizardBase(instance, currentSession, process.Variable);
			if (ModelState.IsValid) {
				//Here we check what are the additional languages besides Finnish and English. In this moment only supports German xoxo
				var cb = new ConnectionBase(instance, currentSession);
				var additionalLanguages = new List<string>();
				var german = Conv.ToInt(cb.db.ExecuteScalar("select COUNT(*) from instance_languages where code = 'de-De'"));
				if (german == 1)
					additionalLanguages.Add("de-DE");
				return wizard.CreateProcess(isExecuted, process, additionalLanguages);
			}
			return null;
		}

		[HttpPost("{process}")]
		public int DoWizard(string process, [FromBody] List<ProcessGroup> Phases) {
			CheckRight("write");
			var wizard = new ProcessWizardBase(instance, currentSession, process);
			if (ModelState.IsValid) {

				//Here we check what are the additional languages besides Finnish and English. In this moment only supports German xoxo
				var cb = new ConnectionBase(instance, currentSession);
				var additionalLanguages = new List<string>();
				var german = Conv.ToInt(cb.db.ExecuteScalar("select COUNT(*) from instance_languages where code = 'de-De'"));
				if (german == 1)
					additionalLanguages.Add("de-DE");
				return wizard.DoWizardMagic(process, Phases, additionalLanguages);
			}
			return 0;
		}

	}
}
