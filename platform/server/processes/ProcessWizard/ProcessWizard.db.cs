using System;
using System.Collections.Generic;
using System.Data;
using DocumentFormat.OpenXml.Spreadsheet;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.actions;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.core
{
	public class ProcessWizardBase : ConnectionBase
	{
		private AuthenticatedSession session;



		public ProcessWizardBase(string instance, AuthenticatedSession session, string process) : base(instance,
			session) {
			this.instance = instance;
			this.session = session;
		}



		private void InvalidateCaches(string process) {
			// Cache.Remove(instance, "list_hierarchy_" + process);
			// Cache.RemoveGroup("list_hierarchy_meta_" + process);
			Cache.Initialize();
			var signalR = new ApiPush(Startup.SignalR);
			var parameters = new List<object> {process};
			signalR.SendToAll("ListChanged", parameters);
		}


		public Process CreateProcess(int isExecuted, Process p, List<string> additionalLanguages) {
			var processes = new Processes(instance, session);
			var NewProcess = processes.Add(isExecuted, p);
			processes.Add(0, GiveStatusesList(NewProcess.Variable, additionalLanguages));
			return NewProcess;
		}


		private Process GiveStatusesList(string processVariable, List<string> additionalLanguages) {
			var ListProcess = new Process();
			ListProcess.Variable = "list_phases_" + processVariable;
			ListProcess.ListProcess = true;
			ListProcess.SystemProcess = false;
			ListProcess.LanguageTranslation = new Dictionary<string, string>();

			ListProcess.LanguageTranslation["en-GB"] = processVariable + " Phases";
			ListProcess.LanguageTranslation["fi-FI"] = processVariable + " Vaiheet";

			if (additionalLanguages.Contains("de-De")) {
				ListProcess.LanguageTranslation["de-DE"] = processVariable + " Phases";
			}

			return ListProcess;
		}

		private int lastAddedItemColumnId;
		private static int detailsTabId;
		private string currentState1;
		private int WriteExistingCondition;
		private int ReadExistingCondition;
		private int processPortfolioId;
		private int closedItemId;
		private int processCreateActionId;
		private int adminGroupId;
		private int selectorPortfolioId;
		private List<int> PhaseActions = new List<int>();
		private Dictionary<int, int> PhaseButtonContainers = new Dictionary<int, int>();
		private Dictionary<int, int> PhaseConditionGroups = new Dictionary<int, int>();
		private List<int> PhaseListItems = new List<int>();
		private Dictionary<int, int> PhaseListCollection = new Dictionary<int, int>();
		private List<int> PhaseDialogs = new List<int>();
		private List<ProcessGroup> PhaseList = new List<ProcessGroup>();
		private List<ItemColumn> PortfolioColumns = new List<ItemColumn>();
		private List<string> AdditionalLanguages = new List<string>();
		private bool hasAddLanguages;
		private ItemColumn Title;

		private void AddItemColumn(string processName,
			int containerId,
			Dictionary<string, string> lt,
			int dataType,
			string colName,
			string orderNo,
			string dataAdditional) {

			ItemColumns icx = null;
			ProcesContainerColumns pcc = null;
			if (icx == null) {
				icx = new ItemColumns(instance, processName, session);
			}

			if (pcc == null) {
				pcc = new ProcesContainerColumns(instance, processName, session);
			}

			var ItemColumn_ = new ItemColumn();
			if (colName == "project_state") {
				ItemColumn_.InConditions = true;
			}

			ItemColumn_.Process = processName;
			ItemColumn_.Name = colName;
			ItemColumn_.DataType = dataType;
			ItemColumn_.OrderNo = orderNo;
			ItemColumn_.StatusColumn = false;
			ItemColumn_.LanguageTranslation = lt;
			ItemColumn_.LanguageTranslationRaw = lt;
			ItemColumn_.DataAdditional = null;

			if (dataAdditional != "") {
				ItemColumn_.DataAdditional = dataAdditional;
			}

			if (colName == "title")
				Title = ItemColumn_;

			var ColumnInContainer = new ProcessContainerField();
			ColumnInContainer.OrderNo = orderNo;
			ColumnInContainer.ItemColumn = icx.Add(processName, ItemColumn_, true, false);
			ColumnInContainer.ProcessContainerId = containerId;
			ColumnInContainer.Process = processName;

			if (ColumnInContainer.ItemColumn.Name == "serial") {
				ColumnInContainer.Validation = new FieldValidation();
				ColumnInContainer.Validation.ReadOnly = true;
			}
			pcc.Add(ColumnInContainer);
			if (ItemColumn_.Name == "project_state") {
				lastAddedItemColumnId = ColumnInContainer.ItemColumn.ItemColumnId;
			} else if (ItemColumn_.Name == "serial" || ItemColumn_.Name == "title" || ItemColumn_.Name == "start_date" ||
			      ItemColumn_.Name == "end_date" || ItemColumn_.Name == "progress_arrows") {
				PortfolioColumns.Add(ColumnInContainer.ItemColumn);
			}
		}

		private Dictionary<string, string> TranslationFactory(string engTrans, string finTrans, string gerTrans = "") {
			var trans1 = new Dictionary<string, string>();
			trans1["en-GB"] = engTrans;
			trans1["fi-FI"] = finTrans;

			if (gerTrans != "") {
				trans1["de-DE"] = gerTrans;
			}

			return trans1;
		}

		private void CreateGlobalConditions(string processName) {
			var c = new Conditions(instance, processName, session);
			var stateWrite = new List<string>();
			var stateRead = new List<string>();
			var stateDelete = new List<string>();
			var stateEmpty = new List<string>();
			var stateHelp = new List<string>();
			var stateConfigure = new List<string>();

			var metafeature = new List<string>();
			var portfolioFeature = new List<string>();
			metafeature.Add("meta");
			stateRead.Add("meta.read");
			stateWrite.Add("meta.write");
			stateDelete.Add("meta.delete");
			stateHelp.Add("meta.help");
			stateConfigure.Add("meta.configure");
			portfolioFeature.Add("portfolio");

			var gerWrite = "";
			var gerRead = "";
			var gerDelete = "";
			var gerPort = "";
			var gerHelp = "";
			var gerConf = "";

			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				gerWrite = "Schreiben";
				gerRead = "Lesen";
				gerDelete = "Löschen";
				gerPort = "Portfolio";
				gerHelp = "Help";
				gerConf = "Configure";
			}

			WriteExistingCondition = ConditionFactory(0, processName, stateEmpty, stateEmpty,
				TranslationFactory("Write right", "Kirjoitusoikeus", gerWrite), c);
			ReadExistingCondition = ConditionFactory(0, processName, stateEmpty, stateEmpty,
				TranslationFactory("Read right", "Lukuoikeus", gerRead), c);

			ConditionFactory(0, processName,  new List<string> {"portfolio.read"} , portfolioFeature,
				TranslationFactory("Portfolio", "Portfolio", gerPort), c);

			ConditionFactory(0, processName, stateDelete, metafeature,
				TranslationFactory("Delete", "Poisto-oikeus", gerDelete), c);

			ConditionFactory(0, processName, stateHelp, metafeature,
				TranslationFactory("Help", "Aputekstit", gerHelp), c);
			ConditionFactory(0, processName, stateConfigure, metafeature,
				TranslationFactory("Configure", "Konfigurointi", gerConf), c);

		}

		public int DoWizardMagic(string processName, List<ProcessGroup> Phases, List<String> additionalLanguages_) {
			if (additionalLanguages_.Count > 0) {
				hasAddLanguages = true;
			}

			adminGroupId = Conv.ToInt(db.ExecuteScalar("SELECT TOP 1 (item_id) FROM _" + instance + "_user_group"));

			AdditionalLanguages = additionalLanguages_;
			SetDBParam(SqlDbType.NVarChar, "@process", processName);
			var listName = "list_phases_" + processName;

			DataRow dr =
				db.GetDataRow(
					"SELECT process_container_id FROM process_containers WHERE right_container = 1 AND process = @process ",
					DBParameters);

			int accessContainerId = Conv.ToInt(dr["process_container_id"]);

			var ProcessGroups = new ProcessGroups(instance, processName, session);
			var ProcessTabs = new ProcessTabs(instance, processName, session);
			var ProcessContainers = new ProcessContainers(instance, processName, session);
			var ProcessTabContainers = new ProcessTabContainers(instance, processName, session);
			var listItemBase = new ItemBase(instance, listName, session);
			var GroupTabs = new ProcessGroupTabs(instance, processName, session);
			var ProcessActionDialogData = new ProcessActionDialogData(instance, processName, session);
			var c = new Conditions(instance, processName, session);


			var currentStateTrans = new Dictionary<string, string>();
			var progressArrowsTrans = new Dictionary<string, string>();
			var projectStateTrans = new Dictionary<string, string>();
			var creatorTrans = new Dictionary<string, string>();
			var creationDateTrans = new Dictionary<string, string>();

			currentStateTrans["en-GB"] = "Current State";
			currentStateTrans["fi-FI"] = "Current State";
			progressArrowsTrans["en-GB"] = "Progress Arrows";
			progressArrowsTrans["fi-FI"] = "Tilanuolet";
			projectStateTrans["en-GB"] = "Phase";
			projectStateTrans["fi-FI"] = "Vaihe";
			creatorTrans["en-GB"] = "Creator";
			creatorTrans["fi-FI"] = "Luoja";
			creationDateTrans["en-GB"] = "Creation Date";
			creationDateTrans["fi-FI"] = "Luonti pvm.";


			//additional languages block
			if (hasAddLanguages) {
				if (additionalLanguages_.Contains("de-DE")) {
					currentStateTrans["de-DE"] = "Current State";
					progressArrowsTrans["de-DE"] = "Fortschrittspfeile";
					projectStateTrans["de-DE"] = "Phase";
					creatorTrans["de-DE"] = "Schöpferin";
					creationDateTrans["de-DE"] = "Schöpferin Datum";
				}
			}

			AddItemColumn(processName,
				accessContainerId,
				currentStateTrans, 0, "current_state", "1", "");

			AddItemColumn(processName,
				accessContainerId,
				projectStateTrans, 6, "project_state", "3", "list_phases_" + processName);

			AddItemColumn(processName,
				accessContainerId,
				creatorTrans, 5, "creator", "4", "user");

			AddItemColumn(processName,
				accessContainerId,
				creationDateTrans, 3, "creation_date", "5", "");

			AddItemColumn(processName,
				accessContainerId,
				progressArrowsTrans, 60, "progress_arrows", "9", "");


			var phaseItemPairs = new Dictionary<int, int>();

			int PhaseOrderNo = 1;

			var DetailsTab = new ProcessTab();
			DetailsTab.ProcessTabId = 0;
			DetailsTab.OrderNo = "0";
			DetailsTab.LanguageTranslation = new Dictionary<string, string>();
			DetailsTab.LanguageTranslation["en-GB"] = "Details";
			DetailsTab.LanguageTranslation["fi-FI"] = "Perustiedot";

			if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
				DetailsTab.LanguageTranslation["de-DE"] = "Detail";
			}

			ProcessTabs.Add(DetailsTab);

			var DetailsContainer = new ProcessContainer();
			DetailsContainer.ProcessContainerId = 0;
			DetailsContainer.LanguageTranslation = new Dictionary<string, string>();
			DetailsContainer.LanguageTranslation["en-GB"] = "Project details";
			DetailsContainer.LanguageTranslation["fi-FI"] = "Perustiedot";

			if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
				DetailsContainer.LanguageTranslation["de-DE"] = "Detail";
			}

			var dc = ProcessContainers.Add(DetailsContainer);

			detailsTabId = Conv.ToInt(db.ExecuteScalar(
				"SELECT TOP 1 process_tab_id FROM process_tabs WHERE process = @process ORDER BY process_tab_id DESC ",
				DBParameters));

			currentState1 = "tab_" + detailsTabId;

			var ContainerInTab = new ProcessTabContainer();
			ContainerInTab.ProcessContainer = dc;
			ContainerInTab.ProcessTab = DetailsTab;
			ContainerInTab.ProcessContainerSide = 1;
			ContainerInTab.OrderNo = "1";

			ProcessTabContainers.Add(ContainerInTab);

			var sdt = new Dictionary<string, string>();
			var edt = new Dictionary<string, string>();
			var td = new Dictionary<string, string>();
			var sd = new Dictionary<string, string>();

			sdt["en-GB"] = "Start Date";
			sdt["fi-FI"] = "Aloitus pvm.";
			edt["en-GB"] = "End Date";
			edt["fi-FI"] = "Lopetus pvm.";
			td["en-GB"] = "Title";
			td["fi-FI"] = "Nimi";
			sd["en-GB"] = "serial";
			sd["fi-FI"] = "serial";

			if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
				sdt["de-DE"] = "Startdatum";
				edt["de-DE"] = "Enddatum";
				td["de-DE"] = "Titel";
				sd["de-DE"] = "serial";
			}

			AddItemColumn(processName,
				dc.ProcessContainerId,
				sdt, 3, "start_date", "B", "");

			AddItemColumn(processName,
				dc.ProcessContainerId,
				edt, 3, "end_date", "C", "");

			AddItemColumn(processName,
				dc.ProcessContainerId,
				td, 0, "title", "A", "");

			AddItemColumn(processName,
				dc.ProcessContainerId,
				sd, 0, "serial", "D", "");

			CreatePortfolios(processName);

			var firstId = 0;
			var isFirst = true;

			foreach (var phase in Phases) {
				var AddedPhase = ProcessGroups.Add(phase);

				var item_id = listItemBase.InsertItemRow();
				if (isFirst)
					firstId = item_id;
				isFirst = false;
				PhaseListCollection.Add(AddedPhase.ProcessGroupId, item_id);

				phaseItemPairs.Add(AddedPhase.ProcessGroupId, item_id);

				CreateDialog(AddedPhase.LanguageTranslation, ProcessActionDialogData);

				var ListItem = new Dictionary<string, object>();
				ListItem.Add("item_id", item_id);
				ListItem.Add("list_item", phase.LanguageTranslation["en-GB"]);
				ListItem.Add("process", "list_states_" + processName);

				listItemBase.SaveItem(ListItem);

				PhaseConditionGroups.Add(AddedPhase.ProcessGroupId,
					ConditionGroupFactory(processName, AddedPhase.LanguageTranslation["en-GB"],
						Conv.ToStr(PhaseOrderNo), c));

				var GateTab = new ProcessTab();

				GateTab.ProcessTabId = 0;
				GateTab.OrderNo = Conv.ToStr(PhaseOrderNo);
				GateTab.LanguageTranslation = new Dictionary<string, string>();
				GateTab.LanguageTranslation["en-GB"] = "G" + Conv.ToStr(PhaseOrderNo);
				GateTab.LanguageTranslation["fi-FI"] = "G" + Conv.ToStr(PhaseOrderNo);


				if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
					GateTab.LanguageTranslation["de-DE"] = "G" + Conv.ToStr(PhaseOrderNo);
				}

				ProcessTabs.Add(GateTab);

				var GateTabInPhase = new ProcessGroupTab();
				GateTabInPhase.ProcessGroup = AddedPhase;
				GateTabInPhase.ProcessTab = GateTab;
				GateTabInPhase.OrderNo = GateTab.OrderNo;
				GateTabInPhase.ProcessGroupTabId = 0;

				var DetailsTabInPhase = new ProcessGroupTab();
				DetailsTabInPhase.ProcessGroup = AddedPhase;
				DetailsTabInPhase.ProcessTab = DetailsTab;
				DetailsTabInPhase.OrderNo = DetailsTab.OrderNo;
				DetailsTabInPhase.ProcessGroupTabId = 0;

				GroupTabs.Add(DetailsTabInPhase);
				GroupTabs.Add(GateTabInPhase);

				PhaseList.Add(AddedPhase);
				var creatorT = new Dictionary<string, string>();
				var dateT = new Dictionary<string, string>();


				creatorT["en-GB"] = "Decision Creator " + "G" + Conv.ToStr(PhaseOrderNo);
				creatorT["fi-FI"] = "Päätöksen tekijä " + "G" + Conv.ToStr(PhaseOrderNo);
				dateT["en-GB"] = "Decision Date " + "G" + Conv.ToStr(PhaseOrderNo);
				dateT["fi-FI"] = "Päätöksen pvm. " + "G" + Conv.ToStr(PhaseOrderNo);

				var GateContainer = new ProcessContainer();
				GateContainer.ProcessContainerId = 0;
				GateContainer.LanguageTranslation = new Dictionary<string, string>();
				GateContainer.LanguageTranslation["en-GB"] = "G" + Conv.ToStr(PhaseOrderNo) + " " + "Decision details";
				GateContainer.LanguageTranslation["fi-FI"] = "G" + Conv.ToStr(PhaseOrderNo) + " " + "Päätöksen tiedot";
				if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
					GateContainer.LanguageTranslation["de-DE"] =
						"G" + Conv.ToStr(PhaseOrderNo) + " " + "Entscheidung detail";
					creatorT["de-DE"] = "G" + Conv.ToStr(PhaseOrderNo) + " " + "Entscheidung Schöpferin";
					dateT["de-DE"] = "G" + Conv.ToStr(PhaseOrderNo) + " " + "Entscheidung Datum";
				}

				var gc = ProcessContainers.Add(GateContainer);

				this.AddItemColumn(processName,
					gc.ProcessContainerId,
					creatorT, 5, "decision_creator_" + "G" + Conv.ToStr(PhaseOrderNo), "4", "user");

				this.AddItemColumn(processName,
					gc.ProcessContainerId,
					dateT, 3, "decision_date" + "G" + Conv.ToStr(PhaseOrderNo), "5", "");

				var TabContainerGate = new ProcessTabContainer();
				TabContainerGate.ProcessContainer = gc;
				TabContainerGate.ProcessTab = GateTab;
				TabContainerGate.ProcessContainerSide = 1;
				TabContainerGate.OrderNo = "1";

				ProcessTabContainers.Add(TabContainerGate);

				var Container = new ProcessContainer();
				Container.ProcessContainerId = 0;
				Container.LanguageTranslation = new Dictionary<string, string>();
				Container.LanguageTranslation["en-GB"] = "Buttons " + phase.LanguageTranslation["en-GB"];
				Container.LanguageTranslation["fi-FI"] = "Nappulat " + phase.LanguageTranslation["en-GB"];
				if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
					Container.LanguageTranslation["de-DE"] = "Taste " + phase.LanguageTranslation["de-DE"];
				}

				var np = ProcessContainers.Add(Container);
				PhaseButtonContainers.Add(AddedPhase.ProcessGroupId, np.ProcessContainerId);

				var TabContainer = new ProcessTabContainer();
				TabContainer.ProcessContainer = np;
				TabContainer.ProcessTab = GateTab;
				TabContainer.ProcessContainerSide = 3;
				TabContainer.OrderNo = "2";

				ProcessTabContainers.Add(TabContainer);

				CreatePhaseActions(processName, item_id, lastAddedItemColumnId, phase.LanguageTranslation["en-GB"]);

				PhaseListItems.Add(item_id);

				PhaseOrderNo++;
			}
			CreateMenuItems(additionalLanguages_);

			InvalidateCaches(listName);

			/*var ListItemClosed = new Dictionary<string, object>();
			ListItemClosed.Add("item_id", listItemBase.InsertItemRow());
			ListItemClosed.Add("list_item", "Closed");
			ListItemClosed.Add("process", "list_states_" + processName);

			var closedItem = listItemBase.SaveItem(ListItemClosed);
			closedItemId = Conv.ToInt(closedItem["item_id"]);

			CreatePhaseActions(processName, Conv.ToInt(closedItem["item_id"]), lastAddedItemColumnId, "Closed");
			var closedick = new Dictionary<string, string>();
			closedick["en-GB"] = "Closed";
			closedick["fi-FI"] = "Suljettu";

			if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
				closedick["de-DE"] = "Geschlossen";
			}

			CreateDialog(closedick, ProcessActionDialogData);*/

			var featuresList = new List<string>();
			var featuresListWrite = new List<string>();
			var statesListRead = new List<string>();
			var stateListWrite = new List<string>();
			var stateListHistory = new List<string>();
			featuresList.Add("meta");
			featuresListWrite.Add("meta");
			featuresList.Add("progressProgress");

			statesListRead.Add("meta.read");
			statesListRead.Add("meta.active");
			statesListRead.Add("progressProgress.yellow");

			stateListWrite.Add("meta.write");

			stateListHistory.Add("meta.history");
			stateListHistory.Add("progressProgress.grey");
			stateListHistory.Add("meta.read");
			var phaseIndex = 0;
			CreateGlobalConditions(processName);

			SetCreationContainer(processName, additionalLanguages_, ProcessContainers);

			foreach (ProcessGroup phaseD in PhaseList) {
				Dictionary<string, string> writeTranslation = new Dictionary<string, string>();
				Dictionary<string, string> readTranslation = new Dictionary<string, string>();
				Dictionary<string, string> historyTranslation = new Dictionary<string, string>();

				writeTranslation["en-GB"] = "Write " + phaseD.LanguageTranslation["en-GB"];
				writeTranslation["fi-FI"] = "Kirjoitus " + phaseD.LanguageTranslation["fi-FI"];
				readTranslation["en-GB"] = "Read " + phaseD.LanguageTranslation["en-GB"];
				readTranslation["fi-FI"] = "Luku " + phaseD.LanguageTranslation["fi-FI"];
				historyTranslation["en-GB"] = "History " + phaseD.LanguageTranslation["en-GB"];
				historyTranslation["fi-FI"] = "Historia " + phaseD.LanguageTranslation["fi-FI"];

				if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
					writeTranslation["de-DE"] = "Schreiben " + phaseD.LanguageTranslation["en-GB"];
					readTranslation["de-DE"] = "Lesen " + phaseD.LanguageTranslation["en-GB"];
					historyTranslation["de-DE"] = "Geschichte " + phaseD.LanguageTranslation["en-GB"];
				}

				if (phaseD.SelectedFeatures.Count > 0) {
					foreach (var feature in phaseD.SelectedFeatures) {
						ForceFeatureNavigation(phaseD.ProcessGroupId, feature.Feature, feature.DefaultState);
						Dictionary<string, string> featureTranslations = new Dictionary<string, string>();
						featureTranslations["en-GB"] = "Write " + feature.Feature;
						featureTranslations["fi-FI"] = "Kirjoitus " + feature.Feature;
						if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
							featureTranslations["de-DE"] = "Schreiben " + feature.Feature;
						}

						var featureNames = new List<string>();
						featureNames.Add(feature.Feature);
						ConditionFactory(PhaseListCollection[phaseD.ProcessGroupId], processName, new List<string> {feature.Feature + ".write"},
							featureNames, featureTranslations, c, phaseD.ProcessGroupId,
							PhaseConditionGroups[phaseD.ProcessGroupId]);
					}
				}

				ConditionFactory(PhaseListCollection[phaseD.ProcessGroupId], processName, stateListWrite,
					featuresListWrite, writeTranslation, c, phaseD.ProcessGroupId,
					PhaseConditionGroups[phaseD.ProcessGroupId]);
				ConditionFactory(PhaseListCollection[phaseD.ProcessGroupId], processName, statesListRead, featuresList,
					readTranslation, c, phaseD.ProcessGroupId, PhaseConditionGroups[phaseD.ProcessGroupId]);

				//töhän

				if(phaseIndex != (PhaseList.Count - 1)) {
					ConditionFactory(PhaseListCollection[phaseD.ProcessGroupId], processName, stateListHistory,
						featuresList, historyTranslation, c, phaseD.ProcessGroupId,
						PhaseConditionGroups[phaseD.ProcessGroupId]);
				}
				phaseIndex++;
			}

			SetButtons(processName, PhaseList);

			SetNewProcessAction(processName, firstId, lastAddedItemColumnId);

			UpdateAccessContainerCondition(ReadExistingCondition, "meta.read", processName);
			UpdateAccessContainerCondition(WriteExistingCondition, "meta.write", processName);

			return processCreateActionId;
		}

		private void SetCreationContainer(string processName,
			List<string> additionalLanguages_,
			ProcessContainers ProcessContainers) {

			var CreationContainer = new ProcessContainer();
			CreationContainer.ProcessContainerId = 0;
			CreationContainer.LanguageTranslation = new Dictionary<string, string>();
			CreationContainer.LanguageTranslation["en-GB"] = "New " + processName;
			CreationContainer.LanguageTranslation["fi-FI"] = "Uusi " + processName;
			if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
				CreationContainer.LanguageTranslation["de-DE"] =
					"Neu " + processName;
			}

			var pcs = new ProcesContainerColumns(instance, processName, session);

			var titleInContainer = new ProcessContainerField();
			titleInContainer.OrderNo = "1";
			titleInContainer.ItemColumn = Title;
			titleInContainer.ProcessContainerId = ProcessContainers.Add(CreationContainer).ProcessContainerId;
			titleInContainer.Process = processName;
			titleInContainer.Validation = new FieldValidation();
			titleInContainer.Validation.Required = true;

			pcs.Add(titleInContainer);

			var Ppd = new ProcessPortfoliosDialog(instance, processName, session);
			var CreationContainer2 = new ProcessPortfolioDialog();
			CreationContainer2.OrderNo = "U";
			CreationContainer2.ProcessContainerId = titleInContainer.ProcessContainerId;
			CreationContainer2.LanguageTranslation = new Dictionary<string, string>();
			CreationContainer2.LanguageTranslation["en-GB"] = "New " + processName;
			CreationContainer2.LanguageTranslation["fi-FI"] = "Uusi " + processName;
			if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
				CreationContainer2.LanguageTranslation["de-DE"] =
					"Neu " + processName;
			}
			Ppd.AddPortfolioDialog(CreationContainer2);
		}

		private void CreateMenuItems(List<string>additionalLanguages_) {
			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, link_type) VALUES(@instance, null, @process + '_menuitem', '0', null, '{\"expand\":true}', 0)", DBParameters);
			var xx = Conv.ToInt(db.ExecuteScalar("SELECT TOP 1 instance_menu_id FROM instance_menu order by instance_menu_id DESC"));
			SetDBParam(SqlDbType.Int, "@menuId", xx);

			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, icon, link_type) VALUES(@instance, @menuId, @process + '_new', '0', 'new.process', '{\"process\":\"' + @process + '\"}','add',2)", DBParameters);

			var x2 = Conv.ToInt(db.ExecuteScalar("SELECT TOP 1 instance_menu_id FROM instance_menu order by instance_menu_id DESC"));
			SetDBParam(SqlDbType.Int, "@menuId2", x2);

			SetDBParam(SqlDbType.NVarChar, "@portfolioId", processPortfolioId);
			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO instance_menu (instance, parent_id, variable, order_no, default_state, params, icon, link_type) VALUES(@instance, @menuId, @process + '_w_portfolio', '0', 'portfolio', '{\"process\":\"' + @process + '\",\"portfolioId\":' + @portfolioId + '}','folder',0)", DBParameters);


			var x3 = Conv.ToInt(db.ExecuteScalar("SELECT TOP 1 instance_menu_id FROM instance_menu order by instance_menu_id DESC"));
			SetDBParam(SqlDbType.Int, "@menuId3", x3);


			db.ExecuteScalar("exec app_set_archive_user_id 0 INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(@menuId2, (SELECT TOP 1 item_id FROM _" + instance + "_user_group ORDER BY item_id ASC))", DBParameters);

			db.ExecuteScalar("exec app_set_archive_user_id 0 INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(@menuId, (SELECT TOP 1 item_id FROM _" + instance + "_user_group ORDER BY item_id ASC))", DBParameters);

			db.ExecuteScalar("exec app_set_archive_user_id 0 INSERT INTO instance_menu_rights (instance_menu_id, item_id) VALUES(@menuId3, (SELECT TOP 1 item_id FROM _" + instance + "_user_group ORDER BY item_id ASC))", DBParameters);

			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'en-GB', @process + '_new', 'New ' + @process)", DBParameters);
			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'fi-FI', @process + '_new', 'Uusi ' + @process)", DBParameters);

			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'en-GB', @process + '_w_portfolio', 'Portfolio')", DBParameters);
			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'fi-FI', @process + '_w_portfolio', 'Portfolio')", DBParameters);


			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'en-GB', @process + '_menuitem', @process)", DBParameters);
			db.ExecuteScalar(
				"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'fi-FI', @process + '_menuitem', @process)", DBParameters);


			if (hasAddLanguages && additionalLanguages_.Contains("de-DE")) {
				db.ExecuteScalar(
					"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'de-DE', @process + '_w_portfolio', 'Portfolio')", DBParameters);
				db.ExecuteScalar(
					"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'de-DE', @process + '_new', 'Neu ' + @process)", DBParameters);

				db.ExecuteScalar(
					"exec app_set_archive_user_id 0 INSERT INTO process_translations (instance,process,code,variable,translation) VALUES (@instance,@process,'de-DE', @process + '_menuitem', @process)", DBParameters);
			}

			Cache.Set(instance, "InstanceMenuCache", null);
			Cache.Set(instance, "InstanceMenuRightCache", null);
			Cache.Set(instance, "InstanceMenuStateCache", null);
			Cache.Set(instance, "pagebase_menu_cache", null);
		}

		private void SetNewProcessAction(string processName, int firstPhaseId, int icId) {
			var Actions = new ProcessActions(instance, processName, session);
			var ActionRows = new ProcessActionRows(instance, processName, session);
			var newAction = new ProcessAction();

			newAction.Process = processName;
			newAction.LanguageTranslation = new Dictionary<string, string>();
			newAction.LanguageTranslation["en-GB"] = "New process row";
			newAction.LanguageTranslation["fi-FI"] = "Uusi prosessirivi";
			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				newAction.LanguageTranslation["de-DE"] = "Neue process Zeilen";
			}

			Actions.Add(newAction);

			int newActionId = Conv.ToInt(db.ExecuteScalar(
				"SELECT TOP 1 process_action_id FROM process_actions WHERE process = @process ORDER BY process_action_id DESC",
				DBParameters));
			var actionGroupId = CreateActionRowGroup(newActionId, processName);
			SetDBParam(SqlDbType.Int, "@newaction", newActionId);
			SetDBParam(SqlDbType.NVarChar, "@process", processName);

			processCreateActionId = newActionId;

			var creatorColumnId =
				Conv.ToInt(
					db.ExecuteScalar(
						"SELECT item_column_id FROM item_columns WHERE process = @process AND name = 'creator'",
						DBParameters));

			var creationDateColumnId =
				Conv.ToInt(
					db.ExecuteScalar(
						"SELECT item_column_id FROM item_columns WHERE process = @process AND name = 'creation_date'",
						DBParameters));

			var actionRow = new ProcessActionRow();

			actionRow.Value = "[" + Conv.ToStr(firstPhaseId) + "]";
			actionRow.ItemColumnId = icId;
			actionRow.ProcessActionId = newActionId;
			actionRow.ProcessActionGroupId = actionGroupId;
			actionRow.ValueType = 0;

			SetDBParam(SqlDbType.Int, "@pa", newActionId);

			var newRow1 = ActionRows.Add(actionRow);

			actionRow.ProcessActionRowId = newRow1.ProcessActionRowId;
			ActionRows.Save(actionRow);

			//CREATION DATE
			var actionRow2 = new ProcessActionRow();
			actionRow2.Value = "";
			actionRow2.ValueType = 1;
			actionRow2.ItemColumnId = creationDateColumnId;
			actionRow2.ProcessActionId = newActionId;
			actionRow2.ProcessActionGroupId = actionGroupId;
			var newRow2 = ActionRows.Add(actionRow2);

			actionRow2.ProcessActionRowId = newRow2.ProcessActionRowId;
			ActionRows.Save(actionRow2);
			//CREATOR
			var actionRow3 = new ProcessActionRow();
			actionRow3.Value = "";
			actionRow3.ValueType = 2;
			actionRow3.ItemColumnId = creatorColumnId;
			actionRow3.ProcessActionId = newActionId;
			actionRow3.ProcessActionGroupId = actionGroupId;
			var newRow3 = ActionRows.Add(actionRow3);

			actionRow3.ProcessActionRowId = newRow3.ProcessActionRowId;
			ActionRows.Save(actionRow3);

			db.ExecuteScalar("UPDATE processes SET new_row_action_id = @newaction WHERE process = @process",
				DBParameters);
		}

		private void CreatePortfolios(string processName) {
			var portfolios = new ProcessPortfolios(instance, processName, session);
			var portfolioGroups = new ProcessPortfolioColumns(instance, processName,session);

			var NewPortfolio = new ProcessPortfolio();
			NewPortfolio.Process = processName;
			NewPortfolio.LanguageTranslation = new Dictionary<string, string>();
			NewPortfolio.LanguageTranslation["en-GB"] = processName + " Portfolio";
			NewPortfolio.LanguageTranslation["fi-FI"] = processName + " Portfolio";
			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				NewPortfolio.LanguageTranslation["de-DE"] = processName + " Portfolio";
			}

			NewPortfolio.ProcessPortfolioType = 0;
			NewPortfolio.IgnoreRights = true;
			NewPortfolio.ShowProgressArrows = true;

			var portfolioId = portfolios.Add(NewPortfolio);

			var arrowGroup = new ProcessPortfolioColumnGroup();
			arrowGroup.ProcessPortfolioId = portfolioId;
			arrowGroup.Process = processName;
			arrowGroup.OrderNo = "2";
			arrowGroup.LanguageTranslation = new Dictionary<string, string>();
			arrowGroup.LanguageTranslation["en-GB"] = "Progress";
			arrowGroup.LanguageTranslation["fi-FI"] = "Prosessi";
			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				arrowGroup.LanguageTranslation["de-DE"] = "Progress";
			}

			var grp = portfolioGroups.AddGroup(processName, arrowGroup);

			processPortfolioId = portfolioId;

			var SelectorPortfolio = new ProcessPortfolio();
			SelectorPortfolio.Process = processName;
			SelectorPortfolio.LanguageTranslation = new Dictionary<string, string>();
			SelectorPortfolio.LanguageTranslation["en-GB"] = "Selector " + processName;
			SelectorPortfolio.LanguageTranslation["fi-FI"] = "Selector " + processName;
			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				SelectorPortfolio.LanguageTranslation["de-DE"] = "Selector " + processName;
			}

			SelectorPortfolio.ProcessPortfolioType = 1;


			var selectorId = portfolios.Add(SelectorPortfolio);
			selectorPortfolioId = selectorId;

			var portfolioColumns = new ProcessPortfolioColumns(instance, processName, session);

			var rightOrder = new Dictionary<string, string>();
			rightOrder["serial"] = "1";
			rightOrder["title"] = "2";
			rightOrder["start_date"] = "3";
			rightOrder["end_date"] = "4";
			rightOrder["progress_arrows"] = "5";

			var widths = new Dictionary<string, int>();
			widths["serial"] = 150;
			widths["title"] = 250;
			widths["start_date"] = 85;
			widths["end_date"] = 85;
			widths["progress_arrows"] = 50;

			foreach (var ic in PortfolioColumns) {
				var p = new ProcessPortfolioColumn();
				p.ItemColumn = ic;
				p.ProcessPortfolioColumnGroupId = 0;
				p.ProcessPortfolioId = portfolioId;
				p.OrderNo = rightOrder[ic.Name];
				p.IsDefault = true;
				p.UseInFilter = p.ItemColumn.Name != "start_date" && p.ItemColumn.Name != "end_date";
				p.Width = widths[ic.Name];
				if (p.ItemColumn.Name == "progress_arrows") {
					p.UseInSelectResult = 5;
					p.ProcessPortfolioColumnGroupId = grp.ProcessPortfolioColumnGroupId;
				}
				portfolioColumns.Add(p);
			}

			foreach (var selectorCol in PortfolioColumns) {
				if (selectorCol.Name == "title" || selectorCol.Name == "serial") {
					var p = new ProcessPortfolioColumn();
					p.ItemColumn = selectorCol;
					p.ProcessPortfolioColumnGroupId = 0;
					p.ProcessPortfolioId = selectorId;
					p.IsDefault = true;
					p.UseInFilter = true;
					p.UseInSelectResult = 1;
					p.OrderNo = "U";
					p.UseInSearch = true;
					portfolioColumns.Add(p);
				}
			}
		}

		private void CreateDialog(Dictionary<string, string> toPhase, ProcessActionDialogData pdd) {
			var NewDialog = new ProcessActionDialogRow();
			NewDialog.Type = 2;
			NewDialog.LanguageTranslation = new Dictionary<string, string>();
			NewDialog.LanguageTranslation["en-GB"] = "Are you sure you want to move to " + toPhase["en-GB"] + " ?";
			NewDialog.LanguageTranslation["fi-FI"] =
				"Oletko varma, että haluat siirtyä vaiheeseen " + toPhase["fi-FI"] + " ?";
			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				NewDialog.LanguageTranslation["de-DE"] =
					"Sind Sie sicher, dass Sie zu bewegen möchten " + toPhase["de-DE"] + " ?";
			}

			var addedDialog = pdd.Add(NewDialog);


			var dialogTranslationEn = new ProcessActionDialogRowTranslation();
			var dialogTranslationFi = new ProcessActionDialogRowTranslation();

			dialogTranslationEn.Code = "en-GB";
			dialogTranslationEn.PrimaryButtonTranslation = "Yes";
			dialogTranslationEn.SecondaryButtonTranslation = "No";
			dialogTranslationEn.TitleTranslation = "Confirmation";
			dialogTranslationEn.TextTranslation = "Are you sure you want to move to " + toPhase["en-GB"] + " ?";


			dialogTranslationFi.Code = "fi-FI";
			dialogTranslationFi.PrimaryButtonTranslation = "Kyllä";
			dialogTranslationFi.SecondaryButtonTranslation = "Ei";
			dialogTranslationFi.TitleTranslation = "Varmistus";
			dialogTranslationFi.TextTranslation =
				"Oletko varma, että haluat siirtyä vaiheeseen " + toPhase["fi-FI"] + " ?";

			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				var dialogTranslationDe = new ProcessActionDialogRowTranslation();
				dialogTranslationDe.Code = "de-DE";
				dialogTranslationDe.PrimaryButtonTranslation = "Ja";
				dialogTranslationDe.SecondaryButtonTranslation = "Nein";
				dialogTranslationDe.TitleTranslation = "Bestätigung";
				dialogTranslationDe.TextTranslation =
					"Sind Sie sicher, dass Sie zu bewegen möchten " + toPhase["de-DE"] + " ?";
				pdd.SaveTranslations(addedDialog.ProcessActionDialogId, dialogTranslationDe);
			}


			pdd.SaveTranslations(addedDialog.ProcessActionDialogId, dialogTranslationEn);

			pdd.SaveTranslations(addedDialog.ProcessActionDialogId, dialogTranslationFi);

			PhaseDialogs.Add(addedDialog.ProcessActionDialogId);
		}

		private void CreatePhaseActions(string processName, int listItemId, int icId, string phaseTranslation) {
			SetDBParam(SqlDbType.NVarChar, "@process", processName);

			var Actions = new ProcessActions(instance, processName, session);
			var ActionRows = new ProcessActionRows(instance, processName, session);
			var newAction = new ProcessAction();
			var actionRow = new ProcessActionRow();

			newAction.Process = processName;
			newAction.LanguageTranslation = new Dictionary<string, string>();
			newAction.LanguageTranslation["en-GB"] = "Change state to " + phaseTranslation;
			newAction.LanguageTranslation["fi-FI"] = "Vaihda tila olemaan " + phaseTranslation;

			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				newAction.LanguageTranslation["de-DE"] = "Ändere den Zustand auf " + phaseTranslation;
			}

			Actions.Add(newAction);


			int newActionId = Conv.ToInt(db.ExecuteScalar(
				"SELECT TOP 1 process_action_id FROM process_actions WHERE process = @process ORDER BY process_action_id DESC",
				DBParameters));

			var actionGroupId = CreateActionRowGroup(newActionId, processName);

			PhaseActions.Add(newActionId);

			actionRow.Value = "[" + Conv.ToStr(listItemId) + "]";
			actionRow.ItemColumnId = icId;
			actionRow.ProcessActionId = newActionId;
			actionRow.ProcessActionGroupId = actionGroupId;
			actionRow.ValueType = 0;

			SetDBParam(SqlDbType.Int, "@pa", newActionId);

			ActionRows.Add(actionRow);

			int newActionRowId = Conv.ToInt(db.ExecuteScalar(
				"SELECT TOP 1 process_action_row_id FROM process_action_rows WHERE process_action_id = @pa",
				DBParameters));

			actionRow.ProcessActionRowId = newActionRowId;
			ActionRows.Save(actionRow);
		}

		private int CreateActionRowGroup(int processActionId, string processName) {

			var groups = new ProcessActionRowGroups(instance, processName, session);
			var processActionRowGroup = new ProcessActionRowGroup();
			processActionRowGroup.Instance = instance;
			processActionRowGroup.Process = processName;
			processActionRowGroup.Type = 1;
			processActionRowGroup.ProcessActionId = processActionId;
			processActionRowGroup.ProcessInGroup = null;
			processActionRowGroup.OrderNo = "W";
			processActionRowGroup.LanguageTranslation = new Dictionary<string, string>();

			processActionRowGroup.LanguageTranslation["en-GB"] = "Default SET";
			processActionRowGroup.LanguageTranslation["fi-FI"] = "Oletus SET";
			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				processActionRowGroup.LanguageTranslation["de-DE"] = "Default SET";
			}

			return groups.AddProcessActionRowGroup(processName, processActionRowGroup).ProcessActionGroupId;

		}

		private void SetButtons(string processName, List<ProcessGroup> phases) {
			int iterator = 1;

			var vfb = new ViewForButton();
			vfb.currentState = currentState1;
			vfb.initialState = currentState1;

			var s = Newtonsoft.Json.JsonConvert.SerializeObject(vfb);
			var ics = new ItemColumns(instance, processName, session);
			var pcs = new ProcesContainerColumns(instance, processName, session);

			var indx = 0;

			foreach (var phase in phases) {
				if (indx != 0) {
					var prevButton = new ItemColumn();
					prevButton.LanguageTranslation = new Dictionary<string, string>();
					prevButton.Name = "prev_button_" + iterator;
					prevButton.DataType = 8;
					prevButton.Process = processName;
					prevButton.OrderNo = Conv.ToStr(iterator + 1);
					prevButton.LanguageTranslationRaw = new Dictionary<string, string>();
					prevButton.LanguageTranslation["en-GB"] = "Previous";
					prevButton.LanguageTranslation["fi-FI"] = "Edellinen";
					prevButton.LanguageTranslationRaw["en-GB"] = "Move to previous phase";
					prevButton.LanguageTranslationRaw["fi-FI"] = "Siirry edelliseen vaiheeseen";
					prevButton.DataAdditional2 = s;
					prevButton.ProcessActionId = PhaseActions[indx - 1];
					prevButton.ProcessDialogId = PhaseDialogs[indx - 1];

					var prevButtonInContainer = new ProcessContainerField();
					prevButtonInContainer.OrderNo = "1";


					if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
						prevButton.LanguageTranslation["de-DE"] = "Zurück";
						prevButton.LanguageTranslationRaw["de-DE"] = "prev_button_" + iterator;
					}

					prevButtonInContainer.ItemColumn = ics.Add(processName, prevButton, true, false);
					prevButtonInContainer.ProcessContainerId = PhaseButtonContainers[phase.ProcessGroupId];
					prevButtonInContainer.Process = processName;

					pcs.Add(prevButtonInContainer);
				}

				if (indx != (phases.Count - 1)) {
					var nextButton = new ItemColumn();
					nextButton.LanguageTranslation = new Dictionary<string, string>();
					nextButton.Name = "next_button_" + iterator;
					nextButton.DataType = 8;
					nextButton.LanguageTranslationRaw = new Dictionary<string, string>();
					nextButton.LanguageTranslation["en-GB"] = "Next";
					nextButton.LanguageTranslation["fi-FI"] = "Seuraava";
					nextButton.LanguageTranslationRaw["en-GB"] = "Move to next phase";
					nextButton.LanguageTranslationRaw["fi-FI"] = "Siirry seuraavaan vaiheeseen";
					nextButton.Process = processName;
					nextButton.OrderNo = Conv.ToStr(iterator);
					nextButton.DataAdditional2 = s;
					nextButton.ProcessActionId = PhaseActions[indx + 1];
					nextButton.ProcessDialogId = PhaseDialogs[indx + 1];

					var nextButtonInContainer = new ProcessContainerField();
					nextButtonInContainer.OrderNo = "2";


					if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
						nextButton.LanguageTranslation["de-DE"] = "Weiter";
						nextButton.LanguageTranslationRaw["de-DE"] = "next_button_" + iterator;
					}

					nextButtonInContainer.ItemColumn = ics.Add(processName, nextButton, true, false);
					nextButtonInContainer.ProcessContainerId = PhaseButtonContainers[phase.ProcessGroupId];
					nextButtonInContainer.Process = processName;
					pcs.Add(nextButtonInContainer);
				}

				/*if (indx == (phases.Count - 1)) {
					var closeButton = new ItemColumn();
					closeButton.LanguageTranslation = new Dictionary<string, string>();
					closeButton.Name = "close_project_button";
					closeButton.DataType = 8;
					closeButton.LanguageTranslationRaw = new Dictionary<string, string>();
					closeButton.LanguageTranslation["en-GB"] = "Close Project";
					closeButton.LanguageTranslation["fi-FI"] = "Sulje projekti";
					closeButton.LanguageTranslationRaw["en-GB"] = "Close Project";
					closeButton.LanguageTranslationRaw["fi-FI"] = "Sulje projekti";
					closeButton.Process = processName;
					closeButton.OrderNo = Conv.ToStr(iterator);
					closeButton.DataAdditional2 = s;
					closeButton.ProcessActionId = PhaseActions[indx + 1]; //väärin
					closeButton.ProcessDialogId = PhaseDialogs[indx + 1]; //väärin

					var closeButtonInContainer = new ProcessContainerField();
					closeButtonInContainer.OrderNo = "3";

					if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
						closeButton.LanguageTranslation["de-DE"] = "Close Project";
						closeButton.LanguageTranslationRaw["de-DE"] = "Close Project";
					}

					closeButtonInContainer.ItemColumn = ics.Add(processName, closeButton, true, false);
					closeButtonInContainer.ProcessContainerId = PhaseButtonContainers[phase.ProcessGroupId];
					closeButtonInContainer.Process = processName;
					pcs.Add(closeButtonInContainer);
				}*/

				indx++;
				iterator++;
			}
		}

		private int ConditionFactory(int phaseListId, string processName, List<string> states, List<string> features,
			Dictionary<string, string> translations, Conditions conds, int phaseId = 0, int condGroupId = 0) {
			Condition condition = new Condition();
			condition.Process = processName;
			condition.ConditionGroupId = condGroupId;
			condition.ConditionJSON = null;
			condition.States = states;
			condition.Features = features;
			condition.LanguageTranslation = translations;

			int whichCondition;

			if (states.Count > 0) {
				if (states[0] != "meta.write") {
					whichCondition = ReadExistingCondition;
				} else {
					whichCondition = WriteExistingCondition;
				}

				if (features[0] == "portfolio") {
					condition.ProcessPortfolioIds = new List<int>();
					condition.ProcessPortfolioIds.Add(processPortfolioId);
					condition.ProcessPortfolioIds.Add(selectorPortfolioId);
				}

				if (phaseId != 0) {
					var phaseIds = new List<int>();
					phaseIds.Add(phaseId);
					condition.ProcessGroupIds = phaseIds;

					ConditionContent content = new ConditionContent();
					content.Disabled = false;
					content.OperatorId = 1;

					var operations = new List<Operation>();
					var existingConditionMain = new Operation();
					var phaseConditionMain = new Operation();
					var existingCondition = new WizardCondition();
					var matchingPhaseCondition = new WizardCondition();

					existingConditionMain.ConditionTypeId = 2;
					existingConditionMain.OperatorId = null;
					existingConditionMain.Condition = existingCondition;

					existingCondition.ComparisonTypeId = 3;
					existingCondition.ConditionId = whichCondition;
					existingCondition.Value = "true";

					operations.Add(existingConditionMain);

					var opg = new OperationGroup();
					opg.Operations = operations;

					var ccc = new List<ConditionContent>();
					content.OperationGroup = opg;
					ccc.Add(content);

					if (states[0] != "meta.history") {
						phaseConditionMain.ConditionTypeId = 1;
						phaseConditionMain.OperatorId = 1;
						phaseConditionMain.Condition = matchingPhaseCondition;

						matchingPhaseCondition.ComparisonTypeId = 3;
						matchingPhaseCondition.ItemColumnId = lastAddedItemColumnId;
						matchingPhaseCondition.Value = phaseListId;
						operations.Add(phaseConditionMain);
					} else {
						ConditionContent contentHistory = new ConditionContent();
						contentHistory.Disabled = false;
						contentHistory.OperatorId = 1;
						var opgHistory = new OperationGroup();
						var operationsHistory = new List<Operation>();
						int indexhere = 0;
						var _phaseIds = new List<int>();
						var o = new Operation();
						var pastCondition = new WizardCondition();
						foreach (int pi in PhaseListItems) {
							if ((indexhere + 1 != PhaseListItems.Count) && pi == phaseListId) {
								for (int i = indexhere + 1; i <= PhaseListItems.Count - 1; i++) {



									o.ConditionTypeId = 1;
									o.OperatorId = 2;
									o.Condition = pastCondition;

									pastCondition.ComparisonTypeId = 3;
									pastCondition.ItemColumnId = lastAddedItemColumnId;

									_phaseIds.Add(PhaseListItems[i]);

								}
							}
							indexhere++;
						}
						pastCondition.Value = _phaseIds.Count > 0 ? _phaseIds : null;
						operationsHistory.Add(o);

						/*var closedValue = new WizardCondition();
						var o2 = new Operation();

						o2.ConditionTypeId = 1;
						o2.OperatorId = 2;
						o2.Condition = closedValue;

						closedValue.ComparisonTypeId = 3;
						closedValue.ItemColumnId = lastAddedItemColumnId;
						closedValue.Value = closedItemId;
						operationsHistory.Add(o2);*/

						opgHistory.Operations = operationsHistory;
						contentHistory.OperationGroup = opgHistory;
						ccc.Add(contentHistory);
					}

					condition.ConditionJSON = ccc;
				}
			}

			if (states.Count > 0) {
				if (states[0] == "meta.help" || states[0] == "meta.configure") {
					condition.ItemIds = new List<int>();
					condition.ItemIds.Add(adminGroupId);
				}
			}

			return conds.Add(condition).ConditionId;
		}

		private int ConditionGroupFactory(string processName, string phaseName, string orderNo, Conditions conds) {
			ConditionsGroup cg = new ConditionsGroup();
			cg.Process = processName;
			cg.OrderNo = orderNo;
			var gerPhase = "";

			if (hasAddLanguages && AdditionalLanguages.Contains("de-DE")) {
				gerPhase = phaseName;
			}

			cg.LanguageTranslation = TranslationFactory(phaseName, phaseName, gerPhase);
			return conds.AddGroup(processName, cg).ConditionGroupId;
		}

		private void ForceFeatureNavigation(int phaseId, string variable, string defaultState) {
			SetDBParam(SqlDbType.Int, "@phaseId", phaseId);
			SetDBParam(SqlDbType.NVarChar, "@variable", variable);
			SetDBParam(SqlDbType.NVarChar, "@defaultState", defaultState);

			db.ExecuteInsertQuery(
				"INSERT INTO process_group_navigation (process_group_id, variable, process_tab_id, default_state, order_no, type_id) VALUES (@phaseId, @variable, @phaseId, @defaultState, 'M', 1)",
				DBParameters);
		}

		private void UpdateAccessContainerCondition(int exstConditionId, string state, string processName) {

			var Conditions = new Conditions(instance, processName, session);

			SetDBParam(SqlDbType.NVarChar, "@process", processName);
			string conditionVariable;
			if (state == "meta.write") {
				conditionVariable = "condition_" + processName + "_access_container_write";
			} else {
				conditionVariable = "condition_" + processName + "_access_container_read";
			}

				SetDBParam(SqlDbType.NVarChar, "@conditionVariable", conditionVariable);

				var condition = Conditions.GetCondition(Conv.ToInt(db.ExecuteScalar(
					"SELECT condition_id FROM conditions WHERE variable = @conditionVariable",
					DBParameters)));

				condition.ConditionJSON = new List<ConditionContent>();
				var conditionJsonContent = new List<ConditionContent>();

				ConditionContent content = new ConditionContent();
				content.Disabled = false;
				content.OperatorId = 1;

				var operations = new List<Operation>();
				var existingConditionMain = new Operation();
				var existingCondition = new WizardCondition();

				existingConditionMain.ConditionTypeId = 2;
				existingConditionMain.OperatorId = null;
				existingConditionMain.Condition = existingCondition;

				existingCondition.ComparisonTypeId = 3;
				existingCondition.ConditionId = exstConditionId;
				existingCondition.Value = "true";

				operations.Add(existingConditionMain);

				var opg = new OperationGroup();
				opg.Operations = operations;

				content.OperationGroup = opg;
				conditionJsonContent.Add(content);
				condition.ConditionJSON = conditionJsonContent;

				Conditions.Save(condition);

		}
	}

	public class ViewForButton
	{
		public string currentState;
		public string initialState;
		public int? optionalDoneDialog;
		public string viewActions = "[]";
	}

	public class WizardCondition
	{
		public int ComparisonTypeId;
		public int ConditionId;
		public dynamic ItemColumnId;
		public dynamic Value;
	}
}