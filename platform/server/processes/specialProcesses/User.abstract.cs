﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.actions;
using Keto5.x.platform.server.processes.processCustomActions;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.specialProcesses {
	[Serializable]
	public class User : ProcessItem {
		private int _currentUserId = 0;
		public User(string instance, int currentUserId) : base(instance, "user", currentUserId) {
			_currentUserId = currentUserId;
		}

		public int CurrentUserId { get { return _currentUserId; } }

		public static List<string> GetRestrictionList() {
			return new () {
				"login_tries",
				"password_changed",
				"password_reset",
				"password_reset_date",
				"twofactor",
				"ketoauth",
				"login_tries",
				"last_login_try",
				"password",
				"token"
			};
		}

		public Dictionary<string, object> SetInstanceDefaults(Dictionary<string, object> data) {
			SetDBParameter("@instance", instance);
			var i = db.GetDataRow("SELECT * FROM instances WHERE instance = @instance", DBParameters);

			if (data.ContainsKey("date_format")) {
				data["date_format"] = i["default_date_format"];
			} else {
				data.Add("date_format", i["default_date_format"]);
			}

			if (data.ContainsKey("first_day_of_week")) {
				data["first_day_of_week"] = i["default_first_day_of_week"];
			} else {
				data.Add("first_day_of_week", i["default_first_day_of_week"]);
			}

			if (data.ContainsKey("locale_id")) {
				data["locale_id"] = i["default_locale_id"];
			} else {
				data.Add("locale_id", i["default_locale_id"]);
			}

			return data;
		}

		public void DeActivate(AuthenticatedSession session) {
			var a = new CustomActionAllocationActions();
			a.session = session;
			a.instance = session.instance;
			a.DoCustomAction(0, _currentUserId, "2", "");
		}

		public List<string> GetEmailAddresses(string delimitedlistofids) {
			var result = new List<string>();

			if (delimitedlistofids.Length == 0) {
				return result;
			}

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT DISTINCT ISNULL(email,'') AS email FROM _" + instance + "_user WHERE item_id IN (" +
					delimitedlistofids +
					")", DBParameters).Rows) {
				if (Conv.ToStr(row["email"]) != "") {
					result.Add(Conv.ToStr(row["email"]));
				}
			}

			return result;
		}

		public List<int> GeneratePasswords(AuthenticatedSession session) {
			var portfolioId =
				(int) db.ExecuteScalar(
					"SELECT MIN(process_portfolio_id) FROM process_portfolios WHERE instance = @instance AND process = @process AND process_portfolio_type = 0",
					DBParameters);
			var ib = new ItemBase(instance, process, session);

			var sqlWhere = ib.EntityRepository.GetWhere(portfolioId);
			var sql =
				"  SELECT pf.item_id, first_name, last_name, password, generate_password, generated_password FROM " +
				tableName + " pf WHERE " + sqlWhere;

			var pData = db.GetDatatable(sql, DBParameters);

			var retval = new List<int>();

			foreach (DataRow row in pData.Rows) {
				var d = row.Table.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => row[c]);
				if (d.ContainsKey("password") && d.ContainsKey("generate_password") &&
				    Conv.ToInt(d["generate_password"]) == 1) {
					var pwd = Modi.Left(Conv.ToStr(d["last_name"]), 5) + Modi.Left(Conv.ToStr(d["first_name"]), 3) +
					          Conv.ToStr(d["item_id"]);
					d["password"] = pwd;
					d["generate_password"] = 0;
					if (d.ContainsKey("generated_password")) {
						d["generated_password"] = pwd;
					}

					var oldData = ib.GetItem(Conv.ToInt(d["item_id"]));

					ib.SaveItem(d, Conv.ToInt(d["item_id"]), true);
					retval.Add(Conv.ToInt(d["item_id"]));

					//INVALIDATE CACHE
					var conditions = new Conditions(instance, process, session);
					foreach (var c in conditions.GetConditions(process)) {
						Cache.Remove(instance, "c_" + c.ConditionId + "_" + Conv.ToStr(d["item_id"]));
					}

					var states = new States(instance, session);
					var newData = ib.GetItem(Conv.ToInt(d["item_id"]));
					states.CheckNotifications(process, Conv.ToInt(d["item_id"]), oldData, newData);
				}
			}

			return retval;
		}
	}
}