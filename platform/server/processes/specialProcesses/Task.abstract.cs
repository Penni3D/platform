﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;

namespace Keto5.x.platform.server.processes.specialProcesses {
	[Serializable]
	public class Task {
		//public List<Task> task { get; set; }

		public Task(DataRow row) {
			id = Conv.ToInt(row["task_id"]);
			responsibleId = Conv.ToInt(row["responsibles"]);
			parentId = Conv.ToInt(row["parent_item_id"]);
			title = Conv.ToStr(row["title"]);
			description = Conv.ToStr(row["description"]);
			status = Conv.ToInt(row["status_id"]);
			taskType = Conv.ToInt(row["task_type"]);
			color = Conv.ToInt(row["color"]);

			if (row["start_date"] != DBNull.Value) startDate = Convert.ToDateTime(row["start_date"]);
			if (row["end_date"] != DBNull.Value) endDate = Convert.ToDateTime(row["end_date"]);
			if (row.Table.Columns.Contains("order_no")) orderNo = Conv.ToStr(row["order_no"]);
			if (row.Table.Columns.Contains("process_id")) processId = Conv.ToInt(row["process_id"]);
			if (row.Table.Columns.Contains("process")) process = Conv.ToStr(row["process"]);
		}

		public Task() {
			startDate = DateTime.Now.ToUniversalTime();
			endDate = DateTime.Now.ToUniversalTime();
			description = "";
			title = "New Item";
		}

		public int id { get; set; }
		public int responsibleId { get; set; }
		public int parentId { get; set; }
		public string title { get; set; }
		public string description { get; set; }
		public DateTime startDate { get; set; }
		public DateTime endDate { get; set; }
		public int status { get; set; }
		public int taskType { get; set; }
		public string orderNo { get; set; }
		public int orderSet { get; set; }
		public int color { get; set; }
		public int processId { get; set; }
		public string process { get; set; }
		public Dictionary<string, object> DynamicData { get; set; }
	}
}