﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.specialProcesses {
	[Route("v1/features/Tasks")]
	public class TasksRest : FeatureBase {
		public override string Name => "gantt";
		public override string Process => "task";

		public override bool ImplementsCopy => false;
		public override bool ImplementsProcessRelations => true;

		public override void EnableFeature() {
			Enabled = ProcessExists();
		}

		public override bool CopyFeature(int sourceOwnerItemId, int targetOwnerItemId) {
			throw new System.NotImplementedException();
		}

		[HttpGet("{portfolioId}/{processId}/{type}/{rowId}")]
		public Tasks Get(int portfolioId, int processId, string type, int rowId) {
			var t = new TaskData(instance, currentSession);
			return t.GetData(portfolioId, processId, type, rowId: rowId);
		}

		[HttpGet("{portfolioId}/{processId}/{type}/{archiveDate}/{rowId}")]
		public Tasks Get(int portfolioId, int processId, string type, DateTime? archiveDate, int rowId) {
			var t = new TaskData(instance, currentSession);
			return t.GetData(portfolioId, processId, type, archiveDate, rowId);
		}

//		[HttpGet("{portfolioId}/{processId}/{type}")]
//		public Tasks Get(int portfolioId, int processId, string type) {
//			var t = new TaskData(instance, currentSession);
//			Console.WriteLine(portfolioId + ", " + processId + ", " + type);
//			return t.GetData(portfolioId, processId, type);
//		}
//
//		[HttpGet("{portfolioId}/{processId}/{type}/{archiveDate}")]
//		public Tasks Get(int portfolioId, int processId, string type, DateTime? archiveDate) {
//			var t = new TaskData(instance, currentSession);
//			Console.WriteLine(portfolioId + ", " + processId + ", " + type);
//			return t.GetData(portfolioId, processId, type, archiveDate);
//		}

		[HttpGet("statuses/{parentItemId}")]
		public List<TaskStatus> GetStatuses(int parentItemId) {
			var t = new TaskData(instance, currentSession);
			return t.GetStatus(parentItemId);
		}

		[HttpPost]
		public IActionResult Post([FromBody] Tasks tasks) {
			var t = new TaskData(instance, currentSession);
			t.SaveStatus(tasks.status);
			return new ObjectResult(tasks);
		}

		[HttpPost("newStatus")]
		public IActionResult NewStatus([FromBody] TaskStatus ts) {
			var t = new TaskData(instance, currentSession);
			return new ObjectResult(t.CreateStatus(ts));
		}


		[HttpPost("delStatus/{statusId}")]
		public void DeleteStatus(int statusId) {
			var t = new TaskData(instance, currentSession);
			t.DeleteStatus(statusId);
		}

		[HttpGet("getBacklogChartData/{processId}")]
		public Dictionary<string, object> GetHistoryBacklog(int processId, string process) {
			var t = new TaskData(instance, currentSession);
			return t.GetBacklogChartData(processId);
		}

		[HttpGet("copyTasks/{sourceId}/{targetId}")]
		public List<Dictionary<string, object>> CopyTasks(int sourceId, int targetId) {
			var t = new TaskData(instance, currentSession);
			var dict = t.CopyTasks(sourceId, targetId);
			return (List<Dictionary<string, object>>) dict["tasks"];
		}

		[HttpPost("copyTasksFromModel/{itemId}")]
		public void CopyTasksFromModel(int itemId, [FromBody] Dictionary<string, object> data) {
			if (data.ContainsKey("copy_from_id")) {
				var t = new TaskData(instance, currentSession);
				if (data.ContainsKey("copy_start_date") && data.ContainsKey("copy_end_date")) {
					t.CopyTasks(Conv.ToInt(data["copy_from_id"]), Conv.ToInt(itemId),
						Conv.ToDateTime(data["copy_start_date"]),
						Conv.ToDateTime(data["copy_end_date"]));
				} else if (data.ContainsKey("copy_start_date")) {
					t.CopyTasks(Conv.ToInt(data["copy_from_id"]), Conv.ToInt(itemId),
						Conv.ToDateTime(data["copy_start_date"]));
				} else {
					t.CopyTasks(Conv.ToInt(data["copy_from_id"]), Conv.ToInt(itemId));
				}
			}
		}
	}
}