﻿using System;
using System.Data;
using Keto5.x.platform.server.common;

namespace Keto5.x.platform.server.processes.specialProcesses {
	[Serializable]
	public class TaskStatus {
		public TaskStatus(DataRow row) {
			status_id = Conv.ToInt(row["status_id"]);
			parent_item_id = Conv.ToInt(row["parent_item_id"]);
			status_list_item_id = Conv.ToInt(row["status_list_item_id"]);
			title = Conv.ToStr(row["title"]);
			description = Conv.ToStr(row["description"]);
			is_done = Conv.ToInt(row["is_done"]) > 0;
			order_no = Conv.ToStr(row["order_no"]);
			creationOrder = 0;
		}

		public TaskStatus() {
			description = "";
			title = "New status";
			is_done = false;
			creationOrder = 0;
		}

		public int status_id { get; set; }
		public int parent_item_id { get; set; }
		public int status_list_item_id { get; set; }
		
		public string title { get; set; }
		public string description { get; set; }
		public bool is_done { get; set; }
		public string order_no { get; set; }

		public int creationOrder { get; set; }
	}

}