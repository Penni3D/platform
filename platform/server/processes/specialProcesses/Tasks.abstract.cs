﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core.resourceManagement;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.specialProcesses {
	[Serializable]
	public class Tasks {
		public List<Dictionary<string, object>> task { get; set; }
		public List<TaskStatus> status { get; set; }
		public Dictionary<int, object> processes { get; set; }
	}

	public class TaskData : ItemBase {
		private static readonly string _process = "task";
		private readonly AuthenticatedSession authenticatedSession;
		private readonly string instance;

		public TaskData(string instance, AuthenticatedSession authenticatedSession) : base(instance, _process,
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
			this.instance = instance;
		}

		public bool PostTaskCreate(Dictionary<string, object> data, int fromCopy = 0) {
			var ownType = Conv.ToInt(data["task_type"]);
			if (ownType == 21 && fromCopy > 0) CopyStatuses(fromCopy, data);
			if (fromCopy > 0 && ownType == 1) CopyCards(fromCopy, data);
			return true;
		}

		private void CopyStatuses(int sourceItemId, Dictionary<string, object> sourceData) {
			var itemId = Conv.ToInt(sourceData["item_id"]);
			ProcessBase.SetDBParam(SqlDbType.Int, "@targetItemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@sourceItemId", sourceItemId);

			var sql =
				"INSERT INTO task_status (parent_item_id, title, description, is_done, order_no, status_list_item_id) " +
				"SELECT @targetItemId, title, description, is_done, order_no, status_list_item_id " +
				"FROM task_status WHERE parent_item_id = @sourceItemId ORDER BY order_no  ASC";

			ProcessBase.db.ExecuteInsertQuery(sql, ProcessBase.DBParameters);
		}

		private void CopyCards(int sourceItemId, Dictionary<string, object> sourceData) {
			var ownerId = Conv.ToInt(sourceData["owner_item_id"]);
			var targetItemId = Conv.ToInt(sourceData["item_id"]);
			ProcessBase.SetDBParam(SqlDbType.Int, "@sourceItemId", sourceItemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@ownerItemId", ownerId);

			var statusId = Conv.ToInt(ProcessBase.db.ExecuteScalar(
				"SELECT TOP 1 status_id FROM task_status WHERE parent_item_id = " + targetItemId +
				" ORDER BY order_no  ASC",
				ProcessBase.DBParameters));

			foreach (DataRow card in ProcessBase.db
				.GetDatatable(
					"SELECT * FROM _" + instance + "_task WHERE task_type = 0 AND parent_item_id = @sourceItemId",
					ProcessBase.DBParameters).Rows) {
				var carddata = new Dictionary<string, object>();
				carddata.Add("parent_item_id", targetItemId);
				carddata.Add("owner_item_id", ownerId);
				carddata.Add("task_type", 0);
				carddata.Add("owner_item_type", "gantt");
				carddata.Add("title", card["title"]);
				carddata.Add("description", card["description"]);
				carddata.Add("start_date", card["start_date"]);
				carddata.Add("end_date", card["end_date"]);
				carddata.Add("color", card["color"]);
				carddata.Add("status_id", statusId);
				var i = new ItemBase(instance, "task", authenticatedSession);
				var cardId = i.InsertItemRow(carddata);
			}
		}

		private void CreateFirstCard(Dictionary<string, object> data) {
			//New Task is a sprint task -- create first card
			var ownerId = Conv.ToInt(data["owner_item_id"]);
			var itemId = Conv.ToInt(data["item_id"]);

			var statusId = Conv.ToInt(ProcessBase.db.ExecuteScalar(
				"SELECT status_id FROM task_status WHERE parent_item_id = @parent_id ORDER BY order_no  ASC",
				ProcessBase.DBParameters));


			var carddata = new Dictionary<string, object>();
			carddata.Add("parent_item_id", itemId);
			carddata.Add("owner_item_id", ownerId);
			carddata.Add("task_type", 0);
			carddata.Add("owner_item_type", "gantt");
			carddata.Add("title", data["title"]);
			carddata.Add("description", data["description"]);
			carddata.Add("start_date", data["start_date"]);
			carddata.Add("end_date", data["end_date"]);
			carddata.Add("color", "yellow");
			carddata.Add("status_id", statusId);

			var i = new ItemBase(instance, "task", authenticatedSession);
			var cardId = i.InsertItemRow(carddata);
			InvalidateCache(Conv.ToInt(cardId["item_id"]));
		}

		public Dictionary<string, object> GetUserTasks() {
			var result = new Dictionary<string, object>();
			return null;
		}

		private bool isTaskOnParentPath(DataRow item, int rowId, DataTable data) {
			if (rowId == 0) return true;
			var parent_item_id = Conv.ToInt(item["parent_item_id"]);
			if (parent_item_id == rowId) return true;
			var parents = data.Select("item_id=" + parent_item_id);
			return parents.Length > 0 && isTaskOnParentPath(parents[0], rowId, data);
		}

		public Dictionary<string, object> GetTasks(string type, int portfolioId, int ownerId,
			DateTime? archiveDate = null, int rowId = 0) {
			var result2 = new List<Dictionary<string, object>>();
			var result = new Dictionary<string, object>();
			var itemColumns = new ItemColumns(instance, _process, authenticatedSession);
			var portfolioSqlService = new PortfolioSqlService(instance, authenticatedSession,
				itemColumns, ProcessBase.tableName, ProcessBase.process);

			var pSqls = portfolioSqlService.GetSql(portfolioId, "", archiveDate);

			ProcessBase.SetDBParam(SqlDbType.Int, "@owner_item_id", ownerId);
			ProcessBase.SetDBParam(SqlDbType.NVarChar, "@owner_item_type", type);

			var eSql1 = "";
			var esql2 = "";
			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					if (Conv.ToInt(col.DataAdditional2) == 5 || Conv.ToInt(col.DataAdditional2) == 6) {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name + "_str";
					} else {
						equationSql += "," + ItemColumns.GetEquationQuery(col) + " " + col.Name;
					}
				}

				eSql1 = " SELECT  * " + equationSql + " FROM ( ";
				esql2 = " ) pf ";
			}


			var sql = eSql1 + " SELECT pf.item_id, " + string.Join(",", pSqls.sqlcols.ToArray());

			if (archiveDate == null) {
				sql += " FROM " + ProcessBase.tableName + " pf ";
			} else {
				ProcessBase.SetDBParam(SqlDbType.DateTime, "@archiveDate", archiveDate);
				sql += " FROM get_" + ProcessBase.tableName + "(@archiveDate) pf ";
			}

			sql += string.Join(" ", pSqls.archiveJoins.ToArray()) + " " +
			       string.Join(" ", pSqls.joins.ToArray()) + esql2 +
			       " WHERE pf.owner_item_type = @owner_item_type AND pf.owner_item_id = @owner_item_id AND " +
			       EntityRepository.GetWhere(portfolioId) +
			       " ORDER BY pf.order_no ";
			var pData = ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters);
			var itemIds = new List<int>();

			for (var i = 0; i < pData.Rows.Count - 1; i++) {
				var row = pData.Rows[i];
				if (isTaskOnParentPath(row, rowId, pData)) {
					itemIds.Add((int)row["item_id"]);
				} else {
					pData.Rows.Remove(row);
				}
			}

			pData.AcceptChanges();

			var idStr = string.Join(",", itemIds.ToArray());
			if (idStr.Length == 0) idStr = "0";

			//var mStates = new States(instance, authenticatedSession);
			var allData = GetAllData(idStr);
			pData = processPortfolioTable(portfolioId, pData, pSqls, allData, GetContainerCols(),
				GetProcessSelections(idStr));
			//var processSelections = GetProcessSelections(idStr);

			//var ics = new ItemColumns(instance, "task", authenticatedSession);
			//var pc = new ProcessContainers(instance, "task", authenticatedSession);
			//var cIds = pc.GetContainerIds(ics.GetItemColumnByName("title").ItemColumnId);

			foreach (DataRow row in pData.Rows) {
//				Diag.LogPerformanceCounter("row " + row["item_id"] + " rights");
				var t = Enumerable.Range(0, pData.Rows[0].Table.Columns.Count)
					.ToDictionary(
						i => pData.Rows[0].Table.Columns[i].ColumnName,
						i => ToNonNullObject(row[pData.Rows[0].Table.Columns[i].ColumnName])
					);
//
//				var hasWriteRight = false;
//				if (archiveDate == null) {
//					var fStates = mStates.GetEvaluatedStates(ProcessBase.process, "meta", (int) row["item_id"],
//						new List<string> {"write"}, allData,
//						processSelections);
//
//					foreach (var cId in cIds) {
//						if (fStates.HasContainerState(cId, "write")) {
//							hasWriteRight = true;
//							break;
//						}
//					}
//				}
//
				t.Add("has_write_right", true);
				result2.Add(t);
			}

			result.Add("processes", HandlePortfolioProcessData(portfolioId, pSqls, pData));
			result.Add("tasks", result2);

			return result;
		}

		public object ToNonNullObject(object o) {
			if (o != null && o != DBNull.Value) {
				return o;
			}

			return "";
		}

		public List<TaskStatus> GetStatus(int parentItemId) {
			var result = new List<TaskStatus>();
			var sql = "SELECT * FROM task_status WHERE parent_item_id = @parent_item_id";
			sql += " ORDER BY order_no ";

			ProcessBase.SetDBParam(SqlDbType.Int, "@parent_item_id", parentItemId);
			foreach (DataRow row in ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters).Rows) {
				result.Add(new TaskStatus(row));
			}

			return result;
		}

		public TaskStatus CreateStatus(TaskStatus ts) {
			var sql = "INSERT INTO task_status ("
			          + " parent_item_id, status_list_item_id, title, description, is_done, order_no"
			          + ") VALUES ("
			          + " @parent_item_id, @status_list_item_id, @title, @description, @is_done, @order_no"
			          + ")";

			ProcessBase.SetDBParam(SqlDbType.Int, "@parent_item_id", ts.parent_item_id);
			if (ts.status_list_item_id == 0) {
				var st = ProcessBase.db.GetDatatable("SELECT item_id FROM _" + instance +
				                                     "_list_task_status WHERE kanban_status IN (0,1,2) ORDER BY kanban_status ASC");
				var c = Conv.ToInt(ProcessBase.db.ExecuteScalar(
					"SELECT COUNT(*) FROM task_status WHERE parent_item_id = @parent_item_id",
					ProcessBase.DBParameters));

				if (ts.is_done) {
					ts.status_list_item_id = Conv.ToInt(st.Rows[st.Rows.Count - 1]["item_id"]);
				} else if (ts.creationOrder == 1) {
					ts.status_list_item_id = Conv.ToInt(st.Rows[0]["item_id"]);
				} else {
					ts.status_list_item_id = Conv.ToInt(st.Rows[st.Rows.Count - 2]["item_id"]);
				}
			}

			ProcessBase.SetDBParam(SqlDbType.NVarChar, "@order_no", ts.order_no);
			ProcessBase.SetDBParam(SqlDbType.Int, "@status_list_item_id", ts.status_list_item_id);
			ProcessBase.SetDBParam(SqlDbType.NVarChar, "@title", ts.title);
			ProcessBase.SetDBParam(SqlDbType.NVarChar, "@description", ts.description);
			ProcessBase.SetDBParam(SqlDbType.Int, "@is_done", ts.is_done ? 1 : 0);

			ts.status_id = ProcessBase.db.ExecuteInsertQuery(sql, ProcessBase.DBParameters);
			return ts;
		}

		public void SaveStatus(List<TaskStatus> statuses) {
			foreach (var ts in statuses) {
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@status_id", ts.status_id);
				ProcessBase.SetDBParam(SqlDbType.Int, "@parent_item_id", ts.parent_item_id);
				ProcessBase.SetDBParam(SqlDbType.Int, "@status_list_item_id", ts.status_list_item_id);
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@title", ts.title);
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@description", ts.description);
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@order_no", ts.order_no);


				var sql = "UPDATE task_status SET "
				          + " parent_item_id = @parent_item_id"
				          + ", status_list_item_id = @status_list_item_id"
				          + ", title = @title"
				          + ", description = @description"
				          + ", order_no = @order_no"
				          + " WHERE status_id = @status_id;";
				var changed = ProcessBase.db.ExecuteUpdateQuery(sql, ProcessBase.DBParameters);
				if (changed < 1) {
					sql = "INSERT INTO task_status ("
					      + " parent_item_id, title, description, order_no, status_list_item_id "
					      + ") VALUES ("
					      + " @parent_item_id, @title, @description, @order_no, @status_list_item_id "
					      + ")";
					ProcessBase.db.ExecuteInsertQuery(sql, ProcessBase.DBParameters);
				}
			}
		}

		public Tasks GetData(int portfolioId, int processId, string type, DateTime? archiveDate = null, int rowId = 0) {
			var tasks = new Tasks();
			var t = GetTasks(type, portfolioId, processId, archiveDate, rowId);
			tasks.task = (List<Dictionary<string, object>>)t["tasks"];
			tasks.processes = (Dictionary<int, object>)t["processes"];

			return tasks;
		}

		public void DeleteStatus(int statusId) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@status_id", statusId);
			ProcessBase.db.ExecuteDeleteQuery("DELETE FROM task_status WHERE status_id = @status_id",
				ProcessBase.DBParameters);
		}

		public Dictionary<string, object> GetBacklogChartData(int processId) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@processId", processId);

			double totalCount = 0;
			double doneCount = 0;
			double totalEffort = 0;
			double doneEffort = 0;

			var ics = new ItemColumns(instance, ProcessBase.process, authenticatedSession);
			var effortCol = ics.GetItemColumnByName("effort");
			var effortSql = "";
			if (effortCol.IsSubquery() && (effortCol.IsOfSubType(ItemColumn.ColumnType.Int) ||
			                               effortCol.IsOfSubType(ItemColumn.ColumnType.Float))) {
				effortSql = ics.GetSubQuery(effortCol, false);
				effortSql += " AS effort,";
				effortSql = effortSql.Replace("pf", "task");
			} else if (effortCol.IsEquation() && (effortCol.IsOfSubType(ItemColumn.ColumnType.Int) ||
			                                      effortCol.IsOfSubType(ItemColumn.ColumnType.Float))) {
				Console.WriteLine("To do, handle equation effort column");
			}

			var backlogHistory = ProcessBase.db.GetDatatableDictionary(
				"select " + effortSql + "*, (select title from _" + instance +
				"_task where item_id =task.parent_item_id) as parent_title  from _" +
				instance + "_task task where item_id in (select distinct item_id from archive__" + instance +
				"_task where owner_item_id = @processId and task_type = 1 and owner_item_type = 'backlog') and owner_item_type != 'backlog'",
				ProcessBase.DBParameters);

			foreach (var r in backlogHistory) {
				totalCount++;
				totalEffort += Conv.ToDouble(r["effort"]);
				if (Conv.ToInt(r["task_status"]) == 1) {
					doneCount++;
					doneEffort += Conv.ToDouble(r["effort"]);
				}
			}

			foreach (DataRow r in ProcessBase.db.GetDatatable(
				"select " + effortSql + "* from _" + instance +
				"_task task  where  owner_item_id = @processId and task_type = 1 and owner_item_type = 'backlog'",
				ProcessBase.DBParameters).Rows) {
				totalCount++;
				totalEffort += Conv.ToDouble(r["effort"]);
			}


			return new Dictionary<string, object> {
				{ "totalCount", totalCount },
				{ "doneCount", doneCount },
				{ "totalEffort", totalEffort },
				{ "doneEffort", doneEffort },
				{ "donePercent", doneCount / totalCount },
				{ "backlogHistory", backlogHistory }
			};
		}

		public Dictionary<string, object> CopyTasks(int sourceId, int targetId, DateTime? copyDate = null,
			DateTime? copyDate2 = null) {
			var s = new Dictionary<string, object>();
			s.Add("owner_item_id", sourceId);

			return CopyTasks(s, targetId, copyDate, copyDate2);
		}

		public Dictionary<string, object> CopyTasks(Dictionary<string, object> searchCriteria, int targetId,
			DateTime? copyDate = null,
			DateTime? copyDate2 = null) {
			var tasks = GetItems(searchCriteria);
			var retval = new Dictionary<string, object>();

			var idPairs = new Dictionary<int, int>();
			var idPairsReversed = new Dictionary<int, int>();

			DateTime? firstDate = null;

			var ics = new ItemColumns(instance, ProcessBase.process, authenticatedSession);
			var linkedTaskCol = ics.GetItemColumnByName("linked_tasks");

			var oldIds = new List<int>();

			foreach (var t in tasks) {
				if (Conv.ToInt(t["owner_item_id"]) == Conv.ToInt(t["parent_item_id"])) t["parent_item_id"] = targetId;

				t["owner_item_id"] = targetId;
				var oldId = Conv.ToInt(t["item_id"]);
				t.Remove("item_id");
				var newId = InsertItemRow();
				t.Add("item_id", newId);
				idPairs.Add(oldId, newId);
				idPairsReversed.Add(newId, oldId);

				if (copyDate != null) {
					if (!DBNull.Value.Equals(t["start_date"])) {
						if (firstDate == null || firstDate > Conv.ToDateTime(t["start_date"])) {
							firstDate = Conv.ToDateTime(t["start_date"]);
						}
					}
				}

				oldIds.Add(oldId);
			}

			if (firstDate == null) {
				retval.Add("tasks", tasks);
				retval.Add("idPairs", idPairs);
				return retval;
			}

			foreach (var oldId in oldIds) {
				ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", oldId);
				ProcessBase.SetDBParam(SqlDbType.Int, "@itemColumnId", linkedTaskCol.ItemColumnId);
				ProcessBase.SetDBParam(SqlDbType.Int, "@newItemId", idPairs[oldId]);
				foreach (DataRow r in ProcessBase.db
					.GetDatatable(
						"SELECT * FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId",
						ProcessBase.DBParameters).Rows) {
					ProcessBase.SetDBParam(SqlDbType.Int, "@selectedItemId",
						idPairs[Conv.ToInt(r["selected_item_id"])]);
					var ibLink = new ItemBase(instance, "task_link_type", authenticatedSession);
					var linkItem = ibLink.GetItem(Conv.ToInt(r["link_item_id"]));
					linkItem.Remove("item_id");
					var linkId = ibLink.InsertItemRow();
					linkItem.Add("item_id", linkId);
					ibLink.SaveItem(linkItem);
					ProcessBase.SetDBParam(SqlDbType.Int, "@linkItemId", linkId);

					ProcessBase.db.ExecuteInsertQuery(
						"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id) VALUES (@newItemId, @itemColumnId, @selectedItemId, @linkItemId) ",
						ProcessBase.DBParameters);
				}
			}

			double totalDays = 0;
			if (copyDate != null) totalDays = (copyDate - firstDate).Value.TotalDays;

			foreach (var t in tasks) {
				if (idPairs.ContainsKey(Conv.ToInt(t["parent_item_id"])))
					t["parent_item_id"] = idPairs[Conv.ToInt(t["parent_item_id"])];

				if (Conv.ToInt(t["task_type"]) == 21)
					CopyStatuses(idPairsReversed[Conv.ToInt(t["item_id"])], t);

				if (copyDate != null && copyDate2 != null) {
					t["start_date"] = copyDate;
					t["end_date"] = copyDate2;
					t["actual_start_date"] = DBNull.Value;
					t["actual_end_date"] = DBNull.Value;
				} else if (copyDate != null) {
					if (!DBNull.Value.Equals(t["start_date"]))
						t["start_date"] = Conv.ToDateTime(t["start_date"]).Value.AddDays(totalDays);

					if (!DBNull.Value.Equals(t["end_date"]))
						t["end_date"] = Conv.ToDateTime(t["end_date"]).Value.AddDays(totalDays);

					t["actual_start_date"] = DBNull.Value;
					t["actual_end_date"] = DBNull.Value;
				}

				t.Remove("linked_tasks");
				if (t.ContainsKey("order_no")) t.Remove("order_no");

				SaveItem(t);
				InvalidateCache(Conv.ToInt(t["item_id"]));
			}

			retval.Add("tasks", tasks);
			retval.Add("idPairs", idPairs);
			return retval;
		}

		public void SaveActualDurations(int taskItemId) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", taskItemId);
			var sql =
				" DECLARE @actualStartDate DATETIME " +
				" DECLARE @actualEndDate DATETIME " +
				" DECLARE @actualEffort DECIMAL(18,2) " +
				" SELECT  " +
				"       @actualStartDate = actual_start_date,  " +
				"       @actualEndDate = actual_end_date,  " +
				"       @actualEffort = actual_effort  " +
				" FROM _" + ProcessBase.instance + "_task WHERE item_id = @itemId " +
				"  " +
				" DECLARE @loop INT = 0 " +
				" DECLARE @days FLOAT = DATEDIFF(day,@actualStartDate,@actualEndDate) " +
				"  " +
				" DELETE FROM actual_durations WHERE task_item_id = @itemId  " +
				" IF (@actualStartDate IS NOT NULL AND @actualEndDate IS NOT NULL AND ISNULL(@actualEffort,0) > 0) BEGIN " +
				"       WHILE @loop <= @days BEGIN " +
				"               INSERT INTO actual_durations ( " +
				"                       task_item_id,  " +
				"                       date,  " +
				"                       hours " +
				"               ) VALUES ( " +
				"                       @itemId, " +
				"                       DATEADD(day, @loop, @actualStartDate), " +
				"                       @actualEffort / (@days + 1) " +
				"               ) " +
				"            SET @loop = @loop + 1 " +
				"       END " +
				" END ";
			ProcessBase.db.ExecuteInsertQuery(sql, ProcessBase.DBParameters);
		}

		public void SaveTask(Dictionary<string, object> data, bool hadLinks = false) {
			if (data.ContainsKey("item_id") && data.ContainsKey("owner_item_id") &&
			    data.ContainsKey("owner_item_type")) {
				ProcessBase.SetDBParam(SqlDbType.Int, "@taskId", Conv.ToInt(data["item_id"]));
				ProcessBase.SetDBParam(SqlDbType.Int, "@ownerItemId", Conv.ToInt(data["owner_item_id"]));
				ProcessBase.SetDBParam(SqlDbType.NVarChar, "@ownerItemType", Conv.ToStr(data["owner_item_type"]));

				var sql = "WITH rec AS ( " +
				          " SELECT t.item_id,(SELECT owner_item_id FROM " +
				          ProcessBase.tableName +
				          " t2 WHERE t.parent_item_id = t2.item_id) AS owner_item_id,  " +
				          " (SELECT owner_item_type FROM " + ProcessBase.tableName +
				          " t2 WHERE t.parent_item_id = t2.item_id) AS owner_item_type " +
				          " FROM " + ProcessBase.tableName + " t " +
				          "	WHERE t.parent_item_id = @taskId " +
				          " UNION ALL " +
				          " SELECT t.item_id, r.owner_item_id AS owner_item_id, r.owner_item_type " +
				          " FROM " + ProcessBase.tableName + " t " +
				          "	INNER JOIN rec r ON t.parent_item_id = r.item_id " +
				          "	)  " +
				          "UPDATE st " +
				          " SET st.owner_item_id = @ownerItemId, st.owner_item_type = @ownerItemType " +
				          " FROM " + ProcessBase.tableName + " st " +
				          " 	INNER JOIN rec " +
				          " ON rec.item_id = st.item_id ";
				ProcessBase.db.ExecuteUpdateQuery(sql, ProcessBase.DBParameters);
			}

			if (data.ContainsKey("actual_start_date") || data.ContainsKey("actual_end_date") ||
			    data.ContainsKey("actual_effort"))
				SaveActualDurations(Conv.ToInt(data["item_id"]));


			if (data.ContainsKey("title") || data.ContainsKey("description") || data.ContainsKey("responsibles")) {
				ProcessBase.SetDBParam(SqlDbType.Int, "item_id", Conv.ToInt(data["item_id"]));

				//Get Task Type & Owner
				var row = ProcessBase.db.GetDataRow(
					"SELECT task_type, owner_item_id FROM _" + instance + "_task WHERE item_id = @item_id",
					ProcessBase.DBParameters);

				var taskType = Conv.ToInt(row["task_type"]);
				var ownerItemId = Conv.ToInt(row["owner_item_id"]);

				//Create update query
				var updatesql = "UPDATE _" + instance + "_task SET ";
				var updateslist = new List<string>();
				if (data.ContainsKey("title")) {
					updateslist.Add("title = @title");
					ProcessBase.SetDBParam(SqlDbType.NVarChar, "title", Conv.ToStr(data["title"]));
				}

				if (data.ContainsKey("description")) {
					updateslist.Add("description = @description");
					ProcessBase.SetDBParam(SqlDbType.NVarChar, "description", Conv.ToStr(data["description"]));
				}

				//Update
				var ck = Config.GetClientConfigurationKey(authenticatedSession,
					Cache.GetItemProcess(ownerItemId, Instance), "View Configurator", "process.gantt", "kanbanMode");
				var SimpleMode = "1" == ck;

				if (taskType == 1 && SimpleMode) {
					//Updating Task is a sprint task -- Update it's cards
					if (updateslist.Count > 0)
						ProcessBase.db.ExecuteUpdateQuery(
							updatesql + Modi.JoinListWithComma(updateslist) +
							" WHERE parent_item_id = @item_id AND task_type = 0", ProcessBase.DBParameters);

					var ics = new ItemColumns(instance, ProcessBase.process, authenticatedSession);
					var linkedTaskCol = ics.GetItemColumnByName("responsibles");
					foreach (DataRow card in ProcessBase.db.GetDatatable("SELECT * FROM _" + instance +
					                                                     "_task WHERE parent_item_id = @item_id AND task_type = 0",
						ProcessBase.DBParameters).Rows) {
						ProcessBase.DBParameters.Clear();
						ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(data["item_id"]));
						ProcessBase.SetDBParam(SqlDbType.Int, "@itemColumnId", linkedTaskCol.ItemColumnId);
						ProcessBase.SetDBParam(SqlDbType.Int, "@newItemId", Conv.ToInt(card["item_id"]));

						ProcessBase.db.ExecuteDeleteQuery(
							"DELETE FROM item_data_process_selections WHERE item_column_id = @itemColumnId AND item_id = @newItemId",
							ProcessBase.DBParameters);

						foreach (DataRow r in ProcessBase.db
							.GetDatatable(
								"SELECT * FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId",
								ProcessBase.DBParameters).Rows) {
							ProcessBase.SetDBParam(SqlDbType.Int, "@selectedItemId", Conv.ToInt(r["selected_item_id"]));
							var ibLink = new ItemBase(instance, "task_user_effort", authenticatedSession);
							var linkItem = ibLink.GetItem(Conv.ToInt(r["link_item_id"]));
							linkItem.Remove("item_id");

							var linkId = ibLink.InsertItemRow();
							linkItem.Add("item_id", linkId);
							ibLink.SaveItem(linkItem);

							ProcessBase.SetDBParam(SqlDbType.Int, "@linkItemId", linkId);
							ProcessBase.db.ExecuteInsertQuery(
								"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id) VALUES (@newItemId, @itemColumnId, @selectedItemId, @linkItemId) ",
								ProcessBase.DBParameters);
							InvalidateCache(Conv.ToInt(linkId));
							InvalidateCache(Conv.ToInt(r["item_id"]));
						}

						InvalidateCache(Conv.ToInt(Conv.ToInt(card["item_id"])));
					}

					InvalidateCache(Conv.ToInt(data["item_id"]));
				} else if (taskType == 0 && SimpleMode) {
					//Updating Task is a card -- Update it's sprint task
					if (updateslist.Count > 0)
						ProcessBase.db.ExecuteUpdateQuery(
							updatesql + Modi.JoinListWithComma(updateslist) +
							" WHERE item_id = (SELECT parent_item_id FROM _" + instance +
							"_task WHERE item_id = @item_id) AND task_type = 1", ProcessBase.DBParameters);

					var ics = new ItemColumns(instance, ProcessBase.process, authenticatedSession);
					var linkedTaskCol = ics.GetItemColumnByName("responsibles");
					foreach (DataRow card in ProcessBase.db.GetDatatable("SELECT * FROM _" + instance +
					                                                     "_task WHERE item_id = (SELECT parent_item_id FROM _" +
					                                                     instance +
					                                                     "_task WHERE item_id = @item_id) AND task_type = 1",
						ProcessBase.DBParameters).Rows) {
						ProcessBase.DBParameters.Clear();
						ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(data["item_id"]));
						ProcessBase.SetDBParam(SqlDbType.Int, "@itemColumnId", linkedTaskCol.ItemColumnId);
						ProcessBase.SetDBParam(SqlDbType.Int, "@newItemId", Conv.ToInt(card["item_id"]));

						ProcessBase.db.ExecuteDeleteQuery(
							"DELETE FROM item_data_process_selections WHERE item_column_id = @itemColumnId AND item_id IN (SELECT item_id FROM _" +
							instance + "_task WHERE item_id = @newItemId)",
							ProcessBase.DBParameters);

						foreach (DataRow r in ProcessBase.db
							.GetDatatable(
								"SELECT * FROM item_data_process_selections WHERE item_id = @itemId AND item_column_id = @itemColumnId",
								ProcessBase.DBParameters).Rows) {
							ProcessBase.SetDBParam(SqlDbType.Int, "@selectedItemId",
								Conv.ToInt(r["selected_item_id"]));
							var ibLink = new ItemBase(instance, "task_user_effort", authenticatedSession);
							var linkItem = ibLink.GetItem(Conv.ToInt(r["link_item_id"]));
							linkItem.Remove("item_id");

							var linkId = ibLink.InsertItemRow();
							linkItem.Add("item_id", linkId);
							ibLink.SaveItem(linkItem);

							ProcessBase.SetDBParam(SqlDbType.Int, "@linkItemId", linkId);
							ProcessBase.db.ExecuteInsertQuery(
								"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id, link_item_id) VALUES (@newItemId, @itemColumnId, @selectedItemId, @linkItemId) ",
								ProcessBase.DBParameters);
							//InvalidateCache(Conv.toInt(linkId));
							//InvalidateCache(Conv.toInt(r["item_id"]));
							//InvalidateCache(Conv.toInt(r["selected_item_id"]));
						}

						//InvalidateCache(Conv.toInt(card["item_id"]));
					}

					//InvalidateCache(Conv.toInt(data["item_id"]));
					InvalidateCache();
				}
			}

			//If allocation automation has been disabled, then no need to do the rest
			if (!Conv.ToBool(Config.GetValue("Gantt", "AutoAllocateTaskInsert")) &&
			    !Conv.ToBool(Config.GetValue("Gantt", "AutoAllocateTaskUpdate")) &&
			    !Conv.ToBool(Config.GetValue("Gantt", "AutoAllocateTaskDelete"))
			) return;

			//Add required, but not supplied fields to task
			var allocationCheck = data.ContainsKey("responsibles") || data.ContainsKey("start_date") ||
			                      data.ContainsKey("end_date") || hadLinks || data.ContainsKey("competencies");

			data = EnsureTaskData(data);

			//Card update
			if (data.ContainsKey("task_type") && Conv.ToInt(data["task_type"]) == 0) {
				ProcessBase.SetDBParam(SqlDbType.Int, "@sid", Conv.ToInt(data["status_id"]));
				var pt = ProcessBase.db.GetDataRow(
					"select kanban_status from _" + instance +
					"_list_task_status where item_id = (select status_list_item_id from task_status where status_id = @sid)",
					ProcessBase.DBParameters);
				if (pt != null) {
					var ks = Conv.ToInt(pt["kanban_status"]);
					if (ks == 2) {
						//Card Done
						ProcessBase.SetDBParam(SqlDbType.Int, "@tid", Conv.ToInt(data["item_id"]));
						ProcessBase.db.ExecuteUpdateQuery(
							"UPDATE _" + instance + "_task SET actual_end_date = getutcdate() WHERE item_id = @tid",
							ProcessBase.DBParameters);
					} else if (ks > 0) {
						//Card Not Done not Not Started
						ProcessBase.SetDBParam(SqlDbType.Int, "@tid", Conv.ToInt(data["item_id"]));
						ProcessBase.db.ExecuteUpdateQuery(
							"UPDATE _" + instance +
							"_task SET actual_start_date = getutcdate() WHERE item_id = @tid AND actual_start_date IS NULL",
							ProcessBase.DBParameters);
					}
				}
			}


			//Check that the owner is gantt
			if (data.ContainsKey("owner_item_type") && Conv.ToStr(data["owner_item_type"]) == "gantt" &&
			    data.ContainsKey("task_type") && Conv.ToInt(data["task_type"]) == 0) return;

			if (!data.ContainsKey("owner_item_id")) return;

			var mStates = new States(instance, authenticatedSession);
			if (!data.ContainsKey("owner_item_id") || Conv.ToInt(data["owner_item_id"]) == 0) return;

			//Check Rights from the Owner -- This model should be changed to something more global
			var pp = new Processes(instance, authenticatedSession).GetProcessByItemId(
				Conv.ToInt(data["owner_item_id"]));
			var fS = mStates.GetEvaluatedStates(pp, "project_allocation", Conv.ToInt(data["owner_item_id"]),
				new List<string>
					{ "project_allocation.write", "project_allocation.request", "project_allocation.send" });
			if (!(fS.States.Contains("project_allocation.write") || fS.States.Contains("project_allocation.request") ||
			      fS.States.Contains("project_allocation.send"))) {
				return;
			}

			//Are allocation modifications required?
			if (!allocationCheck) return;

			//update allocations for task
			var rmr = new ResourceManagementRepository(new ConnectionBase(instance, authenticatedSession),
				new EntityServiceProvider(instance, authenticatedSession));
			var allocationService = new AllocationService(instance,
				authenticatedSession,
				rmr);

			//Check if there are ids that need processing
			var uids = Conv.ToIntArray(data["responsibles"]);
			var cids = Conv.ToIntArray(data["competencies"]);
			//if ((uids == null || uids.Length <= 0) && (cids == null || cids.Length <= 0)) return;
			var allIds = new List<int[]> { uids, cids };

			//Collect task days
			if (Conv.ToStr(data["start_date"]) == "" || Conv.ToStr(data["end_date"]) == "") return;
			var startDate = (DateTime)Conv.ToDateTime(data["start_date"]);
			var endDate = (DateTime)Conv.ToDateTime(data["end_date"]);
			var totalDays = (endDate - startDate).TotalDays + 1;

			//Get Task's Item Data Process Selections
			var idps = new ItemDataProcessSelections(ProcessBase.instance, "task",
				ProcessBase.authenticatedSession);
			var icsTask = new ItemColumns(ProcessBase.instance, "task", ProcessBase.authenticatedSession);

			for (var i = 0; i <= 1; i++) {
				var userMode = i == 0;
				var ids = allIds[i];
				if (ids.Length == 0) {
					//There are no ids -- remove all allocations and continue
					if (!Conv.ToBool(Config.GetValue("Gantt", "AutoAllocateTaskDelete"))) continue;
					if (userMode)
						allocationService.DeleteAllocationsForNotAllocatedUsers(Conv.ToInt(data["owner_item_id"]),
							Conv.ToInt(data["item_id"]), new List<int>());
					else
						allocationService.DeleteAllocationsForNotAllocatedCompetencies(
							Conv.ToInt(data["owner_item_id"]),
							Conv.ToInt(data["item_id"]), new List<KeyValuePair<int, int>>());
					continue;
				}

				//Get Effort Information
				Dictionary<int, Dictionary<string, object>> efforts;
				var competencies = new List<KeyValuePair<int, int>>();
				if (userMode)
					efforts = idps.GetItemDataProcessSelectionIds(Conv.ToInt(data["item_id"]),
						icsTask.GetItemColumnByName("responsibles").ItemColumnId, "task_user_effort");
				else
					efforts = idps.GetItemDataProcessSelectionIds(Conv.ToInt(data["item_id"]),
						icsTask.GetItemColumnByName("competencies").ItemColumnId, "task_competency_effort");

				foreach (var id in ids) {
					//Calculate Daily Effort
					var effort = Conv.ToDouble(efforts[id]["effort"]);
					var dailyEffort = effort / totalDays;
					var costCenterId = 0;
					if (!userMode) {
						foreach (var c in (List<int>)efforts[id]["cost_center"]) {
							costCenterId = Conv.ToInt(c);
							break;
						}
					}

					competencies.Add(new KeyValuePair<int, int>(id, costCenterId));

					//Get Existing Allocation
					var allocationItemId = userMode
						? allocationService.GetAlloctionItemId(
							Conv.ToInt(data["owner_item_id"]),
							Conv.ToInt(data["item_id"]), id)
						: allocationService.GetAlloctionItemId(
							Conv.ToInt(data["owner_item_id"]),
							Conv.ToInt(data["item_id"]), id, costCenterId);

					if (allocationItemId > 0) {
						//Allocation Exists -- Update
						var allocationInfo = allocationService.GetInformationAboutAllocation(allocationItemId);

						//Get Allocation's Dates
						ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(data["item_id"]));
						var pt = ProcessBase.db.GetDataRow(
							"SELECT TOP 1 start_date, end_date FROM archive__" + instance +
							"_task WHERE item_id = @itemId ORDER BY archive_id DESC", ProcessBase.DBParameters);
						DateTime? originalStartDate = null;
						DateTime? originalEndDate = null;
						if (pt != null) {
							originalStartDate = Conv.ToDateTime(pt["start_date"]);
							originalEndDate = Conv.ToDateTime(pt["end_date"]);
						}

						//Allocation is updated only if dates or effort differ
						var allocationShouldBeUpdated =
							originalStartDate == null ||
							originalEndDate == null ||
							originalStartDate.Value.Date != startDate.Date ||
							originalEndDate.Value.Date != endDate.Date ||
							(userMode == false &&
							 costCenterId != allocationService.GetAllocationCostCenter(allocationItemId)) ||
							Math.Round(allocationInfo.TotalHours, 0) != Math.Round(effort, 0); //Floating Point Error

						//Is Update Setting Enabled?
						if (!Conv.ToBool(Config.GetValue("Gantt", "AutoAllocateTaskUpdate"))) continue;

						//Updating hours -- delete and add again
						if (allocationShouldBeUpdated) {
							rmr.DeleteAllocationHours(allocationItemId);
							allocationService.InsertAllocationDurations(allocationItemId, startDate,
								endDate,
								dailyEffort, id, false);
						}

						//Updating task name and status
						var allocationModel = new EditAllocationModel {
							item_id = allocationItemId,
							process_item_id = Conv.ToInt(data["owner_item_id"]),
							title = Conv.ToStr(data["title"])
						};
						if (!userMode) allocationModel.cost_center = costCenterId;

						//Return Allocation To Draft Status as it was Changed
						if (allocationShouldBeUpdated) allocationModel.status = AllocationStatus.Draft;

						//Finally Commit Changes
						allocationService.UpdateAllocation(allocationModel, true);
					} else {
						//Allocation doesn't exist -- Insert if setting is enabled
						if (!Conv.ToBool(Config.GetValue("Gantt", "AutoAllocateTaskInsert"))) continue;

						//Add task details
						var allocationModel = new EditAllocationModel();
						allocationModel.task_item_id = Conv.ToInt(data["item_id"]);
						allocationModel.process_item_id = Conv.ToInt(data["owner_item_id"]);
						allocationModel.title = Conv.ToStr(data["title"]);
						allocationModel.resource_item_id = id;
						if (!userMode) allocationModel.cost_center = costCenterId;

						//Check if autoaccept is needed
						ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(data["owner_item_id"]));
						var process = Cache.GetItemProcess(Conv.ToInt(data["owner_item_id"]), instance);
						var b = Config.GetClientConfiguration(authenticatedSession, process,
							"View Configurator", "process.projectAllocation");

						//Change Status Accordingly
						if (b != null) {
							bool? c = (bool)JObject.Parse(b)["autoAccept"];
							allocationModel.status =
								c == true ? AllocationStatus.StatusForTasks : AllocationStatus.Draft;
						} else {
							allocationModel.status = AllocationStatus.Draft;
						}

						//Insert New Allocation Row
						allocationService.CreateAllocationForSpecs(allocationModel, startDate, endDate,
							dailyEffort,
							authenticatedSession.item_id);
					}
				}

				//There are ids -- remove ids that do not exist in the list
				if (!Conv.ToBool(Config.GetValue("Gantt", "AutoAllocateTaskDelete"))) continue;
				if (userMode)
					allocationService.DeleteAllocationsForNotAllocatedUsers(Conv.ToInt(data["owner_item_id"]),
						Conv.ToInt(data["item_id"]), uids.ToList());
				else
					allocationService.DeleteAllocationsForNotAllocatedCompetencies(
						Conv.ToInt(data["owner_item_id"]),
						Conv.ToInt(data["item_id"]), competencies);
			}
		}

		private Dictionary<string, object> EnsureTaskData(Dictionary<string, object> data) {
			if (data.ContainsKey("item_id") && (!data.ContainsKey("owner_item_id") ||
			                                    !data.ContainsKey("owner_item_type") ||
			                                    !data.ContainsKey("responsibles") ||
			                                    !data.ContainsKey("task_type") ||
			                                    !data.ContainsKey("start_date") ||
			                                    !data.ContainsKey("end_data"))
			) {
				var dataFromDb = GetItem(Conv.ToInt(data["item_id"]), checkRights: false);
				foreach (var d in dataFromDb.Keys) {
					if (!data.ContainsKey(d)) data.Add(d, dataFromDb[d]);
				}
			}

			return data;
		}
	}
}