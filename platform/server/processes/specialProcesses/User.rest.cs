﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.specialProcesses {
	[Route("v1/core/User")]
	public class UserRest : RestBase {
		[HttpGet("generatePasswords")]
		public List<int> Get() {
			var u = new User(instance, currentSession.item_id);
			return u.GeneratePasswords(currentSession);
		}
	}
}
