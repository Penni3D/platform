﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.specialProcesses {
	[Serializable]
	public class UserGroups : ConnectionBase {
		private readonly AuthenticatedSession session;
		private readonly int userId;

		public UserGroups(string instance, AuthenticatedSession s) : base(instance, s) {
			session = s;
			userId = session.item_id;
		}

		public List<int> GetUserGroupsForAuthenticatedUser() {
			var retval = new List<int>();
			SetDBParam(SqlDbType.Int, "@userId", userId);
			foreach (DataRow row in db.GetDatatable(
				"SELECT i.item_id FROM items i INNER JOIN item_data_process_selections idps ON i.item_id = idps.item_id WHERE instance = @instance AND process = 'user_group' AND item_column_id = (SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'user_group' AND name = 'user_id') AND selected_item_id = @userId",
				DBParameters).Rows) {
				retval.Add((int) row["item_id"]);
			}

			var ics = new ItemColumns(instance, "user_group", session);
			if (ics.HasColumnName("all_group")) {
				var ib = new ItemBase(instance, "user_group", session);
				var search = new Dictionary<string, object>();
				search.Add("all_group", 1);
				foreach (var row in ib.GetItems(search)) {
					if (!retval.Contains(Conv.ToInt(row["item_id"]))) {
						retval.Add(Conv.ToInt(row["item_id"]));
					}
				}
			}

			return retval;
		}
	}
}