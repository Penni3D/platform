﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes {
	public class ProcessItem : ConnectionBaseProcess {
		private Dictionary<string, object> dictionary = new Dictionary<string, object>();

		public ProcessItem(string instance, string process, int currentUserId) : base(instance, process, new AuthenticatedSession(instance, currentUserId, null)) { }

		public int ItemId {
			get => (int) dictionary["Item_Id".ToLower()];
			set => AddMember("Item_Id", value);
		}

		public virtual void FillItem(DataRow source) {
			var result = Enumerable.Range(0, source.Table.Columns.Count)
				.ToDictionary(
					i => source.Table.Columns[i].ColumnName,
					i => source[source.Table.Columns[i].ColumnName]
				);

			//Replace Members
			SetMembers(result);
		}

		public object GetMember(string member) {
			if (!dictionary.ContainsKey(member)) {
				throw new CustomArgumentException("Process '" + process + "' of instance '" + instance + "' does not contain or have access to column '" + member + "'");
			}
			return dictionary[member.ToLower()];
		}

		public void AddMember(string member, object value) {
			dictionary[member.ToLower()] = value;
		}

		public Dictionary<string, object> GetMembers() {
			return dictionary;
		}

		public void SetMembers(Dictionary<string, object> data) {
			dictionary = data;
		}
	}
}