﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes {
	public class ItemColumnJoin {
		public ItemColumnJoin() {

		}

		public ItemColumnJoin(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			ItemColumnJoinId = (int)dr[alias + "item_column_join_id"];
			ItemColumnId = (int)dr[alias + "item_column_id"];
			ParentColumnType = (int)dr[alias + "parent_column_type"];
			if (dr[alias + "parent_item_column_id"] != DBNull.Value) {
				ParentItemColumnId = (int?)dr[alias + "parent_item_column_id"];
			} else {
				ParentItemColumnId = null;
			}
			ParentAdditionalJoin = JsonConvert.DeserializeObject<object>((string)dr["parent_additional_join"]);
			SubColumnType = (int)dr[alias + "sub_column_type"];
			if (dr[alias + "sub_item_column_id"] != DBNull.Value) {
				SubItemColumnId = (int?)dr[alias + "sub_item_column_id"];
			} else {
				SubItemColumnId = null;
			}
			SubAdditionalJoin = JsonConvert.DeserializeObject<object>((string)dr["sub_additional_join"]);
		}

		public int ItemColumnJoinId { get; set; }
		public int ItemColumnId { get; set; }
		public int ParentColumnType { get; set; }
		public int? ParentItemColumnId { get; set; }
		public object ParentAdditionalJoin { get; set; }
		public int SubColumnType { get; set; }
		public int? SubItemColumnId { get; set; }
		public object SubAdditionalJoin { get; set; }
	}
	public class ItemColumnJoins : ProcessBase {
		public ItemColumnJoins(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
		}

		public List<ItemColumnJoin> GetItemColumnJoins(int itemColumnId) {
			var result = new List<ItemColumnJoin>();
			//Get Column Names
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnId);

			foreach (DataRow row in db.GetDatatable("SELECT item_column_join_id, item_column_id, parent_column_type, parent_item_column_id, parent_additional_join, sub_column_type, sub_item_column_id, sub_additional_join FROM item_column_joins WHERE item_column_id = @itemColumnId", DBParameters).Rows) {
				var joinCol = new ItemColumnJoin(row, authenticatedSession);
				result.Add(joinCol);
			}
			return result;
		}

		public void Delete(int itemColumnJoinId) {
			SetDBParam(SqlDbType.Int, "itemColumnJoinId", itemColumnJoinId);
			db.ExecuteDeleteQuery("DELETE FROM item_column_joins WHERE item_column_join_id = @itemColumnJoinId", DBParameters);
		}

		public void Add(ItemColumnJoin itemColumnJoin) {
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnJoin.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@parentColumnType", itemColumnJoin.ParentColumnType);
			SetDBParam(SqlDbType.Int, "@parentItemColumnId", Conv.IfNullThenDbNull(itemColumnJoin.ParentItemColumnId));
			SetDBParam(SqlDbType.NVarChar, "@parentAdditionalJoin", JsonConvert.SerializeObject(itemColumnJoin.ParentAdditionalJoin));
			SetDBParam(SqlDbType.Int, "@subColumnType", itemColumnJoin.SubColumnType);
			SetDBParam(SqlDbType.Int, "@subItemColumnId", Conv.IfNullThenDbNull(itemColumnJoin.SubItemColumnId));
			SetDBParam(SqlDbType.NVarChar, "@subAdditionalJoin", JsonConvert.SerializeObject(itemColumnJoin.SubAdditionalJoin));
			db.ExecuteInsertQuery("INSERT INTO item_column_joins (item_column_id, parent_column_type, parent_item_column_id, parent_additional_join, sub_column_type, sub_item_column_id, sub_additional_join) VALUES (@itemColumnId, @parentColumnType, @parentItemColumnId, @parentAdditionalJoin, @subColumnType, @subItemColumnId, @subAdditionalJoin);", DBParameters);
		}

		public void Save(ItemColumnJoin itemColumnJoin) {
			SetDBParam(SqlDbType.Int, "@itemColumnJoinId", itemColumnJoin.ItemColumnJoinId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumnJoin.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@parentColumnType", itemColumnJoin.ParentColumnType);
			SetDBParam(SqlDbType.Int, "@parentItemColumnId", Conv.IfNullThenDbNull(itemColumnJoin.ParentItemColumnId));
			SetDBParam(SqlDbType.NVarChar, "@parentAdditionalJoin", JsonConvert.SerializeObject(itemColumnJoin.ParentAdditionalJoin));
			SetDBParam(SqlDbType.Int, "@subColumnType", itemColumnJoin.SubColumnType);
			SetDBParam(SqlDbType.Int, "@subItemColumnId", Conv.IfNullThenDbNull(itemColumnJoin.SubItemColumnId));
			SetDBParam(SqlDbType.NVarChar, "@subAdditionalJoin", JsonConvert.SerializeObject(itemColumnJoin.SubAdditionalJoin));
			db.ExecuteUpdateQuery("UPDATE item_column_joins SET item_column_id = @itemColumnId, parent_column_type = @parentColumnType, parent_item_column_id = @parentItemColumnId, parent_additional_join = @parentAdditionalJoin, sub_column_type = @subColumnType, sub_item_column_id = @subItemColumnId, sub_additional_join = @subAdditionalJoin WHERE item_column_join_id = @itemColumnJoinId", DBParameters);
		}
	}
}