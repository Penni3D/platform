using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class SendDigestEmails : BackgroundProcessBase {
		public override string Name { get; } = "SendDigestEmails";
		public override string LanguageVariable { get; } = "SEND_DIGEST_EMAILS";
		public override string FinalizeMessageBody { get; } = "DIGEST_EMAILS_READY";
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();

		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session, Dictionary<string,object> parameters, string instance, int notificationId) {
			try {
				var cb = new ConnectionBase(instanceSchedule.Instance, session);
				cb.SetDBParam(SqlDbType.NVarChar, "@instance", instanceSchedule.Instance);

				if (instanceSchedule.ConfigOptions.Length == 0) {
					instanceSchedule.ConfigOptions =
						"{\"@HeaderText\":{\"en-GB\":\"\"},\"@FooterText\":{\"en-GB\":\"\"},\"@Compact\":true,\"@SubjectText\":{\"en-GB\":\"\"} }";
				}

				var jObject = JObject.Parse(instanceSchedule.ConfigOptions);
				var emailList = new List<Email>();
				var emailsByReceiver = new Dictionary<string, List<string>>();
				var languageOfUsers = new Dictionary<string, string>();

				var hideHr = Conv.ToBool(jObject["@Compact"]);

				var digestEmails = cb.db.GetDatatable("SELECT DISTINCT(to_recipients), locale_id, translated_body, translated_subject,subject,body, sent_on FROM (" +
				                                      "SELECT translated_body, translated_subject, k.locale_id, k.item_id, to_recipients, subject,body, sent_on from instance_emails ie" +
				                                      " INNER JOIN _" + Conv.ToSql(instanceSchedule.Instance) + "_user k ON k.email = ie.to_recipients " +
				                                      "where instance = @instance AND digest_status = 1 " +
				                                      "	) as iq", cb.DBParameters);

				foreach (DataRow r in digestEmails.Rows) {
					if (!emailsByReceiver.ContainsKey(Conv.ToStr(r["to_recipients"]))) {
						emailsByReceiver.Add(Conv.ToStr(r["to_recipients"]), new List<string>());
					}
					emailsByReceiver[Conv.ToStr(r["to_recipients"])].Add(Conv.ToStr(r["translated_body"]));

					if (!languageOfUsers.ContainsKey(Conv.ToStr(r["to_recipients"]))) {
						languageOfUsers.Add(Conv.ToStr(r["to_recipients"]), Conv.ToStr(r["locale_id"]));
					}
				}

				foreach (var receiver in emailsByReceiver) {
					Email email_ = null;

					string combinedText;
					if (Conv.ToStr(jObject["@HeaderText"][languageOfUsers[receiver.Key]]).Contains("html")) {
						combinedText = Conv.ToStr(jObject["@HeaderText"][languageOfUsers[receiver.Key]]["html"]) + "<br><br>";
					} else {
						combinedText = Conv.ToStr(jObject["@HeaderText"][languageOfUsers[receiver.Key]]) + "<br><br>";
					}
					foreach (var mailtext in receiver.Value) {
						if (!hideHr) {
							combinedText += mailtext + "<br><br><hr><br><br>";
						} else {
							combinedText += mailtext;
						}
					}

					string footerText;

					if (Conv.ToStr(jObject["@FooterText"][languageOfUsers[receiver.Key]]).Contains("html")) {
						footerText = "<br>" + Conv.ToStr(jObject["@FooterText"][languageOfUsers[receiver.Key]]["html"]);
					} else {
						footerText = "<br>" + Conv.ToStr(jObject["@FooterText"][languageOfUsers[receiver.Key]]);
					}

					combinedText += footerText;

					email_ = new Email(instance, session, combinedText,  Conv.ToStr(jObject["@SubjectText"][languageOfUsers[receiver.Key]]));
					email_.Add(receiver.Key, Email.AddressTypes.ToAdress);
					emailList.Add(email_);
				}

				var affectText = emailList.Count == 0 ? "Complete but no digest emails found" : "Digest emails successfully sent";

				foreach (Email email in emailList) {
					email.Send(digest: false, instanceOpt: instanceSchedule.Instance);
				}

				cb.db.ExecuteUpdateQuery("UPDATE instance_emails SET digest_status = 0 WHERE digest_status = 1",
					cb.DBParameters);

				Result.Description = FinalizeMessageBody;
				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				WriteToLog(affectText);
				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}
		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance, AuthenticatedSession session) {

			var help1 = "DIGEST_BG_HEADER";
			var help2 = "DIGEST_BG_FOOTER";
			var help3 = "DIGEST_BG_SUBJECT";
			var help4 = "EMAIL_MODE";
			var result = new List<BackgroundProcessConfigValue>();

			var langFile = new LanguageFile();
			var langDictionary = langFile.GetLanguageKeyCollectionFromJSON("keto", session.locale_id);

			var ht = new BackgroundProcessConfigValue("@HeaderText", "string", BackgroundOptionType.Translation, null, null,
				null,
				 help1);
			result.Add(ht);

			var ft = new BackgroundProcessConfigValue("@FooterText", "string", BackgroundOptionType.Translation, null, null,
				null,
				help2);
			result.Add(ft);


			var subject = new BackgroundProcessConfigValue("@SubjectText", "string", BackgroundOptionType.Translation, null, null,
				null,
				help3,
				ignoreRichText:true);

			result.Add(subject);

			var values = new List<SendOption>();

			var first = new SendOption(langDictionary.ContainsKey("EMAIL_MODE_COMPACT") ? Conv.ToStr(langDictionary["EMAIL_MODE_COMPACT"]) : "EMAIL_MODE_COMPACT", true);
			var second = new SendOption(langDictionary.ContainsKey("EMAIL_MODE_LOOSE") ? Conv.ToStr(langDictionary["EMAIL_MODE_LOOSE"]) : "EMAIL_MODE_LOOSE", false);

			values.Add(first);
			values.Add(second);

			var hr = new BackgroundProcessConfigValue("@Compact", "list", BackgroundOptionType.List, "value", "name",
				values,
				help4);
			result.Add(hr);

			return result;
		}

		private class SendOption{
			public SendOption(string n, bool v) {
				name = n;
				value = v;
			}

			public string name { get; set; }
			public bool value { get; set; }
		}
	}
}