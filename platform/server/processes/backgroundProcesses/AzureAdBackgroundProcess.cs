using System;
using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class AzureAdBackgroundProcess : BackgroundProcessBase {
		public override string Name { get; } = "AzureADBackgroundProcess";
		public override string LanguageVariable { get; } = "BACKGROUND_PROCESSES_AZUREAD";
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();

		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }
		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule , AuthenticatedSession session, Dictionary<string,object> parameters, string instance, int notificationId) {
			var scheduleConfig = instanceSchedule.ConfigOptions; //cb.db.GetDataRow("SELECT * FROM instance_schedules WHERE schedule_id=1")["config_options"]; 
			if (JObject.Parse(Conv.ToStr(scheduleConfig)).ContainsKey("@AzureAdDomain") && Conv.ToStr(JObject.Parse(Conv.ToStr(scheduleConfig))["@AzureAdDomain"])!="") {
				var azD = new AzureAd(instanceSchedule.Instance, session, this);
				Result.Outcome = azD.Sync(Conv.ToStr(JObject.Parse(Conv.ToStr(scheduleConfig))["@AzureAdDomain"]), -1, -1);
			}
            else {
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
			}
			return Result;
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance, AuthenticatedSession session) {
			var result = new List<BackgroundProcessConfigValue>();
			var domainsToOption = new List<object>();
			foreach (string domain in Config.Domains) {
				domainsToOption.Add(new Dictionary<string, object> { { "Variable", domain }, { "Value", domain } });
			}

			var domains = new BackgroundProcessConfigValue("@AzureAdDomain", "string", BackgroundOptionType.List, "Value", "Variable", domainsToOption);
			result.Add(domains);

			return result;
		}
	}
}