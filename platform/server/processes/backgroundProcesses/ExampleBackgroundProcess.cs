using System;
using System.Collections.Generic;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class ExampleBackgroundProcess : BackgroundProcessBase {

		/*Every Background process must include these properties.
		When your Background process is ready, remember to add it to InitializeServer.cs -file function BackgroundProcesses
		For example: RegisteredBackgroundProcesses.AddType(new JaakkosBackgroundProcess());*/

		public override string Name { get; } = "ExampleBackgroundProcess";
		public override string LanguageVariable { get; } = "BACKGROUND_EXAMPLE";
		public override string FinalizeMessageBody { get; } = "Job is done";
		// public virtual string FinalizeMessageName { get; } = "Name something";
		// public virtual string FinalizeMessageTitle { get; } = "Title something";
		
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();

		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }

		/*Just copy paste the stuff above for every Background process you create. Replace Name and LanguageVariable with our own.*/


		/*This function will be called in the TimedHostedService.cs -file. All the logic and magic will be here. */
		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session, Dictionary<string,object> parameters, string instance, int notificationId) {

			try {
				/*This property Result.Description will be used to create a message in notification in the client*/
				Result.Description = FinalizeMessageBody;
				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				return Result;
			} catch (Exception e) {
				/*Please handle the outcome as well as you can so the log will be good and informative AND remember to use WriteToLog-function!*/
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		/*When different kind of background processes are returned to client-side, this function will be called in the REST-file.
		Stuff that is needed to configure your process will be implemented here*/
		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance, AuthenticatedSession session) {

			/*In this example we will return all the user process columns as a list for an option to choose one column and that column's
			item id will be saved*/

			var listCols = new List<ItemColumn>();

			var itemColumns =
				new ItemColumns(session.instance, "user",
					session);
			listCols = itemColumns.GetItemColumns();

			/*It is possible to set help texts to your config fields. Help text should be a language variable and it will be passed
			for the BackgroundProcessConfigValue -override constructor as a last parameter (see below)*/
			var help1 = "ADDITIONAL_INFO2";

			var result = new List<BackgroundProcessConfigValue>();

			/*Here are a fex examples of different config options. This example would bring three 'config fields' to client-side. If you are
			familiar with how the sub query configs work, this should be easy!*/
			var t = new BackgroundProcessConfigValue("@ItemColumnId", "int", BackgroundOptionType.List, "ItemColumnId", "Variable",
				listCols, help1, true); //Config can be 'use filter'. This means that you can have a dictionary for example Dictionary<string(processName),ItemColumn> as your values
			result.Add(t);

			var z = new BackgroundProcessConfigValue("@ExampleNumber", "float", BackgroundOptionType.Float, null, null,
				null);
			result.Add(z);

			var a = new BackgroundProcessConfigValue("@Examplestring", "string", BackgroundOptionType.Nvarchar, null, null,
				null);
			result.Add(a);

			return result;

		}
		//Write to log
	}
}