using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class SubqueryCacheBackgroundProcess : BackgroundProcessBase {
		public override string Name { get; } = "SubqueryCacheBackgroundProcess";
		public override string LanguageVariable { get; } = "BACKGROUND_SQCACHE";
		public override string FinalizeMessageTitle { get; } = "ACTION_EXECUTE_READY";
		public override string FinalizeMessageBody { get; } = "ACTION_EXECUTE_READY_1";
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();

		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }

		

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session,
			Dictionary<string, object> parameters, string instance, int notificationId) {
			Result.Description = FinalizeMessageBody;
			try {
				new SubqueryCacheUpdater().UpdateCache(instance, "", session);
				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}
	}
}