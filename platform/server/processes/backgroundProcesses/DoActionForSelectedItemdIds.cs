using System;
using System.Collections;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.actions;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class DoActionForSelectedItemIds : BackgroundProcessBase {
		public override string Name { get; } = "DoActionForSelectedItemIds";
		public override string LanguageVariable { get; } = "DO_ACTION_FOR_SEL_ITEM_IDS";
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();
		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }

		public override string FinalizeMessageTitle { get; } = "ACTION_EXECUTE_READY";
		public override string FinalizeMessageBody { get; } = "ACTION_EXECUTE_READY_1";

		public override int NotSchedulable { get; set; } = 1;

		public override bool OnFinish { get; set; } = true;

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session,
			Dictionary<string, object> parameters, string instance, int notificationId) {
			var actionId = Conv.ToInt(parameters["Action"]);
			if (actionId == 0) {
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}

			try {
				var actions = new ProcessActions(instance, Conv.ToStr(parameters["Process"]), session);
				foreach (var v in parameters) {
					if (v.Key != "Ids") continue;
					var idsToList = (IList) v.Value;
					
					foreach (var v2 in idsToList) {
						try {
							actions.DoAction(actionId, Conv.ToInt(v2));
						} catch (Exception e) {
							Diag.SupressException(e);
							WriteToLog("Action " + actionId + " for item " + v2 + " failed.", true);
						}
					}
				}

				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			var result = new List<BackgroundProcessConfigValue>();
			return result;
		}
	}
}