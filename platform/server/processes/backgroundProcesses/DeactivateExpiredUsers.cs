using System;
using System.Collections.Generic;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class DeactivateExpiredUsersBackgroundProcess : BackgroundProcessBase {
		public override string Name { get; } = "DeactivateExpiredUsersBackgroundProcess";
		public override string LanguageVariable { get; } = "BG_DEACTIVATE_EXPIRED_USERS";
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();
		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session, Dictionary<string,object> parameters, string instance, int notificationId) {

			try {
				var cb = new ConnectionBase(instanceSchedule.Instance, session);

				// This returns +3 for every updated rows
				int updated = cb.db.ExecuteUpdateQuery(
					"UPDATE _" + instanceSchedule.Instance +
					"_user SET active = 0 WHERE active = 1 AND expire_date IS NOT NULL AND DATEADD(DAY, 1, expire_date) <= GETUTCDATE()",
					cb.DBParameters);

				Translations t = new Translations(instanceSchedule.Instance, session, session.locale_id);

				if (updated == 0) {
					WriteToLog(t.GetTranslation("BG_DEACTIVATE_EXPIRED_USERS_NOT_FOUND"));
					Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulNotAffected;
				} else if (updated == 3) {
					WriteToLog(t.GetTranslation("BG_DEACTIVATE_EXPIRED_USERS_FOUND"));
					Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				} else {
					WriteToLog((updated / 3) + t.GetTranslation("BG_DEACTIVATE_EXPIRED_USERS_FOUNDS"));
					Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				}

				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			return new List<BackgroundProcessConfigValue>();
		}
	}
}