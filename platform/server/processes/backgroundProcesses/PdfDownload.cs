using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class PdfDownloadBackgroundProcess : BackgroundProcessBase {
		public override string Name { get; } = "PdfDownload";
		public override string LanguageVariable { get; } = "PDF_DOWNLOAD";
		public override string FinalizeMessageBody { get; } = "DOWNLOAD_HERE1";
		public override string FinalizeMessageTitle { get; } = "DOWNLOAD_READY";

		public override int Download { get; set; } = 1;
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();

		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }

		public override int NotSchedulable { get; set; } = 1;

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session, Dictionary<string, object> parameters, string instance, int notificationId) {
			try {
				var ItemsPdf = new ItemsPdf();
				ItemsPdf.SetTemporarySession(session);
				var p = new ItemBase(instance, Conv.ToStr(parameters["Process"]), session);
				var portfolioId = Conv.ToInt(parameters["PortfolioId"]);
				var portfolioOptions = JsonConvert.DeserializeObject<PortfolioOptions>(Conv.ToStr(parameters["q"]));
				var data = p.FindItems(portfolioId, portfolioOptions, onlySelectorColumns: false);

				var pcs = new ProcessPortfolioColumns(instance, Conv.ToStr(parameters["Process"]), session);
				var cols2 = pcs.GetColumns(portfolioId);

				var colTypes = new Dictionary<string, int>();
				var cols = cols2.ToDictionary(c => c.ItemColumn.Name);

				var resultList = new List<Dictionary<string, object>>();
				var tags = ((DataTable)data["rowdata"]).Columns.Cast<DataColumn>().Select(it => it.ColumnName)
					.ToList();

				var attachedProcesses =
					new ProcessPortfolios(instance, Conv.ToStr(parameters["Process"]), session).GetAttachedProcesses(
						portfolioId, Conv.ToStr(parameters["Process"]));

				foreach (DataRow dr in ((DataTable)data["rowdata"]).Rows) {
					var resultDict = new Dictionary<string, object>();

					//Add Columns and Values of main process
					foreach (var col in tags) {
						if (cols.ContainsKey(col)) {
							var wordVal = p.SetWordCellValue(cols[col], dr[col], data, colTypes, session.locale_id);
							resultDict.Add(col, wordVal);
						}
						else {
							var wordVal = dr[col];
							resultDict.Add(col, wordVal);
						}
					}

					//Add Attached Processes to Templater Data
					foreach (var ap in attachedProcesses) {
						resultDict.Add(ap.Tag,
							ItemsPdf.GetAttachedProcess(ap.Process, ap.PortfolioId, Conv.ToInt(dr["item_id"]),
								ap.LinkColumnName, ap.Rows, ap.MatrixStructure));
					}

					resultList.Add(resultDict);
				}

				data["rowdata"] = resultList;
				data["selectedcells"] = ItemsPdf.pp;
				data.Add("colTypes", colTypes);

				if (portfolioOptions.portfolioPdfImageUpload != null) {
					foreach (var tag in portfolioOptions.portfolioPdfImageUpload) {
						if (tag.Key.EndsWith("-labels")) {
							for (var i = 0; i < portfolioOptions.portfolioPdfImageUpload[tag.Key].Count; i++) {
								portfolioOptions.portfolioPdfImageUpload[tag.Key][i] =
									p.ConvertHtmlToBase64(portfolioOptions.portfolioPdfImageUpload[tag.Key][i]);
							}
						}
					}

					data.Add("images", portfolioOptions.portfolioPdfImageUpload);
				}

				var reportArray =
					OfficeTemplater.CreateReport("portfolio", data, Conv.ToInt(parameters["AttachmentId"]),
						"pdf/portfolio", session);
				var cb = new ConnectionBase(instance, session);
				cb.SetDBParam(SqlDbType.Int, "@AttachmentId", Conv.ToInt(parameters["AttachmentId"]));
				string attDb;
				DataRow release = cb.db.GetDataRow("SELECT TOP 1 attachments_data_db FROM release_history ORDER BY installation_date DESC", cb.DBParameters);
				if (release != null && Conv.ToStr(release["attachments_data_db"]).Length > 0) {
					attDb = Conv.ToStr(release["attachments_data_db"]);
				}
				else {
					attDb = Conv.ToStr(cb.db.ExecuteScalar("SELECT DB_NAME()"));
				}
				var filename = Conv.ToStr(cb.db.ExecuteScalar("SELECT filename FROM " + Conv.ToSql(attDb) + ".dbo.attachments_data WHERE attachment_id = @AttachmentId", cb.DBParameters));
				string extension = "";
				string content_type;
				if (filename.EndsWith(".docx")) {
					if (!Conv.ToStr(parameters["FileName"]).EndsWith(".docx")) extension = ".docx";
					content_type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
				}
				else if (filename.EndsWith(".pptx")) {
					if (!Conv.ToStr(parameters["FileName"]).EndsWith(".pptx")) extension = ".pptx";
					content_type = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
				}
				else {
					if (!Conv.ToStr(parameters["FileName"]).EndsWith(".xlsx")) extension = ".xlsx";
					content_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				}

				var uploadForm = new Attachments.UploadForm() {
					name = Conv.ToStr(parameters["FileName"] + extension),
					controlName = "Pdf",
					itemId = Conv.ToStr(notificationId),
					itemColumnId = "",
					parentId = ""
				};

				var uploadFile = new Attachments.UploadFile() {
					FileBytes = reportArray,
					ContentType = content_type,
					Name = Conv.ToStr(parameters["FileName"] + extension),
					type = "file"
				};

				var a = new Attachments(instance, session);
				a.UploadFiles(uploadForm, uploadFile);

				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
			}
			catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
			return Result;
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance, AuthenticatedSession session) {
			var result = new List<BackgroundProcessConfigValue>();
			return result;
		}
	}
}