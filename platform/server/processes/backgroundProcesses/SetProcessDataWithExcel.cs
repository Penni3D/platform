using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.backgroundProcesses
{
	public class SetDataWithExcel : BackgroundProcessBase
	{
		public override string Name { get; } = "SetDataWithExcel";
		public override string LanguageVariable { get; } = "SET_DATA_WITH_EXCEL";

		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();
		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }
		public override string FinalizeMessageBody { get; } = "BG_ROWS_UPDATED";
		public override string FinalizeMessageTitle { get; } = "BG_ROWS_UPDATED";

		public override int NotSchedulable { get; set; }  = 1;

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session,
			Dictionary<string, object> parameters, string instance, int notificationId) {
			parameters["Instance"] = instance;

			try {
				var p = new ItemBase(Conv.ToStr(parameters["Instance"]), Conv.ToStr(parameters["Process"]), session);

				if (parameters.ContainsKey("Insert")) {
					p.ImportExcelInsert(Conv.ToInt(parameters["FileId"]));
				} else if (parameters.ContainsKey("Update")) {
					p.ImportExcelUpdate(Conv.ToInt(parameters["FileId"]));
				} else {
					p.ImportExcel(Conv.ToInt(parameters["FileId"]));
				}

				var attachments = new Attachments(instance, session);
				foreach (DataRow r in p.ProcessBase.db.GetDatatable("SELECT message FROM instance_logs WHERE file_id = " + Conv.ToSql(Conv.ToInt(parameters["FileId"]))).Rows) {
					Result.Description = Result.Description + Conv.ToStr(r["message"] + ". ");
				}

				p.ProcessBase.db.ExecuteDeleteQuery(
					"delete from instance_logs where file_id = " + Conv.ToSql(Conv.ToInt(parameters["FileId"])), p.ProcessBase.DBParameters);

				attachments.DeleteAttachment(Conv.ToInt(parameters["FileId"]));
				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;

				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			var result = new List<BackgroundProcessConfigValue>();
			return result;
		}
	}
}