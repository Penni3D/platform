using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.core.workTimeTracking;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.backgroundProcesses
{
	public class WorktimeExcelBg : BackgroundProcessBase
	{
		public override string Name { get; } = "WorktimeExcelBg";
		public override string LanguageVariable { get; } = "WORKTIME_EXCEL_BG";

		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();
		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }
		public override string FinalizeMessageBody { get; } = "DOWNLOAD_HERE1";
		public override string FinalizeMessageTitle { get; } = "DOWNLOAD_READY";

		public override int Download { get; set; } = 1;

		public override int NotSchedulable { get; set; } = 1;

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session,
			Dictionary<string, object> parameters, string instance, int notificationId) {

			parameters["Instance"] = instance;
			try {
				var excelDb = new WorktimeExcelDB(instance, Conv.ToStr(parameters["Process"]), session);

				var uploadForm = new Attachments.UploadForm() {
					name = "e",
					controlName = "Excel",
					itemId = Conv.ToStr(notificationId),
					itemColumnId = "",
					parentId = ""
				};

				var uploadFile = new Attachments.UploadFile() {
					FileBytes = !parameters.ContainsKey("Ids") ? excelDb.GetExel(Conv.ToDateTime(parameters["From"]), Conv.ToDateTime(parameters["To"]), Conv.ToInt(parameters["v"])) : excelDb.GetExel(Conv.ToDateTime(parameters["From"]), Conv.ToDateTime(parameters["To"]), Conv.ToInt(parameters["v"]), Conv.ToStr(parameters["Ids"]), parameters.ContainsKey("Pid") ? Conv.ToInt(parameters["Pid"]) : 0),
					ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
					Name = Conv.ToStr(parameters["FileName"] + ".xlsx"),
					type = "file"
				};

				var a = new Attachments(instance, session);
				a.UploadFiles(uploadForm, uploadFile);
				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			var result = new List<BackgroundProcessConfigValue>();
			return result;
		}
	}
}