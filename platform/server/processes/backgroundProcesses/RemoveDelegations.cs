using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class RemoveDelegationsBackgroundProcess : BackgroundProcessBase {
		public override string Name { get; } = "RemoveDelegationsBackgroundProcess";
		public override string LanguageVariable { get; } = "REMOVE_DELEGATIONS_BG";

		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();
		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }


		public override BackgroundProcessResult
			Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session, Dictionary<string,object> parameters, string instance, int notificationId) {

			try {
				var cb = new ConnectionBase(instanceSchedule.Instance, session);
				cb.SetDBParam(SqlDbType.NVarChar, "@instance", instanceSchedule.Instance);

				var delegatesColumnId = Conv.ToInt(cb.db.ExecuteScalar(
					"select item_column_id from item_columns where instance = @instance AND process = 'user' AND name = 'delegate_users'",
					cb.DBParameters));

				if (delegatesColumnId == 0) {
					Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
					Result.Description = "Item column 'delegate_users' could not be found from user process";
					return Result;
				}

				cb.SetDBParam(SqlDbType.Int, "@delegates_id", delegatesColumnId);
				var changes = false;
				foreach (DataRow row in cb.db
					.GetDatatable(
						"SELECT *, (SELECT DATEDIFF(day,(SELECT archive_start FROM item_data_process_selections where item_column_id = @delegates_id AND item_id = idps.item_id AND selected_item_id = idps.selected_item_id), GETDATE())) as daydif from item_data_process_selections idps where item_column_id = @delegates_id",
						cb.DBParameters).Rows) {
					if (Conv.ToInt(row["daydif"]) > 31) {
						cb.SetDBParam(SqlDbType.Int, "@item_id", Conv.ToInt(row["item_id"]));
						cb.SetDBParam(SqlDbType.Int, "@selected_item_id", Conv.ToInt(row["selected_item_id"]));

						cb.db.ExecuteDeleteQuery(
							"DELETE FROM item_data_process_selections WHERE item_id = @item_id AND selected_item_id = @selected_item_id AND item_column_id = @delegates_id",
							cb.DBParameters);
						WriteToLog("Delegation by item_id " + Conv.ToInt(row["item_id"]) + " removed from item_id " +
						           Conv.ToInt(row["selected_item_id"]));
						changes = true;
					}
				}

				if (!changes) {
					WriteToLog("There were no delegations to be removed");
					Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulNotAffected;
				} else {
					Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				}

				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			return new List<BackgroundProcessConfigValue>();
		}
	}
}