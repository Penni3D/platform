using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.actions;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class ExecuteActionsPortfolio : BackgroundProcessBase {
		public override string Name { get; } = "ExecuteActionsPortfolio";
		public override string LanguageVariable { get; } = "EXECUTEACTIONSPORTFOLIO";

		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();

		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }

		public override string FinalizeMessageTitle { get; } = "EXECUTEACTIONSPORTFOLIO_READY";
		public override string FinalizeMessageBody { get; } = "EXECUTEACTIONSPORTFOLIO_READY";


		public override BackgroundProcessResult
			Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session, Dictionary<string,object> parameters, string instance, int notificationId) {

			try {
				var jObject = JObject.Parse(instanceSchedule.ConfigOptions);
				var runner = 1;
				var p = new ItemBase(instanceSchedule.Instance, Conv.ToStr(jObject["@Process"]), session);
				var o = new PortfolioOptions();
				foreach (var collection in p.FindItems(Conv.ToInt(jObject["@ProcessPortfolioId"]), o)) {
					if (collection.Key != "rowdata")
						continue;
					var dt = (DataTable) collection.Value;
					foreach (DataRow row in dt.Rows) {
						var itemId = Conv.ToInt(row["item_id"]);
						var actions = new ProcessActions(instanceSchedule.Instance, Conv.ToStr(jObject["@Process"]),
							session);
						if (runner != dt.Rows.Count) {
							//Maybe this works. Only launch signaler and then invalidate cache etc on the last launched portfolio row
							actions.DoAction(Conv.ToInt(jObject["@ProcessActionId"]), itemId, runSignaler: false);
						} else {
							actions.DoAction(Conv.ToInt(jObject["@ProcessActionId"]), itemId, runSignaler: true);
						}

						runner++;
					}

					WriteToLog("Actions executed for portfolio id " + Conv.ToInt(jObject["@ProcessPortfolioId"]));
				}

				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				Result.Description = FinalizeMessageBody;
				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {

			var cb = new ConnectionBase(instance, session);

			cb.SetDBParam(SqlDbType.NVarChar, "@instance", instance);

			var processes = new List<Process>();
			var portfoliosByProcess = new Dictionary<string, List<ProcessPortfolio>>();
			var actionsByProcess = new Dictionary<string, List<ProcessAction>>();

			foreach (DataRow r in cb.db
				.GetDatatable(
					"SELECT * FROM processes WHERE instance = @instance", cb.DBParameters)
				.Rows) {
				processes.Add(new Process(r, session));
				portfoliosByProcess.Add(Conv.ToStr(r["process"]), new List<ProcessPortfolio>());
				actionsByProcess.Add(Conv.ToStr(r["process"]), new List<ProcessAction>());
			}

			foreach (DataRow r2 in cb.db
				.GetDatatable(
					"SELECT * FROM process_portfolios")
				.Rows) {
				portfoliosByProcess[Conv.ToStr(r2["process"])].Add(new ProcessPortfolio(r2, session));
			}

			foreach (DataRow r3 in cb.db
				.GetDatatable(
					"SELECT * FROM process_actions")
				.Rows) {
				actionsByProcess[Conv.ToStr(r3["process"])].Add(new ProcessAction(r3, session));
			}

			var result = new List<BackgroundProcessConfigValue>();

			var processName = new BackgroundProcessConfigValue("@Process", "string", BackgroundOptionType.List,
				"ProcessName", "Variable",
				processes, "BG_ACTIONEXECUTE_HELP1");
			result.Add(processName);

			var portfolioId = new BackgroundProcessConfigValue("@ProcessPortfolioId", "integer",
				BackgroundOptionType.List, "ProcessPortfolioId", "Variable",
				portfoliosByProcess, "BG_ACTIONEXECUTE_HELP2", true);
			result.Add(portfolioId);

			var actionId = new BackgroundProcessConfigValue("@ProcessActionId", "integer", BackgroundOptionType.List,
				"ProcessActionId", "Variable",
				actionsByProcess, "BG_ACTIONEXECUTE_HELP3", true);
			result.Add(actionId);

			return result;
		}
	}
}