using System;
using System.Collections.Generic;
using System.Data;
using System.DrawingCore;
using System.IO;
using System.Linq;
using System.Text;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class AttachmentsReduceImageDimensionsBackgroundProcess : BackgroundProcessBase {

		public override string Name { get; } = "AttachmentsReduceImageDimensionsBackgroundProcess";
		public override string LanguageVariable { get; } = "BACKGROUND_PROCESSES_ATTACHMENTSREDUCEIMAGEDIMENCIONS";
		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();

		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }
		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance, AuthenticatedSession session)
		{
			ConfigValues = new List<BackgroundProcessConfigValue>();
			return ConfigValues;
		}

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session, Dictionary<string,object> parameters, string instance, int notificationId) {
			StringBuilder make_log = new StringBuilder();

			if (Conv.ToInt(Config.GetValue("AttachmentSettings", "ImageMaxDimension")) > 0)
			{
				double image_dimension_max = Conv.ToDouble(Config.GetValue("AttachmentSettings", "ImageMaxDimension"));
				make_log.AppendLine("Server has image dimension limit of " + image_dimension_max);

				make_log.AppendLine("Begin search and handle attachments for oversized images. " + DateTime.UtcNow.ToString());
				var cb = new ConnectionBase(instanceSchedule.Instance, session);
				DataTable attachment_data = cb.db.GetDatatable("SELECT att_data.attachment_id, att_data.content_type, att_data.filesize FROM attachments_data att_data WHERE att_data.content_type LIKE 'image/%' "); // AND att_data.filesize > 524288
				foreach (DataRow att_data in attachment_data.Rows)
				{
					byte[] fileBytes = (byte[])cb.db.ExecuteScalar("SELECT filedata FROM attachments_data WHERE attachment_id = " + att_data["attachment_id"]);
					if (new string[] { "image/png", "image/x-png", "image/jpeg", "image/gif", "image/bmp", "image/tiff" }.Contains(Conv.ToStr(att_data["content_type"]).ToLower()))
					{
						Image uploaded_image = Image.FromStream(new MemoryStream(fileBytes));
						double width_overbounds = (image_dimension_max > 0 ? image_dimension_max / uploaded_image.Width : 1);
						double height_overbounds = (image_dimension_max > 0 ? image_dimension_max / uploaded_image.Height : 1);
						double smaller_overbounds = (width_overbounds < height_overbounds ? width_overbounds : height_overbounds);
						if (smaller_overbounds > 1.0) smaller_overbounds = 1;
						if (smaller_overbounds < 1.0)
						{
							make_log.Append("Image id [" + att_data["attachment_id"] + "] of type " + att_data["content_type"] + " with dimensions " + uploaded_image.Width + "x" + uploaded_image.Height + " (size " + att_data["filesize"] + ") ");
							int height_remade = Conv.ToInt(Math.Round(uploaded_image.Height * smaller_overbounds));
							int width_remade = Conv.ToInt(Math.Round(uploaded_image.Width * smaller_overbounds));

							// For the unknown reason first resized jpg will be corrupted - JKa 12.2.2021
							if (Conv.ToStr(att_data["content_type"]).ToLower() == "image/jpeg") Attachments.ResizeImage(uploaded_image, 1, 1, Conv.ToStr(att_data["content_type"]).ToLower());

							fileBytes = Attachments.ResizeImage(uploaded_image, width_remade, height_remade, Conv.ToStr(att_data["content_type"]).ToLower());
							cb.SetDBParam(SqlDbType.VarBinary, "@filedata", fileBytes);
							cb.SetDBParam(SqlDbType.Int, "@filesize", fileBytes.Length);
							cb.db.ExecuteUpdateQuery("UPDATE attachments_data SET filedata = @filedata, filesize = @filesize WHERE attachment_id = " + att_data["attachment_id"], cb.DBParameters);
							make_log.AppendLine("made to dimensions " + width_remade + "x" + height_remade + " (size " + fileBytes.Length + ") ");
						}
					}
				}
				make_log.AppendLine("Complete. " + DateTime.UtcNow.ToString());
			}
			else
				make_log.AppendLine("No configured max dimensions, did nothing");

			WriteToLog("Attachments Reduce Image Dimensions - check diagnostics for details");
			Diag.LogClient(session.instance, "Attachments Reduce Image Dimensions", make_log.ToString(), session.item_id, Diag.LogCategories.System, Diag.LogTypes.ServerMessage);
			Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
			return Result;
		}

	}
}