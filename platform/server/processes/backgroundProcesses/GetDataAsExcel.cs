using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class GetDataAsExcel : BackgroundProcessBase {
		public override string Name { get; } = "GetDataAsExcel";
		public override string LanguageVariable { get; } = "BG_EXCEL_IMPORT_EXPORT";

		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();
		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }
		public override string FinalizeMessageBody { get; } = "DOWNLOAD_HERE1";
		public override string FinalizeMessageTitle { get; } = "DOWNLOAD_READY";

		public override int Download { get; set; } = 1;

		public override int NotSchedulable { get; set; }  = 1;

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session,
			Dictionary<string, object> parameters, string instance, int notificationId) {
			parameters["Instance"] = instance;

			try {
				var p = new ItemBase(Conv.ToStr(parameters["Instance"]), Conv.ToStr(parameters["Process"]),
					session);
				var portfolioId = Conv.ToInt(parameters["PortfolioId"]);

				byte[] excel;

				if (parameters.ContainsKey("Mode") && Conv.ToStr(parameters["Mode"]) == "ResourcesExcel") {
					var process = Conv.ToStr(parameters["Process"]);
					var dateStart = Conv.ToStr(parameters["DateStart"]);
					var dateEnd = Conv.ToStr(parameters["DateEnd"]);
					var precision = Conv.ToInt(parameters["Precision"]);
					var filter = JsonConvert.DeserializeObject<PortfolioOptions>(Conv.ToStr(parameters["Filters"]));
					var additionalFields = parameters.ContainsKey("AdditionalFields") ? JsonConvert.DeserializeObject<List<int>>(Conv.ToStr(parameters["AdditionalFields"])) : new List<int>();
					var resolution = Conv.ToStr(parameters["Resolution"]);
					var ids = JsonConvert.DeserializeObject<List<int>>(Conv.ToStr(parameters["Ids"]));
					excel = process == "user"
						? p.GetResourceUserExcel(portfolioId, dateStart, dateEnd, precision, filter, additionalFields, resolution, ids)
						: p.GetResourceProcessExcel(process, portfolioId, dateStart, dateEnd, precision, filter, additionalFields, resolution, ids);
				} else {
					if (parameters.ContainsKey("Mode") && Conv.ToStr(parameters["Mode"]) == "CSV") {
						bool isFromPortfolio = parameters.ContainsKey("FromPortfolio");

						var columns = parameters.ContainsKey("ShownColumns")
							? ((Newtonsoft.Json.Linq.JArray)parameters["ShownColumns"]).ToObject<List<int>>()
							: new List<int>();

						var portfolioOptions = isFromPortfolio
							? JsonConvert.DeserializeObject<PortfolioOptions>(Conv.ToStr(parameters["q"]))
							: new PortfolioOptions();

						excel = Conv.ToBool(parameters["StrictMode"])
							? p.PortfolioCsv(portfolioId, portfolioOptions, columns)
							: p.GetCsvExportTemplateWithData(columns, isFromPortfolio, portfolioId, portfolioOptions);

						var csvUploadForm = new Attachments.UploadForm() {
							name = Conv.ToStr(parameters["FileName"] + ".csv"),
							controlName = "CSV",
							itemId = Conv.ToStr(notificationId),
							itemColumnId = "",
							parentId = ""
						};

						var csvUploadFile = new Attachments.UploadFile() {
							FileBytes = excel,
							ContentType = "text/csv",
							Name = Conv.ToStr(parameters["FileName"] + ".csv"),
							type = "file"
						};

						var csv_a = new Attachments(instance, session);
						csv_a.UploadFiles(csvUploadForm, csvUploadFile);

						Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
						return Result;
					} else {
						bool isFromPortfolio = parameters.ContainsKey("FromPortfolio");

						var columns = parameters.ContainsKey("ShownColumns")
							? ((Newtonsoft.Json.Linq.JArray)parameters["ShownColumns"]).ToObject<List<int>>()
							: new List<int>();

						var portfolioOptions = isFromPortfolio
							? JsonConvert.DeserializeObject<PortfolioOptions>(Conv.ToStr(parameters["q"]))
							: new PortfolioOptions();

						excel = Conv.ToBool(parameters["StrictMode"])
							? p.PortfolioExcel(portfolioId, portfolioOptions, columns)
							: p.GetExcelExportTemplateWithData(columns, isFromPortfolio, portfolioId, portfolioOptions);
					}
				}

				var uploadForm = new Attachments.UploadForm() {
					name = Conv.ToStr(parameters["FileName"] + ".xlsx"),
					controlName = "Excel",
					itemId = Conv.ToStr(notificationId),
					itemColumnId = "",
					parentId = ""
				};

				var uploadFile = new Attachments.UploadFile() {
					FileBytes = excel,
					ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
					Name = Conv.ToStr(parameters["FileName"] + ".xlsx"),
					type = "file"
				};

				var a = new Attachments(instance, session);
				a.UploadFiles(uploadForm, uploadFile);

				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			var result = new List<BackgroundProcessConfigValue>();
			return result;
		}
	}
}