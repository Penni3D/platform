using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	public class GetExcelWithClientData : BackgroundProcessBase {
		public override string Name { get; } = "GetExcelWithClientData";
		public override string LanguageVariable { get; } = "BG_EXCEL_CLIENT_DATA";

		public override BackgroundProcessResult Result { get; } = new BackgroundProcessResult();
		public override List<BackgroundProcessConfigValue> ConfigValues { get; set; }
		public override string FinalizeMessageBody { get; } = "DOWNLOAD_HERE1";
		public override string FinalizeMessageTitle { get; } = "DOWNLOAD_READY";

		public override int Download { get; set; } = 1;

		public override int NotSchedulable { get; set; } = 1;

		public override BackgroundProcessResult Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session,
			Dictionary<string, object> parameters, string instance, int notificationId) {
			try {
				var e = (JObject) parameters["Excel"];
				var j = e.ToObject<Excel>();
				var uploadForm = new Attachments.UploadForm() {
					name = Conv.ToStr(parameters["FileName"] + ".xlsx"),
					controlName = "Excel",
					itemId = Conv.ToStr(notificationId),
					itemColumnId = "",
					parentId = ""
				};

				var uploadFile = new Attachments.UploadFile() {
					FileBytes = j.GetExcel().GetAsByteArray(),
					ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
					Name = Conv.ToStr(parameters["FileName"] + ".xlsx"),
					type = "file"
				};

				var a = new Attachments(instance, session);
				a.UploadFiles(uploadForm, uploadFile);

				Result.Outcome = BackgroundProcessResult.Outcomes.SuccessfulAndAffected;
				return Result;
			} catch (Exception e) {
				WriteToLog(e);
				Result.Outcome = BackgroundProcessResult.Outcomes.Failed;
				return Result;
			}
		}

		public override List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			var result = new List<BackgroundProcessConfigValue>();
			return result;
		}
	}
}