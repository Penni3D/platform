﻿using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.backgroundProcesses {
	[Route("v1/settings/[controller]")]
	public class RegisteredBackgroundProcesses : RestBase {
		private static readonly Dictionary<string, BackgroundProcessBase> bgs =
			new Dictionary<string, BackgroundProcessBase>();

		//private AuthenticatedSession _authenticated;

		[HttpGet("")]
		public Dictionary<string, BackgroundProcessBase> GetHttpBackgroundProcesses() {
			foreach (var bg in bgs) {
				bg.Value.ConfigValues = bg.Value.GetConfigValues(instance, currentSession);
			}

			return bgs;
		}

		public static void AddType(BackgroundProcessBase obj) {
			bgs.Add(obj.Name, obj);
		}

		public static BackgroundProcessBase GetBackgroundProcess(string name) {
			return bgs.ContainsKey(name) ? bgs[name] : null;
		}
	}

	public abstract class BackgroundProcessBase {
		public abstract string Name { get; }
		public abstract string LanguageVariable { get; }

		public virtual string FinalizeMessageName { get; }
		public virtual string FinalizeMessageTitle { get; }
		public virtual string FinalizeMessageBody { get; }
		public string NickName = "";

		public virtual bool OnFinish { get; set; }

		public int LogId { get; set; }

		public string BaseLink;

		public virtual int Download { get; set; }

		public virtual int NotSchedulable { get; set; }

		public abstract BackgroundProcessResult
			Execute(InstanceSchedule instanceSchedule, AuthenticatedSession session,
				Dictionary<string, object> parameters = null, string instance = "", int notificationId = 0);

		public abstract BackgroundProcessResult Result { get; }

		public virtual List<BackgroundProcessConfigValue> GetConfigValues(string instance,
			AuthenticatedSession session) {
			return new List<BackgroundProcessConfigValue>();
		}

		public abstract List<BackgroundProcessConfigValue> ConfigValues { get; set; }



		public void WriteToLog(string message, bool error = false) {
			Diag.Log("?", NickName != "" ? NickName + ": " + message : message, 0,
				error ? Diag.LogSeverities.Error : Diag.LogSeverities.SuccessMessage,
				Diag.LogCategories.BackgroundTask, LogId);
		}

		public void WriteToLog(Exception ex) {
			Diag.Log("?", ex, 0, Diag.LogCategories.BackgroundTask, Diag.LogTypes.ServerException);
		}
	}

	public class BackgroundProcessResult {
		public string Description { get; set; }

		public Outcomes Outcome { get; set; }

		public enum Outcomes {
			Pending = 0,
			SuccessfulAndAffected = 1,
			SuccessfulNotAffected = 2,
			Failed = 3
		}
	}

	public class BackgroundProcessConfigValue {
		public string Name;
		public string OptionTranslation;
		public BackgroundOptionType OptionType;
		public string OptionValue;
		public object Value;
		public object Values;
		public string ValueType;
		public string HelpText;
		public bool UseFilter;
		public bool IgnoreRichText;

		public BackgroundProcessConfigValue(string name, string valueType, BackgroundOptionType optionType,
			string optionValue,
			string optionTranslation,
			object values = null, string helpText = "", bool useFilter = false, bool ignoreRichText = false) {
			Name = name;
			ValueType = valueType;
			OptionType = optionType;
			OptionValue = optionValue;
			OptionTranslation = optionTranslation;
			Values = values;
			Value = null;
			HelpText = helpText;
			UseFilter = useFilter;
			IgnoreRichText = ignoreRichText;
		}
	}

	public enum BackgroundOptionType {
		Nvarchar = 0,
		Int = 1,
		Float = 2,
		Date = 3,
		Text = 4,
		Process = 5,
		List = 6,
		MultiSelectList = 7,
		Translation = 8
	}
}