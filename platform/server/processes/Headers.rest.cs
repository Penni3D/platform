﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/core/[controller]")]
	public class Header : RestBase {
		[HttpGet("{process}")]
		public string Get(string process) {
			var itemColumns = new ItemColumns(instance, process, currentSession);
			var ret = "<table class='table'><thead><tr>";
			var cols = itemColumns.GetItemColumns();
			foreach (var col in cols) {
				ret += "<th class='name' translate=\"" + col.Variable + "\"></th>";
			}
			ret += "<th class='controls'><ng-md-icon icon='add' size='24' ng-click='addItem(subprocess.Process.ProcessName, subprocess.ProcessTabSubProcessId)'></ng-md-icon></th>";
			ret += "</tr></thead>";
			ret += "<tbody><tr ng-repeat='item in items | filter:searchText'>";
			foreach (var col in cols) {
				if (col.Name == "list_item") {
					ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item." + col.Name + " | translate}}</td>";
				} else if (col.InputMethod == 1 && col.InputName == "SYMBOLS") {
					ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'><md-icon md-svg-icon='{{item." + col.Name + "}}' style='fill: {{item." + col.Name + "_fill}};'></md-icon>{{item." + col.Name + " | translate}}</td>";
				} else {
					ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item." + col.Name + "}}</td>";
				}
			}
			ret += "<td class='controls'><ng-md-icon icon='delete' size='24' ng-click='deleteItem(item)'></ng-md-icon></td>";
			ret += "</tr> </tbody></table>";
			return ret;
		}

		[HttpGet("{process}/{portfolioId}")]
		public string Get(string process, int portfolioId) {
			var pColumns = new ProcessPortfolioColumns(instance, process, currentSession);
			var ret = "<table class='table' style='width: 100%'><thead><tr>";
			var cols = pColumns.GetColumns(portfolioId);
			foreach (var col in cols) {
				ret += "<th translate=\"" + col.ItemColumn.Variable + "\"></th>";
			}
			ret += "<th class='controls'><ng-md-icon icon='add' size='24' ng-click='addItem(subprocess.Process.ProcessName, subprocess.ProcessTabSubProcessId)'></ng-md-icon></th>";
			ret += "</tr></thead>";
			ret += "<tbody><tr ng-repeat='item in items | filter:searchText'>";
			ret += "<td> <md-checkbox ng-model='item.checked' ng-change='onSelect(item)' aria-label='selectedItems'></md-checkbox></td>";
			foreach (var col in cols) {
				if (col.ItemColumn.IsList()) {
					ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item." + col.ItemColumn.Name + " | translate}}</td>";
				} else {
					ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item." + col.ItemColumn.Name + "}}</td>";
				}
			}
			ret += "<td class='controls'><ng-md-icon icon='delete' size='24' ng-click='deleteItem(item)'></ng-md-icon></td>";
			ret += "</tr> </tbody></table>";
			return ret;
		}

		[HttpGet("{process}/{portfolioType}/{portfolioId}")]
		public string Get(string process, int portfolioType, int portfolioId) {
			var pColumns = new ProcessPortfolioColumns(instance, process, currentSession);
			var ret = "";
			ret += "<md-button class='md-button' ng-click='prevPage();'> < </md-button>";
			ret += " <md-button  ng-repeat='page in pages' ng-click='setPage(page.pageNo);'> {{page.pageNo + 1}} </md-button>";
			ret += "<md-button class='md-button' ng-click='nextPage();'> > </md-button><br>";

			ret += "<table class='table' style='width: 100%'><thead><tr>";
			var cols = pColumns.GetColumns(portfolioId);
			if (portfolioType == 1) {
				ret += "<th></th>";
			}
			foreach (var col in cols) {
				ret += "<th translate=\"" + col.ItemColumn.Variable + "\"></th>";
			}
			if (portfolioType == 0) {
				ret += "<th class='controls'><ng-md-icon icon='add' size='24' ng-click='addItem(subprocess.Process.ProcessName, subprocess.ProcessTabSubProcessId)'></ng-md-icon></th>";
			}
			ret += "</tr></thead>";

			ret += "<tbody><tr ng-repeat='item in items | filter:searchText'>";
			if (portfolioType == 1) {
				ret += "<td> <md-checkbox ng-model='item.checked' ng-change='onSelect(item)' aria-label='selectedItems'></md-checkbox></td>";
			}
			foreach (var col in cols) {
				//ret += "<td style='width: " + 1000 / (cols.Count) + "px;' ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item." + name + "}}</td>";
				if (portfolioType == 1) {
					if (col.ItemColumn.IsList()) {
						ret += "<td ng-click='selectItem(item);'>{{item." + col.ItemColumn.Name + "}}</td>";
					} else {
						ret += "<td ng-click='selectItem(item);'>{{item." + col.ItemColumn.Name + " | translate}}</td>";
					}
				} else {
					if (col.ItemColumn.IsList()) {
						switch (col.ShowType) {
							case 0:
								ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item.list_" + col.ItemColumn.ItemColumnId + "_list_item}}</td>";
								break;
							case 1:
								ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>";
								ret += "<md-icon md-svg-icon='{{item.list_" + col.ItemColumn.ItemColumnId + "_list_symbol}}' style='fill: {{item.list_" + col.ItemColumn.ItemColumnId + "_list_symbol_fill}}'></md-icon>";
								ret += "{{item.list_" + col.ItemColumn.ItemColumnId + "_list_item}}";
								ret += "</td>";
								break;
							case 2:
								ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>";
								ret += "<md-icon md-svg-icon='{{item.list_" + col.ItemColumn.ItemColumnId + "_list_symbol}}' style='fill: {{item.list_" + col.ItemColumn.ItemColumnId + "_list_symbol_fill}}'>";
								ret += "</td>";
								break;
							default:
								ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item.list_" + col.ItemColumn.ItemColumnId + "_list_item}}</td>";
								break;
						}
					} else {
						ret += "<td ng-click='editItem(item.process, item.item_id, item.processTabSubProcessId);'>{{item." + col.ItemColumn.Name + "}}</td>";
					}
				}
			}
			//ret += "<td style='width: 100px;' ><ng-md-icon icon='delete' size='24' ng-click='deleteItem(item.process, item.item_id)'></ng-md-icon></td>";
			if (portfolioType == 0) {
				ret += "<td class='controls'><ng-md-icon icon='delete' size='24' ng-click='deleteItem(item)'></ng-md-icon></td>";
			}
			ret += "</tr> </tbody></table>";
			return ret;
		}
	}
}