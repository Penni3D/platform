﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes {
	public class ProcessBaseline : ProcessBase {
		public ProcessBaseline(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance,
			process, authenticatedSession) { }

		public List<Dictionary<string, object>> GetBaselineDates(int itemId, bool groupPhases = false, int type = 0) {
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			if (groupPhases && (type == 0 || type == 1)) {
				return db.GetDatatableDictionary("SELECT max(process_baseline_id) as process_baseline_id, " +
				                                 " max(instance) as instance, " +
				                                 " max(process) as process, " +
				                                 " max(process_item_id) as process_item_id, " +
				                                 " max(process_group_id) as process_group_id, " +
				                                 " max(baseline_date) as baseline_date, " +
				                                 " max(creator_user_item_id) as creator_user_item_id, " +
				                                 " max(description) as description, " +
				                                 " max(archive_userid) as archive_userid, " +
				                                 " max(archive_start) as archive_start " +
				                                 " from process_baseline WHERE process_item_id = @itemId AND process_group_id > 0 GROUP BY 	CONVERT(nvarchar(max), baseline_date, 100)" +
				                                 " UNION ALL SELECT process_baseline_id, instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id, description, archive_userid, archive_start  from process_baseline WHERE process_item_id = @itemId AND process_group_id IS NULL ", DBParameters);
			}

			if (type == 2) {
				return db.GetDatatableDictionary("SELECT * from process_baseline WHERE process_item_id = @itemId AND process_group_id IS NULL ", DBParameters);
			}

			return db.GetDatatableDictionary("SELECT * from process_baseline WHERE process_item_id = @itemId", DBParameters);
		}

		public Dictionary<string, object> AddBaseline(int itemId, string desc) {

			var retval = new Dictionary<string, object>();
			
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.NVarChar, "@desc", desc);
			var utcNow = DateTime.UtcNow;
			SetDBParam(SqlDbType.DateTime, "@baselineDate", utcNow);

			var baselineId = db.ExecuteInsertQuery(
				"INSERT INTO process_baseline (instance, process, baseline_date, creator_user_item_id, process_item_id, description) VALUES (@instance, @process, @baselineDate, @signedInUserId, @itemId, @desc) ",
				DBParameters);

			retval.Add("baselineId", baselineId);
			retval.Add("baselineDate", utcNow);

			var ib = new ItemBase(instance, process, authenticatedSession);
			retval.Add("data", ib.GetArchiveItem(itemId, utcNow));

			return retval;
		}

		public void DeleteBaseline(int baselineId) {
			SetDBParam(SqlDbType.Int, "@baselineId", baselineId);

			db.ExecuteInsertQuery(
				"DELETE FROM process_baseline WHERE process_baseline_id = @baselineId ",
				DBParameters);			
		}
	}
}