﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/features/ProcessBaseline")]
	public class ProcessBaselineRest : PageBase {
		public ProcessBaselineRest() : base("settings") { }

		[HttpGet("baselineDates/{process}/{itemId}")]
		public List<Dictionary<string, object>> GetBaselineDates(string process, int itemId) {
			var processBaseline = new ProcessBaseline(instance, process, currentSession);
			return processBaseline.GetBaselineDates(itemId);
		}

		[HttpPost("addBaseline/{process}/{itemId}")]
		public Dictionary<string, object> GetBaselineDates(string process, int itemId,
			[FromBody] Dictionary<string, object> data) {
			var processBaseline = new ProcessBaseline(instance, process, currentSession);
			return processBaseline.AddBaseline(itemId, Conv.ToStr(data["description"]));
		}

		[HttpDelete("{process}/{baselineId}")]
		public void DeleteBaseline(string process, int baselineId) {
			var processBaseline = new ProcessBaseline(instance, process, currentSession);
			processBaseline.DeleteBaseline(baselineId);
		}
	}
}