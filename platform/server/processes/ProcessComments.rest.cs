﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/meta/[controller]")]
	public class ProcessComments : RestBase {
		[HttpGet("{identifier}")]
		public Dictionary<string, object> Get(string identifier) {
			var pc = new Comments(instance, process, currentSession);
			return pc.GetComments(identifier);
		}

		[HttpPost("{identifier}")]
		public Comment AddComment(string identifier, [FromBody] Comment comment) {
			var pc = new Comments(instance, "", currentSession);
			return pc.PostComment(identifier, comment.Data);
		}

		[HttpDelete("{identifier}/{commentId}")]
		public int RemoveComment(string identifier, int commentId) {
			var pc = new Comments(instance, process, currentSession);
			return pc.DeleteComment(identifier, commentId);
		}

		[HttpGet("{process}/{itemId}/{identifier}")]
		public Dictionary<string, object> Get(string process, int itemId, string identifier) {
			var pc = new Comments(instance, process, currentSession);
			return pc.GetComments(process, itemId, identifier);
		}

		[HttpPost("{process}/{itemId}/{identifier}")]
		public Comment AddComment(string process, int itemId, string identifier, [FromBody] Comment comment) {
			var pc = new Comments(instance, process, currentSession);
			return pc.PostComment(process, itemId, identifier, comment.Data);
		}

		[HttpDelete("{process}/{itemId}/{identifier}/{commentId}")]
		public int RemoveComment(string process, int itemId, string identifier, int commentId) {
			var pc = new Comments(instance, process, currentSession);
			return pc.DeleteComment(process, itemId, identifier, commentId);
		}
	}
}