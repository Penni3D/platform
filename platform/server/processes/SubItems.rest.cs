﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.layout;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/settings/SubItems")]
	public class SubItemsRest : PageBase {
		public SubItemsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{subProcess}/{parentItemId}/{processTabSubProcessId}")]
		public IEnumerable<Dictionary<string, object>> Get(string subProcess, int parentItemId, int processTabSubProcessId) {
			var p = new ItemCollectionService(instance, subProcess, currentSession);
			p.FillSubItems(parentItemId, processTabSubProcessId);
			return p.GetItems();
		}

		[HttpGet("dummy/{subProcess}")]
		public IEnumerable<Dictionary<string, object>> Get(string subProcess) {
			var p = new ItemCollectionService(instance, subProcess, currentSession);
			p.FillDummySubItems();
			return p.GetItems();
		}

		// POST model/values
		/*[HttpPost]
        public void Post([FromBody]ProcessContainerField[] fields)
        {
			//ContainerField[] s = value;
			Record r = new Record("keto", "user");			
			r.SaveData(currentSession.item_id , fields);
        }*/

		// PUT model/values/5
		[HttpPut("{itemId}")]
		public void Put(int itemId, [FromBody]ProcessContainerField value) {
			CheckRight("write");
			var i = itemId;
			var s = value;
			i++;
		}

		// DELETE model/values/5
		[HttpDelete("{process}/{itemId}")]
		public void Delete(string process, int itemId) {
			CheckRight("delete");
			var ib = new ItemBase(instance, process, currentSession);
			ib.Delete(itemId);
		}
	}
}