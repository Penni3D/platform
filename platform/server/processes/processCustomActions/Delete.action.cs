using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;

namespace Keto5.x.platform.server.processes.processCustomActions {
	public class CustomActionDeleteActions : CustomActionBase {
		public override string Name { get; } = "DeleteActions";

		public override void DoCustomAction(int processActionRowId, int itemId, string value2, string value3) {
			var ib = new ItemBase(instance, Cache.GetItemProcess(itemId, instance), session);
			ib.Delete(itemId);
		}

		public override Dictionary<int, List<Dictionary<string, object>>> GetCustomActionOptions(string instance, int userItemId) {
			return null;
		}
	}
}