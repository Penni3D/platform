using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.database;

namespace Keto5.x.platform.server.processes.processCustomActions {
	public class CustomActionAllocationActions : CustomActionBase {
		public override string Name { get; } = "AllocationActions";

		public override void DoCustomAction(int processActionRowId, int itemId, string value2, string value3) {
			var cb = new ConnectionBase(instance, session);
			cb.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			if (value2 == "0") {
				//Delete allocations from TODAY onwards
				var sql1 = "DELETE FROM allocation_durations WHERE allocation_item_id IN (SELECT item_id FROM _" +
				           instance +
				           "_allocation WHERE process_item_id = @itemId) AND date > GETUTCDATE()";
				cb.db.ExecuteDeleteQuery(sql1, cb.DBParameters);

				//Change existing requests back to draft
				var sql2 = "UPDATE _" + instance +
				           "_allocation SET status = 10 WHERE process_item_id = @itemId AND status = 20";
				cb.db.ExecuteDeleteQuery(sql2, cb.DBParameters);
			} else if (value2 == "1") {
				cb.db.ExecuteUpdateQuery(
					"UPDATE _" + instance + "_allocation SET status = 70 WHERE process_item_id = @itemId",
					cb.DBParameters);
			} else if (value2 == "2") {
				//Delete from TODAY onwards for approved allocations
				var sql1 = "DELETE FROM allocation_durations WHERE allocation_item_id IN (SELECT item_id FROM _" +
				           instance +
				           "_allocation WHERE resource_item_id = @itemId AND status IN (13,40,50,53)) AND date > GETUTCDATE()";
				cb.db.ExecuteDeleteQuery(sql1, cb.DBParameters);
				
				//Release draft and requested allocations
				cb.db.ExecuteUpdateQuery(
					"UPDATE _" + instance + "_allocation SET status = 70 WHERE status IN (10,20) AND resource_item_id = @itemId",
					cb.DBParameters);
			}
		}

		public override Dictionary<int, List<Dictionary<string, object>>> GetCustomActionOptions(string instance,
			int userItemId) {
			var options1 = new List<Dictionary<string, object>> {
				new Dictionary<string, object> {
					{"Variable", "ALLOCATION_ACTION_DELETE"},
					{"Value", 0}
				},
				new Dictionary<string, object> {
					{"Variable", "ALLOCATION_ACTION_RELEASE"},
					{"Value", 1}
				},
				new Dictionary<string, object> {
					{"Variable", "ALLOCATION_ACTION_RELEASE_USER"},
					{"Value", 2}
				}
			};

			return new Dictionary<int, List<Dictionary<string, object>>>
				{{1, options1}, {2, new List<Dictionary<string, object>>()}};
		}
	}
}