using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.processCustomActions {
	public class CustomActionKanbanActions : CustomActionBase {
		public override string Name { get; } = "KanbanActions";

		public override void DoCustomAction(int processActionRowId, int itemId, string value2, string value3) {
			var cb = new ConnectionBase(instance, session);
			cb.SetDBParam(SqlDbType.Int, "@item_id", itemId);
			cb.SetDBParam(SqlDbType.Int, "@status_list_item_id", Conv.ToInt(value2));
			var sql = " UPDATE _" + instance + "_task SET status_id = ( " + 
			          "       SELECT status_id FROM task_status WHERE parent_item_id IN (SELECT parent_item_id FROM _" + instance + "_task WHERE item_id = @item_id) AND status_list_item_id = @status_list_item_id " + 
			          " ) " + 
			          " WHERE item_id IN ( " + 
			          "       SELECT item_id FROM _" + instance + "_task WHERE parent_item_id = @item_id " + 
			          " ) ";
			cb.db.ExecuteUpdateQuery(sql, cb.DBParameters);
		}

		public override Dictionary<int, List<Dictionary<string, object>>> GetCustomActionOptions(string instance, int userItemId) {
			var cb = new ConnectionBase(instance, new AuthenticatedSession(instance, userItemId, null));
			var options1 = cb.db.GetDatatableDictionary("SELECT item_id AS Value, list_item AS Variable FROM _" + instance +
			                             "_list_task_status");

			return new Dictionary<int, List<Dictionary<string, object>>>
				{{1, options1}};
		}
	}
}