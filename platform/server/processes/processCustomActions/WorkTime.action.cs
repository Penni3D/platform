using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;

namespace Keto5.x.platform.server.processes.processCustomActions {
	public class CustomActionWorkTimeActions : CustomActionBase {
		public override string Name { get; } = "RemoveFromTimeSheets";

		public override void DoCustomAction(int processActionRowId, int itemId, string value2, string value3) {
			var cb = new ConnectionBase(instance, session);
			cb.SetDBParam(SqlDbType.Int, "@item_id", itemId);
			var sql = "DELETE FROM worktime_tracking_users_tasks WHERE task_item_id IN (" +
			          "SELECT item_id FROM _" + instance + "_task t WHERE owner_item_id = @item_id)";
			cb.db.ExecuteDeleteQuery(sql, cb.DBParameters);
		}

		public override Dictionary<int, List<Dictionary<string, object>>> GetCustomActionOptions(string instance,
			int userItemId) {
			return null;
		}
	}
}