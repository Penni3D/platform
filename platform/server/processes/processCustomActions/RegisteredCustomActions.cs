﻿using System.Collections.Generic;
using System.IO;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.processCustomActions {
	[Route("v1/settings/[controller]")]
	public class RegisteredCustomActions : RestBase {
		private static readonly Dictionary<string, CustomActionBase> customActions =
			new Dictionary<string, CustomActionBase>();

		[HttpGet("")]
		public Dictionary<string, Dictionary<int, List<Dictionary<string, object>>>> GetProcessCustomActions() {
			var p = new Dictionary<string, Dictionary<int, List<Dictionary<string, object>>>>();
			foreach (var value in customActions.Keys) {
				p.Add(value, customActions[value].GetCustomActionOptions(instance, currentSession.item_id));
			}

			return p;
		}

		public static void AddType(CustomActionBase obj) {
			customActions.Add(obj.Name, obj);
		}

		public static CustomActionBase GetCustomAction(string actionType) {
			if (customActions.ContainsKey(actionType)) {
				return customActions[actionType];
			}

			return null;
		}
	}

	public abstract class CustomActionBase {
		public abstract string Name { get; }

		public string instance { get; set; }
		public AuthenticatedSession session { get; set; }
		public abstract void DoCustomAction(int processActionRowId, int itemId, string value2, string value3);
		public abstract Dictionary<int, List<Dictionary<string, object>>> GetCustomActionOptions(string instance, int userItemId);
	}

	public class CustomActionExecutableLauncher : CustomActionBase {
		public override string Name { get; } = "ExecutableLauncher";

		public override void DoCustomAction(int processActionRowId, int itemId, string value2, string value3) {
			var executables =
				JsonConvert.DeserializeObject<Dictionary<string, object>>(
					Config.Get("customexecutablepaths").ToString());
			if (executables.ContainsKey(value2)) {
				var executable =
					JsonConvert.DeserializeObject<Dictionary<string, string>>(executables[value2].ToString());
				if (File.Exists(executable["value"])) {
					var args = "";
					if (executable.ContainsKey("arguments")) {
						args += executable["arguments"] + " ";
					}

					var p = new System.Diagnostics.Process {
						StartInfo = {
							FileName = executable["value"],
							WorkingDirectory = Path.GetDirectoryName(executable["value"]),
							Arguments = args + itemId,
							CreateNoWindow = true
						}
					};

					p.Start();

					if (Conv.ToInt(value3) != 1) {
						p.WaitForExit();
					}
				} else {
					throw new CustomArgumentException("FILE_MISSING");
				}
			} else {
				throw new CustomArgumentException("CONFIG_MISSING");
			}
		}

		public override Dictionary<int, List<Dictionary<string, object>>> GetCustomActionOptions(string instance, int userItemId) {
			var options1 = new List<Dictionary<string, object>>();

			var executables =
				JsonConvert.DeserializeObject<Dictionary<string, object>>(
					Config.Get("customexecutablepaths").ToString());
			foreach (var exe in executables.Keys) {
				options1.Add(new Dictionary<string, object> {
					{"Variable", exe},
					{"Value", exe}
				});
			}

			var options2 = new List<Dictionary<string, object>> {
				new Dictionary<string, object> {
					{"Variable", "WAIT_FOR_COMPLETION"},
					{"Value", 0}
				},
				new Dictionary<string, object> {
					{"Variable", "DO_NOT_WAIT_FOR_COMPLETION"},
					{"Value", 1}
				}
			};

			return new Dictionary<int, List<Dictionary<string, object>>> {{1, options1}, {2, options2}};
		}
	}
}