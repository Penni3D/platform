using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.processCustomActions {
	public class CustomActionInviteUsers : CustomActionBase {
		public override string Name { get; } = "InviteUsersAction";

		public override void DoCustomAction(int processActionRowId, int itemId, string value2, string value3) {
			new Authentication(instance).SendResetPassword(itemId,
				"https://" + Conv.ToStr(this.session.GetMember("wwwpath")).Replace("//", "/"), false);
		}

		public override Dictionary<int, List<Dictionary<string, object>>> GetCustomActionOptions(string instance,
			int userItemId) {
			return null;
		}
	}
}