﻿using System.Collections.Generic;

namespace Keto5.x.platform.server.processes.portfolios {
	public class ProcessPortfolioFilter {
		public ProcessPortfolioFilter(ProcessPortfolioColumn pPC, List<Dictionary<string, object>> mItems) {
			PortfolioColumn = pPC;
			MenuItems = mItems;
		}

		public ProcessPortfolioColumn PortfolioColumn { get; set; }
		public List<Dictionary<string, object>> MenuItems { get; set; }
	}
}