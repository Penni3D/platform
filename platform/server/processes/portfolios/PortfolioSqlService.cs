﻿using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Microsoft.Graph;

namespace Keto5.x.platform.server.processes.portfolios {
	public class PortfolioSqls {
		public PortfolioSqls() {
			cols = new List<string>();
			sqlcols = new List<string>();
			sumcols = new List<string>();
			UseInSelectSumcols = new List<string>();
			joins = new List<string>();
			searchs = new List<string>();
			archiveCols = new List<int>();
			archiveJoins = new List<string>();
			statusColumns = new List<string>();
			processColumns = new List<ItemColumn>();
			sqCols = new List<ItemColumn>();
			transColumns = new List<ItemColumn>();
			ItemColumn milestoneColumn = null;
			equationColumns = new List<ItemColumn>();
		}

		public List<string> cols { get; set; }
		public List<string> sqlcols { get; set; }
		public List<string> sumcols { get; set; }
		public List<string> UseInSelectSumcols { get; set; }
		public List<string> joins { get; set; }
		public List<string> searchs { get; set; }
		public List<int> archiveCols { get; set; }
		public List<string> archiveJoins { get; set; }
		public List<string> statusColumns { get; set; }
		public List<ItemColumn> processColumns { get; set; }
		public List<ItemColumn> transColumns { get; set; }
		public ItemColumn milestoneColumn { get; set; }
		public List<ItemColumn> equationColumns { get; set; }
		public List<ItemColumn> sqCols { get; set; }
		public List<ItemColumn> resolveValueColumns { get; set; }

		public string GetSearchSql() {
			var searchSql = "";
			if (searchs.Count > 0) {
				searchSql = " AND (1=1 " + string.Join(" ", searchs.ToArray()) + " )";
			}

			return searchSql;
		}
	}

	public interface IPortfolioSqlQueryService {
		PortfolioSqls GetSql(int portfolioId = 0, string textSearch = "", DateTime? archiveDate = null,
			bool isTableProcess = false);
	}

	// This class is a mess. Very hard to understand
	public class PortfolioSqlService : IPortfolioSqlQueryService {
		private readonly AuthenticatedSession _authenticatedSession;
		private readonly string _instance;
		private readonly IItemColumns _itemColumns;
		private readonly string _process;
		private readonly string _processTableName;

		private PortfolioSqls pSqls;

		public PortfolioSqlService(
			string instance,
			AuthenticatedSession session,
			IItemColumns itemColumns,
			string processTableName,
			string process) {
			_itemColumns = itemColumns;
			_processTableName = processTableName;
			_authenticatedSession = session;
			_instance = instance;
			_process = process;
		}


		public PortfolioSqls GetSql(int portfolioId = 0, string textSearch = "", DateTime? archiveDate = null,
			bool isTableProcess = false) {
			pSqls = new PortfolioSqls();
			if (portfolioId > 0) {
				HandlePortfolioSql(portfolioId, textSearch, archiveDate);
			} else {
				HandleNoPortfolioSqls();
			}

			pSqls.cols.Add("order_no");
			pSqls.sqlcols.Add("pf.order_no");


			if (isTableProcess) {
				if (!pSqls.cols.Contains("owner_item_id")) pSqls.cols.Add("owner_item_id");
				if (!pSqls.sqlcols.Contains("pf.owner_item_id")) pSqls.sqlcols.Add("pf.owner_item_id");
			}

			SetArchiveJoins();
			RambleThroughStatusColumns();

			return pSqls;
		}

		public List<ProcessPortfolioColumn> GetColumns(int portfolioId) {
			var pColumns = new ProcessPortfolioColumns(
				_instance,
				_process,
				_authenticatedSession);
			return pColumns.GetColumns(portfolioId);
		}

		private void RambleThroughStatusColumns() {
			foreach (var c in _itemColumns.GetStatusColumns()) {
				pSqls.statusColumns.Add(c.Name);
				if ((c.IsProcess() || c.IsList()) && c.DataAdditional != null) {
					if (pSqls.cols.Contains(c.Name)) continue;
					pSqls.cols.Add(c.Name);
					pSqls.processColumns.Add(c);
					if (c.InputMethod == 3) {
						pSqls.sqlcols.Add(
							"STUFF ((SELECT ','+cast(item_id as nvarchar) " +
							"FROM item_data_process_selections " +
							"WHERE selected_item_id = pf.item_id " +
							" AND item_column_id = " + c.RelativeColumnId + " FOR XML PATH('')) ,1,1,'')  as " +
							c.Name + "_str");
					} else {
						pSqls.sqlcols.Add(
							"STUFF ((SELECT ','+cast(selected_item_id as nvarchar) " +
							"FROM item_data_process_selections " +
							"WHERE item_id = pf.item_id " +
							" AND item_column_id = " + c.ItemColumnId + " FOR XML PATH('')) ,1,1,'')  as " + c.Name +
							"_str");
					}
				} else if (c.IsTag()) {
					pSqls.cols.Add(c.Name);
					pSqls.sqlcols.Add(
						"STUFF ((SELECT ','+cast(tag_name as nvarchar) " +
						"FROM item_data_process_selections idps INNER JOIN _" + _instance +
						"_instance_tags iit ON idps.selected_item_id = iit.item_id " +
						"WHERE idps.item_id = pf.item_id " +
						" AND item_column_id = " + c.ItemColumnId + " FOR XML PATH('')) ,1,1,'')  as " + c.Name +
						"_str");
					pSqls.processColumns.Add(c);
				} else if (!c.IsJoin() && !c.IsButton() && c.DataType < 60 && !c.IsSubquery() && !c.IsDescription() &&
				           !c.IsPassword() && !c.IsPortfolio() && !c.IsEquation() && !c.IsRm() && !c.IsRating() &&
				           !c.IsMatrix() && !c.IsChart()) {
					if (pSqls.cols.Contains(c.Name)) continue;
					pSqls.cols.Add(c.Name);
					pSqls.sqlcols.Add("pf." + c.Name);
				} else if (c.IsSubquery()) {
					if (pSqls.cols.Contains(c.Name)) continue;

					var ics = new ItemColumns(_instance, _process, _authenticatedSession);
					pSqls.sqlcols.Add(ics.GetSubQuery(c));
					pSqls.cols.Add(c.Name);
					if (c.IsOfSubType(ItemColumn.ColumnType.List) || c.IsOfSubType(ItemColumn.ColumnType.Process)) {
						pSqls.processColumns.Add(c);
					}
				} else if (c.IsEquation()) {
					var contains = false;
					foreach (var c2 in pSqls.equationColumns) {
						if (c2.ItemColumnId == c.ItemColumnId) {
							contains = true;
							break;
						}
					}

					if (!contains) {
						pSqls.equationColumns.Add(c);
					}
				}
			}
		}

		private void SetArchiveJoins() {
			foreach (var col in pSqls.archiveCols) {
				var archiveCol = _itemColumns.GetItemColumn(col);
				pSqls.archiveJoins.Add(" LEFT JOIN archive_" + _processTableName + " archive_" + col +
				                       " ON pf.item_id = archive_" + col + ".item_id "
				                       + " AND archive_" + col +
				                       ".archive_start = (SELECT TOP 1 MIN(archive_start) FROM ( SELECT archive_start, " +
				                       archiveCol.Name + " FROM archive_" + _processTableName +
				                       " WHERE item_id = pf.item_id "
				                       + " UNION ALL SELECT archive_start, " + archiveCol.Name + " FROM " +
				                       _processTableName +
				                       " WHERE item_id = pf.item_id) AS tmp GROUP BY " + archiveCol.Name +
				                       " ORDER BY MIN(archive_start) DESC ) ");
			}
		}

		private void HandleNoPortfolioSqls() {
			foreach (var iColumn in _itemColumns.GetItemColumns()) {
				if (string.IsNullOrEmpty(iColumn.Name)) continue;

				if (iColumn.IsProcess() || iColumn.IsList()) {
					pSqls.cols.Add(iColumn.Name);
					AddColSql(iColumn);
					pSqls.processColumns.Add(iColumn);
				} else if (iColumn.IsAttachment() || iColumn.IsLink()) {
					pSqls.cols.Add(iColumn.Name);
					pSqls.sqlcols.Add(
						"STUFF ((SELECT ','+cast(attachment_id as nvarchar) FROM attachments WHERE item_id = pf.item_id  AND item_column_id = " +
						iColumn.ItemColumnId + " FOR XML PATH('')) ,1,1,'')  as " + iColumn.Name + "_str");
					pSqls.processColumns.Add(iColumn);
				} else if (iColumn.IsTag()) {
					pSqls.cols.Add(iColumn.Name);
					pSqls.sqlcols.Add(
						"STUFF ((SELECT ','+cast(tag_name as nvarchar) " +
						"FROM item_data_process_selections idps INNER JOIN _" + _instance +
						"_instance_tags iit ON idps.selected_item_id = iit.item_id " +
						"WHERE idps.item_id = pf.item_id " +
						" AND item_column_id = " + iColumn.ItemColumnId + " FOR XML PATH('')) ,1,1,'')  as " +
						iColumn.Name + "_str");
					pSqls.processColumns.Add(iColumn);
				} else if (!iColumn.IsButton() && !iColumn.IsJoin() && iColumn.DataType < 60 && !iColumn.IsSubquery() &&
				           !iColumn.IsDescription() && !iColumn.IsPassword() && !iColumn.IsPortfolio() &&
				           !iColumn.IsEquation() && !iColumn.IsRm() && !iColumn.IsRating() && !iColumn.IsMatrix()) {
					if (iColumn.IsTranslation()) {
						pSqls.cols.Add(iColumn.Name);
						pSqls.sqlcols.Add(
							" ( SELECT translation FROM process_translations WHERE instance = @instance AND process=@process AND code = '" +
							_authenticatedSession.locale_id + "' AND variable = pf." + Conv.ToSql(iColumn.Name) +
							" ) as  " +
							iColumn.Name);
					} else if (!iColumn.IsComment() && !iColumn.IsChart()) {
						// Comment and chart fields does not create column into the table so we ignore them
						pSqls.cols.Add(iColumn.Name);
						pSqls.sqlcols.Add("pf." + iColumn.Name);
					}
				} else if (iColumn.IsSubquery()) {
					if (pSqls.cols.Contains(iColumn.Name)) continue;
					var ics = new ItemColumns(_instance, _process, _authenticatedSession);
					pSqls.sqlcols.Add(ics.GetSubQuery(iColumn));
					pSqls.cols.Add(iColumn.Name);
					if (iColumn.IsOfSubType(ItemColumn.ColumnType.List) ||
					    iColumn.IsOfSubType(ItemColumn.ColumnType.Process)) {
						pSqls.processColumns.Add(iColumn);
					}
				} else if (iColumn.IsEquation()) {
					if (!pSqls.equationColumns.Contains(iColumn)) {
						pSqls.equationColumns.Add(iColumn);
					}
				}
			}

			pSqls.cols.Add("item_id");
			pSqls.sqlcols.Add("pf.item_id");
		}

		private void AddColSql(ItemColumn iColumn, DateTime? archiveDate = null) {
			var t = "item_data_process_selections";
			if (archiveDate != null) t = "get_item_data_process_selections(@archiveDate)";

			if (iColumn.InputMethod == 3) {
				pSqls.sqlcols.Add(
					"STUFF ((SELECT ','+cast(item_id as nvarchar) FROM " + t +
					" WHERE selected_item_id = pf.item_id  AND item_column_id = " +
					iColumn.RelativeColumnId + " FOR XML PATH('')) ,1,1,'')  as " + iColumn.Name + "_str");
			} else {
				pSqls.sqlcols.Add(
					"STUFF ((SELECT ','+cast(selected_item_id as nvarchar) FROM " + t +
					" WHERE item_id = pf.item_id  AND item_column_id = " +
					iColumn.ItemColumnId + " FOR XML PATH('')) ,1,1,'')  as " + iColumn.Name + "_str");
			}
		}

		private void HandlePortfolioSql(int portfolioId, string textSearch, DateTime? archiveDate = null) {
			//Get Portfolio's configured columns
			var pCols = GetColumns(portfolioId);

			//Add columns required by Server Equation columns
			var toAdd = new List<KeyValuePair<int, ProcessPortfolioColumn>>();
			foreach (var eColumn in pCols) {
				if (!eColumn.ItemColumn.IsEquation()) continue;
				
				//Iterate all equation rows in this equation column
				foreach (var s in eColumn.ItemColumn.Equations) {
					var icId = Conv.ToInt(s["item_column_id"]);
					
					//If row has an id and it hasn't already been added
					if (icId == 0 || toAdd.Exists(p => p.Key == icId) ||
					    pCols.Exists(c => c.ItemColumn.ItemColumnId == icId)) continue;

					//Get inner Item Column and create temporaty Portfolio Column
					var ic = new ItemColumns(_instance, _process, _authenticatedSession);
					toAdd.Add(new KeyValuePair<int, ProcessPortfolioColumn>(
						icId, new ProcessPortfolioColumn {
							ItemColumn = ic.GetItemColumn(icId),
							UseInFilter = false,
							UseInSearch = false,
							ReportColumn = 2,
							IsDefault = false,
							SumColumn = false
						}));
				}
			}
			//Add found temporary columns
			pCols.AddRange(toAdd.Select(i => i.Value).ToList());
			
			pSqls.searchs = GetTextSearchClauses(textSearch, pCols);
			foreach (var pColumn in pCols) {
				if (pColumn.SumColumn && pColumn.ItemColumn.IsEquation() &&
				    (pColumn.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Int) ||
				     pColumn.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Float))) {
					pSqls.sumcols.Add(pColumn.ItemColumn.Name);
					if (pColumn.UseInSelectResult == 6) pSqls.UseInSelectSumcols.Add(pColumn.ItemColumn.Name);
					if (pColumn.UseInSelectResult == 12) pSqls.UseInSelectSumcols.Add(pColumn.ItemColumn.Name);
				}

				if (pColumn.ItemColumn.IsEquation()) pSqls.equationColumns.Add(pColumn.ItemColumn);

				if (string.IsNullOrEmpty(pColumn.ItemColumn.Name) || pColumn.ItemColumn.IsPassword() ||
				    pColumn.ItemColumn.IsPortfolio() || pColumn.ItemColumn.IsEquation() || pColumn.ItemColumn.IsRm() ||
				    pColumn.ItemColumn.IsRating()) {
					continue;
				}

				if (pColumn.ItemColumn.IsProcess() || pColumn.ItemColumn.IsList()) {
					HandleColTypes5And6(pColumn, archiveDate);
				} else if (pColumn.ItemColumn.IsJoin()) {
					HandleColType11(pColumn);
				} else if (pColumn.ItemColumn.IsTag()) {
					pSqls.cols.Add(pColumn.ItemColumn.Name);
					pSqls.sqlcols.Add(
						"STUFF ((SELECT ','+cast(tag_name as nvarchar) " +
						"FROM item_data_process_selections idps INNER JOIN _" + _instance +
						"_instance_tags iit ON idps.selected_item_id = iit.item_id " +
						"WHERE idps.item_id = pf.item_id " +
						" AND item_column_id = " + pColumn.ItemColumn.ItemColumnId +
						" FOR XML PATH('')) ,1,1,'')  as " + pColumn.ItemColumn.Name + "_str");
					pSqls.processColumns.Add(pColumn.ItemColumn);
				} else if (pColumn.ItemColumn.IsTranslation()) {
					pSqls.cols.Add(pColumn.ItemColumn.Name);
					pSqls.sqlcols.Add("pf." + pColumn.ItemColumn.Name + " as " + pColumn.ItemColumn.Name + "_str");
					pSqls.transColumns.Add(pColumn.ItemColumn);
				} else if (!pColumn.ItemColumn.IsButton() && pColumn.ItemColumn.DataType < 60) {
					HandleColType0WithTranslation(pColumn, archiveDate);
				}

				if (pColumn.ArchiveItemColumnId != null) {
					if (!pSqls.archiveCols.Contains((int)pColumn.ArchiveItemColumnId))
						pSqls.archiveCols.Add((int)pColumn.ArchiveItemColumnId);
				}

				if (pColumn.SumColumn && (pColumn.ItemColumn.IsNumber() || pColumn.ItemColumn.IsSubquery() &&
					(pColumn.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Int) ||
					 pColumn.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Float)))) {
					pSqls.sumcols.Add(pColumn.ItemColumn.Name);
					if (pColumn.UseInSelectResult == 6) pSqls.UseInSelectSumcols.Add(pColumn.ItemColumn.Name);
					if (pColumn.UseInSelectResult == 12) pSqls.UseInSelectSumcols.Add(pColumn.ItemColumn.Name);
				}

				if (pColumn.ItemColumn.IsSubquery()) pSqls.sqCols.Add(pColumn.ItemColumn);

				if (pColumn.ItemColumn.IsMilestone() && pSqls.milestoneColumn == null)
					pSqls.milestoneColumn = pColumn.ItemColumn;
			}
		}

		private void HandleColType0WithTranslation(ProcessPortfolioColumn pColumn, DateTime? archiveDate = null) {
			if (pColumn.ArchiveItemColumnId != null) {
				pSqls.cols.Add(pColumn.ItemColumn.Name);
				pSqls.sqlcols.Add("COALESCE(archive_" + pColumn.ArchiveItemColumnId + "." + pColumn.ItemColumn.Name +
				                  ", pf." +
				                  pColumn.ItemColumn.Name + ") AS " + pColumn.ItemColumn.Name);
			} else if (pColumn.ItemColumn.IsSubquery()) {
				var ics = new ItemColumns(_instance, _process, _authenticatedSession);


				var sq = 0;
				if (archiveDate != null) sq = 2;
				pSqls.sqlcols.Add(ics.GetSubQuery(pColumn.ItemColumn, archiveType: sq));


				pSqls.cols.Add(pColumn.ItemColumn.Name);
				if (pColumn.ItemColumn.IsOfSubType(ItemColumn.ColumnType.List) ||
				    pColumn.ItemColumn.IsOfSubType(ItemColumn.ColumnType.Process)) {
					pSqls.processColumns.Add(pColumn.ItemColumn);
				}
			} else if (pColumn.ItemColumn.IsEquation()) {
				if (!pSqls.equationColumns.Contains(pColumn.ItemColumn)) {
					pSqls.equationColumns.Add(pColumn.ItemColumn);
				}
			} else if (!pColumn.ItemColumn.IsChart()) {
				pSqls.cols.Add(pColumn.ItemColumn.Name);
				pSqls.sqlcols.Add("pf." + pColumn.ItemColumn.Name);
			}
		}

		private void HandleColType11(ProcessPortfolioColumn pColumn) {
			var joinIcs = new ItemColumns(_instance, pColumn.ItemColumn.DataAdditional,
				_authenticatedSession);
			var joinCol = joinIcs.GetItemColumn(Conv.ToInt(pColumn.ItemColumn.DataAdditional2));
			var sql = "(SELECT SUM( " + joinCol.Name + " ) FROM _" + _instance + "_" + joinCol.Process +
			          " spf WHERE 1=1 ";
			var joins = new ItemColumnJoins(_instance, joinCol.Process, _authenticatedSession);
			foreach (var j in joins.GetItemColumnJoins(pColumn.ItemColumn.ItemColumnId)) {
				switch (j.ParentColumnType) {
					case 0:
						sql += " AND pf.item_id = ";
						break;
					case 1:
						sql += " AND pf." + _itemColumns.GetItemColumn(Conv.ToInt(j.ParentItemColumnId)).Name + " = ";
						break;
					case 2:
						dynamic dateCol = j.ParentAdditionalJoin;
						switch ((int)dateCol.type.Value) {
							case 0:
								sql += " AND YEAR(DATEADD(year, " + (int)dateCol.offset.Value + ", GETDATE())) ";
								var ct = new ComparisonTypes();
								sql += " " + ct.GetComparisonTypes()[(int)dateCol.comparisonTypeId.Value]
									.ComparisonText + " ";
								break;
						}

						break;
				}

				switch (j.SubColumnType) {
					case 0:
						sql += " spf.item_id ";
						break;
					case 1:
						sql += " spf." + joinIcs.GetItemColumn(Conv.ToInt(j.SubItemColumnId)).Name + " ";
						break;
					case 2:
						dynamic dateCol = j.SubAdditionalJoin;
						if ((int)dateCol.type.Value == 0)
							sql += " AND YEAR(DATEADD(year, " + (int)dateCol.offset.Value + ", GETDATE())) ";
						break;
				}
			}

			sql += " ) AS " + pColumn.ItemColumn.Name;
			pSqls.sqlcols.Add(sql);
			pSqls.cols.Add(pColumn.ItemColumn.Name);
		}

		private void HandleColTypes5And6(ProcessPortfolioColumn pColumn, DateTime? archiveDate = null) {
			var iColumn = pColumn.ItemColumn;
			pSqls.cols.Add(iColumn.Name);
			AddColSql(iColumn, archiveDate);
			pSqls.processColumns.Add(iColumn);
		}

		public List<string> GetTextSearchClauses(string textSearch, List<ProcessPortfolioColumn> pCols) {
			var clauses = new List<string>();
			if (textSearch != null && textSearch.Length > 1) {
				textSearch = textSearch.Replace("'", @"''");
				foreach (var search in textSearch.Split(' ')) {
					var conditions = new List<string>();
					foreach (var pColumn in pCols) {
						if (!pColumn.UseInSearch) {
							continue;
						}

						if (pColumn.ItemColumn.IsDatabasePrimitive()) {
							conditions.Add(" LOWER(COALESCE(pf." + pColumn.ItemColumn.Name + ", '')) LIKE N'%" +
							               search.ToLower() + "%' ");
						}

						if (pColumn.ItemColumn.IsList() && pColumn.ItemColumn.DataAdditional != null) {
							conditions.Add("(select count(item_id) from _" + _instance + "_" +
							               pColumn.ItemColumn.DataAdditional +
							               " where item_id in (select selected_item_id from item_data_process_selections where item_id = pf.item_id and item_column_id = " +
							               pColumn.ItemColumn.ItemColumnId + " ) and lower(list_item) like '%" +
							               search.ToLower() + "%' )  > 0");
						}

						if (pColumn.ItemColumn.IsProcess() && pColumn.ItemColumn.DataAdditional != null &&
						    pColumn.ItemColumn.DataAdditional == "user") {
							conditions.Add("(select count(item_id) from _" + _instance + "_" +
							               pColumn.ItemColumn.DataAdditional +
							               " where item_id in (select selected_item_id from item_data_process_selections where item_id = pf.item_id and item_column_id = " +
							               pColumn.ItemColumn.ItemColumnId + " ) and ( lower(first_name) like N'%" +
							               search.ToLower() + "%' OR lower(last_name) like '%" + search.ToLower() +
							               "%' )  )  > 0");
						}

						if (pColumn.ItemColumn.IsSubquery()) {
							var ics = new ItemColumns(_instance, _process, _authenticatedSession);
							conditions.Add("LOWER(" + ics.GetSubQuery(pColumn.ItemColumn, false) + ") LIKE N'%" +
							               search.ToLower() + "%'");
						}

						if (pColumn.ItemColumn.IsEquation()) {
							var ics = new ItemColumns(_instance, _process, _authenticatedSession);
							ics.GetEquationQuery(pColumn.ItemColumn, true);
							conditions.Add("LOWER(" + ics.GetEquationQuery(pColumn.ItemColumn, true) + ") LIKE N'%" +
							               search.ToLower() + "%'");
						}

						if (pColumn.ItemColumn.IsTag()) {
							conditions.Add("  (SELECT COUNT(tag_name) FROM item_data_process_selections idps " +
							               " INNER JOIN _" + _instance +
							               "_instance_tags iit ON idps.selected_item_id = iit.item_id " +
							               " WHERE item_column_id = " + pColumn.ItemColumn.ItemColumnId +
							               " AND idps.item_id = pf.item_id AND tag_name LIKE N'%" + search.ToLower() +
							               "%' )  > 0 ");
						}
					}

					if (conditions.Count > 0) {
						clauses.Add(" AND (" + string.Join(" OR ", conditions) + " ) ");
					}
				}

				if (clauses.Count == 0) {
					clauses.Add(" AND 1=0 ");
				}
			}

			return clauses;
		}
	}
}