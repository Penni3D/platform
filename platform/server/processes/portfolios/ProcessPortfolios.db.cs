﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.portfolios {
	public class ProcessPortfolio {
		public ProcessPortfolio() { }

		public ProcessPortfolio(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ProcessPortfolioId = (int) dr[alias + "process_portfolio_id"];
			Process = (string) dr[alias + "process"];
			Variable = Conv.ToStr(dr[alias + "variable"]);
			ProcessPortfolioType = (int) dr[alias + "process_portfolio_type"];
			CalendarType = Conv.ToByte(dr[alias + "calendar_type"]);
			CalendarStartId = Conv.ToInt(dr[alias + "calendar_start_id"]);
			CalendarEndId = Conv.ToInt(dr[alias + "calendar_end_id"]);
			ShowProgressArrows = Convert.ToBoolean(dr["show_progress_arrows"]);
			HiddenProgressArrows = Convert.ToString(dr["hidden_progress_arrows"]);
			OpenSplit = Conv.ToInt(dr[alias + "open_split"]);
			Recursive = Conv.ToInt(dr[alias + "recursive"]);
			DefaultState = Conv.ToStr(dr[alias + "default_state"]);
			RecursiveFilterType = Conv.ToInt(dr[alias + "recursive_filter_type"]);
			IgnoreRights = Convert.ToBoolean(dr["ignore_rights"]);
			TimeMachine = Convert.ToBoolean(dr["timemachine"]);
			UseDefaultState = Conv.ToInt(dr[alias + "use_default_state"]);
			OrderItemColumnId = (int?) Conv.IfDbNullThenNull(dr["order_item_column_id"]);
			DownloadOptions = Convert.ToString(dr["download_options"]);
			AlsoOpenCurrent = Conv.ToInt(dr["also_open_current"]);
			SubDefaultState = Conv.ToStr(dr[alias + "sub_default_state"]);
			OpenWidth = Conv.ToStr(dr[alias + "openwidth"]);
			IsUnionPortfolio = dr.Table.Columns.Contains("is_union") && Conv.ToInt(dr[alias + "is_union"]) == 1;

			if (Conv.ToInt(OrderItemColumnId) > 0) {
				var ic = new ItemColumns(session.instance, Process, session);
				var col = ic.GetItemColumn(Conv.ToInt(OrderItemColumnId));
				if (col != null) OrderItemColumnName = col.Name;
			}

			OpenRowItemColumnId = Conv.ToInt(dr["open_row_item_column_id"]);
			OpenRowProcess = Conv.ToStr(dr["open_row_process"]);
			OrderDirection = Conv.ToByte(dr[alias + "order_direction"]);


			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);

			PortfolioDefaultView = Conv.ToInt(dr["portfolio_default_view"]);
		}

		public int ProcessPortfolioId { get; set; }
		public string Process { get; set; }
		public string Variable { get; set; }
		public int ProcessPortfolioType { get; set; }
		public byte CalendarType { get; set; }
		public int CalendarStartId { get; set; }
		public int CalendarEndId { get; set; }
		public bool ShowProgressArrows { get; set; }
		public string HiddenProgressArrows { get; set; }
		public List<int> ProcessGroupIds { get; set; }
		public List<int> ProcessUserGroupIds { get; set; }
		public int OpenSplit { get; set; }
		public string DefaultState { get; set; }
		public int Recursive { get; set; }
		public int RecursiveFilterType { get; set; }
		public bool IgnoreRights { get; set; }
		public bool TimeMachine { get; set; }
		public bool IsUnionPortfolio { get; set; }

		public string DownloadOptions { get; set; }

		public int UseDefaultState { get; set; }
		public int? OrderItemColumnId { get; set; }

		public string OrderItemColumnName { get; set; }

		public byte OrderDirection { get; set; }
		public string SubDefaultState { get; set; }
		public string OpenWidth { get; set; }

		public string OrderDirectionName {
			get {
				if (OrderDirection == 0) return "asc";
				return "desc";
			}
		}

		public int OpenRowItemColumnId { get; set; }
		public string OpenRowProcess { get; set; }
		public int AlsoOpenCurrent { get; set; }

		public Dictionary<string, string> LanguageTranslation { get; set; }

		public int PortfolioDefaultView { get; set; }
	}

	public class ProcessPortfolioDialog {
		public ProcessPortfolioDialog() { }

		public ProcessPortfolioDialog(DataRow dr, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ProcessContainerId = (int) dr[alias + "process_container_id"];
			ProcessPortfolioDialogId = (int) dr[alias + "process_portfolio_dialog_id"];
			Variable = Conv.ToStr(dr[alias + "variable"]);
			OrderNo = Conv.ToStr(dr[alias + "order_no"]);
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
		}

		public int ProcessContainerId { get; set; }
		public int? ProcessPortfolioDialogId { get; set; }
		public string Variable { get; set; }
		public string OrderNo { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
	}

	public class ProcessPortfolioDefault : ProcessPortfolio {
		public ProcessPortfolioDefault(DataRow row, AuthenticatedSession session) {
			ProcessPortfolioId = (int) row["process_portfolio_id"];
			Process = (string) row["process"];
			Variable = Conv.ToStr(row["variable"]);
			ProcessPortfolioType = (int) row["process_portfolio_type"];
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
		}

		public List<ProcessPortfolioColumn> ProcessPortfolioColumns { get; set; }
	}

	public class AttachedProcessResult {
		public string Process;
		public int PortfolioId;
		public string Tag;
		public string LinkColumnName;
		public int Rows;
		public List<Dictionary<string, List<KeyValuePair<string,int>>>> MatrixStructure;
	}

	public class ProcessPortfolios : ProcessBase {
		public ProcessPortfolios(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance,
			process, authenticatedSession) { }

		private DataTable _processPortfolios;

		private DataTable processPortfolios {
			get {
				_processPortfolios = (DataTable) Cache.Get("", "process_portfolio_table");
				if (_processPortfolios != null) return _processPortfolios;

				_processPortfolios = db.GetDatatable("SELECT p.*, CASE WHEN (SELECT process_type FROM processes WHERE process=p.process) = 6 THEN 1 ELSE 0 END AS is_union FROM process_portfolios p");
				Cache.Set("", "process_portfolio_table", _processPortfolios);

				return _processPortfolios;
			}
		}
		
		private DataTable _processPortfolioGroups;
		private DataTable processPortfolioGroups {
			get {
				_processPortfolioGroups = (DataTable) Cache.Get("", "process_portfolio_group_table");
				if (_processPortfolioGroups != null) return _processPortfolioGroups;

				_processPortfolioGroups = db.GetDatatable("SELECT process_group_id, process_portfolio_id FROM process_portfolio_groups");
				Cache.Set("", "process_portfolio_group_table", _processPortfolioGroups);

				return _processPortfolioGroups;
			}
		}
		

		private void ClearCache() {
			_processPortfolios = null;
			Cache.Remove("", "process_portfolio_group_table");
			Cache.Remove("", "process_portfolio_table");
			Cache.Remove("", "process_portfolio_columns_table");
		}

		public List<AttachedProcessResult> GetAttachedProcesses(int portfolioId, string parentProcess) {
			var result = new List<AttachedProcessResult>();

			SetDBParam(SqlDbType.Int, "ppid", portfolioId);
			var p = Conv.ToStr(db.ExecuteScalar(
				"SELECT download_options FROM process_portfolios WHERE process_portfolio_id = @ppid", DBParameters));
			if (p == "") return result;

			var j = JsonConvert.DeserializeObject<Dictionary<string, object>>(p);
			foreach (var id in j.Keys) {
				var o = (JObject) j[id];

				if (!o.ContainsKey("TemplateTag")) continue;

				var r = new AttachedProcessResult {
					Tag = Conv.ToStr(o["TemplateTag"]),
					PortfolioId = Conv.ToInt(o["PortfolioId"]),
					Rows = o.ContainsKey("Rows") && Conv.ToStr(o["Rows"]) != "" ? Conv.ToInt(o["Rows"]) : 0
				};

				var columnIsMatrix = false;
				var matrixProcess = "";

				if (r.PortfolioId == 0) {
					SetDBParam(SqlDbType.Int, "@itemcolumnid", id);

					var ifMatrix = db.GetDataRow(
						"SELECT data_type, data_additional, name FROM item_columns WHERE item_column_id = @itemcolumnid",
						DBParameters);
					if (Conv.ToInt(ifMatrix["data_type"]) != 22)
						continue;

					columnIsMatrix = true;
					matrixProcess = Conv.ToStr(ifMatrix["data_additional"]);
					var matrixRows = new ItemCollectionService(instance, matrixProcess , authenticatedSession);
					matrixRows.FillItems();
					var matrixQuestions = new List<string>();
					r.MatrixStructure = new List<Dictionary<string, List<KeyValuePair<string,int>>>>();
					r.LinkColumnName = Conv.ToStr(ifMatrix["name"]);
					var Translations = new Translations(instance, authenticatedSession);

					foreach (var mr in matrixRows.GetItems().Where(i => Conv.ToInt(i["type"]) == 1)) {
						matrixQuestions.Add(Translations.GetTranslation(Conv.ToStr(mr["name"])));
						var qItemId = Conv.ToInt(mr["item_id"]);


						var pop = new Dictionary<string, List<KeyValuePair<string,int>>>();
						var l = new List<KeyValuePair<string,int>>();

						KeyValuePair<string, int> keyValuePair = new KeyValuePair<string, int>(Translations.GetTranslation(Conv.ToStr(mr["name"])), Conv.ToInt(mr["item_id"]));

						l.Add(keyValuePair);

						pop.Add("Questions", l);
						pop.Add("Columns", new List<KeyValuePair<string,int>>());
						pop.Add("Cells", new List<KeyValuePair<string,int>>());

						foreach (var matrixRow in matrixRows.GetItems().Where(k => Conv.ToInt(k["type"]) == 2)) {
							KeyValuePair<string, int> keyValuePair2 = new KeyValuePair<string, int>(Translations.GetTranslation(Conv.ToStr(matrixRow["name"])), Conv.ToInt(matrixRow["item_id"]));
							pop["Columns"].Add(keyValuePair2);
						}

						foreach (var matrixRow in matrixRows.GetItems().Where(k => Conv.ToInt(k["type"]) == 3 && Conv.ToInt(k["question_item_id"]) == qItemId)) {

							KeyValuePair<string, int> keyValuePair3 = new KeyValuePair<string, int>(Translations.GetTranslation(Conv.ToStr(matrixRow["name"])), Conv.ToInt(matrixRow["item_id"]));

							pop["Cells"].Add(keyValuePair3);
						}
						r.MatrixStructure.Add(pop);
					}
				}

				if (!Conv.ToBool(o["Include"])) continue;
				r.Process = r.PortfolioId != 0 ? GetPortfolio(r.PortfolioId).Process : matrixProcess;

				if (!columnIsMatrix) {
					var itemColumnDa = Conv.ToStr(db.ExecuteScalar(
						"SELECT data_additional2 FROM item_columns WHERE process = '" + Conv.ToSql(parentProcess) +
						"' AND data_type = 18 AND data_additional2 LIKE '%portfolioId\":" + r.PortfolioId + "%'",
						DBParameters));

					if (itemColumnDa == "")
						continue;
					var itemColumnDaJson = JsonConvert.DeserializeObject<Dictionary<string, object>>(itemColumnDa);
					var itemColumnId = 0;
					foreach (var key in itemColumnDaJson.Keys) {
						if (key != "itemColumnId")
							continue;
						itemColumnId = Conv.ToInt(itemColumnDaJson[key]);
						break;
					}

					if (itemColumnId > 0) {
						var ics = new ItemColumns(instance, r.Process, authenticatedSession);
						var ic = ics.GetItemColumn(itemColumnId);
						r.LinkColumnName = ic.Name;
					}
				}

				result.Add(r);
			}

			return result;
		}

		public Dictionary<string, object> GetDefaultPortfolios() {
			var result = new List<ProcessPortfolioDefault>();
			var processes = new Dictionary<int, object>();
			ProcessPortfolioDefault portfolio = null;
			var processPortfolioItemColumns = new List<ProcessPortfolioColumn>();
			var processPortfolioId = -1;
			DataRow r = null;

			foreach (DataRow row in db.GetDatatable(
				"SELECT ic.name, ic.data_additional, ic.data_additional2, ic.input_name,  ic.data_type, ic.item_column_id, pf.process, pf.process_portfolio_id, pf.variable, ppc.use_in_select_result, ppc.use_in_filter, ppc.use_in_search, pf.process_portfolio_type, is_default, ic.variable AS colVariable " +
				"FROM process_portfolio_columns ppc " +
				"INNER JOIN( " +
				"SELECT process, process_portfolio_id, variable, process_portfolio_type " +
				"FROM process_portfolios " +
				"WHERE process_portfolio_id IN( " +
				"SELECT process_portfolio_id " +
				"FROM process_portfolios " +
				"WHERE process_portfolio_type IN (1, 10) AND instance = @instance)) pf " +
				"ON pf.process_portfolio_id = ppc.process_portfolio_id " +
				"INNER JOIN item_columns ic " +
				"ON ic.item_column_id = ppc.item_column_id " +
				"ORDER BY pf.process_portfolio_id, ppc.order_no  ASC;", DBParameters).Rows) {
				var pp = (int) row["process_portfolio_id"];
				var name = (string) row["name"];
				var input_name = Conv.ToStr(row["input_name"]);
				var da = (string) Conv.IfDbNullThenNull(row["data_additional"]); //(string)row["data_additional"];
				var da2 = (string) Conv.IfDbNullThenNull(row["data_additional2"]);
				var dt = (byte) row["data_type"];
				var uif = Convert.ToBoolean(row["use_in_filter"]);
				var ici = (int) row["item_column_id"];
				var uisr = (byte) row["use_in_select_result"];
				var variable = Conv.ToStr(row["colVariable"]);
				var uis = Convert.ToBoolean(row["use_in_search"]);

				r = row;
				if (pp == processPortfolioId) {
					var ic = new ItemColumn();
					var ppc = new ProcessPortfolioColumn();

					if ((dt == 5 || dt == 6) && da != null) {
						if (uif && dt == 6) {
							var p = new ItemCollectionService(instance, da, authenticatedSession);
							p.FillItems();
							var items = p.GetItems();

							foreach (var item in items) {
								item["process"] = da;
								if (!processes.ContainsKey((int) item["item_id"]))
									processes.Add((int) item["item_id"], item);
							}
						}

						ic.DataAdditional = da;
					}

					if (dt == 16 && da != null) {
						ic.DataAdditional = da;
						var subQuery = RegisteredSubQueries.GetType(ic.DataAdditional, ici);
						if (subQuery != null) {
							ic.SubDataType = (int) subQuery.GetConfig().DataType;
							ic.SubDataAdditional = subQuery.GetSubProcess();
						} else {
							ic.SubDataType = 0;
						}
					}

					if (dt == 20 && (Conv.ToInt(da2) != 5 || Conv.ToInt(da2) != 6)) {
						ic.SubDataType = Conv.ToInt(da2);
						ic.SubDataAdditional = input_name;
					}

					ic.Name = name;
					ic.DataType = dt;
					ic.ItemColumnId = ici;
					ic.Variable = variable;
					ppc.UseInFilter = uif;
					ppc.UseInSelectResult = uisr;
					ppc.ItemColumn = ic;
					ppc.UseInSearch = uis;
					processPortfolioItemColumns.Add(ppc);
				} else {
					if (processPortfolioId != -1) {
						portfolio.ProcessPortfolioColumns = processPortfolioItemColumns;
						result.Add(portfolio);

						portfolio = new ProcessPortfolioDefault(row, authenticatedSession);
						processPortfolioItemColumns = new List<ProcessPortfolioColumn>();
					} else {
						portfolio = new ProcessPortfolioDefault(row, authenticatedSession);
					}

					var ic = new ItemColumn();
					var ppc = new ProcessPortfolioColumn();

					if ((dt == 5 || dt == 6) && da != null) {
						if (uif && dt == 6) {
							var p = new ItemCollectionService(instance, da, authenticatedSession);
							p.FillItems();
							var items = p.GetItems();

							foreach (var item in items) {
								item["process"] = da;
								if (!processes.ContainsKey((int) item["item_id"]))
									processes.Add((int) item["item_id"], item);
							}
						}

						ic.DataAdditional = da;
					}

					if (dt == 16 && da != null) {
						ic.DataAdditional = da;
						var subQuery = RegisteredSubQueries.GetType(ic.DataAdditional, ici);
						if (subQuery != null) {
							ic.SubDataType = (int) subQuery.GetConfig().DataType;
							ic.SubDataAdditional = subQuery.GetSubProcess();
						} else {
							ic.SubDataType = 0;
						}
					}

					if (dt == 20 && (Conv.ToInt(da2) != 5 || Conv.ToInt(da2) != 6)) {
						ic.SubDataType = Conv.ToInt(da2);
						ic.SubDataAdditional = input_name;
					}

					ic.DataType = dt;
					ic.Name = name;
					ic.ItemColumnId = ici;
					ic.Variable = variable;
					ppc.UseInSelectResult = uisr;
					ppc.ItemColumn = ic;
					ppc.UseInFilter = uif;
					processPortfolioItemColumns.Add(ppc);

					processPortfolioId = pp;
				}
			}

			if (processPortfolioId != -1) {
				portfolio = new ProcessPortfolioDefault(r, authenticatedSession);
				portfolio.ProcessPortfolioColumns = processPortfolioItemColumns;
				result.Add(portfolio);
			}

			return new Dictionary<string, object> {
				{"portfolios", result},
				{"processes", processes}
			};
		}

		public List<ProcessPortfolio> GetPortfolios(string process, int portfolioType) {
			var result = new List<ProcessPortfolio>();

			var f = new common.Util.Filter();
			f.Add("instance", instance);
			f.Add("process", process);
			f.Add("process_portfolio_type", portfolioType);

			foreach (DataRow row in processPortfolios.Select(f.ToString())) {
				var portfolio = new ProcessPortfolio(row, authenticatedSession);
				portfolio.ProcessGroupIds = GetPortfolioGroups(portfolio.ProcessPortfolioId);
				result.Add(portfolio);
			}

			return result;
		}

		public List<ProcessPortfolio> GetPortfolios(string process) {
			var result = new List<ProcessPortfolio>();

			var f = new common.Util.Filter();
			f.Add("instance", instance);
			f.Add("process", process);

			foreach (DataRow row in processPortfolios.Select(f.ToString())) {
				var portfolio = new ProcessPortfolio(row, authenticatedSession);
				portfolio.ProcessGroupIds = GetPortfolioGroups(portfolio.ProcessPortfolioId);
				result.Add(portfolio);
			}

			return result;
		}


		public List<ProcessPortfolio> GetPortfolios() {
			var result = new List<ProcessPortfolio>();

			var f = new common.Util.Filter();
			f.Add("instance", instance);
			if (process != "") f.Add("process", process);
		//	f.Add("process_portfolio_type", 0);

			//Get Column Names
			foreach (DataRow row in processPortfolios.Select(f.ToString())) {
				var portfolio = new ProcessPortfolio(row, authenticatedSession);
				portfolio.ProcessGroupIds = GetPortfolioGroups(portfolio.ProcessPortfolioId);
				result.Add(portfolio);
			}

			return result;
		}

		public void Delete(int processPortfolioId) {
			SetDBParam(SqlDbType.Int, "processPortfolioId", processPortfolioId);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_portfolios WHERE instance = @instance AND process = @process AND process_portfolio_id = @processPortfolioId",
				DBParameters);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_portfolio_columns_groups WHERE process = @process AND process_portfolio_id = @processPortfolioId",
				DBParameters);

			foreach (DataRow rx in db.GetDatatable("SELECT params, instance_menu_id FROM instance_menu").Rows) {
				var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(Conv.ToStr(rx["params"]));
				try {
					foreach (var item in dictionary) {
						if (item.Key == "portfolioId" && Conv.ToStr(item.Value) == Conv.ToStr(processPortfolioId)) {
							SetDBParam(SqlDbType.Int, "@instanceMenuId", Conv.ToInt(rx["instance_menu_id"]));
							db.ExecuteDeleteQuery("DELETE FROM instance_menu WHERE instance_menu_id = @instanceMenuId",
								DBParameters);
						}
					}
				} catch (Exception e) {
					Console.WriteLine(e.Message);
				}
			}
			ClearCache();
		}

		public int Add(ProcessPortfolio processPortfolio) {
			SetDBParam(SqlDbType.Int, "@portfolioType", processPortfolio.ProcessPortfolioType);
			SetDBParam(SqlDbType.TinyInt, "@calendarType", processPortfolio.CalendarType);
			SetDBParam(SqlDbType.Int, "@calendarStartId", processPortfolio.CalendarStartId);
			SetDBParam(SqlDbType.Int, "@calendarEndId", processPortfolio.CalendarEndId);
			SetDBParam(SqlDbType.TinyInt, "@showProgressArrows", processPortfolio.ShowProgressArrows);
			SetDBParam(SqlDbType.NVarChar, "@hiddenProgressArrows", processPortfolio.HiddenProgressArrows);
			SetDBParam(SqlDbType.TinyInt, "@openSplit", processPortfolio.OpenSplit);
			SetDBParam(SqlDbType.NVarChar, "@defaultState", Conv.IfNullThenDbNull(processPortfolio.DefaultState));
			SetDBParam(SqlDbType.Int, "@recursive", Conv.IfNullThenDbNull(processPortfolio.Recursive));
			SetDBParam(SqlDbType.Int, "@recursiveFilterType", processPortfolio.RecursiveFilterType);
			SetDBParam(SqlDbType.TinyInt, "@ignoreRights", processPortfolio.IgnoreRights);
			SetDBParam(SqlDbType.TinyInt, "@timemachine", processPortfolio.TimeMachine);
			SetDBParam(SqlDbType.TinyInt, "@useDefaultState", processPortfolio.UseDefaultState);
			SetDBParam(SqlDbType.NVarChar, "@hiddenProgressArrows", "");
			SetDBParam(SqlDbType.Int, "@orderItemColumnId", Conv.IfNullThenDbNull(processPortfolio.OrderItemColumnId));
			SetDBParam(SqlDbType.TinyInt, "@orderDirection", processPortfolio.OrderDirection);
			SetDBParam(SqlDbType.Int, "@open_row_item_column_id", processPortfolio.OpenRowItemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@open_row_process", Conv.IfNullThenDbNull(processPortfolio.OpenRowProcess));
			SetDBParam(SqlDbType.TinyInt, "@also_open_current",
				Conv.IfNullThenDbNull(processPortfolio.AlsoOpenCurrent));
			SetDBParam(SqlDbType.Int, "@dtf", Conv.IfNullThenDbNull(processPortfolio.PortfolioDefaultView));

			processPortfolio.ProcessPortfolioId = db.ExecuteInsertQuery(
				"INSERT INTO process_portfolios (instance, process, process_portfolio_type, calendar_type, calendar_start_id, calendar_end_id, show_progress_arrows, hidden_progress_arrows, open_split, default_state, recursive, recursive_filter_type, ignore_rights, timemachine, use_default_state, order_item_column_id, order_direction, open_row_item_column_id, open_row_process, also_open_current, sub_default_state, openwidth, portfolio_default_view) VALUES(@instance, @process, @portfolioType, @calendarType, @calendarStartId, @calendarEndId, @showProgressArrows, @hiddenProgressArrows, @openSplit, @defaultState, @recursive, @recursiveFilterType, @ignoreRights, @timemachine, @useDefaultState, @orderItemColumnId, @orderDirection, @open_row_item_column_id, @open_row_process, @also_open_current, null, null, @dtf)",
				DBParameters);

			if (processPortfolio.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVal = translations.GetTranslationVariable(process,
					processPortfolio.LanguageTranslation[authenticatedSession.locale_id], "PORT");

				SetDBParam(SqlDbType.NVarChar, "@variable", newVal);
				SetDBParam(SqlDbType.Int, "@processPortfolioId", processPortfolio.ProcessPortfolioId);
				db.ExecuteUpdateQuery(
					"UPDATE process_portfolios SET variable = @variable WHERE instance = @instance AND process = @process AND process_portfolio_id = @processPortfolioId",
					DBParameters);

				foreach (var translation in processPortfolio.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVal, translation.Value, translation.Key);
				}
			}

			if (processPortfolio.ProcessGroupIds != null) {
				SetDBParam(SqlDbType.Int, "@processPortfolioId", processPortfolio.ProcessPortfolioId);
				db.ExecuteDeleteQuery(
					"DELETE FROM process_portfolio_groups WHERE process_portfolio_id = @processPortfolioId",
					DBParameters);
				foreach (var id in processPortfolio.ProcessGroupIds) {
					SetDBParam(SqlDbType.Int, "@groupId", id);
					db.ExecuteInsertQuery(
						"INSERT INTO process_portfolio_groups (process_portfolio_id, process_group_id) VALUES(@processPortfolioId, @groupId)",
						DBParameters);
				}
			}

			if (processPortfolio.ProcessUserGroupIds != null) {
				SetDBParam(SqlDbType.Int, "@processPortfolioId", processPortfolio.ProcessPortfolioId);
				db.ExecuteDeleteQuery(
					"DELETE FROM process_portfolio_user_groups WHERE process_portfolio_id = @ProcessPortfolioId",
					DBParameters);
				foreach (var id in processPortfolio.ProcessUserGroupIds) {
					SetDBParam(SqlDbType.Int, "@groupId", id);
					db.ExecuteInsertQuery(
						"INSERT INTO process_portfolio_user_groups (process_portfolio_id, item_id) VALUES(@processPortfolioId, @groupId)",
						DBParameters);
				}
			}

			Cache.Remove(instance, "SelectorColumnNames_" + process);
			ClearCache();
			return processPortfolio.ProcessPortfolioId;
		}

		public void Save(ProcessPortfolio processPortfolio) {
			SetDBParam(SqlDbType.Int, "@processPortfolioId", processPortfolio.ProcessPortfolioId);
			SetDBParam(SqlDbType.Int, "@portfolioType", processPortfolio.ProcessPortfolioType);
			SetDBParam(SqlDbType.TinyInt, "@calendarType", processPortfolio.CalendarType);
			SetDBParam(SqlDbType.Int, "@calendarStartId", processPortfolio.CalendarStartId);
			SetDBParam(SqlDbType.Int, "@calendarEndId", processPortfolio.CalendarEndId);
			SetDBParam(SqlDbType.TinyInt, "@showProgressArrows", processPortfolio.ShowProgressArrows);
			SetDBParam(SqlDbType.NVarChar, "@hiddenProgressArrows", processPortfolio.HiddenProgressArrows);
			SetDBParam(SqlDbType.TinyInt, "@openSplit", processPortfolio.OpenSplit);
			SetDBParam(SqlDbType.NVarChar, "@defaultState", Conv.IfNullThenDbNull(processPortfolio.DefaultState));
			SetDBParam(SqlDbType.Int, "@recursive", Conv.IfNullThenDbNull(processPortfolio.Recursive));
			SetDBParam(SqlDbType.Int, "@recursiveFilterType", processPortfolio.RecursiveFilterType);
			SetDBParam(SqlDbType.TinyInt, "@ignoreRights", processPortfolio.IgnoreRights);
			SetDBParam(SqlDbType.TinyInt, "@timemachine", processPortfolio.TimeMachine);
			SetDBParam(SqlDbType.TinyInt, "@useDefaultState", processPortfolio.UseDefaultState);
			SetDBParam(SqlDbType.Int, "@orderItemColumnId", Conv.IfNullThenDbNull(processPortfolio.OrderItemColumnId));
			SetDBParam(SqlDbType.TinyInt, "@orderDirection", processPortfolio.OrderDirection);
			SetDBParam(SqlDbType.Int, "@open_row_item_column_id", processPortfolio.OpenRowItemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@download_options",
				Conv.IfNullThenDbNull(processPortfolio.DownloadOptions));
			SetDBParam(SqlDbType.NVarChar, "@open_row_process", Conv.IfNullThenDbNull(processPortfolio.OpenRowProcess));
			SetDBParam(SqlDbType.NVarChar, "@also_open_current", processPortfolio.AlsoOpenCurrent);
			SetDBParam(SqlDbType.NVarChar, "@subDefaultState", Conv.IfNullThenDbNull(processPortfolio.SubDefaultState));
			SetDBParam(SqlDbType.NVarChar, "@openWidth", Conv.IfNullThenDbNull(processPortfolio.OpenWidth));
			SetDBParam(SqlDbType.Int, "@dtf", Conv.IfNullThenDbNull(processPortfolio.PortfolioDefaultView));

			db.ExecuteUpdateQuery(
				"UPDATE process_portfolios SET open_row_process = @open_row_process, open_row_item_column_id = @open_row_item_column_id, process_portfolio_type = @portfolioType, calendar_type = @calendarType, calendar_start_id = @calendarStartId, calendar_end_id=@calendarEndId, show_progress_arrows = @showProgressArrows, hidden_progress_arrows = @hiddenProgressArrows, open_split = @openSplit, default_state = @defaultState, recursive = @recursive, recursive_filter_type = @recursiveFilterType, ignore_rights = @ignoreRights, timemachine = @timemachine, use_default_state = @useDefaultState, order_item_column_id = @orderItemColumnId, order_direction = @orderDirection, download_options = @download_options, also_open_current = @also_open_current, sub_default_state = @subDefaultState, openwidth = @openWidth, portfolio_default_view = @dtf WHERE instance = @instance AND process = @process AND process_portfolio_id = @processPortfolioId",
				DBParameters);
			var oldVar =
				db.ExecuteScalar(
					"SELECT variable FROM process_portfolios WHERE instance = @instance AND process = @process AND process_portfolio_id = @processPortfolioId",
					DBParameters);
			if (oldVar != null && oldVar != DBNull.Value &&
			    processPortfolio.LanguageTranslation.ContainsKey(authenticatedSession.locale_id) &&
			    processPortfolio.LanguageTranslation[authenticatedSession.locale_id] != null) {
				foreach (var translation in processPortfolio.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string) oldVar, translation.Value,
						translation.Key);
				}
			} else if (oldVar == DBNull.Value &&
			           processPortfolio.LanguageTranslation.ContainsKey(authenticatedSession.locale_id) &&
			           processPortfolio.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVal = translations.GetTranslationVariable(process,
					processPortfolio.LanguageTranslation[authenticatedSession.locale_id], "PORT");
				SetDBParam(SqlDbType.NVarChar, "@variable", newVal);
				db.ExecuteUpdateQuery(
					"UPDATE process_portfolios SET variable = @variable WHERE instance = @instance AND process = @process AND process_portfolio_id = @processPortfolioId",
					DBParameters);

				foreach (var translation in processPortfolio.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVal, translation.Value, translation.Key);
				}
			}

			if (processPortfolio.ProcessGroupIds != null) {
				SetDBParam(SqlDbType.Int, "@processPortfolioId", processPortfolio.ProcessPortfolioId);
				db.ExecuteDeleteQuery(
					"DELETE FROM process_portfolio_groups WHERE process_portfolio_id = @processPortfolioId",
					DBParameters);
				foreach (var id in processPortfolio.ProcessGroupIds) {
					SetDBParam(SqlDbType.Int, "@groupId", id);
					db.ExecuteInsertQuery(
						"INSERT INTO process_portfolio_groups (process_portfolio_id, process_group_id) VALUES(@processPortfolioId, @groupId)",
						DBParameters);
				}
			}

			if (processPortfolio.ProcessUserGroupIds != null) {
				SetDBParam(SqlDbType.Int, "@processPortfolioId", processPortfolio.ProcessPortfolioId);
				db.ExecuteDeleteQuery(
					"DELETE FROM  process_portfolio_user_groups WHERE process_portfolio_id = @ProcessPortfolioId",
					DBParameters);
				foreach (var id in processPortfolio.ProcessUserGroupIds) {
					SetDBParam(SqlDbType.Int, "@groupId", id);
					db.ExecuteInsertQuery(
						"INSERT INTO process_portfolio_user_groups (process_portfolio_id, item_id) VALUES(@processPortfolioId, @groupId)",
						DBParameters);
				}
			}

			ClearCache();
			Cache.Remove(instance, "SelectorColumnNames_" + process);
		}

		public void CopyPortfolio(int fromCopyId, ProcessPortfolio processPortfolio) {
			SetDBParam(SqlDbType.Int, "@fromCopyId", fromCopyId);
			var np = new ProcessPortfolios(instance, process, authenticatedSession);
			var newId = np.Add(processPortfolio);
			var copyGroups =
				"SELECT * FROM process_portfolio_columns_groups WHERE process = @process AND process_portfolio_id = @fromCopyId";
			//var copyColumns = "SELECT * FROM process_portfolio_columns WHERE process_portfolio_id = @fromCopyId";
			var ppc = new ProcessPortfolioColumns(instance, process, authenticatedSession);
			var oldNewIdPairs = new List<KeyValuePair<int, int>>();

			foreach (DataRow row in db.GetDatatable(copyGroups, DBParameters).Rows) {
				SetDBParam(SqlDbType.Int, "@oldGroupId", Conv.ToInt(row["portfolio_column_group_id"]));

				var newGroup = new ProcessPortfolioColumnGroup(row, authenticatedSession);

				newGroup.ProcessPortfolioId = newId;
				newGroup.ShowType = Conv.ToInt(row["show_type"]);
				var newest = ppc.AddGroup(process, newGroup);
				oldNewIdPairs.Add(new KeyValuePair<int, int>(Conv.ToInt(row["portfolio_column_group_id"]),
					newest.ProcessPortfolioColumnGroupId));
			}

			foreach (DataRow portColumnRow in db.GetDatatable(
				"SELECT process_portfolio_column_id, process_portfolio_id, ic_validation, order_no, use_in_select_result, short_name, report_column, validation, portfolio_column_group_id, COALESCE(show_type, (SELECT portfolio_show_type FROM processes WHERE process = data_additional), 0) as show_type_combined, show_type, use_in_filter, use_in_search, archive_item_column_id, width, priority, sum_column, instance, process, ppc.item_column_id, name, variable, data_type, system_column, data_additional, relative_column_id, data_additional2, input_method, input_name, status_column, process_action_id, link_process, process_dialog_id, check_conditions, use_lucene,use_fk, unique_col, is_default, parent_select_type, in_conditions, sub_data_additional2 FROM process_portfolio_columns ppc INNER JOIN item_columns ic ON ppc.item_column_id = ic.item_column_id WHERE process_portfolio_id = @fromCopyId AND ic.name IS NOT NULL AND LEN(ic.name) > 0 ORDER BY order_no  ASC",
				DBParameters).Rows) {
				var pc = new ProcessPortfolioColumn(portColumnRow, authenticatedSession);
				foreach (var idPair in oldNewIdPairs) {
					if (idPair.Key == Conv.ToInt(portColumnRow["portfolio_column_group_id"])) {
						pc.ProcessPortfolioColumnGroupId = idPair.Value;
					}
				}

				pc.ProcessPortfolioId = newId;
				ppc.Add(pc);
			}

			ClearCache();
		}

		public ProcessPortfolio GetPortfolio(int processPortfolioId) {
			var result = new List<ProcessPortfolio>();

			var f = new common.Util.Filter();
			f.Add("process_portfolio_id", processPortfolioId);

			var dt = processPortfolios.Select(f.ToString());

			if (dt.Length <= 0)
				throw new CustomArgumentException("Column cannot be found with process_portfolio_id " +
				                                  processPortfolioId +
				                                  " AND instance '" + instance + "' AND process '" + process +
				                                  "'. This could be caused by forgetting to add a selector/portfolio in this item column.");

			return new ProcessPortfolio(dt[0], authenticatedSession) {
				ProcessGroupIds = GetPortfolioGroups(processPortfolioId)
			};
		}


		public ProcessPortfolio GetPortfolioById(string variable) {
			SetDBParam(SqlDbType.NVarChar, "variable", variable);

			var f = new common.Util.Filter();
			f.Add("variable", variable);
			f.Add("instance", instance);
			f.Add("process", process);

			var dt = processPortfolios.Select(f.ToString());

			if (dt.Length > 0) return new ProcessPortfolio(dt[0], authenticatedSession);

			throw new CustomArgumentException("Column cannot be found with variable '" + variable + "' AND instance '" +
			                                  instance + "' AND process '" + process + "'");
		}


		public List<int> GetUserGroups(int ProcessPortfolioId) {
			var retval = new List<int>();
			SetDBParam(SqlDbType.Int, "@ProcessPortfolioId", ProcessPortfolioId);
			foreach (DataRow dr in db
				.GetDatatable(
					"SELECT item_id FROM process_portfolio_user_groups WHERE process_portfolio_id = @ProcessPortfolioId",
					DBParameters).Rows) {
				retval.Add(Conv.ToInt(dr["item_id"]));
			}

			return retval;
		}

		private List<int> GetPortfolioGroups(int processPortfolioId) {
			var result = new List<int>();
			foreach (DataRow row in processPortfolioGroups.Select("process_portfolio_id = " + processPortfolioId)) {
				result.Add(Conv.ToInt(row["process_group_id"]));
			}

			return result;
		}

		public void SaveUserGroups(int ProcessPortfolioId, List<int> selections) {
			SetDBParam(SqlDbType.Int, "@ProcessPortfolioId", ProcessPortfolioId);
			db.ExecuteDeleteQuery(
				"DELETE FROM  process_portfolio_user_groups WHERE process_portfolio_id = @ProcessPortfolioId",
				DBParameters);
			foreach (var id in selections) {
				SetDBParam(SqlDbType.Int, "@itemId", id);
				db.ExecuteInsertQuery(
					"INSERT INTO process_portfolio_user_groups (process_portfolio_id, item_id) VALUES(@ProcessPortfolioId, @itemId)",
					DBParameters);
			}
		}

		public int GetDefaultSelectorId() {
			return Conv.ToInt(db.ExecuteScalar(
				"SELECT COALESCE(MIN(process_portfolio_id), 0) FROM process_portfolios WHERE instance = @instance AND process = @process and (process_portfolio_type = 1 OR process_portfolio_type = 10) ",
				DBParameters));
		}

		public List<string> GetDefaultSelectorColumnNames() {
			var c = Cache.Get(instance, "SelectorColumnNames_" + process);
			if (c != null) return (List<string>) c;

			var sql = "SELECT ic.name FROM process_portfolio_columns pc " +
			          "LEFT JOIN item_columns ic ON ic.item_column_id = pc.item_column_id " +
			          "WHERE process_portfolio_id = " + GetDefaultSelectorId();

			var result = new List<string>();
			foreach (var col in db.GetDatatableDictionary(sql, DBParameters)) {
				result.Add(Conv.ToStr(col["name"]));
			}

			Cache.Set(instance, "SelectorColumnNames_" + process, result);

			return result;
		}

		public List<TimeMachineSaved> GetSavedTimeMachines(string process) {
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@process", process);

			var result = new List<TimeMachineSaved>();

			foreach (DataRow r in db
				.GetDatatable(
					"SELECT * FROM process_portfolio_timemachine_saved WHERE process = @process AND instance = @instance",
					DBParameters).Rows) {
				var saved = new TimeMachineSaved(r);
				result.Add(saved);
			}

			return result;
		}

		public TimeMachineSaved AddNewTimeMachineSave(TimeMachineSaved newMachine) {
			SetDBParam(SqlDbType.NVarChar, "@instance", instance);
			SetDBParam(SqlDbType.NVarChar, "@process", newMachine.Process);
			SetDBParam(SqlDbType.NVarChar, "@name", newMachine.Name);
			SetDBParam(SqlDbType.Date, "@date", newMachine.Date);

			db.ExecuteInsertQuery(
				"INSERT INTO process_portfolio_timemachine_saved (instance, name, process, date) VALUES(@instance, @name, @process, @date)",
				DBParameters);

			newMachine.SaveId = Conv.ToInt(
				db.ExecuteScalar(
					"SELECT save_id FROM process_portfolio_timemachine_saved WHERE name = @name AND instance = @instance AND process = @process AND date = @date",
					DBParameters));

			return newMachine;
		}

		public void ClonePortfolioChart(ProcessPortfoliosRest.ChartClone chartClone, string process) {

			var transVariable = translations.GetTranslationVariable(process,
				chartClone.Translation[authenticatedSession.locale_id],
				"DASHCH");

			SetDBParam(SqlDbType.Int, "@chartId", chartClone.ChartId);
			SetDBParam(SqlDbType.Int, "@portfolioId", chartClone.ToPortfolioId);
			SetDBParam(SqlDbType.NVarChar, "@process", process);

			SetDBParam(SqlDbType.NVarChar, "@variable",
				transVariable);


			var newChartId = Conv.ToInt(db.ExecuteInsertQuery("EXEC app_set_archive_user_id 1 " +
			                                                  "INSERT INTO process_dashboard_charts " +
			                                                  "(instance, process, name, json_data, process_portfolio_id, variable) " +
			                                                  "SELECT instance, process, name, json_data, @portfolioId, @variable " +
			                                                  "FROM process_dashboard_charts " +
			                                                  "WHERE process_dashboard_chart_id = @chartId",
				DBParameters));

			SetDBParam(SqlDbType.Int, "@newChartId", newChartId);

			SetDBParam(SqlDbType.Int, "@dashboardId", chartClone.ToDashboardId);
			SetDBParam(SqlDbType.Int, "@fromPortfolioId", chartClone.FromPortfoliodId);
			SetDBParam(SqlDbType.Int, "@fromDashboardId", chartClone.FromDashboardId);

			SetDBParam(SqlDbType.NVarChar, "@chartAsNvarchar", Conv.ToStr(chartClone.ChartId));
			SetDBParam(SqlDbType.NVarChar, "@chartAsNvarchar2", Conv.ToStr(newChartId));

			db.ExecuteInsertQuery("EXEC app_set_archive_user_id 1 INSERT INTO process_dashboard_chart_links" +
				"(instance, process, dashboard_id, source_field, link_process, link_process_field, chart_ids)" +
			"SELECT instance, @process, @dashboardId, source_field, link_process, link_process_field, @chartAsNvarchar2 " +
			"FROM process_dashboard_chart_links " +
			"WHERE chart_ids = @chartAsNvarchar AND dashboard_id = @fromDashboardId", DBParameters);

			db.ExecuteInsertQuery("EXEC app_set_archive_user_id 1 INSERT INTO process_dashboard_chart_map" +
			                      "(process_dashboard_chart_id, process_dashboard_id, order_no) VALUES(@newChartId, @dashboardId, ISNULL((SELECT dbo.getOrderBetween((SELECT TOP 1 order_no FROM process_dashboard_chart_map WHERE process_dashboard_id = @dashboardId ORDER BY order_no  DESC),null)),'U'))",
				DBParameters);


			foreach (var translation in chartClone.Translation) {
				translations.insertOrUpdateProcessTranslation(process, transVariable, translation.Value, translation.Key);
			}

		}
	}

	public class ProcessPortfoliosDialog : ProcessBase {
		public ProcessPortfoliosDialog(string instance, string process, AuthenticatedSession authenticatedSession) :
			base(instance,
				process, authenticatedSession) { }

		public List<ProcessPortfolioDialog> GetPortfolioDialog() {
			var retval = new List<ProcessPortfolioDialog>();
			foreach (DataRow dr in db.GetDatatable(
				"SELECT process_portfolio_dialog_id, process_container_id, order_no, variable FROM process_portfolio_dialogs WHERE instance = @instance AND process = @process ORDER BY order_no  ASC",
				DBParameters).Rows) {
				retval.Add(new ProcessPortfolioDialog(dr, authenticatedSession));
			}

			return retval;
		}

		public Dictionary<string, object> GetPortfolioDialogContainers(int containerId = 0) {
			var retval = new Dictionary<string, object>();
			var inputs = new List<List<ProcessContainerField>>();
			var texts = new List<string>();

			var listCols = new Dictionary<int, ItemColumn>();
			var list = new Processes(instance, authenticatedSession);
			string sql = "";
			if (containerId == 0) {
				sql =
					"SELECT process_portfolio_dialog_id, process_container_id, order_no, variable FROM process_portfolio_dialogs WHERE instance = @instance AND process = @process ORDER BY order_no  ASC";
			} else {
				SetDBParameter("@containerId", containerId);
				sql =
					"SELECT process_portfolio_dialog_id, process_container_id, order_no, variable FROM process_portfolio_dialogs WHERE process_container_id = @containerId AND instance = @instance AND process = @process ORDER BY order_no  ASC";
			}

			foreach (DataRow dr in db.GetDatatable(
				sql,
				DBParameters).Rows) {
				if ((int) dr["process_container_id"] > 0) {
					var containerColumns = new ProcesContainerColumns(instance, process, authenticatedSession);
					var containerFields = containerColumns.GetFields((int) dr["process_container_id"]);
					inputs.Add(containerFields);

					if (dr["variable"] != DBNull.Value) {
						texts.Add(
							new Translations(authenticatedSession.instance, authenticatedSession).GetTranslation(
								(string) dr["variable"]));
					} else {
						texts.Add("");
					}


					foreach (var col in containerFields) {
						var column = col.ItemColumn;
						if (column.IsList() && column.DataAdditional != null &&
						    !listCols.ContainsKey(column.ItemColumnId)) {
							listCols.Add(column.ItemColumnId, column);
						}
					}
				}
			}

			var sublists = list.GetListHierarchy(process, listCols, false);
			foreach (var key in sublists.Keys) {
				retval.Add(key, sublists[key]);
			}

			retval.Add("inputs", inputs);
			retval.Add("texts", texts);


			return retval;
		}

		public Dictionary<string, object> GetPortfolioDialogContainersForMeta(int containerId) {
			var retval = new Dictionary<string, object>();
			var inputs = new List<List<ProcessContainerField>>();
			var texts = new List<string>();

			var listCols = new Dictionary<int, ItemColumn>();
			var list = new Processes(instance, authenticatedSession);

			var containerColumns = new ProcesContainerColumns(instance, process, authenticatedSession);
			var containerFields = containerColumns.GetFields(containerId);
			inputs.Add(containerFields);

			foreach (var col in containerFields) {
				var column = col.ItemColumn;
				if (column.IsList() && column.DataAdditional != null &&
				    !listCols.ContainsKey(column.ItemColumnId)) {
					listCols.Add(column.ItemColumnId, column);
				}
			}

			var sublists = list.GetListHierarchy(process, listCols, false);
			foreach (var key in sublists.Keys) {
				retval.Add(key, sublists[key]);
			}

			retval.Add("inputs", inputs);
			retval.Add("texts", texts);

			return retval;
		}

		public ProcessPortfolioDialog AddPortfolioDialog(ProcessPortfolioDialog portfolioDialog) {
			SetDBParam(SqlDbType.Int, "@processContainerId", portfolioDialog.ProcessContainerId);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", portfolioDialog.OrderNo);
			var new_id =
				db.ExecuteInsertQuery(
					"INSERT INTO process_portfolio_dialogs (instance, process, process_container_id, order_no) VALUES(@instance, @process, @processContainerId, @orderNo)",
					DBParameters);
			SetDBParam(SqlDbType.Int, "@newId", new_id);

			if (portfolioDialog.LanguageTranslation != null &&
			    portfolioDialog.LanguageTranslation[authenticatedSession.locale_id] != null) {
				object oldVar = translations.GetTranslationVariable(process,
					portfolioDialog.LanguageTranslation[authenticatedSession.locale_id], "PORTDG");
				SetDBParam(SqlDbType.NVarChar, "@variable", oldVar);
				SetDBParam(SqlDbType.Int, "@processPortfolioDialogId", new_id);

				db.ExecuteUpdateQuery(
					"UPDATE process_portfolio_dialogs SET variable = @variable WHERE instance = @instance AND process = @process AND process_portfolio_dialog_id = @processPortfolioDialogId",
					DBParameters);

				foreach (var translation in portfolioDialog.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string) oldVar, translation.Value,
						translation.Key);
				}

				portfolioDialog.Variable = (string) oldVar;
			}

			portfolioDialog.ProcessPortfolioDialogId = new_id;
			return portfolioDialog;
		}

		public ProcessPortfolioDialog SavePortfolioDialog(ProcessPortfolioDialog portfolioDialog) {
			SetDBParam(SqlDbType.Int, "@processPortfolioDialogId", portfolioDialog.ProcessPortfolioDialogId);
			SetDBParam(SqlDbType.Int, "@processContainerId", portfolioDialog.ProcessContainerId);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", portfolioDialog.OrderNo);
			db.ExecuteUpdateQuery(
				"UPDATE process_portfolio_dialogs SET process_container_id = @processContainerId, order_no = @orderNo WHERE instance = @instance AND process = @process AND process_portfolio_dialog_id = @processPortfolioDialogId",
				DBParameters);

			SetDBParam(SqlDbType.NVarChar, "@variable", portfolioDialog.Variable);

			var oldVar =
				db.ExecuteScalar(
					"SELECT variable FROM process_portfolio_dialogs WHERE instance = @instance AND process = @process AND process_portfolio_dialog_id = @ProcessPortfolioDialogId",
					DBParameters);
			if (oldVar != null && oldVar != DBNull.Value &&
			    portfolioDialog.LanguageTranslation[authenticatedSession.locale_id] != null) {
				foreach (var translation in portfolioDialog.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string) oldVar, translation.Value,
						translation.Key);
				}
			} else if (String.IsNullOrEmpty(Conv.ToStr(oldVar)) &&
			           portfolioDialog.LanguageTranslation[authenticatedSession.locale_id] != null) {
				oldVar = translations.GetTranslationVariable(process,
					portfolioDialog.LanguageTranslation[authenticatedSession.locale_id], "PORT");
				SetDBParam(SqlDbType.NVarChar, "@variable", oldVar);
				db.ExecuteUpdateQuery(
					"UPDATE process_portfolio_dialogs SET variable = @variable WHERE instance = @instance AND process = @process AND process_portfolio_dialog_id = @processPortfolioDialogId",
					DBParameters);

				foreach (var translation in portfolioDialog.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string) oldVar, translation.Value,
						translation.Key);
				}
			}

			return portfolioDialog;
		}


		public void DeletePortfolioDialogColumn(int ProcessPortfolioContainerId) {
			SetDBParam(SqlDbType.Int, "@processPortfolioContainerId", ProcessPortfolioContainerId);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_portfolio_dialogs WHERE process_portfolio_dialog_id = @processPortfolioContainerId",
				DBParameters);
		}
	}

	public class ProcessPortfolioActions : ProcessBase {
		public ProcessPortfolioActions(string instance, string process, AuthenticatedSession authenticatedSession) :
			base(instance,
				process, authenticatedSession) { }

		public List<ProcessPortfolioAction> GetPortfolioActions(int processPortfolioId) {
			SetDBParameter("@processPortfolioId", processPortfolioId);
			var rows = db.GetDatatable(
				"SELECT * FROM process_portfolio_actions WHERE process_portfolio_id = @processPortfolioId",
				DBParameters).Rows;

			var result = new List<ProcessPortfolioAction>();
			foreach (DataRow row in rows) {
				result.Add(new ProcessPortfolioAction(row));
			}

			return result;
		}

		public ProcessPortfolioAction GetPortfolioAction(int processPortfolioActionId) {
			SetDBParameter("@processPortfolioActionId", processPortfolioActionId);
			var row = db.GetDataRow(
				"SELECT * FROM process_portfolio_actions WHERE process_portfolio_action_id = @processPortfolioActionId",
				DBParameters);
			return new ProcessPortfolioAction(row);
		}

		public void DeleteProcessPortfolioAction(int processPortfolioActionId) {
			SetDBParameter("@processPortfolioActionId", processPortfolioActionId);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_portfolio_actions WHERE process_portfolio_action_id = @processPortfolioActionId",
				DBParameters);
		}

		public void UpdatePortfolioAction(ProcessPortfolioAction action) {
			SetDBParameter("@processPortfolioActionId", action.PortfolioActionId);
			SetDBParameter("@button_text_variable", action.ButtonName);
			SetDBParameter("@process_action_id", action.ActionId);
			db.ExecuteUpdateQuery(
				"UPDATE process_portfolio_actions SET button_text_variable = @button_text_variable, process_action_id = @process_action_id WHERE process_portfolio_action_id = @processPortfolioActionId",
				DBParameters);
		}

		public ProcessPortfolioAction AddPortfolioAction(int processPortfolioId) {
			SetDBParameter("@processPortfolioId", processPortfolioId);
			var id = db.ExecuteInsertQuery(
				"INSERT INTO process_portfolio_actions (process_portfolio_id, process_action_id) VALUES (@processPortfolioId, 0)",
				DBParameters);
			return GetPortfolioAction(id);
		}
	}

	public class ProcessPortfolioAction {
		public string ButtonName { get; set; }
		public int PortfolioActionId { get; set; }
		public int ActionId { get; set; }
		public int PortfolioId { get; set; }

		public ProcessPortfolioAction() { }

		public ProcessPortfolioAction(DataRow row) {
			this.PortfolioActionId = Conv.ToInt(row["process_portfolio_action_id"]);
			this.ButtonName = Conv.ToStr(row["button_text_variable"]);
			this.ActionId = Conv.ToInt(row["process_action_id"]);
			this.PortfolioId = Conv.ToInt(row["process_portfolio_id"]);
		}
	}

	public class TimeMachineSaved {
		public int SaveId { get; set; }
		public string Instance { get; set; }
		public string Name { get; set; }
		public string Process { get; set; }
		public DateTime? Date { get; set; }

		public TimeMachineSaved() { }

		public TimeMachineSaved(DataRow row) {
			SaveId = Conv.ToInt(row["save_id"]);
			Instance = Conv.ToStr(row["instance"]);
			Name = Conv.ToStr(row["name"]);
			Process = Conv.ToStr(row["process"]);
			Date = Conv.ToDateTime(row["date"]);
		}
	}
}