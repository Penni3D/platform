﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.portfolios {
	[Route("v1/meta/ProcessPortfolios")]
	public class ProcessPortfoliosRest : PageBase {
		public ProcessPortfoliosRest() : base("settings") { }

		[HttpGet("{process}/{portfolioType}")]
		public List<ProcessPortfolio> Get(string process, int portfolioType) {
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			return portfolios.GetPortfolios(process, portfolioType);
		}

		[HttpGet("{process}")]
		public List<ProcessPortfolio> GetProcessPortfolio(string process) {
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			return portfolios.GetPortfolios(process);
		}

		// GET: model/values
		[HttpGet]
		public List<ProcessPortfolio> Get() {
			var portfolios = new ProcessPortfolios(instance, "", currentSession);
			return portfolios.GetPortfolios();

			//return new List<ProcessPortfolio>();
		}

		// GET: model/values
		[HttpGet("default")]
		public Dictionary<string, object> GetDefaultPortfolios() {
			var portfolios = new ProcessPortfolios(instance, "", currentSession);
			return portfolios.GetDefaultPortfolios();
		}

		[HttpGet("{process}/{portfolioType}/{processPortfolioId}")]
		public ProcessPortfolio Get(string process, int portfolioType, int processPortfolioId) {
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			return portfolios.GetPortfolio(processPortfolioId);
		}

		[HttpGet("{process}/{variable}")]
		public ProcessPortfolio Get(string process, string variable) {
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			return portfolios.GetPortfolioById(variable);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] ProcessPortfolio portfolio) {
			CheckRight("write");
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			if (ModelState.IsValid) {
				portfolios.Add(portfolio);
				return new ObjectResult(portfolio);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPost("copy/{process}/{fromCopyId}")]
		public IActionResult PostCopy(string process, int fromCopyId, [FromBody] ProcessPortfolio portfolio) {
			CheckRight("write");
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			if (ModelState.IsValid) {
				portfolios.CopyPortfolio(fromCopyId, portfolio);
				return new ObjectResult(portfolio);
			}

			return null;
		}

		[HttpPost("cloneChart/{process}")]
		public void ClonePortfolioChart(string process, [FromBody] ChartClone chartClone) {
			var portfolios = new ProcessPortfolios(instance, "", currentSession);
			portfolios.ClonePortfolioChart(chartClone, process);
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<ProcessPortfolio> portfolio) {
			CheckRight("write");
			var portfolios = new ProcessPortfolios(instance, process, currentSession);

			foreach (var pp in portfolio) {
				portfolios.Save(pp);
			}

			return new ObjectResult(portfolio);
		}

		[HttpDelete("{process}/{processPortfolioId}")]
		public IActionResult Delete(string process, string processPortfolioId) {
			CheckRight("delete");
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			foreach (var id in processPortfolioId.Split(',')) {
				portfolios.Delete(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}

		[HttpGet("{process}/savedTimeMachines")]
		public List<TimeMachineSaved> GetSavedTimeMachines(string process) {
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			return portfolios.GetSavedTimeMachines(process);
		}

		[HttpPost("{process}/savedTimeMachines")]
		public TimeMachineSaved GetSavedTimeMachines(string process, [FromBody] TimeMachineSaved newMachine) {
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			return portfolios.AddNewTimeMachineSave(newMachine);
		}


		public class ChartClone
		{
			public int ChartId { get; set; }
			public int ToPortfolioId { get; set; }

			public int ToDashboardId { get; set; }

			public int FromDashboardId { get; set; }

			public int FromPortfoliodId { get; set; }

			public Dictionary<string,string> Translation { get; set; }
		}


	}

	[Route("v1/settings/ProcessPortfolioActions")]
	public class ProcessPortfolioActionsRest : PageBase {
		public ProcessPortfolioActionsRest() : base("settings") { }

		[HttpGet("{process}/{portfolioId}")]
		public List<ProcessPortfolioAction> Get(string process, int portfolioId) {
			var pa = new ProcessPortfolioActions(instance, process, currentSession);
			return pa.GetPortfolioActions(portfolioId);
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<ProcessPortfolioAction> actions) {
			CheckRight("write");
			var pa = new ProcessPortfolioActions(instance, process, currentSession);
			foreach (var action in actions) { 
				pa.UpdatePortfolioAction(action);
			}
			return new StatusCodeResult(200);
		}

		[HttpPost("{process}/{portfolioId}")]
		public ProcessPortfolioAction Post(string process, int portfolioId) {
			CheckRight("write");
			var pa = new ProcessPortfolioActions(instance, process, currentSession);
			return pa.AddPortfolioAction(portfolioId);
		}

		[HttpDelete("{process}/{portfolioActionId}")]
		public IActionResult Delete(string process, int portfolioActionId) {
			CheckRight("delete");
			var pa = new ProcessPortfolioActions(instance, process, currentSession);
			pa.DeleteProcessPortfolioAction(portfolioActionId);
			return new StatusCodeResult(200);
		}
	}

	[Route("v1/meta/[controller]")]
	public class ProcessPortfolioUserGroups : PageBase {
		public ProcessPortfolioUserGroups() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}/{ProcessGroupId}")]
		public List<int> Get(string process, int ProcessGroupId) {
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			return portfolios.GetUserGroups(ProcessGroupId);
		}

		[HttpPut("{process}/{portfolioId}")]
		public IActionResult Put(string process, int portfolioId, [FromBody] List<int> selections) {
			CheckRight("write");
			var portfolios = new ProcessPortfolios(instance, process, currentSession);
			if (ModelState.IsValid) {
				portfolios.SaveUserGroups(portfolioId, selections);
				return new ObjectResult(selections);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}
	}


	[Route("v1/settings/[controller]")]
	public class ProcessPortfolioDialogs : PageBase {
		public ProcessPortfolioDialogs() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessPortfolioDialog> Get(string process) {
			var portfolios = new ProcessPortfoliosDialog(instance, process, currentSession);
			return portfolios.GetPortfolioDialog();
		}

		[HttpGet("{process}/dialog")]
		public Dictionary<string, object> Get(string process, string dialog) {
			var portfolios = new ProcessPortfoliosDialog(instance, process, currentSession);
			return portfolios.GetPortfolioDialogContainers();
		}

		[HttpGet("{process}/dialog/{dialogId}")]
        		public Dictionary<string, object> GetSpecificContainer(string process, string dialog, int dialogId) {
				var portfolios = new ProcessPortfoliosDialog(instance, process, currentSession);
				return portfolios.GetPortfolioDialogContainers(dialogId);
		}

		[HttpGet("{process}/{containerId}/dialog")]
		public Dictionary<string, object> GetContainerForMetaDialog(string process, int containerId) {
			var portfolios = new ProcessPortfoliosDialog(instance, process, currentSession);
			return portfolios.GetPortfolioDialogContainersForMeta(containerId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody] List<ProcessPortfolioDialog> portfolioDialog) {
			CheckRight("write");
			var portfolios = new ProcessPortfoliosDialog(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pd in portfolioDialog) {
					portfolios.AddPortfolioDialog(pd);
				}

				return new ObjectResult(portfolioDialog);
			}

			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] List<ProcessPortfolioDialog> portfolioDialog) {
			CheckRight("write");
			var portfolios = new ProcessPortfoliosDialog(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pd in portfolioDialog) {
					portfolios.SavePortfolioDialog(pd);
				}

				return new ObjectResult(portfolioDialog);
			}

			return null;
		}

		[HttpDelete("{process}/{processPortfolioContainerId}")]
		public IActionResult Delete(string process, int processPortfolioContainerId) {
			CheckRight("delete");
			var portfolios = new ProcessPortfoliosDialog(instance, process, currentSession);

			portfolios.DeletePortfolioDialogColumn(processPortfolioContainerId);

			return new StatusCodeResult(200);
		}
	}
}