﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.layout;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.portfolios {
	[Route("v1/settings/[controller]")]
	public class ProcessPortfolioGroups : PageBase {
		public ProcessPortfolioGroups() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}/{portfolioId}")]
		public List<ProcessGroup> Get(string process, int portfolioId) {
			var groups = new layout.ProcessPortfolioGroups(instance, process, currentSession);
			return groups.GetGroups(portfolioId);
		}

		[HttpPost("{process}/{portfolioId}")]
		public IActionResult Post(string process, int portfolioId, [FromBody]List<ProcessGroup> groups) {
			CheckRight("write");
			var portfolioGroups = new layout.ProcessPortfolioGroups(instance, process, currentSession);
			portfolioGroups.Add(portfolioId, groups);
			return new ObjectResult(groups);
		}

		[HttpPut("{process}/{portfolioId}")]
		public IActionResult Put(string process, int portfolioId, [FromBody]List<ProcessGroup> groups) {
			CheckRight("write");
			var portfolioGroups = new layout.ProcessPortfolioGroups(instance, process, currentSession);
			portfolioGroups.Add(portfolioId, groups);
			return new ObjectResult(groups);
		}

		[HttpDelete("{process}/{portfolioId}/{processGroupId}")]
		public IActionResult Delete(string process, int portfolioId, string processGroupId) {
			CheckRight("delete");
			var portfolioGroups = new layout.ProcessPortfolioGroups(instance, process, currentSession);
			foreach (var id in processGroupId.Split(',')) {
				portfolioGroups.Delete(portfolioId, Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}
}