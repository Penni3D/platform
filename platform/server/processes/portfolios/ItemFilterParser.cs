﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.portfolios {
	public class ItemFilterParser {
		private readonly Dictionary<int, List<int>> _cols = new Dictionary<int, List<int>>();
		private readonly Dictionary<int, object> _filters;
		private readonly IItemColumns _itemColumns;

		private readonly Dictionary<int, List<int>> _selectFieldFilters = new Dictionary<int, List<int>>();
		private readonly Dictionary<int, List<int>> _subQueryFieldFilters = new Dictionary<int, List<int>>();
		private readonly Dictionary<int, List<string>> _subQueryValueFieldFilters = new Dictionary<int, List<string>>();
		private readonly Dictionary<int, List<string>> _equationFieldFilters = new Dictionary<int, List<string>>();
		private readonly Dictionary<int, List<string>> _tagsFilters = new Dictionary<int, List<string>>();
		private readonly Dictionary<string, List<string>> _valueFieldFilters = new Dictionary<string, List<string>>();
		private readonly string _instance;
		private readonly bool _isRestrictionFilter;

		private readonly string _process;

		private readonly AuthenticatedSession _session;

		public ItemFilterParser(Dictionary<int, object> filters, IItemColumns itemColumns, string instance,
			string process, AuthenticatedSession session, bool isRestrictionFilter = false) {
			_filters = filters;
			if (itemColumns == null) {
				_itemColumns = new ItemColumns(instance, process, session);
			} else {
				_itemColumns = itemColumns;
			}

			_instance = instance;
			_process = process;
			_session = session;
			_isRestrictionFilter = isRestrictionFilter;
			Parse();
		}


		public ItemFilterParser(string f, IItemColumns itemColumns, string instance, string process,
			AuthenticatedSession session) {
			_filters = FilterStrToFilters(f);
			_itemColumns = itemColumns ?? new ItemColumns(instance, process, session);
			_instance = instance;
			_process = process;
			_session = session;
			Parse();
		}

		private static Dictionary<int, object> FilterStrToFilters(string f) {
			var filters = new Dictionary<int, object>();
			if (string.IsNullOrEmpty(f)) return filters;
			foreach (var ff in f.Split(',')) {
				var fff = ff.Split(';');
				var strList = fff[1].Split('-').ToList();
				filters.Add(Conv.ToInt(fff[0]), strList);
			}
			return filters;
		}

		public string Get() {
			var filterWhereSql = "";
			foreach (var selectFieldFilter in _selectFieldFilters.Keys) {
				var idsInString = string.Join(", ", _selectFieldFilters[selectFieldFilter]);
				var ic = _itemColumns.GetItemColumn(selectFieldFilter);

				if (ic.InputMethod == 3) {
					if (idsInString != "")
						filterWhereSql = filterWhereSql +
						                 " AND pf.item_id IN (SELECT selected_item_id FROM item_data_process_selections " +
						                 "WHERE item_column_id = " + ic.RelativeColumnId +
						                 " AND item_id IN (" + idsInString + ")) ";
				} else {
					if (idsInString != "")
						filterWhereSql = filterWhereSql +
						                 " AND pf.item_id IN (SELECT item_id FROM item_data_process_selections " +
						                 "WHERE item_column_id = " + selectFieldFilter +
						                 " AND selected_item_id IN (" + idsInString + ")) ";
				}
			}

			foreach (var tagFilterKey in _tagsFilters.Keys) {
				var tagsString = string.Join(", ", _tagsFilters[tagFilterKey]);

				filterWhereSql = filterWhereSql +
				                 " AND  (SELECT COUNT(tag_name) FROM item_data_process_selections idps " +
				                 " INNER JOIN _" + _instance +
				                 "_instance_tags iit ON idps.selected_item_id = iit.item_id " +
				                 "WHERE item_column_id = " + tagFilterKey +
				                 " AND idps.item_id = pf.item_id AND tag_name IN (" + tagsString + "))  > 0 ";
			}

			var fSql = "";
			foreach (var valueKey in _valueFieldFilters.Keys) {
				var values = _valueFieldFilters[valueKey];

				if (values.Count == 1 && string.IsNullOrEmpty(values[0])) {
					continue;
				}

				if (_isRestrictionFilter) {
					if (values.Count > 0) {
						fSql += " AND (  1 = 0 ";
						foreach (var val in values) {
							fSql += " OR " + Conv.ToSql(valueKey) + " = '" + Conv.Escape(val) + "'";
						}

						fSql += " ) ";
					}
				} else {
					var ic = _itemColumns.GetItemColumnByName(valueKey);
					if (!string.IsNullOrEmpty(values[0])) {
						var coalesceVal = "0";
						if (values[0].StartsWith("'")) {
							coalesceVal = "''";
						}

						if (ic != null && (ic.IsOfSubType(ItemColumn.ColumnType.Date) || ic.IsDate() ||
						                   ic.IsDateTime())) {
							fSql += " AND (" + valueKey + ") >= '" + values[0] + "'";
						} else {
							if (values.Count == 1) {
								fSql += " AND COALESCE(" + valueKey + ", " + coalesceVal + ") = '" + values[0] + "'";
							} else {
								fSql += " AND COALESCE(" + valueKey + ", " + coalesceVal + ") >= '" + values[0] + "'";
							}
						}
					}

					if (values.Count == 2 && !string.IsNullOrEmpty(values[1])) {
						var coalesceVal = "0";
						if (values[1].StartsWith("'")) {
							coalesceVal = "''";
						}

						if (ic != null && (ic.IsOfSubType(ItemColumn.ColumnType.Date) || ic.IsDate() ||
						                   ic.IsDateTime())) {
							fSql += " AND (" + valueKey + ") <= '" + values[1] + "'";
						} else {
							fSql += " AND COALESCE(" + valueKey + ", " + coalesceVal + ") <= '" + values[1] + "'";
						}
					}
				}
			}

			foreach (var f in _equationFieldFilters) {
				var ic = _itemColumns.GetItemColumn(f.Key);
				var idsInString = string.Join(", ", f.Value);

				var filterWhere = "";
				if (ic.SubDataType == 1) {
					var subs = idsInString.Split(',');
					var range1 = "";
					var range2 = "";
					if (subs[0].Length == 0) {
						range1 = "";
					} else {
						range1 = " AND " + _itemColumns.GetEquationQuery(ic) + " >= " + subs[0];
					}

					if (subs.ElementAtOrDefault(1) == null || subs.ElementAtOrDefault(1) == " ") {
						range2 = "";
					} else {
						range2 = " AND " + _itemColumns.GetEquationQuery(ic) + " <= " + subs[1];
					}
					filterWhere =  range1 + range2;
				} else {
					filterWhere = " AND (SELECT COUNT(Value) FROM STRING_SPLIT(CAST(" + _itemColumns.GetEquationQuery(ic) + " AS NVARCHAR),',') WHERE Value IN (" + idsInString + ") ) > 0 ";
				}
				filterWhereSql += filterWhere;
			}

			foreach (var subQueryFilter in _subQueryFieldFilters.Keys) {
				var ic = _itemColumns.GetItemColumn(subQueryFilter);
				var idsInString = string.Join(", ", _subQueryFieldFilters[subQueryFilter]);
				var query = RegisteredSubQueries.GetType(ic.DataAdditional, ic.ItemColumnId);
				var filterSql = query.GetFilterSql(_instance);
				var filterWhere = " AND  " + filterSql.Replace("##idString##", idsInString);
				if (idsInString != "") filterWhereSql += filterWhere;
			}

			foreach (var subQueryFilter in _subQueryValueFieldFilters.Keys) {
				var ic = _itemColumns.GetItemColumn(subQueryFilter);
				var ics = new ItemColumns(_instance, _process, _session);

				if (ic.IsOfSubType(ItemColumn.ColumnType.Int) || ic.IsOfSubType(ItemColumn.ColumnType.Float) ||
				    ic.IsOfSubType(ItemColumn.ColumnType.Date)) {
					var subq = ics.GetSubQuery(ic, false);

					var values = _subQueryValueFieldFilters[subQueryFilter];

					if (values.Count == 1 && string.IsNullOrEmpty(values[0])) {
						continue;
					}

					if (_isRestrictionFilter) {
						if (values.Count > 0) {
							fSql = " AND (  1 = 0 ";
							foreach (var val in values) {
								fSql += " OR " + subq + " = '" + val + "'";
							}

							fSql += " ) ";
						}
					} else {
						if (!string.IsNullOrEmpty(values[0])) {
							var coalesceVal = "0";
							if (values[0].StartsWith("'")) {
								coalesceVal = "''";
							}

							if (ic.IsOfSubType(ItemColumn.ColumnType.Date)) {
								fSql += " AND ((" + subq + ")) >= '" + values[0] + "'";
							} else {
								fSql += " AND COALESCE((" + subq + "), " + coalesceVal + ") >= " + values[0];
							}
						}

						if (values.Count == 2 && !string.IsNullOrEmpty(values[1])) {
							var coalesceVal = "0";
							if (values[1].StartsWith("'")) {
								coalesceVal = "''";
							}

							if (ic.IsOfSubType(ItemColumn.ColumnType.Date)) {
								fSql += " AND ((" + subq + ")) <= '" + values[1] + "'";
							} else {
								fSql += " AND COALESCE((" + subq + "), " + coalesceVal + ") <= " + values[1];
							}
						}
					}
				}
			}

			filterWhereSql += fSql;
			return filterWhereSql;
		}

		private void Parse() {
			if (_filters == null || _filters.Equals("") || _filters.Equals(" ")) return;
			foreach (var key in _filters.Keys) {
				List<object> fArr;

				try {
					fArr = JsonConvert.DeserializeObject<List<object>>(_filters[key].ToString());
				} catch (Exception e) {
					Diag.SupressException(e);
					var fArr2 = (List<string>) _filters[key];
					fArr = fArr2.ToList<object>();
				}

				if (key == 0) {
					if (!_valueFieldFilters.ContainsKey("item_id")) {
						_valueFieldFilters.Add("item_id", new List<string>());
					}

					//Replace item id with pf.item_id
					var temp = _valueFieldFilters["item_id"];
					_valueFieldFilters.Add("pf.item_id", temp);
					_valueFieldFilters.Remove("item_id");

					foreach (var f in fArr) {
						_valueFieldFilters["pf.item_id"].Add(Conv.ToStr(f));
					}

					continue;
				}

				var ic = _itemColumns.GetItemColumn(key);

				if (ic.IsList() || ic.IsProcess()) {
					if (!_selectFieldFilters.ContainsKey(key)) {
						_selectFieldFilters.Add(key, new List<int>());
					}

					foreach (var s in fArr) {
						_selectFieldFilters[key].Add(Conv.ToInt(s));
					}
				} else if ((ic.IsEquation() && ic.SubDataType == 6) ||
				           (ic.IsEquation() && ic.SubDataType == 5) || ic.IsEquation() && ic.SubDataType == 1 || ic.IsEquation() && ic.SubDataType == 2) {
					if (!_equationFieldFilters.ContainsKey(ic.ItemColumnId)) {
						_equationFieldFilters.Add(ic.ItemColumnId, new List<string>());
					}

					foreach (var f in fArr) {
						_equationFieldFilters[ic.ItemColumnId].Add(Conv.ToStr(f));
					}
				} else if (ic.IsNumber() || ic.IsFormula()) {
					if (!_valueFieldFilters.ContainsKey(ic.Name)) {
						_valueFieldFilters.Add(ic.Name, new List<string>());
					}

					foreach (var f in fArr) {
						_valueFieldFilters[ic.Name].Add(Conv.ToStr(f));
					}
				} else if (ic.IsDate() || ic.IsDateTime()) {
					if (!_valueFieldFilters.ContainsKey(ic.Name)) {
						_valueFieldFilters.Add(ic.Name, new List<string>());
					}

					foreach (var f in fArr) {
						_valueFieldFilters[ic.Name].Add(Conv.ToStr(f));
					}
				} else if (ic.IsSubquery() && (ic.IsOfSubType(ItemColumn.ColumnType.List) ||
				                               ic.IsOfSubType(ItemColumn.ColumnType.Process))) {
					if (!_subQueryFieldFilters.ContainsKey(key)) {
						_subQueryFieldFilters.Add(key, new List<int>());
					}

					foreach (var s in Conv.ToIntArray(_filters[key])) {
						_subQueryFieldFilters[key].Add(Conv.ToInt(s));
					}
				} else if (ic.IsSubquery()) {
					if (!_subQueryValueFieldFilters.ContainsKey(ic.ItemColumnId)) {
						_subQueryValueFieldFilters.Add(ic.ItemColumnId, new List<string>());
					}

					foreach (var f in fArr) {
						_subQueryValueFieldFilters[ic.ItemColumnId].Add(Conv.ToStr(f));
					}
				} else if (ic.IsTag()) {
					if (!_tagsFilters.ContainsKey(key)) {
						_tagsFilters.Add(key, new List<string>());
					}

					foreach (var s in fArr) {
						_tagsFilters[key].Add("'" + s + "'");
					}
				}
			}
		}
	}
}