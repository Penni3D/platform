﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.portfolios {
	[Route("settings/ProcessPortfolioFilters")]
	public class ProcessPortfolioFiltersRest : RestBase {
		// GET: model/values
		[HttpGet("{process}/{portfolioId}")]
		public List<ProcessPortfolioFilter> Get(string process, int portfolioId) {
			var r = new List<ProcessPortfolioFilter>();

			var columns = new ProcessPortfolioColumns(instance, process, currentSession);
			foreach (var pPC in columns.GetColumns(portfolioId)) {
				if (pPC.UseInFilter && pPC.ItemColumn.IsList()) {
					var p = new ItemCollectionService(pPC.Instance, pPC.ItemColumn.DataAdditional, currentSession);
					p.FillItems();
					r.Add(new ProcessPortfolioFilter(pPC, p.GetItems()));
				}
			}

			return r;
		}
	}
}