﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.portfolios {
	public class ProcessPortfolioColumn {
		public ProcessPortfolioColumn() { }

		public ProcessPortfolioColumn(DataRow row, AuthenticatedSession session, string alias = "") {
			Instance = (string) row["instance"];
			Process = (string) row["process"];
			ProcessPortfolioColumnId = (int) row["process_portfolio_column_id"];
			ProcessPortfolioColumnGroupId = Conv.ToInt(row["portfolio_column_group_id"]);
			ProcessPortfolioId = (int) row["process_portfolio_id"];
			OrderNo = (string) row["order_no"];
			UseInSelectResult = (byte) row["use_in_select_result"];
			if (DBNull.Value.Equals(row["show_type"])) {
				ShowType = null;
			} else {
				ShowType = Convert.ToByte(row["show_type"]);
			}

			if (row.Table.Columns.Contains("group_name")) GroupName = Conv.ToStr(row["group_name"]);

			ShowTypeCombined = Convert.ToByte(row["show_type_combined"]);
			UseInFilter = Convert.ToBoolean(row["use_in_filter"]);
			ItemColumn = new ItemColumn(row, session);
			SumColumn = Convert.ToBoolean(row["sum_column"]);
			IsDefault = Convert.ToBoolean(row["is_default"]);
			UseInSearch = Convert.ToBoolean(row["use_in_search"]);

			if (row[alias + "archive_item_column_id"] != DBNull.Value) {
				ArchiveItemColumnId = Conv.ToInt(row[alias + "archive_item_column_id"]);
			} else {
				ArchiveItemColumnId = null;
			}

			if (row[alias + "width"] != DBNull.Value) {
				Width = Conv.ToInt(row[alias + "width"]);
			} else {
				Width = null;
			}

			if (row[alias + "priority"] != DBNull.Value) {
				Priority = Conv.ToInt(row[alias + "priority"]);
			} else {
				Priority = 0;
			}

			ReportColumn = Conv.ToInt(row["report_column"]);

			if (!DBNull.Value.Equals(row["short_name"])) {
				ShortNameTranslationRaw =
					new Translations(session.instance, session).GetTranslations(Conv.ToStr(row["short_name"]));
				ShortNameTranslation =
					new Translations(session.instance, session).GetTranslations(Conv.ToStr(row["short_name"]), true);
			}

			if (!DBNull.Value.Equals(row["ic_validation"])) {
				Validation = JsonConvert.DeserializeObject<FieldValidation>(Conv.ToStr(row["ic_validation"]));
			} else {
				Validation = new FieldValidation();
			}
		}

		public string Instance { get; set; }
		public string Process { get; set; }
		public string GroupName { get; set; }
		public int ProcessPortfolioColumnId { get; set; }
		public int ProcessPortfolioId { get; set; }
		public string OrderNo { get; set; }
		public byte UseInSelectResult { get; set; }
		public byte? ShowType { get; set; }
		public byte ShowTypeCombined { get; set; }
		public bool UseInFilter { get; set; }
		public ItemColumn ItemColumn { get; set; }
		public bool hasData { get; set; }
		public int? ArchiveItemColumnId { get; set; }
		public int? Width { get; set; }
		public int? Priority { get; set; }
		public bool SumColumn { get; set; }
		public bool IsDefault { get; set; }
		public bool UseInSearch { get; set; }
		public int ReportColumn { get; set; }
		public int ProcessPortfolioColumnGroupId { get; set; }
		public FieldValidation Validation { get; set; }

		public Dictionary<string, string> ShortNameTranslation { get; set; }
		public Dictionary<string, string> ShortNameTranslationRaw { get; set; }
	}

	public class ProcessPortfolioColumnGroup {
		public ProcessPortfolioColumnGroup() { }

		public ProcessPortfolioColumnGroup(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			ProcessPortfolioColumnGroupId = (int) row["portfolio_column_group_id"];
			Process = (string) row["process"];
			Variable = (string) row["variable"];
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
			ProcessPortfolioId = (int) row["process_portfolio_id"];
			OrderNo = Conv.ToStr(row["order_no"]);
			ShowType = (int) row["show_type"];
		}

		public int ProcessPortfolioColumnGroupId { get; set; }
		public int ProcessPortfolioId { get; set; }
		public string Process { get; set; }
		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public string OrderNo { get; set; }
		public int ShowType { get; set; }
	}

	public class ProcessPortfolioColumns : ProcessBase {
		public ProcessPortfolioColumns(string instance, string process, AuthenticatedSession authenticatedSession) :
			base(instance, process, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		private DataTable _processPortfolioColumns;

		private DataTable processPortfolioColumns {
			get {
				_processPortfolioColumns = (DataTable) Cache.Get("", "process_portfolio_columns_table");
				if (_processPortfolioColumns != null) return _processPortfolioColumns;

				var sql = " SELECT  " +
				          "     process_portfolio_column_id,  " +
				          "       ppc.process_portfolio_id,  " +
				          "       ppc.order_no,  " +
				          "		  ppcg.variable AS group_name, " +
				          "       ppcg.order_no AS group_order, " +
				          "       use_in_select_result,  " +
				          "       short_name,  " +
				          "       report_column,  " +
				          "       validation,  " +
				          "       ppc.portfolio_column_group_id,  " +
				          "       COALESCE( " +
				          "               ppc.show_type,  " +
				          "               ( " +
				          "                       SELECT  " +
				          "                               portfolio_show_Type  " +
				          "                       FROM  " +
				          "                               processes  " +
				          "                       WHERE  " +
				          "                               process = data_additional " +
				          "               ),  " +
				          "               0 " +
				          "       ) AS show_type_combined,  " +
				          "       ppc.show_type,  " +
				          "       use_in_filter,  " +
				          "       use_in_search,  " +
				          "       archive_item_column_id,  " +
				          "       width,  " +
				          "       priority,  " +
				          "       sum_column,  " +
				          "       ic.instance,  " +
				          "       ic.process,  " +
				          "       ppc.item_column_id,  " +
				          "       name,  " +
				          "       ic.variable,  " +
				          "       data_type,  " +
				          "       system_column,  " +
				          "       data_additional,  " +
				          "       relative_column_id,  " +
				          "       data_additional2,  " +
				          "       input_method,  " +
				          "       input_name,  " +
				          "       status_column,  " +
				          "       process_action_id,  " +
				          "       link_process,  " +
				          "       process_dialog_id,  " +
				          "       check_conditions,  " +
				          "       use_lucene,  " +
				          "       use_fk,  " +
				          "       ic.ic_validation AS ic_validation,  " +
				          "       unique_col,  " +
				          "       in_conditions,  " +
				          "       is_default,  " +
				          "       parent_select_type,  " +
				          "       ic_validation,  " +
				          "       sub_data_additional2  " +
				          " FROM  " +
				          "       process_portfolio_columns ppc  " +
				          "       INNER JOIN item_columns ic ON ppc.item_column_id = ic.item_column_id  " +
				          "       LEFT JOIN process_portfolio_columns_groups ppcg ON ppcg.portfolio_column_group_id = ppc.portfolio_column_group_id " +
				          " WHERE  " +
				          "       ic.name IS NOT NULL  " +
				          "       AND LEN(ic.name) > 0  " +
				          " ORDER BY  " +
				          "       ppcg.order_no  ASC, ppc.order_no  ASC ";
				_processPortfolioColumns = db.GetDatatable(sql);
						
				Cache.Set("", "process_portfolio_columns_table", _processPortfolioColumns);

				return _processPortfolioColumns;
			}
		}
		private void ClearCache() {
			_processPortfolioColumns = null;
			Cache.Remove("", "process_portfolio_columns_table");
			Cache.Remove("", "process_portfolio_table");
		}
		
		public List<ProcessPortfolioColumn> GetColumns(int portfolioId) {
			var result = new List<ProcessPortfolioColumn>();
			
			var f = new common.Util.Filter();
			f.Add("process_portfolio_id", portfolioId);
			
			foreach (var row in processPortfolioColumns.Select(f.ToString())) {
				var portfolioColumn = new ProcessPortfolioColumn(row, authenticatedSession);
				AddEquations(portfolioColumn.ItemColumn);
				result.Add(portfolioColumn);
			}

			return result;
		}
		
		private void AddEquations(ItemColumn itemColumn) {
			if (!itemColumn.IsEquation()) return;
			SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumn.ItemColumnId);
			itemColumn.Equations =
				db.GetDatatableDictionary(
					"SELECT * FROM item_column_equations WHERE parent_item_column_id = @itemColumnId",
					DBParameters);
		}

		public ProcessPortfolioColumn GetColumn(int portfolioId, int portfolioColumnId) {
			var f = new common.Util.Filter();
			f.Add("process_portfolio_id", portfolioId);
			f.Add("process_portfolio_column_id", portfolioColumnId);
			var dt = processPortfolioColumns.Select(f.ToString());

			if (dt.Length <= 0)
				throw new CustomArgumentException("Column cannot be found with process_portfolio_id " + portfolioId +
				                                  " AND process_portfolio_column_id " + portfolioColumnId);
			var portfolioColumn = new ProcessPortfolioColumn(dt[0], authenticatedSession);

			AddEquations(portfolioColumn.ItemColumn);

			return portfolioColumn;

		}

		public void Delete(int ProcessPortfolioId, int ProcessPortfolioColumnId) {
			SetDBParam(SqlDbType.Int, "@processPortfolioColumnId", ProcessPortfolioColumnId);
			SetDBParam(SqlDbType.Int, "@processPortfolioId", ProcessPortfolioId);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_portfolio_columns WHERE process_portfolio_id = @processPortfolioId AND process_portfolio_column_id = @processPortfolioColumnId",
				DBParameters);
			ClearCache();
			Cache.Remove(instance, "SelectorColumnNames_" + process);
		}

		public void Save(ProcessPortfolioColumn column) {
			SetDBParam(SqlDbType.Int, "@processPortfolioColumnId", column.ProcessPortfolioColumnId);
			SetDBParam(SqlDbType.Int, "@processPortfolioId", column.ProcessPortfolioId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", column.ItemColumn.ItemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", column.OrderNo);
			SetDBParam(SqlDbType.Int, "@UseInSelectResult", column.UseInSelectResult);
			SetDBParam(SqlDbType.Int, "@showType", Conv.IfNullThenDbNull(column.ShowType));
			SetDBParam(SqlDbType.Int, "@useInFilter", column.UseInFilter);
			SetDBParam(SqlDbType.Int, "@archiveItemColumnId", Conv.IfNullThenDbNull(column.ArchiveItemColumnId));
			SetDBParam(SqlDbType.Int, "@width", Conv.IfNullThenDbNull(column.Width));
			SetDBParam(SqlDbType.Int, "@priority", Conv.IfNullThenDbNull(column.Priority));
			SetDBParam(SqlDbType.Int, "@SumColumn", column.SumColumn);
			SetDBParam(SqlDbType.Int, "@isDefault", column.IsDefault);
			SetDBParam(SqlDbType.Int, "@useInSearch", column.UseInSearch);
			SetDBParam(SqlDbType.Int, "@reportColumn", column.ReportColumn);

			if (column.ProcessPortfolioColumnGroupId == 0) {
				SetDBParam(SqlDbType.Int, "@processPortfolioColumnGroupId", DBNull.Value);
			} else {
				SetDBParam(SqlDbType.Int, "@processPortfolioColumnGroupId", column.ProcessPortfolioColumnGroupId);
			}

			if (column.Validation != null) {
				var validation = JsonConvert.SerializeObject(column.Validation);
				SetDBParam(SqlDbType.NVarChar, "@validation", validation);
			} else {
				SetDBParam(SqlDbType.NVarChar, "@validation", DBNull.Value);
			}

			db.ExecuteUpdateQuery(
				"UPDATE process_portfolio_columns SET item_column_id = @itemColumnId, order_no = @orderNo, use_in_select_result = @UseInSelectResult, show_type = @showType, use_in_filter = @useInFilter, archive_item_column_id = @archiveItemColumnId, width = @width, priority = @priority, sum_column = @SumColumn, is_default = @isDefault, use_in_search = @useInSearch, report_column = @reportColumn, validation = @validation, portfolio_column_group_id = @processPortfolioColumnGroupId WHERE process_portfolio_id = @processPortfolioId AND process_portfolio_column_id = @processPortfolioColumnId",
				DBParameters);

			if (column.ShortNameTranslationRaw != null &&
			    column.ShortNameTranslationRaw.ContainsKey(authenticatedSession.locale_id) &&
			    column.ShortNameTranslationRaw[authenticatedSession.locale_id] != null) {
				var translations = new Translations(authenticatedSession.instance, authenticatedSession);

				var oldVariable =
					db.ExecuteScalar(
						"select short_name FROM process_portfolio_columns WHERE process_portfolio_column_id = @processPortfolioColumnId",
						DBParameters);

				if (DBNull.Value.Equals(oldVariable)) {
					oldVariable = translations.GetTranslationVariable(process,
						column.ShortNameTranslationRaw[authenticatedSession.locale_id], "PPC_SN_");
					SetDBParam(SqlDbType.NVarChar, "@shortName", oldVariable);

					db.ExecuteUpdateQuery(
						"UPDATE process_portfolio_columns SET short_name = @shortName WHERE process_portfolio_column_id = @processPortfolioColumnId ",
						DBParameters);
				}

				foreach (var translation in column.ShortNameTranslationRaw) {
					translations.insertOrUpdateProcessTranslation(process, Conv.ToStr(oldVariable), translation.Value,
						translation.Key);
				}
			}
			ClearCache();
			Cache.Remove(instance, "SelectorColumnNames_" + process);
		}

		public ProcessPortfolioColumnGroup AddGroup(string process, ProcessPortfolioColumnGroup columnGroup) {
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", columnGroup.OrderNo);
			SetDBParam(SqlDbType.Int, "@processPortfolioId", columnGroup.ProcessPortfolioId);

			columnGroup.ProcessPortfolioColumnGroupId = db.ExecuteInsertQuery(
				"INSERT INTO process_portfolio_columns_groups (instance, process, process_portfolio_id, order_no, show_type) VALUES(@instance, @process, @processPortfolioId, @orderNo, 1)",
				DBParameters);
			if (columnGroup.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var variable = translations.GetTranslationVariable(process,
					columnGroup.LanguageTranslation[authenticatedSession.locale_id], "PORTFOLIOCGRP");
				columnGroup.Variable = variable;

				SetDBParam(SqlDbType.NVarChar, "@variable", columnGroup.Variable);
				SetDBParam(SqlDbType.Int, "@PortfolioColumnGroupId", columnGroup.ProcessPortfolioColumnGroupId);
				db.ExecuteUpdateQuery(
					"UPDATE process_portfolio_columns_groups SET variable = @variable WHERE instance = @instance AND process = @process AND portfolio_column_group_id = @PortfolioColumnGroupId",
					DBParameters);
				foreach (var translation in columnGroup.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, variable, translation.Value,
						translation.Key);
				}
			}

			columnGroup.ShowType = 1;
			ClearCache();
			return columnGroup;
		}

		public List<ProcessPortfolioColumnGroup> GetColumnGroup(string process, int portfolioId) {
			SetDBParam(SqlDbType.Int, "@processPortfolioId", portfolioId);
			var result = new List<ProcessPortfolioColumnGroup>();
			var db = new Connections(authenticatedSession);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT portfolio_column_group_id, process, variable, process_portfolio_id, order_no, show_type FROM process_portfolio_columns_groups WHERE process_portfolio_id = @processPortfolioId ORDER BY order_no ASC",
					DBParameters).Rows) {
				var pcg = new ProcessPortfolioColumnGroup(row, authenticatedSession);
				result.Add(pcg);
			}

			return result;
		}

		public void UpdateGroup(string process, ProcessPortfolioColumnGroup portfolioGroup) {
			var variable = portfolioGroup.Variable;
			SetDBParam(SqlDbType.NVarChar, "@orderNo", portfolioGroup.OrderNo);
			SetDBParam(SqlDbType.NVarChar, "@groupId", portfolioGroup.ProcessPortfolioColumnGroupId);
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.Int, "@showType", portfolioGroup.ShowType);
			if (variable != null && portfolioGroup.LanguageTranslation != null) {
				foreach (var translation in portfolioGroup.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, variable, translation.Value,
						translation.Key);
				}
			}

			db.ExecuteUpdateQuery(
				"UPDATE process_portfolio_columns_groups SET order_no = @orderNo, show_type = @showType WHERE portfolio_column_group_id = @groupId",
				DBParameters);
			ClearCache();
		}

		public void DeleteGroup(string process, int groupId) {
			SetDBParam(SqlDbType.Int, "@processPortfolioGroupId", groupId);
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_portfolio_columns_groups WHERE portfolio_column_group_id = @processPortfolioGroupId AND process = @process",
				DBParameters);
			ClearCache();
		}

		public ProcessPortfolioColumn Add(ProcessPortfolioColumn column) {
			SetDBParam(SqlDbType.Int, "@processPortfolioId", column.ProcessPortfolioId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", column.ItemColumn.ItemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@orderNo", column.OrderNo);
			SetDBParam(SqlDbType.Int, "@UseInSelectResult", column.UseInSelectResult);
			SetDBParam(SqlDbType.Int, "@showType", Conv.IfNullThenDbNull(column.ShowType));
			SetDBParam(SqlDbType.Int, "@useInFilter", column.UseInFilter);
			SetDBParam(SqlDbType.Int, "@archiveItemColumnId", Conv.IfNullThenDbNull(column.ArchiveItemColumnId));
			SetDBParam(SqlDbType.Int, "@width", Conv.IfNullThenDbNull(column.Width));
			SetDBParam(SqlDbType.Int, "@priority", Conv.IfNullThenDbNull(column.Priority));
			SetDBParam(SqlDbType.Int, "@SumColumn", column.SumColumn);
			SetDBParam(SqlDbType.Int, "@isDefault", column.IsDefault);
			SetDBParam(SqlDbType.Int, "@useInSearch", column.UseInSearch);
			SetDBParam(SqlDbType.Int, "@reportColumn", column.ReportColumn);
			SetDBParam(SqlDbType.Int, "@processPortfolioColumnGroupId", column.ProcessPortfolioColumnGroupId);
			if (column.ProcessPortfolioColumnGroupId == 0) {
				SetDBParam(SqlDbType.Int, "@processPortfolioColumnGroupId", DBNull.Value);
			} else {
				SetDBParam(SqlDbType.Int, "@processPortfolioColumnGroupId", column.ProcessPortfolioColumnGroupId);
			}

			if (column.Validation != null) {
				var validation = JsonConvert.SerializeObject(column.Validation);
				SetDBParam(SqlDbType.NVarChar, "@validation", validation);
			} else {
				SetDBParam(SqlDbType.NVarChar, "@validation", DBNull.Value);
			}

			column.ProcessPortfolioColumnId = db.ExecuteInsertQuery(
				"INSERT INTO process_portfolio_columns (process_portfolio_id, item_column_id, use_in_select_result, show_type, use_in_filter, order_no, archive_item_column_id, width, priority, sum_column, is_default, use_in_search, report_column, validation, portfolio_column_group_id) VALUES(@processPortfolioId, @itemColumnId, @useInSelectResult, @showType, @useInFilter, @orderNo, @archiveItemColumnId, @width, @priority, @SumColumn, @isDefault, @useInSearch, @reportColumn, @validation, @processPortfolioColumnGroupId)",
				DBParameters);

			if (column.ShortNameTranslationRaw != null &&
			    column.ShortNameTranslationRaw.ContainsKey(authenticatedSession.locale_id) &&
			    column.ShortNameTranslationRaw[authenticatedSession.locale_id] != null) {
				var translations = new Translations(authenticatedSession.instance, authenticatedSession);
				var oldVariable = translations.GetTranslationVariable(process,
					column.ShortNameTranslationRaw[authenticatedSession.locale_id], "PPC_SN_");

				SetDBParam(SqlDbType.NVarChar, "@shortName", oldVariable);
				SetDBParam(SqlDbType.Int, "@ProcessPortfolioColumnId", column.ProcessPortfolioColumnId);

				db.ExecuteUpdateQuery(
					"UPDATE process_portfolio_columns SET short_name = @shortName WHERE process_portfolio_column_id = @ProcessPortfolioColumnId ",
					DBParameters);

				foreach (var translation in column.ShortNameTranslationRaw) {
					translations.insertOrUpdateProcessTranslation(process, oldVariable, translation.Value,
						translation.Key);
				}
			}

			SetDBParam(SqlDbType.NVarChar, "@ppid", column.ProcessPortfolioId);
			SetDBParam(SqlDbType.Int, "@item_column_id", column.ItemColumn.ItemColumnId);
			db.ExecuteStoredProcedure("EXEC ExcludeNewColumnInSavedFilters @ppid, @item_column_id", DBParameters);

			Cache.Remove(instance, "SelectorColumnNames_" + process);
			ClearCache();
			return column;
		}

		public void Delete(int containerColumnId) {
			Cache.Remove(instance, "SelectorColumnNames_" + process);
			var param = new List<SqlParameter>();
			param.Add(GetDBParam(SqlDbType.Int, "@containerColumnId", containerColumnId));
			db.ExecuteDeleteQuery(
				"DELETE FROM process_SELECT_NAME WHERE process_container_column_id = @containerColumnId", param);
			ClearCache();
		}

		public List<ProcessPortfolioColumn> GetPrimaryColumns() {
			var result = new List<ProcessPortfolioColumn>();
			var sql =
				"SELECT process_portfolio_column_id, process_portfolio_id, order_no, use_in_select_result, show_type, COALESCE(show_type, (SELECT portfolio_show_type FROM processes WHERE process = data_additional), 0) as show_type_combined, use_in_filter,  use_in_search, archive_item_column_id, width, priority, sum_column, instance, process, ppc.item_column_id, name, variable, data_type, system_column, data_additional, data_additional2, relative_column_id, input_method, input_name, status_column, check_conditions, process_action_id, link_process, process_dialog_id, use_lucene, use_fk, unique_col, is_default, short_name, report_column, validation, ic_validation, in_conditions, parent_select_type, portfolio_column_group_id,sub_data_additional2 FROM process_portfolio_columns ppc inner join item_columns ic on ppc.item_column_id = ic.item_column_id where process_portfolio_id = " +
				" (SELECT max(process_portfolio_id) " +
				"  FROM process_portfolios WHERE instance = @instance and process = @process and process_portfolio_type IN (1, 10) ) " +
				" AND LEN(ISNULL(ic.name,'')) > 0 AND use_in_select_result = 1 ORDER BY order_no  ASC ";
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var portfolioColumn = new ProcessPortfolioColumn(row, authenticatedSession);

				if (portfolioColumn.ItemColumn.IsEquation()) {
					SetDBParam(SqlDbType.Int, "@itemColumnId", portfolioColumn.ItemColumn.ItemColumnId);
					portfolioColumn.ItemColumn.Equations =
						db.GetDatatableDictionary(
							"SELECT * FROM item_column_equations WHERE parent_item_column_id =  @itemColumnId",
							DBParameters);
				}

				result.Add(portfolioColumn);
			}

			return result;
		}

		public List<ProcessPortfolioColumn> GetSecondaryColumns() {
			var result = new List<ProcessPortfolioColumn>();
			var sql =
				"SELECT process_portfolio_column_id, process_portfolio_id, order_no, use_in_select_result, show_type, COALESCE(show_type, (SELECT portfolio_show_type FROM processes WHERE process = data_additional), 0) as show_type_combined, use_in_filter,  use_in_search, archive_item_column_id, width, priority, sum_column, instance, process, ppc.item_column_id, name, variable, data_type, system_column, data_additional, data_additional2, relative_column_id, input_method, input_name, status_column, check_conditions, process_action_id, link_process, process_dialog_id, use_lucene, use_fk, unique_col, is_default, short_name, report_column, validation, ic_validation, in_conditions, parent_select_type, portfolio_column_group_id,sub_data_additional2 FROM process_portfolio_columns ppc inner join item_columns ic on ppc.item_column_id = ic.item_column_id where process_portfolio_id = " +
				" (SELECT max(process_portfolio_id) " +
				"  FROM process_portfolios WHERE instance = @instance and process = @process and process_portfolio_type IN (1, 10) ) " +
				" AND LEN(ISNULL(ic.name,'')) > 0 AND use_in_select_result = 2 ORDER BY order_no  ASC ";
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var portfolioColumn = new ProcessPortfolioColumn(row, authenticatedSession);

				if (portfolioColumn.ItemColumn.IsEquation()) {
					SetDBParam(SqlDbType.Int, "@itemColumnId", portfolioColumn.ItemColumn.ItemColumnId);
					portfolioColumn.ItemColumn.Equations =
						db.GetDatatableDictionary(
							"SELECT * FROM item_column_equations WHERE parent_item_column_id =  @itemColumnId",
							DBParameters);
				}

				result.Add(portfolioColumn);
			}

			return result;
		}
	}

	public class FieldValidation {
		public int? Precision { get; set; }
		public int? Min { get; set; }
		public int? Max { get; set; }
		public int? Rows { get; set; }
		public bool? Spellcheck { get; set; }
		public string Suffix { get; set; }
		public string ParentId { get; set; }
		public string DateRange { get; set; }
		public bool? CalendarConnection { get; set; }
		public int? AfterMonths { get; set; }
		public int? BeforeMonths { get; set; }
		public bool? Unselectable { get; set; }
		public bool? Required { get; set; }
		public bool? ReadOnly { get; set; }
		public string Label { get; set; }
	}
}