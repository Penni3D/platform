﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.portfolios {
	[Route("v1/settings/ProcessPortfolioColumns")]
	public class ProcessPortfolioColumnsRest : PageBase {
		public ProcessPortfolioColumnsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}/{portfolioId}")]
		public List<ProcessPortfolioColumn> Get(string process, int portfolioId) {
			var columns = new ProcessPortfolioColumns(instance, process, currentSession);
			return columns.GetColumns(portfolioId);
		}

		[HttpGet("{process}/{portfolioId}/{portfolioColumnId}")]
		public ProcessPortfolioColumn Get(string process, int portfolioId, int portfolioColumnId) {
			var columns = new ProcessPortfolioColumns(instance, process, currentSession);
			return columns.GetColumn(portfolioId, portfolioColumnId);
		}

		[HttpPost("{process}/{portfolioId}")]
		public IActionResult Post(string process, int portfolioId, [FromBody] List<ProcessPortfolioColumn> columns) {
			var portfolioColumns = new ProcessPortfolioColumns(instance, process, currentSession);
			CheckRight("write");
			foreach (var c in columns) {
				portfolioColumns.Add(c);
			}

			return new ObjectResult(columns);
		}

		[HttpGet("columngroups/{process}/{portfolioId}")]
		public List<ProcessPortfolioColumnGroup> GetColumnGroups(string process, int portfolioId) {
			var portfolioColumnGroups = new ProcessPortfolioColumns(instance, process, currentSession);
			return portfolioColumnGroups.GetColumnGroup(process, portfolioId);
		}

		[HttpPut("columngroups/{process}")]
		public IActionResult UpdateColumnGroups(string process, [FromBody] ProcessPortfolioColumnGroup group) {
			var portfolioColumnGroups = new ProcessPortfolioColumns(instance, process, currentSession);
			portfolioColumnGroups.UpdateGroup(process, group);
			return new ObjectResult(group);
		}

		[HttpDelete("columngroups/{process}/{groupId}")]
		public IActionResult DeleteColumnGroups(string process, int groupId) {
			var portfolioColumnGroups = new ProcessPortfolioColumns(instance, process, currentSession);
			portfolioColumnGroups.DeleteGroup(process, groupId);
			return new StatusCodeResult(200);
		}

		[HttpPost("{process}")]
		public IActionResult PostColumnGroup(string process, [FromBody] List<ProcessPortfolioColumnGroup> groups) {
			var portfolioColumnGroups = new ProcessPortfolioColumns(instance, process, currentSession);
			CheckRight("write");
			foreach (var xx in groups) {
				portfolioColumnGroups.AddGroup(process, xx);
			}

			return new ObjectResult(groups);
		}

		[HttpPut("{process}/{portfolioId}")]
		public IActionResult Put(string process, int portfolioId, [FromBody] List<ProcessPortfolioColumn> columns) {
			CheckRight("write");
			var portfolioColumns = new ProcessPortfolioColumns(instance, process, currentSession);
			foreach (var c in columns) {
				portfolioColumns.Save(c);
			}

			return new ObjectResult(columns);
		}

		[HttpPut("{process}/{portfolioId}/{portfolioColumnId}")]
		public IActionResult Put(string process, int portfolioId, int portfolioColumnId,
			[FromBody] ProcessPortfolioColumn column) {
			CheckRight("write");
			var portfolioColumns = new ProcessPortfolioColumns(instance, process, currentSession);
			portfolioColumns.Save(column);
			return new ObjectResult(column);
		}

		[HttpDelete("{process}/{portfolioId}/{portfolioColumnId}")]
		public IActionResult Delete(string process, int portfolioId, string portfolioColumnId) {
			CheckRight("delete");
			var portfolioColumns = new ProcessPortfolioColumns(instance, process, currentSession);
			foreach (var id in portfolioColumnId.Split(',')) {
				portfolioColumns.Delete(portfolioId, Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}
	}
}