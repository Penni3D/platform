﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes {
	public class SubItem {
		public SubItem() {
		}

		public SubItem(DataRow row, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			ItemId = (int)row[alias + "item_id"];
			ParentItemId = (int)row[alias + "parent_item_id"];
			Favourite = (byte)row[alias + "favourite"];
		}

		public int ItemId { get; set; }
		public int ParentItemId { get; set; }
		public byte Favourite { get; set; }
	}

	public class SubItems {
		public ProcessBase ProcessBase;


		public SubItems(string instance, string process, AuthenticatedSession authenticatedSession){
			ProcessBase = new ProcessBase(instance, process, authenticatedSession);
		}

		public List<SubItem> GetSubItems(int parentItemId, byte favourite = 0) {
			var result = new List<SubItem>();
			ProcessBase.SetDBParam(SqlDbType.Int, "@parentItemId", parentItemId);
			ProcessBase.SetDBParam(SqlDbType.TinyInt, "@favourite", favourite);
			ProcessBase.SetDBParameter("@instance", ProcessBase.instance);
			var sql =
				"SELECT isi.item_id, isi.parent_item_id, isi.favourite FROM item_subitems isi INNER JOIN items i ON i.item_id = isi.item_id WHERE isi.parent_item_id = @parentItemId AND isi.favourite = @favourite AND i.process = @process AND i.instance = @instance";
			var dataTable = ProcessBase.db.GetDatatable(sql,ProcessBase.DBParameters);
			foreach (DataRow row in dataTable.Rows){ 
				result.Add(new SubItem(row));
			}

			return result;
		}

		public List<SubItem> GetParentSubItems(int itemId, byte favourite = 0) {
			var result = new List<SubItem>();
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.TinyInt, "@favourite", favourite);

			foreach (DataRow row in ProcessBase.db.GetDatatable("SELECT isi.item_id, isi.parent_item_id, isi.favourite FROM item_subitems isi INNER JOIN items i ON i.item_id = isi.parent_item_id WHERE isi.item_id = @itemId AND isi.favourite = @favourite AND i.process = @process AND i.instance = @instance", ProcessBase.DBParameters).Rows) {
				result.Add(new SubItem(row));
			}

			return result;
		}


		public void AddSubItem(int parentItemId, int itemId, byte favourite = 0) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@parentItemId", parentItemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.TinyInt, "@favourite", favourite);

			ProcessBase.db.ExecuteInsertQuery("INSERT INTO item_subitems(parent_item_id, item_id, favourite) VALUES(@parentItemId, @itemId, @favourite)", ProcessBase.DBParameters);
		}

		public void DeleteSubItem(int parentItemId, int itemId, byte favourite = 0) {
			ProcessBase.SetDBParam(SqlDbType.Int, "@parentItemId", parentItemId);
			ProcessBase.SetDBParam(SqlDbType.Int, "@itemId", itemId);
			ProcessBase.SetDBParam(SqlDbType.TinyInt, "@favourite", favourite);

			ProcessBase.db.ExecuteDeleteQuery("DELETE FROM item_subitems WHERE parent_item_id = @parentItemId AND item_id = @itemId AND favourite = @favourite", ProcessBase.DBParameters);
		}
	}
}