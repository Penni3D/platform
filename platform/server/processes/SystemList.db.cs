﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes {
	public class SystemList : ConnectionBase {
		private readonly string[] items = { "YES_NO", "LANGUAGES", "DATE_FORMATS", "FIRST_DAY_OF_WEEK", "TIMEZONES", "SYMBOLS", "SYMBOL_COLORS"};

		public SystemList(string instance, AuthenticatedSession authenticatedSession) : base(instance, authenticatedSession) {
		}

		public IEnumerable<Dictionary<string, object>> GetLists() {
			var retval = new List<Dictionary<string, object>>();
			Dictionary<string, object> li;

			foreach (var i in items) {
				li = new Dictionary<string, object>();
				li.Add("ListName", i);
				retval.Add(li);
			}

			return retval;
		}

		public IEnumerable<Dictionary<string, object>> GetListItems(string systemList) {
			var retval = new List<Dictionary<string, object>>();
			switch (systemList) {
				case "YES_NO":
					retval.Add(CreateItem(0, "NO"));
					retval.Add(CreateItem(1, "YES"));

					return retval;
				case "FIRST_DAY_OF_WEEK":
					retval.Add(CreateItem(0, "CAL_DAYS_LONG_0"));
					retval.Add(CreateItem(1, "CAL_DAYS_LONG_1"));

					return retval;
				case "TIMEZONES":
					foreach (var i in TimeZoneInfo.GetSystemTimeZones()) {
						retval.Add(CreateItem(i.Id + "_" + ((i.BaseUtcOffset.Hours*60) + i.BaseUtcOffset.Minutes), i.DisplayName));
					}
					return retval;
				case "DATE_FORMATS":
					var y = DateTime.Now.ToUniversalTime().Year;
					retval.Add(CreateItem("dd/mm/yy", "dd/mm/yyyy - 31/12/" + y));
					retval.Add(CreateItem("mm/dd/yy", "mm/dd/yyyy - 12/31/" + y));
					retval.Add(CreateItem("dd.mm.yy", "dd.mm.yyyy - 31.12." + y));
					retval.Add(CreateItem("d M yyyy", "d M yyyy - 31 Nov " + y));

					return retval;
				case "LANGUAGES":
					foreach (DataRow row in db.GetDatatable("SELECT code, name FROM instance_languages WHERE instance = @instance", DBParameters).Rows) {
						retval.Add(CreateItem(row["code"], row["name"]));
					}
					return retval;
				case "SYMBOLS":
					return Config.SystemIcons;
				case "ICON_SETS":
					foreach (var file in Directory.EnumerateFiles("wwwroot\\public\\images\\symbols", "*.svg")) {
						retval.Add(CreateItem(Path.GetFileNameWithoutExtension(file), Path.GetFileNameWithoutExtension(file)));
					}
					return retval;
				case "SYMBOL_COLORS":
					retval.Add(CreateItem("rgb(244, 67, 53)", "COLOR_RED"));
					retval.Add(CreateItem("rgb(233, 30, 99)", "COLOR_PINK"));
					retval.Add(CreateItem("rgb(159, 39, 176)", "COLOR_PURPLE"));
					retval.Add(CreateItem("rgb(63, 81, 181)", "COLOR_INDIGO"));
					retval.Add(CreateItem("rgb(33, 150, 243)", "COLOR_BLUE"));
					retval.Add(CreateItem("rgb(0, 188, 212)", "COLOR_CYAN"));
					retval.Add(CreateItem("rgb(77, 182, 172)", "COLOR_TEAL"));
					retval.Add(CreateItem("rgb(76, 175, 80)", "COLOR_GREEN"));
					retval.Add(CreateItem("rgb(205, 220, 57)", "COLOR_LIME"));
					retval.Add(CreateItem("rgb(255, 235, 59)", "COLOR_YELLOW"));
					retval.Add(CreateItem("rgb(255, 193, 7)", "COLOR_AMBER"));
					retval.Add(CreateItem("rgb(255, 152, 0)", "COLOR_ORANGE"));
					retval.Add(CreateItem("rgb(121, 85, 72)", "COLOR_BROWN"));
					retval.Add(CreateItem("rgb(255, 255, 255)", "COLOR_WHITE"));
					retval.Add(CreateItem("rgb(33, 33, 33)", "COLOR_BLACK"));
					return retval;
				default:
					throw new DataException();
			}
		}

		private Dictionary<string, object> CreateItem(object value, object item) {
			var addval = new Dictionary<string, object>();
			addval.Add("value", value);
			addval.Add("name", item);

			return addval;
		}
	}
}
