﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.actions;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes {
	public class ProcessBase : ConnectionBaseProcess {
		public AuthenticatedSession authenticatedSession;
		public Translations translations;

		public ProcessBase(AuthenticatedSession session, string process) : base(session.instance, process, session) {
			authenticatedSession = session;
			translations = new Translations(session.instance, authenticatedSession);
		}

		public ProcessBase(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance,
			process, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
			translations = new Translations(instance, this.authenticatedSession);
		}

		public void InvalidateClientItemAndSqCache(int itemId, Dictionary<string, object> itemData = null) {
			//INVALIDATE CLIENT CACHE
			var SignalR = new ApiPush(Startup.SignalR);
			var InvalidateItemIds = new Dictionary<int, string>();
			var process = this.process;
			var instance = this.instance;

			//INVALIDATE FEATURES
			foreach (var f in RegisteredFeatures.GetFeatures()) {
				if (f.Process.ToLower() != process.ToLower() || !f.Enabled || !f.ImplementsProcessRelations) continue;

				foreach (var childId in f.GetRelatedChildsByOwnerItemId(instance, itemId)) {
					if (!InvalidateItemIds.ContainsKey(childId))
						InvalidateItemIds.Add(childId, f.Process);
				}

				var pid = f.GetRelatedParentByItemId(instance, itemId);
				if (!InvalidateItemIds.ContainsKey(pid))
					InvalidateItemIds.Add(pid, Cache.GetItemProcess(pid, instance));
			}

			if (!InvalidateItemIds.ContainsKey(itemId))
				InvalidateItemIds.Add(itemId, process);

			//INVALIDATE PARENTS
			if (itemData != null) {
				var p = new List<SqlParameter> { new("@itemid", SqlDbType.Int) { Value = itemId } };

				foreach (DataRow col in db
					         .GetDatatable(
						         "SELECT name FROM item_columns WHERE process = (SELECT process FROM items WHERE item_id = @itemid) AND name like '%_item_id'",
						         p).Rows) {
					var name = Conv.ToStr(col["name"]);
					if (!itemData.ContainsKey(name)) continue;

					if (itemData[name].GetType().IsArray || itemData[name].GetType() == new List<int>().GetType()) {
						foreach (var id in (IEnumerable<int>)itemData[name]) {
							var idProcess = Cache.GetItemProcess(id, instance);
							if (id > 0 && !InvalidateItemIds.ContainsKey(id))
								InvalidateItemIds.Add(id, idProcess);
						}
					} else {
						var id = Conv.ToInt(itemData[name]);
						var idProcess = Cache.GetItemProcess(id, instance);
						if (id > 0 && !InvalidateItemIds.ContainsKey(id))
							InvalidateItemIds.Add(id, idProcess);
					}
				}
			}

			//INVALIDATE DATA TABLES
			var pp = new List<SqlParameter> {
				new("@ownerItemId", SqlDbType.Int) { Value = itemId },
				new("@process", SqlDbType.NVarChar) { Value = process }
			};
			foreach (DataRow r in db.GetDatatable(
				         "SELECT DISTINCT data_additional FROM item_columns WHERE process = @process AND data_type = 21",
				         pp).Rows) {
				var table = Conv.ToSql("_" + instance + "_" + Conv.ToStr(r["data_additional"]));
				foreach (DataRow id in db.GetDatatable(
						         "SELECT item_id FROM " + table + " WHERE owner_item_id = @ownerItemId",
						         pp)
					         .Rows) {
					InvalidateItemIds.Add(Conv.ToInt(id["item_id"]), Conv.ToStr(r["data_additional"]));
				}
			}

			foreach (DataRow r in db
				         .GetDatatable(
					         "SELECT item_column_id, data_additional, data_additional2 FROM item_columns WHERE process = @process AND data_type = 18",
					         pp).Rows) {
				var da2 = Conv.ToStr(r["data_additional2"]);
				if (da2 == "")
					continue;
				var da2Dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(da2);

				if (!da2Dict.ContainsKey("itemColumnId")) continue;

				var ppp = new List<SqlParameter> {
					new("@ownerItemId", SqlDbType.Int) { Value = itemId },
					new("@process", SqlDbType.NVarChar) { Value = process },
					new("@ic", SqlDbType.Int) { Value = da2Dict["itemColumnId"] }
				};

				foreach (DataRow id in db
					         .GetDatatable(
						         "SELECT item_id FROM item_data_process_selections WHERE item_column_id = @ic AND selected_item_id = @ownerItemId",
						         ppp)
					         .Rows) {
					if (!InvalidateItemIds.ContainsKey(Conv.ToInt(id["item_id"])))
						InvalidateItemIds.Add(Conv.ToInt(id["item_id"]), Conv.ToStr(r["data_additional"]));
				}
			}

			//Force subquery cache update
			foreach (var (key, value) in InvalidateItemIds) {
				new SubqueryCacheUpdater().UpdateCache(instance, value, authenticatedSession, key);
			}

			var parameters = new List<object> { InvalidateItemIds };
			SignalR.SendToAll("ActionExecuted", parameters);
		}

		public bool ProcessRequiresInsertWriteRight(string process) {
			return Conv.ToInt(db.ExecuteScalar(
				"SELECT COUNT(*) FROM processes WHERE process = @process AND require_insert = 1",
				DBParameters)) == 1;
		}

		public int InsertItemRowNoAction() {
			var itemId =
				db.ExecuteInsertQuery("INSERT INTO items (instance, process) VALUES(@instance, @process)",
					DBParameters);
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			return itemId;
		}

		public int InsertArchiveItemRow(int itemId, DateTime d) {
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.DateTime, "@date", d);

			var newId = 0;

			var hRow =
				db.GetDataRow(
					"select archive_id, archive_start, archive_end from archive_" + tableName +
					" where archive_start < @date and archive_end > @date and item_id = @itemId", DBParameters);
			if (hRow != null) {
				SetDBParam(SqlDbType.DateTime, "@date2", hRow["archive_end"]);
				SetDBParam(SqlDbType.Int, "@archiveId", hRow["archive_id"]);
				db.ExecuteUpdateQuery(
					"update archive_" + tableName + " set archive_end = @date WHERE archive_id = @archiveId ",
					DBParameters);
				newId = db.ExecuteInsertQuery(
					"insert into archive_" + tableName +
					" (item_id, archive_start, archive_end, archive_type) VALUES(@itemId, @date, @date2, 1) ",
					DBParameters);
			} else {
				hRow = db.GetDataRow(
					"select min(archive_start) as startdate, max(archive_end) as enddate from archive_" + tableName +
					" where item_id = @itemId", DBParameters);
				if (hRow["startdate"] != DBNull.Value && Conv.ToDateTime(hRow["startdate"]) > d) {
					SetDBParam(SqlDbType.DateTime, "@date2", hRow["startdate"]);
					db.ExecuteUpdateQuery(
						"update archive_" + tableName +
						" set archive_start = @date where archive_start = @date2 and item_id = @itemId",
						DBParameters);
					newId = db.ExecuteInsertQuery(
						"insert into archive_" + tableName +
						" (item_id, archive_start, archive_end, archive_type) VALUES(@itemId, @date, @date, 1) ",
						DBParameters);
				} else if (hRow["enddate"] != DBNull.Value && Conv.ToDateTime(hRow["enddate"]) < d) {
					SetDBParam(SqlDbType.DateTime, "@date2", hRow["enddate"]);
					db.ExecuteUpdateQuery(
						"update archive_" + tableName +
						" set archive_end = @date where archive_end = @date2 and item_id = @itemId",
						DBParameters);
					newId = db.ExecuteInsertQuery(
						"insert into archive_" + tableName +
						" (item_id, archive_start, archive_end, archive_type) VALUES(@itemId, @date, @date, 1) ",
						DBParameters);
				} else {
					newId = db.ExecuteInsertQuery(
						"insert into archive_" + tableName +
						" (item_id, archive_start, archive_end, archive_type) VALUES(@itemId, @date, @date, 1) ",
						DBParameters);
				}
			}

			return newId;
		}

		public int InsertItemRow(int? parentItemId = 0, bool executeNewRowActions = true, int ownerItemId = 0) {
			//SetDBParam(SqlDbType.NVarChar, "@instance", instance); -- taken away by BSa shouldn't be required

			//Add Item With Owner Item Id, if provided
			var itemId =
				db.ExecuteInsertQuery(
					"INSERT INTO items (instance, process" + (ownerItemId > 0 ? ", owner_item_id" : "") + ") VALUES(@instance, @process" + (ownerItemId > 0 ? ", " + ownerItemId : "") + ")",
					DBParameters);

			if (executeNewRowActions) DoNewRowAction(itemId, checkRights: false);

			var ics = new ItemColumns(instance, process, authenticatedSession);
			var ic = ics.GetItemColumnByName("serial", true);
			if (ic != null) {
				var dis = new DatabaseItemService(instance, process, authenticatedSession);
				var data = new Dictionary<string, object>();
				data.Add("item_id", itemId);
				data.Add("serial", GetNextSerial());
				dis.SaveItem(data, checkRights: false);
			}

			if (parentItemId != null && parentItemId > 0) {
				SetDBParam(SqlDbType.Int, "@parentItemId", parentItemId);
				SetDBParam(SqlDbType.Int, "@itemId", itemId);
				db.ExecuteInsertQuery(
					"INSERT INTO item_subitems (item_id, parent_item_id) VALUES(@itemId, @parentItemId)",
					DBParameters);
			}

			return itemId;
		}

		public void DeleteItemRow(int itemId, bool checkRights, bool removeArchive = false) {
			var dis = new DatabaseItemService(instance, process, authenticatedSession);
			dis.Delete(itemId, checkRights, removeArchive);
		}

		public int UpdateRecursiveParent(int item_id, int parentItemId, string process) {
			SetDBParam(SqlDbType.Int, "@parentItemId", parentItemId);
			SetDBParam(SqlDbType.Int, "@itemId", item_id);

			var c = Conv.ToInt(db.ExecuteScalar(
				"SELECT COUNT (item_id) FROM _" + instance + "_" + process + " WHERE item_id = @parentItemId",
				DBParameters));
			if (c != 0) {
				db.ExecuteUpdateQuery(
					"UPDATE _" + instance + "_" + process +
					" SET parent_item_id = @parentItemId WHERE item_id = @itemId",
					DBParameters);
				return parentItemId;
			}

			return 0;
		}

		public string GetNextSerial() {
			var currentId = (int)db.ExecuteScalar(
				"SELECT COALESCE(MAX(CAST(SUBSTRING(serial, 6, 4) AS integer)), 0) FROM " +
				tableName + " WHERE serial IS NOT NULL AND SUBSTRING(serial, 0, 5) = '" +
				DateTime.Now.ToUniversalTime().Year + "'") + 1;
			var curStr = (currentId + "").PadLeft(4, '0');
			return DateTime.Now.ToUniversalTime().Year + "-" + curStr;
		}

		public void DoNewRowAction(int itemId, bool isAfterNewRowAction = false, bool checkRights = true) {
			var pp = new Processes(instance, authenticatedSession);
			var p = pp.GetProcess(process);
			var actionId = p.NewRowActionId;
			if (isAfterNewRowAction) actionId = p.AfterNewRowActionId;
			if (actionId == null) return;

			var pa = new ProcessActions(instance, process, authenticatedSession);
			pa.DoAction((int)actionId, itemId, checkRights, null, !isAfterNewRowAction);
		}

		public string ValidateOrderColumn(string order) {
			order = order.ToLower();
			if (!string.IsNullOrEmpty(order)) {
				if (new Processes(instance, authenticatedSession).ColumnExist(
					    process,
					    order) || order.Equals("order_no") || order.Equals("item_id")) {
					return order;
				}

				throw new CustomArgumentException("Target order column cannot be validated " + process + "." + order);
			}

			return "";
		}
	}
}