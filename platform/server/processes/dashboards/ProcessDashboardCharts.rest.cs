﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.dashboards {
    [Route("v1/settings/ProcessDashboardCharts")]
    public class ProcessDashboardChartsRest : RestBase {
	    // GET, POST, PUT AND DELETE OPERATIONS FOR CHARTS

	    [HttpGet]
        public List<ProcessDashboardChart> GetCharts() {
            return GetDashboardsInstance().GetCharts();
        }

        [HttpGet("relations")]
        public Dictionary<int,List<int>> GetAllRelations() {
            return GetDashboardsInstance().GetAllRelations();
        }

        [HttpGet("{process}")]
        public List<ProcessDashboardChart> Get(string process) {
            return GetDashboardsInstance(process).GetChartsForProcess(process);
        }
        
        [HttpGet("with/links/{process}")]
        public List<ProcessDashboardChart> GetWithLinks(string process) {
	        return GetDashboardsInstance(process).GetChartsForProcess(process, true);
        }

        [HttpGet("{process}/{chartId}")]
        public ProcessDashboardChart Get(string process, int chartId) {
            return GetDashboardsInstance(process).GetDashboardChart(chartId);
        }

        [HttpPost("{process}")]
        public ProcessDashboardChart Post(string process, [FromBody] ProcessDashboardChart chart) {
            var dashboardInstance = GetDashboardsInstance(process);
            var newId = dashboardInstance.AddChart(chart);
            return dashboardInstance.GetDashboardChart(newId);
        }

        [HttpPut("{process}")]
        public IActionResult Put(string process, [FromBody]ProcessDashboardChart chart) {
            var dashboardsInstance = GetDashboardsInstance(process);
            dashboardsInstance.SaveChart(chart);
            return new ObjectResult(dashboardsInstance.GetDashboardChart(chart.Id));
        }

        [HttpDelete("{process}/{processChartdIds}")]
        public IActionResult Delete(string process, string processChartdIds) {
            var dashboards = GetDashboardsInstance(process);
            foreach (var id in processChartdIds.Split(',')) {
                dashboards.DeleteChart(Conv.ToInt(id));
            }
            return new StatusCodeResult(200);
        }

        private ProcessDashboards GetDashboardsInstance(string process = "") {
            return new ProcessDashboards(instance, process, currentSession);
        }
    }
}
