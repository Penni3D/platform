﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.dashboards {
	[Route("v1/meta/ProcessDashboards")]
	public class ProcessDashboardsRest : PageBase {
		public ProcessDashboardsRest() : base("settings") { }

		[HttpGet]
		public List<ProcessDashboard> GetDashboards() {
			return GetDashboardsInstance().GetDashboards();
		}

		[HttpGet("{process}")]
		public List<ProcessDashboard> Get(string process) {
			return GetDashboardsInstance(process).GetProcessDashboards();
		}

		[HttpGet("{process}/portfolio/{portfolioId}")]
		public List<ProcessDashboard> GetPortfolioDashboards(string process, int portfolioId) {
			return GetDashboardsInstance(process).GetProcessDashboards(portfolioId);
		}

		[HttpGet("{process}/{dashboardId}")]
		public ProcessDashboard Get(string process, int dashboardId) {
			return GetDashboardsInstance(process).GetDashboard(dashboardId);
		}

		[HttpGet("{process}/{dashboardId}/hide")]
		public ProcessDashboard GetHidden(string process, int dashboardId) {
			return GetDashboardsInstance(process).GetDashboardExcludeHidden(dashboardId);
		}


		[HttpPost("{process}")]
		public ProcessDashboard Post(string process, [FromBody] ProcessDashboard dashboard) {
			CheckRight("write");
			if (ModelState.IsValid) {
				var dashboardInstance = GetDashboardsInstance(process);
				var newId = dashboardInstance.AddDashboard(dashboard);
				return dashboardInstance.GetDashboard(newId);
			}

			return null;
		}

        [HttpGet("{process}/linkedChartsData/{dashboardId}")]
        public  List<Dictionary<string, object>> GetChartLinkData(string process, string placeholder, int dashboardId) {
            var dashboardInstance = GetDashboardsInstance(process);
            return GetDashboardsInstance(process).GetChartLinkData(dashboardId);
        }

        [HttpGet("{process}/linkedChartsNew/{dashboardId}")]
        public List<Dictionary<string, object>> CreateChartLinkAndUpdateData(string process, string palceholder, int dashboardId) {
            var dashboardInstance = GetDashboardsInstance(process);
            return GetDashboardsInstance(process).CreateChartLinkAndUpdateData(dashboardId);
        }

        [HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody] ProcessDashboard dashboard) {
			CheckRight("write");
			var dashboardsInstance = GetDashboardsInstance(process);
			dashboardsInstance.SaveDashboard(dashboard);
			return new ObjectResult(dashboard);
		}

		private ProcessDashboards GetDashboardsInstance(string process = "") {
			return new ProcessDashboards(instance, process, currentSession);
		}

		[HttpDelete("{process}/{processDashboardIds}")]
		public IActionResult Delete(string process, string processDashboardIds) {
			CheckRight("delete");
			var dashboards = new ProcessDashboards(instance, process, currentSession);
			foreach (var id in processDashboardIds.Split(',')) {
				dashboards.DeleteDashboard(Conv.ToInt(id));
			}

			return new StatusCodeResult(200);
		}
	}
}