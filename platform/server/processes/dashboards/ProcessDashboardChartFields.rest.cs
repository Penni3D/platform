﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.dashboards.chartConfigs;
using Keto5.x.platform.server.processes.itemColumns;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.dashboards {
	[Route("v1/settings/[controller]")]
	public class RegisteredCharts : RestBase {
		// Configuration and data services for single charts

		private static readonly Dictionary<string, ProcessDashboardFieldsConfigBase> chartTypes =
			new Dictionary<string, ProcessDashboardFieldsConfigBase>();

		[HttpGet("chartTypes")]
		public List<ProcessDashboardChartConfig> GetChartTypes() {
			var configs = new List<ProcessDashboardChartConfig>();
			foreach (var value in chartTypes.Values) {
				configs.Add(value.GetConfig());
			}

			return configs;
		}

		[HttpGet("{process}/{chartType}")]
		public Dictionary<string, object> GetFields(string process, string chartType) {
			if (!chartTypes.ContainsKey(chartType)) return null;
			var config = chartTypes[chartType];
			config.ResetFieldConfig();
			config.SetItemColumns(new ItemColumns(instance, process, currentSession));
			return config.GetFieldConfigs();
		}

		[HttpGet("{process}/data/{dashboardId}")]
		public string GetChartData(string process, int dashboardId) {
			return GetChartData(process, dashboardId, null, null);
		}

		[HttpPost("{process}/data/{dashboardId}")]
		public string GetChartDataPost(string process, int dashboardId,
			[FromBody] Dictionary<string, object> dashboardFilters) {
			return GetChartData(process, dashboardId, null, null, dashboardFilters);
		}

		[HttpGet("{process}/data/{dashboardId}/{searchText}")]
		public string GetChartData(string process, int dashboardId, string searchText) {
			return GetChartData(process, dashboardId, searchText, null);
		}

		[HttpPost("{process}/data/{dashboardId}/{searchText}")]
		public string GetChartDataPost(string process, int dashboardId, string searchText,
			[FromBody] Dictionary<string, object> dashboardFilters) {
			return GetChartData(process, dashboardId, searchText, null, dashboardFilters);
		}

		[HttpGet("{process}/data/{dashboardId}/{searchText}/{filters}")]
		public string GetChartData(string process, int dashboardId, string searchText = "", string filters = "",
			Dictionary<string, object> dashboardFilters = null) {
			var dashboards = new ProcessDashboards(instance, process, currentSession);
			var chart = dashboards.GetDashboardChart(dashboardId);
			if (chart == null) {
				throw new CustomArgumentException("CHART_UNAVAILABLE");
			}
            
			var jsonAsDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(chart.JsonData);
			jsonAsDictionary.Add("process", Conv.ToSql(process));
            jsonAsDictionary.Add("chartId", chart.Id);
            jsonAsDictionary.Add("chartProcess", chart.chartProcess);
            jsonAsDictionary.Add("instance", instance);
			//jsonAsDictionary.Add("filters", filters);
			jsonAsDictionary.Add("searchText", searchText);
			jsonAsDictionary.Add("processPortfolioId", chart.ProcessPortfolioId);
			if (dashboardFilters.ContainsKey("filterItemIds")) {
				jsonAsDictionary.Add("filterItemIds", Conv.ToStr(dashboardFilters["filterItemIds"]));
				dashboardFilters.Remove("filterItemIds");
			}
			else {
				jsonAsDictionary.Add("filterItemIds", "0");
			}
			if (dashboardFilters.ContainsKey("archive")) {
				jsonAsDictionary.Add("archive", Conv.ToDateTime(dashboardFilters["archive"]));
				dashboardFilters.Remove("archive");
			}
			else {
				jsonAsDictionary.Add("archive", null);
			}
			if (dashboardFilters.ContainsKey("phase")) {
				jsonAsDictionary.Add("phase", dashboardFilters["phase"]);
				dashboardFilters.Remove("phase");
			}
			else {
				jsonAsDictionary.Add("phase", null);
			}
			if (dashboardFilters.ContainsKey("offsets")) {
				jsonAsDictionary.Add("offsets", dashboardFilters["offsets"]);
				dashboardFilters.Remove("offsets");
			}
			if (dashboardFilters.ContainsKey("offset_interval")) {
				jsonAsDictionary.Add("offset_interval", dashboardFilters["offset_interval"]);
				dashboardFilters.Remove("offset_interval");
			}
            if (dashboardFilters.ContainsKey("inFrontPage")) {
				jsonAsDictionary.Add("inFrontPage", dashboardFilters["inFrontPage"]);
				dashboardFilters.Remove("inFrontPage");
			}
			if (dashboardFilters.ContainsKey("inMeta")) {
				jsonAsDictionary.Add("inMeta", dashboardFilters["inMeta"]);
				dashboardFilters.Remove("inMeta");
			}
			if (dashboardFilters.ContainsKey("quickFilterPossibilities")) {
				jsonAsDictionary.Add("quickFilterPossibilities", dashboardFilters["quickFilterPossibilities"]);
				dashboardFilters.Remove("quickFilterPossibilities");
			}

			if (dashboardFilters.ContainsKey("dashboardId")) {
				jsonAsDictionary.Add("dashboardId", dashboardFilters["dashboardId"]);
				var connectionB = new ConnectionBase("keto", currentSession);


				string dashFilter = "";
				if (Conv.ToInt(dashboardFilters["dashboardId"]) > 0) {
					dashFilter = " AND process_dashboard_chart_links.dashboard_id= " + Conv.ToStr(jsonAsDictionary["dashboardId"]) + " ";

					connectionB.SetDBParam(SqlDbType.NVarChar, "@link_process", jsonAsDictionary["process"]);
					connectionB.SetDBParam(SqlDbType.NVarChar, "@link_link_process", jsonAsDictionary["chartProcess"]);
					connectionB.SetDBParam(SqlDbType.Int, "@link_chart_id", jsonAsDictionary["chartId"]);
					var linkSql = "SELECT *,process_portfolio_id AS main_process_portfolio_id FROM process_dashboard_chart_links LEFT JOIN process_dashboards ON process_dashboards.process_dashboard_id=process_dashboard_chart_links.dashboard_id WHERE ',' + chart_ids + ',' LIKE '%,' +  Cast(@link_chart_id AS varchar(10)) + ',%'  AND link_process = @link_link_process AND process_dashboard_chart_links.process = @link_process" + dashFilter;
					DataRow linkData = connectionB.db.GetDataRow(linkSql, connectionB.DBParameters);

					if(linkData != null) {
						jsonAsDictionary.Add("link_process_field", linkData["link_process_field"]);
						jsonAsDictionary.Add("source_field", linkData["source_field"]);
						jsonAsDictionary.Add("main_process_portfolio_id", linkData["main_process_portfolio_id"]);
						jsonAsDictionary.Add("linkId", linkData["process_dashboard_chart_link_id"]);

						if (Conv.ToInt(linkData["portfolio_mode"]) == 1) jsonAsDictionary.Add("mainProcessLinkMode", true);
					}

				}
                else if(jsonAsDictionary.ContainsKey("process") && jsonAsDictionary.ContainsKey("chartProcess") && jsonAsDictionary["process"]!= jsonAsDictionary["chartProcess"]) {
					connectionB.SetDBParam(SqlDbType.NVarChar, "@link_process", jsonAsDictionary["process"]);
					connectionB.SetDBParam(SqlDbType.NVarChar, "@link_link_process", jsonAsDictionary["chartProcess"]);
					connectionB.SetDBParam(SqlDbType.Int, "@link_chart_id", jsonAsDictionary["chartId"]);
					var linkSql = "SELECT *,process_portfolio_id AS main_process_portfolio_id FROM process_dashboard_chart_links LEFT JOIN process_dashboards ON process_dashboards.process_dashboard_id=process_dashboard_chart_links.dashboard_id WHERE ',' + chart_ids + ',' LIKE '%,' +  Cast(@link_chart_id AS varchar(10)) + ',%'  AND link_process = @link_link_process AND process_dashboard_chart_links.process = @link_process";

					DataRow linkData = connectionB.db.GetDataRow(linkSql, connectionB.DBParameters);

					if (linkData != null) {
						jsonAsDictionary.Add("link_process_field", linkData["link_process_field"]);
						jsonAsDictionary.Add("source_field", linkData["source_field"]);
						jsonAsDictionary.Add("main_process_portfolio_id", linkData["main_process_portfolio_id"]);
						jsonAsDictionary.Add("linkId", linkData["process_dashboard_chart_link_id"]);

						if (Conv.ToInt(linkData["portfolio_mode"]) == 1) jsonAsDictionary.Add("mainProcessLinkMode", true);
					}
				}

				dashboardFilters.Remove("dashboardId");
			}

			if(jsonAsDictionary.ContainsKey("linkId")==false) jsonAsDictionary.Add("linkId", 0);

			jsonAsDictionary.Add("dashboardFilters", dashboardFilters);
			var chartType = (string) jsonAsDictionary["chartType"];

			var chartFields = new ChartFields(jsonAsDictionary);

			var config = chartTypes[chartType];
			Dictionary<string, object> returnDictionary = null;
			try {
				returnDictionary = config.GetDataFor(chartFields, currentSession);
			} catch (CustomArgumentException exp) {
				returnDictionary = new Dictionary<string, object> {
					{"data", new Dictionary<string, object>()}, {
						"error", new Dictionary<string, object> {
							{"information", exp.Messages},
							{"message", exp.Message},
							{"stacktrace", exp.StackTrace}
						}
					}
				};
			} catch (Exception e) {
				returnDictionary = new Dictionary<string, object> {
					{"data", new Dictionary<string, object>()}, {
						"error", new Dictionary<string, object> {
							{"message", e.Message},
							{"stacktrace", e.StackTrace}
						}
					}
				};
			}

			if (returnDictionary.ContainsKey("data") && returnDictionary["data"].GetType().Name == "Dictionary`2") {
				var dataDict = (Dictionary<string, object>)returnDictionary["data"];
				if (dataDict.ContainsKey("data")) {
					var dataDictL2 = (List<Dictionary<string, object>>)dataDict["data"];
					for (int i = 0; i < dataDictL2.Count; i++) {
						for (int j = 0; j < dataDictL2[i].Keys.Count; j++) {
							var val = dataDictL2[i].ElementAt(j).Value;
							if (val.GetType().Name == "String") {
								if (Conv.ToStr(val).Contains(",")) {
									bool allInts = true;
									List<int> newVals = new List<int>();
									foreach (var splitVal in Conv.ToStr(val).Split(",")) {
										int n;
										bool isNumeric = int.TryParse(splitVal, out n);
										if (isNumeric == false) {
											allInts = false;
										}
										else {
											newVals.Add(Conv.ToInt(splitVal));
										}
									}
									if (allInts) {
										((List<Dictionary<string, object>>)((Dictionary<string, object>)(returnDictionary["data"]))["data"])[i][dataDictL2[i].Keys.ElementAt<string>(j)] = newVals;
									}
								}
							}
						}
					}
				}
			}

			if (returnDictionary == null) {
				returnDictionary = new Dictionary<string, object>();
			}

			returnDictionary.Add("chartFields", chartFields);
			return success(returnDictionary);
		}

		[HttpPost("{process}/data/{dashboardId}/{searchText}/{filters}")]
		public string GetChartDataPost(string process, int dashboardId, string searchText = "", string filters = "",
            [FromBody] Dictionary<string, object> dashboardFilters = null) {
			return GetChartData(process, dashboardId, searchText, filters, dashboardFilters);
		}

		public static void AddType(ProcessDashboardFieldsConfigBase obj) {
			chartTypes.Add(obj.GetConfig().Value, obj);
		}
	}
}