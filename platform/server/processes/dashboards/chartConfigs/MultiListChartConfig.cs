﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class MultiListChartConfig : ProcessDashboardFieldsConfigBase {
	    public MultiListChartConfig() {
            Config.Name = "multiList";
            Config.Value = "multiList";
            Config.TranslationKey = "MULTI_LIST_CHART";
        }

	    protected override void InitializeFieldConfig() {
            var itemColumnList = ItemColumns.GetItemColumns();
            AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
            AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
            AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
        }

	    public override Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession) {

            // Needed fields for multilist chart type
            var multiListDataCommonJson = JObject.Parse(chartFields.OriginalData["multiListDataCommon"].ToString());
            if (Conv.ToStr(multiListDataCommonJson["legendList"].First) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(multiListDataCommonJson["legendList"].First));

            var multiListDataJson = JObject.Parse(chartFields.OriginalData["multiListData"].ToString());
            foreach (var obj in multiListDataJson) {
                if (Conv.ToStr(obj.Value["sourceList"].First) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(obj.Value["sourceList"].First));
            }
            //

            var dashboards =
                new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
            var returnObject = new Dictionary<string, object>();
            returnObject.Add("data", dashboards.GetCurrentData(chartFields));
            return returnObject;
        }
    }
}



