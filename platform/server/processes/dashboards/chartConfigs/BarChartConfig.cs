﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class BarChartConfig : ProcessDashboardFieldsConfigBase {
	    public BarChartConfig() {
            Config.Name = "Bar (Deprecated)";
            Config.Value = "bar";
            Config.TranslationKey = "BAR_CHART";
        }

	    protected override void InitializeFieldConfig() {
            var itemColumnList = ItemColumns.GetItemColumns();
            AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
            AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
            AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
        }

	    public override Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession) {

            //Needed fields for pie and bar chart
            if (Conv.ToStr(chartFields.ValueField) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.ValueField));
            if (Conv.ToStr(chartFields.GroupByField) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.GroupByField));
            if (Conv.ToStr(chartFields.LabelField) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.LabelField));
            if (Conv.ToStr(chartFields.NameField) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.NameField));

            var dashboards = new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
            if (chartFields.Grouping && !string.IsNullOrEmpty(chartFields.SubGroupByField)) {
                return dashboards.GetStackedDataForBarChart(chartFields);
            }
            return dashboards.GetCurrentData(chartFields);
        }
    }
}



