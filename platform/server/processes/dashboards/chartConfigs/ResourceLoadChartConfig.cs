﻿using System.Collections.Generic;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class ResourceLoadChartConfig : ProcessDashboardFieldsConfigBase {
	    public ResourceLoadChartConfig() {
            Config.Name = "resourceLoad";
            Config.Value = "resourceLoad";
            Config.TranslationKey = "RESOURCE_LOAD_CHART";
        }

	    protected override void InitializeFieldConfig() {
            var itemColumnList = ItemColumns.GetItemColumns();
            AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
            AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
            AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
        }

	    public override Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession) {
            var dashboards =
                new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
            var returnObject = new Dictionary<string, object>();
            returnObject.Add("data", dashboards.GetCurrentData(chartFields));
            return returnObject;
        }
    }
}



