using System.Collections.Generic;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
	public class ExampleChartConfig : ProcessDashboardFieldsConfigBase {
		/*
		 * This is server side configuration for dashboard chart.
		 * Remember to add this configuration in Startup.cs or wherever it currently is (just search for usage for other chart type classes) 
		 */

		public ExampleChartConfig() {
			Config.Name = "Example";
			Config.Value = "example";
			Config.TranslationKey = "EXAMPLE_CHART";
		}

		protected override void InitializeFieldConfig() {
			// This method brings column data to configuration page, which you then read in javascript 
			var itemColumnList = ItemColumns.GetItemColumns();
			AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
			AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
			AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
		}

		public override Dictionary<string, object> GetDataFor(
			ChartFields chartFields,
			AuthenticatedSession currentSession) {

			// // this method is called when chart is asking for it's data. You can return whatever you want.
			var dashboards = new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
			if (chartFields.Grouping && !string.IsNullOrEmpty(chartFields.SubGroupByField)) {
				return dashboards.GetStackedDataForBarChart(chartFields);
			}
			return dashboards.GetCurrentData(chartFields);
		}
	}
}



