﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class PivotTableConfig : ProcessDashboardFieldsConfigBase {
	    public PivotTableConfig() {
            Config.Name = "Pivot";
            Config.Value = "pivot";
            Config.TranslationKey = "DASHBOARD_PIVOT_TABLE";
        }

	    protected override void InitializeFieldConfig() {
            var itemColumnList = ItemColumns.GetItemColumns();
            AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
            AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
            AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
        }

	    public override Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession) {

			// Needed fields for pivot
			JObject pivotCommon = JObject.Parse(Conv.ToStr(chartFields.OriginalData["pivotCommon"]));
			if (Conv.ToStr(pivotCommon.GetValue("xAxisLevel1").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("xAxisLevel1").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("xAxisLevel1").First));
			if (Conv.ToStr(pivotCommon.GetValue("xAxisLevel2").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("xAxisLevel2").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("xAxisLevel2").First));
			if (Conv.ToStr(pivotCommon.GetValue("xAxisLevel3").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("xAxisLevel3").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("xAxisLevel3").First));
			if (Conv.ToStr(pivotCommon.GetValue("yAxisLevel1").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("yAxisLevel1").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("yAxisLevel1").First));
			if (Conv.ToStr(pivotCommon.GetValue("yAxisLevel2").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("yAxisLevel2").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("yAxisLevel2").First));
			if (Conv.ToStr(pivotCommon.GetValue("yAxisLevel3").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("yAxisLevel3").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("yAxisLevel3").First));
			if (Conv.ToStr(pivotCommon.GetValue("value").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("value").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("value").First));
			if (pivotCommon.ContainsKey("cumulativeEnd") && Conv.ToStr(pivotCommon.GetValue("cumulativeEnd").First) != "" &&
				chartFields.allowedFieldsByDBName.Contains(Conv.ToStr(pivotCommon.GetValue("cumulativeEnd").First)) == false)
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(pivotCommon.GetValue("cumulativeEnd").First));
			//

			var dashboards =
                new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
            var returnObject = new Dictionary<string, object>();
            returnObject.Add("data", dashboards.GetCurrentData(chartFields));
            return returnObject;
        }
    }
}



