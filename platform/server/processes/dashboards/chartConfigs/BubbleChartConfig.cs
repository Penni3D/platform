﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class BubbleChartConfig : ProcessDashboardFieldsConfigBase {
	    public BubbleChartConfig() {
            Config.Name = "Bubble";
            Config.Value = "bubble";
            Config.TranslationKey = "BUBBLE_CHART";
        }

	    protected override void InitializeFieldConfig() {
            var itemColumnList = ItemColumns.GetItemColumns();
            AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
            AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
            AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
        }

	    public override Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession){

            // needed fields for phase and bubble chart types
            if (Conv.ToStr(chartFields.Value2Field) != "" && Conv.ToStr(chartFields.Value2Field) != "processItemCount") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.Value2Field));
            if (Conv.ToStr(chartFields.ValueField) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.ValueField));
            if (Conv.ToStr(chartFields.NameField) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.NameField));
            if (Conv.ToStr(chartFields.LabelField) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(chartFields.LabelField));
            if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "bubble" &&
                chartFields.OriginalData.ContainsKey("colorList") &&
                Conv.ToStr(chartFields.OriginalData["colorListField"]) != "[]") {
                JArray name = (JArray)chartFields.OriginalData["colorList"];
                string nameStr = name.First.ToString();
                if (nameStr.Length > 0) {
                    chartFields.allowedFieldsByDBName.Add(nameStr);
                }
            }

            var dashboards = new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
            return dashboards.GetDataForBubbleChart(chartFields);
        }
    }
}



