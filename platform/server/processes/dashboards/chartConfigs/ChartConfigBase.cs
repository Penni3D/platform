﻿using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class ProcessDashboardChartField {
	    public string PropertyName { get; set; }
	    public string TranslationKey { get; set; }
	    public string FieldValue { get; set; }
	    public bool Multiple { get; set; } = false;
	    public List<ItemColumn> Values { get; set; }
    }

    public class ProcessDashboardChartConfig {
	    public string Name { get; set; }
	    public string Value { get; set; }
	    public string TranslationKey { get; set; }
    }

    /// <summary>
    /// Chart configuration object
    /// </summary>
    public class ChartFields {
	    public ChartFields(){
            
        }

	    public ChartFields(Dictionary<string, object> jsonAsDictionary) {
            OriginalData = jsonAsDictionary;
            NameField = GetFirstIfArrayOrCastToString("nameField", false);
            NameFieldProperty = GetFirstIfArrayOrCastToString("nameFieldProperty");
            LabelField = GetFirstIfArrayOrCastToString("labelField", true);
            LabelFieldProperty = GetFirstIfArrayOrCastToString("labelFieldProperty");
            ValueField = GetFirstIfArrayOrCastToString("valueField", false);
            ValueFieldProperty = GetFirstIfArrayOrCastToString("valueFieldProperty");
            Value2Field = GetFirstIfArrayOrCastToString("value2Field", false);
            Value2FieldProperty = GetFirstIfArrayOrCastToString("value2FieldProperty");
            Grouping = GetBooleanOrDefaultTo("groupResults", false);
            GroupByField= GetFirstIfArrayOrCastToString("groupField");
            GroupByFieldProperty = GetFirstIfArrayOrCastToString("groupFieldProperty");
            GroupingFunction = GetFirstIfArrayOrCastToString("groupWithFunction");
            SubGroupByField = GetFirstIfArrayOrCastToString("subGroupField", false);
            SubGroupByFieldProperty = GetFirstIfArrayOrCastToString("subGroupFieldProperty");
            ShowNulls = GetBooleanOrDefaultTo("showNulls", false);
            Instance = GetFirstIfArrayOrCastToString("instance", true);
            Process = GetFirstIfArrayOrCastToString("process", true);
            ProcessPortfolioId = (int)jsonAsDictionary["processPortfolioId"];
            filterItemIds = (string)jsonAsDictionary["filterItemIds"];
            allowedFieldsByDBName = new List<string>();
        }

	    public string NameField { get; set; }
	    public string NameFieldProperty { get; set; }
	    public string ValueField { get; set; }
	    public string ValueFieldProperty { get; set; }
	    public string Value2Field { get; set; }
	    public string Value2FieldProperty { get; set; }
	    public string LabelField { get; set; }
	    public string GroupingFunction { get; set; }
	    public bool Grouping { get; set; }
	    public string LabelFieldProperty { get; set; }
	    public string GroupByField { get; set; }
	    public string GroupByFieldProperty { get; set; }
	    public string SubGroupByField { get; set; }
	    public string SubGroupByFieldProperty { get; set; }
	    public bool ShowNulls { get; set; }

	    public int ProcessPortfolioId { get; set; }

        public string filterItemIds { get; set; }

        public List<string> allowedFieldsByDBName { get; set; }

        public string Process { get; set; }
	    public string Instance { get; set; }
	    public Dictionary<string, object> OriginalData { get; set; }
	    public bool IsSetupFine { get; set; } = true;

	    public string GetFieldIfExists(string key) {
		    return OriginalData.ContainsKey(key) ? (string)OriginalData[key] : "";
        }

	    public object GetRawFieldIfExists(string key) {
            return OriginalData.ContainsKey(key) ? OriginalData[key] : null;
        }

	    private string GetFirstIfArrayOrCastToString(string key, bool required = false) {
            if (OriginalData.ContainsKey(key)) {
                if (OriginalData[key] is JArray) {
                    var field = (JArray)OriginalData[key];

                    if (field.Count > 0) {
                        var obj = field.First;
                        return (string)field.First;
                    }
                    return null;
                }
                return (string)OriginalData[key];
            }
            if (required) {
                IsSetupFine = false;
            }
            return null;
        }

	    private bool GetBooleanOrDefaultTo(string key, bool defaultValue) {
            if (OriginalData.ContainsKey(key)) {
                return (bool)OriginalData[key];
            }
            return defaultValue;
        }
    }

    /**
     * Configuration object to define available selections for different chart types
     */
    public abstract class ProcessDashboardFieldsConfigBase {
	    protected const string ValueFieldName = "valueField";
	    protected const string ValueFieldTranslation = "CHART_VALUE_FIELD";
	    public const string ValueFieldDictionaryKey = "valueFields";

	    protected const string LabelFieldName = "labelField";
	    protected const string LabelFieldTranslation = "CHART_LABEL_FIELD";
	    public const string LabelFieldDictionaryKey = "labelFields";

	    protected const string GroupByFieldName = "groupByField";
	    protected const string GroupByFieldTranslation = "CHART_GROUP_BY_FIELD";
	    public const string GroupByFieldDictionaryKey = "groupByFields";
	    protected ProcessDashboardChartConfig Config;

	    protected Dictionary<string, object> FieldConfig;

	    protected ItemColumns ItemColumns;

	    protected List<int> NumberDataTypes = new List<int> { 1, 2 };
	    protected List<int> TextDataTypes = new List<int> { 0, 4 };


	    protected ProcessDashboardFieldsConfigBase() {
            Config = new ProcessDashboardChartConfig();
            ResetFieldConfig();
        }

	    public void ResetFieldConfig(){
            FieldConfig = new Dictionary<string, object>();
        }

	    public void SetItemColumns(ItemColumns itemColumns) {
            ItemColumns = itemColumns;
        }

	    public ProcessDashboardChartConfig GetConfig() {
            return Config;
        }

	    public Dictionary<string, object> GetFieldConfigs() {
            InitializeFieldConfig();
            return FieldConfig;
        }

	    protected void AddFieldConfig(string key, object value) {
            FieldConfig.Add(key, value);
        }

	    protected abstract void InitializeFieldConfig();

	    protected ProcessDashboardChartField GetProcessDashboardChartField(
            string propertyName,
            string translationKey,
            List<ItemColumn> columns) {
            var field = new ProcessDashboardChartField{
                TranslationKey = translationKey,
                Values = columns,
                PropertyName = propertyName
            };
            return field;
        }

	    protected ProcessDashboardChartField GetValueFields(List<ItemColumn> itemColumnList) {
            return GetProcessDashboardChartField(
                ValueFieldName,
                ValueFieldTranslation,
                itemColumnList);
        }

	    protected ProcessDashboardChartField GetLabelFields(List<ItemColumn> itemColumnList) {
            return GetProcessDashboardChartField(
                LabelFieldName,
                LabelFieldTranslation,
                itemColumnList.Where(p => TextDataTypes.Contains(p.DataType)).ToList());
        }

	    protected ProcessDashboardChartField GetGroupByFields(List<ItemColumn> itemColumnList) {
            return GetProcessDashboardChartField(
                GroupByFieldName,
                GroupByFieldTranslation,
                itemColumnList);
        }

	    /// <summary>
        /// Rest service calls for this method. Override it.
        /// </summary>
        /// <param name="chartFields"></param>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        public abstract Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession);
    }
}
