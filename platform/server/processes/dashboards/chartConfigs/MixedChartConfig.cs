﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class MixedChartConfig : ProcessDashboardFieldsConfigBase {
	    public MixedChartConfig() {
            Config.Name = "Mixed";
            Config.Value = "mixed";
            Config.TranslationKey = "MIXED_CHART";
        }

	    protected override void InitializeFieldConfig() {
            var itemColumnList = ItemColumns.GetItemColumns();
            AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
            AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
            AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
        }

	    public override Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession) {

			// Needed fields for mixed chart type

			//useListMode
			var mixedDataCommonJson = JObject.Parse(chartFields.OriginalData["mixedDataCommon"].ToString());
			if (Conv.ToStr(mixedDataCommonJson["xaxis"].First) != "")
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(mixedDataCommonJson["xaxis"].First));
			if (Conv.ToStr(mixedDataCommonJson["cumulativeEnd"].First) != "")
				chartFields.allowedFieldsByDBName.Add(Conv.ToStr(mixedDataCommonJson["cumulativeEnd"].First));

			if (mixedDataCommonJson.ContainsKey("useListMode") == false || Conv.ToInt(mixedDataCommonJson["useListMode"].First) != 1) {
				var mixedDataJson = JObject.Parse(chartFields.OriginalData["mixedData"].ToString());
				foreach (var obj in mixedDataJson) {
					if (Conv.ToStr(obj.Value["dataSource"].First) != "")
						chartFields.allowedFieldsByDBName.Add(Conv.ToStr(obj.Value["dataSource"].First));
				}
			}
			else {
				var mixedListDataJson = JObject.Parse(chartFields.OriginalData["mixedListData"].ToString());
				if (Conv.ToStr(mixedListDataJson["dataSource"].First) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(mixedListDataJson["dataSource"].First));
				if (Conv.ToStr(mixedListDataJson["selectedList"].First) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(mixedListDataJson["selectedList"].First));
			}
			//

			var dashboards =
                new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
			var returnObject = new Dictionary<string, object>();
			returnObject.Add("data", dashboards.GetCurrentData(chartFields));
            return returnObject;
        }
    }
}



