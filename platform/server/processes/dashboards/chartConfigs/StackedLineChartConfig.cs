﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
	public class StackedLineChartConfig : ProcessDashboardFieldsConfigBase {
		public StackedLineChartConfig() {
			Config.Name = "Stacked Lines (Deprecated)";
			Config.Value = "stacked_lines";
			Config.TranslationKey = "STACKED_LINES_CHART";
		}

		protected override void InitializeFieldConfig() {
			var itemColumnList = ItemColumns.GetItemColumns();
			AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
			AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
			AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));

			var timeRanges = new List<ItemColumn>();
			timeRanges.Add(new ItemColumn {Name = "yy"});
			timeRanges.Add(new ItemColumn {Name = "MM"});
			timeRanges.Add(new ItemColumn {Name = "dd"});

			var dateRanges = GetProcessDashboardChartField(
				"dateRangeField",
				"DATA_RANGE_FIELD",
				timeRanges
			);
			AddFieldConfig("dateRangeField", dateRanges);
		}

		public override Dictionary<string, object> GetDataFor(
			ChartFields chartFields,
			AuthenticatedSession currentSession) {
			var dashboards = new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);

			var timespan = chartFields.GetRawFieldIfExists("dateRangeField");
			var span = "dd";
			if (timespan != null) {
				var spanArray = JArray.Parse(timespan.ToString());
				if (spanArray.Count > 0) {
					span = spanArray[0].ToString();
				}
			}

			var dateString = DateTime.Now.ToString("yyyy-MM-dd");
			var historyColumnCount = chartFields.GetFieldIfExists("historyColumnCount");

			var columnCount = 10;
			if (!string.IsNullOrEmpty(historyColumnCount)) {
				columnCount = int.Parse(historyColumnCount);
			}

			var now = DateTime.Now;
			var dates = new List<DateTime>();
			for (var i = 1; i < columnCount; i++) {
				if (span == "yy") {
					dates.Add(now.AddYears(-i));
				} else if (span == "MM") {
					dates.Add(now.AddMonths(-i));
				} else {
					dates.Add(now.AddDays(-i));
				}
			}

			dates.Reverse();
			var values = new Dictionary<string, object>();
			foreach (var date in dates) {
				var d = date.ToString("yyyy-MM-dd");
				var data = dashboards.GetArchivedData(chartFields, d);
				values.Add(d, data);
			}

			values.Add(now.ToString("yyyy-MM-dd"), dashboards.GetCurrentData(chartFields));
			var retVal = dashboards.GetArchivedData(chartFields, dateString);
			if (retVal != null) {
				retVal.Add("values", values);
			}

			return retVal;
		}
	}
}