﻿using System.Collections.Generic;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards.chartConfigs {
    public class CircularVisualizationConfig : ProcessDashboardFieldsConfigBase {
	    public CircularVisualizationConfig() {
            Config.Name = "Circular";
            Config.Value = "circular";
            Config.TranslationKey = "DASHBOARD_CIRCULAR_VISUALIZATION";
        }

	    protected override void InitializeFieldConfig() {
            var itemColumnList = ItemColumns.GetItemColumns();
            AddFieldConfig(ValueFieldDictionaryKey, GetValueFields(itemColumnList));
            AddFieldConfig(LabelFieldDictionaryKey, GetLabelFields(itemColumnList));
            AddFieldConfig(GroupByFieldDictionaryKey, GetGroupByFields(itemColumnList));
        }

	    public override Dictionary<string, object> GetDataFor(
            ChartFields chartFields,
            AuthenticatedSession currentSession) {

            // Needed fields for circular chart type
            var circularData = (JArray)chartFields.OriginalData["circularData"];

            foreach (var obj in circularData) {
                if (Conv.ToStr(obj["value1"].First) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(obj["value1"].First));
                if (Conv.ToStr(obj["value2"].First) != "") chartFields.allowedFieldsByDBName.Add(Conv.ToStr(obj["value2"].First));
            }
            //

            var dashboards =
                new ProcessDashboards(chartFields.Instance, chartFields.Process, currentSession);
            var returnObject = new Dictionary<string, object>();
            returnObject.Add("data", dashboards.GetCurrentData(chartFields));
            return returnObject;
        }
    }
}



