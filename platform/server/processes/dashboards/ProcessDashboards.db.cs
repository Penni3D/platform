﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Wordprocessing;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.dashboards.chartConfigs;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.dashboards {
	/// <summary>
	/// Chart object
	/// </summary>
	public class ProcessDashboardChart {
		public ProcessDashboardChart() { }

		public ProcessDashboardChart(DataRow datarow, AuthenticatedSession session) {
			var id = Conv.ToInt(datarow["process_dashboard_chart_id"]);
			if (id > 0) {
				Id = id;
			}
			DashboardId = Conv.ToInt(datarow["dashboard_id"]);
			chartProcess = Conv.ToStr(datarow["process"]);
			JsonData = Conv.ToStr(datarow["json_data"]);
			ProcessPortfolioId = Conv.ToInt(datarow["process_portfolio_id"]);
			if (datarow.Table.Columns.Contains("dashboard_portfolio_id")) {
				if (Conv.ToStr(datarow["dashboard_portfolio_id"]) == "") {
					DashboardPortfolioId = -1;
				} else {
					DashboardPortfolioId = Conv.ToInt(datarow["dashboard_portfolio_id"]);
				}
			}

			Variable = Conv.ToStr(datarow["variable"]);
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
		}


		public ProcessDashboardChart(DataRow datarow, AuthenticatedSession session, int mode = 1) {
			var id = Conv.ToInt(datarow["process_dashboard_chart_id"]);
			if (id > 0) {
				Id = id;
			}
			DashboardId = Conv.ToInt(datarow["process_dashboard_id"]);
			chartProcess = Conv.ToStr(datarow["process"]);
			JsonData = Conv.ToStr(datarow["json_data"]);
			ProcessPortfolioId = Conv.ToInt(datarow["process_portfolio_id"]);
			if (datarow.Table.Columns.Contains("dashboard_portfolio_id")) {
				if (Conv.ToStr(datarow["dashboard_portfolio_id"]) == "") {
					DashboardPortfolioId = -1;
				} else {
					DashboardPortfolioId = Conv.ToInt(datarow["dashboard_portfolio_id"]);
				}
			}

			Variable = Conv.ToStr(datarow["variable"]);
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
		}



		public int Id { get; set; }
		public string Name { get; set; }
		public int ProcessPortfolioId { get; set; }
		public string JsonData { get; set; }
		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public string chartProcess { get; set; }
		public string process { get; set; }
		public int DashboardPortfolioId { get; set; }
		public int DashboardId { get; set; }
	}

	public class ProcessDashboardOrderedChart : ProcessDashboardChart {
		public ProcessDashboardOrderedChart() { }

		public ProcessDashboardOrderedChart(DataRow datarow, AuthenticatedSession session) : base(datarow, session) {
			if (datarow != null) {
				OrderNo = Conv.ToStr(datarow["order_no"]);
			}
		}

		public string OrderNo { get; set; }
	}

	/// <summary>
	/// Dashboard object
	/// </summary>
	public class ProcessDashboard {
		public ProcessDashboard() {
			Charts = new List<ProcessDashboardOrderedChart>();
		}

		public ProcessDashboard(DataRow datarow, DataTable charts, AuthenticatedSession session,
			List<int> allowedUserGroups = null) {
			var id = Conv.ToInt(datarow["process_dashboard_id"]);
			if (id > 0) {
				Id = id;
			}

			//dashboardId = Conv.ToInt(datarow["process_dashboard_id"]);

			ProcessPortfolioId = Conv.ToInt(datarow["process_portfolio_id"]);
			Variable = (string) datarow["variable"];
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
			//Name = Conv.toStr(LanguageTranslation[session.locale_id]);

			if (charts != null) {
				Charts = new List<ProcessDashboardOrderedChart>();
				foreach (DataRow row in charts.Rows) {
					Charts.Add(new ProcessDashboardOrderedChart(row, session));
				}
			}

			if (allowedUserGroups == null) {
				AllowedUserGroups = new List<int>();
			} else {
				AllowedUserGroups = allowedUserGroups;
			}
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public int ProcessPortfolioId { get; set; }
		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
		public List<int> AllowedUserGroups { get; set; }
		public List<ProcessDashboardOrderedChart> Charts { get; set; }
		public List<JObject> linkData { get; set; }
		public List<JObject> originalLinkData { get; set; }
		public string process { get; set; }
		public string onlyPortfolio { get; set; }

		public List<ProcessDashboardChart> ActualCharts { get; set; }
	}

	/// <summary>
	/// Database services for dashboards and charts
	/// </summary>
	public class ProcessDashboards : ProcessBase {
		private PortfolioSqls pSqlCopy = null;

		public ProcessDashboards(string instance, string process, AuthenticatedSession authenticatedSession) : base(
			instance,
			process, authenticatedSession) {
			SetDBParameters(
				"@process", process,
				"@instance", instance);
		}

		/// <summary>
		/// Fetches all dashboards for all processes and instance of 'this'
		/// This does not include related charts. They will be included when you load specific dashboard
		/// </summary>
		/// <returns>List of dashboards. No chart data incl.</returns>
		public List<ProcessDashboard> GetDashboards() {
			var result = new List<ProcessDashboard>();

			foreach (DataRow row in db
				.GetDatatable("SELECT * FROM process_dashboards WHERE instance = @instance", DBParameters)
				.Rows
			) {
				var dashboard = new ProcessDashboard(row, null, authenticatedSession);
				result.Add(dashboard);
			}

			return result;
		}

		public Dictionary<int,List<int>> GetAllRelations() {
			var returnValue = new Dictionary<int,List<int>>();
			foreach (DataRow row in db
				.GetDatatable("SELECT [process_dashboard_chart_id], process_dashboard_id FROM process_dashboard_chart_map ", DBParameters)
				.Rows) {

				if (!returnValue.ContainsKey(Conv.ToInt(row["process_dashboard_id"]))) {
					returnValue.Add(Conv.ToInt(row["process_dashboard_id"]), new List<int>());
				}

				if (!returnValue[Conv.ToInt(row["process_dashboard_id"])].Contains(Conv.ToInt(row["process_dashboard_chart_id"]))) {
					returnValue[Conv.ToInt(row["process_dashboard_id"])].Add(Conv.ToInt(row["process_dashboard_chart_id"]));
				}

			}
			return returnValue;
		}


		/// <summary>
		/// Fetches all dashboards for process and instance of 'this'
		/// This does not include related charts. They will be included when you load specific dashboard
		/// </summary>
		/// <returns>List of dashboards. No chart data incl.</returns>
		public List<ProcessDashboard> GetProcessDashboards() {
			var result = new List<ProcessDashboard>();

			foreach (DataRow row in db
				.GetDatatable("SELECT * FROM process_dashboards WHERE instance = @instance AND process = @process",
					DBParameters)
				.Rows) {
				var listOfAllowedGroupIds = GetListOfAllowedUserGroupIds((int) row["process_dashboard_id"]);
				var dashboard = new ProcessDashboard(row, null, authenticatedSession, listOfAllowedGroupIds);
				result.Add(dashboard);
			}

			return result;
		}

		public List<ProcessDashboard> GetProcessDashboards(int portfolioId) {
			var result = new List<ProcessDashboard>();
			SetDBParameter("@portfolioId", portfolioId);
			foreach (DataRow row in db
				.GetDatatable("SELECT * FROM process_dashboards " +
				              "WHERE instance=@instance AND process=@process " +
				              "AND process_portfolio_id=@portfolioId", DBParameters)
				.Rows) {
				var listOfAllowedGroupIds = GetListOfAllowedUserGroupIds((int) row["process_dashboard_id"]);
				var dashboard = new ProcessDashboard(row, null, authenticatedSession, listOfAllowedGroupIds);
				if (ShouldUserSeeThisDashboard(dashboard)) {
					SetDBParameter("@pdi", row["process_dashboard_id"]);
					dashboard.ActualCharts = new List<ProcessDashboardChart>();

					var sql = "SELECT dc.*, pdcm.* FROM process_dashboard_chart_map pdcm " +
					          "INNER JOIN process_dashboard_charts dc ON dc.process_dashboard_chart_id = pdcm.process_dashboard_chart_id " +
					          "where process_dashboard_id = @pdi";
					foreach (DataRow row2 in db.GetDatatable(sql, DBParameters).Rows) {
						dashboard.ActualCharts.Add(new ProcessDashboardChart(row2, authenticatedSession, 1));
					}


					result.Add(dashboard);
				}
			}

			return result;
		}

		private bool ShouldUserSeeThisDashboard(ProcessDashboard dashboard) {
			var addDashboardToList = false;
			var usersGroups = (List<int>) authenticatedSession.GetMember("groups");
			if (dashboard.AllowedUserGroups != null && dashboard.AllowedUserGroups.Count > 0) {
				if (dashboard.AllowedUserGroups.Any(usersGroups.Contains)) {
					addDashboardToList = true;
				}
			} else {
				addDashboardToList = true;
			}

			return addDashboardToList;
		}

		public string GetCustomChartSql(ChartFields chartFields) {
			return GetQueryForRows(chartFields);
        }

		/// <summary>
		/// Fetches dashboard by id. Instance must match.
		/// </summary>
		/// <param name="dashboardId">Id of wanted dashboard</param>
		/// <returns>Dashboard object with related charts</returns>
		public ProcessDashboard GetDashboard(int dashboardId) {
			SetDBParam(SqlDbType.Int, "@processDashboardId", dashboardId);
			var data =
				db.GetDataRow(
					"SELECT * FROM process_dashboards WHERE instance = @instance AND process_dashboard_id=@processDashboardId",
					DBParameters);
			if (data == null) {
				throw new CustomArgumentException("DATA_NOT_FOUND");
			}


			/*var tryHideSql =
				"SELECT pdc.*, pd.process_portfolio_id AS dashboard_portfolio_id, map.order_no FROM process_dashboard_chart_map map " +
				"LEFT JOIN process_dashboards pd ON pd.process_dashboard_id = map.process_dashboard_id " +
				"LEFT JOIN process_dashboard_charts pdc ON pdc.process_dashboard_chart_id = map.process_dashboard_chart_id " +
				"WHERE pd.process_dashboard_id = @processDashboardId AND ISNULL((SELECT hide FROM process_dashboard_chart_links WHERE chart_ids = map.process_dashboard_chart_id AND dashboard_id = @processDashboardId),0) = 0 ORDER BY map.order_no ";*/


			var chartSql =
				"select @processDashboardId AS dashboard_id, pdc.*, pdcm.order_no ,pd.process_portfolio_id AS dashboard_portfolio_id from process_dashboard_charts pdc " +
				" LEFT JOIN process_dashboard_chart_map pdcm on pdcm.process_dashboard_chart_id = pdc.process_dashboard_chart_id LEFT JOIN process_dashboards pd ON pd.process_dashboard_id=@processDashboardId " +
				"where pdcm.process_dashboard_id = @processDashboardId OR (',' + (SELECT STUFF((SELECT DISTINCT ',' + chart_ids from process_dashboard_chart_links links WHERE links.dashboard_id=@processDashboardId FOR XML PATH('')),1,1,'')) + ',' LIKE '%,' + CAST(pdc.process_dashboard_chart_id AS varchar(10)) +',%' AND pdcm.process_dashboard_id = @processDashboardId)" +
				"GROUP BY pdc.process_dashboard_chart_id,pdc.instance,pdc.process,pdc.name,pdc.json_data,pdc.process_portfolio_id,pdc.archive_userid,pdc.archive_start,pdc.variable,pdcm.order_no,pd.process_portfolio_id " +
				"ORDER BY pdcm.order_no";

			var relatedCharts = db.GetDatatable(chartSql, DBParameters);


			var listOfAllowedGroupIds = GetListOfAllowedUserGroupIds(dashboardId);

			return new ProcessDashboard(data, relatedCharts, authenticatedSession, listOfAllowedGroupIds);
		}

			public ProcessDashboard GetDashboardExcludeHidden(int dashboardId) {
				SetDBParam(SqlDbType.Int, "@processDashboardId", dashboardId);
			var data =
				db.GetDataRow(
					"SELECT * FROM process_dashboards WHERE instance = @instance AND process_dashboard_id=@processDashboardId",
					DBParameters);
			if (data == null) {
				throw new CustomArgumentException("DATA_NOT_FOUND");
			}


			var tryHideSql =
				"SELECT @processDashboardId AS dashboard_id, pdc.*, pd.process_portfolio_id AS dashboard_portfolio_id, map.order_no FROM process_dashboard_chart_map map " +
				"LEFT JOIN process_dashboards pd ON pd.process_dashboard_id = map.process_dashboard_id " +
				"LEFT JOIN process_dashboard_charts pdc ON pdc.process_dashboard_chart_id = map.process_dashboard_chart_id " +
				"WHERE pd.process_dashboard_id = @processDashboardId AND ISNULL((SELECT hide FROM process_dashboard_chart_links WHERE chart_ids = map.process_dashboard_chart_id AND dashboard_id = @processDashboardId),0) = 0 ORDER BY map.order_no ";

			var relatedCharts = db.GetDatatable(tryHideSql, DBParameters);

			var listOfAllowedGroupIds = GetListOfAllowedUserGroupIds(dashboardId);

			return new ProcessDashboard(data, relatedCharts, authenticatedSession, listOfAllowedGroupIds);
		}

		private List<int> GetListOfAllowedUserGroupIds(int dashboardId) {
			var dashboardIdParam = (IDbDataParameter) SetDBParam(SqlDbType.Int, "@dashboardUserGroupId", dashboardId);
			var allowedUserGroups = BuildSelectQuery()
				.Columns("user_group_id")
				.From("process_dashboard_user_groups")
				.Where("process_dashboard_id", SqlComp.EQUALS, dashboardIdParam)
				.Fetch();
			var listOfAllowedGroupIds = allowedUserGroups.Select(x => (int) x["user_group_id"]).ToList();
			return listOfAllowedGroupIds;
		}


		/// <summary>
		/// Fetches all charts
		/// </summary>
		/// <param name="process"></param>
		/// <returns></returns>
		public List<ProcessDashboardChart> GetCharts() {
			var relatedCharts = db.GetDatatable(
				"SELECT 0 AS dashboard_id ,* FROM process_dashboard_charts WHERE instance = @instance",
				DBParameters);

			var charts = new List<ProcessDashboardChart>();
			foreach (DataRow row in relatedCharts.Rows) {
				charts.Add(new ProcessDashboardChart(row, authenticatedSession));
			}

			return charts;
		}

		/// <summary>
		/// Fetches charts by process name.
		/// </summary>
		/// <param name="process"></param>
		/// <returns></returns>
		public List<ProcessDashboardChart> GetChartsForProcess(string process, bool links = false) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var chartSql = "select 0 AS dashboard_id, * from process_dashboard_charts " +
			               "where process=@process";

			if (links) {
				chartSql =
					" SELECT 0 AS dashboard_id, c.process_dashboard_chart_id, c.instance, c.process, c.name, c.json_data, c.process_portfolio_id, c.variable FROM ( " +
					"       SELECT process_dashboard_chart_id FROM process_dashboard_charts WHERE process = @process " +
					"       UNION " +
					"       SELECT * FROM STRING_SPLIT(" +
						"		ISNULL((SELECT " +
							"		(SELECT chart_ids + ',' FROM process_dashboard_chart_links WHERE process = @process FOR XML PATH('')) + '0'" +
						"		),''),','" +
					"		) WHERE value > 0 " +
					" ) iq " +
					" LEFT JOIN process_dashboard_charts c ON c.process_dashboard_chart_id = iq.process_dashboard_chart_id  " +
					" GROUP BY c.process_dashboard_chart_id, c.instance, c.process, c.name, c.json_data, c.process_portfolio_id, c.variable  ";
			}

			var relatedCharts = db.GetDatatable(chartSql, DBParameters);
			var charts = new List<ProcessDashboardChart>();
			foreach (DataRow row in relatedCharts.Rows) {
				charts.Add(new ProcessDashboardChart(row, authenticatedSession));
			}

			return charts;
		}

		/// <summary>
		/// Fetches single chart
		/// </summary>
		/// <param name="chartId"></param>
		/// <returns>Chart data</returns>
		public ProcessDashboardChart GetDashboardChart(int chartId) {
			SetDBParam(SqlDbType.Int, "@dashboardChartId", chartId);
			var d = db.GetDataRow(
				"SELECT 0 AS dashboard_id, * FROM process_dashboard_charts WHERE instance = @instance AND process_dashboard_chart_id=@dashboardChartId",
				DBParameters);
			if (d != null) {
				return new ProcessDashboardChart(d, authenticatedSession);
			}

			throw new CustomArgumentException("DATA_NOT_FOUND");
		}

		/* CRUD OPERATIONS */
		// DASHBOARD METHODS
		public int AddDashboard(ProcessDashboard dashboard) {
			dashboard.Variable =
				translations.GetTranslationVariable(process,
					dashboard.LanguageTranslation[authenticatedSession.locale_id], "DASH");

			SetDashboardDbParameters(dashboard);

			var sql = "INSERT INTO process_dashboards (instance, process, name, process_portfolio_id, variable) " +
			          "VALUES (@instance, @process, 'unused', @process_portfolio_id, @variable)";

			var result = db.ExecuteInsertQuery(sql, DBParameters);

			foreach (var translation in dashboard.LanguageTranslation) {
				translations.insertOrUpdateProcessTranslation(process, dashboard.Variable, translation.Value,
					translation.Key);
			}

			StoreUserGroupsForDashboard(dashboard);
			return result;
		}

		public List<Dictionary<string, object>> CreateChartLinkAndUpdateData(int dashboard_id) {
			SetDBParam(SqlDbType.NVarChar, "@dashboard_id", dashboard_id);

			var sql = "INSERT INTO process_dashboard_chart_links (instance, process,dashboard_id) " +
			          "VALUES (@instance, @process, @dashboard_id)";

			db.ExecuteInsertQuery(sql, DBParameters);
			return GetChartLinkData(dashboard_id);
		}

		public List<Dictionary<string, object>> GetChartLinkData(int dashboard_id) {
			SetDBParam(SqlDbType.NVarChar, "@dashboard_id", dashboard_id);
			var sql =
				"SELECT *,pd.process_portfolio_id AS dashboard_portfolio_id FROM process_dashboard_chart_links LEFT JOIN process_dashboards pd ON pd.process_dashboard_id=@dashboard_id  WHERE dashboard_id = @dashboard_id";
			return db.GetDatatableDictionary(sql, DBParameters);
		}

		public void SaveDashboard(ProcessDashboard dashboard) {
			SetDashboardDbParameters(dashboard);
			SetDBParam(SqlDbType.NVarChar, "@dashboardId", dashboard.Id);
			//SetDBParam(SqlDbType.NVarChar, "@process_name", dashboard.process);
			var sql = "UPDATE process_dashboards " +
			          "SET instance=@instance, process=@process, name='unused', process_portfolio_id=@process_portfolio_id " +
			          "WHERE process_dashboard_id=@dashboardId";
			db.ExecuteUpdateQuery(sql, DBParameters);
			if (dashboard.onlyPortfolio == null) {
				//var process = "";

				var linkDataSql = "SELECT * FROM process_dashboard_chart_links WHERE dashboard_id=@dashboardId";
				var linkData = db.GetDatatable(linkDataSql, DBParameters);
				var existingLinks = new List<string>();

				var updateLinkDataIds = new List<int>();
				var updateLinkDataOrder = new List<string>();
				var originalLinkDataIds = new List<int>();

				foreach (var linkObj in dashboard.originalLinkData) {
					var ids = Conv.ToStr(linkObj["chart_ids"]).Split(",");
					foreach (string id in ids) {
						if (originalLinkDataIds.Contains(Conv.ToInt(id)) == false) {
							originalLinkDataIds.Add(Conv.ToInt(id));
						}
					}
				}

				foreach (var linkObj in dashboard.linkData) {
					existingLinks.Add(Conv.ToStr(linkObj["process_dashboard_chart_link_id"]));
					SetDBParam(SqlDbType.Int, "@hide", Conv.ToInt(Conv.ToStr(linkObj["hide"])));
					SetDBParam(SqlDbType.Int, "@portfolio_mode", Conv.ToInt(Conv.ToStr(linkObj["portfolio_mode"].Last)));
					SetDBParam(SqlDbType.NVarChar, "@source_field", Conv.ToStr(linkObj["source_field"].Last));
					SetDBParam(SqlDbType.NVarChar, "@link_process", Conv.ToStr(linkObj["link_process"].Last));
					SetDBParam(SqlDbType.NVarChar, "@link_process_field",
						Conv.ToStr(linkObj["link_process_field"].Last));
					SetDBParam(SqlDbType.NVarChar, "@chart_ids",
						String.Join(",", linkObj.Value<JArray>("chart_ids").Select(jv => (string) jv).ToArray()));
					SetDBParam(SqlDbType.NVarChar, "@link_id", Conv.ToStr(linkObj["process_dashboard_chart_link_id"]));

					var idsArr = linkObj.Value<JArray>("chart_ids").Select(jv => (string) jv).ToArray();
					foreach (string id in idsArr) {
						if (updateLinkDataIds.Contains(Conv.ToInt(id)) == false) {
							updateLinkDataIds.Add(Conv.ToInt(id));
							updateLinkDataOrder.Add("");
						}
					}

					var linkSql = "UPDATE process_dashboard_chart_links " +
								  "SET hide = @hide,portfolio_mode=@portfolio_mode,source_field=@source_field,link_process=@link_process,link_process_field=@link_process_field,chart_ids=@chart_ids " +
					              "WHERE dashboard_id=@dashboardId AND process_dashboard_chart_link_id=@link_id";
					db.ExecuteUpdateQuery(linkSql, DBParameters);


				}

				//Delete row if it no longer exist within savedata
				if (linkData.Rows.Count != existingLinks.Count && dashboard.linkData.Count != 0) {
					var toBeRemoved = linkData.Select("process_dashboard_chart_link_id NOT IN(" +
					                                  string.Join(",", existingLinks.ToArray()) + ")");
					foreach (DataRow remR in toBeRemoved) {
						SetDBParam(SqlDbType.Int, "@remove_link_id",
							Conv.ToInt(remR["process_dashboard_chart_link_id"]));
						db.ExecuteDeleteQuery(
							"DELETE FROM process_dashboard_chart_links where process_dashboard_chart_link_id=@remove_link_id",
							DBParameters);
					}
				} else if (dashboard.linkData.Count == 0) {
					foreach (var remR in dashboard.originalLinkData) {
						SetDBParam(SqlDbType.Int, "@remove_link_id",
							Conv.ToInt(remR["process_dashboard_chart_link_id"]));
						db.ExecuteDeleteQuery(
							"DELETE FROM process_dashboard_chart_links where process_dashboard_chart_link_id=@remove_link_id",
							DBParameters);
					}
				}

				StoreUserGroupsForDashboard(dashboard);



				var missingLinkChartIds = new List<int>();
				foreach (var originalId in originalLinkDataIds) {
					if (updateLinkDataIds.Contains(originalId) == false) {
						missingLinkChartIds.Add(originalId);
					}
				}

				db.ExecuteDeleteQuery("DELETE FROM process_dashboard_chart_map where process_dashboard_id=@dashboardId",
					DBParameters);
				var used = new List<int>();
				if (dashboard.Charts != null || dashboard.linkData != null) {
					foreach (var orderedChart in dashboard.Charts) {
						if (missingLinkChartIds.Contains(orderedChart.Id) == false) {
							used.Add(orderedChart.Id);
							SetDBParam(SqlDbType.Int, "@chart_id", orderedChart.Id);
							if (string.IsNullOrEmpty(orderedChart.OrderNo)) {
								orderedChart.OrderNo = "V";
							}

							SetDBParam(SqlDbType.NVarChar, "@orderNo", orderedChart.OrderNo);
							db.ExecuteInsertQuery(
								"INSERT INTO process_dashboard_chart_map (process_dashboard_chart_id, process_dashboard_id, order_no) values(@chart_id, @dashboardId, @orderNo)",
								DBParameters);
						}
					}

					foreach (var id in updateLinkDataIds) {
						if (missingLinkChartIds.Contains(id) == false && used.Contains(id) == false) {
							string orderNo = "";
							SetDBParam(SqlDbType.Int, "@chart_id", id);
							if (string.IsNullOrEmpty(updateLinkDataOrder[updateLinkDataIds.IndexOf(id)])) {
								orderNo = "V";
							}

							SetDBParam(SqlDbType.NVarChar, "@orderNo", orderNo);
							db.ExecuteInsertQuery(
								"INSERT INTO process_dashboard_chart_map (process_dashboard_chart_id, process_dashboard_id, order_no) values(@chart_id, @dashboardId, @orderNo)",
								DBParameters);
						}
					}
				}
			}
			foreach (var translation in dashboard.LanguageTranslation) {
				translations.insertOrUpdateProcessTranslation(process, dashboard.Variable, translation.Value,
					translation.Key);
			}
		}

		private void StoreUserGroupsForDashboard(ProcessDashboard dashboard) {
			// save user groups for dashboard
			if (dashboard.AllowedUserGroups != null) {
				var dashboardIdParameter = SetDBParameter("@dashboardId", dashboard.Id);
				var updateAllowedGroups = BuildDeleteQuery()
					.From("process_dashboard_user_groups")
					.Where(new SqlCondition("process_dashboard_id", SqlComp.EQUALS, dashboardIdParameter))
					.Get();
				var index = 0;

				dashboard.AllowedUserGroups.ForEach(x => {
					var groupIdParam = SetDBParameter("@userGroup" + index, x);
					index++;
					var values = new Dictionary<string, object> {
						{"process_dashboard_id", dashboardIdParameter.ParameterName},
						{"user_group_id", groupIdParam.ParameterName}
					};
					var insertQuery = BuildInsertQuery()
						.To("process_dashboard_user_groups")
						.Values(values)
						.Get();
					var userGroupTable = GetProcessTableName("user_group");
					updateAllowedGroups += "IF EXISTS (SELECT * FROM " + userGroupTable + " WHERE item_id = " +
					                       groupIdParam.ParameterName + ") " +
					                       insertQuery;
				});
				db.ExecuteInsertQuery(updateAllowedGroups, DBParameters);
			}
		}

		public void DeleteDashboard(int id) {
			SetDBParam(SqlDbType.Int, "@dashboardId", id);

			db.ExecuteDeleteQuery("DELETE FROM process_dashboard_chart_map WHERE process_dashboard_id=@dashboardId",
				DBParameters);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_dashboards WHERE process_dashboard_id=@dashboardId AND instance=@instance AND process=@process",
				DBParameters);
			db.ExecuteDeleteQuery("DELETE FROM process_dashboard_chart_links WHERE dashboard_id=@dashboardId",
				DBParameters);
		}

		// CHART CRUD METHODS
		public int AddChart(ProcessDashboardChart chart) {
			chart.Variable =
				translations.GetTranslationVariable(process, chart.LanguageTranslation[authenticatedSession.locale_id],
					"DASHCH");

			SetChartDbParameters(chart);
			var sql =
				"INSERT INTO process_dashboard_charts (instance, process, name, json_data, process_portfolio_id, variable) " +
				"VALUES(@instance, @process, 'unused', @json_data, @process_portfolio_id, @variable)";

			var result = db.ExecuteInsertQuery(sql, DBParameters);

			foreach (var translation in chart.LanguageTranslation) {
				translations.insertOrUpdateProcessTranslation(process, chart.Variable, translation.Value,
					translation.Key);
			}

			return result;
		}

		public void SaveChart(ProcessDashboardChart chart) {
			SetChartDbParameters(chart);
			SetDBParam(SqlDbType.NVarChar, "@processDashboardChartId", chart.Id);
			var sql = "UPDATE process_dashboard_charts " +
			          "SET instance=@instance, process=@process, name='unused', json_data=@json_data, process_portfolio_id=@process_portfolio_id " +
			          "WHERE process_dashboard_chart_id=@processDashboardChartId";
			db.ExecuteUpdateQuery(sql, DBParameters);

			foreach (var translation in chart.LanguageTranslation) {
				translations.insertOrUpdateProcessTranslation(process, chart.Variable, translation.Value,
					translation.Key);
			}
		}

		public void DeleteChart(int id) {
			SetDBParam(SqlDbType.Int, "@chartId", id);
			db.ExecuteDeleteQuery("DELETE FROM process_dashboard_chart_map WHERE process_dashboard_chart_id=@chartId",
				DBParameters);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_dashboard_charts WHERE process_dashboard_chart_id=@chartId AND instance=@instance AND process=@process",
				DBParameters);
			db.ExecuteDeleteQuery(
				"DELETE FROM process_dashboard_chart_links WHERE ',' + chart_ids + ',' LIKE '%,' + CAST(pdcm.process_dashboard_chart_id AS varchar(10)) +',%'",
				DBParameters);
		}

		/* CRUD END */

		/* CHART DATA FETCHING METHODS */

		/// <summary>
		/// Fetches data grouped by field that is referencing to another process
		/// </summary>
		/// <param name="chartFields">ChartField Object</param>
		/// <param name="groupByField">ItemColumn object that is used for grouping</param>
		/// <param name="tableToQuery">Table name to use in query</param>
		/// <returns></returns>
		public DataTable GetGroupedDataByListField(ChartFields chartFields, ItemColumn groupByField,
			string tableToQuery) {
			var linkedValueField = GetLinkedValueFieldTableName(chartFields);

			var textSearchConditions = GetTextSearchConditions(
				chartFields.GetFieldIfExists("searchText"),
				chartFields.ProcessPortfolioId);
			if (!string.IsNullOrEmpty(textSearchConditions)) {
				textSearchConditions = "WHERE " + textSearchConditions;
			} else {
				textSearchConditions = "WHERE (1=1) ";
			}

			var itemColumnIdForGroup = groupByField.ItemColumnId;
			SetDBParam(SqlDbType.Int, "@grouping_item_column_id", itemColumnIdForGroup);
			var aggregateFunction =
				GetAggregateFunctionForGrouping(chartFields.GroupingFunction, chartFields.ValueField);
			var joinsForLinkedItems = "";
			if (!string.IsNullOrEmpty(linkedValueField)) {
				aggregateFunction =
					GetAggregateFunctionForGrouping(chartFields.GroupingFunction,
						"linked." + chartFields.ValueFieldProperty);
				textSearchConditions += " AND ic.name='" + chartFields.ValueField + "'";

				joinsForLinkedItems = "JOIN item_data_process_selections idpc on idpc.item_id = processdata.item_id " +
				                      "JOIN item_columns ic ON ic.item_column_id = idpc.item_column_id " +
				                      "join _" + instance +
				                      "_user_business_value linked on linked.item_id=idpc.selected_item_id ";
			}

			var sql = "SELECT subq.selected_item_id, " + aggregateFunction + " " +
			          "FROM " + tableToQuery + " processdata " +
			          joinsForLinkedItems +
			          "JOIN ( " +
			          GetExternalFieldFilteringString(
				          false,
				          groupByField.Name,
				          chartFields.GetFieldIfExists("filters"),
				          "idps.selected_item_id, idps.item_id") +
			          ") AS subq " +
			          "ON subq.item_id=processdata.item_id " +
			          textSearchConditions +
			          "GROUP BY subq.selected_item_id";

			var groupedData = db.GetDatatable(sql, DBParameters);

			return groupedData;
		}

		public Dictionary<string, object> GetDataForBubbleChart(ChartFields chartFields) {
			if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "phase") {
				if (string.IsNullOrEmpty(chartFields.NameField) ||
				    string.IsNullOrEmpty(chartFields.ValueField) ||
				    string.IsNullOrEmpty(chartFields.Value2Field)) {
					return new Dictionary<string, object>();
				}
			} else {
				if (string.IsNullOrEmpty(chartFields.NameField) ||
				    string.IsNullOrEmpty(chartFields.ValueField) ||
				    string.IsNullOrEmpty(chartFields.Value2Field) ||
				    string.IsNullOrEmpty(chartFields.LabelField)) {
					return new Dictionary<string, object>();
				}
			}

			// Bubble can't be grouped for now
			chartFields.GroupByField = null;
			chartFields.GroupByFieldProperty = null;

			var data = GetCurrentData(chartFields);

			if (!string.IsNullOrEmpty(chartFields.LabelFieldProperty)) {
				var items = GetItemListForFieldName(chartFields.Process, chartFields.LabelField, new List<int>());

				if (items.Count>0) {
					string listName = Conv.ToStr(items[0]["process"]);
					if (listName != null && listName.Contains("list")) {
						var listProcessFieldSql = "SELECT * FROM dbo.processes WHERE process='" + listName + "'";
						DataRow listProcessFields = db.GetDataRow(listProcessFieldSql);

						if (Conv.ToStr(listProcessFields["list_order"]) != "") {
							data.Add("labelItemsOrder", Conv.ToStr(listProcessFields["list_order"]));
						}
						else {
							data.Add("labelItemsOrder", "order_no");
						}
					}
					else {
						data.Add("labelItemsOrder", "order_no");
					}
				}

				data.Add("labelItems", items);
			}

			if (!string.IsNullOrEmpty(chartFields.ValueFieldProperty)) {
				var items = GetItemListForFieldName(chartFields.Process, chartFields.ValueField, new List<int>());

				if (items.Count > 0) {
					string listName = Conv.ToStr(items[0]["process"]);
					if (listName != null && listName.Contains("list")) {
						var listProcessFieldSql = "SELECT * FROM dbo.processes WHERE process='" + listName + "'";
						DataRow listProcessFields = db.GetDataRow(listProcessFieldSql);

						if (Conv.ToStr(listProcessFields["list_order"]) != "") {
							data.Add("valueItemsOrder", Conv.ToStr(listProcessFields["list_order"]));
						}
						else {
							data.Add("valueItemsOrder", "order_no");
						}
					}
					else {
						data.Add("valueItemsOrder", "order_no");
					}
				}

				data.Add("valueItems", items);
			}

			if (!string.IsNullOrEmpty(chartFields.NameFieldProperty)) {
				var items = GetItemListForFieldName(chartFields.Process, chartFields.NameField, new List<int>());
				data.Add("nameItems", items);
			}

			//if (chartFields.OriginalData.ContainsKey("colorListField") &&
			//    Conv.toStr(chartFields.OriginalData["colorListField"]) != "[]") {
			//	JArray name = (JArray) chartFields.OriginalData["colorList"];
			//	var items = GetItemListForFieldName(chartFields.Process, name.First.ToString(), new List<int>());
			//	data.Add("colorItems", items);
			//}

			if (chartFields.OriginalData.ContainsKey("colorListField") && Conv.ToStr(chartFields.OriginalData["colorListField"]) != "[]") {
				JArray name = (JArray)chartFields.OriginalData["colorList"];
				string fieldName = name.Last.ToString();
				string listColumnSql = "SELECT name,data_additional,input_name,data_type,data_additional2 FROM dbo.item_columns WHERE name IN('" + fieldName + "')";
				var listColumns = db.GetDatatableDictionary(listColumnSql);
				string processName = "";

				if (Conv.ToInt(listColumns[0]["data_type"]) == 20) {
					processName = Conv.ToStr(listColumns[0]["input_name"]);
				}
				else if (Conv.ToInt(listColumns[0]["data_type"]) == 16) {
					JObject obj = JObject.Parse(Conv.ToStr(listColumns[0]["data_additional2"]));
					var listColumnId = Conv.ToInt(obj["@SourceColumnId"]);
					if(listColumnId > 0) {
						string sql = "SELECT data_additional FROM dbo.item_columns WHERE item_column_id=" + listColumnId;
						var listRow = db.GetDataRow(sql);
						processName = Conv.ToStr(listRow["data_additional"]);
					}
					else {
						processName = "";
					}

				}
				else {
					processName = Conv.ToStr(listColumns[0]["data_additional"]);
				}

				var listProcessFieldSql = "SELECT * FROM _" + instance + "_" + processName;
				var listProcessFields = db.GetDatatableDictionary(listProcessFieldSql);

				if (processName != null && processName.Contains("list")) {
					var listProcessFieldSql2 = "SELECT * FROM dbo.processes WHERE process='" + processName + "'";
					DataRow listProcessFields2 = db.GetDataRow(listProcessFieldSql2);

					if (Conv.ToStr(listProcessFields2["list_order"]) != "") {
						data.Add("order", Conv.ToStr(listProcessFields2["list_order"]));
					}
					else {
						data.Add("order", "order_no");
					}
				}
				else {
					data.Add("order", "order_no");
				}

				data.Add("colorItems", listProcessFields);
			}

			return data;
		}

		/// <summary>
		/// Get all rows without grouping
		/// </summary>
		/// <param name="chartFields">ChartFields object</param>
		/// <param name="tableToQuery">Table to query</param>
		/// <returns></returns>
		public DataTable GetAllRows(ChartFields chartFields, string tableToQuery) {
			if ((string.IsNullOrEmpty(chartFields.LabelField) || string.IsNullOrEmpty(chartFields.ValueField)) &&
				Conv.ToStr(chartFields.OriginalData["chartType"]) != "phase" &&
				Conv.ToStr(chartFields.OriginalData["chartType"]) != "mixed" &&
				Conv.ToStr(chartFields.OriginalData["chartType"]) != "multiList" &&
				Conv.ToStr(chartFields.OriginalData["chartType"]) != "resourceLoad" &&
				Conv.ToStr(chartFields.OriginalData["chartType"]) != "pivot" &&
				Conv.ToStr(chartFields.OriginalData["chartType"]) != "circular") {
				throw new CustomArgumentException("MISSING_COLUMN_SELECTIONS");
			}

			var query = GetQueryForRows(chartFields, false);
			var items = db.GetDatatable(query);

			if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "pivot") {
				JObject pivotCommon = JObject.Parse(Conv.ToStr(chartFields.OriginalData["pivotCommon"]));

				var ignoreNulls = Conv.ToInt(pivotCommon.GetValue("ignoreNulls").First);

				var ignoreZeros = Conv.ToInt(pivotCommon.GetValue("ignoreZeros").First);
				var valueField = Conv.ToStr(pivotCommon.GetValue("value").First);

				if (ignoreZeros == 1 && ignoreNulls == 1 || ignoreZeros == 1) {
					//filters += " AND " + Conv.toStr(pivotCommon.GetValue("value").First) + " IS NOT NULL AND " + Conv.toStr(pivotCommon.GetValue("value").First) + " !=0";
					var tempResult = items.Select(valueField + " IS NULL OR " + valueField + "=0");
					if (tempResult.Length > 0) {
						foreach (DataRow row in tempResult) {
							items.Rows.Remove(row);
						}

						items.AcceptChanges();
					}
				}
				else if (ignoreNulls == 1) {
					//filters += " AND " + Conv.toStr(pivotCommon.GetValue("value").First) + " IS NOT NULL ";
					var tempResult = items.Select(valueField + "=0");
					if (tempResult.Length > 0) {
						foreach (DataRow row in tempResult) {
							items.Rows.Remove(row);
						}

						items.AcceptChanges();
					}
				}
			}

			ItemDataProcessSelections idps =
				new ItemDataProcessSelections(instance, chartFields.Process, authenticatedSession);
			ItemColumns ic = new ItemColumns(instance, chartFields.Process, authenticatedSession);


			ItemBase ib = new ItemBase(instance, chartFields.Process, authenticatedSession);


			var processTable = GetProcessTableName(chartFields.Process);
			PortfolioSqlService portfolioSqlQueryService =
				new PortfolioSqlService(instance, authenticatedSession, ic, processTable, chartFields.Process);

			var pSqls = pSqlCopy;

			DataTable itemsFinal = items.Copy();
			foreach (var col in pSqls.processColumns) {
				if (itemsFinal.Columns.Contains(col.Name)) itemsFinal.Columns.Remove(col.Name);
				itemsFinal.Columns.Add(col.Name, typeof(object));
			}

			foreach (DataRow row in items.Rows) {
				foreach (var col in pSqls.processColumns) {
					if (row.Table.Columns.Contains(col.Name)) {
						string v = Conv.ToStr(row[col.Name]);

						if (v.Length > 0) {
							if (!col.IsTag()) {
								if (col.LinkProcess != null && col.Name.Equals("linked_tasks")) {
									itemsFinal.Rows[items.Rows.IndexOf(row)][col.Name] =
										idps.GetItemDataProcessSelectionIds(Conv.ToInt(row["item_id"]), col.ItemColumnId,
											col.LinkProcess);
								}
								else {
									itemsFinal.Rows[items.Rows.IndexOf(row)][col.Name] = Array.ConvertAll(v.Split(","), int.Parse);
								}
							}
							else {
								itemsFinal.Rows[items.Rows.IndexOf(row)][col.Name] = v.Split(',');
							}
						}
						else {
							if (!col.IsTag()) {
								itemsFinal.Rows[items.Rows.IndexOf(row)][col.Name] = col.LinkProcess != null && col.Name.Equals("linked_tasks")
									? new object()
									: new int[0];
							}
							else {
								itemsFinal.Rows[items.Rows.IndexOf(row)][col.Name] = new string[0];
							}
						}
					}
				}
			}

            return itemsFinal;
		}

		private string GetQueryForRows(ChartFields chartFields, bool modifyProcessColumns = true) {
			Boolean linkMode = false;
			Boolean archiveMode = false;
			DateTime? archiveDate = null;
			DataRow linkData = null;
			int linkFieldItemColumnId = 0;
			bool linkFieldProcessOrList = false;
			//bool sourceFieldProcessOrList = false;
			int linkFieldMainType = -1;
			//int sourceFieldMainType = -1;
			DataTable tables = null;
			var mainProsessFilters = new List<string>();
			Boolean chartFieldMode = false;

			List<string> offsetIds = new List<string>();
			List<string> offsetAmounts = new List<string>();
			List<string> dateColumns = new List<string>();
			List<int> dateColumnTypes = new List<int>();

			List<string> allowedColumns = chartFields.allowedFieldsByDBName;

			Dictionary<string, List<string>> quickFilterDict = new Dictionary<string, List<string>>();
			Dictionary<string, string> quickFilterKeySql = new Dictionary<string, string>();

			if (chartFields.OriginalData.ContainsKey("filters")) { //Lets add filter fields to allowedColumns		
				JObject quickFilters = new JObject();
				if ((Conv.ToStr(chartFields.OriginalData["chartType"]) == "resourceLoad" || Conv.ToStr(chartFields.OriginalData["chartType"]) == "bubble" || Conv.ToStr(chartFields.OriginalData["chartType"]) == "pie") && chartFields.OriginalData.ContainsKey("quickFilterPossibilities")) {
					JArray jArr = (JArray)chartFields.OriginalData["quickFilterPossibilities"];

					foreach (JObject Obj in jArr) {
						string[] arr = Conv.ToStr(Obj["key"]).Split("_");
						if (Conv.ToInt(arr[arr.Length - 1]) == Conv.ToInt(chartFields.OriginalData["linkId"])) {
							quickFilters = (JObject)Obj["data"];
							break;
						}
					}
				}

				for (int i=0; i< ((JArray)chartFields.OriginalData["filters"]).Count; i++) {
					string filterTxt = Conv.ToStr(((JArray)chartFields.OriginalData["filters"])[i]);
					if (filterTxt != "" && allowedColumns.Contains(filterTxt) == false) {
						allowedColumns.Add(filterTxt);                       
					}
					if (filterTxt != "" && quickFilters.ContainsKey(filterTxt)) {
						List<int> itemList = ((JArray)quickFilters[filterTxt]).ToObject<List<int>>();
						if (itemList.Count > 0 && quickFilterDict.ContainsKey(filterTxt) ==false) {
							quickFilterDict.Add(filterTxt, ((JArray)quickFilters[filterTxt]).ToObject<List<string>>());
						}
					}
				}
            }

			string chartFieldModeIds = "0";
			if (Conv.ToStr(chartFields.OriginalData["filterItemIds"]) != "0" && Conv.ToStr(chartFields.OriginalData["filterItemIds"]).Length > 0) {
				chartFieldMode = true;
				var sl = new List<string>();
				string chartItemsOriginal = Conv.ToStr(chartFields.OriginalData["filterItemIds"]);
				foreach (string id in chartItemsOriginal.Split(",")) {
					sl.Add(Conv.ToStr(Conv.ToInt(id)));
				}
				chartFieldModeIds = string.Join(",", sl.ToArray());
			}
			if (chartFields.OriginalData["archive"] != null) {
				archiveMode = true;
				archiveDate = Conv.ToDateTime(chartFields.OriginalData["archive"]);
				tables = db.GetDatatable("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES");
			}
			//var chartLinkSql =  
			if (Conv.ToStr(chartFields.OriginalData["process"]) !=Conv.ToStr(chartFields.OriginalData["chartProcess"])) {

				if (chartFieldMode == false) {
					if (chartFields.OriginalData.ContainsKey("mainProcessLinkMode")) {
						if(allowedColumns.Contains(Conv.ToStr(chartFields.OriginalData["link_process_field"]))==false) allowedColumns.Add(Conv.ToStr(chartFields.OriginalData["link_process_field"]));
					}
					 mainProsessFilters = GetMainProcessFilterValues(chartFields.Process, chartFields,
						Conv.ToInt(chartFields.OriginalData["main_process_portfolio_id"]), authenticatedSession.item_id,
						Conv.ToStr(chartFields.OriginalData["source_field"]),
						archiveMode, archiveDate, tables);
				}
				chartFields.Process = Conv.ToStr(chartFields.OriginalData["chartProcess"]);
				linkMode = true;
				if (chartFields.GetFieldIfExists("searchText") != null && chartFields.GetFieldIfExists("searchText") != "") {
					chartFields.OriginalData["searchText"] = null;
				}
			}

			var ic = new ItemColumns(instance, chartFields.Process, authenticatedSession);
			if (chartFields.OriginalData.ContainsKey("offsets")) {
                JObject offsetsJObj = (JObject)chartFields.OriginalData["offsets"];

				if (chartFieldModeIds != "0" && chartFieldModeIds != "") {
					foreach (string id in chartFieldModeIds.Split(",")) {
						if (offsetsJObj.ContainsKey(id)) {
							offsetIds.Add(id);
							offsetAmounts.Add(Conv.ToStr(offsetsJObj[id]));
						}
						else {
							offsetIds.Add(id);
							offsetAmounts.Add("0");
						}
					}
				}
				string allowedColumnsWithDatatypesSql = "SELECT name,data_type FROM item_columns WHERE name IN(SELECT * FROM STRING_SPLIT('" + string.Join(",", chartFields.allowedFieldsByDBName.ToArray()) + "',',')) AND data_type IN(3,16) AND process='" + chartFields.Process + "' ";
				DataTable allowedColumnsWithDatatype = db.GetDatatable(allowedColumnsWithDatatypesSql, DBParameters);

                foreach (DataRow row in allowedColumnsWithDatatype.Rows) {
                    if (Conv.ToInt(row["data_type"]) == 3) {
						dateColumnTypes.Add(Conv.ToInt(row["data_type"]));
						dateColumns.Add(Conv.ToStr(row["name"]));
					}
                    else {
						var columnInfo = ic.GetItemColumnByName(Conv.ToStr(row["name"]));
						if (columnInfo.SubDataType == 3) {
							dateColumns.Add(Conv.ToStr(row["name"]));
							dateColumnTypes.Add(16);
						}
					}
				}
            }		
			
			var processTable = GetProcessTableName(chartFields.Process);
			var portfolioSqlQueryService =
				new PortfolioSqlService(instance, authenticatedSession, ic, processTable, chartFields.Process);
			PortfolioSqls pSqls = null;
			if (archiveMode) {
				pSqls = portfolioSqlQueryService.GetSql(0, chartFields.GetFieldIfExists("searchText"), archiveDate);
			}
			else {
				pSqls = portfolioSqlQueryService.GetSql(0, chartFields.GetFieldIfExists("searchText"));
			}

            if (quickFilterDict.Keys.ToArray().Length > 0) {
				for(int i=0;i< quickFilterDict.Keys.ToArray().Length; i++) {
					string key = quickFilterDict.Keys.ToArray()[i];
					string sqlStr = Conv.ToStr(pSqls.sqlcols[pSqls.cols.IndexOf(key)]).ToLower();
					if(sqlStr.EndsWith("as " + key + "_str")) {
						int endLength = ("as " + key + "_str").Length;
						sqlStr = sqlStr.Substring(0, (sqlStr.Length - endLength));
					}
					else if(sqlStr.EndsWith("as " + key)) {
						int endLength = ("as " + key).Length;
						sqlStr = sqlStr.Substring(0, (sqlStr.Length - endLength));
					}
					quickFilterKeySql.Add(key, "(" + sqlStr + ")");					
                }
            }

			if (linkMode || chartFields.OriginalData.ContainsKey("offsets")) {
				if(linkMode && allowedColumns.Contains(Conv.ToStr(chartFields.OriginalData["link_process_field"]))==false) allowedColumns.Add(Conv.ToStr(chartFields.OriginalData["link_process_field"]));
				foreach (var col in pSqls.equationColumns) {
					if(linkMode && col.Name == Conv.ToStr(chartFields.OriginalData["link_process_field"]) && (col.SubDataType==5 || col.SubDataType == 6)) {
						linkFieldProcessOrList = true;
						linkFieldMainType = col.DataType;
						linkFieldItemColumnId = col.ItemColumnId;
					}
                    if (chartFields.OriginalData.ContainsKey("offsets") && allowedColumns.Contains(col.Name) && col.SubDataType==3) {
						dateColumns.Add(col.Name);
						dateColumnTypes.Add(20);
					}
				}
				foreach (var col in pSqls.processColumns) {
					if (linkMode && linkFieldProcessOrList == false && col.Name == Conv.ToStr(chartFields.OriginalData["link_process_field"]) && (col.SubDataType == 5 || col.SubDataType == 6 || col.DataType == 5 || col.DataType == 6)) {
						linkFieldProcessOrList = true;
						linkFieldMainType = col.DataType;
						linkFieldItemColumnId = col.ItemColumnId;
					}
				}
			}

			var conditionWhere = " (1=1) ";
			var c = new EntityRepository(instance, chartFields.Process, authenticatedSession);
			var evaluatedConditions = c.GetWhere(chartFields.ProcessPortfolioId);
			if (evaluatedConditions.Length > 0) conditionWhere = conditionWhere + " AND " + evaluatedConditions;

			foreach (var name in pSqls.cols) {
				if (evaluatedConditions.Contains(name) && allowedColumns.Contains(name)==false) allowedColumns.Add(name);
			}

			var pCols = portfolioSqlQueryService.GetColumns(chartFields.ProcessPortfolioId);

			// This block's purpose is to add possible problematic columns to allowedColumns list. This is ment to handle filter based fields only. -Maybe not needed anymore
			
			if(allowedColumns.Contains("item_id")==false) allowedColumns.Add("item_id");
			pSqls.searchs = portfolioSqlQueryService.GetTextSearchClauses(chartFields.GetFieldIfExists("searchText"), pCols);

			bool searchColumnsHandlingNeeded = false;

			List<string> allowedColumnsOriginalNoEq = new List<string>();
			foreach (string col in allowedColumns) {
				if (allowedColumnsOriginalNoEq.Contains(col) == false) allowedColumnsOriginalNoEq.Add(col);
			}

			foreach (var obj in pSqls.equationColumns) {
				if (allowedColumns.Contains(obj.Name)) {
					searchColumnsHandlingNeeded = true;
					allowedColumnsOriginalNoEq.Remove(obj.Name);
				}
			}

			if (searchColumnsHandlingNeeded && pSqls.searchs.Count > 0 && pSqls.searchs[0] != "" && pSqls.searchs[0] != null) {
				foreach (string str in pSqls.cols) {
					if (pSqls.searchs[0].Contains(str) && allowedColumns.Contains(str) == false) {
						allowedColumns.Add(str);
					};
				}
				foreach (var obj in pSqls.equationColumns) {
					if (pSqls.searchs[0].Contains(obj.Name) && allowedColumns.Contains(obj.Name) == false) {
						allowedColumns.Add(obj.Name);
					};
				}
			}

			var filters = "";
			var dashBoardFilters = new Dictionary<int, object>();
			if (linkMode == false) {
				Dictionary<string, object> dashBoardFiltersOriginal =
					(Dictionary<string, object>)chartFields.OriginalData["dashboardFilters"];
				foreach (var col in dashBoardFiltersOriginal) {
					dashBoardFilters.Add(Conv.ToInt(col.Key), col.Value);
				}

				var itemFilterParser = new ItemFilterParser(dashBoardFilters,
					new ItemColumns(instance, chartFields.Process, authenticatedSession), instance, chartFields.Process,
					authenticatedSession);
				filters = itemFilterParser.Get();
			}

			if (linkMode == false) {
				SetDBParam(SqlDbType.Int, "@process_portfolio_id", chartFields.ProcessPortfolioId);
				var filtersColumnQuary =
					"SELECT item_columns.name AS column_name,item_columns.data_type FROM process_portfolio_columns LEFT JOIN item_columns ON process_portfolio_columns.item_column_id = item_columns.item_column_id where process_portfolio_id=@process_portfolio_id AND process_portfolio_columns.use_in_filter=1";
				//  var filtersColumnQuary = "SELECT item_columns.name AS column_name,item_columns.data_type FROM process_portfolio_columns LEFT JOIN item_columns ON process_portfolio_columns.item_column_id = item_columns.item_column_id where process_portfolio_id=@process_portfolio_id AND process_portfolio_columns.use_in_filter=1 AND item_columns.data_type IN(16,20)";
				DataTable filterColumnResult = db.GetDatatable(filtersColumnQuary, DBParameters);


				foreach (DataRow row in filterColumnResult.Rows) {
					if (Conv.ToInt(row["data_type"]) == 16) {
						if (pSqls.cols.Contains(Conv.ToStr(row["column_name"]))) {
							string jSONText = pSqls.sqlcols[pSqls.cols.IndexOf(Conv.ToStr(row["column_name"]))];
							string[] wordArr = jSONText.Replace("(", " ").Replace(")", " ").Split(" ");
							foreach (string word in wordArr) {
								if (Conv.ToStr(word).Contains("pf.")) {
									if (allowedColumns.Contains(word.Replace("pf.", "")) == false && filters.Contains(word.Replace("pf.", ""))) allowedColumns.Add(word.Replace("pf.", ""));
								}
							}
						}
					}
					else {
						if (allowedColumns.Contains(Conv.ToStr(row["column_name"])) == false && pSqls.cols.Contains(Conv.ToStr(row["column_name"])) && filters.Contains(Conv.ToStr(row["column_name"]))) {
							allowedColumns.Add(Conv.ToStr(row["column_name"]));
						}
					}
				}
			}

			pSqls = removeUnwantedColumns(pSqls, allowedColumns);

			foreach (string col in new List<string>(allowedColumnsOriginalNoEq)) {
				if (pSqls.cols.Contains(col) == false) allowedColumnsOriginalNoEq.Remove(col);
			}

			for (int i = 0; i < allowedColumnsOriginalNoEq.Count; i++) {
				allowedColumnsOriginalNoEq[i] = "pf." + allowedColumnsOriginalNoEq[i];
			}

			pSqlCopy = pSqls;
			
			if (modifyProcessColumns) {
				for (var i = 0; i < pSqls.sqlcols.Count; i++) {
					var colNAme = pSqls.cols[i];
					pSqls.sqlcols[i] = pSqls.sqlcols[i].Replace(colNAme + "_str", colNAme);
				}
			}

			var headers = string.Join(", ", pSqls.sqlcols);

			var eSql1 = "";
			var eqDateSqlParts = new Dictionary<string,string>();
			var esql2 = "";
			string linkFieldSql = "";
			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
						string eqSql = ic.GetEquationQuery(col);
						equationSql += "," + eqSql + " " + col.Name;

						if (col.SubDataType == 3) eqDateSqlParts.Add(col.Name, eqSql);
						if (chartFields.OriginalData != null && linkMode && col.Name == Conv.ToStr(chartFields.OriginalData["link_process_field"])) linkFieldSql = eqSql;
				}
				
				//eSql1 = " SELECT  * " + equationSql + " FROM ( ";
				eSql1 = " SELECT  " + String.Join(",", allowedColumnsOriginalNoEq.ToArray()) + " " + equationSql + " FROM ( ";
				esql2 = " ) pf ";
			}

			var tempTablePart1 = "";
			var temptablePart2 = "";
			if ((Conv.ToStr(chartFields.OriginalData["chartType"]) == "pie" ||
				 Conv.ToStr(chartFields.OriginalData["chartType"]) == "bar") && chartFields.Grouping) {
				//eSql1 = "";
				//esql2 = "";

				tempTablePart1 += " SELECT * INTO #tempT FROM( ";

				temptablePart2 = ") AS tableT";
			}

			if(linkFieldProcessOrList && linkFieldMainType == 16) {
				linkFieldSql = pSqls.sqlcols[pSqls.cols.IndexOf(Conv.ToStr(chartFields.OriginalData["link_process_field"]))];
				linkFieldSql = linkFieldSql.Remove(linkFieldSql.Length - Conv.ToStr(chartFields.OriginalData["link_process_field"]).Length);
			}
			if(linkFieldProcessOrList && linkFieldMainType < 7) {
				linkFieldSql = pSqls.sqlcols[pSqls.cols.IndexOf(Conv.ToStr(chartFields.OriginalData["link_process_field"]))].ToLower().Replace("as " + Conv.ToStr(chartFields.OriginalData["link_process_field"]).ToLower(),"").TrimEnd("_str");
			}
			string offsetFilter = "";
			if (linkMode) {
				offsetFilter = "pf." + Conv.ToStr(chartFields.OriginalData["link_process_field"]);
				if (chartFieldMode) {
					if (linkFieldProcessOrList && linkFieldMainType < 7) {
						filters = " AND (select ',' + (SELECT String_agg(item_id, ',') FROM item_data_process_selections WHERE item_column_id = " + linkFieldItemColumnId + " AND selected_item_id IN(" + chartFieldModeIds + ")) + ',') LIKE '%,' + CAST(item_id AS NVARCHAR) + ',%'";
						offsetFilter = "(SELECT selected_item_id FROM item_data_process_selections WHERE item_column_id = " + linkFieldItemColumnId + " AND selected_item_id IN(" + chartFieldModeIds + ") AND item_data_process_selections.item_id = pf.item_id) ";
					}
					else {
						filters = " AND [" + chartFields.OriginalData["link_process_field"] + "] IN( " + chartFieldModeIds + " )"; //This is not working
					}
				}
				else {
					if (linkFieldProcessOrList) { 
						if (mainProsessFilters.Count > 0) {
							filters = " AND (";
							foreach (string id in mainProsessFilters) {
								if (mainProsessFilters.IndexOf(id) != 0) {
									filters += ") OR (";
								}
								else {
									filters += "(";

								}
								filters += id + " IN (select * FROM STRING_SPLIT(CAST((" + linkFieldSql + ") AS NVARCHAR(MAX)),','))";
							}
							filters += ")) ";
						}
						else {
							filters = " AND 1=2 ";
						}
					}
					else {
						if (mainProsessFilters.Count > 0) {
							filters = " AND [" + chartFields.OriginalData["link_process_field"] + "] IN( " +
									  string.Join(",", mainProsessFilters.ToArray()) + " )";
						}
						else {
							filters = " AND 1=2 ";
						}
					}
				}
				//conditionWhere = "1=1";
			}
			else if (chartFieldMode) {
				//conditionWhere = "1=1";
				filters = " AND item_id IN(" + chartFieldModeIds + ") ";
			}
			string sql = "";
			if (chartFields.OriginalData["phase"] != null && linkMode == false) { // This handless portfolio phase arrow filters
				List<int> selectedPhases = Conv.ToIntArray(chartFields.OriginalData["phase"]).OfType<int>().ToList();
				ItemBase ib = new ItemBase(instance, chartFields.Process, authenticatedSession);
				string phaseWhere = ib.getPhaseFilterWhere(selectedPhases);
				if (filters == "") {
					filters = " AND " + phaseWhere;
				}
				else {
					filters = filters + " AND " + phaseWhere;
				}
			}

			string offsetSqlPart = "";
            if (offsetIds.Count > 0) {
				offsetSqlPart += " SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS row_id,a.value AS offset INTO #tempA FROM STRING_SPLIT('" + String.Join(",", offsetAmounts.ToArray()) + "',',') a ";
				offsetSqlPart += " SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS row_id,b.value AS id INTO #tempB FROM STRING_SPLIT('" + String.Join(",", offsetIds.ToArray()) + "',',') b ";
				offsetSqlPart += " SELECT id,offset INTO #offsets FROM #tempA LEFT JOIN #tempB ON #tempA.row_id=#tempB.row_id ";
			}

			//_str fix - removed _str from column names
			foreach (string name in pSqls.cols) {
				if (offsetSqlPart.Contains(name) && offsetSqlPart.Contains(name + "_str")) offsetSqlPart = offsetSqlPart.Replace(name + "_str", name);//.Replace(name, name + "_str");
				if (tempTablePart1.Contains(name) && tempTablePart1.Contains(name + "_str")) tempTablePart1 = tempTablePart1.Replace(name + "_str", name);//.Replace(name, name + "_str");
				if (eSql1.Contains(name) && eSql1.Contains(name + "_str")) eSql1 = eSql1.Replace(name + "_str", name);//.Replace(name, name + "_str");
				if (headers.Contains(name) && headers.Contains(name + "_str")) headers = headers.Replace(name + "_str", name);//.Replace(name, name + "_str");
				if (esql2.Contains(name) && esql2.Contains(name + "_str")) esql2 = esql2.Replace(name + "_str", name);//.Replace(name, name + "_str");
			}
			//offset replace -- Should not affect to fields inside eq.
			if (dateColumns.Count > 0) {
				if (chartFields.OriginalData.ContainsKey("offset_interval")) {
					string intervalStr = Conv.ToStr(chartFields.OriginalData["offset_interval"]);
					string[] options = { "day", "week", "month", "quarter", "year" };
					string interval = "";
					if (options.Contains<string>(intervalStr)) interval = intervalStr;

					if (interval != "") {
						foreach (string name in dateColumns) {
							int dateColumnType = dateColumnTypes[dateColumns.IndexOf(name)];
							if (linkMode) {
								if (dateColumnType == 3){
									if (eSql1 == "") {
										if (headers.Contains("pf." + name)) {
											headers = headers.Replace("pf." + name, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=" + offsetFilter + "),pf." + name + ") AS " + name + " ");
										}
									}
									else {
										if (eSql1.Contains("pf." + name)) {
											eSql1 = eSql1.Replace("pf." + name, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=" + offsetFilter + "),pf." + name + ") AS " + name + " ");
										}
									}
								}
								if (dateColumnType == 16) {
									string originalText = pSqls.sqlcols[pSqls.cols.IndexOf(name)];
									string replaceText = ReplaceLast(name, "", pSqls.sqlcols[pSqls.cols.IndexOf(name)]);
									if (headers.Contains(originalText)) {
										if (eSql1 == "") {
											headers = headers.Replace(originalText, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=" + offsetFilter + ")," + replaceText + ") AS " + name + " ");
										}
										else {
											if (eSql1.Contains("pf." + name)){
												eSql1 = eSql1.Replace("pf." + name, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=" + offsetFilter + "),pf." + name + ") AS " + name + " ");
											}
											else {
												eSql1 = eSql1.Replace(originalText, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=" + offsetFilter + ")," + replaceText + ") AS " + name + " ");
											}
										}

									}
								}
								if (dateColumnType == 20 && eSql1.Contains(eqDateSqlParts[name])) eSql1 = eSql1.Replace(eqDateSqlParts[name], "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=" + offsetFilter + ")," + eqDateSqlParts[name] + ") ");
							}
							else {
								if (dateColumnType == 3) {
									if (eSql1 == "") {
										if (headers.Contains("pf." + name)) {
											headers = headers.Replace("pf." + name, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=pf.item_id),pf." + name + ") AS " + name + " ");
										}
									}
                                    else {
										if (eSql1.Contains("pf." + name)) {
											eSql1 = eSql1.Replace("pf." + name, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=pf.item_id),pf." + name + ") AS " + name + " ");
										}
									}
								}
								if (dateColumnType == 16) {
									string originalText = pSqls.sqlcols[pSqls.cols.IndexOf(name)];
									string replaceText = ReplaceLast(name, "", pSqls.sqlcols[pSqls.cols.IndexOf(name)]);
									if (headers.Contains(originalText)) {
										if (eSql1 == "") {
											headers = headers.Replace(originalText, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=pf.item_id)," + replaceText + ") AS " + name + " ");
										}
                                        else {
											if (eSql1.Contains("pf." + name)) {
												eSql1 = eSql1.Replace(originalText, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=pf.item_id)pf." + name + ") AS " + name + " ");
											}
                                            else {
												eSql1 = eSql1.Replace(originalText, "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=pf.item_id)," + replaceText + ") AS " + name + " ");
											}
										}
									}
								}
								if (dateColumnType == 20 && eSql1.Contains(eqDateSqlParts[name])) eSql1 = eSql1.Replace(eqDateSqlParts[name], "DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=pf.item_id)," + eqDateSqlParts[name] + ") ");
							}
						}
					}
				}
			}

			if (offsetFilter=="" && Conv.ToStr(chartFields.OriginalData["chartType"]) == "resourceLoad") {
				offsetFilter = "pf.item_id AS original_item_id";
			}
			else if(Conv.ToStr(chartFields.OriginalData["chartType"]) == "resourceLoad" && linkMode) {
				offsetFilter += " AS original_item_id ";
			}

			string offsetFilterInSql = "";
			if (headers.Contains(offsetFilter) == false && (chartFields.OriginalData.ContainsKey("offsets") || Conv.ToStr(chartFields.OriginalData["chartType"]) == "resourceLoad")) {
				offsetFilterInSql = offsetFilter;
			}

			if (offsetFilterInSql != "") offsetFilterInSql += ",";

			if (quickFilterDict.Keys.ToArray().Length > 0) {
				for (int i = 0; i < quickFilterDict.Keys.ToArray().Length; i++) {
					filters += " AND (";
					foreach (string str in quickFilterDict[quickFilterDict.Keys.ToArray()[i]]) {
						string orPart = "";
						if (quickFilterDict[quickFilterDict.Keys.ToArray()[i]].IndexOf(str) + 1 != quickFilterDict[quickFilterDict.Keys.ToArray()[i]].Count) {
							orPart = " OR ";
						}
						filters += " (',' + " + Conv.ToStr(quickFilterKeySql[quickFilterDict.Keys.ToArray()[i]]) + " + ',' LIKE '%," + str + ",%')" + orPart;
					}
					filters += ") ";
				}
			}

			sql = offsetSqlPart + tempTablePart1 + eSql1 + " SELECT " + offsetFilterInSql + headers + " FROM " + processTable + " pf " + esql2 +
						"  WHERE " + conditionWhere + " " +
						filters +
						string.Join("  ", pSqls.searchs) + temptablePart2;
			if (archiveMode) {
				sql = setArchiveSql(sql, tables, archiveDate);
			}
			//}

            return sql;
		}

		private string GetRowQueryForProperty(string property, string subPropertyColumn) {
			var qr =
				"( SELECT ic2.item_column_id sub_column_id, ic2.name sub_name, ic2.process sub_process, ic.* from item_columns ic LEFT JOIN item_columns ic2 ON ic.data_additional = ic2.process WHERE ic.name='{0}' {1} and ic.instance=@instance and ic.process=@process )";
			var subQ = "and ic2.name='{0}'";
			if (string.IsNullOrEmpty(subPropertyColumn)) {
				return string.Format(qr, property, "");
			} else {
				return string.Format(qr, property, string.Format(subQ, subPropertyColumn));
			}
		}

		private string GetLinkedValueFieldTableName(ChartFields chartFields) {
			var linkedValueField =
				db.GetDataRow(
					"SELECT * FROM item_columns WHERE data_additional IS NOT NULL AND instance=@instance AND process=@process AND name='" +
					chartFields.ValueField + "'", DBParameters);
			if (linkedValueField == null || Conv.ToInt(linkedValueField["data_type"]) != 6) {
				return null;
			}

			return Conv.ToStr(linkedValueField["data_additional"]);
		}

		private Dictionary<int, object> GetItemsForIds(string process, string propertyName, List<int> ids = null) {
			var labels = new Dictionary<int, object>();
			var items = GetItemListForFieldName(process, propertyName, ids);
			items.ForEach(x => labels.Add(Conv.ToInt(x["item_id"]), x));
			return labels;
		}

		private List<Dictionary<string, object>> GetItemListForFieldName(string process, string propertyName,
			List<int> ids) {
			var source = GetProcessFieldType(process, propertyName);
			var fetchFromProcess = process;
			if (source.ContainsKey("sub_process") && !string.IsNullOrEmpty(Conv.ToStr(source["sub_process"]))) {
				fetchFromProcess = Conv.ToStr(source["sub_process"]);
			}

			var ib = new ItemBase(authenticatedSession.instance, fetchFromProcess, authenticatedSession);

			if (ids != null) {
				return db.DataTableToDictionary(ib.GetAllData(string.Join(",", ids)));
			}

			return new List<Dictionary<string, object>>();
		}

		private Dictionary<string, object> GetProcessFieldType(string process, string propertyName) {
			SetDBParameter("@process", process);


			var itemColumnQuery =
				"select distinct '' sub_process, ic.name name, ic.data_type from item_columns ic where ic.process=@process and ic.instance=@instance and ic.name=@fieldName";

			if (!string.IsNullOrEmpty(propertyName)) {
				SetDBParameter("@fieldName", propertyName);

				var checkColumnType =
					"SELECT ic.item_column_id, ic.data_type, ic.data_additional FROM item_columns ic WHERE ic.process=@process and ic.instance=@instance and ic.name=@fieldName";
				var ct = db.GetDataRow(checkColumnType, DBParameters);

				if (ct == null)
					throw new CustomArgumentException("Item Column cannot be found with name " + propertyName);

				if (Conv.ToInt(ct["data_type"]) == 16 && Conv.ToStr(ct["data_additional"]) != "") {
					var st = RegisteredSubQueries.GetType(Conv.ToStr(ct["data_additional"]),
						Conv.ToInt(ct["item_column_id"]));
					if (st != null) {
						var result = new Dictionary<string, object>();
						result.Add("sub_process", st.GetSubProcess());
						result.Add("name", propertyName);
						result.Add("data_type", st.GetConfig().DataType);
						return result;
					}
				} else if (Conv.ToInt(ct["data_type"]) == 20 && Conv.ToStr(ct["data_additional"]) != "") {
					var sql = "SELECT * FROM item_columns WHERE item_column_id=" + ct["item_column_id"];
					var column = db.GetDataRow(sql);

					var result = new Dictionary<string, object>();
					result.Add("sub_process", column["input_name"]);
					result.Add("name", propertyName);
					result.Add("data_type", column["data_additional2"]);
					return result;
					//ct["item_column_id"]
				}


				itemColumnQuery = "select distinct ic2.process sub_process, ic.name name, ic.data_type " +
				                  "from item_columns ic " +
				                  "left join item_columns ic2 on ic2.process = ic.data_additional and (ic.data_type=5 or ic.data_type=6) " +
				                  "where ic.process=@process and ic.instance=@instance and ic.name=@fieldName";
			}

			return Conv.DataRowToDictionary(db.GetDataRow(itemColumnQuery, DBParameters));
		}

		/// <summary>
		/// Switch function to return proper data for rest service
		/// </summary>
		/// <param name="chartFields">ChartFields object</param>
		/// <param name="tableToQuery">Table to query (if null, then fetch current)</param>
		/// <returns></returns>
		public Dictionary<string, object> GetCurrentData(ChartFields chartFields, string table = null) {
			var tableToQuery = tableName;
			if (!string.IsNullOrEmpty(table)) {
				tableToQuery = table;
			}

			var returnObject = new Dictionary<string, object>();
			List<Dictionary<string, object>> dTable;
			Dictionary<int, object> labels;
			try {
				if (chartFields.Grouping && (Conv.ToStr(chartFields.OriginalData["chartType"]) == "pie" ||
				                             Conv.ToStr(chartFields.OriginalData["chartType"]) == "bar")) {
					if (string.IsNullOrEmpty(chartFields.GroupByField) &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "phase" &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "mixed" &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "multiList" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "resourceLoad" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "pivot" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "circular") {
						throw new CustomArgumentException("MISSING_COLUMN_SELECTIONS");
					}				

					var queryForAllRows = GetQueryForRows(chartFields);

					string function;
					string valCol;
					if (chartFields.GroupingFunction == "sum") {
						function = "SUM";
						valCol = chartFields.ValueField;
						if (string.IsNullOrEmpty(chartFields.ValueField) &&
						    Conv.ToStr(chartFields.OriginalData["chartType"]) != "phase" &&
						    Conv.ToStr(chartFields.OriginalData["chartType"]) != "mixed" &&
						    Conv.ToStr(chartFields.OriginalData["chartType"]) != "multiList" &&
							Conv.ToStr(chartFields.OriginalData["chartType"]) != "resourceLoad" &&
							Conv.ToStr(chartFields.OriginalData["chartType"]) != "pivot" && 
							Conv.ToStr(chartFields.OriginalData["chartType"]) != "circular") {
							throw new CustomArgumentException("MISSING_COLUMN_SELECTIONS");
						}
					} else {
						function = "COUNT";
						valCol = chartFields.GroupByField;
					}

					var countPart = "";
					var groupedQuery = "";

					if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "pie") {
						if (chartFields.GroupingFunction == "sum") {
							countPart = "CASE WHEN " + valCol + " IS NULL THEN 0 ELSE (CASE WHEN " + valCol +
							" like '%,%' THEN 0 ELSE " + valCol + " END) END";
						}
                        else {
							countPart = "CASE WHEN " + valCol + " IS NULL THEN 1 ELSE (CASE WHEN " + valCol +
							" like '%,%' THEN 1 ELSE " + valCol + " END) END";
						}

						string idsField = "item_id";

						if (chartFields.OriginalData.ContainsKey("mainProcessLinkMode")) idsField = Conv.ToStr(chartFields.OriginalData["link_process_field"]);

						groupedQuery = queryForAllRows +
						               " SELECT " + chartFields.GroupByField + " as title, " +
						               chartFields.GroupByField + " as selected_item_id, " +
						               function + "(" + countPart + ") as val , " +
						               "Stuff ((SELECT ',' + Cast(w." + idsField + " AS NVARCHAR) FROM #tempT w where q." +
						               chartFields.GroupByField + "=w." + chartFields.GroupByField + " or (q." +
						               chartFields.GroupByField + " is null and w." + chartFields.GroupByField +
						               " is null) FOR xml path('')), 1, 1, '') AS ids " +
						               "FROM #tempT q GROUP BY " + chartFields.GroupByField;
					} else {
						groupedQuery = queryForAllRows + " SELECT " + chartFields.GroupByField + " as title, " +
						               chartFields.GroupByField + " as selected_item_id, " +
						               function + "(" + valCol + ") as val " +
						               "FROM #tempT q GROUP BY " + chartFields.GroupByField;
					}

					dTable = db.GetDatatableDictionary(groupedQuery);

					var idList = new List<int>();
					foreach (var rowItem in dTable) {
						var ids = Conv.ToStr(rowItem["title"]);
						ids.Split(",").ToList().ForEach(x => idList.Add(Conv.ToInt(x)));
					}

					labels = GetItemsForIds(chartFields.Process, chartFields.GroupByField, idList);
				} else {
					if (string.IsNullOrEmpty(chartFields.LabelField) &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "phase" &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "mixed" &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "multiList" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "resourceLoad" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "pivot" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "circular") {
						throw new CustomArgumentException("MISSING_COLUMN_SELECTIONS");
					}

					dTable = db.DataTableToDictionary(GetAllRows(chartFields, tableToQuery));

					labels = new Dictionary<int, object>();

					if (Conv.ToStr(chartFields.OriginalData["chartType"]) != "phase" &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "mixed" &&
					    Conv.ToStr(chartFields.OriginalData["chartType"]) != "multiList" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "resourceLoad" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "pivot" &&
						Conv.ToStr(chartFields.OriginalData["chartType"]) != "circular") {
						if (!string.IsNullOrEmpty(chartFields.LabelField)) {
							foreach (var row in dTable) {
								var nameByKey = new Dictionary<string, string>();
								nameByKey.Add(chartFields.LabelField, Conv.ToStr(row[chartFields.LabelField]));
								labels.Add(Conv.ToInt(row["item_id"]), nameByKey);
							}
						}
					}
				}
			} catch (SqlException) {
				throw new CustomArgumentException("SQL_EXCEPTION");
				//return returnObject;
			} catch (NullReferenceException) {
				throw new CustomArgumentException("NULL_POINTER");
			}

			if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "phase") {
				var hiddenList = new List<string>();

				var portfolioRowQuary =
					"SELECT hidden_progress_arrows FROM process_portfolios WHERE process_portfolio_id=" +
					chartFields.ProcessPortfolioId;
				var portfolioRow = db.GetDataRow(portfolioRowQuary);
				var hidden_progress_arrows_value = Conv.ToStr(portfolioRow[0]);

				var mStates = new States(instance, authenticatedSession);
				for (var i = 0; i < dTable.Count; i++) {
					var stateData = mStates.GetEvaluatedStates(process, "progressProgress",
						Conv.ToInt(dTable[i]["item_id"]),
						new List<string>(new[] {"progressProgress.grey", "progressProgress.yellow", "progressProgress.green", "progressProgress.red", "progressProgress.blue"}));
					dTable[i].Add("phaseData", stateData);
				}

				returnObject.Add("phasesExcluded", hidden_progress_arrows_value);
			}

			returnObject.Add("data", dTable);

			if (Conv.ToStr(chartFields.OriginalData["chartType"]) != "phase") {
				returnObject.Add("relatedItems", labels);
			}

			var fieldTypes = new Dictionary<string, object>();
			if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "phase") {
				fieldTypes.Add("nameFieldColumn", GetItemColumnForProcess(chartFields.NameField));
				fieldTypes.Add("valueFieldColumn", GetItemColumnForProcess(chartFields.ValueField));
				fieldTypes.Add("value2FieldColumn", GetItemColumnForProcess(chartFields.Value2Field));
			}
			else if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "mixed") {
				var xAxisFiledName = "";
				var itemListName = "";

				var mixedDataCommonJson = JObject.Parse(chartFields.OriginalData["mixedDataCommon"].ToString());
				if (mixedDataCommonJson["xaxis"].Last != null) {
					xAxisFiledName = mixedDataCommonJson["xaxis"].Last
						.ToString().Replace("{", "").Replace("}", "");
				}

				if (mixedDataCommonJson.ContainsKey("useListMode") && Conv.ToInt(mixedDataCommonJson["useListMode"].First) == 1 && Conv.ToStr(JObject.Parse(chartFields.OriginalData["mixedListData"].ToString())["selectedList"].Last) != "" && Conv.ToInt(mixedDataCommonJson["useListMode"].First) == 1) {
					itemListName = JObject.Parse(chartFields.OriginalData["mixedListData"].ToString())["selectedList"].Last
						.ToString().Replace("{", "").Replace("}", "");
				}

				if (xAxisFiledName != "noSelection" && xAxisFiledName != "") {
					fieldTypes.Add("xAxisFieldType", GetProcessFieldType(chartFields.Process, xAxisFiledName));

					var xAxisColumnInfo = GetItemColumnForProcess(xAxisFiledName);

					string listName = "";
					if (xAxisColumnInfo.DataType == 16) {
						listName = xAxisColumnInfo.SubDataAdditional;
					}
					else if (xAxisColumnInfo.DataType == 20) {
						listName = xAxisColumnInfo.InputName;
					}
					else {
						listName = xAxisColumnInfo.DataAdditional;
					}

					fieldTypes.Add("xAxisColumnInfo", xAxisColumnInfo);

					if (xAxisColumnInfo.DataType == 6 || xAxisColumnInfo.SubDataType == 6) {
						var items = GetItemListForFieldName(chartFields.Process, xAxisFiledName, null);
						var listSql = "SELECT * FROM dbo._" + instance + "_" + listName;

						fieldTypes.Add("xAxisItems", db.GetDatatableDictionary(listSql));

						var listProcessFieldSql = "SELECT * FROM dbo.processes WHERE process='" +
												  listName + "'";
						DataRow listProcessFields = db.GetDataRow(listProcessFieldSql);

						var listFieldToJS = listProcessFields.Table.Columns.Cast<DataColumn>()
							.ToDictionary(c => c.ColumnName, c => listProcessFields[c]);
						returnObject.Add("listProcessFields", listFieldToJS);
					}
				}
				if (itemListName != "") {
					var itemListColumnInfo = GetItemColumnForProcess(itemListName);

					string listName = "";
					if (itemListColumnInfo.DataType == 16) {
						listName = itemListColumnInfo.SubDataAdditional;
					}
					else if (itemListColumnInfo.DataType == 20) {
						listName = itemListColumnInfo.InputName;
					}
					else {
						listName = itemListColumnInfo.DataAdditional;
					}
					fieldTypes.Add("itemListColumnInfo", itemListColumnInfo);

					if (itemListColumnInfo.DataType == 6 || itemListColumnInfo.SubDataType == 6) {
						var items = GetItemListForFieldName(chartFields.Process, itemListName, null);
						var listSql = "SELECT * FROM dbo._" + instance + "_" + listName;

						fieldTypes.Add("itemListItems", db.GetDatatableDictionary(listSql));

						var listProcessFieldSql = "SELECT * FROM dbo.processes WHERE process='" +
												  listName + "'";
						DataRow listProcessFields = db.GetDataRow(listProcessFieldSql);

						var listFieldToJS = listProcessFields.Table.Columns.Cast<DataColumn>()
							.ToDictionary(c => c.ColumnName, c => listProcessFields[c]);
						returnObject.Add("itemListProcessFields", listFieldToJS);
					}
				}
				//x-axis date filter handling

				if(JObject.Parse(chartFields.OriginalData["mixedDataCommon"].ToString())["xaxis"] != null && Conv.ToStr(JObject.Parse(chartFields.OriginalData["mixedDataCommon"].ToString())["xaxis"].First) != "") {
					ItemColumn item = GetItemColumnForProcess(Conv.ToStr(JObject.Parse(chartFields.OriginalData["mixedDataCommon"].ToString())["xaxis"].First));
					if(item != null && (item.DataType == 3 || item.SubDataType == 3)) {
						int itemColumnId = item.ItemColumnId;

						string portfolioFilters = "SELECT 0 AS user_id,value FROM instance_user_configurations_saved where identifier='" + chartFields.ProcessPortfolioId + "' AND [public]=3 AND [group]='portfolios' UNION ALL SELECT user_id,value FROM instance_user_configurations where identifier='" + chartFields.ProcessPortfolioId + "' AND [group]='portfolios' AND user_id=" + authenticatedSession.item_id;

						DataTable portfolioFilterData = db.GetDatatable(portfolioFilters);

						if (portfolioFilterData.Rows.Count > 0) {
							string filterStr = "";
							if (Conv.ToStr(chartFields.OriginalData["process"]) != Conv.ToStr(chartFields.OriginalData["chartProcess"])) {
								DataRow[] defaultFilters = portfolioFilterData.Select("user_id=0");
								if (defaultFilters.Length > 0) filterStr = Conv.ToStr(defaultFilters[0]["value"]);
							}
							else {
								DataRow[] userFilters = portfolioFilterData.Select("user_id>0");
								if (userFilters.Length > 0) {
									filterStr = Conv.ToStr(userFilters[0]["value"]);
								}
                                else {
									DataRow[] defaultFilters = portfolioFilterData.Select("user_id=0");
									if (defaultFilters.Length > 0) filterStr = Conv.ToStr(defaultFilters[0]["value"]);
								}
							}

							if(filterStr!="" && JObject.Parse(filterStr)["filters"] != null && JObject.Parse(filterStr)["filters"]["dateRange"] != null && JObject.Parse(filterStr)["filters"]["dateRange"][item.ItemColumnId.ToString()] != null) {
								DateTime? start = null;
								DateTime? end = null;
								if (Conv.ToStr(JObject.Parse(filterStr)["filters"]["dateRange"][item.ItemColumnId.ToString()]["start"]) != "") start = (DateTime)JObject.Parse(filterStr)["filters"]["dateRange"][item.ItemColumnId.ToString()]["start"];
								if (Conv.ToStr(JObject.Parse(filterStr)["filters"]["dateRange"][item.ItemColumnId.ToString()]["end"]) != "") end = (DateTime)JObject.Parse(filterStr)["filters"]["dateRange"][item.ItemColumnId.ToString()]["end"];

								Dictionary<string, object> xFilter = new Dictionary<string, object>();
								xFilter.Add("start", start);
								xFilter.Add("end", end);
								returnObject.Add("xTimeFilters", xFilter);
							}
						}
					}
				}


			}
			else if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "multiList") {
				var multiListData = chartFields.OriginalData["multiListData"];
				var multiListDataCommon = chartFields.OriginalData["multiListDataCommon"];

				var columnNames = new List<string>();
				if (Conv.ToStr(JObject.Parse(chartFields.OriginalData["multiListDataCommon"].ToString())["legendList"]
						.Last) != "") {
					columnNames.Add(Conv.ToStr(
						JObject.Parse(Conv.ToStr(chartFields.OriginalData["multiListDataCommon"]))["legendList"].Last));
				}

				var index = 0;
				var multiListDataCommonJson = JObject.Parse(chartFields.OriginalData["multiListData"].ToString());
				while (true) {
					var tempText = multiListDataCommonJson[index.ToString()];
					if (tempText != null && tempText["sourceList"] != null &&
						Conv.ToStr(tempText["sourceList"].Last) != "") {
						if (columnNames.Contains(Conv.ToStr(tempText["sourceList"].Last)) == false) {
							columnNames.Add(Conv.ToStr(tempText["sourceList"].Last));
						}
					}
					else if (tempText == null) {
						break;
					}

					index += 1;
				}


				var listColumnSql = "";

				if (columnNames.Count > 0) {
					listColumnSql = "SELECT input_name,data_additional,data_type,data_additional2,name FROM dbo.item_columns WHERE name IN('" +
									string.Join("','", columnNames.ToArray()) + "' ) AND process='" + chartFields.Process + "'";
					var listColumns = db.GetDatatableDictionary(listColumnSql);
					returnObject.Add("listColumns", listColumns);

					var listItemDataSql = "";

					var tempListData = new List<DataTable>();
					var listColumnNames = new List<string>();
					var indexTest = 0;
					for (var i = 0; i < listColumns.Count; i++) {
						string listNameField = "";
						if (Conv.ToInt(listColumns[i]["data_type"]) == 20) {
							listNameField = Conv.ToStr(listColumns[i]["input_name"]);
						}
						else if (Conv.ToInt(listColumns[i]["data_type"]) == 16) {
							JObject obj = JObject.Parse(Conv.ToStr(listColumns[i]["data_additional2"]));
							if (obj.ContainsKey("@SourceColumnId")) {
								var listColumnId = Conv.ToInt(obj["@SourceColumnId"]);
								if (listColumnId > 0) {
									string sql = "SELECT data_additional FROM dbo.item_columns WHERE item_column_id=" + listColumnId;
									var listRow = db.GetDataRow(sql);
									listNameField = Conv.ToStr(listRow["data_additional"]);
								}
								else {
									listNameField = "";
								}
							}
							else if (Conv.ToStr(listColumns[i]["name"]) != "") {
								var itemColumn = GetItemColumnForProcess(Conv.ToStr(listColumns[i]["name"]));
								listNameField = itemColumn.SubDataAdditional;
							}

						}
						else {
							listNameField = Conv.ToStr(listColumns[i]["data_additional"]);
						}

						listItemDataSql = "SELECT '" + listColumns[i]["name"] +
										  "' AS field,processes.list_order AS list_order,tb.* FROM dbo._" + instance +
										  "_" + listNameField +
										  " tb LEFT JOIN dbo.processes ON processes.process='" +
										  listNameField + "'";
						if (i < listColumns.Count) {
							//listItemDataSql += " UNION ALL ";

							var tempTable = db.GetDatatable(listItemDataSql);
							for (int j = 0; j < tempTable.Columns.Count; j++) {
								if (listColumnNames.Contains(tempTable.Columns[j].ColumnName.ToString()) == false)
									listColumnNames.Add(tempTable.Columns[j].ColumnName.ToString());
							}

							//tempTable.Columns.
							tempListData.Add(tempTable);
							indexTest += 1;
						}
					}

					DataTable listItemsTb = new DataTable();
					for (int j = 0; j < listColumnNames.Count; j++) {
						listItemsTb.Columns.Add(listColumnNames[j], typeof(string));
					}

					for (var i = 0; i < tempListData.Count; i++) {
						var itemTemp = new List<string>();
						foreach (DataRow row in tempListData[i].Rows) {
							var newRow = listItemsTb.NewRow();
							for (var j = 0; j < listColumnNames.Count; j++) {
								if (row.Table.Columns.Contains(listColumnNames[j])) {
									newRow[listColumnNames[j]] = Conv.ToStr(row[listColumnNames[j]]);
								}
								else {
									newRow[listColumnNames[j]] = "";
								}
							}
							//listItemsTb.Rows.Add(string.Join(",",itemTemp.ToArray()));                 


							//returnObject.Add("listItemsPerField", db.GetDatatableDictionary(listItemDataSql));
							//foreach (DataRow row in tempListData)
							//{

							//}
							listItemsTb.Rows.Add(newRow);
						}
					}

					returnObject.Add("listItemsPerField", Conv.DataTableToDictionary(listItemsTb));
				}
			}
			else if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "pivot") {
				JObject pivotCommon = JObject.Parse(Conv.ToStr(chartFields.OriginalData["pivotCommon"]));
				var axisFields = new List<string>();
				axisFields.Add(Conv.ToStr(pivotCommon.GetValue("xAxisLevel1").First));
				axisFields.Add(Conv.ToStr(pivotCommon.GetValue("xAxisLevel2").First));
				axisFields.Add(Conv.ToStr(pivotCommon.GetValue("xAxisLevel3").First));
				axisFields.Add(Conv.ToStr(pivotCommon.GetValue("yAxisLevel1").First));
				axisFields.Add(Conv.ToStr(pivotCommon.GetValue("yAxisLevel2").First));
				axisFields.Add(Conv.ToStr(pivotCommon.GetValue("yAxisLevel3").First));

				var showAll = new List<string>();
				showAll.Add(Conv.ToStr(pivotCommon.GetValue("ShowAllItemsX1").First));
				showAll.Add(Conv.ToStr(pivotCommon.GetValue("ShowAllItemsX2").First));
				showAll.Add(Conv.ToStr(pivotCommon.GetValue("ShowAllItemsX3").First));
				showAll.Add(Conv.ToStr(pivotCommon.GetValue("ShowAllItemsY1").First));
				showAll.Add(Conv.ToStr(pivotCommon.GetValue("ShowAllItemsY2").First));
				showAll.Add(Conv.ToStr(pivotCommon.GetValue("ShowAllItemsY3").First));

				Dictionary<int, List<string>> lDict = new Dictionary<int, List<string>>();
				lDict.Add(0, new List<string>() { "-1" });
				lDict.Add(1, new List<string>() { "-1" });
				lDict.Add(2, new List<string>() { "-1" });
				lDict.Add(3, new List<string>() { "-1" });
				lDict.Add(4, new List<string>() { "-1" });
				lDict.Add(5, new List<string>() { "-1" });

				var data = (List<Dictionary<string, object>>)returnObject["data"];

				foreach (Dictionary<string, object> dict in data) {
					//var test = 3;
					foreach (string fieldName in axisFields) {
						if (fieldName != "") {
							if (dict[fieldName].GetType().Name != "DateTime") {
								int[] arr = Conv.ToIntArray(dict[fieldName]);
								if (arr.Length == 1) {
									if (lDict[axisFields.IndexOf(fieldName)].Contains(Conv.ToStr(arr[0])) == false)
										lDict[axisFields.IndexOf(fieldName)].Add(Conv.ToStr(arr[0]));
								}
								else if (arr.Length > 1) {
									foreach (int val in arr) {
										if (lDict[axisFields.IndexOf(fieldName)].Contains(Conv.ToStr(val)) == false)
											lDict[axisFields.IndexOf(fieldName)].Add(Conv.ToStr(val));
									}
								}
							}
						}
					}
				}

				foreach (string fieldName in axisFields) {
					if (fieldName != "") {
						var ic = new ItemColumns(instance, Conv.ToStr(chartFields.OriginalData["chartProcess"]), authenticatedSession);
						var column = ic.GetItemColumnByName(fieldName);
						var processName = "";
						Boolean timeMode = false;
						if (column.DataType == 3 || column.SubDataType == 3) {
							timeMode = true;
						}

						if (timeMode == false) {
							if (column.DataType == 6 || column.DataType == 5) {
								processName = column.DataAdditional;
							}
							else {
								processName = column.SubDataAdditional;
							}

                            var listProcessFieldSql = "SELECT * FROM dbo.processes WHERE process='" + processName + "'";
                            DataRow listProcessFields = db.GetDataRow(listProcessFieldSql);

                            var listFieldToJS = listProcessFields.Table.Columns.Cast<DataColumn>()
								.ToDictionary(c => c.ColumnName, c => listProcessFields[c]);
							returnObject.Add(fieldName + "_listData", listFieldToJS);

							var extraData = GetProcessValuesForExtraData(processName, null,
								(List<string>)lDict[axisFields.IndexOf(fieldName)],
								Conv.ToInt(showAll[axisFields.IndexOf(fieldName)]));

							if (extraData.Count > 0) {
								returnObject.Add(fieldName + "_items", extraData);
							}
						}

						var tempDict = GetProcessFieldType(chartFields.Process, fieldName);
						tempDict.Add("subdataType", column.SubDataType);

						fieldTypes.Add(fieldName + "_type", tempDict);				
					}
				}
			}
			else if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "resourceLoad") {
				int resourceMode = 0;
				if (Conv.ToStr(chartFields.OriginalData["chartProcess"]) == "user") resourceMode = 1;

				Dictionary<int,int[]> linkIdMap = new Dictionary<int,int[]>();

                if (chartFields.OriginalData.ContainsKey("mainProcessLinkMode")) {
					foreach(Dictionary<string,object> dict in (List<Dictionary<string, object>>)returnObject["data"]) {
						linkIdMap.Add(Conv.ToInt(dict["item_id"]), Conv.ToIntArray(dict[Conv.ToStr(chartFields.OriginalData["link_process_field"])]));
                    }
					returnObject.Add("linkIdMap", linkIdMap);
				}


				returnObject.Remove("data"); //We do not need this on client side


				Dictionary<string, object> recallObj = new Dictionary<string, object>();
				recallObj.Add("dashboardFilters", chartFields.OriginalData["dashboardFilters"]);
				recallObj.Add("chartId", chartFields.OriginalData["chartId"]);
				recallObj.Add("searchText", chartFields.OriginalData["searchText"]);
				recallObj.Add("process", chartFields.OriginalData["process"]);

				returnObject.Add("recallInfo", recallObj);
				//Cost center/competence

				List<string> allowedUsers = new List<string>();

				var competenciesForListSelection = new ItemBase(instance, "competency", authenticatedSession);
				var costCentersForListSelection = new ItemBase(instance, "list_cost_center", authenticatedSession);

				returnObject.Add("costCenters", costCentersForListSelection.GetAllData());
				returnObject.Add("competencies", competenciesForListSelection.GetAllData());

				string competencyStr = "";
				string costCenterStr = "";

				JObject quickFilters = new JObject();
				if (chartFields.OriginalData.ContainsKey("quickFilterPossibilities")) {
					JArray jArr = (JArray)chartFields.OriginalData["quickFilterPossibilities"];

					foreach (JObject Obj in jArr) {
						string[] arr = Conv.ToStr(Obj["key"]).Split("_");
						if (Conv.ToInt(arr[arr.Length - 1]) == Conv.ToInt(chartFields.OriginalData["linkId"])) {
							quickFilters = (JObject)Obj["data"];
							break;
						}
					}
				}

				if (quickFilters.ContainsKey("competencies")) {
					JArray arr = (JArray)quickFilters["competencies"];
					competencyStr = String.Join(",", arr.ToObject<string[]>());
				}
				if (quickFilters.ContainsKey("costCenters")) {
					JArray arr = (JArray)quickFilters["costCenters"];
					costCenterStr = String.Join(",", arr.ToObject<string[]>());
				}

				bool extraFilters = false;

				if (competencyStr.Length > 0 || costCenterStr.Length > 0) {
					extraFilters = true;
					DataTable costCentersByUserTable = null;
					DataTable competenciesByUserTable = null;

					if (competencyStr.Length > 0 && costCenterStr.Length > 0) {
						costCentersByUserTable = db.GetDatatable("SELECT item_id,selected_item_id FROM item_data_process_selections WHERE selected_item_id IN(" + costCenterStr + ") AND item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE name='cost_center' AND process='user')");
						competenciesByUserTable = db.GetDatatable("SELECT item_id,selected_item_id FROM item_data_process_selections WHERE selected_item_id IN(" + competencyStr + ") AND item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE name='competency' AND process='user')");

						List<string> allowedCostCenterUsers = new List<string>();
						List<string> allowedCompetencyUsers = new List<string>();

						foreach (DataRow row in costCentersByUserTable.Rows) {
							if (allowedCostCenterUsers.Contains(Conv.ToStr(row["item_id"])) == false) allowedCostCenterUsers.Add(Conv.ToStr(row["item_id"]));
						}
						foreach (DataRow row in competenciesByUserTable.Rows) {
							if (allowedCompetencyUsers.Contains(Conv.ToStr(row["item_id"])) == false) allowedCompetencyUsers.Add(Conv.ToStr(row["item_id"]));
						}

						foreach (string str in allowedCostCenterUsers) {
							if (allowedUsers.Contains(str) == false && allowedCompetencyUsers.Contains(str)) allowedUsers.Add(str);
						}
						foreach (string str in allowedCompetencyUsers) {
							if (allowedUsers.Contains(str) == false && allowedCostCenterUsers.Contains(str)) allowedUsers.Add(str);
						}

					}
					else if (competencyStr.Length > 0) {
						competenciesByUserTable = db.GetDatatable("SELECT item_id,selected_item_id FROM item_data_process_selections WHERE selected_item_id IN(" + competencyStr + ") AND item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE name='competency' AND process='user')");
						
						foreach (DataRow row in competenciesByUserTable.Rows) {
							if (allowedUsers.Contains(Conv.ToStr(row["item_id"])) == false) allowedUsers.Add(Conv.ToStr(row["item_id"]));
						}
					}
					else if (costCenterStr.Length > 0) {
						costCentersByUserTable = db.GetDatatable("SELECT item_id,selected_item_id FROM item_data_process_selections WHERE selected_item_id IN(" + costCenterStr + ") AND item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE name='cost_center' AND process='user')");

						foreach (DataRow row in costCentersByUserTable.Rows) {
							if(allowedUsers.Contains(Conv.ToStr(row["item_id"]))==false) allowedUsers.Add(Conv.ToStr(row["item_id"]));
						}
					}
					if (allowedUsers.Count == 0) allowedUsers.Add("0");
				}

				//

				JObject resourceCommon = JObject.Parse(Conv.ToStr(chartFields.OriginalData["resourceDataCommon"]));
				string showPlanned = Conv.ToStr(resourceCommon.GetValue("showPlanned").First);
				string showActual = Conv.ToStr(resourceCommon.GetValue("showActual").First);
				string showLoad = Conv.ToStr(resourceCommon.GetValue("showLoad").First);
				string showCapacity = "";
				if(resourceCommon.GetValue("showCapacity")!=null) showCapacity = Conv.ToStr(resourceCommon.GetValue("showCapacity").First);

				string useCustomTimeScale = "";
				if(resourceCommon.GetValue("useCustomTimeScale") != null) useCustomTimeScale = Conv.ToStr(resourceCommon.GetValue("useCustomTimeScale").First);

				int timeBefore = Conv.ToInt(Conv.ToStr(resourceCommon.GetValue("timeBefore")));
				int timeAfter = Conv.ToInt(Conv.ToStr(resourceCommon.GetValue("timeAfter")));

				if (showPlanned == "") showPlanned = "0";
				if (showActual == "") showActual = "0";
				if (showLoad == "") showLoad = "0";
				if (showCapacity == "") showCapacity = "0";

				if (useCustomTimeScale == "") useCustomTimeScale = "0";

				string dateType = Conv.ToStr(resourceCommon.GetValue("dateType").First);

				DateTime sqlStartDate = DateTime.MinValue;
				DateTime sqlEndDate = DateTime.MinValue;
				DateTime currentTime = DateTime.MinValue;

				switch (Conv.ToInt(dateType)) {
					case 0:
					 currentTime =	FirstDateOfWeek(DateTime.Now.Year, GetWeekNumber(DateTime.Now));
                        if (useCustomTimeScale == "0") {
							sqlStartDate = currentTime.AddDays(-21);
							sqlEndDate = currentTime.AddDays(7*11+6);
						}
                        else {
							sqlStartDate = currentTime.AddDays(-7 * timeBefore);
							sqlEndDate = currentTime.AddDays(7 * timeAfter+6);
						}
						break;
					case 1:
						currentTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
						if (useCustomTimeScale == "0") {
							sqlStartDate = currentTime.AddMonths(-3);
							sqlEndDate = currentTime.AddMonths(12).AddDays(-1);
						}
						else {
							sqlStartDate = currentTime.AddMonths(-timeBefore);
							sqlEndDate = currentTime.AddMonths(timeAfter+1).AddDays(-1);
						}	
						break;
					case 2:
						int quarterNumber = GetQuarterNumber(DateTime.Now.Month);
						currentTime = new DateTime(DateTime.Now.Year, quarterNumber*3-2, 1);
						if (useCustomTimeScale == "0") {
							sqlStartDate = currentTime.AddMonths(-3);
							sqlEndDate = currentTime.AddMonths(10).AddDays(-1);
						}
						else {
							sqlStartDate = currentTime.AddMonths(-3*timeBefore);
							sqlEndDate = currentTime.AddMonths(3*timeAfter + 1).AddDays(-1);
						}
						break;
					case 3:
						currentTime = new DateTime(DateTime.Now.Year, 1, 1);
						if (useCustomTimeScale == "0") {
							sqlStartDate = currentTime.AddYears(-1);
							sqlEndDate = currentTime.AddYears(3).AddDays(-1);
						}
						else {
							sqlStartDate = currentTime.AddYears(-1 * timeBefore);
							sqlEndDate = currentTime.AddYears(timeAfter + 1).AddDays(-1);
						}
						break;
				}

				DateTime originalStart = sqlStartDate;
				DateTime originalEnd = sqlEndDate;

				if(showLoad == "1") {
					sqlStartDate = DateTime.MinValue;
					sqlEndDate = new DateTime(3000, 1, 1);
				}

				List<string> itemIds = new List<string>();
				var itemDict = new Dictionary<string, string>();

				if (resourceMode == 1) {
					foreach (var obj in dTable) {
						if (extraFilters) {
							if (allowedUsers.Contains(Conv.ToStr(obj["item_id"]))) {
								itemIds.Add(Conv.ToStr(obj["item_id"]));
								itemDict.Add(Conv.ToStr(obj["item_id"]), Conv.ToStr(obj["original_item_id"]));
							}
						}
                        else {
							itemIds.Add(Conv.ToStr(obj["item_id"]));
							itemDict.Add(Conv.ToStr(obj["item_id"]), Conv.ToStr(obj["original_item_id"]));
						}
					}
				}
                else {
					foreach (var obj in dTable) {
						itemIds.Add(Conv.ToStr(obj["item_id"]));
						itemDict.Add(Conv.ToStr(obj["item_id"]), Conv.ToStr(obj["original_item_id"]));
					}
				}
				string actualSql = "";
				string plannedSql = "";

				string plannedSqlWhereField = "allocation.process_item_id";
				string actualSqlWhereField1 = "tr.owner_item_id";
				string actualSqlWhereResourceField = "";
				string actualSqlWhereField2 = "task.owner_item_id";
				if (resourceMode == 1) {
					plannedSqlWhereField = "allocation.resource_item_id";
					actualSqlWhereField1 = "tr.user_item_id";
					actualSqlWhereResourceField  = ",STRING_AGG(tr.user_item_id, ',') AS resource_item_id ";
					actualSqlWhereField2 = "tr.user_item_id";
				}

				string allowedUsersReource1 = "";
				string allowedUsersTracking1 = "";
				string taskResponsibleJoin = "";
				string taskResponsibleArchiveJoin = "";
				string taskResponsibleWherePart = "";


				if (itemIds.Count > 0) {
					if (extraFilters) {
						allowedUsersReource1 = " allocation.resource_item_id IN(" + string.Join(", ", allowedUsers.ToArray()) + ") AND ";
						allowedUsersTracking1 = " AND tr.user_item_id IN(" + string.Join(", ", allowedUsers.ToArray()) + ") ";
						taskResponsibleJoin = " LEFT JOIN item_data_process_selections idps on idps.item_column_id=(select item_column_id from item_columns where name='responsibles' AND process='task') AND idps.item_id=task.item_id ";
						taskResponsibleWherePart = " AND idps.selected_item_id IN(" + string.Join(", ", allowedUsers.ToArray()) + ") ";
					}

					if (chartFields.OriginalData["archive"] != null) {
						//"DATEADD(" + interval + ", (SELECT cast(offset AS int) FROM #offsets WHERE id=pf.item_id),tr.date) AS 'date'
						DateTime? archiveDate = Conv.ToDateTime(chartFields.OriginalData["archive"]);

						if (extraFilters) {
							taskResponsibleArchiveJoin = " LEFT JOIN get_item_data_process_selections('" + archiveDate + "') idps on idps.item_column_id=(select item_column_id from item_columns where name='responsibles' AND process='task') AND idps.item_id=task.item_id ";
						}


						actualSql = "SELECT SUM(tr.hours) AS hours,tr.date,tr.task_item_id,task.status,tr.owner_item_id " + actualSqlWhereResourceField + " FROM GET__" + instance + "_worktime_tracking_hours('" + archiveDate + "') tr " +
						"LEFT JOIN GET__" + instance + "_task('" + archiveDate + "') task ON task.item_id=tr.task_item_id WHERE tr.process='task' AND tr.date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND tr.date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' AND " + actualSqlWhereField1 + " IN(" + string.Join(",", itemIds.ToArray()) + ") AND tr.hours IS NOT NULL " + allowedUsersTracking1 + " GROUP by tr.owner_item_id,date,tr.task_item_id,task.status ";

						if (resourceMode == 0) {
							actualSql += "UNION ALL " +
							"SELECT SUM(actual_durations.hours) AS hours,actual_durations.date,actual_durations.task_item_id,task.status,task.owner_item_id FROM GET_actual_durations('" + archiveDate + "') actual_durations " +
							"LEFT JOIN GET__" + instance + "_task('" + archiveDate + "') task ON task.item_id=actual_durations.task_item_id " +
							taskResponsibleArchiveJoin +
							"WHERE date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' AND date IS NOT NULL AND " + actualSqlWhereField2 + " IN(" + string.Join(",", itemIds.ToArray()) + ") AND actual_durations.hours IS NOT NULL " + taskResponsibleWherePart + " GROUP by owner_item_id,date,actual_durations.task_item_id,task.status";
						}
						else {
							actualSql += "UNION ALL " +
							" SELECT ad.hours * (SELECT (SELECT 1.0 * COUNT(resource_item_id) FROM GET__" + instance + "_allocation('" + archiveDate + "') ka WHERE ka.task_item_id=ad.task_item_id AND resource_item_id IN(" + string.Join(",", itemIds.ToArray()) + ") )/(SELECT 1.0 * COUNT(resource_item_id) FROM GET__" + instance + "_allocation('" + archiveDate + "') ka where ka.task_item_id=ad.task_item_id)) AS hours,ad.date,ad.task_item_id, task.status,task.owner_item_id, (SELECT STRING_AGG(resource_item_id,',') FROM GET__" + instance + "_allocation('" + archiveDate + "') ka WHERE  ka.task_item_id = ad.task_item_id AND resource_item_id IN(" + string.Join(",", itemIds.ToArray()) + ")) AS resource_item_id from actual_durations('" + archiveDate + "') ad " +
							" INNER JOIN GET__" + instance + "_task('" + archiveDate + "') task ON task.item_id = ad.task_item_id " + taskResponsibleArchiveJoin + " WHERE ad.date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND ad.date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' " + taskResponsibleWherePart;
						}

						plannedSql = "SELECT SUM(allocation_durations.hours) AS hours,allocation_durations.date,allocation.process_item_id AS owner_item_id,allocation.task_item_id,task.status,STRING_AGG(allocation.resource_item_id,',') AS resource_item_id FROM GET__" + instance + "_allocation('" + archiveDate + "') allocation " +
							"LEFT JOIN GET_allocation_durations('" + archiveDate + "') allocation_durations ON allocation_durations.allocation_item_id=allocation.item_id " +
							"LEFT JOIN GET__" + instance + "_task('" + archiveDate + "') task ON task.item_id=allocation.task_item_id " +
							"WHERE " + allowedUsersReource1 + " allocation_durations.date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND allocation_durations.date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' AND date IS NOT NULL AND " + plannedSqlWhereField + " IN(" + string.Join(",", itemIds.ToArray()) + ") AND allocation_durations.hours IS NOT NULL GROUP by process_item_id,date,allocation.task_item_id,task.status ORDER BY date";
					}
					else {
						actualSql = "SELECT SUM(tr.hours) AS hours,tr.date,tr.task_item_id,task.status,tr.owner_item_id " + actualSqlWhereResourceField + " FROM _" + instance + "_worktime_tracking_hours tr " +
						"LEFT JOIN _" + instance + "_task task ON task.item_id=tr.task_item_id WHERE tr.process='task' AND tr.date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND tr.date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' AND " + actualSqlWhereField1 + " IN(" + string.Join(",", itemIds.ToArray()) + ") AND tr.hours IS NOT NULL " + allowedUsersTracking1 + " GROUP by tr.owner_item_id,date,tr.task_item_id,task.status ";

						if (resourceMode == 0) {
							actualSql += "UNION ALL " +
						"SELECT SUM(actual_durations.hours) AS hours,actual_durations.date,actual_durations.task_item_id,task.status,task.owner_item_id FROM actual_durations " +
						"LEFT JOIN _" + instance + "_task task ON task.item_id=actual_durations.task_item_id " +
						taskResponsibleJoin +
						"WHERE date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' AND date IS NOT NULL AND " + actualSqlWhereField2 + " IN(" + string.Join(",", itemIds.ToArray()) + ") AND actual_durations.hours IS NOT NULL " + taskResponsibleWherePart + " GROUP by task.owner_item_id,date,actual_durations.task_item_id,task.status";
						}
                        else {
							actualSql += " UNION ALL " +
							" SELECT ad.hours * (SELECT (SELECT 1.0 * COUNT(resource_item_id) FROM _" + instance + "_allocation ka WHERE ka.task_item_id=ad.task_item_id AND resource_item_id IN(" + string.Join(",", itemIds.ToArray()) + ") )/(SELECT 1.0 * COUNT(resource_item_id) FROM _" + instance + "_allocation ka where ka.task_item_id=ad.task_item_id)) AS hours,ad.date,ad.task_item_id, task.status,task.owner_item_id,(SELECT STRING_AGG(resource_item_id,',') FROM   _" + instance + "_allocation ka WHERE  ka.task_item_id = ad.task_item_id AND resource_item_id IN(" + string.Join(",", itemIds.ToArray()) + ")) AS resource_item_id from actual_durations ad " +
							" INNER JOIN _" + instance + "_task task ON task.item_id = ad.task_item_id " + taskResponsibleJoin + " WHERE ad.date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND ad.date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' " + taskResponsibleWherePart;
							
						}

						plannedSql = "SELECT SUM(allocation_durations.hours) AS hours,allocation_durations.date,allocation.process_item_id AS owner_item_id,allocation.task_item_id,task.status,STRING_AGG(allocation.resource_item_id,',') AS resource_item_id FROM _" + instance + "_allocation allocation " +
							"LEFT JOIN allocation_durations ON allocation_durations.allocation_item_id=allocation.item_id " +
							"LEFT JOIN _" + instance + "_task task ON task.item_id=allocation.task_item_id " +
							"WHERE " + allowedUsersReource1 + " date>='" + sqlStartDate.ToString("yyyy-MM-dd") + "' AND date < '" + sqlEndDate.ToString("yyyy-MM-dd") + "' AND date IS NOT NULL AND " + plannedSqlWhereField + " IN(" + string.Join(",", itemIds.ToArray()) + ") AND allocation_durations.hours IS NOT NULL GROUP by process_item_id,date,allocation.task_item_id,task.status ORDER BY date";
					}

					JObject offsetsJObj = null; 
					if(chartFields.OriginalData.ContainsKey("offsets")) offsetsJObj = (JObject)chartFields.OriginalData["offsets"];

					int offsetInterval = 0;
					if (chartFields.OriginalData.ContainsKey("offset_interval")) {
						string intervalStr = Conv.ToStr(chartFields.OriginalData["offset_interval"]);
						string[] options = { "day", "week", "month", "quarter", "year" };
						offsetInterval = Array.IndexOf<string>(options,intervalStr);
					}

					DataTable rawActualData = null;
					DataTable rawPlannedData = null;

					Dictionary<string, ExpandoObject> loadData = new Dictionary<string, ExpandoObject>();

					DateTime startDate = new DateTime();
					DateTime endDate = new DateTime();

					Dictionary<int, double> actualsPerTask = new Dictionary<int, double>();
					Dictionary<int, double> plannedLoadLeftPerDay = new Dictionary<int, double>();


					if (showActual == "1" || showLoad == "1") {
						rawActualData = db.GetDatatable(actualSql);
						if (offsetsJObj != null) {
							foreach (DataRow row in rawActualData.Rows) {
								if (offsetsJObj.ContainsKey(Conv.ToStr(itemDict[Conv.ToStr(row["owner_item_id"])]))) {
									if (row["date"] != null) {
										int offset = Conv.ToInt(offsetsJObj[Conv.ToStr(itemDict[Conv.ToStr(row["owner_item_id"])])]);
										
										DateTime dt = (DateTime)row["date"];
										switch (Conv.ToInt(offsetInterval)) {
											case 0:
												row["date"] = dt.AddDays(offset);
												break;
											case 1:
												row["date"] = dt.AddDays(offset * 7);
												break;
											case 2:
												row["date"] = dt.AddMonths(offset);
												break;
											case 3:
												row["date"] = dt.AddMonths(offset * 3);
												break;
											case 4:
												row["date"] = dt.AddYears(offset);
												break;
											default:
												Console.WriteLine("Seen this is not a good sign! [1]");
												break;
										}
									}
								}

							}
						}
						DataView rawActualDataView = rawActualData.DefaultView;
						rawActualDataView.Sort = "date ASC";
						rawActualData = rawActualDataView.ToTable();

						if (rawActualData.Rows.Count > 0) {
							DataRow first = rawActualData.Rows[0];
							DataRow last = rawActualData.Rows[rawActualData.Rows.Count - 1];
							startDate = DateTime.Parse(Conv.ToStr(first["date"]));
							endDate = DateTime.Parse(Conv.ToStr(last["date"]));

							List<int> tasksIds = new List<int>();
							foreach (DataRow row in rawActualData.Rows) {
								if (tasksIds.Contains(Conv.ToInt(row["task_item_id"])) == false) tasksIds.Add(Conv.ToInt(row["task_item_id"]));
							}
							foreach (int id in tasksIds) {
								double hours = 0;

								foreach (DataRow row in rawActualData.Select("task_item_id=" + id)) {
									hours += Conv.ToDouble(row["hours"]);
								}
								actualsPerTask.Add(id, hours);
							}
						}
					}
					//itemDict
					if (showPlanned == "1" || showLoad == "1") {
						rawPlannedData = db.GetDatatable(plannedSql);
						if (offsetsJObj != null) {
							foreach (DataRow row in rawPlannedData.Rows) {
								if (offsetsJObj.ContainsKey(Conv.ToStr(itemDict[Conv.ToStr(row["owner_item_id"])]))) {
									if (row["date"] != null) {
										int offset = Conv.ToInt(offsetsJObj[Conv.ToStr(itemDict[Conv.ToStr(row["owner_item_id"])])]);
										DateTime dt = (DateTime)row["date"];
										switch (Conv.ToInt(offsetInterval)) {
											case 0:
												row["date"] = dt.AddDays(offset);
												break;
											case 1:
												row["date"] = dt.AddDays(offset * 7);
												break;
											case 2:
												row["date"] = dt.AddMonths(offset);
												break;
											case 3:
												row["date"] = dt.AddMonths(offset * 3);
												break;
											case 4:
												row["date"] = dt.AddYears(offset);
												break;
											default:
												Console.WriteLine("Seen this is not a good sign! [2]");
												break;
										}
									}
								}

							}
						}

						if (rawPlannedData.Rows.Count > 0) {
							DataRow first = rawPlannedData.Rows[0];
							DataRow last = rawPlannedData.Rows[rawPlannedData.Rows.Count - 1];

							if (startDate == DateTime.MinValue || startDate > DateTime.Parse(Conv.ToStr(first["date"]))) {
								startDate = DateTime.Parse(Conv.ToStr(first["date"]));
							}

							if (endDate == DateTime.MinValue || endDate < DateTime.Parse(Conv.ToStr(last["date"]))) {
								endDate = DateTime.Parse(Conv.ToStr(last["date"]));
							}
						}
					}
					//Load
					if (showLoad == "1") {
						Dictionary<int, object> tasksIds = new Dictionary<int, object>();
						DateTime dateNow = DateTime.Now;

						for (int i = 0; i < rawPlannedData.Rows.Count; i++) {
							if (Conv.ToInt(rawPlannedData.Rows[i]["status"]) == 0) {
								int key = Conv.ToInt(rawPlannedData.Rows[i]["task_item_id"]);

								if (tasksIds.ContainsKey(key)) {
									List<double> tempL = (List<double>)tasksIds[key];
									tempL[0] += Conv.ToDouble(rawPlannedData.Rows[i]["hours"]);

									if (new DateTime(dateNow.Year, dateNow.Month, dateNow.Day) <= DateTime.Parse(Conv.ToStr(rawPlannedData.Rows[i]["date"]))) {
										tempL[1] += 1;
									}

									tasksIds[key] = tempL;
								}
								else {
									List<double> tempL = new List<double>();
									tempL.Add(Conv.ToDouble(rawPlannedData.Rows[i]["hours"]));
									tempL.Add(0);
									if (new DateTime(dateNow.Year, dateNow.Month, dateNow.Day) <= DateTime.Parse(Conv.ToStr(rawPlannedData.Rows[i]["date"]))) {
										tempL[1] += 1;
									}
									tasksIds[key] = tempL;
								}

							}

						}
						if (tasksIds.ContainsKey(0)) tasksIds.Remove(0);

						int[] temp = null;
						temp = tasksIds.Keys.ToArray();
						for (int i = 0; i < temp.Length; i++) {
							int key = temp[i];
							List<double> tempL = (List<double>)tasksIds[key];
							double hours = tempL[0];
							double actualHours = 0;
							double taskDaysLeft = tempL[1];

							if (actualsPerTask.ContainsKey(key)) {
								actualHours = actualsPerTask[key];
							}

							if (taskDaysLeft > 0 && (hours - actualHours) > 0) {
								plannedLoadLeftPerDay.Add(key, (hours - actualHours) / taskDaysLeft);
							}
						}
						startDate = originalStart;
						endDate = originalEnd;
					}

					if (dateType == "") dateType = "0";
					loadData = HandleLoadReportData(loadData, plannedLoadLeftPerDay, dateType, startDate, endDate, rawPlannedData, rawActualData,showPlanned,showActual,showLoad,resourceMode);

					returnObject.Add("Data", loadData);

					//Capacity
					Dictionary<string, object> timeDict = new Dictionary<string, object>();
					if (showCapacity == "1") {
						string costCenterItemColumnIdSql = "SELECT ic.item_column_id FROM item_columns ic WHERE ic.name='cost_center' AND ic.process='user'";
						string costCenterOwnerItemColumnIdSql = "SELECT item_column_id FROM item_columns WHERE name='owner' AND process='list_cost_center'";

						int costCenterItemColumnId = Conv.ToInt(db.GetDataRow(costCenterItemColumnIdSql)["item_column_id"]);
						int costCenterOwnerItemColumnId = Conv.ToInt(db.GetDataRow(costCenterOwnerItemColumnIdSql)["item_column_id"]);

						string baseCalendarsSql = "SELECT calendar_id,work_hours FROM calendars WHERE base_calendar=1";
						DataTable baseCalendars = db.GetDatatable(baseCalendarsSql);

						string capacityUsersSql = "SELECT users.item_id,calendars.calendar_id,calendars.work_hours,calendars.use_base_calendar FROM _" + instance + "_user users ";
						capacityUsersSql += "INNER JOIN item_data_process_selections idps ON idps.item_id=users.item_id AND idps.item_column_id=" + costCenterItemColumnId + " ";
						capacityUsersSql += "INNER JOIN _" + instance + "_list_cost_center lcc ON lcc.item_id=idps.selected_item_id ";
						capacityUsersSql += "INNER JOIN item_data_process_selections idps2 ON idps2.item_id=lcc.item_id AND idps2.item_column_id=" + costCenterOwnerItemColumnId + " ";
						capacityUsersSql += "INNER JOIN calendars ON calendars.item_id=users.item_id ";
						if (resourceMode == 1) {
							capacityUsersSql += " WHERE users.item_id IN(" + string.Join(",", itemIds.ToArray()) + ")";
						}
						else if (extraFilters) {
							capacityUsersSql += " WHERE users.item_id IN(" + string.Join(",", allowedUsers.ToArray()) + ")";
						}
						capacityUsersSql += " GROUP by users.item_id,calendars.calendar_id,calendars.work_hours,calendars.use_base_calendar";

						DataTable capacityUsers = db.GetDatatable(capacityUsersSql);
						DateTime compareEndDate = endDate.Date;
						switch (Conv.ToInt(dateType)) {
							case 0:
								startDate = FirstDateOfWeek(startDate.Year, GetWeekNumber(startDate));
								endDate = FirstDateOfWeek(endDate.Year, GetWeekNumber(endDate)).AddDays(6);
								compareEndDate = endDate.AddDays(1);
								break;
							case 1:
								startDate = new DateTime(startDate.Year, startDate.Month, 1);
								endDate = new DateTime(endDate.Year, endDate.Month, 1).AddMonths(1).AddDays(-1);
								compareEndDate = endDate.AddDays(1);
								break;
							case 2:
								startDate = new DateTime(startDate.Year, GetQuarterNumber(startDate.Month), 1);
								endDate = new DateTime(endDate.Year, GetQuarterNumber(endDate.Month) * 3, 1).AddMonths(1).AddDays(-1);
								compareEndDate = endDate.AddDays(1);
								break;
							case 3:
								startDate = new DateTime(startDate.Year, 1, 1);
								endDate = new DateTime(endDate.Year + 1, 1, 1).AddDays(-1);
								compareEndDate = endDate.AddDays(1);
								break;
						}

						string workDaysSql = "";
						Dictionary<int, double> workHoursPerUser = new Dictionary<int, double>();
						List<double> workHours = new List<double>();
						if (capacityUsers.Rows.Count > 0) {
							for (int i = 0; i < capacityUsers.Rows.Count; i++) {
								if (Conv.ToInt(capacityUsers.Rows[i]["work_hours"]) == -1) {
									DataRow[] baseCal = baseCalendars.Select("calendar_id=" + Conv.ToInt(capacityUsers.Rows[i]["use_base_calendar"]));
									if (baseCal.Count() > 0) {
										capacityUsers.Rows[i]["work_hours"] = baseCal[0]["work_hours"];
									}
								}

								workHoursPerUser.Add(Conv.ToInt(capacityUsers.Rows[i]["item_id"]), Conv.ToDouble(capacityUsers.Rows[i]["work_hours"]));
								workHours.Add(Conv.ToDouble(capacityUsers.Rows[i]["work_hours"]));

								List<string> cal = new List<string>();
								cal.Add(Conv.ToStr(capacityUsers.Rows[i]["calendar_id"]));
								if (Conv.ToInt(capacityUsers.Rows[i]["use_base_calendar"]) > 0) {
									cal.Add(Conv.ToStr(capacityUsers.Rows[i]["use_base_calendar"]));
								}

								if (workDaysSql == "") {
									workDaysSql = "SELECT event_date,notwork,calendar_id INTO #temp FROM calendar_data WHERE event_date >= '" + startDate.ToString("yyyy-MM-dd") + "' AND event_date < '" + endDate.ToString("yyyy-MM-dd") + "'";
									workDaysSql += " SELECT user_id,event_date FROM(";

									workDaysSql += "SELECT " + capacityUsers.Rows[i]["item_id"] + " AS user_id,event_date,MIN(notwork) AS notwork FROM #temp WHERE calendar_id IN(" + String.Join(",", cal.ToArray()) + ") GROUP BY event_date";
								}
								else {
									workDaysSql += " UNION ALL SELECT " + capacityUsers.Rows[i]["item_id"] + " AS user_id,event_date,MIN(notwork) AS notwork FROM #temp WHERE calendar_id IN(" + String.Join(",", cal.ToArray()) + ") GROUP BY event_date";
								}
							}
							workDaysSql += ") temp WHERE notwork=1";

							DataTable eventsInTimeScale = db.GetDatatable(workDaysSql);
							List<DateTime> loopStartDates = new List<DateTime>();
							List<DateTime> loopEndDates = new List<DateTime>();
							List<double> dataList = new List<double>();
							List<string> dictionaryTitles = new List<string>();

							while (startDate != compareEndDate) {
								DateTime nextStartDate = new DateTime();
								string dictionaryTitle = "";
								switch (Conv.ToInt(dateType)) {
									case 0:
										nextStartDate = startDate.AddDays(7);
										dictionaryTitle = GetWeekNumber(startDate).ToString() + "/" + startDate.Year;
										break;
									case 1:
										nextStartDate = startDate.AddMonths(1);
										dictionaryTitle = startDate.Month + "/" + startDate.Year;
										break;
									case 2:
										nextStartDate = startDate.AddMonths(3);
										dictionaryTitle = GetQuarterNumber(startDate.Month).ToString() + "/" + startDate.Year;
										break;
									case 3:
										nextStartDate = startDate.AddYears(1);
										dictionaryTitle = startDate.Year.ToString();
										break;
								}

								int workDays = Conv.ToInt((nextStartDate - startDate).TotalDays);
								double totalHoursWithoutMinuses = 0;
								for (int i = 0; i < workHours.Count; i++) {
									totalHoursWithoutMinuses += workDays * workHours[i];

								}

								dataList.Add(totalHoursWithoutMinuses);

								dictionaryTitles.Add(dictionaryTitle);
								loopStartDates.Add(startDate);
								loopEndDates.Add(nextStartDate);

								startDate = nextStartDate;
							}

							for (int i = 0; i < eventsInTimeScale.Rows.Count; i++) {
								for (int j = 0; j < loopStartDates.Count; j++) {
									if ((DateTime)eventsInTimeScale.Rows[i]["event_date"] >= loopStartDates[j] && (DateTime)eventsInTimeScale.Rows[i]["event_date"] < loopEndDates[j]) {
										dataList[j] -= workHoursPerUser[Conv.ToInt(eventsInTimeScale.Rows[i]["user_id"])];
									}
								}
							}

							for (int i = 0; i < dictionaryTitles.Count; i++) {
								timeDict.Add(dictionaryTitles[i], dataList[i]);
							}
						}
					}

					returnObject.Add("Capacity", timeDict);
				}
			}
			else if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "circular") {
				//Placeholder for now. Causes crash if removed
			}
			else {
				fieldTypes.Add("labelFieldType", GetProcessFieldType(chartFields.Process, chartFields.LabelField));
				fieldTypes.Add("valueFieldType", GetProcessFieldType(chartFields.Process, chartFields.ValueField));
				if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "bubble" && Conv.ToStr(chartFields.Value2Field) != "processItemCount" || Conv.ToStr(chartFields.OriginalData["chartType"]) != "bubble") {
					fieldTypes.Add("value2FieldType", GetProcessFieldType(chartFields.Process, chartFields.Value2Field));
				}
				fieldTypes.Add("groupByFieldType", GetProcessFieldType(chartFields.Process, chartFields.GroupByField));
				fieldTypes.Add("nameFieldType", GetProcessFieldType(chartFields.Process, chartFields.NameField));

				var groupColumnInfo = GetItemColumnForProcess(chartFields.GroupByField);
				if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "pie" && chartFields.Grouping &&
					groupColumnInfo.DataType == 20) {
					string sql = "SELECT * FROM _" + instance + "_" +
								 groupColumnInfo
									 .InputName; //+ " WHERE item_id IN(" + string.Join(",",idList.ToArray()) + ")";
					Dictionary<int, object> dictReturn = new Dictionary<int, object>();
					foreach (var obj in db.GetDatatableDictionary(sql)) {
						var dictT = (Dictionary<string, object>)obj;
						int item_id = Conv.ToInt(dictT["item_id"]);
						dictReturn.Add(item_id, dictT);
					}

					returnObject["relatedItems"] = dictReturn;
				}

				if (Conv.ToStr(chartFields.OriginalData["chartType"]) == "pie" && chartFields.Grouping && (groupColumnInfo.DataType == 6 || groupColumnInfo.SubDataType == 6)) {
					string listName = "";
					if (groupColumnInfo.DataType == 16) {
						listName = groupColumnInfo.SubDataAdditional;
					}
					if (groupColumnInfo.DataType == 20) {
						listName = groupColumnInfo.InputName;
					}
					else {
						listName = groupColumnInfo.DataAdditional;
					}
					if (listName != null && listName.Contains("list")) {
						var listProcessFieldSql = "SELECT * FROM dbo.processes WHERE process='" + listName + "'";
						DataRow listProcessFields = db.GetDataRow(listProcessFieldSql);

						if (Conv.ToStr(listProcessFields["list_order"]) != "") {
							returnObject.Add("order", Conv.ToStr(listProcessFields["list_order"]));
						}
						else {
							returnObject.Add("order", "order_no");
						}
					}
					else {
						returnObject.Add("order", "order_no");
					}
				}
			}

			if (chartFields.OriginalData.ContainsKey("filters") && ((JArray)chartFields.OriginalData["filters"]).Count>0) {	
				Dictionary<string, System.Dynamic.ExpandoObject> filterData = new Dictionary<string, System.Dynamic.ExpandoObject>();
				Dictionary<string, DataTable> listDataByListName = new Dictionary<string, DataTable>();

				for (int i = 0; i < ((JArray)chartFields.OriginalData["filters"]).Count; i++) {
					string filterTxt = Conv.ToStr(((JArray)chartFields.OriginalData["filters"])[i]);
					if (filterTxt != "" && filterData.ContainsKey(filterTxt) == false) {
						var ic = new ItemColumns(instance, Conv.ToStr(chartFields.OriginalData["chartProcess"]), authenticatedSession);
						ItemColumn columnInfo = ic.GetItemColumnByName(filterTxt);

						dynamic dynObject = new System.Dynamic.ExpandoObject();
						if (columnInfo.DataType == 6 || columnInfo.SubDataType == 6) {
							string listName = "";
							if (columnInfo.DataType == 16) {
								listName = columnInfo.SubDataAdditional;
							}
							else if (columnInfo.DataType == 20) {
								listName = columnInfo.InputName;
							}
							else {
								listName = columnInfo.DataAdditional;
							}

							if (listName != null && listName != "") {
								dynObject.listData = db.GetDatatable("SELECT * FROM _" + instance + "_" + listName);
							}


						}
						else if (columnInfo.DataType == 5 || columnInfo.SubDataType == 5) {
							DataRow selector = db.GetDataRow("SELECT TOP 1 process_portfolio_id FROM process_portfolios WHERE process_portfolio_type=1 AND process='" + columnInfo.DataAdditional + "'");//Tarkista eq ja lookup sijainti prosessin nimelle
							if (selector != null) {
								dynObject.selector = Conv.ToInt(selector["process_portfolio_id"]);
							}
						}

						dynObject.columnInfo = columnInfo;
						filterData.Add(filterTxt, dynObject);
					}
				}
				if (filterData.Count > 0) returnObject.Add("filterData", filterData);
			}
            else {
				returnObject.Add("filterData", new List<string>());
			}

			returnObject.Add("fieldTypes", fieldTypes);
			returnObject.Add("inMeta", Conv.ToInt(chartFields.OriginalData["filterItemIds"]) > 0);
			return returnObject;
		}

		public Dictionary<string, object> GetStackedDataForBarChart(ChartFields chartFields, string table = null) {
			var returnObject = new Dictionary<string, object>();
			List<Dictionary<string, object>> dTable;
			//Dictionary<int, object> labels;

			var queryForAllRows = GetQueryForRows(chartFields);

			var function = chartFields.GroupingFunction;
			var valCol = chartFields.ValueField;


			var groupedQuery = "SELECT " + chartFields.GroupByField + " as title, " + chartFields.SubGroupByField +
			                   " as sub_title, " +
			                   chartFields.GroupByField + " as selected_item_id, " +
			                   function + "(" + valCol + ") as val " +
			                   "FROM (" + queryForAllRows + ") q GROUP BY " +
			                   ValidateOrderColumn(chartFields.GroupByField) + ", " +
			                   chartFields.SubGroupByField + " ORDER BY " +
			                   ValidateOrderColumn(chartFields.GroupByField);
			dTable = db.GetDatatableDictionary(groupedQuery);

			var subIds = new List<int>();
			var mainIds = dTable
				.Select(x => {
					subIds.Add(Conv.ToInt(x["sub_title"]));
					return Conv.ToInt(x["title"]);
				})
				.Distinct()
				.ToList();
			var mainLabels = GetItemsForIds(chartFields.Process, chartFields.GroupByField, mainIds);

			var subLabels = GetItemsForIds(chartFields.Process, chartFields.SubGroupByField,
				subIds.Distinct().ToList());

			returnObject.Add("data", dTable);
			returnObject.Add("stacked", true);
			returnObject.Add("mainLabels", mainLabels);
			returnObject.Add("subLabels", subLabels);

			return returnObject;
		}

		public Dictionary<string, object> GetArchivedData(ChartFields chartFields, string dateString) {
			var table = "get__" + instance + "_" + process + "(\'" + dateString + "\')";
			return GetCurrentData(chartFields, table);
		}

		public DataTable GetDataForDashboardFromArchive(string fieldsForSql, string whereClause, string archiveDate) {
			SetDBParam(SqlDbType.NVarChar, "@archiveDate", archiveDate);
			var table = "get__" + instance + "_" + process + "(\'" + archiveDate + "\')";
			if (whereClause == null) {
				whereClause = "";
			}

			var sql = "SELECT * FROM " + table;
			return db.GetDatatable(sql);
		}

		private Dictionary<int, object> GetLabels(string processName, List<int> ids) {
			var processItems = new Dictionary<int, object>();
			var ib = new ItemBase(instance, processName, authenticatedSession);
			foreach (var id in ids) {
				processItems.Add(id, ib.GetItem(id));
			}

			return processItems;
		}

		private ItemColumn GetItemColumnForProcess(string fieldName) {
			if (string.IsNullOrEmpty(fieldName)) {
				return null;
			}

			SetDBParam(SqlDbType.NVarChar, "@group_field_name", fieldName);
			// build sql with helpers

			var select = SqlHelper.Select(false, "*");
			var from = SqlHelper.From("item_columns");

			string[] sqlConditions = {
				SqlHelper.ItemsShouldBeEqual("name", "@group_field_name"),
				SqlHelper.ItemsShouldBeEqual("instance", "@instance"),
				SqlHelper.ItemsShouldBeEqual("process", "@process")
			};
			var where = SqlHelper.Where(SqlHelper.JoinWithAnd(sqlConditions));

			var getGroupByFieldIdSql = SqlHelper.JoinWithSpace(select, from, where);

			var itemColumn = new ItemColumn(db.GetDataRow(getGroupByFieldIdSql, DBParameters), authenticatedSession);

			if (itemColumn.IsEquation()) {
				SetDBParam(SqlDbType.Int, "@itemColumnId", itemColumn.ItemColumnId);
				itemColumn.Equations =
					db.GetDatatableDictionary(
						"SELECT * FROM item_column_equations WHERE parent_item_column_id =  @itemColumnId",
						DBParameters);
			}

			return itemColumn;
		}


		private string GetTextSearchConditions(string textSearch, int processPortfolioId) {
			var pColumns =
				new ProcessPortfolioColumns(instance, process, authenticatedSession);
			var searchText = " (1=0) ";
			if (!string.IsNullOrEmpty(textSearch)) {
				var filteringTexts = textSearch.Split(' ').ToList();
				foreach (var col in pColumns.GetColumns(processPortfolioId)) {
					if (col.UseInFilter) {
						if (col.ItemColumn.IsDatabasePrimitive()) {
							foreach (var search in filteringTexts) {
								var fieldName = "processdata." + col.ItemColumn.Name;
								var coalesceFunc = SqlHelper.WrapWithFunction("COALESCE", fieldName, "''");
								var loCaseFunc = SqlHelper.WrapWithFunction("LOWER", coalesceFunc);
								var searchFor = "'%" + search.ToLower() + "%'";
								var likeComparison = loCaseFunc + " LIKE " + searchFor + " ";
								searchText = SqlHelper.JoinWithOr(searchText, likeComparison);
							}
						}
					}
				}
			} else {
				return "";
			}

			return searchText;
		}

		private string GetValueFieldForGroupingSubQuery(string sumOrCount, string fieldName) {
			if (string.IsNullOrEmpty(sumOrCount)) {
				return "";
			}

			if (sumOrCount.ToLower().Equals("count")) {
				return "item_id";
			}

			return SqlHelper.WrapWithFunction("CONVERT", "float", fieldName);
		}

		private string GetAggregateFunctionForGrouping(string sumOrCount, string fieldName) {
			var convertToFloat = SqlHelper.WrapWithFunction("CONVERT", "float", fieldName);
			var aggregateFunction = SqlHelper.WrapWithFunction("SUM", convertToFloat) + " as sum";
			if (string.IsNullOrEmpty(sumOrCount)) return "";
			if (sumOrCount.ToLower().Equals("count")) aggregateFunction = "COUNT(*) as sum";

			return aggregateFunction;
		}

		private void SetDashboardDbParameters(ProcessDashboard dashboard) {
			SetDBParam(SqlDbType.Int, "@process_portfolio_id", dashboard.ProcessPortfolioId);
			SetDBParam(SqlDbType.NVarChar, "@variable", dashboard.Variable);
		}

		private void SetChartDbParameters(ProcessDashboardChart chart) {
			SetDBParam(SqlDbType.NVarChar, "@variable", chart.Variable);
			SetDBParam(SqlDbType.NVarChar, "@json_data", chart.JsonData);
			SetDBParam(SqlDbType.Int, "@process_portfolio_id", chart.ProcessPortfolioId);
		}

		private string GetExternalFieldFilteringString(
			bool distinct,
			string groupByField,
			string filters,
			string selectFields) {
			var groupByClause = string.IsNullOrEmpty(groupByField) ? "" : "ic.name='" + groupByField + "' AND ";
			var selectPhrase = SqlHelper.Select(distinct, selectFields);
			var query = selectPhrase +
			            "FROM item_data_process_selections idps " +
			            "JOIN item_columns ic ON ic.item_column_id=idps.item_column_id " +
			            "WHERE " + groupByClause +
			            SqlHelper.ItemsShouldBeEqual("process", "@process") +
			            " AND " +
			            SqlHelper.ItemsShouldBeEqual("instance", "@instance");
			var filterList = BuildFilteringForLists(filters);
			foreach (var f in filterList) {
				query += "AND idps.item_id in (" + f + ") ";
			}

			return query;
		}


		private string GetWhereClauseForFilter(string itemColumnId, List<string> ids) {
			var joinedIds = string.Join(", ", ids);

			var select = SqlHelper.Select(true, "idps.item_id");
			var from = SqlHelper.From("dbo.item_data_process_selections idps");
			var where = SqlHelper.Where(
				SqlHelper.JoinWithAnd(
					SqlHelper.ItemsShouldBeEqual("idps.item_column_id", itemColumnId),
					"idps.selected_item_id IN (" + joinedIds + ")"
				));
			return SqlHelper.JoinWithSpace(select, from, where);
		}

		/// <summary>
		/// Builds SQL where conditions by filtering received in url parameter
		/// </summary>
		/// <param name="filters"></param>
		/// <returns>List of where clauses (joined with proper operator)</returns>
		private List<string> BuildFilteringForLists(string filters) {
			if (string.IsNullOrEmpty(filters)) return new List<string>();
			var conditions = new List<string>();
			foreach (var listIdPair in filters.Split(',')) {
				var listAndIds = listIdPair.Split(';');
				var idsArray = listAndIds[1].Split('-');
				conditions.Add(GetWhereClauseForFilter(listAndIds[0], idsArray.ToList()));
			}

			return conditions;
		}

		private DataTable CombineDataTables(DataTable dt1, DataTable dt2) {
			DataTable dt3 = dt2.Copy();
			Boolean addrow = false;
			Boolean rowMatched = false;

			for (int i = dt1.Rows.Count - 1; i >= 0; i--) {
				addrow = true;
				for (int j = dt2.Rows.Count - 1; j >= 0; j--) {
					rowMatched = true;
					for (int k = dt1.Columns.Count - 1; k >= 0; k--) {
						if (dt1.Columns[k].ToString() != dt2.Columns[k].ToString()) {
							rowMatched = false;
							break;
						}
					}

					if (rowMatched == true) {
						addrow = false;
						break;
					}
				}

				if (addrow == true) {
					dt3.Rows.Add(dt1.Rows[i].ItemArray);
				}
			}

			return dt3;
		}

		private PortfolioSqls removeUnwantedColumns(PortfolioSqls pSqls, List<string> allowedColumns) {
			var toBeRemoved = new List<int>();

			if (pSqls.equationColumns.Count > 0) {
				var itemCols = new ItemColumns(instance, process, authenticatedSession);

				List<ItemColumn> eqColumns = new List<ItemColumn>();
				List<string> eqHandled = new List<string>();

				foreach (var col in pSqls.equationColumns) {
					if (allowedColumns.Contains(Conv.ToStr(col.Name)))
						eqColumns.Add(col);
				}
				bool eqHandlingInProgress = true;
				while (eqHandlingInProgress) {
					var equationIds = new List<string>();
					foreach (var eq in eqColumns) {
						if (eq.Equations.Count > 0) {
							foreach (var eqRow in eq.Equations) {
								if (Conv.ToInt(eqRow["item_column_id"]) > 0)
									equationIds.Add(Conv.ToStr(eqRow["item_column_id"]));
								if (Conv.ToInt(eqRow["sql_param1"]) > 0)
									equationIds.Add(Conv.ToStr(eqRow["sql_param1"]));
								if (Conv.ToInt(eqRow["sql_param2"]) > 0)
									equationIds.Add(Conv.ToStr(eqRow["sql_param2"]));
								if (Conv.ToInt(eqRow["sql_param3"]) > 0)
									equationIds.Add(Conv.ToStr(eqRow["sql_param3"]));
							}							
						}
						eqHandled.Add(eq.Name);
					}
					if (equationIds.Count > 0) {
						var equationNameQuary = "SELECT name FROM dbo.item_columns WHERE item_column_id IN(" +
												string.Join(",", equationIds.ToArray()) + ")";
						var nameResult = db.GetDatatable(equationNameQuary);
						foreach (DataRow row in nameResult.Rows) {
							allowedColumns.Add(Conv.ToStr(row["name"]));
						}
					}
					eqColumns = new List<ItemColumn>();
					foreach (var col in pSqls.equationColumns) {
						if (allowedColumns.Contains(Conv.ToStr(col.Name)) && eqHandled.Contains(Conv.ToStr(col.Name))==false){
							eqColumns.Add(col);
						}
					}
                    if (eqColumns.Count == 0) {
						eqHandlingInProgress = false;
					}
				}

				if (allowedColumns.Contains("noSelection")) allowedColumns.Remove("noSelection");
				foreach (var col in pSqls.equationColumns) {
					if (allowedColumns.Contains(Conv.ToStr(col.Name)) == false)
						toBeRemoved.Add(pSqls.equationColumns.IndexOf(col));
				}

				toBeRemoved.Reverse();
				foreach (var index in toBeRemoved) {
					pSqls.equationColumns.RemoveAt(index);
				}			
			}

			toBeRemoved = new List<int>();
			foreach (string sql in pSqls.sqlcols) {
				string sqlHandled = sql.Replace(" ", "");
				if ( allowedColumns.Contains(pSqls.cols[pSqls.sqlcols.IndexOf(sql)])) {
					foreach (string name in pSqls.cols) {
						if (sqlHandled.Contains("pf." + name)) {
							if (allowedColumns.Contains(name) == false) {
								allowedColumns.Add(name);
							}
						}
					}
				}
			}

			foreach (var col in pSqls.cols) {
				if (allowedColumns.Contains(col) == false) toBeRemoved.Add(pSqls.cols.IndexOf(col));
			}

			toBeRemoved.Reverse();
			foreach (var index in toBeRemoved) {
				pSqls.cols.RemoveAt(index);
				pSqls.sqlcols.RemoveAt(index);
			}

			toBeRemoved = new List<int>();
			foreach (var col in pSqls.statusColumns) {
				if (allowedColumns.Contains(Conv.ToStr(col)) == false)
					toBeRemoved.Add(pSqls.statusColumns.IndexOf(col));
			}

			toBeRemoved.Reverse();
			foreach (var index in toBeRemoved) {
				pSqls.statusColumns.RemoveAt(index);
			}

			var allowed = new List<Keto5.x.platform.server.processes.itemColumns.ItemColumn>();
			foreach (var col in pSqls.processColumns) {
				if (allowedColumns.Contains(Conv.ToStr(col.Name))) allowed.Add(col);
			}


			pSqls.processColumns = allowed;

			return pSqls;
		}
		//private dynamic GetPortFolioFilters(int portfolio_id, int user_id) { // Lets keep this here for while. May be neeted later if link chart's sub process filters are needed.

		//    Dictionary<int, object> itemFilters = new Dictionary<int, object>();

		//    SetDBParam(SqlDbType.Int, "@link_user_id", user_id);
		//    SetDBParam(SqlDbType.Int, "@link_portfolio_id", portfolio_id);

		//    string sql = "SELECT value FROM instance_user_configurations WHERE [group] = 'portfolios' AND user_id = @link_user_id AND identifier = @link_portfolio_id";

		//    var configurationData = db.GetDataRow(sql, DBParameters);
		//    JToken filterToken = null;
		//    filterToken = JObject.Parse(Conv.toStr(configurationData.ItemArray[0]))["filters"];

		//    string searchTextStr = filterToken.Value<string>("text");
		//    JObject selectItems = filterToken.Value<JObject>("select");

		//    var selectItemIds = selectItems.Properties().Select(p => p.Name).ToList();    

		//    foreach (var item in selectItemIds) {
		//        var valueArr = selectItems[item] as JArray;
		//        itemFilters.Add(Conv.toInt(item), valueArr);
		//    }

		//    JObject dateRangeItems = filterToken.Value<JObject>("dateRange");
		//    var dateRangeIds = dateRangeItems.Properties().Select(p => p.Name).ToList();

		//    foreach (var item in dateRangeIds) {
		//        var valueArr = dateRangeItems[item];

		//        var start = valueArr.Value<DateTime?>("start");
		//        var end = valueArr.Value<DateTime?>("end");

		//        var times = new DateTime?[2];
		//        times[0] = start;
		//        times[1] = end;

		//        if (start != null || end != null) {
		//            itemFilters.Add(Conv.toInt(item), JArray.FromObject(times));
		//        }
		//    }

		//    JObject rangeItems = filterToken.Value<JObject>("range");
		//    var rangeIds = rangeItems.Properties().Select(p => p.Name).ToList();

		//    foreach (var item in rangeIds) {
		//        var valueArr = rangeItems[item];
		//        var start = valueArr.Value<int?>("start");
		//        var end = valueArr.Value<int?>("end");

		//        var numbers = new int?[2];
		//        numbers[0] = start;
		//        numbers[1] = end;

		//        if (start != null || end != null) {
		//            itemFilters.Add(Conv.toInt(item), JArray.FromObject(numbers));
		//        }
		//    }

		//    dynamic dynObject = new System.Dynamic.ExpandoObject();
		//    dynObject.filterParserFilters = itemFilters;
		//    dynObject.searchText = searchTextStr;

		//    return dynObject;
		//}

		private List<string> GetMainProcessFilterValues(string process, ChartFields chartFields, int portfolio_id,
			int user_id, string source_field, bool archiveMode, DateTime? archiveDate, DataTable tables) {
			//dynamic filterObj = GetPortFolioFilters(portfolio_id, user_id);

			//var searchText = filterObj.searchText;
			//Dictionary<int, object> filterData = filterObj.filterParserFilters;
			//int linkSourceItemColumnId = 0;
			//bool linkSourceProcessOrList = false;

			Dictionary<string, object> dashBoardFiltersOriginal =
				(Dictionary<string, object>)chartFields.OriginalData["dashboardFilters"];
			var dashBoardFilters = new Dictionary<int, object>();

			foreach (var col in dashBoardFiltersOriginal) {
				dashBoardFilters.Add(Conv.ToInt(col.Key), col.Value);
			}

			var ic = new ItemColumns(instance, process, authenticatedSession);
			var processTable = GetProcessTableName(process);
			var portfolioSqlQueryService =
				new PortfolioSqlService(instance, authenticatedSession, ic, processTable, process);
			PortfolioSqls pSqls = null;
			if (archiveMode) {
				pSqls = portfolioSqlQueryService.GetSql(0, chartFields.GetFieldIfExists("searchText"), archiveDate);
			}
			else {
				pSqls = portfolioSqlQueryService.GetSql(0, chartFields.GetFieldIfExists("searchText"));
			}

			List<string> allowedColumns = new List<string>();
			allowedColumns.Add(source_field);

			var conditionWhere = " (1=1) ";
			var c = new EntityRepository(instance, process, authenticatedSession);
			var evaluatedConditions = c.GetWhere(portfolio_id);
			if (evaluatedConditions.Length > 0) conditionWhere = conditionWhere + " AND " + evaluatedConditions;

			//foreach (var col in pSqls.equationColumns) {
			//	if (col.Name == Conv.ToStr(source_field) && (col.SubDataType == 5 || col.SubDataType == 6)) {
			//		linkSourceProcessOrList = true;
			//		linkSourceItemColumnId = col.ItemColumnId;
			//	}
			//}
			//if (linkSourceProcessOrList == false) {
			//	foreach (var col in pSqls.processColumns) {
			//		if (col.Name == Conv.ToStr(source_field) && (col.SubDataType == 5 || col.SubDataType == 6 || col.DataType == 5 || col.DataType == 6)) {
			//			linkSourceProcessOrList = true;
			//			linkSourceItemColumnId = col.ItemColumnId;
			//		}
			//	}
			//}

			var pCols = portfolioSqlQueryService.GetColumns(portfolio_id);

			if (allowedColumns.Contains("item_id") == false) allowedColumns.Add("item_id");
			pSqls.searchs = portfolioSqlQueryService.GetTextSearchClauses(chartFields.GetFieldIfExists("searchText"), pCols);

			bool searchColumnsHandlingNeeded = false;
			List<string> allowedColumnsOriginalNoEq = new List<string>();
			foreach (string col in allowedColumns) {
				if (allowedColumnsOriginalNoEq.Contains(col) == false) allowedColumnsOriginalNoEq.Add(col);
			}

			foreach (var obj in pSqls.equationColumns) {
				if (allowedColumns.Contains(obj.Name)) {
					searchColumnsHandlingNeeded = true;
					allowedColumnsOriginalNoEq.Remove(obj.Name);
				}
			}

			if (searchColumnsHandlingNeeded && pSqls.searchs.Count > 0 && pSqls.searchs[0] != "" && pSqls.searchs[0] != null) {
				foreach (string str in pSqls.cols) {
					if (pSqls.searchs[0].Contains(str) && allowedColumns.Contains(str) == false) {
						allowedColumns.Add(str);
					};
				}
				foreach (var obj in pSqls.equationColumns) {
					if (pSqls.searchs[0].Contains(obj.Name) && allowedColumns.Contains(obj.Name) == false) {
						allowedColumns.Add(obj.Name);
					};
				}
			}

			pSqls = removeUnwantedColumns(pSqls, allowedColumns);

			foreach (string col in new List<string>(allowedColumnsOriginalNoEq)) {
				if (pSqls.cols.Contains(col) == false) allowedColumnsOriginalNoEq.Remove(col);
			}

			for (int i = 0; i < allowedColumnsOriginalNoEq.Count; i++) {
				allowedColumnsOriginalNoEq[i] = "pf." + allowedColumnsOriginalNoEq[i];
			}

			// This block's purpose is to add possible problematic columns to allowedColumns list. This is ment to handle filter based fields only.
			//SetDBParam(SqlDbType.Int, "@process_portfolio_id", portfolio_id);
			//var filtersColumnQuary =
			//	"SELECT item_columns.name AS column_name,item_columns.data_type FROM process_portfolio_columns LEFT JOIN item_columns ON process_portfolio_columns.item_column_id = item_columns.item_column_id where process_portfolio_id=@process_portfolio_id AND process_portfolio_columns.use_in_filter=1";
			//DataTable filterColumnResult = db.GetDatatable(filtersColumnQuary, DBParameters);

			pSqlCopy = pSqls;

			var headers = string.Join(", ", pSqls.sqlcols);

			var itemFilterParser = new ItemFilterParser(dashBoardFilters,
				new ItemColumns(instance, process, authenticatedSession), instance, process,
				authenticatedSession);
			var filters = itemFilterParser.Get();

			var eSql1 = "";
			var esql2 = "";
			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					equationSql += "," + ic.GetEquationQuery(col) + " " + col.Name;
				}

				//eSql1 = " SELECT  * " + equationSql + " FROM ( ";
				eSql1 = " SELECT  " + String.Join(",", allowedColumnsOriginalNoEq.ToArray()) + " " + equationSql + " FROM ( ";
				esql2 = " ) pf ";
			}

			if (chartFields.OriginalData["phase"] != null) {
				List<int> selectedPhases = Conv.ToIntArray(chartFields.OriginalData["phase"]).OfType<int>().ToList();
				ItemBase ib = new ItemBase(instance, chartFields.Process, authenticatedSession);
				string phaseWhere = ib.getPhaseFilterWhere(selectedPhases);
				if (filters == "") {
					filters = " AND " + phaseWhere;
				}
				else {
					filters = filters + " AND " + phaseWhere;
				}
			}

			string sql = "";

			sql = eSql1 + " SELECT " + headers + " FROM " + processTable + " pf " + esql2 +
		   "  WHERE " + conditionWhere + " " + filters + string.Join("  ", pSqls.searchs);
			//}
			if (archiveMode) {
				sql = setArchiveSql(sql, tables, archiveDate);
			}

			var result = db.GetDatatable(sql, DBParameters);
			var resultList = new List<string>();
			foreach (DataRow row in result.Rows) {
				foreach (string id in Conv.ToStr(row[0]).Split(",")) {
					if (resultList.Contains(Conv.ToStr(id)) == false && Conv.ToStr(id) != "")
						resultList.Add(Conv.ToStr(id));
				}
			}

			return resultList;
		}

		private List<Dictionary<string, object>> GetProcessValuesForExtraData(string process,
			List<string> allowedColumns, List<string> idFilter, int showAll) {
			var idFilterStr = " item_id IN(" + string.Join(",", idFilter.ToArray()) + ")";
			if (showAll > 0) idFilterStr = "1=1";
			//int portfolio_id = 0;
			//ChartFields chartFields = null;

			var ic = new ItemColumns(instance, process, authenticatedSession);
			var processTable = GetProcessTableName(process);
			var portfolioSqlQueryService =
				new PortfolioSqlService(instance, authenticatedSession, ic, processTable, process);
			PortfolioSqls pSqls = portfolioSqlQueryService.GetSql(0);

			var c = new EntityRepository(instance, process, authenticatedSession);
			if (allowedColumns != null) {
				pSqls = removeUnwantedColumns(pSqls, allowedColumns);
			}

			var headers = string.Join(", ", pSqls.sqlcols);

			var eSql1 = "";
			var esql2 = "";
			if (pSqls.equationColumns.Count > 0) {
				var equationSql = "";
				foreach (var col in pSqls.equationColumns) {
					equationSql += "," + ic.GetEquationQuery(col) + " " + col.Name;
				}
				//foreach (var name in supportFields) {
				//    if (headers.IndexOf(name + "_str") > -1) {
				//        eSql1 = eSql1.Replace(name, name + "_str");
				//    }
				//}

				eSql1 = " SELECT  * " + equationSql + " FROM ( ";
				esql2 = " ) pf ";
			}

			var sql = eSql1 + " SELECT " + headers + " FROM " + processTable + " pf " + esql2 + "  WHERE " +
			          idFilterStr;

			//var result = db.GetDatatableDictionary(sql, DBParameters);
			//return result;
			return db.GetDatatableDictionary(sql);
		}

		private List<string> GetProcessSelectorPrimaryColumnNames(string process, bool compileForClient = false,
			bool includeSecondaries = false) {
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var where = "1";
			if (includeSecondaries) where = "1,2";

			var sql =
				"SELECT ic.*, CASE WHEN use_in_select_result = 1 THEN 'primary' ELSE 'secondary' END AS 'primary' FROM process_portfolios p LEFT JOIN process_portfolio_columns pc ON pc.process_portfolio_id = p.process_portfolio_id LEFT JOIN item_columns ic ON ic.item_column_id = pc.item_column_id WHERE p.process = @process AND (p.process_portfolio_type = 10 OR p.process_portfolio_type = 1) AND pc.use_in_select_result IN (" +
				where + ") ORDER BY pc.order_no  ASC";
			var result = new List<string>();
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var ic = new ItemColumn(row, authenticatedSession);
				var c = ic.Name;
				if (compileForClient && (ic.IsProcess() || ic.IsList())) {
					c =
						"'{ \"item_id\": \"' + CAST(ISNULL((SELECT TOP 1 selected_item_id FROM item_data_process_selections idps WHERE idps.item_column_id = " +
						ic.ItemColumnId +
						" AND idps.item_id = pf.item_id),'') AS nvarchar(255)) + '\", \"process\": \"" +
						ic.DataAdditional + "\", \"primary\": \"" + row["primary"] + "\"}'";
				} else if (compileForClient && ic.IsSubquery()) {
					c = "'{ \"name\": \"' + " + SqlJsonValue(ic.GetColumnQuery(instance)) + " + '\", \"primary\": \"" +
					    row["primary"] + "\"}'";
				} else if (compileForClient && ic.IsDate()) {
					c = "'{ \"date\": \"' + " + SqlJsonValue(ic.Name) + " + '\", \"primary\": \"" + row["primary"] +
					    "\"}'";
				} else if (compileForClient) {
					c = "'{ \"name\": \"' + " + SqlJsonValue(ic.Name) + " + '\", \"primary\": \"" + row["primary"] +
					    "\"}'";
				}

				result.Add(c);
			}

			return result;
		}

		private string setArchiveSql(string sql, DataTable processTable,DateTime? archiveDate) {
			string archiveSql = sql;
			foreach (DataRow row in processTable.Rows) {
				if (archiveSql.ToLower().Contains(Conv.ToStr(row["TABLE_NAME"]).ToLower()) && archiveSql.ToLower().Contains(Conv.ToStr("GET_" + Conv.ToStr(row["TABLE_NAME"])).ToLower())==false) {
					archiveSql = archiveSql.Replace(Conv.ToStr(row["TABLE_NAME"]), "GET_" + Conv.ToStr(row["TABLE_NAME"]) + "('" + archiveDate + "')");
				}
			}
			return archiveSql;
		}

		private string SqlJsonValue(string str) {
			return "REPLACE(CAST(ISNULL(" + str + ",'') AS NVARCHAR(255)),CHAR(34),CHAR(39))";
		}

		private static int GetWeekNumber(DateTime time) {
			//private static int GetIso8601WeekOfYear(DateTime time) {
			DayOfWeek day = System.Globalization.CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
			if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday) {
				time = time.AddDays(3);
			}

			// Return the week of our adjusted day
			return System.Globalization.CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
		}

		private int GetWeeksInYear(int year) {
			System.Globalization.DateTimeFormatInfo dfi = System.Globalization.DateTimeFormatInfo.CurrentInfo;
			DateTime date1 = new DateTime(year, 12, 31);
			System.Globalization.Calendar cal = dfi.Calendar;
			return cal.GetWeekOfYear(date1, dfi.CalendarWeekRule,
												dfi.FirstDayOfWeek);
		}

		public static DateTime FirstDateOfWeek(int year, int weekOfYear) {
			DateTime jan1 = new DateTime(year, 1, 1);
			int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

			DateTime firstThursday = jan1.AddDays(daysOffset);
			var cal = System.Globalization.CultureInfo.CurrentCulture.Calendar;
			int firstWeek = cal.GetWeekOfYear(firstThursday, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

			var weekNum = weekOfYear;

			if (firstWeek == 1) {
				weekNum -= 1;
			}

			var result = firstThursday.AddDays(weekNum * 7);
			return result.AddDays(-3);
		}

		private Dictionary<string, ExpandoObject> HandleLoadReportData(Dictionary<string, ExpandoObject> loadData,Dictionary<int,double> plannedLoadLeftPerDay, string dateType, DateTime startDate, DateTime endDate, DataTable rawDataPlanned,DataTable rawDataActual,string showPlanned,string showActual,string showLoad,int resourceMode) {
			DateTime dateNow = DateTime.Now;
			List<string> loadIds = new List<string>();
			for (int runIndex = 0; runIndex < 2; runIndex++) {
				int index = 0;
				DataTable rawData = null;
				if (runIndex == 0) {
					rawData = rawDataActual;
				}
				else {
					rawData = rawDataPlanned;
				}
				if (rawData != null && rawData.Rows.Count > 0) {
					switch (dateType) {
						case "0": // Week
								  //startDate.
							int startWeek = GetWeekNumber(startDate);
							int startYear = startDate.Year;
							int endWeek = GetWeekNumber(endDate);
							int endYear = endDate.Year;

							for (int i = 0; i < (endYear - startYear + 1); i++) {
								int currentYear = startYear + i;
								int weeksInCurrentYear = GetWeeksInYear(startYear + i);
								//int currentWeek = startWeek;
								for (int j = 1; j <= weeksInCurrentYear; j++) {
									if ((startYear == currentYear && startWeek <= j || startYear < currentYear) && (endYear == currentYear && endWeek >= j || endYear > currentYear)) {
										DateTime firstDayOfweek = FirstDateOfWeek(currentYear, j);
										DateTime smallerThan = firstDayOfweek.AddDays(7);
										//rawActualData
										double hourSum = 0;
										double loadHourSum = 0;
										List<String> ids = new List<string>();
										List<string> resources = new List<string>();
										List<string> loadResources = new List<string>();
										loadIds = new List<string>();
										//bool hasLoadLeft = false;
										foreach (DataRow row in rawData.AsEnumerable().Where(row => (row.Field<DateTime>("date") >= firstDayOfweek && row.Field<DateTime>("date") < smallerThan))) {
											hourSum += Conv.ToDouble(row["hours"]);
											if (ids.Contains(Conv.ToStr(row["owner_item_id"])) == false) ids.Add(Conv.ToStr(row["owner_item_id"]));

											if (showLoad == "1" && runIndex == 1 && plannedLoadLeftPerDay.ContainsKey(Conv.ToInt(row["task_item_id"])) && new DateTime(dateNow.Year, dateNow.Month, dateNow.Day) <= DateTime.Parse(Conv.ToStr(row["date"]))) {
												loadHourSum += plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])];

												if (resourceMode == 1 && plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])]>0) {
													foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
														if (loadResources.Contains(user) == false) loadResources.Add(user);
													}
												}
												else if (plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])] > 0) {
													if (loadIds.Contains(Conv.ToStr(row["owner_item_id"])) == false) loadIds.Add(Conv.ToStr(row["owner_item_id"]));
												}
											}
											if (resourceMode == 1 && Conv.ToDouble(row["hours"])>0) {
												foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
													if (resources.Contains(user) == false) resources.Add(user);
												}
											}
										}
										if (loadData.ContainsKey(Conv.ToStr(index)) == false) {
											dynamic dynObject = new System.Dynamic.ExpandoObject();
											if (runIndex == 0) {
												dynObject.actualHours = hourSum;
												dynObject.actualIds = ids;
												dynObject.actualResources = resources;												
											}
											else {
												dynObject.plannedHours = hourSum;
												dynObject.plannedIds = ids;
												dynObject.loadHours = loadHourSum;
												dynObject.loadIds = loadIds;
												dynObject.plannedResources = resources;
												dynObject.loadResources = loadResources;
											}
											dynObject.timeLabel = j + "/" + currentYear;
											loadData.Add(Conv.ToStr(index), dynObject);
										}
										else {
											dynamic dynObject = loadData[Conv.ToStr(index)];
											dynObject.plannedHours = hourSum;
											dynObject.plannedIds = ids;
											dynObject.loadHours = loadHourSum;
											dynObject.loadIds = loadIds;
											dynObject.plannedResources = resources;
											dynObject.loadResources = loadResources;
										}
										index += 1;
									}
								}
							}

							break;
						case "1": // Month
							int monthsBetween = ((endDate.Year - startDate.Year) * 12) + endDate.Month - startDate.Month + 1;
							DateTime start = new DateTime(startDate.Year, startDate.Month, 1);
							for (int i = 0; i < monthsBetween; i++) {
								DateTime currentTime = start.AddMonths(i);
								//rawActualData
								double hourSum = 0;
								double loadHourSum = 0;
								List<String> ids = new List<string>();
								List<string> resources = new List<string>();
								List<string> loadResources = new List<string>();
								foreach (DataRow row in rawData.AsEnumerable().Where(row => (row.Field<DateTime>("date") >= currentTime && row.Field<DateTime>("date") < currentTime.AddMonths(1)))) {
									hourSum += Conv.ToDouble(row["hours"]);
									if (ids.Contains(Conv.ToStr(row["owner_item_id"])) == false) ids.Add(Conv.ToStr(row["owner_item_id"]));

									if (showLoad == "1" && runIndex == 1 && plannedLoadLeftPerDay.ContainsKey(Conv.ToInt(row["task_item_id"])) && new DateTime(dateNow.Year, dateNow.Month, dateNow.Day) <= DateTime.Parse(Conv.ToStr(row["date"]))) {
										loadHourSum += plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])];
										
										if (resourceMode == 1 && plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])] > 0) {
											foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
												if (loadResources.Contains(user) == false) loadResources.Add(user);
											}
										}
                                        else if(plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])]>0) {
											if (loadIds.Contains(Conv.ToStr(row["owner_item_id"])) == false) loadIds.Add(Conv.ToStr(row["owner_item_id"]));
										}
									}
									if (resourceMode == 1 && Conv.ToDouble(row["hours"]) > 0) {
										foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
											if (resources.Contains(user) == false) resources.Add(user);
										}
									}
								}
								if (loadData.ContainsKey(Conv.ToStr(index)) == false) {
									dynamic dynObject = new System.Dynamic.ExpandoObject();
									if (runIndex == 0) {
										dynObject.actualHours = hourSum;
										dynObject.actualIds = ids;
										dynObject.actualResources = resources;
									}
									else {
										dynObject.plannedHours = hourSum;
										dynObject.plannedIds = ids;
										dynObject.loadHours = loadHourSum;
										dynObject.loadIds = loadIds;
										dynObject.plannedResources = resources;
										dynObject.loadResources = loadResources;
									}
									dynObject.timeLabel = currentTime.Month + "/" + currentTime.Year;
									loadData.Add(Conv.ToStr(index), dynObject);
								}
								else {
									dynamic dynObject = loadData[Conv.ToStr(index)];
									dynObject.plannedHours = hourSum;
									dynObject.plannedIds = ids;
									dynObject.loadHours = loadHourSum;
									dynObject.loadIds = loadIds;
									dynObject.plannedResources = resources;
									dynObject.loadResources = loadResources;
								}
								index += 1;
							}

							break;
						case "2": // Quarter
							int quartersBetween = GetQuarters(startDate, endDate) + 1;
							DateTime startD = new DateTime(startDate.Year, GetQuarterNumber(startDate.Month)*3-2, 1);

							for (int i = 0; i < quartersBetween; i++) {
								DateTime currentTime = startD.AddMonths(i * 3);
								//rawActualData
								double hourSum = 0;
								double loadHourSum = 0;
								List<String> ids = new List<string>();
								List<string> resources = new List<string>();
								List<string> loadResources = new List<string>();
								foreach (DataRow row in rawData.AsEnumerable().Where(row => (row.Field<DateTime>("date") >= currentTime && row.Field<DateTime>("date") < currentTime.AddMonths(3)))) {
									hourSum += Conv.ToDouble(row["hours"]);
									if (ids.Contains(Conv.ToStr(row["owner_item_id"])) == false) ids.Add(Conv.ToStr(row["owner_item_id"]));

									if (showLoad == "1" && runIndex == 1 && plannedLoadLeftPerDay.ContainsKey(Conv.ToInt(row["task_item_id"])) && new DateTime(dateNow.Year, dateNow.Month, dateNow.Day) <= DateTime.Parse(Conv.ToStr(row["date"]))) {
										loadHourSum += plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])];

										if (resourceMode == 1 && plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])] > 0) {
											foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
												if (loadResources.Contains(user) == false) loadResources.Add(user);
											}
										}
										else if (plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])] > 0) {
											if (loadIds.Contains(Conv.ToStr(row["owner_item_id"])) == false) loadIds.Add(Conv.ToStr(row["owner_item_id"]));
										}
									}
									if (resourceMode == 1 && Conv.ToDouble(row["hours"]) > 0) {
										foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
											if (resources.Contains(user) == false) resources.Add(user);
										}
									}
								}
								if (loadData.ContainsKey(Conv.ToStr(index)) == false) {
									dynamic dynObject = new System.Dynamic.ExpandoObject();
									if (runIndex == 0) {
										dynObject.actualHours = hourSum;
										dynObject.actualIds = ids;
										dynObject.actualResources = resources;
									}
									else {
										dynObject.plannedHours = hourSum;
										dynObject.plannedIds = ids;
										dynObject.loadHours = loadHourSum;
										dynObject.loadIds = loadIds;
										dynObject.plannedResources = resources;
										dynObject.loadResources = loadResources;
									}
									dynObject.timeLabel = GetQuarterNumber(currentTime.Month) + "/" + currentTime.Year;
									loadData.Add(Conv.ToStr(index), dynObject);
								}
								else {
									dynamic dynObject = loadData[Conv.ToStr(index)];
									dynObject.plannedHours = hourSum;
									dynObject.plannedIds = ids;
									dynObject.loadHours = loadHourSum;
									dynObject.loadIds = loadIds;
									dynObject.plannedResources = resources;
									dynObject.loadResources = loadResources;
								}
								index += 1;
							}
							break;
						case "3": // Year

							int years = endDate.Year - startDate.Year + 1;

							DateTime startY = new DateTime(startDate.Year, 1, 1);


							for (int i = 0; i < years; i++) {
								DateTime currentTime = startY.AddYears(i);
								//rawActualData
								double hourSum = 0;
								double loadHourSum = 0;
								List<String> ids = new List<string>();
								List<string> resources = new List<string>();
								List<string> loadResources = new List<string>();
								foreach (DataRow row in rawData.AsEnumerable().Where(row => (row.Field<DateTime>("date") >= currentTime && row.Field<DateTime>("date") < currentTime.AddYears(1)))) {
									hourSum += Conv.ToDouble(row["hours"]);
									if (ids.Contains(Conv.ToStr(row["owner_item_id"])) == false) ids.Add(Conv.ToStr(row["owner_item_id"]));

									if (showLoad == "1" && runIndex == 1 && plannedLoadLeftPerDay.ContainsKey(Conv.ToInt(row["task_item_id"])) && new DateTime(dateNow.Year, dateNow.Month, dateNow.Day) <= DateTime.Parse(Conv.ToStr(row["date"]))) {
										loadHourSum += plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])];

										if (resourceMode == 1 && plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])] > 0) {
											foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
												if (loadResources.Contains(user) == false) loadResources.Add(user);
											}
										}
										else if (plannedLoadLeftPerDay[Conv.ToInt(row["task_item_id"])] > 0) {
											if (loadIds.Contains(Conv.ToStr(row["owner_item_id"])) == false) loadIds.Add(Conv.ToStr(row["owner_item_id"]));
										}
									}
									if (resourceMode == 1 && Conv.ToDouble(row["hours"]) > 0) {
										foreach (string user in Conv.ToStr(row["resource_item_id"]).Split(",")) {
											if (resources.Contains(user) == false) resources.Add(user);
										}
									}
								}
								if (loadData.ContainsKey(Conv.ToStr(index)) == false) {
									dynamic dynObject = new System.Dynamic.ExpandoObject();
									if (runIndex == 0) {
										dynObject.actualHours = hourSum;
										dynObject.actualIds = ids;
										dynObject.actualResources = resources;
									}
									else {
										dynObject.plannedHours = hourSum;
										dynObject.plannedIds = ids;
										dynObject.loadHours = loadHourSum;
										dynObject.loadIds = loadIds;
										dynObject.plannedResources = resources;
										dynObject.loadResources = loadResources;
									}
									dynObject.timeLabel = currentTime.Year;
									loadData.Add(Conv.ToStr(index), dynObject);
								}
								else {
									dynamic dynObject = loadData[Conv.ToStr(index)];
									dynObject.plannedHours = hourSum;
									dynObject.plannedIds = ids;
									dynObject.loadHours = loadHourSum;
									dynObject.loadIds = loadIds;
									dynObject.plannedResources = resources;
									dynObject.loadResources = loadResources;
								}
								index += 1;
							}
							break;
						default:
							// Never comes here
							break;
					}
				}
			}
			return loadData;
		}
		private int GetQuarterNumber(int month) {
			if (month < 4) {
				return 1;
			}
			else if(month < 7) {
				return 2;
			}
			else if(month < 10) {
				return 3;
			}
			else {
				return 4;
			}
		}
		private int GetQuarters(DateTime dt1, DateTime dt2) {
			double d1Quarter = GetQuarterNumber(dt1.Month);
			double d2Quarter = GetQuarterNumber(dt2.Month);
			double d1 = d2Quarter - d1Quarter;
			double d2 = (4 * (dt2.Year - dt1.Year));
			return Conv.ToInt(Math.Ceiling((Double)(d1 + d2)));
		}
		private string ReplaceLast(string find, string replace, string str) {
			int lastIndex = str.LastIndexOf(find);

			if (lastIndex == -1) {
				return str;
			}

			string beginString = str.Substring(0, lastIndex);
			string endString = str.Substring(lastIndex + find.Length);

			return beginString + replace + endString;
		}
	}
}