﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/settings/Lucene")]
	public class LuceneRest : RestBase {
		[HttpGet("{process}/{itemId}/{lowerbound}")]
		public List<int> Get(string process, int itemId, double lowerbound) {
			var ib = new ItemBase(instance, process, currentSession);
			var item = ib.GetItem(itemId);
			
			var lucene = new Lucene(instance, currentSession);
			return lucene.Search(item, process, lowerbound);
		}
	}
}