﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers.Classic;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Util;
using Directory = Lucene.Net.Store.Directory;

namespace Keto5.x.platform.server.processes {
	public class Lucene : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;

		private readonly string indexFileLocation;
		//private Translations translations;

		public Lucene(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
			indexFileLocation = Path.Combine(@"lucene", instance);
		}

		private Dictionary<string, List<string>> GetIndexProcesses() {
			var p = new Processes(instance, authenticatedSession);

			var indexProcesses = new Dictionary<string, List<string>>();

			foreach (var process in p.GetProcesses(0)) {
				var ic = new ItemColumns(instance, process.ProcessName, authenticatedSession);
				var indexCols = new List<string>();

				foreach (var col in ic.GetItemColumns()) {
					if (col.IsDatabasePrimitive() && col.UseLucene) {
						indexCols.Add(col.Name);
					}
				}

				if (indexCols.Count > 0) {
					indexCols.Add("item_id");
					indexProcesses.Add(process.ProcessName, indexCols);
				}
			}

			return indexProcesses;
		}

		public void BuildIndex() {
			var indexProcesses = GetIndexProcesses();

			if (indexProcesses.Keys.Count > 0) {
				var pList = "''";
				foreach (var processName in indexProcesses.Keys) {
					pList += ",'" + processName + "'";
				}

				foreach (var processName in indexProcesses.Keys) {
					SetDBParam(SqlDbType.NVarChar, "@processName", processName);
					var rowCount = Conv.ToInt(db.ExecuteScalar(
						"SELECT COUNT(item_id) FROM items WHERE instance = @instance AND process = @processName ",
						DBParameters));

					Directory dir = new SimpleFSDirectory(Path.Combine(indexFileLocation, processName));
					var indexedCount = CountIndexedRows(processName);
					if (rowCount > 0 && rowCount != indexedCount) {
						Diag.LogToConsole(
							"Process " + processName + "  required lucene index rebuild(rowcounts: " + rowCount +
							", indexedCount).", Diag.LogSeverities.Warning);
						BuildIndexForProcess(processName, indexProcesses[processName]);
					} else {
						var indexReader = DirectoryReader.Open(dir);

						var doc = indexReader.Document(0);
						var fieldList = new List<string>();
						foreach (var f in doc.Fields) {
							fieldList.Add(f.Name);
						}

						var rebuild = fieldList.Count != indexProcesses[processName].Count;
						if (!rebuild) {
							foreach (var f in indexProcesses[processName]) {
								if (!fieldList.Contains(f)) {
									rebuild = true;
								}
							}
						}

						if (rebuild) {
							Diag.LogToConsole(
								"Process " + processName +
								"  required lucene index rebuild(columns to index are changed).",
								Diag.LogSeverities.Warning);
							BuildIndexForProcess(processName, indexProcesses[processName]);
						} else {
							Diag.LogToConsole("Process " + processName + " did not require lucene index rebuild.",
								Diag.LogSeverities.Message);
						}
					}
				}
			}
		}

		public void BuildIndexForProcess(string processName, List<string> fields) {
			Directory dir = new SimpleFSDirectory(Path.Combine(indexFileLocation, processName));

			var analyzer = new StandardAnalyzer(LuceneVersion.LUCENE_48);
			//create the index writer with the directory and analyzer defined.
			var indexWriter = new
				IndexWriter(dir, new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer));

			indexWriter.DeleteAll();

			var ib = new ItemBase(instance, processName, authenticatedSession);

			foreach (DataRow row in ib.GetAllData().Rows) {
				//create a document, add in a single field
				var doc = new
					Document();

				foreach (var f in fields) {
					if (row.Table.Columns.Contains(f)) {
						if (f == "item_id") {
							var fldContent =
								//new Field(f,Conv.toStr(row[f]),Field.Store.YES, Field.Index.NOT_ANALYZED);
								new StringField(f, Conv.ToStr(row[f]), Field.Store.YES);
							doc.Add(fldContent);
						} else {
							var fldContent =
								//new Field(f,Conv.toStr(row[f]),Field.Store.YES, Field.Index.ANALYZED);
							new StringField(f, Conv.ToStr(row[f]), Field.Store.YES);

							doc.Add(fldContent);
						}
					}
				}

				//write the document to the index
				indexWriter.AddDocument(doc);
			}

			indexWriter.Flush(true, true);
			indexWriter.Dispose();
		}

		public int CountIndexedRows(string processName) {
			Directory dir = new SimpleFSDirectory(Path.Combine(indexFileLocation, processName));
			if (DirectoryReader.IndexExists(dir)) {
				var indexReader = DirectoryReader.Open(dir);
				return indexReader.NumDocs;
			}

			return 0;
		}


		public List<int> Search(Dictionary<string, object> data, string process, double lowerbound, int maxCount = 10) {
			Directory dir = new SimpleFSDirectory(Path.Combine(indexFileLocation, process));

			if (!DirectoryReader.IndexExists(dir)) {
				throw new CustomArgumentException("INDEX_FILE_NOT_FOUND");
			}

			var retval = new List<int>();
			var indexReader = DirectoryReader.Open(dir);
			var searcher = new IndexSearcher(indexReader);

			var indexProcesses = GetIndexProcesses();

			var parser = new MultiFieldQueryParser(LuceneVersion.LUCENE_48, indexProcesses[process].ToArray(),
				new StandardAnalyzer(LuceneVersion.LUCENE_48));

			var q = new BooleanQuery();
			foreach (var col in indexProcesses[process]) {
				if (data.ContainsKey(col) && col != "item_id") {
					foreach (var word in Conv.ToStr(data[col]).Split(" ")) {
						q.Add(new BooleanClause(new TermQuery(new Term(col, word.ToLower())), Occur.SHOULD));
					}
				}
			}

			var results = searcher.Search(q, maxCount);

			if (results.TotalHits > 0) {
				var hits = results.TotalHits;
				if (results.TotalHits > maxCount) {
					hits = maxCount;
				}

				for (var i = 0; i < hits; i++) {
					var d = results.ScoreDocs[i];
					if (d.Score > lowerbound) {
						var doc = indexReader.Document(d.Doc);
						var f = doc.GetField("item_id");
						if (Conv.ToInt(data["item_id"]) != Conv.ToInt(f.GetStringValue())) {
							retval.Add(Conv.ToInt(f.GetStringValue()));
						}
					}
				}
			}

			return retval;
		}

		public void UpdateIndex(string process, int itemId, Dictionary<string, object> data) {
			var indexProcesses = GetIndexProcesses();
			if (indexProcesses.ContainsKey(process) == false) return;

			var analyzer = new StandardAnalyzer(LuceneVersion.LUCENE_48);
			//create the index writer with the directory and analyzer defined.    
			Directory dir = new SimpleFSDirectory(Path.Combine(indexFileLocation, process));

			var indexWriter = new
				IndexWriter(dir, new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer));

			var q = new BooleanQuery();
			indexWriter.DeleteDocuments(new Term("item_id", Conv.ToStr(itemId)));

			var doc = new
				Document();

			foreach (var f in indexProcesses[process]) {
				if (data.ContainsKey(f)) {
					if (f == "item_id") {
						var fldContent =
							//new Field(f,Conv.toStr(data[f]),Field.Store.YES, Field.Index.NOT_ANALYZED);
						new StringField(f, Conv.ToStr(data[f]), Field.Store.YES);
						doc.Add(fldContent);
					} else {
						var fldContent =
							//new Field(f,Conv.toStr(data[f]),Field.Store.NO, Field.Index.ANALYZED);
						new StringField(f, Conv.ToStr(data[f]), Field.Store.NO);
						doc.Add(fldContent);
					}
				}
			}

			indexWriter.AddDocument(doc);
			indexWriter.Flush(true, true);
			indexWriter.Dispose();
		}


		public void DeleteIndex(string process, int itemId) {
			var indexProcesses = GetIndexProcesses();
			if (indexProcesses.ContainsKey(process) == false) return;

			var analyzer = new StandardAnalyzer(LuceneVersion.LUCENE_48);
			//create the index writer with the directory and analyzer defined.    
			Directory dir = new SimpleFSDirectory(Path.Combine(indexFileLocation, process));

			var indexWriter = new
				IndexWriter(dir, new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer));

			var q = new BooleanQuery();
			indexWriter.DeleteDocuments(new Term("item_id", Conv.ToStr(itemId)));
			indexWriter.Flush(true, true);
			indexWriter.Dispose();
		}
	}
}