﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.httpResponseParsers {
	[Route("v1/settings/[controller]")]
	public class RegisteredHttpResponseParsers : RestBase {
		private static readonly Dictionary<string, HttpParserBase> parsers = new Dictionary<string, HttpParserBase>();

		[HttpGet("")]
		public List<string> GetHttpParsers() {
			var p = new List<string>();
			foreach (var value in parsers.Keys) {
				p.Add(value);
			}

			return p;
		}

		public static void AddType(HttpParserBase obj) {
			parsers.Add(obj.Name, obj);
		}

		public static HttpParserBase GetParser(string parser) {
			if (parsers.ContainsKey(parser)) {
				return parsers[parser];
			}

			return null;
		}
	}

	public abstract class HttpParserBase {
		public abstract string Name { get; }

		public string instance { get; set; }
		public AuthenticatedSession session { get; set; }

		public abstract void ParseResponse(int processActionRowId, int itemId, string requestType, string statusCode,
			string url, string headers, string body, string response, int instanceHttpRequestId);
	}

	public class HttpResponseParserAzureDevops : HttpParserBase {
		public override string Name { get; } = "AzureDevopsParser";

		public override void ParseResponse(int processActionRowId, int itemId, string requestType, string statusCode,
			string url, string headers, string body, string response, int instanceHttpRequestId) {
			Console.WriteLine("PARSE");
			Console.WriteLine("-----------");
			Console.WriteLine(url);
			Console.WriteLine(headers);
			Console.WriteLine(body);
			Console.WriteLine(response);
			Console.WriteLine(instanceHttpRequestId);
			Console.WriteLine("-----------");
		}
	}

	public class HttpResponseParserBitBucket : HttpParserBase {
		public override string Name { get; } = "BicBucketParser";

		public override void ParseResponse(int processActionRowId, int itemId, string requestType, string statusCode,
			string url, string headers, string body, string response, int instanceHttpRequestId) {
			var reposIb = new ItemBase(instance, "bitbucket_repositories", session);
			var repos = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(response);

			foreach (var repo in repos) {
				var repoId = reposIb.InsertItemRow();
				var repoItem = reposIb.GetItem(repoId);
				repoItem["owner_item_id"] = itemId;
				repoItem["title"] = repo["name"];
				reposIb.SaveItem(repoItem);
			}
		}
	}
}