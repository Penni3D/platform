﻿using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/settings/DuplicateVariable")]
	public class DuplicateVariableRest : RestBase {
		[HttpGet("{type}/{variable}/{process}")]
		public string Get(string type, string variable, string process) {
			var DuplicateVariableCheck = new DuplicateVariableCheck(instance, process, currentSession);
			return DuplicateVariableCheck.CheckVariable(type, variable, process);
		}
	}

}