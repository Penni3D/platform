﻿using Keto5.x.platform.server.@base;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryListRisksConfig : SubQueryConfigBase
	{
		public SubqueryListRisksConfig() { 
			Config.Name = "LISTRISKS"; 
			Config.Value = "LISTRISKS"; 
			Config.TranslationKey = "LISTRISKS"; 
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) {
				return;
			}
			AddFieldConfig("@value",new SubQueryConfigValue("@value", "int", SubQueryOptionType.Int, "value", "value", 18));
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT STUFF((SELECT ', ' + description FROM _"+instance+"_risks WHERE risk_value > @value AND owner_item_id = pf.item_id FOR XML PATH('')),1,1,''))";
		}

		public override string GetArchiveSql(string instance) {
			return "(SELECT STUFF((SELECT ', ' + description FROM get__"+instance+"_risks(@archiveDate) WHERE risk_value > @value AND owner_item_id = pf.item_id FOR XML PATH('')),1,1,''))";
		}
	}
}