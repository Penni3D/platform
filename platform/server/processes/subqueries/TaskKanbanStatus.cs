﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryTaskKanbanStatusConfig : SubQueryConfigBase {
		public SubqueryTaskKanbanStatusConfig() {
			Config.Name = "TaskKanbanStatus";
			Config.Value = "TaskKanbanStatus";
			Config.TranslationKey = "PHASE_TYPE_SQ";
			Config.DataType = ItemColumn.ColumnType.List;
			this.SubProcess = "list_task_status";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var db = new Connections(authenticatedSession);

			int default_sprint_item_status_id = (int)db.ExecuteScalar("SELECT TOP 1 item_id FROM _" + instance + "_list_task_status WHERE kanban_status = 0");

			var process_phase_value_from_subrows = 0;
            var approachingAndLateInUse = 0;
            var numberOfApproachingDays = 0;


            if ((SubQueryConfigValue)FieldConfig["@PhaseStatusFromSubRows"] != null && ((SubQueryConfigValue)FieldConfig["@PhaseStatusFromSubRows"]).Value != null && (int)((SubQueryConfigValue)FieldConfig["@PhaseStatusFromSubRows"]).Value > 0)
				process_phase_value_from_subrows = (int)((SubQueryConfigValue)FieldConfig["@PhaseStatusFromSubRows"]).Value;
            if ((SubQueryConfigValue)FieldConfig["@ApproachingAndLateInUse"] != null && ((SubQueryConfigValue)FieldConfig["@ApproachingAndLateInUse"]).Value != null && (int)((SubQueryConfigValue)FieldConfig["@ApproachingAndLateInUse"]).Value > 0)
                approachingAndLateInUse = (int)((SubQueryConfigValue)FieldConfig["@ApproachingAndLateInUse"]).Value;
            if ((SubQueryConfigValue)FieldConfig["@ApproachingDays"] != null && ((SubQueryConfigValue)FieldConfig["@ApproachingDays"]).Value != null)
                numberOfApproachingDays = (int)((SubQueryConfigValue)FieldConfig["@ApproachingDays"]).Value;

            //var process_task_status_column_id = "sq_SubqueryTaskKanbanStatusConfig_incorrect_configuration: " + process;
            //if ((SubQueryConfigValue)FieldConfig["@PhaseNPhasetaskStatusListColumn"] != null && ((SubQueryConfigValue)FieldConfig["@PhaseNPhasetaskStatusListColumn"]).Value != null && (int)((SubQueryConfigValue)FieldConfig["@PhaseNPhasetaskStatusListColumn"]).Value > 0)
            //	process_task_status_column_id = ((int)((SubQueryConfigValue)FieldConfig["@PhaseNPhasetaskStatusListColumn"]).Value).ToString();
            
            int process_task_status_column_id = (int)db.ExecuteScalar("SELECT ic.item_column_id icid FROM item_columns ic WHERE ic.instance = '" + Conv.ToSql(instance) + "' AND ic.process = '" + Conv.ToSql(process) + "' AND ic.data_additional = 'list_task_status'");

            int addEnds = 0;

            var sql = "(";

			// 0 = sprintin alaisen tehtävän kortti
			sql += "CASE WHEN pf.task_type = 0 THEN ";
			sql += "( ";
			sql += "ISNULL(";
			sql += "(SELECT status_list_item_id FROM task_status WHERE status_id = pf.status_id)"; // get_task_status(GETUTCDATE())
			sql += "," + default_sprint_item_status_id + ") ";
			sql += ") ";

            // 1 = sprintin alainen tehtävä, ylempi taso on type 21  
            if (approachingAndLateInUse > 0){
                addEnds += 1;
                //Sprint task late
                sql += " WHEN pf.task_type = 1 AND (SELECT sprint.item_id FROM _" + instance + "_task sprint WHERE sprint.item_id = pf.parent_item_id AND sprint.task_type = 21 AND Convert(date, getdate()) > sprint.end_date ) > 0 AND (SELECT TOP 1 ts.status_list_item_id FROM _" + instance + "_task crd LEFT JOIN task_status ts ON ts.status_id = crd.status_id LEFT JOIN _" + instance + "_list_task_status lts ON lts.item_id = ts.status_list_item_id WHERE crd.task_type = 0 AND crd.parent_item_id = pf.item_id ORDER BY lts.kanban_status ASC)=(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) THEN "; // get__keto_task(GETUTCDATE())
                sql += "( ";
                sql += "ISNULL( ";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinTaskStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=4)";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ") ";
                sql += ") ";
            }

            //Sprint time approaching
            if (approachingAndLateInUse > 0 && numberOfApproachingDays > 0) {
                addEnds += 1;
                sql += " WHEN pf.task_type = 1 AND (SELECT sprint.item_id FROM _" + instance + "_task sprint WHERE sprint.item_id = pf.parent_item_id AND sprint.task_type = 21 AND getDate() BETWEEN DATEADD(day, " + (-numberOfApproachingDays + 1) + ", sprint.end_date) AND DATEADD(day, 1, Convert(date, sprint.end_date))) > 0 AND (SELECT TOP 1 ts.status_list_item_id FROM _" + instance + "_task crd LEFT JOIN task_status ts ON ts.status_id = crd.status_id LEFT JOIN _" + instance + "_list_task_status lts ON lts.item_id = ts.status_list_item_id WHERE crd.task_type = 0 AND crd.parent_item_id = pf.item_id ORDER BY lts.kanban_status ASC)=(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) THEN "; // get__keto_task(GETUTCDATE())
                sql += "( ";
                sql += "ISNULL( ";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinTaskStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=3)";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ") ";
                sql += ") ";
            }

            //All good situation
            sql += " WHEN pf.task_type = 1 AND (SELECT sprint.item_id FROM _" + instance + "_task sprint WHERE sprint.item_id = pf.parent_item_id AND sprint.task_type = 21) > 0 THEN "; // get__keto_task(GETUTCDATE())
			sql += "( ";
            sql += "ISNULL( ";
			sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinTaskStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
			sql += "   SELECT TOP 1 ts.status_list_item_id ";
			sql += "     FROM _" + instance + "_task crd ";  // get__keto_task(GETUTCDATE())
			sql += "     LEFT JOIN task_status ts ON ts.status_id = crd.status_id "; // get_task_status(GETUTCDATE())
			sql += "     LEFT JOIN _" + instance + "_list_task_status lts ON lts.item_id = ts.status_list_item_id "; // get__keto_list_task_status(GETUTCDATE())
			sql += "   WHERE crd.task_type = 0 AND crd.parent_item_id = pf.item_id ";
			sql += "   ORDER BY lts.kanban_status ASC ";
			sql += ") ";
			sql += "," + default_sprint_item_status_id + ") ";
			sql += ") ";
            // 1 = sprintin alainen tehtävä päättyy

            if (approachingAndLateInUse > 0) {
                addEnds += 2;
                 //spintin päätaso "myöhässä"
                // 21 = sprint
                sql += " WHEN pf.task_type = 21 AND Convert(date, getdate()) > pf.end_date AND (SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1)=";
                sql += "ISNULL(";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinSprintStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "   SELECT MAX(ts.status_list_item_id) ";
                sql += "     FROM _" + instance + "_task crd";
                sql += "     LEFT JOIN task_status ts ON ts.status_id = crd.status_id ";
                sql += "     LEFT JOIN _" + instance + "_list_task_status lts ON lts.item_id = ts.status_list_item_id ";
                sql += "   WHERE crd.task_type = 0 AND crd.parent_item_id IN (SELECT tsk.item_id FROM _" + instance + "_task tsk WHERE tsk.parent_item_id = pf.item_id AND tsk.task_type = 1) AND lts.kanban_status<2 ";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ")";
                sql += "THEN ";
                sql += " ( ";
                sql += "ISNULL( ";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinTaskStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=4)";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ") ";
                sql += ") ";

                //spintin päätaso "lähestyy"

                sql += " WHEN pf.task_type = 21 AND getDate() BETWEEN DATEADD(day, " + (-numberOfApproachingDays + 1) + ", Convert(date, pf.end_date)) AND DATEADD(day, 1, pf.end_date) AND (SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=0)!=";
                sql += "ISNULL(";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinSprintStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "   SELECT MIN(ts.status_list_item_id) ";
                sql += "     FROM _" + instance + "_task crd";
                sql += "     LEFT JOIN task_status ts ON ts.status_id = crd.status_id ";
                sql += "     LEFT JOIN _" + instance + "_list_task_status lts ON lts.item_id = ts.status_list_item_id ";
                sql += "   WHERE crd.task_type = 0 AND crd.parent_item_id IN (SELECT tsk.item_id FROM _" + instance + "_task tsk WHERE tsk.parent_item_id = pf.item_id AND tsk.task_type = 1)";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ") AND (SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1)=";         
                sql += "ISNULL(";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinSprintStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "   SELECT MAX(ts.status_list_item_id) ";
                sql += "     FROM _" + instance + "_task crd";
                sql += "     LEFT JOIN task_status ts ON ts.status_id = crd.status_id ";
                sql += "     LEFT JOIN _" + instance + "_list_task_status lts ON lts.item_id = ts.status_list_item_id ";
                sql += "   WHERE crd.task_type = 0 AND crd.parent_item_id IN (SELECT tsk.item_id FROM _" + instance + "_task tsk WHERE tsk.parent_item_id = pf.item_id AND tsk.task_type = 1) AND lts.kanban_status<2 ";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ")";
                sql += "THEN ";
                sql += " ( ";
                sql += "ISNULL( ";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinTaskStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=3)";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ") ";
                sql += ") ";
            }
                // 21 = sprint
                sql += " WHEN pf.task_type = 21 THEN ";
                sql += "( ";
                sql += "ISNULL(";
                sql += "( "; // voisi varmaankin käyttää _KanbanFunction_MinSprintStatusByCards, tulisi kyllä performance hit kun käyttää getutcdate-tauluja
                sql += "   SELECT TOP 1 ts.status_list_item_id ";
                sql += "     FROM _" + instance + "_task crd";
                sql += "     LEFT JOIN task_status ts ON ts.status_id = crd.status_id ";
                sql += "     LEFT JOIN _" + instance + "_list_task_status lts ON lts.item_id = ts.status_list_item_id ";
                sql += "   WHERE crd.task_type = 0 AND crd.parent_item_id IN (SELECT tsk.item_id FROM _" + instance + "_task tsk WHERE tsk.parent_item_id = pf.item_id AND tsk.task_type = 1) ";
                sql += "   ORDER BY lts.kanban_status ASC ";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ") ";
                sql += ") ";

            // 2 = vaihe // markus: täytyy voida olla null
            if (process_phase_value_from_subrows == 1) {
                if (approachingAndLateInUse > 0) {
                    //Late
                    sql += " WHEN pf.task_type = 2 AND (SELECT MAX(item_data_process_selections.selected_item_id) FROM _" + instance + "_task tsk LEFT JOIN item_data_process_selections ON item_data_process_selections.item_id= tsk.item_id WHERE parent_item_id=pf.item_id AND item_data_process_selections.item_column_id=" + process_task_status_column_id + " AND Convert(date, getdate()) > tsk.end_date)=(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) THEN ";
                    sql += "( ";
                    sql += "ISNULL( ";
                    sql += "( "; // 
                    sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=4)";
                    sql += ") ";
                    sql += "," + default_sprint_item_status_id + ") ";
                    sql += ") ";
                    if (numberOfApproachingDays > 0) {
                        //Approaching
                        sql += " WHEN pf.task_type = 2 AND (SELECT MAX(item_data_process_selections.selected_item_id) FROM _" + instance + "_task tsk LEFT JOIN item_data_process_selections ON item_data_process_selections.item_id= tsk.item_id WHERE parent_item_id=pf.item_id AND item_data_process_selections.item_column_id=" + process_task_status_column_id + " AND getDate() BETWEEN DATEADD(day, " + (-numberOfApproachingDays + 1) + ", Convert(date, tsk.end_date)) AND DATEADD(day, 1, tsk.end_date))=(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) AND (SELECT [dbo].[f_" + instance + "_KanbanFunction_MinPhaseStatus] (pf.item_id,GETUTCDATE()," + process_task_status_column_id + "," + default_sprint_item_status_id + ")) !=(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=0) THEN ";
                        sql += "( ";
                        sql += "ISNULL( ";
                        sql += "( "; // 
                        sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=3)";
                        sql += ") ";
                        sql += "," + default_sprint_item_status_id + ") ";
                        sql += ") ";
                    }
                }
                // tutkitaan vaiheen alla olevista riveistä pienin status -Else
                sql += " WHEN pf.task_type = 2 THEN ";
                sql += " (SELECT [dbo].[f_" + instance + "_KanbanFunction_MinPhaseStatus] (pf.item_id,GETUTCDATE()," + process_task_status_column_id + "," + default_sprint_item_status_id + ")) ";
			} else {
                // vaiheella on oma statuksensa (täytyy voida olla null)
                if (approachingAndLateInUse > 0) {
                    //Late
                    sql += " WHEN pf.task_type = 2 AND Convert(date, getdate()) > pf.end_date AND (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " + process_task_status_column_id + ") = (SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) THEN";
                    sql += "( ";
                    sql += "ISNULL( ";
                    sql += "( "; // 
                    sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=4)";
                    sql += ") ";
                    sql += "," + default_sprint_item_status_id + ") ";
                    sql += ") ";

                    //Approaching
                    if (numberOfApproachingDays > 0) {
                        sql += " WHEN pf.task_type = 2 AND getDate() BETWEEN DATEADD(day, " + (-numberOfApproachingDays + 1) + ", Convert(date, pf.end_date)) AND DATEADD(day, 1, pf.end_date) AND (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " + process_task_status_column_id + ") = (SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) THEN"; 
                        sql += "( ";
                        sql += "ISNULL( ";
                        sql += "( "; // 
                        sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=3)";
                        sql += ") ";
                        sql += "," + default_sprint_item_status_id + ") ";
                        sql += ") ";
                    }
                }

                sql += " WHEN pf.task_type = 2 THEN ";
                sql += " (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " + process_task_status_column_id + ") "; //get_item_data_process_selections(GETUTCDATE())
			}

            // 1 = vaiheen alainen tehtävä, ylempi taso on type 2 // markus: ei saa olla null vaan pienin
            if (approachingAndLateInUse > 0) {
                //Late
                sql += " WHEN pf.task_type = 1 AND Convert(date, getdate()) > pf.end_date AND (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " + process_task_status_column_id + ") = (SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) THEN"; 
                sql += "( ";
                sql += "ISNULL( ";
                sql += "( "; // 
                sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=4)";
                sql += ") ";
                sql += "," + default_sprint_item_status_id + ") ";
                sql += ") ";

                //Approaching
                if (numberOfApproachingDays > 0) {
                    sql += " WHEN pf.task_type = 1 AND getDate() BETWEEN DATEADD(day, " + (-numberOfApproachingDays + 1) + ", Convert(date, pf.end_date)) AND DATEADD(day, 1, pf.end_date) AND (SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " + process_task_status_column_id + ") = (SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=1) THEN";
                    sql += "( ";
                    sql += "ISNULL( ";
                    sql += "( "; // 
                    sql += "(SELECT item_id FROM _" + instance + "_list_task_status WHERE kanban_status=3)";
                    sql += ") ";
                    sql += "," + default_sprint_item_status_id + ") ";
                    sql += ") ";
                }
            }
            //Else
            sql += " WHEN pf.task_type = 1 AND (SELECT phase.item_id FROM _" + instance + "_task phase WHERE phase.item_id = pf.parent_item_id AND phase.task_type = 2) > 0 THEN ";
			sql += "( ";
			sql += "ISNULL(";
			sql += "( ";
			sql += " SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " + process_task_status_column_id + " "; //get_item_data_process_selections(GETUTCDATE())
			sql += ") ";
			sql += "," + default_sprint_item_status_id + ") ";
			sql += ") ";

			// 1 = päätason tehtävä // markus: ei saa olla null vaan pienin
			sql += " WHEN pf.task_type = 1 AND (SELECT no_parent.item_id FROM _" + instance + "_task no_parent WHERE no_parent.item_id = pf.parent_item_id) IS NULL THEN ";
			sql += "( ";
			sql += "ISNULL(";
			sql += "( ";
			sql += " SELECT idps.selected_item_id FROM item_data_process_selections idps WHERE idps.item_id = pf.item_id AND idps.item_column_id = " + process_task_status_column_id + " "; //get_item_data_process_selections(GETUTCDATE())
			sql += ") ";
			sql += "," + default_sprint_item_status_id + ") ";
			sql += ") ";

			sql += "ELSE ";
			sql += "  NULL ";
			sql += "END";
			sql += ")";
            
			return "(" + sql + ")"; //ISNULL(" + sql + ",(SELECT TOP 1 item_id FROM _" + instance + "_list_task_status ORDER BY order_no ASC))
		}
		
		public override List<Dictionary<string, object>> GetValues(string instance, int itemColumnId) {
			var db = new Connections(authenticatedSession);
			return db.GetDatatableDictionary("SELECT * FROM _" + instance + "_list_task_status");
		}

		public override string GetArchiveSql(string instance) {
			return "(SELECT status_list_item_id FROM task_status WHERE status_id = pf.status_id)";
		}

		public override string GetFilterSql(string instance) {
			return GetSql(instance) + " IN ( ##idString## ) ";
		}
		
		protected override void InitializeFieldConfig() {
			var db = new Connections(authenticatedSession);

			//AddFieldConfig("@PhaseNPhasetaskStatusListColumn",
			//	new SubQueryConfigValue("@PhaseNPhasetaskStatusListColumn", "int", SubQueryOptionType.List, "item_column_id", "result",
			//				db.GetDatatableDictionary(
			//					"SELECT item_column_id, COALESCE(translation, ic.name) + ' ('+ic.process+')' as result " +
			//					" FROM item_columns ic LEFT JOIN process_translations pt ON ic.variable = pt.variable and code = '" + authenticatedSession.locale_id + "' " +
			//					" WHERE ic.process = '" + process + "' AND data_type IN (6) "), true)
			//);

			AddFieldConfig("@PhaseStatusFromSubRows", new SubQueryConfigValue("@PhaseStatusFromSubRows", "int", SubQueryOptionType.List, "Value", "Name", new List<object> {
				new Dictionary<string, object> {{"Value", 0}, {"Name", "No"}},
				new Dictionary<string, object> {{"Value", 1}, { "Name", "Yes" }}
			}));
            AddFieldConfig("@ApproachingAndLateInUse", new SubQueryConfigValue("@ApproachingAndLateInUse", "int", SubQueryOptionType.List, "Value", "Name", new List<object> {
                new Dictionary<string, object> {{"Value", 0}, {"Name", "No"}},
                new Dictionary<string, object> {{"Value", 1}, { "Name", "Yes" }}
            }));
            AddFieldConfig("@ApproachingDays",
            new SubQueryConfigValue("@ApproachingDays", "int", SubQueryOptionType.Int, "ItemColumnId", "Variable")
            );


            return;
		}
	}
}