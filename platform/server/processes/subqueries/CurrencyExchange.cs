using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class CurrencyExchange : SubQueryConfigBase {
		public CurrencyExchange() {
			Config.Name = "CurrencyExchange";
			Config.Value = "CurrencyExchange";
			Config.TranslationKey = "CURRENCY_EXCHANGE";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var ItemColumns = new ItemColumns(instance, process, authenticatedSession);
			var sqColumn = ItemColumns.GetItemColumn(attachedItemColumnId);
			string[] subs = sqColumn.DataAdditional2.Split("\"");

			string col1 = "";
			string col2 = "";
			int index = 0;

			foreach (var sub in subs) {
				if (sub == "@DateColumnName") {
					col1 = subs[index + 2];
				}

				if (sub == "@CurrencyValue") {
					col2 = subs[index + 2];
				}

				index++;
			}

			return
				"(SELECT dbo.app_convert_currency (" + col1 + "," + col2 +
				",(SELECT currency_id FROM instance_currencies ic " +
				"INNER JOIN _" + instance +
				"_list_system_currencies kc ON kc.list_item = ic.abbr WHERE kc.item_id = (SELECT selected_item_id from " +
				"item_data_process_selections where item_id = pf.item_id AND item_column_id = @CurrencySelectionColumnId) " +
				"),(SELECT currency_id FROM instance_currencies WHERE abbr = (SELECT list_item FROM _" + instance +
				"_list_system_currencies WHERE item_id = @ToCurrencyItemId)),'" + instance + "') " +
				"	as converted from _" + instance + "_" + process + " where item_id = pf.item_id)";
			//
			//
			// return "(SELECT ISNULL((SELECT " + col2 + " FROM _" + instance + "_" + process + " WHERE item_id = pf.item_id) * ( " +
			//        "CASE WHEN (SELECT COUNT (*) FROM instance_currencies_values WHERE date <= (SELECT " + col1 + " FROM _" + instance + "_" + process + " WHERE item_id = pf.item_id) AND currency_id = 1) > 1 " +
			//        "THEN (SELECT exchange_rate FROM instance_currencies_values WHERE currency_id = (SELECT ic.currency_id FROM item_data_process_selections idps " +
			//        "INNER JOIN _" + instance + "_list_system_currencies sc ON idps.selected_item_id = sc.item_id  " +
			//        "INNER JOIN instance_currencies ic ON ic.abbr = sc.list_item " +
			//        "WHERE item_column_id = 1832 AND idps.item_id = pf.item_id AND current_default = 1)) " +
			//        "ELSE " +
			//        "	(SELECT TOP 1 exchange_rate FROM instance_currencies_values " +
			//        "where currency_id = (SELECT ic.currency_id FROM item_data_process_selections idps " +
			//        "INNER JOIN _" + instance + "_list_system_currencies sc ON idps.selected_item_id = sc.item_id " +
			//        "INNER JOIN instance_currencies ic ON ic.abbr = sc.list_item " +
			//        "WHERE item_column_id = 1832 AND idps.item_id = pf.item_id AND date <= (SELECT " + col1 + " FROM _" + instance + "_" + process + " WHERE item_id = pf.item_id )) " +
			//        "order by date desc ) END), " +
			//        "(SELECT TOP 1 exchange_rate FROM instance_currencies_values " +
			//        "where currency_id = (SELECT ic.currency_id FROM item_data_process_selections idps " +
			//        "INNER JOIN _" + instance + "_list_system_currencies sc ON idps.selected_item_id = sc.item_id  " +
			//        "INNER JOIN instance_currencies ic ON ic.abbr = sc.list_item " +
			//        "WHERE item_column_id = 1832 AND idps.item_id = pf.item_id)) " +
			//        "	) iq)";
			//
			//
			//
			//
			//
			// return "(SELECT (SELECT " + col2 + " from _" + instance + "_" + process + " WHERE item_id = pf.item_id) * ( " +
			// "SELECT TOP 1 exchange_rate FROM instance_currencies_values " +
			// "where currency_id = (SELECT ic.currency_id FROM item_data_process_selections idps " +
			// "INNER JOIN _" + instance + "_list_system_currencies sc ON idps.selected_item_id = sc.item_id  " +
			// "INNER JOIN instance_currencies ic ON ic.abbr = sc.list_item " +
			// "WHERE item_column_id = @CurrencySelectionColumnId " +
			// "AND idps.item_id = pf.item_id) " +
			// "AND date <= (SELECT " + col1 + " FROM _" + instance + "_" + process + " WHERE item_id = pf.item_id ) " +
			// " order by date desc ) iq)";
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0)
				return;
			var db = new Connections(authenticatedSession);
			var spSql =
				"SELECT item_column_id, name, ic.process + ' : ' + coalesce(translation, ic.name) AS result FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
				authenticatedSession.locale_id + "' WHERE ic.process = '" + Conv.ToSql(process) +
				"'  AND (data_type =3 OR data_type = 13)  ";

			var spSql2 =
				"SELECT item_column_id, name, ic.process + ' : ' + coalesce(translation, ic.name) AS result FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
				authenticatedSession.locale_id + "' WHERE ic.process = '" + Conv.ToSql(process) +
				"'  AND (data_type =6 AND data_additional = 'list_system_currencies')  ";

			var spSql3 =
				"SELECT item_column_id, name, ic.process + ' : ' + coalesce(translation, ic.name) AS result FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
				authenticatedSession.locale_id + "' WHERE ic.process = '" + Conv.ToSql(process) +
				"'  AND (data_type =1 OR data_type =2)  ";


			var spSql4 = "SELECT list_item, item_id FROM _" + instance + "_list_system_currencies";

			AddFieldConfig("@DateColumnName",
				new SubQueryConfigValue("@DateColumnName", "string", SubQueryOptionType.List, "name", "result",
					db.GetDatatableDictionary(spSql),
					true));

			AddFieldConfig("@CurrencySelectionColumnId",
				new SubQueryConfigValue("@CurrencySelectionColumnId", "int", SubQueryOptionType.List, "item_column_id",
					"result",
					db.GetDatatableDictionary(spSql2),
					true));

			AddFieldConfig("@CurrencyValue",
				new SubQueryConfigValue("@CurrencyValue", "string", SubQueryOptionType.List, "name", "result",
					db.GetDatatableDictionary(spSql3),
					true));

			AddFieldConfig("@ToCurrencyItemId",
				new SubQueryConfigValue("@ToCurrencyItemId", "int", SubQueryOptionType.List, "item_id", "list_item",
					db.GetDatatableDictionary(spSql4),
					true));
		}
	}
}