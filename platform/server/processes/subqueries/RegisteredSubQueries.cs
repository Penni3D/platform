﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.subqueries {
	[Route("v1/settings/[controller]")]
	public class RegisteredSubQueries : RestBase {
		// Configuration and data services for single charts

		private static readonly Dictionary<string, SubQueryConfigBase> queries =
			new Dictionary<string, SubQueryConfigBase>();

		private static readonly Dictionary<int, SubQueryConfigBase> queriesById =
			new Dictionary<int, SubQueryConfigBase>();

		[HttpGet("queryTypes")]
		public List<SubQueryConfig> GetChartTypes() {
			var configs = new List<SubQueryConfig>();
			foreach (var value in queries.Values) {
				configs.Add(value.GetConfig());
			}

			return configs;
		}

		[HttpGet("{queryType}")]
		public Dictionary<string, object> GetConfig(string queryType) {
			if (!queries.ContainsKey(queryType)) return null;
			var config = queries[queryType];
			return config.GetFieldConfigs();
		}

		public class SubQueryTestResult {
			public string ColumnName { get; set; }
			public string ProcessName { get; set; }
			public double TimeMs { get; set; }
			public bool Suspicious { get; set; }
			public bool Error { get; set; }
		}

		[HttpGet("testall")]
		public List<SubQueryTestResult> TestAll() {
			var result = new List<SubQueryTestResult>();

			var p = new Processes(instance, currentSession);
			foreach (var po in p.GetProcesses()) {
				var ics = new ItemColumns(instance, po.ProcessName, currentSession);
				foreach (var ic in ics.GetItemColumns()) {
					if (!ic.IsSubquery()) continue;
					var r = new SubQueryTestResult
						{ProcessName = po.Variable, ColumnName = ic.Variable, Suspicious = true};

					var time = DateTime.Now;
					try {
						RemoveType(ic.ItemColumnId);
						var subQuery = GetType(ic.DataAdditional, ic.ItemColumnId);
						if (subQuery != null) {
							subQuery.SetInstance(instance);
							subQuery.SetProcess(process);
							subQuery.SetSession(currentSession);
							subQuery.SetFieldConfigs(ics.GetItemColumn(ic.ItemColumnId).DataAdditional2);

							var e = new ItemColumnEquations(instance, process, currentSession);
							r.Suspicious = e.TestSubqueryAndEnsureCache(ic.ItemColumnId, ic.Name, subQuery,
								subQuery.GetFieldConfigs(), true);
						}
					} catch (Exception e) {
						Diag.SupressException(e);
						r.Error = true;
					}

					r.TimeMs = DateTime.Now.Subtract(time).TotalMilliseconds;
					result.Add(r);
				}
			}

			return result.OrderByDescending(r => r.TimeMs).ToList();
		}

		[HttpGet("test/{process}/{queryType}/{itemColumnId}")]
		public bool Test(string process, string queryType, int itemColumnId) {
			SubQueryConfigBase subQuery = null;
			var ics = new ItemColumns(instance, process, currentSession);
			var col = ics.GetItemColumn(itemColumnId);

			try {
				if (!queriesById.ContainsKey(itemColumnId) ||
				    queryType != col.DataAdditional) {
					col.DataAdditional2 = null;
				}

				RemoveType(itemColumnId);

				subQuery = GetType(queryType, itemColumnId);
			} catch (Exception e) {
				Diag.SupressException(e);
				//Console.WriteLine("Unable to complete test.");
			}

			if (subQuery == null) return false;

			subQuery.SetInstance(instance);
			subQuery.SetProcess(process);
			subQuery.SetSession(currentSession);
			subQuery.SetFieldConfigs(ics.GetItemColumn(itemColumnId).DataAdditional2);

			try {
				var fc = subQuery.GetFieldConfigs();
				var e = new ItemColumnEquations(instance, process, currentSession);
				return e.TestSubqueryAndEnsureCache(itemColumnId, col.Name, subQuery, fc);
			} catch (Exception e) {
				Diag.SupressException(e);
				return false;
			}
		}

		[HttpGet("donetest/{process}/{queryType}/{itemColumnId}")]
		public bool DoneTest(string process, string queryType, int itemColumnId) {
			var ics = new ItemColumns(instance, process, currentSession);
			var col = ics.GetItemColumn(itemColumnId);
			return !Cache.IsItemColumnSuspicious(col.ItemColumnId);
		}

		[HttpGet("{process}/{queryType}/{itemColumnId}")]
		public Dictionary<string, object> GetConfig(string process, string queryType, int itemColumnId) {
			SubQueryConfigBase subQuery = null;
			var ics = new ItemColumns(instance, process, currentSession);

			try {
				var col = ics.GetItemColumn(itemColumnId);
				if (!queriesById.ContainsKey(itemColumnId) ||
				    queryType != col.DataAdditional) {
					col.DataAdditional2 = null;
				}

				RemoveType(itemColumnId);
				subQuery = GetType(queryType, itemColumnId);
			} catch (Exception e) {
				Diag.SupressException(e);
			}

			if (subQuery == null) return new Dictionary<string, object>();

			subQuery.SetInstance(instance);
			subQuery.SetProcess(process);
			subQuery.SetSession(currentSession);
			subQuery.SetFieldConfigs(ics.GetItemColumn(itemColumnId).DataAdditional2);
			return subQuery.GetFieldConfigs();
		}

		public static void AddType(SubQueryConfigBase obj) {
			queries.Add(obj.GetConfig().Value, obj);
		}

		public static SubQueryConfigBase GetType(string type, int itemColumnId) {
			if (queries.ContainsKey(type)) {
				if (queriesById.ContainsKey(itemColumnId)) {
					return queriesById[itemColumnId];
				}

				var className = queries[type].GetType().ToString();
				if (className.Contains("SubqueryAggregateFunction")) {
					var queryType = ((SubqueryAggregateFunction) queries[type])._type;
					var t = Type.GetType(className);
					var newSubQuery = (SubQueryConfigBase) Activator.CreateInstance(t, queryType);
					queriesById.Add(itemColumnId, newSubQuery);
				} else {
					var t = Type.GetType(className);
					var newSubQuery = (SubQueryConfigBase) Activator.CreateInstance(t);
					queriesById.Add(itemColumnId, newSubQuery);
				}

				return queriesById[itemColumnId];
			}

			return null;
		}

		public static void RemoveType(int itemColumnId) {
			if (queriesById.ContainsKey(itemColumnId)) {
				queriesById.Remove(itemColumnId);
			}
		}

		[HttpGet("subqueryValues/{process}/{queryType}/{itemColumnId}")]
		public List<Dictionary<string, object>> GetSubqueryValues(string process, string queryType, int itemColumnId) {
			var query = GetType(queryType, itemColumnId);
			return query.GetValues(instance, itemColumnId);
		}

// [HttpPost("testSubquery/{process}/{itemColumnId}")]
// public bool TestEquation(string process, int itemColumnId, [FromBody] Dictionary<string, string> query) {
// 	var e = new ItemColumnEquations(instance, process, currentSession);
// 	return e.TestSubquery(itemColumnId, query["type"], query["params"]);
// }
	}
}