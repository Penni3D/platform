﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubQueryUserCalendarField : SubQueryConfigBase {
		public SubQueryUserCalendarField() {
			Config.Name = "UserCalendarField";
			Config.Value = "UserCalendarField";
			Config.TranslationKey = "USER_CALENDAR_FIELD";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var db = new Connections(authenticatedSession);

			var process_CalendarField = "UserCalendarField_incorrect_configuration";
			if ((SubQueryConfigValue) FieldConfig["@CalendarField"] != null &&
			    ((SubQueryConfigValue) FieldConfig["@CalendarField"]).Value != null &&
			    ((string) ((SubQueryConfigValue) FieldConfig["@CalendarField"]).Value).Length > 0)
				process_CalendarField =
					(string) ((SubQueryConfigValue) FieldConfig["@CalendarField"])
					.Value;
			var exec_sql = "( SELECT " + process_CalendarField + " FROM GetCalendarForUser(pf.item_id) )";
			return exec_sql;
		}

		public override string GetArchiveSql(string instance) {
			throw new NotImplementedException("SubQueryUserCalendarField archive is not implemented");
		}

		protected override void InitializeFieldConfig() {
			var db = new Connections(authenticatedSession);

			var translations = new Translations(instance, authenticatedSession);
			var options = new List<Dictionary<string, string>>();
			options.Add(new Dictionary<string, string>()
				{{"field", "work_hours"}, {"translation", translations.GetTranslation("CALENDAR_WORK_HOURS")}});
			options.Add(new Dictionary<string, string>() {
				{"field", "timesheet_hours"}, {"translation", translations.GetTranslation("CALENDAR_TIMESHEET_HOURS")}
			});
			options.Add(new Dictionary<string, string>()
				{{"field", "weekly_hours"}, {"translation", translations.GetTranslation("CALENDAR_WEEKLY_HOURS")}});

			AddFieldConfig("@CalendarField",
				new SubQueryConfigValue("@CalendarField", "string", SubQueryOptionType.List, "field", "translation",
					options, true));
		}
	}
}