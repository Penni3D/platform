﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryCurrentUserConfig : SubQueryConfigBase {
		public SubqueryCurrentUserConfig() {
			Config.Name = "CurrentUser";
			Config.Value = "CurrentUser";
			Config.TranslationKey = "CURRENT_USER";
			Config.DataType = ItemColumn.ColumnType.Process;
			this.SubProcess = "user";
		}
		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT @SystemLoggedInUser)";
		}
		protected override void InitializeFieldConfig() { 
		
		}
	}
}