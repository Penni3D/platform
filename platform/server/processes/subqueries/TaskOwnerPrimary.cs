﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryTaskOwnerPrimaryConfig : SubQueryConfigBase {
		public SubqueryTaskOwnerPrimaryConfig() {
			Config.Name = "TaskOwnerPrimary";
			Config.Value = "TaskOwnerPrimary";
			Config.TranslationKey = "TASK_OWNER_PRIMARY";
			Config.DataType = ItemColumn.ColumnType.Int;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT '#TASK_OWNER_PRIMARY#')";
		}

		protected override void InitializeFieldConfig() { }
	}
}