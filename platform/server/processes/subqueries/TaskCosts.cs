﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryTaskCostConfig : SubQueryConfigBase {
		public SubqueryTaskCostConfig() {
			Config.Name = "TaskCost";
			Config.Value = "TaskCost";
			Config.TranslationKey = "TASK_COST";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return GetSqlCombined(instance);
		}

		public override string GetArchiveSql(string instance) {
			return GetSqlCombined(instance, true);
		}

		private string GetSqlCombined(string instance, bool isArchive = false) {
			var taskEffortTable = Conv.ToSql("_" + instance + "_task_user_effort");
			if (isArchive) taskEffortTable = "get__" + Conv.ToSql(instance) + "_task_user_effort(@archiveDate)";
			var userTable = "_" + Conv.ToSql(instance) + "_user";
			var ic = "us.item_id";
			if (isArchive) { 
				userTable = "get__" + Conv.ToSql(instance) + "_user(@archiveDate)";
				ic = "item_id";
			}

			//whereColId us.item_id > item_id ... is this correct .. crashes on history cases 
			var sql = "(SELECT SUM(effort * hour_rate) FROM " + processSelectionsTable(isArchive, ic, "selected_item_id") + " idps " +
			          " INNER JOIN " + taskEffortTable +  " eff ON idps.link_item_id = eff.item_id " +
			          " INNER JOIN " + userTable + " us ON idps.selected_item_id = us.item_id " +
			          " WHERE idps.item_id IN (select item_id FROM get_" + instance +
					  "_recursive_tasks(pf.item_id)) AND item_column_id = @TaskCostsResponsiblesColumnId) ";

			return sql;
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) {
				return;
			}

			AddFieldConfig("@TaskCostsResponsiblesColumnId",
				new SubQueryConfigValue("@TaskCostsResponsiblesColumnId", "int", SubQueryOptionType.List, "ItemColumnId",
					"Variable", new ItemColumns(instance, "task", authenticatedSession).GetItemColumns())
			);
		}
	}
}