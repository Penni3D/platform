﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryAggregateFunction : SubQueryConfigBase {
		private readonly string _genericAggregateFunction = "@GenericAggregateFunction";

		public int _type;

		public SubqueryAggregateFunction(int type) {
			_type = type;
			switch (type) {
				case 0:
					Config.Name = "GenericNumberAggregateFunction";
					Config.Value = "GenericNumberAggregateFunction";
					Config.TranslationKey = "GENERIC_NUMBER_AGGREGATE_FUNCTION";
					Config.DataType = ItemColumn.ColumnType.Float;
					break;
				case 1:
					Config.Name = "GenericStringAggregateFunction";
					Config.Value = "GenericStringAggregateFunction";
					Config.TranslationKey = "GENERIC_STRING_AGGREGATE_FUNCTION";
					Config.DataType = ItemColumn.ColumnType.Nvarchar;
					break;
				case 2:
					Config.Name = "GenericLongStringAggregateFunction";
					Config.Value = "GenericLongStringAggregateFunction";
					Config.TranslationKey = "GENERIC_LONG_STRING_AGGREGATE_FUNCTION";
					Config.DataType = ItemColumn.ColumnType.Text;
					break;
				case 3:
					Config.Name = "GenericDateAggregateFunction";
					Config.Value = "GenericDateAggregateFunction";
					Config.TranslationKey = "GENERIC_DATE_AGGREGATE_FUNCTION";
					Config.DataType = ItemColumn.ColumnType.Date;
					break;
				case 4:
					Config.Name = "GenericListAggregateFunction";
					Config.Value = "GenericListAggregateFunction";
					Config.TranslationKey = "GENERIC_LIST_AGGREGATE_FUNCTION";
					Config.DataType = ItemColumn.ColumnType.List;
					break;
				case 5:
					Config.Name = "GenericProcessAggregateFunction";
					Config.Value = "GenericProcessAggregateFunction";
					Config.TranslationKey = "GENERIC_PROCESS_AGGREGATE_FUNCTION";
					Config.DataType = ItemColumn.ColumnType.Process;
					break;
			}
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return GetSqlCombined(instance);
		}

		private Dictionary<int, List<Dictionary<string, object>>> _icCache = new();

		private List<Dictionary<string, object>> GetFromIcCache(int itemColumnId) {
			if (_icCache.ContainsKey(itemColumnId)) return _icCache[itemColumnId];

			var db = new Connections(authenticatedSession);
			var result = db.GetDatatableDictionary("SELECT * FROM item_columns WHERE item_column_id = " + itemColumnId);

			_icCache.Add(itemColumnId, result);
			return result;
		}

		private string GetSqlCombined(string instance, bool isArchive = false) {
			var db = new Connections(authenticatedSession);

			var aggr = Aggregate.SUM;
			if (_type == 0 || _type == 3) {
				var sq = (SubQueryConfigValue) FieldConfig[_genericAggregateFunction];
				var sqv = sq.Value;
				aggr = (Aggregate) sqv;
			}

			// TODO: field name is not yet selectable
			var sourceCol =
				GetFromIcCache(Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value))[0];

			var sourceProcess = "";
			if (sourceCol["process"].Equals(process)) {
				if (Conv.ToInt(sourceCol["data_type"]) == 0 || Conv.ToInt(sourceCol["data_type"]) == 1) {
					if (Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value) <= 0) {
						sourceProcess = Conv.ToStr(sourceCol["process"]);
					} else {
						sourceProcess = Conv.ToStr(db.ExecuteScalar(
							"SELECT process from item_columns WHERE item_column_id = " +
							((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value));
					}
				} else {
					sourceProcess = Conv.ToStr(sourceCol["data_additional"]);
				}
			} else {
				sourceProcess = Conv.ToStr(sourceCol["process"]);
			}

			var sourceColumn = "";
			var oColumn = "";
			if (Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value) <= 0) {
				sourceColumn = "item_id";
			} else {
				sourceColumn = Conv.ToSql(db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " +
				                                           ((SubQueryConfigValue) FieldConfig["@SourceColumnId"])
				                                           .Value));
			}

			if (Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalOrderColumnId"]).Value) <= 0) {
				oColumn = "item_id";
			} else {
				oColumn = Conv.ToSql(db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " +
				                                      ((SubQueryConfigValue) FieldConfig["@OptionalOrderColumnId"])
				                                      .Value));
			}

			sourceProcess = Conv.ToSql(sourceProcess);

			var sql = " ";
			var top1 = "";
			var isTop1 = false;
			var top1Val = 0;
			bool lTrim = false;
			var subqueryInLookupWhere = " ";
			var processTable = isArchive
				? "get__" + Conv.ToSql(instance + "_" + sourceProcess) + "(@archiveDate)"
				: Conv.ToSql("_" + instance + "_" + sourceProcess);

			if (_type == 0 || _type == 3) {
				sql += " (SELECT " + aggr + "(" + sourceColumn + ") " + " FROM " + processTable + " processTable ";
			} else if (_type == 1 || _type == 2) {
				lTrim = true;


				//IF LOOKUP HAS 'Choose how many rows and in which order should be returned'

				if (((SubQueryConfigValue) FieldConfig["@OptionalResultType"]).Value != null) {
					var orderDirection = "ASC";

					if (((SubQueryConfigValue) FieldConfig["@OptionalOrderDirection"]).Value != null &&
					    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalOrderDirection"]).Value) == 1) {
						orderDirection = "DESC";
					}

					if (Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalResultType"]).Value) == 0) {
						subqueryInLookupWhere = "ORDER BY " + oColumn + " " + orderDirection;
						sql += "LTRIM(STUFF (( SELECT ', ' + " + sourceColumn + " FROM " + processTable +
						       " processTable ";
					} else {
						//VALUE 1 MEANS SELECTED COLUMN, for example getting titles of subprocess, then ordering by that title col

						// VALUE 2 MEANS 'TOP 1 BY ARCHIVE START'
						if (Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalResultType"]).Value) == 2) {
							oColumn = "processTable.archive_start";
							//VALUE 3 MEANS 'TOP 1 BY ORDER (item_id I imagine)'
						} else if (Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalResultType"]).Value) == 3) {
							oColumn = "pf.order_no ";
						}

						sql += "LTRIM(STUFF (( SELECT ', ' + ( SELECT TOP 1 " + sourceColumn + " FROM " + processTable +
						       " processTable ";


						subqueryInLookupWhere = "ORDER BY " + oColumn + " " + orderDirection + ")";
					}
				} else {
					sql += "LTRIM(STUFF (( SELECT ', ' + " + sourceColumn + " FROM " + processTable + " processTable ";
				}
			} else if (_type == 4 || _type == 5) {
				var sourceList = db.GetDatatableDictionary("SELECT * FROM item_columns WHERE process = '" +
				                                           sourceProcess + "' AND name = '" + sourceColumn + "' ");

				if (((SubQueryConfigValue) FieldConfig["@OptionalResultType"]).Value != null &&
				    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalResultType"]).Value) > 0) {
					isTop1 = true;
					top1 = " TOP 1 ";
					top1Val = Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalResultType"]).Value);
				}

				sql += "STUFF ((SELECT " + top1 + " ', ' + cast(li1.item_id  as nvarchar) " + " FROM " + processTable +
				       " processTable " +
				       " inner join " + processSelectionsTable(isArchive, Conv.ToStr(sourceList[0]["item_column_id"])) +
				       " li_idps on li_idps.item_id = processTable.item_id and item_column_id =  " +
				       Conv.ToInt(sourceList[0]["item_column_id"]);

				if (isArchive) {
					sql += " inner join get__" + instance + "_" + Conv.ToStr(sourceList[0]["data_additional"]) +
					       "(@archiveDate) ";
				} else {
					sql += " inner join _" + instance + "_" + Conv.ToStr(sourceList[0]["data_additional"]);
				}

				sql += " li1 on li_idps.selected_item_id = li1.item_id ";

				SubProcess = Conv.ToStr(sourceList[0]["data_additional"]);
			}


			if (Conv.ToInt(sourceCol["data_type"]) == 5 || Conv.ToInt(sourceCol["data_type"]) == 6) {
				sql += " INNER JOIN " + processSelectionsTable(isArchive, "@SourceProcessColumnId") +
				       " AS parent_link ON ";
				if (Conv.ToInt(sourceCol["input_method"]) == 3) {
					sql +=
						" 	(parent_link.item_id = pf.item_id AND parent_link.item_column_id = " +
						Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value) +
						" AND parent_link.selected_item_id = processTable.item_id ) ";
				} else {
					var flipParentChild = Conv.ToInt(((SubQueryConfigValue) FieldConfig["@FlipParentChild"]).Value);
					if (flipParentChild == 0 && sourceProcess.Equals(sourceCol["process"])) {
						sql +=
							" (parent_link.item_id = processTable.item_id AND parent_link.item_column_id = " +
							Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value) +
							" AND parent_link.selected_item_id = pf.item_id) ";
					} else {
						sql +=
							" (parent_link.selected_item_id = processTable.item_id AND parent_link.item_column_id = " +
							Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value) +
							" AND parent_link.item_id = pf.item_id) ";
					}
				}
			}

			if (((SubQueryConfigValue) FieldConfig["@OptionalList1ItemId"]).Value != null &&
			    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList1ColumnId"]).Value) > 0) {
				sql += " INNER JOIN " + processSelectionsTable(isArchive, "@OptionalList1ColumnId") +
				       " AS list1_param ON list1_param.item_id = processTable.item_id AND list1_param.item_column_id = " +
				       Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList1ColumnId"]).Value) +
				       " AND list1_param.selected_item_id IN (" +
				       Conv.ToDelimitedString((int[]) ((SubQueryConfigValue) FieldConfig["@OptionalList1ItemId"])
					       .Value) +
				       ") ";
			}

			if (((SubQueryConfigValue) FieldConfig["@OptionalList2ItemId"]).Value != null &&
			    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList2ColumnId"]).Value) > 0) {
				sql += " INNER JOIN " + processSelectionsTable(isArchive, "@OptionalList2ColumnId") +
				       " AS list2_param ON list2_param.item_id = processTable.item_id AND list2_param.item_column_id = " +
				       Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList2ColumnId"]).Value) +
				       " AND list2_param.selected_item_id IN (" +
				       Conv.ToDelimitedString((int[]) ((SubQueryConfigValue) FieldConfig["@OptionalList2ItemId"])
					       .Value) +
				       ") ";
			}

			var hasWhere = false;
			if (Conv.ToInt(sourceCol["data_type"]) == 0 || Conv.ToInt(sourceCol["data_type"]) == 1) {
				var flipParentChild = Conv.ToInt(((SubQueryConfigValue) FieldConfig["@FlipParentChild"]).Value);
				if (_type == 4 || _type == 5) {
					if (flipParentChild == 0 && process.Equals(sourceCol["process"])) {
						sql += " WHERE processTable.item_id = pf." + Conv.ToStr(sourceCol["name"]) + " ";
					} else {
						sql += " WHERE processTable." + Conv.ToStr(sourceCol["name"]) + " = pf.item_id ";
					}
				} else {
					if (flipParentChild == 0 && process.Equals(sourceCol["process"])) {
						sql += " WHERE item_id = pf." + Conv.ToStr(sourceCol["name"]) + " ";
					} else {
						sql += " WHERE " + Conv.ToStr(sourceCol["name"]) + " = pf.item_id " + subqueryInLookupWhere;
					}
				}

				hasWhere = true;
			}

			if (((SubQueryConfigValue) FieldConfig["@OptionalDateId"]).Value != null &&
			    Conv.ToStr(((SubQueryConfigValue) FieldConfig["@OptionalDateId"]).Value).Length > 0) {
				var dateType = Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalDateType"]).Value);
				var dateVal = Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalDateOffset"]).Value);
				var dateCol = Conv.ToStr(((SubQueryConfigValue) FieldConfig["@OptionalDateId"]).Value);


				var dateSql = " WHERE ";
				if (hasWhere) {
					dateSql = " AND ";
				}

				switch (dateType) {
					case 0:
						dateSql += " YEAR(" + dateCol + ") = YEAR(DATEADD(year, " + dateVal + ", GETDATE())) ";
						break;
					case 1:
						dateSql += " YEAR(" + dateCol + ") = YEAR(GETDATE()) AND DATEPART(quarter, " + dateCol +
						           ")  = " + dateVal;
						break;
					case 2:
						dateSql += " YEAR(DATEADD(quarter, " + dateVal + ", getdate()))  =  year(" + dateCol +
						           ") and  datepart(quarter, DATEADD(quarter, " + dateVal +
						           ", getdate())) = datepart(quarter, " +
						           dateCol + " ) ";
						break;
					case 3:
						var currMonth = DateTime.Now.Month;
						currMonth += Conv.ToInt(dateVal);

						dateSql += " YEAR(" + dateCol + ") = YEAR(GETDATE()) AND DATEPART(month, " + dateCol + ")  = " +
						           currMonth;
						break;
					case 4:
						dateSql += " YEAR(DATEADD(month, " + dateVal + ", getdate()))  =  year(" + dateCol +
						           ") and  datepart(quarter, DATEADD(month, " + dateVal +
						           ", getdate())) = datepart(month, " + dateCol +
						           " ) ";
						break;
				}

				sql += dateSql;
			}


			if (_type == 0 || _type == 3) {
				sql += " ) ";
			} else if (_type == 1 || _type == 2 || _type == 4 || _type == 5) {
				if (_type == 4 || _type == 5) {
					if (isTop1) {
						var hasOrd = false;
						if (top1Val == 1) {
							sql += " ORDER BY processTable.order_no  ";
							hasOrd = true;
						} else if (top1Val == 2) {
							sql += " ORDER BY processTable.archive_start ";
							hasOrd = true;
						} else if (top1Val == 3) {
							if (((SubQueryConfigValue) FieldConfig["@OptionalOrderColumnId"]).Value != null &&
							    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalOrderColumnId"]).Value) > 0) {
								var orderColId =
									Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalOrderColumnId"]).Value);
								var orderColumnName =
									db.ExecuteScalar(
										"SELECT name from item_columns WHERE data_type IN (0,1,2,3,4) AND item_column_id = " +
										orderColId);
								if (orderColumnName != null && !DBNull.Value.Equals(orderColumnName)) {
									orderColumnName =
										new ProcessBase(instance, process, authenticatedSession).ValidateOrderColumn(
											Conv.ToStr(orderColumnName));
									sql += " ORDER BY processTable." + orderColumnName;
									hasOrd = true;
								}
							}
						}

						if (hasOrd && ((SubQueryConfigValue) FieldConfig["@OptionalOrderDirection"]).Value != null &&
						    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalOrderDirection"]).Value) > 0) {
							sql += " DESC ";
						} else if (hasOrd) {
							sql += " ASC ";
						}
					}
				}

				sql += " FOR XML PATH, TYPE).value(N'.[1]',N'nvarchar(max)'), 1, 1, '')  " + (lTrim ? ") " : "");
			}

			return sql;
		}

		public override string GetOrderSql(string instance = "") {
			return "";
		}

		public override string GetCacheValidationSql(string instance) {
			//Owner Column
			var sourceCol =
				GetFromIcCache(Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value))[0];
            
			//List or Process
			if (Conv.ToInt(sourceCol["data_type"]) == 5 || Conv.ToInt(sourceCol["data_type"]) == 6) {
				var idCol = Conv.ToStr(sourceCol["process"]) == Conv.ToSql(process) ? "item_id" : "selected_item_id";
                var sourceColumnId = Conv.ToInt(((SubQueryConfigValue)FieldConfig["@SourceColumnId"]).Value);
                
                return
					"(SELECT DISTINCT " + idCol + " FROM item_data_process_selections sidps WHERE (sidps.item_column_id = " + sourceCol["item_column_id"] + " AND sidps." + idCol + " = pf.item_id) OR (sidps.item_column_id = " + sourceColumnId + ") AND sidps.archive_start > @cacheDate)";
			}

			//Others
			var processTable = Conv.ToSql("_" + instance + "_" + Conv.ToStr(sourceCol["process"]));
			return "(SELECT DISTINCT " + sourceCol["name"] + " FROM " + processTable +
			       " WHERE archive_start > @cacheDate)";
		}

		protected override void InitializeFieldConfig() {
			var db = new Connections(authenticatedSession);

			var options = new List<object> {
				new Dictionary<string, object> {{"Name", "SUM"}, {"Value", Aggregate.SUM}},
				new Dictionary<string, object> {{"Name", "AVG"}, {"Value", Aggregate.AVG}},
				new Dictionary<string, object> {{"Name", "MAX"}, {"Value", Aggregate.MAX}},
				new Dictionary<string, object> {{"Name", "MIN"}, {"Value", Aggregate.MIN}},
				new Dictionary<string, object> {{"Name", "COUNT"}, {"Value", Aggregate.COUNT}}
			};

			if (_type == 0 || _type == 3) {
				AddFieldConfig(_genericAggregateFunction,
					new SubQueryConfigValue(_genericAggregateFunction, "int", SubQueryOptionType.List, "Value", "Name",
						options)
				);
			}


			var spSql =
				"SELECT item_column_id, ic.process + ' : ' + coalesce(translation, ic.name) AS result FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
				authenticatedSession.locale_id + "' WHERE ((ic.process = '" + Conv.ToSql(process) +
				"' OR data_additional = '" + Conv.ToSql(process) +
				"' ) AND (data_type =5 OR data_type = 6)) OR ( data_type IN (0,1) AND name like '%item_id%' )  ";
			AddFieldConfig("@SourceProcessColumnId",
				new SubQueryConfigValue("@SourceProcessColumnId", "int", SubQueryOptionType.List, "item_column_id",
					"result",
					db.GetDatatableDictionary(spSql),
					true)
			);

			if (FieldConfig.ContainsKey("@SourceProcessColumnId") &&
			    ((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value != null &&
			    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value) > 0) {
				var cachedcol =
					GetFromIcCache(Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value));

				Dictionary<string, object> sourceCol = null;

				if (cachedcol != null && cachedcol.Count > 0) sourceCol = cachedcol[0];

				var sourceProcess = "";

				if (sourceCol != null && sourceCol["process"].Equals(process)) {
					if (_type == 4) {
						var tcol = GetFromIcCache(
							Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value));
						if (tcol != null && tcol.Count > 0) sourceProcess = Conv.ToStr(tcol[0]["process"]);
					} else if (Conv.ToInt(sourceCol["data_type"]) == 0 || Conv.ToInt(sourceCol["data_type"]) == 1) {
						sourceProcess = "";
					} else {
						sourceProcess = Conv.ToStr(sourceCol["data_additional"]);
					}
				} else if (sourceCol != null) {
					sourceProcess = Conv.ToStr(sourceCol["process"]);
				}

				object cols = null;
				object valueCols;
				object listCols = null;
				object processCols = null;
				object dateCols = null;


				if (!sourceProcess.Equals("")) {
					cols = new ItemColumns(instance, sourceProcess, authenticatedSession).GetItemColumns(false);
					valueCols = new List<ItemColumn>();
					listCols = new List<ItemColumn>();
					processCols = new List<ItemColumn>();
					dateCols = new List<ItemColumn>();
					if (_type == 0) {
						var c = new ItemColumn();
						c.ItemColumnId = -1;
						c.Process = sourceProcess;
						c.Name = "item_id";
						c.Variable = "item_id";
						c.DataType = 1;
						((List<ItemColumn>) valueCols).Add(c);
					}

					foreach (var c in (List<ItemColumn>) cols) {
						if (_type == 0 && (c.DataType == 1 || c.DataType == 2)) {
							((List<ItemColumn>) valueCols).Add(c);
						} else if ((_type == 1 || _type == 2) && (c.DataType == 0 || c.DataType == 4 || c.DataType == 23)) {
							((List<ItemColumn>) valueCols).Add(c);
						} else if (_type == 3 && (c.DataType == 3 || c.DataType == 13)) {
							((List<ItemColumn>) valueCols).Add(c);
						} else if (c.DataType == 5) {
							((List<ItemColumn>) processCols).Add(c);
							if (_type == 5) ((List<ItemColumn>) valueCols).Add(c);
						} else if (c.DataType == 6) {
							((List<ItemColumn>) listCols).Add(c);
							if (_type == 4) ((List<ItemColumn>) valueCols).Add(c);
						} else if (c.DataType == 3 || c.DataType == 13) {
							((List<ItemColumn>) dateCols).Add(c);
						}
					}
				} else {
					if (_type == 0) {
						valueCols = db.GetDatatableDictionary(
							"SELECT item_column_id As ItemColumnId, name as Name, ic.process + ' : ' + coalesce(translation, ic.name) As Variable FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
							authenticatedSession.locale_id + "' WHERE data_type IN (1,2) ");
					} else if (_type == 1 || _type == 2) {
						valueCols = db.GetDatatableDictionary(
							"SELECT item_column_id As ItemColumnId, name as Name, ic.process + ' : ' + coalesce(translation, ic.name) As Variable FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
							authenticatedSession.locale_id + "' WHERE data_type IN (0,4,23) ");
					} else if (_type == 3) {
						valueCols = db.GetDatatableDictionary(
							"SELECT item_column_id As ItemColumnId, name as Name, ic.process + ' : ' + coalesce(translation, ic.name) As Variable FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
							authenticatedSession.locale_id + "' WHERE data_type IN (3, 13) ");
					} else if (_type == 4 || _type == 6) {
						valueCols = db.GetDatatableDictionary(
							"SELECT item_column_id As ItemColumnId, name as Name, ic.process + ' : ' + coalesce(translation, ic.name) As Variable FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
							authenticatedSession.locale_id + "' WHERE data_type IN (6) ");
					} else {
						valueCols = db.GetDatatableDictionary(
							"SELECT item_column_id As ItemColumnId, name as Name, ic.process + ' : ' + coalesce(translation, ic.name) As Variable FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
							authenticatedSession.locale_id + "' WHERE data_type IN (5) ");
					}
				}

				AddFieldConfig("@SourceColumnId",
					new SubQueryConfigValue("@SourceColumnId", "int", SubQueryOptionType.List, "ItemColumnId",
						"Variable", valueCols)
				);

				var yes_no = new List<object> {
					new Dictionary<string, object> {{"Name", "FALSE"}, {"Value", 0}},
					new Dictionary<string, object> {{"Name", "TRUE"}, {"Value", 1}},
				};

				AddFieldConfig("@FlipParentChild",
					new SubQueryConfigValue("@FlipParentChild", "int", SubQueryOptionType.List, "Value", "Name",
						yes_no));

				AddFieldConfig("@OptionalList1ColumnId",
					new SubQueryConfigValue("@OptionalList1ColumnId", "int", SubQueryOptionType.List, "ItemColumnId",
						"Variable",
						listCols, true));

				if (FieldConfig.ContainsKey("@OptionalList1ColumnId") &&
				    ((SubQueryConfigValue) FieldConfig["@OptionalList1ColumnId"]).Value != null &&
				    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList1ColumnId"]).Value) > 0) {
					var listProcess = db.ExecuteScalar(
						"SELECT data_additional FROM item_columns WHERE item_column_id = " +
						Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList1ColumnId"]).Value));

					var listVals =
						db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_" + listProcess);
					AddFieldConfig("@OptionalList1ItemId",
						new SubQueryConfigValue("@OptionalList1ItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", listVals));
				} else {
					AddFieldConfig("@OptionalList1ItemId",
						new SubQueryConfigValue("@OptionalList1ItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", new List<Dictionary<string, object>>()));
				}

				AddFieldConfig("@OptionalList2ColumnId",
					new SubQueryConfigValue("@OptionalList2ColumnId", "int", SubQueryOptionType.List, "ItemColumnId",
						"Variable",
						listCols, true));
				if (FieldConfig.ContainsKey("@OptionalList2ColumnId") &&
				    ((SubQueryConfigValue) FieldConfig["@OptionalList2ColumnId"]).Value != null &&
				    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList2ColumnId"]).Value) > 0) {
					var listProcess = db.ExecuteScalar(
						"SELECT data_additional FROM item_columns WHERE item_column_id = " +
						Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalList2ColumnId"]).Value));

					var listVals =
						db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_" + listProcess);
					AddFieldConfig("@OptionalList2ItemId",
						new SubQueryConfigValue("@OptionalList2ItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", listVals));
				} else {
					AddFieldConfig("@OptionalList2ItemId",
						new SubQueryConfigValue("@OptionalList2ItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", new List<Dictionary<string, object>>()));
				}

				AddFieldConfig("@OptionalDateId",
					new SubQueryConfigValue("@OptionalDateId", "string", SubQueryOptionType.List, "Name", "Variable",
						dateCols,
						true));
				if (FieldConfig.ContainsKey("@OptionalDateId") &&
				    ((SubQueryConfigValue) FieldConfig["@OptionalDateId"]).Value != null &&
				    Conv.ToStr(((SubQueryConfigValue) FieldConfig["@OptionalDateId"]).Value).Length > 0) {
					var dateOptions = new List<object> {
						new Dictionary<string, object> {{"Name", "YEAR"}, {"Value", 0}},
						new Dictionary<string, object> {{"Name", "QUARTER_CURRENT_YEAR"}, {"Value", 1}},
						new Dictionary<string, object> {{"Name", "QUARTER_MOVING"}, {"Value", 2}},
						new Dictionary<string, object> {{"Name", "MONTH_CURRENT_YEAR"}, {"Value", 3}},
						new Dictionary<string, object> {{"Name", "MONTH_MOVING"}, {"Value", 4}}
					};

					AddFieldConfig("@OptionalDateType",
						new SubQueryConfigValue("@OptionalDateType", "int", SubQueryOptionType.List, "Value", "Name",
							dateOptions));

					if (FieldConfig.ContainsKey("@OptionalDateType") &&
					    ((SubQueryConfigValue) FieldConfig["@OptionalDateType"]).Value != null &&
					    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalDateType"]).Value) > 0) {
						AddFieldConfig("@OptionalDateOffset",
							new SubQueryConfigValue("@OptionalDateOffset", "int", SubQueryOptionType.Int, "Name",
								"Value", new List<Dictionary<string, object>>()));
					}
				} else {
					AddFieldConfig("@OptionalDateType",
						new SubQueryConfigValue("@OptionalDateType", "int", SubQueryOptionType.List, "Value", "Name",
							new List<Dictionary<string, object>>()));
				}

				if (_type == 4 || _type == 5 || _type == 1 || _type == 2) {
					var resultOptions = new List<object> {
						new Dictionary<string, object> {{"Name", "ALL"}, {"Value", 0}},
						new Dictionary<string, object> {{"Name", "TOP 1 By Custom Order"}, {"Value", 1}},
						new Dictionary<string, object> {{"Name", "TOP 1 By Archive Start"}, {"Value", 2}},
						new Dictionary<string, object> {{"Name", "TOP 1 By OrderNo Column"}, {"Value", 3}}
					};

					AddFieldConfig("@OptionalResultType",
						new SubQueryConfigValue("@OptionalResultType", "int", SubQueryOptionType.List, "Value", "Name",
							resultOptions));

					if (cols != null) {
						AddFieldConfig("@OptionalOrderColumnId",
							new SubQueryConfigValue("@OptionalOrderColumnId", "int", SubQueryOptionType.List,
								"ItemColumnId", "Variable", cols)
						);
					}

					var sortOptions = new List<object> {
						new Dictionary<string, object> {{"Name", "Ascending"}, {"Value", 0}},
						new Dictionary<string, object> {{"Name", "Descending"}, {"Value", 1}}
					};

					AddFieldConfig("@OptionalOrderDirection",
						new SubQueryConfigValue("@OptionalOrderDirection", "int", SubQueryOptionType.List, "Value",
							"Name", sortOptions));
				}
			} else {
				AddFieldConfig("@SourceColumnId",
					new SubQueryConfigValue("@SourceColumnId", "int", SubQueryOptionType.List, "Name", "Variable",
						new List<Dictionary<string, object>>())
				);
				AddFieldConfig("@FlipParentChild",
					new SubQueryConfigValue("@FlipParentChild", "int", SubQueryOptionType.List, "Value", "Name",
						new List<Dictionary<string, object>>()));
				AddFieldConfig("@OptionalList1ColumnId",
					new SubQueryConfigValue("@OptionalList1ColumnId", "int", SubQueryOptionType.List, "ItemColumnId",
						"Variable", new List<Dictionary<string, object>>()));
				AddFieldConfig("@OptionalList1ItemId",
					new SubQueryConfigValue("@OptionalList1ItemId", "int_array", SubQueryOptionType.MultiSelectList,
						"item_id", "list_item", new List<Dictionary<string, object>>()));

				AddFieldConfig("@OptionalList2ColumnId",
					new SubQueryConfigValue("@OptionalListId2", "int", SubQueryOptionType.List, "ItemColumnId",
						"Variable", new List<Dictionary<string, object>>()));
				AddFieldConfig("@OptionalList2ItemId",
					new SubQueryConfigValue("@OptionalList2ItemId", "int_array", SubQueryOptionType.MultiSelectList,
						"item_id", "list_item", new List<Dictionary<string, object>>()));

				AddFieldConfig("@OptionalDateId",
					new SubQueryConfigValue("@OptionalDateId", "string", SubQueryOptionType.List, "Name", "Variable",
						new List<Dictionary<string, object>>()));
				AddFieldConfig("@OptionalDateType",
					new SubQueryConfigValue("@OptionalDateType", "int", SubQueryOptionType.List, "Name", "Value",
						new List<Dictionary<string, object>>()));
				AddFieldConfig("@OptionalDateOffset",
					new SubQueryConfigValue("@OptionalDateOffset", "int", SubQueryOptionType.Int, "Name", "Value",
						new List<Dictionary<string, object>>()));

				AddFieldConfig("@OptionalResultType",
					new SubQueryConfigValue("@OptionalResultType", "int", SubQueryOptionType.List, "Name", "Value",
						new List<Dictionary<string, object>>()));
				AddFieldConfig("@OptionalOrderColumnId",
					new SubQueryConfigValue("@OptionalOrderColumnId", "int", SubQueryOptionType.List, "Name", "Value",
						new List<Dictionary<string, object>>()));
				AddFieldConfig("@OptionalOrderDirection",
					new SubQueryConfigValue("@OptionalOrderDirection", "int", SubQueryOptionType.List, "Name", "Value",
						new List<Dictionary<string, object>>()));
			}
		}

		public override List<Dictionary<string, object>> GetValues(string instance, int itemColumnId) {
			if (_type == 4 || _type == 5) {
				var db = new Connections(authenticatedSession);

				var sourceCol =
					GetFromIcCache(Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value))[0];
				
				//If union process, eliminate union level -- as if the request was coming from the original column
				if (process.Contains("union_process_")) process = Conv.ToStr(sourceCol["process"]);

				var sourceProcess = "";
				if (sourceCol["process"].Equals(process)) {
					if (Conv.ToInt(sourceCol["data_type"]) == 0 || Conv.ToInt(sourceCol["data_type"]) == 1) {
						sourceProcess = Conv.ToStr(db.ExecuteScalar(
							"SELECT process from item_columns WHERE item_column_id = " +
							((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value));
					} else {
						sourceProcess = Conv.ToStr(sourceCol["data_additional"]);
					}
				} else {
					sourceProcess = Conv.ToStr(sourceCol["process"]);
				}

				var sourceColumn = db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " +
				                                    ((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value);
				var sourceList = db.GetDatatableDictionary("SELECT * from item_columns WHERE process = '" +
				                                           sourceProcess + "' AND name = '" + sourceColumn + "' ");

				SubProcess = Conv.ToSql(Conv.ToStr(sourceList[0]["data_additional"]));
				
				var order = "";
				if (_type == 4) {
					var orderItemColumnId =
						Conv.ToInt(((SubQueryConfigValue) FieldConfig["@OptionalOrderColumnId"]).Value);
					var sql = " SELECT TOP 1 name FROM ( " +
					          " SELECT CASE WHEN ISNULL(list_order, '') = '' THEN 'order_no' ELSE list_order END AS name,0 AS order_no FROM processes WHERE process = '" +
					          SubProcess + "' " +
					          " UNION ALL " +
					          " SELECT name,1 AS order_no FROM item_columns WHERE item_column_id = " +
					          orderItemColumnId +
					          " ) iq " +
					          " ORDER BY order_no DESC ";

					var ocol = Conv.ToStr(db.ExecuteScalar(sql));
					if (ocol == "") ocol = "order_no";
					order = "ORDER BY " + ocol + " ASC";
				}

				return db.GetDatatableDictionary("SELECT * FROM _" + instance + "_" + SubProcess + " " + order);
			}

			return new List<Dictionary<string, object>>();
		}

		public override string GetFilterSql(string instance) {
			if (_type == 4 || _type == 5) {
				var subSql = GetSql(instance);

				if (subSql.IndexOf("TOP 1") > 0) {
					foreach (var key in FieldConfig.Keys) {
						var x = (SubQueryConfigValue) FieldConfig[key];
						if (x.ValueType == "int_array") {
							var idStr = string.Join(",", Conv.ToIntArray(x.Value));
							subSql = subSql.Replace(key, idStr);
						} else {
							var val = Conv.ToStr(((SubQueryConfigValue) FieldConfig[key]).Value);
							subSql = subSql.Replace(key, val);
						}
					}

					return subSql + " IN ( ##idString## ) ";
				}

				var db = new Connections(authenticatedSession);

				var sourceCol =
					GetFromIcCache(Conv.ToInt(((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value))[0];

				var sourceProcess = "";
				if (sourceCol["process"].Equals(process)) {
					if (Conv.ToInt(sourceCol["data_type"]) == 0 || Conv.ToInt(sourceCol["data_type"]) == 1) {
						sourceProcess = Conv.ToStr(db.ExecuteScalar(
							"SELECT process from item_columns WHERE item_column_id = " +
							((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value));
					} else {
						sourceProcess = Conv.ToStr(sourceCol["data_additional"]);
					}
				} else {
					sourceProcess = Conv.ToStr(sourceCol["process"]);
				}

				var sourceColumn = db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " +
				                                    ((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value);


				var sourceList = db.GetDatatableDictionary("SELECT * from item_columns WHERE process = '" +
				                                           sourceProcess +
				                                           "' AND name = '" + sourceColumn + "' ");

				SubProcess = Conv.ToStr(sourceList[0]["data_additional"]);

				if (Conv.ToInt(sourceCol["data_type"]) == 5) {
					var sql = " pf.item_id in ( " +
					          "        SELECT " +
					          (Conv.ToInt(sourceCol["input_method"]) == 3 ? "i1.selected_item_id" : "i1.item_id") +
					          "        FROM " +
					          "             item_data_process_selections i1 inner join item_data_process_selections i2 on i1.selected_item_id = i2.item_id " +
					          "        where " +
					          "      i1.item_column_id = " +
					          ((SubQueryConfigValue) FieldConfig["@SourceProcessColumnId"]).Value +
					          " AND (i1.item_id = pf.item_id OR i1.selected_item_id = pf.item_id) " +
					          " AND  i2.item_column_id = " +
					          ((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value +
					          " AND i2.selected_item_id IN (##idString##) ) ";
					return sql;
				}

				if (Conv.ToStr(sourceCol["process"]) == process && Conv.ToInt(sourceCol["data_type"]) == 1) {
					return
						" (SELECT COUNT(*) FROM item_data_process_selections idpsI WHERE idpsI.item_column_id = " +
						((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value + " AND idpsI.item_id = " +
						Conv.ToStr(sourceCol["name"]) + " AND idpsI.selected_item_id IN (##idString##)) > 0";
				}

				if (Conv.ToStr(sourceCol["process"]) != sourceProcess) {
					return
						" pf.item_id IN (SELECT item_id FROM item_data_process_selections idps WHERE idps.selected_item_id IN " +
						"(SELECT idpsI.item_id FROM item_data_process_selections idpsI WHERE idpsI.item_column_id = " +
						((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value + " AND " +
						"idpsI.selected_item_id IN (##idString##)) AND idps.item_column_id = " +
						sourceCol["item_column_id"] + ")";
				}


				return " pf.item_id in (SELECT main.item_id FROM _" + instance + "_" + process + " main " +
				       " inner join _" + instance + "_" + sourceProcess +
				       " li_main on main.item_id = li_main." + sourceCol["name"] +
				       " inner join item_data_process_selections li_idps on li_idps.item_id = li_main.item_id and item_column_id =  " +
				       ((SubQueryConfigValue) FieldConfig["@SourceColumnId"]).Value +
				       " where li_idps.selected_item_id in (##idString##))";
			}

			return "";
		}

		public override string GetSubProcess() {
			if (_type == 4 || _type == 5) {
				try {
					var db = new Connections(authenticatedSession);
					var configData = GetConfigData();
					var sourceCol = GetFromIcCache(Conv.ToInt(configData["@SourceProcessColumnId"]))[0];

					var sourceProcess = "";
					if (sourceCol["process"].Equals(process)) {
						if (Conv.ToInt(sourceCol["data_type"]) == 0 || Conv.ToInt(sourceCol["data_type"]) == 1) {
							sourceProcess = Conv.ToStr(db.ExecuteScalar(
								"SELECT process from item_columns WHERE item_column_id = " +
								configData["@SourceColumnId"]));
						} else {
							sourceProcess = Conv.ToStr(sourceCol["data_additional"]);
						}
					} else {
						sourceProcess = Conv.ToStr(sourceCol["process"]);
					}

					var sourceColumn = db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " +
					                                    configData["@SourceColumnId"]);

					var sourceList = db.GetDatatableDictionary("SELECT * from item_columns WHERE process = '" +
					                                           sourceProcess +
					                                           "' AND name = '" + sourceColumn + "' ");

					SubProcess = Conv.ToStr(sourceList[0]["data_additional"]);
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
					return SubProcess;
				}
			} else {
				SubProcess = "";
			}

			return SubProcess;
		}

		public override string GetArchiveSql(string instance) {
			return GetSqlCombined(instance, true);
			;
		}

		private enum Aggregate {
			SUM = 1,
			AVG = 2,
			MAX = 3,
			MIN = 4,
			COUNT = 5
		}
	}
}