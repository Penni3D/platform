﻿using System;
using System.Collections.Generic;
using System.Threading;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryProcessActualEffortConfig : SubQueryConfigBase {
		public SubqueryProcessActualEffortConfig() {
			Config.Name = "ProcessActualEffort";
			Config.Value = "ProcessActualEffort";
			Config.TranslationKey = "PROCESS_ACTUAL_EFFORT";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return GetSqlCombined(instance);
		}

		public override string GetArchiveSql(string instance) {
			return GetSqlCombined(instance, true);
		}

		private string GetSqlCombined(string instance, bool isArchive = false) {
			var worktimeTrackingHoursTable = Conv.ToSql("_" + instance + "_WORKTIME_TRACKING_HOURS");
			if (isArchive) {
				worktimeTrackingHoursTable = "get__" + Conv.ToSql(instance) + "_WORKTIME_TRACKING_HOURS(@archiveDate)";
			}
			var taskTable = Conv.ToSql("_" + instance + "_task");
			if (isArchive) {
				taskTable = "get__" + Conv.ToSql(instance) + "_task(@archiveDate)";
			}
			var sql = "";
			sql += " (SELECT ISNULL(SUM(hours),0)+ISNULL(SUM(additional_hours),0) AS hours " +
			       " FROM " + worktimeTrackingHoursTable + " w " +
			       " INNER JOIN " + taskTable + " t ON w.task_item_id = t.item_id ";


			var sql_where = " WHERE t.owner_item_id = pf.item_id ";

			if (FieldConfig.ContainsKey("@ProcessActualEffortTaskFilterListColumnId") &&
			    ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTaskFilterListColumnId"]).Value != null &&
			    ((int[]) ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortAllowedListValuesItemId"]).Value).Length >
			    0) {
				//var temp = (int[])((SubQueryConfigValue)FieldConfig["@AllowedListValues"]).Value;
				//string values = string.Join(",", temp.Select(x => x.ToString()).ToArray());

				sql += " INNER JOIN " + processSelectionsTable(isArchive, "@ProcessActualEffortTaskFilterListColumnId") + " AS list1_param ON list1_param.item_id = t.item_id AND list1_param.item_column_id = @ProcessActualEffortTaskFilterListColumnId AND list1_param.selected_item_id IN (@ProcessActualEffortAllowedListValuesItemId) ";

				//sql_where += " AND list1_param.selected_item_id IN(" + values + ") ";
			}

			if (FieldConfig.ContainsKey("@ProcessActualEffortTimeType") &&
			    (int) ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTimeType"]).Value > 0 &&
			    (int) ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTimeCount"]).Value > 0) {
				DateTime? startDate;
				DateTime? endDate;
				var current = DateTime.Now;
				var offset = (int) ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTimeOffset"]).Value;
				var count = (int) ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTimeCount"]).Value;

				switch ((int) ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTimeType"]).Value) {
					case 1:
						startDate = new DateTime(current.AddYears(offset).Year, 1, 1);
						endDate = startDate.Value.AddYears(count);
						break;
					case 2:
						var startTemp2 = new DateTime(current.AddMonths(offset * 3).Year,
							current.AddMonths(offset * 3).Month, 1);

						var quarterNumber = (startTemp2.Month - 1) / 3 + 1;
						var firstDayOfQuarter = new DateTime(startTemp2.Year, (quarterNumber - 1) * 3 + 1, 1);

						startDate = firstDayOfQuarter;
						endDate = firstDayOfQuarter.AddMonths(count * 3);
						break;
					case 3:
						startDate = new DateTime(current.AddMonths(offset).Year, current.AddMonths(offset).Month, 1);
						endDate = startDate.Value.AddMonths(count);
						break;
					case 4:
						var startTemp4 = new DateTime(current.AddDays(offset).Year, current.AddDays(offset).Month,
							current.AddDays(offset * 7).Day);

						var ci = Thread.CurrentThread.CurrentCulture;
						ci.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
						var fdow = ci.DateTimeFormat.FirstDayOfWeek;

						var tempDate = startTemp4.AddDays(-(DateTime.Today.DayOfWeek - fdow));

						startDate = tempDate;
						endDate = tempDate.AddDays(count * 7);
						break;
					case 5:
						startDate = new DateTime(current.AddDays(offset).Year, current.AddDays(offset).Month,
							current.AddDays(offset).Day);
						endDate = startDate.Value.AddDays(count);
						break;
					default:
						startDate = DateTime.Now;
						endDate = DateTime.Now;
						break;
				}

				sql_where += " AND w.date>='" +
				             startDate.Value.ToString("yyyy-MM-dd") + "' AND w.date<'" + endDate.Value.ToString("yyyy-MM-dd") + "'";
			}

			sql += sql_where;
			sql += ")";

			return sql;
		}


		protected override void InitializeFieldConfig() {
			var db = new Connections(authenticatedSession);

			var timeOptions = new List<object> {
				new Dictionary<string, object> {{"Variable", "YEARS"}, {"Value", 1}},
				new Dictionary<string, object> {{"Variable", "QUARTERS"}, {"Value", 2}},
				new Dictionary<string, object> {{"Variable", "MONTHS"}, {"Value", 3}},
				new Dictionary<string, object> {{"Variable", "WEEKS"}, {"Value", 4}},
				new Dictionary<string, object> {{"Variable", "DAYS"}, {"Value", 5}}
			};

			var listCols = new List<ItemColumn>();
			object cols = new ItemColumns(instance, "task", authenticatedSession).GetItemColumns(false);

			foreach (var c in (List<ItemColumn>) cols) {
				if (c.DataType == 6) {
					listCols.Add(c);
				}
			}

			AddFieldConfig("@ProcessActualEffortTaskFilterListColumnId",
				new SubQueryConfigValue("@ProcessActualEffortTaskFilterListColumnId", "int", SubQueryOptionType.List,
					"ItemColumnId", "Variable", listCols, true));

			if (FieldConfig.ContainsKey("@ProcessActualEffortTaskFilterListColumnId") &&
			    ((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTaskFilterListColumnId"]).Value != null &&
			    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTaskFilterListColumnId"]).Value) > 0) {
				var listProcess = db.ExecuteScalar(
					"SELECT data_additional FROM item_columns WHERE item_column_id = " +
					Conv.ToInt(((SubQueryConfigValue) FieldConfig["@ProcessActualEffortTaskFilterListColumnId"]).Value));

				var listVals =
					db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_" + listProcess);

				AddFieldConfig("@ProcessActualEffortAllowedListValuesItemId",
					new SubQueryConfigValue("@ProcessActualEffortAllowedListValuesItemId", "int_array",
						SubQueryOptionType.MultiSelectList, "item_id", "list_item", listVals));
			} else {
				AddFieldConfig("@ProcessActualEffortAllowedListValuesItemId",
					new SubQueryConfigValue("@ProcessActualEffortAllowedListValuesItemId", "int_array",
						SubQueryOptionType.MultiSelectList,
						"item_id", "list_item", new List<Dictionary<string, object>>()));
			}

			AddFieldConfig("@ProcessActualEffortTimeType",
				new SubQueryConfigValue("@ProcessActualEffortTimeType", "int", SubQueryOptionType.List, "Value",
					"Variable", timeOptions)
			);

			AddFieldConfig("@ProcessActualEffortTimeOffset",
				new SubQueryConfigValue("@ProcessActualEffortTimeOffset", "int", SubQueryOptionType.Int, "ItemColumnId",
					"Variable")
			);

			AddFieldConfig("@ProcessActualEffortTimeCount",
				new SubQueryConfigValue("@ProcessActualEffortTimeCount", "int", SubQueryOptionType.Int, "ItemColumnId",
					"Variable")
			);
		}
	}
}