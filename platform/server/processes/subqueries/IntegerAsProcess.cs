﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.subqueries {
	public class IntegerAsProcessConfig : SubQueryConfigBase {
		public IntegerAsProcessConfig() {
			Config.Name = "IntegerAsProcess";
			Config.Value = "IntegerAsProcess";
			Config.TranslationKey = "INTEGER_PROCESS_SQ";
			Config.DataType = ItemColumn.ColumnType.Process;
			SubProcess = "user";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var data = GetConfigData();
			SubProcess = Conv.ToStr(data["@ProcessName"]);
			var name = Conv.ToStr(data["@ColumnName"]);
			return "(SELECT pf." + name + ")";
		}

		protected override void InitializeFieldConfig() {
			var db = new Connections(authenticatedSession);

			var listCols = new List<ItemColumn>();
			var cols = new ItemColumns(instance, process, authenticatedSession).GetItemColumns(false);
			foreach (var c in cols) {
				if (c.DataType == 1) listCols.Add(c);
			}

			AddFieldConfig("@ColumnName",
				new SubQueryConfigValue("@ColumnName", "string", SubQueryOptionType.List,
					"Name", "Variable", listCols, true));


			var processes = new Processes(instance, authenticatedSession);
			AddFieldConfig("@ProcessName",
				new SubQueryConfigValue("@ProcessName", "string", SubQueryOptionType.List,
					"ProcessName", "Variable", processes.GetProcesses(), true));

		}
	}


	
}