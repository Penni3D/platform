﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries
{
	public class SubQueryYearlyPayback : SubQueryConfigBase
	{
		public SubQueryYearlyPayback() {
			Config.Name = "BudgetItemYearPayback";
			Config.Value = "BudgetItemYearPayback";
			Config.TranslationKey = "BUDGET_ITEM_YEAR_PAYBACK";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0)
		{
			var db = new Connections(authenticatedSession);

			var payback_time_type = "payback_incorrect_configuration";
			if((SubQueryConfigValue)FieldConfig["@PaybackModes"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackModes"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackModes"]).Value).Length > 0)
				payback_time_type = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackModes"]).Value);
			var payback_item_type_ids = "payback_incorrect_configuration";
			if ((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"]).Value).Length > 0)
				payback_item_type_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"]).Value); 
			var payback_item_category_income_ids = "-1"; 
			if ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"]).Value).Length > 0)
				payback_item_category_income_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"]).Value); 
			var payback_item_category_expense_ids = "-1"; 
			if ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"]).Value).Length > 0)
				payback_item_category_expense_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"]).Value);

			return "(dbo.GetPaybackYears(pf.item_id, '"+ payback_item_category_income_ids + "', '" + payback_item_category_expense_ids + "', '" + payback_item_type_ids + "', " + Conv.ToInt(payback_time_type) + ", null))";
		}

		public override string GetArchiveSql(string instance) {
			var payback_time_type = "payback_incorrect_configuration";
			if ((SubQueryConfigValue)FieldConfig["@PaybackModes"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackModes"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackModes"]).Value).Length > 0)
				payback_time_type = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackModes"]).Value);
			var payback_item_type_ids = "payback_incorrect_configuration";
			if ((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"]).Value).Length > 0)
				payback_item_type_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackItemTypesItemId"]).Value);
			var payback_item_category_income_ids = "-1"; 
			if ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"]).Value).Length > 0)
				payback_item_category_income_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryIncomeTypesItemId"]).Value);
			var payback_item_category_expense_ids = "-1";
			if ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"]).Value).Length > 0)
				payback_item_category_expense_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PaybackItemCategoryExpenseTypesItemId"]).Value);

			return "(dbo.GetPaybackYears(pf.item_id, '" + payback_item_category_income_ids + "', '" + payback_item_category_expense_ids + "', '" + payback_item_type_ids + "', " + Conv.ToInt(payback_time_type) + ", @archiveDate))";
		}

		protected override void InitializeFieldConfig()
		{
			var db = new Connections(authenticatedSession);

			if (true) {
				var timeOptions = new List<object> {
					new Dictionary<string, object> {{"Variable", "YEAR"}, {"Value", 0}},
					new Dictionary<string, object> {{"Variable", "QUARTER"}, {"Value", 1}},
					new Dictionary<string, object> {{"Variable", "MONTH"}, {"Value", 2}}					
				};
				AddFieldConfig("@PaybackModes",
					new SubQueryConfigValue("@PaybackModes", "int_array", SubQueryOptionType.List,
						"Value", "Variable", timeOptions));

				AddFieldConfig("@PaybackItemTypesItemId",
						new SubQueryConfigValue("@PaybackItemTypesItemId", "int_array", SubQueryOptionType.List,
						"item_id", "list_item", db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_list_budget_item_type")));

				AddFieldConfig("@PaybackItemCategoryIncomeTypesItemId",
						new SubQueryConfigValue("@PaybackItemCategoryIncomeTypesItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_list_budget_category")));

				AddFieldConfig("@PaybackItemCategoryExpenseTypesItemId",
						new SubQueryConfigValue("@PaybackItemCategoryExpenseTypesItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_list_budget_category")));

			}
		}
	}
	

}