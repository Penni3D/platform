using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;
using System.Text;

namespace Keto5.x.platform.server.processes.subqueries {

	public class SQ_ProcessesListUsersOfGroup : SubQueryConfigBase
	{
		public SQ_ProcessesListUsersOfGroup() : base()
		{
			Config.Name = "SQ_ProcessesListUsersOfGroup";
			Config.Value = "SQ_ProcessesListUsersOfGroup";
			Config.TranslationKey = "SQ_PLUGPROCESSESLISTUSERSOFGROUP";
			Config.DataType = ItemColumn.ColumnType.Process;
			SubProcess = "user";
		}

		protected override void InitializeFieldConfig()
		{
			var db = new Connections(authenticatedSession);

			// choose list of this process
			object process_list_columns =
				db.GetDatatableDictionary(
					"SELECT " +
					"	ItemColumnId = ic.item_column_id" +
					"	,Translation = COALESCE((SELECT TOP 1 translation FROM process_translations pt WHERE pt.code = '" + authenticatedSession.locale_id + "' AND pt.variable = ic.variable AND pt.process = ic.process AND pt.instance = '" + instance + "'), ic.name)" +
					" FROM item_columns ic " +
					" WHERE ic.data_type IN (6) AND ic.process = '" + process + "'");
			AddFieldConfig("@PLUGProcessListColumnId", new SubQueryConfigValue("@PLUGProcessListColumnId", "int", SubQueryOptionType.List, "ItemColumnId", "Translation", process_list_columns, true));

			string process_list_column_id = "";
			if ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value != null)
				process_list_column_id = Conv.ToStr(((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value);

			if (Conv.ToStr(process_list_column_id).Length > 0)
			{
				string list_name_of_selected_column = Conv.ToStr(db.ExecuteScalar("SELECT data_additional FROM item_columns WHERE item_column_id = " + process_list_column_id + " "));

				// choose all needed group columns of that list
				List<Dictionary<string, object>> target_list_company_user_columns =
					db.GetDatatableDictionary(
						"SELECT " +
						"	ItemColumnId = ic.item_column_id " +
						"	,Translation = " +
						"		COALESCE((SELECT TOP 1 translation FROM process_translations pt WHERE pt.code = '" + authenticatedSession.locale_id + "' AND pt.variable = ic.variable AND pt.process = ic.process AND pt.instance = '" + instance + "'), ic.name) " +
						"		+ ' (' + COALESCE((SELECT TOP 1 translation FROM process_translations pt WHERE pt.code = '" + authenticatedSession.locale_id + "' AND pt.variable = ic.process AND pt.process = ic.process AND pt.instance = '" + instance + "'), ic.name) + ')' " +
						" FROM item_columns ic " +
						" WHERE ic.data_type IN (5) AND ic.data_additional = 'user_group' AND ic.process = '" + list_name_of_selected_column + "' AND ic.instance = '" + instance + "' ");
				AddFieldConfig("@PLUGListsGroupsColumnId", new SubQueryConfigValue("@PLUGListsGroupsColumnId", "int_array", SubQueryOptionType.MultiSelectList, "ItemColumnId", "Translation", target_list_company_user_columns));

				if (target_list_company_user_columns.Count == 0)
					AddFieldConfig("@PLUGNoUserGroupsColumnId", new SubQueryConfigValue("@PLUGNoUserGroupsColumnId", "int", SubQueryOptionType.Int, "ItemColumnId", "Translation", null));
			}
			else
				AddFieldConfig("@PLUGListsGroupsColumnId", new SubQueryConfigValue("@PLUGListsGroupsColumnId", "int_array", SubQueryOptionType.List, "ItemColumnId", "Translation", new List<Dictionary<string, object>>()));
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0)
		{
			var db = new Connections(authenticatedSession);

			int group_column_id_users = (int)(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE process = 'user_group' AND name = 'user_id' AND instance = '" + instance + "' ")); // 19

			string process_list_column_id = "";
			if ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value != null)
				process_list_column_id = Conv.ToStr(((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value);
			if (Conv.ToStr(process_list_column_id).Length == 0)
				process_list_column_id = "incorrect_configuration";

			string list_group_column_ids = "";
			if ((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value).Length > 0)
				list_group_column_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value);
			if (Conv.ToStr(list_group_column_ids).Length == 0)
				list_group_column_ids = "incorrect_configuration";

			StringBuilder sql_str = new StringBuilder();
			// This will miserably fail if an all_group is selected. However that kind of group should not be valid here
			sql_str.AppendLine("STUFF((SELECT DISTINCT ', '+CAST(idps_groups_user.selected_item_id AS NVARCHAR(15)) ");
			sql_str.AppendLine(" FROM item_data_process_selections idps_groups_user WHERE idps_groups_user.item_id IN (");
			sql_str.AppendLine("	SELECT idps_lists_groups.selected_item_id FROM item_data_process_selections idps_lists_groups WHERE idps_lists_groups.item_id IN ( ");
			sql_str.AppendLine("		SELECT idps_process_list_selections.selected_item_id FROM item_data_process_selections idps_process_list_selections WHERE idps_process_list_selections.item_id = pf.item_id AND idps_process_list_selections.item_column_id = " + process_list_column_id + " ");
			sql_str.AppendLine("	) AND idps_lists_groups.item_column_id IN (" + list_group_column_ids + ") ");
			sql_str.AppendLine(" ) AND idps_groups_user.item_column_id = " + group_column_id_users + " ");
			sql_str.AppendLine(" FOR XML PATH('')),1,1,'')");

			//Diag.LogToConsole("PLUG debug: " + sql_str.ToString(), Diag.LogSeverities.Warning);
			return "(" + sql_str.ToString() + ")";
		}
		public override string GetArchiveSql(string instance)
		{
			var db = new Connections(authenticatedSession);

			int group_column_id_users = (int)(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE process = 'user_group' AND name = 'user_id' AND instance = '" + instance + "' ")); // 19

			string process_list_column_id = "";
			if ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value != null)
				process_list_column_id = Conv.ToStr(((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value);
			if (Conv.ToStr(process_list_column_id).Length == 0)
				process_list_column_id = "incorrect_configuration";

			string list_group_column_ids = "";
			if ((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value).Length > 0)
				list_group_column_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value);
			if (Conv.ToStr(list_group_column_ids).Length == 0)
				list_group_column_ids = "incorrect_configuration";

			StringBuilder sql_str = new StringBuilder();
			// This will miserably fail if an all_group is selected. However that kind of group should not be valid here
			sql_str.AppendLine("STUFF((SELECT DISTINCT ', '+CAST(idps_groups_user.selected_item_id AS NVARCHAR(15)) ");
			sql_str.AppendLine(" FROM get_item_data_process_selections(@archiveDate) idps_groups_user WHERE idps_groups_user.item_id IN (");
			sql_str.AppendLine("	SELECT idps_lists_groups.selected_item_id FROM get_item_data_process_selections(@archiveDate) idps_lists_groups WHERE idps_lists_groups.item_id IN ( ");
			sql_str.AppendLine("		SELECT idps_process_list_selections.selected_item_id FROM get_item_data_process_selections(@archiveDate) idps_process_list_selections WHERE idps_process_list_selections.item_id = pf.item_id AND idps_process_list_selections.item_column_id = " + process_list_column_id + " ");
			sql_str.AppendLine("	) AND idps_lists_groups.item_column_id IN (" + list_group_column_ids + ") ");
			sql_str.AppendLine(" ) AND idps_groups_user.item_column_id = " + group_column_id_users + " ");
			sql_str.AppendLine(" FOR XML PATH('')),1,1,'')");

			//Diag.LogToConsole("PLUG debug: " + sql_str.ToString(), Diag.LogSeverities.Warning);
			return "(" + sql_str.ToString() + ")";
		}
		public override string GetFilterSql(string instance = "")
		{
			var db = new Connections(authenticatedSession);

			int group_column_id_users = (int)(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE process = 'user_group' AND name = 'user_id' AND instance = '" + instance + "' ")); // 19

			string process_list_column_id = "";
			if ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value != null)
				process_list_column_id = Conv.ToStr(((SubQueryConfigValue)FieldConfig["@PLUGProcessListColumnId"]).Value);
			if (Conv.ToStr(process_list_column_id).Length == 0)
				process_list_column_id = "incorrect_configuration";

			string list_group_column_ids = "";
			if ((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value).Length > 0)
				list_group_column_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@PLUGListsGroupsColumnId"]).Value);
			if (Conv.ToStr(list_group_column_ids).Length == 0)
				list_group_column_ids = "incorrect_configuration";

			StringBuilder sql_str = new StringBuilder();
			// This will miserably fail if an all_group is selected. However that kind of group should not be valid here
			sql_str.AppendLine("( ");
			sql_str.AppendLine("SELECT COUNT(idps_groups_user.selected_item_id) ");
			sql_str.AppendLine(" FROM item_data_process_selections idps_groups_user WHERE idps_groups_user.item_id IN (");
			sql_str.AppendLine("	SELECT idps_lists_groups.selected_item_id FROM item_data_process_selections idps_lists_groups WHERE idps_lists_groups.item_id IN ( ");
			sql_str.AppendLine("		SELECT idps_process_list_selections.selected_item_id FROM item_data_process_selections idps_process_list_selections WHERE idps_process_list_selections.item_id = pf.item_id AND idps_process_list_selections.item_column_id = " + process_list_column_id + " ");
			sql_str.AppendLine("	) AND idps_lists_groups.item_column_id IN (" + list_group_column_ids + ") ");
			sql_str.AppendLine(" ) AND idps_groups_user.item_column_id = " + group_column_id_users + " ");
			sql_str.AppendLine("   AND idps_groups_user.selected_item_id IN (##idString##) ");
			sql_str.AppendLine(") > 0");

			//Diag.LogToConsole("PLUG debug: " + sql_str.ToString(), Diag.LogSeverities.Warning);
			return "(" + sql_str.ToString() + ")";
		}
	}

}