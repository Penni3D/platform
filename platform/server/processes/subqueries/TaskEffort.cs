﻿using System;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryTaskEffortConfig : SubQueryConfigBase {
		public SubqueryTaskEffortConfig() {
			Config.Name = "TaskEffort";
			Config.Value = "TaskEffort";
			Config.TranslationKey = "TASK_EFFORT_SQ";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return GetCombinedSql(instance);
		}

		public override string GetArchiveSql(string instance) {
			return GetCombinedSql(instance, true);
		}

		private string GetCombinedSql(string instance, bool isArchive = false) {
			var taskTable = "_" + Conv.ToSql(instance) + "_task";
			var taskEffortTable = Conv.ToSql("_" + instance + "_task_user_effort ");
			var taskCompEffortTable = Conv.ToSql("_" + instance + "_task_competency_effort ");
			if (isArchive) { 
				taskEffortTable = "get__" + Conv.ToSql(instance) + "_task_user_effort(@archiveDate)";
				taskCompEffortTable = "get__" + Conv.ToSql(instance) + "_task_competency_effort(@archiveDate)";
				taskTable = "get__" + Conv.ToSql(instance) + "_task(@archiveDate)";
			}
			
			var sql1 =
				"(SELECT SUM(CASE WHEN t.task_type = 0 THEN 0 ELSE ISNULL(effort,0) END) FROM item_data_process_selections idps " +
				" INNER JOIN " + taskEffortTable + " eff ON idps.link_item_id = eff.item_id " +
				" INNER JOIN  " + taskTable + " t ON idps.item_id = t.item_id " +
				" WHERE idps.item_id IN (select item_id FROM get_" + Conv.ToSql(instance) +
				"_recursive_tasks(pf.item_id)) AND item_column_id = @TaskEffortResponsiblesColumnId)";
			if (Conv.ToInt(((SubQueryConfigValue)FieldConfig["@TaskEffortResponsiblesColumnId"]).Value) == 0)
				sql1 = "(SELECT 0)";


			var sql2 =
				"(SELECT SUM(CASE WHEN t.task_type = 0 THEN 0 ELSE ISNULL(effort,0) END) FROM item_data_process_selections idps " +
				" INNER JOIN " + taskCompEffortTable + " eff ON idps.link_item_id = eff.item_id " +
				" INNER JOIN  " + taskTable + " t ON idps.item_id = t.item_id " +
				" WHERE idps.item_id IN (select item_id FROM get_" + Conv.ToSql(instance) +
				"_recursive_tasks(pf.item_id)) AND item_column_id = @TaskEffortCompetenciesColumnId)";
			if (Conv.ToInt(((SubQueryConfigValue)FieldConfig["@TaskEffortCompetenciesColumnId"]).Value) == 0)
				sql2 = "(SELECT 0)";

			return "(ISNULL(" + sql1 + ",0) + ISNULL(" + sql2 + ",0))";
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) {
				return;
			}

			AddFieldConfig("@TaskEffortResponsiblesColumnId",
				new SubQueryConfigValue("@TaskEffortResponsiblesColumnId", "int", SubQueryOptionType.List,
					"ItemColumnId", "Variable",
					new ItemColumns(instance, "task", authenticatedSession).GetItemColumns(false))
			);
			AddFieldConfig("@TaskEffortCompetenciesColumnId",
				new SubQueryConfigValue("@TaskEffortCompetenciesColumnId", "int", SubQueryOptionType.List,
					"ItemColumnId", "Variable",
					new ItemColumns(instance, "task", authenticatedSession).GetItemColumns(false))
			);
		}
	}

	public class SubqueryTaskActualEffortConfig : SubQueryConfigBase {
		public SubqueryTaskActualEffortConfig() {
			Config.Name = "TaskActualEffort";
			Config.Value = "TaskActualEffort";
			Config.TranslationKey = "TASK_ACTUAL_EFFORT";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var worktimeTrackingHoursTable = "_" + Conv.ToSql(instance) + "_WORKTIME_TRACKING_HOURS";
			return "(SELECT SUM(hours) AS hours FROM _" + Conv.ToSql(instance) + "_task t " +
			       "LEFT JOIN " + worktimeTrackingHoursTable +
			       " h ON t.item_id = h.task_item_id WHERE t.item_id = pf.item_id)";
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) { }
		}
	}
}