﻿using System;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries
{
	public class SubQueryYearlyNPV : SubQueryConfigBase
	{
		public SubQueryYearlyNPV() {
			Config.Name = "BudgetItemYearNPV";
			Config.Value = "BudgetItemYearNPV";
			Config.TranslationKey = "BUDGET_ITEM_YEAR_NPV";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0)
		{
			var db = new Connections(authenticatedSession);

			// npv_incorrect_configuration == Attempted that startup won't crash, although incorrectly configured if that's the case

			var budget_item_type_ids = "npv_incorrect_configuration";
			if ((SubQueryConfigValue)FieldConfig["@BudgetItemTypeValuesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@BudgetItemTypeValuesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@BudgetItemTypeValuesItemId"]).Value).Length > 0)
				budget_item_type_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@BudgetItemTypeValuesItemId"]).Value); 

			var budget_item_category_income_ids = "-1"; // -1 = enable to be used without this //npv_incorrect_configuration
			if ((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryIncomeValuesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryIncomeValuesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryIncomeValuesItemId"]).Value).Length > 0)
				budget_item_category_income_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryIncomeValuesItemId"]).Value); 

			var budget_item_category_expense_ids = "-1"; // -1 = enable to be used without this //npv_incorrect_configuration
			if ((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryExpenseValuesItemId"] != null && ((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryExpenseValuesItemId"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryExpenseValuesItemId"]).Value).Length > 0)
				budget_item_category_expense_ids = string.Join(",", (int[])((SubQueryConfigValue)FieldConfig["@BudgetItemCategoryExpenseValuesItemId"]).Value); 

			var process_npv_discount_value_col_name = "npv_incorrect_configuration";
			if ((SubQueryConfigValue)FieldConfig["@NPVDiscountValueIntFieldColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@NPVDiscountValueIntFieldColumnId"]).Value != null && (int)((SubQueryConfigValue)FieldConfig["@NPVDiscountValueIntFieldColumnId"]).Value > 0)
			{
				var process_npv_discount_value_col_id = (int)((SubQueryConfigValue)FieldConfig["@NPVDiscountValueIntFieldColumnId"]).Value; //Diag.LogToConsole("debug: process_npv_discount_value_col_id: " + process_npv_discount_value_col_id, Diag.LogSeverities.Warning);
				process_npv_discount_value_col_name = (string)db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " + process_npv_discount_value_col_id); 
			}

			var process_npv_start_year_col_name = "npv_incorrect_configuration";
			if ((SubQueryConfigValue)FieldConfig["@NPVStartYearIntFieldColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@NPVStartYearIntFieldColumnId"]).Value != null && (int)((SubQueryConfigValue)FieldConfig["@NPVStartYearIntFieldColumnId"]).Value > 0)
			{
				var process_npv_start_year_col_id = (int)((SubQueryConfigValue)FieldConfig["@NPVStartYearIntFieldColumnId"]).Value; //Diag.LogToConsole("debug: process_npv_start_year_col_id: " + process_npv_start_year_col_id, Diag.LogSeverities.Warning);
				process_npv_start_year_col_name = (string)db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " + process_npv_start_year_col_id); 
			}

			var process_npv_number_of_years_col_name = "npv_incorrect_configuration";
			if ((SubQueryConfigValue)FieldConfig["@NPVNumberOfYearsIntFieldColumnId"] != null && ((SubQueryConfigValue)FieldConfig["@NPVNumberOfYearsIntFieldColumnId"]).Value != null && (int)((SubQueryConfigValue)FieldConfig["@NPVNumberOfYearsIntFieldColumnId"]).Value > 0)
			{
				var process_npv_number_of_years_col_id = (int)((SubQueryConfigValue)FieldConfig["@NPVNumberOfYearsIntFieldColumnId"]).Value; //Diag.LogToConsole("debug: process_npv_number_of_years_col_id: " + process_npv_number_of_years_col_id, Diag.LogSeverities.Warning);
				process_npv_number_of_years_col_name = (string)db.ExecuteScalar("SELECT name from item_columns WHERE item_column_id = " + process_npv_number_of_years_col_id); 
			}

			//Diag.LogToConsole("debug: @BudgetItemTypeValuesItemId: " + budget_item_type_ids, Diag.LogSeverities.Warning);
			//Diag.LogToConsole("debug: @BudgetItemCategoryIncomeValuesItemId" + budget_item_category_income_ids, Diag.LogSeverities.Warning);
			//Diag.LogToConsole("debug: @BudgetItemCategoryExpenseValuesItemId" + budget_item_category_expense_ids, Diag.LogSeverities.Warning);
			//Diag.LogToConsole("debug: process_npv_discount_value_col_name: " + process_npv_discount_value_col_name, Diag.LogSeverities.Warning);
			//Diag.LogToConsole("debug: process_npv_start_year_col_name: " + process_npv_start_year_col_name, Diag.LogSeverities.Warning);
			//Diag.LogToConsole("debug: process_npv_number_of_years_col_name: " + process_npv_number_of_years_col_name, Diag.LogSeverities.Warning);

			var exec_sql = "(" +
				" SELECT [dbo].[f_" + instance + "_BudgetItemYearNPV] (" +
				"  pf.item_id" +
				"  ,CAST(pf." + process_npv_discount_value_col_name + "/100.0 as DECIMAL(19,4)) " +
				"  ,pf." + process_npv_start_year_col_name + " " +
				"  ,pf." + process_npv_number_of_years_col_name + " " +
				"  ," + (budget_item_type_ids == "npv_incorrect_configuration" ? budget_item_type_ids : "'" + budget_item_type_ids + "' ") +
				"  ," + (budget_item_category_income_ids == "npv_incorrect_configuration" ? budget_item_category_income_ids : "'" + budget_item_category_income_ids + "' ") +
				"  ," + (budget_item_category_expense_ids == "npv_incorrect_configuration" ? budget_item_category_expense_ids : "'" + budget_item_category_expense_ids + "' ") +
				" ) " +
				")";

			//Diag.LogToConsole("debug: " + exec_sql, Diag.LogSeverities.Warning);

			return exec_sql;
		}

		public override string GetArchiveSql(string instance) {
			throw new NotImplementedException("SubQuery_YearlyNPV archive is not yet implemented ");
		}

		protected override void InitializeFieldConfig()
		{
			//Diag.LogToConsole("debug: " + process, Diag.LogSeverities.Warning); // -> projekti

			var db = new Connections(authenticatedSession);

			if (true)
			{ // budget item fields
				AddFieldConfig("@BudgetItemTypeValuesItemId",
					new SubQueryConfigValue("@BudgetItemTypeValuesItemId", "int_array", SubQueryOptionType.MultiSelectList,
						"item_id", "list_item", db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_list_budget_item_type")));

				AddFieldConfig("@BudgetItemCategoryIncomeValuesItemId",
						new SubQueryConfigValue("@BudgetItemCategoryIncomeValuesItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_list_budget_category")));

				AddFieldConfig("@BudgetItemCategoryExpenseValuesItemId",
						new SubQueryConfigValue("@BudgetItemCategoryExpenseValuesItemId", "int_array", SubQueryOptionType.MultiSelectList,
							"item_id", "list_item", db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_list_budget_category")));
			} // budget item fields

			if (true)
			{ // linking process fields (that should be in the project_item_id)
				AddFieldConfig("@NPVDiscountValueIntFieldColumnId",
					new SubQueryConfigValue("@NPVDiscountValueIntFieldColumnId", "int", SubQueryOptionType.List, "item_column_id", "result",
							db.GetDatatableDictionary(
								"SELECT item_column_id, COALESCE(translation, ic.name) + ' ('+ic.process+')' as result " +
								" FROM item_columns ic LEFT JOIN process_translations pt ON ic.variable = pt.variable and code = '" + authenticatedSession.locale_id + "' " +
								" WHERE ic.process = '" + Conv.ToSql(process) + "' AND data_type IN (1) "))
					);

				AddFieldConfig("@NPVStartYearIntFieldColumnId",
						new SubQueryConfigValue("@NPVStartYearIntFieldColumnId", "int", SubQueryOptionType.List, "item_column_id", "result",
								db.GetDatatableDictionary(
									"SELECT item_column_id, COALESCE(translation, ic.name) + ' ('+ic.process+')' as result " +
									" FROM item_columns ic LEFT JOIN process_translations pt ON ic.variable = pt.variable and code = '" + authenticatedSession.locale_id + "' " +
									" WHERE ic.process = '" + Conv.ToSql(process) + "' AND data_type IN (1) "))
						);

				AddFieldConfig("@NPVNumberOfYearsIntFieldColumnId",
					new SubQueryConfigValue("@NPVNumberOfYearsIntFieldColumnId", "int", SubQueryOptionType.List, "item_column_id", "result",
							db.GetDatatableDictionary(
								"SELECT item_column_id, COALESCE(translation, ic.name) + ' ('+ic.process+')' as result " +
								" FROM item_columns ic LEFT JOIN process_translations pt ON ic.variable = pt.variable and code = '" + authenticatedSession.locale_id + "' " +
								" WHERE ic.process = '" + Conv.ToSql(process) + "' AND data_type IN (1) "))
					);
			} // linking process fields

		}
	}
	

}