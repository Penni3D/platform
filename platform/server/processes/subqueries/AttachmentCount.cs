using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;
namespace Keto5.x.platform.server.processes.subqueries {
	public class AttachmentCountConfig : SubQueryConfigBase {
		public AttachmentCountConfig() {
			Config.Name = "AttachmentCount";
			Config.Value = "AttachmentCount";
			Config.TranslationKey = "ATTACHMENT_COUNT";
			Config.DataType = ItemColumn.ColumnType.Int;
		}
		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT COUNT (*) as c FROM attachments WHERE item_id = pf.item_id AND item_column_id = @AttachlinkColumnId)";
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0)
				return;
			var db = new Connections(authenticatedSession);
			var spSql =
				"SELECT item_column_id, ic.process + ' : ' + coalesce(translation, ic.name) AS result FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
				authenticatedSession.locale_id + "' WHERE ic.process = '" + Conv.ToSql(process) +
				"'  AND (data_type =10 OR data_type = 15)  ";
			AddFieldConfig("@AttachlinkColumnId",
				new SubQueryConfigValue("@AttachlinkColumnId", "int", SubQueryOptionType.List, "item_column_id", "result",
					db.GetDatatableDictionary(spSql),
					true)

			);

			var types = new List<Dictionary<string, object>>();

			var dict1 = new Dictionary<string, object>();
			dict1["name"] = "DATA_ATTACHMENT";
			dict1["value"] = 1;

			var dict2 = new Dictionary<string, object>();
			dict2["name"] = "DATA_LINK";
			dict2["value"] = 2;
			types.Add(dict1);
			types.Add(dict2);

			AddFieldConfig("@ATTACHLINKCOLUMNTYPE",
				new SubQueryConfigValue("@ATTACHLINKCOLUMNTYPE", "int", SubQueryOptionType.List, "value", "name",
					types,
					true)

			);
		}
	}
}