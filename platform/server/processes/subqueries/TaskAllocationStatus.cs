﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryTaskAllocationStatusConfig : SubQueryConfigBase {
		public SubqueryTaskAllocationStatusConfig() {
			Config.Name = "TaskAllocationStatus";
			Config.Value = "TaskAllocationStatus";
			Config.TranslationKey = "TASK_ALLOCATION_STATUS";
			Config.DataType = ItemColumn.ColumnType.Int;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT min(status) FROM _" + instance + "_allocation WHERE task_item_id = pf.item_id)";
		}

		public override string GetArchiveSql(string instance) {
			return "(SELECT min(status) FROM get__" + instance + "_allocation(@archiveDate) WHERE task_item_id = pf.item_id)";
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) { }
		}
	}
}