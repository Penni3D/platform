﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryTasksRecursive : SubQueryConfigBase {
		public SubqueryTasksRecursive() {
			Config.Name = "TasksRecursive";
			Config.Value = "TasksRecursive";
			Config.TranslationKey = "TASKS_RECURSIVE";
			Config.DataType = ItemColumn.ColumnType.Nvarchar;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0)
		{
			return " ( STUFF((SELECT ' - ' + title  " +
			       "               	FROM get_" + instance + "_recursive_tasks_up(pf.item_id)  i " +
			       "               	inner join _" + instance + "_task t on i.item_id = t.item_id " +
			       "				where i.item_id <> pf.item_id order by level desc " +
			       "               	FOR XML PATH('')), 1, 3, '')  ) ";
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) { }
		}
	}
}