﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryUserActiveConfig : SubQueryConfigBase {
		public SubqueryUserActiveConfig() {
			Config.Name = "UserActive";
			Config.Value = "UserActive";
			Config.TranslationKey = "USER_ACTIVE";
			Config.DataType = ItemColumn.ColumnType.List;
			this.SubProcess = "list_generic_yes_no";
		}
		public override string GetFilterSql(string instance) {
			return GetSql(instance) + " IN ( ##idString## ) ";
		}
		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT TOP 1 item_id FROM _" + instance + "_list_generic_yes_no l WHERE l.value = pf.active)";
		}
		public override List<Dictionary<string, object>> GetValues(string instance, int itemColumnId) {
			var db = new Connections(authenticatedSession);
			return db.GetDatatableDictionary("SELECT * FROM _" + instance + "_list_generic_yes_no");
		}
		protected override void InitializeFieldConfig() { 
		
		}
	}
}