﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryProcessAllocationConfig : SubQueryConfigBase {
		public SubqueryProcessAllocationConfig() {
			Config.Name = "ProcessAllocation";
			Config.Value = "ProcessAllocation";
			Config.TranslationKey = "PROCESS_ALLOCATION_SQ";
			Config.DataType = ItemColumn.ColumnType.Float;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return GetSqlCombined(instance);
		}

		public override string GetArchiveSql(string instance) {
			return GetSqlCombined(instance, true);
		}

		private string GetSqlCombined(string instance, bool isArchive = false) {
			var db = new Connections(authenticatedSession);
			var allocationTable = Conv.ToSql("_" + instance + "_allocation");
			if (isArchive) {
				allocationTable = "get__" + instance + "_allocation(@archiveDate)";
			}

			var durationsTable = "allocation_durations";
			if (isArchive) {
				durationsTable = "get_allocation_durations(@archiveDate)";
			}

			var taskTable = Conv.ToSql("_" + instance + "_task");
			if (isArchive) {
				taskTable = "get__" + Conv.ToSql(instance) + "_task(@archiveDate)";
			}

			var sql = "";
			sql += " (SELECT ISNULL(SUM(hours),0) AS hours " +
			       " FROM " + allocationTable + " a " +
			       " INNER JOIN " + durationsTable + " d ON a.item_id = d.allocation_item_id ";

			var sql_where = " WHERE a.process_item_id = pf.item_id ";

			if (FieldConfig.ContainsKey("@ProcessAllocationWorkCategoriesItemId") &&
			    ((SubQueryConfigValue) FieldConfig["@ProcessAllocationWorkCategoriesItemId"]).Value != null &&
			    ((int[]) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationWorkCategoriesItemId"]).Value).Length >
			    0) {
				sql_where += " AND a.work_category_item_id IN(@ProcessAllocationWorkCategoriesItemId)";
			}

			if (FieldConfig.ContainsKey("@ProcessAllocationStatus") &&
			    ((SubQueryConfigValue) FieldConfig["@ProcessAllocationStatus"]).Value != null &&
			    ((int[]) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationStatus"]).Value).Length > 0) {
				var arr = (int[]) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationStatus"]).Value;
				var li = arr.ToList();
				if (Array.IndexOf(arr, 50) > -1) {
					li.Add(53);
				}

				sql_where += " AND " + allocationTable + ".status IN(" + string.Join(",", li.ToArray()) + ")";
			}

			if (FieldConfig.ContainsKey("@ProcessAllocationTaskFilterListColumnId") &&
			    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@ProcessAllocationTaskFilterListColumnId"]).Value) !=
			    0 &&
			    ((SubQueryConfigValue) FieldConfig["@ProcessAllocationAllowedListValuesItemId"]).Value != null &&
			    ((int[]) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationAllowedListValuesItemId"]).Value)
			    .Length > 0) {
				sql += " INNER JOIN  " + taskTable + " t ON t.item_id = a.task_item_id" +
				       " INNER JOIN " + processSelectionsTable(isArchive, "@ProcessAllocationTaskFilterListColumnId") +
				       " AS list1_param ON list1_param.item_id = t.item_id AND list1_param.item_column_id = @ProcessAllocationTaskFilterListColumnId AND list1_param.selected_item_id IN (@ProcessAllocationAllowedListValuesItemId) ";
			}

			if (FieldConfig.ContainsKey("@ProcessAllocationTimeType") &&
			    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@ProcessAllocationTimeType"]).Value) > 0 &&
			    ((SubQueryConfigValue) FieldConfig["@ProcessAllocationTimeType"]).Value != null &&
			    (int) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationTimeCount"]).Value > 0) {
				DateTime? startDate;
				DateTime? endDate;
				var current = DateTime.Now;
				var offset = (int) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationTimeOffset"]).Value;
				var count = (int) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationTimeCount"]).Value;

				switch ((int) ((SubQueryConfigValue) FieldConfig["@ProcessAllocationTimeType"]).Value) {
					case 1:
						startDate = new DateTime(current.AddYears(offset).Year, 1, 1);
						endDate = startDate.Value.AddYears(count);
						break;
					case 2:
						var startTemp2 = new DateTime(current.AddMonths(offset * 3).Year,
							current.AddMonths(offset * 3).Month, 1);

						var quarterNumber = (startTemp2.Month - 1) / 3 + 1;
						var firstDayOfQuarter = new DateTime(startTemp2.Year, (quarterNumber - 1) * 3 + 1, 1);

						startDate = firstDayOfQuarter;
						endDate = firstDayOfQuarter.AddMonths(count * 3);
						break;
					case 3:
						startDate = new DateTime(current.AddMonths(offset).Year, current.AddMonths(offset).Month, 1);
						endDate = startDate.Value.AddMonths(count);
						break;
					case 4:
						var startTemp4 = new DateTime(current.AddDays(offset).Year, current.AddDays(offset).Month,
							current.AddDays(offset * 7).Day);

						var ci = Thread.CurrentThread.CurrentCulture;
						ci.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
						var fdow = ci.DateTimeFormat.FirstDayOfWeek;

						var tempDate = startTemp4.AddDays(-(DateTime.Today.DayOfWeek - fdow));

						startDate = tempDate;
						endDate = tempDate.AddDays(count * 7);
						break;
					case 5:
						startDate = new DateTime(current.AddDays(offset).Year, current.AddDays(offset).Month,
							current.AddDays(offset).Day);
						endDate = startDate.Value.AddDays(count);
						break;
					default:
						startDate = DateTime.Now;
						endDate = DateTime.Now;
						break;
				}

				sql_where += " AND d.date>='" + startDate.Value.ToString("yyyy-MM-dd") + "' AND d.date<'" +
				             endDate.Value.ToString("yyyy-MM-dd") + "'";
			}

			sql += sql_where;
			sql += ")";

			return sql;
		}

		protected override void InitializeFieldConfig() {
			var db = new Connections(authenticatedSession);


			var WorkCategoryListName = "";
			var allConfigurations = new Configurations(instance, authenticatedSession);
			var clientConfigurations = allConfigurations.GetClientConfigurations();
			var featureConfigIndex = clientConfigurations.FindIndex(conf =>
				conf.Name == "process.simpleMonthlyAllocation" &&
				conf.Process == process);


			if (featureConfigIndex > -1) {
				var Allocationconfig = JObject.Parse(clientConfigurations[featureConfigIndex].Value);
				if (JObject.Parse(clientConfigurations[featureConfigIndex].Value)["WorkCategoryList"] != null) {
					WorkCategoryListName = Conv.ToStr(Allocationconfig["WorkCategoryList"]);
				}
			}


			var timeOptions = new List<object> {
				new Dictionary<string, object> {{"Variable", "YEARS"}, {"Value", 1}},
				new Dictionary<string, object> {{"Variable", "QUARTERS"}, {"Value", 2}},
				new Dictionary<string, object> {{"Variable", "MONTHS"}, {"Value", 3}},
				new Dictionary<string, object> {{"Variable", "WEEKS"}, {"Value", 4}},
				new Dictionary<string, object> {{"Variable", "DAYS"}, {"Value", 5}}
			};

			var statusOptions = new List<object> {
				new Dictionary<string, object> {{"Variable", "ALLOCATION_DRAFT_STATUS"}, {"Value", 10}},
				new Dictionary<string, object> {{"Variable", "ALLOCATION_APPROVE_MODIFIED_STATUS"}, {"Value", 13}},
				new Dictionary<string, object> {{"Variable", "ALLOCATION_REQUEST_MODIFIED_STATUS"}, {"Value", 25}},
				new Dictionary<string, object> {{"Variable", "ALLOCATION_REJECT_STATUS"}, {"Value", 30}},
				new Dictionary<string, object> {{"Variable", "ALLOCATION_APPROVE_MODIFIED_STATUS"}, {"Value", 40}},
				new Dictionary<string, object> {{"Variable", "ALLOCATION_OTHER_APPROVED_ALLOCATIONS"}, {"Value", 50}},
				new Dictionary<string, object> {{"Variable", "ALLOCATION_ASSIGNED_TASK_AUTO_APPROVE"}, {"Value", 55}}
			};

			var listCols = new List<ItemColumn>();
			var listCols2 = new List<ItemColumn>();
			object cols = new ItemColumns(instance, "task", authenticatedSession).GetItemColumns(false);

			foreach (var c in (List<ItemColumn>) cols) {
				if (c.DataType == 6) {
					listCols.Add(c);
				}
			}

			if (WorkCategoryListName != "") {
				var categoriesVals =
					db.GetDatatableDictionary(
						"SELECT item_id, list_item FROM _" + instance + "_" + WorkCategoryListName);

				AddFieldConfig("@ProcessAllocationWorkCategoriesItemId",
					new SubQueryConfigValue("@ProcessAllocationWorkCategoriesItemId", "int_array",
						SubQueryOptionType.MultiSelectList, "item_id", "list_item", categoriesVals));
			}

			AddFieldConfig("@ProcessAllocationStatus",
				new SubQueryConfigValue("@ProcessAllocationStatus", "int_array", SubQueryOptionType.MultiSelectList,
					"Value", "Variable", statusOptions));

			AddFieldConfig("@ProcessAllocationTaskFilterListColumnId",
				new SubQueryConfigValue("@ProcessAllocationTaskFilterListColumnId", "int", SubQueryOptionType.List,
					"ItemColumnId", "Variable", listCols, true));

			if (FieldConfig.ContainsKey("@ProcessAllocationTaskFilterListColumnId") &&
			    ((SubQueryConfigValue) FieldConfig["@ProcessAllocationTaskFilterListColumnId"]).Value != null &&
			    Conv.ToInt(((SubQueryConfigValue) FieldConfig["@ProcessAllocationTaskFilterListColumnId"]).Value) > 0) {
				var listProcess = db.ExecuteScalar(
					"SELECT data_additional FROM item_columns WHERE item_column_id = " +
					Conv.ToInt(((SubQueryConfigValue) FieldConfig["@ProcessAllocationTaskFilterListColumnId"]).Value));

				var listVals =
					db.GetDatatableDictionary("SELECT item_id, list_item FROM _" + instance + "_" + listProcess);

				AddFieldConfig("@ProcessAllocationAllowedListValuesItemId",
					new SubQueryConfigValue("@ProcessAllocationAllowedListValuesItemId", "int_array",
						SubQueryOptionType.MultiSelectList, "item_id", "list_item", listVals));
			} else {
				AddFieldConfig("@ProcessAllocationAllowedListValuesItemId",
					new SubQueryConfigValue("@ProcessAllocationAllowedListValuesItemId", "int_array",
						SubQueryOptionType.MultiSelectList,
						"item_id", "list_item", new List<Dictionary<string, object>>()));
			}

			AddFieldConfig("@ProcessAllocationTimeType",
				new SubQueryConfigValue("@ProcessAllocationTimeType", "int", SubQueryOptionType.List, "Value",
					"Variable", timeOptions)
			);

			AddFieldConfig("@ProcessAllocationTimeOffset",
				new SubQueryConfigValue("@ProcessAllocationTimeOffset", "int", SubQueryOptionType.Int, "ItemColumnId",
					"Variable")
			);

			AddFieldConfig("@ProcessAllocationTimeCount",
				new SubQueryConfigValue("@ProcessAllocationTimeCount", "int", SubQueryOptionType.Int, "ItemColumnId",
					"Variable")
			);
		}
	}


	public class SubqueryAllocationDurationHoursConfig : SubQueryConfigBase {
		public SubqueryAllocationDurationHoursConfig() {
			Config.Name = "AllocationDurationHours";
			Config.Value = "AllocationDurationHours";
			Config.TranslationKey = "PROCESS_ALLOCATION_DURATION_HOURS_SQ";
			Config.DataType = ItemColumn.ColumnType.Float;
			RequireValidation = false;
		}

		
		
		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(system_ad.hours)";
		}

		protected override void InitializeFieldConfig() {
			return;
		}
	}
	public class SubqueryAllocationDurationDatesConfig : SubQueryConfigBase {
		public SubqueryAllocationDurationDatesConfig() {
			Config.Name = "AllocationDurationDates";
			Config.Value = "AllocationDurationDates";
			Config.TranslationKey = "PROCESS_ALLOCATION_DURATION_DATES_SQ";
			Config.DataType = ItemColumn.ColumnType.Date;
			RequireValidation = false;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(system_ad.date)";
		}

		protected override void InitializeFieldConfig() {
			return;
		}
	}
}