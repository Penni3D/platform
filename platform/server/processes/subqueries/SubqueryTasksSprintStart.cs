﻿using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryTasksSprintStartConfig : SubQueryConfigBase {
		public SubqueryTasksSprintStartConfig() {
			Config.Name = "SubqueryTasksSprintStart";
			Config.Value = "SubqueryTasksSprintStart";
			Config.TranslationKey = "TASKS_SPRINT_START_SQ";
			Config.DataType = ItemColumn.ColumnType.Date;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var table = Conv.ToSql("_" + instance + "_task");
			return "(SELECT start_date FROM " + table + " s WHERE s.item_id = (SELECT parent_item_id FROM " + table + " r WHERE r.item_id = pf.parent_item_id))";
		}

		public override string GetArchiveSql(string instance) {
			var table = "get__" + Conv.ToSql(instance) + "_task(@archiveDate)";
			return "(SELECT start_date FROM " + table + " s WHERE s.item_id = (SELECT parent_item_id FROM " + table + " r WHERE r.item_id = pf.parent_item_id))";
		}
		
		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) { }
		}
	}
	
	public class SubqueryTasksSprintEndConfig : SubQueryConfigBase {
		public SubqueryTasksSprintEndConfig() {
			Config.Name = "SubqueryTasksSprintEnd";
			Config.Value = "SubqueryTasksSprintEnd";
			Config.TranslationKey = "TASKS_SPRINT_END_SQ";
			Config.DataType = ItemColumn.ColumnType.Date;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var table = Conv.ToSql("_" + instance + "_task");
			return "(SELECT end_date FROM " + table + " s WHERE s.item_id = (SELECT parent_item_id FROM " + table + " r WHERE r.item_id = pf.parent_item_id))";
		}

		public override string GetArchiveSql(string instance) {
			var table = "get__" + Conv.ToSql(instance) + "_task(@archiveDate)";
			return "(SELECT end_date FROM " + table + " s WHERE s.item_id = (SELECT parent_item_id FROM " + table + " r WHERE r.item_id = pf.parent_item_id))";
		}
		
		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) { }
		}
	}
}