﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class LastModifiedOnConfig : SubQueryConfigBase {
		public LastModifiedOnConfig() {
			Config.Name = "LastModifiedOnConfig";
			Config.Value = "LastModifiedOnConfig";
			Config.TranslationKey = "LAST_MODIFIED_ON";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			if ((SubQueryConfigValue)FieldConfig["@UseDateTime"] != null && ((SubQueryConfigValue)FieldConfig["@UseDateTime"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@UseDateTime"]).Value).Length > 0) {
				Config.DataType = ItemColumn.ColumnType.Datetime;
			}
			else {
				Config.DataType = ItemColumn.ColumnType.Date;
			}
			return "(pf.archive_start)";
		}

		protected override void InitializeFieldConfig() {
			var options = new List<object> {
					new Dictionary<string, object> {{"Variable", "TRUE" }, {"Value", 1}},
				};
			AddFieldConfig("@UseDateTime",
				new SubQueryConfigValue("@UseDateTime", "int_array", SubQueryOptionType.List,
					"Value", "Variable", options));
		}
	}

	public class CreatedOnConfig : SubQueryConfigBase {
		public CreatedOnConfig() {
			Config.Name = "CreatedOnConfig";
			Config.Value = "CreatedOnConfig";
			Config.TranslationKey = "CREATED_ON";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			if ((SubQueryConfigValue)FieldConfig["@UseDateTime"] != null && ((SubQueryConfigValue)FieldConfig["@UseDateTime"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@UseDateTime"]).Value).Length > 0) {
				Config.DataType = ItemColumn.ColumnType.Datetime;
			}
			else {
				Config.DataType = ItemColumn.ColumnType.Date;
			}
			return "(SELECT created_on FROM items WHERE items.item_id = pf.item_id)";
		}

		protected override void InitializeFieldConfig() {
			var options = new List<object> {
					new Dictionary<string, object> {{"Variable", "TRUE" }, {"Value", 1}},
				};
			AddFieldConfig("@UseDateTime",
				new SubQueryConfigValue("@UseDateTime", "int_array", SubQueryOptionType.List,
					"Value", "Variable", options));
		}
	}

	public class LastModifiedOn_MoreExtensive : SubQueryConfigBase {
		public LastModifiedOn_MoreExtensive() {
			Config.Name = "LastModifiedOn_MoreExtensive";
			Config.Value = "LastModifiedOn_MoreExtensive";
			Config.TranslationKey = "LAST_MODIFIED_ON_EXTENSIVE";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			if ((SubQueryConfigValue)FieldConfig["@UseDateTime"] != null && ((SubQueryConfigValue)FieldConfig["@UseDateTime"]).Value != null && ((int[])((SubQueryConfigValue)FieldConfig["@UseDateTime"]).Value).Length > 0) {
				Config.DataType = ItemColumn.ColumnType.Datetime;
			}
			else {
				Config.DataType = ItemColumn.ColumnType.Date;
			}
			string sql =
				"SELECT TOP 1 archive_start FROM (" +
				"	SELECT pf.archive_start, pf.archive_userid" +
				"	UNION ALL " +
				"	SELECT archive_start, archive_userid FROM item_data_process_selections WHERE item_id = pf.item_id " +
				"	UNION ALL " +
				"	SELECT archive_start, archive_userid FROM attachments WHERE item_id = pf.item_id " +
				") x WHERE archive_userid > 0 ORDER BY archive_start DESC";
			return "(" + sql + ")";
		}

		protected override void InitializeFieldConfig() {
			var options = new List<object> {
					new Dictionary<string, object> {{"Variable", "TRUE" }, {"Value", 1}},
				};
			AddFieldConfig("@UseDateTime",
				new SubQueryConfigValue("@UseDateTime", "int_array", SubQueryOptionType.List,
					"Value", "Variable", options));
		}
	}

	public class LastModifiedByConfig : SubQueryConfigBase {
		public LastModifiedByConfig() {
			Config.Name = "LastModifiedByConfig";
			Config.Value = "LastModifiedByConfig";
			Config.TranslationKey = "LAST_MODIFIED_BY";
			Config.DataType = ItemColumn.ColumnType.Process;
			SubProcess = "user";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(pf.archive_userid)";
		}

		protected override void InitializeFieldConfig() { }
	}

	public class CreatedByConfig : SubQueryConfigBase {
		public CreatedByConfig() {
			Config.Name = "CreatedByConfig";
			Config.Value = "CreatedByConfig";
			Config.TranslationKey = "CREATED_BY";
			Config.DataType = ItemColumn.ColumnType.Process;
			SubProcess = "user";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT created_by FROM items WHERE items.item_id = pf.item_id)";
		}

		protected override void InitializeFieldConfig() { }
	}

	public class LastModifiedBy_MoreExtensive : SubQueryConfigBase {
		public LastModifiedBy_MoreExtensive() {
			Config.Name = "LastModifiedBy_MoreExtensive";
			Config.Value = "LastModifiedBy_MoreExtensive";
			Config.TranslationKey = "LAST_MODIFIED_BY_EXTENSIVE";
			Config.DataType = ItemColumn.ColumnType.Process;
			SubProcess = "user";
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			var sql =
				"SELECT TOP 1 archive_userid FROM (" +
				"	SELECT pf.archive_start, pf.archive_userid" +
				"	UNION ALL " +
				"	SELECT archive_start, archive_userid FROM item_data_process_selections WHERE item_id = pf.item_id " +
				"	UNION ALL " +
				"	SELECT archive_start, archive_userid FROM attachments WHERE item_id = pf.item_id " +
				") x WHERE archive_userid > 0 ORDER BY archive_start DESC";
			return "(" + sql + ")";
		}

		protected override void InitializeFieldConfig() { }
	}

	public class ProcessItemIdConfig : SubQueryConfigBase {
		public ProcessItemIdConfig() {
			Config.Name = "ProcessItemIdConfig";
			Config.Value = "ProcessItemIdConfig";
			Config.TranslationKey = "PROCESS_ITEM_ID_SQ";
			Config.DataType = ItemColumn.ColumnType.Int;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(pf.item_id)";
		}

		protected override void InitializeFieldConfig() { }
	}
	public class ProcessItemIdProcessConfig : SubQueryConfigBase {
		public ProcessItemIdProcessConfig() {
			Config.Name = "ProcessItemIdProcessConfig";
			Config.Value = "ProcessItemIdProcessConfig";
			Config.TranslationKey = "PROCESS_ITEM_ID_PROCESS_SQ";
			Config.DataType = ItemColumn.ColumnType.Process;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(pf.item_id)";
		}

		protected override void InitializeFieldConfig() { }
	}

	public class ProcessItemIdListConfig : SubQueryConfigBase {
		public ProcessItemIdListConfig() {
			Config.Name = "ProcessItemIdListConfig";
			Config.Value = "ProcessItemIdListConfig";
			Config.TranslationKey = "PROCESS_ITEM_ID_LIST_SQ";
			Config.DataType = ItemColumn.ColumnType.List;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(pf.item_id)";
		}

		public override string GetFilterSql(string instance) {
			return GetSql(instance) + " IN ( ##idString## ) ";
		}

		public override List<Dictionary<string, object>> GetValues(string instance, int itemColumnId) {
			var db = new Connections(authenticatedSession);
			return db.GetDatatableDictionary("SELECT * FROM _" + instance + "_" + this.process);
		}

		protected override void InitializeFieldConfig() { }
	}
}