﻿using Keto5.x.platform.server.@base;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubqueryListBenefitsConfig  : SubQueryConfigBase
	{
		public SubqueryListBenefitsConfig () { 
			Config.Name = "LISTBENEFITS"; 
			Config.Value = "LISTBENEFITS"; 
			Config.TranslationKey = "LISTBENEFITS"; 
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) {
				return;
			}
		AddFieldConfig("@value",new SubQueryConfigValue("@value", "int", SubQueryOptionType.Int, "value", "value", 18));
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return "(SELECT STUFF((SELECT ', ' + description FROM _"+instance+"_benefits WHERE benefit_value > @value AND owner_item_id = pf.item_id FOR XML PATH('')),1,1,''))";
		}

		public override string GetArchiveSql(string instance) {
			return "(SELECT STUFF((SELECT ', ' + description FROM get__"+instance+"_benefits(@archiveDate) WHERE benefit_value > @value AND owner_item_id = pf.item_id FOR XML PATH('')),1,1,''))";
		}
	}
}