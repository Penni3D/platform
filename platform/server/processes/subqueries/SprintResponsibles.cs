using System;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.processes.subqueries {
	public class SubquerySprintResponsiblesConfig : SubQueryConfigBase {
		public SubquerySprintResponsiblesConfig() {
			Config.Name = "SprintResponsibles";
			Config.Value = "SprintResponsibles";
			Config.TranslationKey = "SPRINT_RESPONSIBLES";
			Config.DataType = ItemColumn.ColumnType.Int;
		}

		public override string GetSql(string instance, int attachedItemColumnId = 0) {
			return GetSqlCombined(instance);
		}

		private string GetSqlCombined(string instance, bool isArchive = false) {

			var sql =
				"(SELECT COUNT(selected_item_id) FROM item_data_process_selections WHERE item_column_id = @SprintResponsiblesColumnId AND selected_item_id = @SystemLoggedInUser AND item_id = (SELECT item_id FROM _" + instance + "_task WHERE item_id = ( " +
			    "SELECT parent_item_id FROM _" + instance + "_task WHERE item_id = ( " +
				"SELECT parent_item_id FROM _" + instance + "_task " +
				"WHERE item_id = pf.item_id " +
				")))) ";

			return sql;
		}

		protected override void InitializeFieldConfig() {
			if (FieldConfig.Count > 0) {
				return;
			}

			AddFieldConfig("@SprintResponsiblesColumnId",
				new SubQueryConfigValue("@SprintResponsiblesColumnId", "int", SubQueryOptionType.List, "ItemColumnId",
					"Variable", new ItemColumns(instance, "task", authenticatedSession).GetItemColumns())
			);
		}
	}
}