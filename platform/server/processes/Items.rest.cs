﻿//System references

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mime;
using BackgroundTasksSample.Services;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.calendar;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.pages.instanceSchedules;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.backgroundProcesses;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes {
	[Route("v1/meta/Items")]
	public class ItemsRest : RestBase {
		[HttpPost("async_no_schedule/{bgProcessName}")]
		public bool AsyncExecuteNoSchedule(string bgProcessName, [FromBody] Dictionary<string, object> settings) {
			//CheckRight("write");
			AsyncTask a = new AsyncTask();
			var bg = RegisteredBackgroundProcesses.GetBackgroundProcess(bgProcessName);


			var d = Config.VirtualDirectory;
			if (d != "")
				d += "/";

			var link2 = HttpContext.Request.IsHttps ? "https://" : "http://";

			link2 += (Conv.ToStr(HttpContext.Request.Host) + d).Replace("//", "/");

			bg.BaseLink = link2;

			a.ExecuteAsync(bg, currentSession, settings, instance);
			return false;
		}


		[HttpPost("{process}/replace")]
		public string ReplaceForRichText(string process, [FromBody] RichTextTemplate richTextTemplate) {
			var states = new States(instance, currentSession);
			var DatabaseItemService = new DatabaseItemService(instance, process, currentSession);
			var oldData = DatabaseItemService.GetItem(richTextTemplate.ItemId);
			return states.ReplaceTags(process, oldData, richTextTemplate.Body);
		}

		// GET: model/values
		[HttpGet("{process}")]
		public IEnumerable<Dictionary<string, object>> Get(string process) {
			var p = GetItemCollectionService(process);
			p.FillItems();
			return p.GetItems();
		}

		private ItemCollectionService GetItemCollectionService(string process) {
			var p = new ItemCollectionService(instance, process, currentSession);
			return p;
		}

		[HttpGet("{process}/order/{orderCol}")]
		public IEnumerable<Dictionary<string, object>> Get(string process, string orderCol) {
			var p = GetItemCollectionService(process);
			p.FillItems(orderCol);
			return p.GetItems();
		}

		[HttpGet("{process}/{col}/deduceparent")]
		public string DeduceParentProcess(string process, string col) {
			var pb = new ProcessBase(currentSession, process);
			var sql = "SELECT process FROM items WHERE item_id = ( " +
			          "SELECT TOP 1 " + Conv.ToSql(col) + " FROM _" + instance + "_" + Conv.ToSql(process) +
			          " WHERE " + Conv.ToSql(col) + " IS NOT NULL)";
			return Conv.ToStr(pb.db.ExecuteScalar(sql));
		}

		[HttpPost("fill/{process}/order/{orderCol}")]
		public IEnumerable<Dictionary<string, object>> GetFilled(string process, string orderCol,
			[FromBody] List<ItemColumn> cols) {
			var p = GetItemCollectionService(process);
			p.FillItems(orderCol);
			var items = p.GetItems();

			var ids = (from col in cols where col.DataType == 5 || col.DataType == 6 select col.ItemColumnId).ToList();

			var idps = new ItemDataProcessSelections(instance, process, currentSession);
			var dt = idps.GetItemDataProcessSelectionCacheTable(ids);
			foreach (var item in items) {
				foreach (var col in cols.Where(col => col.DataType == 5 || col.DataType == 6)) {
					item.Add(col.Name,
						idps.GetItemDataProcessSelectionIds(Conv.ToInt(item["item_id"]), col.ItemColumnId, dt));
				}
			}

			return items;
		}

		[HttpPost("favourites")]
		public Dictionary<string, List<Dictionary<string, object>>> GetFavourites([FromBody] List<string> processes) {
			var result = new Dictionary<string, List<Dictionary<string, object>>>();

			foreach (var process in processes) {
				var favouriteService = new FavouriteService(currentSession.instance, process, currentSession);
				result.Add(process, favouriteService.GetFavourites());
			}

			return result;
		}


		[HttpPost("{process}/favourites")]
		public Dictionary<string, object> AddFavourite(string process, [FromBody] int itemId) {
			var favouriteService = new FavouriteService(currentSession.instance, process, currentSession);
			favouriteService.AddFavourite(itemId);

			var p = new DatabaseItemService(currentSession.instance, process, currentSession);
			return p.GetItem(itemId);
		}

		[HttpGet("{process}/{itemId}/{columnId}/parents")]
		public List<Dictionary<string, object>> GetRelatedParents(string process, int itemId, int columnId) {
			var p = new ItemBase(instance, process, currentSession);
			var t = p.GetRelatedParentItems(itemId, columnId);
			return t;
		}

		[HttpDelete("{process}/favourites/{itemId}")]
		public void DeleteFavourite(string process, int itemId) {
			var favouriteService = new FavouriteService(currentSession.instance, process, currentSession);
			favouriteService.DeleteFavourite(itemId);
		}

		[HttpPost("{process}/unique")]
		public bool GetUnique(string process, [FromBody] UniqueRequest value) {
			if (process == "integration_token" && value.ColumnId == 0) {
				var cb = new ConnectionBase(currentSession.instance, currentSession);
				cb.SetDBParam(SqlDbType.Int, "@id", Conv.ToInt(value.ItemId));
				cb.SetDBParam(SqlDbType.NVarChar, "@keyVal", Conv.ToStr(value.Value));
				if (Conv.ToInt(cb.db.ExecuteScalar(
					    "SELECT TOP 1 instance_integration_key_id FROM instance_integration_keys WHERE [key] = @keyVal AND instance_integration_key_id <> @id",
					    cb.DBParameters)) == 0)
					return true;
				else
					return false;
			}

			var entityService = new DatabaseItemService(instance, process, currentSession);
			return entityService.IsUniqueColumn(value.ColumnId, value.ItemId == null ? 0 : (int)value.ItemId,
				value.Value);
		}

		[HttpPost("{process}/{columnId}/unique")]
		public bool GetUnique(string process, int columnId, [FromBody] UniqueRequest value) {
			var entityService = new DatabaseItemService(instance, process, currentSession);
			return entityService.IsUniqueColumn(columnId, value.Value);
		}

		public class UniqueRequest {
			public int ColumnId { get; set; }
			public int? ItemId { get; set; }
			public string Value { get; set; }
		}

		[HttpPost("{process}/portfolio/{portfolioId}")]
		public Dictionary<string, object>
			PortfolioPost(string process, int portfolioId, [FromBody] PortfolioOptions q) {
			var p = new ItemBase(instance, process, currentSession);
			return p.FindItems(portfolioId, q);
		}

		[HttpGet("{process}/portfolio/{portfolioId}")]
		public Dictionary<string, object>
			PortfolioPost(string process, int portfolioId) {
			return new Dictionary<string, object>();
		}

		//{order:regex(\\b(asc|desc)\\b)}
		[HttpGet("{process}/{itemType}/{id}")]
		public IEnumerable<Dictionary<string, object>> Get(string process, int itemType, int id) {
			var parentItemId = id;
			var itemCollection = new ItemCollectionService(instance, process, currentSession);
			var ib = new ItemBase(instance, process, currentSession);

			var order = "";
			var p = new Processes(instance, currentSession);
			var l = p.GetListProcess(process);
			if (l.ListProcess)
				order = l.ListOrder;

			if (itemType == 2)
				parentItemId = ib.GetParentItemId(id);

			itemCollection.FillSubItems(parentItemId, order);
			return itemCollection.GetItems();
		}

		[HttpGet("{process}/{itemId}")]
		public Dictionary<string, object> Get(string process, int itemId) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetItem(itemId);
		}

		[HttpPost("{process}")]
		public Dictionary<string, object> Post(string process, [FromBody] Dictionary<string, object> data) {
			var ib = new ItemBase(instance, process, currentSession);
			var ownerItemId = data.ContainsKey("owner_item_id") ? Conv.ToInt(data["owner_item_id"]) : 0;
			return ib.AddItem(data, ownerItemId: ownerItemId);
		}

		[HttpPut("{process}")]
		public Dictionary<string, object> Put(string process, [FromBody] Dictionary<string, object> data) {
			var states = new States(instance, currentSession);

			var ib = new ItemBase(instance, process, currentSession);
			var oldData = ib.GetItem(Conv.ToInt(data["item_id"]), checkRights: false);
			ib.SaveItem(data);
			var newData = ib.GetItem(Conv.ToInt(data["item_id"]));

			//INVALIDATE CACHE
			var conditions = new Conditions(instance, process, currentSession);
			foreach (var c in conditions.GetConditions(process)) {
				Cache.Remove(instance, "c_" + c.ConditionId + "_" + data["item_id"]);
			}

			states.CheckNotifications(process, Conv.ToInt(data["item_id"]), oldData, newData);

			return data;
		}

		[HttpDelete("{process}/{itemId}")]
		public void Delete(string process, string itemId) {
			var ib = new ItemBase(instance, process, currentSession);
			foreach (var id in itemId.Split(',')) {
				ib.Delete(Conv.ToInt(id));
				if (process == "user_group") {
					ib.DeleteListUserGroups(Conv.ToInt(id), instance);
				}
			}
		}

		[HttpPost("{process}/delete")]
		public void Delete(string process, [FromBody] List<int> ids) {
			var ib = new ItemBase(instance, process, currentSession);
			foreach (var id in ids) {
				ib.Delete(id);
			}
		}

		[HttpPost("searchAll")]
		public Dictionary<string, object> SearchAll([FromBody] PortfolioOptions q) {
			var p = new Processes(instance, currentSession);
			if (q.feature != null) {
				return p.SearchFromAllSelectors(q.offset, q.limit, q.text, q.feature, q.state);
			}

			return p.SearchFromAllSelectors(q.offset, q.limit, q.text);
		}

		[HttpGet("{process}/baselineData/{itemId}")]
		public Dictionary<string, object> BaselineDatas(string process, int itemId) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetBaselineData(itemId);
		}

		[HttpGet("{process}/baselineData/{itemId}/{type}")]
		public Dictionary<string, object> BaselineDatas(string process, int itemId, int type) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetBaselineData(itemId, type);
		}
	}

	[Route("v1/meta/[controller]")]
	public class ItemsData : RestBase {
		[HttpPost("{process}")]
		public List<APISaveResult> Post(string process, [FromBody] List<APISaveResult> data) {
			var results = new List<APISaveResult>();

			foreach (var o in data) {
				var copyFromId = 0;
				var ownerItemId = o.Data.ContainsKey("owner_item_id") ? Conv.ToInt(o.Data["owner_item_id"]) : 0;

				if (o.Data.ContainsKey("rmCopied"))
					copyFromId = Conv.ToInt(o.Data["item_id"]);

				var ib = new ItemBase(instance, process, currentSession);

				var retval = ib.AddRow(o, 0, process, ib.ProcessBase.ProcessRequiresInsertWriteRight(process), ownerItemId: ownerItemId);
				results.Add(retval);

				switch (process) {
					//Special Processes
					case "user": {
						var calendars = new Calendars(instance, currentSession);
						calendars.CreateUserCalendar(Conv.ToInt(retval.Data["item_id"]));
						break;
					}
					case "task": {
						var t = new TaskData(instance, currentSession);
						t.PostTaskCreate(o.Data, copyFromId);
						break;
					}
				}
			}

			return results;
		}

		// POST model/values
		[HttpPost("{process}/{parentId}")]
		public APISaveResult Post(string process, int parentId, [FromBody] APISaveResult data) {
			var ib = new ItemBase(instance, process, currentSession);
			var ownerItemId = data.Data.ContainsKey("owner_item_id") ? Conv.ToInt(data.Data["owner_item_id"]) : 0;
			return ib.AddRow(data, parentId, process, ownerItemId: ownerItemId);
		}

		// POST model/values
		[HttpPut("{process}/{itemId}")]
		public APISaveResult Put(string process, int itemId, [FromBody] APISaveResult data) {
			var ib = new ItemBase(instance, process, currentSession);
			var retval = ib.SaveRow(itemId, data);
			return retval;
		}

		[HttpPut("{process}")]
		public List<APISaveResult> SaveItems(string process, [FromBody] List<APISaveResult> data) {
			var results = new List<APISaveResult>();
			foreach (var o in data) {
				var ib = new ItemBase(instance, process, currentSession);
				var retval = ib.SaveRow(Conv.ToInt(o.Data["item_id"]), o);
				results.Add(retval);
			}

			return results;
		}

		[HttpGet("{process}/{itemId}")]
		public APISaveResult Get(string process, int itemId) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetRow(itemId);
		}

		[HttpGet("{process}/{itemId}/{itemColumnId}/{selectedItemId}")]
		public Dictionary<string, object>
			GetLinkItem(string process, int itemId, int itemColumnId, int selectedItemId) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetLinkItem(itemId, itemColumnId, selectedItemId);
		}

		[HttpGet("{process}/{itemId}/{itemColumnId}/{selectedItemId}/{archiveDate}")]
		public Dictionary<string, object>
			GetLinkItemArchive(string process, int itemId, int itemColumnId, int selectedItemId, DateTime archiveDate) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetLinkItem(itemId, itemColumnId, selectedItemId, archiveDate);
		}
	}

	[Route("v1/meta/[controller]")]
	public class ItemsDataProcessSelections : RestBase {
		[HttpPut("{process}/{itemId}")]
		public Dictionary<int, List<ItemDataProcessSelection>> Put(string process, int itemId,
			[FromBody] Dictionary<int, List<ItemDataProcessSelection>> data) {
			var ib = new ItemBase(instance, process, currentSession);
			ib.SaveItemDataProcessSelection(itemId, data);

			//INVALIDATE CACHE
			Cache.RemoveGroup(Conv.ToStr(itemId));

			return data;
		}
	}

	[Route("v1/meta/[controller]")]
	public class ArchiveData : RestBase {
		[HttpGet("{process}/{itemId}/{archiveId}")]
		public APISaveResult Get(string process, int itemId, int archiveId) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetArchiveRow(itemId, archiveId);
		}

		[HttpGet("byDate/{process}/{itemId}/{d}")]
		public APISaveResult Get(string process, int itemId, DateTime d) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetArchiveRow(itemId, d);
		}
	}

	[Route("v1/features/[controller]")]
	public class ArchiveReport : RestBase {
		[HttpGet("{process}/{itemId}/{itemColumnId}")]
		public List<Dictionary<string, object>> Get(string process, int itemId, int itemColumnId) {
			var p = new ItemBase(instance, process, currentSession);
			return p.GetArchiveReport(itemId, itemColumnId);
		}
	}

	[Route("v1/features/[controller]")]
	public class FeatureArchiveReport : RestBase {
		[HttpGet("{process}/{itemId}/{feature}")]
		public List<Dictionary<string, object>> Get(string process, int itemId, string feature) {
			var p = new ItemBase(instance, process, currentSession);
			var ics = new ItemColumns(instance, process, currentSession);
			var name = "feature_archive_" + feature;
			if (ics.HasColumnName(name)) {
				return p.GetArchiveReport(itemId, name);
			}

			return null;
		}
	}


	[Route("v1/files/[controller]")]
	public class ItemsExcel : RestBase {
		[HttpPost("{process}/{portfolioId}")]
		public string PortfolioExcel(string process, int portfolioId, [FromBody] PortfolioOptions q) {
			var p = new ItemBase(instance, process, currentSession);
			return Convert.ToBase64String(p.PortfolioExcel(portfolioId, q));
		}

		[HttpGet("{process}")]
		public void ExcelTemplate(string process) {
			var p = new ItemBase(instance, process, currentSession);
			p.GetExcelExportTemplate(Response);
		}

		[HttpGet("{process}/data")]
		public void ExcelTemplateWithdata(string process) {
			var p = new ItemBase(instance, process, currentSession);
			p.GetExcelExportTemplateWithData(Response);
		}

		[HttpGet("{process}/{portfolioId}/data")]
		public void ExcelTemplateWithPortfoliodata(string process, int portfolioId) {
			var p = new ItemBase(instance, process, currentSession);
			p.GetExcelExportTemplateWithData(Response, true, portfolioId);
		}

		[HttpGet("{process}/{fileId}")]
		public void ExcelImportInsert(string process, int fileId) {
			var p = new ItemBase(instance, process, currentSession);
			p.ImportExcelInsert(fileId);
		}

		[HttpGet("update/{process}/{fileId}")]
		public void ExcelImportUpdate(string process, int fileId) {
			var p = new ItemBase(instance, process, currentSession);
			p.ImportExcelUpdate(fileId);
		}
	}

	[Route("v1/files/[controller]")]
	public class ItemsPdf : RestBase {
		[HttpPost("{process}/{portfolioId}/{attachmentId}")]
		public string PortfolioPdf(string process, int portfolioId, int attachmentId, [FromBody] PortfolioOptions q) {
			q.offset = 0;
			var p = new ItemBase(instance, process, currentSession);
			var data = p.FindItems(portfolioId, q);
			var pcs = new ProcessPortfolioColumns(instance, process, currentSession);
			var cols2 = pcs.GetColumns(portfolioId);

			var colTypes = new Dictionary<string, int>();
			var cols = cols2.ToDictionary(c => c.ItemColumn.Name);

			var resultList = new List<Dictionary<string, object>>();
			var tags = ((DataTable)data["rowdata"]).Columns.Cast<DataColumn>().Select(it => it.ColumnName).ToList();

			var attachedProcesses =
				new ProcessPortfolios(instance, process, currentSession).GetAttachedProcesses(portfolioId, process);

			foreach (DataRow dr in ((DataTable)data["rowdata"]).Rows) {
				var resultDict = new Dictionary<string, object>();

				//Add Columns and Values of main process
				foreach (var col in tags) {
					if (cols.ContainsKey(col)) {
						var wordVal = p.SetWordCellValue(cols[col], dr[col], data, colTypes, currentSession.locale_id);
						resultDict.Add(col, wordVal);
					} else {
						var wordVal = dr[col];
						resultDict.Add(col, wordVal);
					}
				}

				//Add Attached Processes to Templater Data
				foreach (var ap in attachedProcesses) {
					resultDict.Add(ap.Tag,
						GetAttachedProcess(ap.Process, ap.PortfolioId, Conv.ToInt(dr["item_id"]), ap.LinkColumnName,
							ap.Rows, ap.MatrixStructure));
				}

				resultList.Add(resultDict);
			}

			data["rowdata"] = resultList;
			data.Add("colTypes", colTypes);

			if (q.portfolioPdfImageUpload != null) {
				foreach (var tag in q.portfolioPdfImageUpload) {
					if (tag.Key.EndsWith("-labels")) {
						for (var i = 0; i < q.portfolioPdfImageUpload[tag.Key].Count; i++) {
							q.portfolioPdfImageUpload[tag.Key][i] =
								p.ConvertHtmlToBase64(q.portfolioPdfImageUpload[tag.Key][i]);
						}
					}
				}

				data.Add("images", q.portfolioPdfImageUpload);
			}

			var reportArray =
				OfficeTemplater.CreateReport("portfolio", data, attachmentId, "pdf/portfolio", currentSession);
			return Convert.ToBase64String(reportArray);
		}

		public Dictionary<string, object> pp = new Dictionary<string, object>();

		public Dictionary<string, object> GetAttachedProcess(string process, int portfolioId, int OwnerItemId,
			string parentColumnName, int maxRows, List<Dictionary<string, List<KeyValuePair<string, int>>>> matrixStructure) {
			//pp.Clear();
			var ib = new ItemBase(instance, process, currentSession);
			var ic = new ItemColumns(instance, process, currentSession);
			var pcs = new ProcessPortfolioColumns(instance, process, currentSession);
			var ps = new ProcessPortfolios(instance, process, currentSession);
			var data = new Dictionary<string, object>();
			var resultList = new List<Dictionary<string, object>>();

			if (portfolioId != 0) {
				//Get Columns
				var oic = ic.GetItemColumnByName(parentColumnName, true);

				if (oic == null)
					return null;

				var cols2 = pcs.GetColumns(portfolioId);
				var colTypes = new Dictionary<string, int>();
				var cols = cols2.ToDictionary(c => c.ItemColumn.Name);

				//Limit to parent
				var op = new PortfolioOptions { restrictions = new Dictionary<int, object>() };
				op.restrictions.Add(oic.ItemColumnId, new List<string> { OwnerItemId.ToString() });

				//Resolve Order & Get Data
				var p = ps.GetPortfolio(portfolioId);
				op.sort = new List<PortfolioSort> {
					new PortfolioSort {
						column = Conv.ToInt(p.OrderItemColumnId) == 0 ? "order_no" : p.OrderItemColumnName,
						order = p.OrderDirectionName
					}
				};

				data = ib.FindItems(portfolioId, op);

				//Result object

				var tags = ((DataTable)data["rowdata"]).Columns.Cast<DataColumn>().Select(it => it.ColumnName).ToList();

				//Loop through columns and rows -- format data for Templater
				var loop = 0;
				foreach (DataRow dr in ((DataTable)data["rowdata"]).Rows) {
					loop += 1;
					if (maxRows > 0 && loop > maxRows)
						break;
					var resultDict = new Dictionary<string, object>();

					//Add Columns and Values of main process
					foreach (var col in tags) {
						if (col == "linked_tasks")
							continue;

						if (cols.ContainsKey(col)) {
							var wordVal = ib.SetWordCellValue(cols[col], dr[col], data, colTypes,
								currentSession.locale_id);
							resultDict.Add(col, wordVal);
						} else {
							var wordVal = dr[col];
							resultDict.Add(col, wordVal);
						}
					}

					resultList.Add(resultDict);
				}

				data["rowdata"] = resultList;
				data.Add("colTypes", colTypes);
			} else {
				var qIterator = 1;
				var selectedCells = pp; //new Dictionary<int, object>();

				var cb = new ConnectionBase(instance, currentSession);
				cb.SetDBParam(SqlDbType.Int, "@ownerItemId", OwnerItemId);

				foreach (var q in matrixStructure) {
					var celIterator = 1;
					var colIterator = 1;
					var resultDict2 = new Dictionary<string, object>();
					//jos valittu, tänne bingo
					foreach (var q1 in q["Questions"]) {
						resultDict2.Add(parentColumnName + "_q", q1.Key);
					}

					foreach (var col in q["Columns"]) {
						resultDict2.Add(parentColumnName + "_col" + colIterator, col.Key);
						colIterator++;
					}

					foreach (var cell in q["Cells"]) {
						cb.SetDBParam(SqlDbType.Int, "@cellItemId", cell.Value);
						resultDict2.Add(parentColumnName + "_c" + celIterator, cell.Key + "_" + cell.Value + "_" + OwnerItemId);
						if (Conv.ToInt(cb.db.ExecuteScalar(
							    "SELECT COUNT(*) FROM _" + instance + "_matrix_process_row_data WHERE owner_item_id = @ownerItemId AND cell_item_id = @cellItemId",
							    cb.DBParameters)) > 0) {
							selectedCells.Add(OwnerItemId + "_" + cell.Value, true);
						}

						celIterator++;
					}

					resultList.Add(resultDict2);
					qIterator++;
				}

				data["rowdata"] = resultList;
				pp = selectedCells;
			}

			return data;
		}

		[HttpGet("itemPdf/{process}/{itemId}/{attachmentId}")]
		public ActionResult ItemPdf(string process, int itemId, int attachmentId) {
			var p = new ItemBase(instance, process, currentSession);
			var ics = new ItemColumns(instance, process, currentSession);

			var data = p.GetItem(itemId);

			var dataToPdf = new Dictionary<string, object>();

			dataToPdf.Add("dateFormat", "dd.MM.yyyy");

			dataToPdf.Add("itemData", OfficeTemplater.ProcessDict(data, p, ics.GetItemColumns()));
			var reportArray = OfficeTemplater.CreateReport("data", dataToPdf, attachmentId, "pdf/item", currentSession);

			var cd = new ContentDisposition {
				FileName = "Report.docx",
				Inline = true
			};

			var h = new KeyValuePair<string, StringValues>("Content-Disposition", cd.ToString());
			Response.Headers.Append(h);

			var atts = new Attachments(instance, currentSession);
			atts.SaveAttachment(reportArray, "Report_" + DateTime.Now.ToString("yyyyMMdd"),
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document", itemId, "walthamReports",
				"Report " + DateTime.Now.ToString("dd.MM.yyyy"));

			return File(reportArray, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		}
	}

	[Route("v1/meta/[controller]")]
	public class ItemsProcess : RestBase {
		[HttpGet("{itemId}")]
		public string GetProcess(int itemId) {
			var p = new Processes(instance, currentSession);
			return p.GetProcessByItemId(itemId);
		}

		[HttpPost("{process}/{portfolioId}")]
		public Dictionary<int, Processes.PrimaryAndSecondary> GetPrimariesAndSecondaries(string process,
			int portfolioId, [FromBody] List<int> itemIds) {
			var p = new Processes(instance, currentSession);
			return p.GetPrimariesAndSecondaries(process, portfolioId, itemIds);
		}
	}

	[Route("v1/meta/ItemDataProcessSelections")]
	public class ItemDataProcessSelectionsRest : RestBase {
		[HttpPut("{process}/{itemId}/{itemColumnId}/{selectedItemId}")]
		public void SaveIdps(string process, int itemId, int itemColumnId, int selectedItemId) {
			var p = new ItemBase(instance, process, currentSession);
			var ic = new ItemColumns(instance, process, currentSession);
			var item = p.GetItem(itemId);
			var col = ic.GetItemColumn(itemColumnId);

			var itemToSave = new Dictionary<string, object>();

			if (item.ContainsKey(col.Name)) {
				var selections = (List<int>)item[col.Name];
				if (!selections.Contains(selectedItemId)) {
					selections.Add(selectedItemId);
					itemToSave.Add(col.Name, selections);
					itemToSave.Add("item_id", itemId);
					p.SaveItem(itemToSave);
				}
			}
		}

		[HttpDelete("{process}/{itemId}/{itemColumnId}/{selectedItemId}")]
		public void DeleteIdps(string process, int itemId, int itemColumnId, int selectedItemId) {
			var p = new ItemBase(instance, process, currentSession);
			var ic = new ItemColumns(instance, process, currentSession);
			var item = p.GetItem(itemId);
			var col = ic.GetItemColumn(itemColumnId);

			var itemToDelete = new Dictionary<string, object>();

			if (item.ContainsKey(col.Name)) {
				var selections = (List<int>)item[col.Name];
				if (selections.Contains(selectedItemId)) {
					selections.Remove(selectedItemId);
					itemToDelete.Add(col.Name, selections);
					itemToDelete.Add("item_id", itemId);
					p.SaveItem(itemToDelete);
				}
			}
		}
	}

	public class PortfolioSort {
		public string column;
		private string _order;

		public string order {
			get => _order;
			set => _order = value.ToLower() == "desc" ? "desc" : "asc";
		}
	}

	public class PortfolioOptions {
		private string _order;
		public Dictionary<int, object> filters { get; set; }
		public Dictionary<int, object> restrictions { get; set; }
		public DateTime? archiveDate { get; set; }
		public string text { get; set; }
		public int offset { get; set; }
		public int limit { get; set; }
		public List<int> phases { get; set; } = new List<int>();
		public string sortBy { get; set; }

		public bool onlyCount { get; set; }

		public List<PortfolioSort> sort { get; set; } = new List<PortfolioSort>();

		public string order {
			get => _order;
			set => _order = value.ToLower() == "desc" ? "desc" : "asc";
		}

		public bool recursive { get; set; }
		public string feature { get; set; }
		public string state { get; set; }
		public List<int> itemsRestriction { get; set; } = new List<int>();

		public PortfolioContextOptions context { get; set; }

		public Dictionary<string, List<string>> portfolioPdfImageUpload { get; set; }

		public List<int> excludeItemIds { get; set; } = new List<int>();
	}

	public class PortfolioContextOptions {
		public string process { get; set; }
		public int itemId { get; set; }
		public int itemColumnId { get; set; }
	}

	public class RichTextTemplate {
		public string Body { get; set; }
		public int ItemId { get; set; }
	}
}