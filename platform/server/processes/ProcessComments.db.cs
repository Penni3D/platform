﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes {
	public class Comment {
		public Comment() { }

		public Comment(DataRow row) {
			CommentId = (int) row["item_id"];
			Sender = (int) row["sender"];
			IsDeleted = Conv.ToBool(row["is_deleted"]);
			Data = IsDeleted ? "" : (string) row["comment"];
			Date = (DateTime) row["date"];
		}

		public int CommentId { get; set; }
		public int Sender { get; set; }
		public string Data { get; set; }
		public bool IsDeleted { get; set; }
		public DateTime Date { get; set; }
	}
	
	public class Comments : ProcessBase {
		//AuthenticatedSession authenticatedSession;

		public Comments(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, "process_comments",
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public Dictionary<string, object> GetComments(string identifier) {
			var result = new Dictionary<string, object>();
			var comments = new List<Comment>();
			var ib = new ItemBase(instance, "user", authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			foreach (DataRow r in db.GetDatatable("SELECT *  FROM  " + tableName + " " +
			                                      "WHERE instance = @instance AND " +
			                                      "identifier = @identifier AND comment_type = 1 ",
				DBParameters).Rows
			) {
				comments.Add(new Comment(r));
			}

			var userIds = comments.Select(x => x.Sender).Distinct();
			var users = new List<Dictionary<string, object>>();
			foreach (var user in userIds) {
				var u = ib.GetItem(user);
				users.Add(u);
			}


			result.Add("processes", users);
			result.Add("comments", comments);
			return result;
		}
		
		public Dictionary<string, object> GetComments(string targetProcess, int itemId, string identifier) {
			var result = new Dictionary<string, object>();
			var comments = new List<Comment>();
			var ib = new ItemBase(instance, "user", authenticatedSession);

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.NVarChar, "@targetProcess", targetProcess);

			foreach (DataRow r in db.GetDatatable("SELECT * FROM " + tableName + " " +
			                                      "WHERE instance = @instance AND " +
			                                      "process = @targetProcess AND " +
			                                      "process_item_id = @itemId AND " +
			                                      "identifier = @identifier",
				DBParameters).Rows
			) {
				comments.Add(new Comment(r));
			}

			var userIds = comments.Select(x => x.Sender).Distinct();
			var users = new List<Dictionary<string, object>>();
			foreach (var user in userIds) {
				var u = ib.GetItem(user);
				users.Add(u);
			}


			result.Add("processes", users);
			result.Add("comments", comments);
			return result;
		}

		public void CopyComments(string sourceIdentifier, int sourceItemId, string targetIdentifier, int targetItemId,
			string targetProcess) {
			SetDBParam(SqlDbType.Int, "@sourceItemId", sourceItemId);
			SetDBParam(SqlDbType.Int, "@targetItemId", targetItemId);

			SetDBParam(SqlDbType.NVarChar, "@sourceIdentifier", sourceIdentifier);
			SetDBParam(SqlDbType.NVarChar, "@targetIdentifier", targetIdentifier);
			SetDBParam(SqlDbType.NVarChar, "@targetProcess", targetProcess);
			
			var newId = InsertItemRow();
			SetDBParam(SqlDbType.Int, "@newId", newId);

			var oldId = (int) db.ExecuteScalar(
				"SELECT item_id FROM " + tableName +
				" WHERE identifier = @sourceIdentifier AND process_item_id = @sourceItemId", DBParameters);
			SetDBParam(SqlDbType.Int, "@oldId", oldId);

			db.ExecuteInsertQuery(
				"UPDATE  " + tableName + " SET instance = @instance, " +
				"identifier = @targetIdentifier, " + 
				"process = @targetProcess, " + 
				"process_item_id = @targetItemId, " + 
				"sender = (SELECT sender FROM " + tableName + " WHERE item_id = @oldId) , " +
				"comment = (SELECT comment FROM " + tableName + " WHERE item_id = @oldId) , " +
				"date = (SELECT date FROM " + tableName + " WHERE item_id = @oldId) , " +
				"comment_type = 1 WHERE item_id = @newId ", DBParameters);
		}

		/*
		    TO-DO CONDITION CHECK:

		    var mStates = new States(instance, authenticatedSession);
		    var states = mStates.GetStatesForStates(process, identifier, itemId, new List<string>{ "write" });

		    if (states.States.Count == 0)
		        return null;

		*/

		public int DeleteComment(string targetProcess, int itemId, string identifier, int commentId) {
			SetDBParam(SqlDbType.NVarChar, "@process", targetProcess);
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.Int, "@commentId", commentId);

			var lastComment = new Comment(db.GetDataRow(
				"SELECT TOP 1 * FROM " + tableName + " WHERE " +
				"instance = @instance AND process = @process AND " +
				"process_item_id = @itemId AND identifier = @identifier AND comment_type = 0" +
				"ORDER BY date DESC", DBParameters)
			);

			UpdateOrDeleteComment(lastComment, commentId);

			return commentId;
		}

		private void UpdateOrDeleteComment(Comment comment, int commentId) {
			if (comment.CommentId == commentId) {
				var ib = new ItemBase(instance, "process_comments", authenticatedSession);
				ib.Delete(commentId);
			} else {
				SetDBParam(SqlDbType.Int, "@sender", authenticatedSession.item_id);
				var updatedComments = db.ExecuteUpdateQuery("UPDATE " + tableName + " SET is_deleted = 1 WHERE item_id = @commentId AND sender = @sender", DBParameters);

				if (updatedComments == 0) {
					var exception = new CustomPermissionException("NOT_ALLOWED_TO_WRITE_TO_PROCESS",
						"Current user can't write to process process_comments");

					exception.Data.Add("process", "process_comments");
					throw exception;
				}
			}
		}

		public int DeleteComment(string identifier, int commentId) {
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.Int, "@commentId", commentId);

			var lastComment = new Comment(db.GetDataRow(
				"SELECT TOP 1 * FROM " + tableName + " WHERE " +
				"instance = @instance AND identifier = @identifier AND comment_type = 1" +
				"ORDER BY date DESC", DBParameters)
			);

			UpdateOrDeleteComment(lastComment, commentId);

			return commentId;
		}


		public Comment PostComment(string targetProcess, int itemId, string identifier, string comment) {
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.Int, "@sender", authenticatedSession.item_id);
			SetDBParam(SqlDbType.NVarChar, "@comment", comment);
			SetDBParam(SqlDbType.NVarChar, "@targetProcess", targetProcess);

			var newId = InsertItemRow();
			SetDBParam(SqlDbType.Int, "@newId", newId);

			db.ExecuteUpdateQuery(
				"UPDATE  " + tableName + " SET instance = @instance, process = @targetProcess, process_item_id = @itemId, identifier = @identifier, sender = @sender, comment = @comment, date = getdate(), comment_type = 0 WHERE item_id = @newId", DBParameters);

			SetDBParam(SqlDbType.Int, "@newId", newId);
			var dr = db.GetDataRow("SELECT * FROM " + tableName + " WHERE item_id = @newId", DBParameters);

			//Do after action
			new ProcessBase(authenticatedSession, "process_comments").DoNewRowAction(newId, true);

			return new Comment(dr);
		}

		public Comment PostComment(string identifier, string comment) {
			SetDBParam(SqlDbType.NVarChar, "@identifier", identifier);
			SetDBParam(SqlDbType.Int, "@sender", authenticatedSession.item_id);
			SetDBParam(SqlDbType.NVarChar, "@comment", comment);

			var newId = InsertItemRow();
			SetDBParam(SqlDbType.Int, "@newId", newId);
			
			db.ExecuteUpdateQuery(
				"UPDATE  " + tableName + " SET instance = @instance, identifier = @identifier, sender = @sender, comment = @comment, date = getdate(), comment_type = 1 WHERE item_id = @newId", DBParameters);

			SetDBParam(SqlDbType.Int, "@newId", newId);
			var dr = db.GetDataRow("SELECT * FROM " + tableName + " WHERE item_id = @newId", DBParameters);

			return new Comment(dr);
		}
	}
}