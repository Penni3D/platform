﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks.Dataflow;

namespace Keto5.x.platform.server.processes.accessControl {
	public class State {
		public string StateName { get; set; }
		public bool Enabled { get; set; }
	}

	[Serializable]
	public class FeatureStates {
		public FeatureStates() {
			States = new List<string>();
			groupStates = new Dictionary<int, EvaluatedGroupState>();
		}

		public List<string> States { get; set; }
		public Dictionary<int, EvaluatedGroupState> groupStates { get; set; }

		public bool HasStateOnAnyLevel(List<string> states) {
			if (States.Any(states.Contains)) return true;
			foreach (var group in groupStates) {
				if (@group.Value.s.Any(states.Contains)) return true;
				foreach (var t in group.Value.tabStates) {
					if (t.Value.s.Any(states.Contains)) return true;
					if (t.Value.containerStates.SelectMany(c => c.Value.s).Any(states.Contains)) return true;
				}
			}
			return false;
		}

		public bool HasGroupState(int groupId, string state) {
			return HasGroupState(groupId, new[] {state});
		}

		public bool HasGroupState(int groupId, string[] states) {
			return groupStates.ContainsKey(groupId) && states.Any(s => groupStates[groupId].s.Contains(s));
		}

		public bool HasTabState(int tabId, string state) {
			return HasTabState(tabId, new[] {state});
		}

		public bool HasTabState(int tabId, string[] states) {
			return groupStates.Keys.Where(gid => groupStates[gid].tabStates.ContainsKey(tabId))
				.Any(gid => states.Any(s => groupStates[gid].tabStates[tabId].s.Contains(s)));
		}

		public bool HasContainerState(int containerId, string state) {
			return HasContainerState(containerId, new[] {state});
		}

		public bool HasContainerState(int containerId, string[] states) {
			return groupStates.Keys.Any(gid =>
				groupStates[gid].tabStates.Keys
					.Where(tid => groupStates[gid].tabStates[tid].containerStates.ContainsKey(containerId)).Any(tid =>
						states.Any(s => groupStates[gid].tabStates[tid].containerStates[containerId].s.Contains(s))));
		}

		public List<int> GetGroupIds(string state) {
			return GetGroupIds(new[] {state});
		}

		public List<int> GetGroupIds(string[] states) {
			var retval = new List<int>();
			foreach (var gid in from gid in groupStates.Keys
				from s in states
				where groupStates[gid].s.Contains(s)
				where !retval.Contains(gid)
				select gid) {
				retval.Add(gid);
			}

			return retval;
		}

		public List<int> GetTabIds(string state) {
			return GetTabIds(new[] {state});
		}

		public List<int> GetTabIds(string[] states) {
			var retval = new List<int>();
			foreach (var tid in groupStates.Keys.SelectMany(gid =>
				from tid in groupStates[gid].tabStates.Keys
				from s in states
				where groupStates[gid].tabStates[tid].s.Contains(s)
				where !retval.Contains(tid)
				select tid)) {
				retval.Add(tid);
			}

			return retval;
		}

		public List<int> GetContainerIds(string state) {
			return GetContainerIds(new[] {state});
		}

		public List<int> GetContainerIds(string[] states) {
			var retval = new List<int>();
			foreach (var cid in from gid in groupStates.Keys
				from tid in groupStates[gid].tabStates.Keys
				from cid in groupStates[gid].tabStates[tid].containerStates.Keys
				from s in states
				where groupStates[gid].tabStates[tid].containerStates[cid].s.Contains(s)
				where !retval.Contains(cid)
				select cid) {
				retval.Add(cid);
			}

			return retval;
		}
	}

	public class EvaluatedGroupState {
		public EvaluatedGroupState() {
			s = new List<string>();
			tabStates = new Dictionary<int, EvaluatedTabState>();
		}

		public List<string> s { get; set; }
		public Dictionary<int, EvaluatedTabState> tabStates { get; set; }
	}

	public class EvaluatedTabState {
		public EvaluatedTabState() {
			s = new List<string>();
			containerStates = new Dictionary<int, EvaluatedContainerState>();
		}

		public List<string> s { get; set; }
		public Dictionary<int, EvaluatedContainerState> containerStates { get; set; }
	}

	public class EvaluatedContainerState {
		public EvaluatedContainerState() {
			s = new List<string>();
		}

		public List<string> s { get; set; }
	}
}