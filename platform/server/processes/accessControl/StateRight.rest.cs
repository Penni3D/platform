﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.accessControl {
	[Route("v1/navigation/StateRights")]
	public class StateRightsRest : PageBase {
		public StateRightsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("")]
		public List<StateRight> Get() {
			var stateRights = new StateRights(instance, currentSession);
			return stateRights.GetStateRights();
		}

		[HttpGet("{process}/{feature}/{state}")]
		public List<StateRight> Get(string process, string feature, string state) {
			var stateRights = new StateRights(instance, currentSession);
			return stateRights.GetStateRights(process, feature, state);
		}

		[HttpGet("{stateRightId}")]
		public StateRight Get(int stateRightId) {
			var stateRights = new StateRights(instance, currentSession);
			return stateRights.GetStateRight(stateRightId);
		}

		[HttpPost]
		public IActionResult Post([FromBody]StateRight stateRight) {
			CheckRight("write");
			var stateRights = new StateRights(instance, currentSession);
			if (ModelState.IsValid) {
				if (stateRight.StateRightId > 0) {
					stateRights.Save(stateRight);
					return new ObjectResult(stateRight);
				}

				stateRights.Add(stateRight);
				return new ObjectResult(stateRight);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpPut]
		public IActionResult Put([FromBody]StateRight stateRight) {
			CheckRight("write");
			var stateRights = new StateRights(instance, currentSession);
			if (ModelState.IsValid) {
				stateRights.Save(stateRight);
				return new ObjectResult(stateRight);
			}

			// This will work in later versions of ASP.NET 5
			//return new BadRequestObjectResult(ModelState);
			return null;
		}

		[HttpDelete("{stateRightId}")]
		public IActionResult Delete(int stateRightId) {
			CheckRight("delete");
			var stateRights = new StateRights(instance, currentSession);
			stateRights.Delete(stateRightId);
			return new StatusCodeResult(200);
		}
	}
}