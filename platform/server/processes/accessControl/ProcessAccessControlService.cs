﻿using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.accessControl {
	public class ProcessAccessControlService {
		private readonly AuthenticatedSession _authenticatedSession;
		private readonly string _instance;
		private readonly List<string> writeActions = new List<string> {"write", "send", "request"};

		public ProcessAccessControlService(string instance, AuthenticatedSession session) {
			_authenticatedSession = session;
			_instance = instance;
		}

		private bool checkPrivileges(string process, string feature, int processItemId, List<string> actions) {
			var mStates = new States(_instance, _authenticatedSession);
			var states = mStates.GetEvaluatedStates(process, feature, processItemId, actions);
			//var groupStates = mStates.GetStatesForGroups(process, feature, actions);

			foreach (var action in actions) {
				if (states.States.Contains(action)) return true;
			}
			
			return true;
		}

		public void IAmGoingToWriteToProcessFeature(int processItemId, string process, string feature) {
			var list = writeActions.Select(s => feature + "." + s).ToList();

			if (!checkPrivileges(process, feature, processItemId, list)) {
				throw new CustomPermissionException("User is not allowed to write to process and feature " + process +
				                                    " - " + feature);
			}
		}
	}
}