﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.interfaces;
using Keto5.x.platform.server.processes.actions;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.processes.subqueries;
using Keto5.x.platform.server.security;
using NCalc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Condition = Keto5.x.platform.server.core.Condition;
using Conditions = Keto5.x.platform.server.core.Conditions;

namespace Keto5.x.platform.server.processes.accessControl {
	public class States : ConnectionBase {
		private readonly Dictionary<string, DatabaseItemService> _databaseItemServices;
		private readonly Dictionary<string, ItemDataProcessSelections> _itemDataProcessSelectionses;
		private readonly AuthenticatedSession authenticatedSession;

		public States(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
			_databaseItemServices = new Dictionary<string, DatabaseItemService>();
			_itemDataProcessSelectionses = new Dictionary<string, ItemDataProcessSelections>();
		}

		public Dictionary<string, FeatureStates> CleanStatesObject(Dictionary<string, FeatureStates> co) {
			foreach (var (key, value) in co) {
				for (var i = 0; i < value.States.Count; i++) {
					value.States[i] = value.States[i].Replace(key + ".", "");
				}

				var r = JsonConvert.SerializeObject(value.groupStates).Replace(key + ".", "");
				value.groupStates = JsonConvert.DeserializeObject<Dictionary<int, EvaluatedGroupState>>(r);
				if (key == "meta") continue;
				foreach (var tab in value.groupStates) {
					tab.Value.tabStates = null;
				}
			}

			return co;
		}

		public List<State> CheckStates(string process, string feature, int itemId, string[] states) {
			var retval = new List<State>();
			var conds = new Conditions(instance, process, authenticatedSession);
			foreach (var s in states) {
				var state = new State {
					StateName = s,
					Enabled = false
				};
				foreach (var c in conds.GetConditions(process, feature, s)) {
					bool result;
					if (itemId == 0) {
						var exp = GetItemlessConditionExpression(process, c);
						result = EvaluateExpression(exp);
					} else {
						var exp = GetItemsConditionExpression(process, itemId, c);
						result = exp.Evaluate();
					}

					if (result) state.Enabled = true;
					retval.Add(state);
				}
			}

			return retval;
		}

		private DatabaseItemService getDatabaseItemService(string process) {
			if (!_databaseItemServices.ContainsKey(process)) {
				_databaseItemServices.Add(process, new DatabaseItemService(instance, process, authenticatedSession));
			}

			return _databaseItemServices[process];
		}

		private Dictionary<string, bool> _solverCache = new Dictionary<string, bool>();

		public bool GetConditionExpressionResult(string process, int itemId, Condition c, DataTable data = null,
			DataTable selections = null, Dictionary<string, object> row = null) {
			var key = process + "_" + itemId + "_" + c.ConditionId;

			if (_solverCache.ContainsKey(key)) return _solverCache[key];

			var exp = GetItemsConditionExpression(process, itemId, c, data, selections, row);
			var result = EvaluateExpression(exp);

			_solverCache.Add(key, result);

			return result;
		}

		public static bool EvaluateExpression(LogicalSolver exp) {
			return exp.Evaluate();
		}

		public static bool EvaluateExpression(string exp) {
			var e = new Expression(exp);
			if (exp.Contains("Contains")) {
				// todo how can i access these
				e.EvaluateFunction += evalFunctionForContains;
			}

			if (exp.Contains("StartsWith")) {
				// todo how can i access these
				e.EvaluateFunction += evalFunctionForStartsWith;
			}

			var result = (bool)e.Evaluate();
			return result;
		}

		private static void evalFunctionForContains(string name, FunctionArgs args) {
			switch (name) {
				case "Contains":
					if (args.Parameters.Length < 2)
						throw new ArgumentException("Contains() takes at least 2 arguments");
					args.Result = args.Parameters[0].Evaluate().ToString().ToLower()
						.Contains(args.Parameters[1].Evaluate().ToString().ToLower());
					break;
			}
		}

		private static void evalFunctionForStartsWith(string name, FunctionArgs args) {
			switch (name) {
				case "StartsWith":
					if (args.Parameters.Length < 2)
						throw new ArgumentException("StartsWith() takes at least 2 arguments");
					args.Result = args.Parameters[0].Evaluate().ToString().ToLower()
						.StartsWith(args.Parameters[1].Evaluate().ToString().ToLower());
					break;
			}
		}

		/// <summary>
		/// This function gets Condition Expression for portfolio based condition checks
		/// </summary>
		/// <param name="process"></param>
		/// <param name="c"></param>
		/// <param name="data"></param>
		/// <param name="selections"></param>
		/// <param name="row"></param>
		/// <returns></returns>
		/// <exception cref="NotImplementedException"></exception>
		public string GetItemlessConditionExpression(string process, Condition c) {
			if (c.ItemIds.Count > 0 &&
			    !c.ItemIds.Intersect((List<int>)authenticatedSession.GetMember("groups")).Any()) {
				return " false ";
			}

			if (c.ConditionJSONServer == null) return " true ";

			var hasOtherConds = false;
			foreach (var c1 in c.ConditionJSONServer) {
				foreach (var o in c1.OperationGroup.Operations) {
					if (o.IsOfType(ConditionType.EXISTING_CONDITION) ||
					    o.IsOfType(ConditionType.PARENT_USER_CONDITION) ||
					    o.IsOfType(ConditionType.SIGNED_IN_USER_CONDITION) ||
					    o.IsOfType(ConditionType.PARENT_PROCESS_EXISTING_CONDITION)) {
						hasOtherConds = true;
						break;
					}
				}
			}

			if (!hasOtherConds) {
				if (Cache.Get(instance, "c_" + c.ConditionId + "_" + 0) != null) {
					var exp2 = (string)Cache.Get(instance, "c_" + c.ConditionId + "_" + 0);
					return exp2;
				}
			}

			var itemCols = new ItemColumns(instance, process, authenticatedSession);
			var cType = new ComparisonTypes();
			var operators = new Operators();

			var exp = "";

			var i = 0;
			foreach (var c1 in c.ConditionJSONServer) {
				if (i > 0) {
					var op = operators.GetOperator((int)c1.OperatorId);
					exp += op.OperatorName;
				}

				var i2 = 0;
				exp += " (";
				foreach (var o in c1.OperationGroup.Operations) {
					if (o.IsOfType(ConditionType.CONDITION)) {
						dynamic cond = o.Condition;

						ItemColumn ic1;
						ic1 = itemCols.GetItemColumn((int)cond.ItemColumnId.Value);

						exp = GetExpressionStubForI2(i2, exp, operators, o);

						if (ic1.IsList() || ic1.IsProcess()) {
							if (cond.ComparisonTypeId.Value == 3 || cond.ComparisonTypeId.Value == 6) {
								var ids = new List<int>();
								if (cond == null || cond.Value.GetType() == new JArray().GetType()) {
									ids = ((int[])Conv.ToIntArray(cond.Value)).ToList();
								} else {
									if (cond == null || cond.Value == null) { } else {
										ids.Add(Conv.ToInt(cond.Value.Value));
									}
								}

								var hasZero = ids.Contains(0);
								ids.Remove(ids.Find(x => x == 0));

								var idString = Modi.JoinListWithComma(ids);
								var hasValue =
									" (SELECT COUNT(selected_item_id) FROM item_data_process_selections WHERE item_id = pf.item_id AND item_column_id = " +
									ic1.ItemColumnId + " AND selected_item_id IN (" + idString +
									")) " + (cond.ComparisonTypeId.Value == 6 ? "=" : ">") + " 0 ";

								var isEmpty =
									" (SELECT COUNT(selected_item_id) FROM item_data_process_selections WHERE item_id = pf.item_id AND item_column_id = " +
									ic1.ItemColumnId + ") " + (cond.ComparisonTypeId.Value == 6 ? ">" : "=") + " 0 ";


								if (hasZero && ids.Count > 0) {
									exp += "(" + isEmpty + " OR " + hasValue + ")";
								} else if (hasZero && ids.Count == 0) {
									exp += isEmpty;
								} else {
									exp += hasValue;
								}
							} else {
								exp += " 0 = 1";
							}
						} else if (ic1.IsDate()) {
							DateTime v2 = DateTime.Parse("" + cond.Value.Value);
							exp += " pf." + ic1.Name + " " +
							       cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value].ComparisonText +
							       " '" + v2.Year + "-" + v2.Month + "-" + v2.Day + "' ";
						} else if (ic1.IsNvarChar() || ic1.IsText()) {
							if (cond.ComparisonTypeId.Value < 7) {
								exp += " COALESCE(pf." + ic1.Name + ",'') " +
								       cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value].ComparisonText +
								       " '" + Conv.ToSql(cond.Value.Value) +
								       "' ";
							} else if (cond.ComparisonTypeId.Value == 7) {
								exp += " COALESCE(pf." + ic1.Name + ",'') LIKE '%" + Conv.ToSql(cond.Value.Value) +
								       "%' ";
							} else if (cond.ComparisonTypeId.Value == 8) {
								exp += " COALESCE(pf." + ic1.Name + ",'') LIKE '" +
								       Conv.ToSql(Conv.ToSql(cond.Value.Value)) + "%' ";
							}
						} else if (ic1.IsSubquery()) {
							var q = RegisteredSubQueries.GetType(ic1.DataAdditional, ic1.ItemColumnId);

							if (ic1.IsOfSubType(ItemColumn.ColumnType.List)) {
								var ids = new List<int>();
								if (cond == null || cond.Value.GetType() == new JArray().GetType()) {
									ids = ((int[])Conv.ToIntArray(cond.Value)).ToList();
								} else {
									if (cond == null || cond.Value == null) { } else {
										ids.Add(Conv.ToInt(cond.Value.Value));
									}
								}

								var hasZero = ids.Contains(0);
								ids.Remove(ids.Find(x => x == 0));

								var idString = Modi.JoinListWithComma(ids);
								var hasValue = " COALESCE(" + q.GetQueryCachedSql(instance, ic1.Name) + ",0) " +
								               (cond.ComparisonTypeId.Value == 6 ? " NOT " : "") + " IN (" + idString +
								               ") ";

								var isEmpty =
									" COALESCE(" + q.GetQueryCachedSql(instance, ic1.Name) + ",0) " +
									(cond.ComparisonTypeId.Value == 6 ? " !" : " ") + "= '' ";


								if (hasZero && ids.Count > 0) {
									exp += "(" + isEmpty + " OR " + hasValue + ")";
								} else if (hasZero && ids.Count == 0) {
									exp += isEmpty;
								} else {
									exp += hasValue;
								}
							} else {
								exp += " COALESCE(" + q.GetQueryCachedSql(instance, ic1.Name) + ",0) " +
								       cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value].ComparisonText +
								       " '" +
								       Conv.ToSql(cond.Value.Value) + "' ";
							}
						} else {
							exp += " COALESCE(pf." + ic1.Name + ",0) " +
							       cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value].ComparisonText + " '" +
							       Conv.ToSql(cond.Value.Value) +
							       "' ";
						}
					} else if (o.IsOfType(ConditionType.EXISTING_CONDITION)) {
						dynamic dbCondition = o.Condition;
						var conds = new Conditions(instance, process, authenticatedSession);
						var c2 = conds.GetCondition((int)dbCondition.ConditionId.Value);

						var expectedResult = Conv.ToBool(dbCondition.Value.Value);

						exp = GetExpressionStubForI2(i2, exp, operators, o, false);

						if (c2.ItemIds.Count > 0 &&
						    !c2.ItemIds.Intersect((List<int>)authenticatedSession.GetMember("groups")).Any()) {
							exp += " " + (!expectedResult).ToString().ToLower() + " ";
						} else {
							var exp2 = "";
							if (c.ConditionId != c2.ConditionId) {
								exp2 +=
									" " + GetItemlessConditionExpression(process, c2) +
									" " +
									cType.GetComparisonTypes()[(int)dbCondition.ComparisonTypeId.Value]
										.ComparisonText + " " +
									(string)dbCondition.Value.Value + "  ";
							} else {
								exp2 += " false ";
							}

							if (exp2.Contains("== false")) exp2 = " NOT " + exp2.Replace("== false", "");

							exp += exp2;
						}
					} else if (o.IsOneOfType(ConditionType.SIGNED_IN_USER_CONDITION,
						           ConditionType.PARENT_USER_CONDITION)) {
						dynamic userCondition = o.Condition;

						ItemColumn ic1 = null;
						if (o.IsOfType(ConditionType.SIGNED_IN_USER_CONDITION) &&
						    (userCondition.UserColumnTypeId == 1 || userCondition.UserColumnTypeId == 2)) {
							ic1 = itemCols.GetItemColumn((int)userCondition.ItemColumnId.Value);
						}

						exp = GetExpressionStubForI2(i2, exp, operators, o);

						if (userCondition.UserColumnTypeId == 1) {
							var itemCols2 = new ItemColumns(instance, "user", authenticatedSession);
							var ic2 = itemCols2.GetItemColumn((int)userCondition.UserColumnId.Value);
							var p2 = new ItemBase(instance, "user", authenticatedSession);
							var d2 = p2.GetItem(authenticatedSession.item_id,
								checkRights: false, onlyConditionColumns: true);

							var val = new object();
							var val2 = new object();

							if (ic1.IsDatabasePrimitive()) {
								exp += " COALESCE(pf." + ic1.Name + ",0) " +
								       cType.GetComparisonTypes()[(int)userCondition.ComparisonTypeId.Value]
									       .ComparisonText + " '" +
								       d2[ic2.Name] + "' ";
							} else if (ic1.IsProcess() || ic1.IsList()) {
								exp +=
									" COALESCE((SELECT COUNT(*) FROM item_data_process_selections idps1 inner join item_data_process_selections idps2 " +
									" on idps1.selected_item_id = idps2.selected_item_id  " +
									" where idps1.item_id = " + authenticatedSession.item_id +
									" and idps2.item_id = pf.item_id " +
									" and idps1.item_column_id = " + ic2.ItemColumnId +
									" and idps2.item_column_id = " + ic1.ItemColumnId +
									" ), 0 ) > 0 ";
							} else if (ic1.IsSubquery() && ic1.SubDataType == 6) {
								var query = RegisteredSubQueries.GetType(ic1.DataAdditional, ic1.ItemColumnId);
								exp +=
									" COALESCE((SELECT COUNT(*) FROM item_data_process_selections idps1 where idps1.item_id = " +
									authenticatedSession.item_id + " AND idps1.item_column_id = " + ic2.ItemColumnId +
									" AND idps1.selected_item_id = " + query.GetQueryCachedSql(instance, ic1.Name) +
									"), 0 ) > 0 ";
							}
						} else if (userCondition.UserColumnTypeId == 2) {
							if (ic1.IsInt()) {
								exp += " " + ic1.Name + " = " + authenticatedSession.item_id + " ";
							} else if (ic1.IsProcess()) {
								exp +=
									" (SELECT COUNT(selected_item_id) FROM item_data_process_selections WHERE item_id = pf.item_id AND item_column_id = " +
									ic1.ItemColumnId + " AND selected_item_id = " + authenticatedSession.item_id +
									") > 0 ";
							} else if (ic1.IsSubquery() &&
							           ic1.IsOfSubType(ItemColumn.ColumnType.Process)) {
								var query = RegisteredSubQueries.GetType(ic1.DataAdditional, ic1.ItemColumnId);
								//var filterSql = query.GetFilterSql(instance);

								exp += query.GetFilterSql(instance).Replace("##idString##",
									Conv.ToStr(authenticatedSession.item_id));
							}
						} else if (userCondition.UserColumnTypeId == 3) {
							if (process == "user") {
								exp += " pf.item_id = " + authenticatedSession.item_id;
							} else {
								exp += " false ";
							}
						}
					} else if (o.IsOfType(ConditionType.PARENT_PROCESS_CONDITION)) {
						dynamic cond = o.Condition;

						exp = GetExpressionStubForI2(i2, exp, operators, o);


						if (cond.Process != null) {
							exp += " false ";
						} else if (cond.ParentItemColumnId != null) {
							var icParent = itemCols.GetItemColumn((int)cond.ParentItemColumnId.Value);
							var ic1 = itemCols.GetItemColumn((int)cond.ItemColumnId.Value);
							exp += " COALESCE((select " + ic1.Name + " from _" + instance +
							       "_task where item_id = pf." + icParent.Name +
							       "), '' ) " + cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value]
								       .ComparisonText + " '" +
							       Conv.ToSql(cond.Value.Value) + "' ";
						}
					} else if (o.ConditionTypeId == 6) {
						throw new NotImplementedException();
					} else if (o.IsOfType(ConditionType.DATE_DIFF_CONDITION)) {
						dynamic cond = o.Condition;
						var ic1 = itemCols.GetItemColumn((int)cond.ItemColumnId.Value);

						exp = GetExpressionStubForI2(i2, exp, operators, o);

						if (cond.ItemColumnId2 != null) {
							var ic2 = itemCols.GetItemColumn((int)cond.ItemColumnId2.Value);
							exp += " datediff(day, " + ic1.Name + ", " + ic2.Name + ") " +
							       cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value].ComparisonText + " " +
							       Conv.ToSql(cond.Value.Value) +
							       "  ";
						} else {
							exp += " datediff(day, " + ic1.Name + ", getdate()) " +
							       cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value].ComparisonText + " " +
							       Conv.ToSql(cond.Value.Value) +
							       "  ";
						}
					} else if (o.IsOfType(ConditionType.PARENT_PROCESS_EXISTING_CONDITION)) {
						dynamic cond = o.Condition;

						string parentProcess = Conv.ToStr(cond.ParentProcess);
						int linkItemColumnId = Conv.ToInt(cond.LinkColumnId);
						int parentConditionId = Conv.ToInt(cond.ConditionId);
						//bool expectation = Conv.toBool(cond.Value); not implemented
						var parentColumn = itemCols.GetItemColumn(linkItemColumnId);
						var ownerItemId = "";

						if (parentColumn.DataType == 5) {
							var idps = new ItemDataProcessSelections(instance, process, authenticatedSession);
							if (Conv.ToInt(parentColumn.RelativeColumnId) > 0) {
								ownerItemId = "(" + idps.GetParentItemDataProcessSelectionSql("pf.item_id",
									Conv.ToInt(parentColumn.RelativeColumnId)) + ")";
							} else {
								ownerItemId =
									"(" + idps.GetItemDataProcessSelectionSql("pf.item_id", parentColumn.ItemColumnId) +
									")";
							}
						} else {
							ownerItemId =
								"(SELECT item_id FROM _" + instance + "_" + parentProcess + " WHERE item_id = pf." +
								parentColumn.Name + ")";
						}

						var conds = new Conditions(instance, parentProcess, authenticatedSession);
						var c2 = conds.GetCondition(parentConditionId);

						exp = GetExpressionStubForI2(i2, exp, operators, o, false);


						if (c2.ItemIds.Count > 0 &&
						    !c2.ItemIds.Intersect((List<int>)authenticatedSession.GetMember("groups")).Any()) {
							//If groups have been defined for the condition, user must belong into at least one of them
							exp += " false ";
						} else {
							var exp2 = "";
							if (c.ConditionId != c2.ConditionId) {
								var e = " " + GetItemlessConditionExpression(parentProcess, c2) + " " +
								        cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value].ComparisonText +
								        " " +
								        (string)cond.Value.Value + "  ";

								exp2 += e;
							} else {
								exp2 += " false ";
							}

							if (exp2.Contains("== false")) exp2 = " NOT " + exp2.Replace("== false", "");
							exp += exp2;

							//todo: exp2 should contain pf.item_id for this condition so consider replacing that instead of exp
						}

						exp = exp.Replace("pf.item_id", ownerItemId);
					}

					i2++;
				}

				exp += ") ";

				if (exp.Length > 0) {
					if (exp.Count(f => f == '(') > exp.Count(f => f == ')')) {
						exp += " ) ";
					}

					if (exp.Count(f => f == '(') == exp.Count(f => f == ')')) {
						exp = " ( " + exp + " ) ";
					}
				}

				i++;
			}

			if (!hasOtherConds) Cache.Set(instance, "c_" + c.ConditionId + "_" + 0, exp);

			return exp;
		}


		private static string GetExpressionStubForI2(int i2, string exp, Operators operators, Operation o,
			bool openingParenthesisIfZero = true) {
			if (i2 > 0) {
				exp += operators.GetOperator((int)o.OperatorId).OperatorName;
			} else if (openingParenthesisIfZero) {
				exp += " ( ";
			}

			return exp;
		}

		public Dictionary<string, FeatureStates> GetFeatureStates(string process, int itemId, int? groupId = null) {
			var retval = new Dictionary<string, FeatureStates>();
			var ics = new ItemColumns(instance, process, authenticatedSession);

			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			var sql = ics.GetItemSql() + " WHERE item_id=@itemId ";
			var dt = db.GetDatatable(sql, DBParameters);

			sql = "SELECT * FROM item_data_process_selections WHERE item_id=@itemId";
			var dt2 = db.GetDatatable(sql, DBParameters);

			foreach (var feature in GetFeatures(process, groupId)) {
				if (feature == "portfolio" || feature == "progressProgress") continue;
				retval.Add(feature, GetEvaluatedStatesForFeature(process, feature, itemId, dt, dt2, groupId));
			}

			return retval;
		}

		public FeatureStates GetEvaluatedStatesForFeature(string process, string feature, int itemId,
			DataTable data = null, DataTable selections = null, int? groupId = null) {
			//Gets all configured states for feature
			var states = GetFeatureStates(process, feature);
			//Builds Structure with accessible groups, tabs, containers and their states -- MOST Expensive Operation
			var result = GetEvaluatedStates(process, feature, itemId, states, data, selections,
				groupId);

			return result;
		}

		public FeatureStates GetEvaluatedStates(string process, string feature, int itemId,
			List<string> statesToEvaluate,
			DataTable data = null, DataTable selections = null, int? groupId = null) {
			//Gets group, tab, container specific condition ids
			var s = GetConditionIdsForStates(process, feature, statesToEvaluate, groupId);

			//Gets item data id not provided
			SetDBParam(SqlDbType.NVarChar, "@process", process);
			SetDBParam(SqlDbType.Int, "@itemId", itemId);
			if (data == null) {
				var ics = new ItemColumns(instance, process, authenticatedSession);
				data = db.GetDatatable(ics.GetItemSql(onlyConditionColumns: true) + " WHERE item_id=@itemId",
					DBParameters);
			}

			//Gets item data process selections if not provided
			if (selections == null) {
				selections = db.GetDatatable("SELECT * FROM item_data_process_selections WHERE item_id=@itemId",
					DBParameters);
			}

			var conds = new Conditions(instance, process, authenticatedSession);
			var ss = new FeatureStates();

			//Loops all states and gets evaluated states from conditions
			foreach (var state in s.Keys) {
				if (!state.Contains("."))
					throw new Exception("not qualified state check: " + process + " - " + feature + " - " + state);

				//No group / tab / container selected
				foreach (var id in s[state].s) {
					var exp = GetConditionExpressionResult(process, itemId, conds.GetCondition(id), data);
					if (exp) {
						ss.States.Add(state);
						break;
					}
				}

				//Groups -- MOST EXPENSIVE OPERATION
				foreach (var gid in s[state].groupStates.Keys) {
					if (!ss.groupStates.ContainsKey(gid)) ss.groupStates.Add(gid, new EvaluatedGroupState());
					foreach (var id in s[state].groupStates[gid].s) {
						var exp = GetConditionExpressionResult(process, itemId, conds.GetCondition(id), data,
							selections);
						if (exp) {
							ss.groupStates[gid].s.Add(state);
							break;
						}
					}

					if (ss.States.Contains(state) && s[state].groupStates[gid].s.Count == 0) {
						if (!ss.groupStates[gid].s.Contains(state)) ss.groupStates[gid].s.Add(state);
					}

					if (feature != "meta") continue;

					//Tabs
					foreach (var tid in s[state].groupStates[gid].tabStates.Keys) {
						if (!ss.groupStates[gid].tabStates.ContainsKey(tid))
							ss.groupStates[gid].tabStates.Add(tid, new EvaluatedTabState());

						foreach (var id2 in s[state].groupStates[gid].tabStates[tid].s) {
							var exp = GetConditionExpressionResult(process, itemId, conds.GetCondition(id2), data,
								selections);
							if (exp) {
								ss.groupStates[gid].tabStates[tid].s.Add(state);
								break;
							}
						}

						if (ss.groupStates[gid].s.Contains(state) &&
						    s[state].groupStates[gid].tabStates[tid].s.Count == 0) {
							if (!ss.groupStates[gid].tabStates[tid].s.Contains(state))
								ss.groupStates[gid].tabStates[tid].s.Add(state);
						}

						//Containers
						foreach (var cid in s[state].groupStates[gid].tabStates[tid].containerStates.Keys) {
							if (!ss.groupStates[gid].tabStates[tid].containerStates.ContainsKey(cid)) {
								ss.groupStates[gid].tabStates[tid].containerStates
									.Add(cid, new EvaluatedContainerState());
							}

							foreach (var id3 in s[state].groupStates[gid].tabStates[tid].containerStates[cid].s) {
								var exp = GetConditionExpressionResult(process, itemId, conds.GetCondition(id3), data,
									selections);

								if (exp) {
									ss.groupStates[gid].tabStates[tid].containerStates[cid].s.Add(state);
									break;
								}
							}

							if (ss.groupStates[gid].tabStates[tid].s.Contains(state) &&
							    s[state].groupStates[gid].tabStates[tid].containerStates[cid].s.Count == 0) {
								if (!ss.groupStates[gid].tabStates[tid].containerStates[cid].s.Contains(state)) {
									ss.groupStates[gid].tabStates[tid].containerStates[cid].s.Add(state);
								}
							}
						}
					}
				}

				//If write state, then history is removed
				if (ss.States.Contains("meta.history") && ss.States.Contains("meta.write"))
					ss.States.Remove("meta.history");
				foreach (var gid in ss.groupStates.Keys) {
					if (ss.groupStates[gid].s.Contains("meta.history") &&
					    ss.groupStates[gid].s.Contains("meta.write")) {
						ss.groupStates[gid].s.Remove("meta.write");
					}

					if (feature != "meta") continue;

					foreach (var tid in ss.groupStates[gid].tabStates.Keys) {
						if (ss.groupStates[gid].tabStates[tid].s.Contains("meta.history") &&
						    ss.groupStates[gid].tabStates[tid].s.Contains("meta.write")) {
							ss.groupStates[gid].tabStates[tid].s.Remove("meta.write");
						}

						foreach (var cid in ss.groupStates[gid].tabStates[tid].containerStates.Keys) {
							if (ss.groupStates[gid].tabStates[tid].containerStates[cid].s.Contains("meta.history") &&
							    ss.groupStates[gid].tabStates[tid].containerStates[cid].s.Contains("meta.write")) {
								ss.groupStates[gid].tabStates[tid].containerStates[cid].s.Remove("meta.write");
							}
						}
					}
				}
			}

			//Return states object
			return ss;
		}

		public Dictionary<string, StateConditionIds> GetConditionIdsForStates(string process, string feature,
			List<string> states,
			int? groupId = null) {
			//Get states for one group
			var statesRetval = new Dictionary<string, StateConditionIds>();
			foreach (var state in states) {
				statesRetval.Add(state, GetConditionIdsForState(process, feature, state, groupId));
			}

			return statesRetval;
		}

		public StateConditionIds GetConditionIdsForState(string process, string feature, string state,
			int? groupId = null) {
			var conds = new Conditions(instance, process, authenticatedSession);
			var pg = new ProcessGroups(instance, process, authenticatedSession);
			var pgt = new ProcessGroupTabs(instance, process, authenticatedSession);
			var ptc = new ProcessTabContainers(instance, process, authenticatedSession);
			var pc = new ProcessContainers(instance, process, authenticatedSession);
			var access = pc.GetAccessContainerIds();

			var s = new StateConditionIds();
			var conditions = conds.GetConditions(process, feature, state);
			foreach (var c in conditions) {
				if (c.ProcessGroupIds.Count == 0 && c.ProcessTabIds.Count == 0 && c.ProcessContainerIds.Count == 0) {
					if (!s.s.Contains(c.ConditionId)) s.s.Add(c.ConditionId);

					var gids = groupId == null ? pg.GetGroupIds() : new List<int> { (int)groupId };

					foreach (var id in gids) {
						if (!s.groupStates.ContainsKey(id)) s.groupStates.Add(id, new GroupStateConditionIds());
						if (!s.groupStates[id].s.Contains(c.ConditionId)) s.groupStates[id].s.Add(c.ConditionId);

						if (feature != "meta") continue;

						foreach (var tId in pgt.GetGroupTabIds(id)) {
							if (!s.groupStates[id].tabStates.ContainsKey(tId))
								s.groupStates[id].tabStates.Add(tId, new TabStateConditionIds());

							if (!s.groupStates[id].tabStates[tId].s.Contains(c.ConditionId))
								s.groupStates[id].tabStates[tId].s.Add(c.ConditionId);

							foreach (var cId in ptc.GetTabContainerIds(tId)) {
								if (!s.groupStates[id].tabStates[tId].containerStates.ContainsKey(cId)) {
									s.groupStates[id].tabStates[tId].containerStates
										.Add(cId, new ContainerStateConditionIds());
								}

								if (!s.groupStates[id].tabStates[tId].containerStates[cId].s.Contains(c.ConditionId))
									s.groupStates[id].tabStates[tId].containerStates[cId].s.Add(c.ConditionId);
							}
						}
					}
				}

				{
					var gids = groupId == null ? pg.GetGroupIds() : new List<int> { (int)groupId };

					if (access.Count > 0 && !gids.Contains(0)) gids.Add(0);

					foreach (var id in gids) {
						if (!s.groupStates.ContainsKey(id)) s.groupStates.Add(id, new GroupStateConditionIds());

						if (feature != "meta") continue;

						foreach (var tId in pgt.GetGroupTabIds(id)) {
							if (!s.groupStates[id].tabStates.ContainsKey(tId))
								s.groupStates[id].tabStates.Add(tId, new TabStateConditionIds());

							foreach (var cId in ptc.GetTabContainerIds(tId)) {
								if (!s.groupStates[id].tabStates[tId].containerStates.ContainsKey(cId)) {
									s.groupStates[id].tabStates[tId].containerStates
										.Add(cId, new ContainerStateConditionIds());
								}
							}
						}

						if (id != 0) continue;
						if (!s.groupStates[id].tabStates.ContainsKey(0))
							s.groupStates[id].tabStates.Add(0, new TabStateConditionIds());
						foreach (var access_id in access) {
							if (!s.groupStates[id].tabStates[0].containerStates.ContainsKey(access_id)) {
								s.groupStates[id].tabStates[0].containerStates
									.Add(access_id, new ContainerStateConditionIds());
							}
						}
					}

					foreach (var id in c.ProcessGroupIds) {
						if (s.groupStates.ContainsKey(id) && c.ProcessTabIds.Count == 0 &&
						    c.ProcessContainerIds.Count == 0) {
							s.groupStates[id].s.Add(c.ConditionId);
						}
					}

					if (feature != "meta") continue;

					foreach (var id in c.ProcessTabIds) {
						foreach (var gId in s.groupStates.Keys) {
							if (c.ProcessGroupIds.Count > 0 && !c.ProcessGroupIds.Contains(gId)) continue;

							if (s.groupStates[gId].tabStates.ContainsKey(id))
								s.groupStates[gId].tabStates[id].s.Add(c.ConditionId);
						}
					}

					foreach (var id in c.ProcessContainerIds) {
						foreach (var gId in s.groupStates.Keys) {
							foreach (var tId in s.groupStates[gId].tabStates.Keys) {
								if (c.ProcessGroupIds.Count > 0 && !c.ProcessGroupIds.Contains(gId)) continue;

								if (c.ProcessTabIds.Count > 0 && !c.ProcessTabIds.Contains(tId)) continue;

								if (s.groupStates[gId].tabStates[tId].containerStates.ContainsKey(id))
									s.groupStates[gId].tabStates[tId].containerStates[id].s.Add(c.ConditionId);
							}
						}
					}
				}
			}

			return s;
		}

		public Dictionary<string, Dictionary<int, List<string>>> GetStatesForGroups(string process, string feature,
			List<string> states) {
			var conds = new Conditions(instance, process, authenticatedSession);
			var retval = new Dictionary<string, Dictionary<int, List<string>>>();

			foreach (var state in states) {
				var GroupStates = new Dictionary<int, List<string>>();

				foreach (var c in conds.GetConditions(process, feature, state)) {
					if (c.ItemIds.Count > 0 &&
					    !c.ItemIds.Intersect((List<int>)authenticatedSession.GetMember("groups")).Any()) {
						continue;
					}

					var exp = GetItemlessConditionExpression(process, c);

					foreach (var id in c.ProcessGroupIds) {
						if (!GroupStates.ContainsKey(id)) GroupStates.Add(id, new List<string>());
						GroupStates[id].Add(exp);
					}
				}

				retval.Add(state, GroupStates);
			}

			return retval;
		}

		public bool HasNotificatioConditions(string instance, string process) {
			var ncs = new NotificationConditions(instance, process, authenticatedSession);
			return ncs.HasNotificationConditionsInUse(instance, process);
		}

		public void CheckNotifications(string process, int itemId, Dictionary<string, object> oldData,
			Dictionary<string, object> newData) {
			var ncs = new NotificationConditions(instance, process, authenticatedSession);
			var conditions = new Conditions(instance, process, authenticatedSession);
			var selections = new DataTable("selections");
			selections.Columns.Add(new DataColumn("item_id", typeof(int)));
			selections.Columns.Add(new DataColumn("item_column_id", typeof(int)));
			selections.Columns.Add(new DataColumn("selected_item_id", typeof(int)));

			var ic = new ItemColumns(instance, process, authenticatedSession);
			var cols = ic.GetItemColumns();
			foreach (var (key, value) in oldData) {
				if (value == null) continue;
				if (value.GetType() != typeof(List<int>)) continue;
				var col = cols.FirstOrDefault(c => c.Name == key);
				if (col == null) continue;
				foreach (var i in (List<int>)value) {
					var row = selections.NewRow();
					row["item_column_id"] = col.ItemColumnId;
					row["item_id"] = itemId;
					row["selected_item_id"] = i;
					selections.Rows.Add(row);
				}
			}

			//Create selections object from old data
			//var selections = db.GetDatatable(sql, null);
			//Otherwise GetItemsConditionExpression will get values from database and the values are new

			foreach (var nc in ncs.GetNotificationConditions(0, 1)) {
				if (nc.InUse && nc.ConditionType == 0 && nc.ItemId != null && nc.BeforeConditionId != null &&
				    nc.AfterConditionId != null &&
				    (nc.ItemColumnIds.Count > 0 || nc.UserGroupIds.Count > 0)) {
					var beforeExp = GetItemsConditionExpression(process, itemId,
						conditions.GetCondition(Conv.ToInt(nc.BeforeConditionId)), null, selections, oldData, true);

					var afterExp = GetItemsConditionExpression(process, itemId,
						conditions.GetCondition(Conv.ToInt(nc.AfterConditionId)),
						null, null, newData, true);

					if (EvaluateExpression(beforeExp) && EvaluateExpression(afterExp)) {
						SendNotification(process, itemId, (int)nc.ItemId, nc.ItemColumnIds.ToArray(), newData,
							nc.View);
					}
				}

				if (nc.ProcessActionId != 0) {
					var beforeExp = GetItemsConditionExpression(process, itemId,
						conditions.GetCondition(Conv.ToInt(nc.BeforeConditionId)), null, selections, oldData, true);

					var afterExp = GetItemsConditionExpression(process, itemId,
						conditions.GetCondition(Conv.ToInt(nc.AfterConditionId)),
						null, null, newData, true);

					if (Conv.ToInt(nc.ProcessActionId) == 0 || !EvaluateExpression(beforeExp) ||
					    !EvaluateExpression(afterExp)) continue;
					var actions = new ProcessActions(instance, process, authenticatedSession);
					actions.DoAction(Conv.ToInt(nc.ProcessActionId), itemId, true, null, false, true);
				}
			}
		}

		public void CheckNotificationsForCreate(string process, int itemId, Dictionary<string, object> data) {
			var ncs = new NotificationConditions(instance, process, authenticatedSession);
			foreach (var nc in ncs.GetNotificationConditions(1, 1)) {
				if (nc.InUse && nc.ConditionType == 1) {
					SendNotification(process, itemId, (int)nc.ItemId, nc.ItemColumnIds.ToArray(), data, nc.View);
				}
			}
		}

		public void CheckNotificationsForDelete(string process, int itemId) {
			var ncs = new NotificationConditions(instance, process, authenticatedSession);
			foreach (var nc in ncs.GetNotificationConditions(2, 1)) {
				if (nc.InUse && nc.ConditionType == 2) {
					var p = new ItemBase(instance, process, authenticatedSession);
					var data = p.GetItem(itemId, checkRights: false);
					SendNotification(process, itemId, (int)nc.ItemId, nc.ItemColumnIds.ToArray(), data, nc.View);
				}
			}
		}

		public void SendNotification(string process, int itemId, int notificationId, string[] receivers,
			Dictionary<string, object> data, string view = null, int[] userGroupReceivers = null,
			ActionRowEmailViewOptions arevo = null, int notificationSubType = 1, int excludeSender = 0) {
			var ib = new ItemBase(instance, "notification", authenticatedSession);
			var notification = ib.GetItem(Conv.ToInt(notificationId), checkRights: false);
			var translations = new Translations(instance, authenticatedSession);
			var mailsByLanguage = new Dictionary<string, Dictionary<string, string>>();

			var instanceDefaultLanguage =
				Conv.ToStr(db.ExecuteScalar(
					"SELECT default_locale_id FROM instances WHERE instance = '" + Conv.ToSql(instance) + "'"));

			foreach (DataRow language in db.GetDatatable("SELECT code FROM instance_languages", null, inThread: true)
				         .Rows) {
				var d = new Dictionary<string, string>();
				d.Add("body",
					translations.GetTranslation((Dictionary<string, string>)notification["body_variable"],
						Conv.ToStr(language["code"])));
				d.Add("title",
					translations.GetTranslation((Dictionary<string, string>)notification["title_variable"],
						Conv.ToStr(language["code"])));
				mailsByLanguage.Add(Conv.ToStr(language["code"]), d);
			}

			var emailList = new Dictionary<string, Email>();

			foreach (var x in mailsByLanguage) {
				var title_ = x.Value["title"];
				var body_ = x.Value["body"];

				var bodyNew = JsonConvert.DeserializeObject<Notification>(body_);

				if (title_ != null) {
					title_ = ReplaceTags(process, data, title_, view, arevo);
					var p = new Processes(instance, authenticatedSession);
					foreach (var pp in p.GetParentProcesses(false, process)) {
						var parentId = ib.GetParentItemId(itemId, pp.ProcessName);
						if (parentId > 0) {
							var ibParent = new ItemBase(instance, pp.ProcessName, authenticatedSession);
							title_ = ReplaceTags(pp.ProcessName, ibParent.GetItem(parentId, checkRights: false), title_,
								view, arevo);
						}
					}
				} else {
					title_ = "";
				}

				if (bodyNew != null) {
					bodyNew.html = ReplaceTags(process, data, bodyNew.html, view, arevo);
					var p = new Processes(instance, authenticatedSession);
					foreach (var pp in p.GetParentProcesses(false, process)) {
						var parentId = ib.GetParentItemId(itemId, pp.ProcessName);
						if (parentId > 0) {
							var ibParent = new ItemBase(instance, pp.ProcessName, authenticatedSession);
							bodyNew.html = ReplaceTags(pp.ProcessName, ibParent.GetItem(parentId, checkRights: false),
								bodyNew.html, view, arevo);
						}
					}
				} else {
					bodyNew = new Notification();
					bodyNew.html = "";
				}

				x.Value["body"] = bodyNew.html;

				bodyNew.digest = Conv.ToInt(notification["digest"]);
				Email email_ = null;
				if (Conv.ToInt(notification["type"]) == 0) {
					email_ = new Email(instance, authenticatedSession, bodyNew.html, title_);
					emailList.Add(x.Key, email_);
				}
			}

			var languagesFound = new List<string>();

			var ib2 = new ItemBase(instance, "notification_history", authenticatedSession);
			var itemColumns = new ItemColumns(instance, process, authenticatedSession);

			var resolvedReceivers = new List<int>();

			foreach (var itemColumnId in receivers) {
				if (itemColumnId.Contains(";")) {
					var ic1ColumnId = Conv.ToInt(itemColumnId.Split(';')[0]);
					var ic2ColumnId = Conv.ToInt(itemColumnId.Split(';')[2]);
					var ic1 = itemColumns.GetItemColumn(ic1ColumnId);
					var ic1Process = ic1.Process;
					var itemColumns2 = new ItemColumns(instance, ic1Process, authenticatedSession);
					var ic2 = itemColumns2.GetItemColumn(ic2ColumnId);


					if (ic1.IsList() && ic2.IsProcess()) {
						var idpss1 = GetItemDataProcessSelections(ic1.DataAdditional);
						foreach (var idps1 in idpss1.GetItemDataProcessSelection(itemId,
							         ic1.ItemColumnId)) {
							var idpss = GetItemDataProcessSelections(ic1Process);

							foreach (var idps in idpss.GetItemDataProcessSelection(
								         idps1.SelectedItemId, ic2ColumnId)) {
								SetDBParam(SqlDbType.Int, "@itemId", idps.SelectedItemId);
								var userLanguage = Conv.ToStr(db.ExecuteScalar(
									"SELECT locale_id FROM _" + instance + "_user WHERE item_id = @itemId",
									DBParameters));

								var hItemId = ib2.InsertItemRow();
								var history_row =
									ib2.GetItem(Conv.ToInt(hItemId), checkRights: false);

								resolvedReceivers.Add(idps.SelectedItemId);

								if (Conv.ToInt(notification["type"]) == 0) {
									emailList[userLanguage].Add((int)idps.SelectedItemId, Email.AddressTypes.ToAdress);
									if (!languagesFound.Contains(userLanguage))
										languagesFound.Add(userLanguage);
								}

								history_row["type"] = notification["type"];
								history_row["notification_item_id"] = notificationId;
								history_row["title"] = mailsByLanguage[userLanguage]["title"];
								history_row["body"] = mailsByLanguage[userLanguage]["body"];
								history_row["is_read"] = 0;
								history_row["receiver_item_id"] = idps.SelectedItemId;
								ib2.SaveItem(history_row, checkRights: false);
							}
						}
					}
				} else {
					var ic = itemColumns.GetItemColumn(Conv.ToInt(itemColumnId));
					var ibParent = new ItemBase(instance, process, authenticatedSession);
					data = ibParent.GetItem(Conv.ToInt(data["item_id"]), checkRights: false);

					if (ic.IsNvarChar() && Conv.ToStr(data[ic.Name]).Length > 0) {
						var hItemId = ib2.InsertItemRow();
						var history_row = ib2.GetItem(Conv.ToInt(hItemId), checkRights: false);

						if (Conv.ToInt(notification["type"]) == 0) {
							foreach (string email_address in Conv.ToStr(data[ic.Name])
								         .Split(new string[] { ",", ";", " " },
									         StringSplitOptions.RemoveEmptyEntries)) {
								emailList[instanceDefaultLanguage].Add(email_address, Email.AddressTypes.ToAdress);
							}
						}

						if (!languagesFound.Contains(instanceDefaultLanguage))
							languagesFound.Add(instanceDefaultLanguage);
						history_row["type"] = notification["type"];
						history_row["notification_item_id"] = notificationId;
						history_row["title"] = mailsByLanguage[instanceDefaultLanguage]["title"];
						history_row["body"] = mailsByLanguage[instanceDefaultLanguage]["body"];
						history_row["is_read"] = 0;
						history_row["receiver_item_id"] = Conv.ToInt(data[ic.Name]);
						ib2.SaveItem(history_row);
					} else if (ic.IsInt()) {
						var hItemId = ib2.InsertItemRow();
						var history_row = ib2.GetItem(Conv.ToInt(hItemId), checkRights: false);
						resolvedReceivers.Add(Conv.ToInt(data[ic.Name]));
						if (Conv.ToInt(notification["type"]) == 0) {
							var recItemId = Conv.ToInt(data[ic.Name]);
							var recProcess =
								new Processes(instance, authenticatedSession).GetProcessByItemId(recItemId);
							switch (recProcess) {
								case "user":
									if (!languagesFound.Contains(instanceDefaultLanguage))
										languagesFound.Add(instanceDefaultLanguage);
									emailList[instanceDefaultLanguage].Add(recItemId, Email.AddressTypes.ToAdress);
									break;
								case "list_cost_center":
									var ib3 = new ItemBase(instance, recProcess, authenticatedSession);
									var recItem = ib3.GetItem(recItemId, checkRights: false);
									if (recItem.ContainsKey("owner")) {
										foreach (var receiverItemId in (List<int>)recItem["owner"]) {
											SetDBParam(SqlDbType.Int, "@itemId", receiverItemId);
											var userLanguage = Conv.ToStr(db.ExecuteScalar(
												"SELECT locale_id FROM _" + instance + "_user WHERE item_id = @itemId",
												DBParameters));
											if (!languagesFound.Contains(userLanguage))
												languagesFound.Add(userLanguage);
											emailList[userLanguage].Add(receiverItemId, Email.AddressTypes.ToAdress);
										}
									}

									break;
							}
						}

						history_row["type"] = notification["type"];
						history_row["notification_item_id"] = notificationId;
						history_row["title"] = mailsByLanguage[instanceDefaultLanguage]["title"];
						history_row["body"] = mailsByLanguage[instanceDefaultLanguage]["body"];
						history_row["is_read"] = 0;
						history_row["receiver_item_id"] = Conv.ToInt(data[ic.Name]);
						ib2.SaveItem(history_row);
					} else if (ic.IsProcess() || ic.IsList()) {
						var idpss = GetItemDataProcessSelections(process);
						foreach (var idps in idpss.GetItemDataProcessSelection(itemId,
							         Conv.ToInt(itemColumnId))
						        ) {
							var userLanguage = Conv.ToStr(db.ExecuteScalar(
								"SELECT locale_id FROM _" + instance + "_user WHERE item_id = " + idps.SelectedItemId,
								null));

							if (!languagesFound.Contains(userLanguage)) {
								languagesFound.Add(userLanguage);
							}

							resolvedReceivers.Add(idps.SelectedItemId);

							var hItemId = ib2.InsertItemRow();
							var history_row =
								ib2.GetItem(Conv.ToInt(hItemId), checkRights: false);
							if (Conv.ToInt(notification["type"]) == 0) {
								emailList[userLanguage].Add((int)idps.SelectedItemId, Email.AddressTypes.ToAdress);
							}

							history_row["type"] = notification["type"];
							history_row["notification_item_id"] = notificationId;
							history_row["title"] = mailsByLanguage[userLanguage]["title"];
							history_row["body"] = mailsByLanguage[userLanguage]["body"];
							history_row["is_read"] = 0;
							history_row["receiver_item_id"] = idps.SelectedItemId;
							ib2.SaveItem(history_row, checkRights: false);
						}
					} else if ((ic.IsSubquery() && ic.SubDataType == 5) || (ic.IsEquation() && ic.SubDataType == 5)) {
						var ipbase = new ItemBase(instance, process, authenticatedSession);

						var fromSendItem = ipbase.GetItem(itemId, checkRights: false);

						foreach (KeyValuePair<string, object> k in fromSendItem) {
							if (k.Key == ic.Name) {
								IList collection = (IList)k.Value;
								foreach (var ff in collection) {
									SetDBParam(SqlDbType.Int, "@itemId", ff);
									var userLanguage = Conv.ToStr(db.ExecuteScalar(
										"SELECT locale_id FROM _" + instance + "_user WHERE item_id = @itemId",
										DBParameters));

									if (!languagesFound.Contains(userLanguage)) {
										languagesFound.Add(userLanguage);
									}

									resolvedReceivers.Add((int)ff);
									if (Conv.ToInt(notification["type"]) == 0) {
										emailList[userLanguage].Add((int)ff, Email.AddressTypes.ToAdress);
									}

									var hItemId = ib2.InsertItemRow();
									var history_row =
										ib2.GetItem(Conv.ToInt(hItemId), checkRights: false);
									history_row["type"] = notification["type"];
									history_row["notification_item_id"] = notificationId;
									history_row["title"] = mailsByLanguage[userLanguage]["title"];
									history_row["body"] = mailsByLanguage[userLanguage]["body"];
									history_row["is_read"] = 0;
									history_row["receiver_item_id"] = ff;
									ib2.SaveItem(history_row, checkRights: false);
								}
							}
						}
					}
				}
			}

			if (userGroupReceivers != null && userGroupReceivers.Length > 0) {
				var idpss = GetItemDataProcessSelections("user_group");
				var ugItemColumns = new ItemColumns(instance, "user_group", authenticatedSession);
				var userCol = ugItemColumns.GetItemColumnByName("user_id");

				foreach (var userGroupId in userGroupReceivers) {
					foreach (var idps in idpss.GetItemDataProcessSelection(userGroupId,
						         userCol.ItemColumnId)) {
						SetDBParam(SqlDbType.Int, "@itemId", idps.SelectedItemId);
						var userLanguage = Conv.ToStr(db.ExecuteScalar(
							"SELECT locale_id FROM _" + instance + "_user WHERE item_id = @itemId",
							DBParameters));

						if (!languagesFound.Contains(userLanguage)) {
							languagesFound.Add(userLanguage);
						}

						resolvedReceivers.Add(idps.SelectedItemId);

						var hItemId = ib2.InsertItemRow();
						var history_row = ib2.GetItem(Conv.ToInt(hItemId), checkRights: false);
						if (Conv.ToInt(notification["type"]) == 0) {
							emailList[userLanguage].Add((int)idps.SelectedItemId, Email.AddressTypes.ToAdress);
						}

						history_row["type"] = notification["type"];
						history_row["notification_item_id"] = notificationId;
						history_row["title"] = mailsByLanguage[userLanguage]["title"];
						history_row["body"] = mailsByLanguage[userLanguage]["body"];
						history_row["is_read"] = 0;
						history_row["receiver_item_id"] = idps.SelectedItemId;
						ib2.SaveItem(history_row, checkRights: false);
					}
				}
			}

			if (authenticatedSession != null && excludeSender == 1) {
				emailList[authenticatedSession.locale_id]
					.Remove(authenticatedSession.item_id, Email.AddressTypes.ToAdress);
				resolvedReceivers.Remove(authenticatedSession.item_id);
			}

			//if (Conv.ToInt(notification["type"]) == 0 && notificationSubType != 2) {
			foreach (var mail in emailList) {
				if (languagesFound.Contains(mail.Key)) {
					//Notification type 0 aka "mail" and if subtype is 1 (send only email) or 3 (send both notification and email) or empty we are sending an email
					if (Conv.ToInt(notification["type"]) == 0 && (notificationSubType == 1 ||
					                                              notificationSubType == 3 ||
					                                              notificationSubType == 0)) {
						mail.Value.Send(notificationItemId: notificationId, async: false,
							digest: Conv.ToBool(notification["digest"]));
					}
				}
			}

			//Notification type 0 aka "mail" and subtype is not 1 we also create a notification
			if (Conv.ToInt(notification["type"]) == 0 && notificationSubType != 1 && notificationSubType != 0) {
				CreateNotification(resolvedReceivers, emailList);
			}
		}

		private void CreateNotification(List<int> receivers, Dictionary<string, Email> emails) {
			var ib = new ItemBase(instance, "notification", authenticatedSession);
			var o = new APISaveResult();

			o.Data = new Dictionary<string, object>();

			var data = ib.AddRow(o, 0, "notification");
			var pb = new ProcessBase(instance, "notification", authenticatedSession);
			var translations = new Translations(instance, authenticatedSession);

			data.Data["name_variable"] = "{}";
			data.Data["title_variable"] = "{}";
			data.Data["body_variable"] = "{}";
			data.Data["type"] = 1;

			ib.SaveRow(Conv.ToInt(data.Data["item_id"]), data);
			pb.SetDBParam(SqlDbType.Int, "@itemId", Conv.ToInt(data.Data["item_id"]));

			var t = pb.db.GetDataRow(
				"SELECT name_variable, title_variable, body_variable FROM _" + instance +
				"_notification WHERE item_id = @itemId", pb.DBParameters);


			pb.SetDBParam(SqlDbType.Int, "@item_column_id", Conv.ToInt(pb.db.ExecuteScalar(
				"SELECT item_column_id FROM item_columns where name = 'user_item_id' AND process = 'notification'")));

			try {
				var SignalR = new ApiPush(Startup.SignalR);
				var parameters = new List<object>();
				var collectParameters = new List<Dictionary<string, string>>();
				foreach (int receiver in receivers) {
					pb.SetDBParam(SqlDbType.Int, "@selected_item_id", Conv.ToInt(receiver));
					pb.db.ExecuteInsertQuery(
						"INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES(@itemId, @item_column_id, @selected_item_id)",
						pb.DBParameters);

					string localeId =
						Conv.ToStr(pb.db.ExecuteScalar(
							"SELECT locale_id FROM _" + instance + "_user WHERE item_id = @selected_item_id",
							pb.DBParameters));

					var bodyText = emails[localeId]._body;
					var nameText = emails[localeId]._subject;
					var titleText = emails[localeId]._subject;

					var messageAsHtml = "{\"html\":\"" + bodyText.Replace("\"", "'") + " " + "\"}";

					translations.insertOrUpdateProcessTranslation("notification", Conv.ToStr(t["body_variable"]),
						messageAsHtml, localeId);
					translations.insertOrUpdateProcessTranslation("notification", Conv.ToStr(t["title_variable"]),
						titleText, localeId);
					translations.insertOrUpdateProcessTranslation("notification", Conv.ToStr(t["name_variable"]),
						nameText, localeId);

					var y = new Dictionary<string, string> {
						["Notificationid"] = Conv.ToStr(data.Data["item_id"]),
						["UserItemId"] = Conv.ToStr(receiver),
						["link"] = "",
						["success"] = "true"
					};
					collectParameters.Add(y);
				}

				parameters.Add(collectParameters);
				SignalR.SendToAll("NotificationsChanged", parameters);
			} catch (Exception e) {
				Console.WriteLine(e);
			}
		}

		public string ReplaceTags(string process, Dictionary<string, object> data, string notification,
			string view = null, ActionRowEmailViewOptions emailViewOptions = null, bool allow_nulls = false) {
			if (notification == null) {
				notification = "";
			}

			var bodyTags = notification.Split(new[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);
			var bodyTagIds = new List<int>();

			var tags = db.GetDatatable("SELECT ISNULL(item_id, 0) AS item_id, list_item FROM _" + instance +
			                           "_notification_tags", null, inThread: true);

			foreach (var tag in bodyTags) {
				try {
					var id = 0;
					var r = tags.Select("list_item = '" + Conv.Escape(tag) + "'");
					id = r.Length > 0 ? Conv.ToInt(r[0]["item_id"]) : 0;
					if (id != 0) bodyTagIds.Add(id);
				} catch (Exception e) {
					Diag.SupressException(e);
					Console.WriteLine("Cannot compare with tag '" + tag + "'");
				}
			}

			if (bodyTagIds.Count > 0) {
				foreach (DataRow r in db.GetDatatable(
					         "SELECT json_tag_id, ntl.item_column_id, name, ic.data_type, ic.data_additional, ic.item_column_id, tg.list_item FROM _" +
					         instance +
					         "_notification_tag_links ntl INNER JOIN _" + instance +
					         "_notification_tags tg ON tg.item_id = ntl.json_tag_id INNER JOIN item_columns ic ON ntl.item_column_id = ic.item_column_id WHERE process_name = '" +
					         Conv.ToSql(process) + "' AND tag_type = 0 AND json_tag_id IN (" +
					         string.Join(",", bodyTagIds.ToArray()) + ")", null, inThread: true).Rows) {
					if (data.ContainsKey(Conv.ToStr(r["name"]))) {
						if (Conv.ToInt(r["data_type"]) == 5 || Conv.ToInt(r["data_type"]) == 6 ||
						    Conv.ToInt(r["data_type"]) == 16) {
							var _itemColumns = new ItemColumns(instance, process, authenticatedSession);

							var itemList = new List<int>();
							string processNameHere = Conv.ToStr(r["data_additional"]);

							var icSubDataType = 1000;

							if (Conv.ToInt(r["data_type"]) == 16) {
								var ic = _itemColumns.GetItemColumn(Conv.ToInt(r["item_column_id"]));
								icSubDataType = ic.SubDataType;
								if (icSubDataType == 4 &&
								    Conv.ToStr(data[Conv.ToStr(r["name"])]).Contains(@"""html"":")) {
									var rich_text = "";
									if (data[Conv.ToStr(r["name"])] != null)
										rich_text = JToken.Parse(Conv.ToStr(data[Conv.ToStr(r["name"])]))
											.Value<string>("html");

									notification = notification.Replace("[" + r["list_item"] + "]",
										rich_text);
								} else if (allow_nulls && icSubDataType == 4 &&
								           Conv.ToStr(data[Conv.ToStr(r["name"])]) == "") {
									notification = notification.Replace("[" + r["list_item"] + "]",
										"null");
								} else if (icSubDataType < 5) {
									notification = notification.Replace("[" + r["list_item"] + "]",
										Conv.ToStr(data[Conv.ToStr(r["name"])]));
								} else if (icSubDataType == 5 || icSubDataType == 6) {
									processNameHere = ic.SubDataAdditional;
									IList collectionOfSelectedIds = (IList)data[Conv.ToStr(r["name"])];
									foreach (var item_id in collectionOfSelectedIds) {
										itemList.Add(Conv.ToInt(item_id));
									}
								}
							} else {
								itemList = data[Conv.ToStr(r["name"])] as List<int>;
							}

							var ib = new ItemBase(instance, processNameHere, authenticatedSession);

							var list_data = "";
							foreach (var id in itemList) {
								if (Conv.ToInt(r["data_type"]) == 5 ||
								    Conv.ToInt(r["data_type"]) == 16 && icSubDataType == 5) {
									var item = ib.GetItem(id, checkRights: false);
									var ppcs2 =
										new ProcessPortfolioColumns(instance, processNameHere,
											authenticatedSession);

									foreach (var pc2 in ppcs2.GetPrimaryColumns()) {
										if (list_data.Length > 0) {
											list_data += " " + item[pc2.ItemColumn.Name];
										} else {
											list_data += item[pc2.ItemColumn.Name];
										}
									}
								} else {
									var item = ib.GetItem(id, checkRights: false);
									if (list_data == "")
										list_data += item["list_item"];
									else
										list_data += ", " + item["list_item"];
								}
							}

							notification = notification.Replace("[" + r["list_item"] + "]", Conv.ToStr(list_data));
						} else if (Conv.ToInt(r["data_type"]) == 23) {
							var rich_text = "";
							if (data[Conv.ToStr(r["name"])] != null)
								rich_text = JToken.Parse(Conv.ToStr(data[Conv.ToStr(r["name"])])).Value<string>("html");

							notification = notification.Replace("[" + r["list_item"] + "]",
								rich_text);
						} else {
							if (Conv.ToInt(r["data_type"]) != 3 && Conv.ToInt(r["data_type"]) != 13) {
								notification = notification.Replace("[" + r["list_item"] + "]",
									Conv.ToStr(data[Conv.ToStr(r["name"])]));
							} else {
								//Converting date columns to {}-tag form so they can be converted to user format later
								if (Conv.ToStr(data[Conv.ToStr(r["name"])]) != "") {
									notification = notification.Replace("[" + r["list_item"] + "]",
										"{" + data[Conv.ToStr(r["name"])] + "}");
								} else {
									notification = notification.Replace("[" + r["list_item"] + "]",
										"");
								}

							}
						}
					}
				}
			}

			if (bodyTagIds.Count > 0) {
				foreach (DataRow r in db.GetDatatable(
					         "SELECT json_tag_id, tag_type, view_state, link_param, tg.list_item FROM _" + instance +
					         "_notification_tag_links ntl INNER JOIN _" + instance +
					         "_notification_tags tg ON tg.item_id = ntl.json_tag_id WHERE tag_type > 0 AND json_tag_id IN (" +
					         string.Join(",", bodyTagIds.ToArray()) + ")", null, inThread: true).Rows) {
					switch (Conv.ToInt(r["tag_type"])) {
						case 1:
							notification = notification.Replace("[" + r["list_item"] + "]",
								Conv.ToStr(DateTime.Now.ToUniversalTime()));
							break;
						case 2:
							if (!DBNull.Value.Equals(view) && view != null) {
								//Huom 1:
								//Mikäli meillä on olemassa ActionRowEmailViewOptions, alempana määriteltävän p-muuttujan prosessiksi tulee näiden ActionRowEmailViewOptions sisältä prosessi,
								//sekä item_id:si prosessidatan parent_item_id. Tämä kyseenalainen switcharoo siitä syystä, että koodillisesti sähköpostien kautta syntyvä näkymä on ylempiarvoinen
								//kuin esimerkiksi pääprosessin datatable josta rivi on avattu, ja sähköposti lähetetty. Mikäli seuraavan iffin sisällä tehtäviä taikatemppuja ei tehdä
								//aukeaisi child näkymä vasemmalle ja parent näkymä oikealle, koska asetukset tehdään kuitenkin kohdeprosessin päässä käyttöliittymässä.

								//Huom2:
								//Mikäli jonkintyyppinen sähköposti ei toimi oikein (ei esimerkiksi splittaa tai osaa näyttää haluttua tabia prosessin sisältä), johtuu se todennäköisesti siitä, että
								//jonkun alempana olevan if-haaran sisältä puuttuu link options stringin muodostuksesta tämä ActionRowEmailViewOptions tutkiskelu

								var t = "";
								var name = "";
								if (emailViewOptions != null) {
									if (emailViewOptions.OpenRowProcess != "" &&
									    emailViewOptions.OpenRowItemColumnId > 0)
										name = new ItemColumns(instance, emailViewOptions.OpenRowProcess,
												authenticatedSession)
											.GetItemColumn(emailViewOptions.OpenRowItemColumnId).Name;

									emailViewOptions.ParentItemId = Conv.ToInt(data["item_id"]);
									t = emailViewOptions.OpenRowProcess;
									emailViewOptions.OpenRowProcess = process;
								}

								if (name == "") name = "owner_item_id";

								var uuid = Guid.NewGuid().ToString();
								if (view.StartsWith("process.tab_")) {
									var tabId = view.Substring(12);
									var p = emailViewOptions == null
										? "[{ \"name\":\"process.tab\",\"params\":{ \"process\":\"" + process +
										  "\",\"itemId\":" +
										  Conv.ToInt(data["item_id"]) + ",\"tabId\":" + tabId + "} }]"
										: "[{ \"name\":\"process.tab\",\"params\":{ \"process\":\"" + t +
										  "\",\"itemId\":" +
										  (data.ContainsKey(name) ? Conv.ToInt(data[name]) : 0) +
										  ",\"tabId\":" + tabId +
										  "}, \"SplitOptions\": " + JsonConvert.SerializeObject(emailViewOptions) +
										  " }]";
									var e = 9125;
									var l = new Links(instance, authenticatedSession);
									l.SetUrl(uuid, p, e);
								} else {
									var p = emailViewOptions == null
										? "[{ \"name\":\"" + view + "\",\"params\":{ \"process\":\"" + process +
										  "\",\"itemId\":" +
										  Conv.ToInt(data["item_id"]) + "} }]"
										: "[{ \"name\":\"" + view + "\",\"params\":{ \"process\":\"" + t +
										  "\",\"itemId\":" +
										  (data.ContainsKey(name)
											  ? Conv.ToInt(data[name])
											  : 0) + "}, \"SplitOptions\": " +
										  JsonConvert.SerializeObject(emailViewOptions) + " }]";
									var e = 9125;
									var l = new Links(instance, authenticatedSession);
									l.SetUrl(uuid, p, e);
								}

								notification = notification.Replace("[" + r["list_item"] + "]",
									"https://" +
									(authenticatedSession.GetMember("wwwpath") + "/?view=" + uuid).Replace("///", "/")
									.Replace("//", "/"));

								if (emailViewOptions != null) {
									emailViewOptions.OpenRowProcess = t;
								}
							} else if (data.ContainsKey("current_state")) {
								var uuid2 = Guid.NewGuid().ToString();
								if (Conv.ToStr(data["current_state"]).StartsWith("tab_")) {
									var tabId = Conv.ToStr(data["current_state"]).Substring(4);
									var p2 = "[{ \"name\":\"process.tab\",\"params\":{ \"process\":\"" + process +
									         "\",\"itemId\":" +
									         Conv.ToStr(data["item_id"]) + ",\"tabId\":" + tabId + "} }]";
									var e2 = 9125;
									var l = new Links(instance, authenticatedSession);
									l.SetUrl(uuid2, p2, e2);
								} else {
									var p2 = "[{ \"name\":\"process." + Conv.ToStr(data["current_state"]) +
									         "\",\"params\":{ \"process\":\"" +
									         process + "\",\"itemId\":" + Conv.ToStr(data["item_id"]) + "} }]";
									var e2 = 9125;
									var l = new Links(instance, authenticatedSession);
									l.SetUrl(uuid2, p2, e2);
								}

								notification = notification.Replace("[" + r["list_item"] + "]",
									"https://" +
									(authenticatedSession.GetMember("wwwpath") + "/?view=" + uuid2).Replace("///", "/")
									.Replace("//", "/"));
							} else {
								var uuid2 = Guid.NewGuid().ToString();
								var p2 = "[{ \"name\":\"process.tab\",\"params\":{ \"process\":\"" + process +
								         "\",\"itemId\":" +
								         Conv.ToStr(data["item_id"]) + "} }]";
								var e2 = 9125;
								var l = new Links(instance, authenticatedSession);
								l.SetUrl(uuid2, p2, e2);

								notification = notification.Replace("[" + r["list_item"] + "]",
									"https://" +
									(authenticatedSession.GetMember("wwwpath") + "/?view=" + uuid2).Replace("///", "/")
									.Replace("//", "/"));
							}

							break;
						case 3:
							var uuid3 = Guid.NewGuid().ToString();
							var view_state = Conv.ToStr(r["view_state"]);
							var link_param = Conv.ToStr(r["link_param"]);
							var p3 = "[{ \"name\":\"" + view_state + "\",\"params\":{ \"process\":\"" + process +
							         "\",\"itemId\":" +
							         Conv.ToStr(data["item_id"]) + "} }]";


							var e3 = 9125;
							var l2 = new Links(instance, authenticatedSession);
							l2.SetUrl(uuid3, p3, e3);
							notification = notification.Replace("[" + r["list_item"] + "]",
								"https://" +
								(authenticatedSession.GetMember("wwwpath") + "/?view=" + uuid3).Replace("///", "/")
								.Replace("//", "/"));

							if (link_param.Length > 0 && data.ContainsKey(link_param)) {
								data[link_param] = "https://" +
								                   (authenticatedSession.GetMember("wwwpath") + "?view=" + uuid3)
								                   .Replace("///", "/").Replace("//", "/");
								var ib = new ItemBase(instance, process, authenticatedSession);
								ib.SaveItem(data);
							}

							break;
						case 4:
							var uuid4 = Guid.NewGuid().ToString();
							var view_state4 = Conv.ToStr(r["view_state"]);
							var link_param4 = Conv.ToStr(r["link_param"]);


							if (data.ContainsKey(link_param4)) {
								var parentProcessId = Conv.ToInt(data[link_param4]);
								var parentProcess =
									new Processes(instance, authenticatedSession).GetProcessByItemId(parentProcessId);
								var p4 = "[{ \"name\":\"" + view_state4 + "\",\"params\":{ \"process\":\"" +
								         parentProcess + "\",\"itemId\":" +
								         parentProcessId + "} }]";

								var e4 = 9125;
								var l4 = new Links(instance, authenticatedSession);
								l4.SetUrl(uuid4, p4, e4);
								notification = notification.Replace("[" + r["list_item"] + "]",
									"https://" +
									(authenticatedSession.GetMember("wwwpath") + "/?view=" + uuid4).Replace("///", "/")
									.Replace("//", "/"));
							}

							break;
					}
				}
			}

			return notification;
		}

		public string GetExpForPortfolio(string process, int portfolioId) {
			var conds = new Conditions(instance, process, authenticatedSession);
			var exp = "";
			SetDBParam(SqlDbType.Int, "@portfolioId", portfolioId);
			foreach (DataRow r in db
				         .GetDatatable(
					         "SELECT cp.condition_id FROM condition_portfolios cp INNER JOIN condition_states cs ON cp.condition_id = cs.condition_id WHERE process_portfolio_id = @portfolioId AND state = 'portfolio.read'",
					         DBParameters).Rows) {
				var c = conds.GetCondition(Conv.ToInt(r["condition_id"]), supressNotFoundError: true);
				if (c == null) continue;

				if (c.ItemIds.Count > 0 &&
				    !c.ItemIds.Intersect((List<int>)authenticatedSession.GetMember("groups")).Any()) continue;
				exp += (exp.Length > 0 ? " OR " : "") + GetItemlessConditionExpression(process, c);
			}

			return exp;
		}

		public List<string> GetFeatures(string process, int? groupId = null) {
			var result = new List<string>();
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@process", process);

			if (groupId == null) {
				if (Cache.Get(instance, "f_" + process) != null) {
					return (List<string>)Cache.Get(instance, "f_" + process);
				}

				foreach (DataRow row in db
					         .GetDatatable(
						         "SELECT DISTINCT feature FROM conditions c INNER JOIN condition_features cf ON c.condition_id = cf.condition_id WHERE instance = @instance AND process = @process",
						         DBParameters).Rows) {
					result.Add((string)row["feature"]);
				}

				Cache.Set(instance, "f_" + process, result);
			} else {
				if (Cache.Get(instance, "f_" + process + "_" + groupId) != null) {
					return (List<string>)Cache.Get(instance, "f_" + process + "_" + groupId);
				}

				SetDBParam(SqlDbType.Int, "@groupId", groupId);
				foreach (DataRow row in db
					         .GetDatatable(
						         "SELECT DISTINCT cf.feature, process_group_id FROM conditions c INNER JOIN condition_features cf ON c.condition_id = cf.condition_id" +
						         " LEFT JOIN process_group_features pgf ON cf.feature = pgf.feature AND process_group_id = @groupId" +
						         " WHERE instance = @instance AND process = @process ",
						         DBParameters).Rows) {
					if (!DBNull.Value.Equals(row["process_group_id"])) {
						result.Add((string)row["feature"]);
					} else if (((string)row["feature"]).Equals("meta")) {
						if (!result.Contains("meta")) {
							result.Add((string)row["feature"]);
						}
					}
				}

				Cache.Set(instance, "f_" + process + "_" + groupId, result);
			}

			return result;
		}

		private DataTable _stateCache;

		private DataRow[] GetConditionStateList(string process, string feature) {
			if (_stateCache == null) {
				var db = new Connections(authenticatedSession);
				SetDBParam(SqlDbType.NVarChar, "@process", process);

				var sql =
					"SELECT DISTINCT state, feature FROM conditions c " +
					"INNER JOIN condition_states cs ON c.condition_id = cs.condition_id " +
					"INNER JOIN condition_features cf ON c.condition_id = cf.condition_id " +
					"WHERE instance = @instance AND process = @process ";

				_stateCache = db.GetDatatable(sql, DBParameters);
			}

			var result = _stateCache.Select("feature = '" + Conv.Escape(feature) + "'")
				.Where(f => Conv.ToStr(f["state"]).Contains(feature + ".")).ToArray();

			return result;
		}

		public List<string> GetFeatureStates(string process, string feature) {
			if (Cache.Get(instance, "f_" + process + "_" + feature) != null) {
				return (List<string>)Cache.Get(instance, "f_" + process + "_" + feature);
			}

			var result = new List<string>();

			foreach (DataRow row in GetConditionStateList(process, feature)) {
				result.Add((string)row["state"]);
			}

			Cache.Set(instance, "f_" + process + "_" + feature, result);
			return result;
		}

		private ItemDataProcessSelections GetItemDataProcessSelections(string process) {
			if (!_itemDataProcessSelectionses.ContainsKey(process)) {
				_itemDataProcessSelectionses.Add(process,
					new ItemDataProcessSelections(instance, process, authenticatedSession));
			}

			return _itemDataProcessSelectionses[process];
		}

		public class StateConditionIds {
			public StateConditionIds() {
				s = new List<int>();
				groupStates = new Dictionary<int, GroupStateConditionIds>();
			}

			public List<int> s { get; set; }
			public Dictionary<int, GroupStateConditionIds> groupStates { get; set; }
		}

		public class GroupStateConditionIds {
			public GroupStateConditionIds() {
				s = new List<int>();
				tabStates = new Dictionary<int, TabStateConditionIds>();
			}

			public List<int> s { get; set; }
			public Dictionary<int, TabStateConditionIds> tabStates { get; set; }
		}

		public class TabStateConditionIds {
			public TabStateConditionIds() {
				s = new List<int>();
				containerStates = new Dictionary<int, ContainerStateConditionIds>();
			}

			public List<int> s { get; set; }
			public Dictionary<int, ContainerStateConditionIds> containerStates { get; set; }
		}

		public class ContainerStateConditionIds {
			public ContainerStateConditionIds() {
				s = new List<int>();
			}

			public List<int> s { get; set; }
		}


		#region NCalc Replacement

		public LogicalSolver GetItemsConditionExpression(string process, int itemId, Condition c, DataTable data = null,
			DataTable selections = null, Dictionary<string, object> row = null, bool skipCache = false) {
			if (itemId == 0)
				throw new Exception(
					"Item Id cannot be zero. This is an error or consider using GetItemlessConditionExpression. Process: " +
					process + ", Condition Id: " +
					c.ConditionId);
			var CachingAllowed = true;

			if (c.ItemIds.Count > 0 &&
			    !c.ItemIds.Intersect((List<int>)authenticatedSession.GetMember("groups")).Any()) {
				return new LogicalSolver().AddExpression(new BooleanEquation(false));
			}

			if (c.ConditionJSONServer == null) return new LogicalSolver().AddExpression(new BooleanEquation(true));

			var hasOtherConds = false;
			foreach (var c1 in c.ConditionJSONServer) {
				foreach (var o in c1.OperationGroup.Operations) {
					if (o.IsOfType(ConditionType.EXISTING_CONDITION) ||
					    o.IsOfType(ConditionType.PARENT_USER_CONDITION) ||
					    o.IsOfType(ConditionType.SIGNED_IN_USER_CONDITION) ||
					    o.IsOfType(ConditionType.DATE_DIFF_CONDITION) ||
					    o.IsOfType(ConditionType.PARENT_PROCESS_EXISTING_CONDITION)) {
						hasOtherConds = true;
						break;
					}
				}
			}

			if (hasOtherConds == false && skipCache == false) {
				if (Cache.Get(instance, Conv.ToStr(itemId), "c_" + c.ConditionId + "_" + itemId) != null &&
				    row == null) {
					var exp2 = (bool)Cache.Get(instance, Conv.ToStr(itemId), "c_" + c.ConditionId + "_" + itemId);
					return new LogicalSolver().AddExpression(new BooleanEquation(exp2));
				}
			}

			Dictionary<string, object> d1 = null;
			var p = getDatabaseItemService(process);
			//Process or List subquery condition won't work with the below cache
			if (row != null) {
				//Row provided so just use it
				d1 = row;
			} else if (data != null) {
				//Data provided search for the row from the collection
				d1 = p.GetItem(itemId, data, false, onlyConditionColumns: true);
				row = d1;
			} else {
				//Nothing provided get from the DB
				d1 = p.GetItem(itemId, checkRights: false, onlyConditionColumns: true);
				row = d1;
			}

			var itemCols = new ItemColumns(instance, process, authenticatedSession);
			var cType = new ComparisonTypes();
			var operators = new Operators();
			var newExp = new LogicalSolver().UseOrOperatorAsDefault();

			foreach (var c1 in c.ConditionJSONServer) {
				var currLogicalSolver = new LogicalSolver();
				if (newExp.ChildCount() > 0) {
					var op = operators.GetOperator((int)c1.OperatorId);
					if (op.OperatorName == "and") newExp.NextIsAnd();
				}

				foreach (var o in c1.OperationGroup.Operations) {
					if (currLogicalSolver.ChildCount() > 0) {
						if (o.OperatorId != null &&
						    operators.GetOperator((int)o.OperatorId).OperatorName == "or") {
							currLogicalSolver.NextIsOr();
						} else {
							currLogicalSolver.NextIsAnd();
						}
					}

					dynamic cond = o.Condition;

					object condValue;
					if (cond != null && cond.Value != null && cond.Value.GetType() == new JArray().GetType()) {
						condValue = Conv.ToIntArray(cond.Value);
					} else {
						condValue = cond == null || cond.Value == null ? null : cond.Value.Value;
					}


					var numeric = true;

					//Regular Condition
					if (o.IsOfType(ConditionType.CONDITION)) {
						ItemColumn ic1 = itemCols.GetItemColumn(Conv.ToInt(cond.ItemColumnId.Value));
						var compType =
							(ComparisonType)cType.GetComparisonType(Conv.ToInt(cond.ComparisonTypeId.Value));

						if (ic1.IsList()) {
							var inSelection = IsIdInSelection(process, itemId, selections, ic1, condValue);
							var exp = new LogicalSolver().AddExpression(new BooleanEquation(inSelection));
							if (compType.Type == ComparisonTypes.Types.NOT_EQUALS) exp.Negate();
							currLogicalSolver.AddExpression(exp);
							continue;
						}

						if (ic1.IsSubquery() && ic1.IsOfSubType(ItemColumn.ColumnType.List)) {
							var sqVal = Array.Empty<int>();
							if (d1.ContainsKey(ic1.Name + "_str")) {
								var v = Conv.ToStr(d1[ic1.Name + "_str"]);
								if (v.Length > 0) sqVal = Array.ConvertAll(v.Split(','), s => int.Parse(s));
							} else {
								sqVal = Conv.ToIntArray(d1[ic1.Name]);
							}

							//Compare condition value to list's selected value(s) -- also handle if no value is selected
							var inSelection = resolveInSelection(condToIntArray(condValue), sqVal);
							var exp = new LogicalSolver().AddExpression(new BooleanEquation(inSelection));

							if (compType.Type == ComparisonTypes.Types.NOT_EQUALS) exp.Negate();
							currLogicalSolver.AddExpression(exp);
							continue;
						}

						if (ic1.IsProcess()) {
							var inSelection = IsIdInSelection(process, itemId, selections, ic1, condValue);
							var exp = new LogicalSolver().AddExpression(new BooleanEquation(inSelection));
							if (compType.Type == ComparisonTypes.Types.NOT_EQUALS) exp.Negate();
							currLogicalSolver.AddExpression(exp);
							continue;
						}

						//If field is a subquery, caching cannot be allowed as the value has to be re-evaluated each time
						if (ic1.IsSubquery()) CachingAllowed = false;

						// Everything else are considered to be strings
						currLogicalSolver.AddExpression(ic1.CreateComparison(d1, condValue, compType));
						continue;
					}

					//Existing Condition
					if (o.IsOfType(ConditionType.EXISTING_CONDITION)) {
						var expectedResult = Conv.ToBool(cond.Value.Value);
						var conds = new Conditions(instance, process, authenticatedSession);
						var c2 = conds.GetCondition((int)cond.ConditionId.Value);
						if (c2.ItemIds.Count > 0 &&
						    !c2.ItemIds.Intersect((List<int>)authenticatedSession.GetMember("groups")).Any()
						    || c.ConditionId == c2.ConditionId) {
							currLogicalSolver.AddExpression(new BooleanEquation(!expectedResult));
							continue;
						}

						if (c.ConditionId != c2.ConditionId) {
							var subCond = GetItemsConditionExpression(process, itemId, c2, data, selections, row);
							if (expectedResult == false) subCond.Negate();
							currLogicalSolver.AddExpression(subCond);
						}

						continue;
					}

					//Condition comparing to signed in user
					if (o.IsOneOfType(ConditionType.SIGNED_IN_USER_CONDITION,
						    ConditionType.PARENT_USER_CONDITION)) {
						dynamic userCondition = o.Condition;

						ItemColumn ic1 = null;
						if (o.IsOfType(ConditionType.SIGNED_IN_USER_CONDITION) &&
						    (userCondition.UserColumnTypeId == 1 || userCondition.UserColumnTypeId == 2)) {
							ic1 = itemCols.GetItemColumn((int)userCondition.ItemColumnId.Value);
						}

						if (o.IsOfType(ConditionType.PARENT_USER_CONDITION) &&
						    (userCondition.UserColumnTypeId == 1 || userCondition.UserColumnTypeId == 2)) {
							var p2 = getDatabaseItemService((string)userCondition.Process.Value);
							var ib = new ItemBase(instance, (string)userCondition.Process.Value,
								authenticatedSession);
							itemCols = new ItemColumns(instance, (string)userCondition.Process.Value,
								authenticatedSession);
							ic1 = itemCols.GetItemColumn((int)userCondition.ItemColumnId.Value);
							var parentItemId = ib.GetParentItemId(itemId, (string)userCondition.Process.Value);
							if (parentItemId > 0) {
								d1 = p2.GetItem(parentItemId, checkRights: false, onlyConditionColumns: true);
							}
						}

						if (userCondition.UserColumnTypeId == 1) {
							var itemCols2 = new ItemColumns(instance, "user", authenticatedSession);
							var ic2 = itemCols2.GetItemColumn((int)userCondition.UserColumnId.Value);
							var p2 = new ItemBase(instance, "user", authenticatedSession);
							var d2 = p2.GetItem(authenticatedSession.item_id,
								checkRights: false, onlyConditionColumns: true);
							if (ic1.IsProcess() || ic1.IsList() || ic1.IsSubquery() &&
							    (ic1.IsOfSubType(ItemColumn.ColumnType.Process) ||
							     ic1.IsOfSubType(ItemColumn.ColumnType.List))) {
								object newLeft = 1;
								object newRight = 0;

								newLeft = 1;
								newRight = 0;
								var inSelection = false;

								foreach (var x in (List<int>)d2[ic2.Name]) {
									inSelection = IsIdInSelection(process, itemId, selections, ic1, x);
									if (inSelection) {
										newRight = 1;
										break;
									}
								}

								var expr = new ComparisonEquation(newLeft, newRight,
									cType.GetComparisonType((int)userCondition.ComparisonTypeId.Value).Type,
									numeric);
								currLogicalSolver.AddExpression(expr);
								continue;
							}

							var expression = ic1.CreateComparison(d1, d2[ic2.Name],
								cType.GetComparisonType((int)userCondition.ComparisonTypeId.Value));

							currLogicalSolver.AddExpression(expression);
							continue;
						}

						if (userCondition.UserColumnTypeId == 2) {
							var val = new object();
							if (ic1.IsNvarChar() || ic1.IsDate() || ic1.IsText()) {
								val = "'" + d1[ic1.Name] + "'";
							} else if (ic1.IsInt()) {
								val = Conv.ToInt(d1[ic1.Name]);
							} else if (ic1.IsDate()) {
								val = Conv.ToDouble(d1[ic1.Name]);
							} else if (ic1.IsProcess()) {
								var userSelected = false;
								var idpss = GetItemDataProcessSelections(process);
								foreach (var idps in idpss.GetItemDataProcessSelection(itemId,
									         (int)userCondition.ItemColumnId.Value, selections)) {
									if (idps.SelectedItemId == authenticatedSession.item_id) {
										userSelected = true;
										break;
									}
								}

								currLogicalSolver.AddExpression(new BooleanEquation(userSelected));
							} else if (ic1.IsSubquery() && ic1.IsOfSubType(ItemColumn.ColumnType.Process)) {
								string[] a;
								if (d1.ContainsKey(ic1.Name + "_str")) {
									a = Conv.ToStr(d1[ic1.Name + "_str"]).Split(',');
								} else {
									if (d1[ic1.Name].GetType() == new List<int>().GetType())
										a = ((List<int>)d1[ic1.Name]).Select(x => x.ToString()).ToArray();
									else
										a = ((int[])d1[ic1.Name]).Select(x => x.ToString()).ToArray();
								}

								var sqval = new int[0];
								if (a.Length > 0) sqval = Array.ConvertAll(a, s => Conv.ToInt(s));

								if (sqval.Contains(authenticatedSession.item_id))
									currLogicalSolver.AddExpression(new BooleanEquation(true));
								else
									currLogicalSolver.AddExpression(new BooleanEquation(false));
							} else {
								val = d1[ic1.Name];
							}

							if (!ic1.IsProcess() &&
							    !(ic1.IsSubquery() && ic1.IsOfSubType(ItemColumn.ColumnType.Process))) {
								var comparator =
									cType.GetComparisonType((int)userCondition.ComparisonTypeId.Value);
								currLogicalSolver.AddExpression(new ComparisonEquation(val,
									authenticatedSession.item_id, comparator.Type));
							}
						} else if (userCondition.UserColumnTypeId == 3) {
							var condition = process == "user" && itemId == authenticatedSession.item_id;
							currLogicalSolver.AddExpression(new BooleanEquation(condition));
						}

						continue;
					}

					//Condition to compare against parent process
					if (o.IsOfType(ConditionType.PARENT_PROCESS_CONDITION)) {
						cond = o.Condition;
						if (cond.Process != null) {
							var itemCols2 = new ItemColumns(instance, (string)cond.Process.Value,
								authenticatedSession);
							var ic1 = itemCols2.GetItemColumn((int)cond.ItemColumnId.Value);

							var p2 = new ItemBase(instance, (string)cond.Process.Value, authenticatedSession);
							var parentItemId = p2.GetParentItemId(itemId, (string)cond.Process.Value);
							if (parentItemId > 0) {
								d1 = p2.GetItem(parentItemId, checkRights: false, onlyConditionColumns: true);
								var exp = ic1.CreateComparison(d1, cond.Value.Value,
									cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value]);
								currLogicalSolver.AddExpression(exp);
							} else {
								currLogicalSolver.GoWithFalse();
							}
						} else if (cond.ParentItemColumnId != null) {
							ItemColumn ic1 = itemCols.GetItemColumn(Conv.ToInt(cond.ItemColumnId.Value));
							var icParent = itemCols.GetItemColumn((int)cond.ParentItemColumnId.Value);

							var parentItemId = Conv.ToInt(d1[icParent.Name]);
							var pp = new Processes(instance, authenticatedSession);
							if (parentItemId > 0 &&
							    (process.Equals("task") && icParent.Name.Equals("parent_item_id") &&
							     Conv.ToInt(d1["owner_item_id"]) != Conv.ToInt(d1["parent_item_id"]) ||
							     pp.GetProcessByItemId(parentItemId) == process)) {
								Dictionary<string, object> d2 = null;
								if (data != null) {
									d2 = p.GetItem(parentItemId, data, false, onlyConditionColumns: true);
								} else {
									d2 = p.GetItem(parentItemId, checkRights: false, onlyConditionColumns: true);
								}

								var exp = ic1.CreateComparison(d2, cond.Value.Value,
									cType.GetComparisonTypes()[(int)cond.ComparisonTypeId.Value]);
								currLogicalSolver.AddExpression(exp);
							} else {
								var expression = new ComparisonEquation("", cond.Value.Value,
									cType.GetComparisonType((int)cond.ComparisonTypeId.Value).Type);
								currLogicalSolver.AddExpression(expression);
							}
						}

						continue;
					}


					if (o.ConditionTypeId == 6) throw new Exception("Not implemented");

					if (o.IsOfType(ConditionType.PARENT_PROCESS_EXISTING_CONDITION)) {
						cond = o.Condition;
						string parentProcess = Conv.ToStr(cond.ParentProcess);
						int linkItemColumnId = Conv.ToInt(cond.LinkColumnId);
						int parentConditionId = Conv.ToInt(cond.ConditionId);
						bool expectation = Conv.ToBool(cond.Value);
						var ownerItemId = 0;
						CachingAllowed = false;

						//Get Existing Condition Database Row
						var existingCondition =
							new Conditions(instance, parentProcess, authenticatedSession).GetCondition(
								parentConditionId);

						//Get Owner
						var parentColumn = itemCols.GetItemColumn(linkItemColumnId);
						if (parentColumn.DataType == 5) {
							var idps = new ItemDataProcessSelections(instance, process, authenticatedSession);
							var idpsList = new List<ItemDataProcessSelection>();
							if (Conv.ToInt(parentColumn.RelativeColumnId) > 0) {
								idpsList = idps.GetParentItemDataProcessSelection(itemId,
									Conv.ToInt(parentColumn.RelativeColumnId));
								if (idpsList.Count == 1) ownerItemId = idpsList[0].ItemId;
							} else {
								idpsList = idps.GetItemDataProcessSelection(itemId, parentColumn.ItemColumnId);
								if (idpsList.Count == 1) ownerItemId = idpsList[0].SelectedItemId;
							}

							if (idpsList.Count > 1)
								throw new Exception(
									"Parent Process Condition cannot work as multiple parents might be returned and only one parent can be evaluated. Count: " +
									idpsList.Count + ", relative column id: " +
									Conv.ToInt(parentColumn.RelativeColumnId));
						} else {
							ownerItemId = Conv.ToInt(d1[parentColumn.Name]);
						}

						//Resolve
						if (ownerItemId == 0) {
							//No owner found
							currLogicalSolver.AddExpression(new BooleanEquation(false));
						} else {
							//It parent process and item process match then check parent's condition otherwise return false for this condition row
							if (Cache.GetItemProcess(ownerItemId, instance) == parentProcess) {
								var r = GetItemsConditionExpression(parentProcess, ownerItemId, existingCondition,
									skipCache: true);
								currLogicalSolver.AddExpression(new BooleanEquation(expectation == r.Evaluate()));
							} else {
								currLogicalSolver.AddExpression(new BooleanEquation(false));
							}
						}
					}

					//Condition to compare dates
					if (o.IsOfType(ConditionType.DATE_DIFF_CONDITION)) {
						var ic1 = itemCols.GetItemColumn((int)cond.ItemColumnId.Value);
						var comparator = cType.GetComparisonType((int)cond.ComparisonTypeId.Value);

						if (d1 != null) {
							var dt1 = Conv.ToDateTime(d1[ic1.Name]);
							if (dt1 == null) {
								currLogicalSolver.AddExpression(new BooleanEquation(false));
							} else {
								DateTime? dt2;
								if (cond.ItemColumnId2 != null) {
									var ic2 = itemCols.GetItemColumn((int)cond.ItemColumnId2.Value);
									if (dt1 != null) {
										dt2 = Conv.ToDateTime(d1[ic2.Name]);
									} else {
										dt2 = DateTime.Now.ToUniversalTime();
									}

									if (dt2 != null) {
										var diff = dt2 - dt1;
										var diffComp = new ComparisonEquation(diff.Value.TotalDays,
											Conv.ToDouble(cond.Value.Value), comparator.Type);
										currLogicalSolver.AddExpression(diffComp);
									} else {
										currLogicalSolver.AddExpression(new BooleanEquation(false));
									}
								} else {
									dt2 = DateTime.Now.ToUniversalTime();
									var diff = dt2 - dt1;
									var diffComp = new ComparisonEquation(diff.Value.TotalDays,
										Conv.ToDouble(cond.Value.Value), comparator.Type);
									currLogicalSolver.AddExpression(diffComp);
								}
							}
						} else {
							currLogicalSolver.AddExpression(new BooleanEquation(false));
						}
					}
				}

				newExp.AddExpression(currLogicalSolver);
			}

			if (CachingAllowed)
				Cache.Set(instance, Conv.ToStr(itemId), "c_" + c.ConditionId + "_" + itemId, newExp.Evaluate());

			return newExp;
		}

		private bool resolveInSelection(int[] conditionValues, int[] selectionValues) {
			var inSelection = selectionValues.Intersect(conditionValues).Any();
			if (conditionValues.Contains(0) && inSelection == false) inSelection = selectionValues.Length == 0;
			return inSelection;
		}

		private int[] condToIntArray(dynamic condValue) {
			var conditionValues = new List<int>();
			if (condValue.GetType() != typeof(int[]))
				conditionValues.Add(Conv.ToInt(condValue));
			else
				conditionValues = ((int[])condValue).ToList();

			return conditionValues.ToArray();
		}

		private bool IsIdInSelection(string process, int itemId, DataTable selections, ItemColumn ic1,
			dynamic condValue) {
			if (condValue == null) return false;
			var inSelection = false;
			if (selections != null) {
				inSelection = resolveInSelection(condToIntArray(condValue),
					selections.Select("item_id = " + itemId + " AND item_column_id =  " + ic1.ItemColumnId)
						.Select(id => Conv.ToInt(id["selected_item_id"])).ToArray());
			} else {
				var idps = GetItemDataProcessSelections(process);
				foreach (var id in condToIntArray(condValue)) {
					inSelection = idps.IdInSelections(itemId, ic1.ItemColumnId, id);
					if (inSelection == true)
						return true;
				}
			}

			return inSelection;
		}

		#endregion
	}
}