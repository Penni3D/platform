﻿using System;
using System.Collections.Generic;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using NCalc;

namespace Keto5.x.platform.server.processes.accessControl {
	public abstract class IEvaluator {
		public abstract string Get();
		public abstract bool Evaluate();

		public override string ToString() {
			return Get();
		}
	}

	public class LogicalSolver : IEvaluator {
		public enum operators {
			unselected,
			or,
			and
		}

		private readonly List<IEvaluator> _children;
		private readonly List<operators> _operators = new List<operators>();

		private operators _lockedOperator = operators.unselected;

		private bool _negate;

		private operators _nextOperator = operators.unselected;

		public LogicalSolver() {
			_children = new List<IEvaluator>();
		}

		public LogicalSolver(string exp) {
			_children = new List<IEvaluator>();
			_children.Add(new StringExpression(exp));
		}
		
		public int ChildCount() {
			return _children.Count;
		}

		public LogicalSolver UseOrOperatorAsDefault() {
			_lockedOperator = operators.or;
			return this;
		}

		public LogicalSolver UseAndOperatorAsDefault() {
			_lockedOperator = operators.and;
			return this;
		}

		public LogicalSolver NextIsOr() {
			_nextOperator = operators.or;
			return this;
		}

		public LogicalSolver NextIsAnd() {
			_nextOperator = operators.and;
			return this;
		}

		public LogicalSolver NextIs(Operator op) {
			if (op.OperatorId == 1) {
				NextIsAnd();
			} else if (op.OperatorId == 2) {
				NextIsOr();
			}

			return this;
		}

		private LogicalSolver AddExpressionToChildList(IEvaluator expression) {
			if (_children.Count > 0 && _lockedOperator == operators.unselected &&
			    _nextOperator == operators.unselected) {
				throw new InvalidOperationException("Operator is not selected. Adding " + expression.Get() + " to " +
				                                    Get()); // call UseOrOperator or UseAndOperator or NextIsOr or NextIsAnd
			}

			_children.Add(expression);
			if (_children.Count > 1) {
				if (_nextOperator != operators.unselected) {
					_operators.Add(_nextOperator);
					_nextOperator = operators.unselected;
				} else {
					_operators.Add(_lockedOperator);
				}
			}

			return this;
		}

		public LogicalSolver Negate() {
			_negate = true;
			return this;
		}

		public LogicalSolver AddExpression(IEvaluator exp) {
			return AddExpressionToChildList(exp);
		}

		public LogicalSolver AddExpression(BooleanEquation exp) {
			return AddExpressionToChildList(exp);
		}


		/*public LogicalSolver AddExpression(StringExpression exp){
			return AddExpressionToChildList(exp);
		}*/

		public LogicalSolver AddExpression(DateComparison exp) {
			return AddExpressionToChildList(exp);
		}

		public LogicalSolver AddExpression(ComparisonEquation exp) {
			return AddExpressionToChildList(exp);
		}


		public LogicalSolver GoWithTrue() {
			AddExpression(new BooleanEquation(false));
			return this;
		}

		public LogicalSolver GoWithFalse() {
			AddExpression(new BooleanEquation(false));
			return this;
		}


		public LogicalSolver Or(string exp) {
			if (_lockedOperator != operators.unselected || _nextOperator != operators.unselected) {
				throw new InvalidOperationException("Operator is already selected");
			}

			_children.Add(new StringExpression(exp));
			_operators.Add(operators.or);
			return this;
		}

		public LogicalSolver Or(LogicalSolver solver) {
			if (_lockedOperator != operators.unselected || _nextOperator != operators.unselected) {
				throw new InvalidOperationException("Operator is already locked");
			}

			_children.Add(solver);
			_operators.Add(operators.or);
			return this;
		}

		public LogicalSolver And(string exp) {
			if (_lockedOperator != operators.unselected || _nextOperator != operators.unselected) {
				throw new InvalidOperationException("Operator is already locked");
			}

			_children.Add(new StringExpression(exp));
			_operators.Add(operators.and);
			return this;
		}

		public LogicalSolver And(LogicalSolver solver) {
			if (_lockedOperator != operators.unselected || _nextOperator != operators.unselected) {
				throw new InvalidOperationException("Operator is already locked");
			}

			_children.Add(solver);
			_operators.Add(operators.and);
			return this;
		}


		// todo: these get methods are pretty useless after ncalc is removed, so they might be used to generate sql-conditions (maybe)
		public string GetMain() {
			if (_children.Count == 0) {
				return " Empty ";
			}

			var strBuilder = _children.First().Get();
			if (_children.Count > 1) {
				for (var i = 1; i < _children.Count; i++) {
					strBuilder += " " + _operators[i - 1] + " " + _children[i].Get();
				}
			}

			if (_negate) {
				return "not ( " + strBuilder + " )";
			}

			return strBuilder;
		}

		public override string Get() {
			if (_children.Count == 0) {
				return " Empty ";
			}

			var strBuilder = _children.First().Get();
			if (_children.Count > 1) {
				for (var i = 1; i < _children.Count; i++) {
					strBuilder += " " + _operators[i - 1] + " " + _children[i].Get();
				}
			}

			if (_negate) {
				return "not ( " + strBuilder + " )";
			}

			return "( " + strBuilder + " )";
		}

		public override bool Evaluate() {
			var currentTruth = _children[0].Evaluate();
			for (var i = 0; i < _operators.Count; i++) {
				if (_operators[i].Equals(operators.or)) {
					// if one or-block returned true then the whole expression is true (when XOR is not included)
					if (currentTruth && _negate) return false;
					if (currentTruth) return true;
					currentTruth = _children[i + 1].Evaluate();
				} else if (_operators[i].Equals(operators.and)) {
					// if one and-block returned false then the whole expression is false (when XOR is not included)
					if (_negate == false && currentTruth == false) return false;
					if (currentTruth) currentTruth = _children[i + 1].Evaluate();
				} else {
					if (currentTruth) currentTruth = _children[i + 1].Evaluate();
				}
			}
			
			if (_negate) return !currentTruth;

			return currentTruth;
		}
	}

	public class StringExpression : IEvaluator {
		private readonly string _expr;

		public StringExpression(string exp) {
			_expr = exp;
		}

		public override string Get() {
			return _expr;
		}

		public override bool Evaluate() {
			var exp = new Expression(_expr);
			try {
				return (bool)exp.Evaluate();
			} catch (Exception e) {
				Diag.SupressException(e);
				Console.WriteLine("Error evaluating " + _expr);
				throw;
			}
		}
	}


	// Equation kind of things

	public class BooleanEquation : IEvaluator {
		private readonly bool _val;

		public BooleanEquation(object obj) {
			_val = Conv.ToBool(obj);
		}

		public override bool Evaluate() {
			return _val;
		}

		public override string Get() {
			if (_val) {
				return "true";
			}

			return "false";
		}
	}

	public class DateComparison : IEvaluator {
		private readonly ComparisonTypes.Types _comparator;
		private readonly DateTime _leftObj;
		private readonly DateTime _rightObj;

		public DateComparison(object left, object right, ComparisonTypes.Types comparator) {
			_leftObj = ((DateTime)Conv.ToDateTime(left)).Date;
			_rightObj = ((DateTime)Conv.ToDateTime(right)).Date;
			;
			_comparator = comparator;
		}

		public override bool Evaluate() {
			var comparisonVal = _leftObj.CompareTo(_rightObj);
			bool? result = null;
			if (_comparator.Equals(ComparisonTypes.Types.EQUALS)) {
				result = comparisonVal == 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.GREATER_OR_EQUAL)) {
				result = comparisonVal >= 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.GREATER_THAN)) {
				result = comparisonVal > 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.LESS_THAN_OR_EQUAL)) {
				result = comparisonVal <= 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.LESS_THAN)) {
				result = comparisonVal < 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.NOT_EQUALS)) {
				result = comparisonVal != 0;
			}

			if (result == null) {
				throw new NotImplementedException("Comparison type is not handled" + _comparator + ", " + _leftObj +
				                                  ", " + _rightObj);
			}

			return (bool)result;
		}

		public override string Get() {
			return Evaluate() ? "true" : "false";
		}
	}

	public class ComparisonEquation : IEvaluator {
		private readonly ComparisonTypes.Types _comparator;

		private readonly bool _dateComparisonAssumed;
		private readonly object _leftObj;
		private readonly bool _numeric;
		private readonly object _rightObj;

		public ComparisonEquation(object left, object right, ComparisonTypes.Types comparator, bool numeric = true) {
			if (left is DateTime || right is DateTime) {
				_dateComparisonAssumed = true;
			}

			_leftObj = left;
			_rightObj = right;
			_comparator = comparator;
			_numeric = numeric;
		}

		public override bool Evaluate() {
			if (_dateComparisonAssumed) {
				return new DateComparison(_leftObj, _rightObj, _comparator).Evaluate();
			}

			if (_comparator.Equals(ComparisonTypes.Types.CONTAINS)) {
				return Conv.ToStr(_leftObj).ToLower().Contains(Conv.ToStr(_rightObj).ToLower());
			}

			if (_comparator.Equals(ComparisonTypes.Types.STARTS_WITH)) {
				return Conv.ToStr(_leftObj).ToLower().StartsWith(Conv.ToStr(_rightObj).ToLower());
			}

			if (_comparator.Equals(ComparisonTypes.Types.EQUALS)) {
				return _leftObj.Equals(_rightObj);
			}

			if (_comparator.Equals(ComparisonTypes.Types.NOT_EQUALS)) {
				return !_leftObj.Equals(_rightObj);
			}

			if (_comparator.Equals(ComparisonTypes.Types.GREATER_OR_EQUAL)) {
				if (_numeric) {
					return Conv.ToDouble(_leftObj) >= Conv.ToDouble(_rightObj);
				}

				return string.Compare(Conv.ToStr(_leftObj), Conv.ToStr(_rightObj), StringComparison.Ordinal) >= 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.GREATER_THAN)) {
				if (_numeric) {
					return Conv.ToDouble(_leftObj) > Conv.ToDouble(_rightObj);
				}

				return string.Compare(Conv.ToStr(_leftObj), Conv.ToStr(_rightObj), StringComparison.Ordinal) > 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.LESS_THAN_OR_EQUAL)) {
				if (_numeric) {
					return Conv.ToDouble(_leftObj) <= Conv.ToDouble(_rightObj);
				}

				return string.Compare(Conv.ToStr(_leftObj), Conv.ToStr(_rightObj), StringComparison.Ordinal) <= 0;
			}

			if (_comparator.Equals(ComparisonTypes.Types.LESS_THAN)) {
				if (_numeric) {
					return Conv.ToDouble(_leftObj) < Conv.ToDouble(_rightObj);
				}

				return string.Compare(Conv.ToStr(_leftObj), Conv.ToStr(_rightObj), StringComparison.Ordinal) < 0;
			}

			throw new NotImplementedException("Comparison type is not handled" + _comparator + ", " + _leftObj + ", " +
			                                  _rightObj);
		}

		public override string Get() {
			if (ComparisonTypes.Types.STARTS_WITH.Equals(_comparator)) {
				return "StartsWith('" + Conv.ToStr(_leftObj).ToLower() + "', '" + Conv.ToStr(_rightObj).ToLower() +
				       "')";
			}

			if (ComparisonTypes.Types.CONTAINS.Equals(_comparator)) {
				return "Contains('" + Conv.ToStr(_leftObj).ToLower() + "', '" + Conv.ToStr(_rightObj).ToLower() + "')";
			}


			var compStr = "";
			switch (_comparator) {
				case ComparisonTypes.Types.EQUALS:
					compStr = "==";
					break;
				case ComparisonTypes.Types.GREATER_OR_EQUAL:
					compStr = ">=";
					break;
				case ComparisonTypes.Types.GREATER_THAN:
					compStr = ">";
					break;
				case ComparisonTypes.Types.LESS_THAN:
					compStr = "<";
					break;
				case ComparisonTypes.Types.LESS_THAN_OR_EQUAL:
					compStr = "<=";
					break;
				case ComparisonTypes.Types.NOT_EQUALS:
					compStr = "!=";
					break;
			}

			var l = _leftObj;
			var r = _rightObj;
			if (!_numeric) {
				l = "'" + l + "'";
				r = "'" + r + "'";
			} else {
				l = Conv.ToStr(l);
				r = Conv.ToStr(r);
			}

			return l + " " + compStr + " " + r;
		}
	}
}