﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.processes.layout;
using Keto5.x.platform.server.processes.specialProcesses;
using Keto5.x.platform.server.security;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.accessControl {
	[Route("v1/navigation/States")]
	public class StatesRest : PageBase {
		public StatesRest() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}/{feature}/{itemId}/{states}")]
		public List<State> Get(string process, string feature, int itemId, string states) {
			var statesArr = states.Split(',');
			var mStates = new States(instance, currentSession);
			return mStates.CheckStates(process, feature, itemId, statesArr);
		}

		[HttpGet("{process}/{feature}/{itemId}")]
		public List<State> Get(string process, string feature, int itemId) {
			string[] states = {"test", "test2"};
			var mStates = new States(instance, currentSession);
			return mStates.CheckStates(process, feature, itemId, states);
		}

		[HttpGet("{process}/{itemId}")]
		public Dictionary<string, FeatureStates> Get(string process, int itemId) {
			var mStates = new States(instance, currentSession);
			return mStates.CleanStatesObject(mStates.GetFeatureStates(process, itemId, null));
		}

		[HttpGet("forGroup/{process}/{itemId}/{groupId}")]
		public Dictionary<string, FeatureStates> Get(string process, int itemId, int groupId) {
			var mStates = new States(instance, currentSession);
			return mStates.CleanStatesObject(mStates.GetFeatureStates(process, itemId, groupId));
		}

		[HttpGet("GetStatesForStates_/{process}")]
		public Dictionary<string, States.StateConditionIds> GetStatesForStates_(string process) {
			var mStates = new States(instance, currentSession);
			var x = new List<string> {"meta.write", "meta.read", "meta.history"};
			return mStates.GetConditionIdsForStates(process, "meta", x);
		}

		[HttpGet("navigationIds/{process}/{itemId}")]
		public Dictionary<string, object> GetNavigationIDs(string process, int itemId) {
			var retval = new Dictionary<string, object>();

			var p = new ItemBase(instance, process, currentSession);
			var item = p.GetItem(itemId);

			var mStates = new States(instance, currentSession);

			if (item.ContainsKey("current_state") && DBNull.Value.Equals(item["current"])) {
				var currentState = Conv.ToStr(item["current_state"]);
				if (currentState.StartsWith("tab_")) {
					var tabId = Conv.ToInt(currentState.Substring(4));
					retval.Add("tabId", tabId);
					retval.Add("feature", null);
					var pgts = new ProcessGroupTabs(instance, process, currentSession);
					var gids = pgts.GetTabGroups(tabId);
					gids.Reverse();

					var readGroup = 0;
					foreach (var gid in gids) {
						var gstates = mStates.GetEvaluatedStatesForFeature(process, "meta", itemId, null, null,
							gid.ProcessGroup.ProcessGroupId);

						if (gstates.HasTabState(tabId, "write")) {
							retval.Add("groupId", gid.ProcessGroup.ProcessGroupId);
							return retval;
						}

						if (gstates.HasTabState(tabId, "read")) {
							readGroup = gid.ProcessGroup.ProcessGroupId;
						}
					}

					retval.Add("groupId", readGroup);
					return retval;
				}

				retval.Add("tabId", null);
				retval.Add("feature", currentState);
				retval.Add("groupId", null);
				return retval;
			}

			{
				var gstates = mStates.GetEvaluatedStatesForFeature(process, "meta", itemId, null, null);

				var pgs = new ProcessGroups(instance, process, currentSession);
				var gids = pgs.GetGroupIds();
				gids.Reverse();

				foreach (var gid in gids) {
					foreach (var tid in gstates.groupStates[gid].tabStates.Keys) {
						if (gstates.groupStates[gid].tabStates[tid].s.Contains("meta.write")) {
							retval.Add("tabId", tid);
							retval.Add("feature", null);
							retval.Add("groupId", gid);
							return retval;
						}
					}

					foreach (var tid in gstates.groupStates[gid].tabStates.Keys) {
						if (gstates.groupStates[gid].tabStates[tid].s.Contains("meta.read")) {
							retval.Add("tabId", tid);
							retval.Add("feature", null);
							retval.Add("groupId", gid);
							return retval;
						}
					}
				}

				retval.Add("tabId", null);
				retval.Add("feature", null);
				retval.Add("groupId", null);
				return retval;
			}

			//Model.db.States mStates = new Model.db.States(instance, currentSession);
			//return mStates.GetFeatureStates(process, itemId, groupId);
		}

		[HttpGet("getConditionsResult/{process}/{conditionId}/{itemId}/{userItemId}")]
		public string GetConditionsResulttStatesForStates(string process, int conditionId, int itemId, int userItemId) {
			//Create Joking Session
			var members = new Dictionary<string, object>();
			members.Add("locale_id", currentSession.locale_id);
			var testSession =
				new AuthenticatedSession(instance, userItemId, members) {
					instance = currentSession.instance
				};

			var ug = new UserGroups(testSession.instance, testSession);
			var groups = ug.GetUserGroupsForAuthenticatedUser();
			testSession.SetMember("groups", groups);


			var mStates = new States(instance, testSession);
			var conds = new Conditions(instance, process, testSession);

			var exp = mStates.GetItemsConditionExpression(process, itemId, conds.GetCondition(conditionId));
			return Conv.ToStr(States.EvaluateExpression(exp));
		}

		/* This is for testing expression evalution on the fly */
		[HttpGet("{expression}")]
		public object Get(string expression) {
			return States.EvaluateExpression(expression);
		}

		[HttpGet("usersGroups")]
		public List<int> GetUsersGroups() {
			var ug = new UserGroups(instance, currentSession);
			return ug.GetUserGroupsForAuthenticatedUser();
		}
	}
}