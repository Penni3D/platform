﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.accessControl {
	public class StateRight {
		public StateRight() { }

		public StateRight(DataRow row, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}

			StateRightId = (int) row["state_right_id"];
			Process = (string) row["process"];
			UserGroupItemId = (int) row["user_group_item_id"];
			ItemColumnId = (int) row["item_column_id"];
			Feature = (string) row["feature"];
			State = (string) row["state"];
			ComparisonTypeId = (int) row["comparison_type_id"];
			UserColumnType = (byte) row["user_column_type"];
			if (row["user_column_id"] != DBNull.Value) {
				UserColumnId = (int) row["user_column_id"];
			} else {
				UserColumnId = null;
			}
		}

		public int StateRightId { get; set; }
		public string Process { get; set; }
		public int UserGroupItemId { get; set; }
		public int ItemColumnId { get; set; }
		public string Feature { get; set; }
		public string State { get; set; }
		public int ComparisonTypeId { get; set; }
		public byte UserColumnType { get; set; }
		public int? UserColumnId { get; set; }
	}

	public class StateRights : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;

		public StateRights(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public List<StateRight> GetStateRights() {
			var result = new List<StateRight>();
			var db = new Connections(authenticatedSession);
			foreach (DataRow row in db
				.GetDatatable(
					"SELECT state_right_id, process, user_group_item_id, item_column_id, feature, state, comparison_type_id, user_column_type, user_column_id FROM state_rights WHERE instance = @instance",
					DBParameters).Rows) {
				var stateRight = new StateRight(row);
				result.Add(stateRight);
			}

			return result;
		}

		public List<StateRight> GetStateRights(string processName, string feature, string state) {
			var result = new List<StateRight>();
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.NVarChar, "@process", processName);
			SetDBParam(SqlDbType.NVarChar, "@feature", feature);
			SetDBParam(SqlDbType.NVarChar, "@state", state);
			foreach (DataRow row in db.GetDatatable(
				"SELECT state_right_id, process, user_group_item_id, item_column_id, feature, state, comparison_type_id, user_column_type, user_column_id FROM state_rights WHERE instance = @instance AND process = @process AND feature = @feature AND state = @state",
				DBParameters).Rows) {
				var stateRight = new StateRight(row);
				result.Add(stateRight);
			}

			return result;
		}

		public List<StateRight> GetStateRightsForUser(int userId, string processName, string feature, string state) {
			var result = new List<StateRight>();
			var db = new Connections(authenticatedSession);
			SetDBParam(SqlDbType.Int, "@userId", userId);
			SetDBParam(SqlDbType.NVarChar, "@process", processName);
			SetDBParam(SqlDbType.NVarChar, "@feature", feature);
			SetDBParam(SqlDbType.NVarChar, "@state", state);
			SetDBParam(SqlDbType.Int, "@userColId",
				new ItemColumns(instance, "user_group", authenticatedSession).GetItemColumnByName("user_id")
					.ItemColumnId);


			foreach (DataRow row in db.GetDatatable(
				"SELECT state_right_id, process, user_group_item_id, sr.item_column_id, feature, state, comparison_type_id, user_column_type, user_column_id FROM state_rights sr INNER JOIN item_data_process_selections idps ON sr.user_group_item_id = idps.item_id AND idps.selected_item_id = @userId AND idps.item_column_id = @userColId WHERE instance = @instance AND process = @process AND feature = @feature AND state = @state",
				DBParameters).Rows) {
				var stateRight = new StateRight(row);
				result.Add(stateRight);
			}

			return result;
		}

		public StateRight GetStateRight(int stateRightId) {
			SetDBParam(SqlDbType.Int, "@stateRightId", stateRightId);
			var dt = db.GetDatatable(
				"SELECT state_right_id, process, user_group_item_id, item_column_id, feature, state, comparison_type_id, user_column_type, user_column_id FROM state_rights  WHERE instance = @instance and stateRightId = @stateRightId",
				DBParameters);
			if (dt.Rows.Count > 0) {
				var sr = new StateRight(dt.Rows[0]);
				return sr;
			}

			throw new CustomArgumentException("State Right cannot be found with stateRightId " + stateRightId);
		}

		public StateRight Add(StateRight stateRight) {
			SetDBParam(SqlDbType.NVarChar, "@process", stateRight.Process);
			SetDBParam(SqlDbType.Int, "@userGroupItemId", stateRight.UserGroupItemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", stateRight.ItemColumnId);
			SetDBParam(SqlDbType.NVarChar, "@feature", stateRight.Feature);
			SetDBParam(SqlDbType.NVarChar, "@state", stateRight.State);
			SetDBParam(SqlDbType.TinyInt, "@comparisonTypeId", stateRight.ComparisonTypeId);
			SetDBParam(SqlDbType.Int, "@userColumnType", stateRight.UserColumnType);
			if (stateRight.UserColumnType > 0) {
				SetDBParam(SqlDbType.Int, "@userColumnId", DBNull.Value);
			} else {
				SetDBParam(SqlDbType.Int, "@userColumnId", stateRight.UserColumnId);
			}

			db.ExecuteInsertQuery(
				"INSERT INTO state_rights (instance, process, user_group_item_id, item_column_id, feature, state, comparison_type_id, user_column_type, user_column_id) VALUES(@instance, @process, @userGroupItemId, @itemColumnId, @feature, @state, @comparisonTypeId, @userColumnType, @userColumnId)",
				DBParameters);
			return stateRight;
		}

		public void Delete(int stateRightId) {
			SetDBParam(SqlDbType.NVarChar, "@stateRightId", stateRightId);
			db.ExecuteDeleteQuery("DELETE FROM state_rights WHERE instance=@instance AND state_right_id=@stateRightId",
				DBParameters);
		}

		public void Save(StateRight stateRight) {
			SetDBParam(SqlDbType.Int, "@stateRightId", stateRight.StateRightId);
			SetDBParam(SqlDbType.Int, "@userGroupItemId", stateRight.UserGroupItemId);
			SetDBParam(SqlDbType.Int, "@itemColumnId", stateRight.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@comparisonTypeId", stateRight.ComparisonTypeId);
			SetDBParam(SqlDbType.TinyInt, "@userColumnType", stateRight.UserColumnType);
			if (stateRight.UserColumnType > 0) {
				SetDBParam(SqlDbType.Int, "@userColumnId", DBNull.Value);
			} else {
				SetDBParam(SqlDbType.Int, "@userColumnId", stateRight.UserColumnId);
			}

			db.ExecuteUpdateQuery(
				"UPDATE state_rights SET user_group_item_id=@userGroupItemId, item_column_id, @itemColumnId, comparison_type_id=@comparisonTypeId, user_column_type = @userColumnType, user_column_id = @userColumnId WHERE instance = @instance AND state_right_id = @stateRightId",
				DBParameters);
		}
	}
}