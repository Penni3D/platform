﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {
	[Route("v1/settings/ItemColumnJoins")]
	public class ItemColumnJoinsRest : PageBase {
		public ItemColumnJoinsRest() : base("settings") { }

		[HttpGet("{process}/{itemColumnId}")]
		public List<ItemColumnJoin> Get(string process, int itemColumnId) {
			var joins = new ItemColumnJoins(instance, process, currentSession);
			return joins.GetItemColumnJoins(itemColumnId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]List<ItemColumnJoin> joinColumns) {
			CheckRight("write");
			var jj = new ItemColumnJoins(instance, process, currentSession);

			foreach (var j in joinColumns) {
				jj.Add(j);
			}
			return new ObjectResult(joinColumns);
		}


		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]List<ItemColumnJoin> joinColumns) {
			CheckRight("write");
			var jj = new ItemColumnJoins(instance, process, currentSession);

			foreach (var j in joinColumns) {
				jj.Save(j);
			}
			return new ObjectResult(joinColumns);

		}

		[HttpDelete("{process}/{itemColumnJoinsIds}")]
		public IActionResult Delete(string process, string itemColumnJoinsIds) {
			CheckRight("delete");
			var joinColumns = new ItemColumnJoins(instance, process, currentSession);
			foreach (var id in itemColumnJoinsIds.Split(',')) {
				joinColumns.Delete(Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}
}