﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.actions {
	[Route("v1/settings/ProcessActions")]
	public class ProcessActionsRest : PageBase {
		public ProcessActionsRest() : base("settings") { }

		// GET: model/values
		[HttpGet("")]
		public Dictionary<string, List<ProcessAction>> GetActions() {
			var actions = new ProcessActions(instance, "", currentSession);
			return actions.GetAllProcessActions();
		}

		[HttpGet("{process}")]
		public List<ProcessAction> Get(string process) {
			var actions = new ProcessActions(instance, process, currentSession);
			return actions.GetActions();
		}

		[HttpGet("{process}/unusedactions")]
		public List<ProcessAction> GetUnusedActions(string process) {
			var actions = new ProcessActions(instance, process, currentSession);
			return actions.GetUnusedActions(process);
		}

		[HttpGet("{process}/buttonsforactions")]
		public List<Dictionary<string, object>> GetButtonsForActions(string process) {
			var actions = new ProcessActions(instance, process, currentSession);
			return actions.GetButtonsForActions(process);
		}

		[HttpGet("itemColumns/{itemColumnId}")]
		public List <Dictionary<string, object>> GetActionItemColumnRelations(int itemColumnId) {
			var actions = new ProcessActions(instance, process, currentSession);
			return actions.GetActionItemColumnRelations(itemColumnId);
		}

		[HttpGet("{process}/{processActionId}")]
		public ProcessAction Get(string process, int processActionId) {
			var actions = new ProcessActions(instance, process, currentSession);
			return actions.GetAction(processActionId);
		}

		[HttpPost("{process}")]
		public IActionResult Post(string process, [FromBody]ProcessAction processAction) {
			CheckRight("write");
			var processActions = new ProcessActions(instance, process, currentSession);
			if (ModelState.IsValid) {
				processActions.Add(processAction);
				return new ObjectResult(processAction);
			}
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]List<ProcessAction> processAction) {
			CheckRight("write");
			var processActions = new ProcessActions(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pt in processAction) {
					processActions.Save(pt);
				}
				return new ObjectResult(processAction);
			}
			return null;
		}

		[HttpDelete("{process}/{processActionId}")]
		public IActionResult Delete(string process, string processActionId) {
			CheckRight("delete");
			var processActions = new ProcessActions(instance, process, currentSession);
			foreach (var id in processActionId.Split(',')) {
				processActions.Delete(Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}

		[HttpPost("{process}/{processActionId}/{itemId}")]
		public Dictionary<string, object> DoAction(string process, int processActionId, int itemId) {
			var actions = new ProcessActions(instance, process, currentSession);
			return actions.DoAction(processActionId, itemId);
		}

		[HttpPost("{process}/{processActionId}/{itemId}/{itemColumnId}")]
		public Dictionary<string, object> DoAction(string process, int processActionId, int itemId, int itemColumnId) {
			var actions = new ProcessActions(instance, process, currentSession);
			return actions.DoAction(processActionId, itemId, true, itemColumnId);
		}
	}
}