﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.processes.actions {
	public class ProcessActionDialogRow {
		public ProcessActionDialogRow() {

		}

		public ProcessActionDialogRow(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			Type = (int)row["type"];
			ProcessContainerId = Conv.ToInt(row["process_container_id"]);
			Variable = Conv.ToStr(row[alias + "variable"]);
			ProcessActionDialogId = (int)row["process_action_dialog_id"];
			LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);

		}

		public int ProcessActionDialogId { get; set; }
		public int Type { get; set; }
		public int? ProcessContainerId { get; set; }
		public string Variable { get; set; }
		public Dictionary<string, string> LanguageTranslation { get; set; }
	}
	public class ProcessActionDialogRowTranslation {
		public ProcessActionDialogRowTranslation() {

		}

		public ProcessActionDialogRowTranslation(DataRow row, AuthenticatedSession session, string alias = "") {
			if (alias.Length > 0) {
				alias = alias + "_";
			}
			Variable = Conv.ToStr(row[alias + "variable"]);
			Code = (string)row["code"];
			TitleTranslation = (string)row["dialog_title_translation"];
			TextTranslation = (string)row["dialog_text_translation"];
			PrimaryButtonTranslation = (string)row["submit_button_translation"];
			SecondaryButtonTranslation = (string)row["cancel_button_translation"];
		}

		public int ProcessActionDialogId { get; set; }
		public string Code { get; set; }
		public string TitleTranslation { get; set; }
		public string TextTranslation { get; set; }
		public string PrimaryButtonTranslation { get; set; }
		public string SecondaryButtonTranslation { get; set; }
		public string Variable { get; set; }
	}

	public class ProcessActionDialogData : ProcessBase {
		public ProcessActionDialogData(string instance, string process, AuthenticatedSession authenticatedSession) : base(instance, process, authenticatedSession) {
			this.instance = instance;
			this.process = process;
			this.authenticatedSession = authenticatedSession;
		}

		public List<ProcessActionDialogRow> GetActionDialogRows() {
			var result = new List<ProcessActionDialogRow>();
			foreach (DataRow row in db.GetDatatable("SELECT process_action_dialog_id, variable, type, process_container_id FROM process_actions_dialog WHERE process = @process", DBParameters).Rows) {
				var action = new ProcessActionDialogRow(row, authenticatedSession);
				result.Add(action);
			}
			
			
			return result;
		}
		public ProcessActionDialogRow GetActionDialogRow(int dialogId) {
			SetDBParam(SqlDbType.Int, "@dialogId", dialogId);

			var dr = db.GetDataRow("SELECT process_action_dialog_id, variable, type, process_container_id FROM process_actions_dialog WHERE process = @process AND process_action_dialog_id = @dialogId", DBParameters);

			var action = new ProcessActionDialogRow(dr, authenticatedSession);
			return action;
		}

		private void AddTranslations(string code, string variable, string variableName, string translationName,Dictionary<string, object>  resultOut ) {
			if (variable == "") return;
			
			SetDBParam(SqlDbType.NVarChar, "@variable", variable);
			SetDBParam(SqlDbType.NVarChar, "@code", code);
			var dt = db.GetDataRow("SELECT translation, variable, code FROM process_translations WHERE instance = @instance AND process = @process AND variable = @variable AND code = @code", DBParameters);
			if (dt != null) {
				if (variableName != "") resultOut.Add(variableName, Conv.ToStr(dt["variable"]));
				if (translationName != "") resultOut.Add(translationName, Conv.ToStr(dt["translation"]));
			}
		}

		public Dictionary<string, Dictionary<string, object>> GetActionDialogTranslations(int ProcessActionDialogId) {
			var result = new Dictionary<string, Dictionary<string, object>>();

			SetDBParam(SqlDbType.Int, "@ProcessActionDialogId", ProcessActionDialogId);
			var row = db.GetDataRow("SELECT dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable FROM process_actions_dialog WHERE process = @process AND process_action_dialog_id = @ProcessActionDialogId", DBParameters);
			foreach (var lang in new SystemList(instance, authenticatedSession).GetListItems("LANGUAGES")) {
				var retval = new Dictionary<string, object>();

				var code = Conv.ToStr(lang["value"]);
				AddTranslations(code, Conv.ToStr(row["dialog_title_variable"]), "TitleVariable", "TitleTranslation", retval);
				AddTranslations(code, Conv.ToStr(row["dialog_text_variable"]), "TextVariable", "TextTranslation", retval);
				AddTranslations(code, Conv.ToStr(row["cancel_button_variable"]), "SecondaryButtonVariable", "SecondaryButtonTranslation", retval);
				AddTranslations(code, Conv.ToStr(row["submit_button_variable"]), "PrimaryButtonVariable", "PrimaryButtonTranslation", retval);
				
				retval.Add("Code", code);
				result.Add(code, retval);
			}
			return result;
		}

		public Dictionary<string, object> GetActionDialogTranslation(int ProcessActionDialogId, string code) {
			SetDBParam(SqlDbType.Int, "@ProcessActionDialogId", ProcessActionDialogId);
			var row = db.GetDataRow("SELECT dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable, type FROM process_actions_dialog WHERE process = @process AND process_action_dialog_id = @ProcessActionDialogId", DBParameters);

			var retval = new Dictionary<string, object>();

			AddTranslations(code, Conv.ToStr(row["dialog_title_variable"]), "", "TitleTranslation", retval);
			AddTranslations(code, Conv.ToStr(row["dialog_text_variable"]), "", "TextTranslation", retval);
			AddTranslations(code, Conv.ToStr(row["cancel_button_variable"]), "", "SecondaryButtonTranslation", retval);
			AddTranslations(code, Conv.ToStr(row["submit_button_variable"]), "", "PrimaryButtonTranslation", retval);
			
			retval.Add("Type", (int)row["type"]);
			return retval;
		}

		public void Delete(int ProcessActionDialogId) {
			SetDBParam(SqlDbType.Int, "@processActionDialogId", ProcessActionDialogId);
			db.ExecuteDeleteQuery("DELETE FROM process_actions_dialog WHERE process_action_dialog_id = @processActionDialogId", DBParameters);
		}

		public ProcessActionDialogRow Save(ProcessActionDialogRow processActionDialogRow) {
			SetDBParam(SqlDbType.Int, "@type", processActionDialogRow.Type);
			SetDBParam(SqlDbType.Int, "@processActionDialogId", processActionDialogRow.ProcessActionDialogId);
			SetDBParam(SqlDbType.Int, "@containerId", processActionDialogRow.ProcessContainerId);

			var oldVar = db.ExecuteScalar("SELECT variable FROM process_actions_dialog WHERE instance = @instance AND process = @process AND process_action_dialog_id = @processActionDialogId", DBParameters);
			if (oldVar != DBNull.Value && processActionDialogRow.LanguageTranslation[authenticatedSession.locale_id] != null) {
				db.ExecuteUpdateQuery("UPDATE process_actions_dialog SET type = @type, process_container_id = @containerId WHERE instance = @instance AND process = @process AND process_action_dialog_id = @processActionDialogId", DBParameters);

				foreach (var translation in processActionDialogRow.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, (string)oldVar, translation.Value, translation.Key);
				}
				
			} else if (oldVar == DBNull.Value && processActionDialogRow.LanguageTranslation[authenticatedSession.locale_id] != null) {
				var newVar = translations.GetTranslationVariable(process, processActionDialogRow.LanguageTranslation[authenticatedSession.locale_id], "ACTIONDG");
				SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
				db.ExecuteUpdateQuery("UPDATE process_actions_dialog SET variable = @variable, type = @type  WHERE instance = @instance AND process = @process AND process_action_dialog_id = @processActionDialogId", DBParameters);

				foreach (var translation in processActionDialogRow.LanguageTranslation) {
					translations.insertOrUpdateProcessTranslation(process, newVar, translation.Value, translation.Key);
				}
			}
 
			return processActionDialogRow;
		}

		public ProcessActionDialogRowTranslation SaveTranslations(int ProcessActionDialogId, ProcessActionDialogRowTranslation ProcessActionDialogRowTranslation) {

			var translations = new Translations(instance, authenticatedSession, ProcessActionDialogRowTranslation.Code);

			SetDBParam(SqlDbType.Int, "@ProcessActionDialogId", ProcessActionDialogId);
			var oldVar = db.GetDataRow("SELECT dialog_title_variable, dialog_text_variable, cancel_button_variable, submit_button_variable FROM process_actions_dialog WHERE instance = @instance AND process = @process AND process_action_dialog_id = @ProcessActionDialogId", DBParameters);

			if (oldVar["dialog_title_variable"] != DBNull.Value && ProcessActionDialogRowTranslation.TitleTranslation != null) {
				translations.insertOrUpdateProcessTranslation(process, (string)oldVar["dialog_title_variable"], ProcessActionDialogRowTranslation.TitleTranslation);
			} else if (oldVar["dialog_title_variable"] == DBNull.Value && ProcessActionDialogRowTranslation.TitleTranslation != null) {
				SetDBParam(SqlDbType.NVarChar, "@dialog_title_variable", translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.TitleTranslation, "ACTIONDG"));
				db.ExecuteUpdateQuery("UPDATE process_actions_dialog SET dialog_title_variable=@dialog_title_variable WHERE instance = @instance AND process = @process AND process_action_dialog_id = @ProcessActionDialogId", DBParameters);
				translations.insertOrUpdateProcessTranslation(process, translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.TitleTranslation, "ACTIONDG"), ProcessActionDialogRowTranslation.TitleTranslation);
			}

			if (oldVar["dialog_text_variable"] != DBNull.Value && ProcessActionDialogRowTranslation.TextTranslation != null) {
				translations.insertOrUpdateProcessTranslation(process, (string)oldVar["dialog_text_variable"], ProcessActionDialogRowTranslation.TextTranslation);
			} else if (oldVar["dialog_text_variable"] == DBNull.Value && ProcessActionDialogRowTranslation.TextTranslation != null) {
				SetDBParam(SqlDbType.NVarChar, "@dialog_text_variable", translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.TextTranslation, "ACTIONDG"));
				db.ExecuteUpdateQuery("UPDATE process_actions_dialog SET dialog_text_variable=@dialog_text_variable WHERE instance = @instance AND process = @process AND process_action_dialog_id = @ProcessActionDialogId", DBParameters);
				translations.insertOrUpdateProcessTranslation(process, translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.TextTranslation, "ACTIONDG"), ProcessActionDialogRowTranslation.TextTranslation);
			}

			if (oldVar["cancel_button_variable"] != DBNull.Value && ProcessActionDialogRowTranslation.SecondaryButtonTranslation != null) {
				translations.insertOrUpdateProcessTranslation(process, (string)oldVar["cancel_button_variable"], ProcessActionDialogRowTranslation.SecondaryButtonTranslation);
			} else if (oldVar["cancel_button_variable"] == DBNull.Value && ProcessActionDialogRowTranslation.SecondaryButtonTranslation != null) {
				SetDBParam(SqlDbType.NVarChar, "@cancel_button_variable", translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.SecondaryButtonTranslation, "ACTIONDG"));
				db.ExecuteUpdateQuery("UPDATE process_actions_dialog SET cancel_button_variable=@cancel_button_variable WHERE instance = @instance AND process = @process AND process_action_dialog_id = @ProcessActionDialogId", DBParameters);
				translations.insertOrUpdateProcessTranslation(process, translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.SecondaryButtonTranslation, "ACTIONDG"), ProcessActionDialogRowTranslation.SecondaryButtonTranslation);
			}

			if (oldVar["submit_button_variable"] != DBNull.Value && ProcessActionDialogRowTranslation.PrimaryButtonTranslation != null) {
				translations.insertOrUpdateProcessTranslation(process, (string)oldVar["submit_button_variable"], ProcessActionDialogRowTranslation.PrimaryButtonTranslation);
			} else if (oldVar["submit_button_variable"] == DBNull.Value && ProcessActionDialogRowTranslation.PrimaryButtonTranslation != null) {
				SetDBParam(SqlDbType.NVarChar, "@submit_button_variable", translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.PrimaryButtonTranslation, "ACTIONDG"));
				db.ExecuteUpdateQuery("UPDATE process_actions_dialog SET submit_button_variable=@submit_button_variable WHERE instance = @instance AND process = @process AND process_action_dialog_id = @ProcessActionDialogId", DBParameters);
				translations.insertOrUpdateProcessTranslation(process, translations.GetTranslationVariable(process, ProcessActionDialogRowTranslation.PrimaryButtonTranslation, "ACTIONDG"), ProcessActionDialogRowTranslation.PrimaryButtonTranslation);
			}

			return ProcessActionDialogRowTranslation;
		}

		public ProcessActionDialogRow Add(ProcessActionDialogRow processActionDialogRow) {
			var newVar = translations.GetTranslationVariable(process, processActionDialogRow.LanguageTranslation[authenticatedSession.locale_id], "ACTIONDG");

			SetDBParam(SqlDbType.NVarChar, "@variable", processActionDialogRow.Variable);
			SetDBParam(SqlDbType.Int, "@type", processActionDialogRow.Type);
			SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
			processActionDialogRow.ProcessActionDialogId = db.ExecuteInsertQuery("INSERT INTO process_actions_dialog (instance, process, variable, type) VALUES(@instance, @process, @variable, @type)", DBParameters);
			foreach (var translation in processActionDialogRow.LanguageTranslation) {
				translations.insertOrUpdateProcessTranslation(process, newVar, translation.Value, translation.Key);
			}
			return processActionDialogRow;
		}
	}
}