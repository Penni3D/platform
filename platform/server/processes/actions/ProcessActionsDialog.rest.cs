﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.actions {
	[Route("v1/settings/ProcessActions/Dialogs")]
	public class ProcessActionDialog : PageBase {
		public ProcessActionDialog() : base("settings") { }

		// GET: model/values
		[HttpGet("{process}")]
		public List<ProcessActionDialogRow> Get(string process) {
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			return processActionsDialog.GetActionDialogRows();
		}

		[HttpGet("{process}/{ProcessActionDialogId}/metadialog")]
		public ProcessActionDialogRow GetMetaDialog(string process, int ProcessActionDialogId) {
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			return processActionsDialog.GetActionDialogRow(ProcessActionDialogId);
		}


		[HttpGet("{process}/{processActionDialogId}/{itemId}/trans")]
		public Dictionary<string, Dictionary<string, object>> Get(string process, int processActionDialogId, int itemId) {
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			var translations = processActionsDialog.GetActionDialogTranslations(processActionDialogId);

			if (itemId != 0) {
				var states = new States(instance, currentSession);
				var DatabaseItemService = new DatabaseItemService(instance, process, currentSession);
				var oldData = DatabaseItemService.GetItem(itemId);

				foreach (var languageSet in translations) {
					if (languageSet.Value.ContainsKey("TitleTranslation")) {
						languageSet.Value["TitleTranslation"] =
							states.ReplaceTags(process, oldData, Conv.ToStr(languageSet.Value["TitleTranslation"]));
					}

					if (languageSet.Value.ContainsKey("TextTranslation")) {
						languageSet.Value["TextTranslation"] =
							states.ReplaceTags(process, oldData, Conv.ToStr(languageSet.Value["TextTranslation"]));
					}
				}
			}
			return translations;
		}


		[HttpGet("{process}/{ProcessActionDialogId}/{language}")]
		public Dictionary<string, object> Get(string process, int ProcessActionDialogId, string language) {
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			return processActionsDialog.GetActionDialogTranslation(ProcessActionDialogId, language);
		}

		[HttpPost("{process}")]
		public ProcessActionDialogRow Post(string process, [FromBody]ProcessActionDialogRow processActionDialogRow) {
			CheckRight("write");
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			if (ModelState.IsValid) {
				return processActionsDialog.Add(processActionDialogRow);
			}
			return null;
		}

		[HttpPut("{process}")]
		public IActionResult Put(string process, [FromBody]List<ProcessActionDialogRow> processActionDialogRow) {
			CheckRight("write");
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pt in processActionDialogRow) {
					processActionsDialog.Save(pt);
				}
				return new ObjectResult(processActionDialogRow);
			}
			return null;
		}

		[HttpPut("{process}/{ProcessActionDialogId}")]
		public IActionResult Put(string process, int ProcessActionDialogId, [FromBody]List<ProcessActionDialogRowTranslation> processActionDialogRow) {
			CheckRight("write");
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			if (ModelState.IsValid) {
				foreach (var pt in processActionDialogRow) {
					processActionsDialog.SaveTranslations(ProcessActionDialogId, pt);
				}
				return new ObjectResult(processActionDialogRow);
			}
			return null;
		}

		[HttpDelete("{process}/{processActionId}")]
		public IActionResult Delete(string process, string processActionId) {
			CheckRight("delete");
			var processActionsDialog = new ProcessActionDialogData(instance, process, currentSession);
			foreach (var id in processActionId.Split(',')) {
				processActionsDialog.Delete(Conv.ToInt(id));
			}
			return new StatusCodeResult(200);
		}
	}
}