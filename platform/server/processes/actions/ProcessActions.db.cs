﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Keto5.x.customer.server;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.httpResponseParsers;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.processCustomActions;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.processes.actions {
    public class ProcessAction {
        public ProcessAction() {
        }

        public ProcessAction(DataRow dr, AuthenticatedSession session, string alias = "") {
            if (alias.Length > 0) {
                alias = alias + "_";
            }

            ProcessActionId = (int)dr[alias + "process_action_id"];
            Variable = Conv.ToStr(dr[alias + "variable"]);
            Process = Conv.ToStr(dr[alias + "process"]);
            ActionHistoryText = Conv.ToStr(dr[alias + "action_history_text"]);
            ActionHistoryItemColumnId = Conv.ToInt(dr[alias + "action_history_item_column_id"]);
            UseActionHistory = Conv.ToBool(dr[alias + "use_action_history"]);
            Process = Conv.ToStr(dr[alias + "process"]);
            LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
            if (!DBNull.Value.Equals(dr[alias + "condition_id"])) {
                ConditionId = (int)dr[alias + "condition_id"];
            } else {
                ConditionId = null;
            }
        }

        public int ProcessActionId { get; set; }
        public string Process { get; set; }
        public string Variable { get; set; }
        public int? ConditionId { get; set; }
        public bool UseActionHistory { get; set; }

        public int ActionHistoryItemColumnId { get; set; }

        public string ActionHistoryText { get; set; }
        public Dictionary<string, string> LanguageTranslation { get; set; }
    }


    public class ProcessActions : ProcessBase {
        private bool history;

        public ProcessActions(string instance, string process, AuthenticatedSession authenticatedSession,
            bool d = false) : base(
            instance,
            process, authenticatedSession) {
            history = d;
        }

        public Dictionary<string, List<ProcessAction>> GetAllProcessActions() {
            var result = new Dictionary<string, List<ProcessAction>>();
            var actions = new List<ProcessAction>();

            foreach (DataRow row in db
                         .GetDatatable(
                             "SELECT process_action_id, variable, process, condition_id, use_action_history, action_history_text, action_history_item_column_id FROM process_actions WHERE instance = @instance ORDER BY process",
                             DBParameters).Rows) {
                var action = new ProcessAction(row, authenticatedSession);

                if (!result.ContainsKey(action.Process)) {
                    result.Add(action.Process, new List<ProcessAction>());
                }

                result[action.Process].Add(action);
            }

            return result;
        }

        public List<ProcessAction> GetActions() {
            var result = new List<ProcessAction>();

            foreach (DataRow row in db
                         .GetDatatable(
                             "SELECT process_action_id, variable, process, condition_id, use_action_history, action_history_text, action_history_item_column_id FROM process_actions WHERE instance = @instance AND process = @process",
                             DBParameters).Rows) {
                var action = new ProcessAction(row, authenticatedSession);
                result.Add(action);
            }

            return result;
        }

        public ProcessAction GetAction(int processActionId) {
            SetDBParam(SqlDbType.Int, "processActionId", processActionId);
            var dt =
                db.GetDatatable(
                    "SELECT process_action_id, variable, process, condition_id, use_action_history, action_history_text, action_history_item_column_id FROM process_actions WHERE instance = @instance AND process = @process AND process_action_id = @processActionId",
                    DBParameters);
            if (dt.Rows.Count > 0) {
                var action = new ProcessAction(dt.Rows[0], authenticatedSession);
                return action;
            }

            throw new CustomArgumentException("Action cannot be found using process_action_id " + processActionId);
        }

        public List<ProcessAction> GetUnusedActions(string process) {
            SetDBParameters("@process", process);
            var result = new List<ProcessAction>();
            var actionsInProcessesTable = new List<double>();

            foreach (DataRow row2 in db
                         .GetDatatable("SELECT new_row_action_id FROM processes WHERE new_row_action_id IS NOT NULL",
                             DBParameters).Rows) {
                actionsInProcessesTable.Add(Conv.ToDouble(row2["new_row_action_id"]));
            }

            foreach (DataRow row2 in db
                         .GetDatatable(
                             "SELECT after_new_row_action_id FROM processes WHERE after_new_row_action_id IS NOT NULL",
                             DBParameters).Rows) {
                actionsInProcessesTable.Add(Conv.ToDouble(row2["after_new_row_action_id"]));
            }

            var sql =
                "SELECT  a.process_action_id, a.variable, a.process, condition_id, use_action_history, action_history_text, action_history_item_column_id FROM process_actions a LEFT JOIN item_columns c ON c.process_action_id = a.process_action_id AND c.data_type = 8 AND c.process = @process " +
                "LEFT JOIN process_translations pt1 ON pt1.variable = c.variable " +
                "WHERE pt1.translation IS NULL AND a.process = @process ";

            foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
                if (!actionsInProcessesTable.Contains(Conv.ToInt(row["process_action_id"]))) {
                    var action = new ProcessAction(row, authenticatedSession);
                    result.Add(action);
                }
            }

            return result;
        }

        public List<Dictionary<string, object>> GetButtonsForActions(string process) {
            SetDBParameters("@process", process);
            SetDBParameters("@locale", authenticatedSession.locale_id);
            var result = new Dictionary<int, object>();
            return db
                .GetDatatableDictionary(
                    "SELECT pt1.translation AS button, c.item_column_id as item_column_id, pt2.translation AS action, a.process_action_id as action_id, condition_id, use_action_history, action_history_text, action_history_item_column_id FROM process_actions a " +
                    "LEFT JOIN item_columns c ON c.process_action_id = a.process_action_id AND c.data_type = 8 AND c.process = @process " +
                    "LEFT JOIN process_translations pt1 ON pt1.variable = c.variable AND pt1.code = @locale " +
                    "LEFT JOIN process_translations pt2 ON pt2.variable = a.variable AND pt2.code = @locale " +
                    "WHERE pt1.translation IS NOT NULL and pt2.process = @process",
                    DBParameters);
        }

        public List<Dictionary<string, object>> GetActionItemColumnRelations(int itemColumnId) {
            SetDBParameters("@itemColumnId", itemColumnId);
            SetDBParameters("@locale", authenticatedSession.locale_id);
            var result = new Dictionary<int, object>();
            return db
                .GetDatatableDictionary(
                    "SELECT pt.translation as action_translation, pa.process_action_id FROM process_action_rows par " +
                    "LEFT JOIN process_actions pa ON pa.process_action_id = par.process_action_id " +
                    "LEFT JOIN process_translations pt ON pt.variable = pa.variable " +
                    "WHERE item_column_id = @itemColumnId AND pt.code = @locale",
                    DBParameters);
        }

        public void Delete(int processActionId) {
            SetDBParam(SqlDbType.Int, "processActionId", processActionId);
            db.ExecuteUpdateQuery(
                "UPDATE item_columns SET process_action_id = NULL WHERE process_action_id = @processActionId",
                DBParameters);

            db.ExecuteDeleteQuery(
                "DELETE FROM process_actions WHERE instance = @instance AND process = @process AND process_action_id = @processActionId",
                DBParameters);
        }

        public void Save(ProcessAction processAction) {
            SetDBParam(SqlDbType.Int, "@processActionId", processAction.ProcessActionId);
            SetDBParam(SqlDbType.Int, "@conditionId", Conv.IfNullThenDbNull(processAction.ConditionId));
            SetDBParam(SqlDbType.Int, "@useactionhistory", Conv.ToInt(processAction.UseActionHistory));
            SetDBParam(SqlDbType.Int, "@actionhistoryitemcolumnid", processAction.ActionHistoryItemColumnId);
            SetDBParam(SqlDbType.NVarChar, "@actionhistorytext", processAction.ActionHistoryText);

            var oldVar =
                db.ExecuteScalar(
                    "SELECT variable FROM process_actions WHERE instance = @instance AND process = @process AND process_action_id = @processActionId",
                    DBParameters);
            if (oldVar != DBNull.Value &&
                processAction.LanguageTranslation.ContainsKey(authenticatedSession.locale_id) &&
                processAction.LanguageTranslation[authenticatedSession.locale_id] != null) {
                foreach (var translation in processAction.LanguageTranslation) {
                    translations.insertOrUpdateProcessTranslation(process, (string)oldVar, translation.Value,
                        translation.Key);
                }
            } else if (oldVar == DBNull.Value &&
                       processAction.LanguageTranslation[authenticatedSession.locale_id] != null) {
                var newVar = translations.GetTranslationVariable(process,
                    processAction.LanguageTranslation[authenticatedSession.locale_id], "ACTION");

                SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
                db.ExecuteUpdateQuery(
                    "UPDATE process_actions SET variable = @variable WHERE instance = @instance AND process = @process AND process_action_id = @processActionId",
                    DBParameters);

                foreach (var translation in processAction.LanguageTranslation) {
                    translations.insertOrUpdateProcessTranslation(process, newVar, translation.Value, translation.Key);
                }
            }

            db.ExecuteUpdateQuery(
                "UPDATE process_actions SET condition_id = @conditionId, use_action_history = @useactionhistory, action_history_text = @actionhistorytext, action_history_item_column_id = @actionhistoryitemcolumnid WHERE instance = @instance AND process = @process AND process_action_id = @processActionId",
                DBParameters);
        }

        public void Add(ProcessAction processAction) {
            processAction.ProcessActionId =
                db.ExecuteInsertQuery("INSERT INTO process_actions (instance, process) VALUES(@instance, @process)",
                    DBParameters);
            if (processAction.LanguageTranslation[authenticatedSession.locale_id] != null) {
                var newVar = translations.GetTranslationVariable(process,
                    processAction.LanguageTranslation[authenticatedSession.locale_id], "ACTION");
                SetDBParam(SqlDbType.NVarChar, "@variable", newVar);
                SetDBParam(SqlDbType.Int, "@processActionId", processAction.ProcessActionId);
                db.ExecuteUpdateQuery(
                    "UPDATE process_actions SET variable=@variable WHERE instance = @instance AND process = @process AND process_action_id = @processActionId",
                    DBParameters);

                foreach (var translation in processAction.LanguageTranslation) {
                    translations.insertOrUpdateProcessTranslation(process, newVar, translation.Value, translation.Key);
                }
            }
        }

        Dictionary<int, int>
            parentOwnerPairs = new Dictionary<int, int>();

        List<int>
            updateRequiredRows =
                new List<int>();

        public void CopySubItemColumnRows(Dictionary<string, object> rowData,
            string process,
            int parent_id,
            Dictionary<string, int> attachmentColumns,
            Dictionary<string, int> dataTableColumns
        ) {
            var parentItemId = 0;
            var ownerItemId = Conv.ToInt(rowData["owner_item_id"]);

            if (rowData.ContainsKey("parent_item_id")) {
                parentItemId = Conv.ToInt(rowData["parent_item_id"]);
            }

            var owningSubRowItemId =
                Conv.ToInt(rowData["item_id"]); //Needed if you copy a datatable row that has datatable column in it

            var thisTableProcessItemBase = new ItemBase(instance, process, authenticatedSession);
            var tableProcessItemColumns = new ItemColumns(instance, process, authenticatedSession);
            tableProcessItemColumns.GetItemColumns();

            var tableProcessRowData = new Dictionary<string, object>();
            tableProcessRowData["item_id"] = thisTableProcessItemBase.InsertItemRow();
            var subparent = Conv.ToInt(tableProcessRowData["item_id"]);

            parentOwnerPairs.Add(Conv.ToInt(rowData["item_id"]),
                Conv.ToInt(tableProcessRowData["item_id"]));

            tableProcessRowData["owner_item_id"] = parent_id;


            if (parentItemId != 0) {
                if (ownerItemId == parentItemId) {
                    //If this row has no parent rows in the hierarchy, just update the owning process row to parent_item_id
                    tableProcessRowData["parent_item_id"] = parent_id;
                } else {
                    //If this row has parent rows in the hierarchy, keep current parent_item_id for later update
                    updateRequiredRows.Add(Conv.ToInt(tableProcessRowData["item_id"]));
                    tableProcessRowData["parent_item_id"] = rowData["parent_item_id"];
                }
            }

            foreach (var column in rowData) {
                var item_id = Conv.ToInt(rowData["item_id"]);

                if (attachmentColumns.ContainsKey(column.Key)) {
                    var attachments = new Attachments(instance, authenticatedSession);
                    attachments.CopyAttachments(item_id, Conv.ToInt(tableProcessRowData["item_id"]),
                        attachmentColumns[column.Key],
                        attachmentColumns[column.Key]);
                }

                if (dataTableColumns.ContainsKey(column.Key)) {
                    foreach (var ic in tableProcessItemColumns.GetItemColumns()) {
                        if (ic.Name == column.Key) {
                            //var _subparent = 0;
                            var subProcessSubProcessIb =
                                new ItemBase(instance, ic.DataAdditional, authenticatedSession);
                            var search = new Dictionary<string, object>();
                            search.Add("owner_item_id", owningSubRowItemId);
                            var subCopyData =
                                subProcessSubProcessIb.GetItems(search);

                            foreach (var oldThirdLevelData in subCopyData) {
                                var ownerItemIdSub = Conv.ToInt(oldThirdLevelData["owner_item_id"]);
                                var parentItemIdSub =
                                    Conv.ToInt(oldThirdLevelData["parent_item_id"]);

                                var newThirdLevelData = new Dictionary<string, object>();
                                newThirdLevelData["item_id"] =
                                    subProcessSubProcessIb.InsertItemRow();

                                var myOldId = Conv.ToInt(oldThirdLevelData["item_id"]);
                                parentOwnerPairs.Add(myOldId,
                                    Conv.ToInt(newThirdLevelData["item_id"]));

                                newThirdLevelData["owner_item_id"] = subparent;

                                if (ownerItemIdSub == parentItemIdSub) {
                                    newThirdLevelData["parent_item_id"] = subparent;
                                } else {
                                    updateRequiredRows.Add(Conv.ToInt(newThirdLevelData["item_id"]));
                                    newThirdLevelData["parent_item_id"] =
                                        oldThirdLevelData["parent_item_id"];
                                }

                                foreach (var subRowDataColumn in oldThirdLevelData) {
                                    if (subRowDataColumn.Key != "owner_item_id" &&
                                        subRowDataColumn.Key != "parent_item_id" && subRowDataColumn.Key != "item_id") {
                                        newThirdLevelData[subRowDataColumn.Key] = subRowDataColumn.Value;
                                    }
                                }

                                subProcessSubProcessIb.SaveItem(newThirdLevelData);
                            }

                            var subSearch = new Dictionary<string, object>();

                            subSearch.Add("owner_item_id", subparent);
                            var subAddedData = subProcessSubProcessIb.GetItems(subSearch);
                            foreach (var rowData2 in subAddedData) {
                                if (updateRequiredRows.Contains(Conv.ToInt(rowData2["item_id"]))) {
                                    rowData2["parent_item_id"] =
                                        parentOwnerPairs[Conv.ToInt(rowData2["parent_item_id"])];
                                }

                                subProcessSubProcessIb.SaveItem(rowData2);
                            }
                        }
                    }
                }

                if (column.Key != "owner_item_id" && column.Key != "parent_item_id" && column.Key != "item_id" &&
                    column.Key != "serial") {
                    tableProcessRowData[column.Key] = column.Value;
                } else if (column.Key == "serial") {
                    var processBase_ =
                        new ProcessBase(authenticatedSession, Conv.ToStr(tableProcessRowData["process"]));
                    tableProcessRowData["serial"] = processBase_.GetNextSerial();
                }
            }

            thisTableProcessItemBase.SaveItem(tableProcessRowData);

            var search2 = new Dictionary<string, object>();
            search2.Add("owner_item_id", parent_id);
            //Get all rows that belong to the new 'owner' process rows. For example all risk rows that new project owns (through datatable item column)
            //We do this because we want to update new parent_item_id values for items that have a hierarchy parent and this is only possible AFTER all rows have been created
            var newAddedData = thisTableProcessItemBase.GetItems(search2);
            foreach (var rowData2 in newAddedData) {
                if (updateRequiredRows.Contains(Conv.ToInt(rowData2["item_id"]))) {
                    //Here we set a new parent_item_id to all rows that actually have a parent in the same datatable. We get it from an associative array that pairs old and new ids of the copied rows

                    if (parentOwnerPairs.ContainsKey(Conv.ToInt(rowData2["parent_item_id"]))) {
                        rowData2["parent_item_id"] = parentOwnerPairs[Conv.ToInt(rowData2["parent_item_id"])];
                    }
                }

                thisTableProcessItemBase.SaveItem(rowData2);
            }
        }

        private readonly Dictionary<int, bool> _actionRowConditionResults = new Dictionary<int, bool>();

        private bool CheckActionRowCondition(States states, int itemId, ProcessActionRow row) {
            if (_actionRowConditionResults.ContainsKey(Conv.ToInt(row.ConditionId)))
                return _actionRowConditionResults[Conv.ToInt(row.ConditionId)];

            if (Conv.ToInt(row.ConditionId) <= 0) return true;
            if (states == null || itemId <= 0) return false;

            var result = states.GetConditionExpressionResult(process, itemId,
                new Conditions(instance, process, authenticatedSession).GetCondition(Conv.ToInt(row.ConditionId)));
            _actionRowConditionResults.Add(Conv.ToInt(row.ConditionId), result);

            return result;
        }

        public Dictionary<string, object> DoAction(int ProcessActionId, int itemId, bool checkRights = true,
            int? itemColumnId = null,
            bool isNewRowAction = false,
            bool fromItself = false,
            bool runSignaler = true) {
            //Set up objects
            var retval = new Dictionary<string, object>();
            var newIds = new Dictionary<string, List<int>>();
            var itemIdDeleted = false;
            var ib = new ItemBase(instance, process, authenticatedSession);
            var actionRows = new ProcessActionRows(instance, process, authenticatedSession);
            var ics = new ItemColumns(instance, process, authenticatedSession);
            var states = new States(instance, authenticatedSession);
            var actions = new ProcessActions(instance, process, authenticatedSession);
            Dictionary<string, object> currentData = null;
            Dictionary<string, object> newRowData = null;
            var newRowItemId = 0;
            ItemBase newRowIb = null;
            ItemColumns newRowIcs = null;
            var parent_id = 0;
            var createActionAfterActions = new Dictionary<int, string>();

            //Get This Action
            var a = actions.GetAction(ProcessActionId);

            //If this action has a condition attached -- evaluate it
            if (a.ConditionId != null) {
                var conds = new Conditions(instance, process, authenticatedSession);
                if (!states.GetConditionExpressionResult(process, itemId, conds.GetCondition((int)a.ConditionId))
                   ) {
                    return retval;
                }
            }

            //Get Data for item if not new row action case
            Dictionary<string, object> oldData = null;
            if (!isNewRowAction)
                oldData = ib.GetItem(itemId, checkRights: false);
            var data = new Dictionary<string, object>();
            AddToDict(data, "item_id", itemId);

            //Execute all action rows in this action
            foreach (var row in actionRows.GetActionRows(ProcessActionId)) {
                //Check if there are conditions for this row -- If yes, evaluate
                if (row.ConditionId != null) {
                    if (!CheckActionRowCondition(states, itemId, row))
                        continue;
                }

                //Data Copy / Set
                if (row.ItemColumnId != null) {
                    ItemColumn iCol = null;
                    if (row.ProcessActionType != 8) iCol = ics.GetItemColumn((int)row.ItemColumnId);

                    switch (row.ProcessActionType) {
                        case 0:
                            switch (row.ValueType) {
                                case 0:
                                    if (iCol.IsLink()) {
                                        var links =
                                            new Attachments(instance, authenticatedSession);
                                        var d = new Dictionary<string, object>();
                                        d.Add("name", row.Value);
                                        d.Add("url", row.Value2);

                                        var x = links.SetLink(itemId, iCol.ItemColumnId, d);
                                    } else if (!iCol.IsProcess() && !iCol.IsList()) {
                                        data = AddToDict(data, iCol.Name, row.Value);
                                    } else {
                                        data = AddToDict(data, iCol.Name, JsonConvert.DeserializeObject(row.Value));
                                    }

                                    break;
                                case 1:
                                    if (iCol.IsDate() || iCol.IsDateTime()) {
                                        data = AddToDict(data, iCol.Name, DateTime.Now.ToUniversalTime());
                                    }

                                    break;
                                case 2:
                                    if (iCol.IsProcess() && iCol.DataAdditional == "user") {
                                        var val = new int[1];
                                        val[0] = authenticatedSession.DelegatorOrItemId;
                                        data = AddToDict(data, iCol.Name, val);
                                    }

                                    break;
                                case 3:
                                    var icsU = new ItemColumns(instance, "user", authenticatedSession);
                                    var iColU = icsU.GetItemColumn((int)row.UserItemColumnId);
                                    var ibU = new ItemBase(instance, "user", authenticatedSession);
                                    data = AddToDict(data, iCol.Name,
                                        ibU.GetItem(authenticatedSession.item_id, checkRights: false)[iColU.Name]);
                                    break;
                                case 4:
                                    if (iCol.IsProcess() || iCol.IsList()) {
                                        data[iCol.Name] = new int[] { };
                                    } else {
                                        data[iCol.Name] = DBNull.Value;
                                    }

                                    break;
                            }

                            break;
                        case 1:
                        case 2:
                            if (iCol.IsNvarChar() || iCol.IsInt()) {
                                if (row.ProcessActionType == 1) {
                                    if (currentData == null) {
                                        currentData = ib.GetItem(itemId, checkRights: false);
                                    }

                                    if (!data.ContainsKey(iCol.Name)) {
                                        data.Add(iCol.Name, currentData[iCol.Name]);
                                    }

                                    data = AddToDict(data, iCol.Name,
                                        Conv.ToInt(data[iCol.Name]) + Conv.ToInt(row.Value));
                                } else {
                                    if (currentData == null) {
                                        currentData = ib.GetItem(itemId, checkRights: false);
                                    }

                                    if (!data.ContainsKey(iCol.Name)) {
                                        data.Add(iCol.Name, currentData[iCol.Name]);
                                    }

                                    data = AddToDict(data, iCol.Name,
                                        Conv.ToInt(data[iCol.Name]) - Conv.ToInt(row.Value));
                                }
                            }

                            break;
                        case 4:
                            if (newRowItemId > 0) {
                                oldData = ib.GetItem(itemId, checkRights: false);
                                newRowData[Conv.ToStr(row.Value)] = oldData[iCol.Name];

                                if (iCol.IsRm() || iCol.IsPortfolio()) {
                                    var portfolioIcs = new ItemColumns(instance, iCol.DataAdditional,
                                        authenticatedSession);

                                    var attachmentColumns = new Dictionary<string, int>();
                                    var dataTableColumns = new Dictionary<string, int>();

                                    foreach (var col in portfolioIcs.GetItemColumns()) {
                                        if (col.IsAttachment()) {
                                            attachmentColumns.Add(col.Name, col.ItemColumnId);
                                        } else if (col.IsPortfolio() || col.IsRm()) {
                                            dataTableColumns.Add(col.Name, col.ItemColumnId);
                                        }
                                    }

                                    var search = new Dictionary<string, object>();
                                    var dataTableIb = new ItemBase(instance, iCol.DataAdditional,
                                        authenticatedSession);
                                    search.Add("owner_item_id", itemId);
                                    //Get all sub process rows that this copied process owns
                                    var copyData = dataTableIb.GetItems(search, "item_id");

                                    foreach (var rowData in copyData) {
                                        CopySubItemColumnRows(rowData, iCol.DataAdditional, parent_id,
                                            attachmentColumns, dataTableColumns);
                                    }
                                } else if (iCol.IsAttachment() || iCol.IsLink()) {
                                    SetDBParameter("@destinationColumn", Conv.ToStr(row.Value));
                                    SetDBParameter("@newid", Conv.ToInt(newRowItemId));
                                    SetDBParameter("@right_process", Conv.ToStr(db.ExecuteScalar(
                                        "SELECT process FROM items WHERE item_id = @newid",
                                        DBParameters)));

                                    var destinationId = Conv.ToInt(db.ExecuteScalar(
                                        "SELECT item_column_id FROM item_columns WHERE name = @destinationColumn AND process = @right_process",
                                        DBParameters));

                                    var attachments = new Attachments(instance, authenticatedSession);
                                    attachments.CopyAttachments(itemId, newRowItemId,
                                        iCol.ItemColumnId,
                                        destinationId);
                                }
                            }

                            break;
                        case 8:
                            iCol = newRowIcs.GetItemColumn((int)row.ItemColumnId);
                            switch (row.ValueType) {
                                case 0:
                                    if (iCol.IsLink()) {
                                        var links =
                                            new Attachments(instance, authenticatedSession);
                                        var d = new Dictionary<string, object>();
                                        d.Add("name", row.Value);
                                        d.Add("url", row.Value2);

                                        var x = links.SetLink(newRowItemId, iCol.ItemColumnId, d);
                                    } else if (!iCol.IsProcess() && !iCol.IsList()) {
                                        newRowData = AddToDict(newRowData, iCol.Name, row.Value);
                                    } else {
                                        newRowData = AddToDict(newRowData, iCol.Name,
                                            JsonConvert.DeserializeObject(row.Value));
                                    }

                                    break;
                                case 1:
                                    if (iCol.IsDate() || iCol.IsDateTime()) {
                                        newRowData = AddToDict(newRowData, iCol.Name,
                                            DateTime.Now.ToUniversalTime());
                                    }

                                    break;
                                case 2:
                                    if (iCol.IsProcess() && iCol.DataAdditional == "user") {
                                        var val = new int[1];
                                        val[0] = authenticatedSession.item_id;
                                        newRowData = AddToDict(newRowData, iCol.Name, val);
                                    }

                                    break;
                                case 3:
                                    var icsU = new ItemColumns(instance, "user", authenticatedSession);
                                    var iColU = icsU.GetItemColumn((int)row.UserItemColumnId);
                                    var ibU = new ItemBase(instance, "user", authenticatedSession);
                                    newRowData = AddToDict(newRowData, iCol.Name,
                                        ibU.GetItem(authenticatedSession.item_id, checkRights: false)[iColU.Name]);
                                    break;
                                case 4:
                                    if (iCol.IsProcess() || iCol.IsList()) {
                                        newRowData[iCol.Name] = new int[] { };
                                    } else {
                                        newRowData[iCol.Name] = DBNull.Value;
                                    }

                                    break;
                            }

                            break;
                        case 9:
                            currentData ??= ib.GetItem(itemId, checkRights: false);

                            //Equation might cause process field to have 0 value -- if so empty
                            if (currentData[iCol.Name].GetType() == Array.Empty<int>().GetType()) {
                                var values = (int[])currentData[iCol.Name];
                                if (values.Contains(0)) values = Array.Empty<int>();
                                data = AddToDict(data, row.Value, values);
                            } else {
                                data = AddToDict(data, row.Value, currentData[iCol.Name]);
                            }

                            break;
                    }

                    //Save data to new row
                    if (newRowItemId > 0)
                        newRowIb.SaveItem(newRowData);
                }

                //Create Action
                if (row.ProcessActionType == 3) {
                    var newRowProcess = Conv.ToStr(row.Value);
                    newRowIb = new ItemBase(instance, newRowProcess, authenticatedSession);
                    newRowIcs = new ItemColumns(instance, newRowProcess, authenticatedSession);
                    newRowItemId = newRowIb.InsertItemRow(ownerItemId: newRowData != null && newRowData.ContainsKey("owner_item_id") ? Conv.ToInt(newRowData.ContainsKey("owner_item_id")) : 0);
                    if (!newIds.ContainsKey(Conv.ToStr(row.Value)))
                        newIds.Add(Conv.ToStr(row.Value), new List<int>());

                    newIds[Conv.ToStr(row.Value)].Add(newRowItemId);
                    parent_id = newRowItemId;

                    newRowData = newRowIb.GetItem(newRowItemId, checkRights: false);
                    if (newRowData.ContainsKey("owner_item_id"))
                        newRowData["owner_item_id"] = itemId;
                    if (row.LinkItemColumnId != null) {
                        SetDBParam(SqlDbType.Int, "@itemColumnId", row.LinkItemColumnId);
                        var linkProcess = Conv.ToStr(db.ExecuteScalar(
                            "SELECT process FROM item_columns WHERE instance = @instance AND item_column_id = @itemColumnId",
                            DBParameters));

                        if (linkProcess == process) {
                            var iColLink = ics.GetItemColumn((int)row.LinkItemColumnId);
                            if (iColLink.RelativeColumnId != null) {
                                var icsLink = new ItemColumns(instance, newRowProcess,
                                    authenticatedSession);
                                var iColLink2 = icsLink.GetItemColumn((int)iColLink.RelativeColumnId);
                                if (iColLink2.DataType.In(5, 6))
                                    newRowData[iColLink2.Name] = new List<int> { itemId };
                                else
                                    newRowData[iColLink2.Name] = itemId;
                                newRowIb.SaveItem(newRowData, checkRights: checkRights);
                            } else {
                                if (currentData == null) {
                                    currentData = ib.GetItem(itemId, checkRights: false);
                                }

                                if (!data.ContainsKey(iColLink.Name)) {
                                    data.Add(iColLink.Name, currentData[iColLink.Name]);
                                }

                                ((List<int>)data[iColLink.Name]).Add(newRowItemId);
                            }
                        } else if (linkProcess == row.Value) {
                            var iColLink = newRowIcs.GetItemColumn((int)row.LinkItemColumnId);
                            if (iColLink.DataType.In(5, 6))
                                newRowData[iColLink.Name] = new List<int> { itemId };
                            else
                                newRowData[iColLink.Name] = itemId;
                            newRowIb.SaveItem(newRowData, checkRights: checkRights);
                        }
                    }

                    createActionAfterActions.Add(newRowItemId, newRowProcess);
                }


                if (row.ProcessActionType == 6)
                    throw new NotImplementedException();

                if (row.ProcessActionType == 7) {
                    SetDBParam(SqlDbType.Int, "@itemId", itemId);
                    db.ExecuteUpdateQuery(
                        "UPDATE _" + instance + "_task SET status = 2 WHERE owner_item_id = @itemId", DBParameters);
                }

                if (row.ProcessActionType == 10) {
                    if (row.ItemColumnId != null && row.Value != null && row.Value2 != null &&
                        row.LinkItemColumnId != null) {
                        if (currentData == null)
                            currentData = ib.GetItem(itemId, checkRights: false);

                        var copyCol = ics.GetItemColumn((int)row.ItemColumnId);
                        var linkCol = ics.GetItemColumn((int)row.LinkItemColumnId);
                        var parentId = Conv.ToInt(currentData[linkCol.Name]);
                        if (parentId > 0) {
                            var parentIb = new ItemBase(instance, row.Value, authenticatedSession);
                            var parentData = parentIb.GetItem(parentId, checkRights: false);
                            parentData[row.Value2] = currentData[copyCol.Name];
                            parentIb.SaveItem(parentData, checkRights: checkRights);
                        }
                    }
                }

                //HTTP Requests
                if (row.ProcessActionType == 11) {
					var notificationIb = new ItemBase(instance, "notification", authenticatedSession);
					var notification = notificationIb.GetItem(Conv.ToInt(row.Value), checkRights: false);

					var http_url = states.ReplaceTags(process, ib.GetItem(itemId, checkRights: false),
						Conv.ToStr(notification["httprequest_url"]));

					var http_header = states.ReplaceTags(process, ib.GetItem(itemId, checkRights: false),
						Conv.ToStr(notification["httprequest_header"]));

					var request = (HttpWebRequest) WebRequest.Create((string) http_url);

					if (!http_header.ToLower().Contains("content-type"))
						request.ContentType = "application/json";

					var httprequest_type = Conv.ToInt(notification["httprequest_type"]);

					switch (httprequest_type) {
						default:
						case 0:
							request.Method = "GET";
							break;
						case 1:
							request.Method = "POST";
							break;
						case 2:
							request.Method = "PUT";
							break;
						case 3:
							request.Method = "DELETE";
							break;
						case 4:
							request.Method = "PATCH";
							break;
					}

					request.KeepAlive = true;

					var headerData =
						JsonConvert.DeserializeObject<Dictionary<string, string>>(http_header);

					foreach (var key in headerData.Keys) {
						request.Headers.Add(key, headerData[key]);
					}

					var http_body = "";
					if (httprequest_type == 1 || httprequest_type == 2 || httprequest_type == 4) {
						http_body = states.ReplaceTags(process, ib.GetItem(itemId, checkRights: false),
							Conv.ToStr(notification["httprequest_body"]), allow_nulls: true);

						var httpdata = Encoding.ASCII.GetBytes(http_body);
						using (var requestStream = request.GetRequestStream()) {
							requestStream.Write(httpdata, 0, httpdata.Length);
						}
					}

					var httpresponse = "";
					var statuscode = "";
					try {
						using (var response = request.GetResponse() as HttpWebResponse) {
							statuscode = response.StatusCode.ToString();
							if ((int)response.StatusCode >= 200 && (int)response.StatusCode < 300) {
								using (var stream = response.GetResponseStream())
								using (var reader = new StreamReader(stream)) {
									httpresponse = reader.ReadToEnd();
								}
							}
						}
					} catch (Exception e) {
						httpresponse = e.Message;
						statuscode = "?";
					}

					SetDBParam(SqlDbType.DateTime, "@requestDate", DateTime.UtcNow);
					SetDBParam(SqlDbType.Int, "@processItemId", itemId);
					SetDBParam(SqlDbType.Int, "@actionRowId", row.ProcessActionRowId);
					SetDBParam(SqlDbType.Int, "@requestId", row.Value);
					SetDBParam(SqlDbType.VarChar, "@requestType", request.Method);
					SetDBParam(SqlDbType.VarChar, "@statusCode", statuscode);
					SetDBParam(SqlDbType.VarChar, "@url", http_url);
					SetDBParam(SqlDbType.VarChar, "@header", http_header);
					SetDBParam(SqlDbType.VarChar, "@body", http_body);
					SetDBParam(SqlDbType.VarChar, "@response", httpresponse);

					var requestRowId = db.ExecuteInsertQuery(
						"INSERT INTO instance_http_requests ( instance, request_date, user_item_id, process_item_id, process_action_row_id, " +
						"httprequest_item_id, httprequest_type, httprequest_status_code, url, header, body, response) " +
						" VALUES (@instance, @requestDate, @signedInUserId, @processItemId, @actionRowId, " +
						" @requestId,  @requestType, @statusCode, @url, @header, @body, @response) ", DBParameters);


					if (row.Value2 != null) {
						var parser = RegisteredHttpResponseParsers.GetParser(row.Value2);
						if (parser != null) {
							try {
								parser.instance = instance;
								parser.session = authenticatedSession;
								parser.ParseResponse(row.ProcessActionRowId, itemId, request.Method, statuscode,
									http_url,
									http_header, http_body, httpresponse, requestRowId);
							} catch (Exception e) 	{
								Diag.Log(instance,
									e.Message,
									0,
									Diag.LogSeverities.Error, Diag.LogCategories.System);
							}
						}
					}
				}

                //Custom Actions
                if (row.ProcessActionType == 12) {
                    var customActions = RegisteredCustomActions.GetCustomAction(row.Value);
                    if (row.Value == "DeleteActions") {
                        itemIdDeleted = true;
                    }

                    if (customActions != null) {
                        customActions.instance = instance;
                        customActions.session = authenticatedSession;
                        customActions.DoCustomAction(row.ProcessActionRowId, itemId, row.Value2, row.Value3);
                    }
                }
            }

            //Run after actions for all rows that have been created
            foreach (var (newProcessItemId, newProcess) in createActionAfterActions) {
                new ProcessBase(authenticatedSession, newProcess).DoNewRowAction(newProcessItemId, true);
            }

            //Set Current State
            if (itemColumnId != null && itemColumnId > 0) {
                var buttonCol = ics.GetItemColumn((int)itemColumnId);
                if (buttonCol.DataAdditional2 != null) {
                    var da2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(buttonCol.DataAdditional2);
                    if (da2.ContainsKey("currentState")) {
                        var cs = da2["currentState"];
                        data = AddToDict(data, "current_state", cs);
                    }
                }
            }

            //Get States
            FeatureStates fStates = null;

            //Save changes
            if (!itemIdDeleted) {
                if (!isNewRowAction)
                    fStates = states.GetEvaluatedStatesForFeature(process, "meta", itemId);
                ib.SaveItem(data, checkRights: checkRights);
            }

            //Send notifications
            foreach (var row3 in actionRows.GetActionRows(ProcessActionId)) {
                if (!CheckActionRowCondition(states, itemId, row3))
                    continue;

                if (row3.ProcessActionType != 5)
                    continue;

                if (row3.ListProcess != "" && row3.ListItemColumnIds.Count > 0)
                    row3.ItemColumnIds.Add(row3.ListProcess + ";;" + row3.ListItemColumnIds[0]);

                var i = ib.GetItem(itemId, checkRights: false);

                new Thread(delegate() {
                    try {
                        states.SendNotification(process, itemId, Conv.ToInt(row3.Value),
                            row3.ItemColumnIds.ToArray(),
                            i, row3.Value3, row3.UserGroupIds.ToArray(),
                            row3.ActionRowEmailViewOptions,
                            row3.NotificationType,
                            row3.ExcludeSender);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message + " " + ex.StackTrace);
                    }
                }).Start();
            }

            //Override Serials
            foreach (var serialprocess in newIds.Keys) {
                foreach (var serialid in newIds[serialprocess]) {
                    var serialics = new ItemColumns(instance, serialprocess, authenticatedSession);
                    var serialic = serialics.GetItemColumnByName("serial", true);
                    if (serialic == null)
                        continue;

                    var customerConfig = new CustomerConfig();
                    var serial =
                        customerConfig.OverrideSerial(serialid, authenticatedSession, instance, serialprocess);
                    if (serial == "")
                        continue;

                    var serialdis = new DatabaseItemService(instance, serialprocess, authenticatedSession);
                    var serialdata = new Dictionary<string, object> { { "item_id", serialid }, { "serial", serial } };
                    serialdis.SaveItem(serialdata, checkRights: checkRights);
                }
            }

            ib.InvalidateCache(itemId);

            //Save Baseline Information
            if (!isNewRowAction && !itemIdDeleted) {
                var states2 = new States(instance, authenticatedSession);
                var fStates2 = states2.GetEvaluatedStatesForFeature(process, "meta", itemId);

                var hidsToSave = fStates.groupStates.Keys
                    .Where(hid => !fStates.groupStates[hid].s.Contains("meta.history")).Where(hid =>
                        fStates2.groupStates.ContainsKey(hid) && fStates2.groupStates[hid].s.Contains("meta.history"))
                    .ToList();

                if (hidsToSave.Count > 0) {
                    SetDBParam(SqlDbType.Int, "@itemId", itemId);
                    SetDBParam(SqlDbType.Int, "@userId", authenticatedSession.item_id);
                    foreach (var gid in hidsToSave) {
                        SetDBParam(SqlDbType.Int, "@processGroupId", gid);
                        db.ExecuteInsertQuery(
                            "INSERT INTO process_baseline (instance, process, process_item_id, process_group_id, baseline_date, creator_user_item_id) VALUES (@instance, @process, @itemId, @processGroupId , DATEADD(SECOND, 3, GETUTCDATE()) , @userId)",
                            DBParameters);
                    }
                }

                var newData = ib.GetRow(itemId);

                if (fromItself != true) {
                    states.CheckNotifications(process, itemId, oldData, newData.Data);
                }

                newData.statesChanged = ib.HaveStatesChanged(fStates, fStates2);
                retval.Add("data", newData);
            } else {
                retval.Add("data", null);
            }

            //Creation notifications
            if (newRowItemId > 0 && fromItself != true) {
                states.CheckNotificationsForCreate(newRowIb.ProcessBase.process, newRowItemId,
                    newRowIb.GetItem(newRowItemId, checkRights: false));
            }

            //Add to Action History
            if (!isNewRowAction && a.UseActionHistory && Conv.ToStr(a.ActionHistoryText).Length > 0) {
                var ahib = new ItemBase(instance, "action_history", authenticatedSession);
                var owner = itemId;
                if (a.ActionHistoryItemColumnId > 0) {
                    var ic = new ItemColumns(instance, "", authenticatedSession);
                    var c = ic.GetItemColumn(a.ActionHistoryItemColumnId);
                    owner = oldData.ContainsKey(c.Name) ? Conv.ToInt(oldData[c.Name]) :
                        data.ContainsKey(c.Name) ? Conv.ToInt(data[c.Name]) : itemId;
                }

                a.ActionHistoryText = states.ReplaceTags(process, ib.GetRow(itemId).Data, a.ActionHistoryText);

                var ahData = new Dictionary<string, object> {
                    { "owner_item_id", owner },
                    { "parent_item_id", owner },
                    { "action_id", a.ProcessActionId },
                    { "action_json", a.ActionHistoryText }
                };

                var newData = ahib.InsertItemRow(ahData, null, false);

                if (!history) {
                    history = true;
                    ExecuteActionHistoryActions(Conv.ToInt(newData["item_id"]), process);
                }
            }

            //Invalidate Server Cache
            ib.InvalidateCache(itemId);
            ib.InvalidateCache(newRowItemId);
            Cache.RemoveGroup("list_hierarchy_meta_" + process);

            //Invalidate Client Cache
            if (runSignaler) ib.ProcessBase.InvalidateClientItemAndSqCache(itemId, ib.GetRow(itemId).Data);

            //Action done
            retval.Add("newIds", newIds);
            return retval;
        }

        private Dictionary<string, object> AddToDict(Dictionary<string, object> data, string name, object val) {
            if (data.ContainsKey(name)) {
                data[name] = val;
            } else {
                data.Add(name, val);
            }

            return data;
        }

        private void ExecuteActionHistoryActions(int actionHistoryId, string process) {
            var historyActions = new ProcessActions(instance, "action_history", authenticatedSession,
                process == "action_history");

            var beforeId = Conv.ToInt(db.ExecuteScalar("SELECT new_row_action_id " +
                                                       "FROM processes where process = 'action_history'"));

            var afterId = Conv.ToInt(db.ExecuteScalar("SELECT after_new_row_action_id " +
                                                      "FROM processes where process = 'action_history'"));

            if (beforeId != 0) historyActions.DoAction(beforeId, actionHistoryId);
            if (afterId != 0) historyActions.DoAction(afterId, actionHistoryId);
        }
    }
}