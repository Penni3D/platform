﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes.actions {
	[Route("v1/settings/ProcessActions/Rows")]
	public class ProcessActionRowsRest : PageBase {
		public ProcessActionRowsRest() : base("settings") { }

        // GET: model/values
        [HttpGet("{process}/{processActionId}")]
        public List<ProcessActionRow> Get(string process, int processActionId) {
            var actions = new ProcessActionRows(instance, process, currentSession);
            return actions.GetActionRows(processActionId);
        }

        [HttpGet("{process}/list_items")]
        public Dictionary<int, List<Dictionary<string, object>>> GetListItems(string process) {
            var actions = new ProcessActionRows(instance, process, currentSession);
            return actions.FillListItems(process);
        }

        [HttpGet("{process}/link_columns")]
        public List<ProcessActionRows.ActionRowLinkColumn> GetLinkColumns(string process) {
            var actions = new ProcessActionRows(instance, process, currentSession);
            return actions.GetLinkColumns(process);
        }

        [HttpPost("{process}")]
        public IActionResult Post(string process, [FromBody] ProcessActionRow processAction) {
            CheckRight("write");
            var processActions = new ProcessActionRows(instance, process, currentSession);
            if (ModelState.IsValid) {
                processActions.Add(processAction);
                return new ObjectResult(processAction);
            }

            return null;
        }

        [HttpPut("{process}")]
        public IActionResult Put(string process, [FromBody] List<ProcessActionRow> processAction) {
            CheckRight("write");
            var processActions = new ProcessActionRows(instance, process, currentSession);
            if (ModelState.IsValid) {
                foreach (var pt in processAction) {
                    processActions.Save(pt);
                }

                return new ObjectResult(processAction);
            }

            return null;
        }

        [HttpDelete("{process}/{processActionId}")]
        public IActionResult Delete(string process, string processActionId) {
            CheckRight("delete");
            var processActions = new ProcessActionRows(instance, process, currentSession);
            foreach (var id in processActionId.Split(',')) {
                processActions.Delete(Int32.Parse(id));
            }

            return new StatusCodeResult(200);
        }

        //Group ID  kamat
        [HttpGet("{process}/{processActionId}/rowsGroups")]
        public List<ProcessActionRowGroup> GetGroups(string process, int processActionId) {
            var processActionGroups = new ProcessActionRowGroups(instance, process, currentSession);
            return processActionGroups.GetActionRowsGroups(processActionId);
        }

        [HttpPost("{process}/group")]
        public IActionResult PostGroup(string process, [FromBody] ProcessActionRowGroup processActionRowGroup) {
            CheckRight("write");
            var processActionRowGroups = new ProcessActionRowGroups(instance, process, currentSession);
            if (ModelState.IsValid) {
                processActionRowGroups.AddProcessActionRowGroup(process, processActionRowGroup);
                return new ObjectResult(processActionRowGroup);
            }

            return null;
        }

        [HttpPut("{process}/groupUpdate")]
        public IActionResult Put(string process, [FromBody] List<ProcessActionRowGroup> processActionRowGroup) {
            CheckRight("write");
            var processActionRowGroups = new ProcessActionRowGroups(instance, process, currentSession);
            if (ModelState.IsValid) {
                processActionRowGroups.UpdateProcessActionRowGroups(process, processActionRowGroup);
                return new ObjectResult(processActionRowGroup);
            }

            return null;
        }

        [HttpDelete("{process}/{processActionGroupId}/groupDelete")]
        public IActionResult DeleteGroup(string process, int processActionGroupId) {
            CheckRight("delete");
            var processActionRowGroups = new ProcessActionRowGroups(instance, process, currentSession);
            processActionRowGroups.DeleteProcessActionRowGroup(processActionGroupId);
            return new StatusCodeResult(200);
        }
    }
}