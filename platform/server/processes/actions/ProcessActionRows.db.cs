﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Keto5.x.platform.server.processes.actions {
public class ProcessActionRow {
    public ProcessActionRow() {
    }
    public ProcessActionRow(DataRow dr, AuthenticatedSession session, string alias = "") {
        if (alias.Length > 0) {
            alias = alias + "_";
        }

        ProcessActionRowId = (int) dr[alias + "process_action_row_id"];
        ProcessActionId = (int) dr[alias + "process_action_id"];
        ProcessActionGroupId = Conv.ToInt(dr[alias + "action_group_id"]);
        ProcessActionType = (byte) dr[alias + "process_action_type"];


        if (!DBNull.Value.Equals(dr[alias + "item_column_id"])) {
            ItemColumnId = (int) dr[alias + "item_column_id"];
        }
        else {
            ItemColumnId = null;
        }

        if (!DBNull.Value.Equals(dr[alias + "value_type"])) {
            ValueType = (int) dr[alias + "value_type"];
        }
        else {
            ValueType = null;
        }

        Value = (string) Conv.IfDbNullThenNull(dr[alias + "value"]);
        if (!DBNull.Value.Equals(dr[alias + "user_item_column_id"])) {
            UserItemColumnId = (int) dr[alias + "user_item_column_id"];
        }
        else {
            UserItemColumnId = null;
        }

        if (!DBNull.Value.Equals(dr[alias + "link_item_column_id"])) {
            LinkItemColumnId = (int) dr[alias + "link_item_column_id"];
        }
        else {
            LinkItemColumnId = null;
        }

        Value2 = (string) Conv.IfDbNullThenNull(dr[alias + "value2"]);
        Value3 = (string) Conv.IfDbNullThenNull(dr[alias + "value3"]);
        if (!DBNull.Value.Equals(dr[alias + "condition_id"])) {
            ConditionId = Conv.ToInt(dr[alias + "condition_id"]);
        }
        else {
            ConditionId = null;
        }
        if (!DBNull.Value.Equals(dr[alias + "split_options"])) {
            ActionRowEmailViewOptions = new ActionRowEmailViewOptions(Conv.ToStr(dr[alias + "split_options"]));
        } else {
            ActionRowEmailViewOptions = null;
        }

        NotificationType = Conv.ToInt(dr[alias + "notification_type"]);
        ExcludeSender = Conv.ToInt(dr[alias + "exclude_sender"]);
    }

    public int ProcessActionRowId { get; set; }
    public int ProcessActionId { get; set; }
    public int ProcessActionGroupId { get; set; }
    public byte ProcessActionType { get; set; }
    public int? ItemColumnId { get; set; }
    public int? ValueType { get; set; }
    public string Value { get; set; }
    public int? UserItemColumnId { get; set; }
    public int? LinkItemColumnId { get; set; }
    public string Value2 { get; set; }
    public string Value3 { get; set; }
    public List<string> ItemColumnIds { get; set; }
    public List<int> UserGroupIds { get; set; }
    public int? ConditionId { get; set; }

    public List<int> ListItemColumnIds { get; set; }

    public String ListProcess { get; set; }

    public String OrderNo { get; set; }
    
    public ActionRowEmailViewOptions ActionRowEmailViewOptions { get; set; }
    public int NotificationType { get; set; }

    public int ExcludeSender { get; set; }
}

public class ActionRowEmailViewOptions
{

    public ActionRowEmailViewOptions() {

    }

    public ActionRowEmailViewOptions(string optionsAsJsonString) {
        JObject jObject = JObject.Parse(optionsAsJsonString);
        UseDefaultState = Conv.ToBool(jObject["UseDefaultState"]);
        DefaultState = Conv.ToStr(jObject["DefaultState"]);
        OpenSplit = Conv.ToStr(jObject["OpenSplit"]);
        OpenWidth = Conv.ToStr(jObject["OpenWidth"]);
        OpenRowItemColumnId = Conv.ToInt(jObject["OpenRowItemColumnId"]);
        OpenRowProcess = Conv.ToStr(jObject["OpenRowProcess"]);
        SubDefaultState = Conv.ToStr(jObject["SubDefaultState"]);
        AlsoOpenCurrent = Conv.ToBool(jObject["AlsoOpenCurrent"]);
        ParentItemId = Conv.ToInt(jObject["ParentItemId"]);
    }

    public bool UseDefaultState { get; set; }
    public string DefaultState { get; set; }
    public string OpenSplit { get; set; }
    public string OpenWidth { get; set; }
    public int OpenRowItemColumnId { get; set; }
    public string OpenRowProcess { get; set; }
    public string SubDefaultState { get; set; }
    public bool AlsoOpenCurrent { get; set; }
    public int ParentItemId { get; set; }
}


public class ProcessActionRowGroup {
    public ProcessActionRowGroup() {
    }

    public ProcessActionRowGroup(DataRow dr, AuthenticatedSession session, string alias = "") {
        if (alias.Length > 0) {
            alias = alias + "_";
        }

        ProcessActionGroupId = (int) dr[alias + "process_action_group_id"];
        ProcessActionId = (int) dr[alias + "process_action_id"];
        Instance = (string) dr[alias + "instance"];
        Process = (string) dr[alias + "process"];
        if (!DBNull.Value.Equals(dr[alias + "variable"])) {
            Variable = (string) dr[alias + "variable"];
        }
        else {
            Variable = null;
        }

        if (!DBNull.Value.Equals(dr[alias + "process_in_group"])) {
            ProcessInGroup = (string) dr[alias + "process_in_group"];
        }
        else {
            ProcessInGroup = null;
        }

        if (!DBNull.Value.Equals(dr[alias + "in_use"])) {
            InUse = (int) dr[alias + "in_use"];
        }
        else {
            InUse = 0;
        }

        OrderNo = (string) dr[alias + "order_no"];
        Type = (int) dr[alias + "type"];
        LanguageTranslation = new Translations(session.instance, session).GetTranslations(Variable);
    }

    // if (!DBNull.Value.Equals(dr[alias + "link_item_column_id"])) {
    // 	LinkItemColumnId = (int)dr[alias + "link_item_column_id"];
    // } else {
    // 	LinkItemColumnId = null;
    // }

    public int ProcessActionGroupId { get; set; }
    public int ProcessActionId { get; set; }
    public int Type { get; set; }
    public String Instance { get; set; }
    public String Process { get; set; }
    public String Variable { get; set; }
    public String ProcessInGroup { get; set; }
    public int InUse { get; set; }
    public String OrderNo { get; set; }

    public Dictionary<string, string> LanguageTranslation { get; set; }
}


public class ProcessActionRows : ProcessBase {
    public ProcessActionRows(string instance, string process, AuthenticatedSession authenticatedSession) : base(
        instance, process, authenticatedSession) {
    }

    public Dictionary<int, List<Dictionary<string, object>>> FillListItems(string process) {

        SetDBParam(SqlDbType.NVarChar, "@process", process);
        var result = new Dictionary<int, List<Dictionary<string, object>>>();
        var cache = new Dictionary<string,List<Dictionary<string, object>>>();


        foreach (DataRow row in db.GetDatatable(
            "SELECT process_action_row_id, par.item_column_id, ic.data_type, ic.data_additional FROM process_action_rows par " +
        "INNER JOIN item_columns ic ON ic.item_column_id = par.item_column_id " +
        "INNER JOIN process_actions pa ON pa.process_action_id = par.process_action_id " +
        "WHERE data_type = 6 AND pa.process = @process", DBParameters).Rows) {


            if (!cache.ContainsKey(Conv.ToStr(row["data_additional"]))) {

                var itemCollection =
                    new ItemCollectionService(instance, Conv.ToStr(row["data_additional"]), authenticatedSession);
                itemCollection.FillItems();

                var items = itemCollection.GetItems();

                result.Add(Conv.ToInt(row["process_action_row_id"]), items);
                cache[Conv.ToStr(row["data_additional"])] = items;
            } else {
                result.Add(Conv.ToInt(row["process_action_row_id"]), cache[Conv.ToStr(row["data_additional"])]);
            }
        }
        return result;
    }

    public List<ProcessActionRow> GetActionRows(int processActionId) {
        var result = new List<ProcessActionRow>();
        SetDBParam(SqlDbType.Int, "@processActionId", processActionId);
        foreach (DataRow row in db.GetDatatable(
            "select * from process_action_rows par " +
                   " left join process_action_rows_groups parg on par.action_group_id = parg.process_action_group_id" +
                   " where par.process_action_id = @processActionId " + 
                   " ORDER BY parg.order_no  ASC, process_action_row_id ASC",
            DBParameters).Rows) {
            var action = new ProcessActionRow(row, authenticatedSession);

            action.ItemColumnIds = new List<string>();
            action.UserGroupIds = new List<int>();
            action.ListItemColumnIds = new List<int>();

            SetDBParam(SqlDbType.Int, "@processActionRowId", action.ProcessActionRowId);
            foreach (DataRow row2 in db
                .GetDatatable(
                    "SELECT item_column_id, list_process, list_item_column_id, user_group_item_id, list_process FROM notification_receivers WHERE process_action_row_id = @processActionRowId",
                    DBParameters).Rows) {
                if (!DBNull.Value.Equals(row2["item_column_id"])) {
                    action.ItemColumnIds.Add(Conv.ToStr(row2["item_column_id"]));
                }

                if (!DBNull.Value.Equals(row2["user_group_item_id"])) {
                    action.UserGroupIds.Add(Conv.ToInt(row2["user_group_item_id"]));
                }

                if (!DBNull.Value.Equals(row2["list_item_column_id"])) {
                    action.ListItemColumnIds.Add(Conv.ToInt(row2["list_item_column_id"]));
                    action.ListProcess = Conv.ToStr(row2["list_process"]);
                }
            }

            result.Add(action);
        }

        return result;
    }

    public ProcessActionRow GetActionRow(int processActionRowId) {
        SetDBParam(SqlDbType.Int, "@processActionRowId", processActionRowId);
        var dt = db.GetDatatable(
            "SELECT process_action_row_id, process_action_id, process_action_type, item_column_id, value_type, action_group_id, value, user_item_column_id, link_item_column_id, value2, value3, condition_id FROM process_action_rows WHERE process_action_row_id = @processActionRowId",
            DBParameters);

        if (dt.Rows.Count > 0) {
            var action = new ProcessActionRow(dt.Rows[0], authenticatedSession);

            action.ItemColumnIds = new List<string>();
            action.UserGroupIds = new List<int>();
            SetDBParam(SqlDbType.Int, "@processActionRowId", action.ProcessActionRowId);
            foreach (DataRow row2 in db
                .GetDatatable(
                    "SELECT item_column_id, list_process, list_item_column_id, user_group_item_id, list_process FROM notification_receivers WHERE process_action_row_id = @processActionRowId",
                    DBParameters).Rows) {
                if (!DBNull.Value.Equals(row2["item_column_id"])) {
                    if (row2["list_item_column_id"] != DBNull.Value) {
                        action.ListItemColumnIds.Add(Conv.ToInt(row2["list_item_column_id"]));
                    }
                    else {
                        action.ItemColumnIds.Add(Conv.ToStr(row2["item_column_id"]));
                    }
                }

                if (!DBNull.Value.Equals(row2["user_group_item_id"])) {
                    action.UserGroupIds.Add(Conv.ToInt(row2["user_group_item_id"]));
                }
            }

            return action;
        }

        throw new CustomArgumentException(
            "Action Row cannot be found using process_action_row_id " + processActionRowId);
    }

    public void Delete(int processActionRowId) {
        SetDBParam(SqlDbType.Int, "@processActionRowId", processActionRowId);
        db.ExecuteDeleteQuery("DELETE FROM process_action_rows WHERE process_action_row_id = @processActionRowId",
            DBParameters);
    }

    public ProcessActionRow Save(ProcessActionRow processActionRow) {
        SetDBParam(SqlDbType.Int, "@processActionRowId", processActionRow.ProcessActionRowId);
        SetDBParam(SqlDbType.Int, "@processActionId", processActionRow.ProcessActionId);
        SetDBParam(SqlDbType.TinyInt, "@processActionType", processActionRow.ProcessActionType);
        SetDBParam(SqlDbType.Int, "@itemColumnId", Conv.IfNullThenDbNull(processActionRow.ItemColumnId));
        SetDBParam(SqlDbType.Int, "@valueType", Conv.IfNullThenDbNull(processActionRow.ValueType));
        SetDBParam(SqlDbType.NVarChar, "@value", Conv.ToStr(processActionRow.Value));
        SetDBParam(SqlDbType.Int, "@userItemColumnId", Conv.IfNullThenDbNull(processActionRow.UserItemColumnId));
        SetDBParam(SqlDbType.Int, "@linkItemColumnId", Conv.IfNullThenDbNull(processActionRow.LinkItemColumnId));
        SetDBParam(SqlDbType.NVarChar, "@value2", Conv.ToStr(processActionRow.Value2));
        SetDBParam(SqlDbType.NVarChar, "@value3", Conv.ToStr(processActionRow.Value3));
        SetDBParam(SqlDbType.Int, "@conditionId", Conv.IfNullThenDbNull(processActionRow.ConditionId));
        SetDBParam(SqlDbType.Int, "@notificationType", Conv.IfNullThenDbNull(processActionRow.NotificationType));
        SetDBParam(SqlDbType.Int, "@es", Conv.IfNullThenDbNull(processActionRow.ExcludeSender));

        if (processActionRow.ActionRowEmailViewOptions != null) {
            var jsonString = JsonConvert.SerializeObject(processActionRow.ActionRowEmailViewOptions);
            SetDBParam(SqlDbType.NVarChar, "@split_options", jsonString);
            db.ExecuteUpdateQuery("UPDATE process_action_rows SET process_action_type = @processActionType, item_column_id = @itemColumnId, value_type = @valueType, value = @value, user_item_column_id = @userItemColumnId, link_item_column_id = @linkItemColumnId, value2 = @value2, value3 = @value3 , condition_id = @conditionId, split_options = @split_options, notification_type = @notificationType, exclude_sender = @es WHERE process_action_row_id = @processActionRowId", DBParameters);
        } else {
            db.ExecuteUpdateQuery("UPDATE process_action_rows SET process_action_type = @processActionType, item_column_id = @itemColumnId, value_type = @valueType, value = @value, user_item_column_id = @userItemColumnId, link_item_column_id = @linkItemColumnId, value2 = @value2, value3 = @value3 , condition_id = @conditionId, notification_type = @notificationType, exclude_sender = @es WHERE process_action_row_id = @processActionRowId", DBParameters);
        }
        
        // db.ExecuteUpdateQuery(
        //     "UPDATE process_action_rows SET process_action_type = @processActionType, item_column_id = @itemColumnId, value_type = @valueType, value = @value, user_item_column_id = @userItemColumnId, link_item_column_id = @linkItemColumnId, value2 = @value2, value3 = @value3 , condition_id = @conditionId WHERE process_action_row_id = @processActionRowId",
        //     DBParameters);

        db.ExecuteDeleteQuery("DELETE FROM notification_receivers WHERE process_action_row_id = @processActionRowId",
            DBParameters);
        if (processActionRow.ItemColumnIds != null) {
            foreach (var id in processActionRow.ItemColumnIds) {
                if (id.Contains(";")) {
                    SetDBParam(SqlDbType.Int, "@itemColumnId", id.Split(';')[0]);
                    SetDBParam(SqlDbType.NVarChar, "@listProcess", id.Split(';')[1]);
                    SetDBParam(SqlDbType.Int, "@listItemColumnId", id.Split(';')[2]);
                }
                else {
                    SetDBParam(SqlDbType.Int, "@itemColumnId", id);
                    SetDBParam(SqlDbType.NVarChar, "@listProcess", DBNull.Value);
                    SetDBParam(SqlDbType.Int, "@listItemColumnId", DBNull.Value);
                }

                db.ExecuteInsertQuery(
                    "INSERT INTO notification_receivers (process_action_row_id, item_column_id, list_process, list_item_column_id) VALUES (@processActionRowId, @itemColumnId, @listProcess, @listItemColumnId)",
                    DBParameters);
            }
        }

        if (processActionRow.UserGroupIds != null) {
            foreach (var id in processActionRow.UserGroupIds) {
                SetDBParam(SqlDbType.Int, "@userGroupItemId", id);
                db.ExecuteInsertQuery(
                    "INSERT INTO notification_receivers (process_action_row_id, user_group_item_id) VALUES (@processActionRowId, @userGroupItemId)",
                    DBParameters);
            }
        }

        if (processActionRow.ListItemColumnIds != null) {
            SetDBParam(SqlDbType.NVarChar, "@listProcess", Conv.IfNullThenDbNull(processActionRow.ListProcess));

            foreach (var id2 in processActionRow.ListItemColumnIds) {
                SetDBParam(SqlDbType.Int, "@listItemId", id2);
                db.ExecuteInsertQuery(
                    "INSERT INTO notification_receivers (process_action_row_id, list_item_column_id, list_process) VALUES (@processActionRowId, @listItemId, @listProcess)",
                    DBParameters);
            }
        }

        return processActionRow;
    }

    public ProcessActionRow Add(ProcessActionRow processActionRow) {
        SetDBParam(SqlDbType.Int, "@processActionId", processActionRow.ProcessActionId);
        SetDBParam(SqlDbType.TinyInt, "@processActionType", processActionRow.ProcessActionType);
        SetDBParam(SqlDbType.Int, "@processActionGroupId", processActionRow.ProcessActionGroupId);

        processActionRow.ProcessActionRowId = db.ExecuteInsertQuery(
            "INSERT INTO process_action_rows (process_action_id, process_action_type) VALUES(@processActionId, @processActionType)",
            DBParameters);
        SetDBParam(SqlDbType.Int, "@processActionRowId", processActionRow.ProcessActionRowId);
        db.ExecuteUpdateQuery(
            "UPDATE process_action_rows SET action_group_id = @processActionGroupId WHERE process_action_row_id = @processActionRowId",
            DBParameters);
        db.ExecuteDeleteQuery("DELETE FROM notification_receivers WHERE process_action_row_id = @processActionRowId",
            DBParameters);
        if (processActionRow.ItemColumnIds != null) {
            foreach (var id in processActionRow.ItemColumnIds) {
                if (id.Contains(";")) {
                    SetDBParam(SqlDbType.Int, "@itemColumnId", id.Split(';')[0]);
                    SetDBParam(SqlDbType.NVarChar, "@listProcess", id.Split(';')[1]);
                    SetDBParam(SqlDbType.Int, "@listItemColumnId", id.Split(';')[2]);
                }
                else {
                    SetDBParam(SqlDbType.Int, "@itemColumnId", id);
                    SetDBParam(SqlDbType.NVarChar, "@listProcess", DBNull.Value);
                    SetDBParam(SqlDbType.Int, "@listItemColumnId", DBNull.Value);
                }

                db.ExecuteInsertQuery(
                    "INSERT INTO notification_receivers (process_action_row_id, item_column_id, list_process, list_item_column_id) VALUES (@processActionRowId, @itemColumnId, @listProcess, @listItemColumnId)",
                    DBParameters);
            }
        }

        if (processActionRow.UserGroupIds != null) {
            foreach (var id in processActionRow.UserGroupIds) {
                SetDBParam(SqlDbType.Int, "@userGroupItemId", id);
                db.ExecuteInsertQuery(
                    "INSERT INTO notification_receivers (process_action_row_id, user_group_item_id) VALUES (@processActionRowId, @userGroupItemId)",
                    DBParameters);
            }
        }

        if (processActionRow.ListItemColumnIds != null) {
            foreach (var id2 in processActionRow.ListItemColumnIds) {
                SetDBParam(SqlDbType.Int, "@listItemId", id2);
                db.ExecuteInsertQuery(
                    "INSERT INTO notification_receivers (process_action_row_id, list_item_column_id, list_process) VALUES (@processActionRowId, @listItemId, @listProcess)",
                    DBParameters);
            }
        }

        return processActionRow;
    }

    public List<ActionRowLinkColumn> GetLinkColumns(string process) {
        var result = new List<ActionRowLinkColumn>();
        var Cb = new ConnectionBase(instance, authenticatedSession);
        var spSql =
            "SELECT item_column_id, ic.process + ' : ' + coalesce(translation, ic.name) AS result FROM item_columns ic left join process_translations pt ON ic.variable = pt.variable and code = '" +
            authenticatedSession.locale_id + "' WHERE ((ic.process = '" + Conv.ToSql(process) +
            "' OR data_additional = '" + Conv.ToSql(process) +
            "' ) AND (data_type =5 OR data_type = 6)) OR ( data_type IN (0,1) AND name like '%item_id%' )  ";
        foreach (DataRow r in Cb.db.GetDatatable(spSql).Rows) {
            result.Add(new ActionRowLinkColumn(r));
        }
        return result;
    }

    public class ActionRowLinkColumn
    {
        public ActionRowLinkColumn(){}

        public ActionRowLinkColumn(DataRow r) {
            ItemColumnId = Conv.ToInt(r["item_column_id"]);
            Name = Conv.ToStr(r["result"]);
        }

        public int ItemColumnId { get; set; }
        public string Name { get; set; }

    }
}


public class ProcessActionRowGroups : ProcessBase {
    public ProcessActionRowGroups(string instance, string process, AuthenticatedSession authenticatedSession) : base(
        instance, process, authenticatedSession) {
    }


    public List<ProcessActionRowGroup> GetActionRowsGroups(int processActionId) {
        var result = new List<ProcessActionRowGroup>();
        SetDBParam(SqlDbType.NVarChar, "@process", process);
        SetDBParam(SqlDbType.NVarChar, "@instance", instance);
        SetDBParam(SqlDbType.Int, "@process_action_id", processActionId);
        
        var sql = " SELECT parg.process_action_group_id, parg.type, parg.instance, parg.process, parg.variable, parg.process_in_group, parg.in_use, parg.order_no, par.process_action_id FROM process_action_rows_groups parg  " + 
                  " left join process_action_rows par on par.action_group_id = parg.process_action_group_id  " + 
                  " where process = @process AND instance = @instance AND par.process_action_id = @process_action_id " + 
                  " GROUP BY parg.process_action_group_id, parg.type, parg.instance, parg.process, parg.variable, parg.process_in_group, parg.in_use, parg.order_no, par.process_action_id ";

        foreach (DataRow groupRow in db.GetDatatable(sql, DBParameters).Rows) {
            var dataToAdd = new ProcessActionRowGroup(groupRow, authenticatedSession);
            result.Add(dataToAdd);
        }

        return result;
    }


    public ProcessActionRowGroup AddProcessActionRowGroup(string process, ProcessActionRowGroup processActionRowGroup) {
        //adds a new action group.
        SetDBParam(SqlDbType.NVarChar, "@instance", instance);
        SetDBParam(SqlDbType.NVarChar, "@process", process);
        SetDBParam(SqlDbType.Int, "@type", processActionRowGroup.Type);
        SetDBParam(SqlDbType.Int, "@process_action_id", processActionRowGroup.ProcessActionId);
        SetDBParam(SqlDbType.NVarChar, "@order_no", processActionRowGroup.OrderNo);
        SetDBParam(SqlDbType.NVarChar, "@process_in_group",
            Conv.IfNullThenDbNull(processActionRowGroup.ProcessInGroup));
        SetDBParam(SqlDbType.Int, "@in_use", Conv.IfNullThenDbNull(processActionRowGroup.InUse));


        processActionRowGroup.ProcessActionGroupId = db.ExecuteInsertQuery(
            "INSERT INTO process_action_rows_groups (type, instance, process, order_no, process_in_group, in_use) VALUES (@type, @instance, @process, @order_no, @process_in_group, 1)",
            DBParameters);

        SetDBParam(SqlDbType.Int, "@process_action_group_id", processActionRowGroup.ProcessActionGroupId);


        if (processActionRowGroup.LanguageTranslation[authenticatedSession.locale_id] != null) {
            var variable = translations.GetTranslationVariable(process,
                processActionRowGroup.LanguageTranslation[authenticatedSession.locale_id], "PARG");
            processActionRowGroup.Variable = variable;

            SetDBParam(SqlDbType.NVarChar, "@variable", Conv.IfNullThenDbNull(processActionRowGroup.Variable));
            db.ExecuteUpdateQuery(
                "UPDATE process_action_rows_groups SET variable = @variable WHERE instance = @instance AND process = @process AND process_action_group_id = @process_action_group_id",
                DBParameters);
            foreach (var translation in processActionRowGroup.LanguageTranslation) {
                translations.insertOrUpdateProcessTranslation(process, variable, translation.Value,
                    translation.Key);
            }
        }

        return processActionRowGroup;
    }

    public void UpdateProcessActionRowGroups(string process, List<ProcessActionRowGroup> processActionRowGroup) {
        //Used for changing the translation of the action groups
        foreach (var item in processActionRowGroup) {
            if (!String.IsNullOrEmpty(item.ProcessInGroup)) {
                SetDBParam(SqlDbType.NVarChar, "@process_in_group", item.ProcessInGroup);
            }
            else {
                SetDBParam(SqlDbType.NVarChar, "@process_in_group", DBNull.Value);
            }

            SetDBParam(SqlDbType.Int, "@process_action_group_id", item.ProcessActionGroupId);
            SetDBParam(SqlDbType.NVarChar, "@order_no", item.OrderNo);

            foreach (var translation in item.LanguageTranslation) {
                translations.insertOrUpdateProcessTranslation(process, item.Variable, translation.Value,
                    translation.Key);
            }


            if (!String.IsNullOrEmpty(item.ProcessInGroup)) {
                db.ExecuteUpdateQuery(
                    "UPDATE process_action_rows_groups SET process_in_group = @process_in_group, order_no = @order_no WHERE process_action_group_id = @process_action_group_id",
                    DBParameters);
            }
            else {
                db.ExecuteUpdateQuery(
                    "UPDATE process_action_rows_groups SET order_no = @order_no  WHERE process_action_group_id = @process_action_group_id",
                    DBParameters);
            }
        }
    }

    public void DeleteProcessActionRowGroup(int processGroupId) {
        SetDBParam(SqlDbType.Int, "@process_action_group_id", processGroupId);

        db.ExecuteDeleteQuery(
            "DELETE FROM process_action_rows_groups WHERE process_action_group_id = @process_action_group_id;" +
            "DELETE FROM process_action_rows WHERE action_group_id = @process_action_group_id ",
            DBParameters);
    }
}
}