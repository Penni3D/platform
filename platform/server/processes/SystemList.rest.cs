﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.processes {

	[Route("v1/meta/SystemList")]
	public class SystemListRest : RestBase {
		[HttpGet("")]
		public IEnumerable<Dictionary<string, object>> Get() {
			return new SystemList(instance, currentSession).GetLists();
		}

		[HttpGet("{systemList}")]
		public IEnumerable<Dictionary<string, object>> Get(string systemList) {
			return new SystemList(instance, currentSession).GetListItems(systemList);
		}
	}
}