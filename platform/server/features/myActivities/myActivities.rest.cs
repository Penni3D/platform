﻿using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.myActivities {

	[Route("v1/core/[controller]")]
	public class myActivities : RestBase {
		[HttpGet("")]
		public APIResult GetData() {
			var isd = new myActivitiesData(instance, "task", currentSession);
			APIResult.data = isd.GetData();
			return APIResult;
		}

		[HttpGet("allocation")]
		public APIResult GetAllocationData() {
			var isd = new myActivitiesData(instance, "task", currentSession);
			APIResult.data = isd.GetAllocationData();
			return APIResult;
		}
	}
}
