﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.core.resourceManagement;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.features.myActivities {
	public class myActivitiesData : ConnectionBase {
		private readonly AuthenticatedSession session;

		public myActivitiesData(string instance, string process, AuthenticatedSession session) : base(instance,
			session) {
			this.instance = instance;
			this.session = session;
		}

		public Dictionary<string, object> GetAllocationData() {
			var returnObject = new Dictionary<string, object>();
			var itemBaseFactory = new EntityServiceProvider(instance, session);
			var repository = new ResourceManagementRepository(this, itemBaseFactory);

			//Get data for allocation request approval
			var allocationService = new ResourceManagementQueryService(instance, session, repository);
			var approvableRequests = allocationService.GetDataForActivitiesWidget();
			returnObject.Add("approvableRequests", approvableRequests);


			var result = new Dictionary<string, object>();
			result.Add("approvableRequests", approvableRequests);
			return result;

		}

		public Dictionary<string, object> GetData() {
		
			SetDBParam(SqlDbType.Int, "@item_id", session);
			var taskData =
				"  SELECT task.item_id, title, task.parent_item_id, task.owner_item_id, task.owner_item_type, start_date, end_date, idps.selected_item_id, task_type, i.process AS owner_process, '' AS owner_name FROM _" +
				instance + "_task  task " +
				"INNER JOIN item_data_process_selections idps ON task.item_id = idps.item_id " +
				"INNER JOIN items i ON task.owner_item_id = i.item_id " +
				"WHERE idps.selected_item_id = @item_id AND(task_type = 1) " +
				"UNION ALL " +
				"SELECT task.item_id, (SELECT title FROM _" + instance + "_task " +
				"WHERE item_id = ( " +
				"SELECT parent_item_id FROM _" + instance + "_task " +
				"WHERE item_id = ( " +
				"SELECT parent_item_id FROM _" + instance + "_task " +
				"WHERE item_id = task.item_id))) " +
				" + ' - ' + title AS title, (SELECT parent_item_id FROM _" + instance +
				"_task WHERE item_id = task.parent_item_id) AS parent_item_id, owner_item_id, owner_item_type, start_date, end_date, idps.selected_item_id, task_type, i.process AS owner_process, '' AS owner_name FROM _" +
				instance + "_task " +
				"	task " +
				"INNER JOIN item_data_process_selections idps ON task.item_id = idps.item_id " +
				"INNER JOIN items i ON task.owner_item_id = i.item_id " +
				"WHERE idps.selected_item_id = @item_id AND(task_type = 0) ";

			var resultdata = db.GetDatatable(taskData, DBParameters);
			var p = new Processes(instance, session);
			p.AddPrimaryNameToDatatableColumn(resultdata, "owner_name", "owner_item_id", "owner_process");

			var result = new Dictionary<string, object>();
			result.Add("rowcount", db.lastRowTotal);
			result.Add("rowdata", db.GetDatatableDictionary(resultdata));
			result.Add("rowstates", null);
			return result;
		}
	}
}