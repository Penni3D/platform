﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.features.auditFeature {
	public class AuditLog : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;

		public AuditLog(string instance, AuthenticatedSession authenticatedSession) : base(instance,
			authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
		}

		public List<int> getItemsForChildProcess(string process, string parentcolumn, int parent_item_id,
			int days = -7) {
			var Result = new List<int>();
			foreach (DataRow row in db.GetDatatable("SELECT item_id FROM _" + instance + "_" + process + " WHERE " +
			                                        parentcolumn + " = " + parent_item_id).Rows) {
				Result.Add(Conv.ToInt(row["item_id"]));
			}

			return Result;
		}

		public int getCountForChildProcess(string process, string parentcolumn, int parent_item_id,
			int days = -7) {
			return Conv.ToInt(db.ExecuteScalar("SELECT COUNT(item_id) FROM _" + instance + "_" + process + " WHERE " +
			                                   parentcolumn + " = " + parent_item_id));
		}


		private bool checkExceptions(string process, string column) {
			process = process.ToLower();
			column = column.ToLower();

			if (process == "user") {
				if (column == "token") return false;
				if (column == "password_reset") return false;
			}

			if (column == "owner_item_id") return false;

			return true;
		}

		public List<AuditUser> getViewersForProcess(string process, int source_item_id) {
			var Result = new List<AuditUser>();

			var p = new Processes(instance, authenticatedSession);

			var path = "%meta/ItemsData/" + process + "/" + source_item_id;
			SetDBParam(SqlDbType.NVarChar, "@path", path);

			var sql =
				"SELECT u.first_name + ' ' + last_name AS viewer, MAX(occurence) AS occurence FROM instance_requests ir " +
				"LEFT JOIN _" + instance + "_user u ON u.item_id = ir.user_item_id " +
				"WHERE path LIKE @path  " +
				"GROUP BY u.first_name + ' ' + last_name";
			foreach (DataRow row in db.GetDatatable(sql, DBParameters).Rows) {
				var item = new AuditUser();
				item.name = Conv.ToStr(row["viewer"]);
				item.occurence = Conv.ToDateTime(row["occurence"]);
				Result.Add(item);
			}

			return Result;
		}

		public List<AuditChange> GetHighlevelChangesForProcess(string process, int sourceItemId,
			List<KeyValuePair<string, string>> supportProcesses) {
			var table = Conv.ToSql("_" + instance + "_" + process);
			var sql =
				"SELECT iq.archive_start AS last_modified_date, u.last_name + ' ' + u.first_name AS last_modified_user, iq.archive_process AS last_modified_process FROM (" +
				"SELECT archive_start, archive_userid, '" + Conv.ToSql(process) + "' AS archive_process FROM " +
				table + " WHERE item_id = @item_id ";

			foreach (var p in supportProcesses) {
				if (Conv.ToStr(p.Key) != "" && Conv.ToStr(p.Value) != "") {
					var tn = Conv.ToSql("_" + instance + "_" + p.Key);
					sql += "UNION " +
					       "SELECT archive_start, archive_userid, '" + Conv.ToSql(p.Key) +
					       "' AS archive_process FROM " + tn + " WHERE item_id = (SELECT TOP 1 item_id " +
					       "FROM " + tn + " " +
					       "WHERE " + Conv.ToSql(p.Value) + " = @item_id " +
					       "ORDER BY archive_start DESC)";
				}
			}

			sql += ") iq " +
			       "LEFT JOIN _" + instance + "_user u ON u.item_id = iq.archive_userid " +
			       "ORDER BY iq.archive_start DESC";

			var result = new List<AuditChange>();
			SetDBParam(SqlDbType.Int, "item_id", sourceItemId);
			foreach (DataRow r in db.GetDatatable(sql, DBParameters).Rows) {
				var u = new AuditChange {
					LastModifiedUser = Conv.ToStr(r["last_modified_user"]),
					LastModifiedDate = Conv.ToDateTime(r["last_modified_date"]),
					LastModifiedProcess = Conv.ToStr(r["last_modified_process"])
				};
				result.Add(u);
			}

			return result;
		}

		public int getCountForProcess(string process, int source_item_id, int days = -7) {
			var source_sql = "WHERE iq.item_id = " + source_item_id;
			if (source_item_id == 0) source_sql = "";
			var completedsql =
				"SELECT COUNT(*) " +
				" FROM (" +
				"SELECT " +
				"item_id " +
				" FROM _" + instance + "_" + process + " " +
				"UNION ALL " +
				"SELECT " +
				"item_id " +
				" FROM archive__" + instance + "_" + process + " " +
				" WHERE archive_start BETWEEN DATEADD(DAY, " + days + ", getutcdate()) AND getutcdate() " +
				") AS iq " +
				source_sql;

			return Conv.ToInt(db.ExecuteScalar(completedsql));
		}

		public List<AuditItem> getHistoryForProcess(string process, int source_item_id, int days = -7, int limit = 10) {
//			Nvarchar = 0,
//			Int = 1,
//			Float = 2,
//			Date = 3,
//			Text = 4,
//			Process = 5,x
//			List = 6,x
//			Password = 7,y
//			Button = 8,y
//			Attachment = 10,
//			Join = 11,y
//			Comment = 12,y
//			Datetime = 13,
//			Formula = 14,y
//			Link = 15,y
//			Subquery = 16,y
//			Description = 17,y
//			Portfolio = 18,y
//			Progress = 60,y
//			Milestone = 65y

			//Get information about all non process columns
			SetDBParam(SqlDbType.NVarChar, "process", process);
			var processname =
				Conv.ToStr(db.ExecuteScalar("SELECT variable FROM processes WHERE process = @process", DBParameters));

			SetDBParam(SqlDbType.NVarChar, "locale", authenticatedSession.locale_id);
			var cols = "";
			var iqcols = "";
			var colsdata =
				db.GetDatatable(
					"SELECT ic.name, pt.translation AS variable, ic.data_type FROM item_columns ic " +
					"LEFT JOIN process_translations pt ON pt.variable = ic.variable AND pt.code = @locale " +
					"WHERE ic.process = @process AND (SELECT COUNT(*) FROM process_container_columns pcc WHERE pcc.item_column_id = ic.item_column_id) > 0",
					DBParameters);
			foreach (var c in colsdata.Select("data_type IN (0,1,2,3,4)")) {
				if (!checkExceptions(process, Conv.ToStr(c["name"]))) continue;
				cols += "," + c["name"];
				iqcols += ",iq." + c["name"];
			}

			cols = cols.TrimStart(',');
			iqcols = iqcols.TrimStart(',');

			//Get process columns from history
			var pcolssql = "SELECT " +
			               "input_method, name, data_additional AS process, " +
			               "CASE WHEN input_method = 3 THEN relative_column_id ELSE item_column_id END AS item_column_id, " +
			               "CASE WHEN ic.data_type = 6 THEN 'list_item' ELSE " +
			               "(SELECT name + ',' FROM process_portfolio_columns ppc " +
			               "LEFT JOIN item_columns ON item_columns.item_column_id = ppc.item_column_id " +
			               "WHERE ppc.process_portfolio_id = pp.process_portfolio_id AND use_in_select_result = 1 FOR XML PATH('')) " +
			               "END AS cols " +
			               "FROM item_columns ic " +
			               "LEFT JOIN process_portfolios pp ON ic.data_additional = pp.process AND pp.process_portfolio_type IN (1,10) " +
			               "WHERE ic.process = @process AND data_type IN (5,6)";

			var s_sql = "";
			var a_sql = "";
			var h_sql = "";
			foreach (DataRow r in db.GetDatatable(pcolssql, DBParameters).Rows) {
				var c = Conv.ToStr(r["cols"]).TrimEnd(',').Replace(",", " + ' ' + ");

				var item_id = "item_id";
				var selected_item_id = "selected_item_id";
				if (Conv.ToInt(r["input_method"]) == 3) {
					selected_item_id = "item_id";
					item_id = "selected_item_id";
				}

				s_sql += ",(SELECT " + c + " + ',' FROM dbo.get__" + instance + "_" + r["process"] +
				         "(iq.archive_start) s WHERE item_id IN (" +
				         "SELECT item FROM dbo.DelimitedStringToTable(LEFT(iq." + r["name"] + ", NULLIF(LEN(iq." +
				         r["name"] + ")-1,-1)),',')" +
				         ") FOR XML PATH('')) AS " + r["name"];

				a_sql +=
					",(SELECT CAST(" + selected_item_id +
					" AS nvarchar) + ',' FROM item_data_process_selections WHERE item_column_id = " +
					r["item_column_id"] + " AND " + item_id + " = _" + instance + "_" + process +
					".item_id FOR XML PATH('')) AS " +
					r["name"];

//				h_sql +=
//					",(SELECT CAST(" + selected_item_id +
//					" AS nvarchar) + ',' FROM dbo.get_item_data_process_selections_audit(archive_start," +
//					r["item_column_id"] + ", " + item_id + ") WHERE item_column_id = " +
//					r["item_column_id"] + " AND " + item_id + " = archive__" + instance + "_" + process +
//					".item_id FOR XML PATH('')) AS " +
//					r["name"];


				h_sql +=
					",(  SELECT   CAST(idps_audit.selected_item_id AS nvarchar) + ','  " +
					" FROM ( " +
					"     SELECT " +
					" selected_item_id " +
					"     FROM " +
					" archive_item_data_process_selections  " +
					"     WHERE  " +
					" archive_id IN ( " +
					"    SELECT  " +
					"        MAX(archive_id)  " +
					"    FROM  " +
					" archive_item_data_process_selections  " +
					" WHERE  " +
					"     item_column_id = " + r["item_column_id"] +
					" AND item_id = " + source_item_id +
					" AND archive_start < archive__" + instance + "_" + process + ".archive_start " +
					" AND archive_end > archive__" + instance + "_" + process + ".archive_start " +
					" AND CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar) NOT IN ( " +
					"    SELECT  " +
					"        CAST([item_column_id] AS nvarchar) + CAST([item_id] AS nvarchar) + CAST([selected_item_id] AS nvarchar)  " +
					" FROM  " +
					"    item_data_process_selections  " +
					" WHERE  " +
					"    item_column_id = " + r["item_column_id"] +
					" AND archive_start < archive__" + instance + "_" + process + ".archive_start " +
					"    )  " +
					" GROUP BY  " +
					"        [item_column_id],  " +
					"    [item_id],  " +
					"    [selected_item_id] " +
					"    )  " +
					" UNION  " +
					"    SELECT  " +
					" selected_item_id  " +
					"    FROM  " +
					" item_data_process_selections  " +
					"    WHERE  " +
					" archive_start < archive__" + instance + "_" + process + ".archive_start " +
					" AND item_column_id = " + r["item_column_id"] +
					" AND item_id = " + source_item_id +
					"    ) idps_audit " +
					"    FOR XML PATH('') " +
					"    ) AS " +
					r["name"];
			}

			//Build query
			var source_sql = "WHERE iq.item_id = " + source_item_id;
			if (source_item_id == 0) source_sql = "";

			var completedsql =
				"SELECT u.first_name + ' ' + u.last_name AS archive_user, iq.item_id, iq.archive_userid, iq.archive_start, " +
				iqcols + s_sql +
				" FROM (" +
				"SELECT " +
				"item_id, archive_userid, archive_start, " +
				cols + a_sql + ", -1 as RowNumber " +
				" FROM _" + instance + "_" + process + " " +
				"UNION ALL " +
				" SELECT * FROM ( SELECT " +
				"item_id, archive_userid, archive_start, " +
				cols + h_sql + " , ROW_NUMBER() OVER (ORDER BY archive_start ASC) AS RowNumber " +
				" FROM (SELECT * FROM archive__" + instance + "_" + process + " UNION ALL SELECT 0 as archive_id, null as archive_end, 1 as archive_type,* FROM _" + instance + "_" + process + ") archive__" + instance + "_" + process + " " +
				" WHERE  item_id = " + source_item_id +
				" ) archive_trail WHERE RowNumber BETWEEN -1 AND " + limit +
				" AND archive_start BETWEEN DATEADD(DAY, " + days + ", getutcdate()) AND getutcdate() " +
				") AS iq " +
				"LEFT JOIN _" + instance + "_user u ON u.item_id = iq.archive_userid " +
				source_sql + " ORDER BY archive_start ASC";

			var Result = new List<AuditItem>();
			Dictionary<string, object> previous = null;
			foreach (var current in db.GetDatatableDictionary(completedsql)) {
				var item = new AuditItem();
				item.process = processname;
				foreach (var c in current.Keys) {
					if (c == "archive_user") {
						item.archive_user = Conv.ToStr(current[c]);
					} else if (c == "archive_start") {
						item.archive_date = Conv.ToDateTime(current[c]);
					} else if (previous == null || (!current[c].Equals(previous[c]) && c != "archive_userid")) {
						var var = c;
						var d = colsdata.Select("name = '" + Conv.Escape(c) + "'");
						if (d.Length > 0) var = Conv.ToStr(d[0]["variable"]);


						var value = Conv.ToStr(current[c]);

						if (checkDate(value)) value = ">>" + value + "<<";

						if (var != "" && Conv.ToStr(current[c]) != "" && var != "item_id" && var != "archive_userid" &&
						    var != "password" && var != "token" && var != "current_state" && var != "task_type" && var != "owner_item_id" && var != "parent_item_id")
							item.changes.Add(new KeyValuePair<string, string>(var, value));
					}
				}

				if (item.changes.Count > 0) Result.Insert(0, item);

				previous = current;
			}

			return Result;
		}

		private bool checkDate(string date) {
			try {
				var dt = DateTime.Parse(date);
				return true;
			} catch {
				return false;
			}
		}

		public class AuditItem {
			public DateTime? archive_date;
			public string archive_user;
			public List<KeyValuePair<string, string>> changes = new List<KeyValuePair<string, string>>();
			public string process;
		}

		public class AuditChange {
			public DateTime? LastModifiedDate;
			public string LastModifiedProcess;
			public string LastModifiedUser;
		}

		public class AuditUser {
			public string name;
			public DateTime? occurence;
		}
	}
}