﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.auditFeature {
	[Route("v1/features/[controller]")]
	public class AuditRest : RestBase {
		private int ItemId;

		private int Status {
			get => Conv.ToInt(Cache.Get(instance, "auditprocessstatus" + process + ItemId));
			set => Cache.Set(instance, "auditprocessstatus" + process + ItemId, Conv.ToStr(value));
		}

		private int Count {
			get => Conv.ToInt(Cache.Get(instance, "auditprocesscount" + process + ItemId));
			set => Cache.Set(instance, "auditprocesscount" + process + ItemId, Conv.ToStr(value));
		}

		[HttpGet("{process}/{item_id}")]
		public List<AuditLog.AuditUser> Get(string process, int item_id) {
			try {
				var a = new AuditLog(instance, currentSession);
				return a.getViewersForProcess(process, item_id);
			} catch (Exception e) {
				throw new CustomArgumentException(
					"Process '" + process + "' and item id '" + item_id + "' caused: " + e.Message,
					"FEATURE_AUDITLOG_ERROR");
			}
		}

		[HttpPost("{process}/{item_id}")]
		public List<AuditLog.AuditChange> GetAuditChanges(string process, int item_id, [FromBody] List<KeyValuePair<string, string>> otherProcesses) {
			try {
				var a = new AuditLog(instance, currentSession);
				return a.GetHighlevelChangesForProcess(process, item_id, otherProcesses);
			} catch (Exception e) {
				throw new CustomArgumentException(
					"Process '" + process + "' and item id '" + item_id + "' caused: " + e.Message,
					"FEATURE_AUDITLOG_ERROR");
			}
		}

		[HttpGet("status/{process}/{item_id}/{mode}")]
		public int GetStatus(string process, int item_id, int mode) {
			ItemId = item_id;
			this.process = process;

			if (mode == 0) {
				return Count;
			}

			return Status;

			//return 0;
		}

		[HttpPost("{process}/{item_id}/{days}")]
	    public List<AuditLog.AuditItem> Get(string process, int item_id, int days,
	        [FromBody] List<KeyValuePair<string, string>> otherProcesses)
	    {
	       return GetWithLimit(process, item_id, days, otherProcesses);
	    }

	    [HttpPost("{process}/{item_id}/{days}/{limit}")]
		public List<AuditLog.AuditItem> GetWithLimit(string process, int item_id, int days, 
			[FromBody] List<KeyValuePair<string, string>> otherProcesses, int limit = 10) {
			var a = new AuditLog(instance, currentSession);
			ItemId = item_id;
			this.process = process;
			var result = new List<AuditLog.AuditItem>();

			try {
				//Prepare Job
				Count = 1;
				Status = 0;
				foreach (var pro in otherProcesses) {
					if (Conv.ToStr(pro.Key) != "" && Conv.ToStr(pro.Value) != "") {
						Count += a.getCountForChildProcess(pro.Key, pro.Value, item_id, days * -1);
					}
				}

				//Add Childs
				foreach (var pro in otherProcesses) {
					if (Conv.ToStr(pro.Key) != "" && Conv.ToStr(pro.Value) != "") {
						foreach (var item in a.getItemsForChildProcess(pro.Key, pro.Value, item_id, days * -1)
						) {
							foreach (var audit in a.getHistoryForProcess(pro.Key, item)) {
								result.Add(audit);
							}

							Status += 1;
						}
					}
				}

				//Add Parent
				foreach (var item in a.getHistoryForProcess(process, item_id, days * -1, limit)) {
					result.Add(item);
				}

				Status += 1;
			} catch (Exception e) {
				throw new CustomArgumentException(
					"Process '" + process + "' and item id '" + item_id + "' caused: " + e.Message,
					"FEATURE_AUDITLOG_ERROR");
			}

			return result;
		}
	}
}