﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.features.budget {
	public class BudgetRow {
		public int item_id { get; set; }
		public string order_no { get; set; }
		public int project_item_id { get; set; }
		public string description { get; set; }
		public int? category_item_id { get; set; }

		public Dictionary<string, object> GetDictionary() {
			var dict = new Dictionary<string, object> {
				{"item_id", item_id},
				{"order_no", order_no},
				{"project_item_id", project_item_id},
				{"description", description}
			};
			if (category_item_id != null) {
				dict.Add("category_item_id", new List<int> {(int) category_item_id});
			}

			return dict;
		}
	}

	public class BudgetItemForSave {
		public decimal amount { get; set; }
		public DateTime date { get; set; }
		public List<int> category_item_id { get; set; }
		public int item_id { get; set; }
		public List<int> parent_item_id { get; set; }
		public int project_item_id { get; set; }
		public List<int> type_item_id { get; set; }

		public Dictionary<string, object> GetDictionary() {
			return new Dictionary<string, object> {
				{"item_id", item_id},
				{"amount", amount},
				{"date", date},
				{"project_item_id", project_item_id},
				{"parent_item_id", parent_item_id},
				{"category_item_id", category_item_id},
				{"type_item_id", type_item_id}
			};
		}
	}

	public class BudgetUpdateModel {
		public Dictionary<string, BudgetRow> BudgetRows { get; set; }
		public Dictionary<string, BudgetRow> YearlyBudgetRows { get; set; }
		public List<BudgetItemForSave> BudgetItems { get; set; }
	}


	public class BudgetDbService : ConnectionBase {
		private readonly AuthenticatedSession _authenticatedSession;

		private readonly string _budgetItemProcesName = "budget_item";
		private readonly string _budgetRowProcesName = "budget_row";

		private readonly string _itemDataProcessSelectionsTable;

		public BudgetDbService(string instance, AuthenticatedSession session, DateTime? archiveDate) : base(instance,
			session, archiveDate) {
			this.instance = instance;
			_authenticatedSession = session;
			if (archiveDate == null) {
				_itemDataProcessSelectionsTable = "item_data_process_selections";
			} else {
				var d = (DateTime) archiveDate;
				SetDBParameter("@archiveDate", d.Date);
				_itemDataProcessSelectionsTable = "get_item_data_process_selections(@archiveDate)";
			}
		}

		private string GetColumnSubQueryFor(string colName, string parentItemIdStr, bool trailingComma = false) {
			var addComma = trailingComma ? ", " : " ";

			return
				"(SELECT selected_item_id FROM " + _itemDataProcessSelectionsTable +
				" idps JOIN item_columns ic ON ic.item_column_id=idps.item_column_id WHERE ic.name='" +
				colName + "' AND item_id=" + parentItemIdStr + " ) " + colName + addComma;
		}

		public Dictionary<string, object> GetQuarterData(int projectId, int fromYear, int toYear) {
			// quarterly budget does not use budget_row list after update 14.3.2018
			var fromParam = SetDBParameter("@fromYear", new DateTime(fromYear, 1, 1));
			var toParam = SetDBParameter("@toYear", new DateTime(toYear, 12, 31, 23, 59, 59));
			var projectIdParam = SetDBParameter("@projectItemId", projectId);

			var resultDictionary = new Dictionary<string, object>();

			var itemTableName = GetProcessTableName(_budgetItemProcesName);

			var queryForSubItems = "SELECT * FROM " +
			                       "(SELECT item_id, order_no, archive_userid, archive_start, date, amount, project_item_id," +
			                       GetColumnSubQueryFor("parent_item_id", "bi.item_id", true) +
			                       GetColumnSubQueryFor("category_item_id", "bi.item_id", true) +
			                       GetColumnSubQueryFor("type_item_id", "bi.item_id", false) +
			                       "FROM " + itemTableName + " bi) q " +
			                       "WHERE project_item_id = @projectItemId";
			var subItems = db.GetDatatableDictionary(queryForSubItems, DBParameters);
			subItems.ForEach(x => {
				ToJsonArray(x, "parent_item_id");
				ToJsonArray(x, "type_item_id");
			});

			resultDictionary.Add("budgetItems", subItems);
			resultDictionary.Add("types",
				new ItemBase(instance, "list_budget_item_type", _authenticatedSession).GetAllData());
			resultDictionary.Add("categories",
				new ItemBase(instance, "list_budget_category", _authenticatedSession).GetAllData());
			return resultDictionary;
		}

		public Dictionary<string, object> GetYearlyData(int projectId, int fromYear, int? toYear = null) {
			var fromParam = SetDBParameter("@fromYear", new DateTime(fromYear, 1, 1));
			if (toYear != null) {
				var toParam = SetDBParameter("@toYear", new DateTime((int) toYear, 12, 31, 23, 59, 59));
			}

			var projectIdParam = SetDBParameter("@projectItemId", projectId);

			var resultDictionary = new Dictionary<string, object>();

			var rows = BuildSelectQuery()
				.Columns("item_id", "order_no", "project_item_id", "description",
					GetColumnSubQueryFor("category_item_id", "br.item_id"))
				.From(GetProcessTableName(_budgetRowProcesName), "br")
				.Where("project_item_id", SqlComp.EQUALS, projectIdParam)
				.Fetch();

			var wantedRowIds = rows.Select(x => Conv.ToInt(x["item_id"]));
			var rowIds = wantedRowIds as int[] ?? wantedRowIds.ToArray();
			var wantedRowIdsInString = string.Join(",", rowIds);

			if (rowIds.Any()) {
				resultDictionary.Add("budgetRows", rows);
				var itemTableName = GetProcessTableName(_budgetItemProcesName);

				var queryForSubItems = "SELECT * FROM " +
				                       "(SELECT item_id, order_no, archive_userid, archive_start, date, amount, project_item_id," +
				                       GetColumnSubQueryFor("parent_item_id", "bi.item_id", true) +
				                       GetColumnSubQueryFor("category_item_id", "bi.item_id", true) +
				                       GetColumnSubQueryFor("type_item_id", "bi.item_id") +
				                       "FROM " + itemTableName + " bi) q " +
				                       "WHERE parent_item_id in (" + wantedRowIdsInString + ")";
				var subItems = db.GetDatatableDictionary(queryForSubItems, DBParameters);
				subItems.ForEach(x => {
					ToJsonArray(x, "parent_item_id");
					ToJsonArray(x, "type_item_id");
				});
				resultDictionary.Add("budgetItems", subItems);
			} else {
				resultDictionary.Add("budgetRows", new List<object>());
				resultDictionary.Add("budgetItems", new List<Dictionary<string, object>>());
			}

			resultDictionary.Add("types",
				new ItemBase(instance, "list_budget_item_type", _authenticatedSession).GetAllData());
			resultDictionary.Add("categories",
				new ItemBase(instance, "list_budget_category", _authenticatedSession).GetAllData());
			return resultDictionary;
		}

		private void ToJsonArray(Dictionary<string, object> item, string property) {
			if (item[property] == null) return;
			item[property] = new JArray {item[property]};
		}

		public Dictionary<string, object> AddRow(BudgetRow data) {
			var ib = GetItemBaseForRowProcess();
			var d = ib.AddItem(data.GetDictionary());
			return ib.GetItem(Conv.ToInt(d["item_id"]));
		}


		public Dictionary<string, object> AddYearlyRow(BudgetRow data) {
			var ib = GetItemBaseForRowProcess();
			var d = ib.AddItem(data.GetDictionary());
			return ib.GetItem(Conv.ToInt(d["item_id"]));
		}


		private void ChangeCategoryForBudgetItems(int parentItemId, int newCategoryItemId) {
			// find all items with given parent

			var conditions = new SqlCondition("instance", SqlComp.EQUALS, SetDBParameter("@instance", instance))
				.And("process", SqlComp.EQUALS, SetDBParameter("@budgetItem", "budget_item"))
				.And("name", SqlComp.EQUALS, SetDBParameter("@parentIdField", "parent_item_id"))
				.And("idps.selected_item_id", SqlComp.EQUALS, SetDBParameter("@parentItemId", parentItemId));
			var ids = BuildSelectQuery()
				.Columns("item_id")
				.From(_itemDataProcessSelectionsTable, "idps")
				.Join(new Join("item_columns", "ic").On("ic.item_column_id", "idps.item_column_id"))
				.Where(conditions)
				.Fetch()
				.Select(x => (int) x["item_id"]).ToList();
			var entityRepository = GetItemBaseForRowItemProcess();
			ids.ForEach(x => {
				var entity = entityRepository.GetItem(x);
				entity["category_item_id"] = new List<int> {newCategoryItemId};
				entityRepository.SaveItem(entity);
			});
		}

		public BudgetUpdateModel Save(BudgetUpdateModel data) {
			var ibForQuarterRows = GetItemBaseForRowProcess();

			var ibForItems = GetItemBaseForRowItemProcess();
			if (data.BudgetRows != null) {
				var rowData = data.BudgetRows;
				foreach (var rowDataKey in rowData) {
					var received = rowDataKey.Value;
					var item = ibForQuarterRows.GetItem(received.item_id);
					if (item.ContainsKey("category_item_id") && item["category_item_id"] != null &&
					    received.category_item_id != null) {
						var existingCategory = (List<int>) item["category_item_id"];
						if (received.category_item_id != null && existingCategory.Any() &&
						    existingCategory.First() != received.category_item_id) {
							ChangeCategoryForBudgetItems(received.item_id, (int) received.category_item_id);
						}
					}

					if (received.category_item_id != null) {
						item["category_item_id"] = new List<int> {(int) received.category_item_id};
					}

					item["description"] = received.description;
					ibForQuarterRows.SaveItem(item);
				}
			}

			if (data.YearlyBudgetRows != null) {
				var rowData = data.YearlyBudgetRows;
				foreach (var rowDataKey in rowData) {
					var received = rowDataKey.Value;
					var item = ibForQuarterRows.GetItem(received.item_id);
					if (item.ContainsKey("category_item_id") && item["category_item_id"] != null &&
					    received.category_item_id != null) {
						var existingCategory = (List<int>) item["category_item_id"];
						if (received.category_item_id != null && existingCategory.Any() &&
						    existingCategory.First() != received.category_item_id) {
							ChangeCategoryForBudgetItems(received.item_id, (int) received.category_item_id);
						}
					}

					if (received.category_item_id != null) {
						item["category_item_id"] = new List<int> {(int) received.category_item_id};
					}

					item["description"] = received.description;
					ibForQuarterRows.SaveItem(item);
				}
			}

			if (data.BudgetItems != null) {
				var rowData = data.BudgetItems;
				foreach (var row in rowData) {
					if (row.item_id == 0) {
						var new_row = ibForItems.AddItem(row.GetDictionary());
						row.item_id = Conv.ToInt(new_row["item_id"]);
					} else {
						var existingItem = ibForItems.GetItem(row.item_id);
						existingItem["amount"] = row.amount;
						if (row.project_item_id > 0) {
							existingItem["project_item_id"] = row.project_item_id;
						}

						ibForItems.SaveItem(existingItem);
					}
				}
			}

			return data;
		}

		public void Delete(List<int> itemIds) {
			var ib = GetItemBaseForRowProcess();
			DeleteParentRowAndChildrenItems(itemIds, ib);
		}

		public void DeleteYearlyRows(List<int> itemIds) {
			var ib = GetItemBaseForRowProcess();
			DeleteParentRowAndChildrenItems(itemIds, ib);
		}

		private void DeleteParentRowAndChildrenItems(List<int> itemIds, ItemBase itemBase) {
			var idsString = string.Join(",", itemIds);
			var subItemIdsQuery =
				"SELECT item_id FROM " + _itemDataProcessSelectionsTable +
				" idps JOIN item_columns ic ON ic.item_column_id=idps.item_column_id WHERE process='budget_item' AND name='parent_item_id' AND selected_item_id IN (" +
				idsString + ")";
			var subItems = db.GetDatatableDictionary(subItemIdsQuery, DBParameters)
				.Select(x => Conv.ToInt(x["item_id"])).ToList();
			foreach (var itemId in itemIds) {
				itemBase.Delete(itemId);
			}

			var ibForSubItems = GetItemBaseForRowItemProcess();
			foreach (var subItem in subItems) {
				ibForSubItems.Delete(subItem);
			}
		}

		/*   PRIVATE   */

		private ItemBase GetItemBaseForRowProcess() {
			return new ItemBase(instance, _budgetRowProcesName, _authenticatedSession);
		}


		private ItemBase GetItemBaseForRowItemProcess() {
			return new ItemBase(instance, _budgetItemProcesName, _authenticatedSession);
		}

		public List<Dictionary<string, object>> GetDashboardData(int projectId, string process_name, string archiveD) {
			var archiveInUse = false;
			if (Conv.ToStr(archiveD) != "NOARCH") {
				archiveInUse = true;
			}

			// quarterly budget does not use budget_row list after update 14.3.2018
			//var fromParam = SetDBParameter("@fromYear", new DateTime(fromYear, 1, 1));
			//var toParam = SetDBParameter("@toYear", new DateTime(toYear, 12, 31, 23, 59, 59));
			//var projectIdParam = SetDBParameter("@projectItemId", projectId);

			//var resultDictionary = new Dictionary<string, object>();
			//var dataResult = "";
			var dataResult = new List<Dictionary<string, object>>();

			var itemTableName = GetProcessTableName(_budgetItemProcesName);
			var rowTableName = GetProcessTableName(_budgetRowProcesName);
			var budget_category = GetProcessTableName("list_budget_category");
			var budget_type = GetProcessTableName("list_budget_item_type");
			var parent_process_name = GetProcessTableName(process_name);


			if (archiveInUse) {
				var dataquary = "";
				dataquary += " SELECT * INTO #budget_item FROM get__" + instance + "_budget_item('" + archiveD + "')";
				dataquary += " SELECT * INTO #budget_row FROM get__" + instance + "_budget_row('" + archiveD + "')";
				dataquary += " SELECT * INTO #budget_category FROM get__" + instance + "_list_budget_category('" +
				             archiveD + "')";
				dataquary += " SELECT * INTO #item_data_process_selection FROM get_item_data_process_selections('" +
				             archiveD + "')";
				dataquary += " SELECT * INTO #budget_item_type FROM get__" + instance + "_list_budget_item_type('" +
				             archiveD + "')";

				dataquary +=
					" SELECT datatype=1, mbi.item_id,mbr.description AS row_description,mbr.item_id AS row_id, mbr.project_item_id" +
					",type_id = (SELECT idps_mbr.selected_item_id FROM #item_data_process_selection idps_mbr WHERE idps_mbr.item_id = mbi.item_id AND idps_mbr.item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_item' AND ic.name = 'type_item_id')) " +
					",type_name = (SELECT list_item FROM #budget_item_type WHERE item_id=(SELECT idps_mbr.selected_item_id FROM #item_data_process_selection idps_mbr WHERE idps_mbr.item_id = mbi.item_id AND idps_mbr.item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_item' AND ic.name = 'type_item_id'))) " +
					",mbi.date AS date,YEAR( mbi.date) AS year,MONTH(mbi.date) AS month,mbi.amount,category.income,category.list_item AS category_name,category.list_symbol_fill AS color,category.item_id AS category_id " +
					"FROM #budget_item mbi " +
					"LEFT JOIN #budget_row mbr ON " +
					"mbr.item_id = (SELECT idps_mbi.selected_item_id FROM #item_data_process_selection idps_mbi WHERE idps_mbi.item_id = mbi.item_id AND idps_mbi.item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_item' AND ic.name = 'parent_item_id')) " +
					"LEFT JOIN #budget_category category ON category.item_id = (SELECT idps_mbr.selected_item_id FROM #item_data_process_selection idps_mbr WHERE idps_mbr.item_id =  mbr.item_id AND idps_mbr.item_column_id = (SELECT ic.item_column_id FROM item_columns ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_row' AND ic.name = 'category_item_id')) " +
					"WHERE mbi.date is not null AND mbi.project_item_id = " + projectId +
					"ORDER BY mbi.date ASC ";

				dataResult = db.GetDatatableDictionary(dataquary, DBParameters);
			} else {
				var dataquary =
					"SELECT datatype=1, mbi.item_id,mbr.description AS row_description,mbr.item_id AS row_id, mbr.project_item_id" +
					",type_id = (SELECT idps_mbr.selected_item_id FROM [item_data_process_selections] idps_mbr WHERE idps_mbr.item_id = mbi.item_id AND idps_mbr.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_item' AND ic.name = 'type_item_id')) " +
					",type_name = (SELECT list_item FROM " + budget_type +
					" WHERE item_id=(SELECT idps_mbr.selected_item_id FROM [item_data_process_selections] idps_mbr WHERE idps_mbr.item_id = mbi.item_id AND idps_mbr.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_item' AND ic.name = 'type_item_id'))) " +
					",mbi.date AS date,YEAR( mbi.date) AS year,MONTH(mbi.date) AS month,mbi.amount,category.income,category.list_item AS category_name,category.list_symbol_fill AS color,category.item_id AS category_id " +
					"FROM " + itemTableName + " mbi " +
					"LEFT JOIN " + rowTableName + " mbr ON " +
					"mbr.item_id = (SELECT idps_mbi.selected_item_id FROM [item_data_process_selections] idps_mbi WHERE idps_mbi.item_id = mbi.item_id AND idps_mbi.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_item' AND ic.name = 'parent_item_id')) " +
					"LEFT JOIN " + budget_category +
					" category ON category.item_id = (SELECT idps_mbr.selected_item_id FROM [item_data_process_selections] idps_mbr WHERE idps_mbr.item_id =  mbr.item_id AND idps_mbr.item_column_id = (SELECT ic.item_column_id FROM [item_columns] ic WHERE ic.instance = '" +
					instance + "' AND ic.process = 'budget_row' AND ic.name = 'category_item_id')) " +
					"WHERE mbi.date is not null AND mbi.project_item_id = " + projectId +
					"ORDER BY mbi.date ASC ";

				dataResult = db.GetDatatableDictionary(dataquary, DBParameters);
			}

			var allConfigurations = new Configurations(instance, _authenticatedSession);
			var clientConfigurations = allConfigurations.GetClientConfigurations();
			var featureConfigIndex = clientConfigurations.FindIndex(conf =>
				conf.Name == "process.yearlyBudget" &&
				conf.Process == process_name); //Finds the index for the configuration of the feature.
			//int featureConfigIndex = clientConfigurations.FindIndex(conf => (conf.Name == "process.yearlyBudget" && conf.Process == ParentProcessName)); //Finds the index for the configuration of the feature.

			var ib = new ItemBase(instance, process_name, _authenticatedSession);

			var extraColumns = new List<string>();

			var Budgetconfig = JObject.Parse(clientConfigurations[featureConfigIndex].Value);
			var BudgetColumn = "";

			if (JObject.Parse(clientConfigurations[featureConfigIndex].Value)["extraColumn1"] != null) {
				extraColumns.Add(Conv.ToStr(Budgetconfig["extraColumn1"]));
			}

			if (JObject.Parse(clientConfigurations[featureConfigIndex].Value)["extraColumn2"] != null) {
				extraColumns.Add(Conv.ToStr(Budgetconfig["extraColumn2"]));
			}

			if (JObject.Parse(clientConfigurations[featureConfigIndex].Value)["extraColumn3"] != null) {
				extraColumns.Add(Conv.ToStr(Budgetconfig["extraColumn3"]));
			}

			if (JObject.Parse(clientConfigurations[featureConfigIndex].Value)["BudgetColumn"] != null) {
				BudgetColumn = Conv.ToStr(Budgetconfig["BudgetColumn"]);
			}

			//JObject.Parse(clientConfigurations[featureConfigIndex].Value)["ExtraColumn1"]
			if (extraColumns.Count > 0) {
				var column_quary = "SELECT * FROM [dbo].[item_columns] WHERE item_column_id IN(" +
				                   string.Join(",", extraColumns.ToArray()) + ")";

				var columnResult = db.GetDatatable(column_quary, DBParameters);

				var extras = new List<string>();
				var extras2 = new List<string>();
				var extrasType = new List<string>();

				foreach (DataRow row in columnResult.Rows) {
					extras.Add(row["name"].ToString());
					extras2.Add(row["variable"].ToString());
					extrasType.Add(row["data_type"].ToString());
				}

				//extras.Add("hyvksytyt_kustannukset_k");
				//extras.Add("ennuste_tilanneraportista");

				var first = 0;
				var first_month = 0;
				var last = 0;
				var last_month = 0;

				for (var i = 0; i < dataResult.Count; i++) {
					if (first == 0 || new DateTime(first, first_month, 1) >
						new DateTime(Conv.ToInt(dataResult[i]["year"]), Conv.ToInt(dataResult[i]["month"]), 1)) {
						first = Conv.ToInt(dataResult[i]["year"]);
						first_month = Conv.ToInt(dataResult[i]["month"]);
					}

					if (last == 0 || new DateTime(last, last_month, 1) < new DateTime(Conv.ToInt(dataResult[i]["year"]),
						Conv.ToInt(dataResult[i]["month"]), 1)) {
						if (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) >
						    new DateTime(Conv.ToInt(dataResult[i]["year"]), Conv.ToInt(dataResult[i]["month"]), 1)) {
							last = Conv.ToInt(dataResult[i]["year"]);
							last_month = Conv.ToInt(dataResult[i]["month"]);
						}
					}
				}

				if (last_month != 0 && first_month != 0) {
					if (new DateTime(last, last_month, 1) > new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)) {
						last = Conv.ToInt(DateTime.Now.Year);
						last_month = Conv.ToInt(DateTime.Now.Month);
					}

					var diff = (last - first) * 12 + (last_month - first_month) + 1;

					var dataquary2 = "";
					var tempD = new DateTime(first, 1, 1);
					var year = first;
					var month = first_month;
					//string fielName = "hyvksytyt_kustannukset_k";
					var last_round = extras.Count;
					if (Conv.ToStr(archiveD) != "NOARCH") {
						SetDBParam(SqlDbType.DateTime, "@archiveDate", DateTime.Parse(archiveD));
					} else {
						SetDBParam(SqlDbType.DateTime, "@archiveDate", DateTime.Now);
					}

					var icols = new ItemColumns(instance, process_name, _authenticatedSession);

					var columnSqlL = new List<string>();
					var columnsL = new List<ItemColumn>();
					foreach (var fieldName in extras) {
						var column = icols.GetItemColumnByName(extras[0]);
						var columnSql = extras[0];
						if (column.IsSubquery()) {
							columnSql = icols.GetSubQuery(column, archiveType: 1, withAlias: false);
							columnSqlL.Add(columnSql);
						} else {
							columnSqlL.Add(fieldName);
						}

						columnsL.Add(column);
						//if (column.IsEquation())
						//{
						//    icols.GetEquationQuery(column);
						//}
					}


					for (var i = 0; i < diff; i++) {
						var sYear = year;
						var sMonth = month;

						var tempD2 = tempD;
						tempD = tempD.AddMonths(1);
						var extraNum = 2;
						//'" + tempD.Year + "-" + tempD.Month + "-01'
						SetDBParam(SqlDbType.DateTime, "@archiveDate" + i,
							DateTime.Parse(tempD.Year + "-" + tempD.Month + "-01"));
						foreach (var fielName in extras) {
							//[ennuste_tilanneraportista]
							if (Conv.ToInt(extrasType[extras.IndexOf(fielName)]) == 16) {
								dataquary2 += " SELECT datatype=" + extraNum + ", '" + tempD2.Year + "' AS year,'" +
								              tempD2.Month + "' AS month, " + columnSqlL[extras.IndexOf(fielName)] +
								              " AS amount,'" + fielName + "' AS field_name, '" +
								              extras2[extras.IndexOf(fielName)] + "' AS variable FROM get__" +
								              instance + "_" + process_name + " ('" + tempD.Year + "-" + tempD.Month +
								              "-01') pf WHERE item_id=" + projectId;
							} else {
								dataquary2 += " SELECT datatype=" + extraNum + ", '" + tempD2.Year + "' AS year,'" +
								              tempD2.Month + "' AS month, " + columnSqlL[extras.IndexOf(fielName)] +
								              " AS amount,'" + fielName + "' AS field_name, '" +
								              extras2[extras.IndexOf(fielName)] + "' AS variable FROM get__" +
								              instance + "_" + process_name + " ('" + tempD.Year + "-" + tempD.Month +
								              "-01') pf WHERE item_id=" + projectId;
							}

							//fielName
							if (i + 1 == diff) {
								last_round -= 1;
							}

							if (i + 1 != diff || last_round > 0) {
								dataquary2 += " UNION ";
							}

							extraNum += 1;
						}
					}

					// if (extras.Count > 0) { dataquary2 += " UNION "; }
					var extraNum2 = 2;
					foreach (var fielName in extras) {
						if (archiveInUse) {
							dataquary2 += " UNION ";
							dataquary2 += " SELECT datatype = " + extraNum2 + " , 'now' AS year,'now' AS month, " +
							              columnSqlL[extras.IndexOf(fielName)] + " AS amount,'" + fielName +
							              "' AS field_name, '" + extras2[extras.IndexOf(fielName)] +
							              "' AS variable FROM get__" + instance + "_" + process_name + " ('" +
							              archiveD + "') pf WHERE item_id = " + projectId;
							extraNum2 += 1;
						} else {
							dataquary2 += " UNION ";

							if (Conv.ToInt(extrasType[extras.IndexOf(fielName)]) == 16) {
								var columnSql2 = icols.GetSubQuery(columnsL[extras.IndexOf(fielName)], false);
								dataquary2 += " SELECT datatype = " + extraNum2 + " , 'now' AS year,'now' AS month, " +
								              columnSql2 + " AS amount,'" + fielName + "' AS field_name, '" +
								              extras2[extras.IndexOf(fielName)] + "' AS variable FROM " +
								              parent_process_name + " pf WHERE item_id = " + projectId;
							} else {
								dataquary2 += " SELECT datatype = " + extraNum2 + " , 'now' AS year,'now' AS month, " +
								              columnSqlL[extras.IndexOf(fielName)] + " AS amount,'" + fielName +
								              "' AS field_name, '" + extras2[extras.IndexOf(fielName)] +
								              "' AS variable FROM " + parent_process_name + " pf WHERE item_id = " +
								              projectId;
							}

							extraNum2 += 1;
						}
					}

					var paramIndex = 0;
					dataquary2 = Regex.Replace(dataquary2, @"@archiveDate", m => "@archiveDate" + paramIndex++);
					var dataResult2 = db.GetDatatableDictionary(dataquary2, DBParameters);
					for (var i = 0; i < dataResult2.Count; i++) {
						dataResult.Add(dataResult2[i]);
					}
				}
			}

			if (archiveInUse && BudgetColumn != "") {
				var column_quary = "SELECT * FROM [dbo].[item_columns] WHERE item_column_id IN(" + BudgetColumn + ")";
				var columnResult = db.GetDataRow(column_quary, DBParameters);

				var historyRow = ib.GetArchiveItem(projectId, DateTime.Parse(archiveD));

				//string dataquary3 = " SELECT datatype = " + "-5" + " , 'barchart' AS year,'" + archiveD + "' AS month, " + columnResult["name"] + " AS amount,'" + columnResult["name"] + "' AS field_name, '" + columnResult["variable"] + "' AS variable FROM get__" + instance + "_" + process_name + "('" + archiveD + "') WHERE item_id = " + projectId;
				var dataquary3 = " SELECT datatype = " + "-5" + " , 'barchart' AS year,'" + archiveD + "' AS month, " +
				                 Conv.ToDouble(historyRow[Conv.ToStr(columnResult["name"])]) + " AS amount,'" +
				                 columnResult["name"] + "' AS field_name, '" + columnResult["variable"] +
				                 "' AS variable FROM get__" + instance + "_" + process_name + "('" + archiveD +
				                 "') WHERE item_id = " + projectId;
				var dataResult3 = db.GetDatatableDictionary(dataquary3, DBParameters);
				dataResult.Add(dataResult3[0]);
			} else if (archiveInUse == false && BudgetColumn != "") {
				var column_quary = "SELECT * FROM [dbo].[item_columns] WHERE item_column_id IN(" + BudgetColumn + ")";
				var columnResult = db.GetDataRow(column_quary, DBParameters);
				var currentRow = ib.GetItem(projectId);
				var dataquary3 = " SELECT datatype = " + "-6" + " , 'barchart' AS year,'" + DateTime.Now +
				                 "' AS month, " + Conv.ToDouble(currentRow[Conv.ToStr(columnResult["name"])]) +
				                 " AS amount,'" + columnResult["name"] + "' AS field_name, '" +
				                 columnResult["variable"] + "' AS variable FROM " + parent_process_name +
				                 " pf WHERE item_id = " + projectId;
				var dataResult3 = db.GetDatatableDictionary(dataquary3, DBParameters);
				dataResult.Add(dataResult3[0]);
			}
			//columnResult["name"]

			return dataResult;
		}
	}
}