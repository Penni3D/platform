﻿using System;
using System.Linq;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.budget {
	[Route("v1/features/[controller]")]
	public class Budget : RestBase{
		// QUARTER BUDGET DATA
		[HttpGet("quarter/{projectId}/{fromYear}/{toYear}")]
		public IActionResult GetQuarterData(int projectId, int fromYear, int toYear) {
			var data = getDbService().GetQuarterData(projectId, fromYear, toYear);	
			return new ObjectResult(data);
		}

		[HttpGet("archive/{date}/quarter/{projectId}/{fromYear}/{toYear}")]
		public IActionResult GetQuarterData(DateTime d, int projectId, int fromYear, int toYear) {
			var data = getDbService(d).GetQuarterData(projectId, fromYear, toYear);	
			return new ObjectResult(data);
		}

		// ADD NEW QUARTER BUDGET ROW
		[HttpPost("quarter")]
		public IActionResult AddRow([FromBody] BudgetRow data) {
			var added = getDbService().AddRow(data);
			return new ObjectResult(added);
		}

		// DELETE QUARTER BUDGET ROWS
		[HttpDelete("quarter/{itemIds}")]
		public IActionResult DeleteRow(string itemIds) {
			var ids = itemIds.Split("-").ToList().Select(Conv.ToInt).ToList();
			getDbService().Delete(ids);
			return new ObjectResult("");
		}

		// YEARLY BUDGET DATA
		[HttpGet("yearly/{projectId}/{fromYear}")]
		public IActionResult GetYearlyData(int projectId, int fromYear) {
			var data = getDbService().GetYearlyData(projectId, fromYear);	
			return new ObjectResult(data);
		}

		// YEARLY BUDGET DATA
		[HttpGet("yearly/{projectId}/{fromYear}/{toYear}")]
		public IActionResult GetYearlyData(int projectId, int fromYear, int toYear) {
			var data = getDbService().GetYearlyData(projectId, fromYear, toYear);	
			return new ObjectResult(data);
		}

		[HttpGet("archive/{datez}/yearly/{projectId}/{fromYear}")]
		public IActionResult GetYearlyData(DateTime datez, int projectId, int fromYear) {
			var data = getDbService(datez).GetYearlyData(projectId, fromYear);	
			return new ObjectResult(data);
		}

		[HttpGet("archive/{datez}/yearly/{projectId}/{fromYear}/{toYear}")]
		public IActionResult GetYearlyData(DateTime datez, int projectId, int fromYear, int toYear) {
			var data = getDbService(datez).GetYearlyData(projectId, fromYear, toYear);	
			return new ObjectResult(data);
		}

		// ADD NEW YEARLY BUDGET ROW
		[HttpPost("yearly")]
		public IActionResult AddYearlyRow([FromBody] BudgetRow data) {
			var added = getDbService().AddYearlyRow(data);
			return new ObjectResult(added);
		}

		// DELETE YEARLY BUDGET ROWS
		[HttpDelete("yearly/{itemIds}")]
		public IActionResult DeleteYearlyRows(string itemIds) {
			var ids = itemIds.Split("-").ToList().Select(Conv.ToInt).ToList();
			getDbService().DeleteYearlyRows(ids);
			return new ObjectResult("");
		}

		// COMMON SAVE
		[HttpPut]
		public IActionResult Save([FromBody] BudgetUpdateModel data) {			
			return new ObjectResult(getDbService().Save(data));
		}

		private BudgetDbService getDbService(DateTime? date = null) {
			return new BudgetDbService(instance, currentSession, date);
			
		}

		// BUDGET DASHBOARD DATA
		[HttpGet("dashboard/{projectId}/{process_name}/{archiveD}")]
        public IActionResult GetDashboardData(int projectId, string process_name, string archiveD)
        {
            var data = getDbService().GetDashboardData(projectId, process_name, archiveD);
            return new ObjectResult(data);
        }
	}
}