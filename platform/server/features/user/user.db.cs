﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;
using Microsoft.Data.SqlClient;

namespace Keto5.x.platform.server.features.user {
	public class UserGroupsData : ConnectionBase {
		private readonly AuthenticatedSession session;

		public UserGroupsData(string instance, string process, AuthenticatedSession session) : base(instance,
			session) {
			this.instance = instance;
			this.session = session;
		}

		public Dictionary<int, Dictionary<string, bool>> GetUserGroups(int selected_item_id) {
			var result = new Dictionary<int, Dictionary<string, bool>>();

			var ics = new ItemColumns(instance, "user_group", session);

			SetDBParam(SqlDbType.Int, "@selected_item_id", selected_item_id);
			SetDBParam(SqlDbType.Int, "@item_column_id", ics.GetItemColumnByName("user_id").ItemColumnId);

			foreach (DataRow row in db
				.GetDatatable(
					"SELECT item_id FROM item_data_process_selections WHERE selected_item_id = @selected_item_id AND item_column_id = @item_column_id",
					DBParameters).Rows) {
				var retval = new Dictionary<string, bool>();
				retval.Add("selected", true);
				result.Add(Conv.ToInt(row["item_id"]), retval);
			}

			return result;
		}

		public void AddUserGroups(int userItemId, int userGroupItemId) {
			var ib = new ItemBase(instance, "user_group", session);
			var ug = ib.GetItem(userGroupItemId);

			//Check that user has write access to user group processes user_id field (contains user item ids)
			if (!ug.ContainsKey("user_id")) {
				var exception = new CustomPermissionException("NOT_ALLOWED_TO_WRITE_TO_PROCESS",
					"Current user can't write to process user_group");
				exception.Data.Add("process", "user_group");
				throw exception;
			}

			var ug2 = new Dictionary<string, object> {{"item_id", ug["item_id"]}, {"user_id", ug["user_id"]}};

			//If user already belongs to this group -- do not save again
			if (((List<int>) ug2["user_id"]).Contains(userItemId)) return;

			//Check that if user group is an admin group and user is in admin group -- save
			var save = true;
			SetDBParam(SqlDbType.Int, "@userGroupItemId", userGroupItemId);
			SetDBParam(SqlDbType.Int, "@currentUserId", session.item_id);
			
			var admintypesql = "(SELECT TOP 1 item_id FROM _" + Conv.ToSql(instance) + "_user_group_type WHERE is_admin_group = 1)";
			const string groupitemsql = "(SELECT item_column_id FROM item_columns where process = 'user_group' AND name = 'user_id')";
			const string grouptypesql = "(SELECT item_column_id FROM item_columns where process = 'user_group' AND name = 'group_type')";

			var sql = "SELECT MAX(c) FROM (SELECT " +
			          " CASE WHEN idps.selected_item_id = " + admintypesql + " THEN 1 ELSE 0 END AS c" +
			          " FROM item_data_process_selections idps " +
			          " WHERE idps.item_column_id = " + grouptypesql + " AND idps.item_id = @userGroupItemId) query";

			var isAdminGroup = Conv.ToInt(db.ExecuteScalar(sql, DBParameters));
			
			if (isAdminGroup == 1) {
				sql = " SELECT COUNT(*) FROM ( " +
				      "       SELECT  " +
				      "               idps1.item_id AS user_group_id,  " +
				      "               idps1.selected_item_id AS user_item_id,  " +
				      "               CASE WHEN (SELECT selected_item_id FROM item_data_process_selections idps2 " +
				      "				  WHERE idps2.item_column_id = " + grouptypesql + " AND idps2.item_id = idps1.item_id) = " +
				      "				  " + admintypesql + " THEN 1 ELSE 0 END AS group_type " +
				      "       FROM item_data_process_selections idps1 " +
				      "       WHERE idps1.item_column_id = " + groupitemsql +
				      " AND idps1.selected_item_id = @currentUserId " +
				      " ) query " +
				      " WHERE group_type = 1 ";
				var userIsAdmin = Conv.ToInt(db.ExecuteScalar(sql, DBParameters));
				if (userIsAdmin == 0) save = false;
			}
			
			if (save) {
				((List<int>) ug2["user_id"]).Add(userItemId);
				ib.SaveItem(ug2, checkRights: true);
			} else {
				var exception = new CustomPermissionException("NOT_ALLOWED_TO_WRITE_TO_PROCESS",
					"Current user can't write to process user_group");
				exception.Data.Add("process", "user_group");
				throw exception;
			}
		}

		public void DeleteUserGroups(int selected_item_id, int item_id) {
			var ib = new ItemBase(instance, "user_group", session);
			var ug = ib.GetItem(item_id);

			if (!ug.ContainsKey("user_id")) {
				var exception = new CustomPermissionException("NOT_ALLOWED_TO_WRITE_TO_PROCESS",
					"Current user can't write to process user_group");
				exception.Data.Add("process", "user_group");
				throw exception;
			}

			var ug2 = new Dictionary<string, object>();
			ug2.Add("item_id", ug["item_id"]);
			ug2.Add("user_id", ug["user_id"]);

			if (((List<int>) ug2["user_id"]).Contains(selected_item_id)) {
				((List<int>) ug2["user_id"]).Remove(selected_item_id);
				ib.SaveItem(ug2, checkRights: true);
			}
		}
	}

	public class UserTwoFactorData : ConnectionBase {
		private readonly int itemId;
		private string login;
		private AuthenticatedSession session;

		public UserTwoFactorData(string instance, int itemId, AuthenticatedSession session) : base(instance,
			session) {
			this.instance = instance;
			this.session = session;
			this.itemId = itemId;
		}

		public bool UsesTwoFactor() {
			SetDBParam(SqlDbType.Int, "item_id", itemId);
			return Conv.ToBool(db.ExecuteScalar("SELECT twofactor FROM _" + instance + "_user WHERE item_id = @item_id",
				DBParameters));
		}
		
		public bool UsesKetoAuth() {
			SetDBParam(SqlDbType.Int, "item_id", itemId);
			return Conv.ToBool(db.ExecuteScalar("SELECT ketoauth FROM _" + instance + "_user WHERE item_id = @item_id",
				DBParameters));
		}

		public string GetSetupCode() {
			SetDBParam(SqlDbType.Int, "item_id", itemId);
			var twof = new TwoFactorAuthentication();

			if (login == null)
				login = Conv.ToStr(db.ExecuteScalar("SELECT login FROM _" + instance + "_user WHERE item_id = @item_id",
					DBParameters));

			return twof.GenerateSetupCode(login);
		}

		public string GetSetupQDR() {
			SetDBParam(SqlDbType.Int, "item_id", itemId);
			var twof = new TwoFactorAuthentication();

			if (login == null)
				login = Conv.ToStr(db.ExecuteScalar("SELECT login FROM _" + instance + "_user WHERE item_id = @item_id",
					DBParameters));

			return twof.GenerateSetupQDR(login);
		}

		public void ToggleTwoFactor(bool enabled) {
			SetDBParam(SqlDbType.Int, "item_id", itemId);
			SetDBParam(SqlDbType.Int, "twofactor", enabled ? 1 : 0);
			db.ExecuteUpdateQuery("UPDATE _" + instance + "_user SET twofactor = @twofactor WHERE item_id = @item_id",
				DBParameters);
		}
		
		public void ToggleKetoAuth(bool enabled) {
			SetDBParam(SqlDbType.Int, "item_id", itemId);
			SetDBParam(SqlDbType.Int, "ketoauth", enabled ? 1 : 0);
			db.ExecuteUpdateQuery("UPDATE _" + instance + "_user SET ketoauth = @ketoauth WHERE item_id = @item_id",
				DBParameters);
		}
	}
}