﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.user {
	[Route("v1/meta/[controller]")]
	public class UserGroups : RestBase {
		[HttpGet("{selectedItemId}")]
		public Dictionary<int, Dictionary<string, bool>> Get(int selectedItemId) {
			var userGroup = new UserGroupsData(instance, "user", currentSession);
			return userGroup.GetUserGroups(selectedItemId);
		}

		[HttpPost("{selectedItemId}")]
		public void Post(int selectedItemId, [FromBody] List<int> data) {
			var userGroup = new UserGroupsData(instance, "user", currentSession);
			foreach (var itemId in data) {
				userGroup.AddUserGroups(selectedItemId, itemId);
			}
		}

		[HttpDelete("{selectedItemId}/{itemId}")]
		public void Delete(int selectedItemId, string itemId) {
			var userGroup = new UserGroupsData(instance, "user", currentSession);
			foreach (var id in itemId.Split(',')) {
				userGroup.DeleteUserGroups(selectedItemId, Conv.ToInt(id));
			}
		}
	}

	[Route("v1/core/[controller]")]
	public class TwoFactor : RestBase {
		[HttpGet("{itemId}")]
		public Dictionary<string, object> Get(int itemId) {
			var result = new Dictionary<string, object>();

			var info = new UserTwoFactorData(instance, itemId, currentSession);
			var enabled = info.UsesTwoFactor();
			var code = "";
			var qdr = "";

			result.Add("enabled", enabled);
			if (enabled) {
				code = info.GetSetupCode();
				qdr = info.GetSetupQDR();
			}
			result.Add("setupCode", code);
			result.Add("setupQdr", qdr);
			result.Add("ketoAuth", info.UsesKetoAuth());

			return result;
		}

		[HttpPut("{itemId}/{enabled}")]
		public void ToggleTwoFactor(int itemId, bool enabled) {
			//TODO: Check access rights
			var info = new UserTwoFactorData(instance, itemId, currentSession);
			info.ToggleTwoFactor(enabled);
		}
		
		[HttpPut("{itemId}/ketoauth/{enabled}")]
		public void ToggleKetoAuth(int itemId, bool enabled) {
			//TODO: Check access rights
			var info = new UserTwoFactorData(instance, itemId, currentSession);
			info.ToggleKetoAuth(enabled);
		}
	}
}