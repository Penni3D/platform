﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.processes.itemColumns;

namespace Keto5.x.platform.server.features.historyEditor {
	public class HistoryEditorProcessResultModel {
		public int ArchiveId { get; set; }
		public int ArchiveType { get; set; }
		public int SelectedItemId { get; set; }
		public string Value { get; set; }
		public string TodaysValue { get; set; }
		public DateTime Date { get; set; }
		public DateTime Start { get; set; }

		public HistoryEditorProcessResultModel() { }

		public HistoryEditorProcessResultModel(DataRow r) {
			ArchiveId = Conv.ToInt(r["archive_id"]);
			ArchiveType = Conv.ToInt(r["archive_type"]);
			SelectedItemId = Conv.ToInt(r["selected_item_id"]);
			Value = Conv.ToStr(r["value"]);
			TodaysValue = Conv.ToStr(r["todays_value"]);
			Date = (DateTime) r["archive_end"];
			Start = (DateTime) r["archive_start"];
		}
	}

	public class HistoryEditor : ConnectionBase {
		private string _instance;
		private AuthenticatedSession _session;

		public HistoryEditor(string instance, AuthenticatedSession session) : base(instance, session) {
			this._instance = instance;
			this._session = session;
		}

		private void Log(int itemId, int itemColumnId, string change) {
			Diag.Log(_instance, "Audit trail altered (" + change+ "). Item Id: " + itemId + ", Item Column Id: " + itemColumnId, _session.item_id, Diag.LogSeverities.Message,
				Diag.LogCategories.System);
		}

		public void RemoveFromArchiveForList(int itemId, int itemColumnId, DateTime archiveEnd) {
			SetDBParameter("@item_id", itemId);
			SetDBParameter("@item_column_id", itemColumnId);
			SetDBParameter("@archive_end", archiveEnd);

			//Delete row
			db.ExecuteDeleteQuery(
				"DELETE FROM archive_item_data_process_selections WHERE item_id = @item_id AND item_column_id = @item_column_id AND archive_end = @archive_end",
				DBParameters);

			//Fix the gap
			var sql1 = " UPDATE archive_item_data_process_selections SET archive_end = @archive_end  " +
			           " WHERE archive_id = ( " +
			           " SELECT TOP 1 archive_id from archive_item_data_process_selections " +
			           " WHERE archive_end < @archive_end " +
			           " AND item_id = @item_id AND item_column_id = @item_column_id AND archive_type = 1 ORDER BY archive_end DESC) ";

			db.ExecuteUpdateQuery(sql1, DBParameters);

			var sql2 = " UPDATE archive_item_data_process_selections SET archive_end = @archive_end  " +
			           " WHERE archive_id = ( " +
			           " SELECT TOP 1 archive_id from archive_item_data_process_selections " +
			           " WHERE archive_end < @archive_end " +
			           " AND item_id = @item_id AND item_column_id = @item_column_id AND archive_type = 2 ORDER BY archive_end DESC) ";

			db.ExecuteUpdateQuery(sql2, DBParameters);

			//Update the last selection to reflect correct values
			db.ExecuteUpdateQueryWithoutArchive(
				"UPDATE item_data_process_selections SET archive_start = (SELECT MAX(archive_end) FROM archive_item_data_process_selections WHERE item_id = @item_id AND item_column_id = @item_column_id) WHERE item_id = @item_id AND item_column_id = @item_column_id",
				DBParameters);

			//Clear cache
			Cache.Initialize();

			Log(itemId, itemColumnId, "Delete");
		}

		public void UpdateArchiveForList(int itemId, int itemColumnId, int archiveId, DateTime archiveStart, DateTime archiveEnd,
			int selectedItemId) {
			SetDBParameter("@item_id", itemId);
			SetDBParameter("@item_column_id", itemColumnId);
			SetDBParameter("@archive_start", archiveStart);
			SetDBParameter("@archive_end", archiveEnd);
			SetDBParameter("@archive_id", archiveId);
			SetDBParameter("@selected_item_id", selectedItemId);

			//Update Archive
			db.ExecuteUpdateQuery(
				"UPDATE archive_item_data_process_selections SET selected_item_id = @selected_item_id, archive_start = @archive_start, archive_end = @archive_end WHERE item_id = @item_id AND item_column_id = @item_column_id AND archive_type = 2 AND archive_end = (SELECT archive_end FROM archive_item_data_process_selections WHERE archive_id = @archive_id)",
				DBParameters);
			
			db.ExecuteUpdateQuery(
				"UPDATE archive_item_data_process_selections SET selected_item_id = @selected_item_id, archive_start = @archive_start, archive_end = @archive_end WHERE archive_id = @archive_id",
				DBParameters);

			//Update the last selection to reflect correct values
			db.ExecuteUpdateQueryWithoutArchive(
				"UPDATE item_data_process_selections SET archive_start = (SELECT MAX(archive_end) FROM archive_item_data_process_selections WHERE item_id = @item_id AND item_column_id = @item_column_id) WHERE item_id = @item_id AND item_column_id = @item_column_id",
				DBParameters);

			//Clear cache
			Cache.Initialize();
			
			Log(itemId, itemColumnId, "Update");
		}

		public void AddArchiveForList(int itemId, int itemColumnId, DateTime archiveEnd, int selectedItemId) {
			SetDBParameter("@item_id", itemId);
			SetDBParameter("@item_column_id", itemColumnId);
			SetDBParameter("@archive_end", archiveEnd);
			SetDBParameter("@selected_item_id", selectedItemId);
			SetDBParameter("@user_item_id", _session.item_id);

			//Add to Archive
			db.ExecuteInsertQuery(
				"INSERT INTO archive_item_data_process_selections (archive_end, archive_type, item_id, item_column_id, selected_item_id, archive_userid, archive_start) VALUES (" +
				"@archive_end, 1, @item_id, @item_column_id, @selected_item_id, @user_item_id, ISNULL((SELECT MAX(archive_end) FROM archive_item_data_process_selections WHERE item_id = @item_id AND item_column_id = @item_column_id AND archive_end < @archive_end),(SELECT MIN(archive_start) FROM archive_item_data_process_selections)))",
				DBParameters);

			//Mind the GAP between records
			var sql1 =
				" UPDATE archive_item_data_process_selections SET archive_start = @archive_end WHERE archive_id = ( " +
				" SELECT TOP 1 archive_id FROM archive_item_data_process_selections " +
				" WHERE archive_start > @archive_end AND item_id = @item_id AND item_column_id = @item_column_id " +
				" AND archive_type = 1)";
			db.ExecuteUpdateQuery(sql1, DBParameters);

			var sql2 =
				" UPDATE archive_item_data_process_selections SET archive_start = @archive_end WHERE archive_id = ( " +
				" SELECT TOP 1 archive_id FROM archive_item_data_process_selections " +
				" WHERE archive_start > @archive_end AND item_id = @item_id AND item_column_id = @item_column_id " +
				" AND archive_type = 2) ";
			db.ExecuteUpdateQuery(sql2, DBParameters);

			//Update the last selection to reflect correct values
			db.ExecuteUpdateQueryWithoutArchive(
				"UPDATE item_data_process_selections SET archive_start = (SELECT MAX(archive_end) FROM archive_item_data_process_selections WHERE item_id = @item_id AND item_column_id = @item_column_id) WHERE item_id = @item_id AND item_column_id = @item_column_id",
				DBParameters);

			//Clear cache
			Cache.Initialize();
			
			Log(itemId, itemColumnId, "Insert");
		}

		public List<HistoryEditorProcessResultModel> GetHistoryForList(int itemId, int itemColumnId) {
			var i = new ItemColumns(instance, "", _session);
			var c = i.GetItemColumn(itemColumnId);
			var t = Conv.ToSql("_" + _instance + "_" + c.DataAdditional);

			var sql = " SELECT  " +
			          "       archive_id,  " +
			          "       1 AS archive_type,  " +
			          "       selected_item_id,  " +
			          "       ISNULL(archive_start, (SELECT MIN(archive_start) FROM archive_item_data_process_selections)) AS archive_start,  " +
			          "       archive_end AS archive_end,  " +
			          "       (SELECT list_item FROM dbo.get_" + t +
			          "(idps.archive_end) WHERE item_id = idps.selected_item_id) AS value, " +
			          "       (SELECT list_item FROM " + t +
			          " WHERE item_id = idps.selected_item_id) AS todays_value " +
			          " FROM archive_item_data_process_selections idps " +
			          " WHERE item_id = @item_id AND item_column_id = @item_column_id AND archive_type <> 2" +
			          " UNION ALL SELECT 0 AS archive_id, 0 AS archive_type, selected_item_id, archive_start AS archive_start, archive_start AS archive_end, " +
			          " (SELECT list_item FROM " + t +
			          " WHERE item_id = idps.selected_item_id) AS value, " +
			          " (SELECT list_item FROM " + t +
			          " WHERE item_id = idps.selected_item_id) AS todays_value " +
			          " FROM item_data_process_selections idps " +
			          " WHERE item_id = @item_id AND item_column_id = @item_column_id " +
			          " ORDER BY archive_type ASC, archive_end DESC";
			SetDBParameter("@item_id", itemId);
			SetDBParameter("@item_column_id", itemColumnId);

			return (from DataRow r in db.GetDatatable(sql, DBParameters).Rows
				select new HistoryEditorProcessResultModel(r)).ToList();
		}
	}
}