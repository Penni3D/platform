﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.historyEditor {
	[Route("v1/features/[controller]")]
	public class HistoryEditorRest : RestBase {
		[HttpGet("process/{itemId}/{itemColumnId}")]
		public List<HistoryEditorProcessResultModel> GetProcessData(int itemId, int itemColumnId) {
			return new HistoryEditor(instance, currentSession).GetHistoryForList(itemId, itemColumnId);
		}

		[HttpPut("{itemId}/{itemColumnId}/{archiveId}/{archiveStart}/{archiveEnd}/{selectedItemId}")]
		public void Update(int itemId, int itemColumnId, int archiveId, DateTime archiveStart, DateTime archiveEnd,
			int selectedItemId) {
			new HistoryEditor(instance, currentSession).UpdateArchiveForList(itemId, itemColumnId, archiveId, archiveStart,
				archiveEnd,
				selectedItemId);
		}

		[HttpPost("{itemId}/{itemColumnId}/{archiveEnd}/{selectedItemId}")]
		public void Add(int itemId, int itemColumnId, DateTime archiveEnd, int selectedItemId) {
			new HistoryEditor(instance, currentSession).AddArchiveForList(itemId, itemColumnId, archiveEnd,
				selectedItemId);
		}

		[HttpDelete("{itemId}/{itemColumnId}/{archiveEnd}")]
		public void Remove(int itemId, int itemColumnId, DateTime archiveEnd) {
			new HistoryEditor(instance, currentSession).RemoveFromArchiveForList(itemId, itemColumnId, archiveEnd);
		}
	}
}