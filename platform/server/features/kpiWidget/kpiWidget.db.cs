﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using DocumentFormat.OpenXml.Spreadsheet;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.processes.portfolios;
using Keto5.x.platform.server.security;
using Newtonsoft.Json;

namespace Keto5.x.platform.server.features.kpiWidget {
	public class KpiWidgetRequest {
		public string Title { get; set; }
		public string Suffix { get; set; }
		public int PortfolioId { get; set; }
		public string PortfolioColumn { get; set; }
		public string Operation { get; set; }
	}

	public class KpiWidgetResult {
		public string Title { get; set; }
		public string Suffix { get; set; }
		public double Value { get; set; }
	}

	public class KpiWidgetService : ConnectionBase {
		private readonly AuthenticatedSession session;

		public KpiWidgetService(string instance, AuthenticatedSession session) : base(instance,
			session) {
			this.instance = instance;
			this.session = session;
		}

		public List<KpiWidgetResult> GetData(List<KpiWidgetRequest> Requests) {
			var results = new List<KpiWidgetResult>();

			var sqls = new Dictionary<int, string>();
			var cols = new Dictionary<int, List<string>>();
			var vals = new Dictionary<string, double>();

			//Generate Inner Sql
			foreach (var r in Requests) {
				if (r.PortfolioId == 0) continue;
				
				var p = new ProcessPortfolios(instance, "", session).GetPortfolio(r.PortfolioId);
				var ics = new ItemColumns(instance, p.Process, session);
				var portfolioSqlService =
					new PortfolioSqlService(instance, session, ics, instance + "_" + p.Process, p.Process);
				var pSqls = portfolioSqlService.GetSql(r.PortfolioId, "");
				var e = new EntityRepository(instance, p.Process, session);

				var eSql1 = "";
				var esql2 = "";
				if (pSqls.equationColumns.Count > 0) {
					var equationSql = "";
					foreach (var col in pSqls.equationColumns) {
						if (Conv.ToInt(col.DataAdditional2) == 5 || Conv.ToInt(col.DataAdditional2) == 6) {
							equationSql += "," + ics.GetEquationQuery(col) + " " + col.Name + "_str";
						} else {
							equationSql += "," + ics.GetEquationQuery(col) + " " + col.Name;
						}
					}

					eSql1 = " SELECT * " + equationSql + " FROM ( ";
					esql2 = " ) pf ";
				}

				var sql = eSql1 + "SELECT " + Modi.JoinListWithComma(pSqls.sqlcols) + ", pf.item_id FROM " +
				          Conv.ToSql("_" + instance + "_" + p.Process) + " pf WHERE " + e.GetWhere(r.PortfolioId) + esql2;

				if (!sqls.ContainsKey(r.PortfolioId)) sqls.Add(r.PortfolioId, sql);

				var item = Conv.ToSql(r.Operation) + "(iq." + r.PortfolioColumn + ") AS " + r.PortfolioColumn + "_" +
				           r.Operation;
				
				if (cols.ContainsKey(r.PortfolioId))
					cols[r.PortfolioId].Add(item);
				else
					cols.Add(r.PortfolioId, new List<string> {item});
			}

			//Generate Outer Sql
			foreach (var portfolioId in sqls.Keys) {
				var sql = sqls[portfolioId];
				var cstr = cols.FirstOrDefault(c => c.Key == portfolioId).Value;
				var isql = "SELECT " + Modi.JoinListWithComma(cstr) + " FROM (" + sql + ") iq";
				var values = db.GetDatatable(isql);
				if (values.Rows.Count <= 0) continue;
				foreach (DataColumn col in values.Columns) {
					if (!vals.ContainsKey(col.ColumnName))
						vals.Add(col.ColumnName, Conv.ToDouble(values.Rows[0][col.ColumnName]));
				}
			}

			foreach (var r in Requests) {
				if (r.PortfolioId == 0) continue;
				var result = new KpiWidgetResult
					{Title = r.Title, Suffix = r.Suffix, Value = vals[r.PortfolioColumn + "_" + r.Operation]};
				results.Add(result);
			}

			return results;
		}
	}
}