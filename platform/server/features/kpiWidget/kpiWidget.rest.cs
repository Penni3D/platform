﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.kpiWidget {
	[Route("v1/features/[controller]")]
	public class KpiWidget : RestBase {
		[HttpPost("")]
		public List<KpiWidgetResult> GetData([FromBody] List<KpiWidgetRequest> Requests) {
			return new KpiWidgetService(instance, currentSession).GetData(Requests);
		}
	}
}