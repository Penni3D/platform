﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.core;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.security;
using Keto5.x.platform.server.processes.accessControl;
using Keto5.x.platform.server.processes.itemColumns;
using Newtonsoft.Json.Linq;

namespace Keto5.x.platform.server.features.financials {
	public class FinancialsModel : ConnectionBase {
		private readonly AuthenticatedSession authenticatedSession;
		private Dictionary<string, object> list_cache = new Dictionary<string, object>();
		private Dictionary<int, Dictionary<string, object>> validation_cache = new Dictionary<int, Dictionary<string, object>>();
		private Translations translations;
		private int project_item_id = 0;
		private string project_process = "";
		private int process_group_id = 0;
		private DateTime? archive_date;
		private bool history_mode = false;
		private bool hide_titles = false;

		private bool RightsDeleteRow = false;
		private bool RightsRead = false;
		private bool RightsWrite = false;
		private bool RightsPlannedRead = false;
		private bool RightsPlannedWrite = false;
		private bool RightsEstimatedRead = false;
		private bool RightsEstimatedWrite = false;
		private bool RightsCommittedRead = false;
		private bool RightsCommittedWrite = false;
		private bool RightsActualRead = false;
		private bool RightsActualWrite = false;

		private Scope scope = Scope.Year;
		private int periods = 5;
		private ShowRowTotals show_row_totals = ShowRowTotals.None;
		private ShowColumnTotals show_column_totals = ShowColumnTotals.Both;
		private int offset = 0;
		private int dashboard_id = 0;
		private ShowLastModified show_last_modified = ShowLastModified.None;
		private int fields_portfolio_id = 0;
		private int one_level_mode = 0;

		public FinancialsModel(string instance, AuthenticatedSession authenticatedSession, int project_item_id, int process_group_id, DateTime? archive_date) : base(instance, authenticatedSession) {
			this.authenticatedSession = authenticatedSession;
			translations = new Translations(instance, authenticatedSession, authenticatedSession.locale_id);

			this.project_item_id = project_item_id;
			this.process_group_id = process_group_id;
			this.archive_date = archive_date;

			SetDBParam(SqlDbType.Int, "project_item_id", project_item_id);
			project_process = Conv.ToStr(db.ExecuteScalar("SELECT process FROM items WHERE item_id = @project_item_id", DBParameters));

			if (archive_date != null) {
				history_mode = true;
				SetDBParam(SqlDbType.DateTime, "archive_date", archive_date);
			}

			States states = new States(instance, authenticatedSession);
			List<string> state_list = new List<string>();
			state_list.Add("financials.financials_delete");
			state_list.Add("financials.financials_read_planned");
			state_list.Add("financials.financials_write_planned");
			state_list.Add("financials.financials_read_estimated");
			state_list.Add("financials.financials_write_estimated");
			state_list.Add("financials.financials_read_ommitted");
			state_list.Add("financials.financials_write_committed");
			state_list.Add("financials.financials_read_actual");
			state_list.Add("financials.financials_write_actual");
			FeatureStates feature_states = states.GetEvaluatedStates(project_process, "financials", project_item_id, state_list, null, null, process_group_id);
			var s = feature_states.groupStates[process_group_id].s;
			RightsPlannedRead = s.Contains("financials.financials_read_planned");
			RightsEstimatedRead = s.Contains("financials.financials_read_estimated");
			RightsCommittedRead = s.Contains("financials.financials_read_ommitted");
			RightsActualRead = s.Contains("financials.financials_read_actual");
			RightsRead = RightsPlannedRead || RightsEstimatedRead || RightsCommittedRead || RightsActualRead;
			if (!history_mode) {
				RightsDeleteRow = s.Contains("financials.financials_delete");
				RightsPlannedWrite = s.Contains("financials.financials_write_planned");
				RightsEstimatedWrite = s.Contains("financials.financials_write_estimated");
				RightsCommittedWrite = s.Contains("financials.financials_write_committed");
				RightsActualWrite = s.Contains("financials.financials_write_actual");
				RightsWrite = RightsPlannedWrite || RightsEstimatedWrite || RightsCommittedWrite || RightsActualWrite;
			}
		}

		private void InitConfigs() {
			var allConfigurations = new Configurations(instance, authenticatedSession);
			var clientConfigurations = allConfigurations.GetClientConfigurations();
			var featureConfigIndex = clientConfigurations.FindIndex(conf => conf.Name == "process.financials" && conf.Process == project_process);
			if (featureConfigIndex > -1) {
				var jsonSettingString = clientConfigurations[featureConfigIndex].Value;
				dynamic settingData = JObject.Parse(jsonSettingString);

				scope = (Scope)Conv.ToInt(settingData["scope"]);
				periods = Conv.ToInt(settingData["periods"]);
				show_row_totals = (ShowRowTotals)Conv.ToInt(settingData["show_row_totals"]);
				show_column_totals = (ShowColumnTotals)Conv.ToInt(settingData["show_column_totals"]);
				offset = Conv.ToInt(settingData["offset"]);
				dashboard_id = Conv.ToInt(settingData["DashboardId"]);
				show_last_modified = (ShowLastModified)Conv.ToInt(settingData["show_last_modified"]);
				fields_portfolio_id = Conv.ToInt(settingData["fieldsPortfolioId"]);
				one_level_mode = Conv.ToInt(settingData["one_level_mode"]);
				hide_titles = Conv.ToBool(settingData["hide_titles"]);
			}
		}

		public Data GetData(DateTime current_date) {
			Data data = new Data();
			if (RightsRead) {
				InitConfigs();

				data.settings.archive = archive_date;

				ItemColumns budget_row_ic = new ItemColumns(instance, "budget_row", authenticatedSession);
				ItemBase budget_row_ib = null;
				DataTable budget_row_columns = null;
				if (fields_portfolio_id > 0) {
					budget_row_ib = new ItemBase(instance, "budget_row", authenticatedSession);
					SetDBParam(SqlDbType.Int, "process_portfolio_id", fields_portfolio_id);
					string portfolio_sql = "SELECT p.item_column_id, p.short_name, p.show_type, p.width, p.validation " +
										   "FROM process_portfolio_columns p " +
										   "INNER JOIN item_columns c ON c.item_column_id = p.item_column_id AND c.data_type IN (0, 1, 2, 3, 4, 5, 6, 16, 20) " +
										   "WHERE p.process_portfolio_id = @process_portfolio_id " +
										   "ORDER BY p.order_no  ASC";
					budget_row_columns = db.GetDatatable(portfolio_sql, DBParameters);

					bool first_field = true;
					foreach (DataRow budget_row_column in budget_row_columns.Rows) {
						int item_column_id = Conv.ToInt(budget_row_column["item_column_id"]);
						if (item_column_id > 0) {
							Dictionary<string, object> portfolio_validation;
							if (validation_cache.ContainsKey(item_column_id)) {
								portfolio_validation = validation_cache[item_column_id];
							}
							else {
								portfolio_validation = new Dictionary<string, object>();
								if (Conv.ToStr(budget_row_column["validation"]).Length == 0) {
									portfolio_validation = new Dictionary<string, object>();
								}
								else {
									portfolio_validation = JObject.Parse(Conv.ToStr(budget_row_column["validation"])).ToObject<Dictionary<string, object>>();
								}
								portfolio_validation.Add("short_name", Conv.ToStr(budget_row_column["short_name"]));
								portfolio_validation.Add("show_type", Conv.ToInt(budget_row_column["show_type"]));
								portfolio_validation.Add("width", Conv.ToInt(budget_row_column["width"]));
								validation_cache.Add(item_column_id, portfolio_validation);
							}

							ItemColumn item_column = budget_row_ic.GetItemColumn(item_column_id);
							portfolio_validation["Max"] = item_column.Validation.Max;
							portfolio_validation["Rows"] = item_column.Validation.Rows;
							portfolio_validation["Spellcheck"] = item_column.Validation.Spellcheck;
							portfolio_validation["Suffix"] = item_column.Validation.Suffix;
							portfolio_validation["ParentId"] = item_column.Validation.ParentId;
							portfolio_validation["DateRange"] = item_column.Validation.DateRange;
							portfolio_validation["CalendarConnection"] = item_column.Validation.CalendarConnection;
							portfolio_validation["AfterMonths"] = item_column.Validation.AfterMonths;
							portfolio_validation["BeforeMonths"] = item_column.Validation.BeforeMonths;
							portfolio_validation["Required"] = item_column.Validation.Required;
							portfolio_validation["ReadOnly"] = item_column.Validation.ReadOnly;
							portfolio_validation["Label"] = item_column.Validation.Label;
							portfolio_validation["Unselectable"] = item_column.Validation.Unselectable;

							Field field = GetField(item_column, null, portfolio_validation);
							if (field != null) {
								data.fields.Add(field);

								FieldTitle field_title = new FieldTitle();
								if (!first_field) {
									field_title.title = field.label;
								}
								field_title.width = field.width;
								data.field_titles.Add(field_title);
								first_field = false;
							}
						}
					}
				}
				else if (one_level_mode == 0) {
					int item_column_id = Conv.ToInt(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'budget_row' AND name='description'", DBParameters));
					Dictionary<string, object> portfolio_validation = new Dictionary<string, object>();
					Field field = GetField(budget_row_ic.GetItemColumn(item_column_id), null, portfolio_validation);
					field.type = "string";
					data.fields.Add(field);
					FieldTitle field_title = new FieldTitle();
					field_title.width = "140px";
					data.field_titles.Add(field_title);
				}

				List<Dictionary<string, object>> list_budget_category = GetList("list_budget_category");
				List<Dictionary<string, object>> list_budget_item_type = GetList("list_budget_item_type");

				int planned_type_item_id = 0;
				int estimated_type_item_id = 0;
				int committed_type_item_id = 0;
				int actual_type_item_id = 0;

				Dictionary<string, object> item_type = list_budget_item_type.Find(x => (int)x["type"] == 1);
				if (item_type != null) {
					planned_type_item_id = Conv.ToInt(item_type["item_id"]);
					data.planned_title = Conv.ToStr(item_type["list_item"]);
				}
				item_type = list_budget_item_type.Find(x => (int)x["type"] == 2);
				if (item_type != null) {
					estimated_type_item_id = Conv.ToInt(item_type["item_id"]);
					data.estimated_title = Conv.ToStr(item_type["list_item"]);
				}
				item_type = list_budget_item_type.Find(x => (int)x["type"] == 3);
				if (item_type != null) {
					committed_type_item_id = Conv.ToInt(item_type["item_id"]);
					data.committed_title = Conv.ToStr(item_type["list_item"]);
				}
				item_type = list_budget_item_type.Find(x => (int)x["type"] == 4);
				if (item_type != null) {
					actual_type_item_id = Conv.ToInt(item_type["item_id"]);
					data.actual_title = Conv.ToStr(item_type["list_item"]);
				}

				DataTable budget_rows = GetBudgetRows();
				DataTable budget_items = GetBudgetItems();

				int d_add = 0;
				if (scope == Scope.Month) {
					data.current_date = new DateTime(current_date.Year, current_date.Month, 1);
					d_add = 1;
				}
				else if (scope == Scope.Quartal) {
					data.current_date = new DateTime(current_date.Year, ((int)Math.Floor((decimal)(current_date.Month - 1) / 3) * 3) + 1, 1);
					d_add = 3;
				}
				else {
					data.current_date = new DateTime(current_date.Year, 1, 1);
					d_add = 12;
				}

				string startToString = data.current_date.AddMonths(offset * d_add).ToString("yyyy-MM-dd");
				string endToString = data.current_date.AddMonths(((periods + offset) * d_add) - 1).ToString("yyyy-MM-dd");

				data.scope = scope;
				data.show_row_totals = show_row_totals;
				data.show_column_totals = show_column_totals;

				for (int p = offset; p < periods + offset; p++) {
					DateTime d = data.current_date.AddMonths(p * d_add);

					if (p == offset) {
						data.current_year = d.Year;
					}

					PeriodTitle pt = new PeriodTitle();

					if (d.Month == 1 || p == offset) {
						pt.upper_title = Conv.ToStr(d.Year);
					}
					if (scope == Scope.Month) {
						pt.title = translations.GetTranslation("CAL_MONTHS_LONG_" + (d.Month - 1));
					}
					else if (scope == Scope.Quartal) {
						pt.title = "Q" + ((int)Math.Floor((decimal)(d.Month - 1) / 3) + 1);
					}
					else {
						pt.title = Conv.ToStr(current_date.Year + p);
					}
					data.periods.Add(pt);

					Period tp = new Period();
					data.totals.Add(tp);
					data.cumulatives.Add(tp);
				}

				data.one_level_mode = one_level_mode == 1;
				data.hide_titles = hide_titles;

				foreach (Dictionary<string, object> category in list_budget_category) {
					Category cat = new Category();
					cat.category_item_id = Conv.ToInt(category["item_id"]);
					cat.name = Conv.ToStr(category["list_item"]);
					cat.income = Conv.ToInt(category["income"]);

					for (int p = offset; p < periods + offset; p++) {
						Period cp = new Period();
						cat.periods.Add(cp);
					}

					DataRow[] budget_row = budget_rows.Select("category_item_id = " + Conv.ToInt(category["item_id"]), "item_id ASC");

					if (!history_mode && one_level_mode == 1 && budget_row.Length == 0) {
						Dictionary<string, object> fields = new Dictionary<string, object>();
						fields.Add("project_item_id", project_item_id);
						fields.Add("category_item_id", new List<int>() { cat.category_item_id });
						fields.Add("description", cat.name);
						ItemBase ib = new ItemBase(instance, "budget_row", authenticatedSession);
						var row_item_id = Conv.ToInt(ib.AddItem(fields)["item_id"]);

						var row = budget_rows.NewRow();
						row["item_id"] = row_item_id;
						row["description"] = cat.name;
						row["category_item_id"] = cat.category_item_id;
						row["project_item_id"] = project_item_id;

						budget_row = new DataRow[1];
						budget_row[0] = row;
					}

					foreach (DataRow r in budget_row) {
						Row row = new Row();
						row.row_item_id = Conv.ToInt(r["item_id"]);

						for (int p = offset; p < periods + offset; p++) {
							DateTime d = data.current_date.AddMonths(p * d_add);
							string dToString = d.ToString("yyyy-MM-dd");
							DateTime e = data.current_date.AddMonths((p + 1) * d_add);
							string eToString = e.ToString("yyyy-MM-dd");

							Period rp = new Period();
							rp.date = d;
							if (RightsPlannedRead) {
								rp.planned_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + planned_type_item_id + " AND date >= #" + dToString + "# AND date < #" + eToString + "#"));
							}
							if (RightsEstimatedRead) {
								rp.estimated_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + estimated_type_item_id + " AND date >= #" + dToString + "# AND date < #" + eToString + "#"));
							}
							if (RightsCommittedRead) {
								rp.committed_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + committed_type_item_id + " AND date >= #" + dToString + "# AND date < #" + eToString + "#"));
							}
							if (RightsActualRead) {
								rp.actual_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + actual_type_item_id + " AND date >= #" + dToString + "# AND date < #" + eToString + "#"));
							}
							row.periods.Add(rp);
						}

						if (show_row_totals == ShowRowTotals.Year || show_row_totals == ShowRowTotals.Both) {
							if (RightsPlannedRead) {
								row.year_periods_others.planned_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + planned_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#) AND year = " + data.current_year));
							}
							if (RightsEstimatedRead) {
								row.year_periods_others.estimated_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + estimated_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#) AND year = " + data.current_year));
							}
							if (RightsCommittedRead) {
								row.year_periods_others.committed_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + committed_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#) AND year = " + data.current_year));
							}
							{
								row.year_periods_others.actual_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + actual_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#) AND year = " + data.current_year));
							}
						}

						if (show_row_totals == ShowRowTotals.Total || show_row_totals == ShowRowTotals.Both) {
							if (RightsPlannedRead) {
								row.total_periods_others.planned_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + planned_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#)"));
							}
							if (RightsEstimatedRead) {
								row.total_periods_others.estimated_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + estimated_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#)"));
							}
							if (RightsCommittedRead) {
								row.total_periods_others.committed_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + committed_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#)"));
							}
							if (RightsActualRead) {
								row.total_periods_others.actual_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + actual_type_item_id + " AND (date < #" + startToString + "# OR date > #" + endToString + "#)"));
							}
						}

						if (RightsPlannedRead) {
							row.cumulatives_others.planned_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + planned_type_item_id + " AND date < #" + startToString + "#"));
						}
						if (RightsEstimatedRead) {
							row.cumulatives_others.estimated_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + estimated_type_item_id + " AND date < #" + startToString + "#"));
						}
						if (RightsCommittedRead) {
							row.cumulatives_others.committed_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + committed_type_item_id + " AND date < #" + startToString + "#"));
						}
						if (RightsActualRead) {
							row.cumulatives_others.actual_value = Conv.ToDouble(budget_items.Compute("SUM(amount)", "parent_item_id = " + Conv.ToInt(r["item_id"]) + " AND type_item_id = " + actual_type_item_id + " AND date < #" + startToString + "#"));
						}

						if (fields_portfolio_id > 0) {
							Dictionary<string, object> budget_row_data;
							if (history_mode) {
								budget_row_data = budget_row_ib.GetArchiveItem(row.row_item_id, (DateTime)archive_date);
							}
							else {
								budget_row_data = budget_row_ib.GetItem(row.row_item_id);
							}
							foreach (DataRow budget_row_column in budget_row_columns.Rows) {
								int item_column_id = Conv.ToInt(budget_row_column["item_column_id"]);
								if (item_column_id > 0) {
									ItemColumn item_column = budget_row_ic.GetItemColumn(item_column_id);
									FieldValue field_value = new FieldValue();
									field_value.value = budget_row_data[item_column.Name];
									row.fields.Add(field_value);
								}
							}
						}
						else if (one_level_mode == 0) {
							FieldValue field_value = new FieldValue();
							field_value.value = Conv.ToStr(r["description"]);
							row.fields.Add(field_value);
						}

						cat.rows.Add(row.row_item_id, row);
					}

					data.categories.Add("C" + Conv.ToStr(cat.category_item_id), cat);
				}
			}

			SetDBParam(SqlDbType.Int, "process_dashboard_id", dashboard_id);

			string sql = @"SELECT c.process_dashboard_chart_id, c.process_portfolio_id, c.variable, CASE WHEN c.json_data LIKE '%""fullWidth"":true%' THEN 1 ELSE 0 END AS wide ";
			if (history_mode) {
				sql += @"FROM get_process_dashboard_chart_map(@archive_date) d " +
					   @"LEFT JOIN get_process_dashboard_charts(@archive_date) c ON c.process_dashboard_chart_id = d.process_dashboard_chart_id ";
			}
			else {
				sql += @"FROM process_dashboard_chart_map d " +
					   @"LEFT JOIN process_dashboard_charts c ON c.process_dashboard_chart_id = d.process_dashboard_chart_id ";
			}
			sql += @"WHERE d.process_dashboard_id = @process_dashboard_id " +
				   @"ORDER BY d.order_no  ASC";

			int left_right = 0;
			foreach (DataRow chart_row in db.GetDatatable(sql, DBParameters).Rows) {
				Chart chart = new Chart();
				chart.chart_id = Conv.ToInt(chart_row["process_dashboard_chart_id"]);
				chart.portfolio_id = Conv.ToInt(chart_row["process_portfolio_id"]);
				chart.name = translations.GetTranslation(Conv.ToStr(chart_row["variable"]));
				if (Conv.ToInt(chart_row["wide"]) == 1) {
					chart.position = "chart_full";
					left_right = 0;
				}
				else if (left_right == 0) {
					chart.position = "chart_left";
					left_right = 1;
				}
				else {
					chart.position = "chart_right";
					left_right = 0;
				}
				data.charts.Add(chart);
			}

			data.show_last_modified = show_last_modified;
			data.last_modified = GetLastModified();

			return data;
		}

		private Field GetField(ItemColumn column, Dictionary<string, object> parent_row, Dictionary<string, object> validation) {
			string Precision = "";
			string Min = "";
			string Max = "";
			string Rows = "";
			string Spellcheck = "";
			string Suffix = "";
			int? ParentId = null;
			string DateRange = null;
			bool CalendarConnection = false;
			int? AfterMonths = null;
			int? BeforeMonths = null;
			bool singleValue = false;
			bool Mandatory = false;
			bool Read_Only = false;
			string HelpText = "";
			string Label = "";
			string Width = "";
			bool Unselectable = false;
			string ShowType = null;
			if (validation != null) {
				if (validation.ContainsKey("Precision") && validation["Precision"] != null) {
					Precision = Conv.ToStr(validation["Precision"]);
				}
				if (validation.ContainsKey("Min") && validation["Min"] != null) {
					Min = Conv.ToStr(validation["Min"]);
				}
				if (validation.ContainsKey("Max") && validation["Max"] != null) {
					Max = Conv.ToStr(validation["Max"]);
				}
				if (validation.ContainsKey("Rows") && validation["Rows"] != null) {
					Rows = Conv.ToStr(validation["Rows"]);
				}
				if (validation.ContainsKey("Spellcheck") && validation["Spellcheck"] != null) {
					Spellcheck = Conv.ToStr(validation["Spellcheck"]);
				}
				if (validation.ContainsKey("Suffix") && validation["Suffix"] != null) {
					Suffix = Conv.ToStr(validation["Suffix"]);
				}
				if (validation.ContainsKey("ParentId") && validation["ParentId"] != null) {
					ParentId = Conv.ToInt(validation["ParentId"]);
				}
				if (validation.ContainsKey("DateRange") && validation["DateRange"] != null) {
					DateRange = Conv.ToStr(validation["DateRange"]);
				}
				if (validation.ContainsKey("CalendarConnection") && Conv.ToBool(validation["CalendarConnection"])) {
					CalendarConnection = true;
				}
				if (validation.ContainsKey("AfterMonths") && validation["AfterMonths"] != null) {
					AfterMonths = Conv.ToInt(validation["AfterMonths"]);
				}
				if (validation.ContainsKey("BeforeMonths") && validation["BeforeMonths"] != null) {
					BeforeMonths = Conv.ToInt(validation["BeforeMonths"]);
				}
				if (validation.ContainsKey("Required") && Conv.ToBool(validation["Required"])) {
					Mandatory = true;
				}
				if (validation.ContainsKey("ReadOnly") && Conv.ToBool(validation["ReadOnly"])) {
					Read_Only = true;
				}
				if (validation.ContainsKey("help_text") && Conv.ToStr(validation["help_text"]).Length > 0) {
					HelpText = translations.GetTranslation(Conv.ToStr(validation["help_text"]));
				}
				if (validation.ContainsKey("Label") && Conv.ToStr(validation["Label"]).Length > 2) {
					Label = GetTranslation(JObject.Parse(Conv.ToStr(validation["Label"])).ToObject<Dictionary<string, string>>(), true);
				}
				if (validation.ContainsKey("short_name") && Conv.ToStr(validation["short_name"]).Length > 0) {
					Label = translations.GetTranslation(Conv.ToStr(validation["short_name"]));
				}
				if (validation.ContainsKey("width") && Conv.ToInt(validation["width"]) > 0) {
					Width = Conv.ToStr(validation["width"] + "px");
				}
				if (validation.ContainsKey("Unselectable") && Conv.ToBool(validation["Unselectable"])) {
					Unselectable = true;
				}
				if (validation.ContainsKey("show_type") && Conv.ToInt(validation["show_type"]) == 2) {
					ShowType = "icon-type";
				}
			}

			if (Label == "") {
				Label = GetTranslation(column.LanguageTranslation);
			}

			switch (column.DataType) {
				case 0:
					return new Field(column.ItemColumnId, column.Name, "string", parent_row == null ? null : parent_row[column.Name], Label, "", "", Max, "", "", Spellcheck, "", "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 1:
					return new Field(column.ItemColumnId, column.Name, "integer", parent_row == null ? null : parent_row[column.Name], Label, "", Min, Max, "", "", "", Suffix, "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 2:
					return new Field(column.ItemColumnId, column.Name, "decimal", parent_row == null ? null : parent_row[column.Name], Label, "", Min, Max, "", Precision, "", Suffix, "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 3:
					return new Field(column.ItemColumnId, column.Name, "date", parent_row == null ? null : parent_row[column.Name], Label, "", "", "", "", "", "", "", "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 4:
					return new Field(column.ItemColumnId, column.Name, "text", parent_row == null ? null : parent_row[column.Name], Label, "", "", Max, Rows, "", Spellcheck, "", "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 5:
					if (Max == "") {
						Max = "1";
					}
					return new Field(column.ItemColumnId, column.Name, "process", parent_row == null ? null : parent_row[column.Name], Label, column.DataAdditional, "", Max, "", "", "", "", "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 6:
					List<Dictionary<string, object>> list;
					if (list_cache.ContainsKey(column.DataAdditional)) {
						list = (List<Dictionary<string, object>>)list_cache[column.DataAdditional];
					}
					else {
						ItemCollectionService ib = new ItemCollectionService(instance, column.DataAdditional, authenticatedSession);
						SetDBParam(SqlDbType.NVarChar, "process", column.DataAdditional);
						string list_order = Conv.ToStr(db.ExecuteScalar("SELECT list_order FROM processes WHERE process = @process", DBParameters));
						if (list_order.Length > 0) {
							ib.FillItems(list_order);
						}
						else {
							ib.FillItems();
						}
						list = ib.GetItems();
						list_cache.Add(column.DataAdditional, list);
					}
					if (Max == "") {
						Max = "1";
					}
					if (Max == "1") {
						singleValue = true;
					}
					return new Field(column.ItemColumnId, column.Name, "select", parent_row == null ? null : parent_row[column.Name], Label, "", "", Max, "", "", "", "", "list_item", "item_id", "list_symbol", "list_symbol_fill", singleValue, list, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				//case 10:
				//	return new Field(column.ItemColumnId, column.Name, "attachment", parent_row == null ? null : parent_row[column.Name], Label, "", "", "", "", "", "", "", "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				//case 15:
				//	return new Field(column.ItemColumnId, column.Name, "link", parent_row == null ? null : parent_row[column.Name], Label, "", "", "", "", "", "", "", "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 16:
					return new Field(column.ItemColumnId, column.Name, "string", parent_row == null ? null : parent_row[column.Name], Label, "", Min, Max, "", Precision, "", Suffix, "", "", "", "", false, null, Mandatory, true, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				case 20:
					return new Field(column.ItemColumnId, column.Name, GetDataType(column.DataAdditional2), parent_row == null ? null : parent_row[column.Name], Label, "", Min, Max, "", Precision, "", Suffix, "", "", "", "", false, null, Mandatory, true, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				//case 23:
				//	return new Field(column.ItemColumnId, column.Name, "rich", parent_row == null ? null : parent_row[column.Name], Label, "", "", "", "", "", "", "", "", "", "", "", false, null, Mandatory, Read_Only, HelpText, Width, Unselectable, ShowType, ParentId, DateRange, CalendarConnection, AfterMonths, BeforeMonths);
				default:
					return null;
					//throw new Exception("STATUS_REPORT_ERROR_INVALID_DATATYPE");
			}
		}

		private string GetDataType(string data_type) {
			switch (data_type) {
				case "1":
					return "integer";
				case "2":
					return "decimal";
				case "3":
					return "date";
				case "4":
					return "text";
				case "5":
					return "process";
				case "6":
					return "select";
			}
			return "string";
		}

		private string GetTranslation(Dictionary<string, string> translation, bool skip_error = false) {
			if (translation.ContainsKey(authenticatedSession.locale_id)) {
				return translation[authenticatedSession.locale_id];
			}
			else {
				if (skip_error) {
					return "";
				}
				else {
					return translations.GetTranslation("STATUS_REPORT_ERROR_NO_TRANSLATION");
				}
			}
		}

		public LastModified Save(List<SaveRow> save_rows) {
			InitConfigs();

			if (RightsWrite) {
				ItemBase budget_row_ib = new ItemBase(instance, "budget_row", authenticatedSession);
				ItemBase budget_item_ib = new ItemBase(instance, "budget_item", authenticatedSession);
				DataTable budget_rows = GetBudgetRows();
				DataTable budget_items = GetBudgetItems();

				List<Dictionary<string, object>> list_budget_item_type = GetList("list_budget_item_type");

				foreach (SaveRow save_row in save_rows) {
					DataRow[] budget_row = budget_rows.Select("item_id = " + save_row.row_item_id + " AND project_item_id = " + project_item_id);
					if (budget_row.Length == 1) {
						Dictionary<string, object> fields = new Dictionary<string, object>();
						foreach (KeyValuePair<string, object> field in save_row.fields) {
							fields.Add(field.Key, field.Value);
						}
						budget_row_ib.SaveItem(fields, save_row.row_item_id);

						foreach (Period period in save_row.periods) {
							if (RightsPlannedWrite) {
								Dictionary<string, object> item_type = list_budget_item_type.Find(x => (int)x["type"] == 1);
								if (item_type != null) {
									SaveItems(Conv.ToInt(item_type["item_id"]), save_row, period, period.planned_value, budget_item_ib, budget_items);
								}
							}
							if (RightsEstimatedWrite) {
								Dictionary<string, object> item_type = list_budget_item_type.Find(x => (int)x["type"] == 2);
								if (item_type != null) {
									SaveItems(Conv.ToInt(item_type["item_id"]), save_row, period, period.estimated_value, budget_item_ib, budget_items);
								}
							}
							if (RightsCommittedWrite) {
								Dictionary<string, object> item_type = list_budget_item_type.Find(x => (int)x["type"] == 3);
								if (item_type != null) {
									SaveItems(Conv.ToInt(item_type["item_id"]), save_row, period, period.committed_value, budget_item_ib, budget_items);
								}
							}
							if (RightsActualWrite) {
								Dictionary<string, object> item_type = list_budget_item_type.Find(x => (int)x["type"] == 4);
								if (item_type != null) {
									SaveItems(Conv.ToInt(item_type["item_id"]), save_row, period, period.actual_value, budget_item_ib, budget_items);
								}
							}
						}
					}
				}
			}

			return GetLastModified();
		}

		private void SaveItems(int type_item_id, SaveRow save_row, Period period, double value, ItemBase budget_item_ib, DataTable budget_items) {
			DataRow[] budget_item = budget_items.Select("parent_item_id = " + save_row.row_item_id + " AND type_item_id = " + type_item_id + " AND date = #" + period.date.ToString("yyyy-MM-dd") + "#");
			if (budget_item.Length > 1) {
				foreach (DataRow item in budget_item) {
					budget_item_ib.Delete(Conv.ToInt(item["item_id"]), false);
				}
				Array.Clear(budget_item, 0, budget_item.Length);
			}
			if (budget_item.Length == 0) {
				Dictionary<string, object> fields = new Dictionary<string, object>();
				fields.Add("project_item_id", project_item_id);
				fields.Add("parent_item_id", new List<int>() { save_row.row_item_id });
				fields.Add("type_item_id", new List<int>() { type_item_id });
				fields.Add("category_item_id", new List<int>() { save_row.category_item_id });
				fields.Add("date", period.date);
				fields.Add("amount", value);
				budget_item_ib.AddItem(fields);
			}
			else if (budget_item.Length == 1 && Conv.ToDouble(budget_item[0]["amount"]) != value) {
				Dictionary<string, object> fields = new Dictionary<string, object>();
				fields.Add("amount", value);
				budget_item_ib.SaveItem(fields, Conv.ToInt(budget_item[0]["item_id"]));
			}
		}

		public Row AddRow(int category_item_id, DateTime current_date) {
			Row row = new Row();
			InitConfigs();

			if (RightsWrite) {

				Dictionary<string, object> fields = new Dictionary<string, object>();
				fields.Add("project_item_id", project_item_id);
				fields.Add("category_item_id", new List<int>() { category_item_id });
				fields.Add("description", "");
				ItemBase ib = new ItemBase(instance, "budget_row", authenticatedSession);
				row.row_item_id = Conv.ToInt(ib.AddItem(fields)["item_id"]);

				int d_add = 0;
				if (scope == Scope.Month) {
					current_date = new DateTime(current_date.Year, current_date.Month, 1);
					d_add = 1;
				}
				else if (scope == Scope.Quartal) {
					current_date = new DateTime(current_date.Year, ((int)Math.Floor((decimal)(current_date.Month - 1) / 3) * 3) + 1, 1);
					d_add = 3;
				}
				else {
					current_date = new DateTime(current_date.Year, 1, 1);
					d_add = 12;
				}
				for (int p = offset; p < periods + offset; p++) {
					DateTime d = current_date.AddMonths(p * d_add);
					Period rp = new Period();
					rp.date = d;
					row.periods.Add(rp);
				}

				if (fields_portfolio_id > 0) {
					SetDBParam(SqlDbType.Int, "process_portfolio_id", fields_portfolio_id);
					string portfolio_sql = "SELECT p.item_column_id, p.short_name, p.show_type, p.width, p.validation " +
										   "FROM process_portfolio_columns p " +
										   "INNER JOIN item_columns c ON c.item_column_id = p.item_column_id AND c.data_type IN (0, 1, 2, 3, 4, 5, 6, 16, 20) " +
										   "WHERE p.process_portfolio_id = @process_portfolio_id " +
										   "ORDER BY p.order_no  ASC";
					foreach (DataRow budget_row_column in db.GetDatatable(portfolio_sql, DBParameters).Rows) {
						if (Conv.ToInt(budget_row_column["item_column_id"]) > 0) {
							row.fields.Add(new FieldValue());
						}
					}
				}
				else {
					FieldValue field_value = new FieldValue();
					field_value.value = "";
					row.fields.Add(field_value);
				}
			}

			row.last_modified = GetLastModified();

			return row;
		}

		public LastModified DeleteRows(List<SelectedRow> selected_rows) {
			InitConfigs();

			if (RightsDeleteRow) {
				ItemBase budget_item_ib = new ItemBase(instance, "budget_item", authenticatedSession);
				foreach (SelectedRow selected_row in selected_rows) {
					Dictionary<string, object> search = new Dictionary<string, object>();
					search.Add("project_item_id", project_item_id);
					foreach (Dictionary<string, object> budget_item in budget_item_ib.GetItems(search)) {
						if (((List<int>)budget_item["parent_item_id"]).Contains(selected_row.row_item_id)) {
							budget_item_ib.Delete(Conv.ToInt(budget_item["item_id"]), false);
						}
					}
					SetDBParam(SqlDbType.Int, "row_item_id", selected_row.row_item_id);
					db.ExecuteDeleteQuery("DELETE FROM items WHERE item_id IN (SELECT item_id FROM _" + instance + "_budget_row WHERE project_item_id = @project_item_id AND item_id = @row_item_id)", DBParameters);
				}
			}

			return GetLastModified();
		}

		private List<Dictionary<string, object>> GetList(string process) {
			ItemCollectionService ib = new ItemCollectionService(instance, process, authenticatedSession);
			string list_order = Conv.ToStr(db.ExecuteScalar("SELECT list_order FROM processes WHERE process = '" + Conv.ToSql(process) + "'", DBParameters));
			if (list_order.Length > 0) {
				ib.FillItems(list_order);
			}
			else {
				ib.FillItems();
			}
			return ib.GetItems();
		}

		private DataTable GetBudgetRows() {
			SetDBParam(SqlDbType.Int, "row_category_column_item_id", Conv.ToInt(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'budget_row' AND name = 'category_item_id'", DBParameters)));

			string sql = "SELECT r.item_id, r.description, cs.selected_item_id AS category_item_id, r.project_item_id";
			if (history_mode) {
				sql += " FROM get__" + instance + "_budget_row(@archive_date) r";
				sql += " LEFT JOIN get_item_data_process_selections_audit(@archive_date, @row_category_column_item_id) cs ON cs.item_id = r.item_id";
			}
			else {
				sql += " FROM _" + instance + "_budget_row r";
				sql += " LEFT JOIN item_data_process_selections cs ON cs.item_id = r.item_id AND cs.item_column_id = @row_category_column_item_id";
			}
			sql += " WHERE r.project_item_id = @project_item_id";
			sql += " ORDER BY r.item_id ASC";
			return db.GetDatatable(sql, DBParameters);
		}

		private DataTable GetBudgetItems() {
			SetDBParam(SqlDbType.Int, "item_parent_column_item_id", Conv.ToInt(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'budget_item' AND name = 'parent_item_id'", DBParameters)));
			SetDBParam(SqlDbType.Int, "item_type_column_item_id", Conv.ToInt(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'budget_item' AND name = 'type_item_id'", DBParameters)));
			SetDBParam(SqlDbType.Int, "item_category_column_item_id", Conv.ToInt(db.ExecuteScalar("SELECT item_column_id FROM item_columns WHERE instance = @instance AND process = 'budget_item' AND name = 'category_item_id'", DBParameters)));

			string sql = "SELECT i.item_id, i.date, YEAR(i.date) AS year, i.amount, i.project_item_id";
			sql += ", ps.selected_item_id AS parent_item_id, ts.selected_item_id AS type_item_id, cs.selected_item_id AS category_item_id ";
			if (history_mode) {
				sql += " FROM get__" + instance + "_budget_item(@archive_date) i";
				sql += " LEFT JOIN get_item_data_process_selections_audit(@archive_date, @item_parent_column_item_id) ps ON ps.item_id = i.item_id";
				sql += " LEFT JOIN get_item_data_process_selections_audit(@archive_date, @item_type_column_item_id) ts ON ts.item_id = i.item_id";
				sql += " LEFT JOIN get_item_data_process_selections_audit(@archive_date, @item_category_column_item_id) cs ON cs.item_id = i.item_id";
			}
			else {
				sql += " FROM _" + instance + "_budget_item i";
				sql += " LEFT JOIN item_data_process_selections ps ON ps.item_id = i.item_id AND ps.item_column_id = @item_parent_column_item_id";
				sql += " LEFT JOIN item_data_process_selections ts ON ts.item_id = i.item_id AND ts.item_column_id = @item_type_column_item_id";
				sql += " LEFT JOIN item_data_process_selections cs ON cs.item_id = i.item_id AND cs.item_column_id = @item_category_column_item_id";
			}
			sql += " WHERE i.project_item_id = @project_item_id";
			sql += " ORDER BY i.item_id ASC";
			return db.GetDatatable(sql, DBParameters);
		}

		private LastModified GetLastModified() {
			LastModified last_modified = new LastModified();
			DataRow row = null;
			if (show_last_modified != ShowLastModified.None) {
				string sql = "SELECT TOP 1 u.last_name + ' ' + u.first_name AS archive_user, tmp.archive_end FROM (";
				sql += "SELECT r.archive_userid, r.archive_end";
				sql += " FROM archive__" + instance + "_budget_row r";
				sql += " WHERE r.archive_end = (SELECT MAX(br.archive_end) FROM archive__" + instance + "_budget_row br WHERE br.project_item_id = @project_item_id)";
				sql += " AND r.project_item_id = @project_item_id";
				sql += " UNION ALL";
				sql += " SELECT i.archive_userid, i.archive_end";
				sql += " FROM archive__" + instance + "_budget_item i";
				sql += " WHERE i.archive_end = (SELECT MAX(bi.archive_end) FROM archive__" + instance + "_budget_item bi WHERE bi.project_item_id = @project_item_id)";
				sql += " AND i.project_item_id = @project_item_id";
				sql += ") AS tmp";
				sql += " LEFT JOIN _" + instance + "_user u ON u.item_id = tmp.archive_userid";
				sql += " ORDER BY tmp.archive_end DESC";
				row = db.GetDataRow(sql, DBParameters);
			}
			if (show_last_modified.In(ShowLastModified.DateAndTime, ShowLastModified.Both)) {
				if (row != null) {
					last_modified.modified_at = Conv.ToDateTime(row["archive_end"]);
				}
			}
			if (show_last_modified == ShowLastModified.Both) {
				last_modified.modified_by += " - ";
			}
			if (show_last_modified.In(ShowLastModified.User, ShowLastModified.Both)) {
				if (row != null) {
					last_modified.modified_by += Conv.ToStr(row["archive_user"]);
				}
			}
			return last_modified;
		}
	}

	[Serializable]
	public class LastModified {
		public string modified_by = "";
		public DateTime? modified_at = null;
	}

	public enum Scope {
		Year = 0,
		Quartal = 1,
		Month = 2
	}

	public enum ShowRowTotals {
		None = 0,
		Year = 1,
		Total = 2,
		Both = 3
	}

	public enum ShowColumnTotals {
		None = 0,
		Cumulative = 1,
		Total = 2,
		Both = 3
	}

	public enum ShowLastModified {
		None = 0,
		DateAndTime = 1,
		User = 2,
		Both = 3
	}

	[Serializable]
	public class Data {
		public DateTime current_date;
		public int current_year = 0;
		public Scope scope = Scope.Year;
		public ShowRowTotals show_row_totals = ShowRowTotals.None;
		public ShowColumnTotals show_column_totals = ShowColumnTotals.None;
		public List<PeriodTitle> periods = new List<PeriodTitle>();
		public string planned_title = "";
		public string estimated_title = "";
		public string committed_title = "";
		public string actual_title = "";
		public PeriodTitle year_periods = new PeriodTitle();
		public PeriodTitle total_periods = new PeriodTitle();
		public Dictionary<string, Category> categories = new Dictionary<string, Category>();
		public List<Period> totals = new List<Period>();
		public Period year_totals = new Period();
		public Period total_totals = new Period();
		public List<Period> cumulatives = new List<Period>();
		public Period year_cumulatives = new Period();
		public List<Chart> charts = new List<Chart>();
		public ShowLastModified show_last_modified = ShowLastModified.None;
		public LastModified last_modified = new LastModified();
		public Settings settings = new Settings();
		public List<FieldTitle> field_titles = new List<FieldTitle>();
		public List<Field> fields = new List<Field>();
		public bool one_level_mode = false;
		public bool hide_titles = false;
	}

	[Serializable]
	public class Category {
		public int category_item_id = 0;
		public string name = "";
		public int income = 0;
		public List<Period> periods = new List<Period>();
		public Period year_periods = new Period();
		public Period total_periods = new Period();
		public Dictionary<int, Row> rows = new Dictionary<int, Row>();
	}

	[Serializable]
	public class Row {
		public int row_item_id = 0;
		public List<Period> periods = new List<Period>();
		public Period year_periods = new Period();
		public Period total_periods = new Period();
		public Period year_periods_others = new Period();
		public Period total_periods_others = new Period();
		public Period cumulatives_others = new Period();
		public LastModified last_modified = new LastModified();
		public List<FieldValue> fields = new List<FieldValue>();
	}

	[Serializable]
	public class FieldValue {
		public object value = null;
	}

	[Serializable]
	public class PeriodTitle {
		public string upper_title = "";
		public string title = "";
	}

	[Serializable]
	public class Period {
		public DateTime date;
		public double planned_value = 0;
		public double estimated_value = 0;
		public double committed_value = 0;
		public double actual_value = 0;
	}

	[Serializable]
	public class Chart {
		public int chart_id = 0;
		public int portfolio_id = 0;
		public string name = "";
		public string position = "";
	}

	[Serializable]
	public class SaveRow {
		public int category_item_id = 0;
		public int row_item_id = 0;
		public List<Period> periods = new List<Period>();
		public Dictionary<string, object> fields = new Dictionary<string, object>();
	}

	[Serializable]
	public class SelectedRow {
		public int category_item_id = 0;
		public int row_item_id = 0;
	}

	[Serializable]
	public class Settings {
		public DateTime? archive = null;
	}

	[Serializable]
	public class FieldTitle {
		public string title = "";
		public string width = "";
	}

	[Serializable]
	public class Field {
		public int id = 0;
		public string name = "";
		public string type = "";
		public object value = null;
		public string label = "";
		public string process = "";
		public string min = "";
		public string max = "";
		public string rows = "";
		public string precision = "";
		public string spellcheck = "";
		public string suffix = "";
		public string option_name = "";
		public string option_value = "";
		public string option_icon = "";
		public string option_icon_color = "";
		public bool singleValue = false;
		public List<Dictionary<string, object>> list = null;
		public bool is_report_name = false;
		public bool mandatory = false;
		public bool read_only = false;
		public string help_text = "";
		public string width = "";
		public bool unselectable = false;
		public string show_type = null;
		public int? parent_id = null;
		public string date_range = null;
		public bool calendar_connection = false;
		public int? after_months = null;
		public int? before_months = null;
		public object model_start = null;
		public object model_end = null;

		public Field(int id, string name, string type, object value, string label, string process, string min, string max, string rows, string precision, string spellcheck, string suffix, string option_name, string option_value, string option_icon, string option_icon_color, bool singleValue, List<Dictionary<string, object>> list, bool mandatory, bool read_only, string help_text, string width, bool unselectable, string show_type, int? parent_id, string date_range, bool calendar_connection, int? after_months, int? before_months) {
			this.id = id;
			this.name = name;
			this.type = type;
			this.value = value;
			this.label = label;
			this.process = process;
			this.min = min;
			this.max = max;
			this.rows = rows;
			this.precision = precision;
			this.spellcheck = spellcheck;
			this.suffix = suffix;
			this.option_name = option_name;
			this.option_value = option_value;
			this.option_icon = option_icon;
			this.option_icon_color = option_icon_color;
			this.singleValue = singleValue;
			this.list = list;
			this.mandatory = mandatory;
			this.read_only = read_only;
			this.help_text = help_text;
			this.width = width;
			this.unselectable = unselectable;
			this.show_type = show_type;
			this.parent_id = parent_id;
			this.date_range = date_range;
			this.calendar_connection = calendar_connection;
			this.after_months = after_months;
			this.before_months = before_months;
			if (date_range == "start") {
				model_start = value;
			}
			if (date_range == "end") {
				model_end = value;
			}
		}
	}

	[Serializable]
	public class Deletes {
		public int processGroupId;
		public List<SelectedRow> selectedRows;
	}
}