﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.financials {
	[Route("v1/features/[controller]")]
	public class FinancialsRest : RestBase {
		private FinancialsModel model;

		[HttpGet("{project_item_id}/{process_group_id}/{archive_date}/{current_date}")]
		public Data GetData(int project_item_id, int process_group_id, DateTime? archive_date, DateTime current_date) {
			model = new FinancialsModel(instance, currentSession, project_item_id, process_group_id, archive_date);
			return model.GetData(current_date);
		}

		[HttpPut("{project_item_id}/{process_group_id}/{archive_date}")]
		public LastModified Save(int project_item_id, int process_group_id, DateTime? archive_date, [FromBody]List<SaveRow> save_rows) {
			model = new FinancialsModel(instance, currentSession, project_item_id, process_group_id, archive_date);
			return model.Save(save_rows);
		}

		[HttpGet("{project_item_id}/{process_group_id}/{archive_date}/{category_item_id}/{current_date}")]
		public Row AddRow(int project_item_id, int process_group_id, DateTime? archive_date, int category_item_id, DateTime current_date) {
			model = new FinancialsModel(instance, currentSession, project_item_id, process_group_id, archive_date);
			return model.AddRow(category_item_id, current_date);
		}

		[HttpPost("{project_item_id}/{process_group_id}/{archive_date}")]
		public LastModified DeleteRows(int project_item_id, int process_group_id, DateTime? archive_date, [FromBody]List<SelectedRow> seleted_rows) {
			model = new FinancialsModel(instance, currentSession, project_item_id, process_group_id, archive_date);
			return model.DeleteRows(seleted_rows);
		}

		[HttpPost("deletes")]
		public Dictionary<int, LastModified> DeleteMuutipleRows([FromBody] Dictionary<int, Deletes> deletes) {
			var result = new Dictionary<int, LastModified>();
			foreach (var delete in deletes) {
				model = new FinancialsModel(instance, currentSession, delete.Key, delete.Value.processGroupId, null);
				result.Add(delete.Key, model.DeleteRows(delete.Value.selectedRows));
			}
			return result;
		}
	}
}