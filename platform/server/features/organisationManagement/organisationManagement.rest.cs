﻿using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.core;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.organisationManagement {
	[Route("v1/settings/[controller]")]
	public class OrganisationManagement : RestBase {
		private new readonly string process;

		public OrganisationManagement() {
			process = "organisation";
		}

		[HttpGet("")]
		public Dictionary<string, List<object>> Get() {
			var result = new Dictionary<string, List<object>>();
			var home = new List<object>();
			var partners = new List<object>();
			var subcontractors = new List<object>();

			var organisationManagement = new OrganisationManagementData(instance, process, currentSession);
			var p = new ItemBase(instance, "organisation", currentSession);
			foreach (var itemId in p.GetItemIds()) {
				var counts = organisationManagement.GetOrganisationManagementOrganisations(itemId);
				var retval = p.GetItem(Conv.ToInt(itemId));
				retval.Add("UserCount", counts["UserCount"]);
				retval.Add("CompetencyCount", counts["CompetencyCount"]);
				if ((int) retval["type"] == 1) {
					subcontractors.Add(retval);
				} else if ((int) retval["type"] == 2) {
					partners.Add(retval);
				} else {
					home.Add(retval);
				}
			}

			result.Add("home", home);
			result.Add("partners", partners);
			result.Add("subcontractors", subcontractors);

			return result;
		}

		[HttpGet("{selectedItemId}")]
		public List<int> Get(int selectedItemId) {
			var organisationManagement = new OrganisationManagementData(instance, process, currentSession);
			return organisationManagement.GetOrganisationManagementUsers(selectedItemId);
		}

		[HttpGet("competency/{selectedItemId}")]
		public List<int> Get(string competency, int selectedItemId) {
			var organisationManagement = new OrganisationManagementData(instance, process, currentSession);
			return organisationManagement.GetOrganisationManagementCompetency(selectedItemId);
		}

		[HttpGet("{process}/order/{orderCol}")]
		public List<Dictionary<string, object>> Get(string process, string orderCol) {
			var result = new List<Dictionary<string, object>>();
			var p = new ItemBase(instance, process, currentSession);
			foreach (var itemId in p.GetItemIds("list_item")) {
				result.Add(p.GetItem(Conv.ToInt(itemId)));
			}

			return result;
		}


		[HttpPost("{selectedItemId}")]
		public List<Dictionary<string, object>> Post(int selectedItemId,
			[FromBody] List<Dictionary<string, object>> users) {
			var organisationManagement = new OrganisationManagementData(instance, process, currentSession);
			return organisationManagement.InsertOrganisationManagementUsers(selectedItemId, users);
		}


		[HttpDelete("{type}/{deleteIds}")]
		public void Delete(string type, string deleteIds) {
			var organisationManagement = new OrganisationManagementData(instance, process, currentSession);
			foreach (var id in deleteIds.Split(',')) {
				organisationManagement.DeleteOrganisationManagement(type, Conv.ToInt(id));
			}
		}
	}

	[Route("v1/settings/[controller]")]
	public class OrganisationManagementCostCenter : RestBase {
		private new readonly string process;

		public OrganisationManagementCostCenter() {
			process = "organisation";
		}

		[HttpGet("")]
		public Dictionary<int, object> Get() {
			var result = new Dictionary<int, object>();
			var p = new ItemBase(instance, "list_location", currentSession);
			foreach (var locationId in p.GetItemIds("list_item")) {
				var location = new Dictionary<int, object>();
				var ib = new ItemBase(instance, "list_function", currentSession);
				foreach (var functionId in ib.GetItemIds("list_item")) {
					var ib2 = new ItemBase(instance, "list_cost_center", currentSession);
					var dataVals = new List<DataVal>();
					dataVals.Add(new DataVal("location_id", locationId));
					dataVals.Add(new DataVal("function_id", functionId));

					var itemId = ib2.GetItemIdByColumn(dataVals);
					if (itemId > 0) {
						location.Add(functionId, ib2.GetItem(itemId));
					} else {
						var newId = ib2.InsertItemRow();
						var data = ib2.GetItem(newId);
						var locationData = p.GetItem(locationId);
						var functionData = ib.GetItem(functionId);
						data["location_id"] = locationId;
						data["function_id"] = functionId;
						data["list_item"] = locationData["list_item"] + " - " + functionData["list_item"];
						location.Add(functionId, ib2.SaveItem(data));
					}
				}

				result.Add(locationId, location);
			}

			return result;
		}


		[HttpPost("{selectedItemId}")]
		public List<Dictionary<string, object>> Post(int selectedItemId,
			[FromBody] List<Dictionary<string, object>> users) {
			var organisationManagement = new OrganisationManagementData(instance, process, currentSession);
			organisationManagement.InsertOrganisationManagementUsers(selectedItemId, users);
			return users;
		}
	}
}