﻿using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.database;
using Keto5.x.platform.server.processes.itemColumns;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.features.organisationManagement {

	public class OrganisationManagementData : ConnectionBase {
		private readonly AuthenticatedSession session;

		public OrganisationManagementData(string instance, string process, AuthenticatedSession session) : base(instance, session) {
			this.instance = instance;
			this.session = session;
		}


		public Dictionary<string, object> GetOrganisationManagementOrganisations(int item_id) {
			var ic = new ItemColumns(instance, "user", session);
			var competency = ic.GetItemColumnByName("competency");
			var organisation = ic.GetItemColumnByName("organisation");

			var result = new Dictionary<string, object>();
			SetDBParam(SqlDbType.Int, "@organisation_id", item_id);
			SetDBParam(SqlDbType.Int, "@organisation_column_id", organisation.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@competency_column_id", competency.ItemColumnId);
			var UserCount = Conv.ToInt(db.ExecuteScalar("SELECT COUNT(item_id) FROM item_data_process_selections WHERE selected_item_id = @organisation_id AND item_column_id = @organisation_column_id", DBParameters));
			var CompetencyCount = Conv.ToInt(db.ExecuteScalar("SELECT COUNT(DISTINCT(selected_item_id)) FROM item_data_process_selections WHERE item_id IN (SELECT item_id FROM item_data_process_selections AS idps WHERE idps.selected_item_id = @organisation_id AND idps.item_column_id = @organisation_column_id) AND item_column_id = @competency_column_id", DBParameters));
			result.Add("UserCount", UserCount);
			result.Add("CompetencyCount", CompetencyCount);
			return result;
		}

		public List<int> GetOrganisationManagementUsers(int selected_item_id) {
			var result = new List<int>();
			var ic = new ItemColumns(instance, "user", session);
			var column = ic.GetItemColumnByName("organisation");

			SetDBParam(SqlDbType.Int, "@item_column_id", column.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@selected_item_id", selected_item_id);
			foreach (DataRow row in db.GetDatatable("SELECT item_id FROM item_data_process_selections WHERE selected_item_id = @selected_item_id AND item_column_id = @item_column_id", DBParameters).Rows) {
				result.Add(Conv.ToInt(row["item_id"]));
			}
			return result;
		}

		public List<int> GetOrganisationManagementCompetency(int organisation_id) {
			var result = new List<int>();
			var ic = new ItemColumns(instance, "user", session);
			var competency = ic.GetItemColumnByName("competency");
			var organisation = ic.GetItemColumnByName("organisation");
			SetDBParam(SqlDbType.Int, "@organisation_id", organisation_id);
			SetDBParam(SqlDbType.Int, "@item_column_id", organisation.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@competency_id", competency.ItemColumnId);
			foreach (DataRow row in db.GetDatatable("SELECT DISTINCT(selected_item_id) FROM item_data_process_selections WHERE item_id IN (SELECT item_id FROM item_data_process_selections AS idps WHERE idps.selected_item_id = @organisation_id AND idps.item_column_id = @item_column_id) AND item_column_id = @competency_id", DBParameters).Rows) {
				result.Add(Conv.ToInt(row["selected_item_id"]));
			}
			return result;
		}

		public List<Dictionary<string, object>> InsertOrganisationManagementUsers(int selected_item_id, List<Dictionary<string, object>> users) {
			var ic = new ItemColumns(instance, "user", session);
			var column = ic.GetItemColumnByName("organisation");
			SetDBParam(SqlDbType.Int, "@item_column_id", column.ItemColumnId);
			SetDBParam(SqlDbType.Int, "@selected_item_id", selected_item_id);

			var result = new List<Dictionary<string, object>>();

			db.ExecuteDeleteQuery("DELETE FROM item_data_process_selections WHERE selected_item_id = @selected_item_id AND item_column_id = @item_column_id", DBParameters);

			foreach (var user in users) {
				SetDBParam(SqlDbType.Int, "@item_id", user["item_id"]);
				if ((int)db.ExecuteScalar("SELECT COUNT(item_id) FROM item_data_process_selections WHERE item_column_id = @item_column_id AND item_id = @item_id", DBParameters) == 0) {
					db.ExecuteInsertQuery("INSERT INTO item_data_process_selections (item_id, item_column_id, selected_item_id) VALUES (@item_id, @item_column_id, @selected_item_id)", DBParameters);
				} else if ((int)db.ExecuteScalar("SELECT COUNT(item_id) FROM item_data_process_selections WHERE item_column_id = @item_column_id AND item_id = @item_id AND selected_item_id != @selected_item_id", DBParameters) > 0) {
					result.Add(user);
				}

				Cache.Remove(instance, "idps_" + user["item_id"] + " " + column.ItemColumnId);
			}

			return result;
		}

		public void DeleteOrganisationManagement(string type, int item_id) {

			var ib = new ItemBase(instance, "list_cost_center", session);
			SetDBParam(SqlDbType.Int, "@item_id", item_id);

			if (type == "function") {
				foreach (DataRow row in db.GetDatatable("SELECT item_id FROM _" + instance + "_list_cost_center WHERE function_id = @item_id", DBParameters).Rows) {
					ib.Delete((int)row["item_id"]);
					SetDBParam(SqlDbType.Int, "@selected_item_id", (int)row["item_id"]);
					db.ExecuteDeleteQuery("DELETE FROM item_data_process_selections WHERE selected_item_id = @selected_item_id", DBParameters);
				}
			} else if (type == "location") {
				foreach (DataRow row in db.GetDatatable("SELECT item_id FROM _" + instance + "_list_cost_center WHERE location_id = @item_id", DBParameters).Rows) {
					ib.Delete((int)row["item_id"]);
					SetDBParam(SqlDbType.Int, "@selected_item_id", (int)row["item_id"]);
					db.ExecuteDeleteQuery("DELETE FROM item_data_process_selections WHERE selected_item_id = @selected_item_id", DBParameters);
				}
			}

		}
	}
}
