﻿using System;
using System.Collections.Generic;
using Keto5.x.platform.server.@base;
using Microsoft.AspNetCore.Mvc;

namespace Keto5.x.platform.server.features.businessCase3 {
    [Route("v1/features/[controller]")]
    public class BusinessCase3 : RestBase {
        [HttpGet("{parent_id}")]
        public string Get(int parent_id) {
            return Get(parent_id, null);
        }
        [HttpGet("{parent_id}/{archive_date}")]
        public string Get(int parent_id, DateTime? archive_date) {
            var r = new APIResult();

            var db = new businessCase3db(instance, "business_3", currentSession);

            r.data = db.GetQuestionsAndChecks(parent_id, archive_date);

            return success(r);
        }

        [HttpPut("{parent_id}")]
        public void Post(int parent_id, [FromBody] Dictionary<int, businessCase3db.Question> data) {
            var db = new businessCase3db(instance, "business_3", currentSession);
            db.Save(parent_id, data);

        }
    }
}
