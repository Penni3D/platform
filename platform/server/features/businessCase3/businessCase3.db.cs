﻿using System;
using System.Collections.Generic;
using System.Data;
using Keto5.x.platform.server.@base;
using Keto5.x.platform.server.common;
using Keto5.x.platform.server.security;

namespace Keto5.x.platform.server.features.businessCase3 {
	public class businessCase3db : ItemBase {
		private readonly ItemBase ib;
		private readonly AuthenticatedSession session;

		public businessCase3db(string instance, string process, AuthenticatedSession session) : base(session.instance, process, session) {
			this.session = session;
			ib = new ItemBase(session.instance, "business_3", session);
		}

		public Dictionary<int, Question> GetQuestionsAndChecks(int idea_id, DateTime? archive_date) {
			var questions = new Dictionary<int, Question>();
			ProcessBase.SetDBParam(SqlDbType.Int, "idea_id", idea_id);
			var sql = "SELECT q.item_id AS question_id, q.order_no AS question_order, q.list_item AS question_name, q.choice1_value AS choice1_value, q.choice2_value AS choice2_value, q.choice3_value AS choice3_value, q.choice1_label AS choice1_label, q.choice2_label AS choice2_label, q.choice3_label AS choice3_label, qv.comments as comments, ISNULL(qv.value,-1) AS choice, c.item_id AS check_id, c.order_no AS check_order, c.list_item AS check_name, cv.value AS check_value ";
			if (archive_date.HasValue) {
				ProcessBase.SetDBParam(SqlDbType.DateTime, "archive_date", archive_date);
				sql += "FROM get__" + session.instance + "_list_classification_questions(@archive_date) q " +
					   "LEFT JOIN get_item_subitems(@archive_date) i ON i.parent_item_id = q.item_id " +
					   "LEFT JOIN get__" + session.instance + "_list_classification_checks(@archive_date) c ON c.item_id = i.item_id " +
					   "LEFT JOIN get__" + session.instance + "_business_3(@archive_date) qv ON qv.list_item_id = q.item_id AND qv.process_item_id = @idea_id " +
					   "LEFT JOIN get__" + session.instance + "_business_3(@archive_date) cv ON cv.list_item_id = c.item_id AND cv.process_item_id = @idea_id";
			}
			else {
				sql += "FROM _" + session.instance + "_list_classification_questions q " +
					   "LEFT JOIN item_subitems i ON i.parent_item_id = q.item_id " +
					   "LEFT JOIN _" + session.instance + "_list_classification_checks c ON c.item_id = i.item_id " +
					   "LEFT JOIN _" + session.instance + "_business_3 qv ON qv.list_item_id = q.item_id AND qv.process_item_id = @idea_id " +
					   "LEFT JOIN _" + session.instance + "_business_3 cv ON cv.list_item_id = c.item_id AND cv.process_item_id = @idea_id";
			}
			foreach (DataRow row in ProcessBase.db.GetDatatable(sql, ProcessBase.DBParameters).Rows) {
				Question q;
				if (questions.ContainsKey(Conv.ToInt(row["question_id"])) == false) {
					q = new Question(Conv.ToInt(row["question_id"]), Conv.ToStr(row["question_name"]), Conv.ToInt(row["choice1_value"]), Conv.ToInt(row["choice2_value"]), Conv.ToInt(row["choice3_value"]), Conv.ToInt(row["choice"]), Conv.ToStr(row["choice1_label"]), Conv.ToStr(row["choice2_label"]), Conv.ToStr(row["choice3_label"]), Conv.ToStr(row["comments"]));
				}
				else {
					q = questions[Conv.ToInt(row["question_id"])];
				}

				if (Conv.ToInt(row["check_id"]) != 0) {
					var val = false;
					if (Conv.ToInt(row["check_value"]) == 1) val = true;
					q.AddCheck(Conv.ToInt(row["check_id"]), Conv.ToStr(row["check_name"]), val);
				}
				if (questions.ContainsKey(Conv.ToInt(row["question_id"]))) {
					questions[Conv.ToInt(row["question_id"])] = q;
				}
				else {
					questions.Add(Conv.ToInt(row["question_id"]), q);
				}
			}
			return questions;
		}

		public void Save(int idea_id, Dictionary<int, Question> questions) {
			foreach (var q in questions.Values) {
				//if (q.choice > 0) {
				Update(idea_id, q.id, q.choice, q.comments);
				//	}

				foreach (var c in q.checks) {
					var val = 0; if (c.value) val = 1;
					Update(idea_id, c.id, val);
				}
			}
		}

		private void Update(int idea_id, int list_item_id, int value, string comments = "") {
			ProcessBase.DBParameters.Clear();
			ProcessBase.SetDBParam(SqlDbType.Int, "idea_id", idea_id);
			ProcessBase.SetDBParam(SqlDbType.Int, "list_item_id", list_item_id);
			ProcessBase.SetDBParam(SqlDbType.NVarChar, "comments", comments);

			ProcessBase.SetDBParam(SqlDbType.Int, "value", value);
			if (Conv.ToInt(ProcessBase.db.ExecuteScalar("SELECT COUNT(*) FROM _" + session.instance + "_business_3 WHERE process_item_id = @idea_id AND list_item_id = @list_item_id", ProcessBase.DBParameters)) == 0) {
				//INSERT
				ProcessBase.SetDBParam(SqlDbType.Int, "item_id", ib.InsertItemRow());
				ProcessBase.db.ExecuteUpdateQuery("UPDATE _" + session.instance + "_business_3 SET process_item_id = @idea_id, list_item_id = @list_item_id, value = @value, comments = @comments WHERE item_id = @item_id", ProcessBase.DBParameters);
			}
			else {
				//UPDATE
				ProcessBase.db.ExecuteUpdateQuery("UPDATE _" + session.instance + "_business_3 SET value = @value, comments = @comments WHERE process_item_id = @idea_id AND list_item_id = @list_item_id", ProcessBase.DBParameters);
			}
		}

		[Serializable]
		public class Question {
			public List<Check> checks = new List<Check>();
			public int choice = 2;
			public int choice1;
			public string choice1_label;
			public int choice2 = 1;
			public string choice2_label;
			public int choice3 = 2;
			public string choice3_label;
			public int id;
			public string name;
			public string comments;

			public Question(int id, string name, int choice1, int choice2, int choice3, int choice, string choice1_label, string choice2_label, string choice3_label, string comments) {
				this.id = id;
				this.name = name;
				this.choice1 = choice1;
				this.choice2 = choice2;
				this.choice3 = choice3;
				this.choice1_label = choice1_label;
				this.choice2_label = choice2_label;
				this.choice3_label = choice3_label;
				this.choice = choice;
				this.comments = comments;
			}

			public void AddCheck(int id, string name, bool value) {
				var c = new Check();
				c.id = id;
				c.name = name;
				c.value = value;
				checks.Add(c);
			}
		}

		[Serializable]
		public class Check {
			public int id;
			public string name;
			public bool value;
		}
	}
}
